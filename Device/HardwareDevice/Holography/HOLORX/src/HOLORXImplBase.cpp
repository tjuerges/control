// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#include "HOLORXImplBase.h"

#include <acserr.h>
#include <acstimeC.h>

using baci::ROlong;
using baci::ROfloat;
/////////////////
// Constructor
/////////////////
HOLORXImplBase::HOLORXImplBase(const ACE_CString& name,
                               maci::ContainerServices* cs)
    :AmbDeviceImpl(name, cs)
    , PLL_STATUS_spp(this)
    , PLL_STATUS_rca(0x00001)
    , Gunn_Oscillator_Locked_value(true)
    , Synthesizer_Locked_value(true)
    , GUNN_H_VOLTAGE_spp(this)
    , GUNN_H_VOLTAGE_rca(0x00002)
    , GUNN_L_VOLTAGE_spp(this)
    , GUNN_L_VOLTAGE_rca(0x00003)
    , LO_DET_OUT_spp(this)
    , LO_DET_OUT_rca(0x00004)
    , REF_DET_OUT_spp(this)
    , REF_DET_OUT_rca(0x00005)
    , SIG_DET_OUT_spp(this)
    , SIG_DET_OUT_rca(0x00006)
    , REF_SENSE_I_spp(this)
    , REF_SENSE_I_rca(0x00007)
    , REF_SENSE_Q_spp(this)
    , REF_SENSE_Q_rca(0x00008)
    , SIG_SENSE_I_spp(this)
    , SIG_SENSE_I_rca(0x00009)
    , SIG_SENSE_Q_spp(this)
    , SIG_SENSE_Q_rca(0x0000a)
    , SUPPLY_CURRENT_spp(this)
    , SUPPLY_CURRENT_rca(0x0000b)
    , TEMP_POWER_SUPPLY_spp(this)
    , TEMP_POWER_SUPPLY_rca(0x0000c)
    , TEMP_SIG_MIX_spp(this)
    , TEMP_SIG_MIX_rca(0x0000d)
    , TEMP_REF_MIX_spp(this)
    , TEMP_REF_MIX_rca(0x0000e)
    , TEMP_29MHZ_OCXO_spp(this)
    , TEMP_29MHZ_OCXO_rca(0x0000f)
    , TEMP_95MHZ_OCXO_spp(this)
    , TEMP_95MHZ_OCXO_rca(0x00010)
    , TEMP_LOCK_BOX_spp(this)
    , TEMP_LOCK_BOX_rca(0x00011)
    , SIG_ATTENUATION_spp(this)
    , SIG_ATTENUATION_rca(0x01001)
    , REF_ATTENUATION_spp(this)
    , REF_ATTENUATION_rca(0x01002)
    , SYNTH_FREQUENCY_spp(this)
    , SYNTH_FREQUENCY_scale(0.125E6)
    , SYNTH_FREQUENCY_offset(8.73E9)
    , SYNTH_FREQUENCY_rca(0x01003)
    , DRIVE_ENABLE_spp(this)
    , DRIVE_ENABLE_rca(0x01004)
    , LOOP_ENABLE_spp(this)
    , LOOP_ENABLE_rca(0x01005)
    , NOMINAL_VOLTAGE_spp(this)
    , NOMINAL_VOLTAGE_rca(0x01006)
    , GUNN_LOOP_GAIN_spp(this)
    , GUNN_LOOP_GAIN_rca(0x01007)
    , GUNN_TUNE_RANGE_spp(this)
    , GUNN_TUNE_RANGE_rca(0x01008)
    , GUNN_SELECT_spp(this)
    , GUNN_SELECT_rca(0x01009)
    , SET_SIG_ATTENUATION_rca(0x01001)
    , SET_REF_ATTENUATION_rca(0x01002)
    , SET_SYNTH_FREQUENCY_scale(.125E6)
    , SET_SYNTH_FREQUENCY_offset(8.73e9)
    , SET_SYNTH_FREQUENCY_rca(0x01003)
    , SET_DRIVE_ENABLE_ON_rca(0x01004)
    , SET_DRIVE_ENABLE_OFF_rca(0x01004)
    , SET_LOOP_ENABLE_CLOSE_rca(0x01005)
    , SET_LOOP_ENABLE_OPEN_rca(0x01005)
    , SET_NOMINAL_VOLTAGE_rca(0x01006)
    , SET_GUNN_LOOP_GAIN_rca(0x01007)
    , SET_GUNN_TUNE_RANGE_rca(0x01008)
    , SET_GUNN_SELECT_HIGH_rca(0x01009)
    , SET_GUNN_SELECT_LOW_rca(0x01009)
{
    ACS_TRACE("HOLORXImplBase::HOLORXImplBase");
}

////////////////
// Destructor
////////////////
HOLORXImplBase::~HOLORXImplBase() {
    ACS_TRACE("HOLORXImplBase:~HOLORXImplBase");
}

/////////////////////////////
// Lifecycle Methods
/////////////////////////////
void HOLORXImplBase::initialize() {
    ACS_TRACE("HOLORXImplBase::initialize");

    // Call the base class implementation so it can set up the common properties
    AmbDeviceImpl::initialize();

    // create the properties specific to the HOLORX
    try {
        const ACE_CString nameWithSep = cdbName_m + ":";
        {
            const ACE_CString propName = "PLL_STATUS";
            PLL_STATUS_spp =
                new ROlong(nameWithSep + propName, getComponent(),
                           new PLL_STATUS_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "GUNN_H_VOLTAGE";
            GUNN_H_VOLTAGE_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new GUNN_H_VOLTAGE_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "GUNN_L_VOLTAGE";
            GUNN_L_VOLTAGE_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new GUNN_L_VOLTAGE_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "LO_DET_OUT";
            LO_DET_OUT_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new LO_DET_OUT_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "REF_DET_OUT";
            REF_DET_OUT_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new REF_DET_OUT_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "SIG_DET_OUT";
            SIG_DET_OUT_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new SIG_DET_OUT_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "REF_SENSE_I";
            REF_SENSE_I_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new REF_SENSE_I_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "REF_SENSE_Q";
            REF_SENSE_Q_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new REF_SENSE_Q_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "SIG_SENSE_I";
            SIG_SENSE_I_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new SIG_SENSE_I_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "SIG_SENSE_Q";
            SIG_SENSE_Q_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new SIG_SENSE_Q_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "SUPPLY_CURRENT";
            SUPPLY_CURRENT_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new SUPPLY_CURRENT_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "TEMP_POWER_SUPPLY";
            TEMP_POWER_SUPPLY_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new TEMP_POWER_SUPPLY_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "TEMP_SIG_MIX";
            TEMP_SIG_MIX_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new TEMP_SIG_MIX_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "TEMP_REF_MIX";
            TEMP_REF_MIX_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new TEMP_REF_MIX_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "TEMP_29MHZ_OCXO";
            TEMP_29MHZ_OCXO_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new TEMP_29MHZ_OCXO_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "TEMP_95MHZ_OCXO";
            TEMP_95MHZ_OCXO_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new TEMP_95MHZ_OCXO_RODevIO(this), true);
        }
        {
            const ACE_CString propName = "TEMP_LOCK_BOX";
            TEMP_LOCK_BOX_spp =
                new ROfloat(nameWithSep + propName, getComponent(),
                            new TEMP_LOCK_BOX_RODevIO(this), true);
        }
    } catch (ControlExceptions::CDBErrorExImpl& ex) {
        // getElement can throw this exception
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
                                                   "HOLORXImpl::startInitialize");
    }
}

///////////////////////////////////
// MonitorPoint: PLL_STATUS
//
// Latched status of the PLL in the previous 48ms (the status is updated
// to the current at the 48ms edge and the bad statuses are latched
// until the new 48ms pulse comes along)
///////////////////////////////////

/**
 * Get the current value of PLL_STATUS as a BACI property.
 */

ACS::ROlong_ptr HOLORXImplBase::PLL_STATUS() {

    ACS_TRACE("HOLORXImplBase::PLL_STATUS");

    ACS::ROlong_var prop = ACS::ROlong::_narrow(PLL_STATUS_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Long  HOLORXImplBase::PLL_STATUS_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Long x = device->get_PLL_STATUS(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of PLL_STATUS from the device.
 */

unsigned char HOLORXImplBase::get_PLL_STATUS(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_PLL_STATUS()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(PLL_STATUS_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_PLL_STATUS()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    unsigned char rtn = raw;

    // Set Gunn_Oscillator_Locked_value.

    Gunn_Oscillator_Locked_value = (raw & 0x01) != 0;

    // Set Synthesizer_Locked_value.

    Synthesizer_Locked_value = (raw & 0x02) != 0;

    // Set REF_Fault_value.

    REF_Fault_value = (raw & 0x04) != 0;

    // Set IF_Fault_value.

    IF_Fault_value = (raw & 0x08) != 0;

    // Set Frame_CRC_Error_value.

    Frame_CRC_Error_value = (raw & 0x10) != 0;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: GUNN_H_VOLTAGE
//
// Get tuning voltage of high band Gunn Oscillator.
///////////////////////////////////

/**
 * Get the current value of GUNN_H_VOLTAGE as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::GUNN_H_VOLTAGE() {

    ACS_TRACE("HOLORXImplBase::GUNN_H_VOLTAGE");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(GUNN_H_VOLTAGE_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::GUNN_H_VOLTAGE_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_GUNN_H_VOLTAGE(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of GUNN_H_VOLTAGE from the device.
 */

float HOLORXImplBase::get_GUNN_H_VOLTAGE(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_H_VOLTAGE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(GUNN_H_VOLTAGE_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_H_VOLTAGE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: GUNN_L_VOLTAGE
//
// Get tuning voltage of low band Gunn Oscillator.
///////////////////////////////////

/**
 * Get the current value of GUNN_L_VOLTAGE as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::GUNN_L_VOLTAGE() {

    ACS_TRACE("HOLORXImplBase::GUNN_L_VOLTAGE");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(GUNN_L_VOLTAGE_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::GUNN_L_VOLTAGE_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_GUNN_L_VOLTAGE(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of GUNN_L_VOLTAGE from the device.
 */

float HOLORXImplBase::get_GUNN_L_VOLTAGE(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_L_VOLTAGE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(GUNN_L_VOLTAGE_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_L_VOLTAGE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: LO_DET_OUT
//
// Get local oscillator (LO) detector level.
///////////////////////////////////

/**
 * Get the current value of LO_DET_OUT as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::LO_DET_OUT() {

    ACS_TRACE("HOLORXImplBase::LO_DET_OUT");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(LO_DET_OUT_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::LO_DET_OUT_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_LO_DET_OUT(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of LO_DET_OUT from the device.
 */

float HOLORXImplBase::get_LO_DET_OUT(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_LO_DET_OUT()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(LO_DET_OUT_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_LO_DET_OUT()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: REF_DET_OUT
//
// Get reference intermediate frequency (IF) detector level.
///////////////////////////////////

/**
 * Get the current value of REF_DET_OUT as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::REF_DET_OUT() {

    ACS_TRACE("HOLORXImplBase::REF_DET_OUT");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(REF_DET_OUT_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::REF_DET_OUT_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_REF_DET_OUT(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of REF_DET_OUT from the device.
 */

float HOLORXImplBase::get_REF_DET_OUT(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_DET_OUT()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(REF_DET_OUT_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_DET_OUT()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: SIG_DET_OUT
//
// Get signal intermediate frequency (IF) detector level.
///////////////////////////////////

/**
 * Get the current value of SIG_DET_OUT as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::SIG_DET_OUT() {

    ACS_TRACE("HOLORXImplBase::SIG_DET_OUT");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(SIG_DET_OUT_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::SIG_DET_OUT_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_SIG_DET_OUT(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of SIG_DET_OUT from the device.
 */

float HOLORXImplBase::get_SIG_DET_OUT(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_DET_OUT()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SIG_DET_OUT_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_DET_OUT()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: REF_SENSE_I
//
// Get the RMS voltage of the Ref. I channel at the Anti-Aliasing module
///////////////////////////////////

/**
 * Get the current value of REF_SENSE_I as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::REF_SENSE_I() {

    ACS_TRACE("HOLORXImplBase::REF_SENSE_I");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(REF_SENSE_I_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::REF_SENSE_I_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_REF_SENSE_I(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of REF_SENSE_I from the device.
 */

float HOLORXImplBase::get_REF_SENSE_I(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_SENSE_I()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(REF_SENSE_I_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_SENSE_I()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: REF_SENSE_Q
//
// Get the RMS voltage of the Ref. Q channel at the Anti-Aliasing module
///////////////////////////////////

/**
 * Get the current value of REF_SENSE_Q as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::REF_SENSE_Q() {

    ACS_TRACE("HOLORXImplBase::REF_SENSE_Q");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(REF_SENSE_Q_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::REF_SENSE_Q_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_REF_SENSE_Q(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of REF_SENSE_Q from the device.
 */

float HOLORXImplBase::get_REF_SENSE_Q(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_SENSE_Q()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(REF_SENSE_Q_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_SENSE_Q()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: SIG_SENSE_I
//
// Get the RMS voltage of the Signal I channel at the Anti-Aliasing module
///////////////////////////////////

/**
 * Get the current value of SIG_SENSE_I as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::SIG_SENSE_I() {

    ACS_TRACE("HOLORXImplBase::SIG_SENSE_I");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(SIG_SENSE_I_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::SIG_SENSE_I_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_SIG_SENSE_I(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of SIG_SENSE_I from the device.
 */

float HOLORXImplBase::get_SIG_SENSE_I(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_SENSE_I()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SIG_SENSE_I_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_SENSE_I()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: SIG_SENSE_Q
//
// Get the RMS voltage of the Signal Q channel at the Anti-Aliasing module
// output.
///////////////////////////////////

/**
 * Get the current value of SIG_SENSE_Q as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::SIG_SENSE_Q() {

    ACS_TRACE("HOLORXImplBase::SIG_SENSE_Q");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(SIG_SENSE_Q_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::SIG_SENSE_Q_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_SIG_SENSE_Q(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of SIG_SENSE_Q from the device.
 */

float HOLORXImplBase::get_SIG_SENSE_Q(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_SENSE_Q()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SIG_SENSE_Q_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_SENSE_Q()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: SUPPLY_CURRENT
//
// Get the supply current in A for the entire holography receiver.
///////////////////////////////////

/**
 * Get the current value of SUPPLY_CURRENT as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::SUPPLY_CURRENT() {

    ACS_TRACE("HOLORXImplBase::SUPPLY_CURRENT");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(SUPPLY_CURRENT_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::SUPPLY_CURRENT_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_SUPPLY_CURRENT(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of SUPPLY_CURRENT from the device.
 */

float HOLORXImplBase::get_SUPPLY_CURRENT(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SUPPLY_CURRENT()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(SUPPLY_CURRENT_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SUPPLY_CURRENT()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: TEMP_POWER_SUPPLY
//
// Get the temperature reading in degC from temperature sensor by the
// power supply.
///////////////////////////////////

/**
 * Get the current value of TEMP_POWER_SUPPLY as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::TEMP_POWER_SUPPLY() {

    ACS_TRACE("HOLORXImplBase::TEMP_POWER_SUPPLY");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(TEMP_POWER_SUPPLY_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::TEMP_POWER_SUPPLY_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_TEMP_POWER_SUPPLY(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of TEMP_POWER_SUPPLY from the device.
 */

float HOLORXImplBase::get_TEMP_POWER_SUPPLY(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_POWER_SUPPLY()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(TEMP_POWER_SUPPLY_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_POWER_SUPPLY()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: TEMP_SIG_MIX
//
// Get the temperature reading in degC from temperature sensor by the
// mixer in signal channel.
///////////////////////////////////

/**
 * Get the current value of TEMP_SIG_MIX as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::TEMP_SIG_MIX() {

    ACS_TRACE("HOLORXImplBase::TEMP_SIG_MIX");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(TEMP_SIG_MIX_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::TEMP_SIG_MIX_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_TEMP_SIG_MIX(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of TEMP_SIG_MIX from the device.
 */

float HOLORXImplBase::get_TEMP_SIG_MIX(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_SIG_MIX()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(TEMP_SIG_MIX_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_SIG_MIX()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: TEMP_REF_MIX
//
// Get the temperature reading in degC from temperature sensor by the
// mixer in reference channel.
///////////////////////////////////

/**
 * Get the current value of TEMP_REF_MIX as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::TEMP_REF_MIX() {

    ACS_TRACE("HOLORXImplBase::TEMP_REF_MIX");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(TEMP_REF_MIX_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::TEMP_REF_MIX_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_TEMP_REF_MIX(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of TEMP_REF_MIX from the device.
 */

float HOLORXImplBase::get_TEMP_REF_MIX(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_REF_MIX()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(TEMP_REF_MIX_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_REF_MIX()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: TEMP_29MHZ_OCXO
//
// Get the temperature reading in degC from temperature sensor by the
// 29 MHz oven-controlled crystal oscillator.
///////////////////////////////////

/**
 * Get the current value of TEMP_29MHZ_OCXO as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::TEMP_29MHZ_OCXO() {

    ACS_TRACE("HOLORXImplBase::TEMP_29MHZ_OCXO");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(TEMP_29MHZ_OCXO_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::TEMP_29MHZ_OCXO_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_TEMP_29MHZ_OCXO(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of TEMP_29MHZ_OCXO from the device.
 */

float HOLORXImplBase::get_TEMP_29MHZ_OCXO(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_29MHZ_OCXO()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(TEMP_29MHZ_OCXO_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_29MHZ_OCXO()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: TEMP_95MHZ_OCXO
//
// Get the temperature reading in degC from temperature sensor by the
// 95 MHz oven-controlled crystal oscillator.
///////////////////////////////////

/**
 * Get the current value of TEMP_95MHZ_OCXO as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::TEMP_95MHZ_OCXO() {

    ACS_TRACE("HOLORXImplBase::TEMP_95MHZ_OCXO");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(TEMP_95MHZ_OCXO_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::TEMP_95MHZ_OCXO_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_TEMP_95MHZ_OCXO(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of TEMP_95MHZ_OCXO from the device.
 */

float HOLORXImplBase::get_TEMP_95MHZ_OCXO(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_95MHZ_OCXO()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(TEMP_95MHZ_OCXO_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_95MHZ_OCXO()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: TEMP_LOCK_BOX
//
// Get the temperature reading in degC from temperature sensor by the
// lock box.
///////////////////////////////////

/**
 * Get the current value of TEMP_LOCK_BOX as a BACI property.
 */

ACS::ROfloat_ptr HOLORXImplBase::TEMP_LOCK_BOX() {

    ACS_TRACE("HOLORXImplBase::TEMP_LOCK_BOX");

    ACS::ROfloat_var prop = ACS::ROfloat::_narrow(TEMP_LOCK_BOX_spp->getCORBAReference());

    return prop._retn();
}

/*
 * Implement the read method in the ACS DevIO class for this BACI property.
 */

CORBA::Float  HOLORXImplBase::TEMP_LOCK_BOX_RODevIO::read(unsigned long long & timestamp) {

    try {

        CORBA::Float x = device->get_TEMP_LOCK_BOX(timestamp);

        return x;
    } catch (ControlExceptions::CAMBErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    } catch (ControlExceptions::INACTErrorExImpl & ex) {
        throw ACSErr::ACSbaseExImpl(ex);
    }
}

/**
 * Get the current value of TEMP_LOCK_BOX from the device.
 */

float HOLORXImplBase::get_TEMP_LOCK_BOX(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_LOCK_BOX()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex;
    }
    AmbRelativeAddr  rca(TEMP_LOCK_BOX_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_TEMP_LOCK_BOX()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    float rtn = raw;

    return rtn;
}

///////////////////////////////////
// MonitorPoint: SIG_ATTENUATION
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of SIG_ATTENUATION from the device.
 */

CORBA::Long HOLORXImplBase::get_SIG_ATTENUATION(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_ATTENUATION()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SIG_ATTENUATION_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SIG_ATTENUATION()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    CORBA::Long rtn = static_cast<CORBA::Long>(raw);

    return rtn;
}

///////////////////////////////////
// MonitorPoint: REF_ATTENUATION
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of REF_ATTENUATION from the device.
 */

CORBA::Long HOLORXImplBase::get_REF_ATTENUATION(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_ATTENUATION()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(REF_ATTENUATION_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_REF_ATTENUATION()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    CORBA::Long rtn = static_cast<CORBA::Long>(raw);

    return rtn;
}

///////////////////////////////////
// MonitorPoint: SYNTH_FREQUENCY
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of SYNTH_FREQUENCY from the device.
 */

CORBA::Double HOLORXImplBase::get_SYNTH_FREQUENCY(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SYNTH_FREQUENCY()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SYNTH_FREQUENCY_rca);
    AmbDataLength_t length(2);

    unsigned short raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SYNTH_FREQUENCY()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    double rtn =  convert_SYNTH_FREQUENCY(raw);

    return rtn;
}

/**
 * Convert the raw value to a world value for this device.
 */
double HOLORXImplBase::convert_SYNTH_FREQUENCY(unsigned short raw) {

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[1]; p[1] = tmp;

#endif

    return SYNTH_FREQUENCY_scale * (double)raw + SYNTH_FREQUENCY_offset;

}

///////////////////////////////////
// MonitorPoint: DRIVE_ENABLE
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of DRIVE_ENABLE from the device.
 */

CORBA::Long HOLORXImplBase::get_DRIVE_ENABLE(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_DRIVE_ENABLE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(DRIVE_ENABLE_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_DRIVE_ENABLE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    CORBA::Long rtn = static_cast<CORBA::Long>(raw);

    return rtn;
}

///////////////////////////////////
// MonitorPoint: LOOP_ENABLE
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of LOOP_ENABLE from the device.
 */

CORBA::Long HOLORXImplBase::get_LOOP_ENABLE(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_LOOP_ENABLE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(LOOP_ENABLE_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_LOOP_ENABLE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    CORBA::Long rtn = static_cast<CORBA::Long>(raw);

    return rtn;
}

///////////////////////////////////
// MonitorPoint: NOMINAL_VOLTAGE
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of NOMINAL_VOLTAGE from the device.
 */

CORBA::Float HOLORXImplBase::get_NOMINAL_VOLTAGE(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_NOMINAL_VOLTAGE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(NOMINAL_VOLTAGE_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_NOMINAL_VOLTAGE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    CORBA::Float rtn = static_cast<CORBA::Float>(raw);

    return rtn;
}

///////////////////////////////////
// MonitorPoint: GUNN_LOOP_GAIN
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of GUNN_LOOP_GAIN from the device.
 */

CORBA::Float HOLORXImplBase::get_GUNN_LOOP_GAIN(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_LOOP_GAIN()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(GUNN_LOOP_GAIN_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_LOOP_GAIN()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    CORBA::Float rtn = static_cast<CORBA::Float>(raw);

    return rtn;
}

///////////////////////////////////
// MonitorPoint: GUNN_TUNE_RANGE
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of GUNN_TUNE_RANGE from the device.
 */

CORBA::Float HOLORXImplBase::get_GUNN_TUNE_RANGE(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_TUNE_RANGE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(GUNN_TUNE_RANGE_rca);
    AmbDataLength_t length(4);

    float raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_TUNE_RANGE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    CORBA::Float rtn = static_cast<CORBA::Float>(raw);

    return rtn;
}

///////////////////////////////////
// MonitorPoint: GUNN_SELECT
//
// See control point.
///////////////////////////////////

/**
 * Get the current value of GUNN_SELECT from the device.
 */

CORBA::Long HOLORXImplBase::get_GUNN_SELECT(Time & timestamp) {


    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_SELECT()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(GUNN_SELECT_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    monitor(rca, length, (AmbDataMem_t *)&raw,
            &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_GUNN_SELECT()");
        ex.addData("ErrorCode",status);
        throw ex;
    }

    CORBA::Long rtn = static_cast<CORBA::Long>(raw);

    return rtn;
}

/**
 * Control Point: SET_SIG_ATTENUATION
 *
 * Set attenuation of signal channel.
 */
void HOLORXImplBase::SET_SIG_ATTENUATION(CORBA::Long world) {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_SIG_ATTENUATION()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_SIG_ATTENUATION_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = world;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_SIG_ATTENUATION()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_REF_ATTENUATION
 *
 * Set attenuation of reference channel.
 */
void HOLORXImplBase::SET_REF_ATTENUATION(CORBA::Long world) {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_REF_ATTENUATION()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_REF_ATTENUATION_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = world;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_REF_ATTENUATION()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_SYNTH_FREQUENCY
 *
 * Set holography synthesizer frequency.  unsigned integer in range [0..5960]
 representing the synthesizer frequency in range 8.730 – 9.475 GHz
 in increments of 0.125 MHz.
*/
void HOLORXImplBase::SET_SYNTH_FREQUENCY(CORBA::Double world) {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_SYNTH_FREQUENCY()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_SYNTH_FREQUENCY_rca);
    AmbDataLength_t length(2);

    unsigned short raw;

    acstime::Epoch cmdTime;

    raw = convert_SET_SYNTH_FREQUENCY(world);

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[1]; p[1] = tmp;

#endif

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_SYNTH_FREQUENCY()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Convert the world value to a raw value for this device.
 */
unsigned short HOLORXImplBase::convert_SET_SYNTH_FREQUENCY(double world) {
    return (unsigned short)((world - SET_SYNTH_FREQUENCY_offset) / SET_SYNTH_FREQUENCY_scale);
}

/**
 * Control Point: SET_DRIVE_ENABLE_ON
 *
 * Turn the Gunn oscillator bias on.
 */
void HOLORXImplBase::SET_DRIVE_ENABLE_ON() {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_DRIVE_ENABLE_ON()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_DRIVE_ENABLE_ON_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = 1;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_DRIVE_ENABLE_ON()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_DRIVE_ENABLE_OFF
 *
 * Turn the Gunn oscillator bias off.
 */
void HOLORXImplBase::SET_DRIVE_ENABLE_OFF() {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_DRIVE_ENABLE_OFF()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_DRIVE_ENABLE_OFF_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = 0;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_DRIVE_ENABLE_OFF()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_LOOP_ENABLE_CLOSE
 *
 * close the phased lock loop to the Gunn oscillator
 */
void HOLORXImplBase::SET_LOOP_ENABLE_CLOSE() {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_LOOP_ENABLE_CLOSE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_LOOP_ENABLE_CLOSE_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = 1;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_LOOP_ENABLE_CLOSE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_LOOP_ENABLE_OPEN
 *
 * open the phased lock loop to the Gunn oscillator
 */
void HOLORXImplBase::SET_LOOP_ENABLE_OPEN() {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_LOOP_ENABLE_OPEN()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_LOOP_ENABLE_OPEN_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = 0;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_LOOP_ENABLE_OPEN()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_NOMINAL_VOLTAGE
 *
 * To set the output voltage of the 12-bit multiplying DAC (LTC8043),
 which in turn will determine the bias voltage to the Gunn oscillator.
*/
void HOLORXImplBase::SET_NOMINAL_VOLTAGE(CORBA::Float world) {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_NOMINAL_VOLTAGE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_NOMINAL_VOLTAGE_rca);
    AmbDataLength_t length(4);

    float raw;

    acstime::Epoch cmdTime;

    raw = world;

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_NOMINAL_VOLTAGE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_GUNN_LOOP_GAIN
 *
 * Set the loop gain of the phased locked Gunn oscillator
 */
void HOLORXImplBase::SET_GUNN_LOOP_GAIN(CORBA::Float world) {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_GUNN_LOOP_GAIN()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_GUNN_LOOP_GAIN_rca);
    AmbDataLength_t length(4);

    float raw;

    acstime::Epoch cmdTime;

    raw = world;

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_GUNN_LOOP_GAIN()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_GUNN_TUNE_RANGE
 *
 * Set tuning range of Gunn oscillator
 */
void HOLORXImplBase::SET_GUNN_TUNE_RANGE(CORBA::Float world) {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_GUNN_TUNE_RANGE()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_GUNN_TUNE_RANGE_rca);
    AmbDataLength_t length(4);

    float raw;

    acstime::Epoch cmdTime;

    raw = world;

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.

    char * p = (char *)&raw;

    char tmp = p[0]; p[0] = p[3]; p[3] = tmp;
    tmp = p[1]; p[1] = p[2]; p[2] = tmp;

#endif

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_GUNN_TUNE_RANGE()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_GUNN_SELECT_HIGH
 *
 * Select high-band Gunn (GUNN_H)
 */
void HOLORXImplBase::SET_GUNN_SELECT_HIGH() {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_GUNN_SELECT_HIGH()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_GUNN_SELECT_HIGH_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = 1;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_GUNN_SELECT_HIGH()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}

/**
 * Control Point: SET_GUNN_SELECT_LOW
 *
 * Select low-band Gunn (GUNN_L)
 */
void HOLORXImplBase::SET_GUNN_SELECT_LOW() {
    if (!isReady() || inErrorState()) {
        ControlExceptions::INACTErrorExImpl ex = ControlExceptions::INACTErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::get_SET_GUNN_SELECT_LOW()");
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        throw ex.getINACTErrorEx();
    }
    AmbRelativeAddr  rca(SET_GUNN_SELECT_LOW_rca);
    AmbDataLength_t length(1);

    unsigned char raw;

    acstime::Epoch cmdTime;

    raw = 0;

    cmdTime.value = 0;

    Time timestamp;
    sem_t synchLock;
    AmbErrorCode_t status;
    sem_init(&synchLock,0,0);
    commandTE(cmdTime.value, rca, length, (AmbDataMem_t *)&raw, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);
    if (status != AMBERR_NOERR) {
        ControlExceptions::CAMBErrorExImpl ex = ControlExceptions::CAMBErrorExImpl(__FILE__,__LINE__,"HOLORXImplBase::SET_GUNN_SELECT_LOW()");
        ex.addData("ErrorCode",status);
        throw ex;
    }
}
