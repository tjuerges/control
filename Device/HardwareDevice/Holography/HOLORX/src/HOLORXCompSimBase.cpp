// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) European Southern Observatory, 2008
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "HOLORXCompSimBase.h"

using Control::HoloRxImpl;

HOLORXCompSimBase::HOLORXCompSimBase(const ACE_CString& name, 
                                     maci::ContainerServices* pCS)
    :HoloRxImpl(name, pCS)
{
    ACS_TRACE(__func__);
}

HOLORXCompSimBase::~HOLORXCompSimBase()
{
    ACS_TRACE(__func__);
}

void HOLORXCompSimBase::getDeviceUniqueId(std::string& deviceID) {
    ACS_TRACE(__func__);

    // Getting the data from GET_SERIAL_NUMBER
    AmbRelativeAddr rca(0x00000);
    AmbDataLength_t length(1);
    AmbDataMem_t rawBytes[8];
    sem_t synchLock;
    ACS::Time timestamp;
    AmbErrorCode_t status;

    sem_init(&synchLock, 0, 0);
    monitor(rca, length, rawBytes, &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    /*
     * Setting the correct number to deviceID
     * If not implemented in the simulation set it to 0x0
     */
    if(status != AMBERR_NOERR) {
        deviceID = "0x0000000000000000";
    }
    else {
        std::ostringstream uniqueId;
        int idx;
        uniqueId << "0x" << std::hex;
        for (idx=0; idx < 8; idx++) {
            uniqueId << std::setw(2) << std::setfill('0')
                     << static_cast<short>(rawBytes[idx]);
        }
        deviceID =  uniqueId.str();
    }
}

void HOLORXCompSimBase::
setSimValue(CORBA::Long rca, 
            const Control::HOLORXCompSimBase::longSeq& data) {
    ACS_TRACE(__func__);

    AmbRelativeAddr RCA = static_cast<AmbRelativeAddr>(rca);
    AmbDataLength_t messageLength = 
        static_cast<AmbDataLength_t>(data.length());
    AmbDataMem_t message[8];

    if (data.length() > 8) messageLength = 8;

    for (unsigned short index=0; index<messageLength; index++) {
        message[index] = static_cast<AmbDataMem_t>(data[index]);
    }
    
    ACS::Time timestamp(0ULL);
    sem_t synchLock;
    AmbErrorCode_t status;

    if (sem_init(&synchLock, 0,0) == -1) {
        ControlExceptions::SimErrorExImpl ex(__FILE__, __LINE__, __func__);
        throw ex.getSimErrorEx();
    }

    try {
        simulationIf_m.command(RCA, messageLength, message, &synchLock, 
                               &timestamp, &status);
    }
    catch (const ControlExceptions::CAMBErrorExImpl& _ex) {
        sem_destroy(&synchLock); 
        ControlExceptions::SimErrorExImpl ex(__FILE__, __LINE__, __func__);
        throw ex.getSimErrorEx();
    }

    if (sem_wait(&synchLock) == -1) {
        ControlExceptions::SimErrorExImpl ex(__FILE__, __LINE__, __func__);
        throw ex.getSimErrorEx();
    }

    if (sem_destroy(&synchLock) == -1) {
        ControlExceptions::SimErrorExImpl ex(__FILE__, __LINE__, __func__);
        throw ex.getSimErrorEx();
    }

    if (status != AMBERR_NOERR) {
        ControlExceptions::SimErrorExImpl ex(__FILE__, __LINE__, __func__);
        throw ex.getSimErrorEx();
    }
}

void HOLORXCompSimBase::monitor(AmbRelativeAddr RCA,
                                AmbDataLength_t& dataLength, 
                                AmbDataMem_t* data,
                                sem_t* synchLock,
                                Time* timestamp,
                                AmbErrorCode_t* status) {
    simulationIf_m.monitor(RCA, dataLength, data, synchLock, timestamp, 
                           status);
}

void HOLORXCompSimBase::command(AmbRelativeAddr RCA,
                                AmbDataLength_t dataLength,
                                const AmbDataMem_t* data,
                                sem_t* synchLock,
                                Time* timestamp,
                                AmbErrorCode_t* status) {
    simulationIf_m.command(RCA, dataLength, data, synchLock, timestamp, 
                           status);
}

void HOLORXCompSimBase::monitorTE(ACS::Time TimeEvent,
                                  AmbRelativeAddr RCA,
                                  AmbDataLength_t& dataLength, 
                                  AmbDataMem_t* data,
                                  sem_t* synchLock,
                                  Time* timestamp,
                                  AmbErrorCode_t* status) {
    simulationIf_m.monitorTE(TimeEvent, RCA, dataLength, data, synchLock, 
                             timestamp, status);
}

void HOLORXCompSimBase::commandTE(ACS::Time TimeEvent,
                                  AmbRelativeAddr RCA,
                                  AmbDataLength_t dataLength, 
                                  const AmbDataMem_t* data,
                                  sem_t* synchLock,
                                  Time* timestamp,
                                  AmbErrorCode_t* status) {
    simulationIf_m.commandTE(TimeEvent, RCA, dataLength, data, synchLock, 
                             timestamp, status);
}

void HOLORXCompSimBase::monitorNextTE(AmbRelativeAddr RCA,
                                      AmbDataLength_t& dataLength, 
                                      AmbDataMem_t* data,
                                      sem_t* synchLock,
                                      Time* timestamp,
                                      AmbErrorCode_t* status) {
    simulationIf_m.monitorNextTE(RCA, dataLength, data, synchLock, 
                                 timestamp, status);
}

void HOLORXCompSimBase::commandNextTE(AMBSystem::AmbRelativeAddr RCA,
                                      AmbDataLength_t dataLength, 
                                      const AmbDataMem_t* data,
                                      sem_t* synchLock,
                                      ACS::Time* timestamp,
                                      AmbErrorCode_t* status) {
    simulationIf_m.commandNextTE(RCA, dataLength, data, synchLock, 
                                 timestamp, status);
}

void HOLORXCompSimBase::flushNode(ACS::Time TimeEvent,
                                  ACS::Time* timestamp,
                                  AmbErrorCode_t* status) {
    simulationIf_m.flushNode(TimeEvent, timestamp, status);
}

void HOLORXCompSimBase::flushRCA(ACS::Time TimeEvent,
                                 AmbRelativeAddr RCA,
                                 ACS::Time* timestamp,
                                 AmbErrorCode_t* status) {
    simulationIf_m.flushRCA(TimeEvent, RCA,  timestamp, status);
}
