#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It defines the HoloRx class that provides low level control of the
holography reciever
"""

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import Acspy.Common.Err
import CCL.HardwareDevice

class HoloRx(CCL.HardwareDevice.HardwareDevice):
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The HoloRx class is a python proxy to the HoloRx
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponentNonSticky
        (stickyFlag = False, default) or getComponent (stickyFlag =
        True).

        EXAMPLE:
        from HoloRx import *
        hrx = HoloRx("CONTROL/DV01/HoloRx")
        hrx.hwConfigure(); hrx.hwInitialize(); hrx.hwOperational();
        freq = hrx.tuneHigh();
        if (!hrx.isLocked()):
          v = hrx.SET_GUNN_TUNE_RANGE();
          hrx.SET_GUNN_TUNE_RANGE(v + 10);
        del(hrx)
        '''
        # initialize the base class
        CCL.HardwareDevice.HardwareDevice.__init__(self, componentName, stickyFlag);

    def __del__(self):
        CCL.HardwareDevice.HardwareDevice.__del__(self)

    def PLL_STATUS(self):
        '''
        Latched status of the PLL in the previous 48ms (the status is
        updated to the current at the 48ms edge and the "bad" 
        statuses are latched until the new 48ms pulse comes along)
        Bit 0: Gunn oscillator lock (1: locked, 0: unlocked)
        Bit 1: Synthesizer oscillator lock (1: locked, 0: unlocked)
        Bit 2: REF. Status (1: sufficient power, 0: fault condition)
        Bit 3: IF. Status (1: sufficient power, 0: fault condition)
        Bit 4: Frame CRC (1: errror in the DSP frames, 0: no error)
        Bits 5-7: Unused
        '''
        (value, completion) = self._HardwareDevice__hw._get_PLL_STATUS().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();

    def GUNN_H_VOLTAGE(self):
        '''
        Get tuning voltage of high band Gunn Oscillator.
        '''
        (value, completion) = self._HardwareDevice__hw._get_GUNN_H_VOLTAGE().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def GUNN_L_VOLTAGE(self):
        '''
        Get tuning voltage of low band Gunn Oscillator.
        '''
        (value, completion) = self._HardwareDevice__hw._get_GUNN_L_VOLTAGE().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def LO_DET_OUT(self):
        '''
        Get local oscillator (LO) detector level.
        '''
        (value, completion) = self._HardwareDevice__hw._get_LO_DET_OUT().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def REF_DET_OUT(self):
        '''
        Get reference intermediate frequency (IF) detector level.
        '''
        (value, completion) = self._HardwareDevice__hw._get_REF_DET_OUT().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def SIG_DET_OUT(self):
        '''
        Get signal intermediate frequency (IF) detector level.
        '''
        (value, completion) = self._HardwareDevice__hw._get_SIG_DET_OUT().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def REF_SENSE_I(self):
        '''
        Get the RMS voltage of the Ref. I channel at the Anti-Aliasing module
        '''
        (value, completion) = self._HardwareDevice__hw._get_REF_SENSE_I().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def REF_SENSE_Q(self):
        '''
        Get the RMS voltage of the Ref. Q channel at the Anti-Aliasing module
        '''
        (value, completion) = self._HardwareDevice__hw._get_REF_SENSE_Q().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def SIG_SENSE_I(self):
        '''
        Get the RMS voltage of the Signal I channel at the Anti-Aliasing module
        '''
        (value, completion) = self._HardwareDevice__hw._get_SIG_SENSE_I().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def SIG_SENSE_Q(self):
        '''
        Get the RMS voltage of the Signal Q channel at the Anti-Aliasing module
        '''
        (value, completion) = self._HardwareDevice__hw._get_SIG_SENSE_Q().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def SUPPLY_CURRENT(self):
        '''
        Get the supply current in A for the entire holography receiver.
        '''
        (value, completion) = self._HardwareDevice__hw._get_SUPPLY_CURRENT().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def TEMP_POWER_SUPPLY(self):
        '''
        Get the temperature reading in degrees C from temperature sensor by
        the power supply.
        '''
        (value, completion) = self._HardwareDevice__hw._get_TEMP_POWER_SUPPLY().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def TEMP_SIG_MIX(self):
        '''
        Get the temperature reading in degrees C from temperature sensor by
        the mixer in signal channel.
        '''
        (value, completion) = self._HardwareDevice__hw._get_TEMP_SIG_MIX().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def TEMP_REF_MIX(self):
        '''
         Get the temperature reading in degrees C from temperature sensor by
         the mixer in reference channel.
        '''
        (value, completion) = self._HardwareDevice__hw._get_TEMP_REF_MIX().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def TEMP_29MHZ_OCXO(self):
        '''
        Get the temperature reading in degrees C from temperature sensor by
        the 29 MHz oven-controlled crystal oscillator.
        '''
        (value, completion) = self._HardwareDevice__hw._get_TEMP_29MHZ_OCXO().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def TEMP_95MHZ_OCXO(self):
        '''
        Get the temperature reading in degrees C from temperature sensor by
        the 29 MHz oven-controlled crystal oscillator.
        '''
        (value, completion) = self._HardwareDevice__hw._get_TEMP_95MHZ_OCXO().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();
        
    def TEMP_LOCK_BOX(self):
        '''
        Get the temperature reading in degrees C from temperature sensor by
        the lock box.
        '''
        (value, completion) = self._HardwareDevice__hw._get_TEMP_LOCK_BOX().get_sync();
        Acspy.Common.Err.addComplHelperMethods(completion);
        if (completion.isErrorFree()):
            return value;
        else:
            completion.log();

    def SET_SIG_ATTENUATION(self, attenuation):
        '''
        Set attenuation of signal channel. The supplied valued should
        be a 6-bit integer equal to the attenuation of the signal
        channel in dB (0-63dB).
        '''
        self._HardwareDevice__hw.SET_SIG_ATTENUATION(attenuation);

    def GET_SIG_ATTENUATION(self):
        '''
        Get attenuation of the signal channel. This monitor point is
        not currently documented in the ICD and is probably a
        read-back of the value of the last SET_SIG_ATTENUATION command
        received by the AMBSI in the holography receiver.
        '''
        return self._HardwareDevice__hw.get_SIG_ATTENUATION()[0];

    def SET_REF_ATTENUATION(self, attenuation):
        '''
        Set attenuation of reference channel. The supplied valued should
        be a 6-bit integer equal to the attenuation of the signal
        channel in dB (0-63dB).
        '''
        self._HardwareDevice__hw.SET_REF_ATTENUATION(attenuation);

    def GET_REF_ATTENUATION(self):
        '''
        Get attenuation of the reference channel. This monitor point
        is not currently documented in the ICD and is probably a
        read-back of the value of the last SET_REF_ATTENUATION command
        received by the AMBSI in the holography receiver.
        '''
        return self._HardwareDevice__hw.get_REF_ATTENUATION()[0];

    def SET_SYNTH_FREQUENCY(self, freqInHz):
        '''
        Set holography synthesizer frequency. The supplied value
        should be Hz and in the range 8.730e9 - 9.475e9 (Hz). The frequency
        will be rounded to a value that is in steps of 0.125e6 (Hz).
        '''
        self._HardwareDevice__hw.SET_SYNTH_FREQUENCY(freqInHz);

    def GET_SYNTH_FREQUENCY(self):
        '''
        Get the synthesizer frequency. This monitor point is not
        currently documented in the ICD and is probably a read-back of
        the value of the last SET_SYNTH_FREQUENCY command received by
        the AMBSI in the holography receiver.
        '''
        return self._HardwareDevice__hw.get_SYNTH_FREQUENCY()[0];

    def SET_DRIVE_ENABLE(self, on):
        '''
        Turn the Gunn oscillator bias on/off
        '''
        if on:
            self._HardwareDevice__hw.SET_DRIVE_ENABLE_ON();
        else:
            self._HardwareDevice__hw.SET_DRIVE_ENABLE_OFF();

    def GET_DRIVE_ENABLE(self):
        '''
        Returns true if the Gunn oscillator bias is on and false
        otherwise. This monitor point is not currently documented in
        the ICD and is probably a read-back of the value of the last
        SET_DRIVE_ENABLE command received by the AMBSI in the
        holography receiver.
        '''
        return self._HardwareDevice__hw.get_DRIVE_ENABLE()[0];

    def SET_LOOP_ENABLE(self, close):
        '''
        Open/close the phased lock loop to the Gunn oscillator
        '''
        if close:
            self._HardwareDevice__hw.SET_LOOP_ENABLE_CLOSE();
        else:
            self._HardwareDevice__hw.SET_LOOP_ENABLE_OPEN();

    def GET_LOOP_ENABLE(self):
        '''
        Returns true if the phase locked loop to the Gunn oscillator
        is closed and false otherwise. This monitor point is not
        currently documented in the ICD and is probably a read-back of
        the value of the last SET_LOOP_ENABLE command received by the
        AMBSI in the holography receiver.
        '''
        return self._HardwareDevice__hw.get_LOOP_ENABLE()[0];

    def SET_NOMINAL_VOLTAGE(self, voltage):
        '''
        To set the output voltage of the 12-bit multiplying DAC
        (LTC8043), which in turn will determine the bias voltage to
        the Gunn oscillator.
        '''
        self._HardwareDevice__hw.SET_NOMINAL_VOLTAGE(voltage);

    def GET_NOMINAL_VOLTAGE(self):
        '''
        Returns the last commanded value to the DAC which determines
        the bias voltage to the Gunn oscillator. This monitor point is
        not currently documented in the ICD and is probably a
        read-back of the value of the last SET_NOMINAL_VOLTAGE command
        received by the AMBSI in the holography receiver.
        '''
        return self._HardwareDevice__hw.get_NOMINAL_VOLTAGE()[0];

    def SET_GUNN_LOOP_GAIN(self, resistanceInOhms):
        '''
        Set the loop gain of the phased locked Gunn oscillator
        '''
        self._HardwareDevice__hw.SET_GUNN_LOOP_GAIN(resistanceInOhms);

    def GET_GUNN_LOOP_GAIN(self):
        '''
        Returns the last commanded value for the loop gain of the
        phased locked Gunn oscillator. This monitor point is not
        currently documented in the ICD and is probably a read-back of
        the value of the last SET_GUNN_LOOP_GAIN command received by
        the AMBSI in the holography receiver.
        '''
        return self._HardwareDevice__hw.get_GUNN_LOOP_GAIN()[0];

    def SET_GUNN_TUNE_RANGE(self, resistanceInOhms):
        '''
        Set tuning range of Gunn oscillator
        '''
        self._HardwareDevice__hw.SET_GUNN_TUNE_RANGE(resistanceInOhms);

    def GET_GUNN_TUNE_RANGE(self):
        '''
        Returns the last commanded value for the tuning range of the
        Gunn oscillator. This monitor point is not currently
        documented in the ICD and is probably a read-back of the value
        of the last SET_GUNN_TUNE_RANGE command received by the AMBSI
        in the holography receiver.
        '''
        return self._HardwareDevice__hw.get_GUNN_TUNE_RANGE()[0];

    def SET_GUNN_SELECT(self, high):
        '''
        Select high/low-band Gunn.
        '''
        if high:
            self._HardwareDevice__hw.SET_GUNN_SELECT_HIGH();
        else:
            self._HardwareDevice__hw.SET_GUNN_SELECT_LOW();

    def GET_GUNN_SELECT(self):
        '''
        Returns the true if the high band Gunn has been selected and
        false otherwise. This monitor point is not currently
        documented in the ICD and is probably a read-back of the value
        of the last SET_GUNN_SELECT received by the AMBSI in the
        holography receiver.
        '''
        return self._HardwareDevice__hw.get_GUNN_SELECT()[0];

    def startSubscan(self, startTime):
        '''
        Starts a sub-scan. The startTime should be an ACS::Time and in
        the future by over a second.
        '''
        return self._HardwareDevice__hw.startSubscan(startTime);

    def stopSubscan(self, stopTime):
        '''
        Stops a sub-scan. The stopTime should be an ACS::Time and in
        the future and after the startTime.
        '''
        return self._HardwareDevice__hw.stopSubscan(stopTime);

    def getSubscanData(self, timeout):
        '''
        '''
        return self._HardwareDevice__hw.getSubscanData(timeout);

    def tuneLow(self):
        '''
        Tune the receiver to the low band. This function will return
        the center frequency of the receiver and normally this will be
        78.92E9Hz. If there are difficulties tuning the receiver the
        software may tune to a nearby frequency. It will throw a
        NotLocked exception if the reciever does not lock.
        '''
        return self._HardwareDevice__hw.tuneLow();

    def tuneHigh(self):
        '''
        Tune the receiver to the high band. This function will return
        the center frequency of the receiver and normally this will be
        104.02E9Hz. If there are difficulties tuning the receiver the
        software may tune to a nearby frequency. It will throw a
        NotLocked exception if the reciever does not lock.
        '''
        return self._HardwareDevice__hw.tuneHigh();

    def getFrequency(self):
        '''
        Returns the center frequency of the holography
        receiver. Typically this will be either 78.92E9Hz or
        104.02E9Hz but may differ slightly from these values if there
        are problems tuning the reciever. This function does not check
        if the receiver is locked - use the isLocked function for that
        purpose.
        '''
        return self._HardwareDevice__hw.getFrequency();

    def isLocked(self):
        '''
        Returns true if the receiver is phase locked (and hence can
        produce valid data). This function just provides some
        interpretation of the bits in the PLL_STATUS monitor point.
        '''
        return self._HardwareDevice__hw.isLocked();

    def optimizeAttenuators(self):
        '''
        Adjusts the attenuators on both the signal and reference
        channels so that the signal levels are at the maximum useful value
        (but are not saturated). To do this correctly:
        1. The receiver must be tuned
        2. The transmitter must be turned on and tuned so that it is
        within the passband of the receiver.
        3. The antenna must be pointing at the transmitter.
        These three steps ensure that the maximum signal level
        expected during a holography scan are being received.

        This function iteratively adjusts the signal attenuator until
        a signal level of just less than 13 Volts is measured using
        the maximum of the SIG_SENSE_I & SIG_SENSE_Q monitor
        points. The same proceedure is used for the reference channel.
        This function then returns, repectively, the signal and
        reference attenuation (in dB).

        At the ATF, during the November 2006 tests using the Vertex
        prototype antenna, the optimum signal attenuation was found to
        0-1dBm and reference attenaution was 5-10dBm.
        '''
        return self._HardwareDevice__hw.optimizeAttenuators();
