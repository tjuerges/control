#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006, 2008
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import string
import time
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import TETimeUtil
import CCL.HoloRx
import Control

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        self.out = ""

    def __del__(self):
        if self.out != None:
            print self.out

    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End " + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"

    def makeOperational(self):
        if (hrx.getHwState() != Control.HardwareDevice.Operational):
            hrx.hwStop();
            hrx.hwStart();
            hrx.hwConfigure();
            hrx.hwInitialize();
            hrx.hwOperational();
        
    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test one checks the software and hardware lifecycle of the component.
        '''
        self.startTestLog("Test 1: Component lifecycle test")
        self.makeOperational();
        self.endTestLog("Test 1: Component lifecycle test")

    def test02(self):
        '''
        This tests that all monitor points return a reasonable value.
        '''
        self.startTestLog("Test 2: Monitor point test")
        self.makeOperational();
        
        monitors = {hrx.PLL_STATUS:(0, 31), hrx.GUNN_H_VOLTAGE:(0, 15.0),
                    hrx.GUNN_L_VOLTAGE:(0, 15.0), hrx.LO_DET_OUT:(0, 5.0),
                    hrx.REF_DET_OUT:(0, 5.0), hrx.SIG_DET_OUT:(0, 5.0),
                    hrx.REF_SENSE_I:(0, 5.0), hrx.REF_SENSE_Q:(0, 5.0),
                    hrx.SIG_SENSE_I:(0, 5.0), hrx.SIG_SENSE_Q:(0, 5.0),
                    hrx.SUPPLY_CURRENT:(0.0, 5.0),
                    hrx.TEMP_POWER_SUPPLY:(0.0, 50.0),
                    hrx.TEMP_SIG_MIX:(0.0, 50.0),
                    hrx.TEMP_REF_MIX:(0.0, 50.0),
                    hrx.TEMP_29MHZ_OCXO:(0.0, 50.0),
                    hrx.TEMP_95MHZ_OCXO:(0.0, 50.0),
                    hrx.TEMP_LOCK_BOX:(0.0, 50.0),
                    hrx.GET_SIG_ATTENUATION:(0, 63),
                    hrx.GET_REF_ATTENUATION:(0, 63),
                    hrx.GET_SYNTH_FREQUENCY:(8.73E9, 9.475E9),
                    hrx.GET_DRIVE_ENABLE:(False, True),
                    hrx.GET_LOOP_ENABLE:(False, True),
                    hrx.GET_NOMINAL_VOLTAGE:(0.0, 10.0),
                    hrx.GET_GUNN_LOOP_GAIN:(0.0, 10e3),
                    hrx.GET_GUNN_TUNE_RANGE:(0.0, 50e3),
                    hrx.GET_GUNN_SELECT:(False, True),
                    }
        for fn in monitors:
            value = apply(fn);
            fnName = string.split(string.split(str(fn))[2], '.')[1];
            llimit = monitors[fn][0]; ulimit = monitors[fn][1]
            msg = 'The ' + fnName + ' function returns an ' + \
                  "out of range value." + \
                  "\nLimits are " + str(llimit) + " to " + str(ulimit) + \
                  ". Measured value is " + str(value)
            self.failIf((value < llimit) | (value > ulimit), msg)
            
        self.endTestLog("Test 2: Monitor point test")

    def test03(self):
        '''
        This tries all the control points.
        '''
        self.startTestLog("Test 3: Control point test")
        self.makeOperational();
        
        controls = {hrx.SET_SIG_ATTENUATION:(33),
                    hrx.SET_REF_ATTENUATION:(62),
                    hrx.SET_SYNTH_FREQUENCY:(9E9),
                    hrx.SET_DRIVE_ENABLE:(True),
                    hrx.SET_LOOP_ENABLE:(True),
                    hrx.SET_NOMINAL_VOLTAGE:(7.8),
                    hrx.SET_GUNN_LOOP_GAIN:(5.67),
                    hrx.SET_GUNN_TUNE_RANGE:(45.67),
                    hrx.SET_GUNN_SELECT:(True),
                   }
        for fn in controls:
            arg = controls[fn];
            fnName = string.split(string.split(str(fn))[2], '.')[1];
            value = apply(fn, (arg,));
            
        self.endTestLog("Test 3: Control point test")

    def test04(self):
        '''
        Test the correct functioning of the sub-scan interface
        '''
        self.startTestLog("Test 4: Sub-scan interface test")
        self.makeOperational();
        scanTime = 6 # ~6 secs. sub-scan
        startTime = TETimeUtil.nearestTE(TETimeUtil.unix2epoch(time.time()));
        startTime = TETimeUtil.plusTE(startTime, 21*2) # ~2 secs. to setup
        stopTime  = TETimeUtil.plusTE(startTime, 21*scanTime)
        hrx.startSubscan(startTime.value)
        hrx.stopSubscan(stopTime.value)
        data = hrx.getSubscanData(scanTime*1.2)

        self.failIf(data.startTime != startTime.value,
                    "Start Time does not match commanded startTime");
        self.failIf(data.sampleInterval != 480000,
                    "Incorrect sample interval returned");
        self.failIf(len(data.holoFlag) != scanTime*21,
                    "Incorrect length data vector returned");
        self.endTestLog("Test 4: Sub-scan interface test")
        

    def test05(self):
        '''
        Verify that we can do multiple subscans in a row
        '''
        self.startTestLog("Test 5: Multiple sub-scan test")
        self.makeOperational();
        scanTime = 6 # ~6 secs. sub-scan
        for iteration in range (0,5):
            startTime = \
                      TETimeUtil.nearestTE(TETimeUtil.unix2epoch(time.time()));
            startTime = TETimeUtil.plusTE(startTime, 21*2) # ~2 secs. to setup
            stopTime  = TETimeUtil.plusTE(startTime, 21*scanTime)
            hrx.startSubscan(startTime.value)
            hrx.stopSubscan(stopTime.value)
            data = hrx.getSubscanData(scanTime*1.2);
          
            self.failIf(data.startTime != startTime.value,
                        "Start Time does not match commanded startTime");
            self.failIf(data.sampleInterval != 480000,
                        "Incorrect sample interval returned");
            self.failIf(len(data.holoFlag) != scanTime*21,
                        "Incorrect length data vector returned");

        self.endTestLog("Test 5: Multiple sub-scan test")
        
    def test06(self):
        '''
        Check the the scaling on the syntheser frequency as this has
        been wrong twice and the previous tests do not detect this.
        '''
        self.startTestLog("Test 6: Synthesizer frequency test")
        self.makeOperational();
        hrx.SET_SYNTH_FREQUENCY(9E9);
        actFreq = hrx.GET_SYNTH_FREQUENCY();
        self.failIf(abs(actFreq - 9E9) > 100,
                    "Bad scaling of the {SET,GET}_SYNTH_FREQUENCY");
        self.endTestLog("Test 6: Synthesizer frequency test")

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self, methodName)

    def __del__(self):
        automated.__del__(self);


# **************************************************
# MAIN Program
if __name__ == '__main__':
    ''' Usage: Phase4Test <testsuite.testname> <antenna>
    <antenna> defaults to DA41
    <testsuite> defaults to interactive
    <testname> defaults to all tests
    eg., Phase4test automated DV01 or
         Phase4test interactive.test01
    '''
    
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("DA41")

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

# The creation of the component must be done outside the unit test
# framework.
    componentName = 'CONTROL/' + antenna + '/HoloRx'
    hrx = CCL.HoloRx.HoloRx(componentName, stickyFlag=True)

    unittest.main()

