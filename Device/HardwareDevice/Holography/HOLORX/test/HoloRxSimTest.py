#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2006-07-14  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

#import sys
import unittest
#import struct
import Control
import ControlExceptions
import time
import TETimeUtil
from Acspy.Clients.SimpleClient import PySimpleClient
#from Acspy.Common.TimeHelper import TimeUtil

#from Acspy.Common.QoS import setObjectTimeout

#import Acspy.Util

class HoloRxSimTest(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self._client=PySimpleClient.getInstance()
        unittest.TestCase.__init__(self,methodName)

    def __del__(self):
        self._client.disconnect()

    def setUp(self):
        self._ref= self._client.getComponent("CONTROL/DV01/HoloRx")
        self._ref.hwConfigure()
        self._ref.hwInitialize()
        self._ref.hwOperational()
        self._ref.hwSimulation()
		
    def tearDown(self):
        self._ref.hwStop()
        self._ref.hwStart()
        self._client.releaseComponent("CONTROL/DV01/HoloRx")
		

    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test 1 Verifies that the Simulated device is in the proper state
        '''
        self.failIf(self._ref.getHwState() !=
                    Control.HardwareDevice.Simulation,
                    "Device failed to go to simulated state");

        
    def test02(self):
        '''
        Test 2 Test the correct functioning of the Subscan interface
        '''
        startTime = TETimeUtil.unix2epoch(time.time()).value
        stopTime  = startTime + 60000000 # 6 Second Run
        
        self._ref.startSubscan(startTime)
        self._ref.stopSubscan(stopTime)
        data = self._ref.getSubscanData(10.0) # 10 Second timeout

        self.failIf(data.startTime != startTime,
                    "Start Time does not match commanded startTime");
        self.failIf(data.sampleInterval != 480000,
                    "Incorrect sample interval returned");
        self.failIf(len(data.holoFlag) != 125,
                    "Incorrect lenghth data vector returned");

    def test03(self):
        '''
        Verify that we can do multiple subscans in a row
        '''
        duration = 360000000
        for iteration in range (0,5):
            startTime = TETimeUtil.unix2epoch(time.time()).value
            stopTime  = startTime + duration # 6 Second Run
            
            self._ref.startSubscan(startTime)
            self._ref.stopSubscan(stopTime)
            data = self._ref.getSubscanData(10.0) # 10 Second timeout
          
            self.failIf(data.startTime != startTime,
                        "Start Time does not match commanded startTime");
            self.failIf(data.sampleInterval != 480000,
                        "Incorrect sample interval returned");
            self.failIf(len(data.holoFlag) != duration/480000,
                        "Incorrect lenghth data vector returned");
            duration -= 60000000

# --------------------------------------------------
# Test Definitions
# **************************************************
# MAIN Program
if __name__ == '__main__':
	# For some reason ACS has problems unless I do this
	client = PySimpleClient.getInstance()
        unittest.main()


#
# ___oOo___
