#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import struct
import ambManager
import Acspy.Common.QoS
import Acspy.Util.ACSCorba

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.manager = ambManager.AmbManager(antenna)
        Acspy.Common.QoS.setObjectTimeout(self.manager._AmbManager__mgrRef, \
                                          20000)
        self.out = ""
        self.node    = 0x1c
        self.channel = 0
        self.monitor = {0x00001:1, 0x00002:4, 0x00003:4,
                        0x00004:4, 0x00005:4, 0x00006:4, 0x00007:4,
                        0x00008:4, 0x00009:4, 0x0000A:4, 0x0000B:4,
                        0x0000C:8, 0x0000D:4, 0x0000E:4, 0x0000F:4,
                        0x00010:4, 0x00011:4,
                        0x01001:1, 0x01002:1, 0x01003:2,
                        0x01004:1, 0x01005:1, 0x01006:4, 0x01007:4,
                        0x01008:4, 0x01009:1,
                        0x30000:3, 0x30001:4, 0x30002:4,
                        0x30003:4}

    def __del__(self):
        del(self.manager);
        self.manager = None
        if self.out != None:
            print self.out

    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"
        
    def scanForUndocumentedMonitors(self, RCALimit):
        errorList = {}

        for RCA in range (0x00000, RCALimit):
            try:
                (r,t) = self.manager.monitor(self.channel, self.node, RCA)
                if self.monitor.has_key(RCA):
                    if len(r) != self.monitor[RCA][2]:
                        errorList[RCA]=len(r)
                else:
                    errorList[RCA]=len(r)
            except:
                pass

        if len(errorList) != 0:
            self.log("%d offending responses" % len(errorList))
            if len(errorList) < 20:
                for idx in errorList.keys():
                    if self.monitor.has_key(RCA):
                        self.log("RCA 0x%x returned %d bytes (%d expected)" % \
                              (idx, errorList[idx], self.monitor[RCA][2]))
                    else:
                        self.log("RCA 0x%x returned %d bytes" % \
                              (idx, errorList[idx]))
            self.fail("%d Monitor points not in ICD Responded" %
                      len(errorList))

    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test one verifies that the device correctly responds to
        broadcast messages and returns the serial number when requested
        by RCA 0x0.

        For recordkeeping the serial number is returned.
        '''
        self.startTestLog("Test 1: Broadcast Response Test")
        try:
            (r,t) = self.manager.getNodes(self.channel)
        except:
            self.fail("Unable to get nodes on bus")

        foundNode = False
        for node in r:
            if node.node == self.node:
                if not foundNode:
                    foundNode = True
                    self.log("Device Serial Number: 0x%x" % node.sn)
                else:
                    self.fail("Node 0x%x responded at least twice" % self.node)

        self.failUnless(foundNode, "Node %d did not respond" % self.node)

#TODO: Enable this once the simulator is fixed. This does not work on
# the hardware either.
#         try:
#             (sn, time) = self.manager.findSN(self.channel, self.node)
#         except:
#             self.fail("Exception retrieving serial number from node")

        self.endTestLog("Test 1: Broadcast Response Test")


    def test02(self):
        '''
        Test 2 Verifies an appropriate response from the
        GET_PLL_STATUS monitor point.
        '''
        self.startTestLog("Test 2: PLL Status Test")
        (r,t) = self.manager.monitor(self.channel, self.node, 0x00001)
        self.failUnless(len(r) == 1, "Return length incorrect RCA:0x00001")
        (rv,) = struct.unpack("!B",r)
        msg = "The top two (unused) bits are not zero! Value is " + \
              str(hex(rv))
        self.failUnless(rv & 0xC0 == 0 , msg);
            
        self.endTestLog("Test 2: PLL Status Test")

    def test03(self):
        '''
        Test 3 Verifies an appropriate response from the
        GET_GUNN_H_VOLTAGE
        GET_GUNN_L_VOLTAGE
        GET_LO_DET_OUT
        GET_REF_DET_OUT
        GET_SIG_DET_OUT
        GET_SIG_SENSE_I
        GET_SIG_SENSE_Q
        GET_REF_SENSE_I
        GET_REF_SENSE_Q
        GET_SUPPLY_CURRENT
        GET_TEMP_POWER_SUPPLY
        GET_TEMP_SIG_MIX
        GET_TEMP_REF_MIX
        GET_TEMP_29MHZ_OCXO
        GET_TEMP_95MHZ_OCXO
        GET_TEMP_LOCK_BOX
        monitor points.
        '''
        self.startTestLog("Test 3: Floating point monitor points")
        limits = {0x00002:(0.0, 15.0), 0x00003:(0.0, 15.0),
                  0x00004:(0.0, 5.0),  0x00005:(0.0, 5.0),
                  0x00006:(0.0, 5.0),  0x00007:(0.0, 5.0),
                  0x00008:(0.0, 5.0),  0x00009:(0.0, 5.0),
                  0x0000A:(0.0, 5.0),  0x0000B:(0.0, 5.0),
                  0x0000C:(0.0, 50.0), 0x0000D:(0.0, 50.0),
                  0x0000E:(0.0, 50.0), 0x0000F:(0.0, 50.0),
                  0x00010:(0.0, 50.0), 0x00011:(0.0, 50.0)};
        for rca in limits:
            (r,t) = self.manager.monitor(self.channel, self.node, rca)
            msg = "Return length incorrect RCA: " + str(hex(rca));
            self.failUnless(len(r) == 4, msg)
            (rv,) = struct.unpack("!f",r)
            llimit = limits[rca][0]; ulimit = limits[rca][1]
            msg = "The monitor point at RCA " + str(hex(rca)) + \
                  " is out of range." + \
                  "\nLimits are " + str(llimit) + " to " + str(ulimit) + \
                  ". Measured value is " + str(rv)
            self.failIf((rv < llimit) | (rv > ulimit), msg)
            
        self.endTestLog("Test 3: Floating point monitor points")


    def test04(self):
        '''
        This test monitors all RCAs between 0 and 0x3FF (a partial
        range as the full range takes too long).  It ensures that only
        monitors documented in the ICD respond, and that the response
        is valid (correct number of bytes).
        '''
        self.startTestLog("Test 4: Monitor Survey test (with limited range)")
        self.scanForUndocumentedMonitors(0x40);
        self.endTestLog("Test 4: Monitor Survey test (with limited range)")

    def test05(self):
        '''
        Test 3 Commands and verifies (via readback) the 
        SET_SIG_ATTENUATION
        SET_REF_ATTENUATION
        SET_SYNTH_FREQUENCY
        SET_DRIVE_ENABLE
        SET_LOOP_ENABLE
        SET_NOMINAL_VOLTAGE
        SET_GUNN_LOOP_GAIN
        SET_GUNN_TUNE_RANGE
        SET_GUNN_SELECT
        control points.
        '''
        self.startTestLog("Test 5: Control points")
        testValues = {0x01001:('!B', 0x2A), 0x01002:('!B', 0x35),
                      0x01003:('!H', 5957), 0x01004:('!B', 0x01),              
                      0x01005:('!B', 0x01), 0x01006:('!f', 7.86),
                      0x01007:('!f', 9876.5), 0x01008:('!f', 49876.5),
                      0x01009:('!B', 0x01)};
        for rca in testValues:
            format = testValues[rca][0];
            value = testValues[rca][1];
            self.manager.command(self.channel, self.node, rca, \
                                 struct.pack(format, value))
            (r,t) = self.manager.monitor(self.channel, self.node, rca)
            (rv,) = struct.unpack(format,r)
            msg = "The monitor point at RCA " + str(hex(rca)) + \
                  " is did not verify correctly." + \
                  "\nSent value is " + str(value) + \
                  " returned value is " + str(rv)
            self.assertAlmostEqual(rv, value, 6, msg)
            
        self.endTestLog("Test 5: Control points")

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self, methodName)
# Assuming that interactive testing is done with the real hardware
# which is on a different channel.
        self.channel = 3

    def __del__(self):
        automated.__del__(self);

    def test04(self):
        '''
        This test monitors all RCAs between 0 and 0x3FFFF (the full
        range allocated to each device).  It ensures that only
        monitors documented in the ICD respond, and that the response
        is valid (correct number of bytes). This test takes 15-20 minutes.
        '''
        self.startTestLog("Test 4: Monitor Survey test (with full range)")
        self.scanForUndocumentedMonitors(0x40000);
        self.endTestLog("Test 4: Monitor Survey test (with full range)")


# **************************************************
# MAIN Program
if __name__ == '__main__':
    ''' Usage: Phase2Test <testsuite.testname> <antenna>
    <antenna> defaults to DV01
    <testsuite> defaults to interactive
    <testname> defaults to all tests
    eg., Phase2test automated DA41 or
         Phase2test.interactive.test01
    '''
    
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("DV01")

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()

