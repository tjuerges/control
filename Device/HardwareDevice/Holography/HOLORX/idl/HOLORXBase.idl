/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * Warning!
 *  -------------------------------------------------------------------- 
 * | This is generated code!  Do not modify this file.                  |
 * | If you do, all changes will be lost when the file is re-generated. |
 *  --------------------------------------------------------------------
 *
 * File HOLORXBase.idl
 */

#ifndef HOLORXBase_IDL
#define HOLORXBase_IDL

/**
 * Device (property) interface to HOLORX.
 */

#include <HardwareDevice.idl>
#include <ControlExceptions.idl>
#include <baci.idl>

#pragma prefix "alma"

module Control
{

  /**
   * Control and monitor the holography receiver.
   */
  interface HOLORXBase : Control::HardwareDevice
  {

	typedef sequence <double> doubleSeq;
	typedef sequence <float> floatSeq;
	typedef sequence <long> longSeq;

	/////////////////////
	// BACI Properties //
	/////////////////////

	/**
	 * Latched status of the PLL in the previous 48ms (the status is updated 
	 * to the current at the 48ms edge and the bad statuses are latched 
	 * until the new 48ms pulse comes along)
	 */

	readonly attribute ACS::ROlong PLL_STATUS;

	/**
	 * Get tuning voltage of high band Gunn Oscillator.
	 */

	readonly attribute ACS::ROfloat GUNN_H_VOLTAGE;

	/**
	 * Get tuning voltage of low band Gunn Oscillator.
	 */

	readonly attribute ACS::ROfloat GUNN_L_VOLTAGE;

	/**
	 * Get local oscillator (LO) detector level.
	 */

	readonly attribute ACS::ROfloat LO_DET_OUT;

	/**
	 * Get reference intermediate frequency (IF) detector level.
	 */

	readonly attribute ACS::ROfloat REF_DET_OUT;

	/**
	 * Get signal intermediate frequency (IF) detector level.
	 */

	readonly attribute ACS::ROfloat SIG_DET_OUT;

	/**
	 * Get the RMS voltage of the Ref. I channel at the Anti-Aliasing module
	 */

	readonly attribute ACS::ROfloat REF_SENSE_I;

	/**
	 * Get the RMS voltage of the Ref. Q channel at the Anti-Aliasing module
	 */

	readonly attribute ACS::ROfloat REF_SENSE_Q;

	/**
	 * Get the RMS voltage of the Signal I channel at the Anti-Aliasing module
	 */

	readonly attribute ACS::ROfloat SIG_SENSE_I;

	/**
	 * Get the RMS voltage of the Signal Q channel at the Anti-Aliasing module 
	 * output.
	 */

	readonly attribute ACS::ROfloat SIG_SENSE_Q;

	/**
	 * Get the supply current in A for the entire holography receiver.
	 */

	readonly attribute ACS::ROfloat SUPPLY_CURRENT;

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * power supply.
	 */

	readonly attribute ACS::ROfloat TEMP_POWER_SUPPLY;

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * mixer in signal channel.
	 */

	readonly attribute ACS::ROfloat TEMP_SIG_MIX;

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * mixer in reference channel.
	 */

	readonly attribute ACS::ROfloat TEMP_REF_MIX;

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * 29 MHz oven-controlled crystal oscillator.
	 */

	readonly attribute ACS::ROfloat TEMP_29MHZ_OCXO;

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * 95 MHz oven-controlled crystal oscillator.
	 */

	readonly attribute ACS::ROfloat TEMP_95MHZ_OCXO;

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * lock box.
	 */

	readonly attribute ACS::ROfloat TEMP_LOCK_BOX;

	/////////////////////////////////////////
	// IDL Definitions for General Methods //
	/////////////////////////////////////////

	/**
	 * Get the channel number for this device.
	 */
	// TODO long getChannelNumber();

	/**
	 * Get the node address for this device.
	 */
	// TODO long getNodeAddress();

	/**
	 * Get the base address for this device.
	 */
	// TODO long getBaseAddress();

	////////////////////////////////////////
	// IDL Definitions for Monitor Points //
	////////////////////////////////////////

	/**
	 * Get the RCA for PLL_STATUS.
	 */
	// TODO long getRCA_PLL_STATUS();

	/**
	 * Latched status of the PLL in the previous 48ms (the status is updated 
	 * to the current at the 48ms edge and the bad statuses are latched 
	 * until the new 48ms pulse comes along)
	 */

	// TODO octet get_PLL_STATUS(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for GUNN_H_VOLTAGE.
	 */
	// TODO long getRCA_GUNN_H_VOLTAGE();

	/**
	 * Get tuning voltage of high band Gunn Oscillator.
	 */

	// TODO float get_GUNN_H_VOLTAGE(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for GUNN_L_VOLTAGE.
	 */
	// TODO long getRCA_GUNN_L_VOLTAGE();

	/**
	 * Get tuning voltage of low band Gunn Oscillator.
	 */

	// TODO float get_GUNN_L_VOLTAGE(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for LO_DET_OUT.
	 */
	// TODO long getRCA_LO_DET_OUT();

	/**
	 * Get local oscillator (LO) detector level.
	 */

	// TODO float get_LO_DET_OUT(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for REF_DET_OUT.
	 */
	// TODO long getRCA_REF_DET_OUT();

	/**
	 * Get reference intermediate frequency (IF) detector level.
	 */

	// TODO float get_REF_DET_OUT(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for SIG_DET_OUT.
	 */
	// TODO long getRCA_SIG_DET_OUT();

	/**
	 * Get signal intermediate frequency (IF) detector level.
	 */

	// TODO float get_SIG_DET_OUT(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for REF_SENSE_I.
	 */
	// TODO long getRCA_REF_SENSE_I();

	/**
	 * Get the RMS voltage of the Ref. I channel at the Anti-Aliasing module
	 */

	// TODO float get_REF_SENSE_I(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for REF_SENSE_Q.
	 */
	// TODO long getRCA_REF_SENSE_Q();

	/**
	 * Get the RMS voltage of the Ref. Q channel at the Anti-Aliasing module
	 */

	// TODO float get_REF_SENSE_Q(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for SIG_SENSE_I.
	 */
	// TODO long getRCA_SIG_SENSE_I();

	/**
	 * Get the RMS voltage of the Signal I channel at the Anti-Aliasing module
	 */

	// TODO float get_SIG_SENSE_I(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for SIG_SENSE_Q.
	 */
	// TODO long getRCA_SIG_SENSE_Q();

	/**
	 * Get the RMS voltage of the Signal Q channel at the Anti-Aliasing module 
	 * output.
	 */

	// TODO float get_SIG_SENSE_Q(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for SUPPLY_CURRENT.
	 */
	// TODO long getRCA_SUPPLY_CURRENT();

	/**
	 * Get the supply current in A for the entire holography receiver.
	 */

	// TODO float get_SUPPLY_CURRENT(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for TEMP_POWER_SUPPLY.
	 */
	// TODO long getRCA_TEMP_POWER_SUPPLY();

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * power supply.
	 */

	// TODO float get_TEMP_POWER_SUPPLY(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for TEMP_SIG_MIX.
	 */
	// TODO long getRCA_TEMP_SIG_MIX();

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * mixer in signal channel.
	 */

	// TODO float get_TEMP_SIG_MIX(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for TEMP_REF_MIX.
	 */
	// TODO long getRCA_TEMP_REF_MIX();

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * mixer in reference channel.
	 */

	// TODO float get_TEMP_REF_MIX(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for TEMP_29MHZ_OCXO.
	 */
	// TODO long getRCA_TEMP_29MHZ_OCXO();

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * 29 MHz oven-controlled crystal oscillator.
	 */

	// TODO float get_TEMP_29MHZ_OCXO(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for TEMP_95MHZ_OCXO.
	 */
	// TODO long getRCA_TEMP_95MHZ_OCXO();

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * 95 MHz oven-controlled crystal oscillator.
	 */

	// TODO float get_TEMP_95MHZ_OCXO(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for TEMP_LOCK_BOX.
	 */
	// TODO long getRCA_TEMP_LOCK_BOX();

	/**
	 * Get the temperature reading in degC from temperature sensor by the 
	 * lock box.
	 */

	// TODO float get_TEMP_LOCK_BOX(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for SIG_ATTENUATION.
	 */
	// TODO long getRCA_SIG_ATTENUATION();

	/**
	 * See control point.
	 */

	long get_SIG_ATTENUATION(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for REF_ATTENUATION.
	 */
	// TODO long getRCA_REF_ATTENUATION();

	/**
	 * See control point.
	 */

	long get_REF_ATTENUATION(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for SYNTH_FREQUENCY.
	 */
	// TODO long getRCA_SYNTH_FREQUENCY();

	/**
	 * See control point.
	 */

	double get_SYNTH_FREQUENCY(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for DRIVE_ENABLE.
	 */
	// TODO long getRCA_DRIVE_ENABLE();

	/**
	 * See control point.
	 */

	long get_DRIVE_ENABLE(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for LOOP_ENABLE.
	 */
	// TODO long getRCA_LOOP_ENABLE();

	/**
	 * See control point.
	 */

	long get_LOOP_ENABLE(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for NOMINAL_VOLTAGE.
	 */
	// TODO long getRCA_NOMINAL_VOLTAGE();

	/**
	 * See control point.
	 */

	float get_NOMINAL_VOLTAGE(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for GUNN_LOOP_GAIN.
	 */
	// TODO long getRCA_GUNN_LOOP_GAIN();

	/**
	 * See control point.
	 */

	float get_GUNN_LOOP_GAIN(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for GUNN_TUNE_RANGE.
	 */
	// TODO long getRCA_GUNN_TUNE_RANGE();

	/**
	 * See control point.
	 */

	float get_GUNN_TUNE_RANGE(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Get the RCA for GUNN_SELECT.
	 */
	// TODO long getRCA_GUNN_SELECT();

	/**
	 * See control point.
	 */

	long get_GUNN_SELECT(out ACS::Time timestamp) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

 	////////////////////////////////////////
 	// IDL Definitions for Control Points //
 	////////////////////////////////////////

	/**
	 * Set attenuation of signal channel.
	 */
	void SET_SIG_ATTENUATION(in long world) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Set attenuation of reference channel.
	 */
	void SET_REF_ATTENUATION(in long world) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Set holography synthesizer frequency.  unsigned integer in range [0..5960] 
	 * representing the synthesizer frequency in range 8.730 – 9.475 GHz 
	 * in increments of 0.125 MHz.
	 */
	void SET_SYNTH_FREQUENCY(in double world) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Turn the Gunn oscillator bias on.
	 */
	void SET_DRIVE_ENABLE_ON() raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Turn the Gunn oscillator bias off.
	 */
	void SET_DRIVE_ENABLE_OFF() raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * close the phased lock loop to the Gunn oscillator
	 */
	void SET_LOOP_ENABLE_CLOSE() raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * open the phased lock loop to the Gunn oscillator
	 */
	void SET_LOOP_ENABLE_OPEN() raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * To set the output voltage of the 12-bit multiplying DAC (LTC8043), 
	 * which in turn will determine the bias voltage to the Gunn oscillator.
	 */
	void SET_NOMINAL_VOLTAGE(in float world) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Set the loop gain of the phased locked Gunn oscillator
	 */
	void SET_GUNN_LOOP_GAIN(in float world) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Set tuning range of Gunn oscillator
	 */
	void SET_GUNN_TUNE_RANGE(in float world) raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Select high-band Gunn (GUNN_H)
	 */
	void SET_GUNN_SELECT_HIGH() raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

	/**
	 * Select low-band Gunn (GUNN_L)
	 */
	void SET_GUNN_SELECT_LOW() raises(ControlExceptions::CAMBErrorEx, ControlExceptions::INACTErrorEx);

  };

};

#endif /* HOLORXBase_IDL */
