// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jun 2, 2010  created
//


#include <FOADImpl.h>
#include <sstream>
#include <loggingMACROS.h>

// For audience logs.
#include <sstream>
#include <iomanip>
#include <LogToAudience.h>

// Alarm system stuff
#include <vector>
#include <controlAlarmSender.h>


// Monitoring done each 1 second. Since each unit is 100E-9 sec, we need
// 10000000 units :)
FOADImpl::FOADImpl(const ACE_CString& name, maci::ContainerServices* cs):
    ::FOADBase(name, cs),
    myName(name.c_str()),
    updateThreadCycleTime_m(10000000),
    updateThread_p(0),
    alarmSender(0),
    Plus7VMaxLimitVoltage(7.2),
    Plus7VMinLimitVoltage(6.8),
    Plus7VMaxLimitCurrent(12),
    Minus7VMaxLimitVoltage(-6.8),
    Minus7VMinLimitVoltage(-7.2),
    Minus7VMaxLimitCurrent(12),
    Plus17VAMaxLimitVoltage(18),
    Plus17VAMinLimitVoltage(16),
    Plus17VAMaxLimitCurrent(16),
    Minus17VMaxLimitVoltage(-16),
    Minus17VMinLimitVoltage(-18),
    Minus17VMaxLimitCurrent(16),
    Plus17VBMaxLimitVoltage(18),
    Plus17VBMinLimitVoltage(16),
    Plus17VBMaxLimitCurrent(16),
    testModeState(false)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    //Get antenna name from component name
    antName = HardwareDeviceImpl::componentToAntennaName(
        myName);
}

FOADImpl::~FOADImpl()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}


void FOADImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSender.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;
    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::SupplyError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Output_Shutdown;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::ErrorMinus17V;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::ErrorMinus7V;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Error7V;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Error17VB;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Error17VA;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Module17VBOn;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::ModuleMinus17VOn;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Module17VAOn;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Module7VOn;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::ModuleMinus7VOn;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::OverTempOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::FanOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::ACOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::DCOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::CurrentLimitOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::OverVoltageOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::FanSpeedOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::AmbientTemperatureOk;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Plus7VMaxVoltageError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Plus7VMinVoltageError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Plus7VMaxCurrentError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Minus7VVoltageOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Minus7VCurrentOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Plus17VAVoltageOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Plus17VACurrentOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Minus17VVoltageOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Minus17VCurrentOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Plus17VBVoltageOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FOADImpl::Plus17VBCurrentOutOfRangeError;
        alarmInformation.push_back(ai);
    }

    alarmSender->initializeAlarms("FOAD", antName,
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void FOADImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


void FOADImpl::execute()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        //Initialise call to the base class.
        ::FOADBase::execute();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    initialiseAlarmSystem();

    // Create the monitoring thread.
    FOADImpl* selfPtr(this);
    updateThread_p = getContainerServices()->getThreadManager()->create<
        Control::UpdateThread, FOADImpl* > (
            std::string(myName + "MonitoringThread").c_str(), selfPtr);

    //Set the thread sleeptime to 1s (ICD).
    updateThread_p->setSleepTime(updateThreadCycleTime_m);
}


void FOADImpl::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareDeviceImpl::hwStop();
    }
    catch(...)
    {
        LOG_TO_OPERATOR(LM_WARNING, "Caught an exception during transition to "
            "the hardware stop state.  Continuing anyway.");
    }

    // Terminate the update thread.
    if(updateThread_p != 0)
    {
        updateThread_p->suspend();
        updateThread_p->terminate();
        updateThread_p = 0;
    }

    // Call base class and monitorHelp's cleanup
    try
    {
        cleanUpAlarmSystem();
        FOADBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}


void FOADImpl::hwInitializeAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Clear all alarms.
    alarmSender->forceTerminateAllAlarms();

    FOADBase::hwInitializeAction();
}

//----------> Hardware Operational
void FOADImpl::hwOperationalAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    FOADBase::hwOperationalAction();

    // Here we continue the execution of the resumed thread.
    if(updateThread_p != 0)
    {
        updateThread_p->resume();
    }
}

//---------> Hardware Stop
void FOADImpl::hwStopAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Suspend the monitor and control point thread.
    if(updateThread_p != 0)
    {
        updateThread_p->suspend();
    }

    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushTime(0ULL);

    // Standard way for flushing  all commands.
    flushNode(0, &flushTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        std::ostringstream msg;
        msg << "Communication failure flushing commands to device.";
        LOG_TO_DEVELOPER(LM_INFO, msg.str());
    }
    else
    {
        std::ostringstream msg;
        msg << "All commands and monitors flushed at "
            << flushTime;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    // Clear all alarms.
    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }

    FOADBase::hwStopAction();
}


// This method will be called in a loop by the update thread.
void FOADImpl::updateThreadAction()
{
    // Queue Status Monitor.
    try
    {
        // Do nothing.
    }
    catch (const ControlExceptions::CAMBErrorExImpl& ex)
    {
        std::ostringstream msg;
        msg << "Received no answer or erroneous one from the CANBus."
            << " AntennaName = "
            << antName;
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        std::ostringstream msg;
        msg << "FOAD device on "
            << antName
            << " is not ready. Please wait...";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    catch(...)
    {
        std::ostringstream msg;
        msg << "Caught an unknown exception.";
        LOG_TO_DEVELOPER(LM_TRACE, msg.str());
    }

}


//
// -------------------------------
// Additional methods for FOAD
// -------------------------------
//
CORBA::Boolean FOADImpl::getTestModeState()
{
    return testModeState;
}

void FOADImpl::setTestModeState(CORBA::Boolean _state)
{
    testModeState = _state;
}


// Thread Monitor Constructor
Control::UpdateThread::UpdateThread(const ACE_CString& name,
    ::FOADImpl* _foad):
    ACS::Thread(name),
    foad(_foad)
{
}

Control::UpdateThread::~UpdateThread()
{
}

// runLoop implementation
void Control::UpdateThread::runLoop()
{
    foad->updateThreadAction();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(FOADImpl)
