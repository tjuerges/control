#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#    who           when            what
#    ======        ========        ===========
#    pburgos        12mar2008    File created to include status and operator logs impl

"""
This module is part of the Control Command Language.
It contains complementary functionality for a FOAD device.
"""
import HardwareDevice
import CCL.FOADBase
from CCL import StatusHelper
from CCL.logging import getLogger

class FOAD(CCL.FOADBase.FOADBase):
    def __init__(self,antennaName = None, instance=None, componentName = None, stickyFlag = False):
        if (componentName==None):
            componentName = "CONTROL/"+antennaName+"/FOADBBpr"+str(instance)
        CCL.FOADBase.FOADBase.__init__(self,None,componentName,stickyFlag)
# Initialize the base class
        for key, val in self._devices.iteritems():
            CCL.HardwareDevice.HardwareDevice.__init__(self, key, stickyFlag);
        self._devices[key]= self._HardwareDevice__hw

    def __del__(self):
        CCL.FOADBase.FOADBase.__del__(self)

    def STATUS(self):
        '''
        This method will display the status information of the device at
        the standard output.
        '''
        '''
        Displays the current status of the FOAD
        '''
        elements=[ ]
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
            try:
                status,t = self._devices[key].GET_STATUS_P_A()
                generalStatus = status[0]
                secondaryStatus = status[1]
                
                #General Status
                #bits0-2
                shutdownValue = (generalStatus & 0x7)
                shutdownStatusValue="Shutdown Status reported a normal condition"
                if (shutdownValue==0x0):
                    shutdownStatusValue= "No attempt has been made to start the PSU"
                if (shutdownValue==0x1):
                    shutdownStatusValue= "EDFA Status Fault"
                if (shutdownValue==0x2):
                    shutdownStatusValue= "Input Power Limits"
                if (shutdownValue==0x3):
                    shutdownStatusValue= "Output Power Limits"
                if (shutdownValue==0x4):
                    shutdownStatusValue= "PSU voltaje out of limits"
                if (shutdownValue==0x5):
                    shutdownStatusValue= "PSU current out of limits"
                if (shutdownValue==0x6):
                    shutdownStatusValue= "HeatSink Temp Out of Limits"
                if (shutdownValue==0x7):
                    shutdownStatusValue= "Shutdown by M&C Command"
                #bit 5
                maintenanceValue = (generalStatus & 0x20)
                if (maintenanceValue==0x20):
                    maintenanceStatusValue="The board is in maintenance mode"
                else:
                    maintenanceStatusValue="Not in maintenance mode"
                
                #bits6-7
                operationValue = (generalStatus & 0xC0)
                if (operationValue == 0x0):
                    operationStatusValue= "The EDFA PSU is on and the EDFA is in normal operation"
                if (operationValue == 0x1):
                    operationStatusValue= "The EDFA PSU is on and the EDFA is initializing"
                if (operationValue == 0x2):
                    operationStatusValue= "The EDFA PSU is shutdown"
                if (operationValue == 0x3):
                    operationStatusValue= "The EDFA PSU is shutdown"
                
                #Secondary Status
                #bits0-2
                lastRestartValue = (secondaryStatus & 0x7)
                lastRestartStatusValue= "Normal Condition"
                if (lastRestartValue==0x0):
                    lastRestartStatusValue= "A reset from SPI MCLR"
                if (lastRestartValue==0x1):
                    lastRestartStatusValue= "PowerUp - reset"
                if (lastRestartValue==0x2):
                    lastRestartStatusValue= "Reset cause by a voltage dip..Brown out"
                if (lastRestartValue==0x3):
                    lastRestartStatusValue= "Watchdog reset ...time out"
                #bits4-6
                serialPortValue = (secondaryStatus & 0x70)
                if (serialPortValue == 0x0):
                    serialPortStatusValue= "The buffer is ready to receive data from the SPI"
                if (serialPortValue == 0x1):
                    serialPortStatusValue= "Data is being sent from the buffer to the EDFA"
                if (serialPortValue == 0x2):
                    serialPortStatusValue= "Waiting for Reply from EDFA"
                if (serialPortValue == 0x3):
                    serialPortStatusValue= "Data is being received by the buffer from EDFA"
                if (serialPortValue == 0x4):
                    serialPortStatusValue= "There is a complete message in the buffer"
                if (serialPortValue == 0x5):
                    serialPortStatusValue= "Timed out waiting data"
                if (serialPortValue == 0x6):
                    serialPortStatusValue= "A data overrun ocurred while receiving EDFA data"
                if (serialPortValue == 0x7):
                    serialPortStatusValue= "A framing error ocurred while receiving EDFA data"
                
            except:
                shutdownStatusValue = "N/A"
                maintenanceStatusValue = "N/A"
                operationStatusValue = "N/A"
                lastRestartStatusValue = "N/A"
                serialPortStatusValue = "N/A"
            
            elements.append( StatusHelper.Line("Shutdown Status",[StatusHelper.ValueUnit(shutdownStatusValue)] ))
            elements.append( StatusHelper.Line("Maintenance Status",[StatusHelper.ValueUnit(maintenanceStatusValue)]))
            elements.append( StatusHelper.Line("Operation Status",
                                               [StatusHelper.ValueUnit(operationStatusValue)]) )
            elements.append( StatusHelper.Line("Last Restart Status",
                                               [StatusHelper.ValueUnit(lastRestartStatusValue)]) )
            elements.append( StatusHelper.Line("Serial Port Status",
                                               [StatusHelper.ValueUnit(serialPortStatusValue)]) )
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName +" SPA", elements)
            statusFrame.printScreen()
            elements =[ ]
            try:
                status,t = self._devices[key].GET_STATUS_P_B()
                generalStatus = status[0]
                secondaryStatus = status[1]
                
                #General Status
                #bits0-2
                shutdownValue = (generalStatus & 0x7)
                shutdownStatusValue="Shutdown Status reported a normal condition"
                if (shutdownValue==0x0):
                    shutdownStatusValue= "No attempt has been made to start the PSU"
                if (shutdownValue==0x1):
                    shutdownStatusValue= "EDFA Status Fault"
                if (shutdownValue==0x2):
                    shutdownStatusValue= "Input Power Limits"
                if (shutdownValue==0x3):
                    shutdownStatusValue= "Output Power Limits"
                if (shutdownValue==0x4):
                    shutdownStatusValue= "PSU voltaje out of limits"
                if (shutdownValue==0x5):
                    shutdownStatusValue= "PSU current out of limits"
                if (shutdownValue==0x6):
                    shutdownStatusValue= "HeatSink Temp Out of Limits"
                if (shutdownValue==0x7):
                    shutdownStatusValue= "Shutdown by M&C Command"
                #bit 5
                maintenanceValue = (generalStatus & 0x20)
                if (maintenanceValue==0x20):
                    maintenanceStatusValue="The board is in maintenance mode"
                else:
                    maintenanceStatusValue="Not in maintenance mode"
                
                #bits6-7
                operationValue = (generalStatus & 0xC0)
                if (operationValue == 0x0):
                    operationStatusValue= "The EDFA PSU is on and the EDFA is in normal operation"
                if (operationValue == 0x1):
                    operationStatusValue= "The EDFA PSU is on and the EDFA is initializing"
                if (operationValue == 0x2):
                    operationStatusValue= "The EDFA PSU is shutdown"
                if (operationValue == 0x3):
                    operationStatusValue= "The EDFA PSU is shutdown"
                
                #Secondary Status
                #bits0-2
                lastRestartValue = (secondaryStatus & 0x7)
                lastRestartStatusValue= "Normal Condition"
                if (lastRestartValue==0x0):
                    lastRestartStatusValue= "A reset from SPI MCLR"
                if (lastRestartValue==0x1):
                    lastRestartStatusValue= "PowerUp - reset"
                if (lastRestartValue==0x2):
                    lastRestartStatusValue= "Reset cause by a voltage dip..Brown out"
                if (lastRestartValue==0x3):
                    lastRestartStatusValue= "Watchdog reset ...time out"
                #bits4-6
                serialPortValue = (secondaryStatus & 0x70)
                if (serialPortValue == 0x0):
                    serialPortStatusValue= "The buffer is ready to receive data from the SPI"
                if (serialPortValue == 0x1):
                    serialPortStatusValue= "Data is being sent from the buffer to the EDFA"
                if (serialPortValue == 0x2):
                    serialPortStatusValue= "Waiting for Reply from EDFA"
                if (serialPortValue == 0x3):
                    serialPortStatusValue= "Data is being received by the buffer from EDFA"
                if (serialPortValue == 0x4):
                    serialPortStatusValue= "There is a complete message in the buffer"
                if (serialPortValue == 0x5):
                    serialPortStatusValue= "Timed out waiting data"
                if (serialPortValue == 0x6):
                    serialPortStatusValue= "A data overrun ocurred while receiving EDFA data"
                if (serialPortValue == 0x7):
                    serialPortStatusValue= "A framing error ocurred while receiving EDFA data"
                
            except:
                shutdownStatusValue = "N/A"
                maintenanceStatusValue = "N/A"
                operationStatusValue = "N/A"
                lastRestartStatusValue = "N/A"
                serialPortStatusValue = "N/A"
            
            elements.append( StatusHelper.Line("Shutdown Status",[StatusHelper.ValueUnit(shutdownStatusValue)] ))
            elements.append( StatusHelper.Line("Maintenance Status",[StatusHelper.ValueUnit(maintenanceStatusValue)]))
            elements.append( StatusHelper.Line("Operation Status",
                                               [StatusHelper.ValueUnit(operationStatusValue)]) )
            elements.append( StatusHelper.Line("Last Restart Status",
                                               [StatusHelper.ValueUnit(lastRestartStatusValue)]) )
            elements.append( StatusHelper.Line("Serial Port Status",
                                               [StatusHelper.ValueUnit(serialPortStatusValue)]) )
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName +" SPB", elements)
            statusFrame.printScreen()
        
