#ifndef WeatherStationEthSim_H
#define WeatherStationEthSim_H
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <WeatherStationEth.h>


/// The WeatherStationEthSim class is the vendor class for the
/// Weather Station.
/// <ul>
/// <li> Device:   Weather Station SimulationComponent
/// <li> Assembly: WeatherStation
/// </ul>
class WeatherStationEthSim: public WeatherStationEth
{
    public:
     /// Constructor
    WeatherStationEthSim();

    /// Destructor
    ~WeatherStationEthSim();
    virtual void refreshXML();    
    virtual void monitor(int address, double& value);
    virtual void monitor(int address, vector< double >& value);
};
#endif

