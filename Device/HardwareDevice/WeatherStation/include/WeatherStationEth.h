#ifndef WeatherStationEth_H
#define WeatherStationEth_H
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <EthernetDeviceInterface.h>

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <time.h>
#include <exception>
// For errno
#include <cerrno>

// For strerror, memset
#include <cstring>

// For atoi
#include <cstdlib>

// For parsing XML
#include <phonyXmlTmcdbComponent.h>
#include <configDataAccess.h>
#include <controlXmlParser.h>

// Generated classed needed to read from the web services
// (they should be regenerated if the webservice definition changes)
#include "soapgetCurrentWeatherDataBindingProxy.h"
#include "getCurrentWeatherDataBinding.nsmap"

#include <pthread.h>

using std::string;
using std::ifstream;
using std::vector;
using std::multimap;
using std::auto_ptr;
using std::ostringstream;
using std::stringstream;



/// The WeatherStationEth class is the vendor class for the
/// Weather Station.
/// <ul>
/// <li> Device:   Weather Station Component
/// <li> Assembly: WeatherStation
/// </ul>
class WeatherStationEth: public EthernetDeviceInterface
{
    public:
    /// Enums allowing the access via names instead of numbers.
    typedef enum
    {
       WS_GetHumidity = 0,
       WS_GetTemperature = 1,
       WS_GetWindDirection = 2,
       WS_GetWindSpeed = 3,
       WS_GetPressure = 4,
       WS_GetLatitude = 5,
       WS_GetLongitude = 6,
       WS_GetAltitude = 7,
       WS_GetWeatherStationName = 8,      
       WS_GetPositionX = 9,
       WS_GetPositionY = 10,
       WS_GetPositionZ = 11,
       WS_GetDewPoint = 12,
       WS_GetUniqueId = 13
    } MonitorControlAddress;


    /// Constructor
    WeatherStationEth();

    /// Destructor
    ~WeatherStationEth();

  protected:
    
    time_t lastTime;
    
    string weatherStationId;    
    double timestamp;
    double temperature;
    double pressure;
    double windDirection;
    double windSpeed;
    double humidity;
    double positionX;
    double positionY;
    double positionZ;
    double dewPoint;

    string uniqueId;
    
    string webServiceXSD;
    // For managing refreshXML thread
    // if the last value read was valid, the monitor function will return the value
    // if the last value was invalid, the refreshError flag will be set to true
    // and a exception will be thrown
    pthread_t refreshXMLpthread;
    bool refreshXMLrunning;
    bool refreshError;
    string errorMessage;

  public:
    /// EthernetDeviceInterface Methods
    /// Takes the IP address and port number of the device and connects to the
    /// device.  Throws a SocketOperationFailed exception
    /// \exception SocketOperationFailedException If the socket connection
    /// fails.
    virtual void open(const string& _hostname, unsigned short _port,
        double _timeout, unsigned int _lingerTime, unsigned short _retries,
        const multimap< string, string >& parameters);

    /// Close the socket.
    virtual int close();
    
    /// parse values from the xmlString
    /// \exception SocketOperationFailedException if there are problems parsing the XML
    double getDoubleValue(char *attributeName);
    /// \exception SocketOperationFailedException if there are problems parsing the XML
    long getLongValue(char *attributeName);
    /// \exception SocketOperationFailedException if there are problems parsing the XML
    string getStringValue(char *attributeName);
    


    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, bool& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned char& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, char& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned short& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, short& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned int& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, int& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, long long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned long long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, float& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, double& value);
    
    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, long double& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, string& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< bool >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< unsigned char >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< char >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< unsigned short >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< short >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< unsigned int >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< int >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< unsigned long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< unsigned long long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< long long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< float >& value);
/*
    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< double >& value);
*/
    /// For humidity, wind direction, wind speed, temperature and pressure
    virtual void monitor(int address, vector< double >& value);
    
    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< long double >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, vector< string >& value);



    
    
    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address);


    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, bool value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned char value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, char value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned short value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, short value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned int value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, int value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned long long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, long long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, float value);


    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, double value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, long double value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const string& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const vector< bool >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< unsigned char >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const vector< char >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< unsigned short >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const vector< short >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< unsigned int >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const vector< int >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< unsigned long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const vector< long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< unsigned long long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< long long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const vector< float >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const vector< double >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< long double >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const vector< string >& value);


    protected:

    /// Utility functions.

    /// toString: these functions return string representations of
    /// various types.
    /// \param in parameter which will be converted to a string.
    /// \return string representation of \ref in
    /// \exception IllegalParameterException
    template< typename T > string toString(const T& in) const;
    
    /// parseParameter: these funtion is used inside open function to get a parameter
    ///                 from the multimap 'parameters'
    /// \param in parameters: multimap with the list of the parameters, in this case: username, password and database
    /// \param in par: the desired parameter
    /// \return the string corresponding to the parameter 'par' 
    /// \exception SocketOperationFailedException If there is any problem trying to get the param
    string parseParameter(multimap< string, string >& parameters,string par);  
    string parseParameter(multimap< string, string >& parameters,char *par);
    
    /// \exception DeviceValueException
    void unassignedAddress(int address) const;

    /// \exception SocketOperationFailedException
    virtual void refreshXML();    

    /// \exception SocketOperationFailedException
    virtual void getHwSerialNumber();

    // this is a static method for calling refreshXML form a thread
    static void *threadHandler(void *pthis);
    
    /// \exception SocketOperationFailedException
    void connectDB();
    
    /// \exception SocketOperationFailedException
    void disconnectDB();    

    
    
    /// Hostname for the currently open connection.
    string hostname;

    /// Port for the currently open connection.
    unsigned short port;

    /// Rx/Tx timeout in s for the currently open connection.
    double timeout;

    /// Linger time in s for the currently open connection.
    unsigned int lingerTime;

    /// Number of allowed retries for the currently open connection.
    unsigned short retries;

    private:
    /// No copy constructor.
    WeatherStationEth(const WeatherStationEth&);

    /// No assignment operator.
    WeatherStationEth& operator=(const WeatherStationEth&);

};

#include <WeatherStationEth.i>

#endif

