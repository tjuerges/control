#include <string>
std::string g_webServiceXSD="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
<xs:schema xmlns:xs='http://www.w3.org/2001/XMLSchema' elementFormDefault='qualified'>\
  <xs:element name='weather'>\
    <xs:complexType>\
      <xs:sequence>\
        <xs:element ref='sensors'/>\
      </xs:sequence>\
      <xs:attribute name='id' use='required' type='xs:integer'/>\
      <xs:attribute name='timestamp' use='required' type='xs:double'/>\
      <xs:attribute name='status' use='required' type='xs:string'/>\
      <xs:attribute name='serialNumber' use='required' type='xs:string'/>\
    </xs:complexType>\
  </xs:element>\
  <xs:element name='sensors'>\
    <xs:complexType>\
      <xs:sequence>\
        <xs:element maxOccurs='unbounded' ref='sensor'/>\
      </xs:sequence>\
    </xs:complexType>\
  </xs:element>\
  <xs:element name='sensor'>\
    <xs:complexType>\
      <xs:simpleContent>\
        <xs:extension base='xs:decimal'>\
          <xs:attribute name='name' use='required'/>\
          <xs:attribute name='unit' use='required'/>\
        </xs:extension>\
      </xs:simpleContent>\
    </xs:complexType>\
  </xs:element>\
</xs:schema>";
