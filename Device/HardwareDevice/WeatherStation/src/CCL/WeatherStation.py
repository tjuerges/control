#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a WeatherStation device.
"""


import CCL.WeatherStationBase
from CCL import StatusHelper  
import os


class WeatherStation(CCL.WeatherStationBase.WeatherStationBase):
    '''
    The WeatherStation class inherits from the code generated WeatherStationBase
    class and adds specific methods.
    '''
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The constructor creates a WeatherStation object by making use of
        the WeatherStationBase constructor.
        EXAMPLE:
        from CCL.WeatherStation import WeatherStation
        ws = WeatherStation()
        '''

        try:
            CCL.WeatherStationBase.WeatherStationBase.__init__(self, None, componentName, stickyFlag)
        except Exception, e:
            print 'WeatherStation component is not running \n' + str(e)

    def __del__(self):
        CCL.WeatherStationBase.WeatherStationBase.__del__(self)


#TODO
#    def STATUS(self):
#
#        '''
#        This method prints the status of WeatherStation
#        '''
#        os.system("clear")
#
#        '''Get component and antenna name'''
#        listNames = self._HardwareDevice__componentName.rsplit("/")
#        antName = listNames[1]
#        compName = listNames[2]
#
#        try:
#            freq = StatusHelper.Line("FREQUENCY", \
#                                     [StatusHelper.ValueUnit(\
#                                     self.GET_FREQUENCY()[0], "Hz")])
#        except:
#            freq = StatusHelper.Line("FREQUENCY", [StatusHelper.ValueUnit("N/A")])
#
#        try:
#            amp = StatusHelper.Line("AMPLITUDE", \
#                                     [StatusHelper.ValueUnit(\
#                                     self.GET_AMPLITUDE()[0], "dBM")])
#        except:
#            amp = StatusHelper.Line("AMPLITUDE", [StatusHelper.ValueUnit("N/A")])
#
#        frame = StatusHelper.Frame(compName + "   Ant: " + antName, \
#                [freq, amp])
#        frame.printScreen()

