/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */


#include <WeatherStationImpl.h>
#include <unistd.h>

WeatherStationImpl::WeatherStationImpl(const ACE_CString& name, maci::ContainerServices* cs):
    WeatherStationBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

std::string WeatherStationImpl::getUniqueId(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::string value;
    try
    {
        EthernetDevice::monitor(addressUniqueId, value, timestamp);
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, "WeatherStationBase::getUniqueId");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::IllegalParameterExImpl nex(ex, __FILE__,
        __LINE__, "WeatherStationBase::getUniqueId");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, "WeatherStationBase::getUniqueId");
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, "WeatherStationBase::getUniqueId");
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value;
}


void WeatherStationImpl::initialize()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);
  string weatherId; 
  std::ostringstream  strPositionX, strPositionY, strPositionZ;
  WeatherStationBase::initialize();
  string elementName;
  
    try {
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_DEBUG,"Reading WeatherStation Configuration"));
        elementName="weatherStationParameters";
        std::auto_ptr<const ControlXmlParser> cdbConfig = getElement(elementName.c_str());

        elementName="weatherStationId";	
        weatherId = cdbConfig->getStringAttribute(elementName.c_str());
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_DEBUG,string(elementName+string("= ")+weatherId).c_str()));

        elementName="positionX";	
	strPositionX.precision(12);	
        strPositionX << cdbConfig->getDoubleAttribute(elementName.c_str());
	ACS_LOG(LM_SOURCE_INFO,__func__,(LM_DEBUG,string(elementName+string("= ")+strPositionX.str()).c_str()));
 		
        elementName="positionY";
	strPositionY.precision(12);	
        strPositionY << cdbConfig->getDoubleAttribute(elementName.c_str());
	ACS_LOG(LM_SOURCE_INFO,__func__,(LM_DEBUG,string(elementName+string("= ")+strPositionY.str()).c_str()));
	
        elementName="positionZ";
	strPositionZ.precision(12);	
        strPositionZ << cdbConfig->getDoubleAttribute(elementName.c_str());
	ACS_LOG(LM_SOURCE_INFO,__func__,(LM_DEBUG,string(elementName+string("= ")+strPositionZ.str()).c_str()));


    } catch(ControlExceptions::XmlParserErrorExImpl &_ex) {
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_ERROR,"Could not read %s CDB element",elementName.c_str()));
        throw acsErrTypeLifeCycle::LifeCycleExImpl(_ex,__FILE__, __LINE__, __func__);
    } catch(...) {
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_ERROR,"Unknown exception caught"));
        throw acsErrTypeLifeCycle::LifeCycleExImpl(__FILE__, __LINE__, __func__);
    }

   EthernetDevice::parameters_m.insert(make_pair(string("weatherStationId"),weatherId));
   EthernetDevice::parameters_m.insert(make_pair(string("positionX"),strPositionX.str()));
   EthernetDevice::parameters_m.insert(make_pair(string("positionY"),strPositionY.str()));
   EthernetDevice::parameters_m.insert(make_pair(string("positionZ"),strPositionZ.str()));
}

void WeatherStationImpl::hwConfigureAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    string serialNumber="";
    ACS::Time timestamp;
    EthernetDevice::hwConfigureAction();
    // at this point, the thread that read from the web service should be working.
    // we will try to read the serial number for a couple of times until we get a valid value
   try {
    serialNumber=getUniqueId(timestamp);


   } catch (const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex) {

        ostringstream message;
        message << "Cannot get serial number for the specified weatherStation. Probably you have no access to the web service"; 
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,__FILE__, __LINE__,
            "WeatherStationImpl::hwConfigureAction()");
        nex.addData("Detail", message.str());
        nex.log();
        throw nex.getHwLifecycleEx();
    }
    setSerialNumber(serialNumber);
}


WeatherStationImpl::~WeatherStationImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

    /// MonitorPoint: HUMIDITY

double WeatherStationImpl::getHumidity(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    vector<double> value;
    if(isReady() == false)

    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            "WeatherStationImpl::getHumidity");
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        EthernetDevice::monitor(addressHumidity, value, timestamp);
	timestamp=(ACS::Time)value[0];
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, "WeatherStationImpl::getHumidity");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::IllegalParameterExImpl nex(ex, __FILE__,
        __LINE__, "WeatherStationImpl::getHumidity");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, "WeatherStationImpl::getHumidity");
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, "WeatherStationImpl::getHumidity");
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value[1];
}


    /// MonitorPoint: TEMPERATURE

double WeatherStationImpl::getTemperature(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    vector<double> value;

    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            "WeatherStationImpl::getTemperature");
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        EthernetDevice::monitor(addressTemperature, value, timestamp);
        timestamp=(ACS::Time)value[0];
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, "WeatherStationImpl::getTemperature");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::IllegalParameterExImpl nex(ex, __FILE__,
        __LINE__, "WeatherStationImpl::getTemperature");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, "WeatherStationImpl::getTemperature");
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, "WeatherStationImpl::getTemperature");
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value[1];

}


    /// MonitorPoint: WINDSPEED

double WeatherStationImpl::getWindspeed(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    vector<double> value;

    if(isReady() == false)

    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            "WeatherStationImpl::getWindspeed");
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        EthernetDevice::monitor(addressWindspeed, value, timestamp);
	timestamp=(ACS::Time)value[0];
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, "WeatherStationImpl::getWindspeed");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::IllegalParameterExImpl nex(ex, __FILE__,
        __LINE__, "WeatherStationImpl::getWindspeed");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, "WeatherStationImpl::getWindspeed");
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, "WeatherStationImpl::getWindspeed");
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value[1];
}


    /// MonitorPoint: WINDDIRECTION

double WeatherStationImpl::getWinddirection(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    vector<double> value;

    if(isReady() == false)

    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            "WeatherStationImpl::getWinddirection");
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        EthernetDevice::monitor(addressWinddirection, value, timestamp);
	timestamp=(ACS::Time)value[0];
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, "WeatherStationImpl::getWinddirection");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::IllegalParameterExImpl nex(ex, __FILE__,
        __LINE__, "WeatherStationImpl::getWinddirection");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, "WeatherStationImpl::getWinddirection");
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, "WeatherStationImpl::getWinddirection");
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value[1];
}


    /// MonitorPoint: PRESSURE

double WeatherStationImpl::getPressure(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    vector<double> value;

    if(isReady() == false)

    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            "WeatherStationImpl::getPressure");
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        EthernetDevice::monitor(addressPressure, value, timestamp);
	timestamp=(ACS::Time)value[0];
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, "WeatherStationImpl::getPressure");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::IllegalParameterExImpl nex(ex, __FILE__,
        __LINE__, "WeatherStationImpl::getPressure");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, "WeatherStationImpl::getPressure");
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, "WeatherStationImpl::getPressure");
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value[1];
}


    /// MonitorPoint: DEWPOINT

double WeatherStationImpl::getDewpoint(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    vector<double> value;

    if(isReady() == false)

    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            "WeatherStationImpl::getDewpoint");
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    try
    {
        EthernetDevice::monitor(addressDewpoint, value, timestamp);
	timestamp=(ACS::Time)value[0];
        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, "WeatherStationImpl::getDewpoint");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::IllegalParameterExImpl nex(ex, __FILE__,
        __LINE__, "WeatherStationImpl::getDewpoint");
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, "WeatherStationImpl::getDewpoint");
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();
        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, "WeatherStationImpl::getDewpoint");
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value[1];
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(WeatherStationImpl)


