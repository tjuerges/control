/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

#include <WeatherStationEthSim.h>
#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()


/// WeatherStationEthSim Methods

WeatherStationEthSim::WeatherStationEthSim()
{
    timestamp=0;
    lastTime=0;
    // Initialization of sensor default values
    humidity=0.3;     // random humidity between 0.2 and 0.4
    temperature=10.0;     // random temperature between 5-15[C]
    windDirection=40.0; // random wind direction between 20-60 degrees
    windDirection=windDirection*3.141592653589793238/180.0; // Converting degrees to rad
    windSpeed=10.0;  // random windSpeed between 5-15 m/s
    pressure=55550.0;   // random pressure, between 55600-55500 Pa
    dewPoint=5.0;       // random dew point, betweeb 0-10 C
}

WeatherStationEthSim::~WeatherStationEthSim()
{
}


void WeatherStationEthSim::refreshXML()  {
    time_t currentTime=time(NULL);
    // we refreash the XML only after 'timeout' seconds
    if ( (currentTime-lastTime) < timeout)
      return;
    lastTime=currentTime;
    srand(time(0));  // Initialize random number generator.
    timestamp =  currentTime* 10000000LL + 122192928000000000LL;

    // setting humidity value (max step=+-0.03)
    humidity=humidity+((rand()%600)/10000.0)-0.03;     // random humidity between  0.2 and 0.4
    if (humidity>0.4) humidity=0.4;
    if (humidity<0.2) humidity=0.2;

    // setting temperature value (max step +-0.2 [C])
    temperature=temperature+((rand()%40)/100.0)-0.2;     // random temperature between 5-15[C]
    if (temperature>15.0) temperature=15.0;
    if (temperature<5.0) temperature=5.0;

    // setting wind direction (max step +- 5 degrees)  // random wind direction between 20-60 degrees
    double windDirectionDelta=((rand()%10000)/1000.0)-5.0;
    windDirection=windDirection+windDirectionDelta*3.141592653589793238/180.0; // adding and converting to rad
    if (windDirection<(20.0*3.141592653589793238/180.0)) windDirection=20.0*3.141592653589793238/180.0;
    if (windDirection>(60.0*3.141592653589793238/180.0)) windDirection=60.0*3.141592653589793238/180.0;
 
    // setting wind speed (max step +- 1 [m/s])
    windSpeed=windSpeed+((rand()%200)/100.0)-1;       // random windSpeed between 5-15 m/s
    if (windSpeed>15.0) windSpeed=15.0;
    if (windSpeed<5.0) windSpeed=5.0;

    // setting pressure (max step +- 10 Pa)
    pressure=pressure+((rand()%200)/10.0)-10;   // random pressure, between 55500-55600 Pa
    if (pressure>55600) pressure=55600;
    if (pressure<55500) pressure=55500;

    // setting dew point (max step +- 0.3 [C])
    dewPoint=dewPoint+((rand()%600)/1000.0)-0.3;       // random dew point, between (-10)-10 C
    if (dewPoint>10.0) dewPoint=10.0;
    if (dewPoint<-10.0) dewPoint=-10.0;
}

void WeatherStationEthSim::monitor(int address, double& value)
{
    refreshXML();
    WeatherStationEth::monitor(address,value);
}

void WeatherStationEthSim::monitor(int address, vector< double >& value)
{
    refreshXML();
    WeatherStationEth::monitor(address,value);
}

