/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */


#include <WeatherStationCompSimImpl.h>
#include <maciACSComponentDefines.h>
#include <TypeConversion.h>
#include <SimulatedSerialNumber.h>

WeatherStationCompSimImpl::WeatherStationCompSimImpl(const ACE_CString& name, maci::ContainerServices* cs):
    WeatherStationImpl(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::string componentName(name.c_str());
    unsigned long long hashed_sn=AMB::Utils::getSimSerialNumber(componentName,"WeatherStation");
    std::ostringstream msg;
    msg<< "simSerialNumber for  " << name.c_str() << "  with assembly name " << "WeatherStation" << " is " << std::hex << "0x" << hashed_sn << std::endl;
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, msg.str().c_str()));
    std::ostringstream sn;
    sn << std::hex << "0x" << hashed_sn;
    setSerialNumber(sn.str());
}
void WeatherStationCompSimImpl::hwConfigureAction()
{
    EthernetDevice::hwConfigureAction();
}

void WeatherStationCompSimImpl::initialize()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);
  WeatherStationImpl::initialize();
  ethernetDeviceIF_mp.reset(new WeatherStationEthSim);
}

WeatherStationCompSimImpl::~WeatherStationCompSimImpl()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);
}

 
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(WeatherStationCompSimImpl)


