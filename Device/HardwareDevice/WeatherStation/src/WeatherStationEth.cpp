/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

#include <WeatherStationEth.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include "webServiceXSD.h"
#include <unistd.h>

extern std::string g_webServiceXSD;

/// WeatherStationEth Methods

WeatherStationEth::WeatherStationEth():
    port(0U),
    timeout(5.0),
    lingerTime(3U),
    retries(0U)
{
    timestamp=0;
    lastTime=0;
    webServiceXSD=g_webServiceXSD;
    refreshXMLrunning=false;
    refreshXMLpthread=0;
    refreshError=false;
    uniqueId="1100";
}

WeatherStationEth::~WeatherStationEth()
{
    close();
}


void WeatherStationEth::open(const string& _hostname, unsigned short _port,
    double _timeout, unsigned int _lingerTime, unsigned short _retries,
    const multimap< string, string >& parameters)
{
    const string fnName("WeatherStationEth::open");
    // Store the current connection parameters and all relevant data for a
    // potential reconnect.

    hostname = _hostname;
    port = _port;
    timeout = _timeout;
    lingerTime = _lingerTime;
    retries = _retries; 
    try {
       weatherStationId=parameters.find("weatherStationId")->second;
       positionX=atof(parameters.find("positionX")->second.c_str());
       positionY=atof(parameters.find("positionY")->second.c_str());
       positionZ=atof(parameters.find("positionZ")->second.c_str());
    } catch(...) {
        string errorMessage=string("WeatherStationEth::open: error reading parameters.");
        throw SocketOperationFailedException(errorMessage);
    }
    refreshXMLrunning=true;
    pthread_create(&refreshXMLpthread, NULL,threadHandler, (void *)this);

}


int WeatherStationEth::close()
{
    refreshXMLrunning=false;
    refreshXMLpthread=0;
    return 0;
}

void *WeatherStationEth::threadHandler(void *pthis)
{
   ((WeatherStationEth *)(pthis))->refreshXML();
   return NULL;
}


void WeatherStationEth::getHwSerialNumber() {
   // Read the SN from the web service 
  ConfigDataAccessor *xmlData=NULL;
  getCurrentWeatherDataBindingProxy service;
  string reading;
  string status;  
  string xmlContent;
  string xmlString;
    try 
    {
	if (service.getCurrentWeatherData(weatherStationId, xmlContent) != SOAP_OK)
	{
	  errorMessage=string("WeatherStationEth::getHwSerialNumber: error connecting to the web service ")+weatherStationId;
	  throw SocketOperationFailedException(errorMessage.c_str());
	}
        xmlString = string("<weather xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='membuffer.xsd'");
        xmlString.append(xmlContent.erase(0,8));
    } 
    catch(...)
    {
	errorMessage=string("WeatherStationEth::getHwSerialNumber: error connecting to the web service, ")+weatherStationId;
        throw SocketOperationFailedException(errorMessage.c_str());
    }
	
    // parse the data from the XML
    try 
    {
        xmlData = new ConfigDataAccessor(xmlString, webServiceXSD);
    } 
    catch(ControlExceptions::XmlParserErrorExImpl ex)
    {
	errorMessage=string("WeatherStationEth::getHwSerialNumber: XmlParserErrorExImpl, error checking XML against XSD: ")+string(ex.getShortDescription())+xmlString;
        throw SocketOperationFailedException(errorMessage.c_str());
    }
    catch(...) {
	errorMessage=string("WeatherStationEth::getHwSerialNumber: error checking XML against XSD. XML=")+xmlString;
        throw SocketOperationFailedException(errorMessage.c_str());
    }
    try {
        reading="serialNumber";
        uniqueId = xmlData->getStringAttribute("serialNumber");
	}
    catch(...)
    {
	errorMessage="WeatherStationEth::refreshXML: error reading ";
	errorMessage=errorMessage+reading;
        throw SocketOperationFailedException(errorMessage.c_str());
    }
}

void WeatherStationEth::refreshXML()  {
  ConfigDataAccessor *xmlData=NULL;
  getCurrentWeatherDataBindingProxy service;
  string reading;
  string status;  
  string xmlContent;
  string xmlString;
  
  while(refreshXMLrunning)
  {
    // Read the value from the web service 
    try {
           if (xmlData!=NULL) delete xmlData;
           xmlData=NULL;
    }
    catch(...)
    {
        // FIXME: OK, I couldn't deleted,I have to check why
    }
    try 
    {
	if (service.getCurrentWeatherData(weatherStationId, xmlContent) != SOAP_OK)
	{
	  errorMessage=string("WeatherStationEth::refreshXML: error reading the XML file for weather station ")+weatherStationId;
          refreshError=true;
          continue;
	}
        xmlString = string("<weather xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='membuffer.xsd'");
        xmlString.append(xmlContent.erase(0,8));
    } 
    catch(...)
    {
	errorMessage=string("WeatherStationEth::refreshXML: error reading the XML file for weather station ")+weatherStationId;
        refreshError=true;
        continue;
    }
	
	
    // parse the data from the XML
    try 
    {
        xmlData = new ConfigDataAccessor(xmlString, webServiceXSD);
    } 
    catch(ControlExceptions::XmlParserErrorExImpl ex)
    {
	errorMessage=string("WeatherStationEth::refreshXML: error checking XML against XSD: ")+string(ex.getShortDescription())+xmlString;
        refreshError=true;
        continue;
    }
    catch(...) {
	errorMessage=string("WeatherStationEth::refreshXML: error checking XML against XSD. XML=")+xmlString;
        refreshError=true;
        continue;
    }

	
    try {
	reading="status";
	status = xmlData->getStringAttribute("status");
    }
    catch(...)
    {
        errorMessage="WeatherStationEth::refreshXML: error reading ";
        errorMessage=errorMessage+reading;
	if (xmlData!=NULL) delete xmlData;
            refreshError=true;
            continue;
    }
    if   ( status!=string("true") ){
           refreshError=true;
           errorMessage="Data marked as invalid in the XML";
           continue;
        }   
        // instead if throwing an exception, we are interested in keep trying

	try {
		reading="timestamp";
		timestamp = xmlData->getDoubleAttribute("timestamp");
		//get the list of sensors
		auto_ptr<vector<ControlXmlParser> > list = xmlData->getElement("sensors")->getElements("sensor");
		for(unsigned int i=0; i < (*list).size(); i++)
		{
			reading=(*list)[i].getStringAttribute("name");
			if (reading == string("humidity"))
			{
			    humidity=atof( (*list)[i].getStringData().c_str() );
			    humidity=humidity/100.0;
			}
			else if (reading == string("temperature"))
			    temperature=atof( (*list)[i].getStringData().c_str() );
			else if (reading == string("wind direction"))
			{
			    windDirection=atof( (*list)[i].getStringData().c_str() );
			    windDirection=windDirection*3.141592653589793238/180.0; // Converting degrees to rad
			}
			else if (reading == string("wind speed"))
			    windSpeed=atof( (*list)[i].getStringData().c_str() );
			else if (reading == string("pressure")) 
			{
			    pressure=atof( (*list)[i].getStringData().c_str() );
			    pressure=100.0*pressure; // Converting hPa to Pa
			}
			else if (reading == string("dewpoint"))
			    dewPoint=atof( (*list)[i].getStringData().c_str() );
		}
	}
	catch(...)
	{
		errorMessage="WeatherStationEth::refreshXML: error reading ";
		errorMessage=errorMessage+reading;
                refreshError=true;
                continue;
	}
        refreshError=false;
        usleep(4000000);
    }
}


void WeatherStationEth::unassignedAddress(int address) const
{
    ostringstream strAddress;
    strAddress << address;
    throw SocketOperationFailedException(string(
        "WeatherStationEth::unassignedAddress: unassigned address ") + toString(address));
}


void WeatherStationEth::monitor(int address, bool& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for bool");
}

void WeatherStationEth::monitor(int address, unsigned char& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for unsigned char");
}

void WeatherStationEth::monitor(int address, char& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for char");
}

void WeatherStationEth::monitor(int address, unsigned short& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for unsigned "
        "short");
}

void WeatherStationEth::monitor(int address, short& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for short");
}

void WeatherStationEth::monitor(int address, unsigned int& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for unsigned int");
}

void WeatherStationEth::monitor(int address, int& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for int");
}

void WeatherStationEth::monitor(int address, unsigned long& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for unsigned long");
}

void WeatherStationEth::monitor(int address, long& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for long long");
}

void WeatherStationEth::monitor(int address, unsigned long long& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for unsigned long "
        "long");
}

void WeatherStationEth::monitor(int address, long long& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for long long");
}

void WeatherStationEth::monitor(int address, float& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for float");
}


void WeatherStationEth::monitor(int address, double& value)
{
    if(refreshError)
    {
        refreshError=false;
        throw SocketOperationFailedException(errorMessage.c_str());
    }

    switch(address)
    {
        case WS_GetHumidity:
        {
	   refreshXML();	  
           value=humidity;
        }
        break;
        case WS_GetTemperature:
        {
           value=temperature;
        }
        break;
        case WS_GetWindDirection:
        {
           value=windDirection;
        }
        break;
        case WS_GetWindSpeed:
        {
           value=windSpeed;
        }
        break;
        case WS_GetPressure:
        {
           value=pressure;
        }
        break;
        case WS_GetPositionX:
        {
           value=positionX;
        }
        break;
        case WS_GetPositionY:
        {
           value=positionY;
        }
        break;
        case WS_GetPositionZ:
        {
           value=positionZ;
        }
        break;
        case WS_GetDewPoint:
        {
           value=dewPoint;
        }
        break;	
	default:
        {
            unassignedAddress(address);
        }
        break;
    };

}

void WeatherStationEth::monitor(int address, long double& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for long double");
}

void WeatherStationEth::monitor(int address, string& value)
{
    getHwSerialNumber();
    if(refreshError)
    {
        refreshError=false;
        throw SocketOperationFailedException(errorMessage.c_str());
    }

    switch(address)
    {
        case WS_GetUniqueId:
        {
           value=uniqueId;
        }
        break;
        default:
        {
            unassignedAddress(address);
        }
        break;
    };
}

void WeatherStationEth::monitor(int address, vector< bool >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< bool >");
}

void WeatherStationEth::monitor(int address, vector< unsigned char >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< unsigned char >");
}

void WeatherStationEth::monitor(int address, vector< char >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< char >");
}

void WeatherStationEth::monitor(int address, vector< unsigned short >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< unsigned short >");
}

void WeatherStationEth::monitor(int address, vector< short >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< short >");
}

void WeatherStationEth::monitor(int address, vector< unsigned int >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< int >");
}

void WeatherStationEth::monitor(int address, vector< int >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< int >");
}

void WeatherStationEth::monitor(int address, vector< unsigned long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< unsigned long >");
}

void WeatherStationEth::monitor(int address, vector< long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< long >");
}

void WeatherStationEth::monitor(int address, vector< unsigned long long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< unsigned long long >");
}

void WeatherStationEth::monitor(int address, vector< long long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< long long >");
}

void WeatherStationEth::monitor(int address, vector< float >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< float >");
}

void WeatherStationEth::monitor(int address, vector< double >& value)
{
    if(refreshError)
    {
        refreshError=false;
        throw SocketOperationFailedException(errorMessage.c_str());
    }

    switch(address)
    {   
        case WS_GetHumidity:
        {
	   value.clear();
	   value.push_back(timestamp);
	   value.push_back(humidity);
        }
        break;
        case WS_GetTemperature:
        {
	   value.clear();
	   value.push_back(timestamp);
	   value.push_back(temperature);
        }
	break;
	case WS_GetWindDirection:
        {
	   value.clear();
	   value.push_back(timestamp);
	   value.push_back(windDirection);
        }
	break;
	case WS_GetWindSpeed:
        {
	   value.clear();
	   value.push_back(timestamp);
	   value.push_back(windSpeed);
        }
	break;
	case WS_GetPressure:
        {
	   value.clear();
	   value.push_back(timestamp);
	   value.push_back(pressure);
        }
        break;	
        case WS_GetDewPoint:
        {
	   value.clear();
	   value.push_back(timestamp);
	   value.push_back(dewPoint);
        }
        break;	
        case WS_GetPositionX:
        {
           value.clear();
           value.push_back(timestamp);
           value.push_back(positionX);
        }
        break;
        case WS_GetPositionY:
        {
           value.clear();
           value.push_back(timestamp);
           value.push_back(positionY);
        }
        break;
        case WS_GetPositionZ:
        {
           value.clear();
           value.push_back(timestamp);
           value.push_back(positionZ);
        }
        break;
        default:
        {
            unassignedAddress(address);
        }
        break;
    };
}

void WeatherStationEth::monitor(int address, vector< long double >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< long double >");
}

void WeatherStationEth::monitor(int address, vector< string >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::monitor: No monitor points defined for "
        "vector< string >");
}


void WeatherStationEth::command(int address)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for void");
}

void WeatherStationEth::command(int address, bool value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for bool");
}

void WeatherStationEth::command(int address, unsigned char value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for unsigned char");
}

void WeatherStationEth::command(int address, char value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for char");
}

void WeatherStationEth::command(int address, unsigned short value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for unsigned short");
}

void WeatherStationEth::command(int address, short value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for short");
}

void WeatherStationEth::command(int address, unsigned int value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for unsigned int");
}

void WeatherStationEth::command(int address, int value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for int");
}

void WeatherStationEth::command(int address, unsigned long value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for unsigned long");
}

void WeatherStationEth::command(int address, long value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for long");
}

void WeatherStationEth::command(int address, unsigned long long value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for unsigned long "
        "long");
}

void WeatherStationEth::command(int address, long long value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for long long");
}

void WeatherStationEth::command(int address, float value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for float");
}


void WeatherStationEth::command(int address, double value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for double");
}

void WeatherStationEth::command(int address, long double value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for long double");
}

void WeatherStationEth::command(int address, const string& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "string");
}

void WeatherStationEth::command(int address, const vector< bool >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< bool >");
}

void WeatherStationEth::command(int address,
    const vector< unsigned char >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< unsigned char >");
}

void WeatherStationEth::command(int address, const vector< char >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< char >");
}

void WeatherStationEth::command(int address,
    const vector< unsigned short >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< unsigned short >");
}

void WeatherStationEth::command(int address, const vector< short >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< short >");
}

void WeatherStationEth::command(int address,
    const vector< unsigned int >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< unsigned int >");
}

void WeatherStationEth::command(int address, const vector< int >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< int >");
}

void WeatherStationEth::command(int address,
    const vector< unsigned long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< unsigned long >");
}

void WeatherStationEth::command(int address, const vector< long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< long >");
}

void WeatherStationEth::command(int address,
    const vector< unsigned long long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< unsigned long long >");
}

void WeatherStationEth::command(int address, const vector< long long >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< long long >");
}

void WeatherStationEth::command(int address, const vector< string >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< string >");
}

void WeatherStationEth::command(int address, const vector< float >& value)
{
    throw SocketOperationFailedException(
        "WeatherStationEth::command: No command points defined for "
        "vector< float >");
}

void WeatherStationEth::command(int address, const vector< double >& value)
{
    throw SocketOperationFailedException(
        "EthernetDeviceInterface: No command points defined for "
        "vector< double >");
}

void WeatherStationEth::command(int address,
    const vector< long double >& value)
{
    throw SocketOperationFailedException(
        "EthernetDeviceInterface: No command points defined for "
        "vector< long double >");
}

// Utility function


string WeatherStationEth::parseParameter(multimap< string, string >& parameters,string par)
{
  
  try {
    return parameters.find(par)->second;
  }
  catch(...)
  {  
     string errorMessage("WeatherStationEth::open: error reading ");
     errorMessage+=par;
     throw SocketOperationFailedException(errorMessage);
  }
}

string WeatherStationEth::parseParameter(multimap< string, string >& parameters,char *par)
{
  return parseParameter(parameters,string(par));
}
