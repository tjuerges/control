/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */


#include <CVREth.h>
#include <string>
#include <iostream>


int main(int c __attribute__((unused)), char* v[] __attribute__((unused)))
{
    double freq(13e9);
    double amp(14.0);

    std::cout << "running....\n";

    try
    {
        // Create the socket
        VendorServerSocket server(7777);
        server.startServer();
        VendorServerSocket new_sock;
        char response[40];
        while(true)
        {
            std::cout << "Accepting connection\n";
            server.accept(new_sock);
            std::cout << "conection accepted\n";
            try
            {
                while(true)
                {
                    std::string data;
                    new_sock >> data;
                    if(data.size() == 0)
                        break;
                    *response = 0;
                    std::cout << "Received: " << data << ".\n";
                    // GET_FREQUENCY
                    if(data.compare(0, 6, ":FREQ?") == 0)
                    {
                        sprintf(response, "%f", freq);
                        std::cout << "Sending: " << response << ".\n";
                        new_sock << std::string(response);
                    }
                    //  GET_AMPLITUDE
                    else if(data.compare(0, 5, ":POW?") == 0)
                    {
                        sprintf(response, "%f", amp);
                        std::cout << "Sending: " << response << "\n";
                        new_sock << std::string(response);
                    }
                    // SET_FREQUENCY
                    else if(data.compare(0, 6, ":FREQ ") == 0)
                    {
                        std::cout << "setting Frequency\n";
                        sscanf(data.c_str(), ":FREQ %f \n", &freq);
                        std::cout << "Frequency set to: " << freq;
                    }
                    // SET_AMPLITUDE
                    else if(data.compare(0, 5, ":POW ") == 0)
                    {
                        std::cout << "setting Amp\n";
                        sscanf(data.c_str(), ":POW %fdbm\n", &amp);
                        std::cout << "Amplitude set to: " << amp;
                    }
                    else
                        std::cout << "No valid data received\n";
                }
            }
            catch ( VendorSocketException& e )
            {
                std::cout << "Exception was caught:" << e.description();
            }

        }
    }
    catch ( VendorSocketException& e )
    {
        std::cout << "Exception was caught:" << e.description()
            << "\nExiting.\n";
    }

    return 0;
}
