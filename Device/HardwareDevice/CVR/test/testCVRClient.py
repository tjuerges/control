#!/usr/bin/env python

'''
Test for the CVR ethernet component
'''
import unittest
import sys
import time
#from time import sleep
from Acspy.Clients.SimpleClient import PySimpleClient

class CVRTestCase(unittest.TestCase):

    def __init__(self, methodName='runTest'):
        try:
            self._client=PySimpleClient.getInstance()
            self._ref= self._client.getComponent("CONTROL/CVR")
        except:
            print 'Could not get Component..'
            exit(1)
        
        unittest.TestCase.__init__(self,methodName)


    def __del__(self):
        try:
            self._client.releaseComponent("CONTROL/CVR")
        except:
            print 'Could not release component'

        self._client.disconnect()


    def test01AMPLSetCommand(self):
        print "setCommand to set amplitude = -19.9 "
        prepend = 'POW:AMPL'
        value = -19.9
        append = 'dBm\n'
        self._ref.setCommand(prepend, value, append)

        
    def test02FreqRWProperty(self):
        '''
        Tests BACI RW property
        '''
        value = 19.8
        print self._ref._get_name(), "setFrequency RWProperty set a value :", value
        c = self._ref._get_setFrequency().set_sync(value)

    def test03FreqROProperty(self):
        '''
        Tests BACI RO property
        '''
        print self._ref._get_name(), "Frequency ROProperty read value of:"
        time.sleep(0.25)
        (r, c) = self._ref._get_Frequency().get_sync()
        print r

    def test04FreqSetCommand(self):
        print "setCommand to set frequency = 19.6"
        prepend = 'FREQ'
        value = 19.6
        append = 'GHz\n'
        self._ref.setCommand(prepend, value, append)

    def test05AmplROProperty(self):
        '''
        Tests BACI RO property
        '''
        print self._ref._get_name(), "Amplitude ROProperty read value of:"
        time.sleep(0.25)
        (r, c) = self._ref._get_Amplitude().get_sync()
        print r
        
if __name__ == '__main__':
    unittest.main()            












