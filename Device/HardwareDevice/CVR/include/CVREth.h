#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef CVREth_i
#define CVREth_i
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Apr 16, 2009  created
//


#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <EthernetDeviceInterface.h>
#include <Mutex.h>

// For sockaddr_in
#include <netinet/in.h>


/// The CVREth class is the vendor class for the
/// Central Variable Reference.
/// <ul>
/// <li> Device:   Central Variable Reference
/// <li> Assembly: CVR
/// </ul>
class CVREth: public EthernetDeviceInterface
{
    public:
    /// Enums allowing the access via names instead of numbers.
    typedef enum
    {
        CVR_GetFrequency = 0,
        CVR_GetAmplitude = 1,
        CVR_SetFrequency = 10,
        CVR_SetAmplitude = 11,
        CVR_RfOn = 12,
        CVR_RfOff = 13,
        CVR_GetSerialNumber = 14
    } MonitorControlAddress;

    /// Constructor
    CVREth();

    /// Destructor
    virtual ~CVREth();

    /// EthernetDeviceInterface Methods
    /// Takes the IP address and port number of the device and connects to the
    /// device.  Throws a SocketOperationFailed exception
    /// \exception SocketOperationFailedException If the socket connection
    /// fails.
    virtual void open(const std::string& hostname, unsigned short port,
        double timeout, unsigned int lingerTime, unsigned short retries,
        const std::multimap< std::string, std::string >& parameters);

    /// Close the socket.
    virtual int close();


    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, bool& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned char& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, char& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned short& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, short& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned int& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, int& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, long long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, unsigned long long& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, float& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    /// virtual void monitor(int address, double& value);
    /// For FREQUENCY, AMPLITUDE
    virtual void monitor(int address, double& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, long double& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::string& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< bool >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< unsigned char >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< char >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< unsigned short >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< short >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< unsigned int >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< int >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< unsigned long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< unsigned long long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< long long >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< float >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< double >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< long double >& value);

    /// Unused monitor function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void monitor(int address, std::vector< std::string >& value);


    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    /// virtual void command(int address);
    /// For SET_RF_ON, SET_RF_OFF
    virtual void command(int address);


    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, bool value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned char value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, char value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned short value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, short value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned int value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, int value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, unsigned long long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, long long value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, float value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    /// virtual void command(int address, double value);
    /// For SET_FREQUENCY, SET_AMPLITUDE
    void command(int address, double value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, long double value);

    /// Unused command function.  When called, it throws an exception.
    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::string& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::vector< bool >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< unsigned char >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::vector< char >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< unsigned short >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::vector< short >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< unsigned int >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::vector< int >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< unsigned long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::vector< long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< unsigned long long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< long long >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::vector< float >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address, const std::vector< double >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< long double >& value);

    /// Unused command function.  When called, it throws an exception.
    /// \exception SocketOperationFailedException
    virtual void command(int address,
        const std::vector< std::string >& value);


    protected:
    /// Initialise the hardware.
    virtual void initialiseHardware();

    /// Utility functions.

    /// toString: these functions return std::string representations of
    /// various types.
    /// \param in parameter which will be converted to a std::string.
    /// \return std::string representation of \ref in
    /// \exception IllegalParameterException
    template< typename T > std::string toString(const T& in) const;

    /// This function will overload the function template.  Preferred over
    /// function template specialisation.
    std::string toString(const char* in) const;

    /// This function will overload the function template.  Preferred over
    /// function template specialisation.
    std::string toString(char* in) const;

    /// This function will overload the function template.  Preferred over
    /// function template specialisation.
    /// This one covers all std::string types that are not pointers to
    /// std::string.
    std::string toString(const std::string& in) const;

    /// This function will overload the function template.  Preferred over
    /// function template specialisation.
    std::string toString(const std::string* in) const;

    /// This function will overload the function template.  Preferred over
    /// function template specialisation.
    std::string toString(std::string* in) const;


    /// \exception DeviceValueException
    void unassignedAddress(int address) const;
    void setConnectFailureType(std::string &eString) const;
    void setSocketFailureType(std::string &eString) const;
    void setGethostbynameFailureType(std::string &eString) const;
    void setSendFailureType(std::string& eString) const;
    void setReadFailureType(std::string& eString) const;

    /// Hostname for the currently open connection.
    std::string hostname;

    /// Port for the currently open connection.
    unsigned short port;

    /// Rx/Tx timeout in s for the currently open connection.
    double timeout;

    /// Linger time in s for the currently open connection.
    unsigned int lingerTime;

    /// Number of allowed retries for the currently open connection.
    unsigned short retries;

    /// Parameters that had been passed to \ref open.
    std::multimap< std::string, std::string > parameters;


    private:
    /// No copy constructor.
    CVREth(const CVREth&);

    /// No assignment operator.
    CVREth& operator=(const CVREth&);

    /// Reentrant safety is protected by \ref communicationMutex.
    int writeEthernetMessage(const std::string& message);

    /// Reentrant safety is protected by \ref communicationMutex.
    int readEthernetMessage(std::string& message);

    void reconnectSocket();

    /// \exception SocketOperationFailedException
    void setSocketOptions(int socket, double timeout,
        unsigned int ligerTime) const;

    void convertToSerialNumber(std::string &value);


    int socketFD;
    struct sockaddr_in serv_addr;

    /// Protect the [read|write]EthernetMessage methods.  The communication
    /// with the CVR is not reentrant safe without it.
    ACE_Mutex communicationMutex;
};

#include <CVREth.i>
#endif
