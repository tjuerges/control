#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef CVRImpl_h
#define CVRImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <CVRS.h>
#include <CVRBase.h>


namespace maci
{
    class maci::ContainerServices;
};


class CVRImpl: public CVRBase, virtual public POA_Control::CVR
{
  public:
    /// Constructor.
	CVRImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /// Destructor.
    virtual ~CVRImpl();

    /// \override hwInitializeAction
    /// \except ControlDeviceExceptions::HwLifecycleEx
    virtual void hwInitializeAction();

    /// \override hwConfigureAction
    /// \except ControlDeviceExceptions::HwLifecycleEx
    virtual void hwConfigureAction();

    /// This method returns the unique serial number of the device hardware.
    /// The method looks very similar to the one declared in \ref AmbDeviceImpl
    /// but it does not overload it because this class inherits from
    /// \ref EthernetDevice and not from \ref AmbDeviceImpl.
    /// \ret serial number: sent by the CVRHW
    /// \exception EthernetDeviceExceptions::SocketOperationFailedExImpl
    virtual std::string getUniqueId(ACS::Time& timestamp);
};
#endif
