#ifndef CVREthSim_h
#define CVREthSim_h
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <CVREth.h>
#include <string>
#include <vector>


class CVREthSim: public CVREth
{
    public:
	/// Constructor
	CVREthSim();

	/// Destructor
	virtual ~CVREthSim();

	/// EthernetDeviceInterface Methods
	virtual void open(const std::string& hostname, unsigned short port,
	    double timeout, unsigned int lingerTime,
	    unsigned short retries,
	    const std::multimap< std::string, std::string >& parameters);

	virtual int close();

	/// EthernetDeviceInterface Methods
	/// Monitor
	/// For FREQUENCY, AMPLITUDE
	virtual void monitor(int address, double &value);

    /// EthernetDeviceInterface Methods
    /// Monitor
    /// For GET_UNIQUE_ID
    virtual void monitor(int address, std::string &value);

	/// Control
    /// For SET_RF_ON, SET_RF_OFF
    virtual void command(int address);
	/// For SET_FREQUENCY, SET_AMPLITUDE
	virtual void command(int address, double value);


    private:
	/// Monitor and Control Values

    /// Current frquency value.
	double valueFREQUENCY;

	/// Current amplitude value.
	double valueAMPLITUDE;

	/// RF on or off.
	bool valueRF;

	/// Register if simulated socket is open or not
	bool isOpen_m;
};
#endif
