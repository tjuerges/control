#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * who      when     what
 * tjuerges  Apr 16, 2009  created
 */


#include <sstream>
#include <string>
// For the exception.
#include <EthernetDeviceInterface.h>


template< typename T > std::string CVREth::toString(const T& in) const
{
    std::stringstream data;
    try
    {
        data << in; // first insert value to stream
    }
    catch(const std::exception& ex)
    {
        throw IllegalParameterException(std::string("CVREth::toString: "
            "Cannot convert the template type to std::string."));
    }

    return data.str();
}
