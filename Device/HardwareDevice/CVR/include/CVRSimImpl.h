#ifndef CVRSimImpl_h
#define CVRSimImpl_h
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <CVRImpl.h>
#include <CVRBase.h>


namespace maci
{
    class ContainerServices;
};


class CVRSimImpl: public CVRImpl, virtual public POA_Control::CVR
{
    public:
	CVRSimImpl(const ACE_CString& name, maci::ContainerServices* cs);

	virtual ~CVRSimImpl();


    protected:
    /// Implement the initialise function here.  This allows to create an
    /// Ethernet simulation device instance before CVRBase has a chace to do it.
    /// When CVRBase::initialise is executed, it will find that the
    /// ethDevice_mp smart pointer is not 0 anymore and not create a new
    /// device.
    virtual void initialize();

    /**
    * @override CVRImp::hwConfigureAction
    * @except ControlDeviceExceptions::HwLifecycleEx
    */
    virtual void hwConfigureAction();

};
#endif
