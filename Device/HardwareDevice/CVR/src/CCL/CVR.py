#! /usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# @(#) $Id$
#

"""
This module is part of the Control Command Language.
It defines the CVR class that provides low level control of the
Agilent Signal Generator (CVR).
"""


import CCL.logging
import Acspy.Common.Err
import Acspy.Clients.SimpleClient
import CCL.CVRBase
from CCL import StatusHelper
import ControlExceptions


class CVR(CCL.CVRBase.CVRBase):
    def __init__(self, photonicReference = None,
             componentName = None, stickyFlag = False):
        '''
        The constructor creates a CVR object using default constructor.
        You can specify which LS you would like either by number (1-6)
        or by PhotonicReference (i.e. PhotonicReference2)
        EXAMPLE:
        import CCL.CVR
        # To get the CVR associated with photonic reference 3
        cvr = CCL.CVR.CVR(3)
        or 
        cvr = CCL.CVR.CVR("PhotonicReference3")
        '''

        if componentName is None:
            if photonicReference is None:
                raise ControlExceptions.IllegalParameterErrorEx(
                    'You must specify either a  photonic reference');
            if isinstance(photonicReference, int):
                photonicReference = 'PhotonicReference%d' % photonicReference

            componentName = "CONTROL/CentralLO/%s/CVR" % photonicReference

        name = componentName.split('/')[2]
        self._cvrNo = int(name[len(name) - 1])

        self.__logger = CCL.logging.getLogger()
        self.__logger.logDebug("Creating CVR CCL Object.")

        try:
            CCL.CVRBase.CVRBase.__init__(self, None,
                                   componentName,
                                   stickyFlag)
        except Exception, e:
            print 'CVR component is not running \n' + str(e)

    def __del__(self):
        CCL.CVRBase.CVRBase.__del__(self)

    def getFrequency(self):
        '''
        Get actual frequency.
        '''
        (value, timestamp) = self._HardwareDevice__hw.GET_FREQUENCY()
        return value

    def getFrequencyProperty(self):
        '''
        Get value of the frequency property.
        '''
        (value, completion) = self._HardwareDevice__hw._get_FREQUENCY().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            self.__logger.logError(completion.log())

    def setFrequency(self, value):
        '''
        Set CVR frequency.

        EXAMPLE:
        from CCL.CVR import *
        cvr = CVR("1")
        cvr.setFrequency(14.140893e9)
        '''
        self._HardwareDevice__hw.SET_FREQUENCY(value)

    def getAmplitude(self):
        '''
        Get actual frequency.
        '''
        (value, timestamp) = self._HardwareDevice__hw.GET_AMPLITUDE()
        return value

    def getAmplitudeProperty(self):
        '''
        Get value of the amplitude property.
        '''
        (value, completion) = self._HardwareDevice__hw._get_AMPLITUDE().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            self.__logger.logError(completion.log())

    def setAmplitude(self, value):
        '''
        Set CVR Amplitude.

        EXAMPLE:
        from CCL.CVR import *
        cvr = CVR("1")
        cvr.setAmplitude(16)
        '''
        self._HardwareDevice__hw.SET_AMPLITUDE(value)

    def enableRFOutput(self, enable):
        '''
        Enable/Disable the CVR RF Output.

        EXAMPLE:
        from CCL.CVR import *
        cvr = CVR("1")
        cvr.enableRFOutput(True)
        '''
        if enable == True:
            self._HardwareDevice__hw.SET_RF_ON()
        elif enable == False:
            self._HardwareDevice__hw.SET_RF_OFF()
        else:
            print "Either provide True or False as parameter!"

    def isSimulated(self):
        '''
        Returns true if the CVR hardware is simulated.
        '''
        return self._HardwareDevice__hw.isSimulated()

    def STATUS(self):
        '''
        Method to print relevant status information to the screen.
        '''
        listNames = self._HardwareDevice__componentName.rsplit("/")
        photoRefName = listNames[2]
        compName     = listNames[3]

        entities = []

        values = []
        # Get Frequency
        try:
            values.append(StatusHelper.
                          ValueUnit("%5.3f" % (self.getFrequency()/1E9),
                                    "GHz","Frequency"))
        except:
            values.append(StatusHelper.ValueUnit("N/A", "GHz", "Frequency"))
        entities.append(StatusHelper.Line("",values))
                                    
        values = []
        # Get Amplitude
        try:
            values.append(StatusHelper.
                          ValueUnit(self.getAmplitude(), "dBm","Amplitude"))
        except:
            values.append(StatusHelper.ValueUnit("N/A", "dBm","Amplitude"))
        entities.append(StatusHelper.Line("",values))
            
        frame = StatusHelper.Frame(compName + "     [" + photoRefName + "]",
                                   entities)
        frame.printScreen()
