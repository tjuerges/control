//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <CVREth.h>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <exception>
// For ACS_TRACE.
#include <loggingACEMACROS.h>
// For errno
#include <cerrno>
// For std::strerror, std::memset
#include <cstring>
// For std::atoi
#include <cstdlib>
// For gethostbyname
#include <netdb.h>
// For htons
#include <netinet/in.h>
// For connect and setsocketopt
#include <sys/socket.h>
// For ::close
#include <unistd.h>
// For ACE_Guard
#include <Guard_T.h>


/// CVREth Methods

CVREth::CVREth():
    port(0U),
    timeout(0.0),
    lingerTime(0U),
    retries(0U),
    socketFD(-1)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

CVREth::~CVREth()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    close();
}

void CVREth::initialiseHardware()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    command(CVR_RfOn);
}

void CVREth::setSocketOptions(int socket, double _timeout,
    unsigned int _lingerTime) const
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    int optval(0);
    const socklen_t optlen(sizeof(int));

    // Set keep alive option for socket.
    if(setsockopt(socket, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0)
    {
        std::string errString;
        setSocketFailureType(errString);
        errString = __PRETTY_FUNCTION__;
        errString += ": " + errString;
        throw SocketOperationFailedException(errString);
    }

    // Set SO_LINGER.
    // This will cause a final close of this socket to wait until all
    // data sent on it has been received by the remote host or timeout occurs.
    struct linger linger_s;
    linger_s.l_onoff = 1;
    linger_s.l_linger = _lingerTime;
    if(setsockopt(socket, SOL_SOCKET, SO_LINGER, &linger_s,
        sizeof(struct linger)) == -1)
    {
        std::string errString;
        setSocketFailureType(errString);
        errString = __PRETTY_FUNCTION__;
        errString += ": " + errString;
        throw SocketOperationFailedException(errString);
    }

    // SO_RCVTIMEO and SO_SNDTIMEO: Set reception and sending timeouts
    // User specifies s so that must be converted to sec/usec.
    struct timeval timer_s;
    timer_s.tv_sec = static_cast< long > (_timeout);
    timer_s.tv_usec
        = static_cast< long > ((_timeout - timer_s.tv_sec) * 1000.0);
    // Set the RX timeout.
    if(setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, &timer_s,
        sizeof(struct timeval)) < 0)
    {
        std::string errString;
        setSocketFailureType(errString);
        errString = __PRETTY_FUNCTION__;
        errString += ": " + errString;

        throw SocketOperationFailedException(errString);
    }

    // Set the TX timeout.
    if(setsockopt(socket, SOL_SOCKET, SO_SNDTIMEO, &timer_s,
        sizeof(struct timeval)) < 0)
    {
        std::string errString;
        setSocketFailureType(errString);
        errString = __PRETTY_FUNCTION__;
        errString += ": " + errString;
        throw SocketOperationFailedException(errString);
    }
}

void CVREth::open(const std::string& _hostname, unsigned short _port,
    double _timeout, unsigned int _lingerTime, unsigned short _retries,
    const std::multimap< std::string, std::string >& _parameters)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Invalidate the current connection parameters.
    hostname.clear();
    port = 0U;
    timeout = 0.0;
    lingerTime = 0U;
    retries = 0U;
    socketFD = -1;

    // Figure out what the hostname means in IP address currency.
    struct hostent* server(0);
    server = gethostbyname(_hostname.c_str());
    if(server == 0)
    {
        std::string errString;
        setGethostbynameFailureType(errString);
        errString = __PRETTY_FUNCTION__;
        errString += ": " + errString;
        throw SocketOperationFailedException(errString);
    }

    // Try to open the socket.
    int tmp_socketFD(-1);
    if((tmp_socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        std::string errString;
        setSocketFailureType(errString);
        errString = __PRETTY_FUNCTION__;
        errString += ": " + errString;
        throw SocketOperationFailedException(errString);
    }

    // Hostname makes sense, socketFD has been created, now parse the
    // parameters.

    // Set the socket options.  If this fails, an exception will be thrown and
    // be caught higher up.
    try
    {
        setSocketOptions(tmp_socketFD, _timeout, _lingerTime);
    }
    catch(const SocketOperationFailedException& ex)
    {
        std::string errorMessage(__PRETTY_FUNCTION__);
        errorMessage += ": Failed to set the socket options. ";
        errorMessage += ex.getErrorMessage();
        throw SocketOperationFailedException(errorMessage);
    }

    // Set up of the connection destination.
    std::memset(&serv_addr, 0, sizeof(sockaddr_in));
    std::memcpy(&serv_addr.sin_addr.s_addr, server->h_addr, server->h_length);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(_port);

    // Try to connect.
    if(connect(tmp_socketFD, reinterpret_cast< sockaddr* > (&serv_addr),
        sizeof(sockaddr_in)) < 0)
    {
        // Store errno.
        int err(errno);
        if(err == EINPROGRESS)
        {
            //This is not actually an error. Use select to wait for the
            // connection to become established.
            fd_set rfds;
            fd_set wfds;
            FD_ZERO(&rfds);
            FD_ZERO(&wfds);
            FD_SET(tmp_socketFD, &rfds);
            FD_SET(tmp_socketFD, &wfds);

            int retval(-1);
            struct timeval tv;
            // Wait up to 60 seconds.
            tv.tv_sec = 60;
            tv.tv_usec = 0;
            retval = select(1, &rfds, &wfds, NULL, &tv);
            if(retval == -1)
            {
                std::string errString;
                setConnectFailureType(errString);
                errString += __PRETTY_FUNCTION__;
                errString += " while waiting for connection: "
                    + errString;
                throw SocketOperationFailedException(errString);
            }
            else if(retval == 0)
            {
                std::string errString(__PRETTY_FUNCTION__);
                errString += " timeout while waiting for connection to be "
                    "ready.";
                throw SocketOperationFailedException(errString);
            }
        }
        else
        {
            std::string errString;
            setConnectFailureType(errString);
            errString += __PRETTY_FUNCTION__;
            errString += ": " + errString;
            throw SocketOperationFailedException(errString);
        }
    }

    // We made it here.  This means that the connection is valid and that the
    // hardware has been successfully initialised.  Store the file descriptor
    // for real use.
    socketFD = tmp_socketFD;

    // Store the current connection parameters and all relevant data for a
    // potential reconnect.
    hostname = _hostname;
    port = _port;
    timeout = _timeout;
    lingerTime = _lingerTime;
    retries = _retries;
    parameters = _parameters;
}

int CVREth::close()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    int ret(0);

    if(socketFD >= 0)
    {
        ret = ::close(socketFD);
        socketFD = -1;
    }

    return ret;
}

void CVREth::setConnectFailureType(std::string& eString) const
{
    switch(errno)
    {
        case EBADF:
            eString = "Connect error first argument does not specify a valid "
                "descriptor";
            break;
        case ENOTSOCK:
            eString = "Connect error first argument does not specify a socket "
                "descriptor";
            break;
        case EAFNOSUPPORT:
            eString = "Connect error The address family specified in the "
                "remote endpoint cannot be used with this type of socket";
            break;
        case EADDRNOTAVAIL:
            eString = "Connect error the specified endpoint address is not "
                "available";
            break;
        case EISCONN:
            eString = "Connect error the socket is already connected";
            break;
        case ETIMEDOUT:
            eString = "Connect error the protocol reached time out without "
                "successfully establishing a connection";
            break;
        case ECONNREFUSED:
            eString = "Connect error connection refused by remote device";
            break;
        case ENETUNREACH:
            eString = "Connect error network is currently unreachable";
            break;
        case EINPROGRESS:
            eString = "Connect error socket nonblocking and a connection "
                "attempt would block";
            break;
        case EALREADY:
            eString = "Connect error socket nonblocking and the call would "
                "wait for a previous connection attempt to complete";
            break;
        case EFAULT:
            eString = "Connect error socket structure address is illegal ";
            break;
        case EACCES:
            eString
                = "Connect error tried contact a broadcast address without "
                    "having the socket broadcast flag enabled ";
            break;
        default:
            eString = "Connect error Cause unknown";
            break;
    }
}

void CVREth::setSocketFailureType(std::string& eString) const
{
    switch(errno)
    {
        case EPROTONOSUPPORT:
            eString = "Socket error error in argument: the requested service "
                "or specified protocol is invalid ";
            break;
        case EMFILE:
            eString = "Socket error applications desriptor table is full";
            break;
        case ENFILE:
            eString = "Socket error internal system file table is full";
            break;
        case EACCES:
            eString = "Socket error permission to create the socket is denied";
            break;
        case ENOBUFS:
            eString = "Socket error system has no buffer space available";
            break;
        case ENOMEM:
            eString = "Socket error system has no memory available";
            break;
        case EINVAL:
            eString = "Socket error protocol or protocol family was unkown or "
                "unavailable";
            break;
        default:
            eString = "Socker error Cause unknown";
            break;
    }
}

void CVREth::setGethostbynameFailureType(std::string& eString) const
{
    switch(errno)
    {
        case HOST_NOT_FOUND:
            eString = "gethostbyname:  error host name unkown";
            break;
        case TRY_AGAIN:
            eString = "gethostbyname:  error temporary error local server "
                "could not contact the authority at present";
            break;
        case NO_RECOVERY:
            eString = "gethostbyname:  error irrecoverable error occurred";
            break;
        case NO_ADDRESS:
            eString = "gethostbyname:  error the specified name is valid, but "
                "it does not correspond to an IP address";
            break;
        default:
            eString = "gethostbyname:  error Cause unknown";
            break;
    }
}

void CVREth::setSendFailureType(std::string& eString) const
{
    switch(errno)
    {
        case EBADF:
            eString = "Send error first argument does not specify a valid "
                "descriptor";
            break;
        case ENOTSOCK:
            eString = "Send error first argument does not specify a socket "
                "descriptor";
            break;
        case EFAULT:
            eString = "Send error argument buffer is incorrect";
            break;
        case EMSGSIZE:
            eString = "Send error message is too large fot the socket";
            break;
        case EAGAIN:
            eString = "Send error socket has no data, but nonblocking I/O has "
                "been specified";
            break;
        case ENOBUFS:
            eString
                = "Send error systen has insufficient resources to perform "
                    "the operation";
            break;
        case EINTR:
            eString = "Send error a signal occurred";
            break;
        case ENOMEM:
            eString = "Send error insufficient memory available";
            break;
        case EINVAL:
            eString = "Send error invalid argument passed";
            break;
        case EPIPE:
            eString
                = "Send error local end has been shut down on a connection "
                    "oriented";
            break;
        default:
            eString = "Send error cause unknown";
            break;
    }
}

void CVREth::setReadFailureType(std::string& eString) const
{
    switch(errno)
    {
        case EBADF:
            eString = "Read error first argument does not specify a valid "
                "descriptor";
            break;
        case EFAULT:
            eString = "Read error address buffer is illegal";
            break;
        case EIO:
            eString = "Read error I/0 error occurred while reading data";
            break;
        case EINTR:
            eString = "Read error a signal interrupted the operation";
            break;
        case EAGAIN:
            eString = "Read error nonblocking I/O is specified but the socket "
                "has no data";
            break;
        case EINVAL:
            eString = "Read error file descriptor is unsuitable for reading";
            break;
        case EISDIR:
            eString = "Read error file descriptor refers to directory";
            break;
        default:
            eString = "Read error cause unknown";
            break;
    }
}

int CVREth::writeEthernetMessage(const std::string& data)
{
    int result(0);

    // Try to send the message at least retries + 1 times.
    for(unsigned int sendCount(0); sendCount <= retries; ++sendCount)
    {
        result = ::write(socketFD, data.c_str(), data.size());
        ++sendCount;

        // If the send fails try to reconnect the socket and resend.
        if(result < 0)
        {
            reconnectSocket();
        }
        else if(static_cast< unsigned int > (result) == data.size())
        {
            break;
        }
    }

    if(result < 0)
    {
        std::string errString;
        setSendFailureType(errString);
        throw SocketOperationFailedException(errString);
    }

    return result;
}

int CVREth::readEthernetMessage(std::string& data)
{
    static char message[1000];
    std::memset(message, 0, sizeof(message));
    int result(0);

    for(unsigned int receiveCount(0U); receiveCount < retries; ++receiveCount)
    {
        result = ::read(socketFD, message, sizeof(message));
        ++receiveCount;
        message[result] = 0;

        if(result < 0) // If the read fails try to reconnect the socket and resend.
        {
            reconnectSocket();
        }
        else if(result > 0)
        {
            break;
        }
    }

    if(result < 0)
    {
        std::string errString;
        setReadFailureType(errString);
        throw SocketOperationFailedException(errString);
    }

    data = message;
    return result;
}

void CVREth::reconnectSocket()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Close the non-working socket.
    close();
    // Reconnect the socket.
    open(hostname, port, timeout, lingerTime, retries, parameters);
}

void CVREth::unassignedAddress(int address) const
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    std::ostringstream strAddress;
    strAddress << address;
    throw SocketOperationFailedException(std::string(
        "CVREth::unassignedAddress: unassigned address ") + toString(address));
}

void CVREth::monitor(int address, bool& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for bool");
}

void CVREth::monitor(int address, unsigned char& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for unsigned char");
}

void CVREth::monitor(int address, char& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for char");
}

void CVREth::monitor(int address, unsigned short& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for unsigned "
            "short");
}

void CVREth::monitor(int address, short& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for short");
}

void CVREth::monitor(int address, unsigned int& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for unsigned int");
}

void CVREth::monitor(int address, int& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for int");
}

void CVREth::monitor(int address, unsigned long& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for unsigned long");
}

void CVREth::monitor(int address, long& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for long long");
}

void CVREth::monitor(int address, unsigned long long& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for unsigned long "
            "long");
}

void CVREth::monitor(int address, long long& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for long long");
}

void CVREth::monitor(int address, float& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for float");
}

// For FREQUENCY, AMPLITUDE
void CVREth::monitor(int address, double& value)
{
    ACE_Guard< ACE_Mutex > guard(communicationMutex);

    switch(address)
    {
        case CVR_GetFrequency:
        {
            writeEthernetMessage(":FREQ?\n");
            std::string refsString;
            readEthernetMessage(refsString);
            std::istringstream val(refsString);
            val >> value;
        }
            break;

        case CVR_GetAmplitude:
        {
            writeEthernetMessage(":POW?\n");
            std::string refsString;
            readEthernetMessage(refsString);
            std::istringstream val(refsString);
            val >> value;
        }
            break;

        default:
        {
            unassignedAddress(address);
        }
            break;
    };
}

void CVREth::monitor(int address, long double& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for long double");
}

void CVREth::monitor(int address, std::string& value)
{
    ACE_Guard< ACE_Mutex > guard(communicationMutex);

    switch(address)
    {

        case CVR_GetSerialNumber:
        {
            writeEthernetMessage("*IDN?\n");
            std::string refsString;
            readEthernetMessage(value);
            convertToSerialNumber(value);
        }
            break;

        default:
        {
            unassignedAddress(address);
        }
    }
}

void CVREth::monitor(int address, std::vector< bool >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< bool >");
}

void CVREth::monitor(int address, std::vector< unsigned char >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< unsigned char >");
}

void CVREth::monitor(int address, std::vector< char >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< char >");
}

void CVREth::monitor(int address, std::vector< unsigned short >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< unsigned short >");
}

void CVREth::monitor(int address, std::vector< short >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< short >");
}

void CVREth::monitor(int address, std::vector< unsigned int >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< int >");
}

void CVREth::monitor(int address, std::vector< int >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< int >");
}

void CVREth::monitor(int address, std::vector< unsigned long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< unsigned long >");
}

void CVREth::monitor(int address, std::vector< long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< long >");
}

void CVREth::monitor(int address, std::vector< unsigned long long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< unsigned long long >");
}

void CVREth::monitor(int address, std::vector< long long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< long long >");
}

void CVREth::monitor(int address, std::vector< float >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< float >");
}

void CVREth::monitor(int address, std::vector< double >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< double >");
}

void CVREth::monitor(int address, std::vector< long double >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< long double >");
}

void CVREth::monitor(int address, std::vector< std::string >& value)
{
    throw SocketOperationFailedException(
        "CVREth::monitor: No monitor points defined for "
            "std::vector< std::string >");
}

void CVREth::command(int address)
{
    ACE_Guard< ACE_Mutex > guard(communicationMutex);

    switch(address)
    {
        case CVR_RfOn:
        {
            writeEthernetMessage(":OUTP ON \n ");
        }
            break;

        case CVR_RfOff:
        {
            writeEthernetMessage(":OUTP OFF \n ");
        }
            break;

        default:
        {
            unassignedAddress(address);
        }
    };
}

void CVREth::command(int address, bool value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for bool");
}

void CVREth::command(int address, unsigned char value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for unsigned char");
}

void CVREth::command(int address, char value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for char");
}

void CVREth::command(int address, unsigned short value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for unsigned short");
}

void CVREth::command(int address, short value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for short");
}

void CVREth::command(int address, unsigned int value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for unsigned int");
}

void CVREth::command(int address, int value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for int");
}

void CVREth::command(int address, unsigned long value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for unsigned long");
}

void CVREth::command(int address, long value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for long");
}

void CVREth::command(int address, unsigned long long value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for unsigned long "
            "long");
}

void CVREth::command(int address, long long value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for long long");
}

void CVREth::command(int address, float value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for float");
}

void CVREth::command(int address, double value)
{
    ACE_Guard< ACE_Mutex > guard(communicationMutex);

    switch(address)
    {
        case CVR_SetFrequency:
        {
            std::ostringstream data;
            data << ":FREQ " << setprecision(13) << value << "HZ\n";
            writeEthernetMessage(data.str());
        }
            break;

        case CVR_SetAmplitude:
        {
            std::ostringstream data;
            data.setf(std::ios::fixed, std::ios::floatfield);
            data.precision(2);
            data << ":POW " << value << "DBM\n";
            writeEthernetMessage(data.str());
        }
            break;

        default:
        {
            unassignedAddress(address);
        }
    };
}

void CVREth::command(int address, long double value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for long double");
}

void CVREth::command(int address, const std::string& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::string");
}

void CVREth::command(int address, const std::vector< bool >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< bool >");
}

void CVREth::command(int address, const std::vector< unsigned char >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< unsigned char >");
}

void CVREth::command(int address, const std::vector< char >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< char >");
}

void CVREth::command(int address, const std::vector< unsigned short >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< unsigned short >");
}

void CVREth::command(int address, const std::vector< short >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< short >");
}

void CVREth::command(int address, const std::vector< unsigned int >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< unsigned int >");
}

void CVREth::command(int address, const std::vector< int >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< int >");
}

void CVREth::command(int address, const std::vector< unsigned long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< unsigned long >");
}

void CVREth::command(int address, const std::vector< long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< long >");
}

void CVREth::command(int address,
    const std::vector< unsigned long long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< unsigned long long >");
}

void CVREth::command(int address, const std::vector< long long >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< long long >");
}

void CVREth::command(int address, const std::vector< std::string >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< std::string >");
}

void CVREth::command(int address, const std::vector< float >& value)
{
    throw SocketOperationFailedException(
        "CVREth::command: No command points defined for "
            "std::vector< float >");
}

void CVREth::command(int address, const std::vector< double >& value)
{
    throw SocketOperationFailedException(
        "EthernetDeviceInterface: No command points defined for "
            "std::vector< double >");
}

void CVREth::command(int address, const std::vector< long double >& value)
{
    throw SocketOperationFailedException(
        "EthernetDeviceInterface: No command points defined for "
            "std::vector< long double >");
}

std::string CVREth::toString(const char* in) const
{
    return std::string(in);
}

std::string CVREth::toString(char* in) const
{
    return std::string(in);
}

std::string CVREth::toString(const std::string& in) const
{
    return in;
}

std::string CVREth::toString(const std::string* in) const
{
    return *in;
}

std::string CVREth::toString(std::string* in) const
{
    return *in;
}

void CVREth::convertToSerialNumber(std::string& value)
{
    std::string errorMessage(__PRETTY_FUNCTION__);
    errorMessage += ": Error converting the "
        "following string to a serial number: ";
    errorMessage = errorMessage + value;

    // The string returned by the CVR have the following format:
    // <company name>, <model number>, <serial number>, <firmware revision>
    // so we need to iterate through the string to get the serial number
    // this put the cursor in the <model number>
    std::string::size_type startIndex(value.find(','));
    if(startIndex == std::string::npos)
    {
        // if we reach the end of the string without found the comma,
        // we're in troubles
        throw SocketOperationFailedException(errorMessage.c_str());
    }

    // this put the cursor in the <serial number>
    startIndex = value.find(',', startIndex + 1);
    if(startIndex == std::string::npos)
    {
        // if we reach the end of the string without found the comma,
        // we're in troubles
        throw SocketOperationFailedException(errorMessage.c_str());
    }

    // Now we look for the end of the serial number
    std::string::size_type endIndex(value.find(',', startIndex + 1));
    if(endIndex == std::string::npos)
    {
        // if we reach the end of the string without found the comma,
        // we're in troubles
        throw SocketOperationFailedException(errorMessage.c_str());
    }

    // to override the blank space and the comma before the S/N
    startIndex += 2;
    value = value.substr(startIndex, endIndex - startIndex);
    // The serial number is an Hex number, so we add the 0x replacing the first
    // 2 letters (US for tCLOTS and MY for AOS CVRs)
    value.replace(0, 2, "0x00000000");

    return;
}
