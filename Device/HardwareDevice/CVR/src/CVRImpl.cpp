//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <CVRImpl.h>
// For AUTO_TRACE.
#include <loggingMACROS.h>
#include <string>


CVRImpl::CVRImpl(const ACE_CString& name, maci::ContainerServices* cs):
    CVRBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

CVRImpl::~CVRImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void CVRImpl::hwConfigureAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    EthernetDevice::hwConfigureAction();

    // Aat this point, the thread that read from the web service should be
    // working. We will try to read the serial number for a couple of times
    // until we get a valid value
    std::string serialNumber = "";
    try
    {
        ACS::Time timestamp(0ULL);
        serialNumber = getUniqueId(timestamp);
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);

        const std::string message("Cannot get serial number from the CVR");
        nex.addData("Detail", message);
        nex.log();
        throw nex.getHwLifecycleEx();
    }

    setSerialNumber(serialNumber);
}


void CVRImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    CVRBase::hwInitializeAction();

    // set the amplitude to 16 db
    try
    {
        setCntlAmplitude(16);
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not set the amplitude to 16dB");
        nex.log(LM_WARNING);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not set the amplitude to 16dB");
        nex.log(LM_WARNING);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not set the amplitude to 16dB");
        nex.log(LM_WARNING);
    }

    // turn on the RF power
    try
    {
        setCntlRfOn();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not turn on the RF power");
        nex.log(LM_WARNING);
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not turn on the RF power");
        nex.log(LM_WARNING);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not turn on the RF power");
        nex.log(LM_WARNING);
    }
}

std::string CVRImpl::getUniqueId(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string value;
    // Defined as 14 in CVREth, we can override this if we add this method to
    // the spreadsheet.
    int addressUniqueId = 14;
    try
    {
        EthernetDevice::monitor(addressUniqueId, value, timestamp);

        Control::HardwareDeviceImpl::clearDeviceErrorCounter();
    }
    catch(const EthernetDeviceExceptions::SocketOperationFailedExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        throw nex;
    }
    catch(const EthernetDeviceExceptions::IllegalParameterExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex, __FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        throw nex;
    }
    catch(const EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl& ex)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl nex(ex,__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught a generic "
            "\"EthernetDeviceExceptions::EthernetDeviceExceptionsExImpl\" "
            "exception.");
        throw nex;
    }
    catch(...)
    {
        Control::HardwareDeviceImpl::incrementDeviceErrorCounter();

        EthernetDeviceExceptions::SocketOperationFailedExImpl ex(__FILE__,
        __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception.");
        throw ex;
    }

    return value;
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CVRImpl)
