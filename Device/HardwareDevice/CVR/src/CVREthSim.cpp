//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <CVREthSim.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
// For ACS_TRACE.
#include <loggingACEMACROS.h>
// For audience logs.
#include <LogToAudience.h>


CVREthSim::CVREthSim():
    CVREth(),
    valueFREQUENCY(13.5),
    valueAMPLITUDE(1.0),
    valueRF(false),
    isOpen_m(false)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

CVREthSim::~CVREthSim()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void CVREthSim::open(const std::string& _hostname, unsigned short _port,
    double _timeout, unsigned int _lingerTime, unsigned short _retries,
    const std::multimap< std::string, std::string >& _parameters)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(isOpen_m == false)
    {
        isOpen_m = true;
    }
    else
    {
        throw SocketOperationFailedException("CVREthSim::open: Error Opening "
            "the socket: Already open.");
    }

    CVREth::hostname = _hostname;
    CVREth::port = port;
    CVREth::timeout = _timeout;
    CVREth::lingerTime = _lingerTime;
    CVREth::retries = _retries;
}

int CVREthSim::close()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(isOpen_m == true)
    {
        isOpen_m = false;
    }
    else
    {
        std::ostringstream msg;
        msg << "Socket is already closed.  Doing nothing.";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }

    return 0;
}

// For FREQUENCY, AMPLITUDE
void CVREthSim::monitor(int address, double& value)
{
    if(isOpen_m == false)
    {
        throw SocketOperationFailedException("CVREthSim::monitor: Socket is "
            "closed.");
    }

    switch(address)
    {
        case CVR_GetFrequency:
        {
            value = valueFREQUENCY;
        }
        break;

        case CVR_GetAmplitude:
        {
            value = valueAMPLITUDE;
        }
        break;

        default:
        {
            unassignedAddress(address);
        }
    }
}

// For GET_UNIQUE_ID
void CVREthSim::monitor(int address, std::string& value)
{
    // this is a just in case, this method should be never called
    switch(address)
    {

        case CVR_GetSerialNumber:
        {
           value="0x0000000000000000";
        }
        break;

        default:
        {
            unassignedAddress(address);
        }
    }

}


// For SET_FREQUENCY, SET_AMPLITUDE
void CVREthSim::command(int address, double value)
{
    if(isOpen_m == false)
    {
        throw SocketOperationFailedException("CVREthSim::command: Socket is "
            "closed.");
    }

    switch(address)
    {

        case CVR_SetFrequency:
        {
            valueFREQUENCY = value;
        }
        break;

        case CVR_SetAmplitude:
        {
            valueAMPLITUDE = value;
        }
        break;

        default:
        {
            unassignedAddress(address);
        }
    }
}

// For SET_RF_ON, SET_RF_OFF
void CVREthSim::command(int address)
{
    if(isOpen_m == false)
    {
        throw SocketOperationFailedException("CVREthSim::command: Socket is "
            "closed.");
    }

    switch(address)
    {
        case CVR_RfOn:
        {
            valueRF = true;
        }
        break;

        case CVR_RfOff:
        {
            valueRF = false;
        }
        break;

        default:
        {
            unassignedAddress(address);
        }
    }
}
