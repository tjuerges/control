//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <CVRSimImpl.h>
#include <sstream>
#include <string>
#include <CVREthSim.h>
// For AUTO_TRACE.
#include <loggingMACROS.h>
// For audience logs.
#include <LogToAudience.h>
#include <maciACSComponentDefines.h>
#include <SimulatedSerialNumber.h>
#include <TypeConversion.h>


CVRSimImpl::CVRSimImpl(const ACE_CString& name, maci::ContainerServices* cs):
    CVRImpl(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string componentName(name.c_str());

    unsigned long long hashed_sn(AMB::Utils::getSimSerialNumber(
        componentName, "CVR"));

    std::ostringstream msg;
    msg << "simSerialNumber for "
        << componentName
        << " with assembly name CVR is 0x"
        << std::hex
        << hashed_sn;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    msg.str("");
    msg  << "0x"
        << std::hex
        << hashed_sn;
    setSerialNumber(msg.str());
}

CVRSimImpl::~CVRSimImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void CVRSimImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Create the Ethernet hardware communication instance.
    if(ethernetDeviceIF_mp.get() == 0)
    {
        std::ostringstream msg;
        msg << "Creating a new Ethernet communication simulation class "
            "instance.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        ethernetDeviceIF_mp.reset(new CVREthSim);
    }
    else
    {
        std::ostringstream msg;
        msg << "Not creating a new Ethernet communication simulation class "
            "instance.  There is already one active.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    // Call the base class implementation
    CVRImpl::initialize();
}

void CVRSimImpl::hwConfigureAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    EthernetDevice::hwConfigureAction();
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CVRSimImpl)
