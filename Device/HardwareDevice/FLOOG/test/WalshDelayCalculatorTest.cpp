/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <walshDelayCalculator.h>
#include <TETimeUtil.h>
#include <ControlExceptions.h>
#include <ControlDelayInterfacesC.h>

#include <sstream> 

using std::ostringstream;

class WalshDelayCalculatorTest : public CppUnit::TestFixture,
                                 public WalshDelayCalculator
{
public:

  WalshDelayCalculatorTest():
    WalshDelayCalculator(0.008, 2500000){}
    


  void testInitialCondition() {
    CPPUNIT_ASSERT_MESSAGE("Initial size of delayList is incorrect",
                           coarseDelayQueue_m.size() == 0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(phaseOffset_m, 12E-3, 1E-9);
  }

  void testStaticDelay() {
    double initialDelay = getPhaseOffset();
    CPPUNIT_ASSERT_DOUBLES_EQUAL(phaseOffset_m, initialDelay, 1E-9);
    
    setPhaseOffset(initialDelay - 0.001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(phaseOffset_m, initialDelay-0.001, 1E-9);
      
    double finalDelay = getPhaseOffset();
    CPPUNIT_ASSERT_DOUBLES_EQUAL(finalDelay, initialDelay-0.001, 1E-9);

    double delayCommand;
    CPPUNIT_ASSERT(isUpdateNeeded(delayCommand));
    CPPUNIT_ASSERT_DOUBLES_EQUAL(delayCommand, initialDelay-0.001, 1E-9);
  }

  void testQueingOfCoarseDelays() {
    /* Construct the First Event */
    ACS::Time startTime= TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value +
      20000000; // 2 Seconds
    ACS::Time endTime = startTime + 100000000; //10 seconds
    
    Control::CoarseDelayValueSeq   coarseDelaySeq;

    addCoarseDelay(coarseDelaySeq,    0,    0);
    addCoarseDelay(coarseDelaySeq, 2000, 3000);
    addCoarseDelay(coarseDelaySeq, 4000, 1000);
    addCoarseDelay(coarseDelaySeq, 9000, 2000);

    
    Control::AntennaDelayEvent event1 = 
      getAntennaDelayEvent(startTime, endTime, coarseDelaySeq);

    newDelayEvent(event1);

    /* Now Check that we have the correct events */
    CPPUNIT_ASSERT(coarseDelayQueue_m.size()== 4);
    CPPUNIT_ASSERT(coarseDelayQueue_m[0].applicationTime == 
                   startTime);
    CPPUNIT_ASSERT(coarseDelayQueue_m[0].coarseDelay == 0);
    
    CPPUNIT_ASSERT(coarseDelayQueue_m[1].applicationTime == 
                   startTime + 20000000);
    CPPUNIT_ASSERT(coarseDelayQueue_m[1].coarseDelay == 3000);

    CPPUNIT_ASSERT(coarseDelayQueue_m[2].applicationTime == 
                   startTime + 40000000);
    CPPUNIT_ASSERT(coarseDelayQueue_m[2].coarseDelay == 1000);

    CPPUNIT_ASSERT(coarseDelayQueue_m[3].applicationTime ==
                   startTime + 90000000);
    CPPUNIT_ASSERT(coarseDelayQueue_m[3].coarseDelay == 2000);

    /* Send in a second event */
    ACS::Time startTime2 = startTime + 40000000;
    ACS::Time endTime2   = endTime - 10000000;
    coarseDelaySeq.length(0); // Empty
    addCoarseDelay(coarseDelaySeq,    0,  500);
    addCoarseDelay(coarseDelaySeq, 3000, 1500);

    Control::AntennaDelayEvent event2 = 
      getAntennaDelayEvent(startTime2, endTime2, coarseDelaySeq);

    newDelayEvent(event2);

    CPPUNIT_ASSERT(coarseDelayQueue_m.size()== 4);
    CPPUNIT_ASSERT(coarseDelayQueue_m[0].applicationTime == 
                   startTime);
    CPPUNIT_ASSERT(coarseDelayQueue_m[0].coarseDelay == 0);
    
    CPPUNIT_ASSERT(coarseDelayQueue_m[1].applicationTime == 
                   startTime + 20000000);
    CPPUNIT_ASSERT(coarseDelayQueue_m[1].coarseDelay == 3000);

    CPPUNIT_ASSERT(coarseDelayQueue_m[2].applicationTime == 
                   startTime + 40000000);
    CPPUNIT_ASSERT(coarseDelayQueue_m[2].coarseDelay == 500);

    CPPUNIT_ASSERT(coarseDelayQueue_m[3].applicationTime ==
                   startTime + 70000000);
    CPPUNIT_ASSERT(coarseDelayQueue_m[3].coarseDelay == 1500);
  }

  void addCoarseDelay(Control::CoarseDelayValueSeq& sequence,
                      unsigned short                relativeStartTime,
                      unsigned long                 delay) {
    unsigned int index = sequence.length();
    Control::CoarseDelayValue cdv;
    cdv.relativeStartTime = relativeStartTime;
    cdv.coarseDelay       = delay;

    sequence.length(index +1);
    sequence[index] = cdv;
  }



  Control::AntennaDelayEvent getAntennaDelayEvent(
                            ACS::Time startTime, ACS::Time stopTime, 
                            Control::CoarseDelayValueSeq cdvs){
    Control::AntennaDelayEvent event;

    event.startTime    = startTime;
    event.stopTime     = stopTime;
    event.antennaName  = strdup("DA41");
    event.fineDelays   = Control::FineDelayValueSeq(0);
    event.delayTables  = Control::DelayTableSeq(0);
    event.coarseDelays = cdvs;

    return event;
  }
    
  


//   void testInsertingOneDelayTable() {
//     Control::DelayTable newDelayTable = createDelayTable(10000,
//                                                          20000,
//                                                          1);
//     insertDelayTable(newDelayTable);
    
//     CPPUNIT_ASSERT_MESSAGE("Failed to insert delay table",
//                            delayList_m.size() == 1);
    
//     std::list<Control::DelayTable>::iterator iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//   }

//   void testInsertingDelayTableInFront() {
//     Control::DelayTable newDelayTable;
//     std::list<Control::DelayTable>::iterator iter;

//     /* Simple insertion test */
//     newDelayTable = createDelayTable(10000, 20000, 1);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(5000, 10000, 2);
//     insertDelayTable(newDelayTable);

//     CPPUNIT_ASSERT_MESSAGE("Failed to insert delay table",
//                            delayList_m.size() == 2);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 5000, 10000, 2);
//     iter++;
//     checkDelayTableList(iter, 10000, 20000, 1);

//     /* Test that the second value needs to be reset */
//     newDelayTable = createDelayTable(4000, 7000, 3);
//     insertDelayTable(newDelayTable);
//     CPPUNIT_ASSERT_MESSAGE("Failed to insert delay table",
//                            delayList_m.size() == 3);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 4000, 7000, 3);
//     iter++;
//     checkDelayTableList(iter, 7000, 10000, 2);
//     iter++;
//     checkDelayTableList(iter, 10000, 20000, 1);

    
//     /* Test that the second value needs to be removed */
//     newDelayTable = createDelayTable(3000, 7000, 4);
//     insertDelayTable(newDelayTable);
//     CPPUNIT_ASSERT_MESSAGE("Failed to insert delay table",
//                            delayList_m.size() == 3);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 3000, 7000, 4);
//     iter++;
//     checkDelayTableList(iter, 7000, 10000, 2);
//     iter++;
//     checkDelayTableList(iter, 10000, 20000, 1);


//     /* Test that the second value needs to be removed and the third reset */
//     newDelayTable = createDelayTable(2000, 9000, 5);
//     insertDelayTable(newDelayTable);
//     CPPUNIT_ASSERT_MESSAGE("Failed to insert delay table",
//                            delayList_m.size() == 3);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 2000, 9000, 5);
//     iter++;
//     checkDelayTableList(iter, 9000, 10000, 2);
//     iter++;
//     checkDelayTableList(iter, 10000, 20000, 1);
//   }

//   void testInsertingDelayTableInBack() {
//     Control::DelayTable newDelayTable;
//     std::list<Control::DelayTable>::iterator iter;

//     /* Simple insertion test */
//     newDelayTable = createDelayTable(10000, 20000, 1);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(20000, 30000, 2);
//     insertDelayTable(newDelayTable);

//     CPPUNIT_ASSERT_MESSAGE("Failed to insert delay table",
//                            delayList_m.size() == 2);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//     iter++;
//     checkDelayTableList(iter, 20000, 30000, 2);

//     /* Test when the previous one needs to be changed */
//     newDelayTable = createDelayTable(25000, 40000, 3);
//     insertDelayTable(newDelayTable);
//     CPPUNIT_ASSERT_MESSAGE("Failed to insert delay table",
//                            delayList_m.size() == 3);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//     iter++;
//     checkDelayTableList(iter, 20000, 25000, 2);
//     iter++;
//     checkDelayTableList(iter, 25000, 40000, 3);
//   }

//   void testInsertingDelayTableInMiddle() {
//     Control::DelayTable newDelayTable;
//     std::list<Control::DelayTable>::iterator iter;

//     /* Test Simple Case */
//     newDelayTable = createDelayTable(10000, 20000, 1);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(50000, 60000, 2);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(20000, 30000, 3);
//     insertDelayTable(newDelayTable);

//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//     iter++;
//     checkDelayTableList(iter, 20000, 30000, 3);
//     iter++;
//     checkDelayTableList(iter, 50000, 60000, 2);
//     iter++;
//     CPPUNIT_ASSERT_MESSAGE("Delay List is Incorrect Length",
//                            iter == delayList_m.end());

//     /* Test Case of overriding previous */
//     newDelayTable = createDelayTable(25000, 35000, 4);
//     insertDelayTable(newDelayTable);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//     iter++;
//     checkDelayTableList(iter, 20000, 25000, 3);
//     iter++;
//     checkDelayTableList(iter, 25000, 35000, 4);
//     iter++;
//     checkDelayTableList(iter, 50000, 60000, 2);
//     iter++;
//     CPPUNIT_ASSERT_MESSAGE("Delay List is Incorrect Length",
//                            iter == delayList_m.end());

//     /* Test Case of overriding next */
//     newDelayTable = createDelayTable(45000, 55000, 5);
//     insertDelayTable(newDelayTable);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//     iter++;
//     checkDelayTableList(iter, 20000, 25000, 3);
//     iter++;
//     checkDelayTableList(iter, 25000, 35000, 4);
//     iter++;
//     checkDelayTableList(iter, 45000, 55000, 5);
//     iter++;
//     checkDelayTableList(iter, 55000, 60000, 2);
//     iter++;
//     CPPUNIT_ASSERT_MESSAGE("Delay List is Incorrect Length",
//                            iter == delayList_m.end());

//     /* Test Case of deleting next */
//     newDelayTable = createDelayTable(44000, 56000, 6);
//     insertDelayTable(newDelayTable);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//     iter++;
//     checkDelayTableList(iter, 20000, 25000, 3);
//     iter++;
//     checkDelayTableList(iter, 25000, 35000, 4);
//     iter++;
//     checkDelayTableList(iter, 44000, 56000, 6);
//     iter++;
//     checkDelayTableList(iter, 56000, 60000, 2);
//     iter++;
//     CPPUNIT_ASSERT_MESSAGE("Delay List is Incorrect Length",
//                            iter == delayList_m.end());

//     /* Test mixed case */
//     newDelayTable = createDelayTable(34000, 57000, 7);
//     insertDelayTable(newDelayTable);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, 10000, 20000, 1);
//     iter++;
//     checkDelayTableList(iter, 20000, 25000, 3);
//     iter++;
//     checkDelayTableList(iter, 25000, 34000, 4);
//     iter++;
//     checkDelayTableList(iter, 34000, 57000, 7);
//     iter++;
//     checkDelayTableList(iter, 57000, 60000, 2);
//     iter++;
//     CPPUNIT_ASSERT_MESSAGE("Delay List is Incorrect Length",
//                            iter == delayList_m.end());
//   }

//   void testRemoveExpiredTables() {
//     Control::DelayTable newDelayTable;
//     std::list<Control::DelayTable>::iterator iter;

//     newDelayTable = createDelayTable(10000, 20000, 1);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(20000, 30000, 2);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(30000, 40000, 3);
//     insertDelayTable(newDelayTable);

//     ACS::Time now =
//       TETimeUtil::floorTE(TETimeUtil::unix2epoch(time(0))).value;
//     newDelayTable = createDelayTable(now - 10000, now, 4);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(now, now + 100000000, 5);
//     insertDelayTable(newDelayTable);

//     CPPUNIT_ASSERT_MESSAGE("Delay Table is of wrong initial size",
//                            delayList_m.size() == 5);
    
//     removeExpiredTables();
//     CPPUNIT_ASSERT_MESSAGE("Delay Table is of wrong final size",
//                            delayList_m.size() == 1);
//     iter = delayList_m.begin();
//     checkDelayTableList(iter, now, now + 100000000, 5);
//   }
    
//   void testCheckInterpolatorTable() {
//     /* Test the simple condition of no table selected */
//     CPPUNIT_ASSERT_MESSAGE("checkInterpolatorTable failed null condition",
//                            !checkInterpolatorTable(50));
    
//     /* Fix the interpolator table to point to a hardcoded entry */
//     Control::DelayTable newDelayTable;
//     newDelayTable = createDelayTable(10000, 20000, 1);
//     insertDelayTable(newDelayTable);
//     interpolatorTable_m = delayList_m.begin();

//     CPPUNIT_ASSERT_MESSAGE("checkInterpolatorTable lower limit",
//                            !checkInterpolatorTable(9999));
//     CPPUNIT_ASSERT_MESSAGE("checkInterpolatorTable failed lower limit",
//                            checkInterpolatorTable(10000));
//     CPPUNIT_ASSERT_MESSAGE("checkInterpolatorTable failed upper limit",
//                            checkInterpolatorTable(19999));
//     CPPUNIT_ASSERT_MESSAGE("checkInterpolatorTable failed upper limit",
//                            !checkInterpolatorTable(20000));
//   }

//   void testGetInterpolator() {
//     /* Populate the delay list */
//     Control::DelayTable newDelayTable;
//     newDelayTable = createDelayTable(10000, 20000, 1);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(30000, 40000, 2);
//     insertDelayTable(newDelayTable);
//     newDelayTable = createDelayTable(40000, 50000, 3);
//     insertDelayTable(newDelayTable);

//     /* Check correct functioning on each of the elements in the 
//        delay list */
//     getInterpolator(15000);
//     CPPUNIT_ASSERT_MESSAGE("Interpolator Iterator pointing to wrong entry",
//                            interpolatorTable_m->delayValues[0].time == 1);
//     getInterpolator(35000);
//     CPPUNIT_ASSERT_MESSAGE("Interpolator Iterator pointing to wrong entry",
//                            interpolatorTable_m->delayValues[0].time == 2);
//     getInterpolator(40000);
//     CPPUNIT_ASSERT_MESSAGE("Interpolator Iterator pointing to wrong entry",
//                            interpolatorTable_m->delayValues[0].time == 3);
//     getInterpolator(45000);
//     CPPUNIT_ASSERT_MESSAGE("Interpolator Iterator pointing to wrong entry",
//                            interpolatorTable_m->delayValues[0].time == 3);
    
//     /* Check exceptions for before start, missing delay, boundary, and too 
//        late conditions*/
//     CPPUNIT_ASSERT_THROW_MESSAGE("Failed to throw exception",
//                                  getInterpolator(5000),
//                                  FTSExceptions::NoValidDelayExImpl);
//     CPPUNIT_ASSERT_MESSAGE("Values for interpolator not nulled",
//                            interpolatorTable_m == delayList_m.end() &&
//                            interpolator_p ==NULL &&
//                            interpolatorTime_p ==NULL &&
//                            interpolatorValue_p == NULL);

//     /* A good value to repopulate the interpolator values */
//     getInterpolator(15000);
//     CPPUNIT_ASSERT_THROW_MESSAGE("Failed to throw exception",
//                                  getInterpolator(25000),
//                                  FTSExceptions::NoValidDelayExImpl);
//     CPPUNIT_ASSERT_MESSAGE("Values for interpolator not nulled",
//                            interpolatorTable_m == delayList_m.end() &&
//                            interpolator_p ==NULL &&
//                            interpolatorTime_p ==NULL &&
//                            interpolatorValue_p == NULL);

//     /* A good value to repopulate the interpolator values */
//     getInterpolator(15000);
//     CPPUNIT_ASSERT_THROW_MESSAGE("Failed to throw exception",
//                                  getInterpolator(20000),
//                                  FTSExceptions::NoValidDelayExImpl);
//     CPPUNIT_ASSERT_MESSAGE("Values for interpolator not nulled",
//                            interpolatorTable_m == delayList_m.end() &&
//                            interpolator_p ==NULL &&
//                            interpolatorTime_p ==NULL &&
//                            interpolatorValue_p == NULL);

//     /* A good value to repopulate the interpolator values */
//     getInterpolator(15000);
//     CPPUNIT_ASSERT_THROW_MESSAGE("Failed to throw exception",
//                                  getInterpolator(70000),
//                                  FTSExceptions::NoValidDelayExImpl);
//     CPPUNIT_ASSERT_MESSAGE("Values for interpolator not nulled",
//                            interpolatorTable_m == delayList_m.end() &&
//                            interpolator_p ==NULL &&
//                            interpolatorTime_p ==NULL &&
//                            interpolatorValue_p == NULL);
//   }

//   void testGetDelay(){
//     /* Populate the delay list */
//     Control::DelayTable newDelayTable;
//     newDelayTable.startTime = 10000;
//     newDelayTable.stopTime  = 20000;
//     newDelayTable.delayValues.length(2);
//     newDelayTable.delayValues[0].time = 10000;
//     newDelayTable.delayValues[0].totalDelay = 1.0;
//     newDelayTable.delayValues[1].time = 20000;
//     newDelayTable.delayValues[1].totalDelay = 1.0;
//     insertDelayTable(newDelayTable);

//     newDelayTable.startTime = 20000;
//     newDelayTable.stopTime  = 30000;
//     newDelayTable.delayValues.length(2);
//     newDelayTable.delayValues[0].time = 20000;
//     newDelayTable.delayValues[0].totalDelay = 2.0;
//     newDelayTable.delayValues[1].time = 30000;
//     newDelayTable.delayValues[1].totalDelay = 2.0;
//     insertDelayTable(newDelayTable);

//     CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, getDelay(15000), 1E-14);
//     CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, getDelay(20000), 1E-14);
//     CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, getDelay(25000), 1E-14);
//     CPPUNIT_ASSERT_THROW_MESSAGE("Failed to throw exception",
//                                  getDelay(30000),
//                                  FTSExceptions::NoValidDelayExImpl);
//   }

  CPPUNIT_TEST_SUITE(WalshDelayCalculatorTest);
  CPPUNIT_TEST(testInitialCondition);
  CPPUNIT_TEST(testStaticDelay);
  CPPUNIT_TEST(testQueingOfCoarseDelays);

//   CPPUNIT_TEST(testInsertingOneDelayTable);
//   CPPUNIT_TEST(testInsertingDelayTableInFront);
//   CPPUNIT_TEST(testInsertingDelayTableInBack);
//   CPPUNIT_TEST(testInsertingDelayTableInMiddle);
//   CPPUNIT_TEST(testRemoveExpiredTables);
//   CPPUNIT_TEST(testCheckInterpolatorTable);
//   CPPUNIT_TEST(testGetInterpolator);
//   CPPUNIT_TEST(testGetDelay);
  CPPUNIT_TEST_SUITE_END();

  /* ---------------- Helper Methods ---------------- */
private:
//   Control::DelayTable createDelayTable(ACS::Time startTime,
//                                        ACS::Time stopTime,
//                                        ACS::Time valueFlag) {
//     Control::DelayTable newDelayTable;
//     newDelayTable.startTime = startTime;
//     newDelayTable.stopTime  = stopTime;
//     newDelayTable.delayValues.length(1);
//     newDelayTable.delayValues[0].time = valueFlag;
//     newDelayTable.delayValues[0].totalDelay = 0.0;

//     return newDelayTable;
//   }
  
//   void checkDelayTableList(std::list<Control::DelayTable>::iterator& iter, 
//                            ACS::Time startTime,
//                            ACS::Time stopTime,
//                            ACS::Time valueFlag) {
 
//     if (startTime != (iter->startTime)) {
//       ostringstream msg; 
//       msg << "Start time for iterator (" << iter->startTime
//           << ") does not match expected value " << startTime;
//       CPPUNIT_FAIL(msg.str());
//     }

//     if (stopTime != iter->stopTime) {
//       ostringstream msg; 
//       msg << "Stop time for iterator (" << iter->stopTime
//           << ") does not match expected value " << stopTime;
//       CPPUNIT_FAIL(msg.str());
//     }

//     if (valueFlag != iter->delayValues[0].time) {
//       ostringstream msg; 
//       msg << "Value flag for iterator (" << iter->delayValues[0].time 
//           << ") does not match expected value " << valueFlag;
//       CPPUNIT_FAIL(msg.str());
//     }
//   }

  
}; // End of Test Class

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(WalshDelayCalculatorTest::suite());
  runner.run();
  return 0;
}
