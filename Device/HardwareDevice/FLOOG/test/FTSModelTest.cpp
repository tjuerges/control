/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <FTSModel.h>
#include <TETimeUtil.h>
#include <ControlExceptions.h>
 
class FTSModelTest : public CppUnit::TestFixture 
{
public:

  void testInitialCondition() {
    FTSModel ftsModel;
    CPPUNIT_ASSERT_MESSAGE("Initial condition is incorrect",
                           ftsModel.getFTSPhase(0) == 0LL);
  }

  void testUnityFrequency() {
    FTSModel ftsModel;

    ftsModel.setFTSFrequency(0x1LL, 0);

    /* The frequency is updating at 125E6/1E7 cycles per ACS Time Unit */
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 10",
                           ftsModel.getFTSPhase(10) == 125);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 20",
                           ftsModel.getFTSPhase(20) == 250);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 4",
                           ftsModel.getFTSPhase(40) == 500);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 100",
                           ftsModel.getFTSPhase(100) == 1250);
    CPPUNIT_ASSERT_MESSAGE("Phase is wrong just before the phase wrap",
                           ftsModel.getFTSPhase(22517998136850LL) 
                           == 0xFFFFFFFFFFe1LL);
    CPPUNIT_ASSERT_MESSAGE("Phase is wrong just after the phase wrap",
                           ftsModel.getFTSPhase(22517998136860LL)
                           ==  0x5eLL);
    CPPUNIT_ASSERT_MESSAGE("Phase is wrong somewhere in 2039",
                           ftsModel.getFTSPhase(144052254169920000LL)
                           == 0x34863a279100LL);
  }

  void testUnityFrequencyNonZeroTime() {
    FTSModel ftsModel;

    ACS::Time now =
      TETimeUtil::nearestTE(TETimeUtil::unix2epoch(time(0))).value;

    ftsModel.setFTSFrequency(0x1LL, now);

     /* The frequency is updating at 125E6/1E7 cycles per ACS Time Unit */
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 10",
                            ftsModel.getFTSPhase(now+10) == 125);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 20",
                            ftsModel.getFTSPhase(now+20) == 250);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 4",
                           ftsModel.getFTSPhase(now+40) == 500);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 100",
                           ftsModel.getFTSPhase(now+100) == 1250);
    CPPUNIT_ASSERT_MESSAGE("Phase is wrong just before the phase wrap",
                           ftsModel.getFTSPhase(now+22517998136850LL) 
                           == 0xFFFFFFFFFFe1LL);
    CPPUNIT_ASSERT_MESSAGE("Phase is wrong just after the phase wrap",
                           ftsModel.getFTSPhase(now+22517998136860LL)
                           ==  0x5eLL);
  }

  void testHighFrequency() {
    FTSModel ftsModel;

    /* This frequency corresponds to 60 MHz */
    ftsModel.setFTSFrequency(0x7ae147ae147aLL, 0);
    
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 10",
                           ftsModel.getFTSPhase(10) == 0xffffffffff92LL);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at Time = 20",
                           ftsModel.getFTSPhase(20) == 0xffffffffff24LL);
    CPPUNIT_ASSERT_MESSAGE("Phase is incorrect at large time",
                           ftsModel.getFTSPhase(144052254169920000LL) == 
                           0x759e5c2f1a00LL);
  }


  void testSetFrequencyInThePast() {
    FTSModel ftsModel;
    ACS::Time now =
      TETimeUtil::nearestTE(TETimeUtil::unix2epoch(time(0))).value;


    ftsModel.setFTSFrequency(0x0ff442233LL, now);
    ftsModel.setFTSFrequency(0x000111, now -10);
  }

  void testGetPhaseInThePast() {
    FTSModel ftsModel;
    ACS::Time now =
      TETimeUtil::nearestTE(TETimeUtil::unix2epoch(time(0))).value;

    ftsModel.setFTSFrequency(0x0ff442233LL, now);
    ftsModel.getFTSPhase(now -10);
  }

  CPPUNIT_TEST_SUITE(FTSModelTest);
  CPPUNIT_TEST(testInitialCondition);
  CPPUNIT_TEST(testUnityFrequency);
  CPPUNIT_TEST(testUnityFrequencyNonZeroTime);
  CPPUNIT_TEST(testHighFrequency);
  CPPUNIT_TEST_EXCEPTION(testSetFrequencyInThePast,
                         ControlExceptions::IllegalParameterErrorExImpl);
  CPPUNIT_TEST_EXCEPTION(testGetPhaseInThePast,
                         ControlExceptions::IllegalParameterErrorExImpl);
  CPPUNIT_TEST_SUITE_END();
  
}; // End of Test Class

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(FTSModelTest::suite());
  runner.run();
  return 0;
}
