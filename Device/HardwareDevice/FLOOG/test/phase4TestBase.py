#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

import sys
import unittest
import struct
import Control
import ControlExceptions
import time
from Control import HardwareDevice

from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.TimeHelper import TimeUtil

from Acspy.Common.QoS import setObjectTimeout

import Acspy.Util

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self._client=PySimpleClient.getInstance()
        setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
        self.out= ""
        self.tu = TimeUtil()
        unittest.TestCase.__init__(self,methodName)

    def __del__(self):
        if self.out != None:
            print self.out
        self._client.disconnect()


    def setUp(self):
        self._ref= self._client.getComponent("CONTROL/DV01/FLOOG")
        setObjectTimeout(self._ref, 20000)
        self._ref.hwConfigure()
        self._ref.hwInitialize()
        self._ref.hwOperational()

    def tearDown(self):
        self._client.releaseComponent(self._ref._get_name())
        
    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"


    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Ensure the Device Goes into operational mode
        '''
        self.failIf(self._ref.getHwState() != HardwareDevice.Operational,
                    "FLOOG Failed to enter Operational State")
    

    def test02(self):
        '''
        Verify our ability to get the Read Only property Status
        '''
        self.startTestLog("Test 2: Property Test")
        prop = self._ref._get_FTS_STATUS()
        self.failUnlessEqual(prop._get_name(),
                             self._ref._get_name() + ':FTS_STATUS',
                             "FTS_Status Name error");
        returnVal = prop.get_sync()
        self.failIf(returnVal[1].code != 0,"Error reading property");
        self.log("Property "+ prop._get_name()+ " reports value "+
                 str(returnVal[0]))
        self.endTestLog("Test 2: FTS Status Property Test")


class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg


 

# **************************************************
# MAIN Program
if __name__ == '__main__':
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("Test")

    container = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()
