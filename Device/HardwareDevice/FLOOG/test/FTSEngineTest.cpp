/* @(#) $Id: testRtFindKernelModules.cpp,v 1.4 2006/08/31 18:37:38 ramestic Exp
$
 *
 * Copyright (C) 2006
 * Associated Universities, Inc. Washington DC, USA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * Correspondence concerning ALMA Software should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <FTSDeviceData.h>
#include <FTSEngine.h>
#include <TETimeUtil.h>
#include <ControlExceptions.h>
#include <ControlDataInterfacesC.h>
#include <sstream>
#include <cmath>


using std::ostringstream;

class FTSEngineTest : public CppUnit::TestFixture,
                    public FTSEngine
{
public:
  void testInitialCondition() {
    CPPUNIT_ASSERT_MESSAGE("Initial size of delayList is incorrect",
                           delayList_m.size() == 0);
    CPPUNIT_ASSERT_MESSAGE("Interpolator Table Iterator is wrong",
                           delayList_m.end() == interpolatorTable_m);
    CPPUNIT_ASSERT_MESSAGE("Frequency Deque was not intinitalized correctly",
                            frequencySpecificationDeque_m.size() == 0);
  }

  void testBasicFrequency() {
    setFrequency(0xABAB, 100E9, 0);
    CPPUNIT_ASSERT_MESSAGE("Frequency not inserted correctly",
                           frequencySpecificationDeque_m.size() == 1);

    FTSEngine::FrequencySpecification freqResp = getNominalFrequencySpec(10);
    checkFrequencyResponse(freqResp,0xABAB, 100E9, 0);

    CPPUNIT_ASSERT_MESSAGE("FTS Frequency response is not as expected",
                           getNominalFTSFrequency()==0xABAB);
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("LO Freq response is incorrect",
                                         getNominalLoFrequency(),100.0E9,
                                         1E-14);
  }

  void testFrequencyQueue() {
    /* Test the outdating of old enties */
    setFrequency(0xABAB, 100E9, 0);
    setFrequency(0xCDCD, 101E9, 20);
    setFrequency(0xEFEF, 102E9, 15);

    CPPUNIT_ASSERT_MESSAGE("Failed to remove outdated frequency",
                           frequencySpecificationDeque_m.size() == 2);

    /* Now test that we get the right frequencies back */
    FTSEngine::FrequencySpecification freqResp;
    freqResp = getNominalFrequencySpec(10);
    checkFrequencyResponse(freqResp,0xABAB, 100E9, 0);
    freqResp = getNominalFrequencySpec(15);
    checkFrequencyResponse(freqResp,0xEFEF, 102E9, 15);
    freqResp = getNominalFrequencySpec(20);
    checkFrequencyResponse(freqResp,0xEFEF, 102E9, 15);

    CPPUNIT_ASSERT_MESSAGE("FTS Frequency response is not as expected",
                           getNominalFTSFrequency()==0xEFEF);
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("LO Freq response is incorrect",
                                         getNominalLoFrequency(),102.0E9,
                                         1E-14);
  }

  void testFrequencySetting() {
    /* Initial Condition is that next frequency command time starts out
       at zero.  Thus the following command should generate a command
       about 5 TEs in the future
    */
    setFrequency(0xABAB, 100E9, 0);
    checkNumberOfFrequencyCommands(1);

    long long lookAhead = static_cast<long long>
      (frequencyCommandQueue_m[0].applicationTime - ::getTimeStamp());
    CPPUNIT_ASSERT_MESSAGE("Frequency Command queued at incorrect time",
                           abs(lookAhead - 5 * 480000) < 480000);
    frequencyCommandQueue_m.clear();

    /* This command is now in the past so we expect to see
       a reset and then have it queued
    */
    flushTimes_m.clear();
    nextFrequencyCommandTime_m = 200000000;
    setFrequency(0xCDCD, 100E9, 100000000);
    checkNumberOfFrequencyCommands(1);

    CPPUNIT_ASSERT_MESSAGE("Frequency Command queued at incorrect time",
                           abs(lookAhead - 5 * 480000) < 480000);
    CPPUNIT_ASSERT_MESSAGE("Did not correctly reset the FTS",
                           flushTimes_m.size() == 1);
    frequencyCommandQueue_m.clear();
    flushTimes_m.clear();

    setFrequency(0xCDCD, 100E9, 110000000);
    checkNumberOfFrequencyCommands(0);
    CPPUNIT_ASSERT_MESSAGE("Incorrectly reset to same frequency",
                           flushTimes_m.empty());
  }

  void testFrequencyOffsetting() {
    /* Use 31.25 MHz and 100 GHz as our nominal frequency */
    setFrequency(0x400000000000ULL, 100.0E9, 0);

    /* Check first in the default condition */
    FTSEngine::FrequencySpecification freqResp;

    freqResp = getOffsetFrequencySpec(10);
    checkFrequencyResponse(freqResp,0x400000000000ULL, 100.0E9, 0);
    CPPUNIT_ASSERT_MESSAGE("Offset FTS Frequency incorrect",
                           getOffsetFTSFrequency() == 0x400000000000ULL);
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Offset LO Frequency incorrect",
                                         getOffsetLoFrequency(), 100.0E9,
                                         1E-14);

    setFrequencyOffsetting(Control::LOOffsettingMode_SecondLO);
    CPPUNIT_ASSERT_MESSAGE("Failed to set frequency offsetting mode to "
        "LOOffsettingMode_SecondLO", getFrequencyOffsettingMode());
    freqResp = getOffsetFrequencySpec(10);
    checkFrequencyResponse(freqResp,0x400000000000ULL, 100.0E9, 0);

    for (int idx = -66; idx < 66; idx++) {
      setFrequencyOffsetFactor(idx);
      CPPUNIT_ASSERT_MESSAGE("Failed to set frequency offsetting factor",
                             idx == getFrequencyOffsetFactor());
      freqResp = getOffsetFrequencySpec(10);
      checkFrequencyResponse(freqResp,
                             0x400000000000ULL + (0x1000000000ULL * idx),
                             100.0E9 + (idx * 30517.578125),
                             0);
      CPPUNIT_ASSERT_MESSAGE("Offset FTS Frequency incorrect",
                             getOffsetFTSFrequency() ==
                             0x400000000000ULL + (0x1000000000ULL * idx));
      CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Offset LO Frequency incorrect",
                                           getOffsetLoFrequency(),
                                           100.0E9 + (idx * 30517.578125),
                                           1E-14);
    }

    setColdMultiplier(2);
    CPPUNIT_ASSERT_MESSAGE("Failed to set Cold Multiplier",
                           getColdMultiplier() == 2);
    for (int idx = -66; idx < 66; idx++) {
      setFrequencyOffsetFactor(idx);
      CPPUNIT_ASSERT_MESSAGE("Failed to set frequency offsetting factor",
                             idx == getFrequencyOffsetFactor());
      freqResp = getOffsetFrequencySpec(10);
      checkFrequencyResponse(freqResp,
                             0x400000000000ULL + (0x800000000ULL * idx),
                             100.0E9 + (idx * 30517.578125),
                             0);
    }

    setColdMultiplier(3);
    CPPUNIT_ASSERT_MESSAGE("Failed to set Cold Multiplier",
                           getColdMultiplier() == 3L);
    for (int idx = -66; idx < 66; idx++) {
      setFrequencyOffsetFactor(idx);
      CPPUNIT_ASSERT_MESSAGE("Failed to set frequency offsetting factor",
                             idx == getFrequencyOffsetFactor());
      freqResp = getOffsetFrequencySpec(10);
      checkFrequencyResponse(freqResp,
                             0x400000000000ULL +
                             (static_cast<long long>(0x1000000000ULL * idx)/3),
                             100.0E9 + (idx * 30517.578125),
                             0);
    }

    setColdMultiplier(4);
    CPPUNIT_ASSERT_MESSAGE("Failed to set Cold Multiplier",
                           getColdMultiplier() == 4);
    for (int idx = -66; idx < 66; idx++) {
      setFrequencyOffsetFactor(idx);
      CPPUNIT_ASSERT_MESSAGE("Failed to set frequency offsetting factor",
                             idx == getFrequencyOffsetFactor());
      freqResp = getOffsetFrequencySpec(10);
      checkFrequencyResponse(freqResp,
                             0x400000000000ULL + (0x400000000ULL * idx),
                             100.0E9 + (idx * 30517.578125),
                             0);
    }
  }

  void testUpdateFrequencyWithoutFringeTracking(){
    /* Use 31.25 MHz and 100 GHz as our nominal frequency */
    setFrequency(0x400000000000ULL, 100.0E9, 0);
    setFrequency(0x410000000000ULL, 100.0E9, 2880000);

    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();
    updateFrequency(480000, false); // Should send a command
    updateFrequency(960000, false); // No Command sent
    updateFrequency(1920000, false); // No Command sent
    updateFrequency(2400000, false); // Should send a command
    updateFrequency(2880000, false); // No Command sent

    checkNumberOfFrequencyCommands(2);
    checkFrequencyCommand(0,  480000,0x400000000000ULL);
    checkFrequencyCommand(1, 2400000,0x410000000000ULL);

    /* Now try with frequency offsetting */
    setFrequencyOffsetFactor(2);
    setFrequencyOffsetting(Control::LOOffsettingMode_SecondLO);

    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();
    updateFrequency(480000, false); // Should send a command
    updateFrequency(960000, false); // No Command sent
    updateFrequency(1920000, false); // No Command sent
    updateFrequency(2400000, false); // Should send a command
    updateFrequency(2880000, false); // No Command sent

    checkNumberOfFrequencyCommands(2);
    checkFrequencyCommand(0,  480000,0x402000000000ULL);
    checkFrequencyCommand(1, 2400000,0x412000000000ULL);

    /* And check the cold multiplier works */
    setColdMultiplier(4);

    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();
    updateFrequency(480000, false); // Should send a command
    updateFrequency(960000, false); // No Command sent
    updateFrequency(1920000, false); // No Command sent
    updateFrequency(2400000, false); // Should send a command
    updateFrequency(2880000, false); // No Command sent

    checkNumberOfFrequencyCommands(2);
    checkFrequencyCommand(0,  480000,0x400800000000ULL);
    checkFrequencyCommand(1, 2400000,0x410800000000ULL);
  }

  void testReturnToPhase(){
    /* Put in zero delay */
    insertDelayTable(createDelayTable(0, 5760000, 0));

    setFrequency(0x400000000001ULL, 100.0E9, 0);
    setFrequency(0x605500000000ULL, 100.0E9, 2880000);

    try {
      enableFringeTracking(true);
    } catch (FTSExceptions::NoValidDelayExImpl ex) {
      /* This is ok since we have no delays for the current
         time */
    }

    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /* All of the following updates should send a command */
    updateFrequency(480000, false); // F = 0x400000000003L
    updateFrequency(960000, false); // F = 0x400000000001L
    updateFrequency(1440000, false); // F = 0x400000000001L
    updateFrequency(1920000, false); // F = 0x400000000001L
    updateFrequency(2400000, false); // F = 0x6054FFB4809FL
    updateFrequency(2880000, false); // F = 0x605500000000L
    updateFrequency(3360000, false); // F = 0x605500000000L

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x400000000003ULL);
    checkFrequencyCommand(1,  960000, 0x400000000001ULL);
    checkFrequencyCommand(2, 1440000, 0x400000000001ULL);
    checkFrequencyCommand(3, 1920000, 0x400000000001ULL);
    checkFrequencyCommand(4, 2400000, 0x6054FFB480A0ULL);
    checkFrequencyCommand(5, 2880000, 0x605500000000ULL);
    checkFrequencyCommand(6, 3360000, 0x605500000000ULL);
  }

  void testOnekHzFringeRate(){
    /* Put in zero delay */
    /* Put in a delay ramp:
       What we want is a fringe rate of 1 kHz.  That corresponds to
       (2**48) * 1000 * 0.048 phase per TE or a frequency offset of
       about 0x1E0000000000000 / 6000000 = 0x53E2D6238L.
    */

    Control::DelayTable newDelayTable;
    newDelayTable.startTime = 0;
    newDelayTable.stopTime  = 60000000; //6 sec
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = newDelayTable.startTime;
    newDelayTable.delayValues[0].totalDelay = 6.3157894736842108e-09;
    newDelayTable.delayValues[1].time = newDelayTable.stopTime;
    newDelayTable.delayValues[1].totalDelay = 0;
    insertDelayTable(newDelayTable);

    setColdMultiplier(3);

    setFrequency(0x400000000001ULL, 950.0E9, 0);
    setFrequency(0x605500000000ULL, 950.0E9, 2880000);

    // try {
      enableFringeTracking(true);
      //    } catch (FTSExceptions::NoValidDelayExImpl ex) {
      /* This is ok since we have no delays for the current
         time */
      //}

    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();


    /* All of the following updates should send a command */
    updateFrequency( 480000, false); // F = 0x400000000003L
    updateFrequency( 960000, false); // F = 0x400000000001L
    updateFrequency(1440000, false); // F = 0x400000000001L
    updateFrequency(1920000, false); // F = 0x400000000001L
    updateFrequency(2400000, false); // F = 0x6054FFB4809FL
    updateFrequency(2880000, false); // F = 0x605500000000L
    updateFrequency(3360000, false); // F = 0x605500000000L

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x40002cbd3f04ULL);
    checkFrequencyCommand(1,  960000, 0x40002cbd3f03ULL);
    checkFrequencyCommand(2, 1440000, 0x40002cbd3f03ULL);
    checkFrequencyCommand(3, 1920000, 0x40002cbd3f03ULL);
    checkFrequencyCommand(4, 2400000, 0x60552c71bfa2ULL);
    checkFrequencyCommand(5, 2880000, 0x60552cbd3f02ULL);
    checkFrequencyCommand(6, 3360000, 0x60552cbd3f01ULL);
  }


  void testNonZeroDelay(){
    setFrequency(0x400000000001ULL, 100.1001E9, 0);
    try {
      enableFringeTracking(true);
    } catch (FTSExceptions::NoValidDelayExImpl ex) {
      /* This is ok since we have no delays for the current
         time */
    }

    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /* Put in zero delay */
    insertDelayTable(createDelayTable(      0, 2400000, 1E-9));
    insertDelayTable(createDelayTable(2400000, 5000000, 1E-7));


    /* All of the following updates should send a command */
    updateFrequency(480000, false);
    updateFrequency(960000, false);
    updateFrequency(1440000, false);
    updateFrequency(1920000, false);
    updateFrequency(2400000, false);
    updateFrequency(2880000, false);
    updateFrequency(3360000, false);

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x3fffffb8587FULL);
    checkFrequencyCommand(1,  960000, 0x400000000001ULL);
    checkFrequencyCommand(2, 1440000, 0x3f93c1e22dcbULL);
    checkFrequencyCommand(3, 1920000, 0x400000000001ULL);
    checkFrequencyCommand(4, 2400000, 0x400000000001ULL);
    checkFrequencyCommand(5, 2880000, 0x400000000001ULL);
    checkFrequencyCommand(6, 3360000, 0x400000000001ULL);
  }

  void testFringeTracking(){
    setFrequency(0x400000000001ULL, 100.1001E9, 0);
    enableFringeTracking(true);

    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /* Put in a delay ramp:  The delays chosen here are for an
       antenna west of the refrence location and should correspond
       to an adjustment of the frequncy by approximatly 0x2A. */
    Control::DelayTable newDelayTable;
    newDelayTable.startTime = 0;
    newDelayTable.stopTime  = 600000000; //6 sec
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = newDelayTable.startTime;
    newDelayTable.delayValues[0].totalDelay = 1.1091128027096443e-14;
    newDelayTable.delayValues[1].time = newDelayTable.stopTime;
    newDelayTable.delayValues[1].totalDelay = 0;
    insertDelayTable(newDelayTable);

    updateFrequency(480000, false);
    updateFrequency(960000, false);
    updateFrequency(1440000, false);
    updateFrequency(1920000, false);
    updateFrequency(2400000, false);
    updateFrequency(2880000, false);
    updateFrequency(3360000, false);

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x3fffffff350DULL);
    checkFrequencyCommand(1,  960000, 0x40000000002AULL);
    checkFrequencyCommand(2, 1440000, 0x40000000002BULL);
    checkFrequencyCommand(3, 1920000, 0x40000000002AULL);
    checkFrequencyCommand(4, 2400000, 0x40000000002BULL);
    checkFrequencyCommand(5, 2880000, 0x40000000002BULL);
    checkFrequencyCommand(6, 3360000, 0x40000000002AULL);
  }

  void testFastFringeTracking(){
    setFrequency(0x400000000001ULL, 950.00E9, 0);
    enableFringeTracking(true);
    setSideband(LSB);
    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /* Put in a delay ramp:
       What we want is a fringe rate of 1 kHz.  That corresponds to
       (2**48) * 1000 * 0.048 phase per TE or a frequency offset of
       about 0x1E0000000000000 / 6000000 = 0x53E2D6238L.
    */


    Control::DelayTable newDelayTable;
    newDelayTable.startTime = 0;
    newDelayTable.stopTime  = 60000000; //6 sec
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = newDelayTable.startTime;
    newDelayTable.delayValues[0].totalDelay = 6.3157894736842108e-09;
    newDelayTable.delayValues[1].time = newDelayTable.stopTime;
    newDelayTable.delayValues[1].totalDelay = 0;
    insertDelayTable(newDelayTable);

    updateFrequency(480000, false);
    updateFrequency(960000, false);
    updateFrequency(1440000, false);
    updateFrequency(1920000, false);
    updateFrequency(2400000, false);
    updateFrequency(2880000, false);
    updateFrequency(3360000, false);

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x3fff79c842feULL);
    checkFrequencyCommand(1,  960000, 0x3fff79c842fbULL);
    checkFrequencyCommand(2, 1440000, 0x3fff79c842fbULL);
    checkFrequencyCommand(3, 1920000, 0x3fff79c842fcULL);
    checkFrequencyCommand(4, 2400000, 0x3fff79c842fbULL);
    checkFrequencyCommand(5, 2880000, 0x3fff79c842fbULL);
    checkFrequencyCommand(6, 3360000, 0x3fff79c842fcULL);
  }

  void testFastFringeTracking2(){
    setFrequency(0x400000000000ULL, 92.00E9, 0);
    setSideband(LSB);
    enableFringeTracking(true);
    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /*
       This test was put in to help diagnose the fringe tracking problems
       seen at the OSF.  The numbers were developed by Peter N.  We
       we want a 1.6e-9 delay in 6 seconds.  This should give a fringe
       rate of 25 MHx (0x35afe53) or a steady state frequency of
       0x3FFFFCA501AE
    */

    Control::DelayTable newDelayTable;
    newDelayTable.startTime = 0;
    newDelayTable.stopTime  = 60000000; //6 sec
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = newDelayTable.startTime;
    newDelayTable.delayValues[0].totalDelay = 1.6e-9;
    newDelayTable.delayValues[1].time = newDelayTable.stopTime;
    newDelayTable.delayValues[1].totalDelay = 0;
    insertDelayTable(newDelayTable);

    updateFrequency(480000, false);
    updateFrequency(960000, false);
    updateFrequency(1440000, false);
    updateFrequency(1920000, false);
    updateFrequency(2400000, false);
    updateFrequency(2880000, false);
    updateFrequency(3360000, false);



    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x3ffffc45f1d2ULL);
    checkFrequencyCommand(1,  960000, 0x3ffffcb50a84ULL);
    checkFrequencyCommand(2, 1440000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(3, 1920000, 0x3ffffcb50a84ULL);
    checkFrequencyCommand(4, 2400000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(5, 2880000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(6, 3360000, 0x3ffffcb50a84ULL);
  }

  void testFastFringeTracking3LSB(){
    setFrequency(0x400000000000ULL, 92.00E9, 0);
    setSideband(LSB);
    enableFringeTracking(true);
    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /*
       This test was put in to help diagnose the fringe tracking problems
       seen at the OSF.  The numbers were developed by Peter N.  We
       we want a 1.6e-9 delay in 6 seconds.  This should give a fringe
       rate of 24.5 MHz or a steady state frequency of
       0x3FFFFCA501AE

       This is the same as FastFringeTracking but the sign of the
       delay derivitive is reversed.
    */

    Control::DelayTable newDelayTable;
    newDelayTable.startTime = 0;
    newDelayTable.stopTime  = 60000000; //6 sec
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = newDelayTable.startTime;
    newDelayTable.delayValues[0].totalDelay = 0;
    newDelayTable.delayValues[1].time = newDelayTable.stopTime;
    newDelayTable.delayValues[1].totalDelay = 1.6E-9;
    insertDelayTable(newDelayTable);

    updateFrequency(480000, false);
    updateFrequency(960000, false);
    updateFrequency(1440000, false);
    updateFrequency(1920000, false);
    updateFrequency(2400000, false);
    updateFrequency(2880000, false);
    updateFrequency(3360000, false);

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x400004493892ULL);
    checkFrequencyCommand(1,  960000, 0x4000034af57bULL);
    checkFrequencyCommand(2, 1440000, 0x4000034af57bULL);
    checkFrequencyCommand(3, 1920000, 0x4000034af57cULL);
    checkFrequencyCommand(4, 2400000, 0x4000034af57bULL);
    checkFrequencyCommand(5, 2880000, 0x4000034af57cULL);
    checkFrequencyCommand(6, 3360000, 0x4000034af57bULL);
  }

  void testFastFringeTracking3USB(){
    setFrequency(0x400000000000ULL, 92.00E9, 0);
    setSideband(USB);
    enableFringeTracking(true);
    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /*
       This test was put in to help diagnose the fringe tracking problems
       seen at the OSF.  The numbers were developed by Peter N.  We
       we want a 1.6e-9 delay in 6 seconds.  This should give a fringe
       rate of 24.5 MHz or a steady state frequency of
       0x3FFFFCA501AE

       This is the same as FastFringeTracking but the sign of the
       delay derivitive is reversed.
    */

    Control::DelayTable newDelayTable;
    newDelayTable.startTime = 0;
    newDelayTable.stopTime  = 60000000; //6 sec
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = newDelayTable.startTime;
    newDelayTable.delayValues[0].totalDelay = 0;
    newDelayTable.delayValues[1].time = newDelayTable.stopTime;
    newDelayTable.delayValues[1].totalDelay = 1.6E-9;
    insertDelayTable(newDelayTable);

    updateFrequency(480000, false);
    updateFrequency(960000, false);
    updateFrequency(1440000, false);
    updateFrequency(1920000, false);
    updateFrequency(2400000, false);
    updateFrequency(2880000, false);
    updateFrequency(3360000, false);

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x3ffffbb6c76eULL);
    checkFrequencyCommand(1,  960000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(2, 1440000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(3, 1920000, 0x3ffffcb50a84ULL);
    checkFrequencyCommand(4, 2400000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(5, 2880000, 0x3ffffcb50a84ULL);
    checkFrequencyCommand(6, 3360000, 0x3ffffcb50a85ULL);
  }

  void testFastFringeTracking4(){
    setFrequency(0x400000000000ULL, 92.00E9, 0);
    enableFringeTracking(true);
    setSideband(LSB);
    frequencyCommandQueue_m.clear();
    ftsHardwareModel_m.resetFTS();

    /*
       This test was put in to help diagnose the fringe tracking problems
       seen at the OSF.  The numbers were developed by Peter N.  We
       we want a 1.6e-9 delay in 6 seconds.  This should give a fringe
       rate of 24.5 MHz or a steady state frequency of
       0x3FFFFCA501AE

       This is the same as FastFringeTracking2 but the sign of the
       delay is reversed.
    */

    Control::DelayTable newDelayTable;
    newDelayTable.startTime = 0;
    newDelayTable.stopTime  = 60000000; //6 sec
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = newDelayTable.startTime;
    newDelayTable.delayValues[0].totalDelay = 0;
    newDelayTable.delayValues[1].time = newDelayTable.stopTime;
    newDelayTable.delayValues[1].totalDelay = -1.6E-9;
    insertDelayTable(newDelayTable);

    updateFrequency(480000, false);
    updateFrequency(960000, false);
    updateFrequency(1440000, false);
    updateFrequency(1920000, false);
    updateFrequency(2400000, false);
    updateFrequency(2880000, false);
    updateFrequency(3360000, false);

    checkNumberOfFrequencyCommands(7);
    checkFrequencyCommand(0,  480000, 0x3ffffbb6c76eULL);
    checkFrequencyCommand(1,  960000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(2, 1440000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(3, 1920000, 0x3ffffcb50a84ULL);
    checkFrequencyCommand(4, 2400000, 0x3ffffcb50a85ULL);
    checkFrequencyCommand(5, 2880000, 0x3ffffcb50a84ULL);
    checkFrequencyCommand(6, 3360000, 0x3ffffcb50a85ULL);
  }


  void testPropagationDelay(){
    setPropagationDelay(1E-6);
    CPPUNIT_ASSERT_MESSAGE("Failed to set the propagation delay",
                           propagationDelay_m == 10);
    CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Failed to get the propagation delay",
                                         1E-6,getPropagationDelay(),1E-14);
  }

  void testPhaseSwitching(){
    setPhaseValues();

    enableSlowPhaseSwitching(true);
    CPPUNIT_ASSERT_MESSAGE("Failed to set slow phase switching",
                           enableSlowPhaseSwitching_m);
    CPPUNIT_ASSERT_MESSAGE("Failed to return slow phase switching status",
                           slowPhaseSwitchingEnabled());

    enableFastPhaseSwitching(true);
    CPPUNIT_ASSERT_MESSAGE("Failed to set fast phase switching",
                           enableFastPhaseSwitching_m);
    CPPUNIT_ASSERT_MESSAGE("Failed to return fast phase switching status",
                           fastPhaseSwitchingEnabled());

    enableSlowPhaseSwitching(false);
    CPPUNIT_ASSERT_MESSAGE("Failed to set slow phase switching",
                           !enableSlowPhaseSwitching_m);
    CPPUNIT_ASSERT_MESSAGE("Failed to return slow phase switching status",
                           !slowPhaseSwitchingEnabled());

    enableFastPhaseSwitching(false);
    CPPUNIT_ASSERT_MESSAGE("Failed to set fast phase switching",
                           !enableFastPhaseSwitching_m);
    CPPUNIT_ASSERT_MESSAGE("Failed to return fast phase switching status",
                           !fastPhaseSwitchingEnabled());

    checkNumberOfPhaseCommands(5);
    checkPhaseCommand(0,   0.0,  0.0,      0.0,      0.0);
    checkPhaseCommand(1,   0.0,  0.0, M_PI*0.5, M_PI*0.5);
    checkPhaseCommand(2,   0.0, M_PI, M_PI*0.5, M_PI*1.5);
    checkPhaseCommand(3,   0.0, M_PI,      0.0,     M_PI);
    checkPhaseCommand(5,   0.0,  0.0,      0.0,      0.0);

    /* Now do a spot check with the cold mulitplier */
    phaseCommandQueue_m.clear();

    setFrequency(0x400000000001ULL, 100.1001E9, 0);
    setColdMultiplier(3);
    enableSlowPhaseSwitching(true);
    enableFastPhaseSwitching(true);
    enableSlowPhaseSwitching(false);
    enableFastPhaseSwitching(false);

    checkNumberOfPhaseCommands(5);
    checkPhaseCommand(0,   0.0,    0.0,        0.0,        0.0);
    checkPhaseCommand(1,   0.0,    0.0, M_PI*0.5/3, M_PI*0.5/3);
    checkPhaseCommand(2,   0.0, M_PI/3, M_PI*0.5/3, M_PI*1.5/3);
    checkPhaseCommand(3,   0.0, M_PI/3,        0.0,     M_PI/3);
    checkPhaseCommand(5,   0.0,    0.0,        0.0,        0.0);
  }

  void testReset(){
    setFrequency(0x600000000000ULL, 100.00E9, 0);
    enableFringeTracking(true);

    /* Now Manually adjust a state points to ensure that we pick them up */
    fringeTrackingEnabled_m = false;

    flushTimes_m.clear();
    frequencyCommandQueue_m.clear();
    phaseCommandQueue_m.clear();
    resetFTSTime_m.clear();
    ACS::Time now = ::getTimeStamp();
    now -= now % TETimeUtil::TE_PERIOD_ACS;

    reset();

    /* The flush should be from now or one TE in the future*/
    CPPUNIT_ASSERT_MESSAGE("Incorrect number of flushes found.",
                           flushTimes_m.size() ==1 );
    CPPUNIT_ASSERT_MESSAGE("Flush happened at incorrect time",
                           flushTimes_m[0] - now <= 480000);

    /* The FTS should be reset at about the same time */
    CPPUNIT_ASSERT_MESSAGE("Incorrect number of resets found.",
                           resetFTSTime_m.size() ==1 );
    CPPUNIT_ASSERT_MESSAGE("Reset happened at incorrect time",
                           resetFTSTime_m[0] - now <= 480000);

    /*
       We expect a frequency command to come out about 5 TE's later
    */
    checkNumberOfFrequencyCommands(1);
    checkFrequencyCommand(0, now + 6 * TETimeUtil::TE_PERIOD_ACS,
                          0x600000000000ULL);

  }

  void testUpdateThreadAction(){
    setFrequency(0x600000000000ULL, 100.00E9, 0);

    frequencyCommandQueue_m.clear();

    /* Because Fringe Tracking is not enabled we expect nothing */
    updateThreadAction();
    checkNumberOfFrequencyCommands(0);

    enableFringeTracking(true);
    frequencyCommandQueue_m.clear();

    /* Still should have no commands because the delays are invalid */
    updateThreadAction();
    checkNumberOfFrequencyCommands(0);

    /* Now give it a valid delay (10 seconds) */
    {
      ACS::Time now = ::getTimeStamp();
      now -= now % TETimeUtil::TE_PERIOD_ACS;

      insertDelayTable(createDelayTable(now - 100000000LL,
                                        now + 100000000LL, 0));
    }
    frequencyCommandQueue_m.clear();



    ACS::Time commandToCheck = nextFrequencyCommandTime_m;
    for (int loop = 0; loop < 5; loop++) {
      /* Sleep a random amount of time between 1 and 5 seconds */
      sleep(static_cast<int>((rand()*5.0)/RAND_MAX));

      ACS::Time beforeUpdate = nextFrequencyCommandTime_m;
      updateThreadAction();
      ACS::Time afterUpdate = nextFrequencyCommandTime_m;

      /* Check that the correct number of command were sent */
      int numCommand = (afterUpdate - beforeUpdate)/480000;
      checkNumberOfFrequencyCommands(numCommand);

      /* Now check that all of the commands are at the correct time */
      for (int idx = 0; idx < numCommand; idx++) {
        checkFrequencyCommand(idx, commandToCheck, 0x600000000000ULL);
        commandToCheck += 480000;
      }

      /* Check that the final time is approximatly correct */
      ACS::Time now = ::getTimeStamp();
      now -= now % TETimeUtil::TE_PERIOD_ACS;
      long long updateLookAhead = nextFrequencyCommandTime_m - now - 20000000;
      CPPUNIT_ASSERT_MESSAGE("Update did not look far enough ahead",
                             abs(updateLookAhead) < 50000000L);
      frequencyCommandQueue_m.clear();
    }
  }

  CPPUNIT_TEST_SUITE(FTSEngineTest);
  CPPUNIT_TEST(testInitialCondition);
  CPPUNIT_TEST(testBasicFrequency);
  CPPUNIT_TEST(testFrequencySetting);
  CPPUNIT_TEST(testFrequencyQueue);
  CPPUNIT_TEST(testFrequencyOffsetting);
  CPPUNIT_TEST(testUpdateFrequencyWithoutFringeTracking);
  CPPUNIT_TEST(testReturnToPhase);
  CPPUNIT_TEST(testOnekHzFringeRate);
  CPPUNIT_TEST(testNonZeroDelay);
  CPPUNIT_TEST(testFringeTracking);
  CPPUNIT_TEST(testFastFringeTracking);
  CPPUNIT_TEST(testFastFringeTracking2);
  CPPUNIT_TEST(testFastFringeTracking3USB);
  CPPUNIT_TEST(testFastFringeTracking3LSB);
  CPPUNIT_TEST(testFastFringeTracking4);
  CPPUNIT_TEST(testPropagationDelay);
  CPPUNIT_TEST(testPhaseSwitching);
  CPPUNIT_TEST(testReset);
  CPPUNIT_TEST(testUpdateThreadAction);
  CPPUNIT_TEST_SUITE_END();

  /* ---------------- Helper Methods ---------------- */
private:
  typedef struct {
    float value1;
    float value2;
    float value3;
    float value4;
  } phaseCommand_t;

  std::deque< phaseCommand_t > phaseCommandQueue_m;

  void sendPhaseValuesCommand(std::vector<float>& phases) {
    phaseCommand_t newCommand;
    newCommand.value1 = phases[0];
    newCommand.value2 = phases[1];
    newCommand.value3 = phases[2];
    newCommand.value4 = phases[3];
    phaseCommandQueue_m.push_back(newCommand);
  }

  void checkNumberOfPhaseCommands(unsigned long commandQueueLength){
    if (phaseCommandQueue_m.size() != commandQueueLength){
      ostringstream msg;
      msg << "Length of command queue (" << phaseCommandQueue_m.size()
          << ") does not match expected value " << commandQueueLength;
      CPPUNIT_FAIL(msg.str());
    }
  }

  void checkPhaseCommand(unsigned long idx, float value1, float value2,
                         float value3, float value4) {

    if (fabs(phaseCommandQueue_m[idx].value1 - value1) > 1E-17) {
      ostringstream msg;
      msg << "Phase Value 1 (" << phaseCommandQueue_m[idx].value1
          << ") does not match expected value " << value1;
      CPPUNIT_FAIL(msg.str());
    }
    if (fabs(phaseCommandQueue_m[idx].value2 - value2) > 1E-17) {
      ostringstream msg;
      msg << "Phase Value 2 (" << phaseCommandQueue_m[idx].value2
          << ") does not match expected value " << value2;
      CPPUNIT_FAIL(msg.str());
    }
    if (fabs(phaseCommandQueue_m[idx].value3 - value3)  > 1E-17) {
      ostringstream msg;
      msg << "Phase Value 3 (" << phaseCommandQueue_m[idx].value3
          << ") does not match expected value " << value3;
      CPPUNIT_FAIL(msg.str());
    }
    if (fabs(phaseCommandQueue_m[idx].value4 - value4) > 1E-17) {
      ostringstream msg;
      msg << "Phase Value 4 (" << phaseCommandQueue_m[idx].value4
          << ") does not match expected value " << value4;
      CPPUNIT_FAIL(msg.str());
    }
  }

  void checkFrequencyResponse(FTSEngine::FrequencySpecification freqResp,
                              unsigned long long nominalFTS,
                              double nominalLo,
                              ACS::Time applicationTime) {

    if (freqResp.ftsFrequency  != nominalFTS) {
      ostringstream msg;
      msg << "FTSFrequency (0x" << std::hex << freqResp.ftsFrequency
          << ") does not match expected value 0x" << nominalFTS;
      CPPUNIT_FAIL(msg.str());
    }

    if (fabs(freqResp.loFrequency - nominalLo) > 1E-14) {
      ostringstream msg;
      msg << "LO Frequency (" << freqResp.loFrequency
          << ") does not match expected value " << nominalLo;
      CPPUNIT_FAIL(msg.str());
    }

    if (freqResp.applicationTime != applicationTime) {
      ostringstream msg;
      msg << "Application Time (0x" << std::hex << freqResp.applicationTime
          << ") does not match expected value " << applicationTime;
      CPPUNIT_FAIL(msg.str());
    }
  }

  void queueFrequencyCommand(unsigned long long& ftsFrequency,
                             ACS::Time& applicationTime) {
    FrequencyCommand newFreqCommand;
    newFreqCommand.ftsFrequency = ftsFrequency;
    newFreqCommand.applicationTime = applicationTime;

    frequencyCommandQueue_m.push_back(newFreqCommand);
  }

  typedef struct {
    unsigned long long ftsFrequency;
    ACS::Time          applicationTime;
  } FrequencyCommand;

  std::deque<FrequencyCommand> frequencyCommandQueue_m;

  void checkNumberOfFrequencyCommands(unsigned long commandQueueLength){
    if (frequencyCommandQueue_m.size() != commandQueueLength){
      ostringstream msg;
      msg << "Length of command queue (" << frequencyCommandQueue_m.size()
          << ") does not match expected value " << commandQueueLength;
      CPPUNIT_FAIL(msg.str());
    }
  }

  void checkFrequencyCommand(unsigned int idx,
                             ACS::Time applicationTime,
                             unsigned long long ftsFrequency) {
    if (frequencyCommandQueue_m[idx].applicationTime != applicationTime){
      ostringstream msg;
      msg << "Application time of command " << idx << " ("
          << frequencyCommandQueue_m[idx].applicationTime
          << ") does not match expected value: " <<  applicationTime;
      CPPUNIT_FAIL(msg.str());
    }

    if (frequencyCommandQueue_m[idx].ftsFrequency != ftsFrequency){
      ostringstream msg;
      msg << "Frequency of command " << idx << " (0x"  << std::hex
          << frequencyCommandQueue_m[idx].ftsFrequency
          << ") does not match expected value: 0x" <<  ftsFrequency;
      CPPUNIT_FAIL(msg.str());
    }
  }

  Control::DelayTable createDelayTable(ACS::Time startTime,
                                       ACS::Time stopTime,
                                       double    delayValue) {
    Control::DelayTable newDelayTable;
    newDelayTable.startTime = startTime;
    newDelayTable.stopTime  = stopTime;
    newDelayTable.delayValues.length(2);
    newDelayTable.delayValues[0].time = startTime;
    newDelayTable.delayValues[0].totalDelay = delayValue;
    newDelayTable.delayValues[1].time = stopTime;
    newDelayTable.delayValues[1].totalDelay = delayValue;

    return newDelayTable;
  }

  const ACS::Time getUpdateThreadCycleTime(){
    ACS::Time threadCycleTime = 10000000;
    return threadCycleTime;
  }


  std::deque<ACS::Time> flushTimes_m;
  void flushFrequencyCommands(ACS::Time& newFlushTime) {
    flushTimes_m.push_back(newFlushTime);
  }

  std::deque<ACS::Time> resetFTSTime_m;
  void resetFTS() {
    ACS::Time now = ::getTimeStamp();
    resetFTSTime_m.push_back(now);

  }

  void setErrorState(int errorCode, bool state) {
  }

}; // End of Test Class

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(FTSEngineTest::suite());
  runner.run();
  return 0;
}
