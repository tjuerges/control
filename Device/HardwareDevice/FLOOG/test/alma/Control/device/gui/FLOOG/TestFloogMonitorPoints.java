/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/**
 * Test for checking the FLOOG monitor points. This test extends TestMonitorPoints to provide
 * for testing. 
 *
 * @author  David Hunter    dhunter@nrao.edu
 * @version 
 * @since 
 */


package alma.Control.device.gui.FLOOG;

import alma.Control.FLOOGCompSimBase;
import alma.Control.HardwareDevice;
import alma.Control.device.gui.common.TestMonitorPoints;
import alma.acs.container.ContainerServices;


public class TestFloogMonitorPoints extends TestMonitorPoints {

    /**
     * The constructor. Just call super...
     * For some reason the ACS component client test case constructor throws exceptions, so we have to
     * pass them along because Java won't let us handle them!
     * @param name
     * @throws Exception
     */
    public TestFloogMonitorPoints(String name) throws Exception {
        super(name);
    }

    /**
     * Set things up for the test!
     *      -Tell the parent class to setup
     *      -Get the FLOOG component.
     *      
     * Setup must be called before running any tests!
     *
     * Precondition: There MUST be a simulated FLOOG up and running before calling setup. This function
     * will fail if there is no FLOOG running.
     */
    protected void setUp() throws Exception {
        super.setUp();
        FLOOGCompSimBase floogRef=null;
        if (null != componentName) {
            ContainerServices c = getContainerServices();
            assertNotNull(c);
            org.omg.CORBA.Object compObj=c.getComponent(componentName);
            assertNotNull(compObj);
            floogRef = (FLOOGCompSimBase) narrowToSimObject(compObj);
            super.setUp(floogRef, componentName, TestDataFileLocation);
        }
        else
            fail("Error: componentName is null!");
    }
    
    /** Narrows a corba FLOOG object into a FLOOGCompSimBase.
     * Based off of FLOOG.narrow, which only works on FLOOGes.
     * @param obj
     * @return
     */
    public HardwareDevice narrowToSimObject(final org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        try
        {
            return (alma.Control.FLOOGCompSimBase)obj;
        }
        catch (ClassCastException c)
        {
            if (obj._is_a("IDL:alma/Control/FLOOG:1.0"))
            {
                alma.Control._FLOOGCompSimBaseStub stub;
                stub = new alma.Control._FLOOGCompSimBaseStub();
                stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
                return stub;
            }
        }
        throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
    }
    private String TestDataFileLocation = "FLOOGMonitorTestData.txt";
    private String componentName = "CONTROL/DA41/FLOOG";
}
