#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2009-04-24  created
#

"""
  This test both tests the functionality of the FLOOG device and
  ensures that the CCL is functioning correctly.
"""


import unittest
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.TimeHelper import getTimeStamp

from CCL.FLOOG import FLOOG
import Control
import ControlDataInterfaces_idl
import time
import TETimeUtil
from math import pi
from PyDataModelEnumeration import PyNetSideband

from Control import AntennaDelayEvent
from Control import CoarseDelayValue


class FloogTestCase(unittest.TestCase):

    def setUp(self):
        self.client = PySimpleClient()
        self.ref = self.client.getComponent("CONTROL/DA41/FLOOG")
        self.floog = FLOOG("DA41")

    def tearDown(self):
        self.client.releaseComponent("CONTROL/DA41/FLOOG")
        
    def testComponentLifecycle(self):
        self.assertEqual(self.ref._get_name(),'CONTROL/DA41/FLOOG')
        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Start)

    def testHwConfigure(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Configure)
        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Configure)

    def testHwInitialize(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Initialize)

        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Initialize)
        
    def testHwOperational(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Operational)
        # Since the requency command is asynchronous and about
        # a quarter second in the future wait 0.5 sec before
        # testing.
        time.sleep(0.5)

        # Test the initial state of the sytem here
        self.assertTrue(abs(31.25E6 - self.ref.GET_FREQ()[0]) < 1E-6)
        self.assertAlmostEqual(self.floog.GET_PHASE_OFFSET()[0], 0.012, 8)

        
    def testFrequencySetting(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        testFreqList = [20.0E6, 20.00000001E6, 31.5E6, 40E6]

        for testFreq in testFreqList:
            self.floog.SetUserFrequency(100E9,testFreq, 0)
            time.sleep(0.5)
            self.assertTrue(abs(testFreq - self.ref.GET_FREQ()[0]) < 1E-6)
            self.assertTrue(abs(testFreq - self.floog.GetNominalFrequency())
                            < 1E-6)

    def testSettingSidebands(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        self.assertEqual(self.floog.GetFTSSideband(),PyNetSideband.USB)
                         
        self.floog.SetFTSSideband("LSB")
        self.assertEqual(self.floog.GetFTSSideband(),PyNetSideband.LSB)

        self.floog.SetFTSSideband("USB")
        self.assertEqual(self.floog.GetFTSSideband(),PyNetSideband.USB)   
        
    def testFrequencyOffsetting(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        self.floog.SetFrequencyOffsetFactor(1)
        time.sleep(0.5)
        self.assertEqual(self.floog.GetFrequencyOffsetFactor(), 1)
        self.assertTrue((self.floog.GetOffsetFTSFrequency() - 31.25E6) < 1E-6)
        self.assertTrue(abs(31.25E6 - self.ref.GET_FREQ()[0]) < 1E-6)

        # Now turn on the offsetting
        import ControlDataInterfaces_idl
        self.floog.setFrequencyOffsetting(Control.LOOffsettingMode_None)
        time.sleep(0.5)
        self.assertEqual(self.floog.getFrequencyOffsettingMode(),
            Control.LOOffsettingMode_None)
        self.assertTrue(abs(self.floog.GetOffsetFTSFrequency() -
                            31280517.578125) < 1E-6)
        self.assertTrue(abs(31280517.578125 - self.ref.GET_FREQ()[0]) < 1E-6)

        # Check that the sideband affects it properly
        self.floog.SetFTSSideband("LSB")
        time.sleep(0.5)
        self.assertTrue(abs(self.floog.GetOffsetFTSFrequency() -
                            31219482.421875) < 1E-6)

        # and off again
        self.floog.setFrequencyOffsettingMode(Control.LOOffsettingMode_None)
        time.sleep(0.5)
        self.assertEqual(self.floog.getFrequencyOffsettingMode(), Control.LOOffsettingMode_None)
        self.assertTrue((self.floog.GetOffsetFTSFrequency() - 31.25E6) < 1E-6)
        self.assertTrue(abs(31.25E6 - self.ref.GET_FREQ()[0]) < 1E-6)

    def testPhaseValues(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        self.floog.SET_PHASE_VALS([0.0,0.0,0.0,0.0])
        self._CheckPhases([0.0,0.0,0.0,0.0],self.floog.GET_PHASE_VALS()[0])
 
        self.floog.SET_PHASE_VALS([1.0,2.0,3.0,4.0])
        self._CheckPhases([1.0,2.0,3.0,4.0],self.floog.GET_PHASE_VALS()[0])
               
        self.floog.EnablePhaseSwitching180Deg(True)
        self.assertTrue(self.floog.PhaseSwitching180DegEnabled())
        self.assertFalse(self.floog.PhaseSwitching90DegEnabled())
        self._CheckPhases([0.0, pi, 0.0, pi], self.floog.GET_PHASE_VALS()[0])

        self.floog.EnablePhaseSwitching90Deg(True)
        self.assertTrue(self.floog.PhaseSwitching180DegEnabled())
        self.assertTrue(self.floog.PhaseSwitching90DegEnabled())
        self._CheckPhases([0.0, pi, pi*0.5, pi*1.5],
                          self.floog.GET_PHASE_VALS()[0])
        
        self.floog.EnablePhaseSwitching180Deg(False)
        self.assertFalse(self.floog.PhaseSwitching180DegEnabled())
        self.assertTrue(self.floog.PhaseSwitching90DegEnabled())
        self._CheckPhases([0.0, 0.0, pi*0.5, pi*0.5],
                          self.floog.GET_PHASE_VALS()[0])
        
        self.floog.EnablePhaseSwitching90Deg(False)
        self.assertFalse(self.floog.PhaseSwitching180DegEnabled())
        self.assertFalse(self.floog.PhaseSwitching90DegEnabled())
        self._CheckPhases([0.0, 0.0, 0.0, 0.0],
                          self.floog.GET_PHASE_VALS()[0])

    def testWalshFunctions(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        self.assertEqual([0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
                         self.floog.GET_PHASE_SEQ1()[0]);
        self.assertEqual([0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
                         self.floog.GET_PHASE_SEQ2()[0]);

        self.floog.SetWalshFunction(15)
        self.assertEqual(self.floog.GetWalshFunction(),15)
        self.assertEqual([0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00],
                         self.floog.GET_PHASE_SEQ1()[0]);
        self.assertEqual([0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00],
                         self.floog.GET_PHASE_SEQ2()[0]);

        self.floog.SetWalshFunction(126)
        self.assertEqual(self.floog.GetWalshFunction(),126)
        self.assertEqual([0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA],
                         self.floog.GET_PHASE_SEQ1()[0]);
        self.assertEqual([0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55],
                         self.floog.GET_PHASE_SEQ2()[0]);

    def testPropogationDelay(self):
        self.assertEqual(self.floog.GetPropagationDelay(),
                         0.0);
        self.floog.SetPropagationDelay(1E-6)
        self.assertEqual(self.floog.GetPropagationDelay(),
                         1E-6);

    def testFringeTracking(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        self.floog.SetUserFrequency(100E9, 32E6)
        self.assertFalse(self.floog.FringeTrackingEnabled())

        self.ref.newDelayEvent(self._CreateDelayRampEvent())

        self.floog.EnableFringeTracking(True)
        self.assertTrue(self.floog.FringeTrackingEnabled())
        time.sleep(0.5) #Allow time for the reset
        self.assertAlmostEqual(self.floog.GET_FREQ()[0],32E6 + 0.5, 5)

        self.floog.SetFTSSideband("LSB")
        time.sleep(0.5) #Allow time for the reset
        self.assertAlmostEqual(self.floog.GET_FREQ()[0],32E6 - 0.5, 5)
        
        self.floog.SetColdMultiplier(2)
        time.sleep(0.5) #Allow time for the reset
        self.assertAlmostEqual(self.floog.GET_FREQ()[0],32E6 - 0.25, 5)

        self.floog.EnableFringeTracking(False)
        time.sleep(0.5) #Allow time for the reset
        self.assertFalse(self.floog.FringeTrackingEnabled())
        self.assertAlmostEqual(self.floog.GET_FREQ()[0],32E6, 5)

    def testSettingFrequencyInTheFuture(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        self.floog.SetUserFrequency(100E9, 32E6)
        cmdTime =TETimeUtil.plusTE(TETimeUtil.unix2epoch(time.time()),60).value
        self.floog.SetUserFrequency(100E9, 25E6, cmdTime)

        for idx in range(10):
            time.sleep(0.5)
            resp = self.floog.GET_FREQ()
            if resp[1] < cmdTime:
                self.assertAlmostEqual(resp[0],32E6,5)
            else:
                self.assertAlmostEqual(resp[0],25E6,5)


    def testWalshFunctionDelayStatic(self):
        '''
        Test that we can set and retreive the Walsh Function Delay
        '''
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        initialDelay = self.floog.GetWalshFunctionDelay()
        self.assertAlmostEqual(self.floog.GET_PHASE_OFFSET()[0],
                               initialDelay, 8)

        
        self.floog.SetWalshFunctionDelay(initialDelay-0.001)
        self.assertAlmostEqual(self.floog.GetWalshFunctionDelay(),
                               initialDelay-0.001, 8)
        # Allow time for the updateThread to cycle ~0.25 s
        time.sleep(0.5)
        self.assertAlmostEqual(self.floog.GET_PHASE_OFFSET()[0],
                               initialDelay - 0.001, 8)

    def testWalshFunctionDelayDynamic(self):
        '''
        Test that the delay is modified correctly as the Coarse Delay value
        changes.

        The technique here is to send in a delay event then check that
        the proper value is set at the correct time.
        '''
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        startTime = getTimeStamp().value + 20000000 #2 seconds
        endTime   = startTime + 100000000 # 20 seconds
        
        coarseDelays = []
        # First Delay Event is at the start and is 0
        coarseDelays.append(CoarseDelayValue(0, 0))

        # Second Delay Event is at 2 s and is 1000 steps (250 ns)
        coarseDelays.append(CoarseDelayValue(2000, 1000))

        # Third Delay Event is at 4 s and is 1016 steps (expect no change)
        coarseDelays.append(CoarseDelayValue(4000, 1016))

        # Fourth Delay Event is at 6 s and is 1064 steps (16 ns change)
        coarseDelays.append(CoarseDelayValue(6000, 1064))

        # Fourth Delay Event is at 8 s and is 1500 steps (annother 125 ns)
        coarseDelays.append(CoarseDelayValue(8000, 1500))

        antennaDelayEvent = AntennaDelayEvent(startTime, endTime, "DA41",
                                              coarseDelays, [], [])
        self.floog._HardwareDevice__hw.newDelayEvent(antennaDelayEvent)

        timestamp = 0;
        while timestamp < endTime:
            time.sleep(0.5)
            (delayOffset, timestamp) = self.floog.GET_PHASE_OFFSET()
            relativeTime = (timestamp - startTime)/1E7

            if relativeTime < 1.75:
                self.assertAlmostEqual(delayOffset, 0.012, 8)
            elif relativeTime > 2.25 and relativeTime < 5.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 250E-9, 8)
            elif relativeTime > 6.25 and relativeTime < 7.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 264E-9, 8)
            elif relativeTime > 8.25:
                self.assertAlmostEqual(delayOffset,  0.012 - 375E-9, 8)

        # Now change the delayOffset and make sure it changes
        self.floog.SetWalshFunctionDelay(0.013)
        time.sleep(0.5)
        self.assertAlmostEqual(self.floog.GET_PHASE_OFFSET()[0],
                               0.013 - 375E-9, 8)

    def testQueuingOfCoarseDelays(self):
        '''
        This test checks that a second delay event correctly overwrites
        a previous one.
        '''
        self.floog.hwStop()
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        startTime = getTimeStamp().value + 20000000 #2 seconds
        endTime   = startTime + 100000000 # 10 seconds
        
        coarseDelays = []
        # First Delay Event is at the start and is 0
        coarseDelays.append(CoarseDelayValue(0, 0))

        # Second Delay Event is at 2 s and is 3000 steps (750 ns)
        coarseDelays.append(CoarseDelayValue(2000, 3000))        

        # Third Delay Event is at 4 s and is 1000 steps (250 ns)
        coarseDelays.append(CoarseDelayValue(4000, 1000))

        # Fourth Delay Event is at 9 s and is 2000 steps (500 ns)
        coarseDelays.append(CoarseDelayValue(9000, 2000))

        antennaDelayEvent = AntennaDelayEvent(startTime, endTime, "DA41",
                                              coarseDelays, [], [])
        self.floog._HardwareDevice__hw.newDelayEvent(antennaDelayEvent)

        # Second Event starts at StartTime + 4 s
        startTime2 = startTime + 40000000
        endTime2   = startTime + 10000000

        coarseDelays = []
        # First Delay Event is at the start and is 500 steps (125 ns)
        coarseDelays.append(CoarseDelayValue(0, 500))

        # Second Delay is at 3 s and is 1500 steps (375 ns)
        coarseDelays.append(CoarseDelayValue(3000, 1500))

        antennaDelayEvent = AntennaDelayEvent(startTime2, endTime2, "DA41",
                                              coarseDelays, [], [])
        self.floog._HardwareDevice__hw.newDelayEvent(antennaDelayEvent)

        timestamp = 0;
        while timestamp < endTime:
            time.sleep(0.5)
            (delayOffset, timestamp) = self.floog.GET_PHASE_OFFSET()
            relativeTime = (timestamp - startTime)/1E7

            if relativeTime < 1.75:
                self.assertAlmostEqual(delayOffset, 0.012, 8)
            elif relativeTime > 2.25 and relativeTime < 3.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 750E-9, 8)
            elif relativeTime > 4.25 and relativeTime < 6.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 125E-9, 8)
            elif relativeTime > 7.25:
                self.assertAlmostEqual(delayOffset,  0.012 - 375E-9, 8)

       
    def _CheckPhases(self, targetPhases, actualPhases):
        for i in range(len(targetPhases)):
            self.assertAlmostEqual(targetPhases[i], actualPhases[i], 3)

    def _HwLifecycleHelper(self, targetState):

        StateList =[(self.ref.hwStop, Control.HardwareDevice.Stop),
                    (self.ref.hwStart,Control.HardwareDevice.Start),
                    (self.ref.hwConfigure,Control.HardwareDevice.Configure),
                    (self.ref.hwInitialize,Control.HardwareDevice.Initialize),
                    (self.ref.hwOperational,Control.HardwareDevice.Operational)
                     ]

        for state in StateList:
            counter = 0
            while counter < 5 and self.ref.getHwState() != state[1]:
                try:
                    state[0]() # Invoke the function
                    if targetState == state[1]:
                        return
                except Exception, e:
                    counter += 1
                    time.sleep(1)

    def _CreateDelayRampEvent(self):
        # This produces a delay ramp with a slope of -5 ps/s as
        # this should produce a frequency change at 100 GHz of half of
        # a hertz.
        startTime = TETimeUtil.unix2epoch(time.time()).value
        stopTime = startTime + 600000000L # 60 sec

        delayTable = Control.DelayTable \
                     (startTime,
                      stopTime,
                      [Control.TotalDelayValue(startTime,1E-6),
                       Control.TotalDelayValue(stopTime, 1E-6 - 3E-10)]
                      )

        return Control.AntennaDelayEvent(startTime,
                                         stopTime,
                                         "DA41",
                                         [],
                                         [],
                                         [delayTable])
        



if __name__ == '__main__':
    suite = unittest.makeSuite(FloogTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
    

#
# ___oOo___
