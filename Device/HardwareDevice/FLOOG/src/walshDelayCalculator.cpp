//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <walshDelayCalculator.h>
#include <ControlDelayInterfacesC.h>
#include <TETimeUtil.h>
#include <cmath>


WalshDelayCalculator::WalshDelayCalculator(double updateThreshold,
    ACS::Time cycleTime):
    updateThreshold_m(updateThreshold),
    cycleTime_m(cycleTime),
    lastPhaseOffsetCommand_m(0),
    lastDelayValue_m(0),
    phaseOffset_m(12E-3)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

WalshDelayCalculator::~WalshDelayCalculator()
{
}

void WalshDelayCalculator::initialize()
{
    lastPhaseOffsetCommand_m = 0;
    lastDelayValue_m = 0;
}

void WalshDelayCalculator::newDelayEvent(
    const Control::AntennaDelayEvent& event)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Lock the queue here
    ACE_Write_Guard< ACE_RW_Mutex > guard(syncMutex_m);

    // When we are not in operational mode it is possible that we would
    // still be receiving events.  Ensure that this does not grow too much
    // by removing outdated entries (i.e. more than 1 second old)
    const ACS::Time expTime(::getTimeStamp() - cycleTime_m);

    while((coarseDelayQueue_m.empty() == false)
    && (expTime > coarseDelayQueue_m.front().applicationTime))
    {
        coarseDelayQueue_m.pop_front();
    }

    // Start from the back and remove any entries which are after the start
    // time of the new event
    while((coarseDelayQueue_m.empty() == false)
    && (event.startTime <= coarseDelayQueue_m.back().applicationTime))
    {
        coarseDelayQueue_m.pop_back();
    }

    // Now copy the coarse delay events into the deque.  Don't forget that
    // the relative start time is in ms, so convert to ACS::Time
    CoarseDelayStruct_t coarseDelay;
    for(unsigned int idx(0U); idx < event.coarseDelays.length(); ++idx)
    {
        coarseDelay.applicationTime = event.startTime +
            (event.coarseDelays[idx].relativeStartTime * 10000);
        coarseDelay.coarseDelay = event.coarseDelays[idx].coarseDelay;

        coarseDelayQueue_m.push_back(coarseDelay);
    }
}

void WalshDelayCalculator::setPhaseOffset(double newDelayValue)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Lock the deque here
    ACE_Write_Guard< ACE_RW_Mutex > guard(syncMutex_m);
    phaseOffset_m = newDelayValue;
    lastPhaseOffsetCommand_m = 0;
}

double WalshDelayCalculator::getPhaseOffset()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Lock the deque here
    ACE_Read_Guard< ACE_RW_Mutex > guard(syncMutex_m);
    return phaseOffset_m;
}

// This method returns true if the difference between the previous
// command sent and the current command is greater than the threshold
bool WalshDelayCalculator::isUpdateNeeded(double& newOffset)
{
    // Lock the deque here
    ACE_Write_Guard< ACE_RW_Mutex > guard(syncMutex_m);

    if(lastPhaseOffsetCommand_m == 0)
    {
        // Flag that we need to send a command regardless
        newOffset = phaseOffset_m - lastDelayValue_m;
    }
    else
    {
        newOffset = lastPhaseOffsetCommand_m;
    }

    // As long as we are not empty find the command value for all coarse delays
    // before the end of the cycle
    const ACS::Time now(::getTimeStamp());

    while((coarseDelayQueue_m.empty() == false)
    && ((now + cycleTime_m) > coarseDelayQueue_m.front().applicationTime))
    {
        lastDelayValue_m = coarseDelayQueue_m.front().coarseDelay * 250E-12;
        newOffset = phaseOffset_m - lastDelayValue_m;
        coarseDelayQueue_m.pop_front();
    }

    if(std::abs(newOffset - lastPhaseOffsetCommand_m) > updateThreshold_m)
    {
        lastPhaseOffsetCommand_m = newOffset;
        return true;
    }

    return false;
}
