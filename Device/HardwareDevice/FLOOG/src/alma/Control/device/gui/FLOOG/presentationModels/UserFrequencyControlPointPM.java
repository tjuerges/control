/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.FLOOG.presentationModels;

import alma.Control.FLOOGOperations;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IControlPoint;
import alma.Control.device.gui.common.IdlControlPointPresentationModel;
import alma.common.log.ExcLog;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

/**
 * Track a read only property of a hardware device to be displayed in a UI.  Isolate the user interface 
 * from the implementation details of a hardware device in the Control system.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 * 
 */
public class UserFrequencyControlPointPM extends IdlControlPointPresentationModel<Double> {
	
	Double value1;
	Double value2;

	public UserFrequencyControlPointPM(
		Logger logger,
		DevicePresentationModel containingDevicePM,
		IControlPoint controlPoint,
		Class controlPointClass
	) {
		
		super(logger, containingDevicePM, controlPoint, controlPointClass);
		controlPointUpdateMethodName = "SetUserFrequency";
		
		value1 = 0.0;
		value2 = 0.0;
	}

	@Override
	protected void setUpdateMethod() {
        try {
        	this.controlPointUpdateMethod = null;
            this.controlPointUpdateMethod = FLOOGOperations.class.getMethod(
                controlPointUpdateMethodName, new Class[] { controlPointClass, controlPointClass, long.class });
            if (this.controlPointUpdateMethod == null)
                logger.severe(
                    "UserFrequencyControlPointPM.setUpdateMethod() - failed to get controlPointUpdateMethod!"
                );
        } catch (SecurityException e) {
            logger.severe(
                "UserFrequencyControlPointPM.setUpdateMethod() - no access rights for method named: " + 
                controlPointUpdateMethodName +
                ExcLog.details(e)
            );
        } catch (NoSuchMethodException e) {
            logger.severe(
                "UserFrequencyControlPointPM.setUpdateMethod() - no method found named: " + 
                controlPointUpdateMethodName +
                ExcLog.details(e)
            );
        }
        
	}
	
    public void setValue(Double v1, Double v2) {
    	this.value1 = v1;
    	this.value2 = v2;
    	
    	if (null == deviceComponent) { 
    		logger.severe("UserFrequencyControlPointPM.setValue() - deviceComponent == null");
    		return;
    	}
    	if (null == controlPointUpdateMethod) {
    		logger.severe("UserFrequencyControlPointPM.setValue() - controlPointUpdateMethod == null!");
    		return;
    	}

    	try {
    	    this.controlPointUpdateMethod.invoke(deviceComponent, value1, value2, 0L);
		} catch (IllegalArgumentException ex) {
        	logger.severe(
        	    "UserFrequencyControlPointPM.setValue() - illegal arguments for method: " + 
        	    controlPointName);
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
        	logger.severe(
        		"UserFrequencyControlPointPM.setValue() - illegal access to method: " + 
        		controlPointName);
			ex.printStackTrace();
		} catch (InvocationTargetException ex) {
        	logger.severe(
        		"UserFrequencyControlPointPM.setValue() - illegal access to object when calling: " + 
        		controlPointName);
			ex.printStackTrace();
		}
    }
}

//
//O_o
