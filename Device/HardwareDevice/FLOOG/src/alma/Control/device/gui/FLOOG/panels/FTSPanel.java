/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.FLOOG.panels;

import alma.Control.device.gui.FLOOG.IcdControlPoints;
import alma.Control.device.gui.FLOOG.IdlMonitorPoints;
import alma.Control.device.gui.FLOOG.presentationModels.DevicePM;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.DoubleMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

public class FTSPanel extends DevicePanel {
    
    public FTSPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }
    
    protected void buildPanel() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor=GridBagConstraints.CENTER;
        c.gridx=1;
        c.gridy=0;

//        add(new JLabel("Current"), c);
        c.gridy++;
        /* Frequency */
        addLeftLabel("Nominal Frequency", c);
        add(new DoubleMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IdlMonitorPoints.NominalFrequency), true, 3), c);
        
        c.gridy++;
//        addLeftLabel("Inverted Clock Count", c);
        /* Inverted Clock Count */
        //TODO: Fix once the FLOOG spreadsheet is fixed.
//        c.gridy++;
//        addLeftLabel("Non-Inverted Clock Count", c);
        /* Non-inverted Clock Count */
        //TODO: Fix once the FLOOG spreadsheet is fixed.
//        c.gridy++;
        /* Reset FTS Status */
        ControlPointPresentationModel<Boolean> controlPointModel = 
            devicePM.getControlPointPM(IcdControlPoints.RESET_FTS_STATUS);
        //Is there some reason we check this here but nowhere else???
        if (null == controlPointModel)
            logger.severe("SummaryPanel.buildPanel(): failed to get model for RESET_FTS_STATUS");
        add(new BooleanControlPointWidget(logger,
                devicePM.getControlPointPM(IcdControlPoints.RESET_FTS_STATUS),
                "Reset FTS Status", true), c);
        
        
//        c.gridx++;
//        c.gridy=0;
//        add(new JLabel("Control"), c);
        //TODO: What is the Control column? What should be done with it?
        
        //TODO: This is left over from old student work.
        //      Figure out what it is and what to do with is.
        //		add(new FloatControlPointWidget(
        //				logger,
        //				devicePM.getControlPointPM(FLOOGControlPoints.FREQ),
        //				0.0,
        //				1.0,
        //				"Set Frequency"),
        //			c
        //		);
        
        
        

    }
    private static final long serialVersionUID = 1L;
}
