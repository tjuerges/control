/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.FLOOG.presentationModels;

import alma.Control.FLOOGOperations;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IControlPoint;
import alma.Control.device.gui.common.IdlControlPointPresentationModel;
import alma.common.log.ExcLog;

import java.util.logging.Logger;

/**
 * Track a read only property of a hardware device to be displayed in a UI.  Isolate the user interface 
 * from the implementation details of a hardware device in the Control system.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 * 
 */
public class PhaseSwitching180DegControlPointPM extends IdlControlPointPresentationModel<Boolean> {

	public PhaseSwitching180DegControlPointPM(
		Logger logger,
		DevicePresentationModel containingDevicePM,
		IControlPoint controlPoint,
		Class controlPointClass
	) {
		
		super(logger, containingDevicePM, controlPoint, controlPointClass);
		controlPointUpdateMethodName = "EnablePhaseSwitching180Deg";
	}

	@Override
	protected void setUpdateMethod() {
        try {
            this.controlPointUpdateMethod = FLOOGOperations.class.getMethod(
                controlPointUpdateMethodName, new Class[] { controlPointClass });
            if (this.controlPointUpdateMethod == null)
                logger.severe(
                    "PhaseSwitching180DegControlPointPM.setUpdateMethod() - failed to get controlPointUpdateMethod!"
                );
        } catch (SecurityException e) {
            logger.severe(
                "PhaseSwitching180DegControlPointPM.setUpdateMethod() - no access rights for method named: " + 
                controlPointUpdateMethodName +
                ExcLog.details(e)
            );
        } catch (NoSuchMethodException e) {
            logger.severe(
                "PhaseSwitching180DegControlPointPM.setUpdateMethod() - no method found named: " + 
                controlPointUpdateMethodName +
                ExcLog.details(e)
            );
        }
	}
}

//
//O_o
