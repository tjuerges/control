/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.FLOOG.presentationModels;

import alma.Control.FLOOGOperations;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.IdlMonitorPointPresentationModel;
import alma.common.log.ExcLog;

import java.util.Date;
import java.util.logging.Logger;

/**
 * Track a read only property of a hardware device to be displayed in a UI.  Isolate the user interface 
 * from the implementation details of a hardware device in the Control system.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public class PropagationDelayMonitorPointPM extends IdlMonitorPointPresentationModel<Double> {
    
    public PropagationDelayMonitorPointPM(Logger logger, DevicePresentationModel container,
                                          IMonitorPoint monitorPoint)
    {super(logger, container, monitorPoint);}
    
    /**
     * Get the current value from the hardware device property this presentation model represents.
     *Note: for the timeStamp we just return the current local time because we don't have a good way to
     *       get the exact time the error occurred. Should a way be found this function should be updated
     *       to return the exact time.
     * @param timeStamp A place to put the time stamp associated with the data.
     * @return the current value held by the hardware device.
     */
    protected Double getValueFromDevice(Date timeStamp) {
        Double res = 0.0;
        try {
            FLOOGOperations dev = (FLOOGOperations)deviceComponent;
            res = dev.GetPropagationDelay();
            timeStamp.setTime((new Date().getTime()));
        } catch (IllegalArgumentException e) {
            logger.severe("PropagationDelayMonitorPointPM.getValueFromDevice()" +
                          " - illegal arguments for method" + ExcLog.details(e));
        }
        return res;
    }

    @Override
    protected Double getValueFromDevice() {
        return getValueFromDevice(new Date());
    }
}

//
// O_o
