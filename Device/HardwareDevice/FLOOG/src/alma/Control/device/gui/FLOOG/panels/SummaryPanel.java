/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.FLOOG.panels;

import alma.Control.device.gui.FLOOG.IcdMonitorPoints;
import alma.Control.device.gui.FLOOG.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanTextMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Summary data for the FLOOG module.
 * 
 * @author David Hunter      dhunter@nrao.edu
 * @author Scott Rankin      srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {
    
    public SummaryPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        clockStatusPanel = new ClockStatusPanel(logger, dPM);
        buildPanel();
    }
    
    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(
                                BorderFactory.createTitledBorder(devicePM.getDeviceDescription()),
                                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx=100;
        c.weighty=100;
        c.gridx=1;
        c.gridy=0;
        
        c.gridx = 1;
        c.gridy = 0;
        /* Output RF Power Level */
        c.anchor=GridBagConstraints.EAST;
        addLeftLabel("Output RF Power Level", c);
        c.anchor=GridBagConstraints.WEST;
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.OUTPUT_RF_LEVEL),
                Status.GOOD, "Output RF Power Level OK",
                Status.FAILURE, "Output RF Power Level not OK"), c);
        
        c.gridy++;
        /* Clock Status */
        c.anchor=GridBagConstraints.EAST;
        addLeftLabel("Clock Status:", c);
        c.anchor=GridBagConstraints.WEST;
        add(new BooleanTextMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.CLOCK_STATUS), "Inverted", "Non-inverted"), c);
        
        //We want this panel to fill the full height.
        c.anchor=GridBagConstraints.CENTER;
        c.gridheight=100;
        c.gridx++;
        c.gridy=0;
        add(clockStatusPanel, c);
        setVisible(true);
    }
    
    private class ClockStatusPanel extends DevicePanel{
        public ClockStatusPanel(Logger logger, DevicePM dPM){
            super(logger, dPM);
            buildPanel();
        }
        @Override
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Inverted Clock Cycle Status"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor=GridBagConstraints.CENTER;
            c.weightx=100;
            c.weighty=100;
            c.gridx=1;
            c.gridy=0;
            
            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("Too few "), c);
            c.gridy++;
            /* Too few non-inverted clock cycles */
            addLeftLabel("Non-inverted", c);
            add(new BooleanMonitorPointWidget(logger, 
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FEW_NON_INV_CYCLES),
                    Status.GOOD, "Non-inverted clock cycles OK",
                    Status.FAILURE, "Too few non-inverted clock cycles"), c);
            
            c.gridy++;
            addLeftLabel("Inverted", c);
            /* Too few inverted clock cycles */
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FEW_INV_CYCLES),
                    Status.GOOD, "Inverted clock cycles OK",
                    Status.FAILURE, "Too few inverted clock cycles"), c);

            c.gridx +=2;
            c.gridy=0;
            add(new JLabel(" Too many"), c);
            c.gridy++;
            /* Too many non-inverted clock cycles */
            add(new BooleanMonitorPointWidget(logger, 
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MANY_NON_INV_CYCLES),
                    Status.GOOD, "Non-inverted clock cycles OK",
                    Status.FAILURE, "Too many non-inverted clock cycles"), c);

            c.gridy++;
            /* Too many inverted clock cycles */
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.MANY_INV_CYCLES),
                    Status.GOOD, "Inverted clock cycles OK",
                    Status.FAILURE, "Too many inverted clock cycles"), c);
        }
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    };
    
    private ClockStatusPanel clockStatusPanel;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

}

//
// O_o
