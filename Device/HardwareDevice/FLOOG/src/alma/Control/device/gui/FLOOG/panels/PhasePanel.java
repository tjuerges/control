 /* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.FLOOG.panels;

import alma.Control.device.gui.FLOOG.IcdMonitorPoints;
import alma.Control.device.gui.FLOOG.presentationModels.DevicePM;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.JLabel;

public class PhasePanel extends DevicePanel {
	
	public PhasePanel(Logger logger, DevicePM dPM) {
		super(logger, dPM);
		buildPanel();
	}
	
	protected void buildPanel() {
		final String[] columnHeaders = {null, "Current", "Control"};
		
		final String[] rowHeaders = {
				null,
				"Offset",
				"Walsh Function",
				"Phase 1",
				"Phase 2",
				"Phase 3",
				"Phase 4"
		};
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		 
		//Add column headers
		c.gridy = 0;
		for (int i = 0;i < columnHeaders.length;i++) {
			if (columnHeaders[i] != null) {
				c.gridx = i;
				add(new JLabel(columnHeaders[i]), c);
			}
		}
		
		//Add row headers
		c.gridx = 0;
		for (int i = 0;i < rowHeaders.length;i++) {
			if (rowHeaders[i] != null) {
				c.gridy = i;
				add(new JLabel(rowHeaders[i]), c);
			}
		}
		
		c.gridy = 1;
		c.gridx = 1;
		/* Offset */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.PHASE_OFFSET),
				true),
			c	
		);
		
		/* Walsh Function */
		
		/* Phase 1 */
		
		/* Phase 2 */
		
		/* Phase 3 */
		
		/* Phase 4 */
	}
}