/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 */

package alma.Control.device.gui.FLOOG.presentationModels;

import alma.Control.device.gui.FLOOG.IdlControlPoints;
import alma.Control.device.gui.FLOOG.IdlMonitorPoints;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IdlControlPointPresentationModel;
import alma.Control.device.gui.common.IdlMonitorPointPresentationModel;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Build IDL Presentation models specific to this device
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  David Hunter  dhunter@nrao.edu
 * @since   ALMA 5.1.1
 * 
 */
public class IdlPMBuilder {
    
    private DevicePresentationModel dPM;
    public HashMap<IdlControlPoints, IdlControlPointPresentationModel> controlPMs = null;
    public HashMap<IdlMonitorPoints, IdlMonitorPointPresentationModel> monitorPMs = null;
    Logger logger;
    
    public IdlPMBuilder(Logger logger, DevicePresentationModel containingDevicePM) {
        this.logger = logger;
        this.dPM = containingDevicePM;
        controlPMs = new HashMap<IdlControlPoints, IdlControlPointPresentationModel>();
        monitorPMs = new HashMap<IdlMonitorPoints, IdlMonitorPointPresentationModel>();
    }

    protected void createControlPointPresentationModels() {
        controlPMs.put(IdlControlPoints.ColdMultiplier,
            new ColdMultiplierControlPointPM(logger, dPM, IdlControlPoints.ColdMultiplier, long.class) );
        controlPMs.put(IdlControlPoints.FrequencyOffsetFactor,
            new FrequencyOffsetFactorControlPointPM(logger, dPM, IdlControlPoints.FrequencyOffsetFactor, int.class) );
        controlPMs.put(IdlControlPoints.FringeTracking,
            new FringeTrackingControlPointPM(logger, dPM, IdlControlPoints.FringeTracking, boolean.class) );
        controlPMs.put(IdlControlPoints.FTSOnUpperSideband,
            new FTSOnUpperSidebandControlPointPM(logger, dPM, IdlControlPoints.FTSOnUpperSideband, boolean.class) );
        controlPMs.put(IdlControlPoints.PhaseSwitching180Deg,
            new PhaseSwitching180DegControlPointPM(logger, dPM, IdlControlPoints.PhaseSwitching180Deg, boolean.class) );
        controlPMs.put(IdlControlPoints.PhaseSwitching90Deg,
            new PhaseSwitching90DegControlPointPM(logger, dPM, IdlControlPoints.PhaseSwitching90Deg, boolean.class));
        controlPMs.put(IdlControlPoints.PropagationDelay,
            new PropagationDelayControlPointPM(logger, dPM, IdlControlPoints.PropagationDelay, double.class));
        controlPMs.put(IdlControlPoints.UserFrequency,
            new UserFrequencyControlPointPM(logger, dPM, IdlControlPoints.UserFrequency, double.class) );
        controlPMs.put(IdlControlPoints.WalshFunction,
            new WalshFunctionControlPointPM(logger, dPM, IdlControlPoints.WalshFunction, int.class));
    }

    protected void createMonitorPointPresentationModels() {
        monitorPMs.put(IdlMonitorPoints.ColdMultiplier,
            new ColdMultiplierMonitorPointPM(logger, dPM, IdlMonitorPoints.ColdMultiplier));
        monitorPMs.put(IdlMonitorPoints.FrequencyOffsetFactor,
            new FrequencyOffsetFactorMonitorPointPM(logger, dPM, IdlMonitorPoints.FrequencyOffsetFactor));
        monitorPMs.put(IdlMonitorPoints.FringeTracking,
                new FringeTrackingMonitorPointPM(logger, dPM, IdlMonitorPoints.FringeTracking));
        monitorPMs.put(IdlMonitorPoints.FTSOnUpperSideband,
            new FTSOnUpperSidebandMonitorPointPM(logger, dPM, IdlMonitorPoints.FTSOnUpperSideband));
        monitorPMs.put(IdlMonitorPoints.NominalFrequency,
            new NominalFrequencyMonitorPointPM(logger, dPM, IdlMonitorPoints.NominalFrequency));
        monitorPMs.put(IdlMonitorPoints.PhaseSwitching180Deg,
            new PhaseSwitching180DegMonitorPointPM(logger, dPM, IdlMonitorPoints.PhaseSwitching180Deg));
        monitorPMs.put(IdlMonitorPoints.PhaseSwitching90Deg,
            new PhaseSwitching90DegMonitorPointPM(logger, dPM, IdlMonitorPoints.PhaseSwitching90Deg));
        monitorPMs.put(IdlMonitorPoints.PropagationDelay,
            new PropagationDelayMonitorPointPM(logger, dPM, IdlMonitorPoints.PropagationDelay));
        monitorPMs.put(IdlMonitorPoints.WalshFunction,
            new WalshFunctionMonitorPointPM(logger, dPM, IdlMonitorPoints.WalshFunction));
        monitorPMs.put(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE,
            new GET_AMBIENT_TEMPERATURE_MonitorPointPM(logger, dPM, IdlMonitorPoints.GET_AMBIENT_TEMPERATURE));
    }
}
//
// O_o
