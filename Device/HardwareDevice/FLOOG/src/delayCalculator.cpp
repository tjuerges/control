//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <delayCalculator.h>
#include <TETimeUtil.h>
#include <Guard_T.h>


DelayCalculator::DelayCalculator():
    interpolatorTable_m(delayList_m.end()),
    interpolator_p(0),
    interpolatorTime_p(0),
    interpolatorValue_p(0)
{
}

DelayCalculator::~DelayCalculator()
{
    deleteInterpolator();
}

void DelayCalculator::setDelays(const Control::DelayTableSeq& newTables)
{
    removeExpiredTables();

    for(unsigned int idx(0); idx < newTables.length(); ++idx)
    {
        insertDelayTable(newTables[idx]);
    }
}

void DelayCalculator::insertDelayTable(const Control::DelayTable& newTable)
{
    std::list< Control::DelayTable >::iterator newEntry;
    std::list< Control::DelayTable >::iterator previousEntry;

    ACE_Write_Guard< ACE_RW_Mutex > guard(delayListMutex_m);

    // We want to search through the list for the first entry in the list
    // which does not start before the new table
    std::list< Control::DelayTable >::iterator iter(delayList_m.begin());
    while((iter != delayList_m.end())
    && (newTable.startTime > (*iter).startTime))
    {
        ++iter;
    };

    // Insert our new table in the list at that point
    newEntry = delayList_m.insert(iter, newTable);

    // Unless this is the first element in the list, check the previous
    // element to ensure that it ends before this one begins.  If not
    // then correct it
    if(newEntry != delayList_m.begin())
    {
        previousEntry = newEntry;
        --previousEntry;

        if((*newEntry).startTime < (*previousEntry).stopTime)
        {
            (*previousEntry).stopTime = (*newEntry).startTime;
        }
    }

    // Now we want to go forward from the newEntry and fix any entries
    // after this one:
    // * If the stop time of the next entry is before the stop time
    // of the new one remove it completely.
    // * If the start time of the next entry is before the stop time
    // of the new one adjust it
    while((iter != delayList_m.end())
    && ((*newEntry).stopTime > (*iter).startTime))
    {
        if((*newEntry).stopTime >= (*iter).stopTime)
        {
            if(iter == interpolatorTable_m)
            {
                interpolatorTable_m = delayList_m.end();
            }

            iter = delayList_m.erase(iter);
        }
        else
        {
            (*iter).startTime = (*newEntry).stopTime;
        }
    }
}

void DelayCalculator::removeExpiredTables()
{
    ACE_Write_Guard< ACE_RW_Mutex > guard(delayListMutex_m);

    std::list< Control::DelayTable >::iterator iter(delayList_m.begin());
    const ACS::Time now(TETimeUtil::floorTE(::getTimeStamp()));

    while((iter != delayList_m.end())
    && ((*iter).stopTime <= now))
    {
        if(iter == interpolatorTable_m)
        {
            interpolatorTable_m = delayList_m.end();
        }

        iter = delayList_m.erase(iter);
    }
}

bool DelayCalculator::checkInterpolatorTable(ACS::Time requestTime)
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(delayListMutex_m);

    if(interpolatorTable_m == delayList_m.end())
    {
        return false;
    }

    return (((*interpolatorTable_m).startTime <= requestTime)
        && ((*interpolatorTable_m).stopTime > requestTime));
}

double DelayCalculator::getDelay(ACS::Time requestTime)
{
    // Ensure we have the correct interpolator for the time
    // this will thow a NoValidDelayExImpl if there are
    // no valid delay events
    ACE_Guard< ACE_Mutex > interpolatorGuard(interpolatorMutex);
    getInterpolator(requestTime);

    // Now use the interpolator
    ACE_Read_Guard< ACE_RW_Mutex > guard(delayListMutex_m);
    return (*interpolator_p).getDelay(requestTime);
}

void DelayCalculator::getInterpolator(ACS::Time requestTime)
{
    if(checkInterpolatorTable(requestTime))
    {
        // We already have the correct one just return
        return;
    }

    // Remove the old Interpolator
    deleteInterpolator();

    // Now find the new intepolator
    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(delayListMutex_m);

        interpolatorTable_m = delayList_m.begin();

        while((interpolatorTable_m != delayList_m.end())
        && ((*interpolatorTable_m).stopTime <= requestTime))
        {
            ++interpolatorTable_m;
        }

        if((interpolatorTable_m == delayList_m.end())
        || ((*interpolatorTable_m).startTime > requestTime))
        {
            interpolatorTable_m = delayList_m.end();
            throw FTSExceptions::NoValidDelayExImpl(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
    }

    createInterpolator();
}

void DelayCalculator::createInterpolator()
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(delayListMutex_m);

    const unsigned int delayValuesSize(
        (*interpolatorTable_m).delayValues.length());

    interpolatorTime_p = new ACS::Time[delayValuesSize];
    interpolatorValue_p = new double[delayValuesSize];

    for(unsigned int idx(0U); idx < delayValuesSize; ++idx)
    {
        interpolatorTime_p[idx] = (*interpolatorTable_m).delayValues[idx].time;
        interpolatorValue_p[idx] =
            (*interpolatorTable_m).delayValues[idx].totalDelay;
    }

    interpolator_p = new PolynomialInterpolator(delayValuesSize - 1,
        interpolatorTime_p, interpolatorValue_p);
}

void DelayCalculator::deleteInterpolator()
{
    // Remove the old interpolator
    delete interpolator_p;
    interpolator_p = 0;

    delete[] interpolatorTime_p;
    interpolatorTime_p = 0;

    delete[] interpolatorValue_p;
    interpolatorValue_p = 0;
}
