#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a FLOOG device.
"""

import CCL.FLOOGBase
import FTSDevice
from CCL import StatusHelper
from CCL.EnumerationHelper import getEnumeration
from PyDataModelEnumeration import PyNetSideband
import CCL.SIConverter

class FLOOG(CCL.FLOOGBase.FLOOGBase,
            FTSDevice.FTSDevice):
    '''
    The FLOOG class inherits from the code generated FLOOGBase
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        This method will display the status information of the device at
        the standard output.
        '''
        CCL.FLOOGBase.FLOOGBase.__init__(self, antennaName, componentName,
                                     stickyFlag)

    def __del__(self):
        CCL.FLOOGBase.FLOOGBase.__del__(self)

    def SetUserFrequency(self, LOFrequency, FTSFrequency, epoch = 0):
        """
        Method to set the frequency of the FLOOG, this is the prefered
        method which will enable proper fringe tracking

        Parameters:
           LOFrequency, the total frequency of the First LO
           FTSFrequency, the frequency of the FTS (offset)
           Epoch the time at which this new frequency becomes effective
                 in ACS::Time units.  A value of zero implies ASAP behavior
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              SetUserFrequency(LOFrequency, FTSFrequency,epoch)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def SetFTSSideband(self, sideband):
        '''
        This method allows you to set if the FTS is locked above or below
        the photonic reference.

        Members of the NetSideband enureation USB and LSB are valid
        parameters (Can also be passed in as a string) all other
        options should throw and exception.
        '''
        sideband = getEnumeration(sideband, PyNetSideband)
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              SetFTSSideband(sideband)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GetFTSSideband(self):
        '''
        This method returns the current setting of the sideband lock.

        Members of the NetSideband enureation USB or LSB are returned.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetFTSSideband()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def PhaseSwitching180DegEnabled(self):
        '''
        Method returns the current state of the 180 Degree phase switching
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].PhaseSwitching180DegEnabled()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def PhaseSwitching90DegEnabled(self):
        '''
        Method returns the current state of the 90 Degree phase switching
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].PhaseSwitching90DegEnabled()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def EnablePhaseSwitching180Deg(self, enable):
        '''
        Method to enable / disable the 180-Degree phase switching
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              EnablePhaseSwitching180Deg(enable)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def EnablePhaseSwitching90Deg(self, enable):
        '''
        Method to enable / disable the 90-Degree phase switching
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              EnablePhaseSwitching90Deg(enable)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def SetWalshFunction(self, WALSequenceNumber):
        '''
        Set the Walsh Function switching cycle to the specified sequence
        number.  The valid values are between 0 and 127.

        A reset is automatically performed whenever this function is
        invoked.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              SetWalshFunction(WALSequenceNumber)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GetWalshFunction(self):
        '''
        Return the last Walsh Function
        switching cycle set using the FLOOG interface.

        Note this returns a chached value, not the readback from the
        hardware.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              GetWalshFunction()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def SetWalshFunctionDelay(self, delay):
        '''
        This configures the nominal Walsh function delay for this FLOOG
        module.  That is the delay when no delay is present in the correlator.
        The value is specified in seconds although strings (i.e. 16 us) are
        acceptable.
        '''
        delay = CCL.SIConverter.toSeconds(delay)
        for key, val in self._devices.iteritems():
            self._devices[key].SetWalshFunctionDelay(delay)

    def GetWalshFunctionDelay(self):
        '''
        This method returns the nominal Walsh function delay for this DTX
        module.  That is the delay when no delay is present in the correlator.
        The return value is in seconds.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetWalshFunctionDelay()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
       
    def SetColdMultiplier(self, ColdMultiplier):
        '''
        Set the Cold Multiplier for the FLOOG.  This is essentially
        any multiplier that the fringe frequency sees.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              SetColdMultiplier(ColdMultiplier)
        if len(self._devices) == 1:
            return result.values()[0]
        return result
        
    def GetColdMultiplier(self):
        '''
        Return the current value of the ColdMultiplier variable
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              GetColdMultiplier()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    
    def SetOffsetToRejectImageSideband(self, rejectImage):
        '''
        Method to specify which sideband is rejected by LO Offsetting
        usually we want to reject the Image Sideband so this is true.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              SetOffsetToRejectImageSideband(rejectImage)
        if len(self._devices) == 1:
            return result.values()[0]
        return result
   
    def OffsetRejectsImageSideband(self):
        '''
        Method to query if the Image sideband is being rejected by the
        LO offsetting (True) or if the state has been modified to reject
        the Signal sideband
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].OffsetRejectsImageSideband()
        if len(self._devices) == 1:
            return result.values()[0]
        return result


    def STATUS(self):
        '''
        This method will display the status information of the device at
        the standard output.
        '''
        
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
        
            try:
                freq,t = self._devices[key].GET_FREQ()
                lfreq = StatusHelper.Line("FREQ", [StatusHelper.ValueUnit(freq,"Hz")]);
            except:
                lfreq = StatusHelper.Line("FREQ", [StatusHelper.ValueUnit("N/A")]);

            try:
                phaseVals,t = self._devices[key].GET_PHASE_VALS()
                phaseValsStr = ""
                for i in range(0, len(phaseVals)):
                    phaseValsStr += str(phaseVals[i]) + "  "
                ######### values need to be converted accordinf to the ICD #########
                lPhaseVals = StatusHelper.Line("PHASE_VALS", [StatusHelper.ValueUnit(phaseValsStr,"Deg")]);
            except:
                lPhaseVals = StatusHelper.Line("PHASE_VALS", [StatusHelper.ValueUnit("N/A")]);
       
            try:
                phaseOffset,t = self._devices[key].GET_PHASE_OFFSET()
                ######### values need to be converted accordinf to the ICD #########
                lPhaseOffset = StatusHelper.Line("PHASE_OFFSET", [StatusHelper.ValueUnit(phaseOffset*1000,"ms")]);
            except:
                lPhaseOffset = StatusHelper.Line("PHASE_OFFSET", [StatusHelper.ValueUnit("N/A")]);

            try:
                bStatus,t = self._devices[key].GET_FTS_STATUS()
                # non-inverted case
                if bStatus[5] == 0:
                    lClockStat = StatusHelper.Line("CLOCK_STATUS", [StatusHelper.ValueUnit("non-inverted")]);
                    if bStatus[1]==1:
                        lClockCycles = StatusHelper.Line("CLOCK_CYCLES",[StatusHelper.ValueUnit("BAD (Too few non-inverted clock cycles)")]);
                    if bStatus[2]==1:
                        lClockCycles = StatusHelper.Line("CLOCK_CYCLES",[StatusHelper.ValueUnit("BAD (Too many non-inverted clock cycles)")]);
                    else:
                        lClockCycles = StatusHelper.Line("CLOCK_CYCLES",[StatusHelper.ValueUnit("ok")]);
                #inverted case
                if bStatus[5] == 1:
                    lClockStat = StatusHelper.Line("CLOCK_STATUS", [StatusHelper.ValueUnit("inverted")]);
                    if bStatus[3]==1 :
                        lClockCycles = StatusHelper.Line("CLOCK_CYCLES",[StatusHelper.ValueUnit("BAD (Too few inverted clock cycles)")]);
                    if bStatus[4]==1 :
                        lClockCycles = StatusHelper.Line("CLOCK_CYCLES",[StatusHelper.ValueUnit("BAD (Too many inverted clock cycles)")]);
                    else:
                        lClockCycles = StatusHelper.Line("CLOCK_CYCLES",[StatusHelper.ValueUnit("ok")]);                  
            except:
                lClockStat = StatusHelper.Line("CLOCK_STATUS", [StatusHelper.ValueUnit("N/A")]);
                lClockCycles = StatusHelper.Line("TE-",[StatusHelper.ValueUnit("N/A")]);
            
            frame = StatusHelper.Frame(compName + "   Ant: " + antName,[lfreq, lPhaseVals, lPhaseOffset,
                                                                        lClockStat, lClockCycles])
            frame.printScreen()
