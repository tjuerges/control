#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module abstracts the common functions from the FLOOG and Second LO
to make them consistent
"""


class FTSDevice():
    '''
    '''

    def GetNominalFrequency(self):
        '''
        This returns the current nominal frequency of the FTS this
        does not include any fringe tracking of lo offsetting contributions.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetNominalFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def SetFrequencyOffsetFactor(self, offsetFactor=0):
        """
        Method to set the Frequency offset factor, this is how many times
        the quantized value the lo is offset by.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              SetFrequencyOffsetFactor(offsetFactor)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GetFrequencyOffsetFactor(self):
        """
        Method to return the Frequency offset factor, this is how many times
        the quantized value the lo is offset by.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              GetFrequencyOffsetFactor()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GetOffsetFTSFrequency(self):
        """
        Method returns the Current frequency of the FTS with any Offsetting
        applied.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              GetOffsetFTSFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def setFrequencyOffsettingMode(self, mode):
        """
        Method to set the offset mode of the FTS frequency.
        The method takes a Control.LOOffsettingMode argument which can have the
        following values:
        Control.LOOffsettingMode_None
        Control.LOOffsettingMode_SecondLO
        Control.LOOffsettingMode_ThirdLO
        The offset mode values can be imported from ControlDataInterfaces_idl.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              setFrequencyOffsettingMode(mode)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def getFrequencyOffsettingMode(self):
        """
        Method returns the currentfrequency offset mode.  The value will be one
        of
        Control.LOOffsettingMode_None
        Control.LOOffsettingMode_SecondLO
        Control.LOOffsettingMode_ThirdLO
        The offset mode values can be imported from ControlDataInterfaces_idl.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              getFrequencyOffsettingMode()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def SetPropagationDelay(self, PropagationDelay):
        """
        Method to set the value of the propagation delay.

        This value is used in fringe tracking to vary the time at which the
        geometric delays are calculated.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              SetPropagationDelay(PropagationDelay)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def GetPropagationDelay(self):
        """
        Method returns the current value of the propagation delay.

        This value is used in fringe tracking to vary the time at which the
        geometric delays are calculated.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetPropagationDelay()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def EnableFringeTracking(self, enabled):
        """
        Method to enable or disable the fringe tracking in the FTS
        frequency. The method takes a boolean argument.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              EnableFringeTracking(enabled)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def FringeTrackingEnabled(self):
        """
        Method returns the current state of fringe tracking in
        the FTS.
        """
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              FringeTrackingEnabled()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

