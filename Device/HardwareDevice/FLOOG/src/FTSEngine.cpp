//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <FTSEngine.h>
#include <string>
#include <TETimeUtil.h>
#include <FTSDeviceData.h>
#include <ControlExceptions.h>
#include <FLOOGImpl.h>
#include <FLOOGAlarmEnums.h>
#include <ControlDataInterfacesC.h>
#include <almaEnumerations_IFC.h>
#include <Guard_T.h>


FTSEngine::FTSEngine():
    nextFrequencyCommandTime_m(0),
    fringeTrackingEnabled_m(false),
    enableSlowPhaseSwitching_m(false),
    enableFastPhaseSwitching_m(false),
    frequencyOffsettingMode_m(Control::LOOffsettingMode_NONE),
    coldMultiplier_m(1),
    frequencyOffsetFactor_m(0),
    propagationDelay_m(0)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    ftsHardwareModel_m.resetFTS();
    ftsTheoryModel_m.resetFTS();
    setSideband(NetSidebandMod::USB, NetSidebandMod::USB);
}

FTSEngine::~FTSEngine()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void FTSEngine::setFrequency(unsigned long long nominalFTSFrequency,
    double nominalLoFrequency)
{
    ACS::Time now(::getTimeStamp());
    now -= now % TETimeUtil::TE_PERIOD_ACS;
    setFrequency(nominalFTSFrequency, nominalLoFrequency, now);
}

void FTSEngine::setFrequency(unsigned long long nominalFTSFrequency,
    double nominalLoFrequency, ACS::Time applicationTime)
{
    try
    {
        FTSEngine::FrequencySpecification ftsSpec(getNominalFrequencySpec(
            applicationTime));
        if((ftsSpec.ftsFrequency == nominalFTSFrequency)
        && (ftsSpec.loFrequency == nominalLoFrequency))
        {
            // Nothing to do, same frequency
            return;
        }
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        // This just means that no frequency has been defined
        nextFrequencyCommandTime_m = applicationTime;
    }

    FTSEngine::FrequencySpecification newFreqSpec;
    newFreqSpec.ftsFrequency = nominalFTSFrequency;
    newFreqSpec.loFrequency = nominalLoFrequency;
    newFreqSpec.applicationTime = applicationTime;
    queueFTSFrequency(newFreqSpec);

    ACS::Time temp(0ULL);
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        temp = nextFrequencyCommandTime_m;
    }

    if(newFreqSpec.applicationTime <= temp)
    {
        reset();
    }
}

void FTSEngine::cleanQueue()
{
    // This method goes through the queue and will remove all obsolete
    // commands.
    cleanQueue(::getTimeStamp());
}

void FTSEngine::cleanQueue(ACS::Time currentTime)
{
    ACE_Write_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

    if(frequencySpecificationDeque_m.empty() == false)
    {
        unsigned int idx(frequencySpecificationDeque_m.size() - 1);
        while((idx > 0)
        && (frequencySpecificationDeque_m[idx].applicationTime > currentTime))
        {
            --idx;
        }

        // idx now points to the first command, everything before this is junk
        while(idx > 0)
        {
            frequencySpecificationDeque_m.pop_front();
            --idx;
        }
    }
}

void FTSEngine::queueFTSFrequency(FTSEngine::FrequencySpecification ftsSpec)
{
    ACE_Write_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

    while((frequencySpecificationDeque_m.empty() == false)
    && (frequencySpecificationDeque_m.back().applicationTime
        >= ftsSpec.applicationTime))
    {
        frequencySpecificationDeque_m.pop_back();
    }

    frequencySpecificationDeque_m.push_back(ftsSpec);
}

unsigned long long FTSEngine::getNominalFTSFrequency()
{
    return getNominalFrequencySpec(::getTimeStamp()).ftsFrequency;
}

double FTSEngine::getNominalLoFrequency()
{
    return getNominalFrequencySpec(::getTimeStamp()).loFrequency;
}

unsigned long long FTSEngine::getOffsetFTSFrequency()
{
    return getOffsetFrequencySpec(::getTimeStamp()).ftsFrequency;
}

double FTSEngine::getOffsetLoFrequency()
{
    return getOffsetFrequencySpec(::getTimeStamp()).loFrequency;
}

FTSEngine::FrequencySpecification FTSEngine::getNominalFrequencySpec(
    ACS::Time requestedTime)
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

    if(frequencySpecificationDeque_m.empty() == true)
    {
        // No frequency specified throw an exception
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "No frequency specification available!");
        ex.log();
        throw ex;
    }

    unsigned int idx(frequencySpecificationDeque_m.size() - 1);
    while((idx > 0)
    && (frequencySpecificationDeque_m[idx].applicationTime > requestedTime))
    {
        --idx;
    }

    return frequencySpecificationDeque_m[idx];
}

FTSEngine::FrequencySpecification FTSEngine::getOffsetFrequencySpec(
    ACS::Time requestedTime)
{
    FTSEngine::FrequencySpecification freqSpec(
        getNominalFrequencySpec(requestedTime));

    if(frequencyOffsettingMode_m != Control::LOOffsettingMode_NONE)
    {
        freqSpec.loFrequency += (frequencyOffsetFactor_m
            * OffsetFrequencySizeHz);
        long long frequencyOffset(
            static_cast< long long > (frequencyOffsetFactor_m
                * OffsetFrequencySizeCount) / coldMultiplier_m);
        if(getOffsetSideband(requestedTime) == NetSidebandMod::USB)
        {
            if(getFringeSideband(requestedTime) == NetSidebandMod::USB)
            {
                freqSpec.ftsFrequency += frequencyOffset;
            }
            else
            {
                //LOG_TO_OPERATOR(LM_ERROR, "FLOOG in Case B");
                freqSpec.ftsFrequency -= frequencyOffset;
            }
        }
        else
        {
            if(getFringeSideband(requestedTime) == NetSidebandMod::USB)
            {
                //LOG_TO_OPERATOR(LM_ERROR, "FLOOG in Case C");
                freqSpec.ftsFrequency -= frequencyOffset;
            }
            else
            {
                //LOG_TO_OPERATOR(LM_ERROR, "FLOOG in Case D");
                freqSpec.ftsFrequency += frequencyOffset;
            }
        }
    }

    return freqSpec;
}

void FTSEngine::setFrequencyOffsetting(const Control::LOOffsettingMode mode)
{
    if(frequencyOffsettingMode_m == mode)
    {
        return;
    }

    frequencyOffsettingMode_m = mode;
    reset();
}

Control::LOOffsettingMode FTSEngine::getFrequencyOffsettingMode()
{
    return frequencyOffsettingMode_m;
}

void FTSEngine::setFrequencyOffsetFactor(int frequencyOffsetFactor)
{
    if(frequencyOffsetFactor_m == frequencyOffsetFactor)
    {
        return;
    }

    frequencyOffsetFactor_m = frequencyOffsetFactor;
    if(frequencyOffsettingMode_m != Control::LOOffsettingMode_NONE)
    {
        reset();
    }
}

int FTSEngine::getFrequencyOffsetFactor()
{
    return frequencyOffsetFactor_m;
}

long FTSEngine::getColdMultiplier()
{
    return coldMultiplier_m;
}

void FTSEngine::setColdMultiplier(long multiplier)
{
    if(coldMultiplier_m == multiplier)
    {
        return;
    }

    coldMultiplier_m = multiplier;

    reset();
    try
    {
        setPhaseValues();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex_)
    {
        // Ok the system is not ready yet, just go on
    }
}

void FTSEngine::setPropagationDelay(double propagationDelay)
{
    propagationDelay_m = static_cast< ACS::Time >(propagationDelay
        * TETimeUtil::ACS_ONE_SECOND);
}

double FTSEngine::getPropagationDelay()
{
    return (static_cast< double >(propagationDelay_m)
        / TETimeUtil::ACS_ONE_SECOND);
}

void FTSEngine::reset()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Flush all frequency commands and monitors
    ACS::Time now(::getTimeStamp());
    now -= now % TETimeUtil::TE_PERIOD_ACS;
    flushFrequencyCommands(now);

    // reset the hardware device
    ftsHardwareModel_m.resetFTS();
    resetFTS();

    // Set the nextFrequencyCommandTime to now, the updateFrequency
    // method should take care of making sure it is in the future
    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        nextFrequencyCommandTime_m = now;
    }

    updateFrequency(true);
    updateThreadAction();
}

void FTSEngine::setSideband(NetSidebandMod::NetSideband fringeSideband,
    NetSidebandMod::NetSideband offsetSideband)
{
    setSideband(fringeSideband, offsetSideband, ::getTimeStamp());
}

void FTSEngine::setSideband(NetSidebandMod::NetSideband fringeSideband,
    NetSidebandMod::NetSideband offsetSideband,
    ACS::Time applicationTime)
{
    try
    {
        const FTSEngine::SidebandSpecification sidebandSpec(getSidebandSpec(applicationTime));
        if((sidebandSpec.FringeSideband == fringeSideband)
        && (sidebandSpec.OffsetSideband == offsetSideband))
        {
            // Nothing to do already set.
            return;
        }
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        // This is ok, just means nothing specified
    }

    if(((fringeSideband != NetSidebandMod::USB)
        && (fringeSideband != NetSidebandMod::LSB))
    || ((offsetSideband != NetSidebandMod::USB)
        && (offsetSideband != NetSidebandMod::LSB)))
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "The selected sideband is neither USB nor LSB!");
        ex.log();
        throw ex;
    }

    if (fringeSideband == NetSidebandMod::USB)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Commanded FTS to USB Fringe Sideband");
    }
    else
    {
        LOG_TO_OPERATOR(LM_ERROR, "Commanded FTS to LSB Fringe Sideband");
    }

    if(offsetSideband == NetSidebandMod::USB)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Commanded FTS to USB Offset Sideband");
    }
    else
    {
        LOG_TO_OPERATOR(LM_ERROR, "Commanded FTS to LSB Offset Sideband");
    }

    FTSEngine::SidebandSpecification sidebandSpec;
    sidebandSpec.FringeSideband = fringeSideband;
    sidebandSpec.OffsetSideband = offsetSideband;
    sidebandSpec.applicationTime = applicationTime;

    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

        while((sidebandSpecificationDeque_m.empty() == false)
        && (sidebandSpecificationDeque_m.back().applicationTime
            >= sidebandSpec.applicationTime))
        {
            sidebandSpecificationDeque_m.pop_back();
        }

        sidebandSpecificationDeque_m.push_back(sidebandSpec);
    }

    cleanSidebandQueue();

    ACS::Time tmp(0ULL);
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        tmp = nextFrequencyCommandTime_m;
    }

    if(applicationTime < tmp)
    {
        reset();
    }
}

NetSidebandMod::NetSideband FTSEngine::getFringeSideband()
{
    return getFringeSideband(::getTimeStamp());
}

NetSidebandMod::NetSideband FTSEngine::getOffsetSideband()
{
    return getOffsetSideband(::getTimeStamp());
}

NetSidebandMod::NetSideband FTSEngine::getFringeSideband(ACS::Time requestTime)
{
    return getSidebandSpec(requestTime).FringeSideband;
}

NetSidebandMod::NetSideband FTSEngine::getOffsetSideband(ACS::Time requestTime)
{
    return getSidebandSpec(requestTime).OffsetSideband;
}

FTSEngine::SidebandSpecification FTSEngine::getSidebandSpec(ACS::Time requestTime)
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

    if(sidebandSpecificationDeque_m.empty() == true)
    {
        // No frequency specified throw an exception
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "No sideband specifications available!");
        ex.log();
        throw ex;
    }

    unsigned int idx(sidebandSpecificationDeque_m.size() - 1);
    while((idx > 0)
        && (sidebandSpecificationDeque_m[idx].applicationTime > requestTime)) {
        --idx;
    }

    return sidebandSpecificationDeque_m[idx];
}

void FTSEngine::cleanSidebandQueue()
{
    cleanSidebandQueue(::getTimeStamp());
}

void FTSEngine::cleanSidebandQueue(ACS::Time currentTime)
{
    ACE_Write_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

    if(sidebandSpecificationDeque_m.empty() == false)
    {
        unsigned int idx(sidebandSpecificationDeque_m.size() - 1);
        while((idx > 0)
        && (sidebandSpecificationDeque_m[idx].applicationTime > currentTime))
        {
            --idx;
        }

        // idx now points to the first command, everything before this is junk
        while(idx > 0)
        {
            sidebandSpecificationDeque_m.pop_front();
            --idx;
        }
    }
}

void FTSEngine::updateFrequency(bool forceCommand)
{
    // This method will update the Frequency at the next reasonable time.  If
    // the lastFrequencyCommandTime is in the future it is used.  Otherwise a
    // time just far enough ahead to be available is used. The caller of this
    // function *must* lock the nextFreqCmdTimeMutex_m prior to calling this
    // function
    const ACS::Time now(::getTimeStamp());

    if((now + CommandLookAhead) > nextFrequencyCommandTime_m)
    {
        nextFrequencyCommandTime_m = now + CommandLookAhead;
        nextFrequencyCommandTime_m -= nextFrequencyCommandTime_m
            % TETimeUtil::TE_PERIOD_ACS;
        nextFrequencyCommandTime_m += TETimeUtil::TE_PERIOD_ACS;
    }

    updateFrequency(nextFrequencyCommandTime_m, forceCommand);
}

void FTSEngine::updateFrequency(ACS::Time applicationTime, bool forceCommand)
{
    // The application time is when we want to send the command, the commanded
    // time is when the frequency should be set.  So that is 1 TE after the
    // floor TE of the application time. The caller of this function *must*
    // lock the nextFreqCmdTimeMutex_m prior to calling this function.
    applicationTime -= applicationTime % TETimeUtil::TE_PERIOD_ACS;
    const ACS::Time commandTime(applicationTime + TETimeUtil::TE_PERIOD_ACS);

    // Find the frequency specification for the current time
    FTSEngine::FrequencySpecification freqSpec;
    try
    {
        freqSpec = getOffsetFrequencySpec(commandTime);
        setErrorState(FLOOG::ErrFTSFrequencyError, false);
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        // No frequency is specified set the FrequencyError
        setErrorState(FLOOG::ErrFTSFrequencyError, true);
    }

    try
    {
        // Check here to see if this is the same frequency
        if((freqSpec.ftsFrequency != ftsTheoryModel_m.getFTSFrequency())
        || (forceCommand == true))
        {
            // New frequency: update the theoretical model
            ftsTheoryModel_m.resetFTS();
            ftsTheoryModel_m.setFTSFrequency(freqSpec.ftsFrequency, 0);
        }
        else if(fringeTrackingEnabled_m == false)
        {
            // Nothing to do
            nextFrequencyCommandTime_m = commandTime;
            return;
        }
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        // Thrown by getFTSPhase and indirectly by setFTSFrequency (calls
        // getFTSPhase.
        setErrorState(FLOOG::ErrFTSFrequencyError, true);
    }

    // New frequency need to send the frequency command
    unsigned long long ftsFrequencyCommand(freqSpec.ftsFrequency);
    if(fringeTrackingEnabled_m == true)
    {
        const ACS::Time evaluationTime(
            commandTime + TETimeUtil::TE_PERIOD_ACS);

        // The thought process is this.  If the phase of the hardware
        // is correct (wrt the delay corrected theory curve) then the
        // change in frequency from the offset value is just the change
        // in delay  converted to a phase by the offset frequency divided
        // by the time between the delay measurements.

        // Matching phases at the TE points is easiest and the errors introduced
        // are small enough (I think) to be negligible.

        // we apply a correction if the hardware does not start at the
        // theoretically correct value.  This ensures return to phase
        // and prevents the buildup of roundoff errors

        // Jeff Kern April 15, 2009

        // This is the fringe tracking correction.
        try
        {
            double cmdDelay(DelayCalculator::getDelay(
                commandTime + propagationDelay_m));
            double evalDelay(DelayCalculator::getDelay(
                evaluationTime + propagationDelay_m));
            NetSidebandMod::NetSideband fringeSideband(
                getFringeSideband(commandTime));
            setErrorState(FLOOG::ErrNoValidDelay, false);

            double totalPhaseCorrection(0.0);
            if(fringeSideband == NetSidebandMod::USB)
            {
                totalPhaseCorrection = (cmdDelay - evalDelay)
                    * freqSpec.loFrequency;
            }
            else
            {
                totalPhaseCorrection = (evalDelay - cmdDelay)
                    * freqSpec.loFrequency;
            }

            double hardwarePhase((ftsHardwareModel_m.getFTSPhaseDouble(
                commandTime) * coldMultiplier_m));

            double nominalPhase((ftsTheoryModel_m.getFTSPhaseDouble(
                commandTime) * coldMultiplier_m));

            double delayPhase(cmdDelay * freqSpec.loFrequency);
            if(fringeSideband == NetSidebandMod::USB)
            {
                delayPhase = -delayPhase; // Sideband Test
            }

            //std::cout << "Hardware Phase: " << hardwarePhase << std::endl;
            //std::cout << "Target Phase: " << nominalPhase + delayPhase
            //<< std::endl;
            // The correctionPhase is the amount of phase that we need
            // to add to the hardware in the next TE to account for the
            // fact that we are not starting where we intended.
            double correctionPhase(std::fmod(nominalPhase + delayPhase
                - hardwarePhase, 1.0));

            // Now we want to put the correction phase on the interval (-0.5,0.5]
            if(correctionPhase > 0.5)
            {
                correctionPhase -= 1.0;
            }

            if(correctionPhase <= -0.5)
            {
                correctionPhase += 1.0;
            }

            // Phase Correction
            totalPhaseCorrection += correctionPhase;

            // Convert to phase correction per clock tick
            totalPhaseCorrection /= (coldMultiplier_m * 6.0E6);

            // Fix the resulting phase into an adjustment to the fts frequency
            const long long frequencyCorrection(
                static_cast< long long > (totalPhaseCorrection
                    * (FTSDeviceData::FTSMask + 1)));
            ftsFrequencyCommand += frequencyCorrection;
            //      std::cout << std::hex << ftsFrequencyCommand << std::endl;
        }
        catch(const FTSExceptions::NoValidDelayExImpl& ex)
        {
            setErrorState(FLOOG::ErrNoValidDelay, true);
        }
        catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
        {
            setErrorState(FLOOG::ErrFTSFrequencyError, true);
        }
    }
    else
    {
        // Fringe tracking is off who cares about delay values
        setErrorState(FLOOG::ErrNoValidDelay, false);
    }

    queueFrequencyCommand(ftsFrequencyCommand, applicationTime);

    // Now update our state keeper
    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        nextFrequencyCommandTime_m = commandTime;
    }

    try
    {
        ftsHardwareModel_m.setFTSFrequency(ftsFrequencyCommand, commandTime);
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        setErrorState(FLOOG::ErrFTSFrequencyError, true);
    }
}

// The update thread will periodically call this method to queue all repeated
// monitors and periodic commands.  The caller of this function *must* lock
// the nextFreqCmdTimeMutex_m prior to calling this function.
void FTSEngine::updateThreadAction()
{
    const ACS::Time updateEndTime(::getTimeStamp() +
        (getUpdateThreadCycleTime() * 2));

    {
        // Watch for the case where we are not initialised
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        if(nextFrequencyCommandTime_m == 0)
        {
            return;
        }
    }

    ACS::Time tmp(0ULL);
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        tmp = nextFrequencyCommandTime_m;
    }
    while(tmp <= updateEndTime)
    {
        updateFrequency(tmp, false);
        {
            ACE_Read_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
            tmp = nextFrequencyCommandTime_m;
        }
    }
}

void FTSEngine::setPhaseValues()
{
    std::vector< float > phases(4, 0);

    if(enableFastPhaseSwitching_m)
    {
        phases[1] += 0.5;
        phases[3] += 0.5;
    }

    if(enableSlowPhaseSwitching_m)
    {
        phases[2] += 0.25;
        phases[3] += 0.25;
    }

    for(int idx(0); idx < 4; ++idx)
    {
        phases[idx] /= coldMultiplier_m;
        phases[idx] *= M_PI * 2.0; // Convert to radians
    }

    // Now actually send the command to the hardware
    try
    {
        sendPhaseValuesCommand(phases);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void FTSEngine::enableFringeTracking(bool enableTracking)
{
    if(fringeTrackingEnabled_m == enableTracking)
    {
        // Nothing to do
        return;
    }

    fringeTrackingEnabled_m = enableTracking;
    reset();
}

void FTSEngine::enableSlowPhaseSwitching(bool enablePhaseSwitching)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(enableSlowPhaseSwitching_m != enablePhaseSwitching)
    {
        enableSlowPhaseSwitching_m = enablePhaseSwitching;

        try
        {
            setPhaseValues();
        }
        catch(const ControlExceptions::INACTErrorExImpl& ex_)
        {
            // Ok the system is not ready yet, just go on
        }
    }
}

void FTSEngine::enableFastPhaseSwitching(bool enablePhaseSwitching)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(enableFastPhaseSwitching_m != enablePhaseSwitching)
    {
        enableFastPhaseSwitching_m = enablePhaseSwitching;

        try
        {
            setPhaseValues();
        }
        catch(const ControlExceptions::INACTErrorExImpl& ex_)
        {
            // Ok the system is not ready yet, just go on
        }
    }
}

bool FTSEngine::fringeTrackingEnabled()
{
    return fringeTrackingEnabled_m;
}

bool FTSEngine::slowPhaseSwitchingEnabled()
{
    return enableSlowPhaseSwitching_m;
}

bool FTSEngine::fastPhaseSwitchingEnabled()
{
    return enableFastPhaseSwitching_m;
}
