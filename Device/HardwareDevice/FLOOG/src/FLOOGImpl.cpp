//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <FLOOGImpl.h>
#include <string>
#include <vector>
#include <ostream>
#include <cstring>
#include <controlAlarmSender.h>
#include <TETimeUtil.h>
#include <loggingMACROS.h>
#include <walshFunction.h>
#include <FTSDeviceData.h>
#include <TMCDBAccessIFC.h>
#include <ControlDataInterfacesC.h>
#include <almaEnumerations_IFC.h>
#include <Guard_T.h>


FLOOGImpl::FLOOGImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    FLOOGBase(name, containerServices),
    nextStatusMonitorTime_m(0),
    updateThread_p(0),
    updateThreadCycleTime_m(2500000),
    WALSequenceNumber_m(0),
    walshOffsetHelper_m(16E-9, updateThreadCycleTime_m),
    alarmSender(0),
    offsetRejectsImageSideband_m(true)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

FLOOGImpl::~FLOOGImpl()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

// --------------- External Methods --------------------------
void FLOOGImpl::SetUserFrequency(CORBA::Double loFrequency,
    CORBA::Double ftsFrequency, ACS::Time epoch)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    const unsigned long long fixedFreq(worldToRawCntlFreq(ftsFrequency));

    if(epoch == 0)
    {
        FTSEngine::setFrequency(fixedFreq, loFrequency);
    }
    else
    {
        FTSEngine::setFrequency(fixedFreq, loFrequency, epoch);
    }
}

double FLOOGImpl::GetNominalFrequency()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        return rawToWorldFreq(getNominalFTSFrequency());
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        throw ControlExceptions::InvalidRequestExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getInvalidRequestEx();
    }
}

double FLOOGImpl::GetPropagationDelay()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return getPropagationDelay();
}

void FLOOGImpl::SetPropagationDelay(CORBA::Double propagationDelay)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    setPropagationDelay(propagationDelay);
}

CORBA::Boolean FLOOGImpl::PhaseSwitching180DegEnabled()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return fastPhaseSwitchingEnabled();
}

CORBA::Boolean FLOOGImpl::PhaseSwitching90DegEnabled()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return slowPhaseSwitchingEnabled();
}

void FLOOGImpl::EnablePhaseSwitching180Deg(CORBA::Boolean enable)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    enableFastPhaseSwitching(enable);
}

void FLOOGImpl::EnablePhaseSwitching90Deg(CORBA::Boolean enable)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    enableSlowPhaseSwitching(enable);
}

CORBA::Long FLOOGImpl::GetWalshFunction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return WALSequenceNumber_m;
}

void FLOOGImpl::SetWalshFunction(CORBA::Long WALSequenceNumber)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        loadWalshFunction(WALSequenceNumber);
        reset();
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        throw ControlExceptions::IllegalParameterErrorExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__).getIllegalParameterErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, true);
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
}

void FLOOGImpl::loadWalshFunction(CORBA::Long WALSequenceNumber)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    WalshFunction wf;
    try
    {
        wf = WalshGenerator::getWalshFunction(WALSequenceNumber);
    }
    catch(const std::out_of_range& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The WALSequenceNumber  "
            << WALSequenceNumber
            << "is out of the valid range (0 - 127)";
        ex.addData("Detail", msg.str());
        ex.log();

        throw ex;
    }

    setCntlPhaseSeq1(wf.firstVector());
    setCntlPhaseSeq2(wf.secondVector());
    WALSequenceNumber_m = WALSequenceNumber;
}

void FLOOGImpl::SetWalshFunctionDelay(double newDelayValue)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    walshOffsetHelper_m.setPhaseOffset(newDelayValue);
}

CORBA::Double FLOOGImpl::GetWalshFunctionDelay()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return walshOffsetHelper_m.getPhaseOffset();
}

void FLOOGImpl::SetFrequencyOffsetFactor(const CORBA::Long freqOffsetFactor)
{
    setFrequencyOffsetFactor(freqOffsetFactor);
}

CORBA::Long FLOOGImpl::GetFrequencyOffsetFactor()
{
    return getFrequencyOffsetFactor();
}

double FLOOGImpl::GetOffsetFTSFrequency()
{
    try
    {
        return rawToWorldFreq(getOffsetFTSFrequency());
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        throw ControlExceptions::InvalidRequestExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getInvalidRequestEx();
    }
}

void FLOOGImpl::setFrequencyOffsettingMode(
    const Control::LOOffsettingMode mode)
{
    setFrequencyOffsetting(mode);
}

Control::LOOffsettingMode FLOOGImpl::getFrequencyOffsettingMode()
{
    return FTSEngine::getFrequencyOffsettingMode();
}

CORBA::LongLong FLOOGImpl::GetColdMultiplier()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return getColdMultiplier();
}

void FLOOGImpl::SetColdMultiplier(CORBA::LongLong multiplier)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    setColdMultiplier(multiplier);
}

void FLOOGImpl::EnableFringeTracking(bool enableTracking)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    enableFringeTracking(enableTracking);
}

CORBA::Boolean FLOOGImpl::FringeTrackingEnabled()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return fringeTrackingEnabled();
}

void FLOOGImpl::SetOffsetToRejectImageSideband(bool rejectImage)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(offsetRejectsImageSideband_m != rejectImage)
    {
        offsetRejectsImageSideband_m = rejectImage;
        // Call SetFTS Sideband to change the offset sideband.
        SetFTSSideband(GetFTSSideband());
    }
}

CORBA::Boolean FLOOGImpl::OffsetRejectsImageSideband()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return offsetRejectsImageSideband_m;
}

void FLOOGImpl::SetFTSSideband(NetSidebandMod::NetSideband sideband)
{
   // For the FLOOG this is fairly simple, usually the offset sideband should
   // be USB unless we are rejecting the signal.

    try
    {
        if(offsetRejectsImageSideband_m)
        {
            setSideband(sideband, NetSidebandMod::LSB);
        }
        else
        {
            setSideband(sideband, NetSidebandMod::USB);
        }
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        throw ControlExceptions::IllegalParameterErrorExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__).getIllegalParameterErrorEx();
    }
}

NetSidebandMod::NetSideband FLOOGImpl::GetFTSSideband()
{
    return getFringeSideband();
}

// ------------------ Component Lifecycle Methods -----------
void FLOOGImpl::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    initialiseAlarmSystem();

    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    try
    {
        FLOOGBase::initialize();
        MonitorHelper::initialize(compName.in(), getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }

    // Prepare the threads for use
    std::string threadName(compName.in());
    threadName += "UpdateThread";
    updateThread_p = getContainerServices()->getThreadManager()->create<
        UpdateThread, FLOOGImpl* const >(threadName.c_str(), this);
    updateThread_p->setSleepTime(updateThreadCycleTime_m);

}

void FLOOGImpl::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Make certain that everything is stopped.
    try
    {
        Control::HardwareDeviceImpl::hwStop();
    }
    catch(...)
    {
        LOG_TO_OPERATOR(LM_WARNING, "Caught an exception during transition to "
            "the hardware stop state.  Continuing anyway.");
    }

    // Shutdown the threads, the threads were suspended by the
    // baseclass call to hwStopAction so we just need to terminate them
    // here.
    if(updateThread_p != 0)
    {
        updateThread_p->exit();
        if(updateThread_p->isSuspended())
        {
            updateThread_p->resume();
        }

        getContainerServices()->getThreadManager()->destroy(updateThread_p);
        updateThread_p = 0;
    }

    try
    {
        MonitorHelper::cleanUp();
        FLOOGBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        cleanUpAlarmSystem();

        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }

    cleanUpAlarmSystem();
}

// ---------------- Hardware Lifecycle Methods --------------
void FLOOGImpl::hwInitializeAction()
{
    const std::string fnName = "FLOOGImpl::hwInitializeAction";
    ACS_TRACE(fnName);

    // Load the configuration Data from the TMCDB.
    defaultPhaseOffset_m = 12E-3;
    walshOffsetHelper_m.setPhaseOffset(defaultPhaseOffset_m);
    walshOffsetHelper_m.initialize();

    FLOOGBase::hwInitializeAction();

    // Load and set the WalshFunction and the LO offset.
    try
    {
        maci::SmartPtr< TMCDB::Access > accessRef;
        accessRef = getContainerServices()->
            getDefaultComponentSmartPtr< TMCDB::Access >(
                "IDL:alma/TMCDB/Access:1.0");

        const std::string antennaName(
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName());

        try
        {
            const CORBA::Long walshIndex(
                accessRef->getAntennaWalshFunctionSeqNumber(
                    antennaName.c_str()));
            SetWalshFunction(walshIndex);
        }
        catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
        {
            ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", "Setting the Walsh function index failed.");
            nex.log();
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            setErrorState(::FLOOG::ErrCommunicationFailure, true);
        }
        catch(const ControlExceptions::INACTErrorExImpl& ex)
        {
            ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "Setting the Walsh function index failed.");
            nex.log();
        }
        catch(const TmcdbErrType::TmcdbErrorEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access failed somehow.  Cannot "
                "set the Walsh function!");
        }
        catch(const TmcdbErrType::TmcdbNoSuchRowEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access did not return a Walsh "
                "function index.  Cannot set the Walsh function!");
        }
        catch(...)
        {
            LOG_TO_OPERATOR(LM_ERROR, "An unexpected error happened during "
                "the retrieval of the Walsh function index from the TMCDB or "
                "the setting of it.  Cannot set the Walsh function!");
        }

        try
        {
            const CORBA::Long loOffsetIndex(
                accessRef->getAntennaLOOffsettingIndex(antennaName.c_str()));
            setFrequencyOffsetFactor(loOffsetIndex);
        }
        catch(const TmcdbErrType::TmcdbErrorEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access failed somehow.  Cannot "
                "set the LO offset!");
        }
        catch(const TmcdbErrType::TmcdbNoSuchRowEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access did not return an LO "
                "offset index.  Cannot set the LO offset!");
        }
        catch(...)
        {
            LOG_TO_OPERATOR(LM_ERROR, "An unexpected error happened during "
                "the retrieval of the LO offset index from the TMCDB or the "
                "setting of it.  Cannot set the LO offset!");
        }
    }
    catch(const maciErrType::NoDefaultComponentExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access component is not available. "
            "  Cannot set the Walsh function and cannot set the LO offset!");
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Got no reference for the TMCDB Access "
            "component.  Cannot set the Walsh function and cannot set the LO "
            "offset!");
    }

    // Set the Frequency Offset Factor
    // Set the initial propagation Delay

    MonitorHelper::resume();
    updateThread_p->suspend();
    nextStatusMonitorTime_m = 0;

    // Turn off the alarms
    if(alarmSender != 0)
    {
        alarmSender->terminateAllAlarms();
    }
}

void FLOOGImpl::hwOperationalAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    FLOOGBase::hwOperationalAction();

    // Set the Cold Multiplier
    setColdMultiplier(1);

    // Set the Phase Values
    enableFringeTracking(false);
    enableSlowPhaseSwitching(false);
    enableFastPhaseSwitching(false);
    setFrequencyOffsettingMode(Control::LOOffsettingMode_NONE);
    setPropagationDelay(0);
    setPhaseValues();

    SetUserFrequency(100E9, 31.25E6, 0);

    ACS::Time tempNextStatusMonitorTime(::getTimeStamp());
    tempNextStatusMonitorTime -= (tempNextStatusMonitorTime %
        TETimeUtil::TE_PERIOD_ACS);
    tempNextStatusMonitorTime += CommandLookAhead + (TETimeUtil::TE_PERIOD_ACS
        / 2);

    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
        nextStatusMonitorTime_m = tempNextStatusMonitorTime;
    }

    // Perform a reset
    // Reset the full FTS to make sure we start at known state.
    FTSEngine::reset();
    updateThread_p->resume();
}

void FLOOGImpl::hwStopAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushTime(0ULL);

    // Suspend the threads
    if((updateThread_p != 0) && (updateThread_p->isSuspended() == false))
    {
        updateThread_p->suspend();
    }

    nextStatusMonitorTime_m = 0;

    // Flush all commands
    flushNode(0, &flushTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        LOG_TO_DEVELOPER(LM_INFO, "Communication failure flushing commands to "
            "device");
    }
    else
    {
        std::ostringstream msg;
        msg << "All commands and monitors flushed at: "
            << flushTime;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    MonitorHelper::suspend();
    FLOOGBase::hwStopAction();

    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }
}

// ================ Monitoring Dispatch Method ================
void FLOOGImpl::processRequestResponse(
    const MonitorHelper::AMBRequestStruct& response)
{
    if(response.Status == AMBERR_FLUSHED)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
        return;
    }

    if(response.Status != AMBERR_NOERR)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, true);
        return;
    }

    setErrorState(::FLOOG::ErrCommunicationFailure, false);

    if(response.RCA == getControlRCACntlFreq())
    {
        // Response from setting the Frequency
        processFrequencyCommand(response);
        return;
    }

    if(response.RCA == getMonitorRCAFreq())
    {
        // Response from a monitor of the Frequency
        processFrequencyMonitor(response);
        return;
    }

    if(response.RCA == getMonitorRCAFtsStatus())
    {
        // Response from the status monitor
        processStatusMonitor(response);
        return;
    }

    // Unrecognized RCA Case
    std::ostringstream msg;
    msg << "Unrecognized RCA detected in process response (RCA = 0x"
        << std::hex
        << response.RCA
        << ")";
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

// Processing Methods
void FLOOGImpl::processStatusMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    bool resetRequired(false);

    if((response.Data[0] & 0x01) != 0)
    {
        // RF Power Error
        setErrorState(::FLOOG::ErrRFPowerError, true);
        resetRequired = true;
    }
    else
    {
        setErrorState(::FLOOG::ErrRFPowerError, false);
    }

    if((response.Data[0] & 0x20) != 0)
    {
        // Inverted Clock
        if((response.Data[0] & 0x05) != 0)
        {
            setErrorState(::FLOOG::ErrClockError, true);
            resetRequired = true;
        }
        else
        {
            setErrorState(::FLOOG::ErrClockError, false);
        }

    }
    else
    {
        // Normal Clock
        if((response.Data[0] & 0x18) != 0)
        {
            setErrorState(::FLOOG::ErrClockError, true);
            resetRequired = true;
        }
        else
        {
            setErrorState(::FLOOG::ErrClockError, false);
        }
    }

    if(resetRequired == true)
    {
        try
        {
            setResetFtsStatus();
            setErrorState(::FLOOG::ErrCommunicationFailure, false);
        }
        catch(ControlExceptions::CAMBErrorExImpl& ex)
        {
            ex.log();
            setErrorState(::FLOOG::ErrCommunicationFailure, true);
        }
        catch(ControlExceptions::INACTErrorExImpl& ex)
        {
            // Very strange situation... bet it never happens
            ex.log();
        }
    }
}

void FLOOGImpl::processFrequencyCommand(
    const MonitorHelper::AMBRequestStruct& response)
{
    setErrorState(::FLOOG::ErrFTSPhaseError, response.Timestamp
        - response.TargetTime > TETimeUtil::TE_PERIOD_ACS / 2);
    std::memcpy(lastFTSFrequencyCommand_m, response.Data, response.DataLength);
}

void FLOOGImpl::processFrequencyMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    const bool errorState((response.DataLength != 6) || std::memcmp(
        response.Data, lastFTSFrequencyCommand_m, response.DataLength));

    setErrorState(::FLOOG::ErrFTSFrequencyError, errorState);
}

// ---------------- Update Thread -------------------------------
void FLOOGImpl::updateThreadAction()
{
    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        FTSEngine::updateThreadAction();
    }

    // Again check that the nextStatusMonitorTime has been initialized
    if(nextStatusMonitorTime_m == 0)
    {
        return;
    }

    const ACS::Time updateEndTime(::getTimeStamp() +
        (getUpdateThreadCycleTime() * 2));
    while(nextStatusMonitorTime_m <= updateEndTime)
    {
        queueFTSStatusMonitor();
    }

    updateWalshFunctionDelay();
}

// ---------------- Delay Client Methods  ------------------------
void FLOOGImpl::newDelayEvent(const Control::AntennaDelayEvent& event)
{
    setDelays(event.delayTables);
    walshOffsetHelper_m.newDelayEvent(event);
}

// ================ Hardware Control Methods =================
void FLOOGImpl::updateWalshFunctionDelay()
{
    double newOffsetCommand(0.0);
    if(walshOffsetHelper_m.isUpdateNeeded(newOffsetCommand))
    {
        try
        {
            setCntlPhaseOffset(newOffsetCommand);
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "Error setting phase offset on FLOOG.");
        }
        catch(ControlExceptions::INACTErrorExImpl& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "Trying to update the Walsh function "
                "delay while the hardware is still inactive failed!");
        }
    }
}

void FLOOGImpl::queueFrequencyCommand(unsigned long long& fixedFtsFrequency,
    ACS::Time& epoch)
{
    MonitorHelper::AMBRequestStruct* cmdReq(getRequestStruct());
    MonitorHelper::AMBRequestStruct* monReq(getRequestStruct());

    // Populate the Command Request
    cmdReq->RCA = getControlRCACntlFreq();
    cmdReq->TargetTime = epoch;
    cmdReq->DataLength = 6;
#if BYTE_ORDER != BIG_ENDIAN
    {
        // Swap the bytes
        char* freqPtr(reinterpret_cast< char* >(&fixedFtsFrequency));
        cmdReq->Data[0] = freqPtr[5];
        cmdReq->Data[1] = freqPtr[4];
        cmdReq->Data[2] = freqPtr[3];
        cmdReq->Data[3] = freqPtr[2];
        cmdReq->Data[4] = freqPtr[1];
        cmdReq->Data[5] = freqPtr[0];
    }
#else
    {
        // Straight Copy
        std::memcpy(cmdReq->Data, &fixedFtsFrequency, 6);
    }
#endif

    // populate the Monitor request
    monReq->RCA = getMonitorRCAFreq();
    monReq->TargetTime = epoch + (TETimeUtil::TE_PERIOD_ACS / 2);

    // Queue these to be executed
    commandTE(cmdReq->TargetTime, cmdReq->RCA, cmdReq->DataLength,
        cmdReq->Data, cmdReq->SynchLock, &cmdReq->Timestamp, &cmdReq->Status);

    monitorTE(monReq->TargetTime, monReq->RCA, monReq->DataLength,
        monReq->Data, monReq->SynchLock, &monReq->Timestamp, &monReq->Status);

    // Add them to the queue
    queueRequest(cmdReq);
    queueRequest(monReq);
}

void FLOOGImpl::flushFrequencyCommands(ACS::Time& flushTime)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    // This method should flush all commands at or after flushTime that have
    // to do with monitorin or setting the frequency

    AmbErrorCode_t flushStatus(AMBERR_NOERR);
    ACS::Time flushExecutionTime(0ULL);

    // Flush all commands
    flushRCA(flushTime, getMonitorRCAFreq(), &flushExecutionTime, &flushStatus);
    setErrorState(::FLOOG::ErrCommunicationFailure, flushStatus
        != AMBERR_NOERR);

    flushRCA(flushTime, getControlRCACntlFreq(), &flushExecutionTime,
        &flushStatus);
    setErrorState(::FLOOG::ErrCommunicationFailure, flushStatus
        != AMBERR_NOERR);

    std::ostringstream msg;
    msg << "All frequency commands and monitors flushed at: "
        << flushTime;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

void FLOOGImpl::resetFTS()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    setRestart();
}

void FLOOGImpl::sendPhaseValuesCommand(std::vector< float >& phaseVals)
{
    try
    {
        setCntlPhaseVals(phaseVals);
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, true);
    }
}

void FLOOGImpl::queueFTSStatusMonitor()
{
    MonitorHelper::AMBRequestStruct* monReq(MonitorHelper::getRequestStruct());
    // Populate the Command Request
    monReq->RCA = FLOOGBase::getMonitorRCAFtsStatus();

    ACS::Time tempNextStatusMonitorTime(0ULL);
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
        tempNextStatusMonitorTime = nextStatusMonitorTime_m;
    }

    monReq->TargetTime = tempNextStatusMonitorTime;

    // Queue this command to be executed
    monitorTE(monReq->TargetTime, monReq->RCA, monReq->DataLength,
        monReq->Data, monReq->SynchLock, &monReq->Timestamp, &monReq->Status);

    queueRequest(monReq);

    // Only monitor the status about once a second
    tempNextStatusMonitorTime += TETimeUtil::ACS_ONE_SECOND;
    ACE_Write_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
    nextStatusMonitorTime_m = tempNextStatusMonitorTime;
}

// ============= FTS Support Methods ================
const ACS::Time FLOOGImpl::getUpdateThreadCycleTime()
{
    return updateThreadCycleTime_m;
}

// ===========================================================
FLOOGImpl::UpdateThread::UpdateThread(const ACE_CString& name,
    const FLOOGImpl* FLOOG):
    ACS::Thread(name),
    floogDevice_p(const_cast< FLOOGImpl* > (FLOOG))
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void FLOOGImpl::UpdateThread::runLoop()
{
    try
    {
        floogDevice_p->updateThreadAction();
    }
    catch(...)
    {
        STATIC_LOG_TO_DEVELOPER(LM_ERROR, "Caught an unexpected exception "
            "while executing this thread.  Continuing anyway.")
    }
}

// ============= Conversion Routines =================
double FLOOGImpl::rawToWorldFreq(unsigned long long freq) const
{
    double frequency(FTSDeviceData::FTSMask& freq);
    frequency *= static_cast< double >(FTSDeviceData::HwClockScale);
    frequency /= (FTSDeviceData::FTSMask + 1);
    return frequency;
}

unsigned long long FLOOGImpl::worldToRawCntlFreq(double freq) const
{
    double freqWord(freq * (FTSDeviceData::FTSMask + 1));
    freqWord /= FTSDeviceData::HwClockScale;
    return static_cast< unsigned long long >(freqWord);
}

// Convert the raw value of FTS_STATUS to a world value.
std::vector< bool > FLOOGImpl::rawToWorldFtsStatus(
    const unsigned char raw) const
{
    std::vector< bool >ret(6);
    ret[0] = raw & 0x01;
    ret[1] = raw & 0x02;
    ret[2] = raw & 0x04;
    ret[3] = raw & 0x08;
    ret[4] = raw & 0x10;
    ret[5] = raw & 0x20;

    return ret;
}

std::vector< unsigned char > FLOOGImpl::rawToWorldPhaseSeq1(const std::vector<
    unsigned char >& raw) const
{
    // TODO
    //std::vector< unsigned char > ret(0);
    return raw;
}

std::vector< unsigned char > FLOOGImpl::rawToWorldPhaseSeq2(const std::vector<
    unsigned char >& raw) const
{
    // TODO
    //std::vector< unsigned char > ret(0);
    return raw;
}

void FLOOGImpl::handleActivateAlarm(int code)
{
    FLOOGBase::setError(alarmSender->createErrorMessage());
}

void FLOOGImpl::handleDeactivateAlarm(int code)
{
    if(alarmSender->isAlarmSet() == true)
    {
        FLOOGBase::setError(alarmSender->createErrorMessage());
    }
    else
    {
        FLOOGBase::clearError();
    }
}

void FLOOGImpl::setErrorState(int errCode, bool state)
{
    if(alarmSender != 0)
    {
        if(state == true)
        {
            alarmSender->activateAlarm(errCode);
        }
        else
        {
            alarmSender->deactivateAlarm(errCode);
        }
    }
}

void FLOOGImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSender.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrNoValidDelay;
        alarmInfo.alarmTerminateCount = 3;
        alarmInfo.alarmDescription = "No valid delays found for requested time";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrCommunicationFailure;
        alarmInfo.alarmTerminateCount = 5;
        alarmInfo.alarmDescription = "Communication error on ALMA Monitor Bus";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrFTSFrequencyError;
        alarmInfo.alarmTerminateCount = 4;
        alarmInfo.alarmDescription
            = "Fine Tuning Synthesizer at wrong frequnecy";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrFTSPhaseLost;
        alarmInfo.alarmTerminateCount = 1;
        alarmInfo.alarmDescription = "Fine Tuning Synthesizer phase unknown";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrFTSPhaseError;
        alarmInfo.alarmTerminateCount = 1;
        alarmInfo.alarmDescription = "Fine Tuning Synthesizer phase incorrect";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrRFPowerError;
        alarmInfo.alarmTerminateCount = 2;
        alarmInfo.alarmDescription = "Fine Tuning Synthesizer RF Power Low";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrClockError;
        alarmInfo.alarmTerminateCount = 5;
        alarmInfo.alarmDescription
            = "Glitches detected in Fine Tuning Synthesizer clock";
        alarmInformation.push_back(alarmInfo);
    }

    alarmSender->initializeAlarms(
        Control::CharacteristicControlDeviceImpl::getAlarmFamilyName("FLOOG"),
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName(),
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void FLOOGImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(FLOOGImpl)
