//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <FLOOGHWSimImpl.h>
#include <loggingMACROS.h>


AMB::FLOOGHWSimImpl::FLOOGHWSimImpl::FLOOGHWSimImpl(node_t node,
    const std::vector< CAN::byte_t >& serialNumber):
    FLOOGHWSimBase::FLOOGHWSimBase(node, serialNumber)
{
}

void AMB::FLOOGHWSimImpl::FLOOGHWSimImpl::setControlSetFreq(
    const std::vector< CAN::byte_t >& data)
{
    associate(monitorPoint_FREQ, data);
    FLOOGHWSimBase::setControlSetFreq(data);
    //setMonitorFreq(data);
}

void AMB::FLOOGHWSimImpl::FLOOGHWSimImpl::setControlSetPhaseVals(
    const std::vector< CAN::byte_t >& data)
{
    associate(monitorPoint_PHASE_VALS, data);
    FLOOGHWSimBase::setControlSetPhaseVals(data);
    //setMonitorPhaseVals(data);
}

void AMB::FLOOGHWSimImpl::FLOOGHWSimImpl::setControlSetPhaseSeq1(
    const std::vector< CAN::byte_t >& data)
{
    FLOOGHWSimBase::setControlSetPhaseSeq1(data);
    setMonitorPhaseSeq1(data);
}

void AMB::FLOOGHWSimImpl::FLOOGHWSimImpl::setControlSetPhaseSeq2(
    const std::vector< CAN::byte_t >& data)
{
    FLOOGHWSimBase::setControlSetPhaseSeq2(data);
    setMonitorPhaseSeq2(data);
}

void AMB::FLOOGHWSimImpl::FLOOGHWSimImpl::setControlSetPhaseOffset(
    const std::vector< unsigned char >& data)
{
    associate(monitorPoint_PHASE_OFFSET, data);
    FLOOGHWSimBase::setControlSetPhaseOffset(data);
}

void AMB::FLOOGHWSimImpl::FLOOGHWSimImpl::associate(
    const AMB::rca_t RCA, const std::vector< CAN::byte_t >& data)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::map< AMB::rca_t, std::vector< CAN::byte_t >* >::iterator position(
        AMB::CommonHWSim::state_m.find(RCA));
    if(position != AMB::CommonHWSim::state_m.end())
    {
        std::vector < CAN::byte_t > *vector((*position).second);
        vector->assign(data.begin(), data.end());
    }
    else
    {
        std::ostringstream msg;
        msg << "Could not find the data for RCA = " << RCA
            << " in AMB::CommonHWSim::state_m.";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    }
}
