//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <FTSModel.h>
#include <ControlExceptions.h>
#include <FTSDeviceData.h>
#include <sstream>


/// Constuctor
FTSModel::FTSModel():
    accumulatorPhaseTime_m(0),
    accumulatorPhase_m(0),
    ftsFrequency_m(0)
{
}

FTSModel::~FTSModel()
{
}

void FTSModel::setFTSFrequency(unsigned long long ftsFrequency,
    ACS::Time commandTime)
{
    accumulatorPhase_m = getFTSPhase(commandTime);
    accumulatorPhaseTime_m = commandTime;
    ftsFrequency_m = ftsFrequency;
}

unsigned long long FTSModel::getFTSFrequency()
{
    return ftsFrequency_m;
}

ACS::Time FTSModel::getCommandTime()
{
    return accumulatorPhaseTime_m;
}

void FTSModel::resetFTS()
{
    accumulatorPhase_m = 0;
    accumulatorPhaseTime_m = 0;
    ftsFrequency_m = 0;
}

unsigned long long FTSModel::getFTSPhase(ACS::Time requestTime)
{
    if(requestTime < accumulatorPhaseTime_m)
    {
        ControlExceptions::IllegalParameterErrorExImpl newEx(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The requested time is before the latest frequency update\n"
            << "\tRequest Time:     "
            << requestTime
            << "\n"
            << "\tAccumulator Time: "
            << accumulatorPhaseTime_m;
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx;
    }

    // Update time (in microseconds) is how long it has been since the
    // last stored accumulation time.  Since the the FTS gets updated every
    // 8 ns (125E6 times per second) we add 125 * frequency every us
    const unsigned long long updateTime(
        (requestTime - accumulatorPhaseTime_m) / 10);
    const unsigned short* shortTime(
        reinterpret_cast< const unsigned short* >(&updateTime));

    unsigned long long accumulatedProduct(0ULL);
    unsigned long long maskValue(0xFFFFFFFFFFFFULL);
    for(int idx(0); idx < 3; ++idx)
    {
        unsigned long long product(ftsFrequency_m * shortTime[idx]);
        product &= (maskValue >> (16ULL * idx));
        accumulatedProduct += (product << (16ULL * idx));
    }

    //   std::cout << "FTSModel: " << requestTime << std::endl << std::hex
    //<< "    Freq: " << ftsFrequency_m  << std::endl
    //<< "   Phase: "
    //<< ((accumulatorPhase_m+(125*accumulatedProduct)) & FTSDeviceData::FTSMask )
    //<< std::dec << std::endl;

    return ((accumulatorPhase_m + (125ULL * accumulatedProduct))
        & FTSDeviceData::FTSMask);
}

double FTSModel::getFTSPhaseDouble(ACS::Time requestTime)
{
    const double fixedPhase(static_cast< double >(getFTSPhase(requestTime)));
    return (fixedPhase / 0x1000000000000ULL);
}
