#ifndef FLOOGImpl_h
#define FLOOGImpl_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <FLOOGBase.h>
#include <FLOOGS.h>
#include <FTSEngine.h>
#include <monitorHelper.h>
#include <walshDelayCalculator.h>
#include <FLOOGAlarmEnums.h>
#include <ControlDataInterfacesC.h>
#include <RW_Mutex.h>


namespace maci
{
    class ContainerServices;
};

namespace Control
{
    class AlarmSender;
};


class FLOOGImpl: public FLOOGBase,
    public FTSEngine,
    public MonitorHelper,
    virtual public POA_Control::FLOOG
{
    protected:
    /* -------------- Thread Classes  -------------- */
    class UpdateThread: public ACS::Thread
    {
        public:
        UpdateThread(const ACE_CString& name, const FLOOGImpl* FLOOG);
        virtual void runLoop();


        protected:
        FLOOGImpl* floogDevice_p;
    };


    public:
    friend class FLOOGImpl::UpdateThread;

    FLOOGImpl(const ACE_CString & name, maci::ContainerServices* cs);

    virtual ~FLOOGImpl();

    /* ----------------- External Interface ----------------------*/
    virtual void SetUserFrequency(CORBA::Double LOFrequency,
        CORBA::Double FTSFrequency, ACS::Time Epoch);

    virtual double GetNominalFrequency();

    virtual double GetPropagationDelay();

    virtual void SetPropagationDelay(CORBA::Double propagationDelay);

    virtual CORBA::Boolean PhaseSwitching180DegEnabled();

    virtual CORBA::Boolean PhaseSwitching90DegEnabled();

    virtual void EnablePhaseSwitching180Deg(CORBA::Boolean enable);

    virtual void EnablePhaseSwitching90Deg(CORBA::Boolean enable);

    virtual CORBA::Long GetWalshFunction();

    /**
     * \exception ControlExceptions::IllegalParameterErrorEx,
     * \exception ControlExceptions::CAMBErrorEx,
     * \exception ControlExceptions::INACTErrorEx
     */
    virtual void SetWalshFunction(CORBA::Long WALSequenceNumber);

    /// See the IDL file for a description of this function.
    virtual void SetWalshFunctionDelay(CORBA::Double delayValue);

    /// See the IDL file for a description of this function.
    virtual CORBA::Double GetWalshFunctionDelay();

    virtual double GetOffsetFTSFrequency();

    virtual CORBA::Long GetFrequencyOffsetFactor();

    virtual void SetFrequencyOffsetFactor(const CORBA::Long freqOffsetFactor);

    virtual void setFrequencyOffsettingMode(
        const Control::LOOffsettingMode mode);

    virtual Control::LOOffsettingMode getFrequencyOffsettingMode();

    virtual CORBA::LongLong GetColdMultiplier();

    virtual void SetColdMultiplier(CORBA::LongLong multiplier);

    virtual void EnableFringeTracking(bool);

    virtual CORBA::Boolean FringeTrackingEnabled();

    virtual void SetOffsetToRejectImageSideband(bool);

    virtual CORBA::Boolean OffsetRejectsImageSideband();

    /**
     * Method to specify if the FTS is locked above or below the reference.
     * this is only important for fringe tracking and frequency offsetting
     *
     * \exception: IllegalParameterErrorEx if the specified sideband is not
     * USB or LSB
     */
    virtual void SetFTSSideband(NetSidebandMod::NetSideband sideband);

    /**
     * Method to get the current specification of if the FTS is
     * locked above or below the reference this is only important for
     * fringe tracking and frequency offsetting
     */
    virtual NetSidebandMod::NetSideband GetFTSSideband();

    /* ---------------- Component Lifecycle -----------------------*/
    /**
     * \exception acsErrTypeLifeCycle::LifeCycleExImpl
     */
    virtual void initialize();

    /**
     * \exception acsErrTypeLifeCycle::LifeCycleExImpl
     */
    virtual void cleanUp();

    /* ---------------- Delay Client Methods  ------------------------*/
    virtual void newDelayEvent(const Control::AntennaDelayEvent& event);


    protected:
    /* ---------------- Hardware Lifecycle Interface ---------------*/
    virtual void hwInitializeAction();

    virtual void hwOperationalAction();

    virtual void hwStopAction();

    /* --------------------Monitor Helper Method -------------------*/
    virtual void processRequestResponse(const MonitorHelper::AMBRequestStruct&);

    virtual void processStatusMonitor(const MonitorHelper::AMBRequestStruct&);

    virtual void processFrequencyCommand(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processFrequencyMonitor(
        const MonitorHelper::AMBRequestStruct&);

    /// This function does not include the reset of the the FTS
    /// and should only be used internally to the FLOOG
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual void loadWalshFunction(CORBA::Long WALSequenceNumber);

    virtual void updateWalshFunctionDelay();

    virtual void queueFTSStatusMonitor();

    /* ----------------- Service Loop Thread ----------------- */
    void updateThreadAction();

    /* ----------------- FTS Support Methods ------------------ */
    virtual const ACS::Time getUpdateThreadCycleTime();

    virtual void queueFrequencyCommand(unsigned long long&, ACS::Time&);

    virtual void flushFrequencyCommands(ACS::Time&);

    virtual void resetFTS();

    /// Method to set the phase values on the hardware,
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual void sendPhaseValuesCommand(std::vector< float >&);

    /* ------------------ Conversion Methods ----------------- */
    virtual double rawToWorldFreq(long long unsigned int) const;

    virtual long long unsigned int worldToRawCntlFreq(double) const;

    /// Convert the raw value of FTS_STATUS to a world value.
    virtual std::vector< bool > rawToWorldFtsStatus(
        const unsigned char raw) const;

    virtual std::vector< unsigned char > rawToWorldPhaseSeq1(
        const std::vector< unsigned char >& raw) const;

    virtual std::vector< unsigned char > rawToWorldPhaseSeq2(
        const std::vector< unsigned char >& raw) const;

    /* -------- These are pure delegation methods -------- */
    virtual void handleActivateAlarm(int code);

    virtual void handleDeactivateAlarm(int code);

    virtual void setErrorState(int errCode, bool state);


    private:
    /// No default ctor.
    FLOOGImpl();

    /// No copy ctor.
    FLOOGImpl(const FLOOGImpl&);

    /// No assignment operator.
    FLOOGImpl& operator=(const FLOOGImpl&);

    /// Initialise the helper system for alarms.
    void initialiseAlarmSystem();

    /// Clean up the helper system for alarms.
    void cleanUpAlarmSystem();

    /// This holds the last successfully executed command to the FTS frequency
    /// register.
    AmbDataMem_t lastFTSFrequencyCommand_m[6];

    /// These are values which we read from the FLOOG configuration file.
    double defaultPhaseOffset_m; // The offset for zero delay in the corr

    ACE_RW_Mutex nextStatusMonitorTimeMutex_m;

    ACS::Time nextStatusMonitorTime_m;

    UpdateThread* updateThread_p;

    ACS::Time updateThreadCycleTime_m;

    /// This holds the currently specified Walsh function sequence number.
    long WALSequenceNumber_m;

    /// This helper class keeps track of the delay needed for the walsh
    /// functions.
    WalshDelayCalculator walshOffsetHelper_m;

    /// Pointer for the alarm system helper instance.
    Control::AlarmSender* alarmSender;

    /// Boolean which specifies if we are rejecting the signal or image 
    /// sideband when we do LO Offsetting
    bool offsetRejectsImageSideband_m;
};
#endif
