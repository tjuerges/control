#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef walshDelayCalculator_h
#define walshDelayCalculator_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <deque>
#include <acstime.h>
#include <RW_Mutex.h>


namespace Control
{
    class AntennaDelayEvent;
}


class WalshDelayCalculator
{
    public:
    WalshDelayCalculator(double updateThreshold, ACS::Time cycleTime);

    virtual ~WalshDelayCalculator();

    /// This set the Walsh Delay Calculator back to the initial state
    virtual void initialize();

    /// Sets the current value of the nominal phase offset
    /// this is expressed in seconds and should be between 8 and 16 ms
    virtual void setPhaseOffset(double newDelayValue);

    /// Returns the current value of the nominal phase offset
    /// this is expressed in seconds and should be between 8 and 16 ms
    virtual double getPhaseOffset();

    /// Called whenever a new delay event arrives this updates the
    /// queue of delays
    virtual void newDelayEvent(const Control::AntennaDelayEvent&);

    /// This method returns true if the difference between the previous
    /// command sent and the current command is greater than the threshold
    /// the offsetValue value always contains the current value when it returns.
    virtual bool isUpdateNeeded(double& offsetValue);


    protected:
    /// If the new and old values differ by more than this value the
    /// isUpdatedNeeded value returns true
    const double updateThreshold_m;

    /// This is the cycle time so we can figure out when to update
    const ACS::Time cycleTime_m;

    /// This is the last value we sent to the hardware and the delay
    /// value that was used to generate it
    double lastPhaseOffsetCommand_m;

    double lastDelayValue_m;

    // The phase offset for zero delay in the corr
    double phaseOffset_m;

    typedef struct
    {
        ACS::Time applicationTime;
        int coarseDelay;
    } CoarseDelayStruct_t;

    ACE_RW_Mutex syncMutex_m;

    std::deque< CoarseDelayStruct_t > coarseDelayQueue_m;
};
#endif
