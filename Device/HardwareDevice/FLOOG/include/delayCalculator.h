#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef delayCalculator_h
#define delayCalculator_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//

#include <ControlInterfacesC.h>
#include <list>
#include <delayReceiver.h>
#include <PolynomialInterpolatorImpl.h>
#include <FTSExceptions.h>
#include <RW_Mutex.h>


class DelayCalculator
{
    public:
    DelayCalculator();

    ~DelayCalculator();

    /// This puts a new set of Delays into the the queue.
    void setDelays(const Control::DelayTableSeq& newTables);

     /// This gets the delay at the specified time
     /// \exception FTSExceptions::NoValidDelayExImpl
    double getDelay(ACS::Time requestTime);


    protected:
    void insertDelayTable(const Control::DelayTable& newTable);

    void removeExpiredTables();

    /// This method checks to see if the table being used by the interpolator
    /// is valid for the requested time.
    /// \return True if the requestedTime is between the start and stop times
    /// for the current interpolator table.
    bool checkInterpolatorTable(ACS::Time requestTime);

    /// This method checks through all current delay tables and tries to
    /// find the one which is valid for the requested time. The
    /// interpolatorTable_m variable is updated to point to the table that
    /// is found, and the createInterpolator method is called to create
    /// an interpolator on this table.
    /// Any existing interpolator is destroyed, and no interpolater exists if
    /// a valid table cannot be found.
    /// \exception FTSExceptions::NoValidDelayExImpl is thrown if no
    /// enties in the delayList satisfies the requested time.
    void getInterpolator(ACS::Time requestTime);

    /// Synchronization Mutex protecting the delayList
    ACE_RW_Mutex delayListMutex_m;

    /// Synchronization Mutex protecting the interpolator pointer.
    ACE_Mutex interpolatorMutex;

    std::list< Control::DelayTable > delayList_m;

    /// This iterator points to the DelayTable currently in use
    /// for the interpolator.  If no table is selected (or none is available)
    /// this points to the delayList_m.end()
    std::list< Control::DelayTable >::iterator interpolatorTable_m;

    /// These values are protected just for the sake of testing
    PolynomialInterpolator* interpolator_p;

    ACS::Time* interpolatorTime_p;

    double* interpolatorValue_p;


    private:
    /// These methods are not synchronized so they are private to protect the
    /// integrety of the delay list
    void createInterpolator();

    void deleteInterpolator();
};
#endif
