#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef FTSEngine_h
#define FTSEngine_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <delayCalculator.h>
#include <NetSideband.h>
#include <FTSModel.h>
#include <ControlDataInterfacesC.h>
#include <RW_Mutex.h>


/// This class is the heart of both the FLOOG and SecondLO modules.
/// It abstracts the hardware portion of the FTS board out, handling fringe
/// tracking and phase coherent frequency switching.
class FTSEngine: public DelayCalculator
{
    public:
    FTSEngine();

    virtual ~FTSEngine();


    protected:
    /// This method sets the new frequency for the FTS.  The loFrequency is
    /// only needed when fringe tracking is enabled.  The FTS will switch
    /// frequency at epoch.
    virtual void setFrequency(unsigned long long nominalFtsFrequency,
        double nominalLoFrequency, ACS::Time epoch);

    /// Same as above but the frequency is set ASAP.
    virtual void setFrequency(unsigned long long nominalFTSFrequency,
        double nominalLoFrequency);

    /// This method returns the current Nominal frequency of the FTS
    /// this does not include the effects of the frequency offset or of
    /// fringe tracking.
    /// \exception InvalideRequestExImpl if their is not frequency for
    /// the requested time.
    virtual unsigned long long getNominalFTSFrequency();

    /// This method returns the current nominal frequency of the LO
    /// to which the FTS is attached.
    /// \exception InvalideRequestExImpl if their is not frequency for
    /// the requested time.
    virtual double getNominalLoFrequency();

    /// This method returns the current offset frequency of the FTS
    /// this does not include the effects of fringe tracking.
    /// \exception InvalideRequestExImpl if their is not frequency for
    /// the requested time.
    virtual unsigned long long getOffsetFTSFrequency();

    /// This method returns the current offset frequency of the LO
    /// to which the FTS is attached.
    /// \exception InvalideRequestExImpl if their is not frequency for
    /// the requested time.
    virtual double getOffsetLoFrequency();

    /// Method to set the antenna specific frequency offset for spurious
    /// signal rejection.
    ///      * The input Factor is multiplied by the factor of 4 GHz/ 2^27
    /// and any cold multiplier is taken into account.
    void setFrequencyOffsetFactor(int frequencyOffsetFactor);

    /// Method get the current frequency offset set for spurious signal
    /// rejection.
    int getFrequencyOffsetFactor();

    /// Method to specify the cold multiplier for the current band
    /// must be properly set for fringe tracking to work.
    void setColdMultiplier(long multiplier);

    /// Method to get the cold multiplier for the current band
    /// must be properly set for fringe tracking or frequency
    /// offsetting to work.
    long getColdMultiplier();

    /// Method to set the LO frequency offset mode.
    void setFrequencyOffsetting(const Control::LOOffsettingMode mode);

    /// Method reporting the state of the frequency offsetting
    Control::LOOffsettingMode getFrequencyOffsettingMode();

    /// Method to set the value of the propagation time constant
    /// this value is used when calculating the delay to take into account
    /// the finite time for the Timing event to reach the antenna.
    void setPropagationDelay(double propagationTime);

    /// Method to get the current value of the propogation time constant
    double getPropagationDelay();

    /// This method can be used to return the FTS to a known state.
    /// All existing frequency commands are flushed from the system
    /// The hardware is reset (Zero Frequency)
    /// We start again with the current state.
    virtual void reset();

    /// Set the sideband that the FTS is locked on only USB or LSB are
    /// valid options, other values cause an exception.
    /// Also specify which sideband to apply offsetting for.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void setSideband(NetSidebandMod::NetSideband fringeSideband,
        NetSidebandMod::NetSideband offsetSideband);

    /// \return the currently selected sideband for the offset sideband.
    /// Although unlikely it is possible that this would throw an excption
    /// \exception ControlExceptions::InvalidRequestExImpl
    NetSidebandMod::NetSideband getOffsetSideband();

    /// \return the currently selected sideband for the fringe sideband.
    /// Although unlikely it is possible that this would throw an excption
    /// \exception ControlExceptions::InvalidRequestExImpl
    NetSidebandMod::NetSideband getFringeSideband();

    /// These functions toggle on or off various features of the FTS
    virtual void enableFringeTracking(bool enableTracking);

    /// These functions toggle on or off various features of the FTS
    virtual void enableSlowPhaseSwitching(bool enablePhaseSwitching);

    /// These functions toggle on or off various features of the FTS
    virtual void enableFastPhaseSwitching(bool enablePhaseSwitching);

    /// These report on the state of various features of the FTS
    virtual bool fringeTrackingEnabled();

    /// These report on the state of various features of the FTS
    virtual bool slowPhaseSwitchingEnabled();

    /// These report on the state of various features of the FTS
    virtual bool fastPhaseSwitchingEnabled();

    typedef struct
    {
        unsigned long long ftsFrequency;
        double loFrequency;
        ACS::Time applicationTime;
    } FrequencySpecification;

    ///
    /// There are a number of signs we need to worry about when we
    /// set the frequency.
    ///
    /// We need to pay attention to if the FTS is locked above or below
    /// the reference.  This I refer to as the FringeSideband.
    ///
    /// We need to pay attention to if the frequency offset is for
    /// an upper sideband or lower sideband spectral window.
    ///
    /// If the loOffestRejectImageSideband_m variable is false then 
    /// we want to reverse the sign of the offset for the first LO only.
    typedef struct
    {
        NetSidebandMod::NetSideband FringeSideband;
        NetSidebandMod::NetSideband OffsetSideband;
        ACS::Time applicationTime;
    } SidebandSpecification;

    /// This is a list of the requested frequencies and the time
    /// at which they become active.  The current specification
    /// is the value at the front of the queue,
    /// The sideband specification, could have been part of the frequency
    /// specification but for histoical reasons is handled seperatly
    std::deque< FrequencySpecification > frequencySpecificationDeque_m;

    std::deque< SidebandSpecification > sidebandSpecificationDeque_m;

    ACE_RW_Mutex frequencyDequeMutex_m;

    /// This method puts the new frequency specification into the
    /// queue, and returns if this change requires an update to
    /// the the frequency.
    void queueFTSFrequency(FTSEngine::FrequencySpecification);

    /// This method goes through the queue and will remove all obsolete
    /// commands
    virtual void cleanQueue(ACS::Time);

    /// This method goes through the queue and will remove all obsolete
    /// commands
    virtual void cleanQueue();

    /// This method sets the sideband to be changed at some point in the
    /// future
    void setSideband(NetSidebandMod::NetSideband, 
        NetSidebandMod::NetSideband, ACS::Time);

    /// \return the offset sideband queued for the specified time
    /// \exception ControlExceptions::InvalidRequestExImpl
    NetSidebandMod::NetSideband getOffsetSideband(ACS::Time);

    /// \return the fringe sideband queued for the specified time
    /// \exception ControlExceptions::InvalidRequestExImpl
    NetSidebandMod::NetSideband getFringeSideband(ACS::Time);

    SidebandSpecification getSidebandSpec(ACS::Time);

    virtual void cleanSidebandQueue(ACS::Time);

    virtual void cleanSidebandQueue();

    /// This method returns the frequency specification for a requested
    /// time. This is the nominal frequency not including the effects
    /// of LO offsetting or fringeRotation
    /// \exception ControlExceptions::InvalidRequestExImpl is thrown if
    /// there are no frequencies defined.
    FrequencySpecification getNominalFrequencySpec(ACS::Time);

    /// This method returns the frequnecy specification for a requested time
    /// if the frequency offset is enabled this frequency is adjusted by
    /// the specified offset.  If offsetting is not enabled this frequnecy
    /// is the same as the NominalFrequency
    /// \exception InvalideRequestExImpl if their is not frequency for
    /// the requested time.
    FrequencySpecification getOffsetFrequencySpec(ACS::Time);

    /// This method is called to maintain the current frequency specification
    /// the frequency is adjusted for fringe tracking but absolute phase
    /// is not set.  This may call setFrequency if it is determined that a
    /// new FrequencySpecification is required
    virtual void updateFrequency(ACS::Time, bool forceCommand);

    /// This method attempts to adjust the frequency at the next "reasonable"
    /// time.  This is either the lastCommandTime or a few TEs from now if
    /// the lastCommandTime is in the past.
    void updateFrequency(bool forceCommand = false);

    /// This method is called to set the phase values on the FTS
    /// it determines the correct values and sends them to the hardware
    void setPhaseValues();

    /// Utility method which gets the forward looking interval over which
    ///the queueFringeTrackingCommands executes
    virtual const ACS::Time getUpdateThreadCycleTime() = 0;

    /// Virtual methods which take care of placing commands in the
    /// queue to be executed.  The queuing is all handled on the
    /// derived device side.
    virtual void queueFrequencyCommand(unsigned long long& ftsFrequency,
        ACS::Time& applicationTime) = 0;

    /// Virtual methods which take care of placing commands in the
    /// queue to be executed.  The queuing is all handled on the
    /// derived device side.
    virtual void flushFrequencyCommands(ACS::Time& epoch) = 0;

    /// Virtual methods which take care of placing commands in the
    /// queue to be executed.  The queuing is all handled on the
    /// derived device side.
    virtual void resetFTS() = 0;

    /// Virtual methods which take care of placing commands in the
    /// queue to be executed.  The queuing is all handled on the
    /// derived device side.
    virtual void sendPhaseValuesCommand(std::vector< float >& phases) = 0;

    /// Virtual methods which take care of placing commands in the
    /// queue to be executed.  The queuing is all handled on the
    /// derived device side.
    virtual void setErrorState(int, bool) = 0;

    /// Members to keep track of what messages have been sent out
    ACS::Time nextFrequencyCommandTime_m;

    /// Members to keep track of what messages have been sent out
    ACE_RW_Mutex nextFreqCmdTimeMutex_m;

    /// This method is responsible for queuing all of the periodic monitor
    /// and control methods for the FTS
    virtual void updateThreadAction();

    /// Flags describing the behavior of the FTS
    bool fringeTrackingEnabled_m;

    /// Flags describing the behavior of the FTS
    bool enableSlowPhaseSwitching_m;

    /// Flags describing the behavior of the FTS
    bool enableFastPhaseSwitching_m;

    /// Flags describing the behavior of the FTS
    Control::LOOffsettingMode frequencyOffsettingMode_m;

    /// This is any multiplier that the LO goes through before it is mixed up
    long coldMultiplier_m;

    /// This is the number of the frequency offsetting factor we want to apply
    long frequencyOffsetFactor_m;

    /// These models keep track of the status of the FTS, both the acutal
    /// value of the accumulator and the theoretical model for the the
    /// current frequency.
    FTSModel ftsHardwareModel_m;

    FTSModel ftsTheoryModel_m;

    /// The peculiar propogation delay of the TE to this device
    ACS::Time propagationDelay_m;

    const static unsigned long long OffsetFrequencySizeCount = 0x1000000000ULL;

    const static double OffsetFrequencySizeHz = 30517.578125;

    const static unsigned long long CommandLookAhead = 2400000ULL;
};
#endif
