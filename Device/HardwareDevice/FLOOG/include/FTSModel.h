#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef FTSModel_h
#define FTSModel_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <acstime.h>


/// This class is broken out from the other FTS classes to simplify
/// testing.
/// This class takes in a series of frequency commands and the time that they
/// are to be executed and provides the current phase of the FTS accumulator
/// at any point in the future.
class FTSModel
{
    public:
    FTSModel();

    ~FTSModel();

    void setFTSFrequency(unsigned long long ftsFrequency,
        ACS::Time applicationTime);

    void resetFTS();

    unsigned long long getFTSFrequency();

    ACS::Time getCommandTime();

    /// This method returns the phase of the FTS at the specified
    /// time.
    /// \exception IllegalParameterErrorExImpl if the time is prior to the
    /// latest frequency update
    unsigned long long getFTSPhase(ACS::Time requestTime);

    /// Same as above method but value is returned in fractions of a cycle
    /// rather than fixed format.
    double getFTSPhaseDouble(ACS::Time requestTime);


    private:
    /// These variables specify the last known state of the phase accumulator
    /// in the FTS and the assumed running frequency.
    ACS::Time accumulatorPhaseTime_m;

    unsigned long long accumulatorPhase_m;

    unsigned long long ftsFrequency_m;
};
#endif
