// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef FLOOGHWSIMIMPL_H
#define FLOOGHWSIMIMPL_H

#include "FLOOGHWSimBase.h"

namespace AMB
{
    class FLOOGHWSimImpl: public FLOOGHWSimBase {
    public:
        FLOOGHWSimImpl(node_t node, 
                       const std::vector<CAN::byte_t>& serialNumber);
	
        // Setting the frequency is sets the corresponding monitor point 
        virtual void setControlSetFreq(const std::vector< CAN::byte_t >& data);

        // Setting the phase values also sets the corresponding monitor point 
        virtual void setControlSetPhaseVals(const std::vector< CAN::byte_t >& data);

        // Setting the phase sequencyes also sets the corresponding monitor point
        virtual void setControlSetPhaseSeq1(const std::vector< CAN::byte_t >& data);
        virtual void setControlSetPhaseSeq2(const std::vector< CAN::byte_t >& data);
        virtual void setControlSetPhaseOffset(const std::vector<unsigned char>& data);

  protected:
        void associate(const AMB::rca_t RCA,
                       const std::vector< CAN::byte_t >& data);

    }; // class FLOOGHWSimImpl    

} // namespace AMB
#endif // FLOOGHWSIMIMPL_H
