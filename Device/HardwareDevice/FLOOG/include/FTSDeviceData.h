#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef FTSDeviceData_h
#define FTSDeviceData_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


/// This namespace just holds the constants that are associated with the FTS
/// Device
namespace FTSDeviceData
{
    /// 10^7
    static const uint32_t AcsTimeScale = 10000000;

    /// 125e6
    static const uint32_t HwClockScale = 125000000;

    /// 6e6
    static const uint32_t ClockCyclesPerTE = 6000000;

    static const uint64_t FTSMask = 0xFFFFFFFFFFFFLL;

    /// These are the minimum and maximum valid frequencies for the FTS
    /// \note that these frequencies are for the FTS in the LO2 Module only
    /// the FTS in the FLOOG has a higher range.

    /// ~20 MHz
    static const uint64_t FTSMinumumFreq = 0x28f5c28f0000ULL;

    /// ~42.5 MHz
    static const uint64_t FTSMaximumFreq = 0x570a3d710000ULL;
};
#endif
