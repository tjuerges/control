/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSReceiverCommon.util;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;

/**
 * Abstract SummaryPannel class for DTS receivers.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */
public abstract class AbstractSummaryPanel extends DevicePanel {
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;

	int columns;

	/**
	 * @param aLogger
	 * @param aDevicePM
	 * @param columns
	 *            Number of columns for layout grid.
	 */
	protected AbstractSummaryPanel(Logger aLogger,
			DevicePresentationModel aDevicePM, int columns) {
		super(aLogger, aDevicePM);
		this.columns = columns;
		buildPanel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alma.Control.device.gui.common.panels.DevicePanel#buildPanel()
	 */
	@Override
	protected void buildPanel() {
		this.setBorder(BorderFactory.createCompoundBorder(BorderFactory
				.createTitledBorder(devicePM.getDeviceDescription()),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 100;
		c.weighty = 100;
		c.gridx = 0;
		c.gridy = 0;
		for (DTSReceiverUtil.Pair<String, Component> element : layoutElementsOfSummaryPanel()) {
			c.anchor = GridBagConstraints.EAST;
			add(new JLabel(element.key), c);
			c.gridx++;

			c.anchor = GridBagConstraints.WEST;
			add(element.value, c);
			c.gridx++;

			if (c.gridx >= columns * 2) {
				c.gridx = 0;
				c.gridy++;
			}
		}
		this.setVisible(true);
	}

	/**
	 * Returns (label, widget) pairs which should be laid out in SummaryPanel.
	 * The pairs will be laid out in columnsxN grid and from left to right, from
	 * top to bottom order.
	 */
	protected abstract List<DTSReceiverUtil.Pair<String, Component>> layoutElementsOfSummaryPanel();
}
