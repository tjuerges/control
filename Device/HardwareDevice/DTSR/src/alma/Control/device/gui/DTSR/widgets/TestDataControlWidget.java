/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * <code>TestDataControlWidget.java</code> presents the user with an interface
 * for controling the SET_DRF_TEST_DATA on the DTSR. The choices are saved as an
 * array of strings in this widget
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

public class TestDataControlWidget extends JPanel implements ActionListener {
	// TODO serialVersionUID
	private static final long serialVersionUID = 1L;
	private final Logger logger;
	private final ControlPointPresentationModel<Integer> controlPointModelD;
	private final ControlPointPresentationModel<Integer> controlPointModelC;
	private final ControlPointPresentationModel<Integer> controlPointModelB;
	private final ControlPointPresentationModel<Integer> controlPointModelAll;
	private final JButton setD, setC, setB, setAll;
	private static final String DLabel = "Set D";
	private static final String CLabel = "Set C";
	private static final String BLabel = "Set B";
	private static final String AllLabel = "Set All";
	private final JComboBox optionMenu;

	static enum OptionList {
		option0(DfrFrameAlignerMonitorPointWidget.LABELS[0][0], 0x00), option1(
				DfrFrameAlignerMonitorPointWidget.LABELS[0][1], 0x10), option2(
				DfrFrameAlignerMonitorPointWidget.LABELS[0][2], 0x20), option3(
				DfrFrameAlignerMonitorPointWidget.LABELS[0][3], 0x30),

		option4(DfrFrameAlignerMonitorPointWidget.LABELS[1][1], 0x01), option5(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][2], 0x02), option6(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][3], 0x03), option7(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][4], 0x04), option8(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][5], 0x05), option9(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][6], 0x06),

		option10(DfrFrameAlignerMonitorPointWidget.LABELS[1][8], 0x08), option11(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][9], 0x09), option12(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][10], 0x0A), option13(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][11], 0x0B), option14(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][12], 0x0C), option15(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][13], 0x0D), option16(
				DfrFrameAlignerMonitorPointWidget.LABELS[1][14], 0x0E);

		OptionList(String label, int value) {
			this.label = label;
			this.value = value;
		}

		@Override
		public String toString() {
			return label;
		}

		public int getValue() {
			return value;
		}

		final String label;
		final int value;
	}

	/**
	 * Construct a new TestDataControlWidget
	 * 
	 * @param logger
	 *            the logger that all logs should be directed to.
	 * @param controlPointModelD
	 *            The control point for SET_DFR_TEST_DATA ch D.
	 * @param controlPointModelC
	 *            The control point for SET_DFR_TEST_DATA ch C.
	 * @param controlPointModelB
	 *            The control point for SET_DFR_TEST_DATA ch B.
	 * @param controlPointModelAll
	 *            The control point for SET_DFR_TEST_DATA all.
	 */
	public TestDataControlWidget(Logger logger,
			ControlPointPresentationModel<Integer> controlPointModelD,
			ControlPointPresentationModel<Integer> controlPointModelC,
			ControlPointPresentationModel<Integer> controlPointModelB,
			ControlPointPresentationModel<Integer> controlPointModelAll) {
		this.logger = logger;
		// Save the control point models
		this.controlPointModelD = controlPointModelD;
		this.controlPointModelC = controlPointModelC;
		this.controlPointModelB = controlPointModelB;
		this.controlPointModelAll = controlPointModelAll;
		
		setD = new JButton(DLabel); // Setup the Ch D button
		setD.addActionListener(this);
		controlPointModelD.getContainingDevicePM().addControl(setD);
		setC = new JButton(CLabel); // Setup the Ch C button
		setC.addActionListener(this);
		controlPointModelC.getContainingDevicePM().addControl(setC);
		setB = new JButton(BLabel); // Setup the Ch B button
		setB.addActionListener(this);
		controlPointModelB.getContainingDevicePM().addControl(setB);
		setAll = new JButton(AllLabel); // Setup the All channels button
		setAll.addActionListener(this);
		controlPointModelAll.getContainingDevicePM().addControl(setAll);

		optionMenu = new JComboBox(); // Setup the optionMenu
		optionMenu.setEditable(false);
		for (OptionList entry : OptionList.values()) {
			optionMenu.addItem(entry);
		}
		controlPointModelAll.getContainingDevicePM().addControl(optionMenu);
		buildWidget();
	}

	/**
	 * When an actionEvent occurs (e.g the control button is pressed) check the
	 * value in the menu. Find the corresponding code and send it to the control
	 * point.
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		OptionList newValue = (OptionList) optionMenu.getSelectedItem();

		if (event.getActionCommand() == DLabel) {
			controlPointModelD.setValue(newValue.getValue());
		}
		if (event.getActionCommand() == CLabel) {
			controlPointModelC.setValue(newValue.getValue());
		}
		if (event.getActionCommand() == BLabel) {
			controlPointModelB.setValue(newValue.getValue());
		}
		if (event.getActionCommand() == AllLabel) {
			controlPointModelAll.setValue(newValue.getValue());
		}
	}

	private void buildWidget() {
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Set Test Data"),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Add all the points in a single column, starting at the top, with the
		// labels on the left.
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		add(optionMenu, c);
		c.gridwidth = 1;
		c.gridy++;
		add(setD, c);
		c.gridx++;
		add(setC, c);
		c.gridx++;
		add(setB, c);
		c.gridx++;
		add(setAll, c);
	}
}
