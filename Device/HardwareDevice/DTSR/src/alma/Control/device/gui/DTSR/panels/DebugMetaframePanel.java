/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdMonitorPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to in
 * panels and tabs to group related data together.
 * 
 * This panel displays the DTSR Debug Metaframe data and EEPROM read/write
 * controls.
 * 
 * Tabbed panels are used to group related data together and separate unrelated
 * data. These are created as required for each GUI.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

class DebugMetaframePanel extends DevicePanel {

	DebugMetaframePanel(Logger logger, DevicePM aDevicePM) {
		super(logger, aDevicePM);
		buildPanel();
	}

	protected void buildPanel() {
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Metaframe Delay"),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 0;

		addLeftLabel("Delay D", c);
		add(new IntegerMonitorPointWidget(logger, devicePM
				.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_D)),
				c);
		c.gridy++;
		addLeftLabel("Delay C", c);
		add(new IntegerMonitorPointWidget(logger, devicePM
				.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_C)),
				c);
		c.gridy++;
		addLeftLabel("Delay B", c);
		add(new IntegerMonitorPointWidget(logger, devicePM
				.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_B)),
				c);
		setVisible(true);
	}

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
