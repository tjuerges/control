/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdControlPoints;
import alma.Control.device.gui.DTSR.IcdMonitorPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.DTSR.widgets.SyncStatusMonitorPointWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FixedValueControlPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.util.logging.Logger;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;

/**
 * SyncStatusPanel offers a JPanel containing a display of all the information
 * provided by GET_DFR_SYNC_STATUS for all channels. This class is a separate
 * class because this information is displayed in 2 places in the DTSR GUI. As
 * per request of the product engineer an extra copy of the information is
 * needed in the debug area.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */
class SyncStatusPanel extends DevicePanel {

	SyncStatusPanel(Logger logger, DevicePM aDevicePM) {
		super(logger, aDevicePM);
		buildPanel();
	}

	protected void buildPanel() {
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Sync Status"),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 0;

		addLeftLabel("Ch D, Sync Found", c);
		add(new BooleanMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_D_STATUS),
				Status.FAILURE, "Channel D Sync Error", Status.GOOD,
				"Channel D Sync OK"), c);
		c.gridy++;
		addLeftLabel("Ch D, Offset", c);
		add(new SyncStatusMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_D_OFFSET),
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_D_OFFSETMSB)),
				c);
		c.gridy++;
		addLeftLabel("Ch C, Sync Found", c);
		add(new BooleanMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_C_STATUS),
				Status.FAILURE, "Channel C Sync Error", Status.GOOD,
				"Channel C Sync OK"), c);
		c.gridy++;
		addLeftLabel("Ch C, Offset", c);
		add(new SyncStatusMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_C_OFFSET),
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_C_OFFSETMSB)),
				c);
		c.gridy++;
		addLeftLabel("Ch B, Sync Found", c);
		add(new BooleanMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_B_STATUS),
				Status.FAILURE, "Channel B Sync Error", Status.GOOD,
				"Channel B Sync OK"), c);
		c.gridy++;
		addLeftLabel("Ch B, Offset", c);
		add(new SyncStatusMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_B_OFFSET),
				devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SYNC_STATUS_B_OFFSETMSB)),
				c);
		c.gridx = 2;
		c.gridy = 0;
		add(new FixedValueControlPointWidget(logger,
				devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_D),
				" Reset D "), c);
		c.gridy += 2;
		add(new FixedValueControlPointWidget(logger,
				devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_C),
				" Reset C "), c);
		c.gridy += 2;
		add(new FixedValueControlPointWidget(logger,
				devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_B),
				" Reset B "), c);
		c.gridy += 2;
		add(new FixedValueControlPointWidget(
				logger,
				devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_ALL),
				"Reset All"), c);
	}

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
