/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.presentationModels;

import alma.Control.DTSRHelper;
import alma.Control.device.gui.DTSR.IcdControlPoints;
import alma.Control.device.gui.DTSR.IcdMonitorPoints;
//import alma.Control.device.gui.DTSReceiverCommon.MonitorPoint;
import alma.Control.device.gui.DTSReceiverCommon.util.DTSReceiverUtil;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IControlPoint;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Model the details of a device to be displayed in a user interface.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 * 
 */
/*
 * TODO: Add public methods to move monitor points between short and long
 * polling groups
 * 
 * TODO: Add public methods to get list of monitor points in short and long
 * polling groups
 * 
 * TODO: Split out Generic code
 * 
 * TODO: Add ExcLog to all logged exceptions.
 */
public class DevicePM extends DevicePresentationModel {
	// public static final
	// alma.Control.device.gui.DTSReceiverCommon.IMonitorPoint<Float>
	// GET_AMBIENT_TEMPERATURE = new MonitorPoint<Float>(
	// float.class, "Ambsi Temperature", "operational",
	// Double.valueOf(218.15), Double.valueOf(323.15), "kelvin");

	static final String DEVICE_NAME = "DTSRBBpr";

	/**
	 * @param antennaName
	 *            the name of the antenna containing this device.
	 * @param services
	 *            the OMC container services.
	 */
	public DevicePM(String antennaName, PluginContainerServices services,
			int instance) {
		super(antennaName, instance, services);
	}

	/**
	 * Return a string description of the device for use in GUI displays.
	 */
	@Override
	public String getDeviceDescription() {
		return "DTSR BBpr" + getInstance() + " - " + getAntenna();
	}

	/**
	 * Return a string name of the device for use in GUI displays.
	 */
	/*
	 * TODO: This is duplicated among several classes. Find a way to move this
	 * to a superclass while allowing access to DEVICE_NAME to the
	 * <device>ChessboardPlugin classes.
	 */
	public static String getDeviceName() {
		return DEVICE_NAME;
	}

	@Override
	protected void createIcdControlPointPresentationModels() {

		for (IcdControlPoints controlPoint : IcdControlPoints.values()) {
			Class<?> aClass = DTSReceiverUtil
					.getCorrespondingMonitorAndControlPointType(controlPoint
							.getTypeClass());
			ControlPointPM<?> newCP = null;
			if (null != aClass) {
				newCP = new ControlPointPM<Object>(logger, this, controlPoint,
						aClass);
			} else {
				if (controlPoint.getArrayLength() == 0) {
					// This control point does not take parameters.
					newCP = new ControlPointPM<Object>(logger, this,
							controlPoint, null);
				} else {
					logger.warning("DevicePM.createControlPointPresentationModels(): "
							+ "Unknown type found while creating new ControlPointPresentationModel: "
							+ controlPoint.getTypeClass());
				}
			}
			controlPointPMs.put(controlPoint, newCP);
		}
	}

	@Override
	protected void createIdlControlPointPresentationModels() {
	}

	@Override
	protected void createIcdMonitorPointPresentationModels() {
		// The newMP holds several types of presentation models.

		for (IcdMonitorPoints monitorPoint : IcdMonitorPoints.values()) {
			Class<?> aClass = DTSReceiverUtil
					.getCorrespondingMonitorAndControlPointType(monitorPoint
							.getTypeClass());
			MonitorPointPM<?> newMP = null;
			if (null != aClass) {
				newMP = new MonitorPointPM<Object>(logger, this, monitorPoint);
			} else {
				logger.warning("DevicePM.createMonitorPointPresentationModels(): "
						+ "Unknown type found while creating new MonitorPointPresentationModel: "
						+ monitorPoint.getTypeClass());
			}
			monitorPointPMs.put(monitorPoint, newMP);
		}
	}

	@Override
	protected void createIdlMonitorPointPresentationModels() {
		/*
		 * Class<Float> aClass = DTSReceiverUtil
		 * .getCorrespondingMonitorAndControlPointType(GET_AMBIENT_TEMPERATURE
		 * .getTypeClass()); assert aClass != null;
		 * monitorPointPMs.put(GET_AMBIENT_TEMPERATURE, new
		 * MonitorPointPM<Float>( logger, this, GET_AMBIENT_TEMPERATURE));
		 */
	}

	@Override
	protected void setComponentName() {
		componentName = HardwareDeviceChessboardPresentationModel
				.getComponentNameForDevice(pluginContainerServices,
						antennaName, DEVICE_NAME + instance);
	}

	@Override
	protected void setDeviceComponent() {
		if (null == componentName) {
			setComponentName();
		}

		if (null == deviceComponent && null != componentName) {
			try {
				org.omg.CORBA.Object corbaObject = this.pluginContainerServices
						.getComponentNonSticky(componentName);
				this.deviceComponent = DTSRHelper.narrow(corbaObject);

				for (IControlPoint controlPoint : controlPointPMs.keySet()) {
					if (null != controlPointPMs.get(controlPoint)) {
						controlPointPMs.get(controlPoint).setDeviceComponent(
								this.deviceComponent);
					} else {
						logger.warning("DevicePM.setDeviceComponent() "
								+ "failed to find controlPointPM for "
								+ controlPoint);
					}
				}
				for (IMonitorPoint monitorPoint : monitorPointPMs.keySet()) {
					if (null != monitorPointPMs.get(monitorPoint)) {
						monitorPointPMs.get(monitorPoint).setDeviceComponent(
								this.deviceComponent);
					} else {
						logger.warning("DevicePM.setDeviceComponent() "
								+ "failed to find monitorPointPM for "
								+ monitorPoint);
					}
				}
			} catch (AcsJContainerServicesEx ex) {
				logger.warning("DevicePM.setDeviceComponent() - "
						+ "unable to obtain DTSR component: " + componentName);
				this.deviceComponent = null;

				for (IControlPoint controlPoint : controlPointPMs.keySet()) {
					controlPointPMs.get(controlPoint).setDeviceComponent(null);
				}
				for (IMonitorPoint monitorPoint : monitorPointPMs.keySet()) {
					monitorPointPMs.get(monitorPoint).setDeviceComponent(null);
				}
				ex.printStackTrace();
				// TODO: Should this exception be propagated up?
				// throw new RuntimeException(ex);
			}
		}
	}
}

//
// O_o
