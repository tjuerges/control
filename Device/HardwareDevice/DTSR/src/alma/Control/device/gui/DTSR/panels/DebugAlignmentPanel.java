/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdControlPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FixedValueControlPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized in panels
 * and tabs to group related data together.
 * 
 * This panel displays the DTSR Debug Frame Aligner sub-panel
 * 
 * Tabbed panels are used to group related data together and separate unrelated
 * data. These are created as required for each GUI.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

class DebugAlignmentPanel extends DevicePanel {

	DebugAlignmentPanel(Logger logger, DevicePM aDevicePM) {
		super(logger, aDevicePM);
		buildSubPanels(logger, aDevicePM);
		buildPanel();
	}

	private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
		fifoPointer = new FifoPointer(logger, aDevicePM);
		syncResetPanel = new SyncResetPanel(logger, aDevicePM);
		syncStatusPanel = new SyncStatusPanel(logger, aDevicePM);
	}

	protected void buildPanel() {
		setBorder(BorderFactory.createEtchedBorder(1));

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.weighty = 100;
		c.weightx = 100;
		c.gridx = 0;
		c.gridy = 0;

		add(syncStatusPanel, c);
		c.gridx++;
		add(syncResetPanel, c);
		c.gridx++;
		add(fifoPointer, c);

		setVisible(true);
	}

	/*
	 * This panel provides the control to initialize one or all of the FIFO read
	 * pointers.
	 */
	static class FifoPointer extends DevicePanel {

		public FifoPointer(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		protected void buildPanel() {
			setBorder(BorderFactory.createCompoundBorder(BorderFactory
					.createTitledBorder("FIFO Pointer Initialization"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));
			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_D),
					"Reset D", true), c);
			c.gridy++;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_C),
					"Reset C", true), c);
			c.gridy++;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_B),
					"Reset B", true), c);
			c.gridy++;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_ALL),
					"Reset All", true), c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	/*
	 * This panel provides the reset buttons for the sync status.
	 */
	static class SyncResetPanel extends DevicePanel {

		public SyncResetPanel(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("Sync Resets"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));

			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = 0;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_D),
					"Reset D"), c);
			c.gridy++;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_C),
					"Reset C"), c);
			c.gridy++;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_B),
					"Reset B"), c);
			c.gridy++;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_ALL),
					"Reset All"), c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	private FifoPointer fifoPointer;
	private SyncResetPanel syncResetPanel;
	private SyncStatusPanel syncStatusPanel;

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
