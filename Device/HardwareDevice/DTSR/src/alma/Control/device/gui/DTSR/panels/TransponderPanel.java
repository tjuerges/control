/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdControlPoints;
import alma.Control.device.gui.DTSR.IcdMonitorPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.DTSReceiverCommon.widgets.NanoWattTodBmMonitorPointWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.ByteSeqControlPointWidget;
import alma.Control.device.gui.common.widgets.FixedValueControlPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to in
 * panels and tabs to group related data together.
 * 
 * This panel contains all of the DTSR transponder related M&C points. This
 * includes TRX Alarms, signal level alarms, Rx temperature alarms, and an
 * interface to the I2C buss.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

class TransponderPanel extends DevicePanel {

	/**
	 * Creates a DTSR transponder panel.
	 * 
	 * @param logger
	 *            The logger to which all logs should be written.
	 * @param devicePM
	 *            The presentation model for the DTSR this panel is being used
	 *            on.
	 */
	TransponderPanel(Logger logger, DevicePM aDevicePM) {
		super(logger, aDevicePM);
		buildSubPanels(logger, aDevicePM);
		buildPanel();
	}

	private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
		i2cPanel = new I2CPanel(logger, aDevicePM);
		signalLevelPanel = new SignalLevelPanel(logger, aDevicePM);
		trxAlarmPanel = new TRXAlarmPanel(logger, aDevicePM);
		trxPowerAlarms = new TRXPowerAlarms(logger, aDevicePM);
	}

	/*
	 * This function sets up the panel and inserts all the sub-panels. It should
	 * only be called by the constructor.
	 */
	protected void buildPanel() {
		setBorder(BorderFactory.createEtchedBorder(1));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weighty = 100;
		c.weightx = 100;

		c.gridx = 0;
		c.gridy = 0;
		add(trxAlarmPanel, c);
		c.gridx++;
		add(signalLevelPanel, c);

		c.gridx = 0;
		c.gridy++;
		add(trxPowerAlarms, c);
		c.gridx++;
		add(i2cPanel, c);

		setVisible(true);
	}

	/*
	 * This inner class displays the general TRX alarms as given by
	 * ALARM_STATUS_REG It also provides a control to clear the alarms as
	 * provided by CLR_ALARM_STATUS_REG_ALL.
	 */
	static class TRXAlarmPanel extends DevicePanel {

		/**
		 * Create a new panel object and prepare it for use.
		 * 
		 * @param logger
		 *            The logger that all logs should be written to.
		 * @param devicePM
		 *            The presentation model for the DTSR this panel should
		 *            attach to.
		 */
		TRXAlarmPanel(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		/**
		 * Build a new TRXAlarmPanel Panel and populate it with widgets.
		 */
		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("TRX Alarm Status"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));

			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.CENTER;

			c.gridx = 0;
			c.gridy = 0;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.CLR_ALARM_STATUS_REG_ALL),
					"Clear Alarms"), c);
			c.gridx = 1;
			c.gridy = 0;
			add(new JLabel("D"), c);
			c.gridy++;
			addLeftLabel("Summary Alarm", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_D_ALMINT),
					Status.FAILURE, "Channel D Rx Summary Error", Status.GOOD,
					"Channel D Rx Alarms OK"), c);
			c.gridy++;
			addLeftLabel("Optical Power", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_D_POWALM),
					Status.FAILURE,
					"Channel D Loss of Average Optical Power Detected",
					Status.GOOD, "Channel D Optical Power Good"), c);
			c.gridy++;
			addLeftLabel("RxPLL Lock", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_D_LOCKERR),
					Status.FAILURE, "Channel D RxPLL Lock Lost", Status.GOOD,
					"Channel D RxPLL Lock OK"), c);

			c.gridx = 3;
			c.gridy = 0;
			add(new JLabel("C"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_C_ALMINT),
					Status.FAILURE, "Channel C Rx Summary Error", Status.GOOD,
					"Channel C Rx Alarms OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_C_POWALM),
					Status.FAILURE,
					"Channel C Loss of Average Optical Power Detected",
					Status.GOOD, "Channel C Optical Power Good"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_C_LOCKERR),
					Status.FAILURE, "Channel C RxPLL Lock Lost", Status.GOOD,
					"Channel C RxPLL Lock OK"), c);
			c.gridx = 5;
			c.gridy = 0;
			add(new JLabel("B"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_B_ALMINT),
					Status.FAILURE, "Channel B Rx Summary Error", Status.GOOD,
					"Channel B Rx Alarms OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_B_POWALM),
					Status.FAILURE,
					"Channel B Loss of Average Optical Power Detected",
					Status.GOOD, "Channel B Optical Power Good"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.ALARM_STATUS_REG_B_LOCKERR),
					Status.FAILURE, "Channel B RxPLL Lock Lost", Status.GOOD,
					"Channel B RxPLL Lock OK"), c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	/*
	 * This private inner class displays the TRX Power alarms as given by
	 * POWER_ALARM_REG. It also provides a control to clear the alarms as given
	 * by CLR_POWER_ALARM_REG_ALL
	 */
	static class TRXPowerAlarms extends DevicePanel {

		/**
		 * Create a new panel object and prepare it for use.
		 * 
		 * @param logger
		 *            The logger that all logs should be written to.
		 * @param devicePM
		 *            The presentation model for the DTSR this panel should
		 *            attach to.
		 */
		TRXPowerAlarms(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		/**
		 * Build a new TRXPowerAlarms Panel and populate it with widgets.
		 */
		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("TRX Power Alarms"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));

			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.CENTER;
			c.gridx = 0;
			c.gridy = 0;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.CLR_POWER_ALARM_REG_ALL),
					"Clear Alarms"), c);
			c.gridx = 1;
			c.gridy = 0;
			addLeftLabel("Channel: ", c);
			add(new JLabel("D"), c);
			c.gridy++;
			addLeftLabel("Summary Alarm", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_D_PSUMMARY),
					Status.FAILURE, "Channel D Power Supply Error",
					Status.GOOD, "Channel D Power Supply OK"), c);
			c.gridy++;
			addLeftLabel("-5.2v(bit6)", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_D_M5P2VALT),
					Status.FAILURE, "Channel D -5.2 Voltage Error",
					Status.GOOD, "Channel D -5.2 Voltage OK"), c);
			c.gridy++;
			addLeftLabel("+1.8v", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_D_1P8V),
					Status.FAILURE, "Channel D +1.8 Voltage Error",
					Status.GOOD, "Channel D +1.8 Voltage OK"), c);
			c.gridy++;
			addLeftLabel("+3.3v(bit4)", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_D_3P3VALT),
					Status.FAILURE, "Channel D +3.3 Voltage Error",
					Status.GOOD, "Channel D +3.3 Voltage OK"), c);
			c.gridy++;
			addLeftLabel("+3.3v(bit3)", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_D_3P3V),
					Status.FAILURE, "Channel D +3.3 Voltage Error",
					Status.GOOD, "Channel D +3.3 Voltage OK"), c);
			c.gridy++;
			addLeftLabel("-5.2v(bit2)", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_D_M5P2V),
					Status.FAILURE, "Channel D -5.2 Voltage Error",
					Status.GOOD, "Channel D -5.2 Voltage OK"), c);

			c.gridx = 3;
			c.gridy = 0;
			add(new JLabel("C"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_C_PSUMMARY),
					Status.FAILURE, "Channel C Power Supply Error",
					Status.GOOD, "Channel C Power Supply OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_C_M5P2VALT),
					Status.FAILURE, "Channel C -5.2 Voltage Error",
					Status.GOOD, "Channel C -5.2 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_C_1P8V),
					Status.FAILURE, "Channel C +1.8 Voltage Error",
					Status.GOOD, "Channel C +1.8 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_C_3P3VALT),
					Status.FAILURE, "Channel C +3.3 Voltage Error",
					Status.GOOD, "Channel C +3.3 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_C_3P3V),
					Status.FAILURE, "Channel C +3.3 Voltage Error",
					Status.GOOD, "Channel C +3.3 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_C_M5P2V),
					Status.FAILURE, "Channel C -5.2 Voltage Error",
					Status.GOOD, "Channel C -5.2 Voltage OK"), c);

			c.gridx = 5;
			c.gridy = 0;
			add(new JLabel("B"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_B_PSUMMARY),
					Status.FAILURE, "Channel B Power Supply Error",
					Status.GOOD, "Channel B Power Supply OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_B_M5P2VALT),
					Status.FAILURE, "Channel B -5.2 Voltage Error",
					Status.GOOD, "Channel B -5.2 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_B_1P8V),
					Status.FAILURE, "Channel B +1.8 Voltage Error",
					Status.GOOD, "Channel B +1.8 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_B_3P3VALT),
					Status.FAILURE, "Channel B +3.3 Voltage Error",
					Status.GOOD, "Channel B +3.3 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_B_3P3V),
					Status.FAILURE, "Channel B +3.3 Voltage Error",
					Status.GOOD, "Channel B +3.3 Voltage OK"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_ALARM_REG_B_M5P2V),
					Status.FAILURE, "Channel B -5.2 Voltage Error",
					Status.GOOD, "Channel B -5.2 Voltage OK"), c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	/*
	 * This inner class displays the TRX signal level as given by SIGNAL_AVG_nW
	 * in both nw and dBm
	 */
	static class SignalLevelPanel extends DevicePanel {

		// TODO confirm valid range for DTSR
		static final String OPERATING_RANGE = "Operating range: -16.02dBm to -0.969dBm";
		static final int PRECISION = 4;

		/**
		 * Create a new panel object and prepare it for use.
		 * 
		 * @param logger
		 *            The logger that all logs should be written to.
		 * @param devicePM
		 *            The presentation model for the DTSR this panel should
		 *            attach to.
		 */
		SignalLevelPanel(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		/**
		 * Build a new SignalLevelPanel Panel and populate it with widgets.
		 */
		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("Signal Average"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));

			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();

			c.gridx = 1;
			c.gridy = 0;
			add(new JLabel("D"), c);
			c.gridy++;
			addLeftLabel("nW", c);
			add(new FloatMonitorPointWidget(logger, devicePM
					.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_D)),
					c);
			c.gridy++;
			addLeftLabel("dBm", c);
			add(new NanoWattTodBmMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_D),
					false, PRECISION, OPERATING_RANGE), c);

			c.gridx = 3;
			c.gridy = 0;
			add(new JLabel("C"), c);
			c.gridy++;
			add(new FloatMonitorPointWidget(logger, devicePM
					.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_C)),
					c);
			c.gridy++;
			add(new NanoWattTodBmMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_C),
					false, PRECISION, OPERATING_RANGE), c);

			c.gridx = 5;
			c.gridy = 0;
			add(new JLabel("B"), c);
			c.gridy++;
			add(new FloatMonitorPointWidget(logger, devicePM
					.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_B)),
					c);
			c.gridy++;
			add(new NanoWattTodBmMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_B),
					false, PRECISION, OPERATING_RANGE), c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	/*
	 * This inner class provides user access & control of the DTSR internal I2C
	 * bus via the control I2C_CMD and the monitor I2C_DATA.
	 */
	static class I2CPanel extends DevicePanel {

		/**
		 * Create a new panel object and prepare it for use.
		 * 
		 * @param logger
		 *            The logger that all logs should be written to.
		 * @param devicePM
		 *            The presentation model for the DTSR this panel should
		 *            attach to.
		 */
		I2CPanel(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		/**
		 * Build a new I2C Panel and populate it with the needed widgets.
		 */
		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("I2C"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));
			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			add(new ByteSeqControlPointWidget(logger,
					devicePM.getControlPointPM(IcdControlPoints.I2C_CMD_D),
					"Set I2C D", 8), c);
			c.gridx++;
			add(new ByteSeqControlPointWidget(logger,
					devicePM.getControlPointPM(IcdControlPoints.I2C_CMD_C),
					"Set I2C C", 8), c);
			c.gridx++;
			add(new ByteSeqControlPointWidget(logger,
					devicePM.getControlPointPM(IcdControlPoints.I2C_CMD_B),
					"Set I2C B", 8), c);
			c.gridy++;
			c.gridx = 0;
			c.gridwidth = 3;
			// TODO: Fix I2C read
			// add(new DtsI2CReadWidget(logger,
			// devicePM.getMonitorPointPM(IcdMonitorPoints.I2C_DATA)), c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;

	private I2CPanel i2cPanel;
	private SignalLevelPanel signalLevelPanel;
	private TRXAlarmPanel trxAlarmPanel;
	private TRXPowerAlarms trxPowerAlarms;
}

//
// O_o
