/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSReceiverCommon;

/** 
 * Generic and reusable version of {@link alma.Control.device.gui.common.MonitorPoint}.
 * 
 * @author Kohji Nakamura	k.nakamura@nao.ac.jp
 * @version $Id$
 * @since   ALMA 8.1.0
 */

public class MonitorPoint<T> implements IMonitorPoint<T> {
    
    public MonitorPoint (
            Class<T> typeClass, 
            String newCommonName,
        String operatingMode
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
    }

	public MonitorPoint (
        Class<T> typeClass, 
        String operatingMode,
        String newCommonName,
        Double rangeLowerBound,
        Double rangeUpperBound
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
    }

    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode,
        Double rangeLowerBound,
        Double rangeUpperBound,
        String units
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
        this.units = units;
    }

    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode, 
        int expectedBooleanValue
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.expectedBooleanValue = (expectedBooleanValue == 1) ? true: false;
        this.operatingMode = operatingMode;
    }

    /*
     * Workaround for code generation systems tendency to convert "0.0" to "0".
     */
    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode, 
        Integer rangeLowerBound, 
        Double rangeUpperBound, 
        String units
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound;
        this.units = units;
    }

    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode, 
        Integer rangeLowerBound, 
        Integer rangeUpperBound
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
    }

    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode, 
        Integer rangeLowerBound, 
        Integer rangeUpperBound, 
        String units
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = units;
    }

    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode, 
        Long rangeLowerBound, 
        Long rangeUpperBound
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
    }

    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode, 
        Long rangeLowerBound, 
        Long rangeUpperBound, 
        String units
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = units;
    }

    public MonitorPoint (
        Class<T> typeClass,
        String newCommonName,
        String operatingMode,
        String units
    ) {
        this.typeClass = typeClass;
        this.commonName = newCommonName;
        this.operatingMode = operatingMode;
        this.units = units;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getArrayLength()
     */
    public int getArrayLength() {
        return arrayLength;
    }

    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getDisplayName()
     */
    public String getDisplayName() {
        return commonName;
    }

    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getDisplayOffset()
     */
    public double getDisplayOffset() {
        return this.displayOffset;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getDisplayScale()
     */
    public double getDisplayScale() {
        return this.displayScale;
    }
    
    public String getDisplayUnits() {
        if (displayUnits == null || displayUnits == "")
            return units;
        else
            return displayUnits;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getExpectedBooleanValue()
     */
    public Boolean getExpectedBooleanValue() {
        return expectedBooleanValue;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getLowerGraphYLabel()
     */
    public String getLowerGraphYLabel() {
        return lowerGraphYLabel;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getOperatingMode()
     */
    public String getOperatingMode() {
        return operatingMode;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getRangeLowerBound()
     */
    public Double getRangeLowerBound() {
        return rangeLowerBound;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getRangeUpperBound()
     */
    public Double getRangeUpperBound() {
        return rangeUpperBound;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getClassType()
     */
    public Class<T> getTypeClass() {
        return typeClass;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getUnits()
     */
    public String getUnits() {
        return units;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#getUpperGraphYLabel()
     */
    public String getUpperGraphYLabel() {
        return upperGraphYLabel;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setCommonName()
     */
    public void setDisplayName(String newCommonName) {
        this.commonName=newCommonName;
    }

    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setDisplayOffset()
     */
    public void setDisplayOffset (double newOffset) {
        this.displayOffset = newOffset;
    }

    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setDisplayScale()
     */
    public void setDisplayScale (double newScale) {
        this.displayScale = newScale;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setDisplayUnits()
     */
    public void setDisplayUnits (String newDisplayUnits) {
        this.displayUnits = newDisplayUnits;
    }

    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setLowerGraphYLabel()
     */
    public void setLowerGraphYLabel(String newLowerGraphYLabel) {
        this.lowerGraphYLabel = newLowerGraphYLabel;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setRangeLowerBound()
     */
    public void setRangeLowerBound(Double newRangeLowerBound) {
        this.rangeLowerBound = newRangeLowerBound;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setRangeUpperBound()
     */
    public void setRangeUpperBound(Double newRangeUpperBound) {
        this.rangeUpperBound = newRangeUpperBound;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#setUpperGraphYLabel()
     */
    public void setUpperGraphYLabel(String newUpperGraphYLabel) {
        this.upperGraphYLabel = newUpperGraphYLabel;
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#supportsRangeChecking()
     */
    public boolean supportsRangeChecking() {
        // TODO: add check for Class type and test ranges appropriately. 
        if (Boolean.class == typeClass) {
            return null != expectedBooleanValue;
        }
        return ((null != rangeLowerBound) && (null != rangeUpperBound));
    }
    
    /**
     * @see alma.Control.device.gui.common.IMonitorPoint#supportsUnits()
     */
    public boolean supportsUnits() {
        return (!units.equals(""));
    }

    private final int arrayLength = 0;
    private String commonName;
    private double displayOffset = 0;
    private double displayScale = 1;
    private String displayUnits = "";
    private Boolean expectedBooleanValue = null;
    private final String operatingMode;
    private String lowerGraphYLabel = "";
    // Doubles are used to store numeric values to prevent loss of precision.
    private Double rangeLowerBound = null;
    private Double rangeUpperBound = null;
    private final Class<T> typeClass;
    private String units = "";
    private String upperGraphYLabel = "";
}

//
//O_o
