/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdControlPoints;
import alma.Control.device.gui.DTSR.IcdMonitorPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FixedValueControlPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.LongMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.TextToCodeControlPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to in
 * panels and tabs to group related data together.
 * 
 * Tabbed panels are used to group related data together and separate unrelated
 * data. These are created as required for each GUI.
 * 
 * This panel provides the general status of the deformatter. Included is the
 * status register, parity info, sync status, metaframe delay, and TE status.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

class DeformatterStatusPanel extends DevicePanel {

	DeformatterStatusPanel(Logger logger, DevicePM aDevicePM) {
		super(logger, aDevicePM);
		buildSubPanels(logger, aDevicePM);
		buildPanel();
	}

	private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
		metaframePanel = new DebugMetaframePanel(logger, aDevicePM);
		parityTimerPanel = new ParityTimerPanel(logger, aDevicePM);
		resetButtonPanel = new ResetButtonPanel(logger, aDevicePM);
		statusRegPanel = new StatusRegPanel(logger, aDevicePM);
		syncStatusPanel = new SyncStatusPanel(logger, aDevicePM);
	}

	protected void buildPanel() {
		setBorder(BorderFactory.createEtchedBorder(1));
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weighty = 100;
		c.weightx = 100;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 2;
		add(statusRegPanel, c);

		c.gridheight = 1;
		c.gridx++;
		add(syncStatusPanel, c);

		c.gridy++;
		add(metaframePanel, c);

		c.gridy++;
		c.gridx = 0;
		add(resetButtonPanel, c);

		c.gridx++;
		add(parityTimerPanel, c);

		setVisible(true);
	}

	/*
	 * This panel contains all the parity timer information.
	 * 
	 * Known bug: The parity accumulators are specified in the ICD as unsigned
	 * 64 bit numbers. The C++ control code thinks of them as such. However,
	 * Java does not support unsigned ints, and the C++/Java interface does not
	 * support Java arbitrary precision types. As a result, if the last bit of
	 * the counter is set the number will overflow and become negative. This is
	 * a low impact bug because it would take an estimated 46764362479.287613
	 * years for the overflow to occur. (Jira ticket COMP-COMP-3671 tracks the
	 * ICD fix for this bug)
	 */
	static class ParityTimerPanel extends DevicePanel {

		ParityTimerPanel(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("Parity Status"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));
			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.NONE;

			c.gridx = 0;
			c.gridy = 0;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_ALL),
					"Reset All"), c);

			c.gridx++;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_D),
					" Reset D "), c);
			c.gridy++;

			add(new JLabel("D"), c);
			c.gridy++;
			addLeftLabel("1s Timer", c);
			add(new IntegerMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_D_TIME)),
					c);
			c.gridy++;
			addLeftLabel("1s Errors", c);
			add(new IntegerMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_D_PARITY)),
					c);
			c.gridy++;
			addLeftLabel("Accumulators", c);
			add(new LongMonitorPointWidget(logger, devicePM
					.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_COUNTER_D)),
					c);

			c.gridx++;
			c.gridy = 0;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_C),
					" Reset C "), c);
			c.gridy++;
			add(new JLabel("C"), c);
			c.gridy++;
			add(new IntegerMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_C_TIME)),
					c);
			c.gridy++;
			add(new IntegerMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_C_PARITY)),
					c);
			c.gridy++;
			add(new LongMonitorPointWidget(logger, devicePM
					.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_COUNTER_C)),
					c);

			c.gridx++;
			c.gridy = 0;
			add(new FixedValueControlPointWidget(
					logger,
					devicePM.getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_B),
					" Reset B "), c);
			c.gridy++;
			add(new JLabel("B"), c);
			c.gridy++;
			add(new IntegerMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_B_TIME)),
					c);
			c.gridy++;
			add(new IntegerMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_B_PARITY)),
					c);
			c.gridy++;
			add(new LongMonitorPointWidget(logger, devicePM
					.getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_COUNTER_B)),
					c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	/*
	 * The status register and FPGA reset buttons are stored in this panel,
	 */
	static class ResetButtonPanel extends DevicePanel {

		ResetButtonPanel(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("FPGA Resets"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));
			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			add(new TextToCodeControlPointWidget(logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_D),
					" Reset D ", resetOptions, resetCodes), c);
			c.gridy++;
			add(new TextToCodeControlPointWidget(logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_C),
					" Reset C ", resetOptions, resetCodes), c);
			c.gridy++;
			add(new TextToCodeControlPointWidget(logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_B),
					" Reset B ", resetOptions, resetCodes), c);
			c.gridy++;
			add(new TextToCodeControlPointWidget(logger,
					devicePM.getControlPointPM(IcdControlPoints.DFR_RESET_ALL),
					"Reset All", resetOptions, resetCodes), c);
		}

		static final String[] resetOptions = { "Clear Status Registers",
				"Master Reset" };
		static final int[] resetCodes = { 1, 0xFF };
		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	/*
	 * Pane to display the contents of the DFR Status register
	 */
	static class StatusRegPanel extends DevicePanel {

		StatusRegPanel(Logger logger, DevicePM aDevicePM) {
			super(logger, aDevicePM);
			buildPanel();
		}

		protected void buildPanel() {
			this.setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createTitledBorder("FPGA Status"),
					BorderFactory.createEmptyBorder(1, 1, 1, 1)));
			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = 0;

			add(new JLabel("D"), c);
			c.gridy++;
			addLeftLabel("System Clock PLL Lock", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYSCLK),
					Status.FAILURE, "Channel D system clock PLL lock lost",
					Status.GOOD, "Channel D system clock PLL locked"), c);
			c.gridy++;
			addLeftLabel("Data Clock PLL Lock", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_DATAINPCLK),
					Status.FAILURE, "Channel D data clock PLL lock lost",
					Status.GOOD, "Channel D data clock PLL locked"), c);
			c.gridy++;
			addLeftLabel("Half Transponder Lock", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_TRXLOCKED),
					Status.FAILURE, "Channel D half transponder lock lost",
					Status.GOOD, "Channel D half transponder locked"), c);
			c.gridy++;
			addLeftLabel("Parity Error", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_PARITYERR),
					Status.FAILURE, "Parity error on channel D ", Status.GOOD,
					"No parity errors on channel D"), c);
			c.gridy++;
			addLeftLabel("Sync Pattern Detected", c);
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYNCDETECTED),
					Status.FAILURE, "Channel D sync pattern error",
					Status.GOOD, "Channel D sync pattern detected"), c);

			c.gridx = 2;
			c.gridy = 0;
			add(new JLabel("C"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYSCLK),
					Status.FAILURE, "Channel C system clock PLL lock lost",
					Status.GOOD, "Channel C system clock PLL locked"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_DATAINPCLK),
					Status.FAILURE, "Channel C data clock PLL lock lost",
					Status.GOOD, "Channel C data clock PLL locked"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_TRXLOCKED),
					Status.FAILURE, "Channel C half transponder lock lost",
					Status.GOOD, "Channel C half transponder locked"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_PARITYERR),
					Status.FAILURE, "Parity error on channel C", Status.GOOD,
					"No parity errors on channel C"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYNCDETECTED),
					Status.FAILURE, "Channel C sync pattern error",
					Status.GOOD, "Channel C sync pattern detected"), c);

			c.gridx = 3;
			c.gridy = 0;
			add(new JLabel("B"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYSCLK),
					Status.FAILURE, "Channel B system clock PLL lock lost",
					Status.GOOD, "Channel B system clock PLL locked"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_DATAINPCLK),
					Status.FAILURE, "Channel B data clock PLL lock lost",
					Status.GOOD, "Channel B data clock PLL locked"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_TRXLOCKED),
					Status.FAILURE, "Channel B half transponder lock lost",
					Status.GOOD, "Channel B half transponder locked"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_PARITYERR),
					Status.FAILURE, "Parity error on channel B", Status.GOOD,
					"No parity errors on channel B"), c);
			c.gridy++;
			add(new BooleanMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYNCDETECTED),
					Status.FAILURE, "Channel B sync pattern error",
					Status.GOOD, "Channel B sync pattern detected"), c);
		}

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
	}

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;

	private DebugMetaframePanel metaframePanel;
	private ParityTimerPanel parityTimerPanel;
	private ResetButtonPanel resetButtonPanel;
	private StatusRegPanel statusRegPanel;
	private SyncStatusPanel syncStatusPanel;
}

//
// O_o
