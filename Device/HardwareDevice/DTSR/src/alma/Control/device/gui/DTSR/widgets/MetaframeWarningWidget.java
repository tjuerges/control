/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.StatusIndicator;

import java.util.logging.Logger;

/**
 * <code>MetaframeWarningWidget</code> displays a single metaframe warning. The
 * warning should be on if metaframe D, C, or B are not = to each other.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 * 
 *        TODO: Factor out common behavior with other widgets to a superclass.
 */
public class MetaframeWarningWidget extends MonitorPointWidget {

	/**
	 * Construct a new MetaframeWarningWidget.
	 * 
	 * @param logger
	 * @param monitorPointModelD
	 * @param monitorPointModelC
	 * @param monitorPointModelB
	 */
	public MetaframeWarningWidget(Logger logger,
			MonitorPointPresentationModel<Integer> monitorPointModelD,
			MonitorPointPresentationModel<Integer> monitorPointModelC,
			MonitorPointPresentationModel<Integer> monitorPointModelB) {

		super(logger, true);

		this.falseStatus = Status.FAILURE;
		this.falseToolTipText = "Error: Metaframe not known";
		this.trueStatus = Status.GOOD;
		this.trueToolTipText = "All metaframes OK";

		delayD = addMonitorPointPM(monitorPointModelD, Integer.valueOf(0));
		delayC = addMonitorPointPM(monitorPointModelC, Integer.valueOf(0));
		delayB = addMonitorPointPM(monitorPointModelB, Integer.valueOf(0));

		buildWidget();
	}

	/**
	 * buildWidget is a private method which is used to construct the entire
	 * widget to be displayed on a panel
	 */
	private void buildWidget() {
		statusIndicator = new StatusIndicator(falseStatus, falseToolTipText);
		add(statusIndicator);
		addPopupMenuToComponent(statusIndicator);
	}

	@Override
	protected void updateAttention(Boolean value) {
		// TODO: remove
	}

	@Override
	protected void updateValue(IMonitorPoint aMonitorPoint) {
		Integer delayDVal_ = (Integer) getCachedValue(delayD);
		assert delayDVal_ != null;
		int delayDVal = delayDVal_;

		Integer delayCVal_ = (Integer) getCachedValue(delayC);
		assert delayCVal_ != null;
		int delayCVal = delayCVal_;

		Integer delayBVal_ = (Integer) getCachedValue(delayB);
		assert delayBVal_ != null;
		int delayBVal = delayBVal_;

		int delays[] = { delayDVal, delayCVal, delayBVal };

		long diff = 0;
		for (int i = 0; i < delays.length - 1; i++) {
			for (int j = i + 1; j < delays.length; j++) {
				diff = Math.max(diff,
						Math.abs((long) delays[i] - (long) delays[j]));
			}
		}
		final long MAX_DELAY = 0;
		if (diff > MAX_DELAY) {
			statusIndicator.setStatus(falseStatus,
					"Error: Some Metaframe Delay difference among B, C, D are greater than "
							+ MAX_DELAY);
		} else {
			statusIndicator.setStatus(trueStatus, trueToolTipText);
		}
	}

	private final IMonitorPoint delayB;
	private final IMonitorPoint delayC;
	private final IMonitorPoint delayD;

	private StatusIndicator statusIndicator;
	private final Status falseStatus;
	private final String falseToolTipText;
	private final Status trueStatus;
	private final String trueToolTipText;

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
