/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.widgets.util;

import alma.Control.device.gui.DTSReceiverCommon.util.DTSReceiverUtil;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.DtsDataCaptureWidget2;
import alma.Control.device.gui.common.widgets.DtsDataCaptureWidget2.Bit;
import alma.Control.device.gui.common.widgets.DtsDataCaptureWidget2.IPayloadHandler;

import java.awt.EventQueue;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/**
 * DataGrabber provides a widget that reads from the DTX & DTSR I2C interface
 * using the GET_I2C_DATA as specified in the ICD for said devices.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

/*
 * We extend the monitor point widget because this is the place that the monitor
 * points are checked. And we use some of the inherited classes to do that. This
 * is not a traditional monitor point because it is never displayed.
 */
public class DtsrPayloadHandler extends MonitorPointWidget implements
		IPayloadHandler<DtsrPayloadHandler.Payload> {
	/**
	 * Payload of captured data.
	 * 
	 */
	public static class Payload {
		public Payload() {
			payload = new byte[3][8];
		}

		/**
		 * Left index: use {@link DtsDataCaptureWidget2.Bit} as an index.<br>
		 * Right index: 0-7
		 */
		public final byte[][] payload;
	}

	/**
	 * Creates an interface widget attached to the given monitor points in the
	 * given device. All of the monitor points must be connected to the proper
	 * points in the DTX or DTSR.
	 * 
	 * @param logger
	 * @param dp
	 * @param payloadD
	 * @param payloadC
	 * @param payloadB
	 * @param payloadFIFOEmpty
	 * @param captureDataControl
	 */
	public DtsrPayloadHandler(Logger logger, DevicePresentationModel dp,
			MonitorPointPresentationModel<int[]> payloadD,
			MonitorPointPresentationModel<int[]> payloadC,
			MonitorPointPresentationModel<int[]> payloadB,
			MonitorPointPresentationModel<Boolean> payloadFIFOEmpty,
			ControlPointPresentationModel<Boolean> captureDataControl) {
		super(logger, true);
		// Setup D bits
		getBits = new IMonitorPoint[Bit.values().length];
		getBits[Bit.D.ordinal()] = addMonitorPointPM(payloadD, new int[8]);

		// Setup C bits
		getBits[Bit.C.ordinal()] = addMonitorPointPM(payloadC, new int[8]);

		// Setup B bits
		getBits[Bit.B.ordinal()] = addMonitorPointPM(payloadB, new int[8]);

		for (Bit bit : Bit.values()) {
			dp.moveMonitorPointToIdleGroup(getBits[bit.ordinal()]);
		}

		// Setup the rest.
		getStatus = addMonitorPointPM(payloadFIFOEmpty, Boolean.FALSE);
		dp.moveMonitorPointToIdleGroup(getStatus);
		devicePresentationModel = dp;
		this.captureData = captureDataControl;
	}

	/*
	 * Required by parent class. Unused because this point has no range
	 * checking.
	 * 
	 * @see
	 * alma.Control.device.gui.common.widgets.MonitorPointWidget#updateAttention
	 * (java.lang.Boolean)
	 */
	@Override
	public void updateAttention(Boolean value) {
		// Not applicable to this widget
	}

	/**
	 * Required by parent class; unused because this widget only reads upon
	 * request.
	 * 
	 * @see alma.Control.device.gui.common.widgets.MonitorPointWidget#updateValue(alma.Control.device.gui.common.IMonitorPoint)
	 */
	@Override
	public void updateValue(IMonitorPoint source) {
		// Nothing to do in this case.
	}

	@Override
	public String format(Payload payload, Bit bit) {
		StringBuilder sb = new StringBuilder();
		String sep = "";
		for (byte by : payload.payload[bit.ordinal()]) {
			sb.append(sep).append(String.format("%02X", by));
			sep = " ";
		}
		return sb.toString();
	}

	/*
	 * WARNING: This function should never be run in the swing dispatch thread.
	 * It can take anywhere from less than a second to tens of minutes to run.
	 * It will dispatch updates to the progress bar so that the user knows
	 * something is going on.
	 * 
	 * The FIFO empty alarm is monitored, and the function halts if the FIFO
	 * becomes empty. The data that was successfully captured will still be
	 * available for use.
	 * 
	 * Data capture: the data is captured in 192 bit collections. This amounts
	 * to 3 channels with 64 bits per channel.
	 * 
	 * CAN traffic: this function can generate a lot of traffic on the CAN bus.
	 * Currently it is limited to 8 transactions a TE.
	 * 
	 * @param dataRequested Amount of dataRequested, where 1 = 64 bits per
	 * channel (8 bytes * 3 channels)
	 * 
	 * @return % of requested data that was successfully captured.
	 */
	@Override
	public int captureData(List<Payload> capturedData, int dataRequested) {
		if (SwingUtilities.isEventDispatchThread()) {
			throw new IllegalThreadStateException(
					"Don't call me from EventDispatchThread");
		}
		stopCapture = false;
		// Capture some fresh data
		captureData.setValue(false/* deformatted data */);
		// Sleep 1 TE to let the data capture command go through
		try {
			Thread.sleep(48);
		} catch (InterruptedException e) {
			logger.severe(this.getClass().getName()
					+ ", captureData Error: failed to sleep 48ms after sending "
					+ "the fill FIFO function.");
		}
		for (int count = 0; count < dataRequested; count++) {
			// If the FIFO is empty there is no more data to get, so we stop the
			// capture and return.
			// And if the stopCapture flag has been set we stop the capture and
			// return.
			final int progress = 100 * count / dataRequested;
			if (captureProgressBar != null) {
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						captureProgressBar.setValue(progress);
					}
				});
			}

			if (fifoIsEmpty() || stopCapture) {
				return progress;
			}
			// Request a poll of each monitor point.
			for (Bit bit : Bit.values()) {
				devicePresentationModel
						.manuallyPollIdleMonitorPoint(getBits[bit.ordinal()]);
			}

			// Sleep a half TE to limit the CAN traffic.
			try {
				Thread.sleep(48 / 2);
			} catch (InterruptedException e) {
				logger.severe(this.getClass().getName()
						+ ", captureData Error: failed to sleep 48ms between reads.");
			}

			// get the data
			Payload payload = new Payload();
			for (Bit bit : Bit.values()) {
				payload.payload[bit.ordinal()] = intArrayToByteArray((int[]) getCachedValue(getBits[bit
						.ordinal()]));
			}
			capturedData.add(payload);
		}
		return 100;
	}

	/**
	 * Convert 8 integers to 8 bytes.
	 * 
	 * @param bytes
	 *            an array of byte data of length 8
	 * @return converted data.
	 */
	static byte[] intArrayToByteArray(int[] bytes) {
		DTSReceiverUtil.checkArg(bytes != null && bytes.length == 8,
				"bytes must an array of int with 8 elements.");
		byte[] result = new byte[bytes.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = (byte) bytes[i];
		}
		return result;
	}

	@Override
	public void setProgressBar(JProgressBar newProgressBar) {
		captureProgressBar = newProgressBar;
	}

	public void shutdown() {
		stopCapture = true;
	}

	/**
	 * Check if any of the device data capture FIFOs is empty. Precondition: the
	 * getStatus monitor point is in the idle group.
	 * 
	 * @return true if the any of the 3 FIFOs are empty, false if it is not.
	 */
	boolean fifoIsEmpty() {
		devicePresentationModel.manuallyPollIdleMonitorPoint(getStatus);
		Boolean isFIFOEmpty = (Boolean) getCachedValue(getStatus);
		return isFIFOEmpty;
	}

	// All sorts of variables
	private ControlPointPresentationModel<Boolean> captureData;
	private JProgressBar captureProgressBar = null;
	private IMonitorPoint[] getBits;
	private IMonitorPoint getStatus; // <Boolean>
	private boolean stopCapture;
	DevicePresentationModel devicePresentationModel; // We need this for
														// manually polling
														// monitor points.
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
