/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.StatusColor;

import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 * DfrFrameAlignerMonitorPointWidget offers a means of displaying the test data
 * current configurations.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */
public class DfrFrameAlignerMonitorPointWidget extends MonitorPointWidget {

	public DfrFrameAlignerMonitorPointWidget(Logger logger,
			MonitorPointPresentationModel<Integer> monitorPointModel) {
		super(logger, true);
		monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));
		buildWidget();
	}

	protected void buildWidget() {
		widgetLabel = new JLabel();
		widgetLabel.setText(DEFAULT_LABEL);
		widgetLabel.setForeground(StatusColor.NORMAL.getColor());
		widgetLabel.setVisible(true);
		add(widgetLabel);
		addPopupMenuToComponent(widgetLabel);
	}

	@Override
	protected void updateAttention(Boolean q) {
	}

	@Override
	// TODO: Check if there is a better way to look up values. Maybe a hash?
	protected void updateValue(IMonitorPoint source) {
		int monitorValue = (Integer) getCachedValue(monitorPoint);
		String label = "";
		final int SHIFT_COUNT = 0;
		final int MASK = SHIFT_COUNT + 1;
		for (int bitRange = 0; bitRange < BITS.length; bitRange++) {
			try {
				int bitValue = (monitorValue >>> BITS[bitRange][SHIFT_COUNT])
						& BITS[bitRange][MASK];
				if (LABELS[bitRange][bitValue].length() == 0) {
					label += LABELS[bitRange][bitValue];
				} else {
					label = LABELS[bitRange][bitValue];
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				assert false;
			}
		}
		widgetLabel.setText(label);
		return;
	}

	static final String DEFAULT_LABEL = "Normal mode(Test data output disabled)";
	static final int[][] BITS = {
			{ 4/* bits to shift right */, 0x3 /* bit mask */},
			{ 0/* bits to shift right */, 0xF /* bit mask */} };
	static final String[][] LABELS = { { DEFAULT_LABEL, // 0
			"Random number generator", // 1
			"Counting data", // 2
			"Random with re-seed every TE" // 3
	}, { "", // 0
			"all ‘0x3C’", // 1
			"all ‘0xC3’", // 2
			"all ‘0x3’", // 3
			"all ‘0xC’", // 4
			"all ‘0x5’", // 5
			"0x5555AAAA...", // 6
			"", // 7
			"all '0x0'", // 8
			"all '0x1'", // 9
			"all ‘0xA’", // 0xa
			"0xAAAA5555...", // 0xb
			"cntr & cntr &...", // 0xc
			"0xFEDCBA98_01234567_BA987654_456789AB", // 0xd
			"0x01234567_89ABCDEF_FEDCBA98_76543210", // 0xe
			"" // 0xf
	} };
	private IMonitorPoint monitorPoint;
	private JLabel widgetLabel;
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
