/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdMonitorPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.DTSR.widgets.MetaframeWarningWidget;
import alma.Control.device.gui.DTSReceiverCommon.util.AbstractSummaryPanel;
import alma.Control.device.gui.DTSReceiverCommon.util.DTSReceiverUtil;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.CombinedBooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import java.awt.Component;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to keep
 * related data together and separate unrelated data.
 * 
 * This panel displays the Summary data.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */
public class SummaryPanel extends AbstractSummaryPanel {
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;

	public SummaryPanel(Logger logger, DevicePM aDevicePM) {
		super(logger, aDevicePM, 4);
	}

	protected List<DTSReceiverUtil.Pair<String, Component>> layoutElementsOfSummaryPanel() {
		List<DTSReceiverUtil.Pair<String, Component>> elements = new ArrayList<DTSReceiverUtil.Pair<String, Component>>();

		final boolean[] theGoodValues = { true, true, true };
		elements.add(new DTSReceiverUtil.Pair<String, Component>(
				"System Clock",
				new CombinedBooleanMonitorPointWidget(
						logger,
						(MonitorPointPresentationModel<Boolean>[]) new MonitorPointPresentationModel<?>[] {
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYSCLK),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYSCLK),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYSCLK) },
						theGoodValues, Status.FAILURE,
						"Error on one or more System Clock PLL Locks",
						Status.GOOD, "System Clock PLL Lock OK on all Chanels")));

		elements.add(new DTSReceiverUtil.Pair<String, Component>(
				"Parity Error",
				new CombinedBooleanMonitorPointWidget(
						logger,
						(MonitorPointPresentationModel<Boolean>[]) new MonitorPointPresentationModel<?>[] {
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_PARITYERR),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_PARITYERR),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_PARITYERR) },
						theGoodValues, Status.FAILURE,
						"Parity error on one or more channels", Status.GOOD,
						"No parity errors on any chanels")));

		elements.add(new DTSReceiverUtil.Pair<String, Component>(
				"Data Clock",
				new CombinedBooleanMonitorPointWidget(
						logger,
						(MonitorPointPresentationModel<Boolean>[]) new MonitorPointPresentationModel<?>[] {
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_DATAINPCLK),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_DATAINPCLK),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_DATAINPCLK) },
						theGoodValues, Status.FAILURE,
						"Data clock error on one or more channels",
						Status.GOOD, "No data clock errors on any chanels")));

		elements.add(new DTSReceiverUtil.Pair<String, Component>(
				"Sync Pattern",
				new CombinedBooleanMonitorPointWidget(
						logger,
						(MonitorPointPresentationModel<Boolean>[]) new MonitorPointPresentationModel<?>[] {
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYNCDETECTED),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYNCDETECTED),
								devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYNCDETECTED) },
						theGoodValues, Status.FAILURE,
						"Error: Sync pattern lost on one or more channels",
						Status.GOOD, "Sync Pattern detected on all channels")));

		elements.add(new DTSReceiverUtil.Pair<String, Component>(
				"Metaframe",
				new MetaframeWarningWidget(
						logger,
						devicePM.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_D),
						devicePM.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_C),
						devicePM.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_B))));

		elements.add(new DTSReceiverUtil.Pair<String, Component>("Delay C",
				new IntegerMonitorPointWidget(logger, devicePM
						.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_C))));
		return elements;
	}
}

//
// O_o
