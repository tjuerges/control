/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.StatusColor;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * SyncStatusMonitorPointWidget offers a JPanel containing a textual
 * representation of an integer (in hex or dec format) from a monitor point with
 * range and communication verification/alarms. Note that this widget doesn't
 * support drawing graph.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id: SyncStatusMonitorPointWidget.java,v 1.1 2011/03/03 07:54:12
 *          konakamu Exp $
 * @since ALMA 8.1.0
 * 
 */
public class SyncStatusMonitorPointWidget extends MonitorPointWidget {

	/**
	 * Construct a new SyncStatusMonitorPointWidget
	 * 
	 * @param logger
	 *            The logger that all logs should be written to.
	 * @param LsMonitorPointModel
	 *            Less significant byte of Sync Status
	 * @param MsMonitorPointModel
	 *            Most significant bit of Sync Status
	 */
	public SyncStatusMonitorPointWidget(Logger logger,
			MonitorPointPresentationModel<Integer> LsMonitorPointModel,
			MonitorPointPresentationModel<Integer> MsMonitorPointModel) {
		super(logger, true);

		LsbMonitorPoint = addMonitorPointPM(LsMonitorPointModel,
				Integer.valueOf(0));
		MsbMonitorPoint = addMonitorPointPM(MsMonitorPointModel,
				Integer.valueOf(0));
		toolTipText = String.format("Operating range: %d to %d", 0, 0x1ff);

		buildWidget();
	}

	protected void buildWidget() {
		widgetLabel = new JLabel();
		widgetLabel.setText("0");
		widgetLabel.setForeground(StatusColor.NORMAL.getColor());
		widgetLabel.setToolTipText(toolTipText);
		add(widgetLabel);
		addPopupMenuToComponent(widgetLabel);
	}

	@Override
	protected void updateAttention(Boolean q) {
	}

	@Override
	protected void updateValue(IMonitorPoint source) {
		Integer msb = (Integer) getCachedValue(MsbMonitorPoint);
		assert msb != null && 0 <= msb && msb <= 1;
		int output = msb * 0x100;

		Integer lsb = (Integer) getCachedValue(LsbMonitorPoint);
		assert lsb != null && lsb < 0x100;
		output += lsb;

		widgetLabel.setText(String.format("%d", output));
	}

	private IMonitorPoint LsbMonitorPoint;
	private IMonitorPoint MsbMonitorPoint;
	private String toolTipText;
	private JLabel widgetLabel;
	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o
