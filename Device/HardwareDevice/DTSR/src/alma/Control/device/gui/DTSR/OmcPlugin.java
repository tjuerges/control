/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR;

import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardPlugin;
import alma.common.gui.chessboard.ChessboardStatusProvider;

/**
 * A plug-in to the Operator Master Client that will display a OmcPlugin. This
 * allows a user to select a hardware device they are interested in by antenna,
 * and display a panel containing control and monitor points for that hardware
 * device.
 * 
 * See: http://almasw.hq.eso.org/almasw/bin/view/EXEC/SubsystemPlugins for a
 * description of the Operator Master Client plug-in interface.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @since ALMA 8.1.0
 */
public class OmcPlugin extends ChessboardPlugin {
	// TODO: serial version UID
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see alma.common.gui.chessboard.ChessboardPlugin#getStatusProvider()
	 */
	@Override
	protected ChessboardStatusProvider getStatusProvider() {
		// TODO make it clear why + "0" ?
		return new HardwareDeviceChessboardPresentationModel(
				pluginContainerServices, DevicePM.getDeviceName() + "0");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alma.common.gui.chessboard.ChessboardPlugin#getDetailsProvider()
	 */
	@Override
	protected ChessboardDetailsPluginFactory getDetailsProvider() {
		return new DetailsPluginFactory();
	}
}

//
// O_o
