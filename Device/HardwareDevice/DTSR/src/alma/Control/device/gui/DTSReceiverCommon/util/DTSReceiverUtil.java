/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSReceiverCommon.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class that is common to DTS receivers(DRX, DTSR).
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */
public class DTSReceiverUtil {
	/**
	 * Returns alternateValue if value is identical with or equals to
	 * invalidValue. Otherwise returns value.
	 * 
	 * @param <T>
	 * @param value
	 *            The value to be tested and returned if not invalid.
	 * @param invalidValue
	 *            The invalid value.
	 * @param alternateValue
	 *            The value returned if value is invalid.
	 * @return Returns alternateValue if value is identical with or equals to
	 *         invalidValue. Otherwise returns value.
	 */
	public static <T> T normalize(T value, T invalidValue, T alternateValue) {
		if (invalidValue == null) {
			if (value == invalidValue) {
				return alternateValue;
			}
			return value;
		}
		if (value == invalidValue || invalidValue.equals(value)) {
			return alternateValue;
		}
		return value;
	}

	public static void checkArg(boolean isValid, String message) {
		if (! isValid) {
			IllegalArgumentException e = new IllegalArgumentException(message);
			StackTraceElement stackTrace[] = e.getStackTrace();
			e.setStackTrace(Arrays.copyOfRange(stackTrace, 1, stackTrace.length));
			throw e;
		}
	}

	/**
	 * Generic Key-Valur Pair class.
	 * 
	 * @param <TKey>
	 *            Type of key.
	 * @param <TValue>
	 *            Type of value.
	 * @author Kohji Nakamura k.nakamura@nao.ac.jp
	 * 
	 */
	public static class Pair<TKey, TValue> {
		/**
		 * Constructor.
		 * 
		 * @param key
		 * @param value
		 */
		public Pair(TKey key, TValue value) {
			this.key = key;
			this.value = value;
		}

		public TKey key;
		public TValue value;
	}

	static Class<?>[][] typeMaping = new Class<?>[][] {
			{ Boolean[].class, boolean.class },
			{ boolean[].class, boolean.class },

			{ Byte[].class, byte.class }, { byte[].class, byte.class },

			{ Short[].class, short.class }, { short[].class, short.class },

			{ Integer[].class, int.class }, { int[].class, int.class },

			{ Long[].class, long.class }, { long[].class, long.class },

			{ Float[].class, float.class }, { float[].class, float.class },

			{ Double[].class, double.class }, { double[].class, double.class }, };

	static Map<Class<?>, Class<?>> typeMap = new HashMap<Class<?>, Class<?>>();

	static {
		for (Class<?>[] pair : typeMaping) {
			typeMap.put(pair[0], pair[1]);
			if (pair[0].isArray()) {
				typeMap.put(pair[0].getComponentType(), pair[1]);
			}
		}
	}

	/**
	 * Returns a corresponding class for clazz. <br/>
	 * For example, this method returns "byte" class for all "Byte", "byte",
	 * "Byte[]" and "byte[]" clazz parameter.
	 * 
	 * @param clazz
	 * @return a class object, or null if there is no corresponding class.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> getCorrespondingMonitorAndControlPointType(
			Class<T> clazz) {
		if (typeMap.containsKey(clazz)) {
			return (Class<T>)typeMap.get(clazz);
		}
		return null;
	}
}
