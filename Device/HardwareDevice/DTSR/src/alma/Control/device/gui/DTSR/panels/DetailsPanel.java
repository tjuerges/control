/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdMonitorPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.ShowHideButton;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to keep
 * related data together and separate unrelated data.
 * 
 * This class contains the Detail data. It also provides a Show/Hide button for
 * the data.
 * 
 * Tabed panels are used to group related data together and separate unrelated
 * data. These are created as required for each GUI.
 * 
 * @author Kohji Nakamura k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

public class DetailsPanel extends DevicePanel {

	public DetailsPanel(Logger logger, DevicePM aDevicePM) {
		super(logger, aDevicePM);
		customizeMonitorPoints();
		buildSubPanels(logger, aDevicePM);
		buildPanel();
	}

	private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
		deformatterPanel = new DeformatterStatusPanel(logger, aDevicePM);
		transponderPanel = new TransponderPanel(logger, aDevicePM);
		debugPanel = new DebugPanel(logger, aDevicePM);
		deviceInfoPanel = new DeviceInfoPanel(logger, aDevicePM);
	}

	protected void buildPanel() {
		this.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("Details"),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)));

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.NORTHWEST;
		c.weighty = 1;
		c.weightx = 100;
		c.gridx = 0;
		c.gridy = 0;
		JTabbedPane tabs = new JTabbedPane();
		ShowHideButton<JTabbedPane> showHideButton = new ShowHideButton<JTabbedPane>(
				tabs, new String[] { "Show Details", "Hide Details" });
		add(showHideButton, c);
		c.anchor = GridBagConstraints.CENTER;
		c.weighty = 100;
		tabs.add("Deformatter", new JScrollPane(deformatterPanel));
		tabs.add("Transponder", new JScrollPane(transponderPanel));
		tabs.add("Debug", debugPanel);
		tabs.add("Device Info", new JScrollPane(deviceInfoPanel));
		c.anchor = GridBagConstraints.CENTER;
		c.gridy++;
		add(tabs, c);

		this.setVisible(true);
		showHideButton.updateAppearance();
	}

	/**
	 * Setup the display settings for the monitor points that need it. All
	 * internal values should be in SI units. But some values should be
	 * displaysed in something other than SI. E.g most users expect temperature
	 * in C.
	 */
	private void customizeMonitorPoints() {
		devicePM.getMonitorPointPM(IcdMonitorPoints.AMBIENT_TEMPERATURE)
				.getMonitorPoint().setDisplayOffset(-273.15);
		devicePM.getMonitorPointPM(IcdMonitorPoints.AMBIENT_TEMPERATURE)
				.getMonitorPoint().setDisplayUnits("\u00B0C");
		devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_D)
				.getMonitorPoint().setDisplayScale(1000000000);
		devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_C)
				.getMonitorPoint().setDisplayScale(1000000000);
		devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_nW_B)
				.getMonitorPoint().setDisplayScale(1000000000);
	}

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;

	private DebugPanel debugPanel;
	private DeformatterStatusPanel deformatterPanel;
	private DeviceInfoPanel deviceInfoPanel;
	private TransponderPanel transponderPanel;

}

//
// O_o
