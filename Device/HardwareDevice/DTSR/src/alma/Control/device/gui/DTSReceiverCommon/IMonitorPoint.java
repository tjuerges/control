/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 */

package alma.Control.device.gui.DTSReceiverCommon;

/**
 * Generic version of {@link alma.Control.device.gui.common.IMonitorPoint}.
 * 
 * @author Kohji Nakamura	k.nakamura@nao.ac.jp
 *
 * @param <T> type of the monitor point
 * @version $Id$
 * @since   ALMA 8.1.0
 */
public interface IMonitorPoint<T> extends
		alma.Control.device.gui.common.IMonitorPoint {
    /**
     * Identify the class type for this monitor point.
     * 
     * @return the class of this monitor point.
     */
    public Class<T> getTypeClass();
}
