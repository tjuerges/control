/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTSR.panels;

import alma.Control.device.gui.DTSR.IcdControlPoints;
import alma.Control.device.gui.DTSR.IcdMonitorPoints;
import alma.Control.device.gui.DTSR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.AmbsiPanel;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * This panel displays the Ambsi panel and the version information panel.
 * 
 * @author Kohji Nakamura	k.nakamura@nao.ac.jp
 * @version $Id$
 * @since ALMA 8.1.0
 */

class DeviceInfoPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    DeviceInfoPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        ambsiPanel = new AmbsiPanel(logger, aDevicePM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.SERIAL_NUMBER),
                devicePM.getMonitorPointPM(IcdMonitorPoints.AMBIENT_TEMPERATURE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.TRANS_NUM),
                devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_SLAVE_ERROR_CODE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.CAN_ERROR_COUNT),
                devicePM.getMonitorPointPM(IcdMonitorPoints.ERROR_CODE_LAST_CAN_ERROR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PATCH_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_PATCH_LEVEL),
                devicePM.getControlPointPM(IcdControlPoints.RESET_AMBSI));
        versionInfoPanel = new VersionInfoPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(ambsiPanel, c);
        c.gridx++;
        add(versionInfoPanel, c);

        setVisible(true);
    }
    
    private AmbsiPanel ambsiPanel;
    private VersionInfoPanel versionInfoPanel;
}

//
// O_o
