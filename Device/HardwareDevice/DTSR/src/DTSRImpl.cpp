/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2002
* (c) Associated Universities Inc., 2002
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* File DTSRImpl.cpp
*
*/

#include <DTSRImpl.h>
#include <string>
#include <TETimeUtil.h>
#include <acstimeTimeUtil.h>

using std::string;

/**
 *-----------------------
 * DTSR Constructor
 *-----------------------
 */
DTSRImpl::DTSRImpl(const ACE_CString& name, maci::ContainerServices* cs) :
    DTSRBase(name,cs),
    resetThreadCycleTime_m(TETimeUtil::ACS_ONE_SECOND)

{
  ACS_TRACE("DTSRImpl::DTSRImpl");
}

/**
 *-----------------------
 * DTSR Destructor
 *-----------------------
 */
DTSRImpl::~DTSRImpl()
{
  ACS_TRACE("DTSRImpl::~DTSRImpl");
  sem_destroy(&resetThreadSem_m);
}

/**
 *---------------------------------
 * ACS Component Lifecycle Methods
 *---------------------------------
 */
void DTSRImpl::initialize()
{
  const char* fnName = "DTSRImpl::initialize";
  ACS_TRACE(fnName);

  diagnosticModeEnabled        = true;

  try {
    DTSRBase::initialize();
  }
  catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,fnName);
  }

  if (sem_init(&resetThreadSem_m, 0, 1)) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Error Initializing Semaphore."));
    throw acsErrTypeLifeCycle::LifeCycleExImpl(__FILE__,__LINE__,fnName);
  }

  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
  std::string threadName(compName.in());
  threadName += "::resetThread";
  resetThread_p = getContainerServices()->getThreadManager()->
    create<ResetThread, DTSRImpl>(threadName.c_str(), *this);
  resetThread_p->setSleepTime(resetThreadCycleTime_m);

}

void DTSRImpl::cleanUp()
{
  const char* fnName = "DTSRImpl::cleanUp";
  ACS_TRACE(fnName);

  resetThread_p->terminate();

  try {
    DTSRBase::cleanUp();
  }
  catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,fnName);
  }
}

/**
 *----------------------------
 * Hardware Lifecycle Methods
 *----------------------------
 */
void DTSRImpl::hwInitializeAction()
{
  const char* fnName = "DTSRImpl::hwInitializeAction";
  ACS_TRACE(fnName);

  // Call the base class implementation
  DTSRBase::hwInitializeAction();

  resetThread_p ->suspend();

  // set configured clock edge to time in
  try {
  configClockEdge();
  }
  catch(const DTSRExceptions::ConfigClockEdgeExImpl& ex) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Failure while configuring clock edge."));
    // report error
    return;
  }
}

void DTSRImpl::hwOperationalAction()
{
  const char* fnName = "DTSRImpl::hwOperationalAction";
  ACS_TRACE(fnName);

  // Call the base class implementation
  DTSRBase::hwOperationalAction();

  // complete reset of DFR
  try {
  ACS_LOG(LM_SOURCE_INFO,fnName,(LM_DEBUG,"Sending DFR_RESET_ALL(128)"));
  setDfrResetAll(128);
  sleep(3);
  }
  catch(const ControlExceptions::CAMBErrorExImpl& ex) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Failed to reset DFR."));
    throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__, fnName);
  }
  catch(const ControlExceptions::INACTErrorExImpl& ex) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Invalid State to reset DFR"));
    throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__, fnName);
  }

  resetThread_p->resume();
}

void DTSRImpl::hwStopAction()
{
  const char* fnName = "DTSRImpl::hwStopAction";
  ACS_TRACE(fnName);

  AmbErrorCode_t flushStatus;
  ACS::Time      flushTime;

  if (isStartup()){
    /* Flush all commands */
    flushNode(0, &flushTime, &flushStatus);
    if (flushStatus != AMBERR_NOERR) {
      ACS_LOG(LM_SOURCE_INFO, fnName,(LM_INFO,"Communication failure flushing commands to device"));
    }
    else {
      ACS_LOG(LM_SOURCE_INFO, fnName,(LM_DEBUG,"All commands and monitors flushed at: %lld",flushTime));
    }
  }
  /* Suspend the threads */
  resetThread_p->suspend();

  // Call the base class implementation
  DTSRBase::hwStopAction();
}

/* ===================  Threads   ========================= */
void DTSRImpl::resetThreadAction() {
  const char* fnName = "DTSRImpl::resetThreadAction";
  try{
    setClrAlarmStatusRegAll();
    setClrPowerAlarmRegAll();
    setDfrResetAll(1);
  }
  catch(const ControlExceptions::CAMBErrorExImpl& ex)
  {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_WARNING,"CAMBErrorEx catched"));
  }
  catch(const ControlExceptions::INACTErrorExImpl& ex)
  {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_WARNING,"INACTErrorEx catched"));
  }
}


DTSRImpl::ResetThread::ResetThread(const ACE_CString&  name,
                                  DTSRImpl&           DTSR):
  ACS::Thread(name),
  drxDevice_p(DTSR)
{
  const string fnName = "DTSRImpl::ResetThread::ResetThread";
  ACS_TRACE(fnName);
}

void DTSRImpl::ResetThread::runLoop() {
  drxDevice_p.resetThreadAction();
}

void DTSRImpl::configClockEdge()
{
  const char* fnName = "DTSRImpl::configClockEdge";
  ACS_TRACE(fnName);

  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
  string fullName = string("alma/") + compName.in();
  string useNegClkEdge;

  // Get the reference of the component
  CDB::DAL_ptr dal_p = getContainerServices()->getCDB();
  CDB::DAO_ptr dao_p = dal_p->get_DAO_Servant(fullName.c_str());

  // get value from CDB
  try {
    useNegClkEdge = dao_p->get_field_data("USE_NEG_CLOCK_EDGE");
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_DEBUG, "Use Negative Clock Edge: %s", useNegClkEdge.c_str()));
  }
  catch(const cdbErrType::WrongCDBDataTypeEx& ex) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Failed to get clock edge config from CDB: Bad Data Type."));
    throw DTSRExceptions::ConfigClockEdgeExImpl(ex, __FILE__, __LINE__, fnName);
  }
  catch(const cdbErrType::CDBFieldDoesNotExistEx& ex) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Failed to get clock edge config from CDB: Field does not exist."));
    throw DTSRExceptions::ConfigClockEdgeExImpl(ex, __FILE__, __LINE__, fnName);
  }

  try {
    if (useNegClkEdge == "true") {
      unsigned char ResetTeWord = 0x02;
      //setResetTeErrs(ResetTeWord);
      ACS_LOG(LM_SOURCE_INFO,fnName,(LM_DEBUG, "Comparing TE with Negative Clk Edge: FR_TE_RESET(0x2)"));
    }
    else if (useNegClkEdge == "false") {
      unsigned char ResetTeWord = 0x00;
      //setResetTeErrs(ResetTeWord);
      ACS_LOG(LM_SOURCE_INFO,fnName,(LM_DEBUG, "Comparing TE with Positive Clk Edge: FR_TE_RESET(0x00)"));
    }
    else {
      ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Invalid data in USE_NEG_CLOCK_EDGE attribute."));
      throw DTSRExceptions::ConfigClockEdgeExImpl(__FILE__, __LINE__, fnName);
    }
  }
  catch(const ControlExceptions::CAMBErrorExImpl& ex) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Failed set clock edge: CAMB error."));
    throw DTSRExceptions::ConfigClockEdgeExImpl(ex, __FILE__, __LINE__, fnName);
  }
  catch(const ControlExceptions::INACTErrorExImpl& ex) {
    ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Failed set clock edge: Invalid State."));
    throw DTSRExceptions::ConfigClockEdgeExImpl(ex, __FILE__, __LINE__, fnName);
  }
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(DTSRImpl)

/*___oOo___*/
