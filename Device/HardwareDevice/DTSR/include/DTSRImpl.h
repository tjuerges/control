#ifndef DTSRImpl_H
#define DTSRImpl_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2002
* (c) Associated Universities Inc., 2002
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* File DTSRImpl.h
*
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

// Base class
#include <DTSRS.h>
#include <DTSRBase.h>
#include <DTSRExceptions.h>

class DTSRImpl: public DTSRBase, 
               public virtual POA_Control::DTSR
{
 protected:
  /* ------------ Writer Thread Class -------------- */
  class ResetThread: public ACS::Thread {
  public:
    ResetThread(const ACE_CString& name, DTSRImpl& DTSR);
    virtual void runLoop();
  protected:
    DTSRImpl& drxDevice_p;
  };
 
 public:
  friend class DTSRImpl::ResetThread;

    /**
     * Constructor
     */
    DTSRImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    ~DTSRImpl();

    /**
     * ACS Component Lifecycle Methods
     */
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

 protected:

    /**
     * Hardware Lifecycle Methods
     */
    virtual void hwInitializeAction();
    virtual void hwOperationalAction();
    virtual void hwStopAction();    

    /// \exception DTSRExceptions::ConfigClockEdgeExImpl
    virtual void configClockEdge();

 private:

    virtual void resetThreadAction();

    sem_t resetThreadSem_m;
    ResetThread* resetThread_p;
    const ACS::Time resetThreadCycleTime_m;

};

#endif /*!DTSRImpl_H*/
