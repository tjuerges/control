/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File LLCHWSimImpl.h
 *
 * $Id$
 */
#ifndef LLCHWSimImpl_H
#define LLCHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "LLCHWSimBase.h"

namespace AMB
{
    /* Please use this class to implement complex functionality for the
     * LLCHWSimBase helper functions. Use AMB::TypeConvertion methods
     * for convenient type convertions. */
    class LLCHWSimImpl : public LLCHWSimBase
    {
      public :
	LLCHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);
	~LLCHWSimImpl(){};

        /// Monitor and Control Points creation (state_m map entries)
        /// and value initialization
        /// \param node Node ID of this device
        /// \param serialNumber S/N of this device
        /// \exception CAN::Error
        virtual void initialize(AMB::node_t node,
            const std::vector< CAN::byte_t >& serialNumber);

        /* Override the control method */
        void control(AMB::rca_t rca, const std::vector< CAN::byte_t >& data);

    protected:
        ACS::Time  lastResetTime_m;
        double     lastResetVoltage_m;

        void setStrchrToLvlSim(const std::vector< CAN::byte_t >& data);

    }; // class LLCHWSimImpl

} // namespace AMB

#endif // LLCHWSimImpl_H
