#ifndef STRAIGHTLINEFIT_H
#define STRAIGHTLINEFIT_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2008 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jsepulve  2008-03-09  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */


#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acstime.h>

class StraightLineFit {
 private:
  std::vector<float> data_x;
  std::vector<float> data_y;
 public:
  StraightLineFit(const std::vector<float>& xArray, 
                  const std::vector<float>& yArray);
  StraightLineFit(const std::vector<float>& xArray);
 
  ~StraightLineFit();

  float getSlope();
  float getConstant ();

 private:
  void linefit();
  float average (const std::vector<float>& data);

  float slope;
  float constant;

  float average_y;
  float average_x;
  const static float max_slope=100;
};





#endif /*STRAIGHTLINEFIT_H*/
