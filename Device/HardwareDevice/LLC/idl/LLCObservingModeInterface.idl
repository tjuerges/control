/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LLC.idl
 */

#ifndef LLCOBSERVINGMODEINTERFACE_IDL
#define LLCOBSERVINGMODEINTERFACE_IDL

/**
 * This defines an external interface to be used by all observing modes
 * which require the use of the LLC.
 */

#include <ControlExceptions.idl>
#include <ControlCommonExceptions.idl>
#include <ControlCommonCallbacks.idl>

#pragma prefix "alma"

module Control {
  interface LLCObservingModeInterface {
    /**
     * Method to enable / disable line length correction in the LLC
     */
    void enableLLCCorrection(in boolean enable)
      raises(ControlCommonExceptions::AsynchronousFailureEx);
      
    /**
     * Method to get the current state of the length correction
     */
    boolean isLLCCorrectionEnabled()
      raises(ControlCommonExceptions::AsynchronousFailureEx);


    /**
     * This method determines the maximum (estimated) time before one
     * of the LLCs associated with this array needs to be reset.
     * @Exception ControlCommonExceptions::AsynchronousFailure
     */
    long long getTimeToLLCReset()
      raises(ControlCommonExceptions::AsynchronousFailureEx);

    /**
     * This method will cause all LLCs in the Array to be reset
     * to the optimum value of the stretcher.
     * Note: Change return type
     */
    void resetLLCStretchers()
      raises (ControlCommonExceptions::AsynchronousFailureEx,
              ControlCommonExceptions::DataCapturerErrorEx);

    /* This method will cause all LLCs in the Array to be reset
     * to the specified fraction of the total travel
     */
    void setLLCStretcherPosition(in float position)
      raises (ControlCommonExceptions::AsynchronousFailureEx,
              ControlCommonExceptions::DataCapturerErrorEx);
    
    /**
     * This method checks if a polarization calibration is required
     * on any LLC associated with this array.
     */
    boolean getLLCPolarizationCalRequired()
      raises (ControlCommonExceptions::AsynchronousFailureEx);

    /**
     * This method will cause all LLCs associated with this array
     * to perform a polarization calibration.  If the force calibration
     * flag is true then the calibration is always performed, if it is
     * false then the calibartion is performed only if required on a LLC
     * by LLC basis.
     *
     * The timeout parameter is the client side timeout (in seconds)
     * 
     * Because this is a long operation only asynchronous methods are 
     * supported
     */
    oneway void doLLCPolarizationCalibrationAsynch(in long    timeout,
                                                   in boolean forceCalibration,
                                                   in SimpleCallback cb);
  };

};


#endif /* LLCOBSERVINGMODEINTERFACE_IDL */
