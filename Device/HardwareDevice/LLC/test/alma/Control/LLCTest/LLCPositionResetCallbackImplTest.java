/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/
/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.LLCTest;

//import java.util.HashMap;
//import java.util.Iterator;
import java.util.ArrayList;

//import alma.acs.component.ComponentQueryDescriptor;

import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

import alma.Control.LLCStretcherResetData;

import alma.Control.LLCPackage.LLCPositionResetData_t;
import alma.Control.LLCPackage.LLCPositionResetCallbackImpl;

import alma.ACSErrTypeOK.wrappers.ACSErrOKAcsJCompletion;
//import alma.acs.exceptions.AcsJCompletion;

//import alma.ControlTest.CallbackTestTarget;
//import alma.ControlTest.CallbackTestTargetHelper;

//import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
//import alma.ControlExceptions.wrappers.AcsJTimeoutEx;


public class LLCPositionResetCallbackImplTest extends ComponentClientTestCase {

    public LLCPositionResetCallbackImplTest(String test) throws Exception {
        super(test);
    }

    public LLCPositionResetCallbackImplTest() throws Exception {
        super(LLCPositionResetCallbackImplTest.class.getName());
    }

    private ContainerServices cs;

//     private HashMap<String, CallbackTestTarget> targets = 
//         new HashMap<String, CallbackTestTarget>();


    ////////////////////////////////////////////////////////////////////
    // Test fixture methods
    //
    // We assume that the basic callback mechanism works since we
    // get that from the base class.  We just need to test the java 
    // interface
    ////////////////////////////////////////////////////////////////////

    protected void setUp() throws Exception {
        super.setUp();
        this.cs = getContainerServices();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    ////////////////////////////////////////////////////////////////////
    // Test cases
    ////////////////////////////////////////////////////////////////////
    public void testTrivialCase() throws Exception {
        /* This simple case just creates a Callback Object and waits
           without actually requiring any responses */
        LLCPositionResetCallbackImpl cb = new LLCPositionResetCallbackImpl(cs);
        cb.waitForCompletion(1);
    }

    public void testGettingStretcherResetData() throws Exception {
        LLCPositionResetCallbackImpl cb = new LLCPositionResetCallbackImpl(cs);


        cb.addExpectedResponse("DV01");
        {
            LLCPositionResetData_t response = new LLCPositionResetData_t();
            response.fringeCounter = 1;
            response.deltaVF       = 1.0;
            response.deltaVS       = 1.5;
            cb.report("DV01", response,
                      new ACSErrOKAcsJCompletion().toCorbaCompletion());
        }


        cb.addExpectedResponse("DV02");
        {
            LLCPositionResetData_t response = new LLCPositionResetData_t();
            response.fringeCounter = 2;
            response.deltaVF       = 2.0;
            response.deltaVS       = 2.5;
            cb.report("DV02", response,
                      new ACSErrOKAcsJCompletion().toCorbaCompletion());
        }

        cb.waitForCompletion(1);

        LLCStretcherResetData[] resetData = cb.getLLCStretcherResetData();
        //new ArrayList<LLCStretcherResetData>(cb.getLLCStretcherResetData());
        assertEquals(2, resetData.length);

        for (int idx = 0; idx < resetData.length; idx++) {
             if (resetData[idx].antennaName.equals("DV01")) {
                 assertEquals(resetData[idx].fringeCounter, 1);
                 assertEquals(resetData[idx].fastStretcherVoltageChange, 1.0);
             } else if (resetData[idx].antennaName.equals("DV02")) {
                 assertEquals(resetData[idx].fringeCounter, 2);
                 assertEquals(resetData[idx].fastStretcherVoltageChange, 2.0);
             } else {
                 fail ("Unexpected antenna response: " +
                       resetData[idx].antennaName);
             }
        }
    }

}
