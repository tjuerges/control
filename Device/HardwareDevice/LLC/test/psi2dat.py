#!/usr/bin/python

import string
import sys
import os
from math import *


if len(sys.argv)< 2:
   print
   '''Insert PSI file name
   CLO-1-LLC----0x018-2008-03-16-LLC_02.csv
   '''
   
else:
   filename = sys.argv[1]
   #dirname  = filename.split('.')[0]
   dirname  = filename.split('.')[0].split('----')[1].split('_')[0]

   ts_stored = 0
   counter   = 0
   period    = 60
   cnt_period= 0
   init_ts   = 0
   line_by_hour = []
   dict_by_hour = {}

   if not os.path.isdir(dirname):
       os.mkdir(dirname)

   input    = open(filename,'r')
   source   = open(dirname + '/out_source.dat','w')
   output   = open(dirname + '/out_' + str(period)+ '.dat','w')
   out_hour = open(dirname + '/out_0.dat','w')
   data     = input.readlines()


   for i in range (len(data)):
       line = string.split(data[i])

       if int(i/3600) > counter:
          counter+=1
          out_hour.close()
          out_hour = open(dirname + '/out_' + str(counter) + '.dat','w')

       try:
          ts = int(float(line[0]))
          value = float(line[4])

          if init_ts == 0:
             init_ts = ts

          epoch = ts - init_ts
          
          print >> out_hour, "%d %s %s" % ( ceil(epoch), ts, value)
          print >> source,   "%d %s %s" % ( ceil(epoch), ts, value)

          if ts >= ts_stored:
             ts_stored = ts + period
             print >> output, "%d %s %s" % (ceil(epoch/60.0), ts, value)

       except Exception, e:
          print e

   output.close()
   input.close()
   out_hour.close()
   source.close()
