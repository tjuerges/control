#! /usr/bin/env python

import ambManager
import struct
import ControlExceptions
import Acspy.Util
import string
import sys
import os
from time import sleep

try:
   if len(sys.argv)< 2:
      print "Insert file name"
   else:
      filename = sys.argv[1]
      dirname  = filename.split('.')[0]

      try:
         input    = open(filename,'r')
         data     = input.readlines()
         input.close()
         mgr=ambManager.AmbManager("DV01")

      except Exception,e:
         print e
         sys.exit(0)

      for i in range (len(data)):
         line = string.split(data[i])
         adc  = int((float(line[len(line)-1])*1024)/5)
         print "set VF value: %s %f" %(adc, (float(adc)/1024)*5)
         mgr.command(0, 0x18, 0x0107, struct.pack(">H",adc))
         sleep(1)
         #struct.unpack(">h",mgr.monitor(0,0x18,0x107)[0])[0]

      del mgr
      output.close()
      input.close()

except (KeyboardInterrupt, SystemExit):
   print "Ctrl-C"
   del mgr

except Exception,e:
   print e
   sys.exit(0)
