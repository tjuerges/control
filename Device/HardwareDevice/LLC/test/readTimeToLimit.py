#! /usr/bin/env python

import ambManager
import struct
import ControlExceptions
import Acspy.Util
import string
import sys
import os
from time import sleep
from CCL.LLC import LLC
from Acspy.Common import TimeHelper
from math import *


class timeToLimit :
   '''
   Return a string of LLC time to reset and vf values
   '''
   def __init__ (self, antennaName='DV01', interval=60.0):
      self.helper   = TimeHelper.TimeUtil()
      self.interval = interval
      self.init_ts  = 0
      self.slope    = 0
      try:
         self.llc      = LLC(antennaName)
      except Exception,e:
         del self.helper
         raise Exception,e

   def __del__ (self):
     del self.llc
     del self.helper

   def string (self):
      line = ""
      if self.state():
         (counter,ts)= self.llc.GET_COUNTER()
         (vf,ts)     = self.llc.GET_VF()
         timeToReset = self.llc.getTimeToLimit()
         slope       = self.llc.getVfLinearSlope ()
         if self.init_ts == 0:
            self.init_ts = self.helper.epoch2py(ts)

         epoch = self.helper.epoch2py(ts) - self.init_ts
         
         line = str(int(float(epoch)/self.interval)) + " " + \
                str(self.helper.epoch2py(ts)) + " " + \
                str(slope)+ " " + \
                str(counter)   + " " + \
                str(timeToReset) + " " + \
                str(vf)
      return line

   def state (self):
      if str(self.llc.getHwState () == 'Operational' ):
         return 1
      return 0
   def time_period (self):
      return self.interval


def main (argv=None):
   if argv is None:
      argv = sys.argv

   if len(argv) < 2:
      print """
      Usage: readTimeToLimit <DV01|DA41> [60]
      antenna name : DV01 | DA41
      sample time  : seconds, default 60 sec.
      """
      sys.exit(1)
   head = "#number time slope counter timeLimit vf" 
   antennaName = str(argv[1]).upper()
   filename = str(antennaName + '.dat') 
   period   = 60.0

   if len(argv) > 2 :
      period = float(argv[2])

   try:
      out      = open(filename, 'w')
      print >> out, head
      obj = timeToLimit(antennaName,period)

      while obj.state():
         print "%s" % (obj.string())
         print >> out, "%s" % (obj.string())
         sleep (obj.time_period())

   except (KeyboardInterrupt, SystemExit):
      print "Ctrl-C"
      out.close ()
      del obj
      sys.exit(0)
   except Exception,e:
      print e
      sys.exit(0)






if __name__ == "__main__":
    sys.exit(main())

