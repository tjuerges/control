#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

import sys
import unittest

from CCL.LLC import LLC
from Control import HardwareDevice


class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)

    def setUp(self):
        self.llc = LLC("DA41",stickyFlag=True)
        self.failIf(self.llc == None, "Failed to activate component")
        self.llc.hwStop()
        self.failUnlessEqual(self.llc.getHwState(), HardwareDevice.Stop,
                             "Unable to get LLC to Stop State")

        self.llc.hwStart()
        self.failUnlessEqual(self.llc.getHwState(), HardwareDevice.Start,
                             "Unable to get LLC to Start State")
        self.llc.hwConfigure()
        self.failUnlessEqual(self.llc.getHwState(), HardwareDevice.Configure,
                             "Unable to get LLC to Configure State")
        self.llc.hwInitialize()
        self.failUnlessEqual(self.llc.getHwState(), HardwareDevice.Initialize,
                             "Unable to get LLC to Initialize State")
        self.llc.hwOperational()
        self.failUnlessEqual(self.llc.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get LLC to Operational State")

    def tearDown(self):
        self.llc.hwStop()
        self.failUnlessEqual(self.llc.getHwState(), HardwareDevice.Stop,
                             "Unable to get LLC to Stop State")

        
    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"




    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Hardware Lifecycle test.  Move the device to operational and
        Ensure that the device is in operational mode.
        '''
        self.startTestLog("Test 1: Component startup & shutdown Test")
        self.failUnlessEqual(self.llc.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get LLC to Operational State")
        self.endTestLog("Test 1: Component startup & shutdown Test")



    def test02(self):
        '''
        Verify Monitor point
        '''
        self.startTestLog("Test 2: BACI property read")
 
        #r = self.llc.GET_CNTR()[0]        
        #self.failIf( r > 4096, "Fringe Counter has wrong value (%f)." % (r))

        r = self.llc.GET_VF_MON()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Level Fast Stretcher out of range (%.3f)." % (r))

        r = self.llc.GET_VS_MON()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Level Slow Stretcher out of range (%.3f)." % (r))

        r = self.llc.GET_POL_MON1()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Level Polarimeter output 1 out of range (%.3f)." % (r))

        r = self.llc.GET_POL_MON2()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Level Polarimeter output 2 out of range (%.3f)." % (r))

        r = self.llc.GET_POL_MON3()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Level Polarimeter output 3 out of range (%.3f)." % (r))

        r = self.llc.GET_POL_MON4()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Level Polarimeter output 4 out of range (%.3f)." % (r))

        r = self.llc.GET_PC_MON1()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Polarization controller line 1 drive has wrong value (%.3f)." % (r))

        r = self.llc.GET_PC_MON2()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Polarization controller line 2 drive has wrong value (%.3f)." % (r))

        r = self.llc.GET_PC_MON3()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Polarization controller line 3 drive has wrong value (%.3f)." % (r))

        r = self.llc.GET_PC_MON4()[0]        
        self.failIf( r < 0 or r > 5.0 , \
        "Polarization controller line 4 drive has wrong value (%.3f)." % (r))

        #r = self.llc.GET_P_DET()[0]        
        #self.failIf( r > -52.5 or r < -7.5 , \
        #"Level PhotoDetector output out of range (%.3f)." % (r))

        #r = self.llc.GET_LVL_50MHZ()[0]        
        #self.failIf( r > -22 or r < 14.0 , \
        #"Signal level detector 50MHZ input out of range (%.3f)." % (r))

        #r = self.llc.GET_TEMP()[0]        
        #self.failIf( r > 16 or r < 22.0 , \
        #"Stretcher temperature out of range (%.3f)." % (r))

        self.endTestLog("Test 2: BACI property read")

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg


 

# **************************************************
# MAIN Program
if __name__ == '__main__':
    # For some reason ACS has problems unless I do this
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("Test")

    container = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()
