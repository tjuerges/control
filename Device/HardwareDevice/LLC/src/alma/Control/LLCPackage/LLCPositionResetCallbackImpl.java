/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.LLCPackage;

import alma.ACSErr.Completion;
import alma.acs.container.ContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

import alma.Control.LLCPositionResetCallback;
import alma.Control.LLCPositionResetCallbackPOATie;
import alma.Control.LLCPositionResetCallbackOperations;
import alma.Control.LLCPositionResetCallbackHelper;
import alma.Control.LLCPackage.LLCPositionResetData_t;

import alma.Control.LLCStretcherResetData;

import alma.Control.CommonCallbacks.AntennaCallbackBase;
import java.util.Iterator;
import java.util.ArrayList;


/**
 * Insert a Class/Interface comment.
 * 
 */
public class LLCPositionResetCallbackImpl 
    extends AntennaCallbackBase<LLCPositionResetData_t,
            LLCPositionResetCallback, LLCPositionResetCallbackPOATie>
    implements LLCPositionResetCallbackOperations {
    
    public LLCPositionResetCallbackImpl(ContainerServices cs)
        throws AcsJContainerServicesEx {
        super(cs);
        tieClass = new LLCPositionResetCallbackPOATie(this);
        externalInterface =  LLCPositionResetCallbackHelper.
            narrow(containerServices.activateOffShoot(tieClass));
    }

    protected void deactivateOffshoot() {
        try {
            containerServices.deactivateOffShoot(tieClass);
        } catch  (AcsJContainerServicesEx ex) {
            logger.warning("Failed to deactive Callback object");
            ex.log(logger);
        }
    }
    
    /* -------------- CORBA Interface ------------------ */
    /* This is a one-way void call so we cannot throw exceptions*/
    public void report(String antennaId, LLCPositionResetData_t value,
                       Completion status) {
        report(antennaId,  new ResponseStructure(value,status));
    }

     /* --------------- Java Interface ----------------- */
    /* This method returns a sequence of the Reset Data 
       for all LLCs which responded.  This structure is designed
       to be passed to the DataCaptuer::setLLCStretcherData method
    */
    public LLCStretcherResetData[] getLLCStretcherResetData() {
        ArrayList<LLCStretcherResetData> resetData = 
            new ArrayList<LLCStretcherResetData>();

        for (Iterator<String> iter = responses.keySet().iterator();
             iter.hasNext();) {
            String antennaId = iter.next();
            ResponseStructure response = responses.get(antennaId);
            if (response != null && !response.getCompletion().isError()) {
                LLCStretcherResetData rd = new LLCStretcherResetData();
                rd.antennaName = antennaId;
                rd.fringeCounter = response.getValue().fringeCounter;
                rd.fastStretcherVoltageChange = response.getValue().deltaVF;

                resetData.add(rd);
            }
        }
        return resetData.toArray(new LLCStretcherResetData[0]);
    }
}
