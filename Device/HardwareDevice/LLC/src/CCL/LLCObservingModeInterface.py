#! /usr/bin/env python
# "@(#) $Id$"
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#
"""
This module is part of the Control Command Language.
It contains complementary functionality for LLC devices.
"""
from ControlCommonCallbacks import SimpleCallbackImpl

class LLCObservingModeInterface:
    '''
    This class add the functionality of the LLCObserving Mode Interface
    to the Observing modes which implement it.
    
    This class is not meant to be used directly but as a portion of
    an Observing mode implementation
    '''
    def __init__(self, obsModeReference):
        self.__reference = obsModeReference

    def enableLLCCorrection(self, enable = True):
        '''
        This method will enable (or disble if the parameter is false) the
        LLC Length correction on all antenna in the Array.

        On error an AsynchronousFailureExcption is thrown
        '''
        self.__reference.enableLLCCorrection(enable)        

    def isLLCCorrectionEnabled(self):
        '''
        This method checks to see if the LLCs on all antennas are enabled
        or disabled.  If the result is a mix, or other issues are encountered
        an AsynchronousFailureExcption is thrown
        '''

        return self.__reference.isLLCCorrectionEnabled()        
        
        
    def getTimeToLLCReset(self):
        '''
        This method determines the maximum (estimated) time before one
        of the LLCs associated with this array needs to be reset.
        
        The value is returned in seconds
        
        On error an AsynchronousFailureExcption is thrown
        '''
        return (self.__reference.getTimeToLLCReset()/1.0E7)

    def resetLLCStretchers(self):
        '''
        This method will cause all LLCs in the Array to be reset
        to the optimum value of the stretcher.

        On error an AsynchronousFailureExcption is thrown
        '''
        self.__reference.resetLLCStretchers()

    def setLLCStretcherPosition(self, position):
        '''
        This method will set the LLC to the specified position (expressed
        as a fraction of the total range) 0.0 - 1.0.  To prevent setting it
        to a saturated position valid values are 0.05- 0.95.
        '''
        self.__reference.setLLCStretcherPosition(position)
        
    def getLLCPolarizationCalRequired(self):
        '''
        This method checks if a polarization calibration is required
        on any LLC associated with this array.
        
        On error an AsynchronousFailureExcption is thrown
        '''
        return self.__reference.getLLCPolarizationCalRequired()


    def doLLCPolarizationCalibration(self, timeout = 300,
                                     forceCalibration=True):
        '''
        This method will cause all LLCs associated with this array
        to perform a polarization calibration.  If the force calibration
        flag is true then the calibration is always performed, if it is
        false then the calibartion is performed only if required on a LLC
        by LLC basis.
        
        The timeout parameter is the client side timeout (in seconds)
        '''
        cb = SimpleCallbackImpl()
        self.__reference.doLLCPolarizationCalibrationAsynch(timeout,
                           forceCalibration, cb.getExternalInterface())
        cb.waitForCalibration(timeout+1)
         
#
# ___oOo___
