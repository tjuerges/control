#! /usr/bin/env python
# "@(#) $Id$"
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#
"""
This module is part of the Control Command Language.
It contains complementary functionality for LLC devices.
"""
import CCL.LLCBase
from CCL import StatusHelper
import CCL.SIConverter

from ControlCommonCallbacks import SimpleCallbackImpl

class LLC(CCL.LLCBase.LLCBase):
    '''
    The LLC class inherits from the code generated LLCBase
    class and adds specific methods.
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        The constructor creates a LLC object using LLCBase constructor.
        EXAMPLE:
        from CCL.LLC import LLC
        obj = LLC("DV01")

        '''
        try:
            CCL.LLCBase.LLCBase.__init__(self, \
                                       antennaName, \
                                       componentName, \
                                       stickyFlag)
        except Exception, e:
            print 'LLC component is not running \n' + str(e)
            raise e

    def __del__(self):
        CCL.LLCBase.LLCBase.__del__(self)


    def enableLengthCorrection(self, enable = True):
        '''
        This method will enable the phase correction in the LLC
        this means making sure that the LLC is in closed loop mode
        and that the SET_RST_CTL is not asserted.  Turning this
        to the off state, sets the RST_CTL back to the on state
        but does not transition to open loop operation.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].enableLengthCorrection(enable)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def lengthCorrectionEnabled(self):
        '''
        This method will return true if length corretion on the LLC is
        enabled.  That is the LLC is in Closed loop mode and the
        SET_RST_CTL bit is not asserted.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].lengthCorrectionEnabled()
        if len(self._devices) == 1:
            return result.values()[0]
        return result


    def setStretcherToOptimumPosition(self):
        '''
        This method will set the LLC to the optimum position given the
        current derivitive of the control voltage.  This should allow
        longer periods between successive resets.

        For now this simply resets to the center.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = \
                            self._devices[key].setStretcherToOptimumPosition()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def setStretcherToPosition(self, position = 0.5):
        '''
        This method will set the LLC to the specified position.  Valid
        values are 0.1 to 0.9 inclusive.  The default value is the center.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = \
                            self._devices[key].setStretcherToPosition(position)
        if len(self._devices) == 1:
            return result.values()[0]
        return result
        
    def polarizationCalRequired(self):
        '''
        This method will initialize a Calibration Check on the LLC
        and return True if the polarization of the LLC is outside of
        operating specifications.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].polarizationCalRequired()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def startPolarizationOptimization(self, forceCalibration = False):
        '''
        If forceCalibration is False, this method will start by checking
        if a polarization calibration  is needed, and will perform one if
        required.  If forceCalibration is True the check is skipped and
        a calibration is always performed.
          
        Optimizing the polarization of the LLC using the firmware method
        can be very long and is therefore implemented as an asynchronous
        method.

        Use the WaitForOptimization method to register a callback
        for when this method completes.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                              startPolarizationOptimization(forceCalibration)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def waitForPolarizationCalibration(self, timeout = "15 min"):
        '''
        This method will wait for an ongoing polarization calibration
        to complete (or timeout). If the timeout is specified as a
        numerical value it must be in seconds, otherwise valid time
        strings are accepted.  If no calibration is currently active
        this method returns immediately.

        This method can only be invoked on a single LLC at a time.  If
        this CCL object points to more than one LLC device an exception
        will be thrown.
        '''
        if len(self._devices) > 1:
            raise InvalidRequestEx("Unable to execute: too many devices"+
                                   " associated with CCL object")

        timeout = int(CCL.SIConverter.toSeconds(timeout) * 1E7)
        cb = SimpleCallbackImpl.SimpleCallbackImpl()
        for key, val in self._devices.iteritems():
            self._devices[key].waitForPolarizationCalibration \
                  (timeout, cb.getExternalInterface()) 

        cb.waitForCompletion(timeout+1000000)

    def doPolarizationCalibration(self, forceCalibration = False,
                                  timeout = "15min"):
        '''
        This method will perform the polarization calibration and return
        when it is complete (or times out).  If the timeout is specified as a
        numerical value it must be in seconds, otherwise valid time
        strings are accepted.

        This method can only be invoked on a single LLC at a time.  If
        this CCL object points to more than one LLC device an exception
        will be thrown.

        Calling this method is equivelent to calling:
        if startPolarizationOptimization(forceCalibration):
           waitForPolarizationCalibration(timeout)
        '''
        if len(self._devices) > 1:
            raise InvalidRequestEx("Unable to execute: too many devices"+
                                   " associated with CCL object")

        if self.startPolarizationOptimization(forceCalibration):
            self.waitForPolarizationCalibration(timeout)

    def STATUS(self):
        '''
        Method to print relevant status information to the screen.
        '''
        
        '''Get component and antenna name'''
        listNames = self._HardwareDevice__componentName.rsplit("/")
        antName   = listNames[1]
        compName  = listNames[2]

        entities = []

        values = []
        try:
            # The Lock is a bit funny in that false is locked
            if (self.GET_LOCK()[0]):
                values.append(StatusHelper.ValueUnit("LOCKED"))
            else:
                values.append(StatusHelper.ValueUnit("UNLOCKED"))
        except:
            values.append(StatusHelper.ValueUnit("N/A"))

        try:
            # The Lock is a bit funny in that false is locked
            if (self.GET_LOCK_ALARM()[0]):
                values.append(StatusHelper.ValueUnit("T", label='Alarm'))
            else:
                values.append(StatusHelper.ValueUnit("F", label='Alarm'))
        except:
            values.append(StatusHelper.ValueUnit("N/A", label='Alarm'))
            
        entities.append(StatusHelper.Line("Lock Status", values))


        values = []
        try:
            if self.lengthCorrectionEnabled():
                values.append(StatusHelper.ValueUnit("enabled"))
            else:
                values.append(StatusHelper.ValueUnit("disabled"))
        except:
            values.append(StatusHelper.ValueUnit("N/A"))

        try:
            values.append(StatusHelper.ValueUnit(self.GET_CNTR()[0],
                                                 label = "Fringe Counter"))
        except:
            values.append(StatusHelper.ValueUnit("N/A",label="Fringe Counter"))
        entities.append(StatusHelper.Line("Length Correction", values))

        values = []
        try:
            values.append(StatusHelper.ValueUnit("%2.2f" %self.GET_VF_MON()[0],
                                                 "V", "Fast"))
        except:
            values.append(StatusHelper.ValueUnit("N/A","V", "Fast"))


        try:
            values.append(StatusHelper.ValueUnit("%2.2f" %self.GET_VS_MON()[0],
                                                 "V", "Slow"))
        except:
            values.append(StatusHelper.ValueUnit("N/A","V", "Slow"))
            
        entities.append(StatusHelper.Line("Stretcher Voltage", values))
                        

        values = []
        for method in [('1',self.GET_POL_MON1), ('2',self.GET_POL_MON2),
                       ('3',self.GET_POL_MON3), ('4',self.GET_POL_MON4)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f" %
                                                     method[1]()[0],
                                                     'V', method[0]))
            except:
                values.append(StatusHelper.ValueUnit("N/A", 'V', method[0]))
        entities.append(StatusHelper.Line("Pol. Monitor", values))
                
        values = []
        for method in [('1',self.GET_PC_MON1), ('2',self.GET_PC_MON2),
                       ('3',self.GET_PC_MON3), ('4',self.GET_PC_MON4)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f" %
                                                     method[1]()[0],
                                                     'V', method[0]))
            except:
                values.append(StatusHelper.ValueUnit("N/A", 'V', method[0]))
        entities.append(StatusHelper.Line("Pol. Controller", values))

        
        values = []
        try:
            values.append(StatusHelper.ValueUnit("%2.2f"%self.GET_P_DET()[0],
                                                 "V", 'Photo Det.'))
        except:
            values.append(StatusHelper.ValueUnit("N/A", "V", 'Photo Det.'))

        try:
            values.append(StatusHelper.ValueUnit("%2.2f" %
                                                 self.GET_LVL_50MHZ()[0],
                                                 'V', '50 MHz'))
        except:
            values.append(StatusHelper.ValueUnit("N/A",'V', '50 MHz'))
        entities.append(StatusHelper.Line("Status",values))

        values =[]
        try:
            values.append(StatusHelper.ValueUnit("%2.2f"% self.GET_TEMP()[0],
                                                 "C"))
        except:
            values.append(StatusHelper.ValueUnit("N/A","C"))
        entities.append(StatusHelper.Line("Temperature",values))

        values =[]
        try:
            values.append(StatusHelper.ValueUnit("%2.6f"% self.GET_SOPC()[0],
                                                 "rad"))
        except:
            values.append(StatusHelper.ValueUnit("N/A","rad"))
        entities.append(StatusHelper.Line("SOPC",values))


        frame = StatusHelper.Frame(compName + "   Ant: " + antName, entities)
        frame.printScreen()




#
# ___oOo___
