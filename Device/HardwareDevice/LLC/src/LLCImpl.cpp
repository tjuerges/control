//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <LLCImpl.h>
#include <string>
#include <hardwareDeviceImpl.h>
#include <ControlExceptions.h>


const ACS::Time LLCImpl::POLARIZATION_CHECK_TIMEOUT = 70000000; //7 s
const ACS::Time LLCImpl::STRETCHER_POSITION_TIMEOUT = 70000000; //7 s


//-----------------------------------------------------------------------------
// LLC Constructor
//-----------------------------------------------------------------------------
LLCImpl::LLCImpl(const ACE_CString& name, maci::ContainerServices* cs):
    LLCBase(name, cs),
    polarization_cal_required_m(false)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

//-----------------------------------------------------------------------------
// LLC Destructor
//-----------------------------------------------------------------------------
LLCImpl::~LLCImpl()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

//-----------------------------------------------------------------------------
// External Methods
//-----------------------------------------------------------------------------
// Reset the fast/slow stretcher voltages to midrange (2.5 Volts).
// To perform reset this method writes on SET_RST_CTL
// "1" reset the stretcher to 2.5Volts and
// "0" to allow the stretchers move
void LLCImpl::resetStretcher()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        setRstCtl(1); //Set the reset.

        //FIXME. I will wait for 5 seconds Ask rbrito.
        const struct timespec sleeptime = {0, 500000000}; // 0.5 second
        int c = 10;
        do
        {
            nanosleep(&sleeptime, NULL);
        }
        while(c--);

        setRstCtl(0);//Stop the reset.
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
}

void LLCImpl::enableLengthCorrection(bool enable)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        if(enable == true)
        {
            enableAlarms();
            setVfCtl(0);
            setRstCtl(0);
        }
        else
        {
            disableAlarms();
            forceTerminateAllAlarms();
            setRstCtl(1);
        }
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
}

bool LLCImpl::lengthCorrectionEnabled()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp(0ULL);
    try
    {
        return (!getVfCtlMon(timestamp) && !getRstCtlMon(timestamp));
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
}

bool LLCImpl::startPolarizationOptimization(bool forceOptimization)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if((forceOptimization == false) && (polarization_cal_required_m == false))
    {
        return false;
    }

    try
    {
        setCalStart(true);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }

    return true;
}

bool LLCImpl::polarizationCalRequired()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp(0ULL);
    // Initialise the calibration check routine
    try
    {
        setCalCheck(true);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }

    try
    {
        waitForFirmwareCompletion(POLARIZATION_CHECK_TIMEOUT);
    }
    catch(const ControlExceptions::TimeoutEx& ex)
    {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getTimeoutEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }

    try
    {
        // If polarization is required. Activate an alarm.
        if(getPolarizationControllerCalibrationStatus(timestamp) == false)
        {
            activateAlarm(PolarizationIsRequired);
            polarization_cal_required_m = true;
        }
        else
        {
            deactivateAlarm(PolarizationIsRequired);
            polarization_cal_required_m = false;
        }
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }

    return polarization_cal_required_m;
}

void LLCImpl::waitForPolarizationCalibration(ACS::Time timeout,
    Control::SimpleCallback* cb)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACSErr::Completion completion;
    try
    {
        waitForFirmwareCompletion(timeout);
        completion = ACSErrTypeOK::ACSErrOKCompletion();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        completion = ControlExceptions::CAMBErrorCompletion(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        completion = ControlExceptions::INACTErrorCompletion(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::TimeoutEx& ex)
    {
        completion = ControlExceptions::TimeoutCompletion(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(...)
    {
        completion = ACSErrTypeCommon::UnknownCompletion(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    cb -> report(completion);
}

void LLCImpl::abortFirmwareRoutine()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        switch(getRunningRoutine())
        {
            case SETCALSTART:
                setCalStart(false);
                break;
            case SETCALCHECK:
                setCalCheck(false);
                break;
            case SETSTRCHTOLVL:
                setStrchrToLvl(false, 2.5);
                break;
            default:
                break;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
}

Control::LLC::LLCPositionResetData_t LLCImpl::setStretcherToPosition(
    float position)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if((position < 0.1) || (position > 0.9))
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The value "
            << position
            << " is outside of the legal range ([0.1, 0.9]).";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    Control::LLC::LLCPositionResetData_t resetData;
    ACS::Time timestamp(0ULL);
    try
    {
        // Get the intial values so we can put the delta's into the
        // return structure
        setRstCntr(true); // Set the counter to 0
        float vF_initial = getVfMon(timestamp);
        float vS_initial = getVsMon(timestamp);

        setStrchrToLvl(true, 5.0 * position);

        try
        {
            waitForFirmwareCompletion(STRETCHER_POSITION_TIMEOUT);
        }
        catch(const ControlExceptions::TimeoutEx& ex)
        {
            ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getTimeoutEx();
        }
        catch(const ControlExceptions::INACTErrorEx& ex)
        {
            ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getINACTErrorEx();
        }
        catch(const ControlExceptions::CAMBErrorEx& ex)
        {
            ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getCAMBErrorEx();
        }

        float vF_final = getVfMon(timestamp);
        float vS_final = getVsMon(timestamp);

        setReadCntr(true); // Move the Fringe Counter value to a readable place

        resetData.fringeCounter = getCntr(timestamp);
        resetData.deltaVF = vF_final - vF_initial;
        resetData.deltaVS = vS_final - vS_initial;

        setRstLockAlarm(1); // Reset the latched alarm bit
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }

    return resetData;
}

Control::LLC::LLCPositionResetData_t LLCImpl::setStretcherToOptimumPosition()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Eventually this should use the current slope to optimize the time
    // between resets, but we need more info before that can be done so:
    try
    {
        return setStretcherToPosition(0.5);
    }
    catch(const ControlExceptions::INACTErrorEx &ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorEx &ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
    catch(const ControlExceptions::TimeoutEx &ex)
    {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getTimeoutEx();
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getIllegalParameterErrorEx();
    }
}

void LLCImpl::setStrchrToLvl(bool runFlag, const float world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Cannot execute control request.  Device is "
            "inactive.");
        ex.log();
        throw ex.getINACTErrorEx();
    }

    int routine = -1;

    try
    {
        routine = getRunningRoutine();
    }
    catch(const ControlExceptions::INACTErrorEx &ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorEx &ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }

    if(routine != NOROUTINERUNNING)
    {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The LLC is busy running "
            << routineToStr(routine)
            << ". In short the LLC is not ready for operations. You should "
                "wait it out, or abort the routine manually";
        ex.addData("Detai;", msg.str());
        ex.log();
        throw ex.getCAMBErrorEx();
    }

    if((world < 0.5) || (world > 4.5))
    {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The value "
            << world
            << " is outside of the legal range ([0.5, 4.5]).";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getCAMBErrorEx();
    }

    const unsigned long rcaStrchrToLvl(0x52UL);
    AmbRelativeAddr rca(rcaStrchrToLvl + AmbDeviceImpl::getBaseAddress());
    AmbDataLength_t length(5U);
    unsigned char rawBytes[8];

    float raw(0U);

    raw = world;

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.
    AMB::Utils::swapBytes(raw);
#endif

    // Set the execution flag
    if(runFlag)
    {
        rawBytes[0] = 0x01;
    }
    else
    {
        rawBytes[0] = 0x00;
    }

    // Assign raw to rawBytes.
    unsigned char* praw = reinterpret_cast< unsigned char* >(&raw);
    for(unsigned short i(0U); i < 4; ++i)
    {
        rawBytes[i + 1] = praw[i];
    }

    // Execute the command.
    acstime::Epoch cmdTime;
    cmdTime.value = 0ULL;

    try
    {
        commandEnc(cmdTime.value, rca, length,
            reinterpret_cast< AmbDataMem_t* >(rawBytes));
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
}

//---------------------------------------------------------------
// Internal Methods
//---------------------------------------------------------------
void LLCImpl::waitForFirmwareCompletion(const ACS::Time& timeout)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // 0.5 seconds
    const struct timespec sleeptime = {0, 500000000};
    ACS::Time timestamp(0ULL);
    const ACS::Time endTime(::getTimeStamp() + timeout);

    try
    {
        while(::getTimeStamp() < endTime)
        {
            if(getGeneralStatus(timestamp) == false)
            {
                return;
            }

            nanosleep(&sleeptime, NULL);
        }
    }
    catch(const ControlExceptions::INACTErrorEx &ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorEx &ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }

    // If we reached here we timed out
    ControlExceptions::TimeoutExImpl ex(__FILE__, __LINE__,
        __PRETTY_FUNCTION__);
    std::ostringstream msg;
    msg << "Timed out waiting for completion of firmware rountine"
        << " (Timeout Value "
        << timeout / 1.0E7
        << " s).";
    ex.addData("Detail", msg.str());
    ex.log();
    throw ex.getTimeoutEx();
}

//-----------------------------------------------------------------------------
// Component Lifecycle Methods
//-----------------------------------------------------------------------------
void LLCImpl::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        // Initialize Base class
        LLCBase::initialize();
    }
    catch(acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        acsErrTypeLifeCycle::LifeCycleExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex;
    }

    const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    antennaName_m = HardwareDeviceImpl::componentToAntennaName(compName.in());

    //register alarms.
    initializeAlarms("LLC", compName.in(), creatAlarmVector());
}

void LLCImpl::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareDeviceImpl::hwStop();
    }
    catch(...)
    {
        LOG_TO_OPERATOR(LM_WARNING, "Caught an exception during transition to "
            "the hardware stop state.  Continuing anyway.");
    }

    forceTerminateAllAlarms();

    try
    {
        LLCBase::cleanUp();
    }
    catch(acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        acsErrTypeLifeCycle::LifeCycleExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

CORBA::Float LLCImpl::GET_SOPC(ACS::Time& timestamp)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    std::vector< unsigned char > cal_result;

    union Conversion
    {
      unsigned char byte[4];
      float sopc;
    } data;

    try
    {
      cal_result = getCalResult(timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex) 
    {
      throw ControlExceptions::CAMBErrorExImpl (ex, __FILE__, __LINE__, 
                                                __PRETTY_FUNCTION__).getCAMBErrorEx();
    } 
    catch(const ControlExceptions::INACTErrorExImpl& ex) {
      throw ControlExceptions::INACTErrorExImpl (ex, __FILE__, __LINE__, 
                                                 __PRETTY_FUNCTION__).getINACTErrorEx();
    }

    data.sopc = 0;
    for (unsigned int i=0; i<4; i++)
    {
        data.byte[i]=cal_result[i+1];
    }

    float valueSOPC(0U);

    valueSOPC = data.sopc;

#if BYTE_ORDER != BIG_ENDIAN
    // Swap the bytes.
    AMB::Utils::swapBytes(valueSOPC);
#endif

   return valueSOPC;

}

//-----------------------------------------------------------------------------
// Hardware LifeCycle Methods
//-----------------------------------------------------------------------------
void LLCImpl::hwInitializeAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    LLCBase::hwInitializeAction();

    // Clear all the alarms
    forceTerminateAllAlarms();
}

void LLCImpl::hwStopAction()
{

    ACS_TRACE(__PRETTY_FUNCTION__);

    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushTime(0ULL);

    // Flush all commands
    flushNode(0, &flushTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "Communication failure flushing commands "
            "to device");
    }
    else
    {
        std::ostringstream msg;
        msg << "All commands and monitors flushed at: "
            << flushTime;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    // Call stop in base class
    LLCBase::hwStopAction();
    // Clean all alarms state into the helper class
    forceTerminateAllAlarms();
}

//-----------------------------------------------------------------------------
// Error Handling Methods
//-----------------------------------------------------------------------------
// handleSetError and handleReactivateAlarm abstract methods on the
// ErrorStateHelper.h
void LLCImpl::handleActivateAlarm(int newErrorFlag)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Turn on the Alarm
    LLCBase::setError(createErrorMessage());

}

void LLCImpl::handleDeactivateAlarm(int newErrorFlag)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Turn off the Alarm
    if(isAlarmSet() == true)
    {
        LLCBase::setError(createErrorMessage());
    }
    else
    {
        LLCBase::clearError();
    }
}

//-----------------------------------------------------------------------------
// LLC Alarm Methods
//-----------------------------------------------------------------------------
std::vector< Control::AlarmInformation > LLCImpl::creatAlarmVector()
{
    std::vector< Control::AlarmInformation > aivector;
    {
        Control::AlarmInformation ai;
        ai.alarmCode = PolarizationIsRequired;
        ai.alarmDescription = "LLC polarization optimization is required.";
        aivector.push_back(ai);
    }

    return aivector;
}

//-----------------------------------------------------------------------------
// Internal Methods
//-----------------------------------------------------------------------------

//ntroncos 2010-11-08: Why this method.
//   To read the real value, you have to instruct the hardware to make it available.
//   Hence setReadCntr(true) must be called, before you read the actual value.
//   Since setReadCntr(true) is one of the RCA that may be bloqued we check if
//   we should command it. We could also call LLCBase::setReadCntr directly, which
//   will throw no exception, but that defeats having full control of what is
//   happening in the hardware.
//ntroncos 2010-11-19. Calling setReadCntr durng automated routines causes no
//   problems
//   (http://jira.alma.cl/browse/CSV-179?focusedCommentId=109660#action_109660)
//   So I disabled the check if a routine is running and just call it.
short LLCImpl::getCntr(ACS::Time& timestamp)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        LLCBase::setReadCntr(true);
        return LLCBase::getCntr(timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

std::string LLCImpl::routineToStr(int r)
{
    switch(r)
    {
        case SETSTRCHTOLVL:
            return "SET_STRCH_TO_LVL";
            break;
        case SETCALCHECK:
            return "SET_CAL_CHECK";
            break;
        case SETCALSTART:
            return "SET_CAL_START";
            break;
        default:
            break;
    }

    return "UNKOWN Routine number";
}

int LLCImpl::getRunningRoutine()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp(0ULL);
    try
    {
        /// \note ntroncos 2010-11-16:
        /// All command that initiate routines in the firmware
        /// or are affected by it must be guarded. This ensures that they are
        /// executed secuantially and guarantee that only one will be sent
        /// to the hardware, and the rest will fail with an appropiate exception.
        ACE_Guard < ACE_Recursive_Thread_Mutex > guard(m_mutex);

        // Ensure that dependent monitor points are updated.
        getRoutineStatus(timestamp);

        if(getGeneralStatus(timestamp) == false)
        {
            return NOROUTINERUNNING;
        }
        else if(getCalStatus(timestamp) == true)
        {
            return SETCALSTART;
        }
        else if(getPolarizationControllerCalibrationCheckerStatus(timestamp)
            == true)
        {
            return SETCALCHECK;
        }
        else if(getStrecherResetToLevelStatus(timestamp) == true)
        {
            return SETSTRCHTOLVL;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }

    //To make the compiler happy.
    return NOROUTINERUNNING;
}

void LLCImpl::checkAndThrow(const char* method)
{
    int routine = -1;
    try
    {
        routine = getRunningRoutine();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    // Ugly as hell as it may be. Control methods may only throw
    // CAMBErrorExImpl or INACTErrorExImpl. So I make the proper exception,
    // and then wrap it up.
    try
    {
        if(routine != NOROUTINERUNNING)
        {
            ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::string msg("It is invalid to call ");
            msg += method;
            msg += " when the LLC is busy doing ";
            msg += routineToStr(routine);
            ex.addData("Detail", msg.c_str());
            throw ex;
        }
    }
    catch(const ControlExceptions::InvalidRequestExImpl &ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

//All below are overloaded control points that are affected when a routine is
//running in the LLC firmware. They will fail with an exception if a routine is
//running. This way the user knows that his command does NOT work and can take
//appropiate actions.
void LLCImpl::setRstCntr(const bool world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setRstCntr(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

//ntroncos 2010-11-19: Acording to Jason Castros comment in CSV-179
// In response to Nicolas' comment yesterday. During normal operation reading
// the counter requires you to perform two operations. The command READ_CNTR
// (0x51) is first sent to load the current value for the counter into memory.
// The monitor request GET_CNTR (0x05) is then sent to read back that value.
// However, when an automated routine is activated the counter value is
// automatically updated in memory so that sending the command READ_CNTR is not
// required to update the value read by GET_CNTR. This behavior is not
// documented in the ICD and needs to be added. From a computing software point
// of view, sending the command READ_CNTR during an automated routine will have
// no adverse effect on the hardware. The firmware will simply ignore the
// command and update the counter value in memory automatically so you could
// removed the exception error from your code.
//void LLCImpl::setReadCntr(const bool world)
//{
//    ACS_TRACE(__PRETTY_FUNCTION__);
//    try {
//        checkAndThrow(__PRETTY_FUNCTION__);
//        LLCBase::setReadCntr(world);
//    } catch(const ControlExceptions::CAMBErrorExImpl& ex){
//        ControlExceptions::CAMBErrorExImpl nex(ex,__FILE__,__LINE__,
//                __PRETTY_FUNCTION__);
//        nex.log();
//        throw nex;
//    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
//        ControlExceptions::INACTErrorExImpl nex(ex,__FILE__,__LINE__,
//                __PRETTY_FUNCTION__);
//        nex.log();
//        throw nex;
//    }
//}

void LLCImpl::setWriteFram(const std::vector< unsigned char >& world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setWriteFram(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setReadFram(const std::vector< unsigned char >& world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setReadFram(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setPolDrv1(const float world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setPolDrv1(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setPolDrv2(const float world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setPolDrv2(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setPolDrv3(const float world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setPolDrv3(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setPolDrv4(const float world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setPolDrv4(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setVfO(const float world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setVfO(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setRstLockAlarm(const bool world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setRstLockAlarm(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setCalCheck(const bool world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setCalCheck(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void LLCImpl::setCalStart(const bool world)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkAndThrow(__PRETTY_FUNCTION__);
        LLCBase::setCalStart(world);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LLCImpl)
