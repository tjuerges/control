/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2008 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jsepulve  2008-03-09  created 
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*
*   
*   PARENT CLASS
*
* 
*   DESCRIPTION
*
*
*   PUBLIC METHODS
*
*
*   PUBLIC DATA MEMBERS
*
*
*   PROTECTED METHODS
*
*
*   PROTECTED DATA MEMBERS
*
*
*   PRIVATE METHODS
*
*
*   PRIVATE DATA MEMBERS
*
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

#include "straightLineFit.h"

using std::string;

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

StraightLineFit::StraightLineFit(const std::vector<float>& xArray,
				 const std::vector<float>& yArray)
  :data_x(xArray),
   data_y(yArray),
   slope(0),
   constant(0),
   average_y(0),
   average_x(0)
{

}

StraightLineFit::StraightLineFit(const std::vector<float>& xArray)
  :data_x(xArray),
   slope(0),
   constant(0)
{
   for (unsigned int i=0; i<data_x.size();i++)
      data_y.push_back(i);

}



StraightLineFit::~StraightLineFit(){

}

float StraightLineFit::getSlope()
{
   if (slope == 0) 
      linefit();
   return slope;
}
float StraightLineFit::getConstant ()
{
   if (constant == 0)
      linefit();
   return constant;
}

void StraightLineFit::linefit ()
{
  float xsum=0;
  float ysum=0;
  float Sx  =0;
  float Sxy =0;
  unsigned int i;

  const string fnName = "StraightLineFit::linefit";

/*
  for (unsigned int k=0; k<data_x.size(); k++)
     ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),(LM_DEBUG,"data_x[%d]:%f",
                                             k,data_x.at(k)));
  for (unsigned int n=0; n<data_y.size(); n++)
     ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),(LM_DEBUG,"data_y[%d]:%+.10e",
                                                n,data_y.at(n)));
*/


  //Average: (x1 + x2 +...+ x10)/10
  average_x = average (data_x);
  average_y = average (data_y);


  //Get slope and constant linear expression y=mx+b
  // (x1-<x>)*(y1-<y>) + ... + (x10-<x>)*(y10-<y>)
  for (i=0; i<data_x.size(); i++){
    xsum += data_x[i] - average_x;
    ysum += data_y[i] - average_y;
    Sxy  += xsum * ysum;

    // (x1-<x>)*(x1-<x>) + ... + (x10-<x>)*(x10-<x>)
    Sx += xsum * xsum;
  }

  try{
    slope = Sxy/Sx;
  }
  catch(std::exception& ex) {
    slope = max_slope;
  }

  constant=average_y - (average_x*slope);

/*
    ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
             (LM_DEBUG,"avg_x: %f avg_y: %E xsum: %f ysum: %E "\
              "Sxy: %f Sx: %f slope: %f Constant: %f",
              average_x,average_y,xsum,ysum,Sxy,Sx,slope,constant));
*/

}


float StraightLineFit::average (const std::vector<float>& data)
{
  unsigned int i;
  double avg = 0;
  //Average: (x1 + x2 +...+ x10) / size
  if ( ! data.empty () ){
     for (i=0; i<data.size(); i++)
        avg += data[i];

     try {
        avg /= static_cast<double>(data.size());
     }
     catch(std::exception& ex){
        string fnEx = "StraightLineFit::average ";
        string strEx(ex.what());
        ACS_TRACE(fnEx + ex.what());
        avg = 0;
     }
  }

  return static_cast<float>(avg);
}

/*___oOo___*/
