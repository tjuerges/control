/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LLCHWSimImpl.cpp
 *
 * $Id$
 */

#include "LLCHWSimImpl.h"

using namespace AMB;

/* Please use this class to implement complex functionality for the
 * LLCHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

LLCHWSimImpl::LLCHWSimImpl(node_t node, 
			   const std::vector<CAN::byte_t>& serialNumber)
  : LLCHWSimBase::LLCHWSimBase(node, serialNumber)
{}

void LLCHWSimImpl::initialize(node_t node, 
                              const std::vector<CAN::byte_t>& serialNumber)
{
  LLCHWSimBase::initialize(node, serialNumber);
  
}

void AMB::LLCHWSimImpl::control(AMB::rca_t rca, 
                                const std::vector< CAN::byte_t >& data){

  ACS_TRACE(__func__);
  const unsigned long rcaStrchrToLvl = 0x052;

  switch (rca - getBaseAddress()) {
  case rcaStrchrToLvl:
    setStrchrToLvlSim(data);
    break;
  default:
    LLCHWSimBase::control(rca, data);
  }
}

void AMB::LLCHWSimImpl::setStrchrToLvlSim(
    const std::vector< CAN::byte_t >& data)
{
  checkSize(data, 5U, "SET_STRCHR_TO_LVL");

  bool flag = data[0];

  std::vector< CAN::byte_t> value;
  for (unsigned int idx = 0; idx < 4; idx++) 
    value.push_back(data[idx+1]);

  //setRoutineStatus
  if (flag) {
    /* Initiate the movement routine */
    setMonitorVfMon(value);
  } else {
    /* Abort */
  }
}


