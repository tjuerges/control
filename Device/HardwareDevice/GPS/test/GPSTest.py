#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#
# who       when      what
# --------  --------  ----------------------------------------------
# bgustafs 2005-07-20 created
#

'''
Define a test class for the GPS device
'''
import sys
import unittest
import Control
import ControlExceptions
from time import sleep

from ACS import acsFALSE
from ACS import acsTRUE
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.TimeHelper import getTimeStamp

'''
Define a test class for the central reference distributor
'''
def testProperty(compRef, propRef, nameStr, testNo, propName):
    '''
    Tests a single BACI property
    '''
    compRef.startTestLog("Test " + str(testNo) + ": " + str(propName) + " Property Test")
    errStr = nameStr + "Name error"
    compRef.failUnlessEqual(propRef._get_name(), nameStr, errStr);
    val = propRef.get_sync()
    errStr = "Error reading property " + propRef._get_name()
    compRef.failIf(val[1].code != 0, errStr);
    compRef.log(propRef._get_name() + " = " + str(val[0]))
    compRef.endTestLog("Test " + str(testNo) + ": " + str(propName) + " Property Test")
    return   

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self._client=PySimpleClient.getInstance()
        self.out= ""
        unittest.TestCase.__init__(self, methodName)

    def __del__(self):
        if self.out != None:
            print self.out
        self._client.disconnect()

    def setUp(self):
        self._ref= self._client.getComponent("CONTROL/AOSTiming/GPS")
        self._ref.hwStop()
        self._ref.hwStart()
        self._ref.hwConfigure()
    	self._ref.hwInitialize()
    	self._ref.hwOperational()
    	sleep(3)
        
    def tearDown(self):
        self._ref.hwStop()
        self._client.releaseComponent("CONTROL/AOSTiming/GPS")
        
    def startTestLog(self, testName):
        self.log("\n")
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self, msg):
        self.out += msg + "\n"


    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test all Read Only Properties
        '''
        testProperty(self, self._ref._get_GPS_Time(), 'CONTROL/AOSTiming/GPS/GPS_Time', 1,
                     'GPS_Time')

        testProperty(self, self._ref._get_Antenna_State(), 'CONTROL/AOSTiming/GPS/Antenna_State', 2,
                     'Antenna_State')

        testProperty(self, self._ref._get_PLL_Locked(), 'CONTROL/AOSTiming/GPS/PLL_Locked', 3,
                     'PLL_Locked')

        testProperty(self, self._ref._get_GPS_Locked(), 'CONTROL/AOSTiming/GPS/GPS_Locked', 4,
                     'GPS_Locked')

        testProperty(self, self._ref._get_RS232_OK(), 'CONTROL/AOSTiming/GPS/RS232_OK', 5,
                     'RS232_OK')

        testProperty(self, self._ref._get_Time_Error(), 'CONTROL/AOSTiming/GPS/Time_Error', 6,
                     'Time_Error')


class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self, methodName)

    def __del__(self):
        automated.__del__(self);

# **************************************************
# MAIN Program
if __name__ == '__main__':
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("Test")

    container = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()
