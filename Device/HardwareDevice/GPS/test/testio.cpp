
#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/signal.h>
//#include <sys/ioctl.h>
//#include <sys/stat.h>
//#include <asm/ioctls.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define MYDEBUG

// Forward declarations
char* stripReply(char* buf);
char* strsep(char** stringp,char* delim);
unsigned long long convertTime(int year,int doy,int hr,int min,int sec,int msec);
#ifdef MYDEBUG
//void hex_print(char* stuff,int len);
#endif

// globals
struct termios orig, new_t;
int fd;
char sd[20];


//------------------------------------------------------------------------------

#ifdef MYDEBUG
void hex_print(char* stuff,int len)
{
    printf("hex bits:");
    for (int i = 0; i < len; i++)
	{
	printf("%x ",stuff[i]);
	}
    printf("\n");
}
#endif

//------------------------------------------------------------------------------

int get_resp(char* buf,int len)
{
    int off = 0;
    char c;

    while (off < len)
	{
	int size = read(fd,&c,1);
	if (size <= 0)
	  {
//	  printf("Read error %i bytes\n", size);	
	  return -1;
	  }
	*(buf + off) = c;
	off++;
	if (c == '\n')
	    break;
	}
    *(buf + off) = 0;

#ifdef MYDEBUG
    printf("read ...%s",buf);
//    hex_print(buf,off);
#endif
    return strlen(buf);
}

//------------------------------------------------------------------------------

char* commandGPS(char* buf)
{
    int len;
    static int next = 0;
    char* ibuf;
    char errStr[40];

    // circular buffer where responses are placed
#define N_ROWS 40
    static char temp[N_ROWS][128];

    // ignore call if given command is null
    if ((buf == 0) || ((len = strlen (buf)) == 0))
	{
	return 0;
	}

#if 0    // ??? this is already done in initGPS() ???
    if (m_gps_fd == -1)
	{
	initGPS();
	if (m_gps_fd == -1)
	    {
	    return 0;
	    }
	}
#endif

    // make sure the serial input is empty
    if (tcflush(fd,TCIFLUSH) < 0)
	{
	sprintf(errStr, "Failed to flush serial device %s with error:", sd);
	perror (errStr);
	return 0;
	}

#ifdef MYDEBUG
    printf("write %s\n",buf);
#endif

    write(fd,buf,len);
    ibuf = &temp[next][0];

    if (get_resp(ibuf,128) <= 0)   /* now get the response */
      {
      return NULL;
      }
    if (strcmp(ibuf,"\r\n") == 0)  /* bad response, try again */
	if (get_resp(ibuf,128) <= 0)
          {
          return NULL;
          }
    next = ++next % N_ROWS;

    return ibuf;
}


int main (int argc, char ** argv)
{
    char errStr[40];
    struct termios oldtio, newtio;
    char ctrlc = 0x3;   // Control-C

    sprintf (sd,"/dev/ttyS1");
    //sprintf (sd,"/dev/tty1");

    fd = open(sd,O_RDWR);
    if (fd < 0)
	{
	sprintf(errStr, "Failed to open serial device %s with error:", sd);
	perror (errStr);
	return -1;
	}
    printf ("Opened serial device %s with fd = %i\n", sd, fd);

    // terminal input properties
    if (tcgetattr(fd, &orig) < 0)
	{
	sprintf(errStr, "Failed to get serial device %s configuration with error:", sd);
	perror (errStr);
	return -1;
	}
    printf ("Actual tty setting are: input modes %#x, output modes %#x, control modes %#x, local modes %#x\n",
	    orig.c_iflag, orig.c_oflag, orig.c_cflag, orig.c_lflag);

    // Set new terminal settings
    memcpy(&newtio,&orig,sizeof(orig));
    newtio.c_cflag = B9600 | CS7 | PARENB | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;
    newtio.c_lflag = 0;
    newtio.c_cc[VMIN] = 0;
    newtio.c_cc[VTIME] = 50; 

    if(tcflush(fd,TCIFLUSH) < 0)
	{
	sprintf(errStr, "Failed to flush serial device %s with error:", sd);
	perror (errStr);
	close(fd);
	return -1;
	}
    printf ("Setting tty to: input modes %#x, output modes %#x, control modes %#x, local modes %#x, o_speed %#x, i_speed %#x\n",
	    newtio.c_iflag, newtio.c_oflag, newtio.c_cflag, newtio.c_lflag,
	    newtio.c_ospeed, newtio.c_ispeed);
    {
    int i;
    printf("Control chars: ");
    for (i=0; i<32; i++)
	{
	printf("%#x ", newtio.c_cc[i]);
	}
    printf("\n");
    }
    if (tcsetattr(fd, TCSANOW, &newtio) < 0)
	{
	sprintf(errStr, "Failed to set serial device %s configuration with error:", sd);
	perror (errStr);
	close(fd);
	return -1;
	} 

    // terminal input properties again
    if (tcgetattr(fd, &orig) < 0)
	{
	sprintf(errStr, "Failed to get serial device %s configuration with error:", sd);
	perror (errStr);
	close(fd);
	return -1;
	}
    printf ("New tty setting are: input modes %#x, output modes %#x, control modes %#x, local modes %#x\n",
    orig.c_iflag, orig.c_oflag, orig.c_cflag, orig.c_lflag);
/* 
    if (write(fd, "F06 OFF\r", 9) < 0)
	{
	sprintf(errStr, "Failed to write to  serial device %s with error:", sd);
	perror (errStr);
	close(fd);
	return -1;
	} 
*/
    // interrupt GPS F08 continuous run with ^C
    if (write(fd,&ctrlc,1) < 0)
	{
	sprintf(errStr, "Failed to write to  serial device %s with error:", sd);
	perror (errStr);
	close(fd);
	return -1;
	}

    /*
     * The first command after ctrlc fails sometimes.  This is because it is 
     * not free running time output on the second.  When the receiver is first 
     * turned on, it defaults to free run time output, so cover that case by 
     * sending a benign command (request 12/24 hour format).
     */
    commandGPS("F02\r");

    // toss garbage from i/o
    if (tcflush(fd,TCIFLUSH) < 0)
	{
	sprintf(errStr, "Failed to flush serial device %s with error:", sd);
	perror (errStr);
	close(fd);
	return -1;
	}
	sleep(1); 
    // initialize GPS with these commands
    char* commands[] = {
	// F06 - lockout keypad
	"F06 ON\r",
	// F02 - set 24 hr time format
	"F02 24\r",
	// F05 - set time quality setup
	"F05 ON 1000 10000 100000 1000000\r",
	// F11 - set time output format
	"F11 DDD:HH:MM:SS.mmmq\r",
	// F69 - set GPS time mode
	"F69 GPS\r",
	// F68 - set year entry
	// N.B. This is only needed if GPS NVRAM is corrupted (dead battery)
	// "F68 1996\r"
    };

    for (unsigned int i = 0; i < sizeof(commands)/sizeof(commands[0]); i++)
	{
	char *reply = commandGPS(commands[i]);
	if (reply == NULL)
	  {
	  printf("Read failure (timeout?)\n");
	  close(fd);
	  return -1;
  	  }
	if (strcmp(reply,"OK\r\n") != 0)
	    {
	    char str[100];
	    strcpy(str,commands[i]);
	    str[strlen(commands[i])-1] = 0;
	    printf("RefGPSDevice::ctor: %s command failed\n",str);
	    }
	}

    close(fd);

    return 0;
}

//------------------------------------------------------------------------------
#if 0
char* stripReply(char* buf)
{
    strsep(&buf," ");
    return buf;
}

//------------------------------------------------------------------------------

char* strsep(char** stringp,char* delim)
{
    char* s;
    const char* spanp;
    int c;
    int sc;
    char* tok;

    if ((s = *stringp) == 0)
	return 0;

    for (tok = s;;)
	{
	c = *s++;
	spanp = delim;
	do
	    {
	    if ((sc = *spanp++) == c)
		{
		if (c == 0)
		    s = 0;
		else
		    s[-1] = 0;
		*stringp = s;
		return tok;
		}
	    } while (sc != 0);
	}
}

//------------------------------------------------------------------------------

unsigned long long convertTime(
	int year,int doy,int hr,int min,int sec,int msec)
{
    //
    // following is lifted from timeService/ws/src/EpochImpl.cpp
    //
    // add number of days from Gregorian calendar start to start of current year
    int yearMinusOne = year - 1;
    unsigned long long accum = doy - 1
	                     + yearMinusOne * 365
                             + yearMinusOne / 4 
                             - yearMinusOne / 100 
                             + yearMinusOne / 400;

    // subtract days at starting date (1582-10-15)
    const unsigned long StartDate = 577735;
    accum -= StartDate;

    // add contribution from hour, minute, second, and micro-second
    accum = ((((accum * 24 + hr) * 60
	                  + min) * 60 
	                  + sec) * 1000
	                  + msec) * 10000;
    return accum;
}
#endif
//------------------------------------------------------------------------------
#if 0
int testGPS()
{
    RefGPSDevice* gpsd = RefGPSDevice::instance();
//    gpsd->time();

    for(int i = 0; i < 10; i++)
	gpsd->tick();
    return 0;
}
#endif


