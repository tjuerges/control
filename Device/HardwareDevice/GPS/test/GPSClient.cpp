// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// Correspondence concerning ALMA Software should be addressed as follows:
//   Internet email: alma-sw-admin@nrao.edu

//
// DESCRIPTION
// An ACS client to test the GPS interface
//


#include <maciSimpleClient.h>
#include <acserr.h>
#include <logging.h>
#include "GPSC.h"


int main(int argc, char** argv)
{

    // create instance of SimpleClient and init() it.
    maci::SimpleClient client;
    if(client.init(argc, argv) == 0)
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_ERROR, "No client init possible."));
        return - 1;
    }

    client.login();

    try
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Getting GPS component..."));
        Control::GPS_var gps(client.getComponent< Control::GPS >(
            "CONTROL/AOSTiming/GPS", 0, true));
        if(CORBA::is_nil(gps.in()) == false)
        {
            ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Got GPS component."));
        }
        else
        {
            ACS_LOG(LM_SOURCE_INFO, "main", (LM_ERROR,
                "Unable to access the GPS component."));
            client.logout();
            return - 1;
        }

        ACSErr::Completion_var completion;

        // Get the GPS_Time value.
        ACS::uLongLong time = gps->GPS_Time()->get_sync(completion.out());
        CompletionImpl c(completion);
        if(c.isErrorFree() == false)
        {
            c.log();
        }
        else
        {
            ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO,
                "GPS time is %llu", time));
        }

        // Get the Antenna_State value.
        Control::GPSAntennaState state(gps->Antenna_State()->get_sync(
            completion.out()));
        c = completion;
        if(c.isErrorFree() == false)
        {
            c.log();
        }
        else
        {
            if(state == Control::ANT_OK)
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO,
                    "Antenna state is OK"));
            }
            else if(state == Control::ANT_OPEN)
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO,
                    "Antenna state is OPEN"));
            }
            else if(state == Control::ANT_SHORTED)
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO,
                    "Antenna state is SHORTED"));
            }
            else
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_ERROR,
                    "Antenna in illegal state (%i)", state));
            }
        }

        // Get the GPS_Locked value.
        ACS::Bool locked(gps->GPS_Locked()->get_sync(completion.out()));
        c = completion;
        if(c.isErrorFree() == false)
        {
            c.log();
        }
        else
        {
            if(locked == ACS::acsTRUE)
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "GPS is locked"));
            }
            else
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "GPS is not locked"));
            }
        }

        // Get the PLL_Locked value.
        locked = gps->PLL_Locked()->get_sync(completion.out());
        c = completion;
        if(c.isErrorFree() == false)
        {
            c.log();
        }
        else
        {
            if(locked == ACS::acsTRUE)
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "PLL is locked"));
            }
            else
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "PLL is not locked"));
            }
        }

        // Get the RS232_OK value.
        ACS::Bool rs232(gps->RS232_OK()->get_sync(completion.out()));
        c = completion;
        if(c.isErrorFree() == false)
        {
            c.log();
        }
        else
        {
            if(rs232 == ACS::acsTRUE)
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "RS232 is OK"));
            }
            else
            {
                ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "RS232 is not OK"));
            }
        }

        // Get the time error value.
        CORBA::Double timeError(gps->Time_Error()->get_sync(completion.out()));
        c = completion;
        if(c.isErrorFree() == false)
        {
            c.log();
        }
        else
        {
            ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO,
                "Time error is %f", timeError));
        }
    }
    catch(...)
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_ERROR, "Error during GPS access."));
        client.releaseComponent("CONTROL/AOSTiming/GPS");
        client.logout();
        return - 1;
    }

    client.releaseComponent("CONTROL/AOSTiming/GPS");
    client.logout();
    return 0;
}
