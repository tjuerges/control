// @(#) $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@alma.org
//

#ifndef GPSDEVICE_H
#define GPSDEVICE_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <string>
#include <map>
#include <acstimeC.h>
#include <logging.h>
#include <ControlExceptions.h>
#include "GPSC.h"


/**
 * \brief Low-level GPS Device Driver
 *
 * A class representing the GPS receiver device.  All access to the hardware
 * device is performed using the RS232 serial port connected to it.
 *
 * This is a Singleton class. Practical reasons for a Singleton are that there's
 * a single GPS device that's accessed only through a specific serial port.
 * The Singleton implemented by providing an instance() function to access
 * the class. The constructor and destructor are private.
 */
class GPSDevice
{
    public:
    /**
     * Class is a Singleton and only accessable through this method.  See
     * the discussion above.
     *
     * \exception ControlExceptions::GPSTimeErrorExImpl
     */
    static GPSDevice* instance(const bool simGPS,
        const std::string& serialPort = std::string());
    /**
     * Method to delete instance
     */
    static void deleteInstance();

    /**
     * Initialize the GPS device
     *
     * \exception ControlExceptions::GPSTimeErrorExImpl
     */
    void initGPS();

    /**
     * Waits for next 1 PPS tick from GPS, and then returns time corresponding
     * to the tick.
     *
     * \return GPS time in 100ns units.
     *
     * \exception ControlExceptions::GPSTimeErrorExImpl
     */
    ACS::Time getTime();

    /**
     * Returns antenna state.
     *
     * \return ANT_OK, ANT_OPEN, or ANT_SHORT
     *
     * \exception ControlExceptions::GPSTimeErrorExImpl
     */
    Control::GPSAntennaState getAntennaState();
    /**
     * Returns Phase Locked Loop (PLL) state.
     *
     * \return true- Unlocked or false- OK
     *
     * \exception ControlExceptions::GPSTimeErrorExImpl
     */
    bool PLLLocked();
    /**
     * Returns GPS state.
     *
     * \return true- unlocked or false- locked
     *
     * \exception ControlExceptions::GPSTimeErrorExImpl
     */
    bool GPSLocked();
    /**
     * Returns RS232 communications state.
     *
     * \return true- Bad or false- OK
     *
     * \exception ControlExceptions::GPSTimeErrorExImpl
     */
    bool RS232OK();

    /**
     * Returns worse case time error.
     *
     * \return time error in seconds
     *
     *\exception ControlExceptions::GPSTimeErrorExImpl
     */
    double getTimeError();

    private:
    /// Returns time obtained from GPS.
    ///
    /// \param keepmsec false - truncate returned time to even second
    ///                  true  - use millisecond field from GPS
    /// \return GPS time in 100ns units.
    ///
    /// \note Should calls to this method be synchronized with the GPS 1 PSS
    ///       tick to avoid bad values?  Fritz sez "It seems the 1 PPS is
    ///       generated separately from the communications.  The receiver com
    ///       port always seems to give the right time."
    ///
    /// \exception ControlExceptions::GPSTimeErrorExImpl
    ACS::Time time(const bool keepmsec = true);

    /// \return year from GPS
    ///
    /// \exception ControlExceptions::GPSTimeErrorExImpl
    unsigned short getYear();

    /// Returns antenna, PLL, and GPS state from GPS
    ///
    /// \exception ControlExceptions::GPSTimeErrorExImpl
    void getFaults(Control::GPSAntennaState& ant, bool& pll, bool& gps);

    /// Sends command to and then gets response from GPS
    ///
    /// \exception ControlExceptions::GPSTimeErrorExImpl
    std::string commandGPS(const std::string& command);

    /// Reads a response string from the GPS serial port
    ///
    /// \exception ControlExceptions::GPSTimeErrorExImpl
    std::string getResponse();

    /// Converts year, doy, hr, min, sec, ms to ALMA timeService units
    /// \return time as returned by the GPS to ACS::Time.
    ACS::Time convertTime(const unsigned short year,
        const unsigned short doy, const unsigned short hr,
        const unsigned short min, const unsigned short sec,
        const unsigned short msec);

    /**
     * \brief Return a substring value either as numeric or as string value.
     *
     * \param str: input string.
     * \param delimiter: string which contains delimiting characters. The first
     * occurence of any of them will match.
     * \param startPos: position in \a str from where the search starts. Will be
     * modified to point to the begin of the found substring.
     *
     * \return The value of the substring. The type depends on \a T,
     */
    template< typename T > T getSubstringValue(const std::string& str,
        const std::string& delimiter, std::string::size_type& pos);

    /**
     * \brief Split input std::string splitThis into key/value pairs.
     *
     * \param entrySplitter: each key/value pair is separated by
     * the other through this string.
     * \param keyValueSplitter: each key is separated from the value
     * through this string.
     *
     * \return Returns a map containing the key/value pairs as strings.
     */
    std::map< std::string, std::string > splitString(
        const std::string& splitThis,
        const std::string& entrySplitter, const std::string& keyValueSplitter);

    /**
     * \brief Print std::string as hexadecimal ASCII values.
     *
     * \param std::string to be printed.
     */
    void hexPrint(const std::string& printThis) const;

    /// ctor and dtor are private as class is Singleton
    ///
    /// \exception ControlExceptions::GPSTimeErrorExImpl
    GPSDevice(const bool simGPS, const std::string& serialPort);

    ~GPSDevice();

    /// holds pointer to class object
    static GPSDevice* m_instance_p;

    const unsigned char ctrlc;

    /// worst case time error
    double m_error;

    /// GPS serial port file desc
    int m_gps_fd;

    /// Parallel I/O IP module file desc
    int m_pio_fd;

    /// Semaphore implementing critical regions
    sem_t m_mutex;

    /// Tasks that wait for this semaphore are released on next GPS 1 PPS tick
    //    static SEM_ID m_tick;

    /// copy and assignment are not allowed
    GPSDevice(const GPSDevice&);
    GPSDevice& operator=(const GPSDevice&);

    // GPS simulation flag
    bool m_simGPS;
};

#endif  /* GPSDEVICE_H */
