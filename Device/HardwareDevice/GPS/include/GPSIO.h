#ifndef GPS_IO_H
#define GPS_IO_H
/* @(#) $Id$
 *
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <baciDevIO.h>
#include <acsutilTimeStamp.h>

#include <ControlExceptions.h>

#include "GPSDevice.h"


//------------------------------------------------------------------------------

class GPSTimeIO: public DevIO<ACS::Time>
{
    public:
    GPSTimeIO()
    {};

    virtual ~GPSTimeIO()
    {};

    virtual bool initializeValue()
    {
        return false;
    };

    /// \exception ControlExceptions::GPSTimeErrorExImpl
    virtual ACS::Time read(ACS::Time& timestamp)
    {
        timestamp = ::getTimeStamp();
        ACS::Time t(0ULL);

        try
        {
            t = GPSDevice::instance(true)->getTime();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__,__LINE__,
                "GPSTimeIO::read");
        }

        return t;
    };
};

//------------------------------------------------------------------------------

class GPSAntennaStateIO: public DevIO<Control::GPSAntennaState>
{
    public:
    GPSAntennaStateIO()
    {};

    virtual ~GPSAntennaStateIO()
    {};

    virtual bool initializeValue()
    {
        return false;
    };

    /// \exception ControlExceptions::GPSTimeErrorExImpl
    virtual Control::GPSAntennaState read(ACS::Time& timestamp)
    {
        timestamp = ::getTimeStamp();
        Control::GPSAntennaState ant;
        try
        {
            ant = GPSDevice::instance(true)->getAntennaState();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__,__LINE__,
                "GPSAntennaStateIO::read");
        }

        return ant;
    };
};

//------------------------------------------------------------------------------

class PLL_LockedIO: public DevIO<ACS::Bool>
{
    public:
    PLL_LockedIO()
    {};

    virtual ~PLL_LockedIO()
    {};

    virtual bool initializeValue()
    {
        return false;
    };

    /// \exception ControlExceptions::GPSTimeErrorExImpl
    virtual ACS::Bool read(ACS::Time& timestamp)
    {
        timestamp = ::getTimeStamp();
        bool state(false);
        try
        {
            state = GPSDevice::instance(true)->PLLLocked();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__,__LINE__,
                "PLL_LockedIO::read");
        }

        if(state == true)
        {
            return ACS::acsTRUE;
        }
        else
        {
            return ACS::acsFALSE;
        }
    };
};

//------------------------------------------------------------------------------

class GPS_LockedIO: public DevIO<ACS::Bool>
{
    public:

    GPS_LockedIO()
    {};

    virtual ~GPS_LockedIO()
    {};

    virtual bool initializeValue()
    {
        return false;
    };

    /// \exception ControlExceptions::GPSTimeErrorExImpl
    virtual ACS::Bool read(ACS::Time& timestamp)
    {
        timestamp = ::getTimeStamp();
        bool state(false);
        try
        {
            state = GPSDevice::instance(true)->GPSLocked();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__,__LINE__,
                "GPS_LockedIO::read");
        }

        if(state == true)
        {
            return ACS::acsTRUE;
        }
        else
        {
            return ACS::acsFALSE;
        }
    };
};

//------------------------------------------------------------------------------

class RS232_OKIO: public DevIO<ACS::Bool>
{
    public:

    RS232_OKIO()
    {};

    virtual ~RS232_OKIO()
    {};

    virtual bool initializeValue()
    {
        return false;
    };

    /// \exception ControlExceptions::GPSTimeErrorExImpl
    virtual ACS::Bool read(ACS::Time& timestamp)
    {
        timestamp = ::getTimeStamp();
        bool state(false);
        try
        {
            state = GPSDevice::instance(true)->RS232OK();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__,__LINE__,
                "GPS_LockedIO::read");
        }

        if(state == true)
        {
            return ACS::acsTRUE;
        }
        else
        {
            return ACS::acsFALSE;
        }
    };
};

//------------------------------------------------------------------------------

class GPSTimeErrorIO: public DevIO<CORBA::Double>
{
    public:

    GPSTimeErrorIO()
    {};

    virtual ~GPSTimeErrorIO()
    {};

    virtual bool initializeValue()
    {
        return false;
    };

    /// \exception ControlExceptions::GPSTimeErrorExImpl
    virtual CORBA::Double read(ACS::Time& timestamp)
    {
        timestamp = ::getTimeStamp();
        double error(-1e99);
        try
        {
            error = GPSDevice::instance(true)->getTimeError();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__,__LINE__,
                "GPS_LockedIO::read");
        }
        return error;
    };
};

#endif  /* GPS_IO_H */

