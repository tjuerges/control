// @(#) $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@alma.org
//

#ifndef GPSIMPL_H
#define GPSIMPL_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

// Forward declarations for classes that this component uses
template< class T > class DevIO;

//Contains the defintion of the standard superclass for C++ components
#include <hardwareDeviceImpl.h>

#include <baciROdouble.h>
#include <baciROuLongLong.h>
#include <enumpropROImpl.h>

///Include the smart pointer for the properties
#include <baciSmartPropertyPointer.h>

// Include control exceptions
#include <ControlExceptions.h>

//CORBA servant header
#include "GPSS.h"

//Custom DevIO for serial port properties
#include "GPSIO.h"
#include "GPSDevice.h"

// For serial port string.
#include <string>


class GPSImpl: public virtual POA_Control::GPS,
    public Control::HardwareDeviceImpl
{
    public:
    /**
     * Constructor
     * \param poa Poa which will activate this and also all other components.
     * \param name component's name. This is also the name that will
     * be used to find the configuration data for the component in the
     * Configuration Database.
     */
    GPSImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    ~GPSImpl();


    /// \exceptionControlExceptions::GPSTimeErrorEx
    void GPS_init();

    // Properties
    virtual ACS::ROuLongLong_ptr GPS_Time();

    virtual Control::ROGPSAntennaState_ptr Antenna_State();

    virtual ACS::ROBool_ptr PLL_Locked();

    virtual ACS::ROBool_ptr GPS_Locked();

    virtual ACS::ROBool_ptr RS232_OK();

    virtual ACS::ROdouble_ptr Time_Error();


    protected:
    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwStartAction();

    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwConfigureAction();

    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwInitializeAction();

    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwOperationalAction();

    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwStopAction();


    private:
    /**
     * No default constructor.
     */
    GPSImpl();

    /**
     * Copy constructor is not allowed following the ACS design rules.
     */
    GPSImpl(const GPSImpl&);

    /**
     * Assignment operator is not allowed due to ACS design rules.
     */
    GPSImpl& operator=(const GPSImpl&);

    /**
     * Helper method which loads the string for the serial device and the
     * simulation setting from the CDB.
     * \exception cdbErrType::CDBRecordDoesNotExistExImpl
     */
    void loadSettingsFromCdb(std::string& device, std::string& simulation) const;


    maci::ContainerServices* containerServices_m;

    // Properties
    baci::SmartPropertyPointer< baci::ROuLongLong > gpsTime_sp;
    GPSTimeIO* gpsTimeDevio_p;

    baci::SmartPropertyPointer< ROEnumImpl< ACS_ENUM_T(Control::GPSAntennaState),
        POA_Control::ROGPSAntennaState > > antennaState_sp;
    GPSAntennaStateIO* antennaStateDevio_p;

    baci::SmartPropertyPointer< ROEnumImpl< ACS_ENUM_T(ACS::Bool),
        POA_ACS::ROBool > > pllLocked_sp;
    PLL_LockedIO* pllLockedDevio_p;

    baci::SmartPropertyPointer< ROEnumImpl< ACS_ENUM_T(ACS::Bool),
        POA_ACS::ROBool > > gpsLocked_sp;
    GPS_LockedIO* gpsLockedDevio_p;

    baci::SmartPropertyPointer< ROEnumImpl< ACS_ENUM_T(ACS::Bool),
        POA_ACS::ROBool > > rs232Ok_sp;
    RS232_OKIO* rs232OkDevio_p;

    baci::SmartPropertyPointer< baci::ROdouble > timeError_sp;
    GPSTimeErrorIO* timeErrorDevio_p;

    /// Instance of GPS device
    GPSDevice* gps_p;

    /// The name of the component with the "alma/" prefix. We need the name to
    /// access the CDB
    std::string fullName_m;

    std::string myName_m;

    /// Serial port for GPS communication.
    std::string serialPort_m;

    /// GPS device simulation flag.
    bool simGPS_m;
};

#endif  /* GPSIMPL_H */
