#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# who       when      what
# --------  --------  ----------------------------------------------
# rsoto   2007-07-30  created 
#
# $Id$
#

"""
This module is part of the Control Command Language.
Defines the GPS class.
"""


import CCL.HardwareDevice
import CCL.logging
import Acspy.Common.Err
import Acspy.Common.ErrorTrace
import ControlExceptions


class GPS(CCL.HardwareDevice.HardwareDevice):
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The GPS class is a python proxy to the GPS
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponentNonSticky
        (stickyFlag = False, default) or getComponent (stickyFlag =
        True).
        
        The GPS receiver is accessed via a RS-232 serial connection.
        A cable connects the CPU serial port to the GPS receiverwithin
        a single antenna. 
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.GPS
        # create the object
        gps = CCL.GPS.GPS()
        # Put the hardware in operation state
        gps.hwConfigure(); gps.hwInitialize(); gps.hwOperational();
        # Initialize the GPS
        gps.GPS_init()
        # Get the GPS Time
        gps.GET_GPS_TIME()
        # Get the current antenna status
        gps.GET_ANTENNA_STATE();
        # Get the Phase Locked Loop status
        gps.GET_PPL_LOCKED()
        # Get the GPS Phase Locked Loop status
        gps.GET_GPS_LOCKED()
        # Get the RS232 communication port status
        gps.GET_RS232_OK()
        # Get the worst case time error
        gps.GET_TIME_ERROR()
        # destroy the component
        del(gps)
        '''
        # initialize the base class
        if componentName == None:
            componentName = "CONTROL/AOSTiming/GPS"
            
        self.__logger = CCL.logging.getLogger()
        self.__logger.logDebug("Creating GPS CCL Object.")

        CCL.HardwareDevice.HardwareDevice.__init__(self, componentName, stickyFlag)

    def __del__(self):
        CCL.HardwareDevice.HardwareDevice.__del__(self)

    def GPS_init(self):
        '''
        Initialze the GPS device

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        timeStamp = 0
        try:
            timeStamp = self._HardwareDevice__hw.GPS_init() 
        except ControlExceptions.GPSTimeErrorEx, ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);
            raise ex
        return timeStamp


    def GET_GPS_TIME(self):
        '''
        Waits for the next 1 PPS tick from the GPS and then returns the 
        external TAI time of the tick.
        Return TAI time of last 1 PPS in ACS time units (100 ns).
         
        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        (value, completion) = self._HardwareDevice__hw._get_GPS_Time().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(completion.log()).log(
                self.__logger)

    def GET_ANTENNA_STATE(self):
        '''
        Return the value of Antenna State Property.
        This Property indicates the state of the antenna.  The Property has 
        the three states listed here.
        ANT_OK      - functioning properly
        ANT_OPEN
        ANT_SHORTED

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        (value, completion) = self._HardwareDevice__hw._get_Antenna_State().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(completion.log()).log(
                self.__logger)

    def GET_PPL_LOCKED(self):
        '''
        Returns the Phase Locked Loop (PLL) State Property.
        This Property indicates the state of the PLL.  
        The state is either ACS::acsTRUE os ACS::acsFALSE.

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        (value, completion) = self._HardwareDevice__hw._get_PLL_Locked().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(completion.log()).log(
                self.__logger)

    def GET_GPS_LOCKED(self):
        '''
        Phase Locked Loop (GPS) State Property.
        This Property indicates the state of the GPS.  The Property has 
        the two states listed here.  See ticsRefGPS::GPSState.
        GPS_LOCKED
        GPS_UNLOCKED

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        (value, completion) = self._HardwareDevice__hw._get_GPS_Locked().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(completion.log()).log(
                self.__logger)


    def GET_RS232_OK(self):
        '''
        Returns the RS232 Communications State Property.
        This Property indicates the state of the RS232 communications with 
        the GPS receiver.  The Property has the two states listed here.  
        See ticsRefGPS::RS232State.
        RS232_OK  - communication is established and working properly
        RS232_BAD - there  a problem with the communication

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        (value, completion) = self._HardwareDevice__hw._get_RS232_OK().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(completion.log()).log(
                self.__logger)

    def GET_TIME_ERROR(self):
        '''
        Returns the Worst-case time error Property.
        This Property holds the error greater than or equal to the worst 
        time error in seconds.

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        (value, completion) = self._HardwareDevice__hw._get_Time_Error().get_sync()
        Acspy.Common.Err.addComplHelperMethods(completion)
        if completion.isErrorFree() == True:
            return value
        else:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(completion.log()).log(
                self.__logger)
