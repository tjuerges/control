// $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//
//        Internet email: alma-sw-admin@nrao.edu
//


#include <unistd.h>
#include <termios.h>
#include <string>
#include <sstream>
#include <iomanip>
// For AUTO_TRACE.
#include <loggingMACROS.h>
// For audience logs.
#include <LogToAudience.h>
#include <acstimeC.h>
#include <acsutilTimeStamp.h>
#include <TETimeUtil.h>
#include <GPSDevice.h>
#include <GPSC.h>

#include <Time_Value.h>
#include <OS_NS_unistd.h>


// static variables
GPSDevice* GPSDevice::m_instance_p(0);


//------------------------------------------------------------------------------

GPSDevice* GPSDevice::instance(const bool simGPS, const std::string& serialPort)
{
    const std::string fnName("GPSDevice::instance");
    AUTO_TRACE(fnName.c_str());

    if(m_instance_p == 0)
    {
        try
        {
            // do the onetime init stuff
            m_instance_p = new GPSDevice(simGPS, serialPort);
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
                fnName.c_str());
        }
        catch(...)
        {
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Unexpected exception caught while creating a "
                "GPSDevice instance.");
            ex.log();
            throw ex;
        }
    }

    return m_instance_p;
}

//------------------------------------------------------------------------------

void GPSDevice::deleteInstance()
{
    const std::string fnName("GPSDevice::deleteInstance");
    AUTO_TRACE(fnName.c_str());

    delete m_instance_p;
    m_instance_p = 0;

    return;
}

//------------------------------------------------------------------------------

GPSDevice::GPSDevice(const bool simGPS, const std::string& serialPort):
    ctrlc(0x3),
    m_gps_fd(-1)
{
    const std::string fnName("GPSDevice::GPSDevice");
    AUTO_TRACE(fnName.c_str());

    // Create semaphore that implements critical region
    if(sem_init(&m_mutex, 0, 1) < 0)
    {
        ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        ex.addData("Detail", "Failed to create GPS port mutex");
        ex.log();
        throw ex;
    }

    // Set simulation flag
    m_simGPS = simGPS;

    if(m_simGPS == false)
    {
        // Wait some time for the kernel/udev to apply the proper access
        // right after the device node (/dev/ttyS0 for the real device)
        // has been created. The device node will be created as soon as
        // the kernel recognises the driver for a device node. Then the
        // udev event will be sent out which udev receives at some point
        // in the future. Let's try it every 10s for one minute. This
        // will guarantee that the permissions are properly set.
        unsigned short loop(6U);
        sem_wait(&m_mutex);
        do
        {
            std::ostringstream msg;
            msg << "Waiting for 10s for udev to set proper access right of the "
                "serial port device node.";
            LOG_TO_OPERATOR(LM_INFO, msg.str());

            ACE_Time_Value waitFor(0ULL);
            waitFor.set(10.0);
            ACE_OS::select(0, 0, 0, 0, waitFor);

            // open serialPort (/dev/ttyS[0-3] serial port connected to GPS
            m_gps_fd = ::open(serialPort.c_str(), O_RDWR, 0);
            if(m_gps_fd != -1)
            {
                break;
            }
            else
            {
                --loop;
            }
        }
        while(loop > 0U);
        sem_post(&m_mutex);

        if(m_gps_fd == -1)
        {
            sem_destroy(&m_mutex);

            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            std::ostringstream msg;
            msg << "Failed to open GPS port.  Check if the permissions of the "
                << "file "
                << serialPort
                << "are rwrwrw!";
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex;
        }

        try
        {
            initGPS();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            ::close(m_gps_fd);
            m_gps_fd = -1;
            sem_post(&m_mutex);
            sem_destroy(&m_mutex);

            ControlExceptions::GPSTimeErrorExImpl nex(ex, __FILE__, __LINE__,
                fnName.c_str());
            nex.addData("Detail", "Could not initialise the GPS hardware.");
            throw nex;
        }
    }
}


//------------------------------------------------------------------------------

GPSDevice::~GPSDevice()
{
    const std::string fnName("GPSDevice::~GPSDevice");
    AUTO_TRACE(fnName.c_str());

    if((m_simGPS == false) && (m_gps_fd != -1))
    {
        sem_wait(&m_mutex); // BEGIN CRITICAL REGION
        try
        {
            if(tcflush(m_gps_fd, TCIFLUSH) < 0)
            {
                ControlExceptions::GPSTimeErrorExImpl ex(__FILE__,
                    __LINE__, fnName.c_str());
                ex.addData("Detail", "Failed to flush GPS port.");
                ex.log();
                throw ex;
            }

            sem_post(&m_mutex);            // END CRITICAL REGION

            // Unlock keypad.
            std::string command("F06 OFF\r");
            std::string reply;
            try
            {
                reply = commandGPS(command);
            }
            catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
            {
                ControlExceptions::GPSTimeErrorExImpl nex(ex, __FILE__,
                    __LINE__, fnName.c_str());
                nex.addData("Detail", "Failed to send command to GPS port.");
                nex.addData("Command", command);
                throw nex;
            }

            if(reply.find("OK") == std::string::npos)
            {
                ControlExceptions::GPSTimeErrorExImpl ex(__FILE__,
                    __LINE__, fnName.c_str());
                ex.addData("Detail", "Failed to get correct reply from GPS "
                    "port.");
                ex.addData("Command", command);
                ex.addData("Reply", reply);
                ex.log();
                throw ex;
            }
        }
        /**
         * A destructor must not throw an exception.
         */
        catch(ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            ControlExceptions::GPSTimeErrorExImpl nex(ex, __FILE__, __LINE__,
                fnName.c_str());
            nex.addData("Detail", "The destruction of the GPS device instance "
                "failed. It it possible that following operations on the GPS "
                "device will fail.");
            nex.log();
        }

        ::close(m_gps_fd);
        m_gps_fd = -1;
    }

    sem_destroy(&m_mutex);
}

//------------------------------------------------------------------------------
void GPSDevice::initGPS()
{
    const std::string fnName("GPSDevice::initGPS");
    AUTO_TRACE(fnName.c_str());

    if((m_simGPS == false) && (m_gps_fd != -1))
    {
        sem_wait(&m_mutex);
        // terminal input properties
        struct termios oldtio, newtio;
        if(tcgetattr(m_gps_fd, &oldtio) < 0)
        {
            sem_post(&m_mutex);
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Failed to get GPS port setting.");
            ex.log();
            throw ex;
        }

        // Set new terminal settings
        memcpy(&newtio, &oldtio, sizeof(oldtio));
        newtio.c_cflag = B9600 | CS7 | PARENB | CLOCAL | CREAD;
        newtio.c_iflag = INPCK;
        newtio.c_oflag = 0;
        newtio.c_lflag = 0;
        newtio.c_cc[VMIN] = 0;
        newtio.c_cc[VTIME] = 50;

        if(tcflush(m_gps_fd, TCIFLUSH) < 0)
        {
            sem_post(&m_mutex);
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Failed to flush GPS port.");
            ex.log();
            throw ex;
        }

        if(tcsetattr(m_gps_fd, TCSANOW, &newtio) < 0)
        {
            sem_post(&m_mutex);
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Failed to set GPS port setting.");
            ex.log();
            throw ex;
        }

        // interrupt GPS F08 continuous run with ^C
        if(::write(m_gps_fd, &ctrlc, 1) < 0)
        {
            sem_post(&m_mutex);
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Failed to write to GPS port.");
            ex.log();
            throw ex;
        }

        sem_post(&m_mutex); // END CRITICAL REGION

        // The first command after ctrlc fails sometimes.  This is because it is
        // not free running time output on the second.  When the receiver is
        // first turned on, it defaults to free run time output, so cover that
        // case by sending a benign command (request 12/24 hour format).
        try
        {
            commandGPS("F02\r");
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            ControlExceptions::GPSTimeErrorExImpl nex(ex, __FILE__, __LINE__,
                fnName.c_str());
            nex.addData("Detail", "Failed to send command F02 to GPS port.");
            throw nex;
        }

        // Flush gps port
        sem_wait(&m_mutex);    // BEGIN CRITICAL REGION
        if(tcflush(m_gps_fd, TCIFLUSH) < 0)
        {
            sem_post(&m_mutex);
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Failed to flush GPS port.");
            ex.log();
            throw ex;
        }

        sem_post(&m_mutex);            // END CRITICAL REGION

        // initialize GPS with these commands
        std::vector< std::string > commands;
            // F06 - lockout keypad
            commands.push_back("F06 ON\r");
            // F02 - set 24 hr time format
            commands.push_back("F02 24\r");
            // F05 - set time quality setup
            commands.push_back("F05 ON 1000 10000 100000 1000000\r");
            // F11 - set time output format
            commands.push_back("F11 DDD:HH:MM:SS.mmmq\r");
            // F69 - set UTC time mode
            commands.push_back("F69 UTC\r");
            // F68 - set year entry
            // N.B. This is only needed if GPS NVRAM is corrupted (dead battery)
            // commands.push_back("F68 1996\r");

        for(std::vector< std::string >::const_iterator iter(
            commands.begin()); iter != commands.end(); ++iter)
        {
            std::string reply;
            try
            {
                reply = commandGPS(*iter);
            }
            catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
            {
                ControlExceptions::GPSTimeErrorExImpl nex(ex, __FILE__,
                    __LINE__, fnName.c_str());
                nex.addData("Detail", "Failed to send command to GPS port");
                nex.addData("Command", *iter);
                throw nex;
            }

            if(reply.find("OK") == std::string::npos)
            {
                ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                    fnName.c_str());
                ex.addData("Detail", "Failed to get correct reply from GPS "
                    "port");
                ex.addData("Command", *iter);
                ex.addData("Reply", reply);
                ex.log();
                throw ex;
            }
        }
    }
}

//------------------------------------------------------------------------------

ACS::Time GPSDevice::getTime()
{
    const std::string fnName("GPSDevice::getTime");
    AUTO_TRACE(fnName.c_str());

    ACS::Time t(0ULL);
    try
    {
        t = GPSDevice::time();
    }
    catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
            fnName.c_str());
    }

    #ifdef GPS_DEBUG
    std::ostringstream msg;
    msg << "time is "
        << t;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    #endif

    return t;
}

//------------------------------------------------------------------------------

Control::GPSAntennaState GPSDevice::getAntennaState()
{
    const std::string fnName("GPSDevice::getAntennaState");
    AUTO_TRACE(fnName.c_str());

    Control::GPSAntennaState ant(Control::ANT_OPEN);
    bool dummy(false);
    try
    {
        getFaults(ant, dummy, dummy);
    }
    catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
            fnName.c_str());
    }

    #ifdef GPS_DEBUG
    std::ostringstream msg;
    msg << "state is "
        << ant;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    #endif

    return ant;
}

//------------------------------------------------------------------------------

bool GPSDevice::PLLLocked()
{
    const std::string fnName("GPSDevice::PLLLocked");
    AUTO_TRACE(fnName.c_str());

    bool pll(false);
    Control::GPSAntennaState dummyi(Control::ANT_OPEN);
    bool dummyb(false);

    try
    {
        getFaults(dummyi, pll, dummyb);
    }
    catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
            fnName.c_str());
    }

    #ifdef GPS_DEBUG
    std::ostringstream msg;
    msg << "PLL is"
        << (pll == true ? " " : " not ")
        << "locked.";
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    #endif

    return pll;
}

//------------------------------------------------------------------------------

bool GPSDevice::GPSLocked()
{
    const std::string fnName("GPSDevice::GPSLocked");
    AUTO_TRACE(fnName.c_str());

    bool gps(false);
    Control::GPSAntennaState dummyi(Control::ANT_OPEN);
    bool dummyb(false);

    try
    {
        getFaults(dummyi, dummyb, gps);
    }
    catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
            fnName.c_str());
    }

    #ifdef GPS_DEBUG
    std::ostringstream msg;
    msg << "GPS is"
        << (gps == true ? " " : " not ")
        << "locked.";
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    #endif

    return gps;
}

//------------------------------------------------------------------------------

bool GPSDevice::RS232OK()
{
    const std::string fnName("GPSDevice::RS232OK");
    AUTO_TRACE(fnName.c_str());

    Control::GPSAntennaState ant(Control::ANT_OPEN);
    bool dummy(false);
    // Probe RS232 link by requesting antenna state
    try
    {
        getFaults(ant, dummy, dummy);
    }
    catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
            fnName.c_str());
    }

    return true;
}

//------------------------------------------------------------------------------

double GPSDevice::getTimeError()
{
    const std::string fnName("GPSDevice::getTimeError");
    AUTO_TRACE(fnName.c_str());

    if(m_simGPS == false)
    {
        if(m_gps_fd == -1)
        {
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "The serial port connection is not up.");
            ex.log();
            throw ex;
        }

        /**
         * Query worst-case error from GPS.
         */
        try
        {
            std::string reply(commandGPS("F13\r"));
            std::string::size_type pos(reply.find(" "));
            if(pos == std::string::npos)
            {
                ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                    fnName.c_str());
                ex.addData("Detail", "Cannot find the max. error value in the "
                    "GPS string.");
                ex.log();
                throw ex;
            }
            else
            {
                std::istringstream error_str(reply.substr(pos));
                error_str >> m_error;
            }
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            ControlExceptions::GPSTimeErrorExImpl nex(ex, __FILE__, __LINE__,
                fnName.c_str());
            nex.addData("Detail", "Requesting worst-case time error from GPS "
                "device failed.");
            throw nex;
        }
    }
    else
    {
        m_error = 0.0;
    }

    return m_error;
}

//------------------------------------------------------------------------------

ACS::Time GPSDevice::time(const bool keepmsec)
{
    const std::string fnName("GPSDevice::time");
    AUTO_TRACE(fnName.c_str());

    // need to worry about the end/start of the year
    // because fetches of year and remaining date is not atomic
    // time is returned as 179:21:32:45.224?

    if(m_simGPS == false)
    {
        if(m_gps_fd == -1)
        {
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "The serial port connection is not up.");
            ex.log();
            throw ex;
        }

        std::string reply;
        unsigned short year(0U);

        try
        {
            // time request is unlike others, see manual if you really want to
            // know
            std::string answer(commandGPS("F09\rT"));

            sem_wait(&m_mutex);

            if(::write(m_gps_fd, &ctrlc, 1) < 0)
            {
                sem_post(&m_mutex);

                ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                    fnName.c_str());
                ex.addData("Detail", "Failed to write to GPS port.");
                ex.log();
                throw ex;
            }

            sem_post(&m_mutex);

            reply = answer.substr(1); // skip SOH
            year = getYear();
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
                fnName.c_str());
        }

        #ifdef GPS_DEBUG
        std::ostringstream msg;
        msg << "time is "
            << reply;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif

        /**
         * The returned GPS time string has this format:
         * DDD:HH:MM:SS.mmmQ
         * DDD: Day of year, HH: hours, MM: minutes, SS: seconds,
         * mmm: milliseconds, Q: quality indicator.
         */
        const std::string delimiter(":.");
        std::string::size_type pos(0);
        unsigned short doy(getSubstringValue< unsigned short >(
            reply, delimiter, pos));
        ++pos;
        unsigned short hour(getSubstringValue< unsigned short >(
            reply, delimiter, pos));
        ++pos;
        unsigned short min(getSubstringValue< unsigned short >(
            reply, delimiter, pos));
        ++pos;
        unsigned short sec(getSubstringValue< unsigned short >(
            reply, delimiter, pos));
        ++pos;
        unsigned short msec(getSubstringValue< unsigned short >(
            reply, delimiter, pos));

        #ifdef GPS_DEBUG
        msg.str("");
        msg << "year "
            << year
            << ", day of the year "
            << doy
            << ", time "
            << hour
            << ":"
            << min
            << ":"
            << sec
            << "."
            << msec;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif

        // Convert GPS year,doy,hour,min,sec,msec to timeSevice epoch.
        if(keepmsec == false)
        {
            return (convertTime(year, doy, hour, min, sec, 0U));
        }
        else
        {
            return (convertTime(year, doy, hour, min, sec, msec));
        }
    }
    else
    {
        /**
         * Simulation of GPS.
         * The global assumption is that the array runs on UTC.
         * The UTC time is provided by an NTP server. So we have to return
         * here a time which is 14s later than GPS: GPS = UTC + 14s.
         */
        return (::getTimeStamp());
    }
}

//------------------------------------------------------------------------------


unsigned short GPSDevice::getYear()
{
    const std::string fnName("GPSDevice::getYear");
    AUTO_TRACE(fnName.c_str());

    unsigned short year;

    try
    {
        std::string reply(commandGPS("F68\r"));
        std::string::size_type pos(reply.find(" "));
        if(pos == std::string::npos)
        {
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Cannot find the year in the GPS string.");
            ex.log();
            throw ex;
        }
        else
        {
            std::istringstream year_str(reply.substr(pos));
            year_str >> year;
        }
    }
    catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
            fnName.c_str());
    }

    return year;
}

//------------------------------------------------------------------------------

void GPSDevice::getFaults(Control::GPSAntennaState& antenna,
    bool& pll, bool& gps)
{
    const std::string fnName("GPSDevice::getFaults");
    AUTO_TRACE(fnName.c_str());

    // F72 returns: Antenna:<ANT_status>PLL:<PLL_status>GPS:<GPS_status>
    // e.g.: Antenna: Open  PLL: OK  GPS: Unlocked
    if(m_simGPS == false)
    {
        if(m_gps_fd == -1)
        {
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "The serial port connection is not up.");
            ex.log();
            throw ex;
        }

        std::map< std::string, std::string > reply;
        try
        {
            const std::string command("F72\r");
            std::string response(commandGPS(command));
            response.assign(response, command.length(), response.length());
            reply = splitString(response, "  ", ": ");
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
                fnName.c_str());
        }

        if(reply["Antenna"] == "SHORT")
        {
            antenna = Control::ANT_SHORTED;
        }
        else if(reply["Antenna"] == "OPEN")
        {
            antenna = Control::ANT_OPEN;
        }
        else
        {
            antenna = Control::ANT_OK;
        }

        if(reply["PLL"] == "OK")
        {
            pll = true;
        }
        else
        {
            pll = false;
        }

        // get GPS state
        if(reply["GPS"] == "Locked")
        {
            gps = true;
        }
        else
        {
            gps = false;
        }

        #ifdef GPS_DEBUG
        std::ostringstream msg;
        msg << "Antenna = "
            << reply["Antenna"]
            << ", PLL = "
            << reply["PLL"]
            << ", GPS = "
            << reply["GPS"]
            << ".";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif
    }
    else
    {
        antenna = Control::ANT_OK;
        pll = true;
        gps = true;
    }
}

//------------------------------------------------------------------------------

std::string GPSDevice::commandGPS(const std::string& command)
{
    const std::string fnName("GPSDevice::commandGPS");
    AUTO_TRACE(fnName.c_str());

    std::string response;

    // ignore call if given command is null
    if(command.length() > 0)
    {
        sem_wait(&m_mutex);    // BEGIN CRITICAL REGION

        // make sure the serial input is empty
        if(tcflush(m_gps_fd, TCIFLUSH) < 0)
        {
            sem_post(&m_mutex);            // END CRITICAL REGION

            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Failed to flush GPS port");
            ex.log();
            throw ex;
        }

        if(::write(m_gps_fd, command.c_str(), command.length()) < 0)
        {
            sem_post(&m_mutex);            // END CRITICAL REGION

            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail", "Failed to write to GPS port");
            ex.addData("Command", command);
            ex.log();
            throw ex;
        }

        #ifdef GPS_DEBUG
        std::ostringstream msg;
        msg << "Sent command \""
            << command
            << "\" to GPS device.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif

        try
        {
            unsigned int tries(0U);
            /**
             * We wait after the sent command for some time so that everything has
             * been transmitted and there was enough time to receive the response.
             * The port speed is 9600 Bit/s, 7 Bit, Parity enabled.
             * This adds up to average eight bits per byte. One byte takes
             * 1.0s / (9600.0 / 8.0) = 0.000833333333...s.
             * Usually a command returns "OK\r" which is 3 bytes.
             * Add the three bytes to the command length, multiply it with the
             * byte length (in s) and sleep for this duration.
             * ACE_OS::sleep expects an ACE_Time_Value as argument which
             * consists of a (second, usend) pair.
             *
             * Still bad/no response? Try again 5 times.
             */
            while((response.empty() == true) && (tries < 5U))
            {
                response = getResponse();
                ++tries;
                ACE_Time_Value sleepTime(0U,
                    static_cast< suseconds_t >(1.0 / (9600.0 / 8.0))
                        * 1000000U * (command.length() + 3));
                ACE_OS::sleep(sleepTime);
            }
        }
        catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
        {
            sem_post(&m_mutex);

            throw ControlExceptions::GPSTimeErrorExImpl(ex, __FILE__, __LINE__,
                fnName.c_str());
        }

        sem_post(&m_mutex);            // END CRITICAL REGION
    }

    return response;
}

//------------------------------------------------------------------------------

std::string GPSDevice::getResponse()
{
    const std::string fnName("GPSDevice::getResponse");
    AUTO_TRACE(fnName.c_str());

    std::string response;
    char c(0);

    while(response.length() < response.max_size())
    {
        if((::read(m_gps_fd, &c, 1) <= 0) || (c == '\0'))
        {
            ControlExceptions::GPSTimeErrorExImpl ex(__FILE__, __LINE__,
                fnName.c_str());
            ex.addData("Detail",
                "Failed to read from GPS port (timeout or framing error or "
                "parity error).");
            ex.log();
            throw ex;
        }

        if((c == '\r') || (c == '\n'))
        {
            break;
        }
        else
        {
            response += c;
        }
    }

    #ifdef GPS_DEBUG
    std::ostringstream msg;
    msg << "read response \""
        << response
        << "\" from GPS device";
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    hexPrint(response);
    #endif

    return response;
}


//------------------------------------------------------------------------------

/**
 * @brief Return a substring value either as numeric or as string value.
 *
 * @param str: input string.
 * @param delimiter: string which contains delimiting characters. The first
 * occurence of any of them will match.
 * @param startPos: position in \a str from where the search starts. Will be
 * modified to point to the begin of the found substring.
 *
 * @return The value of the substring. The type depends on \a T,
 */
template< typename T > T GPSDevice::getSubstringValue(const std::string& str,
    const std::string& delimiter, std::string::size_type& startPos)
{
    T returnValue(0);
    std::istringstream value;

    std::string::size_type pos(str.find_first_of(delimiter, startPos));
    if(pos == std::string::npos)
    {
        pos = str.size() - 1;
    }

    value.str(str.substr(startPos, pos - startPos));
    startPos = pos;
    value >> returnValue;

    return returnValue;
}

/**
 * @brief Split input std::string splitThis into key/value pairs.
 *
 * @param entrySplitter: each key/value pair is separated by
 * the other through this string.
 * @param keyValueSplitter: each key is separated from the value
 * through this string.
 *
 * @return Returns a map containing the key/value pairs as strings.
 */
std::map< std::string, std::string > GPSDevice::splitString(
    const std::string& splitThis,
    const std::string& entrySplitter, const std::string& keyValueSplitter)
{
    std::map< std::string, std::string > result;

    const std::string::size_type keyValueSplitterLength(
        keyValueSplitter.size());
    const std::string::size_type entrySplitterLength(entrySplitter.size());

    /**
     * Test if any of the input strings has 0 size. Return an empty map if
     * true.
     * Test if the key/value separator and the pair separator are the same.
     * Return an ampty map if true.
     */
    if((keyValueSplitterLength == 0)
    || (entrySplitterLength == 0)
    || (entrySplitter == keyValueSplitter))
    {
        return result;
    }

    std::string::size_type entryPosition(0);
    std::string::size_type valuePosition(0);
    std::string::size_type pos(0);

    std::string key("No value");
    std::string value("No value");

    do
    {
        valuePosition = splitThis.find(keyValueSplitter, entryPosition);
        if(valuePosition == std::string::npos)
        {
            break;
        }

        entryPosition = splitThis.find(entrySplitter, valuePosition);

        if(entryPosition == std::string::npos)
        {
            entryPosition = splitThis.size();
        }

        pos = splitThis.rfind(entrySplitter, valuePosition);
        if(pos == std::string::npos)
        {
            pos = 0;
        }
        else
        {
            pos += entrySplitterLength;
        }

        key.assign(splitThis, pos, (valuePosition - pos));
        valuePosition += keyValueSplitterLength;
        value.assign(splitThis, valuePosition, (entryPosition - valuePosition));
        entryPosition += entrySplitterLength;

        result[key] = value;
    }
    while(true);

    return result;
}
//------------------------------------------------------------------------------

ACS::Time GPSDevice::convertTime(const unsigned short year,
    const unsigned short doy, const unsigned short hr, const unsigned short min,
    const unsigned short sec, const unsigned short msec)
{
    const std::string fnName("GPSDevice::convertTime");
    AUTO_TRACE(fnName.c_str());

    //
    // following is lifted from timeService/ws/src/EpochImpl.cpp
    //
    // add number of days from Gregorian calendar start to start of current year
    const unsigned short yearMinusOne(year - 1U);
    ACS::Time accum(doy - 1ULL
        + yearMinusOne * 365U
        + yearMinusOne / 4U
        - yearMinusOne / 100U
        + yearMinusOne / 400U);

    // subtract days at starting date (1582-10-15)
    const unsigned long StartDate(577735UL);
    accum -= StartDate;

    // add contribution from hour, minute, second, and micro-second
    accum = ((((accum * 24 + hr) * 60ULL
                    + min) * 60ULL
                    + sec) * 1000ULL
                    + msec) * 10000ULL;
    return accum;
}

//------------------------------------------------------------------------------

void GPSDevice::hexPrint(const std::string& printThis) const
{
    #if GPS_DEBUG
    const std::string fnName("GPSDevice::hexPrint");
    AUTO_TRACE(fnName.c_str());

    std::ostringstream msg;

    for(std::string::const_iterator iter(printThis.begin());
        iter != printThis.end(); ++iter)
    {
        msg << std::hex
            << "0x"
            << static_cast< int >(*iter)
            << std::dec
            << " ";
    }

    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    #endif
}
