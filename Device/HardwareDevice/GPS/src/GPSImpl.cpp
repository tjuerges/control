// $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//
//        Internet email: alma-sw-admin@nrao.edu
//


#include <GPSImpl.h>

#include <GPSDevice.h>
// ACS headers
#include <acserr.h>
// For AUTO_TRACE.
#include <loggingMACROS.h>
// For audience logs.
#include <LogToAudience.h>
#include <acstimeEpochHelper.h>
#include <maciContainerServices.h>


GPSImpl::GPSImpl(const ACE_CString& name, maci::ContainerServices* cs):
    Control::HardwareDeviceImpl(name, cs),
    containerServices_m(cs),
    gpsTime_sp(this),
    gpsTimeDevio_p(0),
    antennaState_sp(this),
    antennaStateDevio_p(0),
    pllLocked_sp(this),
    pllLockedDevio_p(0),
    gpsLocked_sp(this),
    gpsLockedDevio_p(0),
    rs232Ok_sp(this),
    rs232OkDevio_p(0),
    timeError_sp(this),
    timeErrorDevio_p(0),
    fullName_m(std::string("alma/") + std::string(name.c_str())),
    myName_m(name.c_str() + std::string("/"))
{
    const std::string fnName("GPSImpl::GPSImpl");
    AUTO_TRACE(fnName);
}

GPSImpl::~GPSImpl()
{
    const std::string fnName("GPSImpl::~GPSImpl");
    AUTO_TRACE(fnName);

    GPSDevice::deleteInstance();
}

/* ------------------------[ Lifecycle Methods ] -------------------------*/

void GPSImpl::hwStartAction()
{
    const std::string fnName("GPSImpl::hwStartAction");
    AUTO_TRACE(fnName.c_str());

    try
    {
        gpsTimeDevio_p = new GPSTimeIO;
        gpsTime_sp = new baci::ROuLongLong(
            std::string(myName_m + std::string("GPS_Time")).c_str(),
            getComponent(), gpsTimeDevio_p);

        antennaStateDevio_p = new GPSAntennaStateIO;
        antennaState_sp = new ROEnumImpl< ACS_ENUM_T(
            Control::GPSAntennaState), POA_Control::ROGPSAntennaState >(
            std::string(myName_m + std::string("Antenna_State")).c_str(),
            getComponent(), antennaStateDevio_p);

        pllLockedDevio_p = new PLL_LockedIO;
        pllLocked_sp = new ROEnumImpl< ACS_ENUM_T(ACS::Bool),
            POA_ACS::ROBool >(std::string(
            myName_m + std::string("PLL_Locked")).c_str(), getComponent(),
            pllLockedDevio_p);

        gpsLockedDevio_p = new GPS_LockedIO;
        gpsLocked_sp = new ROEnumImpl< ACS_ENUM_T(ACS::Bool),
            POA_ACS::ROBool >(std::string(
            myName_m + std::string("GPS_Locked")).c_str(), getComponent(),
            gpsLockedDevio_p);

        rs232OkDevio_p = new RS232_OKIO;
        rs232Ok_sp = new ROEnumImpl< ACS_ENUM_T(ACS::Bool),
            POA_ACS::ROBool >(std::string(
            myName_m + std::string("RS232_OK")).c_str(), getComponent(),
            rs232OkDevio_p);

        timeErrorDevio_p = new GPSTimeErrorIO;
        timeError_sp = new baci::ROdouble(std::string(
            myName_m + std::string("Time_Error")).c_str(), getComponent(),
            timeErrorDevio_p);
    }
    catch(...)
    {
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__, __LINE__,
            fnName.c_str());
        ex.addData("Detail", "Failed to create the device properties.");
        ex.log();
        throw ex;
    }
}

void GPSImpl::hwConfigureAction()
{
    const std::string fnName("GPSImpl::hwConfigureAction");
    AUTO_TRACE(fnName);

    // Get serial port and simulation setting from the CDB.
    simGPS_m = true;
    serialPort_m.clear();

    try
    {
        std::string simulationString("true");
        loadSettingsFromCdb(serialPort_m, simulationString);
        if(simulationString == "false")
        {
            simGPS_m = false;
        }
    }
    catch(const cdbErrType::CDBRecordDoesNotExistExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex, __FILE__,__LINE__,
            fnName.c_str());
        nex.addData("Detail", "Failed to read the GPS settings from the CDB.");
        throw nex.getHwLifecycleEx();
    }
}

void GPSImpl::hwInitializeAction()
{
    const std::string fnName("GPSImpl::hwInitializeAction");
    AUTO_TRACE(fnName);

    // Create GPS device
    try
    {
        gps_p = GPSDevice::instance(simGPS_m, serialPort_m);
    }
    catch(const ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,__FILE__,__LINE__,
            fnName.c_str());
        nex.addData("Detail", "Failed to create GPS device");
        throw nex.getHwLifecycleEx();
    }
    catch(...)
    {
        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__,__LINE__,
            fnName.c_str());
        ex.addData("Detail", "Failed to create GPS device, uncaught exception");
        ex.log();
        throw ex.getHwLifecycleEx();
    }
}

void GPSImpl::hwOperationalAction()
{
    const std::string fnName("GPSImpl::hwOperationalAction");
    AUTO_TRACE(fnName);
}

void GPSImpl::hwStopAction()
{
    const std::string fnName("GPSImpl::hwStopAction");
    AUTO_TRACE(fnName);

    gpsTime_sp = 0;
    delete gpsTimeDevio_p;
    gpsTimeDevio_p = 0;

    antennaState_sp = 0;
    delete antennaStateDevio_p;
    antennaStateDevio_p = 0;

    pllLocked_sp = 0;
    delete pllLockedDevio_p;
    pllLockedDevio_p = 0;

    gpsLocked_sp = 0;
    delete gpsLockedDevio_p;
    gpsLockedDevio_p = 0;

    rs232Ok_sp = 0;
    delete rs232OkDevio_p;
    rs232OkDevio_p = 0;

    timeError_sp = 0;
    delete timeErrorDevio_p;
    timeErrorDevio_p = 0;

    // Delete GPS device
    GPSDevice::deleteInstance();
}

//------------------------------------------------------------------------------
void GPSImpl::loadSettingsFromCdb(std::string& device,
    std::string& simulation) const
{
    const std::string fnName("GPSImpl::loadGpsDeviceFromCdb");
    AUTO_TRACE(fnName);

    /**
     * Get the current tty port. It is an attribute of the GPS component. The
     * entry is in the GPS.xsd schema.
     */
    try
    {
        CDB::DAL_var dalRef(containerServices_m->getCDB());
        if(CORBA::is_nil(dalRef.in()) == true)
        {
            acsErrTypeContainerServices::CanNotGetCDBExImpl ex(__FILE__,
                __LINE__, fnName.c_str());
            ex.addData("Detail", "Failed to get a CDB reference.");
            ex.log();
            throw ex;
        }

        const std::string cdbRecord(fullName_m);
        CDB::DAO_var daoRef(dalRef->get_DAO_Servant(cdbRecord.c_str()));
        if(CORBA::is_nil(daoRef.in()) == true)
        {
            cdbErrType::CDBRecordDoesNotExistExImpl ex(__FILE__,
                __LINE__, fnName.c_str());
            ex.addData("Detail", "Failed to get the CDB reference for the "
                "GPS component.");
            ex.log();
            throw ex.getCDBRecordDoesNotExistEx();
        }

        device = daoRef->get_string("serialPort");
        simulation = daoRef->get_string("simulateGPS");
    }
    catch(const acsErrTypeContainerServices::CanNotGetCDBExImpl& ex)
    {
        cdbErrType::CDBRecordDoesNotExistExImpl nex(ex, __FILE__,__LINE__,
            fnName.c_str());
        nex.addData("Detail", "Cannot access the CDB. Reading the GPS "
            "information failed.");
        throw nex;
    }
    catch(const cdbErrType::WrongCDBDataTypeEx& ex)
    {
        cdbErrType::CDBRecordDoesNotExistExImpl nex(ex, __FILE__,__LINE__,
            fnName.c_str());
        nex.addData("Detail", "One of the CDB entries for the GPS is not of "
            "the expected type.");
        throw nex;
    }
    catch(const cdbErrType::CDBRecordDoesNotExistEx& ex)
    {
        cdbErrType::CDBRecordDoesNotExistExImpl nex(ex, __FILE__,__LINE__,
            fnName.c_str());
        nex.addData("Detail", "Cannot access the CDB entries for this "
            "component. Reading the GPS information failed.");
        throw nex;
    }
    catch(const cdbErrType::CDBXMLErrorEx& ex)
    {
        cdbErrType::CDBRecordDoesNotExistExImpl nex(ex, __FILE__,__LINE__,
            fnName.c_str());
        nex.addData("Detail", "Cannot read the XML entry in the CDB for the "
            "GPS information.");
        throw nex;
    }
    catch(const cdbErrType::CDBFieldDoesNotExistEx& ex)
    {
        cdbErrType::CDBRecordDoesNotExistExImpl nex(ex, __FILE__,__LINE__,
            fnName.c_str());
        nex.addData("Detail", "Failed to read the GPS settings, a field does "
            "not exist.");
        throw nex;
    }
    catch(...)
    {
        cdbErrType::CDBRecordDoesNotExistExImpl ex(__FILE__,__LINE__,
            fnName.c_str());
        ex.addData("Detail", "Failed to read the GPS settings, uncaught "
            "exception.");
        ex.log();
        throw ex;
    }

    std::ostringstream msg;
    msg << "The GPS is connected to the "
        << device
        << " serial port, simulation = "
        << simulation
        << ".";
    LOG_TO_OPERATOR(LM_INFO, msg.str());
}

void GPSImpl::GPS_init()
{
    const std::string fnName("GPSImpl::GPS_init");
    AUTO_TRACE(fnName);

    try
    {
        GPSDevice::instance(simGPS_m)->initGPS();
    }
    catch(ControlExceptions::GPSTimeErrorExImpl& ex)
    {
        throw ex.getGPSTimeErrorEx();
    }
}

//-----------------------------------------------------------------------------
// Property interface
//-----------------------------------------------------------------------------
#define PROP(propName, propType, propertyMem) \
propType##_ptr GPSImpl::propName() \
{ \
    if(propertyMem##_sp == 0) \
    { \
        return propType::_nil(); \
    } \
\
    propType##_var prop(propType::_narrow( \
        propertyMem##_sp->getCORBAReference())); \
    return prop._retn(); \
}

PROP(GPS_Time, ACS::ROuLongLong, gpsTime)
PROP(Antenna_State, Control::ROGPSAntennaState, antennaState)
PROP(PLL_Locked, ACS::ROBool, pllLocked)
PROP(GPS_Locked, ACS::ROBool, gpsLocked)
PROP(RS232_OK, ACS::ROBool, rs232Ok)
PROP(Time_Error, ACS::ROdouble, timeError)

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(GPSImpl)
