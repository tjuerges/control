/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSD;

/**
 * @author srankin@nrao.edu
 * @author sharring@nrao.edu
 * @version $Id$
 * @since
 */

import alma.Control.gui.hardwaredevice.common.HardwareDeviceAmbsiPanel;
import alma.Control.gui.hardwaredevice.common.HardwareDevicePresentationModelEvent;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class representing the details for a PSD.
 * 
 * @author srankin
 * 
 */
public class PSDGlobalPanel extends HardwareDeviceAmbsiPanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    private BooleanMonitorPointWidget acOkWidget;
    private BooleanMonitorPointWidget currentLimitOkWidget;
    private BooleanMonitorPointWidget dcOkWidget;
    private BooleanMonitorPointWidget fanOkWidget;
    private BooleanMonitorPointWidget fanSpeedOkWidget;
    private Logger logger = null;
    private JButton outputShutdownButton;
    private BooleanMonitorPointWidget outputShutdownWidget;
    private BooleanMonitorPointWidget overTempOkWidget;
    private BooleanMonitorPointWidget overVoltageOkWidget;
    private DevicePresentationModel presentationModel;
    private JButton restartAllRanges;
    private BooleanMonitorPointWidget supplyErrorWidget;
    private JButton toggleGuiControlButton;

    /**
     * Constructor.
     * 
     */
    public PSDGlobalPanel(Logger logger, DevicePresentationModel presentationModel) {
        super(presentationModel);
        this.logger = logger;
        logger.entering("PSDGlobalPanel", "Constructor");

        this.presentationModel = presentationModel;
        buildPanel();
        
        logger.exiting("PSDGlobalPanel", "Constructor");
    } // method
    
    private JPanel buildControlPanel() {
        
        JPanel panel = new JPanel();
        
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;

        c.gridy = 0;
        c.gridx = 0;
        toggleGuiControlButton = new JButton("Disable Controls");
        toggleGuiControlButton.addActionListener(new ActionListener() {

            private boolean guiControlState = true;

            public void actionPerformed(ActionEvent e) {
                if (false == guiControlState) {
                    guiControlState = true;
                    enableButtons();
                    toggleGuiControlButton.setText("Disable Controls");
                } else {
                    guiControlState = false;
                    disableButtons();
                    toggleGuiControlButton.setText("Enable Controls");
                }
            }
        });
        panel.add(toggleGuiControlButton, c);

        c.gridx = 1;
        restartAllRanges = new JButton("Reset Min/Max Values");
        restartAllRanges.addActionListener(presentationModel.getRestartAllRangesControlPoint());
        panel.add(restartAllRanges, c);

        c.gridx = 2;
        outputShutdownButton = new JButton("Toggle Outputs");
        outputShutdownButton.addActionListener(presentationModel.getOutputShutdownMonitorControlPoint());
        panel.add(outputShutdownButton, c);
        
        return panel;
    } // method


    private JPanel buildMonitorPanel() {
        JPanel panel = new JPanel();
        
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        
        c.gridy = 0;
        c.gridx = 0; 
        panel.add(new JLabel("Supply", JLabel.RIGHT), c);
        c.gridx = 1;
        supplyErrorWidget = new BooleanMonitorPointWidget(logger, "Error", "OK");
        panel.add(supplyErrorWidget, c);
        c.gridx = 2;
        panel.add(new JLabel("Current Limit", JLabel.RIGHT), c);
        c.gridx = 3;
        currentLimitOkWidget = new BooleanMonitorPointWidget(logger, "OK", "Error");
        panel.add(currentLimitOkWidget, c);
        c.gridx = 4;
        panel.add(new JLabel("Over Voltage", JLabel.RIGHT), c);
        c.gridx = 5;
        overVoltageOkWidget = new BooleanMonitorPointWidget(logger, "OK", "Error");
        panel.add(overVoltageOkWidget, c);
        
        c.gridy = 1;
        c.gridx = 0;
        panel.add(new JLabel("Temperature", JLabel.RIGHT), c);
        c.gridx = 1;
        overTempOkWidget = new BooleanMonitorPointWidget(logger, "OK", "Over");
        panel.add(overTempOkWidget, c);
        c.gridx = 2;
        panel.add(new JLabel("AC", JLabel.RIGHT), c);
        c.gridx = 3;
        acOkWidget = new BooleanMonitorPointWidget(logger, "OK", "Error");
        panel.add(acOkWidget, c);
        c.gridx = 4;
        panel.add(new JLabel("DC", JLabel.RIGHT), c);
        c.gridx = 5;
        dcOkWidget = new BooleanMonitorPointWidget(logger, "OK", "Error");
        panel.add(dcOkWidget, c);
        
        c.gridy = 2;
        c.gridx = 0;
        panel.add(new JLabel("Fan", JLabel.RIGHT), c);
        c.gridx = 1;
        fanOkWidget = new BooleanMonitorPointWidget(logger, "OK", "Error");
        panel.add(fanOkWidget, c);
        c.gridx = 2;
        panel.add(new JLabel("Fan Speed", JLabel.RIGHT), c);
        c.gridx = 3;
        fanSpeedOkWidget = new BooleanMonitorPointWidget(logger, "OK", "Error");
        panel.add(fanSpeedOkWidget, c);
        c.gridx = 4;
        panel.add(new JLabel("Output", JLabel.RIGHT), c);
        c.gridx = 5;
        outputShutdownWidget = new BooleanMonitorPointWidget(logger, "Off", "On");
        panel.add(outputShutdownWidget, c);

        return panel;
    } // method
    
    private void buildPanel() {
        logger.entering("PSDGlobalPanel", "buildPanel");

        this.setBorder(
                BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Global Monitor and Control"),
                    BorderFactory.createEmptyBorder(1,1,1,1)
                )
        );
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        this.add(buildMonitorPanel());
        this.add(buildControlPanel());

        logger.exiting("PSDGlobalPanel", "buildPanel");
    } // method
    
    public void disableButtons() {
        logger.entering("PSDGlobalPanel", "disableButtons");

        outputShutdownButton.setEnabled(false);
        restartAllRanges.setEnabled(false);
        
        logger.exiting("PSDGlobalPanel", "disableButtons");
    } // method

    public void enableButtons() {
        logger.entering("PSDGlobalPanel", "enableButtons");
        
        outputShutdownButton.setEnabled(true);
        restartAllRanges.setEnabled(true);

        logger.exiting("PSDGlobalPanel", "enableButtons");
    } // method

    public void specializedCommunicationEstablished() {
        logger.entering("PSDGlobalPanel", "specializedCommunicationEstablished");
        
        acOkWidget.setQuestionable(false);
        currentLimitOkWidget.setQuestionable(false);
        dcOkWidget.setQuestionable(false);
        fanOkWidget.setQuestionable(false);
        fanSpeedOkWidget.setQuestionable(false);
        outputShutdownWidget.setQuestionable(false);
        overTempOkWidget.setQuestionable(false);
        overVoltageOkWidget.setQuestionable(false);
        supplyErrorWidget.setQuestionable(false);
        
        logger.exiting("PSDGlobalPanel", "specializedCommunicationEstablished");
    } // method

    public void specializedCommunicationLost() {
        logger.entering("PSDGlobalPanel", "specializedCommunicationLost");
        
        acOkWidget.setQuestionable(true);
        currentLimitOkWidget.setQuestionable(true);
        dcOkWidget.setQuestionable(true);
        fanOkWidget.setQuestionable(true);
        fanSpeedOkWidget.setQuestionable(true);
        outputShutdownWidget.setQuestionable(true);
        overTempOkWidget.setQuestionable(true);
        overVoltageOkWidget.setQuestionable(true);
        supplyErrorWidget.setQuestionable(true);
        
        logger.exiting("PSDGlobalPanel", "specializedCommunicationLost");

    } // method

    protected void specializedProcessChange(HardwareDevicePresentationModelEvent evt) {
        logger.entering("PSDGlobalPanel", "specializedProcessChange");
        
        if (evt.getEnumeration().equals(MonitorPoints.AC_OK)) {
            acOkWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.CURRENT_LIMIT_OK)) {
            currentLimitOkWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.DC_OK)) {
            dcOkWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.FAN_OK)) {
            fanOkWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.FAN_SPEED_OK)) {
            fanSpeedOkWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.OUTPUT_SHUTDOWN)) {
            outputShutdownWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.OVER_TEMP_OK)) {
            overTempOkWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.OVER_VOLTAGE_OK)) {
            overVoltageOkWidget.setValue(evt.getValue().toString());
        } 
        if (evt.getEnumeration().equals(MonitorPoints.SUPPLY_ERROR)) {
            supplyErrorWidget.setValue(evt.getValue().toString());
        } 
        
        logger.exiting("PSDGlobalPanel", "specializedProcessChange");
    } // method

} // class

//
// O_o
