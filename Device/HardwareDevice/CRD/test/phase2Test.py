#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#
# Phase 2 test of Central Reference Distributor


import sys
import unittest
import struct
import ControlExceptions
import CCL.AmbManager
import CCL.AOSTiming
from time import sleep

def testProperty(compRef, rca, testNo, propName, propLen, format, maxVal, minVal):
    '''
    Tests a single BACI property
    '''
    compRef.startTestLog("Test " + str(testNo) + ": " + str(propName) + " Test")

    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, rca)
    errStr = str(propName) + " returned incorrect length value, actual = "
    errStr += str(len(r)) + " bytes, expected " + str(propLen) + " bytes"
    compRef.failUnless(len(r) == propLen, errStr)
    (resp,)= struct.unpack(format,r)
    compRef.log(str(propName) + " returns value " + str(resp))
    errStr = str(propName) + " returned value out of bounds, value = " + str(resp)
    compRef.failUnless((resp <= maxVal) or (resp >= minVal), errStr)
    compRef.endTestLog("Test " + str(testNo) + ": " + str(propName) + " Test")
    return

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        self.aos = CCL.AOSTiming.AOSTiming(stickyFlag = True)
        self.aos.controllerOperational()
        if self.aos.isAmbManagerRunning() == False:
            self.aos.startAmbManager()
        self.manager = CCL.AmbManager.AmbManager(antenna)

        self.node    = 0x20
        self.channel = 0
        self.monitor = {0x1:8, 0x4:8, 0x5:8, 0x6:2, 0x7:2, 0x8:2,
                        0x9:2, 0xa:2, 0xc:2, 0xd:2, 0xe:2, 0xf:1,
                        0x30000:3,0x30001:4,0x30002:4,0x30003:4}
        self.command = {0x81:(0x1,8,"Set Reset Time"),
                        0x83:(0x5,1,"Maser Counter Reset")}
        self.out = ""

    def __del__(self):
        del self.manager
        self.aos.controllerShutdown()
        del self.aos
        if self.out != None:
            print self.out
        del(self.manager)


    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin " + testName)

    def endTestLog(self, testname):
        self.log("End " + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"
        
    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test one verifies that the device correctly responds to
        broadcast messages and returns the serial number when requested
        by RCA 0x0.

        For recordkeeping the serial number is returned.
        '''
        self.startTestLog("Test 1: Broadcast Response Test")
        sleep(2)
        try:
            (r,t) = self.manager.getNodes(self.channel)
        except:
            self.fail("Unable to get nodes on bus")

        foundNode = False
        for node in r:
            if node.node == self.node:
                if not foundNode:
                    foundNode = True
                    self.log("Device Serial Number: 0x%x" % node.sn)
                else:
                    self.fail("Node 0x%x responded at least twice" % self.node)

        self.failUnless(foundNode, "Node %d did not respond" % self.node)

        try:
            (sn, time) = self.manager.findSN(self.channel,self.node)
        except:
            self.fail("Exception retrieving serial number from node")

        self.endTestLog("Test 1: Broadcast Response Test")

    def test02(self):
        self.log("\nTest 2 not executed (Interactive Test)")
        
    def test03(self):
        self.log("\nTest 3 does not require software")
        
    def test04(self):
        self.log("\nTest 4 not executed (Interactive Test)")

    def test05(self):
        '''
        Test 5 Verifies an appropiate response from the maserVsGPSCounter and
        maserCounter monitor point.  It also ensures that the two monitor
        points are correctly recieved and that their responses are consistent.
        '''
        self.startTestLog("Test 5: Maser Counter Tests")
        (r,t) = self.manager.monitor(self.channel,self.node,0x4)
        errStr = "maserVsGPSCounter returned incorrect length value, actual = "
        errStr += str(len(r)) + " bytes, expected 8 bytes"
        self.failUnless(len(r) == 8, errStr)
        (resp1,) = struct.unpack("!Q",r)
        
        (r,t) = self.manager.monitor(self.channel,self.node,0x5)
        errStr = "maserCounter returned incorrect length value, actual = "
        errStr += str(len(r)) + " bytes, expected 8 bytes"
        self.failUnless(len(r) == 8, errStr)
        (resp2,) = struct.unpack("!Q",r)

        errStr = "ERROR: maserCounter and maserVsGPSCounter are not consistent.\n "
        errStr += "The counters differ more than 2 seconds.\n "
        errStr += "maserCounter = " + str(resp2) + " and maserVsGPSCounter = "
        errStr += str(resp1) + " difference = " + str(resp2 - resp1)
        self.failIf((resp2 - resp1) > 20000000, errStr);
        
        self.endTestLog("Test 5: Maser Counter Tests")

    def test06(self):
        '''
        This test verifies the getResetTime monitor point and the
        setResetTime control point.
        '''
        self.startTestLog("Test 6: getResetTime/setResetTime Tests")
        data = 0x05044210CC441540
        t = self.manager.command(self.channel, self.node, 0x81,
                                 struct.pack("!Q", data))
        (r,t) = self.manager.monitor(self.channel, self.node, 0x1)
        errStr = "getResetTime returned incorrect length value, actual = "
        errStr += str(len(r)) + " bytes, expected 8 bytes"
        self.failUnless(len(r) == 8, errStr)
        (resp,)= struct.unpack("!Q",r)
        self.failUnless(resp == data, "getResetTime returns the incorrect value");

        self.endTestLog("Test 6:  getResetTime/setResetTime Tests")


    def test0x(self):
        '''
        This test verifies all the other monitor points
        '''
        testProperty(self, 0x6, 7, 'efc5MHz', 2, '!H', 260, 260)
        testProperty(self, 0x7, 8, 'pwr5MHz', 2, '!H', 139, 139)
        testProperty(self, 0x8, 9, 'pwr25MHz', 2, '!H', 278, 278)
        testProperty(self, 0x9, 10, 'pwr125MHz', 2, '!H', 71, 71)
        testProperty(self, 0xa, 11, 'pwr2GHz', 2, '!H', 374, 374)
        testProperty(self, 0xc, 12, 'laserCurrent', 2, '!H', 116, 116)
        testProperty(self, 0xd, 13, 'vdcMinus5', 2, '!H', 64994, 64994)
        testProperty(self, 0xe, 14, 'vdc15', 2, '!H', 1556, 1556)
        testProperty(self, 0xf, 15, 'status', 1, '!B', 1, 1)

    def test16(self):
        '''
        This test verifies the xilinxPromReset control point
        '''
        self.startTestLog("Test 16: xilinxPromReset Test")
        t = self.manager.command(self.channel, self.node, 0x31001,
                                 struct.pack("!B", 1))
        self.endTestLog("Test 16: xilinxPromReset Test")

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg


    # --------------------------------------------------
    # Test Definitions
    def test02(self):
        '''
        This test does a CAN monitor at each of the monitor points in
        the ICD.  The timing from the end of the request to the beginning of
        the response should be measured using an oscilloscope to verify that
        the delay is less than 150 us.
        '''
        self.startTestLog("Test 2: Monitor Timing Test")
        self.log('''Connect CAN wires (AMB pin 2 and 7) to oscilliscope and
                    set for single trigger.  Time base should be approximatly
                    200 us.
                    Press <enter> to generate monitor request,
                    n[ext] to process next monitor point,
                    e[xit] to exit.
                 ''')

        rv = raw_input("press <enter> to begin:")
        for rca in self.monitor.keys():
            rv = ''
            while len(rv) == 0 or (rv != "next"[0:len(rv)] and rv != "exit"[0:len(rv)]):
                try:
                    (r,t) = self.manager.monitor(self.channel,self.node,rca)
                    self.failUnless(len(r)==self.monitor[rca],
                                    "Length of response incorrect for RCA:0x%x"
                                    % rca)
                    rv=raw_input("CAN request sent to RCA: 0x%x" % rca)
                except ControlExceptions.CAMBErrorEx:
                    self.fail("Exception generated monitoring point 0x%x"%rca)
                    
            if rv == "exit"[0:len(rv)]:
                break
        self.endTestLog("Test 2: Monitor Timing Test")

    def test04(self):
        '''
        This test toggles the reset wires.  It should be verified that the
        reset pulse is detected and the AMBSI resets.  In addition
        the levels on these lines should be checked using an oscilliscope
        '''
        self.startTestLog("Test 4: AMB Reset Test")
        self.log('''Connect Reset wires (AMB pin 1 and 6) to oscilliscope
                    and set for single trigger.  Time base should be
                    approximatly 200 us

                 ''')

        rv = raw_input("press <enter> to generate reset request, type e[xit] to exit ")
        while  len(rv) == 0 or rv != "exit"[0:len(rv)]:
            t = self.manager.reset(self.channel)
            rv=raw_input("Reset Generated ")

        self.endTestLog("Test 4: AMB Reset Test")

    def test10(self):
        '''
        Test 10 verifies that the device only responds to commands of the
        correct length
        '''
        self.startTestLog("Test 10: Ill formed command test")
        i = 0
        for key in self.command.keys():
            i += i
            (r,t) = self.manager.monitor(self.channel, self.node,
                                         self.command[key][0])
            (resp,) = struct.unpack("!Q",r)
            
            cmd = [0x01]
            while len(cmd) < 8:
                if len(cmd) != self.command[key][1]:
                    try:
                        t = self.manager.command(self.channel, self.node, key,
                                                 struct.pack("!%dB"%len(cmd),*cmd))
                        
                        (r,t) = self.manager.monitor(self.channel, self.node,
                                                     self.command[key][0])
                        (resp1,) = struct.unpack("!Q",r)
                        if i == 1:
                            self.failUnless(resp1 == resp,
                                            "%s set with length %d" %
                                            (self.command[key][2], len(cmd)))
                        elif i == 2:
                            self.failUnless(resp1 > resp,
                                            "%s reset with length %d" %
                                            (self.command[key][2], len(cmd)))
                    except:
                        pass
                cmd.append(0x01)

        # Test to be verified. See DigiClock/test/phas2Test


            
        self.endTestLog("Test 10: Ill formed command test")

    def test11(self):
        '''
        This test monitors all RCAs between 1 and 0x3FFFF (the full
        range allocated to each device).  It ensures that only
        monitors documented in the ICD respond, and that the response
        is valid (correct number of bytes).
        '''
        self.startTestLog("Test 11: Monitor Survey test")
        self.log('''The test can be interrupted by typing ^C''')
        errorList = {}
        for RCA in range (0x1,0x3FFFF):
            try:
                if (RCA % 0x1000) == 0:
                    self.log("Sending monitor request 0x%x" % RCA)
                (r,t) = self.manager.monitor(self.channel, self.node, RCA)
                if self.monitor.has_key(RCA):
                    if len(r) != self.monitor[RCA][2]:
                        errorList[RCA]=len(r)
                else:
                    errorList[RCA]=len(r)
            except KeyboardInterrupt, e:
                break
            except:
                pass

        if len(errorList) != 0:
            self.log("%d offending responses" % len(errorList))
            if len(errorList) < 10:
                for idx in errorList.keys():
                    if self.monitor.has_key(RCA):
                        self.log("RCA 0x%x returned %d bytes (%d expected)" % \
                              (idx, errorList[idx], self.monitor[RCA][2]))
                    else:
                        self.log("RCA 0x%x returned %d bytes" % \
                              (idx, errorList[idx]))
            self.fail("%d Monitor points not in ICD Responded" %
                      len(errorList))

        self.endTestLog("Test 11: Monitor Survey test")

# **************************************************
# MAIN Program
if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("AOSTiming")

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()
