#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#
# Phase 4 test of Central Reference Distributor


import sys
import unittest
import Control
import ControlExceptions
import time


from ACS import acsFALSE
from ACS import acsTRUE
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.TimeHelper import getTimeStamp


'''
Define a test class for the central reference distributor
'''
def testProperty(compRef, propRef, nameStr, testNo, propName):
    '''
    Tests a single BACI property
    '''
    compRef.startTestLog("Test " + str(testNo) + ": " + str(propName) +
        " Property Test")
    errStr = nameStr + "Name error"
    compRef.failUnlessEqual(propRef._get_name(), nameStr, errStr);
    val = propRef.get_sync()
    errStr = "Error reading property " + propRef._get_name()
    compRef.failIf(val[1].code != 0, errStr);
    compRef.log(" The " + propRef._get_name() +
        " BACI property has a value of: " + str(val[0]))
    compRef.endTestLog("Test " + str(testNo) + ": " + str(propName) +
        " Property Test")
    return   



class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self._client = PySimpleClient.getInstance()
        self.out = ""
        self._phaseCommands = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)]
        unittest.TestCase.__init__(self, methodName)

    def __del__(self):
        if self.out != None:
            print self.out
        self._client.disconnect()


    def setUp(self):
        self._ref = self._client.getComponent("CONTROL/" + antenna + "/CRD")
        self._ref.hwStop()
        self._ref.hwStart()
        self._ref.hwConfigure()
        self._ref.hwInitialize()
        self._ref.hwOperational()
        time.sleep(3.0)
        
    def tearDown(self):
        self._ref.hwStop();
        self._client.releaseComponent("CONTROL/" + antenna + "/CRD")
        del self._ref

    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin " + testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self, msg):
        self.out += msg + "\n"


    # --------------------------------------------------
    # Test Definitions
    def test02(self):
        '''
        Test the MASER_COUNTER and MASER_VS_GPS_COUNTER values
        '''
        maserCounter = self._ref.GET_MASER_COUNTER()
        maserVsGPSCounter = self._ref.GET_MASER_COUNTER()
        self.log("MASER_COUNTER reports value " +
            str(maserCounter[0]))
        self.log("MASER_VS_GPS_COUNTER reports value " +
            str(maserVsGPSCounter[0]))
        errStr = "ERROR: maserCounter and maserVsGPSCounter are not consistent.\n "
        errStr += "The counters differ more than 2 seconds.\n "
        errStr += ("maserCounter = " + str(maserCounter[0]) +
            " and maserVsGPSCounter = ")
        errStr += str(maserVsGPSCounter[0]) + " difference = "
        errStr += str(maserCounter[0] - maserVsGPSCounter[0])
        self.failIf((maserCounter[0] - maserVsGPSCounter[0]) > 20000000, errStr);
        self.endTestLog("Test 2: MASER_COUNTER and MASER_VS_GPS_COUNTER test")
        
    def test03(self):
        '''
        Test the getResetTime Read Only Property and setResetTime Read Write
        Property
        '''
        self.startTestLog("Test 3: SET_RESET_TIME/GET_RESET_TIME Test")
        # First set the reset time
        self._ref.SET_RESET_TIME(0x1234567)

        # Now read the reset time
        returnVal = self._ref.GET_RESET_TIME()
        self.log("GET_RESET_TIME reports value " +
            str(returnVal[0]))
        refVal = 0x1234567
        errStr = "Actual value = " + str(returnVal[0]) + " expected "
        errStr += str(refVal) + " = 0x1234567"
        self.failIf(returnVal[0] > 0x1234568, errStr)
        self.failIf(returnVal[0] < 0x1234566, errStr)
        self.endTestLog("Test 3: SET_RESET_TIME/GET_RESET_TIME Test")

    def test0x(self):
        '''
        Test all the other Read Only Properties
        '''
        testProperty(self, self._ref._get_EFC_5_MHZ(),
            "CONTROL/AOSTiming/CRD:EFC_5_MHZ", 4, "EFC_5_MHZ")
        testProperty(self, self._ref._get_PWR_5_MHZ(),
            "CONTROL/AOSTiming/CRD:PWR_5_MHZ", 5, "PWR_5_MHZ")
        testProperty(self, self._ref._get_PWR_25_MHZ(),
            "CONTROL/AOSTiming/CRD:PWR_25_MHZ", 6, "PWR_25_MHZ")
        testProperty(self, self._ref._get_PWR_125_MHZ(),
            "CONTROL/AOSTiming/CRD:PWR_125_MHZ", 7, "PWR_125_MHZ")
        testProperty(self, self._ref._get_PWR_2_GHZ(),
            "CONTROL/AOSTiming/CRD:PWR_2_GHZ", 8, "PWR_2_GHZ")
        testProperty(self, self._ref._get_LASER_CURRENT(),
            "CONTROL/AOSTiming/CRD:LASER_CURRENT", 9, "LASER_CURRENT")
        testProperty(self, self._ref._get_VDC_MINUS_5(),
            "CONTROL/AOSTiming/CRD:VDC_MINUS_5", 10, "VDC_MINUS_5")
        testProperty(self, self._ref._get_VDC_15(),
            "CONTROL/AOSTiming/CRD:VDC_15", 11, "VDC_15")
        testProperty(self, self._ref._get_STATUS(),
            "CONTROL/AOSTiming/CRD:STATUS", 12, "STATUS")

    def test14(self):
        '''
        Test the correct functioning of the master counter methods
        '''
        self.startTestLog("Test 14: maser counter methods Test")

        # Get the reset time
        resetTime = self._ref.GET_RESET_TIME()
        # Add one second to the reset time.
        resetTime = long(resetTime[0] + 1.0e7)
        self.log("Reset time = " + str(resetTime))

        # Set the reset time.
        self._ref.SET_RESET_TIME(resetTime)
        (resetTime1, t) = self._ref.GET_RESET_TIME()
        errStr = "masterCounter: ERROR: reset time not set correctly."
        self.failIf(resetTime1 != resetTime, errStr)


        # Get the maser counter value
        (maserCount, t) = self._ref.GET_MASER_COUNTER()
        self.log(" Maser counter = " + str(maserCount))

        # wait to be sure the reset has taken place
        sleepTime = 5.0
        time.sleep(sleepTime)
        
        # Get the maser counter value again
        (maserCount1, t) = self._ref.GET_MASER_COUNTER()
        self.log(" Maser counter after 5s sleep = " + str(maserCount1))

        difference = abs(sleepTime - ((maserCount1 - maserCount) / 125e6))
        errStr = "masterCounterReset: ERROR: maser counter not counting correctly."
        errStr += " Counter before 5s sleep = " + str(maserCount)
        errStr += " and after = " + str(maserCount1)
        self.failIf(difference > 0.1, errStr)

        self.endTestLog("Test 14: master counter methods Test")

class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self, methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg


    # --------------------------------------------------
    # Test Definitions
    def test06(self):
        '''
        For the time no interactive tests defined
        '''
        self.startTestLog("Test 6: no test defined")

        pass


# **************************************************
# MAIN Program
if __name__ == "__main__":
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("AOSTiming")

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()
