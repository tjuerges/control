#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Jun 2, 2008  created
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for the Central Reference Distributor
device.
"""

import CCL.CRDBase

class CRD(CCL.CRDBase.CRDBase):
    def __init__(self, stickyFlag = False):

        '''
        The constructor creates a CRD object by making use of
        the CRDBase constructor.
        '''
        componentName = "CONTROL/AOSTiming/CRD"
        CCL.CRDBase.CRDBase.__init__(self, None, componentName, stickyFlag)

    def __del__(self):
        CCL.CRDBase.CRDBase.__del__(self)

    def maserCounterReset(self, whenToExecute):
        '''
        Resets the 125 MHz counter on next GPS 1 PPS tick.
        When reset, the counter is zeroed and doesn't begin counting until
        the next GPS 1 PPS tick.  At the moment of reset the maser and GPS
        clocks are aligned to within one 125 MHz period (40 nanosec).
        The parameter whenToExecute gives the absolute time for executing
        the command.
        The command is executed in the interval 0.999 seconds to 0.001
        seconds before the first 1PPS tick after the time given.
        A time of 0 means just before the next 1PPS tick.
        If the time given is too close to the next 1PPS tick, the command
        will be executed one 1PPS tick later.
        The time of reset is returned.

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        return self._HardwareDevice__hw.maserCounterReset(whenToExecute);

    def getArrayTimeOffset(self):
        '''
        Calculate the offset in seconds between the 125MHz counter in the CRD
        and the GPS time.
        \return Time difference between GPS time and the 125MHz Maser counter.
        
        \exception ControlExceptions::CAMBErrorEx
        
        What is really done is that the latched value MASER_VS_GPS_COUNTER is
        read and the value modulo 125MHz returned.  This harbours the danger
        that it is ambiguous how many seconds we are already off.  Let's hope
        that the CRG is accurate enough.
        '''
        return self._HardwareDevice__hw.getArrayTimeOffset()
