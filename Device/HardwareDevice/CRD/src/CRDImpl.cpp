// $Id$
//
// Copyright (C) 2004
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Library General Public License
// as published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Library General Public License for more details.
//
// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
//
// Correspondence concerning ALMA should be addressed as follows:
//
//        Internet email: alma-sw-admin@nrao.edu
//


#include <CRDImpl.h>

// System headers
#include <sstream>
#include <cerrno>
#include <semaphore.h>

// For AUTO_TRACE.
#include <loggingMACROS.h>
// For audience logs.
#include <LogToAudience.h>

// CORRCommon headers
#include <TETimeUtil.h>
#include <acstimeEpochHelper.h>
#include <acstimeTimeUtil.h>

// For maci::ContainerServices
#include <acsContainerServices.h>


CRDImpl::CRDImpl(const ACE_CString& name, maci::ContainerServices* cs):
    CRDBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    sem_init(&synchLock, 0, 0);
}

CRDImpl::~CRDImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__)

    sem_destroy(&synchLock);
}

ACS::Time CRDImpl::maserCounterReset(ACS::Time execute_time)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Reset the master counter
    ACS::Time timestamp(0ULL);
    const AmbErrorCode_t er(resetMaserCounter(execute_time, &timestamp));
    if(er != AMBERR_NOERR)
    {
        std::ostringstream msg;
        msg << "Failed to reset the master counter, CAN error = "
            << er;

        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getCAMBErrorEx();
    }

    return timestamp;
}

void CRDImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    CRDBase::hwInitializeAction();

    // If the 5 MHz is out of lock, then we need to relock it.
    ACS::Time timestamp(0ULL);

    if(getPllOutOfLock5Mhz(timestamp) == true)
    {
        try
        {
            setXilinxPromReset(0x01);
        }
        catch(const ControlExceptions::INACTErrorExImpl& ex)
        {
            throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__).getINACTErrorEx();
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__).getCAMBErrorEx();
        }
    }
}

AmbErrorCode_t CRDImpl::resetMaserCounter(ACS::Time reset_time,
    ACS::Time* timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Make a copy of the reset time in case it has to be modified.
    ACS::Time resetTime(reset_time);

    std::vector< AmbDataMem_t > uch(8);
    uch[0] = 1U;
    const AmbDataLength_t len(1);
    AmbErrorCode_t ambError(AMBERR_PENDING);

    if(resetTime == 0ULL)
    {
        resetTime = TETimeUtil::nearestTE(
            TimeUtil::ace2epoch(ACE_OS::gettimeofday())).value;
        resetTime += (2 * TETimeUtil::TE_PERIOD_ACS);
    }

    // Set time out to 2s.  waitTime is an absolute time, hence add resetTime.
    ACE_Time_Value timeToWait;
    timeToWait.set(2.0);
    timeToWait += TimeUtil::epoch2ace(EpochHelper(resetTime).value());
    const timespec_t waitUntil(timeToWait);

    commandTE(resetTime, getControlRCAMaserCounterReset(), len, &(uch[0]), &synchLock,
        timestamp, &ambError);

    // Try to acquire the semaphore for two seconds. The command should
    // have succeeded by then. If the semaphore would not be timed, chances
    // are that the code will be stuck here forever until reset.
    int err(0);
    if((err = sem_timedwait(&synchLock, &waitUntil)) != 0)
    {
        std::ostringstream msg;
        msg << "Failed to get the synchLock semaphore. Reason: "
            << errno
            << " ("
            << strerror(errno)
            << "). ambError = "
            << ambError;
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }

    std::ostringstream msg;
    msg << "resetTime = "
        << resetTime
        << ", timeToWait = "
        << (static_cast< double >(timeToWait.sec()) + static_cast< double >(
            timeToWait.usec()) * 1e-6)
        << ", exec. time = "
        << *timestamp;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    return ambError;
}

CORBA::Double CRDImpl::getArrayTimeOffset()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Read the MASER_VS_GPS_COUNTER monitor point.  Use the internal method
    // for this.  It does already all the error handling.
    unsigned long long latched125MHzTicks(0ULL);

    try
    {
        ACS::Time timeStamp(0ULL);
        latched125MHzTicks = getMaserVsGpsCounter(timeStamp);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }

    // The time difference between the Maser/Array Time and GPS will be
    // the remainder of the latched number modulo 125MHZ.  Then it will be
    // converted to seconds.
    // E.g.:  latched value = value % 125000000 / 125000000.0
    return (static_cast< CORBA::Double >(latched125MHzTicks % 125000000ULL)
        / static_cast< CORBA::Double >(125000000ULL));
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CRDImpl)
