//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <CRDHWSimImpl.h>
#include <string>
#include <map>
#include <vector>
// For AUTO_TRACE.
#include <loggingMACROS.h>
// For audience logs.
#include <sstream>
#include <LogToAudience.h>

#include <OS_NS_sys_time.h>
#include <acsutilTimeStamp.h>
#include <TETimeUtil.h>


// Please use this class to implement complex functionality for the
// CRDHWSimBase helper functions. Use AMB::TypeConvertion methods
// for convenient type convertions.

AMB::CRDHWSimImpl::CRDHWSimImpl(node_t node,
    const std::vector< CAN::byte_t >& serialNumber):
    CRDHWSimBase::CRDHWSimBase(node, serialNumber),
    // Initialise the resetMaserCounterTime value to something meaningful so
    // that it does not appear we are counting 125MHz ticks since 1592.
    resetMaserCounterTime(::getTimeStamp())
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

AMB::CRDHWSimImpl::~CRDHWSimImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void AMB::CRDHWSimImpl::associate(const AMB::rca_t RCA,
    const std::vector< CAN::byte_t >& data)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::map< AMB::rca_t, std::vector< CAN::byte_t >* >::iterator
        position(AMB::CommonHWSim::state_m.find(RCA));
    if(position != AMB::CommonHWSim::state_m.end())
    {
        std::vector< CAN::byte_t >* vector((*position).second);
        vector->assign(data.begin(), data.end());
    }
    else
    {
        std::ostringstream msg;
        msg << "Could not find the data for RCA = "
            << RCA
            << " in AMB::CommonHWSim::state_m.";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    }
}

void AMB::CRDHWSimImpl::setControlSetResetTime(const std::vector< CAN::byte_t >& data)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    associate(monitorPoint_RESET_TIME, data);
    AMB::CRDHWSimBase::setControlSetResetTime(data);
}

void AMB::CRDHWSimImpl::setControlMaserCounterReset(
    const std::vector< CAN::byte_t >& data)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    resetMaserCounterTime = ::getTimeStamp();
    AMB::CRDHWSimBase::setControlMaserCounterReset(data);
}

std::vector< CAN::byte_t > AMB::CRDHWSimImpl::calcCMaserCounter(
    const bool roundDownTo1PPS) const
{
    // Calculate the 125MHz ticks after the last reset (setControlMaserCounterReset)
    // rounded down to the last 1pps.

    //Now get the current time and calculate the time since the reset
    // (in 100ns).
    ACS::Time counter(::getTimeStamp() - resetMaserCounterTime);

    // Make it 125MHz ticks.  Here is the sequence important, otherwise too
    // much accuracy will be lost.
    counter *= 125000000ULL;
    counter /= TETimeUtil::ACS_ONE_SECOND;

    // If the Maser vs. GPS shall be calculated, then it is necessary to round
    // down to 1PPS accuracy.
    if(roundDownTo1PPS == true)
    {
        counter -= (counter % 125000000ULL);
    }

    // Now convert the value of counter back to CAN data format and...
    std::vector< CAN::byte_t > data(8U);
    AMB::TypeConversion::valueToData(data, counter);

    // return it.
    return data;
}

std::vector< CAN::byte_t > AMB::CRDHWSimImpl::getMonitorMaserVsGpsCounter() const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    return calcCMaserCounter(true);
}

std::vector< CAN::byte_t > AMB::CRDHWSimImpl::getMonitorMaserCounter() const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    return calcCMaserCounter(false);
}
