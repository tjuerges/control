#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef CRDHWSimImpl_h
#define CRDHWSimImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <CRDHWSimBase.h>
#include <map>
#include <vector>
// For ACS::Time:
#include <acscommonC.h>


namespace AMB
{
    // Please use this class to implement complex functionality for the
    // CRDHWSimBase helper functions. Use AMB::TypeConvertion methods
    // for convenient type convertions.
    class CRDHWSimImpl: public CRDHWSimBase
    {
        public:
        /// Constructor.
        CRDHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);

        /// Destructor.
        virtual ~CRDHWSimImpl();


        protected:
        /// Specific Monitor get helper functions

        /// \return time stamp of the maser counter rounded down to second
        /// accuracy.
        virtual std::vector< CAN::byte_t > getMonitorMaserVsGpsCounter() const;

        /// \return time stamp of the maser counter.
        virtual std::vector< CAN::byte_t > getMonitorMaserCounter() const;


        /// Specific Control set helper functions

        /// Set the reset time of the maser counter in the associated monitor
        /// point and call \ref AMB::CRDHWSimBase::setControlSetResetTime(data).
        /// \param data: time stamp of the wanted reset time.
        virtual void setControlSetResetTime(const std::vector< CAN::byte_t >& data);

        /// Reset the maser counter now, i.e. set data = now and call then
        /// \ref AMB::CRDHWSimBase::setControlMaserCounterReset(data).
        virtual void setControlMaserCounterReset(
            const std::vector< CAN::byte_t >& data);


        private:
        /// No default constructor allowed.
        CRDHWSimImpl();

        /// No copy constructor allowed.
        CRDHWSimImpl(const CRDHWSimImpl&);

        /// No assignment operator allowed.
        CRDHWSimImpl& operator=(const CRDHWSimImpl&);

        /// Send data in data to the RCA. Used to share data between getters
        /// and setters.
        /// \parm RCA: Target RCA.
        /// \parm data: Data values (CAN vector format, max. 8 bytes) to be
        /// stored at the target RCA.
        void associate(const AMB::rca_t RCA,
            const std::vector< CAN::byte_t >& data);

        /// Calculate the current Maser counter based on the system time.
        /// \param roundDownTo1PPS: the time data will be rounded down to the
        /// current second, i.e. accuracy is seconds.
        /// \return The time stamp in \ref std::vector< CAN::byte_t > format.
        std::vector< CAN::byte_t > calcCMaserCounter(
            const bool roundDownTo1PPS) const;

        /// Keeps track of the time stamp when the MASER_COUNTER was reset.
        ACS::Time resetMaserCounterTime;
    };

};
#endif
