#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef CRD_h
#define CRD_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  May 12, 2008  created
//


// Base class
#include <CRDBase.h>
//CORBA servant header
#include <CRDS.h>
// ACS header files
#include <acserr.h>
// Header file for AMB semaphore
#include <semaphore.h>


namespace maci
{
    class ContainerServices;
};


class CRDImpl: public CRDBase, public virtual POA_Control::CRD
{
    public:
    /// Constructor
    /// \param poa Poa which will activate this and also all other components.
    /// \param name component's name. This is also the name that will
    /// be used to find the configuration data for the component in the
    /// Configuration Database.
    CRDImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /// Destructor
    virtual ~CRDImpl();

    /* --------------------- [ CORBA interface ] ----------------------*/
    /// Reset the maser counter at given time.  The reset is synchronized to
    /// the TE.
    /// \return Reset time when the AMB request has been executed by
    /// the ambServer kernel module.
    /// \exception ControlExceptions::CAMBErrorEx
    virtual ACS::Time maserCounterReset(ACS::Time whenToExecute);

    /// Calculate the offset in seconds between the 125MHz counter in the CRD
    /// and the GPS time.
    /// \return Time difference between GPS time and the 125MHz Maser counter.
    /// \exception ControlExceptions::CAMBErrorEx
    /// What is really done is that the latched value MASER_VS_GPS_COUNTER is
    /// read and the value modulo 125MHz returned.  This harbours the danger
    /// that it is ambigous how many seconds we are already off.  Let's hope
    /// that the CRG is accurate enough.
    virtual CORBA::Double getArrayTimeOffset();

    /* --------------------- [ Hardware Lifecycle ] ----------------------*/
    /* See the controlDevice.h file for information on this script */
    virtual void hwInitializeAction();


    private:
    /// This method resets the maser counter at time executeTime. It is
    /// synchronized to the 48 ms tick.
    /// \param resetTime: Time in 100 ns units to define when the reset shall be
    /// done. If it is less than the actual time the reset will be done at the
    /// next tick.
    /// \param timestamp: time when the reset was performed.
    /// \return value: The error code (as defined in the ACS Error System) that
    /// occured while reading this value or 0 if no error occured.
    virtual AmbErrorCode_t resetMaserCounter(ACS::Time resetTime,
        ACS::Time* timestamp);

    /// AMB sempahore.
    sem_t synchLock;
};
#endif
