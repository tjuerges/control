#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#
# Phase 2 test of ALMA Calibration Device
#
# who       when        what
# --------  ----------  ----------------------------------------------
# eallaert  2009-03-04  created
#

import sys
import unittest
import struct
import ControlExceptions
from CCL.AmbManager import *
from time import sleep
from time import time
from random import random

# WVR states 
modes = {'operational': 0 , 'idle': 1 , 'configuration': 2 , 'N/A': 3, 'operational2': 4}
modeNrToName ={0: 'operational', 1: 'idle', 2: 'configuration', 3: 'N/A', 4: 'operational2'}

def testProperty(compRef, rca, testName, propName, propLen, format, minVal, maxVal):
    '''
    Tests a single BACI property
    '''
    compRef.startTestLog("Test " + testName + ": " + propName + " Test")

    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, rca)
    errStr = str(propName) + " returned incorrect length value, actual = "
    errStr += str(len(r)) + " bytes, expected " + str(propLen) + " bytes"
    compRef.failUnless(len(r) == propLen, errStr)
    (resp,)= struct.unpack(format,r)
    compRef.log(str(propName) + " returns value " + str(resp))
    errStr = str(propName) + " returned value out of bounds, value = " + str(resp)
    compRef.failUnless((resp <= maxVal) and (resp >= minVal), errStr)

    compRef.endTestLog("Test " + testName + ": " + propName + " Test")
    return

def testProperty2(compRef, rca, testName, propName, propLen, format, minVal1, maxVal1, minVal2, maxVal2):
    '''
    Tests a dual BACI property
    '''
    compRef.startTestLog("Test " + testName + ": " + propName + " Test")

    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, rca)
    errStr = str(propName) + " returned incorrect length value, actual = "
    errStr += str(len(r)) + " bytes, expected " + str(propLen) + " bytes"
    compRef.failUnless(len(r) == propLen, errStr)
    (resp1, resp2)= struct.unpack(format,r)
    compRef.log(str(propName) + " returns values " + str(resp1) + ", " + str(resp2))
    errStr = str(propName) + " returned value out of bounds, values = " + str(resp1) + ", " + str(resp2)
    compRef.failUnless((resp1 <= maxVal1) and (resp1 >= minVal1), errStr)
    compRef.failUnless((resp2 <= maxVal2) and (resp2 >= minVal2), errStr)

    compRef.endTestLog("Test " + testName + ": " + propName + " Test")
    return

def getStatus(compRef):
    '''
    Get the STATE and ALARM monitor points and analyse the bits
    '''
    # GET_WVR_STATE Byte 0:
    #   bits 0-1 = mode
    #   bit 2    = ready for operational mode
    #   bit 3    = LO locked at specified frequency
    #   bit 4    = at least one of the alarms triggered
    #   bit 5    = just booted
    #   bit 6    = 125 MHz external clock available
    #   bit 7    = external TE ticks present
    stateMasks = { 'mode': 0x03, 'op': 0x04, 'LO': 0x08, 'alarm': 0x10, 'boot': 0x20, 'clk': 0x40, 'TE': 0x80} 
    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, compRef.monitor['GET_WVR_STATE'][0])
    ##r = '\x36' 
    (byte0, ) = struct.unpack("!B", r)
    mode     = byte0 & stateMasks['mode']
    ready    = bool(byte0 & stateMasks['op'])
    LOlocked = bool(byte0 & stateMasks['LO'])
    alarm    = bool(byte0 & stateMasks['alarm'])
    boot     = bool(byte0 & stateMasks['boot'])
    clk      = bool(byte0 & stateMasks['clk'])
    TE       = bool(byte0 & stateMasks['TE'])
    
    # GET_WVR_ALARMS:
    #   alarmMasks = {<key=byteNr> : {<key=bitMask> : alarmDescription}, ...}
    alarmMasks = {0: {1  : '12V rail switched off',
                      2  : 'monitor point requested in improper mode',
                      4  : 'control point issued in improper mode',
                      8  : 'control point issued with wrong nr of bytes',
                      16 : 'LOCK state not achieved or lost',
                      224: 'unused bit(s) set in byte 0'},
                  1: {1  : '+12V supply current out of range',
                      2  : '+6V supply current out of range',
                      4  : '-6V supply current out of range',
                      8  : 'chopper wheel current out of range',
                      240: 'unused bit(s) set in byte 1'},
                  2: {1  : '+12V supply voltage out of range',
                      2  : '+6V supply voltage out of range',
                      4  : '-6V supply voltage out of range',
                      248: 'unused bit(s) set in byte 2'},
                  3: {1  : 'hot load over-temperature protection tripped',
                      2  : 'cold load over-temperature protection tripped',
                      4  : 'CTRL over-temperature protection tripped',
                      8  : 'BE over-temperature protection tripped',
                      16 : 'CS over-temperature protection tripped',
                      224: 'unused bit(s) set in byte 3'},
                  4: {1  : 'self-test chopper wheel error',
                      2  : 'self-test calibration file error',
                      4  : 'self-test LO error',
                      248: 'unused bit(s) set in byte 4'}
                  }
    
    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, compRef.monitor['GET_WVR_ALARMS'][0])
    ##r = '\x01\x66\x03\x04\x05'
    bytes = struct.unpack("!5B", r)
    
    alarmMsgs = []
    for byteNr in sorted(alarmMasks.keys()):
        for bitMask in sorted(alarmMasks[byteNr].keys()):
            test = bitMask & bytes[byteNr] 
            if test :
                where = ' (byte%d: 0x%.2x)' % (byteNr, test)
                alarmMsgs.append(alarmMasks[byteNr][bitMask] + where)

    status = {'mode': mode, 'ready' : ready, 'LOlocked' : LOlocked,
              'alarm' : alarm, 'boot' : boot, 'clk' : clk, 'TE' : TE,
              'alarmMsgs' : alarmMsgs}
    
    return status
    
def printStatus(compRef):
    '''
    Get the WVR state and alarm monitor points, analyse the bits and print the status
    '''
    status = getStatus(compRef)
    mode = modeNrToName[status['mode']]
    compRef.log ("WVR status: mode = '%s'" % mode)
    ##print "WVR status: mode = '%s'" % (mode,)
                 
    if status['ready'] :
        compRef.log("            ready for operational mode")
        ##print "            ready for operational mode"
    if status['LOlocked'] :
        compRef.log("            LO locked at specified frequency")
        ##print "            LO locked at specified frequency"
    if status['alarm'] :
        compRef.log("            at least one of the alarms triggered")
        ##print "            at least one of the alarms triggered"
    if status['boot'] :
        compRef.log("            just booted")
        ##print "            just booted"
    if status['clk'] :
        compRef.log("            125 MHz external clock available")
        ##print "            125 MHz external clock available"
    if status['TE'] :
        compRef.log("            external TE ticks present")
        ##print "            external TE ticks present"
    for alarm in status['alarmMsgs'] :
        compRef.log("            %s" % alarm)
        ##print "            %s" % alarm
        
    return status
    
def getChopStatus(compRef):
    '''
    Get the chopper wheel status monitor points and analyse the bits
    '''
    # GET_CHOP_STATE
    # Byte 0:
    #   bits 0-1 = chopper wheel velocity
    #   bits 2-3 = chopper wheel phase setting
    #   bits 4-5 = chopper wheel phase
    # Byte 1:
    #   bits 0-7 = blanking
    stateMasks = { 'velocity': 0x03, 'phaseSetting': 0x0C, 'phase': 0x30, 'illegal': 0xC0} 
    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, compRef.monitor['GET_CHOP_STATE'][0])
    (byte0, byte1) = struct.unpack("!2B", r)
    velocity     = byte0 & stateMasks['velocity']
    phaseSetting = (byte0 & stateMasks['phaseSetting']) >> 2 
    phase        = (byte0 & stateMasks['phase']) >> 4
    illegalState = byte0 & stateMasks['illegal']
    blanking     = byte1

    # GET_CHOP_PWM
    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, compRef.monitor['GET_CHOP_PWM'][0])
    (pwm,) = struct.unpack("!B", r)

    # GET_CHOP_POS
    # Byte 0:
    #   bits 0-3 = high order bits of present chopper wheel position
    # Byte 1:
    #   bits 0-7 = low order bits of present chopper wheel position
    # Byte 2:
    #   bits 0-3 = high order bits of calibrated zero chopper wheel position
    # Byte 3:
    #   bits 0-7 = low order bits of calibrated zero chopper wheel position
    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, compRef.monitor['GET_CHOP_POS'][0])
    (position, zeroPos) = struct.unpack("!2H", r)
  
    # GET_CHOP_CURR
    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, compRef.monitor['GET_CHOP_CURR'][0])
    (current,) = struct.unpack("!f", r)

    status = {'velocity': velocity, 'phaseSetting': phaseSetting, 'phase': phase,
              'illegalState': illegalState, 'blanking': blanking,
              'pwm' : pwm, 'position': position, 'zero': zeroPos, 'current': current}
              
    return status
    
def printChopStatus(compRef):
    '''
    Get the WVR chopper wheel state monitor points, analyse the bits
    and print the status
    '''
    status = getChopStatus(compRef)
    velocityNames = ['Not applicable', '10.42Hz', 'Fixed position', '5.21Hz']
    velocity = velocityNames[status['velocity']]
    
    compRef.log("Chopper wheel status: velocity = %s" % velocity)

    phaseNames = ['cold load', 'skyA', 'hot load', 'skyB']
    phaseSetting = phaseNames[status['phaseSetting']]
    phase = phaseNames[status['phase']]
    compRef.log("                      phase setting = '%s'" % phaseSetting)
    compRef.log("                      phase = '%s'" % phase)

    blanking = float(status['blanking'])*800./4096.
    compRef.log("                      blanking = %d (%.2f%%)" % (status['blanking'], blanking))
                     
    if bool(status['illegalState']) :
        compRef.log("                      NOTE: illegal bits set in status: %.2x" %
                    status['illegalState'])

    pwm = float(status['pwm'])*100./255.
        
    compRef.log("                      PWM = %d (%.2f%%)" % (status['pwm'],pwm))
    compRef.log("                      position = %d" % status['position'])
    compRef.log("                      calibrated zero position = %d" % status['zero'])
    compRef.log("                      current = %.3fA" % status['current'])   
    return status
    
def waitTillInPosition(compRef, timeout):
    '''
    Wait till the in-position bit of the status monitor point is set
    '''
    startTime = time()
    compRef.log("Waiting till in position (timeout = " + str(timeout) + "s)")
    
    sleep(inPositionLatency)

    sleepTime = 0.03125
    elapsedTime = time() - startTime
    status = getStatus(compRef)
    while (not status['inPosition']) and (elapsedTime < timeout) :
        sleep(sleepTime)
        elapsedTime = time() - startTime
        status = getStatus(compRef)
        
    if elapsedTime < timeout :
        compRef.log("Motors in position within %.3fs." % elapsedTime)
    else:
        compRef.log("Motors not in position within %.3fs." % elapsedTime)
        printStatus(compRef)
        compRef.fail("Motors not in position within %.3fs." % elapsedTime)
    
    return elapsedTime

# In PyUnit, test cases are represented by the TestCase class in the unittest module.
# Our own test cases must be subclasses of TestCase.
class Automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        self.out = None
        self.manager = AmbManager(antenna)
        # node number is set in main - it is one of the optional args
        self.node = node
        self.channel = 1

        # Control points
        self.rcaSET_WVR_STATE       = 0x8010
        self.rcaSET_INT_SETS        = 0x8130
        self.rcaSET_CHOP_VEL        = 0x8150
        self.rcaSET_CHOP_PHASE      = 0x8158
        self.rcaSET_HOT_TEMP        = 0x8180
        self.rcaSET_COLD_TEMP       = 0x81A0
        self.rcaSET_LO_FREQ         = 0x82D0

        self.rcaRESET_DEVICE        = 0x31001

        # Monitor points: {<key=name>:(<rca>,(<nrBytesReturned>,<operation>,<idle>,<config>)), ...}
        self.monitor = {'GET_SW_REV'      : (0x0008, (8, True, True,  True )),
                        'GET_WVR_STATE'   : (0x0010, (1, True, True,  True )),
                        'GET_WVR_ALARMS'  : (0x0018, (5, True, True,  True )),
                        'GET_INT_TSRC0'   : (0x0030, (8, True, True,  True )),
                        'GET_INT_TSRC1'   : (0x0038, (8, True, True,  True )),
                        'GET_INT_TSRC2'   : (0x0040, (8, True, True,  True )),
                        'GET_INT_TSRC3'   : (0x0048, (8, True, True,  True )),
                        'GET_INT_EST0'    : (0x0050, (8, True, True,  True )),
                        'GET_INT_EST1'    : (0x0058, (8, True, True,  True )),
                        'GET_INT_EST2'    : (0x0060, (8, True, True,  True )),
                        'GET_INT_EST3'    : (0x0068, (8, True, True,  True )),
                        'GET_INT_HOT0'    : (0x0070, (8, True, True,  True )),
                        'GET_INT_HOT1'    : (0x0078, (8, True, True,  True )),
                        'GET_INT_HOT2'    : (0x0080, (8, True, True,  True )),
                        'GET_INT_HOT3'    : (0x0088, (8, True, True,  True )),
                        'GET_INT_COLD0'   : (0x0090, (8, True, True,  True )),
                        'GET_INT_COLD1'   : (0x0098, (8, True, True,  True )),
                        'GET_INT_COLD2'   : (0x00A0, (8, True, True,  True )),
                        'GET_INT_COLD3'   : (0x00A8, (8, True, True,  True )),
                        'GET_INT_SKYA0'   : (0x00B0, (8, True, True,  True )),
                        'GET_INT_SKYA1'   : (0x00B8, (8, True, True,  True )),
                        'GET_INT_SKYA2'   : (0x00C0, (8, True, True,  True )),
                        'GET_INT_SKYA3'   : (0x00C8, (8, True, True,  True )),
                        'GET_INT_SKYA3'   : (0x00C8, (8, True, True,  True )),
                        'GET_INT_SKYA3'   : (0x00C8, (8, True, True,  True )),
                        'GET_INT_SKYA3'   : (0x00C8, (8, True, True,  True )),
                        'GET_INT_SKYB0'   : (0x00D0, (8, True, True,  True )),
                        'GET_INT_SKYB1'   : (0x00D8, (8, True, True,  True )),
                        'GET_INT_SKYB2'   : (0x00E0, (8, True, True,  True )),
                        'GET_INT_SKYB3'   : (0x00E8, (8, True, True,  True )),
                        'GET_INT_TIMEH'   : (0x00F0, (4, True, True,  True )),
                        'GET_INT_TIMEC'   : (0x00F8, (4, True, True,  True )),
                        'GET_INT_TIMEA'   : (0x0100, (4, True, True,  True )),
                        'GET_INT_TIMEB'   : (0x0108, (4, True, True,  True )),
                        'GET_INT_SETS'    : (0x0130, (4, True, True,  True )),
                        'GET_CHOP_STATE'  : (0x0150, (2, True, True,  True )),
                        'GET_CHOP_PWM'    : (0x0158, (1, True, True,  True )),
                        'GET_CHOP_POS'    : (0x0160, (4, True, True,  True )),
                        'GET_CHOP_CURR'   : (0x0168, (4, True, True,  True )),
                        'GET_HOT_TEMP'    : (0x0180, (8, True, True,  True )),
                        'GET_HOT_PWM'     : (0x0188, (1, True, True,  True )),
                        'GET_HOT_NTC'     : (0x0190, (2, True, True,  True )),
                        'GET_COLD_TEMP'   : (0x01A0, (8, True, True,  True )),
                        'GET_COLD_PWM'    : (0x01A8, (1, True, True,  True )),
                        'GET_COLD_NTC'    : (0x01B0, (2, True, True,  True )),
                        'GET_CTRL_12CURR' : (0x01C0, (4, True, True,  True )),
                        'GET_CTRL_6CURR'  : (0x01C8, (4, True, True,  True )),
                        'GET_CTRL_M6CURR' : (0x01D0, (4, True, True,  True )),
                        'GET_CTRL_12VOLT' : (0x01D8, (4, True, True,  True )),
                        'GET_CTRL_6VOLT'  : (0x01E0, (4, True, True,  True )),
                        'GET_CTRL_M6VOLT' : (0x01E8, (4, True, True,  True )),
                        'GET_CTRL_NTC'    : (0x01F0, (2, True, True,  True )),
                        'GET_TP_TEMP'     : (0x0210, (8, True, True,  True )),
                        'GET_TP_PWM'      : (0x0218, (1, True, True,  True )),
                        'GET_BE_TEMP'     : (0x0220, (8, True, True,  True )),
                        'GET_BE_PWM'      : (0x0228, (1, True, True,  True )),
                        'GET_BE_NTC'      : (0x0230, (2, True, True,  True )),
                        'GET_BE_BIAS0'    : (0x0240, (4, True, True,  True )),
                        'GET_BE_BIAS1'    : (0x0248, (4, True, True,  True )),
                        'GET_BE_BIAS2'    : (0x0250, (4, True, True,  True )),
                        'GET_BE_BIAS3'    : (0x0258, (4, True, True,  True )),
                        'GET_BE_BW0'      : (0x0260, (4, True, True,  True )),
                        'GET_BE_BW1'      : (0x0268, (4, True, True,  True )),
                        'GET_BE_BW2'      : (0x0270, (4, True, True,  True )),
                        'GET_BE_BW3'      : (0x0278, (4, True, True,  True )),
                        'GET_CS_TEMP'     : (0x02A0, (8, True, True,  True )),
                        'GET_CS_PWM'      : (0x02A8, (1, True, True,  True )),
                        'GET_CS_NTC'      : (0x02B0, (2, True, True,  True )),
                        'GET_LO_FREQ'     : (0x02D0, (4, True, True,  True )),
                        'GET_LO_BIAS0'    : (0x02D8, (4, True, True,  True )),
                        'GET_LO_BIAS1'    : (0x02E0, (4, True, True,  True )),
                        'GET_LNA_TEMP'    : (0x02F0, (4, True, True,  True )),
                        'GET_PROTOCOL_REV_LEVEL' : (0x30000, (3, True, True, True)),
                        'GET_CAN_ERROR'          : (0x30001, (4, True, True, True)),
                        'GET_TRANS_NUM'          : (0x30002, (4, True, True, True)),
                        'GET_AMBIENT_TEMPERATURE': (0x30003, (4, True, True, True)),
                        'GET_SW_REV_LEVEL'       : (0x30004, (3, True, True, True)) }
        
        # command: {<key=commandName>:(<rcaCmd>,<nrBytes>,<operation>,<idle>,<config>), ...}
        self.command = {'SET_WVR_STATE' : (self.rcaSET_WVR_STATE,  2, True,  True,  True ),
                        'SET_INT_SETS'  : (self.rcaSET_INT_SETS,   4, False, False, True ),
                        'SET_CHOP_VEL'  : (self.rcaSET_CHOP_VEL,   1, False, False, True ),
                        'SET_CHOP_PHASE': (self.rcaSET_CHOP_PHASE, 1, True,  False, True ),
                        'SET_HOT_TEMP'  : (self.rcaSET_HOT_TEMP,   4, False, False, True ),
                        'SET_COLD_TEMP' : (self.rcaSET_COLD_TEMP,  4, False, False, True ),
                        'SET_LO_FREQ'   : (self.rcaSET_LO_FREQ,    4, True,  False, True )
                        }
        self.genericMonitor = {'GET_SERIAL_NUMBER'      : (0x00000, 8),
                               'GET_PROTOCOL_REV_LEVEL' : (0x30000, 3),
                               'GET_CAN_ERROR'          : (0x30001, 4),
                               'GET_TRANS_NUM'          : (0x30002, 4),
                               'GET_AMBIENT_TEMPERATURE': (0x30003, 4),
                               'GET_SW_REV_LEVEL'       : (0x30004, 3) }
        # genericMonitorRCAs: {<key=RCA>:<nrBytes>, ...}
        self.genericMonitorRCAs = dict(self.genericMonitor.values())
        # monitorRCAs: {<key=RCA>:<nrBytes>, ...}
        self.monitorRCAs = dict(self.monitor.values())

        self.out = ""

    def __del__(self):
        if self.out != None:
            print self.out
        
        del(self.manager)


    def startTestLog(self, testDescription):
        self.log("\n")
        self.log("-------------------------------------------------------")
        callerName = sys._getframe(1).f_code.co_name
        self.log(callerName + ": start " + testDescription + "\n")

    def endTestLog(self, testDescription):
        callerName = sys._getframe(1).f_code.co_name
        self.log("\n" + callerName + ": end " + testDescription)
        self.log("-------------------------------------------------------")
                 
    def log(self, msg):
        #!# I wonder why you'd want to collect output for printing only
        #!# when the test is finished, instead of printing as the test goes.
        #self.out += msg + "\n"
        print msg
        # In case the output is re-directed to a file (e.g. by tat), these prints
        # may get intermixed with the output from pyunit, due to buffering. To
        # avoid that, do a flush.
        sys.stdout.flush()
        
    def getConfirmationOrWait(self, prompt, errStr):
        '''
        Either prompt and wait for acknowledge or wait a number of seconds.
        This method is intended to be used within methods that move motors
        to different position, where each new position must be verified
        (in this case there will simply be a pause).
        '''
        self.log(prompt + " after sleeping %ds." % confDelay)
        sleep (confDelay)

    ############################################################
    # Test Definitions
    def broadcast(self):
        '''
        Verify that the device correctly responds to broadcast messages
        and return the serial number when requested by RCA 0x0.

        For recordkeeping the serial number is returned.
        '''
        self.startTestLog("Broadcast Response test")
        sleep(1)
        
        try:
            (r,t) = self.manager.getNodes(self.channel)
        except:
            self.fail("Unable to get nodes on bus")

        foundNode = False
        for node in r:
            if node.node == self.node:
                if not foundNode:
                    foundNode = True
                    self.log("Device on node 0x%.4x serial number: 0x%x" %
                             (node.node,node.sn))
                else:
                    self.fail("Node 0x%x responded at least twice" % self.node)
            else:
                self.log("(Device on node 0x%.4x replied, serial Number 0x%x)" %
                         (node.node, node.sn))
                pass

        self.failUnless(foundNode, "Node %d did not respond" % self.node)

        try:
            (sn, time) = self.manager.findSN(self.channel, self.node)
        except:
            self.fail("Exception retrieving serial number from node")

        self.endTestLog("Broadcast Response test")

    # --------------------------------------------------
    def genericMonitors(self):
        '''
        Verify that all mandatory generic monitor points are implemented.
        '''
        self.startTestLog("Mandatory Generic Monitor Points test")
        # The keys() method returns an *unsorted* list!
        for name in sorted(self.genericMonitor.keys()):
            rca = self.genericMonitor[name][0]
            try:
                (r,t) = self.manager.monitor(self.channel, self.node, rca)
                self.failUnless(len(r) == self.genericMonitorRCAs[rca],
                                "Length of response incorrect for %s (0x%.5x)"
                                % (name,rca))
                
                if len(r) == 8:
                    # Must be request for serial number
                    (sn,) = struct.unpack("!Q", r)
                    self.log("Reply on %s (0x%.5x): 0x%.8x = %d" % (name, rca, sn, sn))
                elif len(r) == 4:
                    # must be number of CAN errors, number of transmissions or ambient temperature
                    if name == 'GET_AMBIENT_TEMPERATURE' :
                        (LSB, MSB, cRemain, cPerDegree) = struct.unpack("!4B", r)
                        (val,) = struct.unpack("!h",struct.pack("!2B", MSB, LSB))
                        # See DS1820 data sheet pages 3 & 4 for details
                        temp = val/2 - 0.25 + float(cPerDegree - cRemain)/cPerDegree
                        self.log("Reply on %s (0x%.5x): %.1f degrees C (%d, %d, %d)" % (name, rca, temp, val, cRemain, cPerDegree))
                    else:    
                        (val,)  = struct.unpack("!I", r)
                        self.log("Reply on %s (0x%.5x): %d" % (name, rca, val))
                elif len(r) == 3:
                    # must be release number major.minor.patchLevel
                    (major, minor, patch)  = struct.unpack("!3B", r)
                    self.log("Reply on %s (0x%.5x): %d.%d.%d" % (name, rca, major, minor, patch))
                
            except ControlExceptions.CAMBErrorEx:
                self.fail("Exception generated, monitoring point 0x%.5x" % rca)
                    
        self.endTestLog("Mandatory Generic Monitor Points test")


    # --------------------------------------------------
    def replyLength(self):
        '''
        This test ensures that monitors documented in the ICD respond with
        the correct number of bytes. On top of that, it also checks for all
        monitor points if they can only be called in the right WVR mode
        (as defined in the ICD).
        '''
        self.startTestLog("Monitor reply length test")
        errorList = {}

        for mode in range(0,3) :
            modeName = modeNrToName[mode]
                
            status = getStatus(self)
            startTime = time()
            if modeName == 'operational':
                if not status['ready']:
                    self.log("WVR not ready for operational mode!! Skipping this mode")
                    continue
            # reset tripped alarms and timestamp counter
            data = 0x03
            self.log("\nreplyLength - setting WVR to state %d/'%s'" % (mode,modeName))
            t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                     struct.pack("!2B", mode, data))
            
            while status['mode'] != mode :
                elapsedTime = time() - startTime
                if elapsedTime > 10 :
                    printStatus(self)
                    self.fail ("'%s' mode not reached within 10s" % modeName)
                sleep(0.100)
                status = getStatus(self)
                
            printStatus(self)
            
            # The keys() method returns an *unsorted* list!
            for name in sorted(self.monitor.keys()):
                rca = self.monitor[name][0]
                try:
                    
                    (r,t) = self.manager.monitor(self.channel, self.node, rca)
                    if self.monitor[name][1][1+mode] :
                        self.failIf(len(r) != self.monitor[name][1][0],
                                    "RCA 0x%.5x (%s) returned %d bytes (expected %d)" %
                                    (rca, name, len(r),self.monitor[name][1][0]))
                    else:
                        status = getStatus(self)
                        # alarm should be raised, as this was a request in improper mode
                        if status['alarm'] == True:
                            alarmFound = False
                            for alarm in status['alarmMsgs']:
                                if alarm.find('monitor point requested in improper mode') != -1:
                                    alarmFound = True
                                    break
                            if alarmFound:
                                ###self.log("proper alarm bit set for %s in mode %s" % (name, modeName))    
                                # this triggered a proper alarm - reset it
                                pass
                            else:
                                ###self.log("alarm set for %s in mode %s, but not flagging access in improper mode??"
                                ###         % (name, modeName))    
                                if errorList.has_key(name):
                                    errorList[name] += modeName,
                                else:
                                    errorList[name] = modeName,
                               
                            data = 0x02   
                            t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                                      struct.pack("!2B", mode, data))
                        else:
                            ###self.log("no alarm set for %s in mode %s??" % (name, modeName))
                            if errorList.has_key(name):
                                errorList[name] += modeName,
                            else:
                                errorList[name] = modeName,
                            
                    
                except Exception, e:
                    #print("---> exception " + str(e))
                    if self.monitor[name][1][1+mode]:
                        if errorList.has_key(name):
                            errorList[name] += modeName,
                        else:
                            errorList[name] = modeName,
                                
                #!# Without this sleep, there are CAMB errors - rate too high?
                #sleep (0.001953125)
                sleep (0.002)
                
        if len(errorList) != 0:

            # The keys() method returns an *unsorted* list!
            for name in sorted(errorList.keys()):
                for modeName in errorList[name]:
                    mode = modes[modeName]
                    if self.monitor[name][1][1+mode]:
                        self.log("%s did not reply in WVR state '%s'" % (name,modeName))
                    else:
                        self.log("no or improper alarm bit set for %s in WVR state '%s'" % (name,modeName))

            self.fail("Monitor point responses not conform with WVR state table in ICD")

        self.endTestLog("Monitor reply length test")

    # --------------------------------------------------
    def nodeId(self):
        '''
        This test verifies if the device responds to a wrong node-ID,
        for monitors documented in the ICD.
        '''
        self.startTestLog("Node-ID test")
        # The keys() method returns an *unsorted* list!
        nodeId = self.node
        for name in sorted(self.monitor.keys()):
            rca = self.monitor[name][0]
            nodeId = nodeId + 1
            try:
                (r,t) = self.manager.monitor(self.channel, nodeId, rca)
                self.fail("Got a reply for node-id 0x%.2x, RCA 0x%.5x (%s)" % (nodeId, rca, name))
                
            except Exception, e:
                #print("---> exception " + str(e))
                pass
            

        self.endTestLog("Node-ID test")

    # --------------------------------------------------
    def getWvrState(self):
        '''
        Show the reply for the GET_WVR_STATE monitor point.
        '''
        self.startTestLog("GET_WVR_STATE tests")
        self.log("\n")
        printStatus(self)

        
        self.endTestLog("GET_WVR_STATE tests")

    # --------------------------------------------------
    def getChopState(self):
        '''
        Show the reply for the GET_CHOP_STATE, GET_CHOP_PWM,
        GET_CHOP_POS AND GET_CHOP_CURR monitor points.
        '''
        #!# NOTE: at least the current is NOT updated when in idle mode
        #!# (it is left at the current before going to idle mode).
        self.startTestLog("GET_CHOP_* tests")
        self.log("\n")
        
        loop = 0
        while loop < nrLoops :
            printChopStatus(self)
            loop += 1
            sleep(1.0)
        
        self.endTestLog("GET_CHOP_* tests")

    # --------------------------------------------------
    def setWvrState(self):
        '''
        Verify the SET_WVR_STATE control point, and an appropiate response
        from the GET_WVR_STATE monitor point.
        '''
        self.startTestLog("SET_WVR_STATE tests")
        self.log("\n")
        printStatus(self)

        for mode in sorted(modeNrToName.keys()) :
            modeName = modeNrToName[mode]
                
            
            # reset tripped alarms and timestamp counter
            #!# Note: self-test bits do NOT get reset (there is a reboot needed for these).
            data = 0x03
            self.getConfirmationOrWait("Will set WVR to state %d/'%s'" % (mode,modeName),
                                       "User cancelled state-change test.")
            self.log("\nsetWvrState - setting WVR to state %d/'%s'" % (mode,modeName))
            t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                     struct.pack("!2B", mode, data))
            sleep(0.05)
            status = getStatus(self)
            startTime = time()
            if mode % 4 == 0:
                if not status['ready']:
                    self.log("WVR not ready for operational mode!! Skipping this mode")
                    continue

            # Mode > 3 gets bitwise and-ed with 3
            while status['mode'] != mode % 4 :
                elapsedTime = time() - startTime
                if elapsedTime > 10 :
                    printStatus(self)
                    self.fail ("'%s' mode not reached within 10s" % modeName)
                sleep(0.100)
                status = getStatus(self)
                
            #!# Is the mode set immediately active, or is there a delay needed ??
            printStatus(self)
            
            
        self.endTestLog("SET_WVR_STATE tests")

    # --------------------------------------------------
    def setIntSets(self):
        '''
        Verify the SET_INT_SETS control point, and an appropiate response
        from the GET_INT_SETS monitor point.
        '''
        self.startTestLog("SET_INT_SETS tests")
        self.log("\n")
        printStatus(self)

        # Do a SET_WVR_STATE to configuration mode
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetIntSets - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep (0.050)
                
        printStatus(self)
            
        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_INT_SETS'][0])
        (tOld,eOld)= struct.unpack("!2H", r)
        
        # Now issue the SET_INT_SETS command 
        for tsrc in [0, 1, 128, 1023, 1024, 32765] :
            for est in [0, 1, 128, 1023, 1024, 32765] :
                self.log("setIntSets - setting Tsrc to %d, EST to %d." % (tsrc, est))
                t = self.manager.command(self.channel, self.node, self.rcaSET_INT_SETS,
                                         struct.pack("!2H", tsrc, est))

                # monitor value should be updated within next timing period.
                sleep(0.050)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_INT_SETS'][0])
                (t,e)= struct.unpack("!2H", r)
                
                #!# setting to > 1023 means effectively ignoring upper bits (AND 1023)
                tsrc = tsrc & 1023
                est = est & 1023
                
                if (tsrc == 0) or (est == 0) :
                    #!# looks like setting to 0 isn't possible (set to 1)
                    #!# should be out-of-range value -> no reaction (apart from setting alarm/status bit)
                    self.failIf ((tsrc == 0 and t != tOld) or (est == 0 and e != eOld)) 
                else:
                    self.failIf (t != tsrc or e != est)

                tOld = t
                eOld = e

                #!# No alarm/status bit set to indicate out-of-range setting?

        self.endTestLog("SET_INT_SETS tests")
    
    # --------------------------------------------------
    def setChopVel(self):
        '''
        Verify the SET_CHOP_VEL control point, and an appropiate response
        from the GET_CHOP_STATE monitor point.
        '''
        self.startTestLog("SET_CHOP_VEL tests")
        self.log("\n")
        printStatus(self)

        # Do a SET_WVR_STATE to configuration mode
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetChopVel - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep (0.050)
                
        printStatus(self)
            
        # Now issue the SET_CHOP_VEL command 
        velocityNames = ['N/A', '10.42Hz', 'fixed position at phase', '5.21Hz', 'N/A', '10.42Hz']
        currentVelocity = getChopStatus(self)['velocity']
        for velocity in range(0,6) :
            #!#for velocity in range(3,4) :
            self.log("\nsetChopVel - setting chopper to %d (%s)." % (velocity, velocityNames[velocity]))
            t = self.manager.command(self.channel, self.node, self.rcaSET_CHOP_VEL,
                                     struct.pack("!B", velocity))

            sleep (0.050)
            status = getChopStatus(self)
            startTime = time()
            #!# velocity 0 leads to no change, > 3 is bitwise and-ed with 3
            if velocity % 4 == 0 :
                #!# Should keep current velocity
                self.failIf(status['velocity'] != currentVelocity)
            else:
                # It takes ~8s to change the velocity
                while (velocityNames[status['velocity']] != velocityNames[velocity]) :
                    elapsedTime = time() - startTime
                    if elapsedTime > 10 :
                        printChopStatus(self)
                        self.fail ("chopper wheel velocity %d (%s) not reached within 10s"
                                   % (velocity, velocityNames[velocity]))
                    sleep(0.100)
                    status = getChopStatus(self)

            currentVelocity = status['velocity']
            
            printChopStatus(self)

        self.endTestLog("SET_WVR_STATE tests")
    # --------------------------------------------------
    def setChopPhase(self):
        '''
        Verify the SET_CHOP_PHASE control point, and an appropiate response
        from the GET_CHOP_STATE monitor point.
        '''
        self.startTestLog("SET_CHOP_PHASE tests")
        self.log("\n")
        printStatus(self)

        # Do a SET_WVR_STATE to configuration mode
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetChopPhase - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep (0.050)
                
        printStatus(self)
            
        # Now issue the SET_CHOP_PHASE command 
        phaseNames = ['cold load', 'skyA', 'hot load', 'skyB', 'cold load', 'skyA']
        for phase in range(0,6) :
            self.log("\nsetChopPhase - setting chopper phase to %d (%s)." % (phase, phaseNames[phase]))
            t = self.manager.command(self.channel, self.node, self.rcaSET_CHOP_PHASE,
                                     struct.pack("!B", phase))

            sleep (0.050)
            status = getChopStatus(self)
            startTime = time()
            #!# phase > 4 is bitwise and-ed with 3
            while (phaseNames[status['phaseSetting']] != phaseNames[phase]) :
                elapsedTime = time() - startTime
                if elapsedTime > 10 :
                    printChopStatus(self)
                    self.fail ("chopper wheel phase %d (%s) not reached within 10s"
                               % (phase, phaseNames[phase]))
                sleep(0.100)
                status = getChopStatus(self)

            printChopStatus(self)
        
        #!# At the end of this test, the chopper's phase should be set to skyA, unless
        #!# the action on an illegal chopper phase (5) is wrong.

        self.endTestLog("SET_WVR_PHASE tests")

    # --------------------------------------------------
    def setHotTemp(self):
        '''
        Verify the SET_HOT_TEMP control point, and an appropiate response
        from the GET_HOT_TEMP, GET_HOT_PWM and GET_HOT_NTC monitor points.
        '''
        self.startTestLog("SET_HOT_TEMP tests")
        self.log("\n")
        printStatus(self)

        # Do a SET_WVR_STATE to configuration mode
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetHotTemp - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep (0.050)
        printStatus(self)
            
        # Now issue the SET_HOT_TEMP command
        #!# What is the legal range of this temp?? (max 373K - see GET_WVR_ALARMS?)
        #!# What is the behaviour when an out-of-range temp is given? No alarm bit set?
        #!# --> TEMPS in GET_HOT_TEMP will not change for out-of-range setting
        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_TEMP'][0])
        (temp, ts)= struct.unpack("!2f",r)
        # start off from the current setting (which must be within range)
        origTemp = ts
        tempSet = origTemp + 2. * random() - 0.5
        if tempSet > 370. or tempSet < 350.:
            tempSet = 363.
        elif tempSet > origTemp and tempSet - origTemp < 0.25:
            # Heating up with at least 0.25 degrees
            tempSet += 0.25
        elif tempSet <= origTemp and origTemp - tempSet < 0.25:
            # cooling down with at least 0.25 degrees
            tempSet -= 0.25

        for temp in [tempSet, 274., 399.]:
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_TEMP'][0])
            (startTemp, ts)= struct.unpack("!2f",r)
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_PWM'][0])
            (pwm,)= struct.unpack("!B",r)
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_NTC'][0])
            (ntc,)= struct.unpack("!H",r)
            self.log("\nsetHotTemp - changing temperature from %.2f (pwm %d, ntc %d) to %.2f degrees K."
                     % (startTemp, pwm, ntc, temp))
            t = self.manager.command(self.channel, self.node, self.rcaSET_HOT_TEMP,
                                     struct.pack("!f", temp))

            sleep(0.050)
            startTime = time()
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_TEMP'][0])
            (currentTemp, ts)= struct.unpack("!2f", r)
            # For the out-of-range values, TEMPS should remain at the previous legal value
            self.failIf(abs(ts - tempSet) > 0.001, "temperature setting read back is %.3f" % ts)

            # Waiting for an effective temp change only makes sens for legal value
            if temp == tempSet:
        
                while (abs(currentTemp - startTemp) < 0.0625 ) :
                    elapsedTime = time() - startTime
                    if elapsedTime > 30 :
                        self.fail ("hot load did not adjust within 30s (currently at %.2f degrees K)" % currentTemp)
                    sleep(0.100)
                    (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_TEMP'][0])
                    (currentTemp, ts)= struct.unpack("!2f",r)
        
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_PWM'][0])
                (pwm,)= struct.unpack("!B",r)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_NTC'][0])
                (ntc,)= struct.unpack("!H",r)
                self.log("OK: temperature changed to %.2f degrees K (pwm %d, ntc %d) within %.1fs."
                         % (currentTemp, pwm, ntc, elapsedTime))

        self.endTestLog("SET_HOT_LOAD tests")

    # --------------------------------------------------
    def setColdTemp(self):
        '''
        Verify the SET_COLD_TEMP control point, and an appropiate response
        from the GET_COLD_TEMP, GET_COLD_PWM and GET_COLD_NTC monitor points.
        '''
        self.startTestLog("SET_COLD_TEMP tests")
        self.log("\n")
        printStatus(self)

        # Do a SET_WVR_STATE to configuration mode
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetColdTemp - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep (0.050)
        printStatus(self)
            
        # Now issue the SET_COLD_TEMP command
        #!# What is the legal range of this temp?? (max 323K - see GET_WVR_ALARMS?)
        #!# What is the behaviour when an out-of-range temp is given? No alarm bit set?
        #!# --> TEMPS in GET_COLD_TEMP will not change for out-of-range setting
        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_TEMP'][0])
        (temp, ts)= struct.unpack("!2f",r)
        # start off from the current setting (which must be within range)
        origTemp = ts
        tempSet = origTemp + 2. * random() - 0.5
        if tempSet > 295. or tempSet < 275.0:
            tempSet = 283.
        elif tempSet > origTemp and tempSet - origTemp < 0.25:
            # Heating up with at least 0.25 degrees
            tempSet += 0.25
        elif tempSet <= origTemp and origTemp - tempSet < 0.25:
            # cooling down with at least 0.25 degrees
            tempSet -= 0.25

        for temp in [tempSet, 259., 325.]:
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_TEMP'][0])
            (startTemp, ts)= struct.unpack("!2f",r)
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_PWM'][0])
            (pwm,)= struct.unpack("!B",r)
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_NTC'][0])
            (ntc,)= struct.unpack("!H",r)
            self.log("\nsetColdTemp - changing temperature from %.2f (pwm %d, ntc %d) to %.2f degrees K."
                     % (startTemp, pwm, ntc, temp))
            t = self.manager.command(self.channel, self.node, self.rcaSET_COLD_TEMP,
                                     struct.pack("!f", temp))

            sleep(0.050)
            startTime = time()
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_TEMP'][0])
            (currentTemp, ts)= struct.unpack("!2f", r)
            # For the out-of-range values, TEMPS should remain at the previous legal value
            self.failIf(abs(ts - tempSet) > 0.001, "cold load temperature setting read back is %.3f" % ts)

            # Waiting for an effective temp change only makes sens for legal value
            if temp == tempSet:
        
                while (abs(currentTemp - startTemp) < 0.0625 ) :
                    elapsedTime = time() - startTime
                    if elapsedTime > 30 :
                        self.fail ("cold load did not adjust within 30s (currently at %.2f degrees K)" % currentTemp)
                    sleep(0.100)
                    (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_TEMP'][0])
                    (currentTemp, ts)= struct.unpack("!2f",r)
        
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_PWM'][0])
                (pwm,)= struct.unpack("!B",r)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_NTC'][0])
                (ntc,)= struct.unpack("!H",r)
                self.log("OK: temperature changed to %.2f degrees K (pwm %d, ntc %d) within %.1fs."
                         % (currentTemp, pwm, ntc, elapsedTime))

        self.endTestLog("SET_COLD_LOAD tests")

    # --------------------------------------------------
    def setLoFreq(self):
        '''
        Verify the SET_LO_FREQ control point, and an appropiate response
        from the GET_LO_FREQ monitor point.
        '''
        self.startTestLog("SET_LO_FREQ tests")
        self.log("\n")
        printStatus(self)

        # Do a SET_WVR_STATE to configuration mode
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetLoFreq - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep(0.050)
                
        printStatus(self)
            
        # Now issue the SET_LO_FREQ command
        #!# Legal range of this freq is 183300000 - 183350000 kHz. This is in the
        #!# operation mnl, but NOT the ICD ...
        #!! Not said anywhere how long this command takes. Currently, the
        #!# corresponding monitor point is updated immediately, i.e. BEFORE
        #!# the frequency has actually reached the set-value.
        #!# The behaviour for an out-of-range freq is that the monitor point
        #!# will not change.

        for freqSet in [183300000, 183300010, 183300029, 183299999, 183350000, 183300050, 183350001, 183350010]:
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_LO_FREQ'][0])
            (freq,)= struct.unpack("!I", r)
            self.log("\nsetLoFreq - setting LO frequency to %d kHz." % freqSet)
            sleep (0.050)
            t = self.manager.command(self.channel, self.node, self.rcaSET_LO_FREQ,
                                     struct.pack("!I", freqSet))

            # Note: the WVR does a modulo-10 on values within range.
            if freqSet > 183350000 or freqSet < 183300000:
                corrFreqSet = freqSet
            else:
                corrFreqSet = freqSet - (freqSet % 10)

            startTime = time()
            #!# while (freq != freqSet) :
            while (freq != corrFreqSet) :
                elapsedTime = time() - startTime
                if elapsedTime > 2 :
                    if corrFreqSet > 183350000 or corrFreqSet < 183300000:
                        self.log("LO frequency left at %d kHz" %freq)
                        break
                    else:
                        self.fail ("LO frequency not set within 2s (currently at %d kHz)" % freq)
                sleep(0.100)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_LO_FREQ'][0])
                (freq,)= struct.unpack("!I",r)
        
        self.endTestLog("SET_LO_FREQ tests")

    # --------------------------------------------------
    def getSwRev(self):
        '''
        Verify the GET_SW_REV monitor point.
        '''
        self.startTestLog("GET_SW_REV tests")
        self.log("\n")
        printStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_SW_REV'][0])
        (cpuh, cpul, fpgah, fpgal, calh, call, serialh, seriall)= struct.unpack("!8B",r)

        self.log ("CPU: %d.%d, FPGA: %d.%d, CAL: %d.%d, SERIAL: %d.%d" %
                  (cpuh, cpul, fpgah, fpgal, calh, call, serialh, seriall))
        
        self.endTestLog("GET_SW_REV tests")

    # --------------------------------------------------
    def getControllerData(self):
        '''
        Verify the GET_CTRL_* monitor points.
        '''
        self.startTestLog("GET_CTRL_* tests")
        self.log("\n")
        printStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CTRL_12CURR'][0])
        (current12V,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CTRL_6CURR'][0])
        (current6V,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CTRL_M6CURR'][0])
        (currentM6V,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CTRL_12VOLT'][0])
        (volt12V,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CTRL_6VOLT'][0])
        (volt6V,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CTRL_M6VOLT'][0])
        (voltM6V,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CTRL_NTC'][0])
        (ntc,) = struct.unpack("!H",r)

        self.log ("\nPower supply: %.2fV/%.3fA, %.2fV/%.3fA, %.2fV/%.3fA, NTC = %d" %
                  (volt12V, current12V, volt6V, current6V, voltM6V, currentM6V, ntc))
        
        self.endTestLog("GET_CTRL_* tests")

    # --------------------------------------------------
    def getTP(self):
        '''
        Verify the GET_TP_* monitor points.
        '''
        self.startTestLog("GET_TP_* tests")
        self.log("\n")
        printStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_TP_TEMP'][0])
        (temp, tempSet) = struct.unpack("!2f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_TP_PWM'][0])
        (pwm,) = struct.unpack("!B",r)

        self.log ("\nTP temperature: %.3f (set), %.3f (current) degrees K; pwm = %d" %
                  (tempSet, temp, pwm))
        
        self.endTestLog("GET_TP_* tests")

    # --------------------------------------------------
    def getBE(self):
        '''
        Verify the GET_BE_* monitor points.
        '''
        self.startTestLog("GET_BE_* tests")
        self.log("\n")
        printStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_TEMP'][0])
        (temp, tempSet) = struct.unpack("!2f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_PWM'][0])
        (pwm,) = struct.unpack("!B",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_NTC'][0])
        (ntc,) = struct.unpack("!H",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BIAS0'][0])
        (bias0,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BIAS1'][0])
        (bias1,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BIAS2'][0])
        (bias2,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BIAS3'][0])
        (bias3,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BW0'][0])
        (bw0,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BW1'][0])
        (bw1,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BW2'][0])
        (bw2,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_BE_BW3'][0])
        (bw3,) = struct.unpack("!f",r)

        self.log ("\nBE: temperature %.3f (set), %.3f (current) degrees K; pwm %d; ntc %d" %
                  (tempSet, temp, pwm, ntc))
        self.log ("BE: bias %.3f/%.3f/%.3f/%.3f" % (bias0, bias1, bias2, bias3))
        self.log ("BE: bandwidth %.3f/%.3f/%.3f/%.3f MHz" % (bw0, bw1, bw2, bw3))
        
        self.endTestLog("GET_BE_* tests")

    # --------------------------------------------------
    def getCS(self):
        '''
        Verify the GET_CS_* monitor points.
        '''
        self.startTestLog("GET_CS_* tests")
        self.log("\n")
        printStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CS_TEMP'][0])
        (temp, tempSet) = struct.unpack("!2f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CS_PWM'][0])
        (pwm,) = struct.unpack("!B",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_CS_NTC'][0])
        (ntc,) = struct.unpack("!H",r)

        self.log ("TP temperature: %.3f (set), %.3f (current) degrees K; pwm = %d, ntc = %d" %
                  (tempSet, temp, pwm, ntc))
        
        
        self.endTestLog("GET_CS_* tests")

    # --------------------------------------------------
    def getLoBias(self):
        '''
        Verify the GET_LO_BIAS* monitor points.
        '''
        self.startTestLog("GET_LO_BIAS* tests")
        self.log("\n")
        printStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_LO_BIAS0'][0])
        (bias0,) = struct.unpack("!f",r)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_LO_BIAS1'][0])
        (bias1,) = struct.unpack("!f",r)

        self.log ("LO bias settings: %.3fV/%.3fV" % (bias0, bias1))
        
        self.endTestLog("GET_LO_BIAS* tests")

    # --------------------------------------------------
    def getLnaTemp(self):
        '''
        Verify the GET_LNA_TEMP monitor point.
        '''
        self.startTestLog("GET_LNA_TEMP tests")
        self.log("\n")
        printStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_LNA_TEMP'][0])
        (temp,) = struct.unpack("!f",r)

        self.log ("LNA temperature: %.3f degrees K" % (temp,))
        
        self.endTestLog("GET_LNA_TEMP tests")

    # --------------------------------------------------
    def monitorPoints(self):
        '''
        This test verifies all the other monitor points that are not tested elsewhere.
        '''
        self.startTestLog("Other Monitor Points tests")

        # Do a SET_WVR_STATE to configuration mode, as some of these monitor points
        # should only be retrieved in this mode.
        #!# Note that you can request a monitor in any mode, but if it is requested
        #!# in an "unsupported" mode, you'll get values which are not up-to-date
        #!# (i.e. the values at the time the mode was switched).
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetLoFreq - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep(0.050)
                
        printStatus(self)
            
        # testProperty2(compRef, rca, testName, propName, propLen, format, minVal1, maxVal1, minVal2, maxVal2)
        testProperty2(self, self.monitor['GET_INT_TSRC0'][0], 'GET_INT_TSRCi', 'TSRC0', 8, '!2f', 0., 3000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_TSRC1'][0], 'GET_INT_TSRCi', 'TSRC1', 8, '!2f', 0., 3000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_TSRC2'][0], 'GET_INT_TSRCi', 'TSRC2', 8, '!2f', 0., 3000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_TSRC3'][0], 'GET_INT_TSRCi', 'TSRC3', 8, '!2f', 0., 3000., 0., 1.e38)

        # 1st value: counts/K (estimated gain factor)
        # 2nd value: time-stamp
        testProperty2(self, self.monitor['GET_INT_EST0'][0], 'GET_INT_ESTi', 'EST0', 8, '!2f', 0., 3000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_EST1'][0], 'GET_INT_ESTi', 'EST1', 8, '!2f', 0., 3000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_EST2'][0], 'GET_INT_ESTi', 'EST2', 8, '!2f', 0., 3000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_EST3'][0], 'GET_INT_ESTi', 'EST3', 8, '!2f', 0., 3000., 0., 1.e38)
        
        # 1st value: counts
        # 2nd value: time-stamp
        testProperty2(self, self.monitor['GET_INT_HOT0'][0], 'GET_INT_HOTi', 'HOT0', 8, '!2f', 0., 99999., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_HOT1'][0], 'GET_INT_HOTi', 'HOT1', 8, '!2f', 0., 99999., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_HOT2'][0], 'GET_INT_HOTi', 'HOT2', 8, '!2f', 0., 99999., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_HOT3'][0], 'GET_INT_HOTi', 'HOT3', 8, '!2f', 0., 99999., 0., 1.e38)

        # 1st value: counts
        # 2nd value: time-stamp
        testProperty2(self, self.monitor['GET_INT_COLD0'][0], 'GET_INT_COLDi', 'COLD0', 8, '!2f', 0., 300000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_COLD1'][0], 'GET_INT_COLDi', 'COLD1', 8, '!2f', 0., 300000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_COLD2'][0], 'GET_INT_COLDi', 'COLD2', 8, '!2f', 0., 300000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_COLD3'][0], 'GET_INT_COLDi', 'COLD3', 8, '!2f', 0., 300000., 0., 1.e38)
        
        # 1st value: counts
        # 2nd value: time-stamp
        testProperty2(self, self.monitor['GET_INT_SKYA0'][0], 'GET_INT_SKYAi', 'SKYA0', 8, '!2f', 0., 99999., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_SKYA1'][0], 'GET_INT_SKYAi', 'SKYA1', 8, '!2f', 0., 99999., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_SKYA2'][0], 'GET_INT_SKYAi', 'SKYA2', 8, '!2f', 0., 99999., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_SKYA3'][0], 'GET_INT_SKYAi', 'SKYA3', 8, '!2f', 0., 99999., 0., 1.e38)
        
        # 1st value: counts
        # 2nd value: time-stamp
        testProperty2(self, self.monitor['GET_INT_SKYB0'][0], 'GET_INT_SKYBi', 'SKYB0', 8, '!2f', 0., 300000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_SKYB1'][0], 'GET_INT_SKYBi', 'SKYB1', 8, '!2f', 0., 300000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_SKYB2'][0], 'GET_INT_SKYBi', 'SKYB2', 8, '!2f', 0., 300000., 0., 1.e38)
        testProperty2(self, self.monitor['GET_INT_SKYB3'][0], 'GET_INT_SKYBi', 'SKYB3', 8, '!2f', 0., 300000., 0., 1.e38)
        # 1st and only value: time-counter
        testProperty(self, self.monitor['GET_INT_TIMEH'][0], 'GET_INT_TIMEH', 'TIMEH', 4,  '!I',  0,  1536000)
        testProperty(self, self.monitor['GET_INT_TIMEC'][0], 'GET_INT_TIMEC', 'TIMEC', 4,  '!I',  0,  1536000)
        testProperty(self, self.monitor['GET_INT_TIMEA'][0], 'GET_INT_TIMEA', 'TIMEA', 4,  '!I',  0,  1536000)
        testProperty(self, self.monitor['GET_INT_TIMEB'][0], 'GET_INT_TIMEB', 'TIMEB', 4,  '!I',  0,  1536000)

        self.endTestLog("Other Monitor Points tests")

    # --------------------------------------------------
    def resetDevice(self):
        '''
        This test verifies the RESET_DEVICE control point
        '''
        self.startTestLog("RESET_DEVICE test")
        t = self.manager.command(self.channel, self.node, self.rcaRESET_DEVICE,
                                 struct.pack("!B", 1))
        # A WVR_STATE request before the controller is properly booted will return rubbish
        timeToSleep = 30.
        self.log ("Will sleep now for %d seconds to let the controller finish booting." % (timeToSleep))
        sleep (timeToSleep)
        printStatus(self)

        self.endTestLog("RESET_DEVICE test")

        
class Interactive(Automated):
    def __init__(self, methodName="runTest"):
        Automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg
        # In case the output is re-directed to a file (e.g. by tat), these prints
        # may get intermixed with the output from pyunit, due to buffering. To
        # avoid that, do a flush.
        sys.stdout.flush()

    def getConfirmationOrWait(self, prompt, errStr):
        '''
        Prompt and wait for acknowledge.
        This method is intended to be used within methods that move motors
        to different position, where each new position must be verified
        (in this case the test will wait for an acknowledge via the keyboard).
        '''
        rv = raw_input("\n" + prompt + "Press <Enter> to proceed or <a> to abort: ")
        self.failIf (len(rv) != 0 and rv == "abort"[0:len(rv)], errStr)

    ########################################################
    # Test Definitions
    def monitorTiming(self):
        '''
        This test does a CAN monitor at each of the monitor points in
        the ICD.  The timing from the end of the request to the beginning of
        the response should be measured using an oscilloscope to verify that
        the delay is less than 150 us.
        '''
        self.startTestLog("Monitor Timing test")
        self.log('''Connect CAN_L and CAN_H signals (AMB D-sub connector pins 2 resp 7) to
oscilloscope and set for single trigger.  Time base should be approximatly
100 microseconds.
           
Press <enter> to generate monitor request,
a[utomatic] to generate 1 monitor request per second and then process next,
n[ext] to process next monitor point,
e[xit] to exit.
                 ''')

        rv = ''
        # The keys() method returns an *unsorted* list!
        for name in sorted(self.monitor.keys()):
            rca = self.monitor[name][0]
            if len(rv) == 0 or rv != "automatic"[0:len(rv)] :
                rv = raw_input("Ready for %s (RCA 0x%.2x). Press <Enter> to begin ('e' to exit, ...) " %
                               (name, rca))
             
            while len(rv) == 0 or (rv != "next"[0:len(rv)] and rv != "exit"[0:len(rv)]):
                try:
                    (r,t) = self.manager.monitor(self.channel, self.node, rca)
                    self.failUnless(len(r)==self.monitor[rca],
                                    "Length of response incorrect for %s (RCA 0x%.2x)."
                                    % (name, rca))
                    if len(rv) != 0 and rv == "automatic"[0:len(rv)] :
                        self.log("CAN request sent for %s (RCA 0x%.2x)" % (name, rca))
                        sleep(1.0)
                        break
                    else:
                        rv = raw_input("CAN request sent to RCA: 0x%.2x. Press <Enter> to continue ('e' to exit, ...) " % rca)
                        
                except ControlExceptions.CAMBErrorEx:
                    self.fail("Exception generated monitoring %s (RCA 0x%.2x) " % (name, rca))
                    
            if rv == "exit"[0:len(rv)]:
                break
            
        self.endTestLog("Test 2: Monitor Timing Test")

    # --------------------------------------------------
    def ambReset(self):
        '''
        This test toggles the reset wires.  It should be verified that the
        reset pulse is detected and the AMBSI resets.  In addition
        the levels on these lines should be checked using an oscilliscope
        '''
        self.startTestLog("AMB Reset test")
        self.log('''Connect Reset wires (AMB D-sub connector pins 1 and 6) to oscilliscope and
set for single trigger.  Time base should be approximatly 100 us.

                 ''')

        rv = raw_input("Press <Enter> to generate reset request, type e[xit] to exit ")
        while  len(rv) == 0 or rv != "exit"[0:len(rv)]:
            t = self.manager.reset(self.channel)
            rv=raw_input("Reset Generated ")

        self.endTestLog("AMB Reset test")

    # --------------------------------------------------
    def commandLength(self):
        '''
        Verifies that the device only responds to commands of the
        correct length.
        '''
        self.startTestLog("Ill formed command test")

        # Do a SET_WVR_STATE to configuration mode
        modeName = 'configuration'
        mode = modes[modeName]
        # reset tripped alarms and timestamp counter
        data = 0x03   
        self.log("\nsetIntSets - setting WVR to state %d/'%s'" % (mode,modeName))
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                 struct.pack("!2B", mode, data))
        sleep (0.050)
                
        printStatus(self)
        
        for command in self.command.keys():
            rcaCommand = self.command[command][0]
            commandLen =self.command[command][1]
            self.log("\ncommand %s (rca 0x%.4x, %d bytes)"
                     % (command, rcaCommand, commandLen))
            
            bytes = [0xFF]
            self.log (".............................")
            while len(bytes) <= 8:
                if len(bytes) != commandLen:
                    self.log("sending %d bytes to RCA 0x%.4x" % (len(bytes), rcaCommand))
                    t = self.manager.command(self.channel, self.node, rcaCommand,
                                             struct.pack("!%dB"%len(bytes),*bytes))
                    sleep (0.050)

                    status = getStatus(self)
                    # alarm should be raised, as this was a request with wrong nr of bytes
                    if status['alarm'] == True:
                        alarmFound = False
                        for alarm in status['alarmMsgs']:
                            if alarm.find('control point issued with wrong nr of bytes') != -1:
                                alarmFound = True
                                break
                        if alarmFound:
                            self.log("proper alarm bit set for %s with %d bytes" % (command, len(bytes)))    
                            # this triggered a proper alarm - reset it
                            pass
                        else:
                            self.fail("alarm set for %s with %d bytes, but not flagging wrong nr of bytes??"
                                      % (command, len(bytes)))    

                        # reset alarm
                        data = 0x02   
                        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                                      struct.pack("!2B", mode, data))
                    else:
                        self.fail("no alarm set for %s with %d bytess??" % (command, len(bytes)))
                bytes.append(0xFF)
            
        self.endTestLog("Ill formed command test")

    # --------------------------------------------------
    def commandMode(self):
        '''
        Verifies that the device sets an alarm when sent in an improper mode.
        '''
        self.startTestLog("Command mode test")

        for mode in range(0,3) :
            modeName = modeNrToName[mode]
                
            status = getStatus(self)
            startTime = time()
            if modeName == 'operational':
                if not status['ready']:
                    self.log("WVR not ready for operational mode!! Skipping this mode")
                    continue
            # reset tripped alarms and timestamp counter
            data = 0x03
            self.log("\ncommandWrongMode - setting WVR to state %d/'%s'" % (mode,modeName))
            t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                     struct.pack("!2B", mode, data))
            
            while status['mode'] != mode :
                elapsedTime = time() - startTime
                if elapsedTime > 10 :
                    printStatus(self)
                    self.fail ("'%s' mode not reached within 10s" % modeName)
                sleep(0.100)
                status = getStatus(self)
                
            ##printStatus(self)
        
            for command in self.command.keys():
                rcaCommand = self.command[command][0]
                commandLen = self.command[command][1]
                allowed = self.command[command][2+mode]
                if allowed:
                    ##self.log("\ncommand %s (rca 0x%.4x, %d bytes) allowed in %s mode"
                    ##         % (command, rcaCommand, commandLen, modeName))
                    continue
                
                ##self.log("\ncommand %s (rca 0x%.4x, %d bytes) not allowed in %s mode"
                ##         % (command, rcaCommand, commandLen, modeName))
                
                bytes = []
                while len(bytes) != commandLen:
                    bytes.append(0xFF)
                    
                t = self.manager.command(self.channel, self.node, rcaCommand,
                                         struct.pack("!%dB"% commandLen,*bytes))
                sleep (0.050)
                        
                status = getStatus(self)
                # alarm should be raised, as this was a request in wrong mode
                if status['alarm'] == True:
                    alarmFound = False
                    for alarm in status['alarmMsgs']:
                        if alarm.find('control point issued in improper mode') != -1:
                            alarmFound = True
                            break
                    if alarmFound:
                        ##self.log("proper alarm bit set for %s in %s mode" % (command, modeName))    
                        pass
                    else:
                        self.fail("alarm set for %s, but not flagging improper mode??"
                                  % (command)) 
                        
                    # reset alarm
                    data = 0x02   
                    t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE, 
                                             struct.pack("!2B", mode, data))
                else:
                    self.fail("no alarm set for %s in %s mode??" % (command, modeName))
            
        self.endTestLog("Command mode test")

    # --------------------------------------------------
    def unknownMonitors(self):
        '''
        This test monitors all RCAs between 1 and 0x3FFFF (the full
        range allocated to each device).  It ensures that only
        monitors documented in the ICD respond, and that the response
        is valid (correct number of bytes).
        '''
        self.startTestLog("Monitor Survey test")
        self.log('''The test can be interrupted by typing ^C''')
        errorList = {}
        ##for RCA in range (0x1,0x3FFFF +1):
        for RCA in range (0x00001, 0x3FFFF + 1):
            try:
                if (RCA % 0x1000) == 0:
                    self.log("Sending monitor request 0x%.5x" % RCA)

                (r,t) = self.manager.monitor(self.channel, self.node, RCA)
                if self.monitorRCAs.has_key(RCA) :
                    if len(r) != self.monitorRCAs[RCA][0]:
                        #self.log("expected %d, got %d bytes" % (self.monitorRCAs[RCA][0], len(r)))
                        errorList[RCA]=len(r)
                else :
                    #self.log("expected no reply, got %d bytes" % len(r))
                    errorList[RCA]=len(r)
                        
            except KeyboardInterrupt, e:
                break
            except Exception, e:
                #print("---> exception " + str(e))
                pass
                                
            # If introducing a sleep, the likelihood of seeing a Ctrl-C
            # becomes higher (at least in simulation).
            #!# Without this sleep, there are CAMB errors - rate too high?
            #sleep (0.001953125)
            sleep (0.002)
            
        if len(errorList) != 0:
            if len(errorList) > 50:
                self.log("%d offending responses, will list first 50:" % len(errorList))
            else :
                self.log("%d offending responses" % len(errorList))

            count = 1
            # The keys() method returns an *unsorted* list!
            for idx in sorted(errorList.keys()):
                if self.monitorRCAs.has_key(RCA):
                    self.log("RCA 0x%.5x returned %d bytes (%d expected)" % \
                             (idx, errorList[idx], self.monitorRCAs[RCA][0]))
                else:
                    self.log("RCA 0x%.5x returned %d bytes" % \
                             (idx, errorList[idx]))
                count = count + 1
                if count > 50 :
                    break

            self.fail("%d Monitor points not in ICD Responded" %
                      len(errorList))

        self.endTestLog("Monitor Survey test")

    # --------------------------------------------------
    def reboot(self):
        '''
        Verify the booting of the WVR via the SET_WVR_STATE control point,
        and an appropiate response from the GET_WVR_STATE monitor point.
        '''
        self.startTestLog("SET_WVR_STATE boot tests")
        self.log("\n")
        printStatus(self)

        modeName = 'idle'
        mode = modes[modeName]
        
        # set reboot bit
        data = 0x07
        self.log("\nreboot - setting WVR to %s state, rebooting" % modeName)
        self.log("If you're close to the WVR, you'll hear a.o. that the fans will turn off temporarily.")
        t = self.manager.command(self.channel, self.node, self.rcaSET_WVR_STATE,
                                     struct.pack("!2B", mode, data))
        startTime = time()
        # The WVR can of course not respond while it is booting, so don't
        # monitor the state for a while
        #!# ICD should say how long this booting takes - 30s?
        self.log("Giving WVR 30s to boot and re-enable communication - hold on.")
        sleep (30.)
        status = getStatus(self)
        if (status['mode'] != mode) :
            self.fail ("didn't switch to '%s' mode?!" % modeName)
            
        while status['boot'] != mode :
            elapsedTime = time() - startTime
            if elapsedTime > 35. :
                printStatus(self)
                self.fail ("didn't boot within 35s?!")
            sleep(0.100)
            status = getStatus(self)

        #!# Note: even if the WVR was "ready for operational mode" just before
        #!# the reboot, after the reboot it can take a hell of a lot of time
        #!# (20min)? before it is again acknowledging to be "ready for operational mode".
        self.getConfirmationOrWait("Will wait up to 30min till WVR is ready for operational mode.\n",
                                   "User skipped waiting for operational mode readiness.")

        firstPass = True
        while not status['ready'] :
            elapsedTime = time() - startTime
            # report every 20s the hot and cold load temperatures
            if elapsedTime % 20 < 0.1 or firstPass:
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_TEMP'][0])
                (coldTemp, coldTempSet)= struct.unpack("!2f",r)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_TEMP'][0])
                (hotTemp, hotTempSet)= struct.unpack("!2f",r)
                if firstPass:
                    firstPass = False
                    self.log ("\n        temperature set for cold load: %.3fK; hot load: %.3fK\n" %
                              (coldTempSet, hotTempSet))
                self.log("after %3ds: actual temp for cold load: %.3fK; hot load: %.3fK" %
                         (elapsedTime, coldTemp, hotTemp))
            
            if elapsedTime > 1800. :
                printStatus(self)
                self.fail ("didn't get ready for operational mode within 1800s?!")
            sleep(0.100)
            status = getStatus(self)

        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_COLD_TEMP'][0])
        (coldTemp, coldTempSet)= struct.unpack("!2f",r)
        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_HOT_TEMP'][0])
        (hotTemp, hotTempSet)= struct.unpack("!2f",r)
        self.log("after %3ds: actual temp for cold load: %.3fK; hot load: %.3fK" %
                 (elapsedTime, coldTemp, hotTemp))
        self.log ("\ngot ready for operational mode within %ds from sending boot cmd." % elapsedTime)                   

        printStatus(self)
    # --------------------------------------------------


# Define the test suites
# automatedSuite: contains all tests in the class Automated, except the calibration tests
def automatedSuite():
    suite = unittest.TestSuite()
    suite.addTest(Automated("broadcast"))
    suite.addTest(Automated("genericMonitors"))
    suite.addTest(Automated("replyLength"))
    suite.addTest(Automated("nodeId"))
    suite.addTest(Automated("getWvrState"))
    suite.addTest(Automated("getChopState"))
    suite.addTest(Automated("setWvrState"))
    suite.addTest(Automated("setIntSets"))
    suite.addTest(Automated("setChopVel"))
    suite.addTest(Automated("setChopPhase"))
    suite.addTest(Automated("setHotTemp"))
    suite.addTest(Automated("setColdTemp"))
    suite.addTest(Automated("setLoFreq"))
    suite.addTest(Automated("getSwRev"))
    suite.addTest(Automated("getControllerData"))
    suite.addTest(Automated("getTP"))
    suite.addTest(Automated("getBE"))
    suite.addTest(Automated("getCS"))
    suite.addTest(Automated("getLoBias"))
    suite.addTest(Automated("getLnaTemp"))
    suite.addTest(Automated("monitorPoints"))
    suite.addTest(Automated("resetDevice"))
    
    return suite

# tatSuite: subset of automatedSuite, containing tests that do not
# involve motor movements (as this would timeout and fail)
def tatSuite():
    suite = unittest.TestSuite()
    suite.addTest(Automated("broadcast"))
    suite.addTest(Automated("genericMonitors"))
    suite.addTest(Automated("replyLength"))
    suite.addTest(Automated("nodeId"))
    ##suite.addTest(Automated("monitorPoints"))
    suite.addTest(Automated("resetDevice"))
    
    return suite

# interactiveSuite: contains all tests of the Interactive class
def interactiveSuite():
    suite = unittest.TestSuite()
    suite.addTest(Interactive("monitorTiming"))
    suite.addTest(Interactive("ambReset"))
    suite.addTest(Interactive("commandLength"))
    suite.addTest(Interactive("unknownMonitors"))
    suite.addTest(Interactive("reboot"))
    
    return suite

def all():
    return unittest.TestSuite((interactiveSuite(), automatedSuite()))


# **************************************************
# MAIN Program
if __name__ == '__main__':
    # For some reason ACS has problems unless I do this
    client = PySimpleClient.getInstance()

    runstring = sys.argv[0]
    for i in range(1,len(sys.argv)) :
        runstring += " " + sys.argv[i]

    print "Original runstring:\n    " + runstring

    # In case the output is re-directed to a file (e.g. by tat), these prints
    # may get intermixed with the output from pyunit, due to buffering. To
    # avoid that, do a flush.
    sys.stdout.flush()

    # First argument is for unittest (testcase/suite).
    # Try --help for unittest help on arguments.
    if len(sys.argv) == 1:
        # No arguments passed defaults to the "all" test suite.
        # See suites defined above: "automatedSuite", "interactiveSuite",
        # "all" and "checkPositions", or execute individual tests
        # (e.g. "Automated.broadcast")
        sys.argv.append("all")

    # Second argument is antenna name
    if len(sys.argv) == 2:
        sys.argv.append("LA03")

    # Third argument is CAN node number.
    #!# Default should be 0x24 (36 decimal)
    if len(sys.argv) == 3:
        sys.argv.append("%d" % 0x24)

    # Fourth argument is number of loops for some tests
    if len(sys.argv) == 4:
        sys.argv.append("1")

    # Fifth argument is "confirmation delay" for interactive tests
    if len(sys.argv) == 5:
        sys.argv.append("2.0")

    runstring = sys.argv[0]
    for i in range(1,len(sys.argv)) :
        runstring += " " + sys.argv[i]

    print "Converted into:\n    " + runstring + "\n"
    sys.stdout.flush()

    confDelay = float(sys.argv[5])
    sys.argv.remove(sys.argv[5])

    nrLoops = int(sys.argv[4])
    sys.argv.remove(sys.argv[4])
    
    node = int(sys.argv[3])
    sys.argv.remove(sys.argv[3])

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main(defaultTest='all')
