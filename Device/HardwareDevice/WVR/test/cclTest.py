#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

import unittest
from TETimeUtil import *
from CCL.WVR import WVR
from Control import HardwareDevice
from time import *
from math import *

class automated(unittest.TestCase):
    def setUp(self):
        self.wvr =WVR("DV01",stickyFlag=True)
        self.failIf(self.wvr == None, "Failed to activate component")
        self.wvr.hwStop()
        self.failUnlessEqual(self.wvr.getHwState(), HardwareDevice.Stop,
                             "Unable to get WVR to Stop State")
        
        self.wvr.hwStart() 
        self.failUnlessEqual(self.wvr.getHwState(), HardwareDevice.Start,
                             "Unable to get WVR to Start State")
        self.wvr.hwConfigure()
        self.failUnlessEqual(self.wvr.getHwState(), HardwareDevice.Configure,
                             "Unable to get WVR to Configure State")
        self.wvr.hwInitialize()
        self.failUnlessEqual(self.wvr.getHwState(), HardwareDevice.Initialize,
                             "Unable to get WVR to Initialize State")
        self.wvr.hwOperational()
        self.failUnlessEqual(self.wvr.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get WVR to Operational State")
        
    def tearDown(self):
        self.wvr.hwStop() 
        self.failUnlessEqual(self.wvr.getHwState(), HardwareDevice.Stop,
                             "Unable to get WVR to Stop State")
        
        
    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Hardware Lifecycle test.  Move the device to operational and
        Ensure that the device is in operational mode.
        '''
        self.failUnlessEqual(self.wvr.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get WVR to Operational State")
        
    def test02(self):
        '''
        Check that we can properly set WVR to Configuration state
        '''
        # Set WVR Operational state
        self.wvr.SET_WVR_STATE([0,0])
        sleep(0.5)
        self.failUnlessEqual(self.wvr.GET_WVR_STATE_MODE()[0],
                             0,
                             "WVR can not be passed to Operational.")

    def test03(self):
        '''
        Check that we can properly set a TSRC integration time
        '''
        # Set WVR Configuration state
        self.wvr.SET_WVR_STATE([2,0])
        sleep(0.5)
        self.failUnlessEqual(self.wvr.GET_WVR_STATE_MODE()[0],
                             2,
                             "WVR can not be passed to Configuration.")

        # Set integration time and gain smoothing time
        self.wvr.SET_INT_SETS([1.152,115.2])
        self.failIf(abs(self.wvr.GET_INT_TSRC()[0] - 1.152) > 0.00001,
                    "TSRC integration time did not match with required value.")
        self.failIf(abs(self.wvr.GET_INT_EST()[0] - 115.2) > 0.00001,
                    "TSRC integration time did not match with required value.")
        
            
# **************************************************
# MAIN Program
if __name__ == '__main__':
    unittest.main()
        
