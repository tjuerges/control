#!/usr/bin/env python

from Acspy.Nc.Consumer import Consumer
from Acspy.Clients.SimpleClient import PySimpleClient
import Control
import time
import sys

antennaName = None
def wvrDataHandler(WVRData):
    if WVRData.antennaName == antennaName:
        print " AntennaName: %s" % WVRData.antennaName, \
	      " Timestamp: %ld" % WVRData.timestamp, \
              " Interval: %ld" % WVRData.interval, \
	      " Valid: %d" %  WVRData.valid, \
	      " Temp0: %10.6f" % WVRData.temp0, \
	      " Temp1: %10.6f" % WVRData.temp1, \
	      " Temp2: %10.6f" % WVRData.temp2, \
              " Temp3: %10.6f" % WVRData.temp3
    return
               

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print "antenna name must be specified."
        print "Use: WVREventClient.py <antennaName>"
        sys.exit(1)
        
    antennaName = sys.argv[1]
    
    c = Consumer(Control.CHANNELNAME_CONTROLREALTIME)
    c.addSubscription(Control.WVRData, wvrDataHandler)
    c.consumerReady()
    
    print "Connected to Notification Channel, press ENTER to quit client..."
    sys.stdin.readline()
    c.disconnect()

