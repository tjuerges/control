#
# Remove ISO time-stamps by symbolic value
s/[0-9][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]T[0-5][0-9]:[0-5][0-9]:[0-5][0-9][.0-9]*/<timestamp>/g
#
#
# Replace number of CAN transactions by generic number
s/Reply on GET_TRANS_NUM (0x30002): [0-9]*/Reply on GET_TRANS_NUM (0x30002): <#transactions>/g
#
# Replace timing on execution of tests
s/Ran 6 tests in [0-9.]*s/Ran 6 tests in <#seconds>/g
#
# Replace pathname for local file logger by generic pathname
s/Local file logger: Cache saved to .*/Local file logger: Cache saved to '<pathName>: <PID>_<PID>'./g