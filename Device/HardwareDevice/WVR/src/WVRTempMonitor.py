#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) UNSPECIFIED - FILL IN, 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#

from Acspy.Common.TimeHelper import *
from time import sleep
from math import *
import sys
import Acspy.Common.EpochHelper
import acstime
import TETimeUtil
from CCL.WVR import *
import optparse


def getData(fd, wvr):
    #Compute time
    ts = getTimeStamp()
    time_helper = Acspy.Common.EpochHelper.EpochHelper(ts)
    current_time = "%s" %(time_helper.toString(acstime.TSArray, "%Y-%m-%dT%H:%M:%S.%3q", 0, 0))

    #Reading data
    try:
        tsrc0 = wvr.GET_INT_TSRC0_TEMP()[0]
        tsrc1 = wvr.GET_INT_TSRC1_TEMP()[0]
        tsrc2 = wvr.GET_INT_TSRC2_TEMP()[0]
        tsrc3 = wvr.GET_INT_TSRC3_TEMP()[0]
        hotTmp = wvr.GET_HOT_TEMP_ACTUAL()[0]
        coldTmp = wvr.GET_COLD_TEMP_ACTUAL()[0]
        stMode = wvr.GET_WVR_STATE_MODE()[0]
        stOp =  wvr.GET_WVR_STATE_OPERATIONAL()[0]
        stLock = wvr.GET_WVR_STATE_LO_LOCKED()[0]
        stAlarms = wvr.GET_WVR_STATE_ALARMS()[0]
        stBoot = wvr.GET_WVR_STATE_BOOTED()[0]
        stClock = wvr.GET_WVR_STATE_CLOCK_PRESENT()[0]
        stTe = wvr.GET_WVR_STATE_TE_PRESENT()[0]
        
        fd.write("%s\t %0.3f\t %0.3f\t %0.3f\t %0.3f\t %0.3f\t %0.3f\t %d\t %s\t %s\t %s\t %s\t %s\t %s\t\n"  %
                 (current_time, tsrc0, tsrc1, tsrc2, tsrc3, hotTmp, coldTmp, stMode, stOp, stLock, stAlarms, stBoot, stClock, stTe))
    except:
        fd.write("%s\t DATA NOT AVAILABLE\t\n" % current_time)
        

def isOperational(wvr):  
    try:
        if wvr.GET_WVR_STATE_MODE()[0] != 0:
                response = False
        else:
            response = True
    except:
        response = False    
    return response


def setTrscIntTime(wvr, intTime, gainEst):
    ### Set WVR into Configuration Mode
    try:
        wvr.SET_WVR_STATE([2,2])
        sleep(0.5);
        if wvr.GET_WVR_STATE_MODE()[0] != 2:
            print "# ERROR: WVR can not be set to Configuration Mode"
            sys.exit()
    except:
        "#ERROR: WVR can not be set to Configuration Mode"
        sys.exit()

    ### Set Frequenxy
    try:
        wvr.SET_INT_SETS([intTime,gainEst])
        sleep(0.5)
        if abs(wvr.GET_INT_TSRC()[0] - intTime) > 0.001 or abs(wvr.GET_INT_EST()[0] - gainEst) > 0.001:
            print "# ERROR: WVR could not set integration time"
            sys.exit()
    except:
        "#ERROR: WVR could not set integration time"
        sys.exit()

    ### Set WVR into Operational Mode
    try:
        wvr.SET_WVR_STATE([0,2])
        sleep(0.5)
        if wvr.GET_WVR_STATE_MODE()[0] != 0:
            print "# ERROR: WVR can not be set to Operational Mode"
            sys.exit()
    except:
        "#ERROR: WVR can not be set to Operational Mode"
        sys.exit()  

#
# Main
#
if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option("-a", "--antenna", dest="antenna", type = "string",  default = None,
                      help="Select the Antenna where WVR is installed, ie DV01, LA01")
    parser.add_option("-s", "--sampling", dest="samplTime", type = "float",  default = "0.8",
                      help="Sampling time between every read (Default= 0.8 sec.)")
    parser.add_option("-i", "--inttime", dest="intTime", type = "float",  default = "0.96",
                      help="Tsrc integration time (Default= 0.96 sec.)")
    parser.add_option("-g", "--gainest", dest="gainEst", type = "float",  default = "96",
                      help="Gain estimate smoothing (Default= 96 sec.)")
    parser.add_option("-d", "--duration", dest="duration", type = "int",  default = "1000",
                      help="Duration of monitoring (Default= 1000 sec.)")
    (options, args) = parser.parse_args()
    
    # Validate parameters
    if options.antenna == None:
        print "The -a is a mandatory parameter"
        sys.exit()
    if options.samplTime < 0.20:
        print "# WARNING: Minimal sampling time must be 0.20s. Setting to this value .."

    ### Prepare file name
    ts = getTimeStamp()
    time_helper = Acspy.Common.EpochHelper.EpochHelper(ts)
    current_time = "%s" %(time_helper.toString(acstime.TSArray, "%Y-%m-%dT%H:%M:%S.%3q", 0, 0))
    fileName = "WVRTemps_" + options.antenna + "_"+ current_time + ".txt"

    ### Compute monitoring duration
    maxMonTimes = round(options.duration / options.samplTime)
    monTimes = 1

    ### Open file and save header
    try:
        fd = file(fileName,"w+")
        fd.write("Timestamp\t||TSRC0\t\tTSCR1\t\tTSCR2\t\tTSCR3\t\tHOT_TEMP\t\tCOLD_TEMP\t||MODE\tOPERATIONAL\tLOCKED\tALARMS\tBOOTED\tCLOCK\tTE ||\t\n")
    except:
        print "#ERROR: Failed opening file.. Exited"
        sys.exit()

    ### Getting WVR reference
    try:
        wvr = WVR(options.antenna)
    except:
        "#ERROR: Failed getting WVR reference.. Exited"
        sys.exit()

    ### Set integration time
    setTrscIntTime(wvr, options.intTime, options.gainEst)
        
    # Output to the user
    print '# Sampling Time: %0.3f[s]' % options.samplTime
    print '# Integration Time: %0.3f[s]' % options.intTime
    print '# Gain Estimate: %0.3f[s]'  % options.gainEst
    print '# Duration:: %d[s]' % options.duration
    print '# Monitoring data saved at: %s\n' % fileName
    print '# Press <Ctrl>-C to exit at any time.'
    

    # Loop for reading data
    try:
        while True:
            if isOperational(wvr) != True:
                print "# ERROR: WVR is not at Operational Mode"
                break
            getData(fd,wvr)
            if monTimes == maxMonTimes:
                break
            monTimes += 1
            sleep(options.samplTime)
    except KeyboardInterrupt:
        pass

    #close file and destroy wvr object
    fd.close()
    del(wvr) 
    
