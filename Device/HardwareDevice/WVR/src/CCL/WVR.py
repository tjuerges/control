#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when          what
# --------  ------------  ----------------------------------------------
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for the Water Vapor Radiometric (WVR) device.
"""

import CCL.WVRBase
from CCL import StatusHelper
from CCL.logging import getLogger
from log_audience import OPERATOR
import ACSLog
import os

class WVR(CCL.WVRBase.WVRBase):
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):       
        '''
        The constructor creates a WVR object by making use of
        the WVRBase constructor.
        '''
        
        CCL.WVRBase.WVRBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.WVRBase.WVRBase.__del__(self)

    def setIntegrationGain(self, valueSeq):
        '''
        This method allow to change the integration time
        and gain estimated (both at seconds) of WVR and 
        also will change the event publishing rate to 
        the integration time previously defined. Integration
        time and gain estimated must be a multiple of 0.192s
        EXAMPLE:
        from CCL.WVR import WVR
        wvr = WVR("DV01")
        wvr.setIntegrationGain([1.152, 115.2])
        '''
        for key, val in self._devices.iteritems():
            self._devices[key].setIntegrationGain(valueSeq)          
        
    def STATUS(self):
        '''
        This method will display the status information of the device at
        the standard output.
        '''
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            # List of status elements
            elements = []

            #reading sglobal status monitor point
            try:
                value,t = self._devices[key].GET_WVR_STATE()
            except:
                pass
            
            ############################### General  ###################################################
            elements.append( StatusHelper.Separator("General") )
            
            try:
                value,t = self._devices[key].GET_WVR_STATE_MODE()
                if value == 0:
                    value_t = "Operational"
                elif value == 1:
                    value_t = "Idle"
                elif value == 2:
                    value_t = "Configuration"
                else:
                    value_t = "Not Applicable"
            except:
                value_t = "N/A"
            elements.append(StatusHelper.Line("Mode", [StatusHelper.ValueUnit(value_t)]));

            try:
                value,t = self._devices[key].GET_WVR_STATE_OPERATIONAL()
                if value == True:
                    value_t = "Yes"
                else:
                    value_t = "No"
            except:
                value_t = "N/A"
            elements.append(StatusHelper.Line("Ready for Operational", [StatusHelper.ValueUnit(value_t)]));

            try:
                value,t = self._devices[key].GET_WVR_STATE_ALARMS()
                if value == True:
                    value_t = "Yes"
                else:
                    value_t = "No"
            except:
                value_t = "N/A"
            elements.append(StatusHelper.Line("Alarms", [StatusHelper.ValueUnit(value_t)]));

            try:
                value,t = self._devices[key].GET_WVR_STATE_BOOTED()
                if value == True:
                    value_t = "Yes"
                else:
                    value_t = "No"
            except:
                value_t = "N/A"
            elements.append(StatusHelper.Line("Booted", [StatusHelper.ValueUnit(value_t)]));

            try:
                value,t = self._devices[key].GET_WVR_STATE_CLOCK_PRESENT()
                if value == True:
                    value_t = "Yes"
                else:
                    value_t = "No"
            except:
                value_t = "N/A"
            elements.append(StatusHelper.Line("Clock Present", [StatusHelper.ValueUnit(value_t)]));
            
            try:
                value,t = self._devices[key].GET_WVR_STATE_TE_PRESENT()
                if value == True:
                    value_t = "Yes"
                else:
                    value_t = "No"
            except:
                value_t = "N/A"
            elements.append(StatusHelper.Line("TE Present", [StatusHelper.ValueUnit(value_t)]));


            frame = StatusHelper.Frame(compName + "   Ant: " + antName,elements)
            frame.printScreen()
        
