/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WVRImpl.cpp
 */
#include <WVRImpl.h>
#include <TETimeUtil.h>
#include <acstimeTimeUtil.h>
#include <acstimeEpochHelper.h>
#include <TypeConversion.h>

#include <ControlBasicInterfacesC.h> // for CHANNELNAME_CONTROLREALTIME

using log_audience::OPERATOR;
using ControlExceptions::CAMBErrorExImpl;
using ControlExceptions::CAMBErrorEx;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::INACTErrorEx;
using std::string;
using Control::AlarmSender;
using Control::AlarmInformation;

/**
 *-----------------------
 * WVR Constructor
 *-----------------------
 */
WVRImpl::WVRImpl(const ACE_CString& name, maci::ContainerServices* cs) :
  WVRBase(name,cs),
  MonitorHelper(),
  wvrLastIntegrationTime_m(11520000),
  wvrStartTimeStamp_m(0),
  wvrLastGainEstimated_m(115.2),
  guard(10000000ULL * 10ULL, 0),
  updateThreadCycleTime_m(5 * TETimeUtil::TE_PERIOD_ACS),
  wvrAlarmsInterval_m(104*TETimeUtil::TE_PERIOD_ACS),
  wvrDataInterval_m(11520000)
{
  ACS_TRACE("WVRImpl::WVRImpl");

  alarmSender_p  = NULL;
  wvrPublisher_p = NULL;

  /* Initialize TSRC structure */
  tsrcData_m.temp = 0.0;
  tsrcData_m.timeStamp = 0.0;
}

/**
 *-----------------------
 * WVR Destructor
 *-----------------------
 */
WVRImpl::~WVRImpl()
{
  ACS_TRACE("WVRImpl::~WVRImpl");
}

/**
 *------------------------
 * Component Lifecycle Methods
 *------------------------
 */
void WVRImpl::initialize()
{
  const string fnName = "WVRImpl::initialize";
  ACS_TRACE(fnName);

  wvrPublisher_p = new WVRPublisher(Control::CHANNELNAME_CONTROLREALTIME,this);

  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());

  try {
    WVRBase::initialize();
    MonitorHelper::initialize(compName.in(), getContainerServices());
  } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,
					       fnName.c_str());
  }

  /* Prepare the threads for use */
  const std::string threadName(compName.in() + std::string("UpdateThread"));
  updateThread_p = getContainerServices()->getThreadManager()->
    create<UpdateThread, WVRImpl>(threadName.c_str(), *this);
  updateThread_p->setSleepTime(updateThreadCycleTime_m);

}

void WVRImpl::cleanUp()
{
  string fnName = "WVRImpl::cleanUp";
  ACS_TRACE(fnName);
  updateThread_p->terminate();

  // Disconnect data publication
  if (wvrPublisher_p != NULL) {
    wvrPublisher_p->disconnect();
    wvrPublisher_p = NULL;
  }

  try {
    MonitorHelper::cleanUp();
    WVRBase::cleanUp();
  } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,
					       fnName.c_str());
  }

  /* Clear all the alarms */
  //disable alarms since we will shutdown.
  if (alarmSender_p != NULL) {
    alarmSender_p->disableAlarms();
    alarmSender_p->forceTerminateAllAlarms();
    delete alarmSender_p;
    alarmSender_p = NULL;
  }
}

void WVRImpl::aboutToAbort()
{
  string fnName = "WVRImpl::aboutToAbort";
  ACS_TRACE(fnName);

  // Disconnect data publication
  if (wvrPublisher_p != NULL) {
    wvrPublisher_p->disconnect();
    wvrPublisher_p = NULL;
  }

  /* Clear all the alarms */
  //disable alarms since we will shutdown.
  if (alarmSender_p != NULL) {
    alarmSender_p->disableAlarms();
    alarmSender_p->forceTerminateAllAlarms();
    delete alarmSender_p;
    alarmSender_p = NULL;
  }
}

/* ---------------- Hardware Lifecycle Methods -------------- */
void WVRImpl::hwInitializeAction() {
  const string fnName = "WVRImpl::hwInitializeAction";
  ACS_TRACE(fnName);

  WVRBase::hwInitializeAction();

  alarmSender_p = new AlarmSender();
  wvrDataQueue_m.clear();

  MonitorHelper::resume();
  updateThread_p ->suspend();

  /* intialize alarms for the sender */
  initializeAlarms();
  /* Clear all the alarms */
  alarmSender_p->forceTerminateAllAlarms();
}


void WVRImpl::hwOperationalAction() {
  const string fnName ="WVRImpl::hwOperationalAction";
  ACS_TRACE(fnName);

  WVRBase::hwOperationalAction();

  acstime::Epoch startTime = TETimeUtil::unix2epoch(time(0));
  lastWVRAlarmsTime_m   = TETimeUtil::rtMonitorTime(startTime,5).value;

  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
  antennaName_m = HardwareDeviceImpl::componentToAntennaName(compName.in());
  Control::FloatSeq wvrInt;
  wvrInt.length(2);

  /* Set default integration time and gain estimated */
  wvrInt[0] = 1.15200;
  wvrInt[1] = 115.2;
  setIntegrationGain(wvrInt);

  updateThread_p->resume();
}

void WVRImpl::hwStopAction(){
  const string fnName = "WVRImpl::hwStopAction";
  ACS_TRACE(fnName);

  AmbErrorCode_t flushStatus;
  ACS::Time      flushTime;

  /* Suspend the threads */
  updateThread_p->suspend();

  /* Flush all commands */
  flushNode(0, &flushTime, &flushStatus);
  if (flushStatus != AMBERR_NOERR) {
    ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
            (LM_INFO,"Communication failure flushing commands to device"));
  } else {
    ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
            (LM_DEBUG,"All commands and monitors flushed at: %lld",flushTime));
  }

  MonitorHelper::suspend();

  //disable alarms since we will shutdown.
  if (alarmSender_p != NULL) {
    alarmSender_p->disableAlarms();
    alarmSender_p->forceTerminateAllAlarms();
    delete alarmSender_p;
    alarmSender_p = NULL;
  }

  WVRBase::hwStopAction();
}

/* ================ Monitoring Dispatch Method ================ */
void WVRImpl::processRequestResponse(const AMBRequestStruct& response){
  const string fnName = "WVRImpl::processRequestResponse";

  if (response.Status == AMBERR_FLUSHED) {
    /* Nothing to do, just return */
    return;
  }

  if (response.RCA == getMonitorRCAWvrAlarms()) {
    /* Response from WVR Alarms */
    processWVRAlarmsMon(response);
    return;
  }

  if (response.RCA == getMonitorRCAIntTsrc0() ||
      response.RCA == getMonitorRCAIntTsrc1() ||
      response.RCA == getMonitorRCAIntTsrc2() ||
      response.RCA == getMonitorRCAIntTsrc3()) {
    /* Response from WVR INT TSRCX  */
    processWVRData(response);
    return;
  }
}

/* ================ WVR Alarm Method ======================*/

void WVRImpl::sendWVRAlarm(bool isActive, int alarmType)
{
  const string fnName = "WVRImpl::sendWVRAlarm";
  ACS_TRACE(fnName);

  if (isActive)
    alarmSender_p->activateAlarm(alarmType);
  else
    alarmSender_p->deactivateAlarm(alarmType);
}

void WVRImpl::initializeAlarms()
{
  const string fnName = "WVRImpl::initializeAlarms";
  AUTO_TRACE(fnName);

  std::vector<AlarmInformation> aivector;
  for(unsigned int idx=RailOff12V; idx<= LOError; idx++) {
    AlarmInformation ai;
    ai.alarmCode = idx;
    aivector.push_back(ai);
  }

  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
  alarmSender_p->initializeAlarms("WVR", compName.in(), aivector);
}

/* ================ Hardware Queueing Methods  ================= */

void WVRImpl::queueWVRAlarms(){
  MonitorHelper::AMBRequestStruct* monReq;


  while (TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value +
	 (updateThreadCycleTime_m * 2) > lastWVRAlarmsTime_m) {
    lastWVRAlarmsTime_m += wvrAlarmsInterval_m;

    monReq = getRequestStruct();

    monReq->RCA        = getMonitorRCAWvrAlarms();
    monReq->TargetTime = lastWVRAlarmsTime_m;

    monitorTE(monReq->TargetTime,
	      monReq->RCA,
	      monReq->DataLength,
	      monReq->Data,
	      monReq->SynchLock,
	      &monReq->Timestamp,
	      &monReq->Status);

    queueRequest(monReq);
  }
}

void WVRImpl::queueWVRData(){
  MonitorHelper::AMBRequestStruct* monReq0;
  MonitorHelper::AMBRequestStruct* monReq1;
  MonitorHelper::AMBRequestStruct* monReq2;
  MonitorHelper::AMBRequestStruct* monReq3;

  while (TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value +
	 (updateThreadCycleTime_m * 2) > lastWVRDataTime_m) {
    lastWVRDataTime_m += wvrDataInterval_m;

    monReq0 = getRequestStruct();
    monReq0->RCA        = getMonitorRCAIntTsrc0();
    monReq0->TargetTime = lastWVRDataTime_m;
    monitorTE(monReq0->TargetTime,
	      monReq0->RCA,
	      monReq0->DataLength,
	      monReq0->Data,
	      monReq0->SynchLock,
	      &monReq0->Timestamp,
	      &monReq0->Status);
    queueRequest(monReq0);

    monReq1 = getRequestStruct();
    monReq1->RCA        = getMonitorRCAIntTsrc1();
    monReq1->TargetTime = lastWVRDataTime_m;
    monitorTE(monReq1->TargetTime,
	      monReq1->RCA,
	      monReq1->DataLength,
	      monReq1->Data,
	      monReq1->SynchLock,
	      &monReq1->Timestamp,
	      &monReq1->Status);
    queueRequest(monReq1);

    monReq2 = getRequestStruct();
    monReq2->RCA        = getMonitorRCAIntTsrc2();
    monReq2->TargetTime = lastWVRDataTime_m;
    monitorTE(monReq2->TargetTime,
	      monReq2->RCA,
	      monReq2->DataLength,
	      monReq2->Data,
	      monReq2->SynchLock,
	      &monReq2->Timestamp,
	      &monReq2->Status);
    queueRequest(monReq2);

    monReq3 = getRequestStruct();
    monReq3->RCA        = getMonitorRCAIntTsrc3();
    monReq3->TargetTime = lastWVRDataTime_m;
    monitorTE(monReq3->TargetTime,
	      monReq3->RCA,
	      monReq3->DataLength,
	      monReq3->Data,
	      monReq3->SynchLock,
	      &monReq3->Timestamp,
	      &monReq3->Status);
    queueRequest(monReq3);
  }
}



/* ================ Hardware Processing Methods =================*/
void WVRImpl::processWVRAlarmsMon(const MonitorHelper::AMBRequestStruct& response){
  const string fnName = "WVRImpl::processWVRAlarmsMonitor";

   /* Check Status */
   if (response.Status != AMBERR_NOERR) {
     if(guard.checkAndIncrement() == true)
       ACS_LOG(LM_SOURCE_INFO,fnName.c_str(),
 	      (LM_WARNING,"WVR Alarms Monitor Failed  (Status = %d)",
 	       response.Status));
     return;
   }

   /* Check Time */
   if (response.Timestamp - response.TargetTime > TETimeUtil::TE_PERIOD_ACS/2){
     if(guard.checkAndIncrement() == true)
       ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
	       (LM_DEBUG,"WVR Alarms Monitor at incorrect time ( %f s off)",
		(response.Timestamp - response.TargetTime) * 1E-7));
   }

   // Alarms activation.
   // Alarm vector lenghts are taken from ICD.

   int alarmCode = RailOff12V;
   int alarmSet = 0;

   //Miscellaneous alarms
   for (unsigned int idx = 0; idx < 5; idx++)
     {
       alarmSet = response.Data[0]&0x01;
       //Monitor and No Lock alarms has been removed at ICD rev E. So, they will be always disabled
       if (idx == 1 || idx == 4)
	 alarmSet = 0;

       if (alarmSet)
	 sendWVRAlarm(true, alarmCode);
       else
	 sendWVRAlarm(false, alarmCode);
       alarmCode++;
     }

   //Over current alarms
   for (unsigned int idx = 0; idx < 4; idx++)
     {
       alarmSet = (response.Data[1]>>idx)&0x01;
       if (alarmSet)
	 sendWVRAlarm(true, alarmCode);
       else
	 sendWVRAlarm(false, alarmCode);
       alarmCode++;
     }

   //Over voltage alarms
   for (unsigned int idx = 0; idx < 3; idx++)
     {
       alarmSet = (response.Data[2]>>idx)&0x01;
       if (alarmSet)
	 sendWVRAlarm(true, alarmCode);
       else
	 sendWVRAlarm(false, alarmCode);
       alarmCode++;
     }

   //Over temperatures alarms
   for (unsigned int idx = 0; idx < 5; idx++)
     {
       alarmSet = (response.Data[3]>>idx)&0x01;
       if (alarmSet)
	 sendWVRAlarm(true, alarmCode);
       else
	 sendWVRAlarm(false, alarmCode);
       alarmCode++;
     }

   //Self test alarms
   for (unsigned int idx = 0; idx < 3; idx++)
     {
       alarmSet = (response.Data[4]>>idx)&0x01;
       if (alarmSet)
	 sendWVRAlarm(true, alarmCode);
       else
	 sendWVRAlarm(false, alarmCode);
       alarmCode++;
     }
}


void WVRImpl::processWVRData(const MonitorHelper::AMBRequestStruct& response){
  const string fnName = "WVRImpl::processWVRData";

  std::vector<WVRStruct>::iterator idx = getEventIndex(response.TargetTime);
  WVRStruct newData;

  // This is a new data event
  if (idx == wvrDataQueue_m.end())
    {
      newData.targetTime        = response.TargetTime;
      newData.event.antennaName = antennaName_m.c_str();
      newData.event.interval    = wvrDataInterval_m;
      newData.event.valid       = 1;

      /* TimeStamp calculation */
      getWVRDataValue(response);
      /* CSV-189: I am returning target time until understand the timing issues detected on this device */
      //ACS::Time wvrHwTimeStamp;
      //wvrHwTimeStamp = ACS::Time(std::floor((tsrcData_m.timeStamp/0.048) + 0.5) * TETimeUtil::TE_PERIOD_ACS);
      //newData.event.timestamp = wvrStartTimeStamp_m + wvrHwTimeStamp;
      newData.event.timestamp = response.TargetTime;
    }

  /* Check Status */
  if (response.Status != AMBERR_NOERR) {
    if(guard.checkAndIncrement() == true)
      ACS_LOG(LM_SOURCE_INFO,fnName.c_str(),
 	      (LM_WARNING,"WVR Data Failed  (Status = %d)",
 	       response.Status));
    newData.event.valid   = 0;
  }

  /* Check Time */
  if (response.Timestamp - response.TargetTime > TETimeUtil::TE_PERIOD_ACS/2){
    if(guard.checkAndIncrement() == true)
      ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
	      (LM_DEBUG,"WVR Data  at incorrect time ( %f s off)",
	       (response.Timestamp - response.TargetTime) * 1E-7));
  }


  // Fill WVR data queue
  if (response.RCA == getMonitorRCAIntTsrc0())
    {
      ACS::ThreadSyncGuard guard(&syncMutex_m);
      if (idx == wvrDataQueue_m.end())
	{
	  getWVRDataValue(response);
	  newData.event.temp0 = tsrcData_m.temp;
	  newData.dataArrived=1;
	  //add this event to the queue
	  wvrDataQueue_m.push_back(newData);
	} else {
     	  getWVRDataValue(response);
	  idx->event.temp0 = tsrcData_m.temp;
	  idx->dataArrived++;
	}
    }

  if (response.RCA == getMonitorRCAIntTsrc1())
    {
      ACS::ThreadSyncGuard guard(&syncMutex_m);
      if (idx == wvrDataQueue_m.end())
	{
	  getWVRDataValue(response);
	  newData.event.temp1 = tsrcData_m.temp;
	  newData.dataArrived=1;
	  //add this event to the queue
	  wvrDataQueue_m.push_back(newData);
	} else {
	  getWVRDataValue(response);
	  idx->event.temp1 = tsrcData_m.temp;
	  idx->dataArrived++;
	}
    }

  if (response.RCA == getMonitorRCAIntTsrc2())
    {
      ACS::ThreadSyncGuard guard(&syncMutex_m);
      if (idx == wvrDataQueue_m.end())
	{
	  getWVRDataValue(response);
	  newData.event.temp2 = tsrcData_m.temp;
	  newData.dataArrived=1;
	  //add this event to the queue
	  wvrDataQueue_m.push_back(newData);
	} else {
	  getWVRDataValue(response);
	  idx->event.temp2 = tsrcData_m.temp;
	  idx->dataArrived++;
	}
    }

  if (response.RCA == getMonitorRCAIntTsrc3())
    {
       ACS::ThreadSyncGuard guard(&syncMutex_m);
       if (idx == wvrDataQueue_m.end())
	 {
	   getWVRDataValue(response);
	   newData.event.temp3 = tsrcData_m.temp;
	   newData.dataArrived=1;
	   //add this event to the queue
	   wvrDataQueue_m.push_back(newData);
	 } else {
	   getWVRDataValue(response);
	   idx->event.temp3 = tsrcData_m.temp;
	   idx->dataArrived++;
	 }
     }

  // Check if some data must be published
  idx = getEventIndex(response.TargetTime);
  if(idx != wvrDataQueue_m.begin() &&
     idx != wvrDataQueue_m.end() &&
     idx->dataArrived == 4)
    {
      // Publish data and delete the event from the queue
      ACS::ThreadSyncGuard guard(&syncMutex_m);
      publishWVRData(idx->event, idx->targetTime);
      wvrDataQueue_m.erase(idx);
    }
}
/* ========================================================= */
void WVRImpl::updateThreadAction() {
  const string fnName = "WVRImpl::updateThreadAction";

  /* Queue WVR Alarms Monitor */
  queueWVRAlarms();

  /* Queue WVR Data */
  queueWVRData();
}

/*===========================================================*/
WVRImpl::UpdateThread::UpdateThread(const ACE_CString&  name,
				    WVRImpl&            WVR):
  ACS::Thread(name),
  wvrDevice_p(WVR)
{
  const string fnName = "WVRImpl::UpdateThread::UpdateThread";
  ACS_TRACE(fnName);
}

void WVRImpl::UpdateThread::runLoop() {
  wvrDevice_p.updateThreadAction();
}

/*==========================================================*/
std::vector<WVRStruct>::iterator WVRImpl::getEventIndex(const ACS::Time& requestTime)
{
  const string fnName="WVRImpl::getEvenIndex";
  ACS::ThreadSyncGuard guard(&syncMutex_m);

  std::vector<WVRStruct>::iterator idx = wvrDataQueue_m.begin();

  while (idx != wvrDataQueue_m.end() &&
	 idx->targetTime != requestTime) {
    idx++;
  }

  return idx;
}

void WVRImpl::getWVRDataValue(const MonitorHelper::AMBRequestStruct& response)
{
  std::vector< float > raw(2U);

  // The type conversion class needs that the size of the rawByte
  // should be the exact size of the monitored data and NOT 8 always.
  std::vector< AmbDataMem_t > rawBytes2(8U);
  for (int j=0; j < 8; j++){
    rawBytes2[j]= response.Data[j];
  }

  // Assign the rawBytes2 to the proper raw data.
  AMB::TypeConversion::dataToValue(rawBytes2, raw);

  tsrcData_m.temp = static_cast< float >(raw[0]);
  tsrcData_m.timeStamp = static_cast< float >(raw[1]);
}

void WVRImpl::publishWVRData(const Control::WVRData& event, ACS::Time targetTime)
{
  const string fnName = "WVRImpl::publishWVRData";

  if (wvrPublisher_p) {
    try {
      wvrPublisher_p->publishData<Control::WVRData>(event);
      std::ostringstream output;
      output << "WVR event published: "
	     << " Publish Time: " << TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value
	     << " AntennaName: " << event.antennaName
             << " TargetTime: " << (ACS::Time)targetTime
	     << " TimeStamp: " << (ACS::Time)event.timestamp
	     << " Interval: " << event.interval
	     << " Valid: " << event.valid
	     << " Temp0: " << event.temp0
	     << " Temp1: " << event.temp1
	     << " Temp2: " << event.temp2
	     << " Temp3: " << event.temp3
	     << std::endl;
      ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
	      (LM_DEBUG,output.str().c_str()));
    } catch (ACSErrTypeCommon::CORBAProblemEx& ex) {
      /* Since this is from a thread not much to do but log it and continue */
      ACS_LOG(LM_SOURCE_INFO,fnName.c_str(),
	      (LM_WARNING,"Unable to publish WVR data event."));
    }
  }
}

/*==========================================================*/
void WVRImpl::SET_INT_SETS(const Control::FloatSeq& world)
{
  const string fnName = "WVRImpl::SET_INT_SETS";

  /* Set integration time and gain estimated values */
  try {
    WVRBase::SET_INT_SETS(world);
  } catch (CAMBErrorEx& ex) {
    string msg = "Cannot set default integration time and gain estimated";
    msg += " as there is a communications problem with the Hardware.";
    LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
    CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
    newEx.addData("Detail", msg);
    newEx.log();
    return;
    // TODO Set an alarm
  } catch (INACTErrorEx& ex) {
    string msg = "Cannot set default integration time and gain estimated";
    msg += " as the control software is not operational.";
    LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
    INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
    newEx.addData("Detail", msg);
    newEx.log();
    return;
    // TODO Set an alarm
  }

  /* Save last values applied */
  wvrLastIntegrationTime_m = ACS::Time(std::floor((world[0] * 1E7) + 0.5));
  wvrLastGainEstimated_m   = ACS::Time(std::floor((world[1] * 1E7) + 0.5));
}

void WVRImpl::setWVRState(const Control::LongSeq& world)
{
  const string fnName = "WVRImpl::setIntegrationGain";

  std::vector< unsigned char > cworld(world.length());
  std::size_t i(0U);

  for(std::vector< unsigned char >::iterator iter(cworld.begin());
      iter != cworld.end(); ++iter, ++i)
    {
      (*iter) = static_cast< unsigned char >(world[i]);
    }

  MonitorHelper::AMBRequestStruct* cmdReq;
  unsigned char rawBytes[8];
  std::vector< unsigned char > raw(2U);
  raw = cworld;

  /* Assign raw to rawBytes. */
  int iRawBytes = 0;
  for(unsigned short i(0U); i < 2; ++i)
    {
      unsigned char* praw = reinterpret_cast< unsigned char* >(&raw[i]);
      for(unsigned short j(0U); j < 1; ++j)
        {
	  rawBytes[iRawBytes++] = praw[j];
        }
    }

  /* Execute the command */
  ACS::Time cmdExecutionTime = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value + TETimeUtil::TE_PERIOD_ACS;

  cmdReq = getRequestStruct();
  cmdReq->TargetTime = cmdExecutionTime;
  cmdReq->RCA        = getControlRCACntlWvrState();
  cmdReq->DataLength = 2U;
  memcpy(cmdReq->Data, reinterpret_cast< AmbDataMem_t* >(rawBytes), 8);

  commandTE(cmdReq->TargetTime,
	    cmdReq->RCA,
	    cmdReq->DataLength,
	    cmdReq->Data,
	    cmdReq->SynchLock,
	    &cmdReq->Timestamp,
	    &cmdReq->Status);

  /* Waiting for command execution */
  int err(0);
  ACE_Time_Value timeToWait;
  timeToWait.set(2.0);
  timeToWait += TimeUtil::epoch2ace(EpochHelper(cmdExecutionTime).value());
  const timespec_t waitUntil(timeToWait);
  /**
   * Try to acquire the semaphore for two seconds. The command should
   * have succeeded by then. If the semaphore would not be timed, chances
   * are that the code will be stuck here forever until reset.
   */
  if((err = sem_timedwait(cmdReq->SynchLock, &waitUntil)) != 0)
    {
      std::ostringstream output;
      output << "Failed to get the synchLock semaphore. Reason: "
	     << "command status = "
	     << cmdReq->Status;
      ACS_LOG(LM_SOURCE_INFO, fnName, (LM_ERROR, output.str().c_str()));
    };

  // Save time (rounded to nextTE) if we are resetting the internal counter
  if (world[1] == 0x3)
    wvrStartTimeStamp_m = TETimeUtil::ceilTE(cmdReq->Timestamp);
}


void WVRImpl::setIntegrationGain(const Control::FloatSeq& world)
{
  const string fnName = "WVRImpl::setIntegrationGain";

  /* Set default integration time and gain estimated values */
  try {
        Control::LongSeq wvrMode;
        Control::FloatSeq wvrInt;
        wvrMode.length(2);

	// Set WVR device to Configuration mode
        wvrMode[0] = 0x2;
        wvrMode[1] = 0x0;
        setWVRState(wvrMode);

	// Set Integration time and gain estimated by default values
	SET_INT_SETS(world);
        usleep(static_cast<useconds_t>(0.25*1E6));

	// Put WVR in Operational State, reset timestamp counter and clear alarms
	wvrMode[0] = 0x0;
        wvrMode[1] = 0x3;
        setWVRState(wvrMode);

	//Calculate and restart publishing thread
	restartWVRData();

    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot change WVR mode";
        msg += " as there is a communications problem with the Hardware.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot change WVR mode";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    }
}

void WVRImpl::restartWVRData()
{
  const string fnName = "WVRImpl::restartWVRData";

  AmbErrorCode_t flushStatus;
  ACS::Time      flushTime;

  /* Suspend the threads */
  updateThread_p->suspend();

  /* Flush all commands */
  flushNode(0, &flushTime, &flushStatus);
  if (flushStatus != AMBERR_NOERR) {
    ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
            (LM_INFO,"Communication failure flushing commands to device"));
  } else {
    ACS_LOG(LM_SOURCE_INFO, fnName.c_str(),
            (LM_DEBUG,"All commands and monitors flushed at: %lld",flushTime));
  }

  /* Restart times for alarm and WVR data threads */
  lastWVRDataTime_m  = (wvrStartTimeStamp_m / wvrLastIntegrationTime_m + 1) * wvrLastIntegrationTime_m;
  wvrDataQueue_m.clear();

  wvrDataInterval_m =  wvrLastIntegrationTime_m;

  /* Resume the threads */
  updateThread_p ->resume();
}


/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(WVRImpl)
