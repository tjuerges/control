
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WVRHWSimImpl.cpp
 *
 * $Id$
 */

#include "WVRHWSimImpl.h"
#include <TypeConversion.h>

using namespace AMB;
using std::vector;
using CAN::byte_t;

/* Please use this class to implement complex functionality for the
 * WVRHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

WVRHWSimImpl::WVRHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: WVRHWSimBase::WVRHWSimBase(node, serialNumber)
{
    // Load some default temperatures into the simulator. These values comes
    // from testing by Bojan at the OSF in August 2009. Somewhat realistic
    // values are needed for telcal to produce results.


    // TODO. Factor out all this duplicated code into a simple function like
    // vector<byte_t> temperatureToData(temperature);
    // and replace the code below with four calls like
    // setIntTsrc0(temperatureToData(171.78));

  startTime_m = TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value);
}

std::vector<CAN::byte_t> WVRHWSimImpl::getMonitorIntTsrc0() const 
{
  vector<CAN::byte_t> dataAndTime(8, 0);
  float temperature = 171.78;
  float time =  (TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value) - startTime_m) / 1E7;
  vector<byte_t> data;
  TypeConversion::valueToData(data, temperature);
  for (int i = 0; i < 4; i++) {
    dataAndTime[i] = data[i];
  }
  TypeConversion::valueToData(data, time);
  int j = 0;
  for (int i = 4; i < 8; i++) {
    dataAndTime[i] = data[j];			
    j++;
  }
  //setIntTsrc1(dataAndTime);
  //return WVRHWSimBase::getIntTsrc0();
  return dataAndTime;
}


std::vector< CAN::byte_t > WVRHWSimImpl::getMonitorIntTsrc1() const
{
  vector<byte_t> dataAndTime(8, 0);
  float temperature = 92.54;
  float time = (TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value) - startTime_m) / 1E7;
  vector<byte_t> data;
  TypeConversion::valueToData(data, temperature);
  for (int i = 0; i < 4; i++) {
    dataAndTime[i] = data[i];
  }
  TypeConversion::valueToData(data, time);
  int j = 0;
  for (int i = 4; i < 8; i++) {
    dataAndTime[i] = data[j];
    j++;
  }
   //setIntTsrc1(dataAndTime);
   //return WVRHWSimBase::getIntTsrc1();
   return dataAndTime;
}

std::vector<CAN::byte_t> WVRHWSimImpl::getMonitorIntTsrc2() const
{
  vector<byte_t> dataAndTime(8, 0);
  float temperature = 51.24;
  float time = (TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value) - startTime_m) / 1E7;
  vector<byte_t> data;
  TypeConversion::valueToData(data, temperature);
  for (int i = 0; i < 4; i++) {
    dataAndTime[i] = data[i];
  }
  TypeConversion::valueToData(data, time);
  int j = 0;
  for (int i = 4; i < 8; i++) {
    dataAndTime[i] = data[j];
    j++;
  }
  //setIntTsrc2(dataAndTime);
  //return WVRHWSimBase::getIntTsrc2();
  return dataAndTime;
}

std::vector<CAN::byte_t> WVRHWSimImpl::getMonitorIntTsrc3() const
{
  vector<byte_t> dataAndTime(8, 0);
    float temperature =  39.85;
    float time = (TETimeUtil::ceilTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value) - startTime_m) / 1E7;
    vector<byte_t> data;
    TypeConversion::valueToData(data, temperature);
    for (int i = 0; i < 4; i++) {
      dataAndTime[i] = data[i];
    }
    TypeConversion::valueToData(data, time);
    int j = 0;
    for (int i = 4; i < 8; i++) {
      dataAndTime[i] = data[j];
      j++;
    }
    //setIntTsrc3(dataAndTime);
    //return WVRHWSimBase::getIntTsrc3();
    return dataAndTime;
}


void WVRHWSimImpl::setControlSetWvrState(
    const std::vector< CAN::byte_t >& data)
{
  startTime_m = TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
}


//     {
//         float temperature = 92.54;
//         vector<byte_t> data;
//         TypeConversion::valueToData(data, temperature);
//         for (int i = 0; i < 4; i++) {
//             dataAndTime[i] = data[i];
//         }
//     }
//     setIntTsrc1(dataAndTime);

//     {
//         float temperature = 51.24;
//         vector<byte_t> data;
//         TypeConversion::valueToData(data, temperature);
//         for (int i = 0; i < 4; i++) {
//             dataAndTime[i] = data[i];
//         }
//     }
//     setIntTsrc2(dataAndTime);

//     {
//         float temperature = 39.85;
//         vector<byte_t> data;
//         TypeConversion::valueToData(data, temperature);
//         for (int i = 0; i < 4; i++) {
//             dataAndTime[i] = data[i];
//         }
//     }
//     setIntTsrc3(dataAndTime);
//}
