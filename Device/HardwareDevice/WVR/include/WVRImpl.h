 /*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WVRImpl.h
 */
#ifndef WVRImpl_H
#define WVRImpl_H

// Base class(es)
#include <WVRBase.h>
#include <WVRS.h>

#include <monitorHelper.h>
#include <errorStateHelper.h>
#include <RepeatGuard.h>
#include <acstimeTimeUtil.h>
#include <vector>
#include <acsncSimpleSupplier.h>
#include <ControlAntennaInterfacesC.h>
#include <controlAlarmSender.h>

enum WVRAlarmCondition {
  RailOff12V                = 1,
  MonitorMode               = 2,
  ControlMode               = 3,
  BytesNumber               = 4,
  NoLock                    = 5,
  CurrentOutOfRange12V      = 6,
  CurrentOutOfRange6V       = 7,
  CurrentOutOfRangeMinus6V  = 8,
  CurrentOutOfRangeChopper  = 9,
  VoltageOutOfRange12V      = 10,
  VoltageOutOfRange6V       = 11,
  VoltageOutOfRangeMinus6   = 12,
  HotLoadOverTemperature    = 13,
  ColdLoadOverTemperature   = 14,
  CTRLOverTemperature       = 15,
  BEOverTemperature         = 16,
  CSOverTemperature         = 17,
  ChopperWheelError         = 18,
  CalibFileError            = 19,
  LOError                   = 20
};

struct WVRStruct {
  Control::WVRData event;
  ACS::Time targetTime;
  int dataArrived;
};

struct TSRCStruct {
  double temp;
  double timeStamp;
};

class WVRImpl: public WVRBase,
	       public MonitorHelper,
	       virtual public POA_Control::WVR
{
 protected:
  /* -------------- Thread Classes  -------------- */
  class UpdateThread : public ACS::Thread {
  public:
    UpdateThread(const ACE_CString&     name,
		 WVRImpl&               wvrDevice);
    virtual void runLoop();
  protected:
    WVRImpl&  wvrDevice_p;
  };

  /* ------------- Publisher Class -------------- */
  class WVRPublisher : public nc::SimpleSupplier {
  public:
    WVRPublisher(const char *channelName,
		 acscomponent::ACSComponentImpl *component) :
      nc::SimpleSupplier(channelName,component){}
  };
  
 public:
  friend class WVRImpl::UpdateThread;
  
  // ------------------- Constructor & Destructor -------------------
  WVRImpl(const ACE_CString & name, maci::ContainerServices* cs);
  virtual ~WVRImpl();

  /* ---------------- Component Lifecycle -----------------------*/
  /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
  virtual void initialize();
  
  /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
  virtual void cleanUp();

  virtual void aboutToAbort();
  
 protected:
  /* ---------------- Hardware Lifecycle Interface ---------------*/
  virtual void hwInitializeAction();
  virtual void hwOperationalAction();
  virtual void hwStopAction();
  
  /* --------------------Monitor Helper Method -------------------*/
  virtual void processRequestResponse(const AMBRequestStruct& response);
  //virtual void handleSetErrorFlag(WVRErrorCondition newFlag);
  //virtual void handleResetErrorFlag(WVRErrorCondition newFlag);
  //virtual string createErrorMessage();
  
  /* ---------------- Error State Methods ------------------------ */
  void updateThreadAction();
  
 private:
  /* ======== WVR Alarm Method ============== */ 
  virtual void sendWVRAlarm(bool isActive, int alarmType);
  void  initializeAlarms();
  
  /* ========= Hardware queuing methods =============== */
  virtual void queueWVRAlarms(); 
  virtual void queueWVRData();
 
  /* ========= Hardware Processing Methods ============ */
  virtual void 
    processWVRAlarmsMon(const MonitorHelper::AMBRequestStruct&);
  virtual void  
    processWVRData(const MonitorHelper::AMBRequestStruct&);
  
  /* Wrapper method for saving last integration time set */
  virtual void SET_INT_SETS(const Control::FloatSeq& world);
  ACS::Time wvrLastIntegrationTime_m;
  ACS::Time wvrStartTimeStamp_m;
  float wvrLastGainEstimated_m;

  /* Method for setting a new integration time, 
   * estimated gain and modify the publication thread 
   */
  virtual void setIntegrationGain(const Control::FloatSeq& world);
  virtual void setWVRState(const Control::LongSeq& world);
  virtual void restartWVRData();

  /* Repeat guard for avoid logging flooding */
  RepeatGuard guard;

  /* WVR data event queue */
  WVRPublisher* wvrPublisher_p;
  std::vector<WVRStruct> wvrDataQueue_m; 
  ACE_Recursive_Thread_Mutex syncMutex_m;
  std::vector<WVRStruct>::iterator getEventIndex(const ACS::Time& requestTime);
  void getWVRDataValue(const MonitorHelper::AMBRequestStruct&);
  void publishWVRData(const Control::WVRData&, ACS::Time targetTime);
  TSRCStruct tsrcData_m;

 
  UpdateThread* updateThread_p;
  const ACS::Time updateThreadCycleTime_m;
  std::string antennaName_m;

  /* Time and interval for alarms */
  const ACS::Time wvrAlarmsInterval_m;
  ACS::Time lastWVRAlarmsTime_m;
  Control::AlarmSender* alarmSender_p;

  /* Time and interval for WVR data */
  ACS::Time     wvrDataInterval_m;
  ACS::Time     lastWVRDataTime_m;
   
};

#endif // WVRImpl_H
