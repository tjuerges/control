/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2009
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
#ifndef DIGITIZER_H
#define DIGITIZER_H

#include <fstream>
#include <iostream>
#include <ace/Recursive_Thread_Mutex.h>

#include <acsThread.h>

#include <PacketHandler.h>
#include "DigitizerBase.h"
#include "DataReceiverThread.h"
#include "SubscanData.h"

/**
 * The digitizer receives the data from an IFProc.
 * <P>
 * The data are stored in a buffer that will be sent to the
 * bulkdata receiver.
 * Depending on the deployment (CDB),  the Digitizer write raw data
 * in a file in CSV format.
 */
class Digitizer : public DigitizerBase, PacketHandler {

public:

	/**
	 * Constructor to be used when data must be backed up on file
	 *
	 * @param antennaName The name of the antenna
	 * @param polarization The polarization
	 * @param reactorManager The reactor manager for registering event handlers
	 */
	Digitizer(
			const char* antennaName,
			CORBA::Long polarization,
			ACS::ThreadManager* threadManager);

	/**
	 * Destructor
	 */
	virtual ~Digitizer();

	/**
	 * @return The port used by the server for incoming connections
	 */
	int getServerPort() { return m_dataReceiverThread_p->getServerPort(); }

	/**
	 * Stop the threads and free the allocated resources
	 */
	void shutdown();

	/**
	 * Interrupt the current subscan by discarding data received
	 * from the socket.
	 */
	void abortSubscan();

	/**
	 * Begin a subscan
	 *
	 * This method does not really start a subscan if the previous one has not yet finished.
	 * It can happen when the start time of the next subscan is really close to the end
	 * time of the previous one.
	 * In this case the parameter are stored in member variables and the new subscan is launched
	 * only when the previous one really terminates (in subscanDone)
	 */
	void beginSubscan(const Control::NewTPP::SubscanInfo& info,DataListener* dataListener);

	/**
	 * A new packet has been received by the socket.
	 *
	 * @see PacketHandler
	 */
	virtual void packetReceived(
			int samples,
			ACS::Time startTimeOfFirstSample,
			int firstSampleOffset,
			int lastSampleOffset,
			ACS::Time startTime,
			ACS::Time endTime,
			unsigned char* buffer,
			unsigned int drift);

	/**
	 * A subscan has terminated
	 *
	 * @return true if the listener has been notified
	 * @see PacketHandler
	 */
	virtual bool subscanDone(int exitCode);

	/**
	 * Return the data read from the digitizer for the passed dataID or
	 * NULL if it does not exist
	 *
	 * @param dataID The ID of the data of this subscan
	 */
	SubscanData* getSubscanData(ACE_CString dataID);

	/**
	 * Delete the SubscanData with the passed ID
	 *
	 * This methos is called by the DataCollector after reducing the data
	 * because at that point the SubscanData is useless.
	 *
	 * @param The ID of the subscan data to delete
	 */
	void releaseSubscanData(ACE_CString dataID);

	/**
	 * Return true if the digitizer is getting data for
	 * the subscan with the passes data ID and return false otherwise
	 */
	bool isGettingData(ACE_CString dataID);

	/**
	 * @return true when the digitizer is connected to the socket;
	 *         false otherwise
	 */
	bool isConnected();

private:

	/**
	 * Signal if the digitizer has been shut down
	 */
	volatile bool m_Closed;

	/**
	 * The server listening to incoming socket connections
	 * and getting data from the remote client
	 */
	DataReceiverThread* m_dataReceiverThread_p;

	/**
	 * The object to be notified when the subscan has terminated
	 */
	DataListener* m_dataListener;

	/**
	 * Avoid to send the signel to the listener more then once
	 */
	bool m_listenerNotified;

	/**
	 * The ACS thread manager
	 */
	ACS::ThreadManager* m_threadManager_p;

	/**
	 * The mutex to safely access/modify the dates and the buffer
	 * and all other internal data
	 */
	ACE_Recursive_Thread_Mutex m_mutex;

	/**
	 * The file for writing data in CSV.
	 * <P>
	 * It is NULL if no writing on file has been requested
	 */
	ofstream* outStream_p;

	/**
	 * The start time of the requested subscan
	 */
	ACS::Time m_startTime;

	/**
	 * The end time of the requested subscan
	 */
	ACS::Time m_endTime;

	/**
	 * The buffer of data received from the digitizer for the current subscan
	 */
	SubscanData* m_currentSubscanData_p;

	/**
	 * true if the data received from this digitizer for the
	 * current subscan must be discarded
	 */
	bool m_subscanAborted;

	/**
	 * true if the digitizer is getting data for a subscan;
	 * false otherwise
	 */
	bool m_subscanInprogress;

	/**
	 * The data ID of the subscan
	 */
	ACE_CString m_dataID;

	/**
	 * It can happen that the next subscan to execute is so close to
	 * the actual one that when the beginSubscan is called the previous
	 * subscan is still running.
	 *
	 * This is the DataListener for the next subscan
	 */
	DataListener* m_nextSubscanListener_p;

	/**
	 * It can happen that the next subscan to execute is so close to
	 * the actual one that when the beginSubscan is called the previous
	 * subscan is still running.
	 *
	 * This is the start time for the next subscan
	 */
	ACS::Time m_nextStartTime;

	/**
	 * It can happen that the next subscan to execute is so close to
	 * the actual one that when the beginSubscan is called the previous
	 * subscan is still running.
	 *
	 * This is the end for the next subscan
	 */
	ACS::Time m_nextEndTime;

	/**
	 * It can happen that the next subscan to execute is so close to
	 * the actual one that when the beginSubscan is called the previous
	 * subscan is still running.
	 *
	 * This is the data ID for the next subscan
	 */
	ACE_CString m_nextDataID;

	/**
	 * The vector of acquired subscans
	 *
	 * As a temporary solution, the vector stores *ALL* the acquired subscan
	 * TODO: optimize the memory management by reducing the number of subscan
	 *       in memory (for example the DataCollector could tell when it does not need
	 *       anymore the data and then they can be deleted)
	 */
	std::vector<SubscanData* > m_subscanDatas_v;

	/**
	 * launchSubscan starts a new subscan.
	 *
	 */
	void launchSubscan(ACS::Time start, ACS::Time end,ACE_CString dataID,DataListener* dataListener);

	/**
	 * Instantiate the DataReciver and start its thread
	 */
	void startReceivingThread(
			const char* antennaName,
			CORBA::Long polarization);
};
#endif /* DIGITIZER_H_ */
