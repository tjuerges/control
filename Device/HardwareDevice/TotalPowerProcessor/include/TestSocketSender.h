#ifndef TESTSOCKETSENDER_H
#define TESTSOCKETSENDER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-27  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

/**
 * A class sending data as it would be a digitizer.
 * <P>
 * The socket is opened in the local host 127.0.0.1
 */
class TestSocketSender {
public:
	/**
	 * Constructor
	 */
	TestSocketSender(int port);

	/**
	 * Destructor
	 * <P>
	 * Close the socket if has been successfully opened
	 */
	virtual ~TestSocketSender();

	/**
	 * Connect to the server socket
	 *
	 * @return true if the connection has been established
	 */
	bool connectSocket();

	/**
	 * Send the data through the socket.
	 * <P>
	 * sendData send up to numOfPackets packets in the passed interval.
	 * The packets and the data they contain are built in order to be
	 * compared with the received data (i.e unique identifiers are used and so on..)
	 * <P>
	 * This method is not executed in real time so it makes its best to
	 * send the packets in the given interval.
	 *
	 * @param timeToStart The instant of time (sec) to start sending data
	 * @param timeToFinish The instant of time (sec) to stop sending data
	 * @param numOfPackets The max number of packets to send in the passed interval;
	 * 						if 0 then the number of packets to send is unlimited
	 * @return the number of packets effectively sent; -1 means error
	 */
	int sendData(long timeToStart, long timeToFinish, unsigned int numOfPackets=0);

private:
	/**
	 * The port of the server socket
	 */
	int m_port;

	/**
	 * The socket file descriptor.
	 * <P>
	 * It is < 0 if the socket has not been connected
	 */
	int m_sockfd;

	/**
	 * The size of a data block is a fixed value
	 */
	const unsigned int m_dataBlockSize;

	/**
	 * The size of the buffer
	 */
	const unsigned int m_bufferSize;

	/**
	 * The buffer containing a packet to send (8*m_dataBlockSize)
	 * <P>
	 * The same buffer is used to send all the packets!
	 */
	char* m_buffer;

	/**
	 * Build a packet to send to the receiver.
	 * <P>
	 * The packet has an identifier.
	 *
	 * @param id The ID of the packet (0<=id<=65530)
	 */
	void buildPacket(unsigned short id);

	/**
	 * Send the buffer through the socket
	 *
	 * @return true if the packet has been sent without errors
	 */
	bool sendPacket();
};

#endif /* TESTSOCKETSENDER_H*/
