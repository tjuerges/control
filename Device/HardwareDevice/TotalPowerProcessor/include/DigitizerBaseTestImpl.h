#ifndef DIGITIZERS_BASE_TEST_IMPL_H
#define DIGITIZERS_BASE_TEST_IMPL_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* almadev  2009-05-13  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <iostream>

#include "DigitizerBase.h"

/**
 * DigitizerBase has a pure virtual method that needs to be
 * implemented for instantiating objects of this class.
 *
 * DigitizerBaseTestImpl implements such a method with a stub.
 */
class DigitizerBaseTestImpl: public DigitizerBase {
public:

	DigitizerBaseTestImpl(const char* antennaName, CORBA::Long polarization):
		DigitizerBase(antennaName, polarization) {}

	virtual ~DigitizerBaseTestImpl() {}

	void shutdown() {
		std::cout<<"Shutting down antenna: "<<m_antennaName<<", pol="<<m_polarization<<std::endl;
	}

	void abortSubscan() {
		std::cout<<"Abort subscan for antenna: "<<m_antennaName<<", pol="<<m_polarization<<std::endl;
	}

	void beginSubscan(const Control::NewTPP::SubscanInfo& info,DataListener* dl) {
		std::cout<<"beginSubscan subscan for antenna: "<<m_antennaName<<", pol="<<m_polarization<<std::endl;
	}
};

#endif /*! DIGITIZERS_BASE_TEST_IMPL_H */
