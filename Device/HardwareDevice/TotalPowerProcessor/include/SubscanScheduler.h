#ifndef SUBSCANSCHEDULER_H
#define SUBSCANSCHEDULER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-07-22  created
*/

/************************************************************************
 *
 * The beginSubscan() IDL method can be executed while one subscan is
 * still running.
 * <P>
 * Objects of this class owns a FIFO list of subscan invocations
 * and start the next one without aborting the current subscan. if
 * possible.
 * <P>
 * NOTE: SubscanScheduler assumes that the subscans are timely ordered.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <vector>
#include <ace/Recursive_Thread_Mutex.h>
#include <ace/Semaphore.h>

#include <acsThread.h>

#include <NewTPPC.h>

class NewTPPImpl;

/**
 * An helper object to pass parameter to the constructor.
 */
class SubscanSchedulerParam {
public:
	NewTPPImpl* m_tpp_p;
};

/**
 * The scheduler to launch one subscan after the other.
 * <P>
 * Each new subscan is inserted in a vector. The thread wakes up
 * when a subscan terminated and if the vector is not empty, runs
 * another subscan.
 */
class SubscanScheduler: public ACS::Thread {
private:

	/**
	 * The vector of subscans to schedule.
	 * <P>
	 * Newly arrived subscans are appended at the end of the vector.
	 * <P>
	 * Note: each subscan is removed only when the subscan terminates
	 *       and NOT when the subscan is launched
	 */
	std::vector<Control::NewTPP::SubscanInfo> m_subscans_v;

	/**
	 * The mutex to safely modify internal data
	 */
	ACE_Recursive_Thread_Mutex m_mutex;

	/**
	 * The semaphore used to by the thread to sleep until
	 * a subscan terminates or the shutdwon is executed
	 *
	 */
	ACE_Semaphore m_semaphore;

	/**
	 * Signal if the scheduler has been closed
	 */
	bool m_closed;

	/**
	 * The TPP to launch subscans
	 */
	NewTPPImpl* m_tpp_p;

	/**
	 * Check if the vector contains subscans to execute and, if it is the case,
	 * launch the first one
	 */
	void executeSubscan();

public:

	/**
	 * Constructor
	 */
	SubscanScheduler(const ACE_CString& name, SubscanSchedulerParam& param);

	/**
	 * Destructor
	 */
	~SubscanScheduler();

	/**
	 * Add a subscan to the list of subscan to schedule
	 */
	void addSubscan(Control::NewTPP::SubscanInfo subscanInfo);

	/**
	 * Inform the scheduler that the last launched subscan has terminated
	 * and a new one can be launched.
	 */
	void subscanTerminated();

	/**
	 * close the scheduler i.e. no new subscan will be submitted
	 */
	void shutdown();

	/**
	 * The thread to launch a new subscan.
	 * <P>
	 * The thread is basically waiting a signal to launch a new
	 * subscan (if present in the vector)
	 *
	 * @see ACS::Thread
	 */
	virtual void run();

};

#endif /*SUBSCANSCHEDULER!_H*/
