#ifndef TOTALPOWERSENDER_H
#define TOTALPOWERSENDER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-03-20  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <bulkDataDistributerC.h>

#include <baci.h>
#include <bulkDataSender.h>
#include <bulkDataSenderDefaultCb.h>
#include <ACSErrTypeCommon.h>

/**
   The TotalPowerSender is a singleton class which acts as the interface
   to the ARCHIVE_TOTALPOWER_DISTRIBUTOR.  
*/


class TotalPowerSender : 
  public AcsBulkdata::BulkDataSender<BulkDataSenderDefaultCallback>
{
 public:
	  /**
	   * Build the TotalPowerSender and connect it to the distributer.
	   *
	   * @param name The name of the distributer to conenct to
	   * @param  containerServices The maci ContainerServices
	   *
	   * @throw ACSErrTypeCommon::BadParameterExImpl If the param are NULL or empty
	   * @thorw ACSErrTypeCommon::CouldntPerformActionExImpl If failed to connect to the distributer
	   */
  TotalPowerSender(const char* name, maci::ContainerServices* containerServices);
  
  ~TotalPowerSender();

 private:
  void connect(const char*)
    throw (ACSErrTypeCommon::CouldntPerformActionExImpl);
  void disconnect();

  /**
   * The Distributer
   */
  bulkdata::BulkDataDistributer_var distributorRef_v;

  /**
   * The maci ContainerServices
   */
  maci::ContainerServices* m_contSvcs_p;
};

#endif /*!TOTALPOWERSENDER_H*/
