/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2009
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
#ifndef TOTALPOWERPROCESSOR2IMPL_H_
#define TOTALPOWERPROCESSOR2IMPL_H_

#include <acscomponentImpl.h>

#include <ace/Thread_Mutex.h>

#include "NewTPPS.h"

#include "AntennasContainer.h"
class DataCollector;
#include "DataCollector.h"
#include "SubscanScheduler.h"
#include "TotalPowerSender.h"

/**
 *  DataCollectorsContainer is a container of dataColelctors.
 *
 *  DataCollectors are created when a new subscan starts and they can be deleted
 *  only when the data have been sent to the Distributer (i.e. the ARCHIVE).
 *
 *  In this first implementation, DataColelctors are simply stored in the vector
 *  and deleted only when the destructor is called i.e. when the process
 *  shuts down.
 */
class DataCollectorsContainer {

private:
	/**
	 * The data collector of the actual observation
	 */
	DataCollector* m_actualCollector_p;

	/**
	 * The vectors of DataCollectors
	 */
	std::vector<DataCollector*> m_collectors_v;

	/**
	 * The mutex to protect shareddata against concurrent accesses
	 */
	ACE_Recursive_Thread_Mutex m_mutex;

public:
	/**
	 * Constructor
	 */
	DataCollectorsContainer();

	/**
	 * Destructor
	 */
	virtual ~DataCollectorsContainer();

	/**
	 * Add a not NULL DataCollector.
	 * This DataColelctor becomes the actual DataCollector
	 */
	void addCollector(DataCollector *dc);

	/**
	 * Return true if the current subscan has terminated collecting data
	 * and false otherwise
	 */
	bool isCollectingData();

	/**
	 * abort the current subscan by delegating to the DataCollector
	 */
	void abortSubscan();

	/**
	 * Check if all the thread of the DataCollectors of the container
	 * are still running
	 *
	 * @return true if at least one thread is still running;
	 *         false otherwise
	 */
	bool areCollectorsRunning();
};

class NewTPPImpl :
	public virtual acscomponent::ACSComponentImpl,
	public virtual POA_Control::NewTPP {

	public:

		/**
		 * Constructor
		 *
		 * @param name The name of the component
		 * @param containerServices The ContainerServices
		 */
		NewTPPImpl(const ACE_CString& name, maci::ContainerServices* containerServices);

		/**
		 * Destructor
		 */
		virtual ~NewTPPImpl();

		/**
		 *  Life cycle method
		 */
		virtual void initialize();

		/**
		 *  Life cycle method
		 */
		virtual void execute();

		/**
		 *  Life cycle method
		 */
		virtual void cleanUp();

		 /**
		  *  Life cycle method
		  */
		virtual void aboutToAbort();

		/**
		 * @see BulkDataSender
		 */
		void startSend();

		/**
		 * @see BulkDataSender
		 */
		void paceData();

		/**
		 * @see BulkDataSender
		 */
		void stopSend();

		/**
		 * A beginSubcan can be called when a subscan is already running.
		 * If it is the case, the newly arrived subscan must be launched
		 * only when the current one has terminated.
		 *
		 * Launch a subscan instead executes the passed subscan when
		 * no other subscan is running.
		 */
		void launchSubscan(const Control::NewTPP::SubscanInfo& info);

		////////////////////////////////////////////////////////
		///
		/// IDL methods
		///
		////////////////////////////////////////////////////////

		/**
		 * Shutdown the process and free all the resources
		 */
		void shutdown();

		/**
		 * Connect to the distributer
		 */
		virtual void setDistributer(const char* distributerName);

		/**
		 * Add a digitizer
		 */
		 Control::NewTPP::ConnectionInfo addDigitizer(const char* antennaName, CORBA::Long polarization);

		 /**
		  * Configure the antennas of the array
		  */
		 virtual void configureArray(const Control::AntennaSeq&);

		 /**
		  * Set the integration duration
		  */
		 virtual void setIntegrationDuration(CORBA::Double);

		 /**
		  * Return the integration duration
		  */
		 virtual CORBA::Double getIntegrationDuration();

		 /**
		  * Abort all the subscans
		  */
		 virtual void abortSubscan();

		 /**
		  * Begin subscan
		  */
		 virtual void beginSubscan(const Control::NewTPP::SubscanInfo&);

		 /**
		  * Wait until the subscan completes or a timeout occurs.
		  *
		  * @param timeout The number of seconds to wait for successful completion before returning.
		  * @return true if the subscan completes before the tmeout elapses
		  */
		 virtual CORBA::Boolean subscanComplete(CORBA::Long timeout);

		 /**
		  * Return the data of the last subscan
		  *
		  * @param  numOf Data Data produced
		  * @param numOfIntegration integrations
		  * @param numOfFlagged Data flagged (i.e. with error)
		  */
		 void getDataInfo(CORBA::Long& numOfData, CORBA::Double& numOfIntegration, CORBA::Long& numOfFlagged);

		 /**
		  * Set the data of the last subscan.
		  * This method is called by the DatCollector when it has all the info.
		  *
		  * @param  numOf Data Data produced
		  * @param numOfIntegration integrations
		  * @param numOfFlagged Data flagged (i.e. with error)
		  */
		 void setDataInfo(CORBA::Long numOfData, CORBA::Double numOfIntegration, CORBA::Long numOfFlagged);

		 /**
		  * Interrupt the reception of data from the digitizer with the passed
		  * antenna name and polarization
		  *
		  * @param antennaName The name of the antenna of the digitizer
		  * @param polarization The polarization of the digitizer
		  */
		 void interruptClient(const char* antennaName, CORBA::Long polarization);

		 /**
		  * Check if the digitizer with the passed name and polarization
		  * is connected to the ethernet socket
		  *
		  * @param antennaName The name of the antenna of the digitizer
		  * @param polarization The polarization of the digitizer
		  * @return true if the digitizer is connected to the socket
		  *         false otherwise
		  */
		 CORBA::Boolean isConnected(const char*, CORBA::Long);

		 /**
		  * Return the bulkdata sender
		  */
		 TotalPowerSender* getSender() { return m_bulkDataSender_p; }

	public:
		 /*
		 * The rate of the samplers (2 KHz) as an ACS::Time
		 */
		static const unsigned int sm_sampleInterval = 5000; //0.5 [ms]

		/*
		 * There are 4 basebands i.e. 4 samples interleaved.
		 */
		static const unsigned int sm_channelsPerSample = 4;

		/*
		 * Constant of 2000 kHz used to translate between the averaging length
		 * and the sample rate
		 */
		static const unsigned int sm_defaultSampleRate = 10000000/sm_sampleInterval;

	private:

		/**
		 * The DataCollectorsContainer that hold a reference to all the generated DataCollectors
		 */
		DataCollectorsContainer m_dataCollectors;

		/**
		 * The mutex to protect data against concurrent accesses
		 *
		 * @see m_cond_p
		 */
		ACE_Recursive_Thread_Mutex m_mutex;

		/**
		 * The IPAddress represented as a 32 bit integer
		 */
		unsigned int m_IP;

		/**
		 * The four bytes of the IP address
		 */
		CORBA::Octet m_IP_Bytes[4];

		/**
		 * The host name where this component runs
		 */
		ACE_CString m_localHostName;

		/**
		 * The IP address of the host name where this component runs
		 */
		ACE_CString m_IPAddressString;

		/**
		 * The ACS thread manager
		 */
		ACS::ThreadManager* m_threadManager_p;

		/**
		 * The digitizers receiving data from the sockets
		 */
		DigitizersContainer m_digitizers;

		/**
		 * The antennas of the array
		 */
		AntennaContainer* m_antennas_p;

		/**
		 * The integration duration
		 */
		CORBA::Double m_duration;

		/**
		 * The mutex to protect shared data generated by the last subscan
		 * i.e. m_dataProduced, m_flaggedData and m_numIntegrations.
		 * <P>
		 * <B>Note</B>: we cannot use m_mutex here because of a critical race in shutdown
		 */
		ACE_Recursive_Thread_Mutex m_lastSubscanDataMutex;

		/**
		 * The number of data produced by the last subscan
		 *
		 * @see getDataInfo(...)
		 */
		CORBA::Long m_dataProduced;

		/**
		 * The number of errors produced by the last subscan
		 *
		 * @see getDataInfo(...)
		 */
		CORBA::Long m_flaggedData;

		/**
		 * The number of integrations produced by the last subscan
		 *
		 * @see getDataInfo(...)
		 */
		CORBA::Double m_numIntegrations;

		/* This is the length (in number of samples) that we need when averaging
		 * the data.
		 * The default value of 1 corresponds to no averaging
		 */
		int m_averagingLength;

		/**
		 * The bulk data sender
		 */
		TotalPowerSender* m_bulkDataSender_p;

		/**
		 * The scheduler of subscans
		 */
		SubscanScheduler* m_scheduler_p;

		/**
		 * Initialize the host name and IP address
		 *
		 * @throw InvalidIPExImpl In case of network problems
		 */
		void initNetAddress();

		/**
		 * Signal if the component has been shut down
		 */
		volatile bool m_Closed;

		/**
		 * The name of the distributer to connect to
		 */
		ACE_CString m_distributerName;

};
#endif /* TOTALPOWERPROCESSOR2IMPL_H_ */
