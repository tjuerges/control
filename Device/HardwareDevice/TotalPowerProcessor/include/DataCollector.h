#ifndef DATACOLLECTOR_H
#define DATACOLLECTOR_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-02-14  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ace/Condition_Recursive_Thread_Mutex.h>
#include <ace/Thread_Mutex.h>

#include <vector>

#include <acsThread.h>

class NewTPPImpl;
#include "NewTPPImpl.h"

#include <SDMDataObjectPartTypes.h>

class DigitizersContainer;

#include "SubscanScheduler.h"
#include "DataListener.h"
#include <SubscanHeader.h>
class TPPBinaryBlock;
#include <TPPBinaryBlock.h>

/**
 * An helper object to pass parameter to the constructor.
 */
class DataCollectorParam {
public:
	AntennaContainer* m_antCnt_p;
	DigitizersContainer* m_dgtCnt_p;
	Control::NewTPP::SubscanInfo m_subscanInfo;
	int m_avgLength;
	NewTPPImpl* m_tpp_p;
	SubscanScheduler* m_scheduler_p;
};

/**
 * A DataCollector:
 * <UL>
 *   <LI>wait for a subscan to finish
 *   <LI>collect data from all the digitizers
 *   <LI>reduce data
 *   <LI>send data to the BulkDataReceiver
 * </UL>
 * <P>
 * One DataCollector is created for each subscan.
 * <P>
 * To check if all the digitizers in the subscan have terminated
 * an ACE_Condition is used.
 * <P>
 * If the subscan is closed, this object will terminate without further
 * computation.
 * <P>
 * The DataCollector allocates new buffers for data and flags where
 * to collect data received from the digitizers; the size of such
 * buffers is so big to contain all the received data.
 * In this stage I don't care if there is enough RAM available because
 * I don't have numbers to estimate memory consumption.
 * If the available RAM is not enough I will consider the case of buffering
 * data on files for both the Digitizers and the DataCollector.
 *
 */
class DataCollector: public DataListener, public ACS::Thread {
public:
	/**
	 * Constructor.
	 * <P>
	 * Build the subscan header and start the thread to wait until
	 * all the digitizers receive data from the socket
	 *
	 * @param name The name of the thread
	 * @param param The parameters
	 * @param subscanHeader The subscan header
	 */
	DataCollector(const ACE_CString& name, DataCollectorParam& param);

	/**
	 * Destructor
	 */
	virtual ~DataCollector();

	/**
	 * The thread to wait for data.
	 *
	 * @see ACS::Thread
	 */
	virtual void run();

	/**
	 * @see DataListener
	 */
	void digitizerDataArrived(const char* digitizerName);

	/**
	 * @see DataListener
	 */
	 ACE_CString getListenerName() { return getName(); }

	/**
	 * Return the data to send through the bulkdata sender
	 * by delegating to the TPPBinaryBlock.
	 * <P>
	 * This method returns a boolean because the binary object is built by a thread that
	 * could have not yet produced the object at the moment it is requested.
	 *
	 * @see TPPBinaryBlock.getData(...)
	 */
	bool getData(const char*& data0_id, const char*& data, size_t& size);

	/**
	 * return true while it is waiting for data from the digitizers
	 * and false when all the data of the subscan has been received
	 */
	bool isCollectingData();

	/**
	 * Stop waiting for the termination of the subscan.
	 * <P>
	 * The thread terminates and if data received from the digitizer discarded.
	 */
	void abortSubscan();

private:
	/**
	 * The Header of the current subscan
	 */
	SubscanHeader* m_subscanHeader_p;

	/**
	 * The container with the antennas involved in the subscan
	 */
	AntennaContainer* m_antennasContainer_p;

	/**
	 * The container with the antennas involved in the subscan
	 */
	DigitizersContainer* m_digitizersContainer_p;

	/**
	 * A pointer to the component to send the binary object
	 * to the BulkDataReceiver
	 */
	NewTPPImpl* m_tpp_p;

	/**
	 * The mutex for checking if all the digitizer sent the data.
	 * It is used together with the ACE_condition
	 *
	 * @see m_cond_p
	 */
	ACE_Thread_Mutex* m_mutex_p;

	/**
	 * The condition for checking if all the digitizer sent the data.
	 * It is used together with the ACE_Thread_Mutex.
	 *
	 * @see m_mutex_p
	 */
	ACE_Condition<ACE_Thread_Mutex>* m_cond_p;

	/**
	 * Signal that this subscan has been closed.
	 */
	volatile bool m_closed;

	/**
	 * The size of the buffers m_dataBuffer_p and m_flagBuffer_p
	 */
	unsigned int m_buffersSize;

	/**
	 * The buffer of data
	 */
	std::vector<asdmbinaries::AUTODATATYPE> m_dataBuffer_v;

	/**
	 * The flag of data
	 */
	std::vector<asdmbinaries::FLAGSTYPE> m_flagsBuffer_v;

	/**
	 * The averaging length
	 */
	int m_avgLength;

	/**
	 * The ASDM binary object
	 */
	TPPBinaryBlock* m_binaryBlock_p;

	/**
	 * true while the digitizer are acquiring data from the socket and
	 * false when all the data arrived.
	 * <P>
	 * This variable is checked to know when all the digitizer sent their data.
	 */
	volatile bool m_aquiringData;

	/**
	 * True if the thread has been waked up, false otherwise
	 * <P>
	 * The purpose is to avoid signaling more then once
	 */
	bool m_signaled;

	/**
	 * The subscan scheduler to be informed when the subscan finishes
	 */
	SubscanScheduler* m_scheduler_p;

	/**
	 * The data ID of the subscan
	 */
	ACE_CString m_dataID;

	/**
	 * Wait until all data have been sent by the digitizers.
	 */
	void waitDataAcquired();

	/**
	 * Init the buffers to store subscan data into
	 */
	void initBuffers();

	/**
	 * Format the data received from the digitizers as expected by the
	 * bulk data received.
	 */
	void reduce();

	/**
	 * Check if the passed digitizer must be included
	 * while reducing data.
	 * For example if the antenna name is not in the antennas container
	 * then the digitizer must be discarded.
	 *
	 * @param digitizer The digitizer to check (can be NULL)
	 * @return true If the digitizer is part of the subscan
	 */
	bool checkDigitizer(DigitizerBase* digitizer);

	/**
	 * Add the data of a digitizer to the output buffer
	 *
	 * @param digitizer The digitizer whose data has to be added to the buffer
	 * 					(can be NULL)
	 * @return The number of sample added
	 */
	int addDigitizerData(DigitizerBase* digitizer);

	/**
	 * Dump the content of the buffer on stdout for debugging.
	 */
	void dump();
};

#endif /*!DATACOLLECTOR_H*/
