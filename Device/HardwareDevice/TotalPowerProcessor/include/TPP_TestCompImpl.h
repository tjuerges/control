#ifndef TPP_TESTCOMPONENT_H
#define TPP_TESTCOMPONENT_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-03-16  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "TPP_TestComponentS.h"
#include "bulkDataReceiverImpl.h"

template<class TCallback>
class TPP_TestCompImpl :
	public virtual BulkDataReceiverImpl<TCallback>,
	public virtual POA_Control::TPP_TestComponent {

	public:
		/**
		 * Constructor
		 *
		 * @param name The name of the component
		 * @param containerServices The ContainerServices
		 */
		 TPP_TestCompImpl(const ACE_CString& name, maci::ContainerServices* containerServices);

		/**
		 * Destructor
		 */
		virtual ~TPP_TestCompImpl();

		/**
		 *  Life cycle method
		 */
		virtual void initialize();

		/**
		 *  Life cycle method
		 */
		virtual void execute();

		/**
		 *  Life cycle method
		 */
		virtual void cleanUp();

		 /**
		  *  Life cycle method
		  */
		virtual void aboutToAbort();
};

#include "TPP_TestCompImpl.i"

#endif /*!TPP_TESTCOMPONENT_H*/
