#ifndef DATALISTENER_H
#define DATALISTENER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
>>>>>>> 1.2.2.3
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-02-16  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

/**
 * The listener to be notified when all the data from a digitizer
 * has been received from the socket.
 */
class DataListener {
public:
	/**
	 * Destructor
	 */
	virtual ~DataListener() {}

	/**
	 * Signal that all the subscan data expected by one digitizer
	 * has arrived.
	 *
	 * @param digitizerName The name (ant:pol) of the digitizer who received all the data
	 */
	virtual void digitizerDataArrived(const char* digitizerName)=0;

	/**
	 * Return the name of the data listern.
	 *
	 * This is the name of th eobject (and thread) that receives and reduces the data
	 */
	virtual ACE_CString getListenerName()=0;
};

#endif /*DATALISTENER!_H*/
