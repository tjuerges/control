#ifndef ANTENNAS_CONTAINER_H
#define ANTENNAS_CONTAINER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-13  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <vector>
#include <set>

#include <ace/Recursive_Thread_Mutex.h>

#include "ControlAntennaInterfacesS.h"
#include "DigitizersContainer.h"

/**
 * The container of name of antennas (strings) of the array.
 * <P>
 * Objects of this class, apart of storing the antennas in the passed order,
 * deletes from the digitizers container all the digitizers having an
 * antenna names not part of the array
 */
class AntennaContainer {
public:
	/**
	 * Constructor
	 *
	 * @param The container of digitizers
	 */
	AntennaContainer(DigitizersContainer&);

	/**
	 * Destructor
	 */
	~AntennaContainer();

	/**
	 * Empty the container by removing all the antenna nmaes.
	 * <P>
	 * Note that the digitizers are not removed from the digitizers container.
	 */
	void clear();

	/**
	 * Set a new array.
	 * <P>
	 * First the old container is emptied then the new array is set (this is to preserve the order
	 * of antennas).
	 * All the digitizers in the container, whose antenna name is not in the passed sequence,
	 * are shut down and deleted.
	 */
	void setArray(const Control::AntennaSeq& antennas);

	/**
	 * Return the number of antenna names in the container
	 */
	unsigned int size();

	/**
	 * Return true if the container contains an antenna with the passed name
	 */
	bool contains(const char* antennaName);

	/**
	 * Return the name of the antenna in position i
	 *
	 * @param  The index of the antenna
	 * @return The name of the antenna at index i;
	 *  		NULL if i is out of range
	 */
	const char* getAntennaAt(unsigned int i);

	/**
	 * Find the position of a given antenna name.
	 *
	 * @param antennaName the name of the antenna (can be NULL)
	 * @return The index of the antenna with the given name in the container
	 * 		   or -1 if the container does not contain an antenna such name
	 */
	int findAntenna(const char* antennaName);

	/**
	 * Return a set of string with the antenna names in the container.
	 * <P>
	 * The returned set contains a copy of the string in the container.
	 *
	 * @return The name of the antennas in the container
	 */
	std::set<ACE_CString> getAntennaNames();

private:

	/**
	 * The container of digitizers
	 */
	DigitizersContainer& m_digitizers;

	/**
	 * The vector of antenna names
	 */
	std::vector<ACE_CString> m_antennas;

	/**
	 * The mutex to safely access the digitizers vector
	 */
	ACE_Recursive_Thread_Mutex m_antennasMutex;

};

#endif /*! ANTENNAS_CONTAINER_H*/
