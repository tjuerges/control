#ifndef SUBSCANHEADER_H
#define SUBSCANHEADER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-02-04  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ace/SString.h>

#include "NewTPPS.h"
#include "AntennasContainer.h"

class AntennaContainer;
/**
 * A class collecting the info for generating the binary header
 * to send to the bulk data receiver.
 */
class SubscanHeader {
public:
	/**
	 * Constructor
	 *
	 * @param info The SubscanInfo
	 * @param avgLength The averaging length
	 * @parma antennaList The container of the antennas
	 */
	SubscanHeader(
			const Control::NewTPP::SubscanInfo& info,
			int avgLength,
			AntennaContainer* antennaList);

	ACS::Time getStartTime() { return m_subscanInfo.startTime; }

	ACS::Time getEndTime() { return m_subscanInfo.endTime; }

	ACE_CString getDataOID() { return m_subscanInfo.dataOID.in(); }

	ACE_CString getExecID() { return m_subscanInfo.execID.in(); }

	ACS::Time getInterval() { return m_subscanInfo.endTime-m_subscanInfo.startTime; }

	ACS::Time getTime() { return m_subscanInfo.startTime + (getInterval()/2); }

	int getScanNumber() { return m_subscanInfo.scanNumber; }

	int getSubscancanNumber() { return m_subscanInfo.subscanNumber; }

	int getAveragingLength() { return m_averagingLength; }

	int getNumIntegration();

	int getBasebandLength() { return m_subscanInfo.baseband.length(); }

	int getBasebandOrder(int idx) { return m_subscanInfo.baseband[idx]; }

	int getPolarizationLength() { return m_subscanInfo.polarization.length(); }

	/**
	 * @return the polarization at the passed position in the sequence
	 *
	 */
	int getPolarizationAt(unsigned int i) { return m_subscanInfo.polarization[i]; }

	/**
	 * @param polarizaton The polarization to look for in the array
	 * @return The index of the passed polarization
	 * 			or -1 if the polarization is not present
	 */
	int getPolarizationIndex(int polarizaton);

	int getNumBins() { return m_numBins; }

	int getNumberOfAntennas();

	Control::NewTPP::SubscanInfo getSubscanInfo() { return m_subscanInfo; }

private:
	/**
	 * The subscan info describing the subscan
	 */
	const Control::NewTPP::SubscanInfo m_subscanInfo;

	/**
	 * This is 1 for the time being
	 */
	static const int m_numBins=1;

	/* These we need to allow us to reduce the data */
	int   m_averagingLength;

	/**
	 * The antennas of this subscan
	 */
	AntennaContainer* m_antennaList;
};

#endif /*!SUBSCANHEADER_H*/
