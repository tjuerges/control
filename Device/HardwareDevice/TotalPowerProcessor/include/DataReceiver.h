#ifndef DATARECEIVER_H
#define DATARECEIVER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-01-16  created
*/

#include <ace/Basic_Types.h>
#include <SOCK_Acceptor.h>
#include <ace/SOCK_Stream.h>
#include <ace/Recursive_Thread_Mutex.h>

#include "PacketHandler.h"

class DataReceiverThread;

/**
 * Objects of this class establish a socket connection and read incoming data.
 * <P>
 * It first instantiates the server socket and waits for the remote client to
 * connect, the starts receiving data.
 *
 * When a full packet has been read, it is sent to the callback.
 */
class DataReceiver {

public:
	/**
	 * Constructor
	 *
	 * @param name The name of the thread
	 * @param ph The object to send received packets to
	 * @param swapDate true if the date of the packet must be swapped
	 * @param swapSamples true if the samples of the packet must be swapped
	 * @oaram thread The thread that owns
	 */
	DataReceiver(
			const ACE_CString& name,
			PacketHandler* ph,
			bool swapDate,
			bool swapSamples,
			DataReceiverThread* thread);

	/**
	 * Destructor
	 */
	virtual ~DataReceiver();

	/**
	 * Receive data from socket
	 */
	virtual void socketReceiver();

	/**
	 * Close the sockets and stops receiving data
	 */
	void shutdown();

	/**
	 * @return The port used by the server for incoming connections
	 */
	int getServerPort() { return m_port; }

	/**
	 * Start acquiring data for the passed time interval
	 *
	 * @param startTime The start time of the subscan
	 * @param endTime The end time of the subscan
	 */
	void beginReceivingData(
		ACS::Time startTime,
		ACS::Time endTime);

	/**
	 * Abort the current subscan.
	 * <P>
	 * abortSubscan() causes to discard the packets received
	 * from the socket but does not close the connection
	 * with the digitizer.
	 */
	void abortSubscan();

	/**
	 * @return true when the digitizer is connected to the socket;
	 *         false otherwise
	 */
	bool isConnected() { return m_connected; }

private:
	/**
	 * The name of this DataReceiver
	 */
	ACE_CString m_name;

	/**
	 * The port used by the server for incoming connections.
	 *
	 * NOTE: to avoid races, this port is 0 until the server is ready for accepting a new connection.
	 *       before the thread starts, the port is stored in m_tempPort
	 *
	 *  To avoid temporary races, the port is set just before accepting a new connection in waitForClient..
	 *  NewTPPImpl::addDigitizer check the value of the port is not 0, before returning to the caller
	 *
	 *  @see m_tempPort
	 *  @see NewTPPImpl::addDigitizer
	 */
	int m_port;

	/**
	 * The port that will be used for listening for clients.
	 * This variable is temporary to avoid races
	 *
	 * @see m_port
	 */
	int m_tempPort;

	/*!
	 * true if the dates received from the socket must be swapped
	 */
	bool m_swapDate;

	/*!
	 * true if the samples received from the socket must be swapped
	 */
	bool m_swapSamples;

	/**
	 * The socket to listen to the incoming connections
	 * from the digitizer
	 */
	ACE_SOCK_Acceptor m_serverSocket;

	/**
	 * The socket to receive data from
	 */
	ACE_SOCK_Stream m_socket;

	/**
	 * The buffer to write data received from the socket into.
	 * <P>
	 * It is initialized to the max dimension
	 */
	unsigned char m_buffer[1+sizeof(ACS::Time)+8*255];

	/**
	 * The mutex to safely modify the buffers and the start/end times
	 */
	ACE_Recursive_Thread_Mutex m_bufferMutex;

	/**
	 * The start time of the subscan
	 * <P>
	 * A value of 0 means that no subscan is started and all the packet
	 * must be discarded.
	 * <P>
	 * The start time of the subscan does not change while acquiring data.
	 * If the data received from the digitizer are misaligned, then the time
	 * shifts and is recorded in the m_driftedStartTime.
	 */
	ACS::Time m_startTime;

	/**
	 * The end time of the subscan
	 * <P>
	 * A value of 0 means that no subscan is started and all the packet
	 * must be discared
	 */
	ACS::Time m_endTime;

	/**
	 * The drifted start time of the subscan.
	 * <P>
	 * It is equal to 0 unless the first sample of the first packet
	 * of a subscan starts some time before m_startTime and the last sample
	 * ends after m_startTime (i.e. part of first packet is before the start
	 * time and part after the start time of the subscan).
	 * If it is the case then m_driftedTime is set to be equal to the time of the
	 * first sample of such a packet (shifted back):
	 * 	- m_driftedTime<=m_startTime
	 *  - m_startTime-m_driftedTime<NetTPPImpl::sm_sampleInterval
	 *
	 *  The following shows the drifted time
	 *   subscan:          s |-------------------------|e
	 *   first sample     f|---|
	 *   Drifted d=s-f
	 */
	unsigned int m_driftedTime;

	/**
	 * Signal if the drifted time has been successfully
	 * calculated
	 */
	bool m_driftedTimeCalulated;

	/**
	 * Signal if all the samples of the current subscan have
	 * arrived.
	 * <P>
	 * When all the sample have been sent by the digitizer, this boolean
	 * signal to inform once and only once the PacketHandler object
	 * that the subscan is complete.
	 */
	volatile bool m_subscanComplete;

	bool m_debugFlag;

	/**
	 * The object to send received packets to
	 */
	PacketHandler* m_packetHandler_p;

	/**
	 * true when the digitizer is connected
	 */
	bool m_connected;

	/**
	 * true if a timeout occurred while connecting.
	 * <P>
	 * This also means that the connection never happened
	 */
	bool m_timedout;

	/**
	 * The length of the timeout (in seconds) while waiting for an incoming connection
	 */
	static const unsigned int m_timeoutInterval;

	/**
	 * true if the receiver has been shut down
	 */
	volatile bool m_closed;

	/**
	 * The thread that owns this object
	 */
	DataReceiverThread* m_thread_p;

	/**
	 * Open the server socket
	 *
	 * @return the port number of the server socket
	 * @throws SocketErrorEx in case of error creating the server socket
	 */
	int initServerSocket();

	/**
	 * Wait until the client connects
	 *
	 * @return true If client is connected to the socket,
	 * 			false otherwise
	 */
	bool waitForClient();

	/**
	 * Check if a packet read from the socket has a valid date
	 * and send it to the listener.
	 *
	 * @param samples The number of samples in the packet
	 * @parm timestamp The timestamp in the packet
	 * @param packet The packet of samples
	 *
	 * @return true if the data in the packet, or part of the data
	 * 				in the packet have been accepted because fall
	 * 				inside [startTime,endTime]
	 */
	bool handlePacket(unsigned int samples, ACS::Time timestamp,unsigned char* packet);

	/**
	 * Swap the byte order of an unsigned long long like ACS::Time
	 */
	unsigned long long swap(const unsigned long long* l);

	/**
	 * Swap the bytes of the buffer from network to
	 * host byte order.
	 *
	 * @param samples The number of samples in buffer
	 * @param The buffer contaning all the packet
	 */
	void swapSamples(int samples, unsigned char* buffer);

	/**
	 * Convert the timestamp received from the IFProc into ACS::Time units
	 */
	ACS::Time convertTimestamp(ACS::Time timestamp);

	/**
	 * Calculate the first and last valid samples in the packet.
	 *
	 * @param timestamp The timestamp of the first sample received
	 * 					from the device
	 * @param samples The number of samples
	 * @param first the index of the first valid sample in the packet (inclusive)
	 * @param last the index of the last valid sample in the packet (inclusive)
	 * @return true if the packet has at least one packet part of the subscan
	 */
	bool calculateIndexes(
			ACS::Time timestamp,
			unsigned int samples,
			unsigned int& first,
			unsigned int& last);

	/**
	 * Calculate the drifted time for a timestamp produced by the device.
	 * <P>
	 * Each sample produced by an IFProc device has a fixed time length of
	 * NewTPPImpl::sm_sampleInterval so to calculate the driftedTime we
	 * only need to know a produced timestamp whatever it is.
	 *
	 * @param timestamp The timestamp of a sample received from the device
	 * @see m_driftedTime
	 */
	void calculateDriftedTime(ACS::Time timestamp);

	/**
	 * A timeout occurred while waiting for the client to connect the socket.
	 */
	void timeout();

	/**
	 * Receive sz bytes from the socket and stores them in the passed buffer
	 *
	 * @param buffer The buffer to store the data into (can't be NULL)
	 * @param sz The number of bytes to read (must be greater then 0)
	 *
	 * @return true if all the data has been successfully received;
	 * 			false otherwise
	 */
	bool receiveData(unsigned char buffer[], unsigned int sz);
};

#endif /*DATARECEIVER_H*/
