#ifndef DIGITIZER_BASE_H
#define DIGITIZER_BASE_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-13  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ace/SString.h>

#include "NewTPPC.h"

#include "DataListener.h"

/**
 * The base class of a Digitizer containing pure virtual methods, used by the
 * DigitizersContainer.
 *
 * This interface has been introduced mainly to allow testing.
 */
class DigitizerBase {

public:

	/**
	 * Constructor
	 *
	 * @param antennaName The name of the antenna
	 * @param polarization The polarization
	 */
	DigitizerBase(const char* antennaName, CORBA::Long polarization);

	/**
	 * Destructor
	 */
	virtual ~DigitizerBase() {}

	/**
	 * Stop the threads and free the allocated resources
	 */
	virtual void shutdown()=0;

	/**
	 * Interrupt the current subscan by stopping getting data
	 * from the socket.
	 * <P>
	 * A new subscan can initiate by calling beginSubscan because the socket is not shut down
	 * by this method
	 */
	virtual void abortSubscan()=0;

	/**
	  * Begin subscan
	  *
	  * @param The SubscanInfo defining the subscan to start
	  */
	 virtual void beginSubscan(const Control::NewTPP::SubscanInfo& info,DataListener* dataListener)=0;

	/**
	 * Return the polarization of the digitizer
	 */
	const CORBA::Long getPolarization() { return m_polarization; }

	/**
	 * Return the antenna of the digitizer
	 */
	const char* getAntenna() { return m_antennaName.c_str(); }

	/**
	 * Return the name of the digitizer
	 */
	const char* getName() { return m_digitizerName.c_str(); }

	/**
	 * Compare this digitizer with another.
	 *
	 * @param d The digitizer to compare to this one
	 * @return true if the digitizers are equals i.e. they
	 * 				have the same antenna and polarization
	 */
	bool equals(DigitizerBase& d);

	/**
	 * Compare this digitizer with the passed antenna name and
	 * polarization
	 *
	 * @param antennaName the name of the antenna
	 * @param pol The polarization
	 * @return true if the digitizers has the same antenna name and
	 * 				polarization of the passed parameters
	 */
	bool equals(const char* antennaName, CORBA::Long pol);

protected:
	/**
	 * The name of the antenna
	 */
	ACE_CString m_antennaName;

	/**
	 * Polarization
	 */
	CORBA::Long m_polarization;

	/**
	 * The name of the digitizer formatted as antenna:pol
	 */
	ACE_CString m_digitizerName;
};

#endif /*! DIGITIZER_BASE_H*/
