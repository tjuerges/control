#ifndef PACKET_HANDLER_H
#define PACKET_HANDLER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-01-17  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

/**
 * The callback for receiving packets.
 * <P>
 * The packets are received by the DataReceiver that checks if they are
 * inside the subscan and notify the listeners by calling the methods
 * of this class.
 */
class PacketHandler {
public:

	/**
	 * One of the possible exit code for a subscan.
	 * <P>
	 * The socket has been closed
	 */
	static const int SOCKET_CLOSED=1;

	/**
	 * One of the possible exit code for a subscan.
	 * <P>
	 * An error reading data from the socket
	 */
	static const int SOCKET_ERROR=2;

	/**
	 * One of the possible exit code for a subscan.
	 * <P>
	 * The digitizer sent all the packets of the subscan
	 */
	static const int SUBSCAN_COMPLETE=3;

	/**
	 * One of the possible exit code for a subscan.
	 * <P>
	 * A timeout occurred while getting data from the digitizer
	 */
	static const int TIMEOUT=4;

	/**
	 * One of the possible exit code for a subscan.
	 * <P>
	 * The subscan has been aborted
	 */
	static const int ABORTED=5;

	/**
	 * Destructor
	 */
	virtual ~PacketHandler() {};

	/**
	 * This method is called whenever a new packet has been received
	 * from the socket.
	 *
	 * @param samples The total number of samples in the packet
	 * @param startTimeOfFirstSample The time of the first valid sample of the packet
	 * @param firstSampleOffset The position in the buffer of the first valid sample (inclusive)
	 * @param lastSampleOffset The position in the buffer of the last valid sample (inclusive)
	 * @param startTime The time of the first sample of the packet
	 * @param endTime The time of the last sample of the packet
	 * @param buffer The buffer containing the packet
	 * @param drif The drift if the timestamp from the device is not
	 * 			   aligned with the subscan start time
	 *
	 */
	virtual void packetReceived(
		int samples,
		ACS::Time startTimeOfFirstSample,
		int firstSampleOffset,
		int lastSampleOffset,
		ACS::Time startTime,
		ACS::Time endTime,
		unsigned char* buffer,
		unsigned int drift)=0;

	/**
	 * Inform the receiver that a subscan has terminated.
	 * <P>
	 * This method is executed in the loop that receives the
	 * data from the socket and therefore shuold be very fast
	 * in order to avoid blocking the sender too long.
	 *
	 * @param exitCode Determine how the subscan terminated
	 * @return true if the caller sould continue getting data
	 * 				from the sender of false if the socket must be
	 * 				closed
	 */
	virtual bool subscanDone(int exitCode)=0;
};

#endif /*!PACKET_HANDLER_H*/
