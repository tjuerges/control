#ifndef DIGITIZERS_CONTAINER_H
#define DIGITIZERS_CONTAINER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-13  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <vector>
#include <set>

#include <ace/Recursive_Thread_Mutex.h>

#include "DigitizerBase.h"
#include "DataListener.h"

/**
 * The container of digitizers.
 */
class DigitizersContainer {

public:
	/**
	 * Constructor
	 */
	DigitizersContainer();

	/**
	 * Destructor
	 */
	virtual ~DigitizersContainer();

	/**
	 * Add a new digitizer in the tail of the vector.
	 *
	 * The container does not allow to insert more then one
	 * digitizer for <antenna, polarization>.
	 * If the digitizer for one antenna, polarization is already in the vector,
	 * the new one is not added and false i s returned.
	 *
	 * @param newDigitizer The DigitizerBase to add
	 * @return true if the digitizer has been added to the vector;
	 * 		   false otherwise
	 */
	bool appendDigitizer(DigitizerBase* newDigitizer);

	/**
	 * Return the number of digitizers in the container
	 */
	int size();

	/**
	 * Check if the container contains a Digitizer with the passed
	 * name and polarization.
	 *
	 * @param antennaName The name of the antenna
	 * @param pol The polarization
	 * @return true If such a digitizer is already ion the container
	 */
	bool containsDigitizer(const char* antennaName, CORBA::Long pol);

	/**
	 * Check if the container contains a digitizer with the same
	 * antenna name and polarization of the passed digitizer
	 *
	 * @param d The digitizer whose antenna name and polarization are to be
	 * 			searched in the container
	 * @return true If such a digitizer is already ion the container
	 */
	bool containsDigitizer(DigitizerBase& d);

	/**
	 * Return the digitizer with the give antenna name and polarization
	 *
	 * @return the digitizer with the give antenna name and polarization
	 * 			NULL if such a digitizer does not exist in the container
	 */
	DigitizerBase* getDigitizer(const char* antennaName, CORBA::Long pol);

	/**
	 * @return A vector of polarizations for the digitizers of the passed
	 * 			antenna names
	 */
	std::set<CORBA::Long> getPolarizations(const char* antennaName);

	/**
	 * Return a copy of the names of the antennas of the
	 * digitizers in the container.
	 *
	 * @return The antenna names of the digitizers
	 */
	std::set<ACE_CString> getAntennas();

	/**
	 * Remove the digitizer with the passed antenna name and polarization from the container.
	 * <P>
	 * <B>Note</B>: the digitizer removed from the container is not deleted.
	 *
	 * @param antennaName The name of the antenna
	 * @param pol The polarization
	 * @param shutdown If true, shutdown the digitizer
	 * @return A pointer to the DigitizerBase removed from the container;
	 * 			NULL if such a deigitizer does not exist
	 */
	DigitizerBase* removeDigitizer(const char* antennaName, CORBA::Long pol, bool shutdown=false);

	/**
	 * Remove (and delete) all the digitizers from the container.
	 *
	 * clearAll is executed automatically by the destructor;
	 *
	 * @param shutdown If true, shutdown each digitizer before deleting
	 */
	void clearAll(bool shutdown=false);

	/**
	 * Remove from the container all the digitizers whose antenna name is not in the passed vector.
	 *
	 * @param antennaNames The name of the antennas of the array to check against the digitizers
	 * @param shutdown if true, the digitizer is shutdown before removal
	 * @param del if true the digitizer is deleted after removal
	 * @return the number of digitizers removed from the container
	 */
	int removeUnneededDigitizers(const std::vector<ACE_CString>& antennaNames, bool shutdown=true, bool del=true);

	/**
	 * Notify all the digitizers to interrupt the current subscan
	 */
	virtual void abortSubscan();

	/**
	  * Notify all the digitizers to begin a new subscan
	  *
	  * @param The SubscanInfo defining the subscan to start
	  */
	 virtual void beginSubscan(const Control::NewTPP::SubscanInfo& info, DataListener* dataListener);

	 /**
	 * Shutdown all the digitizers
	 */
	void shutdown();

private:
	/**
	 * The vector of digitizers
	 */
	std::vector<DigitizerBase*> m_digitizers;

	/**
	 * The mutex to safely access the digitizers vector
	 */
	ACE_Recursive_Thread_Mutex m_digitizersMutex;
};

#endif /*! DIGITIZERS_CONTAINER_H*/
