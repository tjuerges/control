#ifndef COLLECTORSVECTOR_H
#define COLLECTORSVECTOR_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-12-17  created
*/

#include <vector>

#include <ace/Thread_Mutex.h>

class DataCollectorsContainer;
#include <DataCollector.h>

/**
 *  DataCollectorsContainer is a container of dataColelctors.
 *
 *  DataCollectors are created when a new subscan starts and they can be deleted
 *  only when the data have been sent to the Distributer (i.e. the ARCHIVE).
 *
 *  In this first implementation, DataColelctors are simply stored in the vector
 *  and deleted only when the destructor is called i.e. when the process
 *  shuts down.
 */
class DataCollectorsContainer {

private:
	/**
	 * The data collector of the actual observation
	 */
	DataCollector* m_actualCollector_p;

	/**
	 * The vectors of DataCollectors
	 */
	std::vector<DataCollector*> m_collectors_v;

	/**
	 * The mutex to protect shareddata against concurrent accesses
	 */
	ACE_Recursive_Thread_Mutex m_mutex;

public:
	/**
	 * Constructor
	 */
	DataCollectorsContainer();

	/**
	 * Destructor
	 */
	virtual ~DataCollectorsContainer();

	/**
	 * Add a not NULL DataCollector.
	 * This DataColelctor becomes the actual DataCollector
	 */
	void addCollector(DataCollector *dc);

	/**
	 * Return true if the current subscan has terminated collecting data
	 * and false otherwise
	 */
	bool isCollectingData();

	/**
	 * abort the current subscan by delegating to the DataCollector
	 */
	void abortSubscan();

	/**
	 * Check if all the thread of the DataCollectors of the container
	 * are still running
	 *
	 * @return true if at least one thread is still running;
	 *         false otherwise
	 */
	bool areCollectorsRunning();
};

#endif /*!COLLECTORSVECTOR_H*/
