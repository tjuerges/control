#ifndef SUBSCANDATA_H
#define SUBSCANDATA_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-09-22  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ace/Recursive_Thread_Mutex.h>
#include <ace/SString.h>

#include "acscommonC.h"


/**
 * The data read from the digitizer for each subscan.
 * <P>
 * This class encapsulates the timestamp and the buffers of data
 * produced by the digitizer.
 * <P>
 * Objects of this class are created when a subscan finishes and sent
 * to the proper object for reducing and sending to the bulk data
 * listener.
 * <P>
 * This class take ownership of the objects and is in charge of
 * deleting them when not needed
 */
class SubscanData {

public:
	/**
	 * Constructor
	 *
	 * @param start start time of the subscan (msec)
	 * @param end   end time of the subscan (msec)
	 * @param name  The name of the digitizer producing data
	 * @param dataID The data ID of this subscan data
	 */
	SubscanData(ACS::Time start, ACS::Time end, ACE_CString name, ACE_CString dataID);

	/**
	 * Destructor
	 * <P>
	 * Delete all the BasicDataBlocks in the container
	 */
	virtual ~SubscanData();

	/**
	 * Add the samples in the buffer and updates the flags.
	 * <P>
	 * If this method executes when the subscan has already terminated
	 * it returns doing nothing and without locking the mutex.
	 * <P>
	 * This method adds in the local buffer the samples in srcBuf starting from position
	 * first to position last (i.e. adds a total of last-first+1 samples).
	 * The exact position where those samples are added depends on the timestamp and
	 * the drifted timestamp.
	 * <P>
	 * srcBuf is the buffer as it arrives from the IFProc with thsi format
	 * 	<numSamples><timestam><samp0><samp1>...
	 *
	 * @param srcBuffer The buffer with the sample received from the socket
	 * @param firstTimestamp The timestamp of the first valid sample
	 * @param first The index of the first valid sample (inclusive)
	 * @param last  The index of the last valid sample (inclusive)
	 * @param drift The drift between the subscan time and the device time
	 */
	bool addSamples(
			unsigned char* srcBuf,
			ACS::Time firstTimestamp,
			int first,
			int last,
			unsigned int drift);

	/**
	 * All the data of the subscan has been acquired from the
	 * digitizer.
	 * <P>
	 * @param code An integer identifying how the acquiring of data
	 * 				terminated
	 */
	void subscanComplete(int code);

	/**
	 * Return true if the digitizer terminated getting data
	 * from the socket with ot without an error.
	 *
	 * @return true if the digitizer terminates acquiring data.
	 */
	bool dataAcquired() { return m_dataAcquired>0; }

	/**
	 * @return the completion code of the subscan or -1
	 * 		it the subscan is still running.
	 *
	 * @see PacketHanler.h
	 */
	int getSubscanCompletion() { return m_dataAcquired; }

	/**
	 * Get the buffers received from the digitizer.
	 * <P>
	 * The buffer should be accessed only when the subscan has
	 * terminated because otherwise they can be changed:
	 * the caller should verify the return value for that.
	 *
	 * @param buffer The buffer of data
	 * @param numOfSamples The number of samples in buffer
	 * @param flag The flags
	 * @return true if the subscan has terminated and the
	 * 				buffers are valid; false otherwise
	 */
	bool getAcquiredData(unsigned char** buffer, unsigned int* numOfSamples, bool** flag);

	/**
	 * @return The start time of the subscan
	 */
	ACS::Time getStartTime() { return m_startTime; }

	/**
	 * @return The end time of the subscan
	 */
	ACS::Time getEndTime() { return m_endTime; }

	/**
	 * Return the dataID of this subscan
	 */
	ACE_CString getDataID() { return m_dataID; }


private:
	/**
	 * The start time of the subscan (ACS::Time)
	 */
	ACS::Time m_startTime;

	/**
	 * The end of the subscan  (ACS::Time)
	 */
	ACS::Time m_endTime;

	/**
	 * The data ID of this subscan
	 */
	ACE_CString m_dataID;

	/**
	 * The mutex to safely access/modify the dates and the buffer
	 * and all other internal data
	 */
	ACE_Recursive_Thread_Mutex m_mutex;

	/**
	 * The drift between the timestamp of the device and the subscan time.
	 */
	unsigned int m_drift;

	/**
	 * The number of samples of the subscan
	 * m_goodSamples_p contains m_numOfSamples booleans each of which is
	 * initialized to false;
	 * 0 if no subscan.
	 */
	unsigned int m_numOfSamples;

	/**
	 * We need to know if each sample is good or not.
	 * <P>
	 * There is one boolean for each sample whose value is true
	 * if the sample is good (i.e. arrived from the socket) and
	 * false otherwise.
	 * <P>
	 * m_goodSamples_p is NULL if no subscan.
	 * <P>
	 * m_goodSamples_p contains m_numOfSamples booleans each of which is
	 * initialized to false;
	 */
	bool* m_goodSamples_p;

	/**
	 * The number of bytes of dataBuffer.
	 * <P>
	 * 0 if no subscan.
	 */
	unsigned int m_bufferLen;

	/**
	 * The buffer for the samples.
	 * <P>
	 * The buffer contains all the samples of the subscand and
	 * is NULL when no subscan.
	 * <P>
	 * The length of this buffer is the number of samples in the subscan
	 * where each sample is composed of 2 bytes for each of the 4 baseband.
	 * <P>
	 * Each char in the m_dataBuffer_p array is initialized to 0
	 */
	unsigned char* m_dataBuffer_p;

	/**
	 * Remember if the drift has been updated:
	 * it should happen only once.
	 */
	bool m_driftUpdated;

	/**
	 * Signal that the data from the digitizer has been added.
	 * <P>
	 * This property is equal to -1 when the subscan is running
	 * and is equal to one of the PacketHandler exit code when
	 * the subscan terminated.
	 */
	int m_dataAcquired;

	/**
	 * Te name of the digitizer producing data
	 * (in the form antenna_name:polarization)
	 */
	ACE_CString m_name;

	/**
	 * Dump the buffer on stdout when the subscan has terminated.
	 * <P>
	 * This method is intended for debugging purposes only.
	 */
	void dumpBuffer();

	/**
	 * Dump the data in a file.
	 * <P>
	 * This is intended for debugging purposes only.
	 */
	void dumpOnFile();
};

#endif /*!SUBSCANDATA_H_H*/
