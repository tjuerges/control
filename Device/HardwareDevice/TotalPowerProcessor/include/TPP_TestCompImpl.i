#include "bulkDataDistributerC.h"

template<class TCallback>
TPP_TestCompImpl<TCallback>::TPP_TestCompImpl(const ACE_CString& name,maci::ContainerServices* containerServices) :
	BulkDataReceiverImpl<TCallback>(name,containerServices) {
	ACS_TRACE("TPP_TestCompImpl<>::TPP_TestCompImpl");
}

template<class TCallback>
TPP_TestCompImpl<TCallback>::~TPP_TestCompImpl() {
    ACS_TRACE("TPP_TestCompImpl<>::~TPP_TestCompImpl");
}


template<class TCallback>
void TPP_TestCompImpl<TCallback>::initialize() {
    ACS_TRACE("TPP_TestCompImpl<>::initialize");
}

template<class TCallback>
void TPP_TestCompImpl<TCallback>::execute() {
    ACS_TRACE("TPP_TestCompImpl<>::execute");
}

template<class TCallback>
void TPP_TestCompImpl<TCallback>::cleanUp() {
    ACS_TRACE("TPP_TestCompImpl<>::cleanUp");
}

template<class TCallback>
void TPP_TestCompImpl<TCallback>::aboutToAbort() {
    ACS_TRACE("TPP_TestCompImpl<>::aboutToAbort");
}
