#ifndef TPP_TIMEHELPER_H
#define TPP_TIMEHELPER_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-07-20  created
*/

/************************************************************************
 * A collection of useful methods for handling times
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "acscommonC.h"
#include <ace/Time_Value.h>

class TPP_TimeHelper {
public:
	/**
	 * return the actual time as ACS::Time
	 */
	static ACS::Time now();

	/**
	 * Return an human readable string of the passed time
	 */
	static ACE_CString toString(ACS::Time time);
};

#endif /*!TPP_TIMEHELPER_H*/
