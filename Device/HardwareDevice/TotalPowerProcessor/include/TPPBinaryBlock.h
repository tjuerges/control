#ifndef TPP_BINARY_BLOCK_H
#define TPP_BINARY_BLOCK_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-03-18  created
*/

/************************************************************************
 * TPPBinaryBlock is an helper class to build and manage an ASDM binary
 * block with the data read fromn the digitizers.
 * <P>
 * The binary block i what the component sends to the bulkdata receiver.
 * <P>
 * Methods of this class derives from IFProc/TPBulkDataSenderInterface.
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <sstream>
#include <vector>

#include "NewTPPC.h"
#include "NewTPPImpl.h"
#include "SubscanHeader.h"

#include <SDMDataObjectPartTypes.h>

class TPPBinaryBlock {
public:
	/**
	 * Constructor
	 *
	 * @param ssInfo The SubscanInfo with subscan parameters
	 */
	TPPBinaryBlock(
			SubscanHeader* hdr,
			std::vector<asdmbinaries::AUTODATATYPE>* data,
			std::vector<asdmbinaries::FLAGSTYPE>* flags,
			unsigned int dataSize);

	/**
	 * Destructor
	 */
	virtual ~TPPBinaryBlock();

	/**
	 * Write a Blob to a file
	 *
	 * @param basePath The folder where the file has to written
	 */
	void writeBlobToDisk(std::string basePath);

	/**
	 * Send the binary object to the BulkDataReceiver
	 *
	 * @param tpp The pointer to the component to get the sender
	 * @param flow The flow to send data to the receiver
	 */
	void sendBinaryobject(NewTPPImpl* tpp, CORBA::ULong flow);

	/**
	 * Return the data to send through the bulkdata sender
	 * by delegating to the TPPBinaryBlock.
	 * <P>
	 * This method returns a boolean because the binary object is build by a thread that
	 * could have not yet produced the object at the moment it is requested.
	 *
	 * @param data0_id The data ID
	 * @param data The data to send
	 * @param size The size of the data to send
	 * @return true if the binary object has been built and false otherwise
	 * 				and in this case all the returned values are invalid
	 */
	bool getData(const char*& data0_id, const char*& data, size_t& size);

private:

	/**
	 * The ASDM binary block
	 */
	std::ostringstream m_tpBlock;

	/**
	 * The pointer to the subscan header
	 */
	SubscanHeader* m_header_p;

	/**
	 * Create the binary block
	 */
	void createBinaryBlock(
			std::vector<asdmbinaries::AUTODATATYPE>* pData,
			std::vector<asdmbinaries::FLAGSTYPE>*  pFlags,
	        unsigned int length);
};

#endif /*!TPP_BINARY_BLOCK_H*/
