#ifndef DATARECEIVERTHREAD_H
#define DATARECEIVERTHREAD_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-04-22  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <acsThread.h>

#include "DataReceiver.h"

/**
 * The class extends ACS::Thread in order to read data from a separate thread.
 * <P>
 * Note that there is no reason to separate this class from DataReceiver apart for testing.
 * With this separation, it is possible to separate DataReceiver without writing a whole
 * ACS component.
 */
class DataReceiverThread: public ACS::Thread {
private:
	/**
	 * The data receiver does all the job!
	 */
	DataReceiver m_dataReceiver;

public:
	/**
	 * Constructor
	 *
	 *
	 * @param name The name of the thread
	 * @param ph The object to send received packets to
	 * @param swapDate true if the date of the packet must be swapped
	 * @param swapSamples true if the samples of the packet must be swapped
	 */
	DataReceiverThread(const ACE_CString& name,PacketHandler* ph, bool swapDate=true, bool swapSamples=true);

	/**
	 * Destructor
	 */
	virtual ~DataReceiverThread();

	/**
	 * Delegate to DataReceiver
	 *
	 * @return The port used by the server for incoming connections
	 */
	int getServerPort();

	/**
	 * Close the sockets and stops receiving data by delegating to DataReceiver
	 */
	void shutdown();

	/**
	 * Start acquiring data for the passed time interval by delegating to DataReceiver
	 *
	 * @param startTime The start time of the subscan
	 * @param endTime The end time of the subscan
	 */
	void beginReceivingData(
		ACS::Time startTime,
		ACS::Time endTime);

	/**
	 * All the data read from the socket after caling this method
	 * will be discarded.
	 */
	void abortSubscan();

	/**
	 * Delegate to DataReceiver
	 *
	 * @return true when the digitizer is connected to the socket;
	 *         false otherwise
	 */
	bool isConnected();

	/**
	 * @see ACS::Thread
	 */
	virtual void run();

private:
	/**
	 * The name of this DataReceiverThread
	 */
	ACE_CString m_name;

	/**
	 * true if the object has been shut down
	 */
	bool m_shutdown;

};

#endif /*!DATARECEIVERTHREAD_H*/


