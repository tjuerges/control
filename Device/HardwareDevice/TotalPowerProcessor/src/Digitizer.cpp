/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2009
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
#include <iostream>
#include <sstream>

#include <sys/unistd.h>
#include <sys/socket.h>
#include <netdb.h>

#include <INET_Addr.h>
#include <SOCK_Stream.h>
#include <Addr.h>
#include <Guard_T.h>

#include <acsThreadManager.h>

#include <NewTotPwrProcErr.h>

#include "Digitizer.h"
#include "TPP_TimeHelper.h"

Digitizer::Digitizer(
				const char* antennaName,
				CORBA::Long polarization,
				ACS::ThreadManager* threadManager):
		DigitizerBase(antennaName,polarization),
		m_Closed(false),
		m_dataReceiverThread_p(NULL),
		m_dataListener(NULL),
		m_listenerNotified(false),
		m_threadManager_p(threadManager),
		outStream_p(NULL),
		m_startTime(0),
		m_endTime(0),
		m_currentSubscanData_p(NULL),
		m_subscanAborted(false),
		m_subscanInprogress(false),
		m_nextSubscanListener_p(NULL),
		m_nextStartTime(0),
		m_nextEndTime(0)
{
	ACS_SHORT_LOG((LM_DEBUG,"New digitizer %s",m_digitizerName.c_str()));
	startReceivingThread(antennaName,polarization);
	m_subscanDatas_v.clear();
}

Digitizer::~Digitizer() {
	// Ensure the digitizer has been closed
	AUTO_TRACE("Digitizer::~Digitizer");
	ACS_SHORT_LOG((LM_DEBUG,"Digitizer::~Digitizer %s",m_digitizerName.c_str()));
	shutdown();
	if (m_dataReceiverThread_p!=NULL) {
		std::cout<<"Digitizer::~Digitizer deleting DataReceiverThread "<<m_digitizerName.c_str()<<std::endl;
		delete m_dataReceiverThread_p;
		m_dataReceiverThread_p=NULL;
	}
	std::cout<<"Digitizer::~Digitizer clearing subscans "<<m_digitizerName.c_str()<<std::endl;
	for (unsigned int t=0; t<m_subscanDatas_v.size(); t++) {
		if (m_subscanDatas_v[t]!=NULL) {
			delete m_subscanDatas_v[t];
		}
	}
	m_subscanDatas_v.clear();
	std::cout<<"Digitizer::~Digitizer Done "<<m_digitizerName.c_str()<<std::endl;
}

void Digitizer::shutdown() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		// Already closed
		return;
	}
	m_Closed=true;
	ACS_SHORT_LOG((LM_DEBUG,"Digitizer::shutdown: Shutting down digitizer %s",m_digitizerName.c_str()));
	if (m_dataReceiverThread_p!=NULL) {
		std::cout<<"Digitizer::shutdown shutting down DataReceiverThread "<<m_digitizerName.c_str()<<std::endl;
		m_dataReceiverThread_p->shutdown();
	}
	std::cout<<"Digitizer::shutdown Done "<<m_digitizerName.c_str()<<std::endl;
}

void Digitizer::abortSubscan() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (!m_subscanInprogress) {
		return;
	}
	m_subscanAborted=true;
	m_dataReceiverThread_p->abortSubscan();
}

void Digitizer::beginSubscan(const Control::NewTPP::SubscanInfo& info,DataListener* dataListener) {
	AUTO_TRACE("Digitizer::beginSubscan");
	ACS_SHORT_LOG((LM_DEBUG,"Digitizer %s beginSubscan %s with listener %s",m_digitizerName.c_str(),info.dataOID.in(),dataListener->getListenerName().c_str()));
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_subscanInprogress) {
		ACS_SHORT_LOG((LM_DEBUG,"Digitizer %s: subscan %s delayed till the current subscan %s terminates",m_digitizerName.c_str(),info.dataOID.in(),m_dataID.c_str()));
		if (m_nextSubscanListener_p!=NULL) {
			// There is already a subscan waiting!!!
			ACS_SHORT_LOG((LM_ERROR,"Digitizer %s: One subscan is already waiting!!! executing %s, queued %s, arrived %s",m_digitizerName.c_str(), m_dataID.c_str(),m_nextDataID.c_str(),info.dataOID.in()));
		}
		m_nextStartTime=info.startTime;
		m_nextEndTime=info.endTime;
		m_nextSubscanListener_p=dataListener;
		m_nextDataID=info.dataOID.in();
	} else {
		// No subscan running
		launchSubscan(info.startTime, info.endTime,info.dataOID.in(),dataListener);
	}

}

void Digitizer::launchSubscan(ACS::Time start, ACS::Time end,ACE_CString dataID,DataListener* dataListener) {
	AUTO_TRACE("Digitizer::launchSubscan");
	std::cout<<"Digitizer "<<m_digitizerName.c_str()<<":launchSubscan ["<<TPP_TimeHelper::toString(start)<<','<<TPP_TimeHelper::toString(end)<<']';
	std::cout<<" dataID="<<dataID.c_str()<<std::endl;
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);

	m_subscanAborted=false;
	m_subscanInprogress=true;
	m_listenerNotified=false;

	m_currentSubscanData_p= new SubscanData(start,end,m_digitizerName,dataID);
	m_subscanDatas_v.push_back(m_currentSubscanData_p);

	m_dataListener=dataListener;
	m_startTime=start;
	m_endTime=end;
	m_dataID=dataID;
	if (!m_dataReceiverThread_p->isConnected()) {
		ACS_SHORT_LOG((LM_WARNING,"Starting a subscan but digitizer %s is not (yet) connected",m_digitizerName.c_str()));
	}
	m_dataReceiverThread_p->beginReceivingData(
			start,
			end);
}

/**
 * Instantiate the DataReciver and start its thread
 */
void Digitizer::startReceivingThread(
		const char* antennaName,
		CORBA::Long polarization) {
	std::stringstream sstream;
	sstream<<"DataReceiver"<<'_';
	sstream<<m_digitizerName;
	std::string str;
	sstream>>str;
	ACE_CString name(str.c_str());
	ACS_SHORT_LOG((LM_DEBUG,"Creating receiving thread %s",name.c_str()));
	PacketHandler* ph=this;
	m_dataReceiverThread_p = m_threadManager_p->create<DataReceiverThread,PacketHandler*>(name,ph);
	ACS_SHORT_LOG((LM_DEBUG,"Starting thread %s",name.c_str()));
	m_dataReceiverThread_p->resume();
}

/**
 * A new packet has been received by the socket.
 *
 * @see PacketHandler
 */
void Digitizer::packetReceived(
		int samples,
		ACS::Time startTimeOfFirstSample,
		int firstSampleOffset,
		int lastSampleOffset,
		ACS::Time startTime,
		ACS::Time endTime,
		unsigned char* buffer,
		unsigned int drift) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	// discard packets if a subscan has not been started or the current
	// subscan has been aborted
	if (m_Closed || m_subscanAborted || !m_subscanInprogress) {
		return;
	}
	// Check parameters
	if (firstSampleOffset<0 ||
			lastSampleOffset<0 ||
			firstSampleOffset>lastSampleOffset ||
			lastSampleOffset>=samples) {
		// Wrong sample indexes inside packet!
		ACS_SHORT_LOG((LM_ERROR,"Wrong indexes [%d,%d] samples=%d ",firstSampleOffset,lastSampleOffset,samples));
		return;
	}

	if (startTimeOfFirstSample<startTime ||
			startTimeOfFirstSample>=endTime ||
			startTime>endTime ||
			startTime<=0) {
		// Wrong sample indexes inside packet!
		ACS_SHORT_LOG((LM_ERROR,"Wrong times [%d,%d] first sample at %d ",startTime,endTime,startTimeOfFirstSample));
		return;
	}

	// Store the samples in the buffer
	m_currentSubscanData_p->addSamples(
			buffer,
			startTimeOfFirstSample,
			firstSampleOffset,
			lastSampleOffset,
			drift);
}

bool Digitizer::subscanDone(int exitCode) {
	AUTO_TRACE("Digitizer::subscanDone");
	std::cout<<"Digitizer::subscanDone "<<m_digitizerName.c_str()<<' '<<m_dataID<<std::endl;
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	ACS_SHORT_LOG((LM_TRACE,"Digitizer::subscanDone %s with code %d dataID %s",m_digitizerName.c_str(),exitCode,m_dataID.c_str()));
	if (m_listenerNotified) {
		return false;
	}
	std::cout<<"===>>> subscanDone: "<<m_digitizerName.c_str()<<' '<<m_dataID.c_str()<<' ';
	if (m_Closed) {
		return false;
	}
	switch (exitCode) {
	case PacketHandler::SOCKET_CLOSED: {
		std::cout<<"socket closed"<<std::endl;
		break;
	}
	case PacketHandler::SOCKET_ERROR: {
		std::cout<<"socket error"<<std::endl;
		break;
	}
	case PacketHandler::SUBSCAN_COMPLETE: {
		std::cout<<"subscan completed ok"<<std::endl;
		break;
	}
	case PacketHandler::TIMEOUT: {
		std::cout<<"timeout"<<std::endl;
		break;
	}
	case PacketHandler::ABORTED: {
			std::cout<<"aborted"<<std::endl;
			break;
		}
	default: {
		std::cout<<"unknown exit code"<<std::endl;
		break;
	}
	}
	m_subscanInprogress=false;
	if (m_currentSubscanData_p!=NULL) {
		m_currentSubscanData_p->subscanComplete(exitCode);
	}
	if (m_dataListener!=NULL) {
		m_dataListener->digitizerDataArrived(m_digitizerName.c_str());
		m_listenerNotified=true;
	}
	if (m_nextSubscanListener_p!=NULL) {
		launchSubscan(m_nextStartTime,m_nextEndTime,m_nextDataID,m_nextSubscanListener_p);
		m_nextSubscanListener_p=NULL;
		m_nextEndTime=0;
		m_nextStartTime=0;
		m_nextDataID="";
	}
	return m_listenerNotified;
}

bool Digitizer::isConnected() {
	if (m_dataReceiverThread_p==NULL) {
		return false;
	}
	return m_dataReceiverThread_p->isConnected();
}
bool Digitizer::isGettingData(ACE_CString dataID) {
	AUTO_TRACE("Digitizer::isGettingData");
	//ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	std::cout<<"Digitizer::isGettingData("<<dataID.c_str()<<") on digitizer "<<m_digitizerName.c_str();
	std::cout<<"; current data ID="<<m_dataID.c_str()<<std::endl;
	if (dataID==m_dataID) {
		return m_subscanInprogress;
	}
	return false;
}

SubscanData* Digitizer::getSubscanData(ACE_CString dataID) {
	AUTO_TRACE("Digitizer::getSubscanData");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	for (unsigned int t=0; t<m_subscanDatas_v.size(); t++) {
		if (m_subscanDatas_v[t]!=NULL && m_subscanDatas_v[t]->getDataID()==dataID) {
			return m_subscanDatas_v[t];
		}
	}
	return NULL;
}

void Digitizer::releaseSubscanData(ACE_CString dataID) {
	AUTO_TRACE("Digitizer::releaseSubscanData");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	for (unsigned int t=0; t<m_subscanDatas_v.size(); t++) {
		if (m_subscanDatas_v[t]!=NULL && m_subscanDatas_v[t]->getDataID()==dataID) {
			delete m_subscanDatas_v[t];
			m_subscanDatas_v[t]=NULL;
		}
	}
}
