/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-02-04  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "NewTPPImpl.h"
#include "SubscanHeader.h"


SubscanHeader::SubscanHeader(
		const Control::NewTPP::SubscanInfo& info,
		int avgLength,
		AntennaContainer* antennaList):
		m_subscanInfo(info),
		m_averagingLength(avgLength),
		m_antennaList(antennaList)
{

}

int SubscanHeader::getNumIntegration() {
	return (int)(getInterval()/(NewTPPImpl::sm_sampleInterval*m_averagingLength));
}

int SubscanHeader::getNumberOfAntennas() {
	return m_antennaList->size();
}

int SubscanHeader::getPolarizationIndex(int polarizaton) {
	int ret=-1;
	for (unsigned int t=0; t<m_subscanInfo.polarization.length(); t++) {
		if (m_subscanInfo.polarization[t]==polarizaton) {
			ret=t;
			break;
		}
	}
	return ret;
}
/*___oOo___*/
