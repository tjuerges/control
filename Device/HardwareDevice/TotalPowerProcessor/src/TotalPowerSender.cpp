/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2007 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * "@(#) $Id$"
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * jkern    2007-03-20  created
 * acaproni 2010-03-29 Imported and adapted from IFProc module
 */

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);


#include <TotalPowerSender.h>
#include <iostream>
#include <utilBulkUserParam.h>
#include <ACSErrTypeCommon.h>

using std::string;

TotalPowerSender::TotalPowerSender(const char* distributorName,maci::ContainerServices* containerServices):
	AcsBulkdata::BulkDataSender<BulkDataSenderDefaultCallback>(),
	m_contSvcs_p(containerServices)
{
    const char* __METHOD__ = "TotalPowerSender::TotalPowerSender";
    AUTO_TRACE(__METHOD__);
    if (distributorName==NULL || containerServices==NULL) {
    	ACSErrTypeCommon::BadParameterExImpl ex (__FILE__, __LINE__, __func__);
    	ex.addData("distributorName or containerServices","NULL");
    	throw ex;
    }
    if (strlen(distributorName)==0) {
    	ACSErrTypeCommon::BadParameterExImpl ex (__FILE__, __LINE__, __func__);
		ex.addData("distributorName","Empty string");
		throw ex;
    }

    // Connect the distributer
    try{
        connect(distributorName);
    } catch(ACSErrTypeCommon::CouldntPerformActionExImpl &ex) {
    	ACSErrTypeCommon::CouldntPerformActionExImpl newEx(ex,__FILE__, __LINE__, __func__);
        throw newEx;
    } catch(...) {
    	ACSErrTypeCommon::CouldntPerformActionExImpl newEx(__FILE__, __LINE__, __func__);
		throw newEx;
    }       
}

TotalPowerSender::~TotalPowerSender() {
    const char* __METHOD__ = "TotalPowerSender::~TotalPowerSender";
    AUTO_TRACE(__METHOD__);
    disconnect();
}

void TotalPowerSender::connect(const char* distributorName)
    throw (ACSErrTypeCommon::CouldntPerformActionExImpl) {
        const char* __METHOD__ = "TotalPowerSender::connect";
        AUTO_TRACE(__METHOD__);

        ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                (LM_INFO, "Connecting to distributor %s",
                 distributorName));

        /* Now we want to get a reference to the TotalPower Distributor component*/
        try {
            distributorRef_v =  m_contSvcs_p->getComponent<bulkdata::BulkDataDistributer>
                (distributorName);
        } catch (maciErrType::CannotGetComponentExImpl& ex){
            ACS_LOG(LM_SOURCE_INFO,__METHOD__,
                    (LM_ERROR, "Exception getting reference to %s Component",
                     distributorName));
            ex.log();
            throw ACSErrTypeCommon::CouldntPerformActionExImpl(ex,__FILE__,__LINE__,__METHOD__);
        }
        catch(...) {
            throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
        }

        /* when the passed object reference points to no where then complain. */
        if ( CORBA::is_nil(distributorRef_v) ) {
            ACS_LOG(LM_SOURCE_INFO,__METHOD__,
                    (LM_ERROR, "Received Data Distributor reference is nil!"));
            throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
        }

        /* initialization of the sender object */
        try {
            /* This is the expected bulkDataSender sequence of actions for
             * connecting with our peer.
             */
            initialize();
            createMultipleFlows("TCP");
            distributorRef_v->openReceiver();

            bulkdata::BulkDataReceiverConfig_var receiverConfig_v =
                distributorRef_v->getReceiverConfig();

            if (receiverConfig_v == NULL) {
                ACS_LOG(LM_SOURCE_INFO,__METHOD__,
                        (LM_ERROR, "Receiver Configureation is  Null"));
                throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
            }

            connectToPeer(receiverConfig_v);

            /* Roberto Cirami has suggested this addition to the CORR group,
             *  I and just copying it over.
             */
            bulkdata::BulkDataReceiverDistr_var distrObj_p =
               bulkdata::BulkDataReceiverDistr::_narrow(distributorRef_v);
            if (CORBA::is_nil(distrObj_p.in())) {
                ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                        (LM_ERROR,"Failed to narrow distributor object"));
                throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
            }
            distrObj_p->setReceiver(receiverConfig_v);
        }//catch exceptions individually!!!!!!!!!!
        catch(...) {
            ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                    (LM_ERROR, 
                     "Exception while attempting to initialize sender object!"));
            throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
        }
    }

void TotalPowerSender::disconnect() {
    const char* __METHOD__ = "TotalPowerSender::disconnect";
    AUTO_TRACE(__METHOD__);

    /* if the receiver is not available then there is nothing to do */
    if (CORBA::is_nil(distributorRef_v)) {
        ACS_LOG(LM_SOURCE_INFO, __METHOD__, 
                (LM_INFO, "Request for disconnect, even though not connected"));
        printf("Request for disconnect, even though not connected\n");
        return;
    }

    try {
        int count = 0;

        /* Check each flow to be complete */
        CompletionImpl compFlow1 = distributorRef_v->getCbStatus(1);
        //CompletionImpl compFlow2 = distributorRef_v->getCbStatus(2);
        ACSErr::CompletionCode f1 = compFlow1.getCode();
        //ACSErr::CompletionCode f2 = compFlow2.getCode();

        /* wait for at most 10 seconds for the flows to be ready or timed out */
        while ((f1 != ACSBulkDataStatus::AVCbReady &&
                    f1 != ACSBulkDataStatus::AVCbTimeout)
                && count < 60 ) {
            ACE_OS::sleep(1);
            count++;

            f1 = compFlow1.getCode();
            //f2 = compFlow2.getCode();
        }

        // if waiting for the flows for being ready timeout out then
        // exit here with an error
        //
        if ( count < 60 ) {
            ACS_LOG(LM_SOURCE_INFO, __METHOD__, 
                    (LM_INFO, "Ready for disconnection after %d seconds", count));
        } else {
            ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                    (LM_ERROR,"Flows not ready for disconnecting"))
            printf("Flows not ready for disconnecting\n");
                return;
        }
    } catch ( ACSBulkDataError::AVInvalidFlowNumberExImpl &ex ) {
        ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                (LM_ERROR, 
                 "Invalid flow number exception when checking flows status!"));
        printf("Invalid flow number exception when checking flows status!\n");
        return;
    } catch ( CORBA::SystemException &ex ) {
        ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                (LM_ERROR, "Corba exception when checking flows status!"));
        printf("Corba exception when checking flows status!\n");
        return;
    } catch ( ... ) {
        ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                (LM_ERROR, "Unexpected exception when checking flows status!"));
        printf("Unexpected exception when checking flows status!\n");
        return;
    }

    // Diconnect sender and close receiver
    try {
        ACS_LOG(LM_SOURCE_INFO, __METHOD__,(LM_INFO, "disconnecting peer ..."));
        disconnectPeer();
        ACE_OS::sleep(5);
        ACS_LOG(LM_SOURCE_INFO, __METHOD__,(LM_INFO, "closing receiver ..."));
        distributorRef_v->closeReceiver();

        ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                (LM_INFO, "releasing receiver reference ..."));
        m_contSvcs_p->releaseComponent(distributorRef_v->name());
        distributorRef_v = bulkdata::BulkDataDistributer::_nil();
    } catch(...) {
        ACS_LOG(LM_SOURCE_INFO, __METHOD__,
                (LM_ERROR, "Got an exception while diconnecting peer!"));
        printf("Got an exception while diconnecting peer!\n");
        return;
    }
}

/*___oOo___*/
