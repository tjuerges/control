/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-07-20  created 
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <iostream>
#include <sstream>
#include <time.h>

#include "acstimeTimeUtil.h"
#include <acstimeEpochHelper.h>

#include "TPP_TimeHelper.h"

ACS::Time  TPP_TimeHelper::now() {
		struct timeval tv;
		gettimeofday(&tv,NULL);
		ACE_Time_Value now(tv);
		acstime::Epoch ep=TimeUtil::ace2epoch(now);
		return ep.value;
}

ACE_CString TPP_TimeHelper::toString(ACS::Time time) {
	EpochHelper  eh(time);
	std::stringstream sstream;
	sstream<<eh.year()<<'-'<<eh.month()<<'-'<<eh.day()<<'T'<<eh.hour()<<':'<<eh.minute()<<':'<<eh.second()<<'.'<<eh.microSecond();
	ACE_CString ret(sstream.str().c_str());
	return ret;
}

/*___oOo___*/
