/* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-09-22  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <Guard_T.h>
#include <string>
#include <sstream>
#include <fstream>

#include <SubscanData.h>
#include <NewTPPImpl.h>
#include <TPP_TimeHelper.h>

SubscanData::SubscanData(ACS::Time start, ACS::Time end, ACE_CString name, ACE_CString dataID):
	m_startTime(start),
	m_endTime(end),
	m_dataID(dataID),
	m_drift(0),
	m_numOfSamples((end-start)/NewTPPImpl::sm_sampleInterval),
	m_goodSamples_p(new bool[m_numOfSamples]),
	m_bufferLen(m_numOfSamples*2*NewTPPImpl::sm_channelsPerSample),
	m_dataBuffer_p(new unsigned char[m_bufferLen]),
	m_driftUpdated(false),
	m_dataAcquired(-1),
	m_name(name)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	ACS_SHORT_LOG((LM_DEBUG,"SubscanData for %s allocated a buffer for %d samples",m_name.c_str(),m_numOfSamples));
	// We need to know if each sample is good or not
	//
	// Initially mark all samples as bad (false) because we have
	// not yet received anything.
	for (unsigned int t=0; t<m_numOfSamples; t++) {
		m_goodSamples_p[t]=false;
	}
	// Clear the buffer
	for (unsigned int t=0; t<m_bufferLen; t++) {
		m_dataBuffer_p[t]=0;
	}
}

SubscanData::~SubscanData()
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	m_bufferLen=m_numOfSamples=0;
	if (m_dataBuffer_p!=NULL) {
		delete[] m_dataBuffer_p;
		m_dataBuffer_p=NULL;
	}
	if (m_goodSamples_p!=NULL) {
		delete[] m_goodSamples_p;
		m_goodSamples_p=NULL;
	}
}

bool SubscanData::addSamples(
		unsigned char* srcBuf,
		ACS::Time firstTimestamp,
		int first,
		int last,
		unsigned int drift) {

	// No reason to block if all the data has been received.
	if (m_dataAcquired>0) {
		return false;
	}

	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);

	// Drift can be set only once when the first valid sample arrives.
	if (!m_driftUpdated) {
		m_drift=drift;
		// Check if the drifted time >= subscan startTime
		// It should never happen because DataReceiver checks that
		// Try to recover rejecting the packet in fact a drifted timestamp less then
		// the subscan start time could trigger an error (illegal access)
		// while storing samples in the buffer
		if (m_drift>=NewTPPImpl::sm_sampleInterval) {
			ACS_SHORT_LOG((LM_ERROR,"The drift can't be greater then the size of the interval [0<=%d<=%d].",m_drift,NewTPPImpl::sm_sampleInterval));
			return false;
		}
		m_driftUpdated=true;
	} else {
		// Check for correctness because the driftedStartTime
		// should be set only once for each subscan
		if (m_drift!=drift) {
			ACS_SHORT_LOG((LM_ERROR,"Drift already set: packet discarded."));
			return false;
		}
	}

	// The number of samples in the packet to copy the buffer.
	//
	// If the packet is the first or the last of a subscan then
	// this number can be different from the total number of
	// samples in the packet.
	int samplesToStore=last-first+1; // Indexes are inclusive

//	std::cout<<"storeSamples["<<first<<","<<last<<"] ";
//	std::cout<<" samples="<<samplesToStore;
//	std::cout<<" Tot. buffer size="<<m_bufferLen;
//	std::cout<<" Expected samples="<<m_numOfSamples;
//	std::cout<<"; value of first samples is "<<(int)srcBuf[1+sizeof(ACS::Time)+(first)*2*NewTPPImpl::sm_channelsPerSample]<<std::endl;

	// The index of the first sample in the buffer.
	//
	// Each sample is composed of 2*NewTPPImpl::sm_channelsPerSample bytes
	int startBufferPos=8*((firstTimestamp+drift)-m_startTime)/NewTPPImpl::sm_sampleInterval;
	if (startBufferPos%8!=0) {
		ACS_SHORT_LOG((LM_ERROR,"Calculated a wrong position [%d] to store the packet: packet rejected",startBufferPos));
		return false;
	}

	if (startBufferPos<0 || startBufferPos>(int)m_bufferLen-8) {
		ACS_SHORT_LOG((LM_ERROR,"Index of first sample out of index %d (num of samples %d)",startBufferPos,m_bufferLen));
		return false;
	}
	// The index of the last sample in the buffer
	//
	// Each sample is composed of 2*NewTPPImpl::sm_channelsPerSample bytes
	int lastBufferPos=startBufferPos+samplesToStore*2*NewTPPImpl::sm_channelsPerSample-1;
	if (lastBufferPos<0 || lastBufferPos>=(int)m_bufferLen) {
		ACS_SHORT_LOG((LM_ERROR,"Index of last sample out of index %d (num of samples %d)",lastBufferPos,m_bufferLen));
		return false;
	}

	// Store the samples
	unsigned int size=samplesToStore*2*NewTPPImpl::sm_channelsPerSample;
	unsigned int posInSrcBuffer=1+sizeof(ACS::Time)+(first)*2*NewTPPImpl::sm_channelsPerSample;
	//std::cout<<"Copying "<<size<<" bytes into offset "<<startBufferPos;
	//std::cout<<" from source offset "<<posInSrcBuffer<<std::endl;
	memcpy(&m_dataBuffer_p[startBufferPos],&srcBuf[posInSrcBuffer],size);

	// Update the flags
	//std::cout<<"Setting "<<samplesToStore<<" flags to TRUE starting from index "<<startBufferPos/8<<std::endl;
	for (int t=0; t<samplesToStore; t++) {
		m_goodSamples_p[startBufferPos/8+t]=true;
	}

//	std::cout<<"**********************************************************************\n";
//	for (unsigned int t=0; t<m_bufferLen; t++) {
//		if (t%8==0) {
//			std::cout<<' ';
//		}
//		std::cout<<(unsigned int)m_dataBuffer_p[t];
//	}
//	std::cout<<"\n**********************************************************************\n\n";
	return true;
}

void SubscanData::subscanComplete(int code) {
	AUTO_TRACE("SubscanData::subscanComplete");
	ACS_SHORT_LOG((LM_DEBUG,"SubscanData:;subscanComplete with code %d for digitizer %s",code,m_name.c_str()))
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	m_dataAcquired=code;
	int nFlaggedWrong=0;
	for (unsigned int t=0; t<m_numOfSamples; t++) {
		if (!m_goodSamples_p[t]) {
			nFlaggedWrong++;
		}
	}
	ACS_SHORT_LOG((LM_DEBUG,"Subscan of %s contains %d wrong samples out of %d",m_name.c_str(),(int)nFlaggedWrong,(int)m_numOfSamples));
	// dumpBuffer();
	// dumpOnFile();
}

void SubscanData::dumpBuffer() {
	std::cout<<"Dumping buffer "<<m_name.c_str()<<'\n';
	std::cout<<"VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV\n";
	std::cout<<"Samples: "<<m_numOfSamples<<std::endl;
	for (unsigned int t=0; t<m_numOfSamples; t++) {
		if (m_goodSamples_p[t]) {
			// Print only valid samples
			std::cout<<t<<"> TRUE";
			for (unsigned int j=0; j<2*NewTPPImpl::sm_channelsPerSample; j++) {
				std::cout<<' '<<(int)m_dataBuffer_p[t*2*NewTPPImpl::sm_channelsPerSample+j];
			}
			std::cout<<' ';
			for (unsigned int j=0; j<2*NewTPPImpl::sm_channelsPerSample; j+=2) {
				unsigned short tmp;
				unsigned char* d0=&m_dataBuffer_p[t*2*NewTPPImpl::sm_channelsPerSample+j];
				unsigned char* d1=&m_dataBuffer_p[t*2*NewTPPImpl::sm_channelsPerSample+j+1];
				unsigned char* c=(unsigned char*)&tmp;
				*c=*d0;
				c++;
				*c=*d1;
				std::cout<<' '<<tmp;
			}
			std::cout<<std::endl;
		} else {
			std::cout<<t<<"> FALSE"<<std::endl;
		}
	}
	std::cout<<"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
}

void SubscanData::dumpOnFile() {
	std::stringstream sstream;
	sstream<<"/users/almaproc/SubscanData_";
	sstream<<time(NULL)<<m_name.c_str()<<".bin";
	std::string fileName;
	sstream>>fileName;
	std::ofstream ofs(fileName.c_str(),ios::binary|ios::out);
	ofs<<m_name.c_str()<<' '<<m_dataID.c_str()<<std::endl;
	ofs<<'['<<TPP_TimeHelper::toString(m_startTime).c_str()<<',';
	ofs<<TPP_TimeHelper::toString(m_endTime).c_str()<<']'<<std::endl<<std::endl;
	std::cout<<"SubscanData::dumpOnFile() "<<fileName<<std::endl;
	for (unsigned int t=0; t<m_numOfSamples; t++) {
		if (m_goodSamples_p[t]) {
			// Print only valid samples
			ofs<<t<<"> TRUE";
			for (unsigned int j=0; j<2*NewTPPImpl::sm_channelsPerSample; j++) {
				ofs<<' '<<(int)m_dataBuffer_p[t*2*NewTPPImpl::sm_channelsPerSample+j];
			}
			ofs<<'\t';
			for (unsigned int j=0; j<2*NewTPPImpl::sm_channelsPerSample; j+=2) {
				unsigned short tmp;
				unsigned char* d0=&m_dataBuffer_p[t*2*NewTPPImpl::sm_channelsPerSample+j];
				unsigned char* d1=&m_dataBuffer_p[t*2*NewTPPImpl::sm_channelsPerSample+j+1];
				unsigned char* c=(unsigned char*)&tmp;
				*c=*d0;
				c++;
				*c=*d1;
				ofs<<' '<<tmp;
			}
			ofs<<std::endl;
		} else {
			ofs<<t<<"> FALSE"<<std::endl;
		}
	}
	ofs.flush();
	ofs.close();
}

bool SubscanData::getAcquiredData(unsigned char** buffer, unsigned int* numOfSamples, bool** flag) {
	if (!this->dataAcquired()) {
		*buffer=NULL;
		*flag=NULL;
		*numOfSamples=0;
		return false;
	}
	*buffer=m_dataBuffer_p;
	*numOfSamples=m_numOfSamples;
	*flag=m_goodSamples_p;
	return true;
}
/*___oOo___*/
