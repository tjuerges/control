/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
<<<<<<< DigitizersContainer.cpp
* "@(#) $Id$"
=======
* "@(#) $Id$"
>>>>>>> 1.11.2.2
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-13  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <String_Base.h>
#include <Guard_T.h>

#include "loggingMACROS.h"
#include "DigitizersContainer.h"

DigitizersContainer::DigitizersContainer() {
	m_digitizers.clear();
}

DigitizersContainer::~DigitizersContainer() {
	clearAll(true);
}

bool DigitizersContainer::appendDigitizer(DigitizerBase* newDigitizer) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	if (newDigitizer==NULL) {
		return false;
	}
	if (this->containsDigitizer(*newDigitizer)) {
		return false;
	}
	m_digitizers.push_back(newDigitizer);
	return true;
}

int DigitizersContainer::size() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	int sz= m_digitizers.size();
	return sz;
}

bool DigitizersContainer::containsDigitizer(const char* antennaName, CORBA::Long pol) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	bool ret=false;
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		if (m_digitizers[t]->equals(antennaName,pol)) {
			ret=true;
			break;
		}
	}
	return ret;
}


bool DigitizersContainer::containsDigitizer(DigitizerBase& d) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	bool ret=false;
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		if (m_digitizers[t]->equals(d)) {
			ret=true;
			break;
		}
	}
	return ret;
}

DigitizerBase* DigitizersContainer::getDigitizer(const char* antennaName, CORBA::Long pol) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	DigitizerBase* ret=NULL;
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		if (m_digitizers[t]->equals(antennaName,pol)) {
			ret=m_digitizers[t];
			break;
		}
	}
	return ret;
}

DigitizerBase* DigitizersContainer::removeDigitizer(const char* antennaName, CORBA::Long pol, bool shutdown) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	DigitizerBase* ret=NULL;
	std::vector<DigitizerBase*>::iterator iter;
	for (iter=m_digitizers.begin(); iter!=m_digitizers.end(); iter++) {
		if ((*iter)->equals(antennaName,pol)) {
			ret=*iter;
			if (shutdown) {
				(*iter)->shutdown();
			}
			m_digitizers.erase(iter);
			break;
		}
	}
	return ret;
}

void DigitizersContainer::clearAll(bool shutdown) {
	AUTO_TRACE("DigitizersContainer::clearAll");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	std::vector<DigitizerBase*>::iterator iter;
	for (iter=m_digitizers.begin(); iter!=m_digitizers.end(); iter++) {
		if (shutdown) {
			(*iter)->shutdown();
		}
		delete (*iter);
	}
	m_digitizers.clear();
}

int DigitizersContainer::removeUnneededDigitizers(const std::vector<ACE_CString>& antennaNames, bool shutdown, bool del)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	int ret=0; // The number of removed digitizers
	if (m_digitizers.empty()) {
		return ret;
	}

	std::vector<DigitizerBase*>::iterator iter=m_digitizers.begin();
	while (iter!=m_digitizers.end()) {
		bool found=false;
		// Check if the name of the antenna is somwhere in antennaNames
		for (unsigned int t=0; t<antennaNames.size(); t++) {
			if (antennaNames[t]==(*iter)->getAntenna()) {
				found=true;
				break;
			}
		}
		if (found) {
			iter++;
			continue;
		}
		// Antenna not found ==> remove the digitizer
		if (shutdown) {
			(*iter)->shutdown();
		}
		if (del) {
			delete (*iter);
		}
		iter=m_digitizers.erase(iter);
		ret++;
	}
	return ret;
}

void DigitizersContainer::abortSubscan() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		m_digitizers[t]->abortSubscan();
	}
}

void DigitizersContainer::beginSubscan(const Control::NewTPP::SubscanInfo& info, DataListener* dataListener) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		m_digitizers[t]->beginSubscan(info,dataListener);
	}
}

std::set<CORBA::Long> DigitizersContainer::getPolarizations(const char* antennaName) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	std::set<CORBA::Long> ret;
	ACE_CString name(antennaName);
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		ACE_CString antName(m_digitizers[t]->getAntenna());
		if (name!=antName) {
			continue;
		}
		ret.insert(m_digitizers[t]->getPolarization());
	}
	return ret;
}

std::set<ACE_CString> DigitizersContainer::getAntennas() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	std::set<ACE_CString> ret;
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		ACE_CString name(m_digitizers[t]->getAntenna());
		ret.insert(name);
	}
	return ret;
}

void DigitizersContainer::shutdown() {
	AUTO_TRACE("DigitizersContainer::shutdown");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_digitizersMutex);
	for (unsigned int t=0; t<m_digitizers.size(); t++) {
		m_digitizers[t]->shutdown();
	}
}

/*___oOo___*/
