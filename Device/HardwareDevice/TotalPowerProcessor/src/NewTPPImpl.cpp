/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2009
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
#include <unistd.h>
#include <iostream>
#include <cmath>
#include <string>
#include <time.h>

#include "ControlAntennaInterfacesS.h"

#include <NewTotPwrProcErr.h>

class DataCollectorParam;
class DataCollector;

#include "NewTPPImpl.h"
#include "Digitizer.h"
#include "TPP_TimeHelper.h"

/////////////////////////////////////////////

///// Constructor
/////////////////////////////////////////////

NewTPPImpl::NewTPPImpl(
		const ACE_CString& name,
		maci::ContainerServices* containerServices) :
			acscomponent::ACSComponentImpl(name,containerServices),
			m_dataProduced(0),
			m_flaggedData(0),
			m_numIntegrations(0),
			m_averagingLength(1),
			m_bulkDataSender_p(NULL),
			m_Closed(false),
			m_distributerName()
{
	m_antennas_p = new AntennaContainer(m_digitizers);
	m_threadManager_p=containerServices->getThreadManager();
}

NewTPPImpl::~NewTPPImpl() {
	AUTO_TRACE("NewTPPImpl::~NewTPPImpl");
	if (m_antennas_p!=NULL) {
		std::cout<<"NewTPPImpl::~NewTPPImpl Deleting m_antennas_p"<<std::endl;
		delete m_antennas_p;
		m_antennas_p=NULL;
	}
	std::cout<<"NewTPPImpl::~NewTPPImpl Deleting m_antennas_p done"<<std::endl;
}

/////////////////////////////////////////////
///// Life cycle
/////////////////////////////////////////////

void NewTPPImpl::initialize()
{

	AUTO_TRACE("NewTPPImpl::initialize");

	try {
		initNetAddress();
	} catch (NewTotPwrProcErr::InvalidIPExImpl &ex) {
		throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__,__LINE__,"NewTPPImpl::initialize");
	}
	ACS_SHORT_LOG((LM_DEBUG,"Network address: [%d,%d,%d,%d]",m_IP_Bytes[0],m_IP_Bytes[1],m_IP_Bytes[2],m_IP_Bytes[3]));

	// Start the subscan scheduler
	SubscanSchedulerParam param;
	param.m_tpp_p=this;
	m_scheduler_p=m_threadManager_p->create<SubscanScheduler,SubscanSchedulerParam>("SubscanScheduler",param);
	m_scheduler_p->resume();
}

void NewTPPImpl::execute()
{
	AUTO_TRACE("NewTPPImpl::execute");
}

void NewTPPImpl::cleanUp()
{
	AUTO_TRACE("NewTPPImpl::cleanUp");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	shutdown();
	std::cout<<"NewTPPImpl::cleanUp aborting subscan"<<std::endl;
	abortSubscan();
	std::cout<<"NewTPPImpl::cleanUp Clearing digitizers"<<std::endl;
	m_digitizers.clearAll(true);
	std::cout<<"NewTPPImpl::cleanUp Clearing antennas"<<std::endl;
	m_antennas_p->clear();
	if (m_bulkDataSender_p!=NULL) {
		std::cout<<"NewTPPImpl::cleanUp Deleting m_bulkDataSender_p"<<std::endl;
		delete m_bulkDataSender_p;
		m_bulkDataSender_p=NULL;
	}
	std::cout<<"NewTPPImpl::cleanUp m_bulkDataSender_p deleted"<<std::endl;
	if (m_scheduler_p!=NULL) {
		std::cout<<"NewTPPImpl::cleanUp Deleting m_scheduler_p"<<std::endl;
		delete m_scheduler_p;
		m_scheduler_p=NULL;
	}
	std::cout<<"NewTPPImpl::cleanUp m_scheduler_p deleted"<<std::endl;
	std::cout<<"NewTPPImpl::cleanUp CleanUp done"<<std::endl;
}


void NewTPPImpl::aboutToAbort()
{
	AUTO_TRACE("NewTPPImpl::aboutToAbort");
	shutdown();
}

/////////////////////////////////////////////
///// IDL methods
/////////////////////////////////////////////

Control::NewTPP::ConnectionInfo NewTPPImpl::addDigitizer(const char* antennaName, CORBA::Long polarization)
{
	ACS_SHORT_LOG((LM_DEBUG,"Adding digitizer for %s:%d",antennaName,polarization));
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	if (!m_antennas_p->contains(antennaName)) {
		NewTotPwrProcErr::UnknownAntennaNameExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Unknown antenna %s",antennaName));
		throw ex.getUnknownAntennaNameEx();
	}
	Control::NewTPP::ConnectionInfo ret;
	for (int t=0; t<4; t++) {
		ret.IpAddress[t]=m_IP_Bytes[t];
	}
	if (m_digitizers.containsDigitizer(antennaName,polarization)) {
		ACS_SHORT_LOG((LM_DEBUG,"Removing old digitizer for antenna %s, polarization %d",antennaName,polarization));
		// Remove the old digitizer with the same antenna name
		// and polarization
		DigitizerBase* oldDigitizer=m_digitizers.removeDigitizer(antennaName,polarization,true);
		if (oldDigitizer!=NULL) {
			delete oldDigitizer;
		}
	}

	Digitizer* newDigitizer= new Digitizer(antennaName,polarization,m_threadManager_p);

	ret.portNumber=ntohs(newDigitizer->getServerPort());
	// The port is not 0, only when the server (in DataReceiver) is waiting for a connection
	// @see DataReceiver::m_port
	while (ret.portNumber==0) {
		usleep(50);
		ret.portNumber=ntohs(newDigitizer->getServerPort());
	}

	m_digitizers.appendDigitizer(newDigitizer);
	ACS_SHORT_LOG((LM_DEBUG,"Waiting digitizer %s_%d connection at %d.%d.%d.%d:%d",antennaName,polarization,ret.IpAddress[0],ret.IpAddress[1],ret.IpAddress[2],ret.IpAddress[3],ret.portNumber));
	ACS_SHORT_LOG((LM_DEBUG,"TotalPowerProcessor has now %d digitizers",m_digitizers.size()));
	return ret;
}

void NewTPPImpl::shutdown()
{
	AUTO_TRACE("NewTPPImpl::shutdown");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		// already shut down
		return;
	}
	m_Closed=true;
	std::cout<<"NewTPPImpl::shutdown Shutting down the scheduler"<<std::endl;
	m_scheduler_p->shutdown();
	std::cout<<"Waiting for the termination of all the DataScheduler threads"<<std::endl;
	while (m_dataCollectors.areCollectorsRunning()) {
		std::cout<<"At least one DataCollectors thread is still alive!"<<std::endl;
		usleep(250);
	}
	// shut down the digitizers
	std::cout<<"NewTPPImpl::shutdown Shutting down the digitizers"<<std::endl;
	m_digitizers.shutdown();
	std::cout<<"NewTPPImpl::shutdown Done"<<std::endl;
	ACS_SHORT_LOG((LM_DEBUG,"Process is shut down"));
}

void NewTPPImpl::configureArray(const Control::AntennaSeq& antennas) {
	AUTO_TRACE("NewTPPImpl::configureArray");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	m_antennas_p->setArray(antennas);
}

void NewTPPImpl::setIntegrationDuration(CORBA::Double duration) {
	AUTO_TRACE("NewTPPImpl::setIntegrationDuration");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	m_duration=duration;
	m_averagingLength = (int)round(sm_defaultSampleRate*m_duration);
	if (m_averagingLength < 1) {
		m_averagingLength = 1;
	}
	ACS_SHORT_LOG((LM_DEBUG,"Duration %f, averaging length %d",m_duration,m_averagingLength));
}

CORBA::Double NewTPPImpl::getIntegrationDuration() {
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	return m_duration;
}

void NewTPPImpl::abortSubscan() {
	AUTO_TRACE("NewTPPImpl::abortSubscan");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
        return;
    }
	std::cout<<"NewTPPImpl::abortSubscan Aborting subscan on digitizers"<<std::endl;
	m_digitizers.abortSubscan();
	std::cout<<"NewTPPImpl::abortSubscan Aborting subscan on DataColelctor"<<std::endl;
	m_dataCollectors.abortSubscan();
	std::cout<<"NewTPPImpl::abortSubscan Done"<<std::endl;
}

void NewTPPImpl::beginSubscan(const Control::NewTPP::SubscanInfo& info) {
	AUTO_TRACE("NewTPPImpl::beginSubscan");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}

	// Check if the distributer has been set
	if (m_distributerName.length()==0) {
		NewTotPwrProcErr::NoDistributerExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"No distributor set"));
		throw ex;
	}

	// Check if the array contains some antenna
	if (m_antennas_p->size()==0) {
		NewTotPwrProcErr::NoAntennasExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"No antennas"));
		throw ex;
	}

	// Check the content of the baseband
	// It must be not empty and contain at the most one entry
	// for each channel
	if (info.baseband.length()==0 || info.baseband.length()>4) {
		NewTotPwrProcErr::NoBasebandExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"No baseband"));
		throw ex;
	}
	// Check the content of the baseband sequence
	for (unsigned int i=0; i<info.baseband.length(); i++) {
		if (info.baseband[i]<=0 || info.baseband[i]>(signed)sm_channelsPerSample) {
			NewTotPwrProcErr::NoBasebandExImpl ex(__FILE__, __LINE__, __func__);
			ACS_SHORT_LOG((LM_ERROR,"Invalid baseband %d",info.baseband[i]));
			throw ex;
		}
	}
	ACS_SHORT_LOG((LM_DEBUG,"Subscan accepted with data ID=%s and exec ID=%s [%s, %s]",
			info.dataOID.in(),
			info.execID.in(),
			TPP_TimeHelper::toString(info.startTime).c_str(),
			TPP_TimeHelper::toString(info.endTime).c_str()));

	m_scheduler_p->addSubscan(info);
}

void NewTPPImpl::launchSubscan(const Control::NewTPP::SubscanInfo& info) {
	AUTO_TRACE("NewTPPImpl::launchSubscan");
	if (m_Closed) {
		return;
	}
	ACS_SHORT_LOG((LM_INFO,"Launching subscan %s [%s, %s]",
			info.dataOID.in(),
			TPP_TimeHelper::toString(info.startTime).c_str(),
			TPP_TimeHelper::toString(info.endTime).c_str()));
	// Instantiate and start a new DataCollector
	DataCollectorParam param;
	param.m_antCnt_p=m_antennas_p;
	param.m_avgLength=NewTPPImpl::m_averagingLength;
	param.m_dgtCnt_p=&m_digitizers;
	param.m_subscanInfo=info;
	param.m_tpp_p=this;
	param.m_scheduler_p=m_scheduler_p;
	ACE_CString threadName("DataCollector_");
	threadName+=info.dataOID.in();
	DataCollector* dataCollector_p = m_threadManager_p->create<DataCollector,DataCollectorParam>(threadName,param);
	dataCollector_p->resume();
	m_dataCollectors.addCollector(dataCollector_p);
	// Signal the digitizers to begin a new subscan
	m_digitizers.beginSubscan(info,dataCollector_p);
}

CORBA::Boolean NewTPPImpl::subscanComplete(CORBA::Long timeout) {
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	time_t startTime=time(NULL);
	do {
		m_mutex.acquire();
		if (m_dataCollectors.isCollectingData()) {
			// No subscan in progress
			m_mutex.release();
			return true;
		}
		m_mutex.release();
		// still getting data...
		time_t now=time(NULL);
		if (now>=startTime+timeout) {
			// timeout elapsed
			return false;
		}
		usleep(500);
	} while (true);
}

void NewTPPImpl::getDataInfo(CORBA::Long& numOfData, CORBA::Double& numOfIntegration, CORBA::Long& numOfFlagged) {
	AUTO_TRACE("NewTPPImpl::getDataInfo");
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_lastSubscanDataMutex);
	numOfData=m_dataProduced;
	numOfIntegration=m_numIntegrations;
	numOfFlagged=m_flaggedData;
}

void NewTPPImpl::interruptClient(const char* antennaName, CORBA::Long polarization) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	Digitizer* dgt =(Digitizer*)m_digitizers.getDigitizer(antennaName,polarization);
	if (dgt==NULL) {
		// The digitizer for the passed antenna and polarization
		// is not in the container
		return;
	}
	dgt->abortSubscan();
}

/////////////////////////////////////////////
///// Internal methods
/////////////////////////////////////////////

void NewTPPImpl::initNetAddress() {
	ACE_INET_Addr localaddr;

	char hostname[128];
	int ret=localaddr.get_host_name(hostname,sizeof(hostname));
	if (ret==-1) {
		NewTotPwrProcErr::InvalidIPExImpl ex(__FILE__, __LINE__, __func__);
		ex.setReason("Impossible to get the host name");
		throw ex;
	}
	m_localHostName.set(hostname,true);

	struct hostent *he;
	he = gethostbyname(m_localHostName.c_str());
	if (he == NULL) {
		NewTotPwrProcErr::InvalidIPExImpl ex(__FILE__, __LINE__, __func__);
		ex.setReason("Impossible to get the IP from the host name");
		throw ex;
	} else {
		m_IPAddressString.set(inet_ntoa(*(struct in_addr*)he->h_addr));
		m_IP= (*(struct in_addr*)he->h_addr).s_addr;
	}

	unsigned int b=m_IP;
	for (int t=0; t<4; t++) {
		m_IP_Bytes[t]=b&0xFF;
		b=b>>8;
	}
	ACS_SHORT_LOG((LM_INFO,"Local host: %d.%d.%d.%d",m_IP_Bytes[0],m_IP_Bytes[1],m_IP_Bytes[2],m_IP_Bytes[3]));
}

void NewTPPImpl::setDistributer(const char* distributerName) {
	AUTO_TRACE("NewTPPImpl::setDistributer");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_Closed) {
		NewTotPwrProcErr::AlreadyShutDownExImpl ex(__FILE__, __LINE__, __func__);
		ACS_SHORT_LOG((LM_ERROR,"Process already shut down"));
		throw ex.getAlreadyShutDownEx();
	}
	if (distributerName==NULL || strlen(distributerName)==0) {
		NewTotPwrProcErr::DistributerNAExImpl ex(__FILE__, __LINE__, __func__);
		throw ex;
	}
	m_distributerName=distributerName;
	if (m_bulkDataSender_p!=NULL) {
		delete m_bulkDataSender_p;
		m_bulkDataSender_p=NULL;
	}
	try {
		m_bulkDataSender_p = new TotalPowerSender(distributerName, getContainerServices());
	} catch (ACSErrTypeCommon::CouldntPerformActionExImpl& ex) {
		m_bulkDataSender_p=NULL;
		NewTotPwrProcErr::DistributerNAExImpl newEx(ex,__FILE__, __LINE__, __func__);
		newEx.setDistributerName(m_distributerName);
		throw newEx;
	} catch (ACSErrTypeCommon::BadParameterExImpl& ex) {
		m_bulkDataSender_p=NULL;
		NewTotPwrProcErr::DistributerNAExImpl newEx(ex,__FILE__, __LINE__, __func__);
		newEx.setDistributerName(m_distributerName);
		throw newEx;
	} catch (...) {
		m_bulkDataSender_p=NULL;
		NewTotPwrProcErr::DistributerNAExImpl newEx(__FILE__, __LINE__, __func__);
		newEx.setDistributerName(m_distributerName);
		throw newEx;
	}

    ACS_SHORT_LOG((LM_DEBUG,"BulkDataSender connected"));
}

CORBA::Boolean NewTPPImpl::isConnected(const char* antennaName, CORBA::Long polarization) {
	Digitizer* dgt=(Digitizer*)m_digitizers.getDigitizer(antennaName, polarization);
	if (dgt==NULL) {
		return false;
	}
	return dgt->isConnected();
}

void NewTPPImpl::setDataInfo(CORBA::Long numOfData, CORBA::Double numOfIntegration, CORBA::Long numOfFlagged) {
	AUTO_TRACE("NewTPPImpl::setDataInfo");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_lastSubscanDataMutex);
	numOfData=m_dataProduced=numOfData;
	m_numIntegrations=numOfIntegration;
	m_flaggedData=numOfFlagged;
 }

/* --------------- [ MACI DLL support functions ] -----------------*/
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(NewTPPImpl)
/* ----------------------------------------------------------------*/

DataCollectorsContainer::DataCollectorsContainer():
	m_actualCollector_p(NULL) {
	ACS_TRACE("DataCollectorsContainer::DataCollectorsContainer()");
	m_collectors_v.clear();
}

DataCollectorsContainer::~DataCollectorsContainer() {
	ACS_TRACE("DataCollectorsContainer::~DataCollectorsContainer()");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	for (unsigned int t=0; t<m_collectors_v.size(); t++) {
		delete m_collectors_v[t];
	}
	m_collectors_v.clear();
}

void DataCollectorsContainer::addCollector(DataCollector *dc) {
	ACS_TRACE("DataCollectorsContainer::addCollector");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	ACS_TRACE("DataCollectorsContainer::addCollector(...)");
	if (dc==NULL) {
		return;
	}
	m_actualCollector_p=dc;
	m_collectors_v.push_back(dc);
}

bool DataCollectorsContainer::isCollectingData() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_actualCollector_p==NULL) {
		return true;
	}
	return m_actualCollector_p->isCollectingData();
}

void DataCollectorsContainer::abortSubscan() {
	ACS_TRACE("DataCollectorsContainer::abortSubscan");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_actualCollector_p!=NULL) {
		m_actualCollector_p->abortSubscan();
	}
}

bool DataCollectorsContainer::areCollectorsRunning() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	ACS_TRACE("DataCollectorsContainer::areCollectorsRunning()");
	for (unsigned int t=0; t<m_collectors_v.size(); t++) {
		if (m_collectors_v[t]->isAlive()) {
			std::cout<<m_collectors_v[t]->getName()<<" still alive"<<std::endl;
			return true;
		}
	}
	return false;
}
