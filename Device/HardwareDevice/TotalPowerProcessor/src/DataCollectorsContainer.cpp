/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-12-17  created 
*/

#include "DataCollectorsContainer.h"

DataCollectorsContainer::DataCollectorsContainer():
	m_actualCollector_p(NULL) {
	ACS_TRACE("DataCollectorsContainer::DataCollectorsContainer()");
	m_collectors_v.clear();
}

DataCollectorsContainer::~DataCollectorsContainer() {
	ACS_TRACE("DataCollectorsContainer::~DataCollectorsContainer()");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	for (unsigned int t=0; t<m_collectors_v.size(); t++) {
		delete m_collectors_v[t];
	}
	m_collectors_v.clear();
}

void DataCollectorsContainer::addCollector(DataCollector *dc) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	ACS_TRACE("DataCollectorsContainer::addCollector(...)");
	if (dc==NULL) {
		return;
	}
	m_actualCollector_p=dc;
	m_collectors_v.push_back(dc);
}

bool DataCollectorsContainer::isCollectingData() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	if (m_actualCollector_p==NULL) {
		return true;
	}
	return m_actualCollector_p->isCollectingData();
}

void DataCollectorsContainer::abortSubscan() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	ACS_TRACE("DataCollectorsContainer::abortSubscan()");
	if (m_actualCollector_p!=NULL) {
		m_actualCollector_p->abortSubscan();
	}
}

bool DataCollectorsContainer::areCollectorsRunning() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	ACS_TRACE("DataCollectorsContainer::areCollectorsRunning()");
	for (unsigned int t=0; t<m_collectors_v.size(); t++) {
		if (m_collectors_v[t].isAlive()) {
			return true;
		}
	}
	return false;
}

/*___oOo___*/
