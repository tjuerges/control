/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-03-18  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <vector>
#include <string>
#include <sstream>

#include <ACSErrTypeCommon.h>
#include <utilBulkUserParam.h>
#include <TPPBinaryBlock.h>
#include <SDMDataObjectWriter.h>

/**
 * A macro to ease the use of enumvec.
 */
#define ENUMVEC(enumName,strlits) asdmbinaries::Utils::enumvec<enumName, C ## enumName>(strlits)


TPPBinaryBlock::TPPBinaryBlock(
		SubscanHeader* hdr,
		std::vector<asdmbinaries::AUTODATATYPE>* data,
		std::vector<asdmbinaries::FLAGSTYPE>* flags,
		unsigned int dataSize):
		m_header_p(hdr) {
	AUTO_TRACE("TPPBinaryBlock::TPPBinaryBlock()");
	createBinaryBlock(data,flags,dataSize);
	ACS_SHORT_LOG((LM_DEBUG,"Binary object created"));
}

TPPBinaryBlock::~TPPBinaryBlock() {}

void TPPBinaryBlock::createBinaryBlock(
		std::vector<asdmbinaries::AUTODATATYPE>* pData,
		std::vector<asdmbinaries::FLAGSTYPE>*  pFlags,
        unsigned int length)
{
	AUTO_TRACE("TPPBinaryBlock::createBinaryBlock(...)");
    // Get all of the relevent data from the input data and format for
    //   the SDMDataObjectWriter

	ACE_CString execBlockID(m_header_p->getExecID());
	ACE_CString data0ID(m_header_p->getDataOID());

    ACS::Time time=m_header_p->getTime();
    ACS::Time interval=m_header_p->getInterval();
    uint32_t subscanNum=(unsigned int)m_header_p->getSubscancanNumber();
    uint32_t numIntegration=(unsigned int)m_header_p->getNumIntegration();
    uint32_t numAntenna=(unsigned int)m_header_p->getNumberOfAntennas();
    uint32_t numBaseband=(unsigned int)m_header_p->getBasebandLength();
    uint32_t scanNum=(unsigned int)m_header_p->getScanNumber();
    int numBins=1; // Hardcoded for now

    std::vector<asdmbinaries::FLAGSTYPE> flags(length);

	bool useFlags = false;
	for (unsigned int idx = 0; idx < length; idx++) {
	  flags[idx] = pFlags->at(idx);
	  if (pFlags->at(idx) != 0) {
		useFlags = true;
	  }
	}

     // Prepare the description of the binary data.
    std::vector<asdmbinaries::SDMDataObject::Baseband> basebands;
    std::vector<asdmbinaries::SDMDataObject::SpectralWindow> spectralWindows;

    std::vector<StokesParameter> sdPolProducts;
    //for (int pol = 0; pol < numPolarization; pol++) {
    // Note we need to pass in a real description here
    sdPolProducts.push_back(XX);
    sdPolProducts.push_back(YY);
	//}

    //TODO Hardcoded!!!!!
    NetSideband netSideBand = LSB;

    // Creating a Spectran window
    spectralWindows.push_back(asdmbinaries::SDMDataObject::SpectralWindow(sdPolProducts, 1,
    							  numBins, netSideBand));

    // Wow this base band stuff needs a lot of work but for now...
    if (numBaseband > 0 ) {
        basebands.push_back(asdmbinaries::SDMDataObject::Baseband(BB_1, spectralWindows));
    }

    if (numBaseband > 1 ) {
        basebands.push_back(asdmbinaries::SDMDataObject::Baseband(BB_2, spectralWindows));
    }

    if (numBaseband > 2 ) {
        basebands.push_back(asdmbinaries::SDMDataObject::Baseband(BB_3, spectralWindows));
    }

    if (numBaseband > 3 ) {
        basebands.push_back(asdmbinaries::SDMDataObject::Baseband(BB_4, spectralWindows));
    }

    vector<AxisName> axis = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");

    //TODO: Change for reasonable values
    vector<AxisName> flagsAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");

    if (!useFlags) {
	  flags.clear();
	}

    vector<AxisName> actualTimesAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");
    vector<long long> actualTimes;
    vector<AxisName> actualDurationsAxes = ENUMVEC(AxisName, "TIM ANT BAB BIN POL");
    vector<long long> actualDurations;

    asdmbinaries::SDMDataObjectWriter sdmdow(&m_tpBlock, data0ID.c_str(), "ALMA Total Power Data");

    try{
        // New version
        sdmdow.tpData((100L * (time - interval/2) - 8712576000000000000LL),      // startTime in Julian Time. Maybe a utility class should be used
                execBlockID.c_str(),    // ExecBlock UID (?)
                (uint32_t)1,    // execBlockNum
                scanNum,        // scanNum
                subscanNum,     // subscanNum
                numIntegration, // number of integrations
                numAntenna,     // number of antennas
                basebands,      // vector of basebands.
                (uint64_t)(100L * time - 8712576000000000000LL),           // time = midpoint
                (uint64_t)(interval * 100),       // interval in [ns]
                flagsAxes,
                flags,
                actualTimesAxes,
                actualTimes,
                actualDurationsAxes,
                actualDurations,
                axis,
                *pData);

    }catch(asdmbinaries::SDMDataObjectWriterException &ex){
        ACSErrTypeCommon::GenericErrorExImpl *ex2 = new ACSErrTypeCommon::GenericErrorExImpl(__FILE__,__LINE__,"createBinaryBlock");
        ex2->setErrorDesc(ex.getMessage().c_str());
        throw ex2;
    }catch(...){
        ACSErrTypeCommon::GenericErrorExImpl *ex2 = new ACSErrTypeCommon::GenericErrorExImpl(__FILE__,__LINE__,"createBinaryBlock");
        ex2->setErrorDesc("sdmdow.tpData() call failed.");
        throw ex2;
    }
    sdmdow.done();
}

void TPPBinaryBlock::writeBlobToDisk(std::string basePath) {
	AUTO_TRACE("TPPBinaryBlock::writeBlobToDisk");
	try{
		std::stringstream sstream;
		sstream<<basePath<<"/TPPBinaryBlock_";
		sstream<<time(NULL)<<".bin";
		std::string fileName;
		sstream>>fileName;

		ACS_SHORT_LOG((LM_DEBUG,"Creating file %s",fileName.c_str()));
		ofstream ofs(fileName.c_str(),ios::binary|ios::out);
		ofs<<m_tpBlock.str();
		ACS_SHORT_LOG((LM_DEBUG,"Data successfully written on file %s",fileName.c_str()));
	}catch(...){
		ACS_SHORT_LOG((LM_ERROR,"Exception caught while writing data on file"));
	};
}

bool TPPBinaryBlock::getData(const char*& data0_id, const char*& data, size_t& size) {
	data0_id=m_header_p->getDataOID().c_str();
	std::string dataString = m_tpBlock.str();
	data=dataString.c_str();
	size=dataString.size();
	return true;
}

void TPPBinaryBlock::sendBinaryobject(NewTPPImpl* tpp, CORBA::ULong flow) {
	const char* __METHOD__ = "TPPBinaryBlock::sendBinaryobject";
	AUTO_TRACE(__METHOD__);

	ACE_CString uidCString(m_header_p->getDataOID());
	std::string dataString = m_tpBlock.str();
	const char *data=dataString.c_str();
	size_t size=dataString.size();
	try {
		if(size==0){
			ACSErrTypeCommon::GenericErrorExImpl *ex = new ACSErrTypeCommon::GenericErrorExImpl(__FILE__,__LINE__,__METHOD__);
			ex->setErrorDesc("Trying to send data with size 0.");
			throw ACSErrTypeCommon::CouldntPerformActionExImpl(*ex,__FILE__,__LINE__,__METHOD__);
		}
		string frameId = BulkUserParam::toString(uidCString.c_str(), size);
		ACS_SHORT_LOG((LM_DEBUG, "Subscan UID=%s data size: %d",uidCString.c_str(),size));

		ACS_SHORT_LOG((LM_DEBUG, "Starting send with [%s] and length=%d",frameId.c_str(), frameId.length() + 1));
		std::cout<<"TPPBinaryBlock::sendBinaryobject "<<frameId<<std::endl;
		// plus one as to include the null terminating char

		tpp->getSender()->startSend(flow, frameId.c_str(), frameId.length() + 1);

		tpp->getSender()->sendData(flow, (const char *)data, size);

		tpp->getSender()->stopSend(flow);

	} catch(ACSBulkDataError::AVInvalidFlowNumberExImpl &ex) {
		ex.log();
		ACS_LOG(LM_SOURCE_INFO,__METHOD__,
				(LM_ERROR, "invalid flow number exception caught!"));
		throw ACSErrTypeCommon::CouldntPerformActionExImpl(ex,__FILE__,__LINE__,__METHOD__);
	} catch(ACSBulkDataError::AVSendFrameErrorExImpl &ex) {
		ex.log();
		ACS_LOG(LM_SOURCE_INFO,__METHOD__,
				(LM_ERROR, "send frame exception caught!"));
		throw ACSErrTypeCommon::CouldntPerformActionExImpl(ex,__FILE__,__LINE__,__METHOD__);
	} catch(ACSBulkDataError::AVFlowEndpointErrorExImpl &ex) {
		ex.log();
		ACS_LOG(LM_SOURCE_INFO,__METHOD__,
				(LM_ERROR, "flow endpoint exception caught!"));
		throw ACSErrTypeCommon::CouldntPerformActionExImpl(ex,__FILE__,__LINE__,__METHOD__);
	} catch(ACSBulkDataError::AVProtocolErrorExImpl &ex) {
		ex.log();
		ACS_LOG(LM_SOURCE_INFO,__METHOD__,
				(LM_ERROR, "protocol exception caught!"));
		throw ACSErrTypeCommon::CouldntPerformActionExImpl(ex,__FILE__,__LINE__,__METHOD__);
	} catch(...) {
		ACS_LOG(LM_SOURCE_INFO,__METHOD__,
				(LM_ERROR, "unknown exception catched while sending frame!"));
		throw ACSErrTypeCommon::CouldntPerformActionExImpl(__FILE__,__LINE__,__METHOD__);
	}
}

/*___oOo___*/
