/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
<<<<<<< DataReceiverThread.cpp
* "@(#) $Id$"
=======
* "@(#) $Id$"
>>>>>>> 1.3.2.3
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-04-22  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <DataReceiverThread.h>

DataReceiverThread::DataReceiverThread(
		const ACE_CString& name,
		PacketHandler* ph,
		bool swapDate,
		bool swapSamples):
		ACS::Thread(name),
		m_dataReceiver(name,ph,swapDate,swapSamples,this),
		m_name(name),
		m_shutdown(false) {
}

DataReceiverThread::~DataReceiverThread() {
	AUTO_TRACE("DataReceiverThread::~DataReceiverThread");
	shutdown();
	std::cout<<"DataReceiverThread::~DataReceiverThread("<<m_name.c_str()<<") shutdown completed"<<std::endl;
	std::cout<<"DataReceiverThread::~DataReceiverThread("<<m_name.c_str()<<") done"<<std::endl;
}

int DataReceiverThread::getServerPort() {
	AUTO_TRACE("DataReceiverThread::getServerPort");
	return m_dataReceiver.getServerPort();
}

void DataReceiverThread::shutdown() {
	AUTO_TRACE("DataReceiverThread::shutdown");
	if (m_shutdown) {
		ACS_SHORT_LOG((LM_DEBUG,"DataReceiverThread::shutdown %s already shutdown",getName().c_str()));
		return;
	}
	m_shutdown=true;
	ACS_SHORT_LOG((LM_DEBUG,"DataReceiverThread::shutdown %s",getName().c_str()));
	m_dataReceiver.shutdown();
	// Wait until the thread terminates
	while (isAlive()) {
		usleep(500);
	}
	std::cout<<"DataReceiverThread::shutdown Done "<<getName().c_str()<<std::endl;
}

void DataReceiverThread::beginReceivingData(
		ACS::Time startTime,
		ACS::Time endTime) {
	AUTO_TRACE("DataReceiverThread::beginReceivingData");
	if (m_shutdown) {
		return;
	}
	m_dataReceiver.beginReceivingData(startTime,endTime);
}

void DataReceiverThread::abortSubscan() {
	AUTO_TRACE("DataReceiverThread::abortSubscan");
	m_dataReceiver.abortSubscan();
}

bool DataReceiverThread::isConnected() {
	AUTO_TRACE("DataReceiverThread::isConnected");
	return m_dataReceiver.isConnected();
}

void DataReceiverThread::run() {
	ACS_SHORT_LOG((LM_DEBUG,"DataReceiverThread::run()"));
	m_dataReceiver.socketReceiver();
	ACS_SHORT_LOG((LM_DEBUG,"Stopping DataReceiverThread thread %s",m_name.c_str()));
	setStopped();
	ACS_SHORT_LOG((LM_DEBUG,"DataReceiverThread thread %s stopped",m_name.c_str()));
}

/*___oOo___*/

