/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-13  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <Guard_T.h>

#include "AntennasContainer.h"

AntennaContainer::AntennaContainer(DigitizersContainer& digitizers):
	m_digitizers(digitizers)
{
	clear();
}

AntennaContainer::~AntennaContainer() {
	clear();
}

void AntennaContainer::clear() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_antennasMutex);
	m_antennas.clear();
}

unsigned int AntennaContainer::size() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_antennasMutex);
	unsigned int sz=m_antennas.size();
	return sz;
}

bool AntennaContainer::contains(const char* antennaName) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_antennasMutex);
	bool ret=false;
	for (unsigned int t=0; t<m_antennas.size(); t++) {
		if (m_antennas[t]==(antennaName)) {
			ret=true;
			break;
		}
	}
	return ret;
}

int AntennaContainer::findAntenna(const char* antennaName) {
	int ret=-1;
	if (antennaName==NULL) {
		return ret;
	}
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_antennasMutex);
	for (unsigned int t=0; t<m_antennas.size(); t++) {
		if (m_antennas[t]==(antennaName)) {
			ret=t;
			break;
		}
	}
	return ret;
}

void AntennaContainer::setArray(const Control::AntennaSeq& antennas) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_antennasMutex);
	// Empty the container
	this->clear();
	// Put the antenna names in the sequence into the array
	// by preserving the order
	for (CORBA::ULong t=0; t<antennas.length(); t++) {
		ACE_CString temp(antennas[t]);
		m_antennas.push_back(temp);
	}
	m_digitizers.removeUnneededDigitizers(m_antennas,true,true);
}

const char* AntennaContainer::getAntennaAt(unsigned int i) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_antennasMutex);
	if (i>=m_antennas.size()) {
		return NULL;
	}
	return m_antennas[i].c_str();
}

std::set<ACE_CString> AntennaContainer::getAntennaNames() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_antennasMutex);
	std::set<ACE_CString> ret;
	for (unsigned int t=0; t<m_antennas.size(); t++) {
		ACE_CString name(m_antennas[t]);
		ret.insert(name);
	}
	return ret;
}

/*___oOo___*/
