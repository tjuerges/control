/** ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-02-14  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <set>
#include <Guard_T.h>
#include <NewTPPImpl.h>
#include "Digitizer.h"

DataCollector::DataCollector(const ACE_CString& name, DataCollectorParam& param):
		ACS::Thread(name),
		m_subscanHeader_p(NULL),
		m_mutex_p(new ACE_Thread_Mutex()),
		m_cond_p(new ACE_Condition<ACE_Thread_Mutex>(*m_mutex_p)),
		m_closed(false),
		m_buffersSize(0),
		m_dataBuffer_v(),
		m_flagsBuffer_v(),
		m_binaryBlock_p(NULL),
		m_aquiringData(true),
		m_signaled(false),
		m_scheduler_p(NULL),
		m_dataID(param.m_subscanInfo.dataOID.in())
{
	AUTO_TRACE("DataCollector::DataCollector");
	m_antennasContainer_p=param.m_antCnt_p;
	m_digitizersContainer_p=param.m_dgtCnt_p;
	m_tpp_p=param.m_tpp_p;
	m_avgLength=param.m_avgLength;
	m_subscanHeader_p= new SubscanHeader(param.m_subscanInfo,param.m_avgLength,m_antennasContainer_p);
	m_scheduler_p=param.m_scheduler_p;
	ACS_SHORT_LOG((LM_DEBUG,"Data collector for subscan %s built",m_subscanHeader_p->getDataOID().c_str()));
}

DataCollector::~DataCollector() {
	AUTO_TRACE("DataCollector::~DataCollector");
	std::cout<<"DataCollector::~DataCollector terminating thread "<<getName().c_str()<<std::endl;
	//m_closed=true;
	//m_cond_p->signal();
	while (isAlive()) {
		usleep(500);
	}
	std::cout<<"DataCollector::~DataCollector thread terminated "<<getName().c_str()<<std::endl;
	delete m_cond_p;
	std::cout<<"DataCollector::~DataCollector m_cond_p deleted "<<getName().c_str()<<std::endl;
	delete m_mutex_p;
	std::cout<<"DataCollector::~DataCollector m_mutex_p deleted "<<getName().c_str()<<std::endl;
	delete m_subscanHeader_p;
	std::cout<<"DataCollector::r~DataCollector() m_subscanHeader_p deleted "<<getName().c_str()<<std::endl;


}

void DataCollector::run() {
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::run() for subcan %s",m_subscanHeader_p->getDataOID().c_str()));
	waitDataAcquired();
	if (!check() || m_closed) {
		ACS_SHORT_LOG((LM_DEBUG,"DataCollecotr closed. Terminating thread %s.",getName().c_str()));
		setStopped();
		return;
	}
	std::cout<<"Collected all data for subscan "<<m_subscanHeader_p->getDataOID().c_str()<<std::endl;
	
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::run() thread %s subscan %s: data received from all digitizers",getName().c_str(),m_subscanHeader_p->getDataOID().c_str()));

	// Signal the scheduler that the current subscan terminated
	// so that a new started immediately
	// m_scheduler_p->subscanTerminated();

	// Init the buffers
	initBuffers();

	if (check()) {
		reduce();
		ACS_SHORT_LOG((LM_DEBUG,"DataCollector::run() thread %s subscan %s data reduced",getName().c_str(),m_subscanHeader_p->getDataOID().c_str()));
	}

	// Count the number of flagged data
	long flagged=0;
	for (unsigned int t=0; t<m_buffersSize; t++) {
		if (m_flagsBuffer_v[t]!=0) {
			flagged++;
		}
	}
	// Set the subscan info in the TPP so that the getDataInfo() IDL method
	// return valid info
	//m_tpp_p->setDataInfo(m_buffersSize,m_subscanHeader_p->getNumIntegration(),flagged);

	//dump();
	if (check() && !m_closed) {
		m_binaryBlock_p = new TPPBinaryBlock(
			m_subscanHeader_p,
			&m_dataBuffer_v,
			&m_flagsBuffer_v,
			m_buffersSize);
	}
	if (!check() || m_closed) {
		ACS_SHORT_LOG((LM_DEBUG,"Stopping DataCollector thread %s for subscan %s",getName().c_str(),m_subscanHeader_p->getDataOID().c_str()));
		setStopped();
		return;
	}

	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::run(): thread %s Sending data to bulk data receiver",getName().c_str()));
	CORBA::ULong flow(1);
	m_binaryBlock_p->sendBinaryobject(m_tpp_p,flow);
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::run(): thread %s binary object %s sent to the BulkDataReceiver",getName().c_str(),m_subscanHeader_p->getDataOID().c_str()));

        // Calling subScanTerminated after sending the data instead of before
        // sending the binary data to avoid a synchronization problem with TelCal.	
	m_scheduler_p->subscanTerminated();

	// free unused memory
	//
	// DataCollectorsContainer save the DataCollectors till the end of the computation
	// so it is better to free the memory as soon as possible
	m_dataBuffer_v.clear();
	std::cout<<"DataCollector::run() buffer cleared "<<getName().c_str()<<std::endl;
	m_flagsBuffer_v.clear();
	std::cout<<"DataCollector::run() flags buffer cleared "<<getName().c_str()<<std::endl;
	if (m_binaryBlock_p!=NULL) {
		std::cout<<"DataCollector::run() deleting m_binaryBlock "<<getName().c_str()<<std::endl;
		delete m_binaryBlock_p;
		m_binaryBlock_p=NULL;
	}
	// Stop the thread
	ACS_SHORT_LOG((LM_DEBUG,"Stopping DataCollector thread for subscan %s",m_subscanHeader_p->getDataOID().c_str()));
	setStopped();
	std::cout<<"DataCollector::run() thread terminated for subscan "<< m_subscanHeader_p->getDataOID().c_str()<<' '<<getName().c_str() <<std::endl;
}

/**
 * Functioning note:
 * the thread is waked up only once when all the digitizers terminated getting data.
 */
void DataCollector::waitDataAcquired() {
	AUTO_TRACE("DataCollector::waitDataAcquired()");
	// Check if all the digitizer have finished acquiring data
	m_mutex_p->acquire();
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::waitDataAcquired() %s: waiting!",getName().c_str()));
	m_cond_p->wait();
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::waitDataAcquired() %s: waked up!",getName().c_str()));
	m_mutex_p->release();
}

/**
 * Functioning note:
 * this method is executed when a digitizer terminated getting values from the IFProc.
 * If the thread has not already waked up, this method checks the status of the digitizers
 * and signals the thread only if all the digitizers teminated getting data for the subscan.
 * <P>
 * It is needed to check if the thread has already been waked up before signaling:
 * dgt A terminates getting data, and get the mutex
 * dgt B terminates getting data, and wait for the mutex
 * The method checks if all the digitizers have terminated, signal the thread and release the mutex
 * dgt B thread get the mute, check the digitizers and signal again the thread
 *
 * To avoid this last operation we need to remeber if the thread has been already waked up
 * by setting m_signaled to true.
 */
void DataCollector::digitizerDataArrived(const char* digitizerName) {
	AUTO_TRACE("DataCollector::digitizerDataArrived");
	ACE_Guard<ACE_Thread_Mutex> guard(*m_mutex_p);
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived(%s) mutex acquired",digitizerName));
	if (m_signaled) {
		ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived(%s) thread %s already waked up",digitizerName,getName().c_str()));
		return;
	}
	if (m_closed) {
			ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived(%s) %s: subscan aborted/closed",digitizerName,getName().c_str()));
			return;
		}
	// Evaluate the condition
	m_aquiringData=false; // suppose all the data received
	for (unsigned int a=0; a<m_antennasContainer_p->size() && !m_aquiringData; a++) {
		for (int p=0; p<m_subscanHeader_p->getPolarizationLength()  && !m_aquiringData; p++) {
			Digitizer* dgt_p=(Digitizer*)m_digitizersContainer_p->getDigitizer(
					m_antennasContainer_p->getAntennaAt(a),
					m_subscanHeader_p->getPolarizationAt(p));
			if (dgt_p==NULL) {
				continue;
			}
			if (dgt_p->isGettingData(m_dataID)) {
				ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived Digitizer %s (%s): still acquiring data",dgt_p->getName(),getName().c_str()));
				m_aquiringData=true;
				break;
			} else {
				ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived Digitizer %s (%s) terminated acquiring data",dgt_p->getName(),getName().c_str()));
			}
		}
	}
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived(%s) %s: signaling with condition %d",digitizerName,getName().c_str(),m_aquiringData));
	if (!m_aquiringData && !m_signaled) {
		ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived(%s) %s: signaling thread",digitizerName,getName().c_str()));
		m_signaled=true;
		m_cond_p->signal();
	}
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::digitizerDataArrived(%s) %s: done",digitizerName,getName().c_str()));

}

void DataCollector::abortSubscan() {
	AUTO_TRACE("DataCollector::abortSubscan()");
	if (m_closed) {
		return;
	}
	m_closed=true;
	m_cond_p->signal();
}

void DataCollector::initBuffers() {
	AUTO_TRACE("DataCollector::initBuffers");
	int integrationSize =
		m_subscanHeader_p->getNumberOfAntennas() *
		m_subscanHeader_p->getBasebandLength() *
		m_subscanHeader_p->getPolarizationLength();
		m_buffersSize =
			m_subscanHeader_p->getNumIntegration()*integrationSize;

	m_flagsBuffer_v.clear();
	m_flagsBuffer_v.resize(m_buffersSize,1);
	m_dataBuffer_v.clear();
	m_dataBuffer_v.resize(m_buffersSize,0.0);

	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::initBuffers instantiated flags and data buffer of size %d",m_buffersSize));

}

bool DataCollector::checkDigitizer(DigitizerBase* digitizer) {
	AUTO_TRACE("DataCollector::checkDigitizer");
	if (digitizer==NULL) {
		ACS_SHORT_LOG((LM_DEBUG,"Digitizer is NULL"));
		return false;
	}
	// Check the antenna name of the digitizer
	if (!m_antennasContainer_p->contains(digitizer->getAntenna())) {
		ACS_SHORT_LOG((LM_DEBUG,"The antenna container does not contains %s",digitizer->getAntenna()));
		return false;
	}
	// Check the polarization
	bool found = false;
	for (int t=0; t<m_subscanHeader_p->getPolarizationLength(); t++) {
		if (m_subscanHeader_p->getPolarizationAt(t)==digitizer->getPolarization()) {
			found=true;
			break;
		}
	}
	if (!found) {
		ACS_SHORT_LOG((LM_DEBUG,"Polarization %d not found in the subscan header",digitizer->getPolarization()));
		return false;
	}

	// Check the times
	Digitizer* dgt=(Digitizer*)digitizer;

	// Check if the digitizer is still getting data
	//
	// This should never happen otherwise there is an error in the waitDataAquired(..)
	// that checks if all the digitizers have terminated acquiring data before
	// exiting.
	//
	// If it is the case, then log an error message and discard the digitizer
	if (dgt->isGettingData(m_dataID)) {
		ACS_SHORT_LOG((LM_ERROR,"Digitizer %s is still getting data for subscan %s while it is supposed to have finished: internal error",
				dgt->getName(),m_dataID.c_str()));
		return false; //
	}

	// The SubscanData of the digitizer must not be null otherwise it means that
	// it never began a subscan.
	//
	// This should never happen otherwise this digitizer has not been notified to start
	// getting data or the same digitizer appears more then once in the container or..
	//
	// If it is the case, the log an error message and discard the digitizer
	if (dgt->getSubscanData(m_dataID)==NULL) {
		ACS_SHORT_LOG((LM_ERROR,"Digitizer %s never started %s subscan: internal error",
				m_dataID.c_str(),
				dgt->getName()));
		return false;
	}

	// Finally accepts the digitizer
	ACS_SHORT_LOG((LM_DEBUG,"Data from digitizer %s accepted",
			dgt->getName()));
	return true;
}

int DataCollector::addDigitizerData(DigitizerBase* digitizer) {
	AUTO_TRACE("DataCollector::addDigitizerData(...)");
	if (digitizer==NULL) {
		ACS_SHORT_LOG((LM_WARNING,"NULL digitizer!"));
		return 0;
	}
	ACS_SHORT_LOG((LM_DEBUG,"DataCollector::addDigitizerData %s: for %s",getName().c_str(),digitizer->getName()));
	int antIdx=m_antennasContainer_p->findAntenna(digitizer->getAntenna());
	if (antIdx==-1) {
		return 0;
	}

	// Check the exit code of the subscan
	int subscanCompletion=((Digitizer*)digitizer)->getSubscanData(m_dataID)->getSubscanCompletion();
	if (subscanCompletion!=PacketHandler::SUBSCAN_COMPLETE) {
		ACS_SHORT_LOG((LM_WARNING,"Digitizer %s terminated abnormally (code=%d)",
				digitizer->getName(),
				subscanCompletion));
	}

	int polIdx=m_subscanHeader_p->getPolarizationIndex(digitizer->getPolarization());
	if (polIdx==-1) {
		return 0;
	}
	//std::cout<<"Polarization index="<<polIdx<<std::endl;

	int clientOffset =
		(antIdx * m_subscanHeader_p->getBasebandLength()* m_subscanHeader_p->getPolarizationLength())
		+ polIdx;
	//std::cout<<"Client offset="<<clientOffset<<std::endl;

	// Get data from the digitizer
	Digitizer* dgt=(Digitizer*)digitizer;
	bool* dgtFlagsBuffer=NULL;
	unsigned char* dgtDataBuffer=NULL;
	unsigned int dgtSamples=0;

	if (!dgt->getSubscanData(m_dataID)->getAcquiredData(&dgtDataBuffer,&dgtSamples,&dgtFlagsBuffer)) {
		// Data are invalid
		return 0;
	}

//	std::cout<<"Samples: "<<dgtSamples<<" sizeof(float)="<<sizeof(float)<<" sizeof(short)="<<sizeof(short)<<std::endl;
//	for (unsigned int k=0; k<dgtSamples; k++) {
//		if (dgtFlagsBuffer[k]) {
//			std::cout<<k<<"] ";
//			for (unsigned int kk=0; kk<8; kk++) {
//				std::cout<<(int)dgtDataBuffer[k*2*NewTPPImpl::sm_channelsPerSample+kk]<<' ';
//			}
//			std::cout<<std::endl;
//		}
//	}

	// The number of lost packets
	long lost=0;

	// The number of samples added
	int ret=0;

	// If averaging lenght==1, integration iterates through each sample in the time interval
	for (int integration = 0; integration < m_subscanHeader_p->getNumIntegration(); integration++) {

		// The accumulator is needed to sum data when the averaging length>1;
		// If the avgLen==1, each accumulator contains the value read from a channel
		float accumulator[NewTPPImpl::sm_channelsPerSample];

		/* Zero the accumulation register */
		memset(accumulator,0,sizeof(accumulator));

		// The number of good samples
		//
		// If the averagingLength>1 there can be more samples the average
		int numSamples = 0;

		// Accumulate the data
		// Only one iteration with idx=0 if the averaging length==1
		for (int idx=0; idx<m_subscanHeader_p->getAveragingLength(); idx++) {
			int flagOffset = integration * m_subscanHeader_p->getAveragingLength();
			if (dgtFlagsBuffer[flagOffset+idx]) {
				int dataOffset =(flagOffset + idx)*NewTPPImpl::sm_channelsPerSample*2;
				numSamples++;
//				std::cout<<"Offset="<<flagOffset<<' ';
				for (unsigned int bbidx=0; bbidx<NewTPPImpl::sm_channelsPerSample; bbidx++) {
					unsigned short temp=0;
					unsigned char* uc=(unsigned char*)&temp;
					*uc=dgtDataBuffer[dataOffset+2*bbidx];
//					std::cout<<"oc[0]="<<(int)*uc;
					uc++;
					*uc=dgtDataBuffer[dataOffset+2*bbidx+1];
//					std::cout<<" oc[1]="<<(int)*uc;
					// each sample is composed of 2 bytes (for each channel)
					accumulator[bbidx] += temp;
//					std::cout<<" dgtData=0x"<<std::hex<<temp<<std::dec<<", acc="<<accumulator[bbidx]<<", ";
				}
//				std::cout<<std::endl;
			}
		}

		/* Fill the output data buffer */
		int integrationSize=
				m_subscanHeader_p->getNumberOfAntennas()*
				m_subscanHeader_p->getBasebandLength() *
				m_subscanHeader_p->getPolarizationLength();

		for (int bbidx=0; bbidx<m_subscanHeader_p->getBasebandLength(); bbidx++) {
			int idx = (integration * integrationSize) + clientOffset +
				(bbidx * m_subscanHeader_p->getPolarizationLength());
			if (numSamples==0) {
				lost++;
				m_dataBuffer_v[idx] = 0.0F;
				m_flagsBuffer_v[idx] = 1;
				continue;
			}

			if (bbidx>m_subscanHeader_p->getBasebandLength()-1) {
				ACS_SHORT_LOG((LM_ERROR,"Index %d out of SubscanInfo baseband array bound",bbidx));
				continue;
			}

//			std::cout<<"numSamples "<<numSamples<<" at baseband iteration "<<bbidx<<", index="<<idx<<", acc index="<<(m_subscanHeader_p->getBasebandOrder(bbidx)-1)<<std::endl;
			// Here the minus 1 is needed because the baseband
			// order is 1 based and the indexes are 0 based
			m_dataBuffer_v[idx]=
					accumulator[m_subscanHeader_p->getBasebandOrder(bbidx)-1]/numSamples;
//			std::cout<<"Value "<<m_dataBuffer_v[idx]<<" from "<<accumulator[m_subscanHeader_p->getBasebandOrder(bbidx)-1]<<" stored at "<<idx<<" and flagged ";
			if (numSamples == m_avgLength) {
				m_flagsBuffer_v[idx] = 0;
//				std::cout<<"TRUE";
				ret++;
			} else {
				m_flagsBuffer_v[idx] = 1;
//				std::cout<<"FALSE";
			}
//			std::cout<<" :(m_dataBuffer_v[idx]="<<m_dataBuffer_v[idx]<<')'<<std::endl;
//			printf(">>>>>>>>> %f %f ",m_dataBuffer_v[idx],accumulator[m_subscanHeader_p->getBasebandOrder(bbidx)-1]);
		}
	}

	if (lost>0) {
		ACS_SHORT_LOG((LM_WARNING,"%d packets lost from digitizer %s",lost,dgt->getName()));
	} else {
		ACS_SHORT_LOG((LM_DEBUG,"No packets lost for digitizer %s",dgt->getName()));
	}
	return ret;
}

void DataCollector::reduce() {
	AUTO_TRACE("DataCollector::reduce");
	// Iterate throw digitizers
	ACS_SHORT_LOG((LM_DEBUG,"%s: %d antennas to reduce",getName().c_str(),m_antennasContainer_p->size()));
	int totAddedSamples=0;
	for (unsigned int ant=0; ant<m_antennasContainer_p->size() && check(); ant++) {
		ACE_CString antName(m_antennasContainer_p->getAntennaAt(ant));
		ACS_SHORT_LOG((LM_DEBUG,"%s: Reducing data from antenna %s",getName().c_str(),antName.c_str()));
		// Get the polarizations for the passed antenna
		std::set<CORBA::Long> polarizatons=m_digitizersContainer_p->getPolarizations(antName.c_str());
		ACS_SHORT_LOG((LM_DEBUG,"%s: %d polarizations for antenna %s",getName().c_str(),polarizatons.size(),antName.c_str()));

		std::set<CORBA::Long>::iterator it;
		for (it=polarizatons.begin(); it!=polarizatons.end() && check(); it++) {
		    CORBA::Long polarization= *it;
		    ACS_SHORT_LOG((LM_DEBUG,"%s: Reducing data of %s:%d",getName().c_str(),antName.c_str(),polarization));

		    // Get the digitizer with this antenna name and polarization
		    DigitizerBase* digitizer=m_digitizersContainer_p->getDigitizer(antName.c_str(),polarization);
		    if (checkDigitizer(digitizer)) {
		    	// This digitizer is part of the subscan
		    	int added=addDigitizerData(digitizer);
		    	((Digitizer*)digitizer)->releaseSubscanData(m_dataID);
		    	ACS_SHORT_LOG((LM_DEBUG,"%s: %d samples added from %s",getName().c_str(),added,digitizer->getName()));
		    	totAddedSamples+=added;
		    } else {
		    	ACS_SHORT_LOG((LM_ERROR,"%s: digitizer %s:%d discarded",getName().c_str(),antName.c_str(),polarization));
		    }
		}
	}
	ACS_SHORT_LOG((LM_DEBUG,"%s: %d samples reduced",getName().c_str(),totAddedSamples));
}

bool DataCollector::getData(const char*& data0_id, const char*& data, size_t& size) {
	if (m_binaryBlock_p==NULL) {
		data0_id=data=NULL;
		size=0;
		return false;
	}
	return m_binaryBlock_p->getData(data0_id,data,size);
}

void DataCollector::dump() {
	int count=0;
	for (unsigned int t=0; t<m_buffersSize; t+=4) {
		for (unsigned int j=0; j<4; j++) {
			if (t+j>=m_buffersSize) {
				return;
			}
			if (m_flagsBuffer_v[t+j]==0) {
				count++;
				std::cout<<(t+j)<<"]"<<" f=OK  ";
				unsigned char* cf=(unsigned char*)&m_dataBuffer_v[t+j];
				for (unsigned int h=0; h<sizeof(float); h++) {
					std::cout<<std::hex<<((int)*cf)<<std::dec<<' ';
					cf++;
				}
				std::cout<<m_dataBuffer_v[t+j]<<std::endl;
			}
		}
	}
	std::cout<<"Total samples: "<<count<<std::endl;
}

bool DataCollector::isCollectingData() {
	ACE_Guard<ACE_Thread_Mutex> guard(*m_mutex_p);
	return m_aquiringData;
}

/*___oOo___*/

