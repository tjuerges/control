/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-13  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <sstream>

#include "DigitizerBase.h"

DigitizerBase::DigitizerBase(const char* antennaName, CORBA::Long polarization):
	m_antennaName(antennaName),
	m_polarization(polarization)
{
	std::stringstream sstream;
	sstream<<antennaName<<':'<<polarization;
	std::string str;
	sstream>>str;
	m_digitizerName=str.c_str();
}

bool DigitizerBase::equals(DigitizerBase& d) {
	return this->equals(d.getAntenna(),d.getPolarization());
}

bool DigitizerBase::equals(const char* antennaName, CORBA::Long pol) {
	return (pol==this->getPolarization()) &&
		(strcmp(this->getAntenna(),antennaName)==0);
}

/*___oOo___*/
