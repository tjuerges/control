/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-07-22  created 
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "loggingACEMACROS.h"

#include "SubscanScheduler.h"
#include "NewTPPImpl.h"

SubscanScheduler::SubscanScheduler(const ACE_CString& name, SubscanSchedulerParam& param):
		ACS::Thread(name),
		m_semaphore(0),
		m_closed(false)
{
	AUTO_TRACE("SubscanScheduler::SubscanScheduler");
	m_tpp_p=param.m_tpp_p;
}

SubscanScheduler::~SubscanScheduler()
{
	AUTO_TRACE("SubscanScheduler::~SubscanScheduler");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	std::cout<<"SubscanScheduler::~SubscanScheduler clearing subscan_v "<<std::endl;
	m_subscans_v.clear();
	std::cout<<"SubscanScheduler::~SubscanScheduler shutting down "<<std::endl;
	shutdown();
	std::cout<<"SubscanScheduler::~SubscanScheduler Done "<<std::endl;
}

void SubscanScheduler::addSubscan(Control::NewTPP::SubscanInfo subscanInfo) {
	AUTO_TRACE("SubscanScheduler::addSubscan");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	std::cout<<"SubscanScheduler::addSubscan mutex acquired"<<std::endl;
	if (m_closed) {
		ACS_SHORT_LOG((LM_DEBUG,"New subscan rejected because the SubscanScheduler has been shutdown"));
		return;
	}

	std::cout<<"SubscanScheduler::addSubscan subscan added to vector"<<std::endl;
	ACS_SHORT_LOG((LM_DEBUG,"Subscan queued size=%d",m_subscans_v.size()));
	if (m_subscans_v.size()==0) {
		m_subscans_v.push_back(subscanInfo);
		m_semaphore.release();
		std::cout<<"SubscanScheduler::addSubscan semaphore signaled"<<std::endl;
	} else {
		m_subscans_v.push_back(subscanInfo);
	}
}

void SubscanScheduler::subscanTerminated() {
	AUTO_TRACE("SubscanScheduler::subscanTerminated");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	m_subscans_v.erase(m_subscans_v.begin());
	if (!m_subscans_v.empty()) {
		m_semaphore.release();
	}
}

void SubscanScheduler::shutdown() {
	AUTO_TRACE("SubscanScheduler::shutdown");
	m_semaphore.release();
	if (m_closed) {
		return;
	}
	std::cout<<"SubscanScheduler::shutdown shutting down thread "<<std::endl;
	m_closed=true;
	while (isAlive()) {
		m_semaphore.release();
		usleep(500);
	}
	std::cout<<"SubscanScheduler::shutdown thread terminated"<<std::endl;
}

void SubscanScheduler::run() {
	ACS_SHORT_LOG((LM_DEBUG,"SubscanScheduler::run()"));
	do {
		m_semaphore.acquire();
		ACS_SHORT_LOG((LM_DEBUG,"SubscanScheduler::run(): signal received"));
		if (!check()) {
			break;
		}
		if (!m_closed) {
			executeSubscan();
		}
	} while (!m_closed);
	ACS_SHORT_LOG((LM_DEBUG,"Stopping SubscanScheduler thread"));
	setStopped();
}

void SubscanScheduler::executeSubscan() {
	AUTO_TRACE("SubscanScheduler::executeSubscan");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_mutex);
	std::cout<<"SubscanScheduler::executeSubscan mutex acquired"<<std::endl;
	if (m_subscans_v.empty()) {
		ACS_SHORT_LOG((LM_DEBUG,"No subscan to launch"));
		return;
	} else {
		ACS_SHORT_LOG((LM_DEBUG,"Launching a new subscan"));
		Control::NewTPP::SubscanInfo sInfo=m_subscans_v.front();
		m_tpp_p->launchSubscan(sInfo);
	}
}

/*___oOo___*/

