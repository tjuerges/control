/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
<<<<<<< DataReceiver.cpp
* "@(#) $Id$"
=======
* "@(#) $Id$"
>>>>>>> 1.15.2.4
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-01-16  created
*/
#include <Guard_T.h>
#include <Time_Value.h>

#include <errno.h>

#include "loggingMACROS.h"

#include <NewTotPwrProcErr.h>

#include <DataReceiver.h>
#include <DataReceiverThread.h>
#include <NewTPPImpl.h>
#include "TPP_TimeHelper.h"

const unsigned int DataReceiver::m_timeoutInterval=15;

DataReceiver::DataReceiver(
		const ACE_CString& name,
		PacketHandler* ph,
		bool swapDate,
		bool swapSamples,
		DataReceiverThread* thread):
	m_name(name),
	m_port(0),
	m_swapDate(swapDate),
	m_swapSamples(swapSamples),
	m_startTime(0),
	m_endTime(0),
	m_driftedTime(0),
	m_driftedTimeCalulated(false),
	m_subscanComplete(false),
	m_packetHandler_p(ph),
	m_connected(false),
	m_timedout(false),
	m_closed(false),
	m_thread_p(thread),
	m_debugFlag(false)
{
	m_tempPort=initServerSocket();
}

DataReceiver::~DataReceiver() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	if (!m_closed) {
		shutdown();
	}
	std::cout<<"DataReceiver::~DataReceiver("<<m_name.c_str()<<") done"<<std::cout<<endl;
}

int DataReceiver::initServerSocket() {
	AUTO_TRACE("DataReceiver::initServerSocket");
	struct sockaddr_in sockAddr;
	bzero(&sockAddr,sizeof(sockAddr));
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_addr.s_addr = INADDR_ANY;
	sockAddr.sin_port = htons(0);

	ACE_INET_Addr aceAddr(&sockAddr,sizeof(sockAddr));
	int ret=m_serverSocket.open(aceAddr,1);
	if (ret==-1) {
		NewTotPwrProcErr::SocketErrorExImpl ex(__FILE__, __LINE__, __func__);
		ex.setServerName(m_name);
		ex.setReason("Error opening server socket");
		throw ex.getSocketErrorEx();
	}

	ACE_HANDLE handle = m_serverSocket.get_handle();

   struct sockaddr_in sa;
   int sa_len;
	  /* We must put the length in a variable.              */
   sa_len = sizeof(sa);
	  /* Ask getsockname to fill in this socket's local     */
	  /* address.                                           */
   ret=getsockname(handle, (sockaddr*)&sa, (socklen_t*)&sa_len);
   if (ret==-1) {
   		NewTotPwrProcErr::SocketErrorExImpl ex(__FILE__, __LINE__, __func__);
   		ex.setServerName(m_name);
   		ex.setReason("Error getting the server port");
   		throw ex.getSocketErrorEx();
   	}
   ACS_SHORT_LOG((LM_DEBUG,"Server socket from %s listens on port %d",m_name.c_str(),(int)sa.sin_port));
   return (int)sa.sin_port;
}

bool DataReceiver::waitForClient() {
	ACE_INET_Addr peerAddr;
  	ACE_Time_Value tmout(DataReceiver::m_timeoutInterval);
	ACS_SHORT_LOG((LM_DEBUG,"DataReceiver %s waiting for incoming socket connection on port %d",m_name.c_str(),m_port));
	// Set the port so that the client can now connect
	m_port=m_tempPort;
  	int temp=m_serverSocket.accept (m_socket,&peerAddr,&tmout);
  	if (temp==-1) {
  		switch (ACE_OS::last_error()) {
  		case EINTR: {
  			ACS_SHORT_LOG((LM_WARNING,"Server socket interrupted (%s)",m_name.c_str()));
  			break;
  		}
  		case ETIMEDOUT: {
  			ACS_SHORT_LOG((LM_ERROR,"Timeout waiting connection from IFProc %s",m_name.c_str()));
  			timeout();
  			break;
  		}
  		default: {
  			ACS_SHORT_LOG((LM_ERROR,"Error %d connecting client socket (%s)",ACE_OS::last_error(),m_name.c_str()));
  		}
  		}
  		return false;
	}
	ACS_SHORT_LOG((LM_DEBUG,"%s connected with %s:%d",m_name.c_str(),peerAddr.get_host_addr(),peerAddr.get_port_number()));
	m_serverSocket.close();
	return true;
}

void DataReceiver::beginReceivingData(
		ACS::Time startTime,
		ACS::Time endTime) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	ACS_SHORT_LOG((LM_DEBUG,"DataReceiver::beginReceivingData %s [%s,%s]",
			m_name.c_str(),
			TPP_TimeHelper::toString(startTime).c_str(),
			TPP_TimeHelper::toString(endTime).c_str()));
	if (m_timedout) {
		// it never connected
		m_packetHandler_p->subscanDone(PacketHandler::TIMEOUT);
	}
	if (!m_subscanComplete && m_startTime>0) {
		ACS_SHORT_LOG((LM_ERROR,"%s Starting a subscan while another one is running old [%s,%s] new {%s, %s]",m_name.c_str(),
				TPP_TimeHelper::toString(m_startTime).c_str(),
				TPP_TimeHelper::toString(m_endTime).c_str(),
				TPP_TimeHelper::toString(startTime).c_str(),
				TPP_TimeHelper::toString(endTime).c_str()));
	}
	m_startTime=startTime;
	m_endTime=endTime;
	m_driftedTime=0;
	m_driftedTimeCalulated=false;
	m_subscanComplete=false;
	m_debugFlag=false;
}

void DataReceiver::abortSubscan() {
	AUTO_TRACE("DataReceiver::abortSubscan()");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	m_startTime=0;
	m_endTime=0;
	m_driftedTime=0;
	m_driftedTimeCalulated=false;
	m_subscanComplete=true;
	m_packetHandler_p->subscanDone(PacketHandler::ABORTED);
}

/**
 * The worker thread that establish the connection and reads incoming data
 *
 * @see ACS::Thread
 */
void DataReceiver::socketReceiver() {
	ACS_SHORT_LOG((LM_DEBUG,"DataReceiver::socketReceiver() started for %s",m_name.c_str()));
	// Wait for an incoming connection
	m_connected=waitForClient();
	 if (!m_connected) {
		 ACS_SHORT_LOG((LM_ERROR,"Socket for %s not connected: exiting receiving thread",m_name.c_str()));
		 return;
	 }
	 while (m_thread_p->check() && m_connected && !m_closed) {
		 // Read one packet

		//std::cout<<"DataReceiver::handle_input "<<m_name.c_str()<<std::endl;

		// Read the dimension of the buffer
		unsigned char samples=0;
		if (!m_thread_p->check() || !receiveData(&samples,1)||m_closed) {
			break;
		}

		if (m_thread_p->check()) {
			m_bufferMutex.acquire();
			m_buffer[0]=samples;
			m_bufferMutex.release();
		}

		// Read the timestamp
		ACS::Time timestamp;
		if (!m_thread_p->check() || !receiveData((unsigned char *)&timestamp,sizeof(ACS::Time))|| m_closed) {
			break;
		}
		// Swap the time stamp
		//std::cout<<"Timestamp before swapping: "<<timestamp<<" (HEX "<<std::hex<<timestamp<<std::dec<<") ";
		if (m_swapDate) {
			timestamp=swap(&timestamp);
		}
		//std::cout<<", after: "<<std::hex<<timestamp<<std::dec;
		//std::cout<<" ==> "<<timestamp<<" converted to ACS::TIME = ";
		// Convert the timestamp in ACS::Time format
		timestamp=convertTimestamp(timestamp);
		//std::cout<<timestamp<<std::endl;
		// NOTE: write the timestamp in the packet in linux format
		//       Do I have to swap again before returning to the client?
		if (m_thread_p->check()) {
			m_bufferMutex.acquire();
			memcpy(&m_buffer[1],&timestamp,sizeof(timestamp));
			m_bufferMutex.release();
		}


		// read the rest of the message

		if (!m_thread_p->check() || !receiveData(&m_buffer[1+sizeof(ACS::Time)],8*samples)||m_closed) {
			break;
		}
		if (m_thread_p->check() && m_connected && !m_closed && !m_subscanComplete) {
			m_bufferMutex.acquire();
			if (m_swapSamples) {
				swapSamples(samples,m_buffer);
			}
			if (m_thread_p->check() && m_connected && !m_closed) {
				// Check again because the mutex can be released during
				// a shudown
				handlePacket(m_buffer[0], timestamp, m_buffer);
			}
			m_bufferMutex.release();
		}
	 }

	 m_socket.close();
	 m_connected=false;
	 ACS_SHORT_LOG((LM_DEBUG,"DataReceiver::socketReceiver() for %s done",m_name.c_str()));
}

void DataReceiver::shutdown() {
	AUTO_TRACE("DataReceiver::shutdown");
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	if (m_closed) {
		// Already shut down
		return;
	}
	m_closed=true;
	ACS_SHORT_LOG((LM_DEBUG,"Shutting down the sockets (%s)",m_name.c_str()));
	m_serverSocket.close();
	m_socket.close();
	m_connected=false;
	std::cout<<"DataReceiver::shutdown Done "<<m_name.c_str()<<std::endl;
}

bool DataReceiver::receiveData(unsigned char buffer[], unsigned int sz) {
	// pos is the position in the buffer to store the data read
	// when ps>=sz then all the data have been read from the socket
	unsigned int pos=0;
	// The bytesTransferred can be usefule to know the bytes transferred in case of error/timeout
	size_t bytesTransferred=0;
	// Timeout 0.5 sec needed to know if the thread must stop
	const ACE_Time_Value timeout(0,500);
	// Chars read for each iteration (it is -1 in case of error/timeout and in that
	// case bytesTransferred must be used
	int charsRead=0;

	while (pos<sz) {
		charsRead=m_socket.recv_n(&buffer[pos],sz-pos,&timeout,&bytesTransferred);

		if (charsRead==-1) {
			// Timeout or ERROR
			int error=ACE_OS::last_error();
			if (error==ETIME || error==ETIMEDOUT) {
				// Timeout. Should the thread terminate?
				if (!m_thread_p->check() || m_closed || !m_connected) {
					return false;
				} else {
					pos+=bytesTransferred;
					continue;
				}
			} else {
				// Other error
				if (!m_closed) {
					m_packetHandler_p->subscanDone(PacketHandler::SOCKET_ERROR);
				}
				return false;
			}
		} else {
			pos+=charsRead;
		}
	}
	// std::cout<<"DataReceiver::receiveData ok num. of chars="<<pos<<std::endl;
	return true;
}

/**
 * Check the data in the packet against the requested interval and send it
 * to the listener
 *
 * <B>Implementation notes</B>: it can happen that a sample in a packet
 * 		falls only partially in the time interval of the subscan.
 * 		This method will accept those samples
 *
 * @param sample The number of samples in the packet
 * @param timestamp The timestamp in linux format
 * @param buffer The buffer
 * @return true if at least one sample of the packet has been accepted
 */
bool DataReceiver::handlePacket(unsigned int samples, ACS::Time timestamp, unsigned char* buffer) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	//std::cout<<"handlePacket "<<m_name.c_str()<<" samples="<<samples<<" timestamp "<<timestamp<<std::endl;
	// No subscan running ==> discard all the packets
	if (m_startTime==0 && m_endTime==0) {
		return false;
	}
	if (m_closed) {
		return false;
	}

	ACS::Time startPacketTime=timestamp+m_driftedTime;

	// Check if the sample of the packet are outside of the subscan
	if (startPacketTime>=m_endTime) {
		if (m_debugFlag==false) {
			std::cout<<"DataReceiver::handlePacket startPacketTime>=m_endTime "<<m_subscanComplete<<std::endl;
			m_debugFlag=true;
		}
		// After the subscan
		if (!m_subscanComplete) {
			m_packetHandler_p->subscanDone(PacketHandler::SUBSCAN_COMPLETE);
			m_subscanComplete=true;
		}
		//std::cout<<"Packet discarded because AFTER the subscan interval "<<m_name.c_str()<<std::endl;
		return false;
	}
	ACS::Time endPacketTime=timestamp+samples*NewTPPImpl::sm_sampleInterval;
	if (endPacketTime<=m_startTime) {
		// Before the subscan start
		//std::cout<<"Packet discarded because BEFORE the subscan interval"<<m_name.c_str()<<std::endl;
		return false;
	}

	// Calculate the drifted timestamp
	if (!m_driftedTimeCalulated) {
		calculateDriftedTime(startPacketTime);
	}

//	std::cout<<"handlePacket: Packet accepted (timestamp "<<timestamp<<", m_startTime ";
//	std::cout<<m_startTime<<", m_driftedTime="<<m_driftedTime;
//	std::cout<<" , sm_sampleInterval "<<NewTPPImpl::sm_sampleInterval<<' '<<m_name.c_str()<<std::endl;

	unsigned int first=0;
	unsigned int last=0;
	if (calculateIndexes(timestamp,samples,first,last)) {
		// Send packet to listener
		m_packetHandler_p->packetReceived(
				samples,
				timestamp+first*NewTPPImpl::sm_sampleInterval, // Timestamp of the first valid sample
				first,
				last,
				timestamp, // Time of the first sample
				timestamp+(samples-1)*NewTPPImpl::sm_sampleInterval, // Time of the last sample
				buffer,
				m_driftedTime);
		return true;
	}

	// return true if there is at least one valid sample
	std::cout<<"??????????????"<<std::endl;
	return false;
}



unsigned long long DataReceiver::swap(const unsigned long long* l) {
    unsigned long long ret;
    unsigned char *src = (unsigned char *)l;
    unsigned char *dst = (unsigned char *)&ret;

    dst[0] = src[7];
    dst[1] = src[6];
    dst[2] = src[5];
    dst[3] = src[4];
    dst[4] = src[3];
    dst[5] = src[2];
    dst[6] = src[1];
    dst[7] = src[0];

    return ret;
}

void DataReceiver::swapSamples(int samples, unsigned char* buffer) {
	for (int t=0; t<samples; t++) {
		int samplePos=1+sizeof(ACS::Time)+t*8;
		for (int j=0; j<4; j++) {
			uint16_t* can=(uint16_t*)&buffer[samplePos+2*j];
			uint16_t newVal=ntohs(*can);
			memcpy(&buffer[samplePos+2*j],&newVal,sizeof(uint16_t));
		}
	}
}

ACS::Time DataReceiver::convertTimestamp(ACS::Time timestamp) {
	return (timestamp>>2)/25;
}

bool DataReceiver::calculateIndexes(
		ACS::Time timestamp,
		unsigned int samples,
		unsigned int& first,
		unsigned int& last) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	// Start and end time of the packet taking drift into account
	ACS::Time packetStart=timestamp+m_driftedTime;
	ACS::Time packetEnd=timestamp+(samples-1)*NewTPPImpl::sm_sampleInterval+m_driftedTime;

	// First sample after the end time of the subscan
	if (packetStart>=m_endTime) {
		return false;
	}

	// Last sample before the start time of the subscan
	if (packetEnd<=m_startTime) {
		return false;
	}

	//
	// In the packet there is at least one packet that belongs to the subscan!!
	//

	// Check if all the samples belongs to the subscan
	if (packetStart>=m_startTime && packetEnd<=m_endTime) {
		first=0;
		last=samples-1;
		return true;
	}

	// Not all the samples of the packet belong to the subscan
	first=last=0;
	bool foundFirst = false;

	for (unsigned int t=0; t<samples; t++) {
		ACS::Time sampleStartTime=timestamp+t*NewTPPImpl::sm_sampleInterval+m_driftedTime;
		ACS::Time sampleEndTime=sampleStartTime+NewTPPImpl::sm_sampleInterval;

		if (!foundFirst && (sampleStartTime>=m_startTime &&sampleEndTime<=m_endTime)) {
			foundFirst=true;
			first=last=t;
		} else {
			if (sampleStartTime>=m_startTime &&sampleEndTime<=m_endTime) {
				last=t;
			}
		}
	}
	return true;
}

void DataReceiver::calculateDriftedTime(ACS::Time timestamp) {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	if (m_driftedTimeCalulated) {
		return;
	}
	while (!(timestamp<=m_startTime && timestamp+NewTPPImpl::sm_sampleInterval>m_startTime)) {
		if (timestamp<m_startTime) {
			timestamp+=NewTPPImpl::sm_sampleInterval;
		} else {
			timestamp-=NewTPPImpl::sm_sampleInterval;
		}
	}
	m_driftedTime=m_startTime-timestamp;

	if (m_driftedTime<0 || m_driftedTime>=NewTPPImpl::sm_sampleInterval) {
		ACS_SHORT_LOG((LM_ERROR,"Error calculating drift time %d",m_driftedTime));
	} else {
		m_driftedTimeCalulated=true;
	}
	ACS_SHORT_LOG((LM_DEBUG,"%s has a drift of %d",m_name.c_str(),m_driftedTime));
}

void DataReceiver::timeout() {
	ACE_Guard<ACE_Recursive_Thread_Mutex> guard(m_bufferMutex);
	m_timedout=true;
	if (m_startTime==0) {
		// no subscan in progress
		return;
	}
	m_packetHandler_p->subscanDone(PacketHandler::TIMEOUT);
}

/*___oOo___*/

