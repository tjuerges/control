/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2009
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package alma.Control.NewTotPwrProc.test;

import java.net.InetAddress;
import java.net.Socket;

import alma.Control.NewTPP;
import alma.Control.NewTPPHelper;
import alma.Control.NewTPPPackage.ConnectionInfo;
import alma.NewTotPwrProcErr.UnknownAntennaNameEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.component.client.ComponentClientTestCase;

/**
 * Test TPP methods
 * 
 * @author acaproni
 *
 */
public class NetworkTest  extends ComponentClientTestCase {

	/**
	 * The name of the component to test
	 */
	private String componentName;
	
	/**
	 * The TPP component
	 */
	private NewTPP tpp;
	
	/**
	 * Constructor
	 * 
	 * @throws Exception
	 */
	 public NetworkTest() throws Exception {
         super(NetworkTest.class.getName());
	 }
	
	 /**
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
    	super.setUp();
    	getTPP();
    	assertNotNull(tpp);
    }

    /**
     * @see junit.framework.TestCase#tearDown()
     */
    @Override
    protected void tearDown() throws Exception {
    	super.tearDown();
    	releaseTPP(tpp);
    	tpp=null;
    }
    
    /**
     * Get the TPP dynamic component
     * 
     * @return
     * @throws Exception
     */
    private void getTPP() throws Exception {
    	
    	ComponentQueryDescriptor cqDesc= new ComponentQueryDescriptor("NewTPP", "IDL:alma/Control/NewTPP:1.0");
        org.omg.CORBA.Object obj=this.getContainerServices().getDynamicComponent(cqDesc, false);
       	assertNotNull(obj);
       	tpp=NewTPPHelper.narrow(obj);
		assertNotNull(tpp);
		componentName=tpp.name();
    }
    
    private void releaseTPP(NewTPP tpp) {
    	if (tpp==null) {
    		throw new IllegalArgumentException("The component can't be null");
    	}
   		this.getContainerServices().releaseComponent(componentName);
    }
    
    /**
     * Test if adding a digitizer without an array throws an exception
     * 
     * @throws Exception
     */
    public void testAddDigitizerNoArray() throws Exception {
    	// set an empty array
    	String[] array = new String[0];
    	tpp.configureArray(array);
    	
    	// Add a digitizer: this triggers an UnknownAntennaNameEx  exception
    	ConnectionInfo ci=null;
    	try {
    		ci=tpp.addDigitizer("Antenna", 0);
    	} catch (UnknownAntennaNameEx ex) {
    		// this is the expected behavior
    		releaseTPP(tpp);
    		return;
    	}
    	throw new Exception("addDigitizer did not return an Exception while adding a digitzer nont in the array");
    }

    /**
     * Test the add digitizer by:
     * <UL>
     * 	<LI>create an array with one antenna
     * 	<LI>adding a digitizer
     * 	<LI>connecting a socket based on the {@link ConnectionInfo} returned by the IDL method
     * </UL>
     * 
     * @throws Exception
     */
    public void testAddDigitizer() throws Exception {
    	
    	// Set the array
    	String[] array = {
    			"Antenna"	
    	};
    	tpp.configureArray(array);
    	
    	ConnectionInfo ci=null;
    	ci=tpp.addDigitizer(array[0], 0);
    	assertNotNull("Got a NULL ConnectionInfo from the remote component",ci);
    	assertEquals(4,ci.IpAddress.length);
    	
    	InetAddress remoteAddress = InetAddress.getByAddress(ci.IpAddress);
    	Socket socket = new Socket(remoteAddress,ci.portNumber);
    	try {
    		Thread.sleep(10000);
    	} catch (InterruptedException e) {}
    	
    }
    
    /**
     * Test getting/setting the duration.
     * 
     * @throws Exception
     */
    public void testDuration() throws Exception {
    	double duration=5.25;
    	tpp.setIntegrationDuration(duration);
    	double returnedDuration=tpp.getIntegrationDuration();
    	assertEquals(duration, returnedDuration, 0);
    }
    
    /**
     * Test the isConencted method by adding a digitizer and 
     * checking the returned value.
     * <P>
     * The algorithm is very close to that of <code>testAddDigitizer</code>.
     * 
     * @throws Exception
     */
    public void testIsConnected() throws Exception {
    	assertFalse("This dizitizer shoult not appear as conencted", tpp.isConnected("Name", 3));
    	// Set the array
    	String[] array = {
    			"Antenna"	
    	};
    	tpp.configureArray(array);
    	
    	ConnectionInfo ci=null;
    	ci=tpp.addDigitizer(array[0], 0);
    	assertNotNull("Got a NULL ConnectionInfo from the remote component",ci);
    	assertEquals(4,ci.IpAddress.length);
    	
    	InetAddress remoteAddress = InetAddress.getByAddress(ci.IpAddress);
    	Socket socket = new Socket(remoteAddress,ci.portNumber);
    	try {
    		Thread.sleep(10000);
    	} catch (InterruptedException e) {}
    	assertTrue(tpp.isConnected(array[0], 0));
    	assertFalse(tpp.isConnected(array[0], 1));
    	assertFalse(tpp.isConnected("UnknownName", 0));
    }
}
