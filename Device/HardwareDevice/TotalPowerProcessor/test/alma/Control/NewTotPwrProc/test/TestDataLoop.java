/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2009
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package alma.Control.NewTotPwrProc.test;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

import alma.Control.NewTPP;
import alma.Control.NewTPPHelper;
import alma.Control.NewTPPPackage.ConnectionInfo;
import alma.Control.NewTPPPackage.SubscanInfo;
import alma.Control.TPP_TestComponent;
import alma.Control.TPP_TestComponentHelper;

import alma.bulkdata.BulkDataDistributer;
import alma.bulkdata.BulkDataDistributerHelper;

import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.acs.time.TimeHelper;

/**
 * Test the sending of the data through the socket.
 * <P>
 * The purpose of this test is to check that the TPP gets
 * all and only the data sent by the digitizer(s) during
 * a specific interval of time.
 * <P>
 * 
 * @author acaproni
 *
 */
public class TestDataLoop extends ComponentClientTestCase {
	
	/**
	 * The name of the TPP component to test
	 */
	private String componentName;
	
	/**
	 * The name of the bulk data receiver component
	 */
	private static final String recvCompName="TPP_RECEIVER";
	
	/**
	 * The name of the bulk data distributer 
	 */
	private static final String distrCompName="BulkDataDistributer";
	
	/**
	 * The name of the antenna for the digitizer to test
	 */
	private static final String[] antennaNames = {
		"SimulatedAntennaName",
		"AnotherAntenna"
	};
	
	/**
	 * The polarizations of the simulated digitizers
	 */
	private static final int[] polarizations = {
		0,
		1
	};
	
	/**
	 * The digitizers sending data to the TPP
	 * <P>
	 * The test creates one digitizer for each polarization
	 * and antenna name defined in <code>polarizations</code> 
	 * and <code>antennaNames</code> respectively.
	 * So there will be a total of <code>antennames.length*polarizations.length</code> 
	 * simulated digitizers.
	 */
	private SimulatedDigitizer[] digitizers;
	
	/**
	 * The TotalPowerProcess to test
	 */
	private NewTPP tpp=null;
	
	/**
	 * The bulkdata receiver
	 */
	private TPP_TestComponent receiver;
	
	/**
	 * The bulkdata distributer
	 */
	private BulkDataDistributer distributer;
	
	/**
	 * The logger
	 */
	private Logger logger;
	
	/**
	 * Container services
	 */
	private ContainerServices contSvcs;
	
	/**
	 * The size of the data sent to the TPP
	 */
	private static final int dataSize=192;

	/**
	 * Constructor
	 */
	public TestDataLoop() throws Exception {
		super(TestDataLoop.class.getName());
		System.out.println("Started");
	}

	/**
	 * @see ComponentClientTestCase
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		contSvcs=getContainerServices();
		assertNotNull(contSvcs);
		
		logger=contSvcs.getLogger();
		assertNotNull(logger);
		
		// Get a reference to the receiver
		org.omg.CORBA.Object recvObj=null;
		try {
			recvObj = this.getContainerServices().getComponent(recvCompName);
		} catch (Throwable t) {
			throw new Exception("Error getting component",t);
		} 
		assertNotNull(recvObj);
		receiver=TPP_TestComponentHelper.narrow(recvObj);
		assertNotNull(receiver);
		
		// Get a reference to the distributer
		org.omg.CORBA.Object distrObj=null;
		try {
			distrObj= this.getContainerServices().getComponent(distrCompName);
		} catch (Throwable t) {
			throw new Exception("Error getting component",t);
		} 
		assertNotNull(distrObj);
		distributer=BulkDataDistributerHelper.narrow(distrObj);
		assertNotNull(receiver);
		
		// Connect the distributer to the receiver
		distributer.multiConnect(receiver);
		
		// Get a reference to the TTP
		ComponentQueryDescriptor cqDesc= new ComponentQueryDescriptor("NewTPP", "IDL:alma/Control/NewTPP:1.0");
		org.omg.CORBA.Object obj=this.getContainerServices().getDynamicComponent(cqDesc, false);
     	assertNotNull(obj);
     	tpp=NewTPPHelper.narrow(obj);
		assertNotNull(tpp);
		componentName=tpp.name();
	}

	/**
	 * @see ComponentClientTestCase
	 */
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		if (tpp!=null) {
			this.getContainerServices().releaseComponent(componentName);
		}
		if (receiver!=null) {
			this.getContainerServices().releaseComponent(recvCompName);
		}
		if (distributer!=null) {
			this.getContainerServices().releaseComponent(distrCompName);
		}
	}
	
	/**
	 * Configure the array by adding an antenna
	 */
	private void configureArray() throws Exception {
		tpp.configureArray(antennaNames);
	}
	
	/**
	 * Add the digitizer to the TPP
	 * 
	 * @param antennaName The name of the antenna
	 * @param polarization The polarization
	 */
	private ConnectionInfo addDigitizer(String antennaName, int polarization) throws Exception {
		return tpp.addDigitizer(antennaName, polarization);
	}
	
	/**
	 * This method is the core of this test: it sends data to the
	 * TPP. Later one the same data are read to check for their integrity:
	 * <UL>
	 * 	<LI>define a {@link SubscanInfo} with a time interval
	 *  <LI>start sending data before during and after the time interval
	 *  <LI>check that the TPP discarded all the data out of the time interval
	 *  <LI>check that TPP accepted all the data inside the time interval
	 * </UL>
	 * 
	 * 
	 * @throws Exception
	 */
	public void testDataSending() throws Exception {
		logger.info("Adding the array");
		configureArray();
		logger.info("Adding one digitizer to the TPP");
		
		// Create the digitizers
		digitizers=new SimulatedDigitizer[antennaNames.length*polarizations.length];
		int t=0;
		byte base=0;
		for (String name: antennaNames) {
			for (int pol: polarizations) {
				logger.info("Adding digitizer "+name+":"+pol+" to the TPP");
				ConnectionInfo cInfo=addDigitizer(name,pol);
				assertNotNull(cInfo);
				digitizers[t]=new SimulatedDigitizer(name, pol, dataSize, cInfo,base,logger);
				assertNotNull(digitizers[t]);
				t++;
				base+=10;
			}
		}
		
		// Define the SubscanInfo.
		// Most of the fields of this structure are ignored at this stage.
		long javaStartTime=System.currentTimeMillis()+30*1000;
		long javaEndTime=javaStartTime+30*1000;
		long startTime=TimeHelper.javaToEpoch(javaStartTime).value;
		long endTime=TimeHelper.javaToEpoch(javaEndTime).value;
		SubscanInfo si = new SubscanInfo(
				startTime, 
				endTime, 
				"dataOID", 
				"execID", 
				0, // scanNumber 
				0, // subscanNumber
				new int[] {3,1,2,4}, //baseband, 
				polarizations);
		// Set the distributer
		tpp.setDistributer("BulkDataDistributer");
		
		tpp.setIntegrationDuration(1);
		
		// Start sending data
		tpp.beginSubscan(si);
		
		// Start the threads
		for (SimulatedDigitizer digitizer: digitizers) {
			logger.info("testDataSending starting thread "+digitizer.id);
			digitizer.sendData(javaStartTime, javaEndTime);
		}
		// wait until all the digitizers terminated
		for (SimulatedDigitizer digitizer: digitizers) {
			logger.info("testDataSending waiting termination of "+digitizer.id);
			digitizer.join();
		}
		logger.info("testDataSending done");
		
		// Leave the TPP time to reduce and send data
		try {
			Thread.sleep(30000);
		} catch (InterruptedException ie) {}
	}
}
