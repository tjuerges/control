/*
 * ALMA - Atacama Large Millimiter Array (c) European Southern Observatory, 2010
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package alma.Control.NewTotPwrProc.test;

import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import junit.framework.Assert;

import alma.Control.NewTPPPackage.ConnectionInfo;
import alma.acs.time.TimeHelper;

/**
 * A simulated digitizer for testing.
 * <P>
 * The digitizer sends data for the given antenna and polarization
 * with a dedicated thread.
 * 
 * @author acaproni
 *
 */
public class SimulatedDigitizer extends Assert implements Runnable {

	/**
	 * The name of the antenna
	 */
	public final String antennaName;
	
	/**
	 * The polarization of thie digitizer
	 */
	public final int polarization;
	
	/**
	 * A string to identify a digitizer.
	 * It is composed by the name and polarizations.
	 */
	public final String id;
	
	/**
	 * The logger
	 */
	private final Logger logger;
	
	/**
	 * The socket to send data to the TPP
	 */
	private Socket socket=null;
	
	/**
	 * The stream for sending data
	 */
	private DataOutputStream out;
	
	/**
	 * The packet to send through the socket to the TPP
	 */
	private final DataPacket dataPacket;
	
	/**
	 * The IP and port to connect the socket
	 */
	private final ConnectionInfo cInfo;
	
	/**
	 * The thread to send data
	 */
	private Thread thread=null; 
	
	/**
	 * Start time of the subscan
	 */
	private long start=0;
	
	/**
	 * End time of the subscan
	 */
	private long end=0;

	/**
	 * Build a simulated digitizer for the passed
	 * antenna and polarization
	 * 
	 * @param antennaName The name of the antenna
	 * @param polarization The polarization
	 * @param dataSize The size of a packet
	 * @param info The IP and port for connecting the socket
	 * @param base The base to set the values in the buffer
	 * @param logger The logger
	 */
	public SimulatedDigitizer(
			String antennaName, 
			int polarization,
			int dataSize,
			ConnectionInfo info,
			byte base,
			Logger logger) throws Exception {
		if (antennaName==null || antennaName.isEmpty()) {
			throw new IllegalArgumentException("Invalid antenna name");
		}
		assertNotNull(logger);
		assertNotNull(info);
		this.antennaName = antennaName;
		this.polarization = polarization;
		id=antennaName+":"+polarization;
		this.logger=logger;
		dataPacket= new DataPacket(dataSize,base);
		this.cInfo=info;
	}
	
	/**
	 * Connect the socket to the TPP
	 * 
	 * @param ci The server address and port to connect to
	 */
	private void connectSocket() throws Exception {
		assertNotNull(cInfo);
		StringBuilder str = new StringBuilder("Digitizer ");
		str.append(antennaName);
		str.append(':');
		str.append(polarization);
		str.append(" connecting to ");
		
		str.append(((int)cInfo.IpAddress[0]));
		str.append('.');
		Byte b=Byte.valueOf(cInfo.IpAddress[1]);
		str.append(b.toString());
		str.append('.');
		str.append(cInfo.IpAddress[2]);
		str.append('.');
		str.append(cInfo.IpAddress[3]);
		str.append(':');
		str.append(cInfo.portNumber);
		logger.info(str.toString());
		InetAddress addr=InetAddress.getByAddress(cInfo.IpAddress);
		socket=new Socket(addr,cInfo.portNumber);
		assertNotNull(socket);
		out = new DataOutputStream(socket.getOutputStream());
		assertNotNull(out);
		logger.info("Digitizer "+antennaName+":"+polarization+" socket connected on port "+cInfo.portNumber);
	}
	
	
	
	/**
	 * Send the data through the socket 
	 * 
	 * @param start The start time of the subscan
	 * @param end   The end time of the subscan
	 */
	public void sendData(long start, long end) throws Exception {
		logger.info("Digitizer "+antennaName+":"+polarization+" starting thread to send data");
		this.start=start;
		this.end=end;
		thread = new Thread(this);
		thread.setName(antennaName+":"+polarization);
		thread.start();
	}

	/**
	 * The thread to send data through the socket.
	 * <P>
	 * The thread aborts in case of exception
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.info("Digitizer "+antennaName+":"+polarization+" sending data");
		// Connect the socket
		try {
			connectSocket();
		} catch (Throwable t) {
			logger.log(Level.SEVERE,"Digitizer "+antennaName+":"+polarization+" error connecting socket "+t.getMessage());
			return;
		}
		
		// Define the SubscanInfo.
		// Most of the fields of this structure are ignored at this stage.
		long startTime=TimeHelper.javaToEpoch(start).value;
		long endTime=TimeHelper.javaToEpoch(end).value;
		
		// The following data should be discarded by TPP
		logger.info(id+" Sending data before interval");
		while (System.currentTimeMillis()<start-5*1000) {
			byte[] b=dataPacket.getBufferToSend();
			try {
				out.write(b);
			} catch (Throwable t) {
				logger.log(Level.SEVERE,"Digitizer "+antennaName+":"+polarization+" error sending data: "+t.getMessage());
				return;
			}
			for (int t=0; t<9; t++) {
				System.out.print(Integer.toHexString(b[t]).toUpperCase()+" ");
			}
			System.out.println();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				continue;
			}
		}
		logger.info(id+" Waiting begin of interval");
		while (System.currentTimeMillis()<start+5*1000) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				continue;
			}
		}
		while (System.currentTimeMillis()<end-5*1000) {
			try {
				out.write(dataPacket.getBufferToSend());
			} catch (Throwable t) {
				logger.log(Level.SEVERE,"Digitizer "+antennaName+":"+polarization+" error sending data: "+t.getMessage());
				return;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				continue;
			}
		}
		logger.info(id+" Waiting end of interval");
		while (System.currentTimeMillis()<end+5*1000) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				continue;
			}
		}
		logger.info(id+" Sending data after interval");
		while (System.currentTimeMillis()<end+15*1000) {
			try {
				out.write(dataPacket.getBufferToSend());
			} catch (Throwable t) {
				logger.log(Level.SEVERE,"Digitizer "+antennaName+":"+polarization+" error sending data: "+t.getMessage());
				return;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				continue;
			}
		}

		logger.info("Digitizer "+antennaName+":"+polarization+" data sent");
		try {
			socket.close();
			logger.info("Digitizer "+antennaName+":"+polarization+" socket closed");
		} catch (Throwable t) {
			logger.severe("Digitizer "+antennaName+":"+polarization+" error closing socket");
		}
	}
	
	/**
	 * Wait until the thread terminates
	 * 
	 * @throws InterruptedException
	 * @see {@link Thread#join()}
	 */
	public void join() throws InterruptedException {
		if (thread==null) {
			throw new NullPointerException("thread is null!"); 
		}
		thread.join();
	}
}
