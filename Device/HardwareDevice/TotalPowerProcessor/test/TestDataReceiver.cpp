/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-04-23  created
*/



#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

/**
 * Test the DataReceiver:
 * <UL>
 * 	<LI>Socket connection
 *  <LI>Data receipt
 *  <LI>Data sending (through the PacketHandler interface)
 * </UL>
 */

#include <iostream>
#include <pthread.h>
#include <unistd.h>

#include <logging.h>
#include <loggingGenericLogger.h>

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "ace/SStringfwd.h"
#include <ace/SOCK_Connector.h>

#include "acscommonC.h"
#include "NewTPPImpl.h"
#include "DataReceiver.h"

/**
 * The thread that executes the DataReceive thread (i.e. class run).
 *
 * @param ptr The DataReceiver whose run method is executed by this
 * 			  thread
 */
void* dataReceiverThread(void* ptr) {
	std::cout<<"Thread started "<<ptr<<std::endl;
	DataReceiver* recv=(DataReceiver*)ptr;
	recv->run();
	std::cout<<"Thread terminated"<<std::endl;
	pthread_exit(NULL);
}

const int maxBufferSize=1+sizeof(ACS::Time)+255*4*2;

class PacketReceiver: public PacketHandler {
public:
	/**
	 * Constructor
	 */
	PacketReceiver();

	/**
	 * Destructor
	 */
	virtual ~PacketReceiver();
	/**
	 * @see PacketHandler
	 */
	void packetReceived(
			int samples,
			ACS::Time startTimeOfFirstSample,
			int firstSampleOffset,
			int lastSampleOffset,
			ACS::Time startTime,
			ACS::Time endTime,
			unsigned char* buffer,
			unsigned int drift);
	/**
	 * Not used here because the test does not simulate a subscan but checks
	 * the receipt of data through the socket.
	 *
	 * @see PacketHandler
	 */
	bool subscanDone(int exitCode) { return true;};

	/**
	 * Compare the content of the local buffer with the passed buffer
	 *
	 * @param buffer The buffer to compare with the local buffer
	 * @return true if the content of the buffers is equal
	 */
	bool compareBuffers(unsigned char* buffer);

	/**
	 * Empty the buffer to be ready for a next iteration.
	 */
	void reset();

	/**
	 *
	 * @return true if at least one packet has been received
	 */
	bool hasReceivedPkts();

	/**
	 *
	 * @return The drift
	 */
	unsigned int getDrift() { return m_drift; }

private:
	unsigned char* m_localBuffer_p;

	// The drift received from tDataReceiver
	unsigned int m_drift;
};

PacketReceiver::PacketReceiver() {
	m_localBuffer_p=NULL;
}

PacketReceiver::~PacketReceiver() {
	reset();
}

void PacketReceiver::reset() {
	if (m_localBuffer_p!=NULL) {
		delete[] m_localBuffer_p;
		m_localBuffer_p=NULL;
	}
}

void PacketReceiver::packetReceived(
		int samples,
		ACS::Time startTimeOfFirstSample,
		int firstSampleOffset,
		int lastSampleOffset,
		ACS::Time startTime,
		ACS::Time endTime,
		unsigned char* buffer,
		unsigned int drift) {
	m_drift=drift;
	if (m_localBuffer_p==NULL) {
		m_localBuffer_p=new unsigned char[maxBufferSize];
	}
	// Clear the local buffer
	for (int t=0; t<maxBufferSize; t++) {
		m_localBuffer_p[t]=0;
	}
	// Copy the packet in the local buffer
	int sz=1+sizeof(ACS::Time)+samples*4*2;
	memcpy(m_localBuffer_p,buffer,sz);
}

bool PacketReceiver::hasReceivedPkts() {
	return m_localBuffer_p!=NULL;
}

bool PacketReceiver::compareBuffers(unsigned char* buffer) {
	if (buffer==NULL) {
		// Nothing to compare?
		std::cout<<"Passing a NULL buffer for comparison????"<<std::endl;
		return false;
	}
	if (m_localBuffer_p==NULL) {
		// No packet received
		std::cout<<"No packet received!"<<std::endl;
		return false;
	}
	// Check num of samples
	if (buffer[0]!=m_localBuffer_p[0]) {
		std::cout<<"Num of samples differ: sent="<<(int)buffer[0]<<", received="<<m_localBuffer_p[0]<<std::endl;
		return false;
	}
	// Check times
	ACS::Time localTime;
	memcpy(&localTime,&m_localBuffer_p[1],sizeof(ACS::Time));
	ACS::Time bufferTime;
	memcpy(&bufferTime,&buffer[1],sizeof(ACS::Time));
	if (bufferTime!=localTime*100) {
		std::cout<<"Dates differ: sent="<<bufferTime<<", received="<<localTime<<std::endl;
		return false;
	}
	// Check content
	for (int pkt=0; pkt<buffer[0]; pkt++) {
		int pos=1+sizeof(ACS::Time)+pkt*2*4;
		for (int t=pos; t<pos+8; t++) {
			if (buffer[t]!=m_localBuffer_p[t]) {
				std::cout<<"Data of packet "<<pkt<<" differs at pos="<<t;
				std::cout<<" (expected="<<(int)buffer[t]<<", received="<<(int)m_localBuffer_p[t];
				std::cout<<')'<<std::endl;
				return false;
			}
		}
	}
	return true;
}

class DataReceiverTestCase : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE(DataReceiverTestCase);
	CPPUNIT_TEST(testConnection);
	CPPUNIT_TEST(testDataSending);
	CPPUNIT_TEST(testDataSendingDrift);
	CPPUNIT_TEST(testDataSendingAfter);
	CPPUNIT_TEST(testDataSendingBefore);
	CPPUNIT_TEST(testNoDates);
	CPPUNIT_TEST(testStress);
	CPPUNIT_TEST_SUITE_END();
public:
	DataReceiverTestCase();
	virtual ~DataReceiverTestCase() {}
	void setUp();
	void tearDown();

protected:
	/**
	 * Connect the socket and test if the isConnected method
	 * returns the expected value
	 */
	void testConnection();

	/**
	 * Send a packet and check if it is correctly received
	 * by the handler (no drift)
	 */
	void testDataSending();

	/**
	 * Send a packet and check if it is correctly received
	 * by the handler (with drift)
	 */
	void testDataSendingDrift();

	/**
	 * Check if a packet whose date is before the time interval
	 * is discarded
	 */
	void testDataSendingBefore();

	/**
	 * Check if a packet whose date is before the time interval
	 * is discarded
	 */
	void testDataSendingAfter();

	/**
	 * Test if packets are discared when the beginReceivingData
	 * is not called.
	 */
	void testNoDates();

	/**
	 * Test the socket by sending and checking more packages
	 */
	void testStress();

private:
	/**
	 * The object notified when a packet is received
	 * through the socket
	 */
	PacketReceiver* m_receiver_p;

	/**
	 * The object to test
	 */
	DataReceiver* m_dataRecv_p;

	/**
	 * The socket to send data to the DataReceiver
	 */
	ACE_SOCK_Stream m_socket;

	/**
	 * DataReceiver thread.
	 * <P>
	 * It is started as posix thread in the test but int the component executes
	 * as ACSThread.
	 */
	pthread_t m_dataRecvThread;

	/**
	 * The buffer of data to send to the DataReceiver.
	 * <P>
	 * The length of the buffer is the biggest
	 */
	unsigned char* m_buffer_p;

	/**
	 * Connect the socket to the given port number of the local host
	 *
	 * @param port The port to connect to
	 * @return true if the socket is connected
	 */
	bool connect(int port);

	/**
	 * Build a packet to send to the DataReceiver
	 *
	 * @param date The date of the packet
	 * @param samples The samples in the packet (randomly generated)
	 * @param buffer The buffer to store the values into (randomly generated)
	 * @return the size of the buffer of valid dat
	 */
	int buildPacket(ACS::Time date, unsigned char& samples, unsigned char* buffer);
};

DataReceiverTestCase::DataReceiverTestCase():
	m_dataRecv_p(NULL),
	m_buffer_p(NULL)
{
	std::cout<<"DataReceiverTestCase::DataReceiverTestCase"<<std::endl;
}

void DataReceiverTestCase::setUp() {
	std::cout<<"DataReceiverTestCase::setUp"<<std::endl;
	m_buffer_p=new unsigned char[maxBufferSize];
	m_receiver_p=new PacketReceiver();
	m_dataRecv_p=new DataReceiver("DataReceiverTest",m_receiver_p,false,false);
	// Run the thread to receive data from socket
	int ret = pthread_create( &m_dataRecvThread, NULL, dataReceiverThread, (void*)m_dataRecv_p);
	CPPUNIT_ASSERT_MESSAGE("Error starting the DataReceiver thread", (ret==0));
	// Give the thread time to start
	// TODO: use a semaphore instead
	sleep(5);
	// initialize random seed
    srand ( time(NULL) );

}

void DataReceiverTestCase::tearDown() {
	std::cout<<"DataReceiverTestCase::tearDown"<<std::endl;
	m_dataRecv_p->shutdown();
	m_socket.close();
	// Wait the termination of the thread
	std::cout<<"DataReceiverTestCase::tearDown waiting thread termination"<<std::endl;
	pthread_join(m_dataRecvThread , NULL);

	std::cout<<"DataReceiverTestCase::tearDown deleting objects"<<std::endl;
	delete m_dataRecv_p;
	m_dataRecv_p=NULL;
	delete m_receiver_p;
	m_receiver_p=NULL;
	delete[] m_buffer_p;
	m_buffer_p=NULL;
}

bool DataReceiverTestCase::connect(int port) {
	ACE_INET_Addr srvr (ntohs(port), ACE_LOCALHOST);
	ACE_SOCK_Connector connector;
	if (-1 == connector.connect (m_socket, srvr)) {
		std::cout<<"Error connecting the socket"<<std::endl;
	  return false;
	}
	std::cout<<"Socket connected"<<std::endl;
	return true;
}

void DataReceiverTestCase::testConnection() {
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should not be connected!", (m_dataRecv_p->isConnected()==false));
	bool connected=connect(m_dataRecv_p->getServerPort());
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should be connected!", connected);
	m_socket.close();
	m_dataRecv_p->shutdown();
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should not be connected after shut down!", (m_dataRecv_p->isConnected()==false));
}

void DataReceiverTestCase::testDataSending() {
	bool connected=connect(m_dataRecv_p->getServerPort());
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should be connected!", connected);
	ACS::Time start=1000000;
	ACS::Time end=2000000;
	m_dataRecv_p->beginReceivingData(start,end);
	unsigned char nSamples=0;
	int sz=buildPacket(1500000,nSamples,m_buffer_p);
	int sent=m_socket.send_n(m_buffer_p,sz);
	CPPUNIT_ASSERT_MESSAGE("Socket failed sending data", sent==sz);
	// Give time to receive data
	sleep(3);
	CPPUNIT_ASSERT_MESSAGE("Data differs!", m_receiver_p->compareBuffers(m_buffer_p));
	// The drift should be 0
	CPPUNIT_ASSERT_MESSAGE("Drift should be 0!", (m_receiver_p->getDrift()==0));
	m_socket.close();
}

void DataReceiverTestCase::testDataSendingDrift() {
	bool connected=connect(m_dataRecv_p->getServerPort());
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should be connected!", connected);
	ACS::Time start=1000000;
	ACS::Time end=2000000;
	unsigned int drift=NewTPPImpl::sm_sampleInterval/2;
	m_dataRecv_p->beginReceivingData(start,end);
	unsigned char nSamples=0;
	int sz=buildPacket(1500000-drift,nSamples,m_buffer_p);
	int sent=m_socket.send_n(m_buffer_p,sz);
	CPPUNIT_ASSERT_MESSAGE("Socket failed sending data", sent==sz);
	// Give time to receive data
	sleep(3);
	CPPUNIT_ASSERT_MESSAGE("Data differs!", m_receiver_p->compareBuffers(m_buffer_p));
	// The drift should be drift
	CPPUNIT_ASSERT_MESSAGE("Drift should be 0!", (m_receiver_p->getDrift()==drift));
	m_socket.close();
}

void DataReceiverTestCase::testDataSendingBefore() {
	bool connected=connect(m_dataRecv_p->getServerPort());
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should be connected!", connected);
	ACS::Time start=2000000;
	ACS::Time end=5000000;
	m_dataRecv_p->beginReceivingData(start,end);
	unsigned char nSamples=0;
	int sz=buildPacket(500000,nSamples,m_buffer_p);
	int sent=m_socket.send_n(m_buffer_p,sz);
	CPPUNIT_ASSERT_MESSAGE("Socket failed sending data", sent==sz);
	// Give time to receive data
	sleep(3);
	CPPUNIT_ASSERT_MESSAGE("Data should differ!", !m_receiver_p->hasReceivedPkts());
	m_socket.close();
}

void DataReceiverTestCase::testDataSendingAfter() {
	bool connected=connect(m_dataRecv_p->getServerPort());
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should be connected!", connected);
	ACS::Time start=1000000;
	ACS::Time end=2000000;
	m_dataRecv_p->beginReceivingData(start,end);
	unsigned char nSamples=0;
	int sz=buildPacket(2500000,nSamples,m_buffer_p);
	int sent=m_socket.send_n(m_buffer_p,sz);
	CPPUNIT_ASSERT_MESSAGE("Socket failed sending data", sent==sz);
	// Give time to receive data
	sleep(3);
	CPPUNIT_ASSERT_MESSAGE("Data should differ!", !m_receiver_p->hasReceivedPkts());
	m_socket.close();
}

void DataReceiverTestCase::testNoDates() {
	bool connected=connect(m_dataRecv_p->getServerPort());
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should be connected!", connected);
	unsigned char nSamples=0;
	int sz=buildPacket(2500000,nSamples,m_buffer_p);
	int sent=m_socket.send_n(m_buffer_p,sz);
	CPPUNIT_ASSERT_MESSAGE("Socket failed sending data", sent==sz);
	// Give time to receive data
	sleep(3);
	CPPUNIT_ASSERT_MESSAGE("Data should differ!", !m_receiver_p->hasReceivedPkts());
	m_socket.close();
}

void DataReceiverTestCase::testStress() {
	bool connected=connect(m_dataRecv_p->getServerPort());
	CPPUNIT_ASSERT_MESSAGE("The DataReceiver should be connected!", connected);
	ACS::Time start=1000000;
	ACS::Time end=2000000;
	m_dataRecv_p->beginReceivingData(start,end);
	for (int t=0; t<60; t++) {
		m_receiver_p->reset();
		unsigned char nSamples=0;
		int sz=buildPacket(1500000,nSamples,m_buffer_p);
		int sent=m_socket.send_n(m_buffer_p,sz);
		CPPUNIT_ASSERT_MESSAGE("Socket failed sending data", sent==sz);
		// Give time to receive data
		sleep(1);
		CPPUNIT_ASSERT_MESSAGE("Data differs!", m_receiver_p->compareBuffers(m_buffer_p));
	}
	m_socket.close();
}

int DataReceiverTestCase::buildPacket(ACS::Time date, unsigned char& samples, unsigned char* buffer) {
	// Set the num. of samples greater then 50
	samples=0;
	while (samples<50) {
		samples=(unsigned char)rand()%256;
	}
	m_buffer_p[0]=samples;
	// The date sent by IFPRoc are not in ACS::Time format but in ACS::Time*100
	// so we correct the passed date for the conversion
	ACS::Time acsTimestamp=date*100;
	// Set the date
	memcpy(&m_buffer_p[1],&acsTimestamp,sizeof(date));
	// Set the samples
	for (int pos=1+sizeof(ACS::Time); pos<maxBufferSize; pos++) {
		m_buffer_p[pos]=(unsigned char)rand()%256;
	}
	return 1+sizeof(ACS::Time)+samples*4*2;
}

CPPUNIT_TEST_SUITE_REGISTRATION(DataReceiverTestCase);

int main(int argc, char *argv[])
{
	Logging::Logger::setGlobalLogger(new Logging::GenericLogger("loggerAntennasCnt"));

	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;

	// Add a listener that colllects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );

	// Add a listener that print dots as test run.
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );

	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );

	// Print test in a compiler compatible format.
	std::cout.flush();
	CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}
