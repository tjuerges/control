/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-04-25  created
*/



#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <sstream>

#include <logging.h>
#include <loggingGenericLogger.h>

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <SubscanData.h>
#include "PacketHandler.h"
#include "NewTPPImpl.h"

class SubScanDataTestCase : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE(SubScanDataTestCase);
	CPPUNIT_TEST(testDataAcquired);
	CPPUNIT_TEST(testGetAcquiredData);
	CPPUNIT_TEST(testAddingSamples);
	CPPUNIT_TEST(testWrongAddingSamples);
	CPPUNIT_TEST(testAddingSamplesDrifetd);
	CPPUNIT_TEST(testFillAll);
	CPPUNIT_TEST_SUITE_END();
public:
	SubScanDataTestCase();
	virtual ~SubScanDataTestCase() {}
	void setUp();
	void tearDown();
protected:
	/**
	 * Test if the dataAcquired method
	 */
	void testDataAcquired();

	/**
	 * Test the buffer with data when no samples have
	 * been added
	 */
	void testGetAcquiredData();

	/**
	 * Check the content of the flags and the buffer after adding
	 * a few samples (do not use the drifted time here)
	 */
	void testAddingSamples();

	/**
	 * Try to add sample in a wrong way. The addSamples should fail
	 * returning false;
	 */
	void testWrongAddingSamples();

	/**
	 * Check the content of the flags and the buffer after adding
	 * a few samples and with a defined drifted time
	 */
	void testAddingSamplesDrifetd();

	/**
	 * Fill all the samples of a subscan and check their values
	 */
	void testFillAll();

private:
	/**
	 * The start time of the subscan to test
	 */
	ACS::Time m_startTime;

	/**
	 * The end time of the subscan to test
	 */
	ACS::Time m_endTime;

	/**
	 * Number of samples in buffer
	 */
	unsigned int m_samplesInBuffer;

	/**
	 * The SubscanData to test
	 */
	SubscanData m_subscanData;
};

SubScanDataTestCase::SubScanDataTestCase():
		m_startTime(1000000),
		m_endTime(5000000),
		m_samplesInBuffer((m_endTime-m_startTime)/NewTPPImpl::sm_sampleInterval),
		m_subscanData(m_startTime,m_endTime,"SubScanDataTestCase")
{
}

void SubScanDataTestCase::setUp() {

}

void SubScanDataTestCase::tearDown() {

}

void SubScanDataTestCase::testDataAcquired() {
	CPPUNIT_ASSERT_MESSAGE("No data has been sent to the SubscanData!", (!m_subscanData.dataAcquired()) );
	m_subscanData.subscanComplete(PacketHandler::SUBSCAN_COMPLETE);
	CPPUNIT_ASSERT_MESSAGE("All data has been sent to the SubscanData!", (m_subscanData.dataAcquired()) );
}

void SubScanDataTestCase::testGetAcquiredData() {
	unsigned char* buffer;
	unsigned int samples;
	bool* flags;

	// No samples in buffer...
	bool ret=m_subscanData.getAcquiredData(&buffer,&samples,&flags);
	CPPUNIT_ASSERT_MESSAGE("No samples in buffer!", (!ret) );
	CPPUNIT_ASSERT_MESSAGE("Samples in buffer not 0!", (samples==0) );
	CPPUNIT_ASSERT_MESSAGE("Buffer not NULL!", (buffer==NULL) );
	CPPUNIT_ASSERT_MESSAGE("Flags not NULL!", (flags==NULL) );

	// Simulate a complete subscan: all the flags should be false and
	// the chars NULL
	m_subscanData.subscanComplete(PacketHandler::SUBSCAN_COMPLETE);
	ret=m_subscanData.getAcquiredData(&buffer,&samples,&flags);
	CPPUNIT_ASSERT_MESSAGE("No samples in buffer!", (ret) );
	CPPUNIT_ASSERT_MESSAGE("0 samples in buffer!", (samples==m_samplesInBuffer) );
	CPPUNIT_ASSERT_MESSAGE("Buffer not NULL!", (buffer!=NULL) );
	CPPUNIT_ASSERT_MESSAGE("Flags not NULL!", (flags!=NULL) );
	// Check if all flags are false
	int numOfTrues=0;
	for (unsigned int t=0; t<m_samplesInBuffer; t++) {
		if (flags[t]) {
			numOfTrues++;
		}
	}
	CPPUNIT_ASSERT_MESSAGE("All flags should be false!", (numOfTrues==0) );
	// Check if all the chars in the buffer are 0
	int numOfNotZero=0;
	for (unsigned int t=0; t<m_samplesInBuffer*2*NewTPPImpl::sm_channelsPerSample; t++) {
		if (buffer[t]!=0) {
			numOfNotZero++;
		}
	}
	CPPUNIT_ASSERT_MESSAGE("All chars should be 0!", (numOfTrues==0) );
}

void SubScanDataTestCase::testAddingSamples() {
	// Write some samples
	unsigned char samples[9+80] = {
			10, // size
			22,23,24,25,26,27,28,29, // Timestamp
			1,1,1,1,1,1,1,1,
			2,2,2,2,2,2,2,2,
			3,3,3,3,3,3,3,3, // (*)
			4,4,4,4,4,4,4,4,
			5,5,5,5,5,5,5,5,
			6,6,6,6,6,6,6,6,
			7,7,7,7,7,7,7,7,
			8,8,8,8,8,8,8,8,
			9,9,9,9,9,9,9,9,
			10,10,10,10,10,10,10,10 };
	// Add the samples upon (sample in position 2 (*) will be added
	// in position 0 in the SubscanData
	ACS::Time timestamp=m_startTime;
	int first=2;
	int last=5;
	unsigned int drift=0;
	bool accepted=m_subscanData.addSamples(samples,timestamp,first,last,drift);
	CPPUNIT_ASSERT_MESSAGE("Those samples should be accepted!", (accepted) );

	unsigned char* buffer;
	unsigned int nSamples;
	bool* flags;
	// Check the first 40 chars
	m_subscanData.subscanComplete(PacketHandler::SUBSCAN_COMPLETE);
	bool ret=m_subscanData.getAcquiredData(&buffer, &nSamples,&flags);
	CPPUNIT_ASSERT_MESSAGE("Subscan data should be available!", (ret) );

	// Check the flags
	for (int t=0; t<(signed)nSamples; t++) {
		if (t<last-first+1) {
			CPPUNIT_ASSERT_MESSAGE("The flag should be true!", (flags[t]) );
		} else {
			CPPUNIT_ASSERT_MESSAGE("The flag should be false!", (!flags[t]) );
		}
	}

	// Check the samples
	for (int t=0; t<(signed)nSamples; t++) {
		unsigned char cmp=0; // The char contained in the sample
		if (t>=0 && t<last-first+1) {
			cmp=samples[1+sizeof(ACS::Time)+(2*NewTPPImpl::sm_channelsPerSample)*(t+first)];
		}
		// Check each char of the sample
		for (unsigned int c=0; c<2*NewTPPImpl::sm_channelsPerSample; c++) {
			unsigned int idx=(t*2*NewTPPImpl::sm_channelsPerSample+c);
			CPPUNIT_ASSERT_MESSAGE("The content of the buffer is wrong!", (buffer[idx]==cmp) );
		}
	}
}

void SubScanDataTestCase::testAddingSamplesDrifetd() {
	// Write some samples
	unsigned char samples[9+80] = {
			10, // size
			22,23,24,25,26,27,28,29, // Timestamp
			1,1,1,1,1,1,1,1,
			2,2,2,2,2,2,2,2,
			3,3,3,3,3,3,3,3, // (*)
			4,4,4,4,4,4,4,4,
			5,5,5,5,5,5,5,5,
			6,6,6,6,6,6,6,6,
			7,7,7,7,7,7,7,7,
			8,8,8,8,8,8,8,8,
			9,9,9,9,9,9,9,9,
			10,10,10,10,10,10,10,10 };
	// Add the samples upon (sample in position 2 (*) will be added
	// in position 0 in the SubscanData
	ACS::Time timestamp=m_startTime;
	int first=2;
	int last=5;
	unsigned int drift=2500;
	bool accepted=m_subscanData.addSamples(samples,timestamp-drift,first,last,drift);
	CPPUNIT_ASSERT_MESSAGE("Those samples should be accepted!", (accepted) );

	unsigned char* buffer;
	unsigned int nSamples;
	bool* flags;
	// Check the first 40 chars
	m_subscanData.subscanComplete(PacketHandler::SUBSCAN_COMPLETE);
	bool ret=m_subscanData.getAcquiredData(&buffer, &nSamples,&flags);
	CPPUNIT_ASSERT_MESSAGE("Those samples should be accepted!", (ret) );

	// Check the flags
	for (int t=0; t<(signed)nSamples; t++) {
		if (t<last-first+1) {
			std::stringstream msg;
			msg<<"The flag at pos "<<t<<" should be true!";
			CPPUNIT_ASSERT_MESSAGE(msg.str().c_str(),(flags[t]) );
		} else {
			std::stringstream msg;
			msg<<"The flag at pos "<<t<<" should be false!";
			CPPUNIT_ASSERT_MESSAGE(msg.str().c_str(),(!flags[t]) );
		}
	}

	// Check the samples
	for (int t=0; t<(signed)nSamples; t++) {
		unsigned char cmp=0; // The char contained in the sample
		if (t>=0 && t<last-first+1) {
			cmp=samples[1+sizeof(ACS::Time)+(2*NewTPPImpl::sm_channelsPerSample)*(t+first)];
			std::cout<<"t="<<t<<", cmp="<<(int)cmp<<std::endl;
		}
		// Check each char of the sample
		for (unsigned int c=0; c<2*NewTPPImpl::sm_channelsPerSample; c++) {
			unsigned int idx=(t*2*NewTPPImpl::sm_channelsPerSample+c);
			CPPUNIT_ASSERT_MESSAGE("The content of the buffer is wrong!", (buffer[idx]==cmp) );
		}
	}
}

void SubScanDataTestCase::testWrongAddingSamples() {
	// Write some samples
	unsigned char samples[9+80] = {
		10, // size
		22,23,24,25,26,27,28,29, // Timestamp
		1,1,1,1,1,1,1,1,
		2,2,2,2,2,2,2,2,
		3,3,3,3,3,3,3,3,
		4,4,4,4,4,4,4,4,
		5,5,5,5,5,5,5,5,
		6,6,6,6,6,6,6,6,
		7,7,7,7,7,7,7,7,
		8,8,8,8,8,8,8,8,
		9,9,9,9,9,9,9,9,
		10,10,10,10,10,10,10,10 };

	ACS::Time timestamp=m_startTime;
	int first=2;
	int last=5;
	unsigned int drift=0;
	bool accepted=m_subscanData.addSamples(samples,timestamp,first,last,drift);
	CPPUNIT_ASSERT_MESSAGE("Those samples should be accepted!", (accepted) );

	// These samples should be rejected because of the drifted timestamp
	accepted=m_subscanData.addSamples(samples,timestamp,first,last,drift+5);
	CPPUNIT_ASSERT_MESSAGE("Samples with wrong drifted!", (!accepted) );

	// These should be rejected because the first timestamp is less then the
	// start timestamp of the SubscanData
	accepted=m_subscanData.addSamples(samples,500000,first,last,drift);
	CPPUNIT_ASSERT_MESSAGE("Samples with wrong timestamp!", (!accepted) );

	// These should be rejected because the pos of the first samples is wrong
	m_subscanData.addSamples(samples,timestamp,-1,last,drift);
	CPPUNIT_ASSERT_MESSAGE("First can't be negative!", (!accepted) );

	// These should be rejected because the pos of the first samples is wrong
	m_subscanData.addSamples(samples,timestamp,m_samplesInBuffer+1,last,drift);
	CPPUNIT_ASSERT_MESSAGE("First can't be greater then the size of the buffer!", (!accepted) );

	// These should be rejected because the pos of the last samples is wrong
	m_subscanData.addSamples(samples,timestamp,first,-1,drift);
	CPPUNIT_ASSERT_MESSAGE("Last can't be negative!", (!accepted) );

	// These should be rejected because the pos of the last samples is wrong
	m_subscanData.addSamples(samples,timestamp,first,m_samplesInBuffer+1,drift);
	CPPUNIT_ASSERT_MESSAGE("Last can't be greater then the size of the buffer!", (!accepted) );

	// Those should be rejected because the subscan is complete
	m_subscanData.subscanComplete(PacketHandler::SUBSCAN_COMPLETE);
	accepted=m_subscanData.addSamples(samples,timestamp,first,last,drift);
	CPPUNIT_ASSERT_MESSAGE("Samples should be rejected when subscan complete!", (!accepted) );
}

void SubScanDataTestCase::testFillAll() {
	// The samples to write in the subscan data
	// The test srores 10 samples at each iteration
	// The value of each byte of the sample is dynamically set
	unsigned char samples[9+80];
	samples[0]=10; // The packet contains 10 samples
	unsigned int drift=2500;
	ACS::Time timestamp=m_startTime-drift; // The timestamp of each packet

	unsigned int samplesInSubscan=(m_endTime-m_startTime)/NewTPPImpl::sm_sampleInterval;
	std::cout<<"Setting "<<samplesInSubscan<<" samples in the subscan ["<<m_startTime<<", "<<m_endTime<<']'<<std::endl;

	// Prepare the packet of data
	unsigned char c=0;
	for (unsigned int ss=0; ss<samplesInSubscan; ss+=10) {
		memcpy(&samples[1],&timestamp, sizeof(ACS::Time));
		for (unsigned char idx=0; idx<samples[0]; idx++) {
			for (unsigned int p=0; p<2*NewTPPImpl::sm_channelsPerSample;  p++) {
				samples[1+sizeof(ACS::Time)+idx*2*NewTPPImpl::sm_channelsPerSample+p]=c+idx;
			}
		}
		c+=samples[0];
		if (c>=200) {
			c=0;
		}
		// Print the buffer
//		std::cout<<"Buffer to store: "<<std::endl;
//		for (int j=0; j<samples[0]; j++) {
//			std::cout<<j<<"] ";
//			for (int y=0; y<2*NewTPPImpl::sm_channelsPerSample; y++) {
//				std::cout<<(int)samples[1+sizeof(ACS::Time)+2*j*NewTPPImpl::sm_channelsPerSample+y]<<' ';
//			}
//			std::cout<<std::endl;
//		}
		// Add samples to subscan
		m_subscanData.addSamples(samples,timestamp,0,9,drift);
		timestamp+=samples[0]*NewTPPImpl::sm_sampleInterval;
	}
	// All data has been sent
	m_subscanData.subscanComplete(PacketHandler::SUBSCAN_COMPLETE);
	// Get the data from the subscan
	unsigned char* buffer;
	unsigned int nSamples;
	bool* flags;
	bool ret=m_subscanData.getAcquiredData(&buffer, &nSamples,&flags);
	CPPUNIT_ASSERT_MESSAGE("The subscan should be terminated!", (ret) );
	CPPUNIT_ASSERT_MESSAGE("The returned and passed samples do not match", (nSamples==samplesInSubscan));

	// Check if all the flags are true
	for (unsigned int t=0; t<samplesInSubscan; t++) {
		CPPUNIT_ASSERT_MESSAGE("All the flags should be TRUE!", (flags[t]) );
	}

//	for (unsigned int t=0; t<samplesInSubscan; t++) {
//		std::cout<<t<<']';
//		if (flags[t]) {
//			std::cout<<" TRUE ";
//		} else {
//			std::cout<<" FALSE ";
//		}
//		for (unsigned int p=0; p<2*NewTPPImpl::sm_channelsPerSample;  p++) {
//			std::cout<<(int)buffer[t*2*NewTPPImpl::sm_channelsPerSample+p]<<' ';
//		}
//		std::cout<<std::endl;
//	}
//
//	for (unsigned int t=0; t<2*NewTPPImpl::sm_channelsPerSample*samplesInSubscan; t++) {
//		std::cout<<(int)buffer[t]<<' ';
//		if ((t+1)%8==0) {
//			std::cout<<std::endl;
//		}
//	}

	// Check the bytes of the samples
	c=0;
	for (unsigned int sample=0; sample<samplesInSubscan; sample++) {
		for (unsigned int p=0; p<2*NewTPPImpl::sm_channelsPerSample;  p++) {
			if(buffer[sample*2*NewTPPImpl::sm_channelsPerSample+p]!=c) {
				std::cout<<"Wrong data in buffer! at sample="<<sample<<", p="<<p<<" (pos="<<sample*2*NewTPPImpl::sm_channelsPerSample+p;
				std::cout<<") buffer[pos]="<<(int)buffer[sample*2*NewTPPImpl::sm_channelsPerSample+p]<<", c="<<(int)c<<std::endl;
			}
			CPPUNIT_ASSERT_MESSAGE("Wrong data in buffer!", (buffer[sample*2*NewTPPImpl::sm_channelsPerSample+p]==c) );
		}
		c++;
		if (c>=200) {
			c=0;
		}
	}


}

CPPUNIT_TEST_SUITE_REGISTRATION(SubScanDataTestCase);

int main(int argc, char *argv[])
{
	Logging::Logger::setGlobalLogger(new Logging::GenericLogger("loggerSubscanDataTest"));

	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;

	// Add a listener that colllects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );

	// Add a listener that print dots as test run.
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );

	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );

	// Print test in a compiler compatible format.
	std::cout.flush();
	CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}
