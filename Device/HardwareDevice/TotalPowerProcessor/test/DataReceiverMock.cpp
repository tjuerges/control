/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-27  created
*/

#include "vltPort.h"

#include "DataReceiverMock.h"

TestDataReceiver::TestDataReceiver():
	m_reactorManager_p(NULL),
	m_acceptor_p(NULL),
	m_receiver_p(NULL)
{
	m_reactorManager_p = new ReactorManager("TestreactorManager");
	m_receiver_p = new SocketReceiver(*m_reactorManager_p);
	m_acceptor_p = new SocketAcceptor("TestSocketAcceptor",*m_reactorManager_p,*m_receiver_p);
}

TestDataReceiver::~TestDataReceiver() {
	if (m_acceptor_p!=NULL) {
		delete m_acceptor_p;
		m_acceptor_p=NULL;
	}
	if (m_receiver_p!=NULL) {
		delete m_receiver_p;
		m_receiver_p=NULL;
	}
	if (m_reactorManager_p!=NULL) {
		m_reactorManager_p->shutdown();
		delete m_reactorManager_p;
		m_reactorManager_p=NULL;
	}
}
