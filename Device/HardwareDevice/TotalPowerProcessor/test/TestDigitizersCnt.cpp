/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-13  created
*/

////////////////////////////////////////////////////////////////////
//
//    Test DigitizersContainer
//
// Note: this test is done directly instantiating C++ objects i.e.
//		 does not need a running ACS instance
//
////////////////////////////////////////////////////////////////////

#include <iostream>

#include <logging.h>
#include <loggingGenericLogger.h>

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "NewTPPC.h"
#include "DigitizersContainer.h"
#include "DigitizerBaseTestImpl.h"

class DigitizersContainerTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(DigitizersContainerTestCase);
    CPPUNIT_TEST(testSize);
    CPPUNIT_TEST(testContains);
    CPPUNIT_TEST(testRemove);
    CPPUNIT_TEST(testClearAll);
    CPPUNIT_TEST(testGetDigitizer);
    CPPUNIT_TEST(testRmUnneededDgzs);
    CPPUNIT_TEST(testGetAntennas);
    CPPUNIT_TEST(testGetPolarizations);
    CPPUNIT_TEST(testAbortSubscan);
    CPPUNIT_TEST(testBeginSubscan);
    CPPUNIT_TEST_SUITE_END();

public:
	DigitizersContainerTestCase();
	virtual ~DigitizersContainerTestCase();
	void setUp();
	void tearDown();

protected:
	void testSize();
	void testContains();
	void testRemove();
	void testGetDigitizer();
	void testRmUnneededDgzs();
	void testGetAntennas();
	void testClearAll();
	void testGetPolarizations();
	void testBeginSubscan();
	void testAbortSubscan();

private:
	/**
	 * The containers to test
	 */
	DigitizersContainer cnt;

	/**
	 * The antenna name used for testing
	 */
	ACE_CString name;

	/**
	 * Put sz digitizers in the container.
	 * <P>
	 * The name of each added digitizer has DigitizersContainerTestCase.name as
	 * antenna name; the polarization is a sequential zero-based integer.
	 */
	int fillContainer(const unsigned int sz);

};

DigitizersContainerTestCase::DigitizersContainerTestCase():
	name("TestName")
{
}

DigitizersContainerTestCase::~DigitizersContainerTestCase()
{
	cnt.clearAll(false);
}

int DigitizersContainerTestCase::fillContainer(const unsigned int sz) {
	for (unsigned int t=0; t<sz; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(name.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == (int)sz) );
	return cnt.size();
}

/**
 * Test the size()
 */
void DigitizersContainerTestCase::testSize()
{
	CPPUNIT_ASSERT_MESSAGE("The size of the empty container should 0", (cnt.size() == 0) );
	for (unsigned int t=0; t<16; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(name.c_str(),t);
		cnt.appendDigitizer(temp);
		CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == (int)(t+1)) );
	}
}

void DigitizersContainerTestCase::testContains() {
	int sz=fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == sz) );
	// The following object is in the container
	DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(name.c_str(),25);
	CPPUNIT_ASSERT_MESSAGE("The item is in the container!", cnt.containsDigitizer(*temp) );
	CPPUNIT_ASSERT_MESSAGE("The digitizer is in the container!", cnt.containsDigitizer(name.c_str(),25) );
	delete temp;
	// Now try with an elment not in the container
	temp = new DigitizerBaseTestImpl(name.c_str(),1000);
	CPPUNIT_ASSERT_MESSAGE("The item is NOT in the container!", (cnt.containsDigitizer(*temp)==false) );
	CPPUNIT_ASSERT_MESSAGE("The digitizer is in the container!", (cnt.containsDigitizer("Prova",25)==false));
	delete temp;
}

void DigitizersContainerTestCase::testRemove() {
	fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 50) );
	// Remove item 25
	DigitizerBase* removed=cnt.removeDigitizer(name.c_str(),25);
	CPPUNIT_ASSERT_MESSAGE("Item to remove not found!", (removed!=NULL));
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 49) );
	CPPUNIT_ASSERT_MESSAGE("The item is still in the container!", (cnt.containsDigitizer(name.c_str(),25)==false) );
}

void DigitizersContainerTestCase::testClearAll() {
	fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 50) );
	cnt.clearAll();
	CPPUNIT_ASSERT_MESSAGE("The container is not empty!", (cnt.size() == 0) );
}

void DigitizersContainerTestCase::testGetDigitizer() {
	fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 50) );
	DigitizerBase* temp = cnt.getDigitizer(name.c_str(),25);
	CPPUNIT_ASSERT_MESSAGE("Digitizer not found!", (temp!=NULL) );
	temp=cnt.getDigitizer(name.c_str(),1000);
	CPPUNIT_ASSERT_MESSAGE("Digitizer found?!?!", (temp==NULL) );
	temp=cnt.getDigitizer("Test",25);
	CPPUNIT_ASSERT_MESSAGE("Digitizer found?!?!", (temp==NULL) );
}

void DigitizersContainerTestCase::testGetAntennas() {
	CPPUNIT_ASSERT_MESSAGE("The size of the empty container should 0", (cnt.size() == 0) );
	for (unsigned int t=0; t<16; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(name.c_str(),t);
		cnt.appendDigitizer(temp);
		CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == (int)(t+1)) );
	}
	std::set<ACE_CString> names=cnt.getAntennas();
	// It is enough to check the size because the set has no repeated items
	// The size should be 0 because all the added digitizer have the same name
	CPPUNIT_ASSERT_MESSAGE("Wrong set of names", (names.size()==1));
	std::set<ACE_CString>::iterator it=names.find(name);
	CPPUNIT_ASSERT_MESSAGE("Name not found in set", (*it==name));

	// Add one more digitizer with another name
	DigitizerBaseTestImpl* dgt = new DigitizerBaseTestImpl("AnotherAntenna",0);
	CPPUNIT_ASSERT_MESSAGE("Error adding a digitizer", cnt.appendDigitizer(dgt));
	std::set<ACE_CString> newNames=cnt.getAntennas();
	CPPUNIT_ASSERT_MESSAGE("Wrong new set of names", (newNames.size()==2));
}

/**
 * Test the method to remove unneeded digitizers
 */
void DigitizersContainerTestCase::testRmUnneededDgzs() {
	//
	// First remove entries from beginning and end
	//

	// Fill the container by adding 50 items with an antenna name equal to name
	fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 50) );
	// Add 10 items with antenna Name2
	ACE_CString newName2("Name2");
	for (unsigned int t=0; t<10; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName2.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 60) );
	// Add 20 items with antenna Name3
	ACE_CString newName3("Name3");
	for (unsigned int t=0; t<20; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName3.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 80) );
	std::vector<ACE_CString> array;
	array.push_back(newName2);
	int ret=cnt.removeUnneededDigitizers(array,false,true);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 10) );
	CPPUNIT_ASSERT_MESSAGE("Wrong value returned from method!", (ret == 70) );
	//
	// Remove entries from the middle
	//
	cnt.clearAll();
	// Fill the container by adding 50 items with an antenna name equal to name
	fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 50) );
	// Add 10 items with antenna Name2
	for (unsigned int t=0; t<10; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName2.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 60) );
	// Add 20 items with antenna Name3
	for (unsigned int t=0; t<20; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName3.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 80) );
	array.clear();
	array.push_back(newName3);
	array.push_back(name);
	ret=cnt.removeUnneededDigitizers(array,false,true);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 70) );
	CPPUNIT_ASSERT_MESSAGE("Wrong value returned from method!", (ret == 10) );

	//
	// Remove all the entries
	//
	cnt.clearAll();
	// Fill the container by adding 50 items with an antenna name equal to name
	fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 50) );
	// Add 10 items with antenna Name2
	for (unsigned int t=0; t<10; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName2.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 60) );
	// Add 20 items with antenna Name3
	for (unsigned int t=0; t<20; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName3.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 80) );
	array.clear();
	array.push_back(ACE_CString("AnotherName"));
	ret=cnt.removeUnneededDigitizers(array,false,true);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 0) );
	CPPUNIT_ASSERT_MESSAGE("Wrong value returned from method!", (ret == 80) );

	//
	// Keep all the entries
	//
	cnt.clearAll();
	// Fill the container by adding 50 items with an antenna name equal to name
	fillContainer(50);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 50) );
	// Add 10 items with antenna Name2
	for (unsigned int t=0; t<10; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName2.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 60) );
	// Add 20 items with antenna Name3
	for (unsigned int t=0; t<20; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName3.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 80) );
	array.clear();
	array.push_back(newName3);
	array.push_back(name);
	array.push_back(newName2);
	ret=cnt.removeUnneededDigitizers(array,false,true);
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 80) );
	CPPUNIT_ASSERT_MESSAGE("Wrong value returned from method!", (ret == 0) );
}

void DigitizersContainerTestCase::setUp()
{
	cnt.clearAll(false);
	CPPUNIT_ASSERT_MESSAGE("The size of the new container should 0", (cnt.size() == 0) );
}

void DigitizersContainerTestCase::tearDown()
{
	cnt.clearAll(false);
}

void DigitizersContainerTestCase::testGetPolarizations()
{
	fillContainer(10);
	ACE_CString newName("NewName");
	for (int t=10; t<20; t++) {
		DigitizerBaseTestImpl* temp = new DigitizerBaseTestImpl(newName.c_str(),t);
		cnt.appendDigitizer(temp);
	}
	CPPUNIT_ASSERT_MESSAGE("The size of the container is wrong!", (cnt.size() == 20) );
	std::set<CORBA::Long> pols=cnt.getPolarizations(name.c_str());
	CPPUNIT_ASSERT_MESSAGE("The size of the polarizations is wrong!", (pols.size() == 10) );
	for (int t=0; t<10; t++) {
		CPPUNIT_ASSERT_MESSAGE("Missing polarization!", (pols.count(t) == 1) );
	}
	for (int t=11; t<20; t++) {
		CPPUNIT_ASSERT_MESSAGE("Wrong polarization in set!", (pols.count(t) == 0) );
	}
}

/**
 * Test if the begin subscan is propagated to all the digitizer
 * in the container
 * <P>
 * The beginSubscan method of DigitizerBasetestImpl prints out a message
 * when it is calles so tat will check for correctness of the test
 */
void DigitizersContainerTestCase::testBeginSubscan() {
	Control::NewTPP::SubscanInfo info;
	info.startTime=10;
	info.endTime=100;
	fillContainer(5);
	cnt.beginSubscan(info,NULL);
}

/**
 * Test if the abort subscan is propagated to all the digitizer
 * in the container.
 * <P>
 * The abortSubscan method of DigitizerBasetestImpl prints out a message
 * when it is calles so tat will check for correctness of the test
 */
void DigitizersContainerTestCase::testAbortSubscan() {
	fillContainer(5);
	cnt.abortSubscan();
}

CPPUNIT_TEST_SUITE_REGISTRATION(DigitizersContainerTestCase);

int main(int argc, char *argv[])
{
	Logging::Logger::setGlobalLogger(new Logging::GenericLogger("loggerDigitizersCnt"));

	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;

	// Add a listener that colllects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );

	// Add a listener that print dots as test run.
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );

	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );

	// Print test in a compiler compatible format.
	std::cout.flush();
	CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}
