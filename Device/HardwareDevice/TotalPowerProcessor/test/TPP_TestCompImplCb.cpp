/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2010-03-16  created
*/

#include "vltPort.h"

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "TPP_TestCompImplCb.h"

using namespace ACSBulkDataError;

TPP_TestCompImplCb::TPP_TestCompImplCb(): m_bytesToRead(0), m_bytesRead(0)
{
    ACS_TRACE("TPP_TestCompImplCb::TPP_TestCompImplCb");

    // internal timeout for this callback: 5 loops for 1 sec = 12 sec
    ACE_Time_Value waitPeriod_m;
    waitPeriod_m.set(1L, 0L);
    setSleepTime(waitPeriod_m);
    setSafeTimeout(5);

}

TPP_TestCompImplCb::~TPP_TestCompImplCb()
{
    ACS_TRACE("TPP_TestCompImplCb::~TPP_TestCompImplCb");
}

int
TPP_TestCompImplCb::cbStart(ACE_Message_Block * userParam_p)
{
	ACS_TRACE("TPP_TestCompImplCb::cbStart");

    char message[256];
    ACE_OS::strcpy(message, userParam_p->rd_ptr());
    ACS_SHORT_LOG((LM_INFO,"TPP_TestCompImplCb::cbStart - message: [%s], len=%d",message,strlen(message)));

    // Get the len out of the string
    char num[32];
    char uid[32];
    // find the latest ' '
    int pos=-1;
    for (int t=0; t<strlen(message); t++) {
    	if (message[t]==' ') {
    		pos=t;
    	}
    }
    if (pos==-1 || pos==strlen(message)) {
    	ACS_SHORT_LOG((LM_ERROR,"TPP_TestCompImplCb::cbStart - format error"));
    }

    for (int t=0; t<=pos; t++) {
    	uid[t]=message[t];
    }
    uid[pos]=0;

    for (int t=pos+1; t<strlen(message); t++) {
    	num[t-pos-1]=message[t];
    }
    num[strlen(message)]=0;
    sscanf(num,"%d",&m_bytesToRead);

    ACS_SHORT_LOG((LM_INFO,"TPP_TestCompImplCb::cbStart - data UID: [%s], num [%d], pos=%d",uid,m_bytesToRead,pos));

    m_bytesRead=0;
    return 0;
}

int
TPP_TestCompImplCb::cbReceive(ACE_Message_Block * frame_p)
{
	ACS_TRACE("TPP_TestCompImplCb::cbReceive");
	ACE_Data_Block* data=frame_p->data_block();
	m_bytesRead+=data->size();

    return 0;
}

int
TPP_TestCompImplCb::cbStop()
{
	ACS_TRACE("TPP_TestCompImplCb::cbStop");

	ACS_SHORT_LOG((LM_INFO,"TPP_TestCompImplCb::cbStart - %d bytes received <==> %d bytes expected",m_bytesRead,m_bytesToRead));

    return 0;
}


/* --------------- [ MACI DLL support functions ] -----------------*/
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(TPP_TestCompImpl<TPP_TestCompImplCb>)
/* ----------------------------------------------------------------*/


/*___oOo___*/
