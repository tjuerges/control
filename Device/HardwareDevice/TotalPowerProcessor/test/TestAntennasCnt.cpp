/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2009
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* acaproni  2009-05-14  created
*/

////////////////////////////////////////////////////////////////////
//
//    Test AntennaContainer
//
// Note: this test is done directly instantiating C++ objects i.e.
//		 does not need a running ACS instance
//
////////////////////////////////////////////////////////////////////

#include <iostream>

#include <logging.h>
#include <loggingGenericLogger.h>

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include "ControlAntennaInterfacesS.h"

#include "AntennasContainer.h"
#include "DigitizerBaseTestImpl.h"
#include "DigitizersContainer.h"

class AntennasContainerTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(AntennasContainerTestCase);
    CPPUNIT_TEST(testSize);
    CPPUNIT_TEST(testClear);
    CPPUNIT_TEST(testContains);
    CPPUNIT_TEST(testGetAntennaAt);
    CPPUNIT_TEST(testFindAntenna);
    CPPUNIT_TEST(testGetAntennaNames);
    CPPUNIT_TEST_SUITE_END();

public:
	AntennasContainerTestCase();
	virtual ~AntennasContainerTestCase();
	void setUp();
	void tearDown();

protected:
	void testSize();
	void testClear();
	void testContains();
	void testGetAntennaAt();
	void testFindAntenna();
	void testGetAntennaNames();

private:

	/**
	 * The container of digitizers
	 */
	DigitizersContainer digitizers;

	/**
	 * The containers to test
	 */
	AntennaContainer* antennas;

	/**
	 * The antenna name used for testing
	 */
	ACE_CString name;

	/**
	 * Return an array of sz antenna names whose name is
	 * built by adding the number to the member string name
	 */
	Control::AntennaSeq fillArray(unsigned int sz);

};

AntennasContainerTestCase::AntennasContainerTestCase():
	antennas(NULL),
	name("Antenna")
{
	antennas = new AntennaContainer(digitizers);
}

AntennasContainerTestCase::~AntennasContainerTestCase()
{
	delete antennas;
}

/**
 * Test the size()
 */
void AntennasContainerTestCase::testSize()
{
	CPPUNIT_ASSERT_MESSAGE("The size of the empty container should 0", (antennas->size() == 0) );
	Control::AntennaSeq seq = fillArray(50);
	antennas->setArray(seq);
	CPPUNIT_ASSERT_MESSAGE("The size of the container should be 50!", (antennas->size() == 50) );
}

void AntennasContainerTestCase::testContains() {
	Control::AntennaSeq seq = fillArray(50);
	antennas->setArray(seq);
	CPPUNIT_ASSERT_MESSAGE("The size of the container should be 50!", (antennas->size() == 50) );
	char* buffer = new char[name.length()+8];
	for (int t=0; t<50; t++) {
		sprintf(buffer,"%s_%d",name.c_str(),t);
		CPPUNIT_ASSERT_MESSAGE("Antenna not found in the container", (antennas->contains(buffer)));
	}
	delete[] buffer;
	// Try to get an antenna not in the container
	CPPUNIT_ASSERT_MESSAGE("Antenna found in the container", (!antennas->contains("Test")));
}

void AntennasContainerTestCase::testClear() {
	Control::AntennaSeq seq = fillArray(50);
	antennas->setArray(seq);
	CPPUNIT_ASSERT_MESSAGE("The size of the container should be 50!", (antennas->size() == 50) );
	antennas->clear();
	CPPUNIT_ASSERT_MESSAGE("The size of the empty container should 0", (antennas->size() == 0) );
	seq = fillArray(25);
	antennas->setArray(seq);
	CPPUNIT_ASSERT_MESSAGE("The size of the container should be 50!", (antennas->size() == 25) );
}

void AntennasContainerTestCase::testGetAntennaAt() {
	Control::AntennaSeq seq = fillArray(50);
	antennas->setArray(seq);
	CPPUNIT_ASSERT_MESSAGE("The size of the container should be 50!", (antennas->size() == 50) );
	char* buffer = new char[name.length()+8];
	for (unsigned int t=0; t<50; t++) {
		sprintf(buffer,"%s_%d",name.c_str(),t);
		CPPUNIT_ASSERT_MESSAGE("Names do not match!", (strcmp(buffer,antennas->getAntennaAt(t))==0) );
	}
	delete[] buffer;
}

void AntennasContainerTestCase::testFindAntenna() {
	Control::AntennaSeq seq = fillArray(50);
	antennas->setArray(seq);
	CPPUNIT_ASSERT_MESSAGE("The size of the container should be 50!", (antennas->size() == 50) );
	// try to find an antenna not present in the container
	int pos=antennas->findAntenna("PIPPO");
	// Check existing antenna names
	CPPUNIT_ASSERT_MESSAGE("An antenna with this name does not exist", (pos==-1));
	char* buffer = new char[name.length()+8];
	for (int t=0; t<50; t++) {
		sprintf(buffer,"%s_%d",name.c_str(),t);
		int idx=antennas->findAntenna(buffer);
		CPPUNIT_ASSERT_MESSAGE("wrong index returned", (idx==t) );
	}
	delete[] buffer;
}

void AntennasContainerTestCase::testGetAntennaNames() {
	Control::AntennaSeq seq = fillArray(50);
	antennas->setArray(seq);
	CPPUNIT_ASSERT_MESSAGE("The size of the container should be 50!", (antennas->size() == 50) );
	std::set<ACE_CString>  names=antennas->getAntennaNames();
	// If is enough to test the size because the set does not contain duplicated items
	CPPUNIT_ASSERT_MESSAGE("Wrong number of antenna names returned", (names.size()==50) );
}

Control::AntennaSeq AntennasContainerTestCase::fillArray(unsigned int sz) {
	Control::AntennaSeq ret;
	ret.length(sz);
	for (CORBA::ULong t=0; t<ret.length(); t++) {
		char* buffer = new char[name.length()+8];
		sprintf(buffer,"%s_%d",name.c_str(),t);
		ret[t]=buffer;
	}
	return ret;
}

void AntennasContainerTestCase::setUp()
{
	antennas->clear();
	CPPUNIT_ASSERT_MESSAGE("The size of the new container should be 0", (antennas->size() == 0) );
}

void AntennasContainerTestCase::tearDown()
{
	antennas->clear();
	CPPUNIT_ASSERT_MESSAGE("The size of the new container should be 0", (antennas->size() == 0) );
}

CPPUNIT_TEST_SUITE_REGISTRATION(AntennasContainerTestCase);

int main(int argc, char *argv[])
{
	Logging::Logger::setGlobalLogger(new Logging::GenericLogger("loggerAntennasCnt"));

	// Create the event manager and test controller
	CPPUNIT_NS::TestResult controller;

	// Add a listener that colllects test result
	CPPUNIT_NS::TestResultCollector result;
	controller.addListener( &result );

	// Add a listener that print dots as test run.
	CPPUNIT_NS::BriefTestProgressListener progress;
	controller.addListener( &progress );

	// Add the top suite to the test runner
	CPPUNIT_NS::TestRunner runner;
	runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
	runner.run( controller );

	// Print test in a compiler compatible format.
	std::cout.flush();
	CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}
