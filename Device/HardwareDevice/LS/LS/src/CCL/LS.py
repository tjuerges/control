#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


"""
This module is part of the Control Command Language.
It contains complementary functionality for a LS device.
"""

import CCL.LSBase
from CCL import StatusHelper

class LS(CCL.LSBase.LSBase):
    '''
    The LS class inherits from the code generated CCL.LSBase
    class and adds specific methods.
    '''
    def __init__(self, photonicReference = None,
                 componentName = None, stickyFlag = False):
        '''
        The constructor creates a LS object using default constructor.
        You can specify which LS you would like either by number (1-6)
        or by PhotonicReference (i.e. PhotonicReference2)
        EXAMPLE:
        import CCL.LS
        # To get the LS associated with photonic reference 3
        ls = CCL.LS.LS(3)
        or 
        ls = CCL.LS.LS("PhotonicReference3")
        '''

        if componentName is None:
            if photonicReference is None:
                raise IllegalParameterErrorEx('You must specify either a '
                                              + ' photonic reference');
            if isinstance(photonicReference, int):
                photonicReference= 'PhotonicReference%d' % photonicReference

            componentName = "CONTROL/CentralLO/%s/LS" % photonicReference

        name = componentName.split('/')[2]
        self._lsNo = int(name[len(name)-1])
        
        try:
            CCL.LSBase.LSBase.__init__(self,
                antennaName = None, componentName = componentName,
                stickyFlag = stickyFlag)
        except Exception, e:
            print 'LS component is not running \n' + str(e)

    def __del__(self):
        CCL.LSBase.LSBase.__del__(self)

    def STATUS(self):
        '''
        Method to print relevant status information to the screen
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

        elements = []
        #
        # GET_SYSTEM_STATUS
        #
        try:
            value = self._devices[key].GET_SYSTEM_GET_STATUS()
        except:
            value = -1

        if(value != -1):
            state = self._devices[key].GET_SYSTEM_GET_STATUS_SYSTEM_STATE()[0]
            if (state == 0x00):
                system_state = "Startup"
            elif(state == 0x01):
                system_state = "Wait for Interlock key"
            elif(state == 0x02):
                system_state = "Standby"
            elif(state == 0x03):
                system_state = "Phase locking"
            elif(state == 0x04):
                system_state = "Operational"
            elif(state == 0x05):
                system_state = "Manual"
            else:
                system_state = "N/A"

            error_flag = self._devices[key].GET_SYSTEM_GET_STATUS_ERROR_FLAG()[0]
            warning_flag = self._devices[key].GET_SYSTEM_GET_STATUS_WARNING_FLAG()[0]
            pending_flag = self._devices[key].GET_SYSTEM_GET_STATUS_OPERATION_PENDING_FLAG()[0]
        else:
            system_state = -1
            error_flag = -1
            warning_flag = -1
            pending_flag = -1

        system_status_l_1 = StatusHelper.Line("System Status:", [StatusHelper.ValueUnit(system_state, label = "State"),
                                     StatusHelper.ValueUnit(error_flag, label = " Error")])

        system_status_l_2 = StatusHelper.Line("", [StatusHelper.ValueUnit(warning_flag, label = "Warning"),
                                     StatusHelper.ValueUnit(pending_flag, label = " Pending")])
        elements.append(system_status_l_1)
        elements.append(system_status_l_2)

        #
        # GET_PHASELOCK_GET_STATUS
        #
        try:
            value = self._devices[key].GET_PHASELOCK_GET_STATUS()
        except:
            value = -1

        if(value != -1):
            lock_flag = self._devices[key].GET_PHASELOCK_GET_STATUS_LOCKED()[0]
            lock_error_flag = 0
            tuning_ready_flag = self._devices[key].GET_PHASELOCK_GET_STATUS_TUNING_READY()[0]
            rf_input_ready_flag = self._devices[key].GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()[0]
        else:
            lock_flag = -1
            lock_error_flag = -1
            tuning_ready_flag = -1
            rf_input_ready_flag = -1

        phaselock_status_l_1 = StatusHelper.Line("Phaselock Status:", [StatusHelper.ValueUnit(lock_flag, label = "Locked")])

        phaselock_status_l_2 = StatusHelper.Line("", [ StatusHelper.ValueUnit(tuning_ready_flag, label = "Tuning Ready"),
                                     StatusHelper.ValueUnit(rf_input_ready_flag, label = " RF Ready")])
        elements.append(phaselock_status_l_1)
        elements.append(phaselock_status_l_2)

        #
        # GET_PHASELOCK_GET_SELECTED_LASER
        #
        try:
            selected_laser = self._devices[key].GET_PHASELOCK_GET_SELECTED_LASER()[0]
        except:
            selected_laser = -1

        selected_laser_status_l = StatusHelper.Line("Selected Slave Laser:",
            [StatusHelper.ValueUnit(selected_laser, label = "Slave Laser ID")])

        elements.append(selected_laser_status_l)

        #
        # GET_PHASELOCK_GET_SELECTED_BAND
        #
        try:
            selected_band = self._devices[key].GET_PHASELOCK_GET_SELECTED_BAND()[0]
        except:
            selected_band = -1

        selected_band_status_l = StatusHelper.Line("Selected Band:",
            [StatusHelper.ValueUnit(selected_band, label = "Band ID")])
        elements.append(selected_band_status_l)

        #
        # GET_LASER_GET_STATUS_0
        #
        try:
            laser_status = self._devices[key].GET_LASER_GET_STATUS_0()[0]
            laser_status_0_0 = laser_status[0]
            laser_status_0_1 = laser_status[1]
        except:
            laser_status_0_0 = -1
            laser_status_0_1 = -1

        if(laser_status_0_0 != -1):
            laser_0_temp_limit_flag = laser_status_0_0 & 0x01
            laser_0_current_limit_flag = laser_status_0_0 >> 1 & 0x01
            laser_0_voltage_limit_flag = laser_status_0_0 >> 2 & 0x01
            laser_0_thermistor_open_flag = laser_status_0_0 >> 3 & 0x01
            laser_0_thermistor_overtemp_flag = laser_status_0_0 >> 4 & 0x01
            laser_0_tec_power_on_flag = laser_status_0_0 >> 5 & 0x01
            laser_0_laser_on_flag = laser_status_0_0 >> 6 & 0x01
        else:
            laser_0_temp_limit_flag = -1
            laser_0_current_limit_flag = -1
            laser_0_voltage_limit_flag = -1
            laser_0_thermistor_open_flag = -1
            laser_0_thermistor_overtemp_flag = -1
            laser_0_tec_power_on_flag = -1
            laser_0_laser_on_flag = -1

        if(laser_status_0_1 != -1):
            laser_0_calibration_update_flag = laser_status_0_1 & 0x04
        else:
            laser_0_calibration_update_flag = -1

        laser_0_status_l_1 = StatusHelper.Line("Laser_0 status:",
            [StatusHelper.ValueUnit(laser_0_temp_limit_flag, label = "Temp limit"),
                StatusHelper.ValueUnit(laser_0_current_limit_flag,
                    label = " Cur limit"),
                StatusHelper.ValueUnit(laser_0_voltage_limit_flag,
                    label = " Vol limit")])

        laser_0_status_l_2 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_0_thermistor_open_flag,
                label = "Therm open"),
            StatusHelper.ValueUnit(laser_0_thermistor_open_flag,
                label = " Therm_overtemp")])

        laser_0_status_l_3 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_0_tec_power_on_flag,
                label = "TEC ON"),
            StatusHelper.ValueUnit(laser_0_laser_on_flag,
                label = " Laser ON")])

        laser_0_status_l_4 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_0_calibration_update_flag,
                label = "Calibration update")])

        elements.append(laser_0_status_l_1)
        elements.append(laser_0_status_l_2)
        elements.append(laser_0_status_l_3)
        elements.append(laser_0_status_l_4)

        #
        # GET_LASER_GET_STATUS_1
        #
        try:
            laser_status = self._devices[key].GET_LASER_GET_STATUS_1()[0]
            laser_status_1_0 = laser_status[0]
            laser_status_1_1 = laser_status[1]
        except:
            laser_status_1_0 = -1
            laser_status_1_1 = -1

        if(laser_status_1_0 != -1):
            laser_1_temp_limit_flag = laser_status_1_0 & 0x01
            laser_1_current_limit_flag = laser_status_1_0 >> 1 & 0x01
            laser_1_voltage_limit_flag = laser_status_1_0 >> 2 & 0x01
            laser_1_thermistor_open_flag = laser_status_1_0 >> 3 & 0x01
            laser_1_thermistor_overtemp_flag = laser_status_1_0 >> 4 & 0x01
            laser_1_tec_power_on_flag = laser_status_1_0 >> 5 & 0x01
            laser_1_laser_on_flag = laser_status_1_0 >> 6 & 0x01
        else:
            laser_1_temp_limit_flag = -1
            laser_1_current_limit_flag = -1
            laser_1_voltage_limit_flag = -1
            laser_1_thermistor_open_flag = -1
            laser_1_thermistor_overtemp_flag = -1
            laser_1_tec_power_on_flag = -1
            laser_1_laser_on_flag = -1

        if(laser_status_1_1 != -1):
            laser_1_calibration_update_flag = laser_status_1_1 & 0x04
        else:
            laser_1_calibration_update_flag = -1

        laser_1_status_l_1 = StatusHelper.Line("Laser_1 status:",
            [StatusHelper.ValueUnit(laser_1_temp_limit_flag, label = "Temp limit"),
                StatusHelper.ValueUnit(laser_1_current_limit_flag,
                    label = " Cur limit"),
                StatusHelper.ValueUnit(laser_1_voltage_limit_flag,
                    label = " Vol limit")])

        laser_1_status_l_2 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_1_thermistor_open_flag,
                label = "Therm open"),
            StatusHelper.ValueUnit(laser_1_thermistor_open_flag,
                label = " Therm_overtemp")])

        laser_1_status_l_3 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_1_tec_power_on_flag,
                label = "TEC ON"),
            StatusHelper.ValueUnit(laser_1_laser_on_flag,
                label = " Laser ON")])

        laser_1_status_l_4 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_1_calibration_update_flag,
                label = "Calibration update")])

        elements.append(laser_1_status_l_1)
        elements.append(laser_1_status_l_2)
        elements.append(laser_1_status_l_3)
        elements.append(laser_1_status_l_4)

        #
        # GET_LASER_GET_STATUS_2
        #
        try:
            laser_status = self._devices[key].GET_LASER_GET_STATUS_1()[0]
            laser_status_2_0 = laser_status[0]
            laser_status_2_1 = laser_status[1]
        except:
            laser_status_2_0 = -1
            laser_status_2_1 = -1

        if(laser_status_2_0 != -1):
            laser_2_temp_limit_flag = laser_status_2_0 & 0x01
            laser_2_current_limit_flag = laser_status_2_0 >> 1 & 0x01
            laser_2_voltage_limit_flag = laser_status_2_0 >> 2 & 0x01
            laser_2_thermistor_open_flag = laser_status_2_0 >> 3 & 0x01
            laser_2_thermistor_overtemp_flag = laser_status_2_0 >> 4 & 0x01
            laser_2_tec_power_on_flag = laser_status_2_0 >> 5 & 0x01
            laser_2_laser_on_flag = laser_status_2_0 >> 6 & 0x01
        else:
            laser_2_temp_limit_flag = -1
            laser_2_current_limit_flag = -1
            laser_2_voltage_limit_flag = -1
            laser_2_thermistor_open_flag = -1
            laser_2_thermistor_overtemp_flag = -1
            laser_2_tec_power_on_flag = -1
            laser_2_laser_on_flag = -1

        if(laser_status_2_1 != -1):
            laser_2_calibration_update_flag = laser_status_2_1 & 0x04
        else:
            laser_2_calibration_update_flag = -1

        laser_2_status_l_1 = StatusHelper.Line("Laser_2 status:",
            [StatusHelper.ValueUnit(laser_2_temp_limit_flag, label = "Temp limit"),
                StatusHelper.ValueUnit(laser_2_current_limit_flag,
                    label = " Cur limit"),
                StatusHelper.ValueUnit(laser_2_voltage_limit_flag,
                    label = " Vol limit")])

        laser_2_status_l_2 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_2_thermistor_open_flag,
                label = "Therm open"),
            StatusHelper.ValueUnit(laser_2_thermistor_open_flag,
                label = " Therm_overtemp")])

        laser_2_status_l_3 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_2_tec_power_on_flag,
                label = "TEC ON"),
            StatusHelper.ValueUnit(laser_2_laser_on_flag,
                label = " Laser ON")])

        laser_2_status_l_4 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_2_calibration_update_flag,
                label = "Calibration update")])

        elements.append(laser_2_status_l_1)
        elements.append(laser_2_status_l_2)
        elements.append(laser_2_status_l_3)
        elements.append(laser_2_status_l_4)

        # GET_LASER_GET_STATUS_3
        #
        try:
            laser_status = self._devices[key].GET_LASER_GET_STATUS_1()[0]
            laser_status_3_0 = laser_status[0]
            laser_status_3_1 = laser_status[1]
        except:
            laser_status_3_0 = -1
            laser_status_3_1 = -1

        if(laser_status_3_0 != -1):
            laser_3_temp_limit_flag = laser_status_3_0 & 0x01
            laser_3_current_limit_flag = laser_status_3_0 >> 1 & 0x01
            laser_3_voltage_limit_flag = laser_status_3_0 >> 2 & 0x01
            laser_3_thermistor_open_flag = laser_status_3_0 >> 3 & 0x01
            laser_3_thermistor_overtemp_flag = laser_status_3_0 >> 4 & 0x01
            laser_3_tec_power_on_flag = laser_status_3_0 >> 5 & 0x01
            laser_3_laser_on_flag = laser_status_3_0 >> 6 & 0x01
        else:
            laser_3_temp_limit_flag = -1
            laser_3_current_limit_flag = -1
            laser_3_voltage_limit_flag = -1
            laser_3_thermistor_open_flag = -1
            laser_3_thermistor_overtemp_flag = -1
            laser_3_tec_power_on_flag = -1
            laser_3_laser_on_flag = -1

        if(laser_status_3_1 != -1):
            laser_3_calibration_update_flag = laser_status_3_1 & 0x04
        else:
            laser_3_calibration_update_flag = -1

        laser_3_status_l_1 = StatusHelper.Line("Laser_3 status:",
            [StatusHelper.ValueUnit(laser_3_temp_limit_flag, label = "Temp limit"),
                StatusHelper.ValueUnit(laser_3_current_limit_flag,
                    label = " Cur limit"),
                StatusHelper.ValueUnit(laser_3_voltage_limit_flag,
                    label = " Vol limit")])

        laser_3_status_l_2 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_3_thermistor_open_flag,
                label = "Therm open"),
            StatusHelper.ValueUnit(laser_3_thermistor_open_flag,
                label = " Therm_overtemp")])

        laser_3_status_l_3 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_3_tec_power_on_flag,
                label = "TEC ON"),
            StatusHelper.ValueUnit(laser_3_laser_on_flag,
                label = " Laser ON")])

        laser_3_status_l_4 = StatusHelper.Line("",
            [StatusHelper.ValueUnit(laser_3_calibration_update_flag,
                label = "Calibration update")])

        elements.append(laser_3_status_l_1)
        elements.append(laser_3_status_l_2)
        elements.append(laser_3_status_l_3)
        elements.append(laser_3_status_l_4)

        #
        # Status frame rendering
        #

        statusFrame = StatusHelper.Frame("Laser Synthesizer %d (LS)" % self._lsNo,
            elements)
        statusFrame.printScreen()

