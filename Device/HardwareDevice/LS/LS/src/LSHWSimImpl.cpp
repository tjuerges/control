//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Mar 7, 2011  created
//


#include <LSHWSimImpl.h>
#include <vector>
// For AMB::node_t and CAN::byte_t
#include <CANTypes.h>


AMB::LSHWSimImpl::LSHWSimImpl(AMB::node_t node,
    const std::vector< CAN::byte_t >& serialNumber):
    LSHWSimBase::LSHWSimBase(node, serialNumber)
{
    /// Set the serial number to 0xfc00080162131f10 which is an LS serial
    /// number.
    std::vector < CAN::byte_t > sn;
    sn.push_back(0xfc);
    sn.push_back(0x00);
    sn.push_back(0x08);
    sn.push_back(0x01);
    sn.push_back(0x62);
    sn.push_back(0x13);
    sn.push_back(0x1f);
    sn.push_back(0x10);
    initialize(node, sn);
}

AMB::LSHWSimImpl::~LSHWSimImpl()
{
}
