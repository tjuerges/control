//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <LSImpl.h>
#include <string> // for std::string
#include <cmath> // for std::fabs, ::round
#include <loggingMACROS.h> // for AUTO_TRACE
// For audience logs.
#include <sstream>
#include <iomanip>
#include <LogToAudience.h>

#include <acstime.h> // for ::getTimeStamp

#include <ControlCommonExceptions.h>
#include <ControlExceptions.h>

// Alarm system stuff
#include <controlAlarmSender.h>
#include <LSCommonImpl.h>


//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
LSImpl::LSImpl(const ACE_CString& name, maci::ContainerServices* cs):
    LSBase(name, cs),
    LSBFreq(0.0),
    LSBFreqStatus(false),
    FreqTolerance(0.0)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

LSImpl::~LSImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

//-----------------------------------------------------------------------------
// Component Lifecycle Methods
//-----------------------------------------------------------------------------
void LSImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        LSBase::initialize();
        //MonitorHelper::initialize(name(),getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}


void LSImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        //MonitorHelper::cleanUp();
        LSBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void LSImpl::hwDiagnostic()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        LSBase::diagnosticModeEnabled = true;
        LSBase::hwDiagnostic();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

}

//-----------------------------------------------------------------------------
// Hardware Lifecycle Methods
//-----------------------------------------------------------------------------
void LSImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LSCommonImpl::alarmSender->forceTerminateAllAlarms();

    ACS::Time timeStamp(0ULL);
    try
    {
        if(LSBase::getSystemStartupMode(timeStamp) != 1U)
        {
            LSCommonImpl::alarmSender->activateAlarm(
                LsWasNotInAutomaticStartupMode);

            try
            {
                setCntlSystemStartupMode(1U);
            }
            catch(const ControlExceptions::INACTErrorExImpl& ex)
            {
                ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
                nex.addData("Detail", "The LS is not at least in startup mode "
                    "yet.  Could not set the start up mode to automatic.");
                nex.log();
            }
            catch(const ControlExceptions::CAMBErrorExImpl& ex)
            {
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
                nex.addData("Detail", "Could not set the start up mode to "
                    "automatic.");
                nex.log();
            }
        }
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "The LS is not at least in startup"
            "mode yet.  Could not read the start up mode.");
            nex.log();
        }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not read the start up mode.");
        nex.log();
    }

    LSBase::hwInitializeAction();
    //MonitorHelper::resume();
}

void LSImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LSBase::hwOperationalAction();
}

void LSImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LSBase::diagnosticModeEnabled = false;
    //MonitorHelper::suspend();
    LSBase::hwStopAction();
}

void LSImpl::hwDiagnosticAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Diagnostic mode (software) = manual mode in LS hardware.
    // Switch to manual mode of the LS hardware.
    try
    {
        setCntlSystemManualModeRequest();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "The LS is hardware could not be switched to "
            "manual mode.");
        nex.log();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "The LS is hardware could not be switched to "
            "manual mode.");
        nex.log();
    }

    LSBase::hwDiagnosticAction();
}

//-----------------------------------------------------------------------------
// Monitoring Distpach Method
//-----------------------------------------------------------------------------
void LSImpl::processRequestResponse(const AMBRequestStruct& response)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

//-----------------------------------------------------------------------------
// Overwriting unit conversion methods
//-----------------------------------------------------------------------------
unsigned short LSImpl::worldToRawCntlLaserTempSetpoint0(const unsigned short world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LSImpl::worldToRawCntlLaserTempSetpoint1(const unsigned short world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LSImpl::worldToRawCntlLaserTempSetpoint2(const unsigned short world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LSImpl::worldToRawCntlLaserTempSetpoint3(const unsigned short world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LSImpl::worldToRawCntlLaserTempSetpoint(const double world) const
{
    const double tmp(std::exp(LASER_TEMP_W
        + LASER_TEMP_X / world
        + LASER_TEMP_Y / std::pow(world, 2)
        + LASER_TEMP_Z / std::pow(world, 3)));
    const double r_target(std::exp(tmp) * LASER_TEMP_R);
    const double setpoint((r_target * 65535.0) / (r_target + LASER_TEMP_R));

    return static_cast< unsigned short >(::round(setpoint));
}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LSImpl)
