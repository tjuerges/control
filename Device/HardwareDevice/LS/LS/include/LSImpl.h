#ifndef LSIMPL_H
#define LSIMPL_H
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
// @(#) $Id$
//


// Base class(es)
#include <LSBase.h>
#include <monitorHelper.h>


//CORBA servant header
#include <LSS.h>
#include <LSCommonExceptions.h>


class LSImpl:  public ::LSBase, public virtual POA_Control::LS
{
    public:
    ///
    /// The constructor for any ACS C++ component must have this signature
    ///
    LSImpl(const ACE_CString& name, maci::ContainerServices* cs);

    ///
    /// The destructor must be virtual because this class contains virtual
    /// functions.
    ///
    virtual ~LSImpl();

    /* ---------------- Component Lifecycle -----------------------*/
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwDiagnostic();


    protected:
    /* ---------------- Hardware Lifecycle Interface ---------------*/
    virtual void hwInitializeAction();

    virtual void hwOperationalAction();

    virtual void hwStopAction();

    virtual void hwDiagnosticAction();

    /* ------------------ Monitor Helper Methods ------------------- */
    virtual void processRequestResponse(const AMBRequestStruct& response);

    /* ------------------ Common methods defined at LSCommin ------------------------*/
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception  ControlExceptions::INACTErrorEx
    /// \exception LSCommonExceptions::TimeOutEx
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    /// \exception LSCommonExceptions::TimeOutEx
    /// \exception ControlCommonExceptions::LockErrorEx

    /* ------------------ Overloaded unit conversion methods ------------------------*/
    virtual unsigned short
        worldToRawCntlLaserTempSetpoint0(const unsigned short world) const;

    virtual unsigned short
        worldToRawCntlLaserTempSetpoint1(const unsigned short world) const;

    virtual unsigned short
        worldToRawCntlLaserTempSetpoint2(const unsigned short world) const;

    virtual unsigned short
        worldToRawCntlLaserTempSetpoint3(const unsigned short world) const;

    unsigned short
        worldToRawCntlLaserTempSetpoint(const double world) const;


    private:
    // The copy constructor is made private to prevent a compiler generated one
    // from being used. It is not implemented.
    LSImpl(const LSImpl& other);

    void setLSFrequency(CORBA::Double frequency);


    /* -----------------  global variables ---------------------- */
    // variable to hold the last set frequency requested in PHASELOCK_TUNING_INIT command
    double LSBFreq;

    // variable to hold to correctness of the LSBFreq,  everytime the PHASELOCK_TUNING_INIT
    // command is invoked (through setFrquency()), it will be set to False, and when the command Set_Frequency
    // finished correctly it will be set to true
    bool LSBFreqStatus;

    const double FreqTolerance;
};
#endif
