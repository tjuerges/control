//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <LSPPImpl.h>

#include <string> // for string
#include <loggingMACROS.h> // for AUTO_TRACE
#include <acstime.h>
#include <ControlCommonExceptions.h>
#include <ControlExceptions.h>


//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
LSPPImpl::LSPPImpl(const ACE_CString& name, maci::ContainerServices* cs):
    ::LSPPBase(name, cs), LSBFreq(0.0),
    LSBFreqStatus(false),
    FreqTolerance(0.0)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

LSPPImpl::~LSPPImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

//-----------------------------------------------------------------------------
// Component Lifecycle Methods
//-----------------------------------------------------------------------------
void LSPPImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        LSPPBase::initialize();
        //MonitorHelper::initialize(name(),getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void LSPPImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        //MonitorHelper::cleanUp();
        LSPPBase::cleanUp();
    }
    catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void LSPPImpl::hwDiagnostic()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    //LSPPBase::diagnosticModeEnabled = true;
    //LSPPBase::hwDiagnostic();

    try
    {
        LSPPBase::diagnosticModeEnabled = true;
        LSPPBase::hwDiagnostic();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

//-----------------------------------------------------------------------------
// Hardware Lifecycle Methods
//-----------------------------------------------------------------------------
void LSPPImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LSPPBase::hwInitializeAction();
    //MonitorHelper::resume();
}

void LSPPImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LSPPBase::hwOperationalAction();
}

void LSPPImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LSPPBase::diagnosticModeEnabled = false;
    //MonitorHelper::suspend();
    LSPPBase::hwStopAction();
}

void LSPPImpl::hwDiagnosticAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    //enter en manual mode, diagnostic mode (software) = manual mode in LSPP hardware
    setCntlSystemManualModeRequest();

    LSPPBase::hwDiagnosticAction();
}

//-----------------------------------------------------------------------------
// Monitoring Distpach Method
//-----------------------------------------------------------------------------
void LSPPImpl::processRequestResponse(const AMBRequestStruct& response)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

//-----------------------------------------------------------------------------
// Overwriting unit conversion methods
//-----------------------------------------------------------------------------
double LSPPImpl::rawToWorldLaserTempSetpoint0(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LSPPImpl::rawToWorldLaserTempSetpoint1(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LSPPImpl::rawToWorldLaserTempSetpoint2(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LSPPImpl::rawToWorldLaserTempSetpoint3(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

unsigned short LSPPImpl::worldToRawCntlLaserTempSetpoint0(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LSPPImpl::worldToRawCntlLaserTempSetpoint1(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LSPPImpl::worldToRawCntlLaserTempSetpoint2(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LSPPImpl::worldToRawCntlLaserTempSetpoint3(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

double LSPPImpl::rawToWorldSignalGetLaserTempMon0(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSPPImpl::rawToWorldSignalGetLaserTempMon1(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSPPImpl::rawToWorldSignalGetLaserTempMon2(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSPPImpl::rawToWorldSignalGetLaserTempMon3(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSPPImpl::rawToWorldSignalGetExternThernMon(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSPPImpl::rawToWorldSignalGetInternThernMon(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

unsigned short LSPPImpl::worldToRawCntlLaserTempSetpoint(const float world) const
{
    const double tmp(LASER_TEMP_W
        + LASER_TEMP_X / world
        + LASER_TEMP_Y / std::pow(world, 2.0F)
        + LASER_TEMP_Z / std::pow(world, 3.0F));
    const double r_target(std::exp(tmp) * LASER_TEMP_R);
    const double setpoint((r_target * 65535) / (r_target + LASER_TEMP_R));

    return static_cast< unsigned short >(::round(setpoint));
}

double LSPPImpl::rawToWorldLaserTempSetpoint(const unsigned short raw) const
{
    //returned value in Kelvin
    const double r_target(raw * LASER_TEMP_R
        / static_cast< double >(65535.0 - raw));
    const double r_relative(r_target / LASER_TEMP_R);
    const double temp_inv(LASER_TEMP_A
        + LASER_TEMP_B * std::log(r_relative)
        + LASER_TEMP_C * std::pow(std::log(r_relative), 2.0)
        + LASER_TEMP_D * std::pow(std::log(r_relative), 3.0));

    return (1.0 / temp_inv);
}

double LSPPImpl::rawToWorldLaserTemp(const unsigned short raw) const
{
    //returned value in Kelvin
    const double r_target(raw * LASER_TEMP_R
        / static_cast< double >(32768.0 - raw));
    const double r_relative(r_target / LASER_TEMP_R);
    const double temp_inv(LASER_TEMP_A
        + LASER_TEMP_B * std::log(r_relative)
        + LASER_TEMP_C * std::pow(std::log(r_relative), 2.0)
        + LASER_TEMP_D * std::pow(std::log(r_relative), 3.0));

    return (1.0 / temp_inv);
}


//-----------------------------------------------------------------------------
// Hack for Teraxion's LSPP device, as the ambient temperature has a bug in the
// bytes ordering
//-----------------------------------------------------------------------------

float LSPPImpl::rawToWorldAmbientTemperature(
    const std::vector< unsigned char >& raw)
{

    // This calculate the AMBSI temperature.
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Return the temperature in Kelvin
    return (ds1820Temp(raw[1], raw[0], raw[2], raw[3]) + 273.15);
}

float LSPPImpl::ds1820Temp(const u_int8_t temp, const int8_t sign,
    const u_int8_t countRem, const u_int8_t countPerC) const
{
    // The DS1820 data sheet states that count per c is always 16.
    // If it is not then something is very broken
    if(countPerC != 16)
    {
        return -9999.99;
    }

    short tempVal(temp); //Copy the temp to a 16-bit
    tempVal |= (sign << 8U); //Put the sign byte in the upper bytes
    //Do this before the divide so that the
    //sign is handled properly.
    tempVal = tempVal >> 1; //As per the ds1850data sheet, we need
    //to divide by 2 AND drop the last bit.
    float floatTemp(tempVal); //Switch to floating point for decimal calculations
    return (floatTemp - 0.25 + ( //Use the equation in the data sheet
        (static_cast< float > (countPerC) - static_cast< float > (countRem))
            / static_cast< float > (countPerC)));

}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LSPPImpl)
