//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Mar 7, 2011  created
//


#include <LSPPHWSimImpl.h>
#include <LSPPHWSimBase.h>
#include <vector>
#include <CANTypes.h>


AMB::LSPPHWSimImpl::LSPPHWSimImpl(node_t node,
    const std::vector< CAN::byte_t >& serialNumber):
    LSPPHWSimBase::LSPPHWSimBase(node, serialNumber)
{
    /// Initialize monitor and control points
    /// Set the serial number to 0x3b0008016207b410 which is an LSPP serial
    /// number.
    std::vector < CAN::byte_t > sn;
    sn.push_back(0x3b);
    sn.push_back(0x00);
    sn.push_back(0x08);
    sn.push_back(0x01);
    sn.push_back(0x62);
    sn.push_back(0x07);
    sn.push_back(0xb4);
    sn.push_back(0x10);
    initialize(node, sn);
}

AMB::LSPPHWSimImpl::~LSPPHWSimImpl()
{
}

void AMB::LSPPHWSimImpl::initialize(AMB::node_t node,
    const std::vector< CAN::byte_t >& serialNumber)
{
    // Initialize the extended class
    AMB::LSPPHWSimBase::initialize(node, serialNumber);

    // Specific monitor points
    std::vector< CAN::byte_t >* vvalue(new std::vector< CAN::byte_t >);
    std::vector< CAN::byte_t > rawVector(2, 0);

    AMB::TypeConversion::valueToData(*vvalue, rawVector);

    std::map< rca_t , std::vector<CAN::byte_t>* >::iterator iter(
        state_m.find(monitorPoint_SYSTEM_GET_STATUS));
    if(iter != state_m.end())
    {
        state_m.erase(iter);
    }

    state_m.insert(std::make_pair(monitorPoint_SYSTEM_GET_STATUS, vvalue));
}
