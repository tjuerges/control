#ifndef LSPPIMPL_H
#define LSPPIMPL_H
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Base class(es)
#include <LSPPBase.h>
#include <monitorHelper.h>
#include <math.h>

//CORBA servant header
#include <LSPPS.h>
#include <LSCommonExceptions.h>

//namespace Control {
  class LSPPImpl:
    public virtual POA_Control::LSPP,
    public ::LSPPBase
  {
  public:

    // ------------------- Constructor & Destructor -------------------

    ///
    /// The constructor for any ACS C++ component must have this signature
    ///
    LSPPImpl(const ACE_CString& name, maci::ContainerServices* cs);

    ///
    /// The destructor must be virtual because this class contains virtual
    /// functions.
    ///
    virtual ~LSPPImpl();
  /* ---------------- Component Lifecycle -----------------------*/
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwDiagnostic();

 protected:
  /* ---------------- Hardware Lifecycle Interface ---------------*/
  virtual void hwInitializeAction();
  virtual void hwOperationalAction();
  virtual void hwStopAction();
  virtual void hwDiagnosticAction();
  /* ------------------ Monitor Helper Methods ------------------- */
  virtual void processRequestResponse(const AMBRequestStruct& response);

  /* ------------------ Common methods defined at LSCommin ------------------------*/
  /// \exception ControlExceptions::CAMBErrorEx
  /// \exception ControlExceptions::INACTErrorEx
  /// \exception LSCommonExceptions::TimeOutEx
  /// \exception ControlExceptions::CAMBErrorEx
  /// \exception ControlExceptions::INACTErrorEx
  /// \exception LSCommonExceptions::TimeOutEx
  /// \exception ControlExceptions::CAMBErrorEx
  /// \exception ControlExceptions::INACTErrorEx
  /// \exception LSCommonExceptions::TimeOutEx
  //virtual void setLSPPFrequency(CORBA::Double);


  /* ------------------ Overloaded unit conversion methods ------------------------*/
  virtual double rawToWorldLaserTempSetpoint0(const unsigned short raw) const;
  virtual double rawToWorldLaserTempSetpoint1(const unsigned short raw) const;
  virtual double rawToWorldLaserTempSetpoint2(const unsigned short raw) const;
  virtual double rawToWorldLaserTempSetpoint3(const unsigned short raw) const;
  virtual unsigned short worldToRawCntlLaserTempSetpoint0(const float world) const;
  virtual unsigned short worldToRawCntlLaserTempSetpoint1(const float world) const;
  virtual unsigned short worldToRawCntlLaserTempSetpoint2(const float world) const;
  virtual unsigned short worldToRawCntlLaserTempSetpoint3(const float world) const;
  virtual double rawToWorldSignalGetLaserTempMon0(const unsigned short raw) const;
  virtual double rawToWorldSignalGetLaserTempMon1(const unsigned short raw) const;
  virtual double rawToWorldSignalGetLaserTempMon2(const unsigned short raw) const;
  virtual double rawToWorldSignalGetLaserTempMon3(const unsigned short raw) const;
  virtual double rawToWorldSignalGetExternThernMon(const unsigned short raw) const;
  virtual double rawToWorldSignalGetInternThernMon(const unsigned short raw) const;

  virtual double rawToWorldLaserTempSetpoint(const unsigned short raw) const;
  virtual double rawToWorldLaserTemp(const unsigned short raw) const;
  virtual unsigned short worldToRawCntlLaserTempSetpoint(const float world) const;

  //-----------------------------------------------------------------------------
  // Hack for Teraxion's LSPP device, as the ambient temperature has a bug in the
  // bytes ordering
  //-----------------------------------------------------------------------------
  virtual float rawToWorldAmbientTemperature(
      const std::vector<unsigned char>& raw);

  virtual float ds1820Temp(const u_int8_t temp,
      const int8_t sign, const u_int8_t countRem,
      const u_int8_t countPerC) const;


  private:

    // The copy constructor is made private to prevent a compiler generated one
    // from being used. It is not implemented.
    LSPPImpl(const LSPPImpl& other);

    const static float LASER_TEMP_W = -1.414196e+1;
    const static float LASER_TEMP_X = 4.430783e+3;
    const static float LASER_TEMP_Y = -3.407898e+4;
    const static float LASER_TEMP_Z = -8.894193e+6;
    const static float LASER_TEMP_A = 3.3540154e-3;
    const static float LASER_TEMP_B = 2.5627725e-4;
    const static float LASER_TEMP_C = 2.0829221e-6;
    const static float LASER_TEMP_D = 7.3003206e-8;
    const static float LASER_TEMP_R = 10000;
    const static float LASER_TEMP_KELVIN_TO_CELSIUS = -273.15;

    /* -----------------  global variables ---------------------- */
    // variable to hold the last set frequency requested in PHASELOCK_TUNING_INIT command
    double LSBFreq;
    // variable to hold to correctness of the LSBFreq,  everytime the PHASELOCK_TUNING_INIT
    // command is invoked (through setFrquency()), it will be set to False, and when the command Set_Frequency
    // finished correctly it will be set to true
    bool LSBFreqStatus;
    const double FreqTolerance;
  }; // end  LSPPImpl class
//} // end Control namespace

#endif // LSPPIMPL_H
