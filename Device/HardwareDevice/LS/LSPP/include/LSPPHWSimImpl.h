#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef LSPPHWSimImpl_H
#define LSPPHWSimImpl_H
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Mar 7, 2011  created
//


#include <LSPPHWSimBase.h>
#include <vector>
#include <CANTypes.h>


namespace AMB
{
    class LSPPHWSimImpl: public LSPPHWSimBase
    {
        public:
        LSPPHWSimImpl(node_t node,
            const std::vector< CAN::byte_t >& serialNumber);

        virtual ~LSPPHWSimImpl();


        virtual void initialize(AMB::node_t node,
            const std::vector< CAN::byte_t >& serialNumber);


        private:
        /// No default ctor.
        LSPPHWSimImpl();

        /// No copy ctor.
        LSPPHWSimImpl(const LSPPHWSimImpl&);

        /// No assignment operator.
        LSPPHWSimImpl& operator=(const LSPPHWSimImpl&);
    };
};
#endif
