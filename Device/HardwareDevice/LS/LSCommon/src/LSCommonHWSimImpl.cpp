//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Mar 7, 2011  created
//


#include <LSCommonHWSimImpl.h>
#include <LSCommonHWSimBase.h>
#include <vector>
#include <sstream>
#include <CANTypes.h>
#include <loggingMACROS.h>


AMB::LSCommonHWSimImpl::LSCommonHWSimImpl(node_t node,
    const std::vector< CAN::byte_t >& serialNumber):
    AMB::LSCommonHWSimBase::LSCommonHWSimBase(node, serialNumber)
{
    /// Initialize monitor and control points.
    initialize(node, serialNumber);
}

AMB::LSCommonHWSimImpl::~LSCommonHWSimImpl()
{
}

void AMB::LSCommonHWSimImpl::setControlSetSystemManualModeRequest(
    const std::vector< CAN::byte_t >& data)
{
    // Read back SYSTEM_STATUS and set the manual mode's bits.
    std::vector< CAN::byte_t > currentStatus(getMonitorSystemGetStatusProxy());

    // There are LS which return two bytes for the status and others which
    // return four bytes.  Support both.
    std::vector< CAN::byte_t >::size_type index(currentStatus.size() - 1);

    // Reset the system state bits.
    currentStatus[index] &= 0xc7U; // 11000111
    // Set the manual mode bits.
    currentStatus[index] |= 0x28U; // 00101000
    setMonitorSystemGetStatusProxy(currentStatus);
}

void AMB::LSCommonHWSimImpl::setControlSetSystemStandbyModeRequest(
    const std::vector< CAN::byte_t >& data)
{
    std::vector< CAN::byte_t > currentStatus(getMonitorSystemGetStatusProxy());

    // There are LS which return two bytes for the status and others which
    // return four bytes.  Support both.
    std::vector< CAN::byte_t >::size_type index(currentStatus.size() - 1);

    // Reset the system state bits [5:3].
    currentStatus[index] &= 0xc7U; // 11000111
    // Set the standby mode bits.
    currentStatus[index] |= 0x10U; // 00010000

    setMonitorSystemGetStatusProxy(currentStatus);

    // Reset all bits from   PHASELOCK_GET_STATUS monitoring points (process
    // idle).
    std::vector< CAN::byte_t > phaselockStatus(2, 0);
    setMonitorPhaselockGetStatusProxy(phaselockStatus);
}

void AMB::LSCommonHWSimImpl::setControlSetPhaselockCommandTuningInit(
    const std::vector< CAN::byte_t >& data)
{
    std::vector< CAN::byte_t > currentStatus(
        getMonitorSystemGetStatusProxy());

    // There are LS which return two bytes for the status and others which
    // return four bytes.  Support both.
    std::vector< CAN::byte_t >::size_type index(currentStatus.size() - 1);

    // Get the system state bits [5:3].
    CAN::byte_t state(currentStatus[index] & 0x38U); // 00111000

    // Verify if it is in standby or operational mode
    if((state != 0x10U) && (state != 0x20U))
    {
        std::ostringstream msg;
        msg.setf(std::ios::hex, std::ios::basefield);
        msg << "Error: system not in STANDBY or OPERATIONAL MODE (value = 0x"
            << static_cast< unsigned int >(currentStatus[index])
            << ", cmd aborted.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        return;
    }

    // Set system status to PHASELOCKING.
    currentStatus[index] &= 0xc3U;
    currentStatus[index] |= 0x18U;
    setMonitorSystemGetStatusProxy(currentStatus);

    // Reset all bits from   PHASELOCK_GET_STATUS monitoring points
    // (beginning of lock process)
    // and set "Tuning Ready" bit <1><6>
    std::vector< CAN::byte_t > phaselockStatus;
    phaselockStatus.push_back(0x0U);
    phaselockStatus.push_back(0x40U);
    setMonitorPhaselockGetStatusProxy(phaselockStatus);

    // Set "RF input ready" bit at PHASELOCK_GET_STATUS <1><7>
    phaselockStatus = getMonitorPhaselockGetStatusProxy();
    phaselockStatus[1] |= 0x80U;
    setMonitorPhaselockGetStatusProxy(phaselockStatus);
}

void AMB::LSCommonHWSimImpl::setControlSetPhaselockCommandTuningFinalize(
    const std::vector< CAN::byte_t >& data)
{
    std::vector< CAN::byte_t > currentStatus(
        getMonitorSystemGetStatusProxy());

    // There are LS which return two bytes for the status and others which
    // return four bytes.  Support both.
    std::vector< CAN::byte_t >::size_type index(currentStatus.size() - 1);

    // Get the system state bits [5:3].
    currentStatus[index] &= 0x38U; // 00111000

    // Verify if it is in Phase-Locking mode.
    if(currentStatus[index] != 0x18U)
    {
        std::ostringstream msg;
        msg.setf(std::ios::hex, std::ios::basefield);
        msg << "Error system not in PHASE-LOCKING mode (value = 0x"
            << static_cast< unsigned int >(currentStatus[index])
            << "), cmd aborted.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        return;
    }

    // Verify if "Tuning Input Ready" and "RF Input Ready" flags are activated.
    std::vector< CAN::byte_t > phaselockStatus(
        getMonitorPhaselockGetStatusProxy());

    if(phaselockStatus[1] != 0xc0U)
    {
        std::ostringstream msg;
        msg.setf(std::ios::hex, std::ios::basefield);
        msg << "Expected 'Tuning Ready' and 'RF input ready' flags are not "
            "activated yet (value = 0x"
            << static_cast< unsigned int >(phaselockStatus[1])
            << "), cmd aborted.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        return;
    }

    // Set "Locked" bit <0><0> of PHASELOCK_GET_STATUS
    phaselockStatus[1] |= 0x01U;
    setMonitorPhaselockGetStatusProxy(phaselockStatus);

    // Set system status to Operational.
    currentStatus = getMonitorSystemGetStatusProxy();
    currentStatus[index] &= 0xc7U; // 1100 0111
    currentStatus[index] |= 0x20U; // 0010 0000

    setMonitorSystemGetStatusProxy(currentStatus);
}

std::vector< CAN::byte_t > AMB::LSCommonHWSimImpl::getMonitorSystemGetError() const
{
    std::vector< CAN::byte_t > raw;
    const short value(-99);

    TypeConversion::valueToData(raw, value, 2);
    setMonitorSystemGetErrorProxy(raw);

    return raw;
}

std::vector< CAN::byte_t > AMB::LSCommonHWSimImpl::getMonitorLaserFrequency0() const
{
    std::vector< CAN::byte_t > raw;
    // Ref laser frequency  192.643913E12Hz;
    const unsigned int value(192643913U);

    TypeConversion::valueToData(raw, value, 4);
    setMonitorGetLaserFrequency0Proxy(raw);

    return raw;
}

std::vector< CAN::byte_t > AMB::LSCommonHWSimImpl::getMonitorSignalGetLaserBiasMon1() const
{
    std::vector< CAN::byte_t > raw;
    const unsigned short value(32768U);

    TypeConversion::valueToData(raw, value, 2);
    setMonitorSignalGetLaserBiasMon1Proxy(raw);

    return raw;
}


std::vector<CAN::byte_t> AMB::LSCommonHWSimImpl::getMonitorPhaselockRefLaserFrequency() const
{
    std::vector< CAN::byte_t > raw;
    // Ref. laser frequency  192.643913E12;
    const unsigned int value(192643913U);

    TypeConversion::valueToData(raw, value, 4);
    setMonitorPhaselockRefLaserFrequencyProxy(raw);

    return raw;
}
