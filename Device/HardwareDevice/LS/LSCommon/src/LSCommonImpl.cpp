// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 20010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
// @(#) $Id$
//


#include <LSCommonImpl.h>
#include <loggingMACROS.h>
// Alarm system stuff
#include <vector>
#include <controlAlarmSender.h>


//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
LSCommonImpl::LSCommonImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    ::LSCommonBase(name, cs),
     MonitorHelper(),
     alarmSender(0)
{
}

LSCommonImpl::~LSCommonImpl()
{
}

void LSCommonImpl::initialiseAlarmSystem()
{
    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSende.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    {
        Control::AlarmInformation ai;
        ai.alarmCode = LsDidNotLock;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = LsWasNotInAutomaticStartupMode;
        alarmInformation.push_back(ai);
    }

    alarmSender->initializeAlarms(
        Control::CharacteristicControlDeviceImpl::getAlarmFamilyName("LS"),
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName(),
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void LSCommonImpl::cleanUpAlarmSystem()
{
    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


//-----------------------------------------------------------------------------
// Component Lifecycle Methods
//-----------------------------------------------------------------------------
void LSCommonImpl::initialize()
{
    try
    {
        LSCommonBase::initialize();
        //MonitorHelper::initialize(name(),getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    initialiseAlarmSystem();
}

void LSCommonImpl::cleanUp()
{
    cleanUpAlarmSystem();

    try
    {
        //MonitorHelper::cleanUp();
        LSCommonBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void LSCommonImpl::hwDiagnostic()
{
    try
    {
        LSCommonBase::diagnosticModeEnabled = true;
        LSCommonBase::hwDiagnostic();
    }
    catch(const ControlDeviceExceptions::HwLifecycleExImpl& ex)
    {
        throw ControlDeviceExceptions::HwLifecycleExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }

}

//-----------------------------------------------------------------------------
// Hardware Lifecycle Methods
//-----------------------------------------------------------------------------
void LSCommonImpl::hwInitializeAction()
{
    LSCommonBase::hwInitializeAction();
}

void LSCommonImpl::hwOperationalAction()
{
    LSCommonBase::hwOperationalAction();
}

void LSCommonImpl::hwStopAction()
{
    // Check if the pointer has been set to 0.  This would be true when the
    // component had already executed cleanUp of this component.  The cleanUp
    // call makes it in the inheritance hierarchy up to
    // Control::HardwareDeviceImpl::cleanUp, which calls
    //  -> Control::HardwareDeviceImpl::hwStop which in turn calls
    //     -> Control::HardwareDeviceImpl::hwStopAction which calls
    //        -> all the hwStop methods in the inheriting classes.
    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }

    LSCommonBase::diagnosticModeEnabled = false;
    LSCommonBase::hwStopAction();
}

void LSCommonImpl::hwDiagnosticAction()
{
    LSCommonBase::hwDiagnosticAction();
}

//-----------------------------------------------------------------------------
// Monitoring Distpach Method
//-----------------------------------------------------------------------------
void LSCommonImpl::processRequestResponse(const AMBRequestStruct& response)
{
}

//-----------------------------------------------------------------------------
// Other Methods
//-----------------------------------------------------------------------------
CORBA::ULong LSCommonImpl::GET_SYSTEM_GET_STATUS_SYSTEM_STATE(ACS::Time& timestamp)
{
    try
    {
        CORBA::ULong ret(getSystemGetStatusSystemState(timestamp));
        return ret;
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
}

unsigned char LSCommonImpl::getSystemGetStatusSystemState(ACS::Time& timestamp)
{
    return 0U;
}

CORBA::Boolean LSCommonImpl::GET_PHASELOCK_GET_STATUS_LOCKED(ACS::Time& timestamp)
{
    try
    {
        CORBA::Boolean ret(getPhaselockGetStatusLocked(timestamp));
        return ret;
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
}

bool LSCommonImpl::getPhaselockGetStatusLocked(ACS::Time& timestamp)
{
    return true;
}

CORBA::Boolean LSCommonImpl::GET_PHASELOCK_GET_STATUS_TUNING_READY(ACS::Time& timestamp)
{
    try
    {
        CORBA::Boolean ret(getPhaselockGetStatusTuningReady(timestamp));
        return ret;
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
}

bool LSCommonImpl::getPhaselockGetStatusTuningReady(ACS::Time& timestamp)
{
    return true;
}

CORBA::Boolean LSCommonImpl::GET_PHASELOCK_GET_STATUS_RF_INPUT_READY(ACS::Time& timestamp)
{
    try
    {
        CORBA::Boolean ret(getPhaselockGetStatusRfInputReady(timestamp));
        return ret;
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
}

bool LSCommonImpl::getPhaselockGetStatusRfInputReady(ACS::Time& timestamp)
{
    return true;
}

char* LSCommonImpl::getSerialNumberStr()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    std::string sn = HardwareDeviceImpl::getSerialNumber();

    return CORBA::string_dup(sn.c_str());
}

std::vector< unsigned char > LSCommonImpl::getSystemGetStatus(
    ACS::Time& timestamp)
{
    std::vector< unsigned char > ret;
    return ret;
}

double LSCommonImpl::rawToWorldLaserTempSetpoint0(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LSCommonImpl::rawToWorldLaserTempSetpoint1(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LSCommonImpl::rawToWorldLaserTempSetpoint2(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LSCommonImpl::rawToWorldLaserTempSetpoint3(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LSCommonImpl::rawToWorldSignalGetLaserTempMon0(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSCommonImpl::rawToWorldSignalGetLaserTempMon1(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSCommonImpl::rawToWorldSignalGetLaserTempMon2(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSCommonImpl::rawToWorldSignalGetLaserTempMon3(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSCommonImpl::rawToWorldSignalGetExternThernMon(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSCommonImpl::rawToWorldSignalGetInternThernMon(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LSCommonImpl::rawToWorldLaserTemp(const unsigned short raw) const
{
    //returned value in Kelvin
    const double r_target(raw * LASER_TEMP_R
        / static_cast< double >(32768.0 - raw));
    const double r_relative(r_target / LASER_TEMP_R);
    const double temp_inv(LASER_TEMP_A
        + LASER_TEMP_B * std::log(r_relative)
        + LASER_TEMP_C * std::pow(std::log(r_relative), 2.0)
        + LASER_TEMP_D * std::pow(std::log(r_relative), 3.0));

    return (1.0 / temp_inv);
}

double LSCommonImpl::rawToWorldLaserTempSetpoint(const unsigned short raw) const
{
    //returned value in Kelvin
    const double r_target(raw * LASER_TEMP_R
        / static_cast< double >(65535 - raw));
    const double r_relative(r_target / LASER_TEMP_R);
    const double temp_inv(LASER_TEMP_A
        + LASER_TEMP_B * std::log(r_relative)
        + LASER_TEMP_C * std::pow(std::log(r_relative), 2.0)
        + LASER_TEMP_D * std::pow(std::log(r_relative), 3.0));

    return (1.0 / temp_inv);
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LSCommonImpl)
