#! /usr/bin/env python
# "@(#) $Id$"
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#
"""
This module is part of the Control Command Language.
It contains complementary functionality for a LSCommon device.
"""

import CCL.HardwareDevice

from CCL.Container import getComponent
from CCL.Container import getDynamicComponent
from CCL.logging import getLogger
import CCL.LSCommonBase

class LSCommon(CCL.LSCommonBase.LSCommonBase):
    '''
    The LSCommon class inherits from the code generated LSCommonBase
    class and adds specific methods.
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        The constructor creates a LSCommon object using LSCommonBase constructor.
        EXAMPLE:
        from CCL.LSCommon import LSCommon
        obj = LSCommon("AOSTiming")

        '''
        # Dictionary of device instances
        self._devices = {}

        # Group of antenna or component names.
        if ((isinstance(antennaName,list) == True) |
            (isinstance(componentName,list) == True)):
            #antenna names
            if (isinstance(antennaName,list) == True):
                if (len(antennaName) != 0):
                    for idx in range (0,len(antennaName)):
                        self._devices["CONTROL/"+antennaName[idx]+"/LORTM"]=""
            #component names
            if (isinstance(componentName,list) == True):
                if (len(componentName) != 0):
                    for idx in range (0,len(componentName)):
                        self._devices[componentName[idx]]=""
        # One component
        else:
            if not((antennaName == None) ^ (componentName == None)):
                raise NameError, "missing antennaName or componentName"

            if antennaName != None:
                self._devices["CONTROL/"+antennaName+"/LORTM"]=""

            if componentName != None:
                self._devices[componentName] = ""

        # Initialize the base class
        for key, val in self._devices.iteritems():
            CCL.LSCommonBase.LSCommonBase.__init__(self, \
                                                   None, \
                                                   key, \
                                                   stickyFlag);
            self._devices[key]= self._HardwareDevice__hw

        self.__logger = getLogger()

    def __del__(self):
        for key, val in self._devices.iteritems():
            instance = self._devices[key]
            del(instance)
        CCL.LSCommonBase.LSCommonBase.__del__(self)


    #
    # SYSTEM_GET_STATUS_SYSTEM_STATE
    #
    def GET_SYSTEM_GET_STATUS_SYSTEM_STATE(self):
        '''
Retrieves system state
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = \
                self._devices[key].GET_SYSTEM_GET_STATUS_SYSTEM_STATE()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # PHASELOCK_GET_STATUS_LOCKED
    #
    def GET_PHASELOCK_GET_STATUS_LOCKED(self):
        '''
Retrieves slave laser is locked to the reference. Lock monitoring
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = \
                self._devices[key].GET_PHASELOCK_GET_STATUS_LOCKED()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # PHASELOCK_GET_STATUS_TUNING_READY
    #
    def GET_PHASELOCK_GET_STATUS_TUNING_READY(self):
        '''
Retrieves Tunning Ready flag. When RF synthesizer are ready, it indicates that the Tuning Finalize command can be sent
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = \
                self._devices[key].GET_PHASELOCK_GET_STATUS_TUNING_READY()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # PHASELOCK_GET_STATUS_RF_INPUT_READY
    #
    def GET_PHASELOCK_GET_STATUS_RF_INPUT_READY(self):
        '''
Retrieves RF Input Ready flag. RF synthesizer can be set to new frquency / power. that the Tuning Finalize command can be sent
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = \
                self._devices[key].GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def STATUS(self):
        '''
        Method to print relevant status information to the screen
        '''
        print "not implemented yet"

#
# ___oOo___
