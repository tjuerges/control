#ifndef LSCommonIMPL_H
#define LSCommonIMPL_H
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
// @(#) $Id$
//


// Base class(es)
#include <LSCommonBase.h>
#include <monitorHelper.h>

//CORBA servant header
#include <LSCommonS.h>


namespace Control
{
    class AlarmSender;
}


class LSCommonImpl: public virtual POA_Control::LSCommon,
    public ::LSCommonBase,
    public MonitorHelper
{
  public:

    // ------------------- Constructor & Destructor -------------------
    ///
    /// The constructor for any ACS C++ component must have this signature
    ///
    LSCommonImpl(const ACE_CString& name, maci::ContainerServices* cs);

    ///
    /// The destructor must be virtual because this class contains virtual
    /// functions.
    ///
    virtual ~LSCommonImpl();

    /* ---------------- Component Lifecycle -----------------------*/
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /// \exception ControlDeviceExceptions::HwLifecycleExImpl
    virtual void hwDiagnostic();

    /// MonitorPoint: SYSTEM_GET_STATUS_SYSTEM_STATE
    /// Retrieves system state
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::ULong GET_SYSTEM_GET_STATUS_SYSTEM_STATE(ACS::Time& timestamp);

    virtual unsigned char getSystemGetStatusSystemState(ACS::Time& timestamp);

    /// MonitorPoint: PHASELOCK_GET_STATUS_LOCKED
    /// Retrieves slave laser is locked to the reference. Lock monitoring
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Boolean GET_PHASELOCK_GET_STATUS_LOCKED(ACS::Time& timestamp);

    virtual bool getPhaselockGetStatusLocked(ACS::Time& timestamp);

    /// MonitorPoint: PHASELOCK_GET_STATUS_TUNING_READY
    /// Retrieves Tunning Ready flag. When RF synthesizer are ready,
    /// it indicates that the Tuning Finalize command can be sent
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Boolean GET_PHASELOCK_GET_STATUS_TUNING_READY(ACS::Time& timestamp);

    virtual bool getPhaselockGetStatusTuningReady(ACS::Time& timestamp);

    /// MonitorPoint: PHASELOCK_GET_STATUS_RF_INPUT_READY
    /// Retrieves RF Input Ready flag. RF synthesizer can be set
    /// to new frquency / power. that the Tuning Finalize command
    /// can be sent
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual CORBA::Boolean GET_PHASELOCK_GET_STATUS_RF_INPUT_READY(ACS::Time& timestamp);

    virtual bool getPhaselockGetStatusRfInputReady(ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual char* getSerialNumberStr();


  protected:
    /* ---------------- Hardware Lifecycle Interface ---------------*/
    virtual void hwInitializeAction();

    virtual void hwOperationalAction();

    virtual void hwStopAction();

    virtual void hwDiagnosticAction();

    /* ------------------ Monitor Helper Methods ------------------- */
    virtual void processRequestResponse(const AMBRequestStruct& response);

    /* ------------------Common Methods for all ALMA systhetizers -- */
    double rawToWorldLaserTemp(const unsigned short raw) const;

    double rawToWorldLaserTempSetpoint(const unsigned short raw) const;

    virtual double rawToWorldSignalGetLaserTempMon0(
        const unsigned short raw) const;

    virtual double rawToWorldSignalGetLaserTempMon1(
        const unsigned short raw) const;

    virtual double rawToWorldSignalGetLaserTempMon2(
        const unsigned short raw) const;

    virtual double rawToWorldSignalGetLaserTempMon3(
        const unsigned short raw) const;

    virtual double rawToWorldSignalGetExternThernMon(
        const unsigned short raw) const;

    virtual double rawToWorldSignalGetInternThernMon(
        const unsigned short raw) const;

    virtual double rawToWorldLaserTempSetpoint0(
        const unsigned short raw) const;

    virtual double rawToWorldLaserTempSetpoint1(
        const unsigned short raw) const;

    virtual double rawToWorldLaserTempSetpoint2(
        const unsigned short raw) const;

    virtual double rawToWorldLaserTempSetpoint3(
        const short unsigned raw) const;

    /// This is a stub implementation which allows the inheriting classes
    /// to overwrite this method.
    virtual std::vector< unsigned char > getSystemGetStatus(
        ACS::Time& timestamp);

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    /// \exception LSCommonExceptions::TimeOutEx

    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    /// \exception LSCommonExceptions::TimeOutEx
    /// \exception ControlCommonException::LockError

    /// Pointer for the alarm system helper instance.  Keep it protected, then
    /// inheriting classes are allowed to use it.
    Control::AlarmSender* alarmSender;

    /// Enums for alarm conditions.  Add alarm conditions which are specific
    /// for a certain hardware, i.e. LORTM or LS, here, too.  This allows to
    /// keep all alarms in one place.  Keep it protected, then
    /// inheriting classes are allowed to use it.
    enum LsAlarmConditions
    {
        LsDidNotLock = 1,
        LsWasNotInAutomaticStartupMode
    };

    const static double LASER_TEMP_W = -1.414196e+1;

    const static double LASER_TEMP_X = 4.430783e+3;

    const static double LASER_TEMP_Y = -3.407898e+4;

    const static double LASER_TEMP_Z = -8.894193e+6;

    const static double LASER_TEMP_A = 3.3540154e-3;

    const static double LASER_TEMP_B = 2.5627725e-4;

    const static double LASER_TEMP_C = 2.0829221e-6;

    const static double LASER_TEMP_D = 7.3003206e-8;

    const static double LASER_TEMP_R = 10000;

    const static double LASER_TEMP_KELVIN_TO_CELSIUS = -273.15;


  private:
    // The copy constructor is made private to prevent a compiler generated one
    // from being used. It is not implemented.
    LSCommonImpl(const LSCommonImpl& other);

    /// Initialise the helper system for alarms.
    void initialiseAlarmSystem();

    /// Clean up the helper system for alarms.
    void cleanUpAlarmSystem();
};
#endif
