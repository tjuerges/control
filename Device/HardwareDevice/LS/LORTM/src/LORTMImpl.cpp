// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
// @(#) $Id$
//


#include <LORTMImpl.h>
#include <string> // for string
#include <cmath> // for std::fabs and ::round.
#include <loggingMACROS.h> // for AUTO_TRACE
// For audience logs.
#include <sstream>
#include <iomanip>
#include <LogToAudience.h>

#include <acstime.h>

#include <ControlCommonExceptions.h>
#include <ControlExceptions.h>

// Alarm system stuff
#include <controlAlarmSender.h>
#include <LSCommonImpl.h>


//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
LORTMImpl::LORTMImpl(const ACE_CString& name, maci::ContainerServices* cs):
    ::LORTMBase(name, cs),
    LSBFreq(0.0),
    LSBFreqStatus(false),
    FreqTolerance(0.0)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

LORTMImpl::~LORTMImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

//-----------------------------------------------------------------------------
// Component Lifecycle Methods
//-----------------------------------------------------------------------------
void LORTMImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        LORTMBase::initialize();
        //MonitorHelper::initialize(name(),getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void LORTMImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        //MonitorHelper::cleanUp();
        LORTMBase::cleanUp();
    }
    catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void LORTMImpl::hwDiagnostic()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        LORTMBase::diagnosticModeEnabled = true;
        LORTMBase::hwDiagnostic();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

}

//-----------------------------------------------------------------------------
// Hardware Lifecycle Methods
//-----------------------------------------------------------------------------
void LORTMImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LSCommonImpl::alarmSender->forceTerminateAllAlarms();

    ACS::Time timeStamp(0ULL);
    try
    {
        if(LORTMBase::getSystemStartupMode(timeStamp) != 1U)
        {
            LSCommonImpl::alarmSender->activateAlarm(
                LsWasNotInAutomaticStartupMode);

            try
            {
                setCntlSystemStartupMode(1U);
            }
            catch(const ControlExceptions::INACTErrorExImpl& ex)
            {
                ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
                nex.addData("Detail", "The LS is not at least in startup mode "
                    "yet.  Could not set the start up mode to automatic.");
                nex.log();
            }
            catch(const ControlExceptions::CAMBErrorExImpl& ex)
            {
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
                nex.addData("Detail", "Could not set the start up mode to "
                    "automatic.");
                nex.log();
            }
        }
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "The LS is not at least in startup"
            "mode yet.  Could not read the start up mode.");
            nex.log();
        }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not read the start up mode.");
        nex.log();
    }

    LORTMBase::hwInitializeAction();
    //MonitorHelper::resume();
}

void LORTMImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LORTMBase::hwOperationalAction();
}

void LORTMImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LORTMBase::diagnosticModeEnabled = false;
    //MonitorHelper::suspend();
    LORTMBase::hwStopAction();
}

void LORTMImpl::hwDiagnosticAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Diagnostic mode (software) = manual mode in LS hardware.
    // Switch to manual mode of the LS hardware.
    try
    {
        setCntlSystemManualModeRequest();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "The LS is hardware could not be switched to "
            "manual mode.");
        nex.log();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "The LS is hardware could not be switched to "
            "manual mode.");
        nex.log();
    }

    LORTMBase::hwDiagnosticAction();
}

//-----------------------------------------------------------------------------
// Monitoring Distpach Method
//-----------------------------------------------------------------------------
void LORTMImpl::processRequestResponse(const AMBRequestStruct& response)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

}

//-----------------------------------------------------------------------------
// Overwriting unit conversion methods
//-----------------------------------------------------------------------------
double LORTMImpl::rawToWorldLaserTempSetpoint0(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LORTMImpl::rawToWorldLaserTempSetpoint1(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LORTMImpl::rawToWorldLaserTempSetpoint2(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

double LORTMImpl::rawToWorldLaserTempSetpoint3(const unsigned short raw) const
{
    return rawToWorldLaserTempSetpoint(raw);
}

unsigned short LORTMImpl::worldToRawCntlLaserTempSetpoint0(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LORTMImpl::worldToRawCntlLaserTempSetpoint1(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LORTMImpl::worldToRawCntlLaserTempSetpoint2(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

unsigned short LORTMImpl::worldToRawCntlLaserTempSetpoint3(const float world) const
{
    return worldToRawCntlLaserTempSetpoint(world);
}

double LORTMImpl::rawToWorldSignalGetLaserTempMon0(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LORTMImpl::rawToWorldSignalGetLaserTempMon1(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LORTMImpl::rawToWorldSignalGetLaserTempMon2(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LORTMImpl::rawToWorldSignalGetLaserTempMon3(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LORTMImpl::rawToWorldSignalGetExternThernMon(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

double LORTMImpl::rawToWorldSignalGetInternThernMon(const unsigned short raw) const
{
    return rawToWorldLaserTemp(raw);
}

unsigned short LORTMImpl::worldToRawCntlLaserTempSetpoint(const float world) const
{
    const float tmp(LASER_TEMP_W
        + LASER_TEMP_X / world
        + LASER_TEMP_Y / std::pow(world, 2.0F)
        + LASER_TEMP_Z / std::pow(world, 3.0F));
    const float  r_target(std::exp(tmp) * LASER_TEMP_R);
    const float setpoint((r_target * 65535.0) / (r_target + LASER_TEMP_R));

    return static_cast< unsigned short >(::round(setpoint));
}

float LORTMImpl::rawToWorldLaserTempSetpoint(const unsigned short raw) const
{
    //returned value in Kelvin
    const float r_target(raw * LASER_TEMP_R
        / static_cast< float >(65535.0 - raw));
    const float r_relative(r_target / LASER_TEMP_R);
    const float temp_inv(LASER_TEMP_A
        + LASER_TEMP_B * std::log(r_relative)
        + LASER_TEMP_C * std::pow(std::log(r_relative), 2.0F)
        + LASER_TEMP_D * std::pow(std::log(r_relative), 3.0F));

    return (1.0 / temp_inv);
}

float LORTMImpl::rawToWorldLaserTemp(const unsigned short raw) const
{
    //returned value in Kelvin
    const float r_target(raw * LASER_TEMP_R
        / static_cast< float >(32768.0 - raw));
    const float r_relative(r_target / LASER_TEMP_R);
    const float temp_inv(LASER_TEMP_A
        + LASER_TEMP_B * std::log(r_relative)
        + LASER_TEMP_C * std::pow(std::log(r_relative), 2.0F)
        + LASER_TEMP_D * std::pow(std::log(r_relative), 3.0F));

    return (1.0 / temp_inv);
}


//-----------------------------------------------------------------------------
// Hack for Teraxion's LORTM device, as the ambient temperature has a bug in the
// bytes ordering
//-----------------------------------------------------------------------------

float LORTMImpl::rawToWorldAmbientTemperature(const std::vector<unsigned char>& raw){

    // This calculate the AMBSI temperature.
    const std::string fnName("LORTMImpl::rawToWorldAmbientTemperature");
    AUTO_TRACE(fnName);

    // Return the temperature in Kelvin
    return (ds1820Temp(raw[1], raw[0], raw[2], raw[3]) + 273.15 );
}

float LORTMImpl::ds1820Temp(const u_int8_t temp, const int8_t sign,
    const u_int8_t countRem, const u_int8_t countPerC) const
{
    // The DS1820 data sheet states that count per c is always 16.
    // If it is not then something is very broken
    if(countPerC != 16)
    {
        return -9999.99;
    }

    short tempVal(temp);              //Copy the temp to a 16-bit
    tempVal |= (sign << 8U);          //Put the sign byte in the upper bytes
                                      //Do this before the divide so that the
                                      //sign is handled properly.
    tempVal = tempVal >> 1;             //As per the ds1850data sheet, we need
                                      //to divide by 2 AND drop the last bit.
    float floatTemp(tempVal);         //Switch to floating point for decimal calculations
    return (floatTemp - 0.25 + (      //Use the equation in the data sheet
        (static_cast< float >(countPerC) - static_cast< float >(countRem))
            / static_cast< float >(countPerC)));

}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LORTMImpl)
