#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import time
from Acspy.Clients.SimpleClient import PySimpleClient
import Acspy.Util.ACSCorba
from Acspy.Common.QoS import setObjectTimeout
from Control import HardwareDevice
from Acspy.Common.TimeHelper import TimeUtil

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
    def __init__(self, methodName="runTest"):
        self.tu = TimeUtil()
        unittest.TestCase.__init__(self,methodName)

    def __del__(self):
        if self.out != None:
          print self.out
        self.client.disconnect()
        del(self.client)

    def setUp(self):
        setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
	self.client = PySimpleClient.getInstance()
        self.componentName = "CONTROL/CentralLO/PhotonicReference1/LS"
        self.out = ""

    def tearDown(self):
        self.client.releaseComponent(self.componentName)
#        self.client.disconnect()
#        del(self.client)
#        if self.out != "":
#           print self.out

    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin "+testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")
                 
    def log(self,msg):
        self.out += msg + "\n"

    # --------------------------------------------------
    # Test Definitions    
    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        self.startTestLog("Test 1: Component startup & shutdown Test")
        lortm = self.client.getComponent (self.componentName);
        self.client.releaseComponent(lortm._get_name())
        self.endTestLog("Test 1: Component startup & shutdown Test")

    def test02(self):
        '''
        Verify hardware lifecycle functionality
        '''
        self.startTestLog("Test 2: Component hardware lifecycle functionality")
        lortm = self.client.getComponent(self.componentName)
        setObjectTimeout(lortm, 2000)
        if (str(lortm.getHwState()) != 'Operational'):
            lortm.hwStop()
            lortm.hwStart()
            lortm.hwConfigure()
            lortm.hwInitialize()
            lortm.hwOperational()
        self.endTestLog("Test 2: Component hardware lifecycle functionality")

    def test03(self):
        '''
        Check Monitor points
        '''
        self.startTestLog("Test 3: Check Monitor Points")
        #setObjectTimeout(lortm, 2000)
        lortm = self.client.getComponent(self.componentName)
        if (str(lortm.getHwState()) == 'Operational'):
           val = lortm.GET_SYSTEM_GET_STATUS()[0]
           self.failUnless( (val != 0 ),\
                          "GET_SYSTEM_GET_STATUS not in STARTUP mode")

        self.endTestLog("Test 3: Check Monitor Points")

    def test04(self):
        '''
        Check Monitor points: SIGNAL_GET_LASER_TEMP_MON0
        '''
        self.startTestLog("Test 4: Check Monitor Points: SIGNAL_GET_LASER_TEMP_MON0")
        lortm = self.client.getComponent(self.componentName)
        if (str(lortm.getHwState()) == 'Operational'):
           val = lortm.GET_SYSTEM_GET_STATUS()[0]
           self.failUnless( (val != 0 ),\
                          "GET_SYSTEM_GET_STATUS not in STARTUP mode")

        self.endTestLog("Test 3: Check Monitor Points")

    def test04(self):
        '''
        Check PHASE LOCK PROCESS, see ICD Appendix A, page 29
        '''
        self.startTestLog("Test 4: PHASE-LOCK process")
        #setObjectTimeout(lortm, 2000)
        lortm = self.client.getComponent(self.componentName)
        if (str(lortm.getHwState()) != 'Operational'):
	   lortm.hwStop()
	   lortm.hwStart()
  	   lortm.hwConfigure()
	   lortm.hwOperational()

	# send the lortm firmware to STANDBY mode
	self.log("   -> send LORTM firmware to STANDBY mode")
	lortm.SET_SYSTEM_STANDBY_MODE_REQUEST()			
	val, ts = lortm.GET_SYSTEM_GET_STATUS()
	val, ts = lortm.GET_SYSTEM_GET_STATUS_SYSTEM_STATE();
   	self.failUnless( ( val == 2 ),\
                          "System could not be set into STANDBY mode")

	# send TUNING_INIT to LORTM firmware
	self.log("   -> send TUNING_INIT to LORTM firmware, set laser to 190.670 TeraHz")
	lortm.SET_PHASELOCK_COMMAND_TUNING_INIT(190.670e12)
	time.sleep(2)
	
	#check "RF input ready" flag
	self.log("   <- check 'RF input ready' flag")
	time.sleep(1)
	(val, ts) = lortm.GET_PHASELOCK_GET_STATUS()
	(val, ts) = lortm.GET_PHASELOCK_GET_STATUS_RF_INPUT_READY()
   	self.failUnless( ( val == True ),\
                          "'RF input ready' was not set")

	#check "Tuning ready" flag
	self.log("   <- check 'Tuning ready' flag")
	time.sleep(1)
	(val, ts) = lortm.GET_PHASELOCK_GET_STATUS()
	(val, ts) = lortm.GET_PHASELOCK_GET_STATUS_TUNING_READY()
   	self.failUnless( ( val == True ),\
                          "'Tuning ready' flag was not set")
	
	#send TUNING_FINALIZE command"
	self.log("   -> send TUNING_FINALIZE command")
	lortm.SET_PHASELOCK_COMMAND_TUNING_FINALIZE() 
	time.sleep(2)

	#check "locked" flag
	self.log("   <- check 'Locked' flag")
	time.sleep(1)
	(val, ts) = lortm.GET_PHASELOCK_GET_STATUS()
	(val, ts) = lortm.GET_PHASELOCK_GET_STATUS_LOCKED()
   	self.failUnless( ( val == True ),\
                          "'Locked' flag was not set")
	
	#check SYSTEM is in "OPERATIONAL" mode
	self.log("   -> check SYSTEM is in 'OPERATIONAL' mode")
	time.sleep(1)
	(val, ts) = lortm.GET_SYSTEM_GET_STATUS()
	(val, ts) = lortm.GET_SYSTEM_GET_STATUS_SYSTEM_STATE();
   	self.failUnless( ( val == 4 ),\
                          "System is not in 'OPERATIONAL' mode")

        self.endTestLog("Test 4: PHASE-LOCK process")
class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg



# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    client = PySimpleClient.getInstance()
    if len(sys.argv) == 1:
        sys.argv.append("interactive")

    if len(sys.argv) == 2:
        sys.argv.append("Test")

    container = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main()
    
