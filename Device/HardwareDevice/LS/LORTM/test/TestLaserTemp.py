#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tshen  2008-12-02  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#
import unittest
from math import exp, pow, log

class laser_temperature():
	def __init__(self):
		self.constants = {}
		self.constants["a"] = 3.3540154e-3
		self.constants["b"] = 2.5627725e-4
		self.constants["c"] = 2.082921e-6
		self.constants["d"] = 7.3003206e-8
		self.constants["w"] = -1.414196e1
		self.constants["x"] = 4.430783e3
		self.constants["y"] = -3.407898e4
		self.constants["z"] = -8.894193e6
		self.constants["r25"] = 10000
	
	def temp_to_raw(self, temperature):
		#temperature in celsius 
		
		#converto Kelvin
		temperature = temperature + 273.15
		tmp = self.constants['w'] + self.constants['x']/temperature + \
		                                 self.constants['y']/pow(temperature,2)	+ \
                                                 self.constants['z']/pow(temperature,3)
		r_target = exp(tmp) * self.constants['r25']
		raw = (r_target*65535)/(r_target+self.constants['r25'])
		return raw

	def raw_to_temp(self, raw):
		r_target = raw*self.constants['r25']/float(65535-raw)
		r_relative = r_target /self.constants['r25']
		temp_inv = self.constants['a'] + \
			   self.constants['b'] * log(r_relative)+\
			   self.constants['c'] * pow(log(r_relative),2)+\
			   self.constants['d'] * pow(log(r_relative),3)
		temp = (1/temp_inv) - 273.15
		return (r_relative, temp)

	def generate_data(self):
		r_relative = []
		temperature = []
		for i in range(1,65535, 100):
			#print "i=", i 
			(r, temp) = self.raw_to_temp(i)
			#print "temp=", temp
			r_relative.append(r)
			temperature.append(temp)
			#print "data[%d]=%f" % (i,data[i-1])

		return (r_relative,  temperature)
	 
 


#unit test
class laser_temperature_unit_test(unittest.TestCase):
	def setUp(self):
		self.laser = laser_temperature()
	def tearDown(self):
		pass

	def testGetConstansts(self):
		self.assert_("a" in self.laser.constants)
		self.assert_("b" in self.laser.constants)
		self.assert_("c" in self.laser.constants)
		self.assert_("d" in self.laser.constants)
		self.assert_("w" in self.laser.constants)
		self.assert_("x" in self.laser.constants)
		self.assert_("y" in self.laser.constants)
		self.assert_("z" in self.laser.constants)
			
	def testraw_to_temp(self):
		temp = self.laser.raw_to_temp(1)
		print "temp=" ,temp
		self.assertEqual(temp, -1, "error in temerature to raw convertion!")
		
if __name__ == '__main__':
	unittest.main()	

#
# ___oOo___
