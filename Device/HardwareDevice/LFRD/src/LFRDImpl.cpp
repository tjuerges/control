/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) European Southern Observatory, 2002
* (c) Associated Universities Inc., 2002
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* File LFRDImpl.cpp
*
*/

#include <LFRDImpl.h>

using std::string;
using std::ostringstream;

/**
 *-----------------------
 * LFRD Constructor
 *-----------------------
 */
LFRDImpl::LFRDImpl(const ACE_CString& name, maci::ContainerServices* cs) : 
	LFRDBase(name,cs)
{
  ACS_TRACE("LFRDImpl::LFRDImpl");
}

/**
 *-----------------------
 * LFRD Destructor
 *-----------------------
 */
LFRDImpl::~LFRDImpl()
{
  ACS_TRACE("LFRDImpl::~LFRDImpl");
}

/**
 *---------------------------------
 * ACS Component Lifecycle Methods
 *---------------------------------
 */
void LFRDImpl::initialize()
  throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
  const char* fnName = "LFRDImpl::initialize";
  ACS_TRACE(fnName);

  try {
    LFRDBase::initialize();
  } 
  catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,fnName);
  }
}

void LFRDImpl::cleanUp()
  throw(acsErrTypeLifeCycle::LifeCycleExImpl)
{
  const char* fnName = "LFRDImpl::cleanUp";
  ACS_TRACE(fnName);

  try {
    LFRDBase::cleanUp();
  } 
  catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,fnName);
  }
}

/**
 *----------------------------
 * Hardware Lifecycle Methods
 *----------------------------
 */
void LFRDImpl::hwInitializeAction()
{
  const char* fnName = "LFRDImpl::hwInitializeAction";
  ACS_TRACE(fnName);

  // Call the base class implementation
  LFRDBase::hwInitializeAction();
}

void LFRDImpl::hwOperationalAction()
{
  const char* fnName = "LFRDImpl::hwOperationalAction";
  ACS_TRACE(fnName);

  // Call the base class implementation
  LFRDBase::hwOperationalAction();
}

void LFRDImpl::hwStopAction()
{
  const char* fnName = "LFRDImpl::hwStopAction";
  ACS_TRACE(fnName);
  
  // Call the base class implementation
  LFRDBase::hwStopAction();
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LFRDImpl)

/*___oOo___*/
