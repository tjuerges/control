#! /usr/bin/env python
# "@(#) $Id$"
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#
"""
This module is part of the Control Command Language.
It contains complementary functionality for LFRD devices.
"""
import CCL.LFRDBase
from CCL import StatusHelper


class LFRD(CCL.LFRDBase.LFRDBase):
    '''
    The LFRD class inherits from the code generated LFRDBase
    class and adds specific methods.
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        The constructor creates a LFRD object using LFRDBase constructor.
        EXAMPLE:
        from CCL.LFRD import LFRD
        obj = LFRD("DV01")

        '''
        try:
            CCL.LFRDBase.LFRDBase.__init__(self, \
                              antennaName, \
                              componentName, \
                              stickyFlag)
        except Exception, e:
            print 'LFRD component is not running \n' + str(e)

    def __del__(self):
        CCL.LFRDBase.LFRDBase.__del__(self)

    def STATUS(self):
        '''
        Method to print relevant status information to the screen.
        '''
        
        '''Get component and antenna name'''
        listNames = self._HardwareDevice__componentName.rsplit("/")
        antName   = listNames[1]
        compName  = listNames[2]
        
#
# ___oOo___
