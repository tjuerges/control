/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File LFRDHWSimImpl.h
 *
 * $Id$
 */
#ifndef LFRDHWSimImpl_H
#define LFRDHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "LFRDHWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * LFRDHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class LFRDHWSimImpl : public LFRDHWSimBase
  {
  public :
	LFRDHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);
    
  protected:

  }; // class LFRDHWSimImpl

} // namespace AMB

#endif // LFRDHWSimImpl_H
