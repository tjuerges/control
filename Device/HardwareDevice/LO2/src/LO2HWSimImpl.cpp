// $Id$
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008
// Copyright by AUI (in the framework of the ALMA collaboration),
// All rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY, without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA 02111-1307  USA

#include "LO2HWSimImpl.h"

using AMB::LO2HWSimImpl;

/* Please use this class to implement complex functionality for the
 * LO2HWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

LO2HWSimImpl::LO2HWSimImpl(node_t node, 
			   const std::vector<CAN::byte_t>& serialNumber,
			   int antenna,
			   SHARED_SIMULATOR::SharedSimulator_var ss)
  : LO2HWSimBase::LO2HWSimBase(node, serialNumber)
{
  ss_m = ss;
  if(!CORBA::is_nil(ss.in()))
    ssi_m = new SharedSimWrapper(antenna, ss_m);
}

LO2HWSimImpl::~LO2HWSimImpl()
{
  if (!ssi_m) delete ssi_m;
}

void LO2HWSimImpl::setControlSetLo2Frequency(
    const std::vector< CAN::byte_t >& data)
{
    LO2HWSimBase::setControlSetLo2Frequency(data);
    setMonitorLo2Frequency(data);
}

void LO2HWSimImpl::setControlSetRangeLow(const std::vector< CAN::byte_t >& data)
{
  LO2HWSimBase::setControlSetRangeLow(data);
  std::vector< CAN::byte_t > status = getMonitorModuleStatus();
  if ((data[0] & 0x01) == 0x01) { // set bit 1 of status
    status[0] |= 0x02;
  } else { // clear bit 1 
    status[0] &= ~0x02;
  }
  setMonitorModuleStatus(status);
}

void LO2HWSimImpl::setControlSetLock(const std::vector< CAN::byte_t >& data)
{
  LO2HWSimBase::setControlSetLock(data);
  std::vector< CAN::byte_t > status = getMonitorModuleStatus();
  if ((data[0] & 0x01) == 0x01) { 
    /* Tune State... everything unlocked */
    status[0] |= 0x01;
    status[0] &= ~0x04; // Clear bit 2
    status[0] |= 0x08;  // Set bit 3
  } else {
    /* Lock state */
    status[0] &= ~0x01;
    status[0] |= 0x04; // Set bit 2
  }
  setMonitorModuleStatus(status);
}

void LO2HWSimImpl::setControlResetLockStatus(const std::vector< CAN::byte_t >& data)
{
  LO2HWSimBase::setControlResetLockStatus(data);
  std::vector< CAN::byte_t > status = getMonitorModuleStatus();
  if ((data[0] & 0x01) == 0x01) {
    if (status[0] & 0x04) {
      status[0] &= ~0x08;
    } else {
      status[0] |= 0x08;
    }
    setMonitorModuleStatus(status);
  }
}

void LO2HWSimImpl::setControlSetPhaseVals(const std::vector< CAN::byte_t >& data)
{
    LO2HWSimBase::setControlSetPhaseVals(data);
    setMonitorPhaseVals(data);
}

void LO2HWSimImpl::setControlSetAftOff(const std::vector< CAN::byte_t >& data)
{
  LO2HWSimBase::setControlSetAftOff(data);
  std::vector< CAN::byte_t > status = getMonitorModuleStatus();
  if (data[0] & 0x01) {
    status[0] |= 0x10;
  } else {
    status[0] &= ~0x10;
  }
  setMonitorModuleStatus(status);
}

void LO2HWSimImpl::setControlSetFreq(
    const std::vector< CAN::byte_t >& data)
{
    LO2HWSimBase::setControlSetFreq(data);
    setMonitorFreq(data);
}
