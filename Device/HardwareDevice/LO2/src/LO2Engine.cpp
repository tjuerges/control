//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <LO2Engine.h>
#include <acsutilTimeStamp.h>
#include <TETimeUtil.h>
#include <iostream>
#include <FTSDeviceData.h>
#include <ControlExceptions.h>
#include <almaEnumerations_IFC.h>
#include <Guard_T.h>


LO2Engine::LO2Engine():
    FTSEngine()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

/// LO2 Destructor
LO2Engine::~LO2Engine()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void LO2Engine::setUserFrequency(double loFrequency,
    NetSidebandMod::NetSideband offsetSideband)
{
    ACS::Time now(::getTimeStamp());
    now -= now % TETimeUtil::TE_PERIOD_ACS;
    setUserFrequency(loFrequency, offsetSideband, now);
}

void LO2Engine::setUserFrequency(double loFrequency,
    NetSidebandMod::NetSideband offsetSideband,
    ACS::Time applicationTime)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        if((loFrequency == getNominalFrequency(applicationTime))
        && (offsetSideband == getLO2FrequencySpec(
            applicationTime).OffsetSideband))
        {
            // Nothing to do, we are at the correct frequency.
            return;
        }
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        // This is ok, just an empty queue.
        nextFrequencyCommandTime_m = applicationTime;
    }

    queueFrequency(loFrequency, offsetSideband, applicationTime);

    if(applicationTime <= nextFrequencyCommandTime_m)
    {
        FTSEngine::reset();
    }

    cleanQueue();
}

void LO2Engine::queueFrequency(double loFrequency,
    NetSidebandMod::NetSideband offsetSideband,
    ACS::Time applicationTime)
{
    // At this point assume that we have already checked and this is a new
    // frequency, so we want to queue commands for both the LO2 and for the
    // FTS.
    // This can throw ControlExceptions::IllegalParameterErrorExImpl.
    LO2Engine::LO2FreqSpec loSpec;
    FTSEngine::FrequencySpecification ftsSpec;
    ftsSpec.loFrequency = loFrequency;
    ftsSpec.applicationTime = applicationTime;

    loSpec = findTuningSetup(ftsSpec);
    loSpec.OffsetSideband = offsetSideband;

    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

        while((lo2frequencySpecificationDeque_m.empty() == false)
        && (lo2frequencySpecificationDeque_m.back().applicationTime
            >= loSpec.applicationTime))
        {
            lo2frequencySpecificationDeque_m.pop_back();
        }

        lo2frequencySpecificationDeque_m.push_back(loSpec);
    }

    FTSEngine::queueFTSFrequency(ftsSpec);
}

void LO2Engine::cleanQueue()
{
    // This method goes through the queue and will remove all obsolete
    // commands.
    const ACS::Time now(::getTimeStamp());

    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

        if(lo2frequencySpecificationDeque_m.empty() == false)
        {
            unsigned int idx(lo2frequencySpecificationDeque_m.size() - 1);

            while((idx > 0)
            && (lo2frequencySpecificationDeque_m[idx].applicationTime > now))
            {
                --idx;
            }

            // idx now points to the first command, everything before this is junk
            while(idx > 0)
            {
                lo2frequencySpecificationDeque_m.pop_front();
                --idx;
            }
        }
    }

    FTSEngine::cleanQueue(now);
}

double LO2Engine::getNominalFrequency()
{
    return LO2Engine::getNominalFrequency(::getTimeStamp());
}

double LO2Engine::getNominalFrequency(ACS::Time applicationTime)
{
    return calculateLO2Frequency(getLO2FrequencySpec(applicationTime),
        getNominalFrequencySpec(applicationTime));
}

double LO2Engine::getOffsetFrequency()
{
    return getOffsetFrequency(::getTimeStamp());
}

double LO2Engine::getOffsetFrequency(ACS::Time applicationTime)
{
    return calculateLO2Frequency(getLO2FrequencySpec(applicationTime),
        getOffsetFrequencySpec(applicationTime));
}

LO2Engine::LO2FreqSpec LO2Engine::getLO2FrequencySpec(ACS::Time requestTime)
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(frequencyDequeMutex_m);

    if(lo2frequencySpecificationDeque_m.empty() == true)
    {
        // No frequency specified throw an exception
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "No frequency specifications available!");
        ex.log();
        throw ex;
    }

    unsigned int idx(lo2frequencySpecificationDeque_m.size() - 1);
    while((idx > 0)
    && (lo2frequencySpecificationDeque_m[idx].applicationTime > requestTime))
    {
        --idx;
    }

    return lo2frequencySpecificationDeque_m[idx];
}

double LO2Engine::calculateLO2Frequency(LO2Engine::LO2FreqSpec loSpec,
    FTSEngine::FrequencySpecification ftsSpec)
{
    double lo2CombLineFrequency(CombLineSpacing
        * static_cast< int >((loSpec.loFrequency / CombLineSpacing) + 0.5));

    double lo2Frequency(0.0);
    if(loSpec.FringeSideband == NetSidebandMod::USB)
    {
        lo2Frequency = lo2CombLineFrequency + rawToWorldFreq(
            ftsSpec.ftsFrequency);
    }
    else
    {
        lo2Frequency = lo2CombLineFrequency - rawToWorldFreq(
            ftsSpec.ftsFrequency);
    }

    return lo2Frequency;
}

void LO2Engine::updateFrequency(ACS::Time applicationTime, bool forceCommand)
{
    applicationTime -= applicationTime % TETimeUtil::TE_PERIOD_ACS;
    const ACS::Time commandTime(applicationTime + TETimeUtil::TE_PERIOD_ACS);

    LO2Engine::LO2FreqSpec loSpec(getLO2FrequencySpec(commandTime));

    if((forceCommand == true)
    || (loSpec.loFrequency != lastFrequencySpecCommand_m.loFrequency)
    || (loSpec.FringeSideband != lastFrequencySpecCommand_m.FringeSideband)
    || (loSpec.OffsetSideband != lastFrequencySpecCommand_m.OffsetSideband))
    {
        retuneLO2(loSpec.loFrequency, loSpec.FringeSideband,
            loSpec.OffsetSideband, applicationTime);
    }

    FTSEngine::updateFrequency(applicationTime, forceCommand);
}

void LO2Engine::retuneLO2(double loFrequency,
    NetSidebandMod::NetSideband fringeSideband,
    NetSidebandMod::NetSideband offsetSideband,
    ACS::Time applicationTime)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Ok the FTS will get the new Frequency command at application time so
    // arrange around that
    queueLO2FrequencyCommand(loFrequency, applicationTime);
    queueSidebandCommand(fringeSideband, offsetSideband, applicationTime);
    queueLockCommand(false, applicationTime);
    queueLockCommand(true, applicationTime + TETimeUtil::TE_PERIOD_ACS);
    lastFrequencySpecCommand_m.FringeSideband = fringeSideband;
    lastFrequencySpecCommand_m.OffsetSideband = offsetSideband;
    lastFrequencySpecCommand_m.loFrequency = loFrequency;
}

LO2Engine::LO2FreqSpec LO2Engine::findTuningSetup(
    FrequencySpecification& ftsSpec)
{
    LO2Engine::LO2FreqSpec lo2Spec;
    lo2Spec.loFrequency = ftsSpec.loFrequency;
    lo2Spec.applicationTime = ftsSpec.applicationTime;

    const double freqFactor(ftsSpec.loFrequency / CombLineSpacing);
    const int combNumber(static_cast< int >(freqFactor));

    if((combNumber < minCombLineNumber) || (combNumber > maxCombLineNumber))
    {
        // Reference signal is out of range
        std::ostringstream msg;
        msg << "Requested frequency ("
            << ftsSpec.loFrequency
            << ") not available (reference signal out of range)";
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        throw ex;
    }

    if(freqFactor - combNumber < 0.5)
    {
        lo2Spec.FringeSideband = NetSidebandMod::USB;
        ftsSpec.ftsFrequency = worldToRawCntlFreq((freqFactor - combNumber)
            * CombLineSpacing);
    }
    else
    {
        lo2Spec.FringeSideband = NetSidebandMod::LSB;
        ftsSpec.ftsFrequency = worldToRawCntlFreq((1 - freqFactor + combNumber)
            * CombLineSpacing);
    }

    if((ftsSpec.ftsFrequency > FTSDeviceData::FTSMaximumFreq)
    || (ftsSpec.ftsFrequency < FTSDeviceData::FTSMinumumFreq))
    {
        // FTS Frequency out of range
        std::ostringstream msg;
        msg << "Requested frequency ("
            << ftsSpec.ftsFrequency
            << ") not available (FTS signal out of range)";
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        throw ex;
    }

    return lo2Spec;
}
