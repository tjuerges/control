//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <LO2Impl.h>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <cstring>
#include <arpa/inet.h>
#include <loggingMACROS.h>
#include <controlAlarmSender.h>
#include <TETimeUtil.h>
#include <FTSDeviceData.h>
#include <TMCDBAccessIFC.h>
#include <FLOOGAlarmEnums.h>
#include <LO2Engine.h>
#include <ControlDataInterfacesC.h>
#include <almaEnumerations_IFC.h>
#include <Guard_T.h>


/// -----------------------
/// LO2 Constructor
/// -----------------------
LO2Impl::LO2Impl(const ACE_CString& name, maci::ContainerServices* cs):
    LO2Base(name, cs),
    MonitorHelper(),
    updateThread_p(0),
    updateThreadCycleTime_m(10000000),
    alarmSender(0)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

/// -----------------------
/// LO2 Destructor
/// -----------------------
LO2Impl::~LO2Impl()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

///-----------------------
/// External Interface
///-----------------------
void LO2Impl::SetUserFrequency(CORBA::Double loFrequency,
    NetSidebandMod::NetSideband offsetSideband,
    ACS::Time applicationTime)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        if(applicationTime == 0)
        {
            setUserFrequency(loFrequency, offsetSideband);
        }
        else
        {
            setUserFrequency(loFrequency, offsetSideband, applicationTime);
        }
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        throw LO2Exceptions::FreqNotAvailableExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getFreqNotAvailableEx();
    }
    // Test for inactive exceptions and camberrorex
}

double LO2Impl::GetNominalFrequency()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        return LO2Engine::getNominalFrequency();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        // This really should never happen unless the LO2 is inactive
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
}

double LO2Impl::GetNominalFTSFrequency()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        return rawToWorldFreq(getNominalFTSFrequency());
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        throw ControlExceptions::InvalidRequestExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getInvalidRequestEx();
    }
}

double LO2Impl::GetOffsetFrequency()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        return getOffsetFrequency();
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        throw ControlExceptions::InvalidRequestExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getInvalidRequestEx();
    }
}

double LO2Impl::GetOffsetFTSFrequency()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        return rawToWorldFreq(getOffsetFTSFrequency());
    }
    catch(const ControlExceptions::InvalidRequestExImpl& ex)
    {
        throw ControlExceptions::InvalidRequestExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getInvalidRequestEx();
    }
}

double LO2Impl::GetActualFrequency()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp(0ULL);
    FTSEngine::FrequencySpecification ftsSpec;
    LO2Impl::LO2FreqSpec loSpec;
    try
    {
        ftsSpec.ftsFrequency = worldToRawCntlFreq(getFreq(timestamp));
        loSpec.loFrequency = getLo2Frequency(timestamp);

        if(getHighLowSwitch(timestamp))
        {
            loSpec.FringeSideband = NetSidebandMod::USB;
        }
        else
        {
            loSpec.FringeSideband = NetSidebandMod::LSB;
        }
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }

    return calculateLO2Frequency(loSpec, ftsSpec);
}

void LO2Impl::setFrequencyOffsettingMode(
    const Control::LOOffsettingMode mode)
{
    LO2Engine::setFrequencyOffsetting(mode);
}

Control::LOOffsettingMode LO2Impl::getFrequencyOffsettingMode()
{
    return LO2Engine::getFrequencyOffsettingMode();
}

CORBA::Long LO2Impl::GetFrequencyOffsetFactor()
{
    return LO2Engine::getFrequencyOffsetFactor();
}

void LO2Impl::SetFrequencyOffsetFactor(const CORBA::Long freqOffsetFactor)
{
    LO2Engine::setFrequencyOffsetFactor(freqOffsetFactor);
}

double LO2Impl::GetPropagationDelay()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return LO2Engine::getPropagationDelay();
}

void LO2Impl::SetPropagationDelay(CORBA::Double propagationDelay)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    LO2Engine::setPropagationDelay(propagationDelay);
}

void LO2Impl::EnableFringeTracking(bool enableTracking)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    LO2Engine::enableFringeTracking(enableTracking);
}

CORBA::Boolean LO2Impl::FringeTrackingEnabled()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return LO2Engine::fringeTrackingEnabled();
}

// ================ Component Lifecycle ======================
void LO2Impl::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    initialiseAlarmSystem();

    const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    try
    {
        LO2Base::initialize();
        MonitorHelper::initialize(compName.in(), getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }

    // Prepare the threads for use
    std::string threadName(compName.in());
    threadName += "UpdateThread";
    updateThread_p = getContainerServices()->getThreadManager()-> create<
        UpdateThread, LO2Impl* const >(threadName.c_str(), this);
    updateThread_p->setSleepTime(updateThreadCycleTime_m);
}

void LO2Impl::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    /// Make certain that the device is stopped.
    try
    {
        Control::HardwareDeviceImpl::hwStop();
    }
    catch(...)
    {
        LOG_TO_OPERATOR(LM_WARNING, "Caught an exception during transition to "
            "the hardware stop state.  Continuing anyway.");
    }

    /// Shutdown the threads, the threads were suspended by the
    /// baseclass call to hwStopAction so we just need to terminate them
    /// here.
    if(updateThread_p != 0)
    {
        updateThread_p->exit();
        if(updateThread_p->isSuspended())
        {
            updateThread_p->resume();
        }

        getContainerServices()->getThreadManager()->destroy(updateThread_p);
        updateThread_p = 0;
    }

    try
    {
        MonitorHelper::cleanUp();
        LO2Base::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        cleanUpAlarmSystem();

        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getLifeCycleEx();
    }

    cleanUpAlarmSystem();
}

// ================= Overloaded control points ===============
void LO2Impl::setCntlRangeHigh()
{
    /* Changing the setting of the High/Low flag is actually changing
     the frequency of the LO2.  In order for these to not contradict
     change the setting by changing the frequency */
    ACS_TRACE(__PRETTY_FUNCTION__);

    LO2Impl::LO2FreqSpec loSpec;
    FTSEngine::FrequencySpecification ftsSpec;
    double newLo2Frequency(0.0);
    ACS::Time timestamp = ::getTimeStamp();
    try
    {
        loSpec.loFrequency = getLo2Frequency(timestamp);

        if(loSpec.FringeSideband == NetSidebandMod::USB)
        {
            // Nothing to do here, just return
            return;
        }

        loSpec.FringeSideband = NetSidebandMod::USB;
        ftsSpec.ftsFrequency = worldToRawCntlFreq(getFreq(timestamp));
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }

    newLo2Frequency = calculateLO2Frequency(loSpec, ftsSpec);
    setUserFrequency(newLo2Frequency, getOffsetSideband());
}

void LO2Impl::setCntlRangeLow()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    LO2Impl::LO2FreqSpec loSpec;
    FTSEngine::FrequencySpecification ftsSpec;
    double newLo2Frequency(0.0);
    ACS::Time timestamp(::getTimeStamp());
    try
    {
        loSpec.loFrequency = getLo2Frequency(timestamp);

        if(loSpec.FringeSideband == NetSidebandMod::LSB)
        {
            // Nothing to do here, just return
            return;
        }

        loSpec.FringeSideband = NetSidebandMod::LSB;
        ftsSpec.ftsFrequency = worldToRawCntlFreq(getFreq(timestamp));
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }

    newLo2Frequency = calculateLO2Frequency(loSpec, ftsSpec);
    setUserFrequency(newLo2Frequency, getOffsetSideband());
}

void LO2Impl::setCntlTune()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    queueLockCommand(false, 0);
}

void LO2Impl::setCntlLock()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    queueLockCommand(true, 0);
}

void LO2Impl::setCntlAftOff()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    lo2AFTCommandedOff_m = true;
    LO2Base::setCntlAftOff();
}

void LO2Impl::setCntlAftOn()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    lo2AFTCommandedOff_m = false;
    LO2Base::setCntlAftOn();
}

// ================= Hardware Lifecycle ======================
void LO2Impl::hwConfigureAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    LO2Base::hwConfigureAction();
}

void LO2Impl::hwInitializeAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    LO2Base::hwInitializeAction();

    // Set the Frequency Offset Factor.
    // Load and set the WalshFunction and the LO offset.
    try
    {
        maci::SmartPtr< TMCDB::Access > accessRef;
        accessRef = getContainerServices()->
            getDefaultComponentSmartPtr< TMCDB::Access >(
                "IDL:alma/TMCDB/Access:1.0");

        const std::string antennaName(
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName());

        try
        {
            const CORBA::Long loOffsetIndex(
                accessRef->getAntennaLOOffsettingIndex(antennaName.c_str()));
            LO2Engine::setFrequencyOffsetFactor(loOffsetIndex);
        }
        catch(const TmcdbErrType::TmcdbErrorEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access failed somehow.  Cannot "
                "set the LO offset!");
        }
        catch(const TmcdbErrType::TmcdbNoSuchRowEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access did not return an LO "
                "offset index.  Cannot set the LO offset!");
        }
        catch(...)
        {
            LOG_TO_OPERATOR(LM_ERROR, "An unexpected error happened during "
                "the retrieval of the LO offset index from the TMCDB or the "
                "setting of it.  Cannot set the LO offset!");
        }
    }
    catch(const maciErrType::NoDefaultComponentExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access component is not available. "
            "  Cannot set the LO offset!");
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Got no reference for the TMCDB Access "
            "component.  Cannot set the LO offset!");
    }

    alarmSender->terminateAllAlarms();

    MonitorHelper::resume();
    updateThread_p->suspend();
    nextStatusMonitorTime_m = 0;
}

void LO2Impl::hwOperationalAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    LO2Base::hwOperationalAction();

    LO2Engine::enableFringeTracking(false);

    std::vector< float >phase(4, 0.0);
    setCntlPhaseVals(phase);
    setCntlAftOn();
    LO2Engine::setUserFrequency(12.035E9, NetSidebandMod::USB);
    updateThreadAction();
    updateThread_p->resume();

    {
        ACE_Write_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
        nextStatusMonitorTime_m = ::getTimeStamp();
        nextStatusMonitorTime_m -= nextStatusMonitorTime_m
            % TETimeUtil::TE_PERIOD_ACS;
        nextStatusMonitorTime_m += CommandLookAhead + (TETimeUtil::TE_PERIOD_ACS
            / 2);
    }

    LO2Engine::reset();
    updateThread_p->resume();
}

void LO2Impl::hwStopAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Suspend the threads
    if(updateThread_p != 0)
    {
        if(updateThread_p->isSuspended() == false)
        {
            updateThread_p->suspend();
        }
    }

    // Flush all commands and monitors
    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushTime(0ULL);
    flushNode(0, &flushTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        LOG_TO_OPERATOR(LM_WARNING, "Communication failure flushing commands "
            "to device");
    }
    else
    {
        std::ostringstream msg;
        msg << "LO2 commands flushed at: "
            << flushTime;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    MonitorHelper::suspend();
    LO2Base::hwStopAction();

    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }
}

// ---------------- Delay Receiver Methods --------------------
void LO2Impl::newDelayEvent(const Control::AntennaDelayEvent& event)
{
    DelayCalculator::setDelays(event.delayTables);
}

// ================ Monitoring Dispatch Method ================
void LO2Impl::processRequestResponse(const AMBRequestStruct& response)
{

    if(response.Status == AMBERR_FLUSHED)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
        return;
    }

    if(response.Status != AMBERR_NOERR)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, true);
        return;
    }

    setErrorState(::FLOOG::ErrCommunicationFailure, false);

    if(response.RCA == getControlRCACntlFreq())
    {
        // Response from setting the Frequency
        processFTSFrequencyCommand(response);
        return;
    }

    if(response.RCA == getMonitorRCAFreq())
    {
        // Response from a monitor of the Frequency
        processFTSFrequencyMonitor(response);
        return;
    }

    if(response.RCA == getMonitorRCAFtsStatus())
    {
        // Response from the status monitor
        processFTSStatusMonitor(response);
        return;
    }

    if(response.RCA == getControlRCACntlLo2Frequency())
    {
        // Response from the LO2 Frequency command
        processLO2FrequencyCommand(response);
        return;
    }

    if(response.RCA == getMonitorRCALo2Frequency())
    {
        // Response from the LO2 Frequency command
        processLO2FrequencyMonitor(response);
        return;
    }

    if(response.RCA == getMonitorRCAModuleStatus())
    {
        // Response from the LO2 Frequency command
        processLO2StatusMonitor(response);
        return;
    }

    if(response.RCA == getControlRCACntlLock())
    {
        // Response from the LO2 Frequency command
        alarmSender->inhibitAlarms(1000000);
        processLO2LockCommand(response);
        return;
    }

    // We don't have to do anything with these cases
    if(response.RCA == getControlRCACntlRangeHigh())
    {
        alarmSender->inhibitAlarms(1000000);
        return;
    }

    // Unrecognized RCA Case
    std::ostringstream msg;
    msg << "Unrecognized RCA detected in process respons (RCA=0x"
        << std::hex
        << response.RCA;
    LOG_TO_DEVELOPER(LM_WARNING, msg.str());
}

// Processing Methods
void LO2Impl::processFTSStatusMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    bool resetRequired = false;

    if(response.Data[0] & 0x20)
    {
        // Inverted Clock
        if(response.Data[0] & 0x05)
        {
            setErrorState(::FLOOG::ErrClockError, true);
            resetRequired = true;
        }
        else
        {
            setErrorState(::FLOOG::ErrClockError, false);
        }

    }
    else
    {
        // Normal Clock
        if(response.Data[0] & 0x18)
        {
            setErrorState(::FLOOG::ErrClockError, true);
            resetRequired = true;
        }
        else
        {
            setErrorState(::FLOOG::ErrClockError, false);
        }
    }

    if(resetRequired)
    {
        try
        {
            setResetFtsStatus();
            setErrorState(::FLOOG::ErrCommunicationFailure, false);
        }
        catch(ControlExceptions::CAMBErrorExImpl& ex)
        {
            ex.log();
            setErrorState(::FLOOG::ErrCommunicationFailure, true);
        }
        catch(ControlExceptions::INACTErrorExImpl& ex)
        {
            // Very strange situation... bet it never happens
            ex.log();
        }
    }
}

void LO2Impl::processFTSFrequencyCommand(
    const MonitorHelper::AMBRequestStruct& response)
{
    setErrorState(::FLOOG::ErrFTSPhaseError, response.Timestamp
        - response.TargetTime > TETimeUtil::TE_PERIOD_ACS / 2);
    std::memcpy(lastFTSFrequencyCommand_m, response.Data, response.DataLength);
}

void LO2Impl::processFTSFrequencyMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    bool errorState((response.DataLength != 6) || std::memcmp(
        response.Data, lastFTSFrequencyCommand_m, response.DataLength));

    setErrorState(::FLOOG::ErrFTSFrequencyError, errorState);
}

void LO2Impl::processLO2FrequencyCommand(
    const MonitorHelper::AMBRequestStruct& response)
{
    setErrorState(WrongLO2Frequency, (response.Timestamp
        - response.TargetTime) > (TETimeUtil::TE_PERIOD_ACS / 2));
    std::memcpy(lastLO2FrequencyCommand_m, response.Data, response.DataLength);
}

void LO2Impl::processLO2FrequencyMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    const bool errorState((response.DataLength != 2) || std::memcmp(
        response.Data, lastLO2FrequencyCommand_m, response.DataLength));

    setErrorState(WrongLO2Frequency, errorState);
}

void LO2Impl::processLO2LockCommand(
    const MonitorHelper::AMBRequestStruct& response)
{
    // If bit 0 is set to zero then we have specified that we are in
    // the lock state
    lo2TuneStateCommanded_m = response.Data[0] & 0x01;
}

void LO2Impl::processLO2StatusMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    // Check the Tune High / Tune Low state
    // This is bit 1 of the status byte.
    // Tune Low = unset, Tune High is set
    switch(LO2Engine::getFringeSideband(response.TargetTime))
    {
        case NetSidebandMod::USB:
            if(response.Data[0] & 0x02)
            {
                setErrorState(WrongSideband, false);
            }
            else
            {
                setErrorState(WrongSideband, true);
            }
        break;

        case NetSidebandMod::LSB:
            if(response.Data[0] & 0x02)
            {
                setErrorState(WrongSideband, true);
            }
            else
            {
                setErrorState(WrongSideband, false);
            }
        break;

        default:
            // This really should never happen
            LOG_TO_DEVELOPER(LM_ERROR, "LO2 Tuning status is unrecognized");
    }

    // bit 4 AutoFineTune (1 -> Off)
    if(lo2AFTCommandedOff_m != static_cast< bool  >(response.Data[0] & 0x10))
    {
        setErrorState(AFTStateIncorrect, true);
    }
    else
    {
        setErrorState(AFTStateIncorrect, false);
    }

    // bit 0 Tune/Lock (lock = 0)
    if(lo2TuneStateCommanded_m != (response.Data[0] & 0x01))
    {
        setErrorState(WrongTuneState, true);
    }
    else
    {
        setErrorState(WrongTuneState, false);
    }

    // Only Check the lock bits if we are commanded to be locked
    if(lo2TuneStateCommanded_m == false)
    {
        // Check to see if we are currently unlocked
        // bit 2 PLL Lock Indicator (0-> Not Locked)
        if((response.Data[0] & 0x04) && !(response.Data[0] & 0x08))
        {
            // We are currently locked just fine clear the alarm
            setErrorState(LO2Unlocked, false);
        }
        else
        {
            // We are not locked ok
            setErrorState(LO2Unlocked, true);
            if(response.Data[0] & 0x04)
            {
                // Only the latched bit is unlocked, clear it
                try
                {
                    setResetLockStatus();
                    setErrorState(::FLOOG::ErrCommunicationFailure,
                        false);
                }
                catch(const ControlExceptions::CAMBErrorExImpl& ex)
                {
                    // Set Communication Error
                    setErrorState(::FLOOG::ErrCommunicationFailure, true);
                }
                catch(ControlExceptions::INACTErrorExImpl& ex)
                {
                    // Bet this never happens
                    ex.log();
                }
            }
        }
    }
}

// ------------------ FTSEngine Support Methods -----------------------
void LO2Impl::queueFrequencyCommand(unsigned long long& fixedFtsFrequency,
    ACS::Time& epoch)
{
    MonitorHelper::AMBRequestStruct* cmdReq(MonitorHelper::getRequestStruct());
    MonitorHelper::AMBRequestStruct* monReq(MonitorHelper::getRequestStruct());

    // Populate the Command Request
    cmdReq->RCA = getControlRCACntlFreq();
    cmdReq->TargetTime = epoch;
    cmdReq->DataLength = 6;
#if BYTE_ORDER != BIG_ENDIAN
    {
        // Swap the bytes
        char* freqPtr = reinterpret_cast< char* >(&fixedFtsFrequency);
        cmdReq->Data[0] = freqPtr[5];
        cmdReq->Data[1] = freqPtr[4];
        cmdReq->Data[2] = freqPtr[3];
        cmdReq->Data[3] = freqPtr[2];
        cmdReq->Data[4] = freqPtr[1];
        cmdReq->Data[5] = freqPtr[0];
    }
#else
    {
        // Straight Copy
        std::memcpy(cmdReq->Data, &fixedFtsFrequency, 6);
    }
#endif

    // populate the Monitor request
    monReq->RCA = getMonitorRCAFreq();
    monReq->TargetTime = epoch + (TETimeUtil::TE_PERIOD_ACS / 2);

    // Queue these to be executed
    AmbDeviceInt::commandTE(cmdReq->TargetTime, cmdReq->RCA,
        cmdReq->DataLength, cmdReq->Data, cmdReq->SynchLock,
        &cmdReq->Timestamp, &cmdReq->Status);

    AmbDeviceInt::monitorTE(monReq->TargetTime, monReq->RCA,
        monReq->DataLength, monReq->Data, monReq->SynchLock,
        &monReq->Timestamp, &monReq->Status);

    // Add them to the queue
    MonitorHelper::queueRequest(cmdReq);
    MonitorHelper::queueRequest(monReq);
}

// ------------------ LO2 Engine Support Methods -----------------------
void LO2Impl::queueLO2FrequencyCommand(double frequency,
    ACS::Time applicationTime)
{
    MonitorHelper::AMBRequestStruct* cmdReq(MonitorHelper::getRequestStruct());
    MonitorHelper::AMBRequestStruct* monReq(MonitorHelper::getRequestStruct());

    // Populate the Command Request
    cmdReq->RCA = getControlRCACntlLo2Frequency();
    cmdReq->TargetTime = applicationTime;
    cmdReq->DataLength = 2;

    unsigned short freqData = htons(worldToRawCntlLo2Frequency(frequency));
    std::memcpy(cmdReq->Data, &freqData, 2);

    // populate the Monitor request
    monReq->RCA = getMonitorRCALo2Frequency();
    monReq->TargetTime = applicationTime + (TETimeUtil::TE_PERIOD_ACS / 2);

    // Queue these to be executed
    AmbDeviceInt::commandTE(cmdReq->TargetTime, cmdReq->RCA,
        cmdReq->DataLength, cmdReq->Data, cmdReq->SynchLock,
        &cmdReq->Timestamp, &cmdReq->Status);

    AmbDeviceInt::monitorTE(monReq->TargetTime, monReq->RCA,
        monReq->DataLength, monReq->Data, monReq->SynchLock,
        &monReq->Timestamp, &monReq->Status);

    // Add them to the queue
    MonitorHelper::queueRequest(cmdReq);
    MonitorHelper::queueRequest(monReq);
}

void LO2Impl::queueSidebandCommand(NetSidebandMod::NetSideband fringeSideband,
    NetSidebandMod::NetSideband offsetSideband,
    ACS::Time applicationTime)
{
    MonitorHelper::AMBRequestStruct* cmdReq(MonitorHelper::getRequestStruct());

    /**
    There are two sets of sidebands that we need to worry about here.
    One is which side of the YIG the FTS will locked on, that is what is
    what is needed by the LO2 hardware.  And which way the fringe rate
    should be added.

    The other is what is used in the calculation of the LO Offsetting.
    */

    // Tell the FTS that the sideband will be changing
    if(offsetSideband == NetSidebandMod::USB)
    {
        LOG_TO_OPERATOR(LM_DEBUG, "LO2 (queueing) for offsetting on USB");
    }
    else
    {
        LOG_TO_OPERATOR(LM_DEBUG, "LO2 (queueing) for offsetting on LSB");
    }

    LO2Engine::setSideband(fringeSideband, offsetSideband, applicationTime);

    // Populate the Command Request
    cmdReq->RCA = getControlRCACntlRangeHigh();
    cmdReq->TargetTime = applicationTime;
    cmdReq->DataLength = 1;

    if(fringeSideband == NetSidebandMod::USB)
    {
        cmdReq->Data[0] = 1;
    }
    else
    {
        cmdReq->Data[0] = 0;
    }

    // Queue these to be executed
    AmbDeviceInt::commandTE(cmdReq->TargetTime, cmdReq->RCA,
        cmdReq->DataLength, cmdReq->Data, cmdReq->SynchLock,
        &cmdReq->Timestamp, &cmdReq->Status);

    // Add them to the queue
    MonitorHelper::queueRequest(cmdReq);
}

void LO2Impl::queueLockCommand(bool lockState, ACS::Time applicationTime)
{
    // No need to associate a monitor with this method, take care of it
    // in the Error Helper class
    MonitorHelper::AMBRequestStruct* cmdReq(MonitorHelper::getRequestStruct());

    // Populate the Command Request
    cmdReq->RCA = getControlRCACntlLock();
    cmdReq->TargetTime = applicationTime;
    cmdReq->DataLength = 1;

    if(lockState)
    {
        cmdReq->Data[0] = 0;
    }
    else
    {
        cmdReq->Data[0] = 1;
    }

    if(cmdReq->TargetTime != 0)
    {
        // Queue these to be executed
        AmbDeviceInt::commandTE(cmdReq->TargetTime, cmdReq->RCA,
            cmdReq->DataLength, cmdReq->Data, cmdReq->SynchLock,
            &cmdReq->Timestamp, &cmdReq->Status);
    }
    else
    {
        // Queue these to be executed ASAP
        AmbDeviceInt::command(cmdReq->RCA, cmdReq->DataLength, cmdReq->Data,
            cmdReq->SynchLock, &cmdReq->Timestamp, &cmdReq->Status);
    }

    // Add them to the queue
    MonitorHelper::queueRequest(cmdReq);
}

// ============= FTS Support Methods ================
const ACS::Time LO2Impl::getUpdateThreadCycleTime()
{
    return updateThreadCycleTime_m;
}

void LO2Impl::sendPhaseValuesCommand(std::vector< float >& phaseVals)
{
    LOG_TO_DEVELOPER(LM_ERROR, "sendPhaseValues should never be called on LO2");
}

void LO2Impl::queueFTSStatusMonitor()
{
    MonitorHelper::AMBRequestStruct* monReq(MonitorHelper::getRequestStruct());

    // Populate the Command Request
    monReq->RCA = getMonitorRCAFtsStatus();
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
        monReq->TargetTime = nextStatusMonitorTime_m;
    }

    // Queue this command to be executed
    AmbDeviceInt::monitorTE(monReq->TargetTime, monReq->RCA,
        monReq->DataLength, monReq->Data, monReq->SynchLock,
        &monReq->Timestamp, &monReq->Status);

    MonitorHelper::queueRequest(monReq);
}

void LO2Impl::queueLO2StatusMonitor()
{
    MonitorHelper::AMBRequestStruct* monReq(MonitorHelper::getRequestStruct());

    // Populate the Command Request
    monReq->RCA = getMonitorRCAModuleStatus();
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
        monReq->TargetTime = nextStatusMonitorTime_m;
    }

    // Queue this command to be executed
    AmbDeviceInt::monitorTE(monReq->TargetTime, monReq->RCA,
        monReq->DataLength, monReq->Data, monReq->SynchLock,
        &monReq->Timestamp, &monReq->Status);

    MonitorHelper::queueRequest(monReq);

    ACE_Write_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
    nextStatusMonitorTime_m += TETimeUtil::TE_PERIOD_ACS;
}

void LO2Impl::resetFTS()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    setRestart();
}

void LO2Impl::flushFrequencyCommands(ACS::Time& flushTime)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // This method should flush all commands at or after flushTime that
    // have to do with monitorin or setting the frequency.

    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushExecutionTime(0ULL);

    // Flush all commands
    AmbDeviceInt::flushRCA(flushTime, getMonitorRCAFreq(),
        &flushExecutionTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
    }

    AmbDeviceInt::flushRCA(flushTime, getControlRCACntlFreq(),
        &flushExecutionTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
    }

    // Should flush the LO2 Frequency Commands as well here
    AmbDeviceInt::flushRCA(flushTime, getControlRCACntlLo2Frequency(),
        &flushExecutionTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
    }

    AmbDeviceInt::flushRCA(flushTime, getMonitorRCALo2Frequency(),
        &flushExecutionTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        setErrorState(::FLOOG::ErrCommunicationFailure, false);
    }

    std::ostringstream msg;
    msg << "All frequency commands and monitors flushed at: "
        << flushTime;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

// ------------------ Update Thread Action ----------------------

void LO2Impl::updateThreadAction()
{
    FTSEngine::updateThreadAction();

    // Again check that the nextStatusMonitorTime has been initialised
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextFreqCmdTimeMutex_m);
        if(nextStatusMonitorTime_m == 0)
        {
            return;
        }
    }

    const ACS::Time updateEndTime(::getTimeStamp() +
        (getUpdateThreadCycleTime() * 2));

    ACS::Time tmp(0ULL);
    {
        ACE_Read_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
        tmp = nextStatusMonitorTime_m;
    }
    while(tmp <= updateEndTime)
    {
        if((tmp % (21 * TETimeUtil::TE_PERIOD_ACS)) == 240000)
        {
            queueFTSStatusMonitor();
        }

        queueLO2StatusMonitor(); // Here nextStatusMonitor gets updated
        {
            ACE_Read_Guard< ACE_RW_Mutex > guard(nextStatusMonitorTimeMutex_m);
            tmp = nextStatusMonitorTime_m;
        }
    }
}

// ===========================================================
LO2Impl::UpdateThread::UpdateThread(const ACE_CString& name,
    const LO2Impl* lo2Device):
    ACS::Thread(name),
    lo2Device_p(const_cast< LO2Impl* >(lo2Device))
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void LO2Impl::UpdateThread::runLoop()
{
    lo2Device_p->updateThreadAction();
}

void LO2Impl::linearInterpolation(double value, double startRange1,
    double endRange1, double startRange2, double endRange2, double& result) const
{
    if((value >= startRange1) && (value < endRange1))
    {
        result = (value - startRange1) / (endRange1 - startRange1) *
            (endRange2 - startRange2) + startRange2;
    }
}

double LO2Impl::rawToWorldDetectedIfPower(const unsigned short raw) const
{
    // Conversion as specified in ICD document: this implementation
    // takes the table of values and interpolates linearly in between
    // two known values.

    // Transform the raw data into voltage:
    // 0x0000 to 0x07FF -> 0 to 10 Volts
    const double voltage((raw / 2047.0) * 10.0);

    // Transfrom into power [dBm]
    double power(0.0);

    // Here the table of values from the ICD document:
    linearInterpolation(voltage, 0.0, 0.265, -90, -34, power);
    linearInterpolation(voltage, 0.265, 0.27, -34, -30, power);
    linearInterpolation(voltage, 0.27, 0.28, -30, -26, power);
    linearInterpolation(voltage, 0.28, 0.30, -26, -22, power);
    linearInterpolation(voltage, 0.30, 0.36, -22, -18, power);
    linearInterpolation(voltage, 0.36, 0.46, -18, -14, power);
    linearInterpolation(voltage, 0.46, 0.58, -14, -10, power);
    linearInterpolation(voltage, 0.58, 0.67, -10, -6, power);
    linearInterpolation(voltage, 0.67, 0.77, -6, -2, power);
    linearInterpolation(voltage, 0.77, 1.5, -2, -0.1, power);

    return power;
}
double LO2Impl::rawToWorldDetectedRfPower(short int raw) const
{
    const double value(raw); // this put in 0 the 12bit (negative)
    return (value * 20.0e-3 / -2048.0);
}

double LO2Impl::rawToWorldDetectedFtsPower(const unsigned short raw) const
{
    // Conversion as specified in ICD document: this implementation
    // takes the table of values and interpolates linearly in between
    // two known values.

    // Transform the raw data into voltage:
    // 0x0000 to 0x07FF -> 0 to 10 Volts
    const double voltage((raw / 2047.0) * 10.0);

    // Transfrom into power [dBm]
    double power(0.0);

    // Here the table of values from the ICD document:
    linearInterpolation(voltage, 0.0, 0.265, -90, -34, power);
    linearInterpolation(voltage, 0.265, 0.270, -34, -30, power);
    linearInterpolation(voltage, 0.27, 0.275, -30, -20, power);
    linearInterpolation(voltage, 0.275, 0.305, -20, -18, power);
    linearInterpolation(voltage, 0.305, 0.342, -18, -16, power);
    linearInterpolation(voltage, 0.342, 0.398, -16, -14, power);
    linearInterpolation(voltage, 0.398, 0.474, -14, -12, power);
    linearInterpolation(voltage, 0.474, 0.535, -12, -10, power);
    linearInterpolation(voltage, 0.535, 0.580, -10, -8, power);
    linearInterpolation(voltage, 0.580, 0.670, -8, -6, power);
    linearInterpolation(voltage, 0.670, 0.770, -6, -2, power);
    linearInterpolation(voltage, 0.770, 1.500, -2, -0.1, power);

    return power;
}

double LO2Impl::rawToWorldFreq(long long unsigned int freq) const
{
    double returnFreq((FTSDeviceData::FTSMask & freq)
        * static_cast< double  >(FTSDeviceData::HwClockScale));
    returnFreq /= FTSDeviceData::FTSMask + 1;

    return returnFreq;
}

long long unsigned int LO2Impl::worldToRawCntlFreq(double freq) const
{
    double freqWord(freq * (FTSDeviceData::FTSMask + 1));
    freqWord /= FTSDeviceData::HwClockScale;

    return static_cast< unsigned long long >(freqWord);
}

// Convert the raw value of FTS_STATUS to a world value.
std::vector< bool > LO2Impl::rawToWorldFtsStatus(const unsigned char raw) const
{
    std::vector< bool > ret(6);
    ret[0] = raw & 0x01;
    ret[1] = raw & 0x02;
    ret[2] = raw & 0x04;
    ret[3] = raw & 0x08;
    ret[4] = raw & 0x10;
    ret[5] = raw & 0x20;

    return ret;
}

void LO2Impl::handleActivateAlarm(int code)
{
    LO2Base::setError(alarmSender->createErrorMessage());
}

void LO2Impl::handleDeactivateAlarm(int code)
{
    if(alarmSender->isAlarmSet() == true)
    {
        LO2Base::setError(alarmSender->createErrorMessage());
    }
    else
    {
        LO2Base::clearError();
    }
}

void LO2Impl::setErrorState(int errCode, bool state)
{
    if(alarmSender != 0)
    {
        if(state == true)
        {
            alarmSender->activateAlarm(errCode);
        }
        else
        {
            alarmSender->deactivateAlarm(errCode);
        }
    }
}

void LO2Impl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSender.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    // Ugly, ugly, ugly.  I had to copy the following lines from the FLOOG to
    // here because some FLOOG alarms are reused and therefore have to be
    // initialised here.
    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrNoValidDelay;
        alarmInfo.alarmTerminateCount = 3;
        alarmInfo.alarmDescription = "No valid delays found for requested time";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrCommunicationFailure;
        alarmInfo.alarmTerminateCount = 5;
        alarmInfo.alarmDescription = "Communication error on ALMA Monitor Bus";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrFTSFrequencyError;
        alarmInfo.alarmTerminateCount = 4;
        alarmInfo.alarmDescription
            = "Fine Tuning Synthesizer at wrong frequnecy";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrFTSPhaseLost;
        alarmInfo.alarmTerminateCount = 1;
        alarmInfo.alarmDescription = "Fine Tuning Synthesizer phase unknown";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrFTSPhaseError;
        alarmInfo.alarmTerminateCount = 1;
        alarmInfo.alarmDescription = "Fine Tuning Synthesizer phase incorrect";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrRFPowerError;
        alarmInfo.alarmTerminateCount = 2;
        alarmInfo.alarmDescription = "Fine Tuning Synthesizer RF Power Low";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = ::FLOOG::ErrClockError;
        alarmInfo.alarmTerminateCount = 5;
        alarmInfo.alarmDescription
            = "Glitches detected in Fine Tuning Synthesizer clock";
        alarmInformation.push_back(alarmInfo);
    }
    // End of FLOOG alarms.

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = WrongLO2Frequency;
        alarmInfo.alarmDescription =  "LO2 is locked on wrong comb line";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = WrongTuneState;
        alarmInfo.alarmDescription =  "Communication error on ALMA Monitor Bus";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = WrongSideband;
        alarmInfo.alarmDescription = "LO2 is attempting to lock on the wrong "
            "sideband";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = LO2Unlocked;
        alarmInfo.alarmTerminateCount = 5;
        alarmInfo.alarmDescription =  "LO2 is out of lock";
        alarmInformation.push_back(alarmInfo);
    }

    {
        Control::AlarmInformation alarmInfo;
        alarmInfo.alarmCode = AFTStateIncorrect;
        alarmInfo.alarmTerminateCount = 2;
        alarmInfo.alarmDescription =  "Auto Fine Tune state Incorrect";
        alarmInformation.push_back(alarmInfo);
    }

    alarmSender->initializeAlarms(
        Control::CharacteristicControlDeviceImpl::getAlarmFamilyName("LO2"),
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName(),
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void LO2Impl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LO2Impl)
