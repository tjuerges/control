/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 */

package alma.Control.device.gui.LO2.presentationModels;

import alma.Control.device.gui.LO2.IdlControlPoints;
import alma.Control.device.gui.LO2.IdlMonitorPoints;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IdlControlPointPresentationModel;
import alma.Control.device.gui.common.IdlMonitorPointPresentationModel;

import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Build IDL Presentation models specific to this device
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @since   ALMA 5.1.1
 * 
 */
public class LO2IdlPresentationModelBuilder {
    
    private DevicePM containingDevicePM;
    public HashMap<IdlControlPoints, IdlControlPointPresentationModel> controlPointPMs = null;
    public HashMap<IdlMonitorPoints, IdlMonitorPointPresentationModel> monitorPointPMs = null;
    Logger logger;

    public LO2IdlPresentationModelBuilder(Logger logger, DevicePM containingDevicePM) {
    	this.logger = logger;
    	this.containingDevicePM = containingDevicePM;
    	
    	controlPointPMs = new HashMap<IdlControlPoints, IdlControlPointPresentationModel>();
    	monitorPointPMs = new HashMap<IdlMonitorPoints, IdlMonitorPointPresentationModel>();
    }

    protected void createControlPointPresentationModels() {
        controlPointPMs.put(
            IdlControlPoints.FrequencyOffsetFactor,
           	new FrequencyOffsetFactorControlPointPresentationModel(logger, containingDevicePM, IdlControlPoints.FrequencyOffsetFactor, int.class) 
        );
        
        controlPointPMs.put(
           	IdlControlPoints.FringeTracking,
           	new FringeTrackingControlPointPresentationModel(logger, containingDevicePM, IdlControlPoints.FringeTracking, boolean.class) 
        );
               
        controlPointPMs.put(
            IdlControlPoints.PropagationDelay,
            new PropagationDelayControlPointPresentationModel(logger, containingDevicePM, IdlControlPoints.PropagationDelay, double.class) 
        );
                
        controlPointPMs.put(
            IdlControlPoints.UserFrequency,
            new UserFrequencyControlPointPresentationModel(logger, containingDevicePM, IdlControlPoints.UserFrequency, double.class) 
        );
    }

    protected void createMonitorPointPresentationModels() {
            monitorPointPMs.put(
                IdlMonitorPoints.FringeTracking,
                new FringeTrackingMonitorPointPresentationModel(logger, containingDevicePM, IdlMonitorPoints.FringeTracking)
            );
            
            monitorPointPMs.put(
                IdlMonitorPoints.FTSOnUpperSideband,
                new FTSOnUpperSidebandMonitorPointPresentationModel(logger, containingDevicePM, IdlMonitorPoints.FTSOnUpperSideband)
            );
            
            monitorPointPMs.put(
                IdlMonitorPoints.FrequencyOffsetFactor,
                new FrequencyOffsetFactorMonitorPointPresentationModel(logger, containingDevicePM, IdlMonitorPoints.FrequencyOffsetFactor)
            );
            
            monitorPointPMs.put(
                IdlMonitorPoints.NominalFrequency,
                new NominalFrequencyMonitorPointPresentationModel(logger, containingDevicePM, IdlMonitorPoints.NominalFrequency)
            );
                       
            monitorPointPMs.put(
                IdlMonitorPoints.PropagationDelay,
                new PropagationDelayMonitorPointPresentationModel(logger, containingDevicePM, IdlMonitorPoints.PropagationDelay)
            );
    }

}
//
// O_o
        
