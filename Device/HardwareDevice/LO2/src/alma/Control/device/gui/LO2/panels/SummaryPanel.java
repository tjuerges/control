 /* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LO2.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

import alma.Control.device.gui.LO2.IcdControlPoints;
import alma.Control.device.gui.LO2.IcdMonitorPoints;
import alma.Control.device.gui.LO2.presentationModels.DevicePM;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Summary data.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author Steve Harrington  sharring@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    public SummaryPanel(Logger logger, DevicePM dPM) {
    	super(logger, dPM);
    	buildPanel();
    }
    
    protected void buildPanel() {
        final String[][] labelGrid = {
        		{"PLL Lock", null, "Tune/Lock Switch"},
        		{"PLL Out of Lock", null, "High/Low Switch"},
        		{"User Frequency", null, "Side Band"}
        };

        this.setBorder(
                BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Summary"),
                    BorderFactory.createEmptyBorder(1,1,1,1)
                )
        );
        
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        
        c.ipadx = 5;
        c.ipady = 5;
        
        //Add label grid
        for (int i = 0; i < labelGrid.length; i++) { 
        	
        	c.gridy = i;
        	c.gridx = 1;
        
        	for (int j = 0; j < labelGrid[i].length; j++) {    
        		if (labelGrid[i][j] != null) {
        			add(new JLabel(labelGrid[i][j]), c);
        		}
        		c.gridx += 2;  //Leave space for monitor point
        	}	        
	    } 
        
        c.gridy = 0;
        c.gridx = 0;
        /* PLL Lock */
        add(new BooleanMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.PLL_LOCK_IND),
                Status.OFF,
                "PLL not locked",
                Status.GOOD,
                "PLL locked"),
        	c
        );  
       
        c.gridx = 2;
        /* Fringe Tracking */
        /* TODO Fix to match actual monitor point */
//        add(new BooleanMonitorPointWidget(
//                logger, 
//                devicePM.getMonitorPointPM(LO2MonitorPoints.FRINGE_TRACKING),
//                Status.OFF,
//                "Fringe tracking disabled",
//                Status.GOOD,
//                "Fringe tracking enabled"),
//        	c
//        );   

        c.gridx = 3;
        /* TODO Fix to match actual monitor and control points */
//        add(new BooleanToggleWidget(
//        		logger
//        		devicePM.getMonitorPointPM(LO2MonitorPoints.FRINGE_TRACKING)
//        		devicePM.getControlPointPM(LO2ControlPoints.SET_FRING_TRACKING),
//        		"Enable Fringe Tracking",
//        		"Disable Fring Tracking"),
//        	c
//        );
   
        c.gridx = 4;
        /* Tune/Lock Switch */
        /* TODO Fix to match actual control point */
//        add(new BooleanMonitorControlPointWidget(
//        		logger,
//        		devicePM.getMonitorPointPM(LO2MonitorPoints.TUNE_LOCK_SWITCH),
//        		devicePM.getControlPointPM(LO2ControlPoints.SET_LOCK),
//        		"Lock position",
//        		"Tune position"),
//        	c
//        );
        
        c.gridy = 1;
        c.gridx = 0;
        /* PLL Out of Lock */
        add(new BooleanMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.PLL_OUT_OF_LOCK),
                Status.GOOD,
                "PLL has not gone out of lock",
                Status.FAILURE,
                "PLL has gone out of lock"),
        	c
        );
        
        c.gridx = 2;
        /* Delay Tracking */
        /* TODO Fix to match actual monitor point */
//        add(new BooleanMonitorPointWidget(
//                logger, 
//                devicePM.getMonitorPointPM(LO2MonitorPoints.DELAY_TRACKING),
//                Status.OFF,
//                "Delay tracking disabled",
//                Status.GOOD,
//                "Delay tracking enabled"),
//        	c
//        );   

        c.gridx = 3;
        /* TODO Fix to match actual monitor and control points */
//        add(new BooleanToggleWidget(
//        		loggger,
//        		devicePM.getMonitorPointPM(LO2MonitorPoints.DELAY_TRACKING),
//        		devicePM.getControlPointPM(LO2ControlPoints.SET_DELAY_TRACKING),
//        		"Enable Delay Tracking",
//        		"Disable Delay Tracking"),
//        	c
//        );
     
        c.gridx = 4;
        /* High/Low Switch */
        /* TODO Fix to match actual control point */
//        add(new BooleanMonitorControlPointWidget(
//        		logger,
//        		devicePM.getMonitorPointPM(LO2MonitorPoints.HIGH_LOW_SWITCH),
//        		devicePM.getControlPointPM(LO2ControlPoints.SET_RANGE),
//        		"Tune low",
//        		"Tune high"),
//        	c
//        );
        
        c.gridy = 2;
        c.gridx = 0;
        /* User Frequency */
        /* TODO add units */
        add(new FloatMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.LO2_FREQUENCY)),
        	c
        );
        
        c.gridx = 2;
        /* 90 Phase Switching */
        /* TODO Fix to match actual monitor point */
//        add(new BooleanMonitorPointWidget(
//                logger, 
//                devicePM.getMonitorPointPM(LO2MonitorPoints.90_DEGREE_PHASE_SWITCHING),
//                Status.OFF,
//                "90 degree phase switching disabled",
//                Status.GOOD,
//                "90 degree phase switching enabled"),
//        	c
//        ); 
        
        c.gridx = 3;
        /* TODO Fix to match actual monitor and control points */
//        add(new BooleanToggleWidget(
//        		logger,
//        		devicePM.getMonitorPointPM(LO2MonitorPoints.90_DEGREE_PHASE_SWITCHING),
//        		devicePM.getControlPointPM(LO2ControlPoints.SET_90_DEGREE_PHASE_SWITCHING),
//        		"Enable 90 Degree Phase Switching",
//        		"Disable 90 Degree Phase Switching"),
//        	c	
//        );
        
        c.gridx = 4;
        /* Side Band */        
        
        c.gridy = 3;
        c.gridx = 0;
        /* Reset Lock Status */
        add(new ResetLockStatusButton(), c);
        
        c.gridx = 2;
        /* 180 Phase Switching */
        /* TODO Fix to match actual monitor point */
//        add(new BooleanMonitorPointWidget(
//                logger, 
//                devicePM.getMonitorPointPM(LO2MonitorPoints.180_DEGREE_PHASE_SWITCHING),
//                Status.OFF,
//                "180 degree phase switching disabled",
//                Status.GOOD,
//                "180 degree phase switching enabled"),
//        	c
//        );   

        c.gridx = 3;
        /* TODO Fix to match actual monitor and control points */
//      add(new BooleanToggleWidget(
//      		logger,
//      		devicePM.getMonitorPointPM(LO2MonitorPoints.180_DEGREE_PHASE_SWITCHING),
//      		devicePM.getControlPointPM(LO2ControlPoints.SET_180_DEGREE_PHASE_SWITCHING),
//      		"Enable 180 Degree Phase Switching",
//      		"Disable 180 Degree Phase Switching"),
//      	c	
//      );
        
        this.setVisible(true);
    }
    
    private class ResetLockStatusButton
    	extends JButton
    	implements ActionListener {
    	
    	private ControlPointPresentationModel<Boolean> resetLockStatus; 
    	
    	public ResetLockStatusButton() {
    		
    		setText("Reset Lock Status");
    		
    		resetLockStatus = 
    			devicePM.getControlPointPM(IcdControlPoints.RESET_LOCK_STATUS);
    		
    		addActionListener(this);
    	}
            	  
        public void actionPerformed(ActionEvent e) {
                
        	resetLockStatus.setValue(true);
        }
    }
}    

//
// O_o
