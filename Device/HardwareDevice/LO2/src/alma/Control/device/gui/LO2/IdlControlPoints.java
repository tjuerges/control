/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LO2;

import alma.Control.device.gui.common.IControlPoint;

/**
 * List all Monitor Points implemented in this device.
 *
 * See IMonitorPoint for docs.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @since   ALMA 5.0.3
 */
 
//
// This is a temporary hack!
// 

public enum IdlControlPoints implements IControlPoint {

    FrequencyOffsetFactor(int.class, "operational"),
    FringeTracking(boolean.class, "operational"),
    FTSOnUpperSideband(boolean.class, "operational"),
    PropagationDelay(double.class, "operational"),
    UserFrequency(double.class, "operational"),
    ;
    
    private final Class typeClass;
    private final Boolean expectedBooleanValue;
    private final String operatingMode;
    private final int arrayLength;
    // Doubles are used below to prevent loss of precision.
    private final Double rangeLowerBound;
    private final Double rangeUpperBound;    
    private final String units;

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = null;
        this.rangeUpperBound = null;
        this.units = "";
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        Double rangeLowerBound, 
        Double rangeUpperBound
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
        this.units = "";
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        Double rangeLowerBound, 
        Double rangeUpperBound, 
        String units
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
        this.units = units;
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        int expectedBooleanValue
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = (expectedBooleanValue == 1) ? true: false;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = null;
        this.rangeUpperBound = null;
        this.units = "";
    }

    /*
     * Workaround for code generation systems tendency to convert "0.0" to "0".
     */
    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        Integer rangeLowerBound, 
        Double rangeUpperBound, 
        String units
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound;
        this.units = units;
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        Integer rangeLowerBound, 
        Integer rangeUpperBound
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = "";
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        Integer rangeLowerBound, 
        Integer rangeUpperBound, 
        String units
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = units;
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        Long rangeLowerBound, 
        Long rangeUpperBound
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = "";
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        Long rangeLowerBound, 
        Long rangeUpperBound, 
        String units
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = units;
    }

    private IdlControlPoints (
        Class typeClass, 
        String operatingMode, 
        String units
    ) {
        this.typeClass = typeClass;
        this.expectedBooleanValue = null;
        this.operatingMode = operatingMode;
        this.arrayLength = 1;
        this.rangeLowerBound = null;
        this.rangeUpperBound = null;
        this.units = units;
    }
    
    public int getArrayLength() {
        return arrayLength;
    }
    
    public Boolean getExpectedBooleanValue() {
        return expectedBooleanValue;
    }
    
    public String getOperatingMode() {
        return operatingMode;
    }

    public Double getRangeLowerBound() {
        return rangeLowerBound;
    }
    
    public Double getRangeUpperBound() {
        return rangeUpperBound;
    }
    
    public Class getTypeClass() {
        return typeClass;
    }

    public String getUnits() {
        return units;
    }
    
    public boolean supportsRangeChecking() {
        return ((null != rangeLowerBound) && (null != rangeUpperBound));
    }
    
    public boolean supportsUnits() {
        return (!units.equals(""));
    }
}

//
// O_o
        
