/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LO2.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import alma.Control.device.gui.LO2.IcdMonitorPoints;
import alma.Control.device.gui.LO2.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;



/**
 * @author Scott Rankin      srankin@nrao.edu
 * @author Steve Harrington  sharring@nrao.edu
 * @author Justin Kenworthy	 jkenwort@nmt.edu
 * @version $Id$
 */

public class RFDetailsPanel extends DevicePanel {
	
	private class DDSStatusPanel extends DevicePanel {
		
		public DDSStatusPanel(Logger logger, DevicePM dPM) {
	       	super(logger, dPM);
	       	buildPanel();
		}
		
		protected void buildPanel() {
			
			final String [] rowLabels = {
			    	"FTS Frequency",
			    	"Frequency Offset",
			    	"Phase Values",
			    	"Phase Offset",
			    	"Clock Counts"
			};
			
			setBorder(
			        BorderFactory.createCompoundBorder(
			        BorderFactory.createTitledBorder("DDS Status"),
			        BorderFactory.createEmptyBorder(1, 1, 1, 1)
			        )
			 );    		 
			 
			 setLayout(new GridBagLayout());
			 GridBagConstraints c = new GridBagConstraints();
			 c.fill = GridBagConstraints.HORIZONTAL;
			 
			 c.ipadx = 5;
			 c.ipady = 5;
    		 
    		// Add row headers.
 	        c.gridx = 0;
 	        for (int i = 0; i < rowLabels.length; i++) {
 	            c.gridy = i;
 	            if (rowLabels[i] != null) {
 	            	add(new JLabel(rowLabels[i]), c);
 	            }
 	        } 
 	        
 	        c.gridx = 1;
 	        c.gridy = 0;
 	        /* FTS Frequency */
			add(new FloatMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.FREQ),
					true),
				c
			);
 	        
 	        c.gridy = 1;
 	        /* Frequency Offset */
 	        add(new FloatMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.FREQUENCY_OFFSET),
					true),
				c
			);
 	        
 	        c.gridy = 2;
 	        /* Phase Values */
 	        add(new FloatMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.PHASE_VALS),
					true),
				c
			);
 	        
 	        c.gridy = 3;
 	        /* Phase Offset */
 	        add(new FloatMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.PHASE_OFFSET),
					true),
				c
			);
 	        
 	        c.gridy = 4;
 	        /* Clock Counts */
 	        add(new FloatMonitorPointWidget(
					logger,
					devicePM.getMonitorPointPM(IcdMonitorPoints.CLOCK_COUNTS)),
				c
			);
       		}
	}
	
	private class ControlPanel extends DevicePanel {
		
		public ControlPanel(Logger logger, DevicePM dPM) {
	       	super(logger, dPM);        
	       	buildPanel();
		}
		
		protected void buildPanel() {
			
			setBorder(
			        BorderFactory.createCompoundBorder(
			        BorderFactory.createTitledBorder("Control"),
			        BorderFactory.createEmptyBorder(1, 1, 1, 1)
			        )
			 );    		 
			 
			 setLayout(new GridBagLayout());
			 GridBagConstraints c = new GridBagConstraints();
			 c.fill = GridBagConstraints.HORIZONTAL;
			 
			 c.ipadx = 5;
			 c.ipady = 5;
			 
			 /* Set FTS Frequency */
//			 add(new FloatControlPointWidget(
//					 logger,
//					devicePM.getControlPointPM(LO2ControlPoints.SET_FREQ),
//					8000.00,
//					1.465,
//					"Set FTS Frequency"),
//				c
//			 );			
		}
	}
	
	private class ClockPanel extends DevicePanel {
		
		public ClockPanel(Logger logger, DevicePM dPM) {
	       	super(logger, dPM);
	       	buildPanel();
		}
		
		protected void buildPanel() {		
			
			final String [] rowLabels = {
			    	"Output RF Power Level",
			    	"Too few non-inverted clock cycles",
			    	"Too many non-inverted clock cycles",
			    	"Too few inverted clock cycles",
			    	"Too many inverted clock cycles",
			    	"Clock Status"
			};
			
			setBorder(
			        BorderFactory.createCompoundBorder(
			        BorderFactory.createTitledBorder("Clock"),
			        BorderFactory.createEmptyBorder(1, 1, 1, 1)
			        )
			 );    		 
			 
			 setLayout(new GridBagLayout());
			 GridBagConstraints c = new GridBagConstraints();
			 c.fill = GridBagConstraints.HORIZONTAL;
			 
			 c.ipadx = 5;
			 c.ipady = 5;
    		 
    		// Add row headers.
 	        c.gridx = 1;
 	        for (int i = 0; i < rowLabels.length; i++) {
 	            c.gridy = i;
 	            if (rowLabels[i] != null) {
 	            	add(new JLabel(rowLabels[i]), c);
 	            }
 	        } 
 	        
 	        c.gridx = 0;
 	        c.gridy = 0;
 	        /* Output RF Power Level */

                /* Hack Fix until Scott gets back */
			add(new BooleanMonitorPointWidget(
					logger, 
					devicePM.getMonitorPointPM(IcdMonitorPoints.FEW_NON_INV_CYCLES),
					Status.GOOD,
					"RF output level ok",
					Status.FAILURE,
					"Problem with RF output level"),
				c
			);
 	       
			c.gridy = 1;
			/* Too few non-inverted clock cycles */
			add(new BooleanMonitorPointWidget(
					logger, 
					devicePM.getMonitorPointPM(IcdMonitorPoints.FEW_NON_INV_CYCLES),
					Status.GOOD,
					"Non-inverted clock cycles OK",
					Status.FAILURE,
					"Too few non-inverted clock cycles"),
				c
			);
 	      
 	       	c.gridy = 2;
 	      	/* Too many non-inverted clock cycles */
			add(new BooleanMonitorPointWidget(
					logger, 
					devicePM.getMonitorPointPM(IcdMonitorPoints.MANY_NON_INV_CYCLES),
					Status.GOOD,
					"Non-inverted clock cycles OK",
					Status.FAILURE,
					"Too many non-inverted clock cycles"),
				c
			);
			
			c.gridy = 3;
			/* Too few inverted clock cycles */
			add(new BooleanMonitorPointWidget(
					logger, 
					devicePM.getMonitorPointPM(IcdMonitorPoints.FEW_INV_CYCLES),
					Status.GOOD,
					"Inverted clock cycles OK",
					Status.FAILURE,
					"Too few inverted clock cycles"),
				c
			);
 	      
 	       	c.gridy = 4;
 	      	/* Too many inverted clock cycles */
			add(new BooleanMonitorPointWidget(
					logger, 
					devicePM.getMonitorPointPM(IcdMonitorPoints.MANY_INV_CYCLES),
					Status.GOOD,
					"Inverted clock cycles OK",
					Status.FAILURE,
					"Too many inverted clock cycles"),
				c
			);
			
			/* Clock Status */
			/* TODO add textual boolean widget */
		}
	}
	
	public RFDetailsPanel(Logger logger, DevicePM dPM) {
	       	super(logger, dPM);
	       	buildPanel();
	}
	
	protected void buildPanel() {
		
	}
}
