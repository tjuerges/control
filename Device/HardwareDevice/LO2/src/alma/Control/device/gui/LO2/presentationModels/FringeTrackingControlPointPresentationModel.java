/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LO2.presentationModels;

import alma.Control.LO2Operations;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IControlPoint;
import alma.Control.device.gui.common.IdlControlPointPresentationModel;
import alma.common.log.ExcLog;

import java.util.logging.Logger;

/**
 * Track a read only property of a hardware device to be displayed in a UI.  Isolate the user interface 
 * from the implementation details of a hardware device in the Control system.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 * 
 */
public class FringeTrackingControlPointPresentationModel extends IdlControlPointPresentationModel<Boolean> {

	public FringeTrackingControlPointPresentationModel(
		Logger logger,
		DevicePresentationModel containingDevicePM,
		IControlPoint controlPoint,
		Class controlPointClass
	) {
		
		super(logger, containingDevicePM, controlPoint, controlPointClass);
		controlPointUpdateMethodName = "EnableFringeTracking";
	}

	@Override
	protected void setUpdateMethod() {
        try {
            this.controlPointUpdateMethod = LO2Operations.class.getMethod(
                controlPointUpdateMethodName, new Class[] { controlPointClass });
            if (this.controlPointUpdateMethod == null)
                logger.severe(
                    "FringeTrackingControlPointPresentationModel.setUpdateMethod() - failed to get controlPointUpdateMethod!"
                );
        } catch (SecurityException e) {
            logger.severe(
                "FringeTrackingControlPointPresentationModel.setUpdateMethod() - no access rights for method named: " + 
                controlPointUpdateMethodName +
                ExcLog.details(e)
            );
        } catch (NoSuchMethodException e) {
            logger.severe(
                "FringeTrackingControlPointPresentationModel.setUpdateMethod() - no method found named: " + 
                controlPointUpdateMethodName +
                ExcLog.details(e)
            );
        }
	}
}

//
//O_o
