/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LO2.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.JLabel;

import alma.Control.device.gui.LO2.IcdMonitorPoints;
import alma.Control.device.gui.LO2.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;

/**
 * @author Scott Rankin      srankin@nrao.edu
 * @author Steve Harrington  sharring@nrao.edu
 * @author Justin Kenworthy	 jkenwort@nmt.edu
 * @version $Id$
 */

public class PowerPanel extends DevicePanel {
	
	public PowerPanel(Logger logger, DevicePM dPM) {
		
	       	super(logger, dPM);        
	}
	
	protected void buildPanel() {
		
		final String[][] labelGrid = {
				{"Power Supply 1", "RF Power", "FM Coil Voltage"},
				{"Power Supply 2", "IF Power", null},
				{"Power Supply 3", null, null},
				{"Power Supply 3", null, null}
		};
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		 
		c.ipadx = 5;
		c.ipady = 5;
		
		//Add label grid
		for (int i = 0; i < labelGrid.length; i++) {       
			
        	c.gridy = i;
        	c.gridx = 0;
        	
        	for (int j = 0; j < labelGrid[i].length; j++) {
        		
        		if (labelGrid[i][j] != null) {
        			add(new JLabel(labelGrid[i][j]), c);
        		}
        		c.gridx += 2;  //Leave space for monitor point
        	}	        
	    } 
		
		c.gridy = 0;
		c.gridx = 1;
		/* Power Supply 1 */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_SUPPLY_1_VALUE),
				true),
			c
		);
		
		c.gridx = 3;
		/* RF Power */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DETECTED_RF_POWER),
				true),
			c
		);
		
		c.gridx = 5;
		/* FM Coil Voltage */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.FM_COIL_VOLTAGE),
				true),
			c
		);
		
		c.gridy = 1;
		c.gridx = 1;
		/* Power Supply 2 */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_SUPPLY_2_VALUE),
				true),
			c
		);
		
		c.gridx = 3;
		/* IF Power */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.DETECTED_IF_POWER),
				true),
			c
		);
		
		c.gridy = 2;
		c.gridx = 0;
		/* Power Supply 3 */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_SUPPLY_3_VALUE),
				true),
			c
		);
		
		c.gridy = 3;
		/* Power Supply 4 */
		add(new FloatMonitorPointWidget(
				logger,
				devicePM.getMonitorPointPM(IcdMonitorPoints.POWER_SUPPLY_4_VALUE),
				true),
			c
		);
	}
}
