/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LO2.presentationModels;

import alma.Control.LO2Operations;

import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.IdlMonitorPointPresentationModel;

import java.util.Date;
import java.util.logging.Logger;
import org.omg.CORBA.LongHolder;
import alma.acs.time.TimeHelper;
import alma.ControlExceptions.INACTErrorEx;
import alma.ControlExceptions.CAMBErrorEx;

/**
 * Track a read only property of a hardware device to be displayed in a UI.  Isolate the user interface 
 * from the implementation details of a hardware device in the Control system.
 * 
 * @author  Scott Rankin  srankin@nrao.edu   
 * @author  David Hunter  dhunter@nrao.edu
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public class FTSOnUpperSidebandMonitorPointPresentationModel extends IdlMonitorPointPresentationModel<Boolean> {
        
    public FTSOnUpperSidebandMonitorPointPresentationModel(Logger logger, DevicePresentationModel container,
                                                           IMonitorPoint monitorPoint)
    {
        super(logger, container, monitorPoint);
    }
    
    /**
     * Get the current value from the hardware device property this presentation model represents.
     * @param timeStamp A place to put the time stamp associated with the data.
     * @return the current value held by the hardware device.
     */
    protected Boolean getValueFromDevice(Date timeStamp) {
        Boolean res = false;
        LongHolder l = new LongHolder();
        try {
            LO2Operations dev = (LO2Operations)deviceComponent;
            res = dev.GET_HIGH_LOW_SWITCH(l);
        } catch (INACTErrorEx e) {
            logger.severe("FTSOnUpperSidebandMonitorPointPresentationModel.getValueFromDevice() Device is INACTIVE");
            e.printStackTrace();
        } catch (CAMBErrorEx e) {
            logger.severe("FTSOnUpperSidebandMonitorPointPresentationModel.getValueFromDevice() CAMBError Communicating with device");
            e.printStackTrace();
        } // catch (IllegalArgumentException e) {
//         	logger.severe(
//         		"FTSOnUpperSidebandMonitorPointPresentationModel.getValueFromDevice()" +
//         		" - illegal arguments for method" + 
//         		ExcLog.details(e)
//         	);
// 			e.printStackTrace();
// 		}
        timeStamp.setTime(TimeHelper.utcOmgToJava(l.value));
        return res;
    }

    @Override
    protected Boolean getValueFromDevice() {
        return getValueFromDevice(new Date());
    }
}

//
// O_o
