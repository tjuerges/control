#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
# $Source$
#


"""
This module is part of the Control Command Language.
It contains complementary functionality for a LO2 device.
"""


import CCL.LO2Base
import CCL.FTSDevice
import ControlDataInterfaces_idl
from CCL.EnumerationHelper import getEnumeration
from PyDataModelEnumeration import PyNetSideband


from CCL import StatusHelper
import os


class LO2(CCL.LO2Base.LO2Base,
          CCL.FTSDevice.FTSDevice):
    '''
    The LO2 class inherits from the code generated LO2Base
    class and adds specific methods.
    '''
    def __init__(self, antennaName = None, instance = None, componentName = None, stickyFlag = False):
        '''
        The constructor creates a LO2 object by making use of
        the LO2Base constructor. The stickyFlag can be set to
        True if the component should be instantiated in a sticky
        way.
        EXAMPLE:
        import CCL.LO2  LO2
        # Directly through component name
        lo2_0 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2BBpr0", stickyFlag = False)
        # or specifying the antenna name and the instance number of LO2.
        # Instance can be 0, 1, 2 or 3
        lo2_0 = CCL.LO2.LO2(antennaName = "DV01", instance = 0, stickyFlag = False)
        '''
        if componentName == None:
            if ((antennaName == None) or (instance == None)):
                raise NameError, "missing component name or the antennaName and instance number"
            if (isinstance(antennaName,list) == True):
                if (len(antennaName) != 0):
                    componentName=[]
                    for idx in range (0,len(antennaName)):
                        componentName.append("CONTROL/"+antennaName[idx]+"/LO2BBpr"+str(instance))
            else:
                componentName = "CONTROL/"+antennaName+"/LO2BBpr"+str(instance)
        CCL.LO2Base.LO2Base.__init__(self, None, componentName, stickyFlag)

    def __del__(self):
        CCL.LO2Base.LO2Base.__del__(self)

    def SetUserFrequency(self, loFrequency, offsetSideband = 'USB', epoch = 0):
        '''
        This method finds the tuning setups and sets the coarse- and
        FTS-frequencies. LOFrequency is the total frequency for the LO2,
        and epoch is the timestamp when the command should be executed
        (use 0 for immediately).

        Note that this method stores the state of the LO2 and uses
        the information for fringe tracking.  Any adjustment of the
        LO2 using low level commands (i.e. SET_FREQ, SET_RANGE_HIGH,etc.)
        will likely cause failure of fringe tracking.
        
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        lo2.SetUserFrequency(13720e6, 0)
        '''
        sideband = getEnumeration(offsetSideband, PyNetSideband)

        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].\
                SetUserFrequency(loFrequency, offsetSideband, epoch)
        if len(self._devices) == 1:
            return result.values()[0]
        return result 

    def GetNominalFrequency(self):
        '''
        Method returns the "Nominal" Frequency of the LO2, that is the
        frequency without the Frequency Offset or Fringe tracking applied.
        Note this is the software model of the frequency.  It can be in
        error is the Frequency has been changed using low level access
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        nfreq = lo2.GetNominalFrequency()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetNominalFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result 

    def GetNominalFTSFrequency(self):
        '''
        This method returns the "Nominal" Frequency of the FTS, that is the
        frequency without the Frequency Offset or Fringe tracking applied.
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        nfreq = lo2.GetNominalFTSFrequency()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetNominalFTSFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result 

    def GetActualFrequency(self):
        '''
        Method returns the actual frequency of the FTS as reported by
        the hardware.  Only available in operational mode 
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        nfreq = lo2.GetActualFrequency()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetActualFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result 

    def GetPropagationDelay(self):
        '''
        Method to get the propogation delay, this is a delta which is
        applied to the point in time when the phase is calculated.
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        nfreq = lo2.GetPropagationDelay()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetPropagationDelay()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

##     def SetPropagationDelay(self, propogationDelay):
##         '''
##         This method sets the propogation delay, this is a delta which is
##         applied to the point in time when the phase is calculated.
##         '''
##         result = {}
##         for key, val in self._devices.iteritems():
##             antName = key.rsplit("/")[1]
##             result[antName] = self._devices[key].SetPropagationDelay(propogationDelay)
##         if len(self._devices) == 1:
##             return result.values()[0]
##         return result 

##     def EnableFringeTracking(self, enable):
##         '''
##         This method enables/disables fringe tracking
##         '''
##         result = {}
##         for key, val in self._devices.iteritems():
##             antName = key.rsplit("/")[1]
##             result[antName] = self._devices[key].EnableFringeTracking(enable)
##         if len(self._devices) == 1:
##             return result.values()[0]
##         return result
    
##     def FringeTrackingEnabled(self):
##         '''
##         This method returns the current state of fringe tracking (boolean)
##         '''
##         result = {}
##         for key, val in self._devices.iteritems():
##             antName = key.rsplit("/")[1]
##             result[antName] = self._devices[key].FringeTrackingEnabled()
##         if len(self._devices) == 1:
##             return result.values()[0]
##         return result

##     def SetFrequencyOffset(self, offsetFrequency):
##         '''
##         This method seta the antenna specific frequency offset for spurious
##         signal rejection.
##         '''
##         result = {}
##         for key, val in self._devices.iteritems():
##             antName = key.rsplit("/")[1]
##             result[antName] = self._devices[key].SetFrequencyOffset(offsetFrequency)
##         if len(self._devices) == 1:
##             return result.values()[0]
##         return result
    
    def GetOffsetFrequency(self):
        '''
        Method get the current frequency includeing any offset set for
        spurious signal rejection.
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        nfreq = lo2.GetOffsetFrequency()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetOffsetFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result 

    def GetOffsetFTSFrequency(self):
        '''
        Method get the current frequency includeing any offset set for
        spurious signal rejection.
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        nfreq = lo2.GetOffsetFTSFrequency()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetOffsetFTSFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result 

    def GetFrequencyOffsetFactor(self):
        '''
        Method to get the current Frequency Offset Factor.
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        nfreq = lo2.GetFrequencyOffsetFactor()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetFrequencyOffsetFactor()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def setFrequencyOffsettingMode(self, mode = None):
        '''
        Method to get the current frequency offset Mode.
        EXAMPLE:
        import CCL.LO2
        import ControlDataInterfaces_idl
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        # Set the offset mode to second LO:
        mode = Control.LOOffsettingMode_SecondLO
        lo2.setFrequencyOffsetingMode(mode)
        # Set the offset mode to third LO:
        mode = Control.LOOffsettingMode_ThirdLO
        lo2.setFrequencyOffsetingMode(mode)
        # No offset mode:
        mode = Control.LOOffsettingMode_None
        lo2.setFrequencyOffsetingMode(mode)
        '''
        result = {}
        if (mode == None) or \
            ((mode != Control.LOOffsettingMode_None) \
            and (mode != Control.LOOffsettingMode_SecondLO) \
            and (mode != Control.LOOffsettingMode_ThirdLO)):
            print "The provided mode is not allowed!  Read the help!"
        else:
            for key, val in self._devices.iteritems():
                antName = key.rsplit("/")[1]
                result[antName] = self._devices[key].setFrequencyOffsetingMode(mode)
            if len(self._devices) == 1:
                return result.values()[0]
        return result 
    
    def getFrequencyOffsettingMode(self):
        '''
        Method to get the current frequency offset Mode.
        EXAMPLE:
        import CCL.LO2
        lo2 = CCL.LO2.LO2(componentName = "CONTROL/DV01/LO2A", stickyFlag = False)
        mode = lo2.getFrequencyOffsettingMode()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getFrequencyOffsettingMode()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def STATUS(self):
        '''
        This method will display the status information of the device at
        the standard output.
        '''
        os.system("clear")
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            try:
                ps1Volt,t = self._devices[key].GET_POWER_SUPPLY_1_VALUE()
                ps1IndVol = StatusHelper.Line("POWER SUPPLY 1", [StatusHelper.ValueUnit(ps1Volt,"V")])
            except:
                ps1IndVol = StatusHelper.Line("POWER SUPPLY 1", [StatusHelper.ValueUnit("N/A")])

            try:
                ps2Volt,t = self._devices[key].GET_POWER_SUPPLY_2_VALUE()
                ps2IndVol = StatusHelper.Line("POWER SUPPLY 2", [StatusHelper.ValueUnit(ps2Volt,"V")])
            except:
                ps2IndVol = StatusHelper.Line("POWER SUPPLY 2", [StatusHelper.ValueUnit("N/A")])

            try:
                ps3Volt,t = self._devices[key].GET_POWER_SUPPLY_3_VALUE()
                ps3IndVol = StatusHelper.Line("POWER SUPPLY 3", [StatusHelper.ValueUnit(ps3Volt,"V")])
            except:
                ps3IndVol = StatusHelper.Line("POWER SUPPLY 3", [StatusHelper.ValueUnit("N/A")])
                
            try:
                ps4Volt,t = self._devices[key].GET_POWER_SUPPLY_4_VALUE()
                ps4IndVol = StatusHelper.Line("POWER SUPPLY 4", [StatusHelper.ValueUnit(ps4Volt,"V")])
            except:
                ps4IndVol = StatusHelper.Line("POWER SUPPLY 4", [StatusHelper.ValueUnit("N/A")])   

            try:
                lo2freq,t = self._devices[key].GET_LO2_FREQUENCY()
                lo2freq = lo2freq / 1.0E6
                lo2freqInd = StatusHelper.Line("COARSE FREQUENCY", [StatusHelper.ValueUnit(lo2freq,"MHz")])
            except:
                lo2freqInd = StatusHelper.Line("COARSE FREQUENCY", [StatusHelper.ValueUnit("N/A")])

            try:
                freq,t = self._devices[key].GET_FREQ()
                freqInd = StatusHelper.Line("FTS FREQUENCY", [StatusHelper.ValueUnit(freq,"Hz")])
            except:
                freqInd = StatusHelper.Line("FTS FREQUENCY", [StatusHelper.ValueUnit("N/A")])       

            try:
                coilVolt,t = self._devices[key].GET_FM_COIL_VOLTAGE()
                coilVoltInd = StatusHelper.Line("FM COIL VOLTAGE", [StatusHelper.ValueUnit(coilVolt,"V")])
            except:
                coilVoltInd = StatusHelper.Line("FM COIL VOLTAGE", [StatusHelper.ValueUnit("N/A")])
                
            try:
                phVals,t = self._devices[key].GET_PHASE_VALS()
                phValsInd = StatusHelper.Line("PHASE VALS", [StatusHelper.ValueUnit(phVals,"Deg")])
            except:
                phValsInd = StatusHelper.Line("PHASE VALS", [StatusHelper.ValueUnit("N/A")])

            try:
                phSeq1,t = self._devices[key].GET_PHASE_SEQ1()
                phSeq1Ind = StatusHelper.Line("PHASE SEQ1", [StatusHelper.ValueUnit(phSeq1,"None")])
            except:
                phSeq1Ind = StatusHelper.Line("PHASE SEQ1", [StatusHelper.ValueUnit("N/A")])
                
            try:
                phSeq2,t = self._devices[key].GET_PHASE_SEQ2()
                phSeq2Ind = StatusHelper.Line("PHASE SEQ2", [StatusHelper.ValueUnit(phSeq2,"None")])
            except:
                phSeq2Ind = StatusHelper.Line("PHASE SEQ2", [StatusHelper.ValueUnit("N/A")])

            try:
                clock,t = self._devices[key].GET_CLOCK_COUNTS()
                clockInd = StatusHelper.Line("CLOCK COUNTS", [StatusHelper.ValueUnit(clock,"None")])
            except:
                clockInd = StatusHelper.Line("CLOCK COUNTS", [StatusHelper.ValueUnit("N/A")])

            try:
                phOffs,t = self._devices[key].GET_PHASE_OFFSET()
                phOffsInd = StatusHelper.Line("PHASE OFFSET", [StatusHelper.ValueUnit(phOffs,"ms")])
            except:
                phOffsInd = StatusHelper.Line("PHASE OFFSET", [StatusHelper.ValueUnit("N/A")])

            try:
                rfPow,t = self._devices[key].GET_DETECTED_RF_POWER()
                rfPowInd = StatusHelper.Line("DETECTED RF POWER", [StatusHelper.ValueUnit(rfPow*1e6,"uW")])
            except:
                rfPowInd = StatusHelper.Line("DETECTED RF POWER", [StatusHelper.ValueUnit("N/A")])

            try:
                ifPow,t = self._devices[key].GET_DETECTED_IF_POWER()
                ifPowInd = StatusHelper.Line("DETECTED IF POWER", [StatusHelper.ValueUnit(ifPow,"dBm")])
            except:
                ifPowInd = StatusHelper.Line("DETECTED IF POWER", [StatusHelper.ValueUnit("N/A")])

            try:
                ftsPow,t = self._devices[key].GET_DETECTED_FTS_POWER()
                ftsPowInd = StatusHelper.Line("DETECTED FTS POWER", [StatusHelper.ValueUnit(ftsPow,"dBm")])
            except:
                ftsPowInd = StatusHelper.Line("DETECTED FTS POWER", [StatusHelper.ValueUnit("N/A")])

            # For the MODULE_STATUS we first have to refresh the value
            try:
                self._devices[key].GET_MODULE_STATUS()
            except:
                nothing = 0

            try:
                statLock,t = self._devices[key].GET_TUNE_LOCK_SWITCH()
                if statLock == True:
                    statLockInd = StatusHelper.Line("TUNE/LOCK SWITCH", [StatusHelper.ValueUnit("Tune")])
                else:
                    statLockInd = StatusHelper.Line("TUNE/LOCK SWITCH", [StatusHelper.ValueUnit("Lock")])
            except:
                statLockInd = StatusHelper.Line("TUNE/LOCK SWITCH", [StatusHelper.ValueUnit("N/A")])

            try:
                statHilo,t = self._devices[key].GET_HIGH_LOW_SWITCH()
                if statHilo == True:
                    statHiloInd = StatusHelper.Line("HIGH/LOW SWITCH", [StatusHelper.ValueUnit("High")])
                else:
                    statHiloInd = StatusHelper.Line("HIGH/LOW SWITCH", [StatusHelper.ValueUnit("Low")])
            except:
                statHiloInd = StatusHelper.Line("HIGH/LOW SWITCH", [StatusHelper.ValueUnit("N/A")])

            try:
                statPllLock,t = self._devices[key].GET_PLL_LOCK_IND()
                if statPllLock == True:
                    statPllLockInd = StatusHelper.Line("PLL LOCK INDICATOR", [StatusHelper.ValueUnit("Locked")])
                else:
                    statPllLockInd = StatusHelper.Line("PLL LOCK INDICATOR", [StatusHelper.ValueUnit("Unlocked")])
            except:
                statPllLockInd = StatusHelper.Line("PLL LOCK INDICATOR", [StatusHelper.ValueUnit("N/A")])

            try:
                statPllOut,t = self._devices[key].GET_PLL_OUT_OF_LOCK()
                if statPllOut == True:
                    statPllOutInd = StatusHelper.Line("PLL OUT OF LOCK", [StatusHelper.ValueUnit("Lock lost")])
                else:
                    statPllOutInd = StatusHelper.Line("PLL OUT OF LOCK", [StatusHelper.ValueUnit("Ok")])
            except:
                statPllOutInd = StatusHelper.Line("PLL OUT OF LOCK", [StatusHelper.ValueUnit("N/A")])

            # For the FTS_STATUS we first have to refresh the value
            try:
                self._devices[key].GET_FTS_STATUS()
            except:
                nothing = 0

            try:
                statFewNonInv,t = self._devices[key].GET_FEW_NON_INV_CYCLES()
                if statFewNonInv == True:
                    statFewNonInvInd = StatusHelper.Line("TOO FEW NON-INV. CYCLES", [StatusHelper.ValueUnit("PROBLEM")])
                else:
                    statFewNonInvInd = StatusHelper.Line("TOO FEW NON-INV. CYCLES", [StatusHelper.ValueUnit("Ok")])
            except:
                statFewNonInvInd = StatusHelper.Line("TOO FEW NON-INV. CYCLES", [StatusHelper.ValueUnit("N/A")])

            try:
                statManyNonInv,t = self._devices[key].GET_MANY_NON_INV_CYCLES()
                if statManyNonInv == True:
                    statManyNonInvInd = StatusHelper.Line("TOO MANY NON-INV. CYCLES", [StatusHelper.ValueUnit("PROBLEM")])
                else:
                    statManyNonInvInd = StatusHelper.Line("TOO MANY NON-INV. CYCLES", [StatusHelper.ValueUnit("Ok")])
            except:
                statManyNonInvInd = StatusHelper.Line("TOO MANY NON-INV. CYCLES", [StatusHelper.ValueUnit("N/A")])

            try:
                statFewInv,t = self._devices[key].GET_FEW_INV_CYCLES()
                if statFewInv == True:
                    statFewInvInd = StatusHelper.Line("TOO FEW INV. CYCLES", [StatusHelper.ValueUnit("PROBLEM")])
                else:
                    statFewInvInd = StatusHelper.Line("TOO FEW INV. CYCLES", [StatusHelper.ValueUnit("Ok")])
            except:
                statFewInvInd = StatusHelper.Line("TOO FEW INV. CYCLES", [StatusHelper.ValueUnit("N/A")])

            try:
                statManyInv,t = self._devices[key].GET_MANY_INV_CYCLES()
                if statManyInv == True:
                    statManyInvInd = StatusHelper.Line("TOO MANY INV. CYCLES", [StatusHelper.ValueUnit("PROBLEM")])
                else:
                    statManyInvInd = StatusHelper.Line("TOO MANY INV. CYCLES", [StatusHelper.ValueUnit("Ok")])
            except:
                statManyInvInd = StatusHelper.Line("TOO MANY INV. CYCLES", [StatusHelper.ValueUnit("N/A")])
                
            frame = StatusHelper.Frame(compName + "   Ant: " + antName,[freqInd, phValsInd, phSeq1Ind, phSeq2Ind, clockInd, statFewNonInvInd, statManyNonInvInd, statFewInvInd, statManyInvInd, phOffsInd, statLockInd, statHiloInd, statPllLockInd, statPllOutInd, lo2freqInd, ps1IndVol, ps2IndVol, ps3IndVol, ps4IndVol, rfPowInd, ifPowInd, ftsPowInd, coilVoltInd])
            frame.printScreen()
