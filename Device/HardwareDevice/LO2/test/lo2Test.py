#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2009-04-24  created
#

"""
  This test both tests the functionality of the FLOOG device and
  ensures that the CCL is functioning correctly.
"""


import unittest
from Acspy.Clients.SimpleClient import PySimpleClient
from CCL.LO2 import LO2
import Control
import ControlDataInterfaces_idl
import time
import TETimeUtil
from math import pi
import LO2Exceptions

from PyDataModelEnumeration import PyNetSideband


class LO2TestCase(unittest.TestCase):

    def setUp(self):
        self.client = PySimpleClient()
        self.ref = self.client.getComponent("CONTROL/DA41/LO2BBpr1")
        self.lo2 = LO2("DA41",1)

    def tearDown(self):
        self.client.releaseComponent("CONTROL/DA41/LO2BBpr1")
        
    def testComponentLifecycle(self):
        self.assertEqual(self.ref._get_name(),'CONTROL/DA41/LO2BBpr1')
        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Start)

    def testHwConfigure(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Configure)
        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Configure)

    def testHwInitialize(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Initialize)

        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Initialize)

    def testHwOperational(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        self.assertEqual(self.ref.getHwState(),
                         Control.HardwareDevice.Operational)
        # Since the frequency command is asynchronous and about
        # a quarter second in the future wait 0.5 sec before
        # notesting.
        time.sleep(0.5)

        # test the initial state of the system here
        self.assertAlmostEqual(12.035E9, self.lo2.GetNominalFrequency(), 6)
        self.assertAlmostEqual(35.0E6, self.ref.GET_FREQ()[0], 6)
        self.assertAlmostEqual(12.035E9, self.lo2.GetActualFrequency(), 6)
        self.assertTrue(self.lo2.GET_HIGH_LOW_SWITCH()[0])
        self.assertFalse(self.lo2.GET_TUNE_LOCK_SWITCH()[0])
        
    def testSettingTuneLock(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        time.sleep(1) #Make sure the system is stable before we start

        self.lo2.SET_LOCK()
        self.assertFalse(self.lo2.GET_TUNE_LOCK_SWITCH()[0])
        self.lo2.SET_TUNE()
        self.assertTrue(self.lo2.GET_TUNE_LOCK_SWITCH()[0])
        self.lo2.SET_LOCK()
        self.assertFalse(self.lo2.GET_TUNE_LOCK_SWITCH()[0])
                
    def testSettingHighLow(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        time.sleep(1) #Make sure the system is stable before we start
        
        self.lo2.SET_RANGE_LOW()
        time.sleep(0.5)
        self.assertFalse(self.lo2.GET_HIGH_LOW_SWITCH()[0])
        #time.sleep(0.25)
        
        self.lo2.SET_RANGE_HIGH()
        time.sleep(0.5)
        self.assertTrue(self.lo2.GET_HIGH_LOW_SWITCH()[0])
                
        self.lo2.SET_RANGE_LOW()
        time.sleep(0.5)
        self.assertFalse(self.lo2.GET_HIGH_LOW_SWITCH()[0])
        
                
    def testFrequencySetting(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)

        # Check that we throw exceptions for a bad frequency
        self.assertRaises(LO2Exceptions.FreqNotAvailableEx,
                          self.lo2.SetUserFrequency,12.5,0);

        # Now check a normal execution (USB)
        self.lo2.SetUserFrequency(12.53E9,0)
        time.sleep(0.5)
        self.assertAlmostEqual(12.530E9, self.lo2.GetNominalFrequency(),6)
        self.assertAlmostEqual(30E6, self.lo2.GetNominalFTSFrequency(),5)
        self.assertAlmostEqual(30E6, self.ref.GET_FREQ()[0],5)
        self.assertAlmostEqual(12.530E9, self.lo2.GetActualFrequency(), 6)
        self.assertTrue(self.lo2.GET_HIGH_LOW_SWITCH()[0])
        
        # Now check a normal execution (LSB)
        self.lo2.SetUserFrequency(12.46E9,0)
        time.sleep(0.5)
        self.assertAlmostEqual(12.460E9, self.lo2.GetNominalFrequency(),6)
        self.assertAlmostEqual(40E6, self.lo2.GetNominalFTSFrequency(),5)
        self.assertAlmostEqual(40E6, self.ref.GET_FREQ()[0],5)
        self.assertAlmostEqual(12.460E9, self.lo2.GetActualFrequency(), 6)
        self.assertFalse(self.lo2.GET_HIGH_LOW_SWITCH()[0])


    def testDelayedFrequencySetting(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        now = TETimeUtil.unix2epoch(time.time())
        target = TETimeUtil.plusTE(now,63).value

        self.lo2.SetUserFrequency(12.535E9,0)
        self.lo2.SetUserFrequency(9.975E9, target)

        time.sleep(0.5) # Time for the reset to take effect
        for idx in range(20):
            time.sleep(0.1)
            if abs(TETimeUtil.unix2epoch(time.time()).value-target) < 3000000:
                continue
            if TETimeUtil.unix2epoch(time.time()).value < target:
                self.assertAlmostEqual(12.535E9,
                                       self.lo2.GetNominalFrequency(),6)
                self.assertAlmostEqual(35E6,
                                       self.lo2.GetNominalFTSFrequency(),5)
                self.assertAlmostEqual(35E6, self.ref.GET_FREQ()[0],5)
                self.assertAlmostEqual(12.535E9,
                                       self.lo2.GetActualFrequency(), 6)
                self.assertTrue(self.lo2.GET_HIGH_LOW_SWITCH()[0])
            else:
                self.assertAlmostEqual(9.975E9,
                                       self.lo2.GetNominalFrequency(),6)
                self.assertAlmostEqual(25E6, self.lo2.GetNominalFTSFrequency(),
                                       5)
                self.assertAlmostEqual(25E6, self.ref.GET_FREQ()[0],5)
                self.assertAlmostEqual(9.975E9,self.lo2.GetActualFrequency(),6)
                self.assertFalse(self.lo2.GET_HIGH_LOW_SWITCH()[0])
        
    def testFrequencyOffsetting(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        self.lo2.SetUserFrequency(12.535E9,0)

        self.lo2.SetFrequencyOffsetFactor(1)
        time.sleep(0.5)
        self.assertEqual(self.lo2.GetFrequencyOffsetFactor(), 1)
        self.assertEqual(self.lo2.getFrequencyOffsettingMode(), Control.LOOffsettingMode_None)
        self.assertAlmostEqual(35.00E6, self.ref.GET_FREQ()[0], 6)
        self.assertAlmostEqual(35.00E6, self.lo2.GetOffsetFTSFrequency(),6)
        self.assertAlmostEqual(12.535E9, self.lo2.GetNominalFrequency(),6)
        self.assertAlmostEqual(12.535E9, self.lo2.GetOffsetFrequency(),6)
        self.assertAlmostEqual(12.535E9, self.lo2.GetActualFrequency(),6)

        # Now turn on the offsetting
        self.lo2.setFrequencyOffsettingMode(Control.LOOffsettingMode_SecondLO)
        time.sleep(0.5)
        self.assertEqual(self.lo2.GetFrequencyOffsetFactor(), 1)
        self.assertEqual(self.lo2.getFrequencyOffsettingMode(), Control.LOOffsettingMode_None)
        self.assertAlmostEqual(35.00E6+30517.578125,
                               self.ref.GET_FREQ()[0], 6)
        self.assertAlmostEqual(35.00E6+30517.578125,
                               self.lo2.GetOffsetFTSFrequency(),6)
        self.assertAlmostEqual(12.535E9, self.lo2.GetNominalFrequency(),6)
        self.assertAlmostEqual(12.535E9+30517.578125,
                               self.lo2.GetOffsetFrequency(),6)
        self.assertAlmostEqual(12.535E9+30517.578125,
                               self.lo2.GetActualFrequency(),6)


        # Now switch to the other sideband
        self.lo2.SetUserFrequency(9.975E9,0)
        time.sleep(0.5)
        self.assertEqual(self.lo2.GetFrequencyOffsetFactor(), 1)
        self.assertEqual(self.lo2.getFrequencyOffsettingMode(), Control.LOOffsettingMode_None)
        self.assertAlmostEqual(25.00E6-30517.578125, self.ref.GET_FREQ()[0], 6)
        self.assertAlmostEqual(25.00E6-30517.578125,
                               self.lo2.GetOffsetFTSFrequency(),6)
        self.assertAlmostEqual(9.975E9, self.lo2.GetNominalFrequency(),6)
        self.assertAlmostEqual(9.975E9 + 30517.578125,
                               self.lo2.GetOffsetFrequency(),6)
        self.assertAlmostEqual(9.975E9 + 30517.578125,
                               self.lo2.GetActualFrequency(),6)

        # Change the frequency factor
        self.lo2.SetFrequencyOffsetFactor(2)
        time.sleep(0.5)
        self.assertEqual(self.lo2.GetFrequencyOffsetFactor(), 2)
        self.assertEqual(self.lo2.getFrequencyOffsettingMode(), Control.LOOffsettingMode_None)
        self.assertAlmostEqual(25.00E6-61035.15625, self.ref.GET_FREQ()[0], 6)
        self.assertAlmostEqual(25.00E6-61035.15625,
                               self.lo2.GetOffsetFTSFrequency(),6)
        self.assertAlmostEqual(9.975E9, self.lo2.GetNominalFrequency(),6)
        self.assertAlmostEqual(9.975E9 + 61035.15625,
                               self.lo2.GetOffsetFrequency(),6)
        self.assertAlmostEqual(9.975E9 + 61035.15625,
                               self.lo2.GetActualFrequency(),6)

        # Turn off the frequency offsetting
        self.lo2.setFrequencyOffsetting(Control.LOOffsettingMode_None)
        time.sleep(0.5)
        self.assertEqual(self.lo2.GetFrequencyOffsetFactor(), 2)
        self.assertEqual(self.lo2.getFrequencyOffsettingMode(), Control.LOOffsettingMode_None)
        self.assertAlmostEqual(25.00E6, self.ref.GET_FREQ()[0], 6)
        self.assertAlmostEqual(25.00E6, self.lo2.GetOffsetFTSFrequency(),6)
        self.assertAlmostEqual(9.975E9, self.lo2.GetNominalFrequency(),6)
        self.assertAlmostEqual(9.975E9, self.lo2.GetOffsetFrequency(),6)
        self.assertAlmostEqual(9.975E9, self.lo2.GetActualFrequency(),6)

    def testPropogationDelay(self):
        self.assertEqual(self.lo2.GetPropagationDelay(),
                         0.0);
        self.lo2.SetPropagationDelay(1E-6)
        self.assertEqual(self.lo2.GetPropagationDelay(),
                         1E-6);

    def testFringeTracking(self):
        self._HwLifecycleHelper(Control.HardwareDevice.Operational)
        self.lo2.SetUserFrequency(10.032E9)
        self.assertFalse(self.lo2.FringeTrackingEnabled())

        self.ref.newDelayEvent(self._CreateDelayRampEvent())

        self.lo2.EnableFringeTracking(True)
        self.assertTrue(self.lo2.FringeTrackingEnabled())
        time.sleep(0.5) #Allow time for the reset
        self.assertAlmostEqual(self.lo2.GET_FREQ()[0],32E6 + 0.05016, 5)
        self.assertAlmostEqual(self.lo2.GetActualFrequency(),
                               10.032E9 + 0.05016, 5)
        
        self.lo2.SetUserFrequency(11.976E9)
        time.sleep(0.5) #Allow time for the reset
        self.assertAlmostEqual(self.lo2.GET_FREQ()[0],24E6 - 0.05988, 5)
        self.assertAlmostEqual(self.lo2.GetActualFrequency(),
                               11.976E9 + 0.05988, 5)

        self.lo2.EnableFringeTracking(False)
        time.sleep(1.5) #Allow time for the reset
        self.assertFalse(self.lo2.FringeTrackingEnabled())
        self.assertAlmostEqual(self.lo2.GET_FREQ()[0],24E6, 5)
        self.assertAlmostEqual(self.lo2.GetActualFrequency(), 11.976E9, 5)
         
    def _CheckPhases(self, targetPhases, actualPhases):
        for i in range(len(targetPhases)):
            self.assertAlmostEqual(targetPhases[i], actualPhases[i], 3)

    def _HwLifecycleHelper(self, targetState):

        StateList =[(self.ref.hwStop, Control.HardwareDevice.Stop),
                    (self.ref.hwStart,Control.HardwareDevice.Start),
                    (self.ref.hwConfigure,Control.HardwareDevice.Configure),
                    (self.ref.hwInitialize,Control.HardwareDevice.Initialize),
                    (self.ref.hwOperational,Control.HardwareDevice.Operational)
                     ]

        for state in StateList:
            counter = 0
            while counter < 5 and self.ref.getHwState() != state[1]:
                try:
                    state[0]() # Invoke the function
                    if targetState == state[1]:
                        return
                except Exception, e:
                    counter += 1
                    time.sleep(1)

    def _CreateDelayRampEvent(self):
        # This produces a delay ramp with a slope of -50 ps/s as
        # this should produce a frequency change at 10 GHz of 0.05 of
        # a hertz.
        startTime = TETimeUtil.unix2epoch(time.time()).value
        stopTime = startTime + 600000000L # 60 sec

        delayTable = Control.DelayTable \
                     (startTime,
                      stopTime,
                      [Control.TotalDelayValue(startTime,1E-6),
                       Control.TotalDelayValue(stopTime, 1E-6 - 3E-10)]
                      )

        return Control.AntennaDelayEvent(startTime,
                                         stopTime,
                                         "DA41",
                                         [],
                                         [],
                                         [delayTable])
        



if __name__ == '__main__':
    suite = unittest.makeSuite(LO2TestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
    

#
# ___oOo___
