#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The LO2Test provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
from Acspy.Clients.SimpleClient import PySimpleClient

# Other imports
from time import sleep
import LO2Exceptions
import exceptions

# Automated test class
class automated( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()

    def tearDown(self):

        self.client.releaseComponent(self.componentName)
        self.client.disconnect()
        del(self.client)

    def initLO2(self):

        # Define the component name to be used
        self.componentName = "CONTROL/DV01/LO2BBpr0"
        print "Instantiating LO2 component..."

        try:
            # Get the component reference
            self.lo2 = self.client.getComponent(self.componentName)

            # Initialize the component
            print "Initializing component via HW lifecycle..."
            self.lo2.hwStart()
            self.lo2.hwConfigure()
            self.lo2.hwInitialize()
            self.lo2.hwOperational()

            # Wait for 2 seconds to start everything up
            sleep(2)

        except Exception, e:
            print "Exception: ", str(e)

    def test01(self):
        '''
        Component instantiation and intialization test.
        '''
        self.initLO2()
        sleep(1)
        self.client.releaseComponent(self.componentName)
	sleep(1)

    def test02(self):
        '''
        Functional test: setting/getting the coarse frequency
        '''
        self.initLO2()

        try: 
            # Test setting/getting the coarse frequency
            expCoarseFreq = 12180.0E6
            self.lo2.SET_LO2_FREQUENCY(expCoarseFreq)
            # Wait for 1 second
            sleep(1)
            coarseFreq = self.lo2.GET_LO2_FREQUENCY()
            print "coarseFreq = ", coarseFreq[0]
            self.assertAlmostEquals(expCoarseFreq, coarseFreq[0], -7)
            
            # Releasing the component
            self.client.releaseComponent(self.componentName)
	    sleep(1)
                    
        except Exception, e:
            print "Exception: ", str(e)


    def test03(self):
        '''
        Functional test: setting the user frequency
        '''
        self.initLO2()

        try: 
            # Define the user frequency in Hz and the expected results
            userFreq = 13720E6
            expDytoFreq = 13720E6
            expFtsFreq  = 30E6
            expFtsOnUpperSideband = 0

            # Set the LO2 User Frequency
            self.lo2.SetUserFrequency(userFreq, 0)
            sleep(3)
            
            # Reading back the DYTO frequency
            dytoFreq = self.lo2.GET_LO2_FREQUENCY()
            print "dytoFreq = ", dytoFreq[0]
            self.assertAlmostEquals(expDytoFreq, dytoFreq[0], -7)
            
            # Reading back the FTS frequency
            ftsFreq = self.lo2.GET_FREQ()
            print "ftsFreq = ", ftsFreq[0]
            self.assertAlmostEquals(expFtsFreq, ftsFreq[0], -5)

            # Reading back the FTSOnUpperSidenband flag
            upperSBFlag = self.lo2.GetFTSOnUpperSideband()
            print "upperSBFlag = ", upperSBFlag
            self.assertEquals(expFtsOnUpperSideband, upperSBFlag)

            # Reading back the Nominal Frequency
            nominalFreq = self.lo2.GetNominalFrequency()
            print "nominalFrequency = ", nominalFreq
            self.assertAlmostEquals(expDytoFreq - expFtsFreq, nominalFreq, -7)
             
            # Releasing the component
            self.client.releaseComponent(self.componentName)
	    sleep(1)
                    
        except Exception, e:
            print "Exception: ", str(e)

    def test04(self):
        '''
        Error condition test: setting unavailable user frequency
        '''
        self.initLO2()

        try: 
            # Try to set an unavailable user frequency
            # 1. reference signal out of range
            userFreq = 15000.0E6
            self.lo2.SetUserFrequency(userFreq, 0)
        except (LO2Exceptions.FreqNotAvailableEx), ex:
            print "Expected exception (reference signal) catched. Going on."

        try:
            # Try to set an unavailable user frequency
            # 2. FTS signal out of range
            userFreq = 13760.0E6
            self.lo2.SetUserFrequency(userFreq, 0)
        except (LO2Exceptions.FreqNotAvailableEx), ex:
            print "Expected exception (FTS signal) catched. Going on."

        try:
            self.client.releaseComponent(self.componentName)
        except Exception, e:
            print "Exception: ", str(e)

    def test05(self):
        '''
        Functional test: check conversion to power
        '''
        self.initLO2()

        try: 
            sleep(2)           
 
            # Read the detected IF power
            expIfPower = -12.7236;
            ifPower = self.lo2.GET_DETECTED_IF_POWER()
            self.assertAlmostEquals(expIfPower, ifPower[0], 2)
            
            # Read the detected FTS power
            expFtsPower = -15.7903;
            ftsPower = self.lo2.GET_DETECTED_FTS_POWER()
            self.assertAlmostEquals(expFtsPower, ftsPower[0], 2)
            
            # Releasing the component
            self.client.releaseComponent(self.componentName)
	    sleep(1)
                    
        except Exception, e:
            print "Exception: ", str(e)

            
# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()


