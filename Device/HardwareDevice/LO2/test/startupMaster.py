#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import TMCDB_IDL;
import asdmIDLTypes;
import shutil
import Control

#
# Local lib has caused import issues in some cases...
shutil.rmtree("../lib/python/site-packages", True) 

client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
master = client.getComponent('CONTROL/MASTER')
Acspy.Common.QoS.setObjectTimeout(master, 60000)
if (str(master.getMasterState()) == 'INACCESSIBLE'):
    antennaName = 'DA41'
    lo2BBpr0 = TMCDB_IDL.AssemblyLocationIDL("LO2BBpr0", "LO2BBpr0", 0x40, 0, 0)
    lo2BBpr1 = TMCDB_IDL.AssemblyLocationIDL("LO2BBpr1", "LO2BBpr1", 0x41, 0, 0)
    lo2BBpr2 = TMCDB_IDL.AssemblyLocationIDL("LO2BBpr2", "LO2BBpr2", 0x42, 0, 0)
    lo2BBpr3 = TMCDB_IDL.AssemblyLocationIDL("LO2BBpr3", "LO2BBpr3", 0x43, 0, 0)
    sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "A001", "", 1, [], \
                                      [lo2BBpr0, lo2BBpr1, lo2BBpr2, lo2BBpr3])
    tmcdb = client.getDefaultComponent("IDL:alma/TMCDB/TMCDBComponent:1.0");
    tmcdb.setStartupAntennasInfo([sai])
    ai = TMCDB_IDL.AntennaIDL(0, antennaName,  "", \
                              asdmIDLTypes.IDLLength(12), \
                              asdmIDLTypes.IDLArrayTime(0), \
                              asdmIDLTypes.IDLLength(1.0), \
                              asdmIDLTypes.IDLLength(2.0), \
                              asdmIDLTypes.IDLLength(10.0), \
                              asdmIDLTypes.IDLLength(0.0), \
                              asdmIDLTypes.IDLLength(0.0), \
                              asdmIDLTypes.IDLLength(0.0), 0)
    tmcdb.setAntennaInfo(antennaName, ai)
    pi = TMCDB_IDL.PadIDL(0, "A001", asdmIDLTypes.IDLArrayTime(0), \
                          asdmIDLTypes.IDLLength(-1601361.555455), \
                          asdmIDLTypes.IDLLength(-5042191.805932), \
                          asdmIDLTypes.IDLLength(3554531.803007))
    tmcdb.setAntennaPadInfo(antennaName, pi)
    master.startupPass1()
    master.startupPass2()
    client.releaseComponent(tmcdb._get_name());

arrayId = master.createAutomaticArray(['DA41'], [], Control.NONE)
arrayName = client.getComponent(arrayId.arrayComponentName)

#
# O_o
