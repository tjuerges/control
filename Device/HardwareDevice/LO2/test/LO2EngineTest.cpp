//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <FTSDeviceData.h>
#include <LO2Engine.h>
#include <TETimeUtil.h>
#include <ControlExceptions.h>
#include <ControlDataInterfacesC.h>


class LO2EngineTest: public CppUnit::TestFixture, public LO2Engine
{
    public:
    void testFindTuningSetup()
    {
        // This method verifies that the frequency setup is correctly
        // calculated
        FrequencySpecification ftsSpec;
        LO2Engine::LO2FreqSpec loSetup;

        ftsSpec.loFrequency = 8.020E9;
        loSetup = findTuningSetup(ftsSpec);
        CPPUNIT_ASSERT_EQUAL(ftsSpec.ftsFrequency, 0x28f5c28f5c28ULL);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(ftsSpec.loFrequency, 8.020E9, 1E-12);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(loSetup.loFrequency, 8.020E9, 1E-12);
        CPPUNIT_ASSERT_EQUAL(loSetup.FTSSideband, USB);

        ftsSpec.loFrequency = 13.980E9;
        loSetup = findTuningSetup(ftsSpec);
        CPPUNIT_ASSERT_EQUAL(ftsSpec.ftsFrequency, 0x28f5c28f5c28ULL);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(ftsSpec.loFrequency, 13.980E9, 1E-12);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(loSetup.loFrequency, 13.980E9, 1E-12);
        CPPUNIT_ASSERT_EQUAL(loSetup.FTSSideband, LSB);

        // These should all fail
        ftsSpec.loFrequency = 12.0E9;
        CPPUNIT_ASSERT_THROW(findTuningSetup(ftsSpec),
            ControlExceptions::IllegalParameterErrorExImpl);

        ftsSpec.loFrequency = 12.0;
        CPPUNIT_ASSERT_THROW(findTuningSetup(ftsSpec),
            ControlExceptions::IllegalParameterErrorExImpl);

        ftsSpec.loFrequency = 8.050E9;
        CPPUNIT_ASSERT_THROW(findTuningSetup(ftsSpec),
            ControlExceptions::IllegalParameterErrorExImpl);

        ftsSpec.loFrequency = 12.95E9;
        CPPUNIT_ASSERT_THROW(findTuningSetup(ftsSpec),
            ControlExceptions::IllegalParameterErrorExImpl);
    }

    void testQueueingFrequency()
    {
        // Put in a value at some time
        queueFrequency(12.035E9, 10000);

        // Check that there is only one value in the queue
        checkFrequencyQueueLength(1);

        // Put in a later time should now be two values in the queue
        queueFrequency(13.035E9, 30000);
        checkFrequencyQueueLength(2);

        // Put in a time in between, should now be 2 values in the queue
        queueFrequency(12.535E9, 15000);
        checkFrequencyQueueLength(2);

        // Clean the queue, ensure that there is one value in the queue
        cleanQueue();
        checkFrequencyQueueLength(1);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(
            getNominalFrequency(40000), 12.535E9, 1E-9);
    }

    void testReadbackFromQueue()
    {
        CPPUNIT_ASSERT_THROW(getNominalFrequency(10000),
            ControlExceptions::InvalidRequestExImpl);

        queueFrequency(12.035E9, 10000);
        queueFrequency(13.035E9, 15000);

        CPPUNIT_ASSERT_DOUBLES_EQUAL(
            getNominalFrequency(10000), 12.035E9, 1E-9);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(
            getNominalFrequency(14999), 12.035E9, 1E-9);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(
            getNominalFrequency(15000), 13.035E9, 1E-9);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(
            getNominalFrequency(17000), 13.035E9, 1E-9);
        CPPUNIT_ASSERT_DOUBLES_EQUAL(
            getNominalFrequency(), 13.035E9, 1E-9);
    }

    void testUpdateFrequency()
    {
        queueFrequency(12.035E9, 4800000);
        queueFrequency(13.980E9, 10080000);

        updateFrequency(5280000, true);
        checkFrequencyCommandQueueLength(1);
        checkLockQueue(5280000, 5760000);
        checkFrequencyCommandQueue(5280000, 12.035E9, 35E6);
        checkSidebandCommandQueue(5280000, USB);

        sidebandCommands_m.clear();
        tuneCommands_m.clear();
        frequencyCommandQueue_m.clear();
        lockCommands_m.clear();

        // Should do nothing
        updateFrequency(5760000, false);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of LO2 frequency commands",
            tuneCommands_m.size() == 0);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of FTS frequency commands",
            frequencyCommandQueue_m.size() == 0);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of lock commands",
            lockCommands_m.size() == 0);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of sideband commands",
            sidebandCommands_m.size() == 0);

        // Again does nothing
        updateFrequency(9120000, false);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of LO2 frequency commands",
            tuneCommands_m.size() == 0);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of FTS frequency commands",
            frequencyCommandQueue_m.size() == 0);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of lock commands",
            lockCommands_m.size() == 0);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of sideband commands",
            sidebandCommands_m.size() == 0);

        // Should update the frequencies
        updateFrequency(9600000, false);
        checkFrequencyCommandQueueLength(1);
        checkLockQueue(9600000, 10080000);
        checkFrequencyCommandQueue(9600000, 13.980E9, 20E6);
        checkSidebandCommandQueue(9600000, LSB);

    }

    void testGetOffsetFrequency()
    {
        queueFrequency(9.035E9, 4800000);

        CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Incorrect frequency returned",
            getOffsetFrequency(), 9.035E9, 1E-6);
        setFrequencyOffsetFactor(2);
        CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Incorrect frequency returned",
            getOffsetFrequency(), 9.035E9, 1E-6);

        FTSEngine::setFrequencyOffsetting(
            Control::LOOffsettingMode_SecondLO);
        CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Incorrect frequency returned",
            getOffsetFrequency(),9035061035.15625, 1E-6);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of LO2 frequency commands",
            tuneCommands_m.size() == 1);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of FTS frequency commands",
            frequencyCommandQueue_m.size() == 1);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of lock commands",
            lockCommands_m.size() == 2);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of sideband commands",
            sidebandCommands_m.size() == 1);

        sidebandCommands_m.clear();
        tuneCommands_m.clear();
        frequencyCommandQueue_m.clear();
        lockCommands_m.clear();

        FTSEngine::setFrequencyOffsetting(
            Control::LOOffsettingMode_None);
        CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Incorrect frequency returned",
            getOffsetFrequency(), 9.035E9, 1E-6);

        CPPUNIT_ASSERT_MESSAGE("Incorrect number of LO2 frequency commands",
            tuneCommands_m.size() == 1);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of FTS frequency commands",
            frequencyCommandQueue_m.size() == 1);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of lock commands",
            lockCommands_m.size() == 2);
        CPPUNIT_ASSERT_MESSAGE("Incorrect number of sideband commands",
            sidebandCommands_m.size() == 1);
    }

    CPPUNIT_TEST_SUITE(LO2EngineTest);
        CPPUNIT_TEST(testFindTuningSetup);
        CPPUNIT_TEST(testQueueingFrequency);
        CPPUNIT_TEST(testReadbackFromQueue);
        CPPUNIT_TEST(testUpdateFrequency);
        CPPUNIT_TEST(testGetOffsetFrequency);
    CPPUNIT_TEST_SUITE_END();

    // ---------------- Helper Methods ----------------
    private:
    void checkFrequencyQueueLength(unsigned long expectedLength)
    {
        if(lo2frequencySpecificationDeque_m.size() != expectedLength)
        {
            std::ostringstream msg;
            msg << "LO2 Frequency Queue length ("
                << lo2frequencySpecificationDeque_m.size()
                << ") did not match expected value!" << expectedLength;
            CPPUNIT_FAIL(msg.str());
        }

        if(frequencySpecificationDeque_m.size() != expectedLength)
        {
            std::ostringstream msg;
            msg << "FTS Frequency Queue length ("
                << frequencySpecificationDeque_m.size()
                << ") did not match expected value!" << expectedLength;
            CPPUNIT_FAIL(msg.str());
        }
    }

    void checkFrequencyCommandQueueLength(unsigned long expectedLength)
    {
        if(frequencyCommandQueue_m.size() != expectedLength)
        {
            std::ostringstream msg;
            msg << "FTS Frequency Command Queue length ("
                << frequencyCommandQueue_m.size()
                << ") did not match expected value!" << expectedLength;
            CPPUNIT_FAIL(msg.str());
        }

        if(tuneCommands_m.size() != expectedLength)
        {
            std::ostringstream msg;
            msg << "Tune Command Queue length (" << tuneCommands_m.size()
                << ") did not match expected value!" << expectedLength;
            CPPUNIT_FAIL(msg.str());
        }
    }

    void queueFrequencyCommand(unsigned long long& ftsFrequency,
        ACS::Time& applicationTime)
    {
        FrequencyCommand newFreqCommand;
        newFreqCommand.ftsFrequency = ftsFrequency;
        newFreqCommand.applicationTime = applicationTime;

        frequencyCommandQueue_m.push_back(newFreqCommand);
    }

    void queueLO2FrequencyCommand(double frequency, ACS::Time applicationTime)
    {
        FreqCmdLO2 loFreqCmd;
        loFreqCmd.frequency = frequency;
        loFreqCmd.applicationTime = applicationTime;
        tuneCommands_m.push_back(loFreqCmd);
    }

    void queueSidebandCommand(NetSidebandMod::NetSideband sideband,
        ACS::Time applicationTime)
    {
        SidebandCmd sidebandCmd;
        sidebandCmd.sideband = sideband;
        sidebandCmd.applicationTime = applicationTime;
        sidebandCommands_m.push_back(sidebandCmd);

    }

    void checkSidebandCommandQueue(ACS::Time applicationTime,
        NetSidebandMod::NetSideband sideband)
    {
        if(sidebandCommands_m.size() != 1)
        {
            std::ostringstream msg;
            msg << "Sizeband Command Queue did not have 1 entry ("
                << sidebandCommands_m.size() << " found)";
            CPPUNIT_FAIL(msg.str());
        }

        if(sidebandCommands_m[0].sideband != sideband)
        {
            std::ostringstream msg;
            msg << "Incorrect sideband  is selected";
            CPPUNIT_FAIL(msg.str());
        }

        if(sidebandCommands_m[0].applicationTime != applicationTime)
        {
            std::ostringstream msg;
            msg << "Sideband application time ("
                << sidebandCommands_m[0].applicationTime
                << "incorrect, correct value is" << applicationTime;
            CPPUNIT_FAIL(msg.str());
        }
    }

    void checkFrequencyCommandQueue(ACS::Time applicationTime,
        double lo2Frequency, double ftsFrequency)
    {
        if(tuneCommands_m.size() != 1)
        {
            std::ostringstream msg;
            msg << "Frequency Command Queue did not have 1 entrie ("
                << tuneCommands_m.size() << " found)";
            CPPUNIT_FAIL(msg.str());
        }

        if(tuneCommands_m[0].applicationTime != applicationTime)
        {
            std::ostringstream msg;
            msg << "LO2 Frequency Command Time ("
                << tuneCommands_m[0].applicationTime << ") incorrect"
                << applicationTime;
            CPPUNIT_FAIL(msg.str());
        }

        if(fabs(tuneCommands_m[0].frequency - lo2Frequency) > 1E-9)
        {
            std::ostringstream msg;
            msg << "LO2 Frequency Command (" << tuneCommands_m[0].frequency
                << ") incorrect: " << lo2Frequency;
            CPPUNIT_FAIL(msg.str());
        }

        if(frequencyCommandQueue_m.size() != 1)
        {
            std::ostringstream msg;
            msg << "FTS Frequency Command Queue did not have 1 entry ("
                << tuneCommands_m.size() << " found)";
            CPPUNIT_FAIL(msg.str());
        }

        if(fabs(rawToWorldFreq(frequencyCommandQueue_m[0].ftsFrequency)
            - ftsFrequency) > 1E-6)
        {
            std::ostringstream msg;
            msg << "FTS Frequency Command (" << rawToWorldFreq(
                frequencyCommandQueue_m[0].ftsFrequency) << ") incorrect: "
                << ftsFrequency;
            CPPUNIT_FAIL(msg.str());
        }

        if(frequencyCommandQueue_m[0].applicationTime != applicationTime)
        {
            std::ostringstream msg;
            msg << "FTS Frequency Command Time ("
                << frequencyCommandQueue_m[0].applicationTime << ") incorrect"
                << applicationTime;
            CPPUNIT_FAIL(msg.str());
        }
    }

    void checkLockQueue(ACS::Time unlockTime, ACS::Time lockTime)
    {
        if(lockCommands_m.size() != 2)
        {
            std::ostringstream msg;
            msg << "Lock Command Queue did not have 2 entries ("
                << lockCommands_m.size() << " found)";
            CPPUNIT_FAIL(msg.str());
        }

        if(lockCommands_m[0].lockState != false)
        {
            std::ostringstream msg;
            msg << "Fist lock command was not an unlock!";
            CPPUNIT_FAIL(msg.str());
        }

        if(lockCommands_m[0].applicationTime != unlockTime)
        {
            std::ostringstream msg;
            msg << "Unlock command at incorrect time ("
                << lockCommands_m[0].applicationTime << ") should have been"
                << unlockTime;
            CPPUNIT_FAIL(msg.str());
        }

        if(lockCommands_m[1].lockState != true)
        {
            std::ostringstream msg;
            msg << "Second lock command was not a lock!";
            CPPUNIT_FAIL(msg.str());
        }

        if(lockCommands_m[1].applicationTime != lockTime)
        {
            std::ostringstream msg;
            msg << "Lock command at incorrect time ("
                << lockCommands_m[1].applicationTime << ") should have been"
                << lockTime;
            CPPUNIT_FAIL(msg.str());
        }
    }

    void queueLockCommand(bool lock, ACS::Time applicationTime)
    {
        LockCommand lockCommand;
        lockCommand.lockState = lock;
        lockCommand.applicationTime = applicationTime;
        lockCommands_m.push_back(lockCommand);
    }

    void flushFrequencyCommands(ACS::Time& newFlushTime)
    {
        flushTimes_m.push_back(newFlushTime);
    }

    void resetFTS()
    {
        ACS::Time now = TETimeUtil::unix2epoch(time(0)).value;
        resetFTSTime_m.push_back(now);
    }

    void setErrorState(int alarm, bool state)
    {

    }

    void sendPhaseValuesCommand(std::vector< float >& phases)
    {
    }

    typedef struct
    {
        unsigned long long ftsFrequency;
        ACS::Time applicationTime;
    } FrequencyCommand;

    typedef struct
    {
        bool lockState;
        ACS::Time applicationTime;
    } LockCommand;

    typedef struct
    {
        double frequency;
        ACS::Time applicationTime;
    } FreqCmdLO2;

    typedef struct
    {
        NetSidebandMod::NetSideband sideband;
        ACS::Time applicationTime;
    } SidebandCmd;

    std::deque< FrequencyCommand > frequencyCommandQueue_m;
    std::deque< ACS::Time > flushTimes_m;
    std::deque< ACS::Time > resetFTSTime_m;
    std::deque< LockCommand > lockCommands_m;
    std::deque< FreqCmdLO2 > tuneCommands_m;
    std::deque< SidebandCmd > sidebandCommands_m;

    const ACS::Time getUpdateThreadCycleTime()
    {
        ACS::Time threadCycleTime = 10000000;
        return threadCycleTime;
    }

    long long unsigned int worldToRawCntlFreq(double freq) const
    {
        double freqWord = freq * (FTSDeviceData::FTSMask + 1);
        freqWord /= FTSDeviceData::HwClockScale;
        return static_cast< unsigned long long > (freqWord);
    }

    double rawToWorldFreq(long long unsigned int freq) const
    {
        double returnFreq = (FTSDeviceData::FTSMask & freq)
            * static_cast< double > (FTSDeviceData::HwClockScale);
        returnFreq /= FTSDeviceData::FTSMask + 1;
        return returnFreq;
    }

}; // End of Test Class

int main(int argc, char **argv)
{
    CppUnit::TextUi::TestRunner runner;
    runner.addTest(LO2EngineTest::suite());
    runner.run();
    return 0;
}
