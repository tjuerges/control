#ifndef LO2Engine_h
#define LO2Engine_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Oct 21, 2010  created
//

/// This class is an extension of the FTSEngine which is responsible
/// for handeling the frequnecy changes in the LO2 Modules.
/// This class is split out to allow for easy testing


#include <FTSEngine.h>
#include <almaEnumerations_IFC.h>


class LO2Engine: public FTSEngine
{
    public:
    LO2Engine();

    virtual ~LO2Engine();

    /// Set the frequency
    ///
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    virtual void setUserFrequency(double loFrequency,
        NetSidebandMod::NetSideband offsetSideband,
        ACS::Time applicationTime);

    virtual void setUserFrequency(double loFrequency,
        NetSidebandMod::NetSideband offsetSideband);

    /// Return the nominal frequency of the LO2, this throws an
    /// InvalidRequestExImpl if no frequencies have been defined
    virtual double getNominalFrequency(ACS::Time applicationTime);

    virtual double getNominalFrequency();

    /// Method returns the offset frequency, not including any
    /// contribution from the Fringe Tracking
    virtual double getOffsetFrequency(ACS::Time applicationTime);

    virtual double getOffsetFrequency();


    protected:
    typedef struct
    {
        double loFrequency;
        NetSidebandMod::NetSideband FringeSideband;
        NetSidebandMod::NetSideband OffsetSideband;
        ACS::Time applicationTime;
    } LO2FreqSpec;

    /// This is a list of the frequencies that the LO2 should put in place*/
    std::deque< LO2FreqSpec > lo2frequencySpecificationDeque_m;

    /// This method inserts the new frequency commands into the queue for
    /// both the FTS and the LO2.  It can throw and IllegalParameterErrorExImpl
    /// if the frequency is an illegal value.
    void queueFrequency(double, NetSidebandMod::NetSideband, ACS::Time);

    /// This method goes through the queue and will remove all obsolete
    /// commands.
    void cleanQueue();

    /// Update Frequency
    ///This method checks to see if we need to queue a command to adjust the
    /// LO2 and does so if necessary.  It calls the same method on the
    /// FTSEngine to maintain offsets etc.
    virtual void updateFrequency(ACS::Time, bool forceCommand);

    /// This is the method that does all of the actual work in retuning
    /// the LO2
    virtual void retuneLO2(double, NetSidebandMod::NetSideband,
        NetSidebandMod::NetSideband, ACS::Time);

    /// These methods allow conversion to and from a frequency setup
    /// and a world frequency
    ///  @ exception ControlExceptions::IllegalParameterErrorExImpl
    LO2FreqSpec findTuningSetup(FrequencySpecification& ftsFreqSpec);

    double calculateLO2Frequency(LO2FreqSpec,
        FTSEngine::FrequencySpecification);

    /// Return the LO2 Frequency Specification for a given time
    /// if no frequency is specified then it will throw an
    ///  @ exception ControlExceptions::IllegalParameterErrorExImpl
    LO2FreqSpec getLO2FrequencySpec(ACS::Time);

    virtual double rawToWorldFreq(long long unsigned int freq) const = 0;

    virtual long long unsigned int worldToRawCntlFreq(double) const = 0;

    // --------------------- Hardware Methods --------------------

    virtual void queueLO2FrequencyCommand(double, ACS::Time) = 0;

    virtual void queueSidebandCommand(NetSidebandMod::NetSideband,
    NetSidebandMod::NetSideband, ACS::Time) = 0;

    virtual void queueLockCommand(bool, ACS::Time) = 0;

    /// The last frequency specification sent to the hardware
    LO2FreqSpec lastFrequencySpecCommand_m;

    const static double CombLineSpacing = 125.0E6;

    const static int minCombLineNumber = 64;

    const static int maxCombLineNumber = 112;

};
#endif
