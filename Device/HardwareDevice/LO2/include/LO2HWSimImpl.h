// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef LO2HWSIMIMPL_H
#define LO2HWSIMIMPL_H

#include "LO2HWSimBase.h"
#include <SharedSimWrapper.h>
#include <SharedSimulatorC.h>

namespace AMB
{
    class LO2HWSimImpl : public LO2HWSimBase
    {
    public :
        LO2HWSimImpl(node_t node,
		     const std::vector<CAN::byte_t>& serialNumber,
		     int antenna=0,
		     SHARED_SIMULATOR::SharedSimulator_var ss=SHARED_SIMULATOR::SharedSimulator::_nil());
        ~LO2HWSimImpl();
        // Setting the LO2 frequency is sets the corresponding monitor point 
        virtual void setControlSetLo2Frequency(const std::vector<CAN::byte_t>& data);
        
        // Setting the range also sets the corresponding bit in the status 
        virtual void setControlSetRangeLow(const std::vector<CAN::byte_t>& data);
        
        // Setting the lock also sets the corresponding bit in the status i.e.,
        // this LO always locks.
        virtual void setControlSetLock(const std::vector<CAN::byte_t>& data);
        
        // Setting the state of the AFT adjusts the status word
        virtual void setControlSetAftOff(const std::vector<CAN::byte_t>& data);

        // Resetting the lock status, sets the corresponding bit in the
        // status to be what the current lock status is.
        virtual void setControlResetLockStatus(const std::vector<CAN::byte_t>& data);

        // Setting the phase values also sets the corresponding monitor point 
        virtual void setControlSetPhaseVals(const std::vector<CAN::byte_t>& data);

        // Setting the FTS frequency is sets the corresponding monitor point 
        virtual void setControlSetFreq(const std::vector<CAN::byte_t>& data);

	SHARED_SIMULATOR::SharedSimulator_var getSharedSimulatorReference() { return ss_m;};
  protected:
        SHARED_SIMULATOR::SharedSimulator_var ss_m;
        SharedSimWrapper* ssi_m;
    }; // class LO2HWSimImpl
    
} // namespace AMB
#endif // LO2HWSIMIMPL_H
