#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef LO2Impl_h
#define LO2Impl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <LO2Base.h>
#include <LO2S.h>
#include <LO2Engine.h>
#include <monitorHelper.h>
#include <LO2Exceptions.h>
#include <ControlDataInterfacesC.h>
#include <almaEnumerations_IFC.h>
#include <RW_Mutex.h>


namespace maci
{
    class ContainerServices;
};

namespace Control
{
    class AlarmSender;
};


class LO2Impl: public LO2Base,
    public LO2Engine,
    public MonitorHelper,
    virtual public POA_Control::LO2
{
    protected:
    /// -------------- Thread Classes  --------------
    class UpdateThread: public ACS::Thread
    {
        public:
        UpdateThread(const ACE_CString& name, const LO2Impl* lo2Device);
        virtual void runLoop();


        protected:
        LO2Impl* lo2Device_p;
    };


    public:
    friend class LO2Impl::UpdateThread;

    LO2Impl(const ACE_CString& name, maci::ContainerServices* cs);

    virtual ~LO2Impl();

    /// ----------------- External Interface ----------------------
    /// \exception ControlExceptions::INACTErrorEx
    /// \exception LO2Exceptions::FreqNotAvailableEx
    virtual void SetUserFrequency(CORBA::Double LOFrequency,
        NetSidebandMod::NetSideband offsetSideband, ACS::Time Epoch);

    /// \exception ControlExceptions::INACTErrorEx
    /// \exception ControlExceptions::InvalidRequestEx
    /// \exception ControlExceptions::CAMBErrorEx
    virtual double GetNominalFrequency();

    /// Method to return the nominal frequency of the FTS
    /// \exception ControlExceptions::InvalidRequestEx
    virtual double GetNominalFTSFrequency();

    /// Frequency Offsetting Interface
    /// \exception ControlExceptions::InvalidRequestEx
    virtual double GetOffsetFrequency();

    virtual double GetOffsetFTSFrequency();

    /// Method to return the Actual Frequency of the FTS
    /// \exception ControlExceptions::InvalidRequestEx
    virtual double GetActualFrequency();

    virtual CORBA::Long GetFrequencyOffsetFactor();

    virtual void SetFrequencyOffsetFactor(const CORBA::Long freqOffsetFactor);

    void setFrequencyOffsettingMode(Control::LOOffsettingMode mode);
    Control::LOOffsettingMode getFrequencyOffsettingMode();

    /// Delay Compensation
    virtual void EnableFringeTracking(bool);

    virtual CORBA::Boolean FringeTrackingEnabled();

    virtual double GetPropagationDelay();

    virtual void SetPropagationDelay(CORBA::Double propogationDelay);

    /// ---------------- Component Lifecycle -----------------------
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /// ---------------- Delegation Methods ------------------------
    virtual void newDelayEvent(const Control::AntennaDelayEvent& event);

    enum LO2AlarmConditions
    {
        WrongLO2Frequency = 8,
        WrongTuneState,
        WrongSideband,
        LO2Unlocked,
        AFTStateIncorrect
    };


    protected:
    /// ---------------- Hardware Lifecycle Interface ---------------
    virtual void hwConfigureAction();

    virtual void hwInitializeAction();

    virtual void hwOperationalAction();

    virtual void hwStopAction();

    /// --------------------Monitor Helper Method -------------------
    virtual void processRequestResponse(const AMBRequestStruct& response);

    /// ----------------- LO2 Support Methods ------------------
    /// Override the sideband setting methods to ensure that the
    /// FTS stays in sync with the LO2
    /// \exception: ControlExceptions::INACTErrorExImpl
    virtual void setCntlRangeLow();

    virtual void setCntlRangeHigh();

    /// Override the tune/lock commands to ensure that the monitored state
    /// matches the controlled state.
    virtual void setCntlTune();

    virtual void setCntlLock();

    /// Override the AFT commands to ensure that we know the commanded state
    virtual void setCntlAftOff();

    virtual void setCntlAftOn();

    /// ----------------- FTS Support Methods ------------------
    virtual const ACS::Time getUpdateThreadCycleTime();

    virtual void queueFrequencyCommand(unsigned long long&, ACS::Time&);

    virtual void flushFrequencyCommands(ACS::Time&);
    virtual void resetFTS();


    virtual void processFTSStatusMonitor(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processFTSFrequencyCommand(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processFTSFrequencyMonitor(
        const MonitorHelper::AMBRequestStruct&);

    /// Method to set the phase values on the hardware,
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual void sendPhaseValuesCommand(std::vector< float >&);

    /// ---------------- LO2 Support Methods ------------------
    virtual void queueLO2FrequencyCommand(double, ACS::Time);

    virtual void queueSidebandCommand(NetSidebandMod::NetSideband,
        NetSidebandMod::NetSideband, ACS::Time);

    virtual void queueLockCommand(bool, ACS::Time);

    virtual void processLO2FrequencyCommand(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processLO2FrequencyMonitor(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processLO2StatusMonitor(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processLO2LockCommand(const MonitorHelper::AMBRequestStruct&);

    /// ----------------- Error Handleing Methods ------------------
    virtual void handleActivateAlarm(int code);

    virtual void handleDeactivateAlarm(int code);

    virtual void setErrorState(int errCode, bool state);

    /// ------------------- LO2 Control Methods -------------
    virtual void queueFTSStatusMonitor();

    virtual void queueLO2StatusMonitor();

    virtual void updateThreadAction();

    virtual double rawToWorldDetectedIfPower(const unsigned short raw) const;

    virtual double rawToWorldDetectedFtsPower(const unsigned short raw) const;

    virtual double rawToWorldDetectedRfPower(short int raw) const;

    virtual std::vector< bool > rawToWorldFtsStatus(
        const unsigned char raw) const;

    virtual double rawToWorldFreq(long long unsigned int) const;

    long long unsigned int worldToRawCntlFreq(double) const;


    private:
    /// No default ctor.
    LO2Impl();

    /// No copy ctor.
    LO2Impl(const LO2Impl&);

    /// No assignment operator.
    LO2Impl& operator=(const LO2Impl&);

    /// Initialise the helper system for alarms.
    void initialiseAlarmSystem();

    /// Clean up the helper system for alarms.
    void cleanUpAlarmSystem();

    /// Helper method for the conversion from voltage to power
    void linearInterpolation(double value, double startRange1, double endRange1,
            double startRange2, double endRange2, double& result) const;

    /// The time of the last Module Status that has been queued
    ACE_RW_Mutex nextStatusMonitorTimeMutex_m;

    ACS::Time nextStatusMonitorTime_m;

    UpdateThread* updateThread_p;
    ACS::Time updateThreadCycleTime_m;

    /// This holds the last successfully executed command to the FTS frequency
    /// register
    AmbDataMem_t lastFTSFrequencyCommand_m[6];

    /// This holds the last successfully executed command to the LO2 frequency
    /// command
    AmbDataMem_t lastLO2FrequencyCommand_m[2];

    ///This is used to keep track of the current commanded state of the lock.
    bool lo2TuneStateCommanded_m;

    /// This structure is used to keep track of the current state of the AFT
    bool lo2AFTCommandedOff_m;

    /// Pointer for the alarm system helper instance.
    Control::AlarmSender* alarmSender;
};
#endif
