#ifndef CryostatImpl_H
#define CryostatImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File CryostatImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <CryostatBase.h>
#include <CryostatS.h>

// ALMA Enum types
#include <Enum.hpp>

// Exceptions
#include <CryostatExceptions.h>

class CryostatImpl: public CryostatBase,
	public virtual POA_Control::Cryostat
{
 public:
  /**
   * Constructor
   */
  CryostatImpl(const ACE_CString& name, maci::ContainerServices* cs);

  /**
   * Destructor
   */
  virtual ~CryostatImpl();

  /* ----------------- External Interface ----------------------*/
  /**
   * void initPumping()
   * Prepare the initial condition for cool-down pumping:
   * - Stops pumping if the backing pump is on:
   *   - Closes the gate valve
   *   - Closes the solenoid valve
   *   - Disables the turbo pump
   *   - Confirms that the valves are closed and that the turbo pump is off
   *   - Disables the backing pump
   * - Enables the vacuum gauge
   * - Final substate: INIT
   * @except CryostatExceptions::IncorrectSubstateEx
   * @except CryostatExceptions::DeviceAccessEx
   * @except CryostatExceptions::TimeoutEx
   * @except CryostatExceptions::ConditionsNotMetEx
   */
  virtual void initPumping();

  /**
   * void startPumping()
   * Starts cool-down pumping:
   * - Checks that the pressure (sensor 0) is below the rough
   *   pumping threshold
   * - Enables the backing pump
   * - Waits until the pressure is below the expected threshold
   * - Opens the solenoid valve
   * - Waits until the pressure is below the expected threshold
   * - Enables the turbo pump
   * - Waits until the turbo pump comes up to speed
   * - Opens the gate valve
   * - Final substate: TURBO_PUMPING
   * @except CryostatExceptions::IncorrectSubstateEx
   * @except CryostatExceptions::DeviceAccessEx
   * @except CryostatExceptions::TimeoutEx
   * @except CryostatExceptions::ConditionsNotMetEx
   * @except CryostatExceptions::ThreadErrorEx
   */
  virtual void startPumping();

  /**
   * void stopPumping()
   * Stops cool-down pumping:
   * - Checks that the cryocooler stages temperatures are below 30K
   * - Stops pumping if the backing pump is on:
   *   - Closes the gate valve
   *   - Closes the solenoid valve
   *   - Disables the turbo pump
   *   - Confirms that the valves are closed and that the turbo pump is off
   *   - Disables the backing pump
   * - Final substate: COOLING
   * @except CryostatExceptions::IncorrectSubstateEx
   * @except CryostatExceptions::DeviceAccessEx
   * @except CryostatExceptions::TimeoutEx
   * @except CryostatExceptions::ConditionsNotMetEx
   */
  virtual void stopPumping();

  /**
   * bool isExtRoughPumpingPressReached()
   * Once a external roughing pump has been connected this method
   * permits to check if the expected rough pumping pressure
   * has already been reached or not.
   * @return bool - true if pressure is reached.
   * @except CryostatExceptions::DeviceAccessEx
   */
  virtual bool isExtRoughPumpingPressReached();

  /**
   * bool isCoolingPressReached()
   * This method checks if the expected cooling pressure
   * has been already reached or not.
   * @return bool - true if cooling pressure is reached
   * @except CryostatExceptions::DeviceAccessEx
   */
  virtual bool isCoolingPressReached();

  /**
   * bool isCryoStagesTempReached()
   * This method checks if the expected cryocooler stages
   * temperatures have been already reached or not.
   * @return bool - true if cryo temperatures reached
   * @except CryostatExceptions::DeviceAccessEx
   */
  virtual bool isCryoStagesTempReached();

  /**
   * bool isWarmUpTempReached(in double temperature
   * This method checks if the given warm-up temperature [K]
   * has been already reached or not.
   * @param in double temperature - target temperature
   * @return bool - true if target is reached.
   * @except CryostatExceptions::DeviceAccessEx
   */
  virtual bool isWarmUpTempReached(double temperature);

  /**
   * PumpingSubstate getPumpingSubstate()
   * Returns the current pumping substate
   * @return PumpingSubstate
   */
  virtual Control::Cryostat::PumpingSubstate getPumpingSubstate();

 protected:
  /**
   * float getTemp0Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp0Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp0Temp(ACS::Time& timestamp);

  /**
   * float getTemp1Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp1Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp1Temp(ACS::Time& timestamp);

  /**
   * float getTemp2Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp2Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp2Temp(ACS::Time& timestamp);

  /**
   * float getTemp3Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp3Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp3Temp(ACS::Time& timestamp);

  /**
   * float getTemp4Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp4Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp4Temp(ACS::Time& timestamp);

  /**
   * float getTemp5Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp5Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp5Temp(ACS::Time& timestamp);

  /**
   * float getTemp6Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp6Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp6Temp(ACS::Time& timestamp);

  /**
   * float getTemp7Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp7Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp7Temp(ACS::Time& timestamp);

  /**
   * float getTemp8Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp8Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp8Temp(ACS::Time& timestamp);

  /**
   * float getTemp9Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp9Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp9Temp(ACS::Time& timestamp);

  /**
   * float getTemp10Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp10Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp10Temp(ACS::Time& timestamp);

  /**
   * float getTemp11Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp11Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp11Temp(ACS::Time& timestamp);

  /**
   * float getTemp12Temp(ACS::Time& timestamp)
   * @override CryostatBase::getTemp12Temp
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getTemp12Temp(ACS::Time& timestamp);

  /**
   * float getVacuumGaugeSensor0Pressure(ACS::Time& timestamp)
   * @override CryostatBase::getVacuumGaugeSensor0Pressure
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getVacuumGaugeSensor0Pressure(ACS::Time& timestamp);

  /**
   * float getVacuumGaugeSensor1Pressure(ACS::Time& timestamp)
   * @override CryostatBase::getVacuumGaugeSensor1Pressure
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getVacuumGaugeSensor1Pressure(ACS::Time& timestamp);

  /**
   * unsigned char getTurboPumpEnable(ACS::Time& timestamp)
   * @override CryostatBase::getTurboPumpEnable
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  unsigned char getTurboPumpEnable(ACS::Time& timestamp);

  /**
   * unsigned char getTurboPumpState(ACS::Time& timestamp)
   * @override CryostatBase::getTurboPumpState
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  unsigned char getTurboPumpState(ACS::Time& timestamp);

  /**
   * unsigned char getTurboPumpSpeed(ACS::Time& timestamp)
   * @override CryostatBase::getTurboPumpSpeed
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  unsigned char getTurboPumpSpeed(ACS::Time& timestamp);

  /**
   * unsigned char getGateValveState(ACS::Time& timestamp)
   * @override CryostatBase::getGateValveState
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  unsigned char getGateValveState(ACS::Time& timestamp);

  /**
   * unsigned char getSolenoidValveState(ACS::Time& timestamp)
   * @override CryostatBase::getSolenoidValveState
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  unsigned char getSolenoidValveState(ACS::Time& timestamp);

  /**
   * float getSupplyCurrent230v(ACS::Time& timestamp)
   * @override CryostatBase::getSupplyCurrent230v
   * @except ControlExceptions::CAMBErrorExImpl
   * @except ControlExceptions::INACTErrorExImpl
   */ 
  float getSupplyCurrent230v(ACS::Time& timestamp);
    
	
  /* ------------ PumpingThread class -------------- */
  class PumpingThread : public ACS::Thread {
  public:
    PumpingThread(const ACE_CString& name, CryostatImpl& Cryostat);
    virtual void run();
  protected:
    CryostatImpl& cryostatDevice_p;
  };

 public:
  friend class CryostatImpl::PumpingThread;

  // Threshold definitions
  static const float EXT_ROUGHING_PUMP_PRESS_THRESHOLD;
  static const float COOLING_PRESS_THRESHOLD;
  static const float CRYOCOOLER_STAGES_TEMP_THRESHOLD;

 private:
  /**
   * void stopPumpingHelper()
   * @except CryostatExceptions::DeviceAccessExImpl
   * @except CryostatExceptions::TimeoutExImpl
   * @except CryostatExceptions::ConditionsNotMetExImpl
   */
  void stopPumpingHelper();

  Control::Cryostat::PumpingSubstate pumpingSubstate_m;

  PumpingThread* pumpingThread_p;

  float temp0cache_m;
  float temp1cache_m;
  float temp2cache_m;
  float temp3cache_m;
  float temp4cache_m;
  float temp5cache_m;
  float temp6cache_m;
  float temp7cache_m;
  float temp8cache_m;
  float temp9cache_m;
  float temp10cache_m;
  float temp11cache_m;
  float temp12cache_m;
};
#endif // CryostatImpl_H
