
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File CryostatHWSimImpl.h
 *
 * $Id$
 */
#ifndef CryostatHWSimImpl_H
#define CryostatHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "CryostatHWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * CryostatHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class CryostatHWSimImpl : public CryostatHWSimBase
  {
  public :
	CryostatHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);

    
	static const rca_t GET_SETUP_INFO;
        
  protected:
        virtual void initialize(node_t node, const std::vector<CAN::byte_t>& serialNumber);
        
        virtual std::vector<CAN::byte_t> monitor(rca_t rca) const;
        
        virtual std::vector<CAN::byte_t> getSetupInfo() const;

        virtual void setControlSetupInfo(const std::vector<CAN::byte_t>& data);

        //temp
        virtual std::vector<CAN::byte_t> getTemp0Temp() const;
        virtual std::vector<CAN::byte_t> getTemp1Temp() const;
        virtual std::vector<CAN::byte_t> getTemp2Temp() const;
        virtual std::vector<CAN::byte_t> getTemp3Temp() const;
        virtual std::vector<CAN::byte_t> getTemp4Temp() const;
        virtual std::vector<CAN::byte_t> getTemp5Temp() const;
        virtual std::vector<CAN::byte_t> getTemp6Temp() const;
        virtual std::vector<CAN::byte_t> getTemp7Temp() const;
        virtual std::vector<CAN::byte_t> getTemp8Temp() const;
        virtual std::vector<CAN::byte_t> getTemp9Temp() const;
        virtual std::vector<CAN::byte_t> getTemp10Temp() const;
        virtual std::vector<CAN::byte_t> getTemp11Temp() const;
        virtual std::vector<CAN::byte_t> getTemp12Temp() const;
        /*backing pump dependant monitor points*/
        virtual std::vector<CAN::byte_t> getMonitorTurboPumpEnable() const;
        virtual std::vector<CAN::byte_t> getMonitorTurboPumpState() const;
        virtual std::vector<CAN::byte_t> getMonitorTurboPumpSpeed() const;
        virtual std::vector<CAN::byte_t> getMonitorGateValveState() const;
        virtual std::vector<CAN::byte_t> getMonitorSolenoidValveState() const;
        virtual std::vector<CAN::byte_t> getMonitorSupplyCurrent230v() const;
        /*ON/OFF control points that change other monitor points*/
        virtual void setControlSetBackingPumpOn(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetBackingPumpOff(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetTurboPumpOn(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetTurboPumpOff(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetGateValveOpen(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetGateValveClosed(const std::vector<CAN::byte_t>& data);
 	virtual void setControlSetSolenoidValveOpen(const std::vector<CAN::byte_t>& data);
 	virtual void setControlSetSolenoidValveClosed(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetVacuumGaugeOn(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetVacuumGaugeOff(const std::vector<CAN::byte_t>& data);

  private:
        
    unsigned int monitorPoint_m;
    unsigned int tcount_m;

    static const unsigned int TEMP0_TEMP;
    static const unsigned int TEMP1_TEMP;
    static const unsigned int TEMP2_TEMP;
    static const unsigned int TEMP3_TEMP;
    static const unsigned int TEMP4_TEMP;
    static const unsigned int TEMP5_TEMP;
    static const unsigned int TEMP6_TEMP;
    static const unsigned int TEMP7_TEMP;
    static const unsigned int TEMP8_TEMP;
    static const unsigned int TEMP9_TEMP;
    static const unsigned int TEMP10_TEMP;
    static const unsigned int TEMP11_TEMP;
    static const unsigned int TEMP12_TEMP;
    static const unsigned int TEMP_RETRY;

    float simulateTemp(unsigned int stage);
  	
  }; // class CryostatHWSimImpl

} // namespace AMB

#endif // CryostatHWSimImpl_H
