#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The CryostatTest provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
from Acspy.Clients.SimpleClient import PySimpleClient

# Other imports
from time import sleep

# Exceptions
import ControlDeviceExceptions
import CryostatExceptions
import exceptions

# For Control
import Control

# Automated test class
class automated( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()
        self.initCryostat()

    def tearDown(self):
        self.client.disconnect()
        del(self.client)

    def initCryostat(self):

        # Define the component name to be used
        self.componentName = "CONTROL/DA41/FrontEnd/Cryostat"
        print "Instantiating Cryostat component..."

        try:
            # Get the component reference
            self.cryo = self.client.getComponent(self.componentName)
            
            # Bring component to operational
	    print "Bringing component to operational..."
            self.cryo.hwConfigure()
	    self.cryo.hwInitialize()
            self.cryo.hwOperational()
        except Exception, e:
            print "Exception: ", str(e)

    def cleanup(self):
            print "Cleaning up..."
            # Nothing to be done for now
            
    def test01(self):
        '''
        Functional test
        '''
        try:

            # Initialize pumping
            self.cryo.initPumping()

            # Check substate
            substate = self.cryo.getPumpingSubstate()
            print "Pumping substate is now:", substate
            self.assertEquals(substate, Control.Cryostat.INIT)

            # Start pumping
            self.cryo.startPumping()

            # Check substate
            substate = self.cryo.getPumpingSubstate()
            print "Pumping substate is now:", substate
            self.assertEquals(substate, Control.Cryostat.PUMPING)

            sleep(12)

            substate = self.cryo.getPumpingSubstate()
            print "Pumping substate is now:", substate
            self.assertEquals(substate, Control.Cryostat.TURBO_PUMPING)

            # Stop pumping
            self.cryo.stopPumping()

            substate = self.cryo.getPumpingSubstate()
            print "Pumping substate is now:", substate
            self.assertEquals(substate, Control.Cryostat.COOLING)
                    
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()

# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()


