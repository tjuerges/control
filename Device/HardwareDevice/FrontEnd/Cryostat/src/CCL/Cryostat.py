#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a Cryostat device.
"""

import CCL.CryostatBase
from time import sleep
from CCL import StatusHelper

class Cryostat(CCL.CryostatBase.CryostatBase):
    '''
    The Cryostat class inherits from the code generated CryostatBase
    class and adds specific methods.
    '''
    
    # Import the cryostat enum types
    from Cryostat_idl import _0_Control

    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Creates an instance of the Cryostat component.

        Example:
        from CCL.Cryostat import Cryostat
        cryo=Cryostat("DA41")
        '''
        # FrontEnd subcomponents also have the prefix "FrontEnd/" in their
        # name, so we prepare always the componentName:
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/Cryostat"
            antennaName = None
        CCL.CryostatBase.CryostatBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.CryostatBase.CryostatBase.__del__(self)

    def initPumping(self):
        '''
        FOR MAINTENANCE INTERVENTION ONLY! Use the specific
        coolDownFromAtmosPressAndTemp() method for a guided
        cool-down sequence.
        
        Prepares the initial condition for cool-down pumping.
        '''
        if len(self._devices) > 1:
            print "This method can not be run for a group of devices. Aborting."
            return
        self._HardwareDevice__hw.initPumping()

    def startPumping(self):
        '''
        FOR MAINTENANCE INTERVENTION ONLY! Use the specific
        coolDownFromAtmosPressAndTemp() method for a guided
        cool-down sequence.
        
        Starts cool-down pumping.
        '''
        if len(self._devices) > 1:
            print "This method can not be run for a group of devices. Aborting."
            return
        self._HardwareDevice__hw.startPumping()   

    def stopPumping(self):
        '''
        FOR MAINTENANCE INTERVENTION ONLY! Use the specific
        coolDownFromAtmosPressAndTemp() method for a guided
        cool-down sequence.
        
        Stops cool-down pumping.
        '''
        if len(self._devices) > 1:
            print "This method can not be run for a group of devices. Aborting."
            return
        self._HardwareDevice__hw.stopPumping()

    def isExtRoughPumpingPressReached(self):
        '''
        Once a external roughing pump has been connected this method
        permits to check if the expected rough pumping pressure
        has already been reached or not.

        Returns True or False.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].isExtRoughPumpingPressReached()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def isCoolingPressReached(self):
        '''
        This method checks if the expected cooling pressure
        has been already reached or not.

        Returns True or False.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].isCoolingPressReached()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def isCryoStagesTempReached(self):
        '''
        This method checks if the expected cryocooler stages
        temperatures have been already reached or not.

        Returns True or False.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].isCryoStagesTempReached()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def isWarmUpTempReached(self, temperature):
        '''
        This method checks if the given warm-up temperature [K]
        has been already reached or not.

        Returns True or False.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].isWarmUpTempReached(temperature)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def getPumpingSubstate(self):
        '''
        Returns the current pumping substate.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getPumpingSubstate()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def coolDownFromAtmosPressAndTemp(self):
        '''
        This method supports the FE cryostat cool-down procedure,
        starting from atmospheric pressure and temperature. Note
        that manual intervention is required.

        Sequence:
        - Request: confirm that you want to start the pumping
        - Pumping is initialized
        - Request: attach external rough pump and start it
        - Wait until the rough pumping pressure is reached
        - Turbo pumping is started
        - Wait for turbo pumping started
        - Request: remove external roughing pump
        - Wait until cooling pressure is reached
        - Request: turn on the compressor
        - Wait until the cryocooler stages temperatures are reached
        - Stop pumping

        Ref.: FEND-40.00.00.00-089-A-MAN

        Example of usage:
        from CCL.Cryostat import Cryostat
        cryo = Cryostat("DV01")
        cryo.coolDownFromAtmosPressAndTemp()
        '''
        if len(self._devices) > 1:
            print "This method can not be run for a group of devices. Aborting."
            return
        
        print "Cool-down procedure starting from atmospheric pressure and"
        print "temperature."
        print ""

        # Confirm that the cool-down procedure should be started
        inp = raw_input("Do you want to start the procedure now (y/n)?")
        if (inp != "y") and (inp != "Y"):
            print "Interrupted."
            return

        # Initialize pumping
        self._HardwareDevice__hw.initPumping()

        # Request: attach external rough pump and start it
        print "------------------------------------------------------------"
        print "REQUEST: attach an external roughing pump to the cryostat's"
        print "auxiliary vacuum port to speed evacuation. Start pumping."
        print ""
        inp = raw_input("Press ENTER when you are done...")
        
        # Wait until the rough pumping pressure is reached
        print "------------------------------------------------------------"
        print "Waiting until the rough pumping pressure is reached..."
        flag = False
        while (not flag):
            flag = self._HardwareDevice__hw.isExtRoughPumpingPressReached()
            pressure,t = self._HardwareDevice__hw.GET_VACUUM_GAUGE_SENSOR0_PRESSURE()
            print "Current pressure (sensor 0) = ", pressure
            sleep(10)
        print "Rough pumping pressure reached."

        # Start turbo pumping
        print "------------------------------------------------------------"
        print "Starting turbo pumping..."
        self._HardwareDevice__hw.startPumping()

        # Wait for turbo pumping started
        print "Waiting for turbo pump to come to full speed (about 1-2 min)..."
        flag = False
        while (not flag):
            state = self._HardwareDevice__hw.getPumpingSubstate()
            if (state == self._0_Control.Cryostat.TURBO_PUMPING):
                flag = True
            sleep(10)
        print "Turbo pump at full speed."

        # Request: remove external rough pump
        print "------------------------------------------------------------"
        print "REQUEST: remove the external roughing pump."
        print ""
        inp = raw_input("Press ENTER when you are done...")

        # Wait until cooling pressure is reached
        print "------------------------------------------------------------"
        print "Waiting until the cooling pressure is reached..."
        flag = False
        while (not flag):
            flag = self._HardwareDevice__hw.isCoolingPressReached()
            pressure,t = self._HardwareDevice__hw.GET_VACUUM_GAUGE_SENSOR0_PRESSURE()
            print "Pressure (sensor 0) = ", pressure
            sleep(10)
        print "Cooling pressure reached."
        self._HardwareDevice__hw.longCoolDown()
        return
    
    def longCoolDown(self):
        # Request: turn on the compressor
        print "------------------------------------------------------------"
        print "REQUEST: turn on the compressor."
        print ""
        inp = raw_input("Press ENTER when you are done...")

        # Wait until the cryocooler stages temperatures are reached
        print "------------------------------------------------------------"
        print "Waiting until the cryocooler stages temperatures are"
        print "reached (about 24 hrs)..."
        flag = False
        while (not flag):
            flag = self._HardwareDevice__hw.isCryoStagesTempReached()
            temp0,t = self._HardwareDevice__hw.GET_TEMP0_TEMP()
            temp6,t = self._HardwareDevice__hw.GET_TEMP6_TEMP()
            print "Temperatures = ", temp0, temp6
            sleep(300)
        print "Cryocooler stages temperatures reached."

        # Stop pumping
        print "------------------------------------------------------------"
        print "Stopping pumping..."
        self._HardwareDevice__hw.stopPumping()
        sleep(10)

        print ""
        print "Cryostat cooled down successfully."
        
        return
    
    def restorationOfVacuum(self):
        '''
        This method supports the procedure of restoration of vacuum
        while installed in the antenna.

        TBD

        Ref.: FEND-40.00.00.00-089-A-MAN
        
        Example of usage:
        from CCL.Cryostat import Cryostat
        cryo = Cryostat("DV01")
        cryo.restorationOfVacuum()
        '''
        
        if len(self._devices) > 1:
            print "This method can not be run for a group of devices. Aborting."
            return
        
        # TBD
        print "Functionality not yet implemented."
        
        return

    def STATUS(self):
        '''
        This method displays the current status of the Cryostat
        '''

        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]

            # List of status elements
            elements = []

            ################################### Status ###########################################################
            elements.append( StatusHelper.Separator("Status") )
            # Supply current 230V
            try:
                value = self._devices[key].GET_SUPPLY_CURRENT_230v()[0]
            except:
                value = "N/A";
            elements.append( StatusHelper.Line("Supply Current",
                                               [StatusHelper.ValueUnit(value, "A")]) )

            # Pumping substate
            try:
                value = self._devices[key].getPumpingSubstate()
            except:
                value = "N/A";
            elements.append( StatusHelper.Line("Pumping substate",
                                               [StatusHelper.ValueUnit(value)]) )

            # Backing pump enabled
            try:
                value = self._devices[key].GET_BACKING_PUMP_ENABLE()[0]
                if value == 0:
                    value_t = "Off"
                elif value == 1:
                    value_t = "On"
                else:
                    value_t = "UNKNOWN"
            except:
                value_t = "N/A";
            elements.append( StatusHelper.Line("Backing pump",
                                               [StatusHelper.ValueUnit(value_t)]) )

            elements.append( StatusHelper.Line("Note: the following values are only available with the backing pump on"))

           # Turbo pump enabled
            try:
                value = self._devices[key].GET_TURBO_PUMP_ENABLE()[0]
                if value == 1:
                    value_t = "On"
                else:
                    value_t = "Off"
            except:
                value_t = "N/A";
            elements.append( StatusHelper.Line("Turbo pump",
                                               [StatusHelper.ValueUnit(value_t)]) )

           # Turbo pump speed
            try:
                value = self._devices[key].GET_TURBO_PUMP_SPEED()[0]
                if value == 0:
                    value_t = "LOW"
                elif value == 1:
                    value_t = "Ok"
                else:
                    value_t = "UNKNOWN"
            except:
                value_t = "N/A";
            elements.append( StatusHelper.Line("Turbo pump speed",
                                               [StatusHelper.ValueUnit(value_t)]) )

            # Gate valve
            try:
                value = self._devices[key].GET_GATE_VALVE_STATE()[0]
                if value == 0:
                    value_t = "Closed"
                elif value == 1:
                    value_t = "Open"
                elif value == 2:
                    value_t = "Transition"
                elif value == 3:
                    value_t = "ERROR"
                else:
                    value_t = "UNKNOWN"
            except:
                value_t = "N/A";
            elements.append( StatusHelper.Line("Gate valve",
                                               [StatusHelper.ValueUnit(value_t)]) )

            # Solenoid valve
            try:
                value = self._devices[key].GET_SOLENOID_VALVE_STATE()[0]
                if value == 0:
                    value_t = "Closed"
                elif value == 1:
                    value_t = "Open"
                elif value == 2:
                    value_t = "Transition"
                elif value == 3:
                    value_t = "ERROR"
                else:
                    value_t = "UNKNOWN"
            except:
                value_t = "N/A";
            elements.append( StatusHelper.Line("Solenoid valve",
                                               [StatusHelper.ValueUnit(value_t)]) )

            ################################### Temperature ######################################################
            elements.append( StatusHelper.Separator("Temperature") )

            # TEMP0
            try:
                value = self._devices[key].GET_TEMP0_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("0: 4K cryocooler",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP1
            try:
                value = self._devices[key].GET_TEMP1_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("1: 4K plate near link1",
                                               [StatusHelper.ValueUnit(value, "K")]) )    

            # TEMP2
            try:
                value = self._devices[key].GET_TEMP2_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("2: 4K plate near link2",
                                               [StatusHelper.ValueUnit(value, "K")]) )
           
            # TEMP3
            try:
                value = self._devices[key].GET_TEMP3_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("3: 4K plate far side1",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP4
            try:
                value = self._devices[key].GET_TEMP4_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("4: 4K plate far side2",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP5
            try:
                value = self._devices[key].GET_TEMP5_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("5: 15K cryocooler",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP6
            try:
                value = self._devices[key].GET_TEMP6_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("6: 15K plate near link",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP7
            try:
                value = self._devices[key].GET_TEMP7_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("7: 15K plate far side",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP8
            try:
                value = self._devices[key].GET_TEMP8_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("8: 15K shield top",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP9
            try:
                value = self._devices[key].GET_TEMP9_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("9: 110K cryocooler",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP10
            try:
                value = self._devices[key].GET_TEMP10_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("10: 110K plate near link",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP11
            try:
                value = self._devices[key].GET_TEMP11_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("11: 110K plate far side",
                                               [StatusHelper.ValueUnit(value, "K")]) )

            # TEMP12
            try:
                value = self._devices[key].GET_TEMP12_TEMP()[0];
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("12: 110K shield top",
                                               [StatusHelper.ValueUnit(value, "K")]) )
            
            ########################################### Pressure ###############################################
            elements.append( StatusHelper.Separator("Vacuum Gauge") )

            # Vacuum gauge enabled
            try:
                value = self._devices[key].GET_VACUUM_GAUGE_ENABLE()[0]
                if value == 1:
                    value_t = "OFF"
                elif value == 0:
                    value_t = "On"
                else:
                    value_t = "UNKNOWN"
                    print "value=", value, "type=", type(value)
            except:
                value_t = "N/A"
            elements.append( StatusHelper.Line("Vacuum gauge",
                                               [StatusHelper.ValueUnit(value_t)]) )
            
            # SENSOR0
            try:
                value = self._devices[key].GET_VACUUM_GAUGE_SENSOR0_PRESSURE()[0]
                value_pascal = "%.4e" % value
                value_mbar = "%.4e" % (value / 100.0)
            except:
                value_pascal = "N/A"
                value_mbar = "N/A"
            elements.append( StatusHelper.Line("Sensor 0", [StatusHelper.ValueUnit(value_pascal, "pascal"),
                                                            StatusHelper.ValueUnit(value_mbar, "mbar")]) )

            # SENSOR1
            try:
                value = self._devices[key].GET_VACUUM_GAUGE_SENSOR1_PRESSURE()[0]
                value_pascal = "%.2f" % value
                value_mbar = "%.2f" % (value / 100.0)
            except:
                value_pascal = "N/A"
                value_mbar = "N/A"
            elements.append( StatusHelper.Line("Sensor 1", [StatusHelper.ValueUnit(value_pascal, "pascal"),
                                                            StatusHelper.ValueUnit(value_mbar, "mbar")]) )                       
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()

    def CompactTempSTATUS(self):
        elements = []
        temps = []

        # TEMP0
        try:
            value = self.GET_TEMP0_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 0"))

        # TEMP1
        try:
            value = self.GET_TEMP1_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 1"))

        # TEMP2
        try:
            value = self.GET_TEMP2_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 2"))
        elements.append(StatusHelper.Line("4k stage",temps))

        temps = []
        # TEMP3
        try:
            value = self.GET_TEMP3_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 3"))

        # TEMP4
        try:
            value = self.GET_TEMP4_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 4"))
        temps.append(StatusHelper.ValueUnit(""))
        elements.append(StatusHelper.Line("4k stage",temps))

        temps = []
        # TEMP5
        try:
            value = self.GET_TEMP5_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 5"))

        # TEMP6
        try:
            value = self.GET_TEMP6_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 6"))

        # TEMP7
        try:
            value = self.GET_TEMP7_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 7"))
        elements.append(StatusHelper.Line("15k stage",temps))
      
        temps = []

        # TEMP8
        try:
            value = self.GET_TEMP8_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 8"))
        temps.append(StatusHelper.ValueUnit(""))
        temps.append(StatusHelper.ValueUnit(""))
        elements.append(StatusHelper.Line("15k stage",temps))

        temps = []
        # TEMP9
        try:
            value = self.GET_TEMP9_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label=" 9"))
        
        # TEMP10
        try:
            value = self.GET_TEMP10_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label="10"))
        
        # TEMP11
        try:
            value = self.GET_TEMP11_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label="11"))
        elements.append(StatusHelper.Line("110k stage",temps))
        
        temps = []
        # TEMP12
        try:
            value = self.GET_TEMP12_TEMP()[0];
            value = "%.2f" % value
        except:
            value = "N/A"
        temps.append(StatusHelper.ValueUnit(value, "K", label="12"))
        temps.append(StatusHelper.ValueUnit(""))
        temps.append(StatusHelper.ValueUnit(""))
        elements.append(StatusHelper.Line("110k stage",temps))
        return elements
        

    def CompactPressureSTATUS(self):
        # SENSOR0
        try:
            value0 = self.GET_VACUUM_GAUGE_SENSOR0_PRESSURE()[0]
            value0_mbar = "%.4e" % (value0 / 100.0)
        except:
            value0_mbar = "N/A"

        # SENSOR1
        try:
            value1 = self.GET_VACUUM_GAUGE_SENSOR1_PRESSURE()[0]
            value1_mbar = "%.2f" % (value1 / 100.0)
        except:
            value1_mbar = "N/A"
        return StatusHelper.Line("Pressure", [StatusHelper.ValueUnit(value0_mbar, "mbar", label="0"),
                                       StatusHelper.ValueUnit(value1_mbar, "mbar", label="1")])

