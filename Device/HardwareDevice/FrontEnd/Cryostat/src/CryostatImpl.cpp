/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File CryostatImpl.cpp
 *
 * $Id$
 */

#include <CryostatImpl.h>
#include <unistd.h> //quick fix

/**
 *-----------------------
 * Cryostat Constructor
 *-----------------------
 */
CryostatImpl::CryostatImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    CryostatBase(name,cs),
    pumpingSubstate_m(Control::Cryostat::UNKNOWN),
    pumpingThread_p(NULL),
    temp0cache_m(-1.0),
    temp1cache_m(-1.0),
    temp2cache_m(-1.0),
    temp3cache_m(-1.0),
    temp4cache_m(-1.0),
    temp5cache_m(-1.0),
    temp6cache_m(-1.0),
    temp7cache_m(-1.0),
    temp8cache_m(-1.0),
    temp9cache_m(-1.0),
    temp10cache_m(-1.0),
    temp11cache_m(-1.0),
    temp12cache_m(-1.0)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    assembly_m = "Cryostat";
    hasEsn_m = true;
}

/**
 *-----------------------
 * Cryostat Destructor
 *-----------------------
 */
CryostatImpl::~CryostatImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

///////////////////////////////////////
// Additional methods for Cryostat
///////////////////////////////////////

// Rough pump until this limit [pascal]
const float CryostatImpl::EXT_ROUGHING_PUMP_PRESS_THRESHOLD = 400.0;
// Turbo pump until this limit [pascal]
const float CryostatImpl::COOLING_PRESS_THRESHOLD           = 5.0E-2;
// Below this temperature [K] we are ready to start with cooling
const float CryostatImpl::CRYOCOOLER_STAGES_TEMP_THRESHOLD  = 30.0;

void CryostatImpl::initPumping()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // Check that we are currently not PUMPING
    if (pumpingSubstate_m == Control::Cryostat::PUMPING) {
        CryostatExceptions::IncorrectSubstateExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Can not initialize while substate is PUMPING";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIncorrectSubstateEx();
    }

    // Stop pumping if the backing pump is on
    try {
        ACS::Time timestamp;
        if (getBackingPumpEnable(timestamp) == 1) {
            stopPumpingHelper();
        }
        // Enable the vacuum gauge
        setCntlVacuumGaugeOn();
    } catch(CryostatExceptions::DeviceAccessExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    } catch(CryostatExceptions::TimeoutExImpl& ex) {
        CryostatExceptions::TimeoutExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getTimeoutEx();
    } catch(CryostatExceptions::ConditionsNotMetExImpl& ex) {
        CryostatExceptions::ConditionsNotMetExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getConditionsNotMetEx();
    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    }

    pumpingSubstate_m = Control::Cryostat::INIT;

    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_INFO, "Cryostat pumping initialized"));
}

void CryostatImpl::startPumping()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // Check that the current pumping substate is INIT
    if (pumpingSubstate_m != Control::Cryostat::INIT) {
        CryostatExceptions::IncorrectSubstateExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Can not start pumping, substate must be INIT";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIncorrectSubstateEx();
    }

    // Check that sensor0 pressure is below the threshold
    // Note: here we do not check for isExtRoughPumpingPressReached()
    // exceptions as these should be thrown up transparently
    if (!isExtRoughPumpingPressReached()) {
        CryostatExceptions::ConditionsNotMetExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Pressure is still too high, can not start pumping";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getConditionsNotMetEx();
    }
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_INFO, "External roughing pump pressure reached"));

    pumpingSubstate_m = Control::Cryostat::PUMPING;

    // Enable the backing pump
    try {
        setCntlBackingPumpOn();
    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    }
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_INFO, "Backing pump turned on"));

    // Spawn PumpingThread to follow up asynchronously
    try {
        CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
        std::string tcompname(compName.in());
        tcompname+="PumpingThread";
        pumpingThread_p = getContainerServices()->getThreadManager()->create<PumpingThread, CryostatImpl>(tcompname.c_str(), *this);

        // Check if the pumpingThread_p is NULL
        if (pumpingThread_p == NULL) {
            CryostatExceptions::ThreadErrorExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Pumping thread pointer is NULL";
            ex.addData("Detail", msg);
            ex.log();
            pumpingSubstate_m = Control::Cryostat::ERROR;
            throw ex.getThreadErrorEx();
        }

        // Start thread (oneshot)
        if (!pumpingThread_p->resume()) {
            // Operation was not successful
            CryostatExceptions::ThreadErrorExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Could not start pumping thread";
            ex.addData("Detail", msg);
            ex.log();
            pumpingSubstate_m = Control::Cryostat::ERROR;
            throw ex.getThreadErrorEx();
        }
    } catch (acsthreadErrType::ThreadAlreadyExistExImpl& ex) {
        CryostatExceptions::ThreadErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getThreadErrorEx();
    } catch (acsthreadErrType::CanNotCreateThreadExImpl& ex) {
        CryostatExceptions::ThreadErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getThreadErrorEx();
    }
}

CryostatImpl::PumpingThread::PumpingThread(const ACE_CString& name, CryostatImpl& Cryostat) :
    ACS::Thread(name),
    cryostatDevice_p(Cryostat)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void CryostatImpl::PumpingThread::run()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    int time_counter, timeout;

    try {

        // Wait until the pressure is below the expected threshold
        time_counter = 0;
        timeout = 60; // seconds
        while ( (cryostatDevice_p.getVacuumGaugeSensor1Pressure(timestamp) > CryostatImpl::EXT_ROUGHING_PUMP_PRESS_THRESHOLD) && (time_counter < timeout) ) {
            usleep(10000000);
            time_counter = time_counter + 10;
        }
        if (time_counter >= timeout) {
            CryostatExceptions::TimeoutExImpl
        ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Timeout when waiting for pressure dropping below expected level (backing pump). Manual recovery required.";
            ex.addData("Detail", msg);
            cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::ERROR;
            // Inside the thread we just log the exception and return
            ex.log();
            return;
        }
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Pressure is now below the expected rough pumping threshold"));

        // Open solenoid valve
        cryostatDevice_p.setCntlSolenoidValveOpen();

        // ---------------------------------------------------------
        // WORKAROUND bugzilla #96, fix once ticket is resolved

        // TBDWORKAROUND: remove this usleep
        for(int i=0;i<10;i++)
                usleep(1000000);
        // TBDWORKAROUND: uncomment the following lines

        //     // Wait until the solenoid valve is open
        //     time_counter = 0;
        //     timeout            = 10; // seconds
        //     while ( (cryostatDevice_p.getSolenoidValveState(timestamp) != 1) && (time_counter < timeout) ) {
        //         usleep(1000000);
        //         time_counter = time_counter + 1;
        //     }
        //     if (time_counter >= timeout) {
        //         CryostatExceptions::TimeoutExImpl
        //             ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        //         string msg = "Timeout when waiting for solenoid valve to open. Manual recovery required.";
        //         ex.addData("Detail", msg);
        //         cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::ERROR;
        //         // Inside the thread we just log the exception and return
        //         ex.log();
        //         return;
        //     }

        // END OF WORKAROUND
        // ---------------------------------------------------------

        // TBD exceptions & error substate
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Solenoid valve opened"));

        // Wait until the pressure is below the expected threshold

        time_counter = 0;
        timeout = 60; // seconds
        while ( (cryostatDevice_p.getVacuumGaugeSensor1Pressure(timestamp) > CryostatImpl::EXT_ROUGHING_PUMP_PRESS_THRESHOLD) && (time_counter < timeout) ) {
            usleep(10000000);
            time_counter = time_counter + 10;
        }
        if (time_counter >= timeout) {
            CryostatExceptions::TimeoutExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Timeout when waiting for pressure dropping below expected level (with solenoid valve open). Manual recovery required.";
            ex.addData("Detail", msg);
            cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::ERROR;
            // Inside the thread we just log the exception and return
            ex.log();
            return;
        }

        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Pressure is now below the expected rough pumping threshold"));

        // Enable the turbo pump
        cryostatDevice_p.setCntlTurboPumpOn();

        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Turbo pump turned on"));

        // Wait until the turbo pump comes up to speed
        time_counter = 0;
        timeout            = 300; // seconds
        while ( (cryostatDevice_p.getTurboPumpSpeed(timestamp) != 1) && (time_counter < timeout) ) {
            usleep(10000000);
            time_counter = time_counter + 10;
        }
        if (time_counter >= timeout) {
            CryostatExceptions::TimeoutExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Timeout when waiting for turbo pump to come to speed. Manual recovery required.";
            ex.addData("Detail", msg);
            cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::ERROR;
            // Inside the thread we just log the exception and return
            ex.log();
            return;
        }
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Turbo pump is now up to speed"));

        // Open gate valve
        cryostatDevice_p.setCntlGateValveOpen();
        // Wait until the gate valve is open
        time_counter = 0;
        timeout = 30; // seconds
        while ( (cryostatDevice_p.getGateValveState(timestamp) != 1) && (time_counter < timeout) ) {
            usleep(1000000);
            time_counter = time_counter + 1;
        }

        if (time_counter >= timeout) {
            CryostatExceptions::TimeoutExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Timeout when waiting for gate valve to open. Manual recovery required.";
            ex.addData("Detail", msg);
            cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::ERROR;
            // Inside the thread we just log the exception and return
            ex.log();
            return;
        }
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Gate valve opened"));

    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::ERROR;
        // Inside the thread we just log the exceptions and return
        return;
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::ERROR;
        // Inside the thread we just log the exceptions and return
        return;
    }

    cryostatDevice_p.pumpingSubstate_m = Control::Cryostat::TURBO_PUMPING;

    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_INFO,"Cryostat pumping started"));
}

void CryostatImpl::stopPumping()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // Check that we are currently not PUMPING
    if (pumpingSubstate_m == Control::Cryostat::PUMPING) {
        CryostatExceptions::IncorrectSubstateExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Can not stop pumping while substate is PUMPING";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIncorrectSubstateEx();
    }

    try {
        // Check that the cooler stages temperatures are below 30K
        if (!isCryoStagesTempReached()) {
            CryostatExceptions::ConditionsNotMetExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Cryocooler stages temperatures are still too high, can not stop pumping";
            ex.addData("Detail", msg);
            ex.log();
            throw ex.getConditionsNotMetEx();
        }

        // Stop pumping
        stopPumpingHelper();

    } catch(CryostatExceptions::DeviceAccessExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    } catch(CryostatExceptions::TimeoutExImpl& ex) {
        CryostatExceptions::TimeoutExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getTimeoutEx();
    } catch(CryostatExceptions::ConditionsNotMetExImpl& ex) {
        CryostatExceptions::ConditionsNotMetExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getConditionsNotMetEx();
    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        pumpingSubstate_m = Control::Cryostat::ERROR;
        throw newEx.getDeviceAccessEx();
    }

    pumpingSubstate_m = Control::Cryostat::COOLING;

    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_INFO,"Cryostat pumping stopped, now cooling"));
}

void CryostatImpl::stopPumpingHelper()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // Helper variables for timeout checks
    int time_counter, timeout;
    ACS::Time timestamp;

    try {
        // Close the gate valve
        setCntlGateValveClosed();

        // Wait until the gate valve is closed
        time_counter = 0;
        timeout            = 30; // seconds
        while ( (getGateValveState(timestamp) != 0) && (time_counter < timeout) ) {
            usleep(1000000);
            time_counter = time_counter + 1;
        }
        if (time_counter >= timeout) {
            CryostatExceptions::TimeoutExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Timeout when waiting for gate valve to close. Manual recovery required.";
            ex.addData("Detail", msg);
            throw ex;
        }
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Gate valve closed"));

        // ------------------------------------------------------------
        // WORKAROUND -- change once bugzilla ticket #96 is fixed
        // Close the solenoid valve, if not closed already

        // TBDWORKAROUND: remove this comment
        //if (getSolenoidValveState(timestamp) != 0) {
        setCntlSolenoidValveClosed();
        //}

        // Wait until the solenoid valve is closed

        // TBDWORKAROUND: remove this usleep
        usleep(1000000);

        // TBDWORKAROUND: uncomment the following lines
        //     time_counter = 0;
        //     timeout            = 10; // seconds
        //     while ( (getSolenoidValveState(timestamp) != 0) && (time_counter < timeout) ) {
        //         usleep(1000000);
        //         time_counter = time_counter + 1;
        //     }
        //     if (time_counter >= timeout) {
        //         CryostatExceptions::TimeoutExImpl
        //             ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        //         string msg = "Timeout when waiting for solenoid valve to close. Manual recovery required.";
        //         ex.addData("Detail", msg);
        //         throw ex;
        //     }

        // END OF WORKAROUND
        // ------------------------------------------------------------

        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Solenoid valve closed"));

        // Disable the turbo pump
        setCntlTurboPumpOff();

        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Turbo pump turned off"));

        // Confirm that the valves are closed and that the turbo pump is off
        ACS::Time timestamp1, timestamp2, timestamp3;
        if ( (getTurboPumpEnable(timestamp1) != 0) ||
            (getGateValveState(timestamp2) != 0) ||
            (getSolenoidValveState(timestamp3) != 0) ) {
            CryostatExceptions::ConditionsNotMetExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "Valves might be open and/or the turbo pump is still on. Manual recovery required.";
            ex.addData("Detail", msg);
            throw ex;
        }

        // Disable the backing pump
        setCntlBackingPumpOff();
        // TBD exceptions & error substate
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_INFO, "Backing pump turned off"));

    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx;
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx;
    }

}

bool CryostatImpl::isExtRoughPumpingPressReached()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        ACS::Time timestamp;
        if (getVacuumGaugeSensor0Pressure(timestamp) < EXT_ROUGHING_PUMP_PRESS_THRESHOLD) {
            return true;
        } else {
            return false;
        }
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    }
}

bool CryostatImpl::isCoolingPressReached()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        ACS::Time timestamp;
        if (getVacuumGaugeSensor0Pressure(timestamp) < COOLING_PRESS_THRESHOLD) {
            return true;
        } else {
            return false;
        }
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    }
}

bool CryostatImpl::isCryoStagesTempReached()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        ACS::Time timestamp1, timestamp2;
        if ( (getTemp0Temp(timestamp1) < CRYOCOOLER_STAGES_TEMP_THRESHOLD) &&
            (getTemp6Temp(timestamp2) < CRYOCOOLER_STAGES_TEMP_THRESHOLD) ) {
            return true;
        } else {
            return false;
        }
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
/*    WORKAROUND TO BE REMOVED WHEN THE FE FIRMWARE GET FIXED    */
            ACE_CString error = ex.getData("ErrorCode");
            if(error==ACE_CString("250"))
        return false;

        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    }
}

bool CryostatImpl::isWarmUpTempReached(double temperature)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        ACS::Time timestamp1, timestamp2, timestamp3;
        if ( (getTemp0Temp(timestamp1) > temperature) &&
         (getTemp6Temp(timestamp2) > temperature) &&
         (getTemp9Temp(timestamp3) > temperature) ) {
            return true;
        } else {
            return false;
        }
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
/*    WORKAROUND TO BE REMOVED WHEN THE FE FIRMWARE GET FIXED    */
            ACE_CString error = ex.getData("ErrorCode");
            if(error==ACE_CString("250"))
                return false;

        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    } catch(ControlExceptions::INACTErrorExImpl& ex) {
        CryostatExceptions::DeviceAccessExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getDeviceAccessEx();
    }
}

Control::Cryostat::PumpingSubstate
CryostatImpl::getPumpingSubstate()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return pumpingSubstate_m;
}

float CryostatImpl::getTemp0Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp0cache_m=CryostatBase::getTemp0Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp0cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp0cache_m;
}

float CryostatImpl::getTemp1Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp1cache_m=CryostatBase::getTemp1Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp1cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp1cache_m;
}

float CryostatImpl::getTemp2Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp2cache_m=CryostatBase::getTemp2Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp2cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp2cache_m;
}

float CryostatImpl::getTemp3Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp3cache_m=CryostatBase::getTemp3Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp3cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp3cache_m;
}

float CryostatImpl::getTemp4Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp4cache_m=CryostatBase::getTemp4Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp4cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp4cache_m;
}

float CryostatImpl::getTemp5Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp5cache_m=CryostatBase::getTemp5Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp5cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp5cache_m;
}

float CryostatImpl::getTemp6Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp6cache_m=CryostatBase::getTemp6Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp6cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp6cache_m;
}

float CryostatImpl::getTemp7Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp7cache_m=CryostatBase::getTemp7Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp7cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp7cache_m;
}

float CryostatImpl::getTemp8Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp8cache_m=CryostatBase::getTemp8Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp8cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp8cache_m;
}

float CryostatImpl::getTemp9Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp9cache_m=CryostatBase::getTemp9Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp9cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp9cache_m;
}

float CryostatImpl::getTemp10Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp10cache_m=CryostatBase::getTemp10Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp10cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp10cache_m;
}

float CryostatImpl::getTemp11Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp11cache_m=CryostatBase::getTemp11Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
                usleep(1);
        continue;
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp11cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp11cache_m;
}

float CryostatImpl::getTemp12Temp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    int retry = 21;
    while(retry>=0){
        try{
            temp12cache_m=CryostatBase::getTemp12Temp(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250")){
        throw ex;
            }
            retry--;
            if(retry>0){
        //Chew the exception and read again
        continue;
                usleep(1);
            }else {
        //If we have never been able to read the value except,
                //else return the cached value
                if(temp12cache_m == -1.0)
                throw ex;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp12cache_m;
}

// ------------------------------------------------------------------------
// WORKAROUND: vacuum sensors do not yet reply CAMB error 250

// TBDWORKAROUND: uncomment the following lines once resolved and
// remove the previous methods

// float CryostatImpl::getVacuumGaugeSensor0Pressure(ACS::Time& timestamp)
// throw(ControlExceptions::CAMBErrorExImpl,
//                 ControlExceptions::INACTErrorExImpl)
// {
//         int retry = 15;
//         float value=999999;
//         while(retry>=0){
//                 try{
//                         value=CryostatBase::getVacuumGaugeSensor0Pressure(timestamp);
//                 }catch(ControlExceptions::CAMBErrorExImpl &ex){
//                         ACE_CString error = ex.getData("ErrorCode");
//                         if(error!=ACE_CString("250")){
//                                 throw ex;
//                         }
//                         if(retry>0){
//                                 //Chew the exception and read again
//                                 retry--;
//        continue;
//                         }else {
//                                 //We have tried enough. bubble up the exception.
//                                 throw ex;
//                         }
//                 }
//                 //if everything is OK. break the loop.
//                 break;
//         }
//         return value;
// }

// float CryostatImpl::getVacuumGaugeSensor1Pressure(ACS::Time& timestamp)
// throw(ControlExceptions::CAMBErrorExImpl,
//                 ControlExceptions::INACTErrorExImpl)
// {
//         int retry = 15;
//         float value=999999;
//         while(retry>=0){
//                 try{
//                         value=CryostatBase::getVacuumGaugeSensor1Pressure(timestamp);
//                 }catch(ControlExceptions::CAMBErrorExImpl &ex){
//                         ACE_CString error = ex.getData("ErrorCode");
//                         if(error!=ACE_CString("250")){
//                                 throw ex;
//                         }
//                         if(retry>0){
//                                 //Chew the exception and read again
//                                 retry--;
//        continue;
//                         }else {
//                                 //We have tried enough. bubble up the exception.
//                                 throw ex;
//                         }
//                 }
//                 //if everything is OK. break the loop.
//                 break;
//         }
//         return value;
// }

// END OF WORKAROUND
// ------------------------------------------------------------------------

float CryostatImpl::getVacuumGaugeSensor0Pressure(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    float value = 9999.9;
    try{
        value=CryostatBase::getVacuumGaugeSensor0Pressure(timestamp);
    } catch(ControlExceptions::CAMBErrorExImpl &ex) {
        usleep(1);
    }
    try{
        value=CryostatBase::getVacuumGaugeSensor0Pressure(timestamp);
    } catch(ControlExceptions::CAMBErrorExImpl &ex) {
        usleep(1);
    }
    try{
        value=CryostatBase::getVacuumGaugeSensor0Pressure(timestamp);
    } catch(ControlExceptions::CAMBErrorExImpl &ex) {
        usleep(1);
    }

    value=CryostatBase::getVacuumGaugeSensor0Pressure(timestamp);
    return value;
}

float CryostatImpl::getVacuumGaugeSensor1Pressure(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    float value = 9999.9;//Phony initialization just as a precaution
    try{
        value=CryostatBase::getVacuumGaugeSensor1Pressure(timestamp);
    } catch(ControlExceptions::CAMBErrorExImpl &ex) {
        usleep(1);
    }
    try{
        value=CryostatBase::getVacuumGaugeSensor1Pressure(timestamp);
    } catch(ControlExceptions::CAMBErrorExImpl &ex) {
        usleep(1);
    }
    try{
        value=CryostatBase::getVacuumGaugeSensor1Pressure(timestamp);
    } catch(ControlExceptions::CAMBErrorExImpl &ex) {
        usleep(1);
    }
    value=CryostatBase::getVacuumGaugeSensor1Pressure(timestamp);
    return value;
}
// ------------- end of temporary hack

// Deal with the monitoring of disabled monitoring points (error 253) ---//
// COMP-2567

unsigned char CryostatImpl::getTurboPumpEnable(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(getBackingPumpEnable(timestamp) == 1){
        return CryostatBase::getTurboPumpEnable(timestamp);
    }
    return 253;//(-3 Hardware Blocked)
}


unsigned char CryostatImpl::getTurboPumpState(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(getBackingPumpEnable(timestamp) == 1){
        return CryostatBase::getTurboPumpState(timestamp);
    }
    return 253;//(-3 Hardware Blocked)
}

unsigned char CryostatImpl::getTurboPumpSpeed(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(getBackingPumpEnable(timestamp) == 1){
        return CryostatBase::getTurboPumpSpeed(timestamp);
    }
    return 253;//(-3 Hardware Blocked)
}

unsigned char CryostatImpl::getGateValveState(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(getBackingPumpEnable(timestamp) == 1){
        return CryostatBase::getGateValveState(timestamp);
    }
    return 253;//(-3 Hardware Blocked)
}

unsigned char CryostatImpl::getSolenoidValveState(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(getBackingPumpEnable(timestamp) == 1){
         return CryostatBase::getSolenoidValveState(timestamp);
    }
    return 253;//(-3 Hardware Blocked)
}

float CryostatImpl::getSupplyCurrent230v(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(getBackingPumpEnable(timestamp) == 1){
        return CryostatBase::getSupplyCurrent230v(timestamp);
    }
    return -3;//(-3 Hardware Blocked)
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(CryostatImpl)
