
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File CryostatHWSimImpl.cpp
 *
 * $Id$
 */

#include "CryostatHWSimImpl.h"
#include <cstdlib>

using namespace AMB;

const unsigned int CryostatHWSimImpl::TEMP0_TEMP = 0x1;
const unsigned int CryostatHWSimImpl::TEMP1_TEMP = 0x2;
const unsigned int CryostatHWSimImpl::TEMP2_TEMP = 0x4;
const unsigned int CryostatHWSimImpl::TEMP3_TEMP = 0x8;
const unsigned int CryostatHWSimImpl::TEMP4_TEMP = 0x10;
const unsigned int CryostatHWSimImpl::TEMP5_TEMP = 0x20;
const unsigned int CryostatHWSimImpl::TEMP6_TEMP = 0x40;
const unsigned int CryostatHWSimImpl::TEMP7_TEMP = 0x80;
const unsigned int CryostatHWSimImpl::TEMP8_TEMP = 0x100;
const unsigned int CryostatHWSimImpl::TEMP9_TEMP = 0x200;
const unsigned int CryostatHWSimImpl::TEMP10_TEMP = 0x400;
const unsigned int CryostatHWSimImpl::TEMP11_TEMP = 0x800;
const unsigned int CryostatHWSimImpl::TEMP12_TEMP = 0x1000;
const unsigned int CryostatHWSimImpl::TEMP_RETRY = 0; //make > 0 for bad behavoiur
/* Please use this class to implement complex functionality for the
 * CryostatHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

CryostatHWSimImpl::CryostatHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: CryostatHWSimBase::CryostatHWSimBase(node, serialNumber),
    monitorPoint_m(0),
    tcount_m(0)
{
    initialize(node, serialNumber);
}
//Implementation of the GET_SETUP_INFO special monitor point.
const rca_t CryostatHWSimImpl::GET_SETUP_INFO = 0x20001U;


void CryostatHWSimImpl::initialize(node_t node, const std::vector<CAN::byte_t>& serialNumber)
{

	long long raw = 0LL;
        CryostatHWSimBase::initialize(node, serialNumber);

	if (state_m.find(GET_SETUP_INFO) == state_m.end()) {

		raw = static_cast<long long>(0);

		    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
		    TypeConversion::valueToData(*vvalue, static_cast<unsigned char>(raw), 1);

		state_m.insert(std::make_pair(GET_SETUP_INFO, vvalue));
	}
}

std::vector<CAN::byte_t> CryostatHWSimImpl::monitor(rca_t rca) const
{
        //we look for RCA not RCA - BASEADDRESS, because GET_SETUP_INFO has a fix position.
	switch (rca) {
		/// Specific points
		case GET_SETUP_INFO:
			return getSetupInfo();
			break;

		/// Others
		default:
			return CryostatHWSimBase::monitor(rca);
	}
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getSetupInfo() const {
	return *(state_m.find(GET_SETUP_INFO)->second);
}

void CryostatHWSimImpl::setControlSetupInfo(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"GET_SETUP_INFO");
	if (state_m.find(GET_SETUP_INFO) != state_m.end())
		*(state_m.find(GET_SETUP_INFO)->second) = data;
	else
		throw CAN::Error("Trying to set GET_SETUP_INFO. Member not found.");
}
float CryostatHWSimImpl::simulateTemp(unsigned int stage){
    float origin = 0.0;
    float span = 0.0;
    switch(stage){
        case TEMP0_TEMP:
        case TEMP1_TEMP:
        case TEMP2_TEMP:
        case TEMP3_TEMP:
        case TEMP4_TEMP:
            span = (4.0 - 1.6);
            origin = 1.6;
            break;
        case TEMP5_TEMP:
        case TEMP6_TEMP:
        case TEMP7_TEMP:
        case TEMP8_TEMP:
            span = (18.0 - 10.0);
            origin =  10.0;
            break;
        case TEMP9_TEMP:
        case TEMP10_TEMP:
        case TEMP11_TEMP:
        case TEMP12_TEMP:
            span = (130.0 - 70.0);
            origin = 70.0;
            break;
    }
    float jitter = rand() / (RAND_MAX + 1.0);
    int roundoff = (int)((jitter*span + origin) * 100);
    return roundoff / 100.0;

}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp0Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP0_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP0_TEMP) == 0){
        cch->monitorPoint_m = TEMP0_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP0_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP0_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP0_TEMP. Member not found.");
    return *vvalue;
}
std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp1Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP1_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP1_TEMP) == 0){
        cch->monitorPoint_m = TEMP1_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP1_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP1_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP1_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp2Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP2_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP2_TEMP) == 0){
        cch->monitorPoint_m = TEMP2_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP2_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP2_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP2_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp3Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP3_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP3_TEMP) == 0){
        cch->monitorPoint_m = TEMP3_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP3_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP3_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP3_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp4Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP4_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP4_TEMP) == 0){
        cch->monitorPoint_m = TEMP4_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP4_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP4_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP4_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp5Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP5_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP5_TEMP) == 0){
        cch->monitorPoint_m = TEMP5_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP5_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP5_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP5_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp6Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP6_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP6_TEMP) == 0){
        cch->monitorPoint_m = TEMP6_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP6_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP6_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP6_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp7Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP7_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP7_TEMP) == 0){
        cch->monitorPoint_m = TEMP7_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP7_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP7_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP7_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp8Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP8_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP8_TEMP) == 0){
        cch->monitorPoint_m = TEMP8_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP8_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP8_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP8_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp9Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP9_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP9_TEMP) == 0){
        cch->monitorPoint_m = TEMP9_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP9_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP9_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP9_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp10Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP10_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP10_TEMP) == 0){
        cch->monitorPoint_m = TEMP10_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP10_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP10_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP10_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp11Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP11_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP11_TEMP) == 0){
        cch->monitorPoint_m = TEMP11_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP11_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP11_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP11_TEMP. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getTemp12Temp() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    CryostatHWSimImpl* cch = const_cast<CryostatHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP12_TEMP);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP12_TEMP) == 0){
        cch->monitorPoint_m = TEMP12_TEMP;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMP12_TEMP) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMP12_TEMP)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMP12_TEMP. Member not found.");
    return *vvalue;
}

/**
* backing pump dependant monitor points
*/

std::vector<CAN::byte_t> CryostatHWSimImpl::getMonitorTurboPumpEnable() const {
    std::vector<CAN::byte_t> backingPump = getMonitorBackingPumpEnable();
    std::vector<CAN::byte_t> turboPump = CryostatHWSimBase::getMonitorTurboPumpEnable();
    if(backingPump[0] != 1) {
        /*If the backing punp is diabled push error code*/
        turboPump[1] = 253;
    }
    return turboPump;
}

std::vector<CAN::byte_t> CryostatHWSimImpl::getMonitorTurboPumpState() const {
    std::vector<CAN::byte_t> backingPump = getMonitorBackingPumpEnable();
    std::vector<CAN::byte_t> turboPump = CryostatHWSimBase::getMonitorTurboPumpState();
    if(backingPump[0] != 1) {
        /*If the backing punp is diabled push error code*/
        turboPump[1] = 253;
    }
    return turboPump;
}
std::vector<CAN::byte_t> CryostatHWSimImpl::getMonitorTurboPumpSpeed() const {
    std::vector<CAN::byte_t> backingPump = getMonitorBackingPumpEnable();
    std::vector<CAN::byte_t> turboPump = CryostatHWSimBase::getMonitorTurboPumpSpeed();
    if(backingPump[0] != 1) {
        /*If the backing punp is diabled push error code*/
        turboPump[1] = 253;
    }
    return turboPump;
}
std::vector<CAN::byte_t> CryostatHWSimImpl::getMonitorGateValveState() const {
    std::vector<CAN::byte_t> backingPump = getMonitorBackingPumpEnable();
    std::vector<CAN::byte_t> gateValve = CryostatHWSimBase::getMonitorGateValveState();
    if(backingPump[0] != 1) {
        /*If the backing punp is diabled push error code*/
        gateValve[1] = 253;
    }
    return gateValve;
}
std::vector<CAN::byte_t> CryostatHWSimImpl::getMonitorSolenoidValveState() const {
    std::vector<CAN::byte_t> backingPump = getMonitorBackingPumpEnable();
    std::vector<CAN::byte_t> sValve = CryostatHWSimBase::getMonitorSolenoidValveState();
    if(backingPump[0] != 1) {
        /*If the backing punp is diabled push error code*/
        sValve[1] = 253;
    }
    return sValve;
}
std::vector<CAN::byte_t> CryostatHWSimImpl::getMonitorSupplyCurrent230v() const {
    std::vector<CAN::byte_t> backingPump = getMonitorBackingPumpEnable();
    std::vector<CAN::byte_t> supply = CryostatHWSimBase::getMonitorSupplyCurrent230v();
    if(backingPump[0] != 1) {
        /*If the backing punp is diabled push error code*/
        supply[1] = 253;
    }
    return supply;
}

void
CryostatHWSimImpl::setControlSetBackingPumpOn(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetBackingPumpOn(data);
  setMonitorBackingPumpEnable(data);
}

void
CryostatHWSimImpl::setControlSetBackingPumpOff(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetBackingPumpOff(data);
  setMonitorBackingPumpEnable(data);
}

void
CryostatHWSimImpl::setControlSetTurboPumpOn(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetTurboPumpOn(data);
  setMonitorTurboPumpEnable(data);
  setMonitorTurboPumpSpeed(data);
}

void
CryostatHWSimImpl::setControlSetTurboPumpOff(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetTurboPumpOff(data);
  setMonitorTurboPumpEnable(data);
  setMonitorTurboPumpSpeed(data);
}

void
CryostatHWSimImpl::setControlSetGateValveOpen(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetGateValveOpen(data);
  setMonitorGateValveState(data);
}

void
CryostatHWSimImpl::setControlSetGateValveClosed(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetGateValveClosed(data);
  setMonitorGateValveState(data);
}

void
CryostatHWSimImpl::setControlSetSolenoidValveOpen(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetSolenoidValveOpen(data);
  setMonitorSolenoidValveState(data);
}

void
CryostatHWSimImpl::setControlSetSolenoidValveClosed(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetSolenoidValveClosed(data);
  setMonitorSolenoidValveState(data);
}

void
CryostatHWSimImpl::setControlSetVacuumGaugeOn(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetVacuumGaugeOn(data);
  setMonitorVacuumGaugeEnable(data);
}

void
CryostatHWSimImpl::setControlSetVacuumGaugeOff(const std::vector< CAN::byte_t >& data)
{
  CryostatHWSimBase::setControlSetVacuumGaugeOff(data);
  setMonitorVacuumGaugeEnable(data);
}
