#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for an ACD device.
"""

import CCL.ACDBase
from CCL import StatusHelper
from CCL.logging import getLogger
from log_audience import OPERATOR
import ACSLog
import os
from time import sleep
from time import time

class ACD(CCL.ACDBase.ACDBase):
    '''
    The ACD class inherits from the code generated ACDBase
    class and adds specific methods.
    '''

    '''
    The ACD class inherits from the code generated ACDBase
    class and adds specific methods.
    '''
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        The constructor creates a ACD object by making use of
        the ACDBase constructor.

        EXAMPLE:
        from CCL.ACD import ACD
        acd = ACD("DA41")
        
        acd.getHwState() #it must be in Operational

        acd.SET_HOT_LOAD(7)
        acd.STATUS()

        acd.SET_LOAD0_XY([0.05,-0.10])
        acd.STATUS()
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/ACD"
            antennaName = None

        CCL.ACDBase.ACDBase.__init__(self, antennaName, componentName,
                                           stickyFlag)
        self.cartridgeNames = ['stow',
                               'band 1',
                               'band 2',
                               'band 3',
                               'band 4',
                               'band 5',
                               'band 6',
                               'band 7',
                               'band 8',
                               'band 9',
                               'band 10',
                               'wvr',
                               'park0',
                               'park1',
                               'not aligned',
                               'undefined']
        self.loadNames = ['ambient', 'hot', 'solar filter','quarter wave plate']

        # Status Byte 1:
        #  bits 0-3 = cartridge number
        #  bits 4-5 = address of the loads
        #  bit 6    = error on X/Y position
        #  bit 7    = in-position bit
        self.cartridgeMask  = 0x0F
        self.loadMask = 0x30
        self.xyErrorMask = 0x40
        self.inPositionMask = 0x80

        # Status Byte 2:
        #  bit 0: set if SET_LOADi_XY attempted a displacement out-of-range last time it was executed
        #  bit 1: set if SET_LOADi_dXdY attempted a displacement out-of-range last time it was executed
        #  bit 2: set if SET_ARMi attempted a displacement out-of-range last time it was executed
        #  bit 3: set if CAN error on last communication
        #  bit 4: set if arm motor not in position
        #  bit 5: set if wheel motor not in position
        #  bit 6: set if qwp motor not in position
        #  bit 7: set if last displacement attempt was made while motor was not in position (FW 2.7)
        #         set if last command was given with wrong number of bytes (FW 2.9)
        self.setLoadiXYOORMask = 0x01
        self.setLoadidXdYOORMask = 0x02
        self.setArmiOORMask = 0x04
        self.canErrorMask = 0x08
        self.armErrorMask = 0x10
        self.wheelErrorMask = 0x20
        self.qwpErrorMask = 0x40
        self.wrongNrBytesMask = 0x80

        self.stHLIdle              = 0x01
        self.stHLActive            = 0x02
        self.stHLReset             = 0x04
        self.stHLError             = 0x08
        self.stHLOutOfRange        = 0x10
        self.stHLNoStatusAvailable = 0x20
        
        # error-messages for byte2: [[<mask1>, error-msg1], [<mask2>, error-msg2], ...]
        self.errorMsgsByte2 = [[self.setLoadiXYOORMask,   'SET_LOADi_XY   displacement out of range'],
                               [self.setLoadidXdYOORMask, 'SET_LOADi_dXdY displacement out of range'],
                               [self.setArmiOORMask,      'SET_ARMi       displacement out of range'],
                               [self.canErrorMask,        'CAN communication error'],
                               [self.armErrorMask,        'Arm motor blocked'],
                               [self.wheelErrorMask,      'Wheel motor blocked'],
                               [self.qwpErrorMask,        'QWP motor blocked'],
                               [self.wrongNrBytesMask,    'Last command had wrong number of bytes']]

    def __del__(self):
        CCL.ACDBase.ACDBase.__del__(self)

    def SET_INIT(self):
        '''
        This method overrides the code-generated SET_INIT() method
        from the base class. It does that so it can wait for the
        initialization to be finished after the INIT command has
        been sent to the ACD.
        '''
        result = CCL.ACDBase.ACDBase.SET_INIT(self)
        elapsed = self.waitTillInPosition(27.0)
        return result
        
    def INIT(self):
        '''
        Initialization ACD.
        '''
        result = self._HardwareDevice__hw.SET_INIT()
        return result

    def RESET(self):
        '''
        Reset ACD.
        '''
        result = self._HardwareDevice__hw.RESET()
        return result

    def getStatus(self):
        '''
        Get the status monitor point and analyse the bits
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            try:
                [byte1,byte2] = self._devices[key].GET_STATUS()[0]
            except:
                byte1 = 0x00
                byte2 = 0xFF

            cartridge = byte1 & self.cartridgeMask
            load = byte1 & self.loadMask
            xyError = byte1 & self.xyErrorMask
            if (byte1 & self.inPositionMask) == self.inPositionMask :
                inPosition = True
            else:
                inPosition = False
    
            setLoadiXYOOR = byte2 & self.setLoadiXYOORMask
            setLoadidXdYOOR = byte2 & self.setLoadidXdYOORMask
            setArmiOOR = byte2 & self.setArmiOORMask
            canError = byte2 & self.canErrorMask
            armError = byte2 & self.armErrorMask
            wheelError = byte2 & self.wheelErrorMask
            qwpError = byte2 & self.qwpErrorMask
            wrongNrBytes = byte2 & self.wrongNrBytesMask

            result[key] = {'cartridge': cartridge, 'load' : load, 'xyError' : xyError,
                           'inPosition' : inPosition, 'setLoadiXYOOR' : setLoadiXYOOR,
                           'setLoadidXdYOOR' : setLoadidXdYOOR, 'setArmiOOR' : setArmiOOR,
                           'canError' : canError, 'armError' : armError, 'wheelError' : wheelError,
                           'qwpError' : qwpError, 'wrongNrBytes' : wrongNrBytes}

        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def waitTillInPosition(self, timeout):
        '''
        Wait till the in-position bit of the status monitor point is set
        '''
        elapsed = self._HardwareDevice__hw.waitTillInPosition(timeout)
        return elapsed    

    def setHotLoadTemperature(self, temp):
        '''
        Init and set heat hot load temperature
        '''
        self._HardwareDevice__hw.setHotLoadTemperature(temp)

    def getMonitorList(self):
        '''
        This method extends the monitor list method of base class.
        Monitor points which have not been included by code generator
        should be added to the here
        '''
        missingList = ["MISSING_MONITOR_1",
                       "MISSING_MONITOR_2"]
                       
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[key] = CCL.ACDBase.ACDBase.getMonitorList(self)
            result[key].extend(missingList)
        if len(self._devices) == 1:
            return result.values()[0]
        return result    

    def STATUS(self):
        '''
        This method will display the status information of the device at
        the standard output.
        '''

        os.system("clear")
        status = []
        temp   = []
        xyload = []
        arms   = []
        regs   = []
        errmsg = []
        errpos = []
        hlstat = []
        checkErrorMessages = False

        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            # Status information of the calibration device
            status.append( StatusHelper.Separator("Status") )
            try:
                [byte1,byte2] = self._devices[key].GET_STATUS()[0]
                status.append(StatusHelper.Line("Cartridge" ,[ StatusHelper.ValueUnit(self.cartridgeNames[byte1 & self.cartridgeMask]) ]) );
                status.append(StatusHelper.Line("Load" ,     [ StatusHelper.ValueUnit(self.loadNames[(byte1 & self.loadMask) >> 4]) ]) );
                checkErrorMessages = True
            except:
                status.append(StatusHelper.Line("Cartridge" ,[ StatusHelper.ValueUnit('N/A') ]) );
                status.append(StatusHelper.Line("Load" ,     [ StatusHelper.ValueUnit('N/A') ]) );
                checkErrorMessages = False
            # Error messages
            if checkErrorMessages:
                errmsg.append( StatusHelper.Separator("Error Messages") )

                # Check if all three motors are in-position
                if not (byte1 & self.inPositionMask) :
                    errmsg.append( StatusHelper.Line ( "At least one motor is not \'in-position\'" ) );
                    
                # Check for errors (2nd status byte)
                for i in range(len(self.errorMsgsByte2)) :
                    if (byte2 & self.errorMsgsByte2[i][0]) :
                        errmsg.append(StatusHelper.Line(self.errorMsgsByte2[i][1]))
                        
            # X and Y positions of the loads
            xyload.append( StatusHelper.Separator("X-Y load position") )
            try:
                pos = self._devices[key].GET_LOAD0_XY()[0]
                pos= [ "%.3f" % float(pos[0]*1000), "%.3f" % float(pos[1]*1000) ]
                xyload.append( StatusHelper.Line ("Ambient load", [ StatusHelper.ValueUnit(pos[0], "mm"),
                                                                    StatusHelper.ValueUnit(pos[1], "mm") ]) );
            except:
                pos = ["N/A"]*2
                xyload.append( StatusHelper.Line ("Ambient Load", [ StatusHelper.ValueUnit(pos[0]),
                                                                    StatusHelper.ValueUnit(pos[1]) ]) );

            try:
                pos = self._devices[key].GET_LOAD1_XY()[0]
                pos= [ "%.3f" % float(pos[0]*1000), "%.3f" % float(pos[1]*1000) ]
                xyload.append( StatusHelper.Line ("Hot load", [ StatusHelper.ValueUnit(pos[0], "mm"),
                                                                StatusHelper.ValueUnit(pos[1], "mm") ]) );
            except:
                pos = ["N/A"]*2
                xyload.append( StatusHelper.Line ("Hot Load", [ StatusHelper.ValueUnit(pos[0]),
                                                                StatusHelper.ValueUnit(pos[1]) ]) );

            try:
                pos = self._devices[key].GET_LOAD2_XY()[0]
                pos= [ "%.3f" % float(pos[0]*1000), "%.3f" % float(pos[1]*1000) ]
                xyload.append( StatusHelper.Line ("Solar filter", [ StatusHelper.ValueUnit(pos[0], "mm"),
                                                                    StatusHelper.ValueUnit(pos[1], "mm") ]) );
            except:
                pos = ["N/A"]*2
                xyload.append( StatusHelper.Line ("Solar filter", [ StatusHelper.ValueUnit(pos[0]),
                                                                    StatusHelper.ValueUnit(pos[1]) ]) );

            # Encoder position for the motors 1, 2, 3
            arms.append( StatusHelper.Separator("Encoder Position") )
            try:
                arm = self._devices[key].GET_ARM0()[0]
                arms.append ( StatusHelper.Line("ARM0", [ StatusHelper.ValueUnit(arm,"counts") ]) );
            except:
                arms.append(  StatusHelper.Line("ARM0", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                arm = self._devices[key].GET_ARM1()[0]
                arms.append ( StatusHelper.Line("ARM1", [ StatusHelper.ValueUnit(arm,"counts") ]) );
            except:
                arms.append(  StatusHelper.Line("ARM1", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                arm = self._devices[key].GET_ARM2()[0]
                arms.append ( StatusHelper.Line("ARM2", [ StatusHelper.ValueUnit(arm,"counts") ]) );
            except:
                arms.append(  StatusHelper.Line("ARM2", [ StatusHelper.ValueUnit("N/A") ]) );



            # Temperatures of calibration loads
            temp.append( StatusHelper.Separator("Load temperatures") )
            try:
                tmp = "%.3f" % self._devices[key].GET_TEMP01()[0]
                temp.append ( StatusHelper.Line("TEMP01", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMP01", [ StatusHelper.ValueUnit("N/A") ]) );
            try:
                tmp = "%.3f" % self._devices[key].GET_TEMP02()[0]
                temp.append ( StatusHelper.Line("TEMP02", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMP02", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                tmp = "%.3f" % self._devices[key].GET_TEMP11()[0]
                temp.append ( StatusHelper.Line("TEMP11", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMP11", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                tmp = "%.3f" % self._devices[key].GET_TEMP12()[0]
                temp.append ( StatusHelper.Line("TEMP12", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMP12", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                tmp = "%.3f" % self._devices[key].GET_TEMP20()[0]
                temp.append ( StatusHelper.Line("TEMP20", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMP20", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                tmp = "%.3f" % self._devices[key].GET_TEMP21()[0]
                temp.append ( StatusHelper.Line("TEMP21", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMP21", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                tmp = "%.3f" % self._devices[key].GET_TEMP22()[0]
                temp.append ( StatusHelper.Line("TEMP22", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMP22", [ StatusHelper.ValueUnit("N/A") ]) );

            try:
                tmp = "%.3f" % self._devices[key].GET_TEMPLC()[0]
                temp.append ( StatusHelper.Line("TEMPLC", [ StatusHelper.ValueUnit(float(tmp)-273.15, "C"),
                                                            StatusHelper.ValueUnit(tmp, "K") ]) );
            except:
                temp.append ( StatusHelper.Line("TEMPLC", [ StatusHelper.ValueUnit("N/A") ]) );


            # Values of the Registers
            regs.append( StatusHelper.Separator("Registers") )
            try:
                reg = self._devices[key].GET_REG0()[0]
                regs.append( StatusHelper.Line( "REG0", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG0", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            try:
                reg = self._devices[key].GET_REG1()[0]
                regs.append( StatusHelper.Line( "REG1", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG1", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );

            try:
                reg = self._devices[key].GET_REG2()[0]
                regs.append( StatusHelper.Line( "REG2", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG2", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );

            try:
                reg = self._devices[key].GET_REG3()[0]
                regs.append( StatusHelper.Line( "REG3", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG3", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );

            try:
                reg = self._devices[key].GET_REG4()[0]
                regs.append( StatusHelper.Line( "REG4", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG4", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );

            try:
                reg = self._devices[key].GET_REG5()[0]
                regs.append( StatusHelper.Line( "REG5", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG5", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );

            try:
                reg = self._devices[key].GET_REG6()[0]
                regs.append( StatusHelper.Line( "REG6", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG6", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );

            try:
                reg = self._devices[key].GET_REG7()[0]
                regs.append( StatusHelper.Line( "REG7", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );
            except:
                reg = ["N/A"]*4
                regs.append( StatusHelper.Line( "REG7", [ StatusHelper.ValueUnit(reg[0]),
                                                          StatusHelper.ValueUnit(reg[1]),
                                                          StatusHelper.ValueUnit(reg[2]),
                                                          StatusHelper.ValueUnit(reg[3]) ]) );

            # Hot Load Controller status
            hlstat.append( StatusHelper.Separator("HL Controller Status") )
            hlbyte = self._devices[key].GET_HL_STATUS()[0]

            if (hlbyte & self.stHLIdle):
                str = "yes"
            else:
                str = "no"
            hlstat.append( StatusHelper.Line ("Idle               : " + str));

            if (hlbyte & self.stHLActive):
                str = "yes"
            else:
                str = "no"
            hlstat.append( StatusHelper.Line ("Active             : " + str));

            if (hlbyte & self.stHLReset):
                str = "yes"
            else:
                str = "no"
            hlstat.append( StatusHelper.Line ("Reset              : " + str));

            if (hlbyte & self.stHLError):
                str = "yes"
            else:
                str = "no"
            hlstat.append( StatusHelper.Line ("Error              : " + str));

            if (hlbyte & self.stHLOutOfRange):
                str = "yes"
            else:
                str = "no"
            hlstat.append( StatusHelper.Line ("Out of Range       : " + str));

            if (hlbyte & self.stHLNoStatusAvailable):
                str = "yes"
            else:
                str = "no"
            hlstat.append( StatusHelper.Line ("No status available: " + str ));

            lines = status + temp + xyload + arms + regs + errmsg + hlstat
            frame = StatusHelper.Frame(compName + "   Ant: " + antName, lines )
            frame.printScreen()


    def logToOperator(self, priority, logMessage, component):
        '''
        Method which logs a report to with the OPERATOR flag set.
        '''
        #self._logger.logNotSoTypeSafe(priority, "%s: %s" %
        #                              (component, logMessage),
        #                              OPERATOR,
        #                              antenna = component.split('/')[1])
        pass

