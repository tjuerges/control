/* @(#) $Id$
 *
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it it
 * under the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */
#include "ACDImpl.h"
#include <string>
#include <sstream>
#include <maciACSComponentDefines.h>
#include <TETimeUtil.h>

#include <HardwareControllerC.h>

#include <acstimeTimeUtil.h>
#include <FrontEndUtils.h>

#include <phonyXmlTmcdbComponent.h>
#include <configDataAccess.h>

using std::string;
using std::ostringstream;
using namespace maci;

const unsigned int ACDImpl::CYCLE_TIME_DURATION = 6;
const float ACDImpl::DEFAULT_HL_TEMPERATURE = 360.0;
const float ACDImpl::updateThreshold = 1.0;
//------------------------------------------------------------------------------
/**
 *-----------------------
 *ACD Constructor
 *-----------------------
 */

ACDImpl::ACDImpl(const ACE_CString& name, maci::ContainerServices* pCS)
  : ACDBase(name,pCS),
    hotLoadTemp_m(DEFAULT_HL_TEMPERATURE),
    heatEnabled_m(false),
    startCheckTempAlarm_m(false),
    wasBandInPosition_m(false),
    wasLoadInPosition_m(false),
    wasInPosition_m(false),
    updateThreadCycleTime_m( THREAD_CYCLE_TIME ),
    threadCycleCounter_m(0),
    calDeviceReportingEnabled(false),
    calDeviceDataGuard_m(TETimeUtil::ACS_ONE_SECOND * 600, 0),
    lastCommandedBand_m(ReceiverBandMod::UNSPECIFIED),
    lastCommandedLoad_m(CalibrationDeviceMod::NONE)
{
  ACS_TRACE("ACDImpl::ACDImpl");

  /* I use the calDeviceData structure to store the last reported values
     of the Hot and Cold Loads.  This prevents having to initialize it
     repeatedly */

  calDeviceData_m.antennaName=
    CORBA::string_dup(componentToAntennaName(name.c_str()).c_str());
  calDeviceData_m.numCalLoad = 2;
  calDeviceData_m.calLoadNames.length(2);
  calDeviceData_m.calLoadNames[0] = CalibrationDeviceMod::AMBIENT_LOAD;
  calDeviceData_m.calLoadNames[1] = CalibrationDeviceMod::HOT_LOAD;
  calDeviceData_m.temperatureLoad.length(2);
}

//------------------------------------------------------------------------------
/**
 *-----------------------
 * ACD Destructor
 *-----------------------
 */

ACDImpl::~ACDImpl()
{
  ACS_TRACE("ACDImpl::~ACDImpl");
}

//------------------------------------------------------------------------------

void ACDImpl::getDeviceUniqueId(std::string& deviceID)
{
  ACS_TRACE("ACDImpl::getDeviceUniqueId");
  broadcastBasedDeviceUniqueID(deviceID);
}

//-----------------------------------------------------------------------------
// Component Lifecycle Methods
//-----------------------------------------------------------------------------
void ACDImpl::initialize()
{
  ACS_TRACE(__func__);

  try {
    ACDBase::initialize();
  } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__, __func__);
  }

  // Initialize Semaphores
  if (sem_init(&updateThreadSem_m, 0, 1)) {
    ostringstream msg;
    msg << "Error Initializing Semaphore";
    getLogger()->log(Logging::BaseLog::LM_ERROR, msg.str(),
                     __FILE__, __LINE__, __func__);
    throw acsErrTypeLifeCycle::LifeCycleExImpl(__FILE__,__LINE__, __func__);
  }

  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
  std::string threadName(compName.in());
  threadName += "MonitoringThread";

  updateThread_p = getContainerServices()->
    getThreadManager()->
    create<UpdateThread, ACDImpl>
    (threadName.c_str(), *this);

  updateThread_p->setSleepTime(updateThreadCycleTime_m);
  updateThread_p->suspend();
}

void ACDImpl::cleanUp()
{
  ACS_TRACE(__func__);

  updateThread_p->terminate();

  try {
    ACDImpl::clearAlarm();
    ACDBase::cleanUp();
  } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__, __func__);
  }
}

void ACDImpl::hwOperational()
{
  ACS_TRACE(__func__);

  try {
    ACDBase::hwOperational();
  }catch (const acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    acsErrTypeLifeCycle::LifeCycleExImpl nex(ex,__FILE__,__LINE__, __func__);
    throw nex.getLifeCycleEx();
  }

  try {
      //setHotLoadTemperature(DEFAULT_HL_TEMPERATURE);
      lookup();
  } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
      acsErrTypeLifeCycle::LifeCycleExImpl nex(ex,__FILE__,__LINE__, __func__);
      throw nex.getLifeCycleEx();
  }
  catch(const ControlExceptions::INACTErrorExImpl& ex) {
    acsErrTypeLifeCycle::LifeCycleExImpl nex(ex,__FILE__,__LINE__, __func__);
    throw nex.getLifeCycleEx();
  }

  updateThread_p->resume();
}

//-----------------------------------------------------------------------------
// Hardware Lifecycle Interface
//-----------------------------------------------------------------------------
void ACDImpl::hwInitializeAction()
{
    ACS_TRACE(__func__);

    ACDBase::hwInitializeAction();

    updateThread_p ->suspend();
    try {
        lastCommandedBand_m = getCurrentCartrigeInternal();
        lastCommandedLoad_m = getCurrentLoadBareInternal();
    } catch (...) {
        //This will not be deemed as a failure. It just wont have the initial variable and
        // a spurious flagging may be triggered.
        ACS_LOG(LM_SOURCE_INFO, __func__,
                    (LM_WARNING,"Could not determine current ACD load and position."));
    }

    ACDImpl::clearAlarm();
}

void ACDImpl::hwStopAction()
{
  ACS_TRACE(__func__);

  AmbErrorCode_t flushStatus;
  ACS::Time      flushTime;


  updateThread_p->suspend();

  flushNode(0, &flushTime, &flushStatus);
  if (flushStatus != AMBERR_NOERR) {
    ACS_LOG(LM_SOURCE_INFO, __func__,
            (LM_INFO,"Communication failure flushing commands to device"));
  } else {
    ACS_LOG(LM_SOURCE_INFO, __func__,
            (LM_DEBUG,"All commands and monitors flushed at: %lld",flushTime));
  }

  ACDBase::hwStopAction();

  heatEnabled_m = false;
  resetErrorFlags();
}

//-----------------------------------------------------------------------------
// External Interface
//-----------------------------------------------------------------------------
void ACDImpl::lookup()
{
    const char* fnName = "ACDImpl::lookup";
    // 1.- get serial number from hardware
    string deviceID;
    bool set_hot_load_temp = false;

    try {
        getDeviceUniqueId(deviceID);
          deviceID = deviceID.substr(2,deviceID.size()-2);
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Device failed to respond to request for unique ID."));
    }

    // Get the schema and the instance file for this device
    PhonyXmlTmcdbComponent tmcdb;

    std::string xsd = tmcdb.getConfigXsd("ACD");
    std::string xml = tmcdb.getConfigXml(deviceID);

    if (xml.size() == 0){
        // no configuration available for selected device
        // log a warning and return
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_WARNING, "Could not access config data for ACD: S/N %s",deviceID.c_str()));
        return;
    } else if (xsd.size() == 0){
        // no schema available for selected device
        // log a warning and return
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                (LM_WARNING, "Could not access schema ACD.xsd on TMCDB_DATA directory"));
        return;
    }

    try {
        ConfigDataAccessor *configData;
        configData = new ConfigDataAccessor(xml, xsd);

        std::auto_ptr<const ControlXmlParser> hot_load_element = configData->getElement("SET_HOT_LOAD_TEMPERATURE");
        set_hot_load_temp = (*hot_load_element).getBoolAttribute("value");
    }
    catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Failure while accessing config data for S/N: %s.",deviceID.c_str()));
    }

    // 3.- Set HOT LOAD TEMPERATURE acording to file.
    try {
        if (set_hot_load_temp == true ){
            ACS_LOG(LM_SOURCE_INFO,fnName,(LM_DEBUG, "Device S/N: %s set hot load temperature by software: %d.",deviceID.c_str(),(int)set_hot_load_temp));

            try {
                setHotLoadTemperature(DEFAULT_HL_TEMPERATURE);
            } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
                acsErrTypeLifeCycle::LifeCycleExImpl nex(ex,__FILE__,__LINE__, __func__);
                throw nex.getLifeCycleEx();
            }
        }else{
            ACS_LOG(LM_SOURCE_INFO,fnName,(LM_DEBUG, "Device S/N: %s set hot load temperature by firmware. ",deviceID.c_str()));
        }
    }
    catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Communication failure while setting Phase Offset."));
    }
    catch (ControlExceptions::INACTErrorExImpl& ex) {
        ACS_LOG(LM_SOURCE_INFO,fnName,(LM_ERROR, "Invalid State for setting Phase Offset."));
    }
}

// Helper method
void ACDImpl::RESET()
{
  ACS_TRACE(__func__);
  try
    {
      setReset();
      heatEnabled_m = false;
      startCheckTempAlarm_m = false;
      resetErrorFlags();
    }
  catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
      ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
      throw nex.getCAMBErrorEx();
    }
  catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
      ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,__func__);
      throw nex.getINACTErrorEx();
    }
}

void ACDImpl::SET_HEAT(CORBA::Float world)
{
  ACS_TRACE(__func__);

  try
    {
      setCntlHeat(world);
      heatEnabled_m = true;
    }
  catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
      ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
      throw nex.getCAMBErrorEx();
    }
  catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
      ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,__func__);
      throw nex.getINACTErrorEx();
    }

}

CORBA::Long ACDImpl::waitTillInPosition(CORBA::Double timeout)
{
  ACS_TRACE(__func__);

  ACS::Time ts;
  CORBA::Boolean statusInPos;
  ACS::Time startTime;


  try {
    /* An initial read, to set the cache if necessary and set the startTime*/
    getStatusInPos(startTime);
    do {
      //Get information for the calibration device
      msecSleep(250L);
      statusInPos = getStatusInPos(ts);
    } while ((ts - startTime < timeout * 1E7) && !statusInPos);
  } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
    ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getCAMBErrorEx();
  } catch(const ControlExceptions::INACTErrorExImpl& ex) {
    ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getINACTErrorEx();
  }

  if (!statusInPos) {
    ControlExceptions::TimeoutExImpl ex(__FILE__, __LINE__, __func__);
    ex.log();
    throw ex.getTimeoutEx();

  }
  return static_cast<CORBA::Long>((ts - startTime)/1E7);
}

void ACDImpl::setHotLoadTemperature(CORBA::Float temp)
{
  ACS_TRACE(__func__);
  ACS::Time ts;
  //CORBA::Long timeout(350);

  //Get HL bits status of Active (0x02), Error (0x08), and Out of Range(0x10)
  unsigned char status = getHlStatus(ts) & 0x1A;

  try{
    if (getStatusCanComm(ts)){
      SET_INIT();
      ACS_SHORT_LOG((LM_WARNING, "ACD was power cycled, "\
		     "performing INIT command."));
      msecSleep(301L);
      SET_INIT();
      msecSleep(1000L);
      SET_HEAT(temp);

    } else if (isInResetState()){
      ACS_SHORT_LOG((LM_WARNING, "It seems ACD is in a RESET state, "\
		     " making INIT command for reinitializing and "
		     "setting up HOT LOAD temperature."));
      SET_INIT();
      msecSleep(1001L);
      SET_HEAT(temp);

    } else if ( status != 0 ){
      //set temperature if bits are active, error, out of range
      SET_HEAT(temp);

    }else if (getHlStatusIdle(ts)){
      SET_HEAT(temp);
      msecSleep(1000L);
      if (!getHlStatusActive(ts)){
	ACS_SHORT_LOG((LM_WARNING, "HL is in idle state, but the temperature "\
		       "can not be set up. Making an initialization of ACD "\
		       "before try to set up temperature again."));
	SET_INIT();
	msecSleep(1001L);
	SET_HEAT(temp);

      }

    } else {
      SET_HEAT(temp);

    }

    if (getHlStatusActive(ts))
      hotLoadTemp_m = temp;

  } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
    ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getCAMBErrorEx();
  } catch(const ControlExceptions::INACTErrorExImpl& ex) {
    ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getINACTErrorEx();
  }
}

CORBA::Boolean ACDImpl::areMotorsBlocked()
{

  ACS_TRACE(__func__);
  ACS::Time ts;
  std::vector< unsigned char > status;

  try {
    status = getStatus(ts);
  } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
    ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    nex.log();
    throw nex;
  } catch(const ControlExceptions::INACTErrorExImpl& ex) {
    ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    nex.log();
    throw nex;
  }

  //QWP is disabled, so it is not checked (qwpErrorMASK)
  return (( status[1] & (armErrorMASK | wheelErrorMASK) ) != 0) ? true : false;
}

CORBA::Boolean ACDImpl::isInResetState()
{
  ACS_TRACE(__func__);
  ACS::Time ts;

  try {
    return (areMotorsBlocked() & getHlStatusOff(ts));
  } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
    ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex;
  } catch(const ControlExceptions::INACTErrorExImpl& ex) {
    ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex;
  }
}

void ACDImpl::stopHeat()
{
  ACS_TRACE(__func__);
  try
    {
      if (heatEnabled_m) {
	RESET();
	msecSleep(500L);
	SET_INIT();
	heatEnabled_m = false;
      }
    }
  catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
      ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
      throw nex.getCAMBErrorEx();
    }
  catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
      ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,__func__);
      throw nex.getINACTErrorEx();
    }
}

void ACDImpl::enableCalDeviceDataReporting(bool enable){
  ACS_TRACE(__func__);

  if (enable) {
    /* Set these to -1 so that we force an update */
    calDeviceData_m.temperatureLoad[0] = -1.0;
    calDeviceData_m.temperatureLoad[1] = -1.0;
    reportCalDeviceData();
  }
  calDeviceReportingEnabled = enable;
}

bool ACDImpl::calDeviceDataReportingEnabled() {
  ACS_TRACE(__func__);
  return calDeviceReportingEnabled;
}

void ACDImpl::reportCalDeviceData() {
  ACS_TRACE(__func__);

  double ambLoadTemp = 0.0;
  double hotLoadTemp = 0.0;

  try {
    ambLoadTemp = (getTemp11(calDeviceData_m.timestamp) +
                   getTemp12(calDeviceData_m.timestamp))/2.0;
    hotLoadTemp = (getTemp20(calDeviceData_m.timestamp) +
                   getTemp21(calDeviceData_m.timestamp))/2.0;
  } catch (ControlExceptions::INACTErrorExImpl& ex) {
    if (calDeviceDataGuard_m.checkAndIncrement() == true) {
      ControlExceptions::INACTErrorExImpl nex(ex,__FILE__, __LINE__, __func__);
      nex.addData("ErrorMessage",
         "Calibration Device Data not recorded in ASDM device is Inactive");
      nex.log();
    }
    return;
  } catch (ControlExceptions::CAMBErrorExImpl& ex) {
    if (calDeviceDataGuard_m.checkAndIncrement() == true) {
      ControlExceptions::CAMBErrorExImpl nex(ex,__FILE__, __LINE__, __func__);
      nex.addData("ErrorMessage",
         "Calibration Device Data not recorded in ASDM failure communicating with device");
      nex.log();
    }
    return;
  }

  if (fabs(calDeviceData_m.temperatureLoad[0]-ambLoadTemp)>updateThreshold  ||
      fabs(calDeviceData_m.temperatureLoad[1]-hotLoadTemp)>updateThreshold){

    /* Here we want to update calDeviceData_m */
    calDeviceData_m.timestamp = ::getTimeStamp();
    calDeviceData_m.temperatureLoad[0] = ambLoadTemp;
    calDeviceData_m.temperatureLoad[1] = hotLoadTemp;

    Control::AntennaStatusReporting* antenna;

      try {

        antenna = getAntennaStatusReportingReference();

        antenna->reportCalDeviceData(calDeviceData_m);

      } catch (const ControlExceptions::INACTErrorEx& ex) {
        /*  This just means that the Antenna is not available yet
            This is ok, just proceed */
      } catch (const maciErrType::CannotGetComponentExImpl &ex){
        /* could not get The antenna component*/
      }

  }
}


Control::AntennaStatusReporting_ptr ACDImpl::getAntennaStatusReportingReference()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    const string antName = componentToAntennaName(compName.in());
    maci::ContainerServices * cs = getContainerServices();
    string acdName = "CONTROL/"+antName;
    try {
    return cs->getComponentNonSticky<Control::AntennaStatusReporting>(acdName.c_str());
    } catch (maciErrType::CannotGetComponentExImpl &ex) {
       maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
       nex.log();
       throw nex;
    }
}


ReceiverBandMod::ReceiverBand ACDImpl::getCurrentCartrige()
{
   try {

       return getCurrentCartrigeInternal();

   } catch (ControlExceptions::INACTErrorExImpl& ex){
       ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
       throw nex.getINACTErrorEx();
   } catch (ControlExceptions::CAMBErrorExImpl& ex) {
       ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
       throw nex.getHardwareErrorEx();
   } catch (ControlExceptions::HardwareErrorExImpl& ex) {
       ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
       throw nex.getHardwareErrorEx();
   } catch (...) {
       ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
       ex.addData("Additional", "An UNKOWN Exception was caught.");
       ex.log();
       throw ex.getHardwareErrorEx();
   }
}

ReceiverBandMod::ReceiverBand ACDImpl::getCurrentCartrigeInternal()
{
    ReceiverBandMod::ReceiverBand band = ReceiverBandMod::UNSPECIFIED;
    ACS::Time timestamp;
    int  intBand = getStatusCartNr(timestamp);
    try {
        band = FrontEndUtils::intToBand(intBand);
    } catch (ControlExceptions::IllegalParameterErrorExImpl &ex) {
        if (intBand != 12 && intBand != 13) {
            ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            std::stringstream ss;
            ss<<"Could not determine the Calibrationd device position. It returned: "<<intBand;
            nex.addData("Additional", ss.str());
            nex.log(LM_DEBUG);
            //throw nex;
        }
    }
    return band;
}

CalibrationDeviceMod::CalibrationDevice ACDImpl::getCurrentLoad()
{
    try {

        return getCurrentLoadInternal();

    } catch (ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    } catch (ControlExceptions::HardwareErrorExImpl& ex){
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    } catch (...) {
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        //ex.log();
        throw ex.getHardwareErrorEx();
    }
}

CalibrationDeviceMod::CalibrationDevice ACDImpl::getCurrentLoadBareInternal()
{
    ACS::Time timestamp;
    CalibrationDeviceMod::CalibrationDevice load;

    int intLoad = getStatusLoad(timestamp);

    switch (intLoad) {
    case 0:
        load = CalibrationDeviceMod::AMBIENT_LOAD;
        break;
    case 1:
        load = CalibrationDeviceMod::HOT_LOAD;
        break;
    case 2:
        load = CalibrationDeviceMod::SOLAR_FILTER;
        break;
    case 3:
        load = CalibrationDeviceMod::QUARTER_WAVE_PLATE;
        break;
    default:
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg("Unrecognized response from the hardware");
        ex.addData("Error Message", msg);
        ex.log();
        throw ex;
    }
    return load;
}


CalibrationDeviceMod::CalibrationDevice ACDImpl::getCurrentLoadInternal()
{
    // Part of fix for http://jira.alma.cl/browse/COMP-5531
    // If the motors are not in position yet. The readback should not
    // be trusted, so just return what is expected. In any
    // case data during this time will be flaged.
    ACS::Time timestamp;
    CalibrationDeviceMod::CalibrationDevice load;
    load = getCurrentLoadBareInternal();
    if (getStatusInPos(timestamp) == false) {
        load = lastCommandedLoad_m;
    }
    return load;
}

void ACDImpl::setCalibrationDeviceBand(
        CalibrationDeviceMod::CalibrationDevice cd,
        ReceiverBandMod::ReceiverBand band)
{
    try {

        setCalibrationDeviceBandInternal(cd, band);

    } catch (ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch (...) {
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getHardwareErrorEx();
    }
}

void ACDImpl::setCalibrationDeviceBandInternal(CalibrationDeviceMod::CalibrationDevice cd,
    ReceiverBandMod::ReceiverBand band)
{
    lastCommandedBand_m = band;
    lastCommandedLoad_m = cd;
    switch (cd) {
    case CalibrationDeviceMod::AMBIENT_LOAD:
        ACS_LOG(
                LM_SOURCE_INFO,
                __PRETTY_FUNCTION__,
                (LM_NOTICE, "Setting Ambient load in front of band %d", FrontEndUtils::bandToInt(
                        band)));
        setCntlAmbientLoad(FrontEndUtils::bandToInt(band));
        break;
    case CalibrationDeviceMod::HOT_LOAD:
        ACS_LOG(
                LM_SOURCE_INFO,
                __PRETTY_FUNCTION__,
                (LM_NOTICE, "Setting Hot load in front of band %d", FrontEndUtils::bandToInt(
                        band)));
        setCntlHotLoad(FrontEndUtils::bandToInt(band));
        break;
    case CalibrationDeviceMod::NONE:
        /* Note for the future maintainer:
         * It used to be that it had to be chosen if setPark0Loads or setPark1Loads
         * was used depending on the band. Since the ACD's plate was sawed of setPark0Loads
         * is safe for all bands.
         */
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "Parking the Calibration Device"));
        lastCommandedBand_m = ReceiverBandMod::UNSPECIFIED;
        setPark0Loads();
        break;
    case CalibrationDeviceMod::SOLAR_FILTER:
        ACS_LOG(
                LM_SOURCE_INFO,
                __PRETTY_FUNCTION__,
                (LM_NOTICE, "Setting Solar Filter in front of band %d", FrontEndUtils::bandToInt(
                        band)));
        setCntlSolarFilter(FrontEndUtils::bandToInt(band));
        break;
    case CalibrationDeviceMod::QUARTER_WAVE_PLATE:
    case CalibrationDeviceMod::COLD_LOAD:
    case CalibrationDeviceMod::NOISE_TUBE_LOAD:
    default:
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg("The requested calibration device is not supported");
        ex.log();
        throw ex;
    }
}
//-----------------------------------------------------------------------------
// Alarm Monitor Thread
//-----------------------------------------------------------------------------
void ACDImpl::checkHLTemperature() {
  ACS_TRACE(__func__);
  ACS::Time ts;

  CORBA::Float minHLTemp = hotLoadTemp_m - (hotLoadTemp_m*0.03);
  CORBA::Float currentTemp = GET_TEMP20(ts);

  if (startCheckTempAlarm_m) {
    if (currentTemp < minHLTemp) {
      if (!errorFlagSet(HotLoadUnderDefaultValue))
	setErrorFlag (HotLoadUnderDefaultValue);
    } else {
      if (errorFlagSet(HotLoadUnderDefaultValue))
	resetErrorFlag (HotLoadUnderDefaultValue);
    }
  }
  else {
    if (currentTemp >= minHLTemp)
      startCheckTempAlarm_m = true;
  }

  CORBA::Boolean outOfRange = GET_HL_STATUS_OUT_OF_RANGE(ts);
  if (outOfRange){
    if (!errorFlagSet(HotLoadOutOfRange))
      setErrorFlag (HotLoadOutOfRange);
  } else {
    if (errorFlagSet(HotLoadOutOfRange))
      resetErrorFlag (HotLoadOutOfRange);
  }
}

/* Method called in loop by thread */
void ACDImpl::updateThreadAction()
{
    ACS_TRACE("ACDImpl::updateThreadAction()");
    try {
        checkHLTemperature();
    } catch (...) {
        //Could not check the temperature.
    }
    try {
        if (calDeviceReportingEnabled) {
            reportCalDeviceData();
        }
    }catch(...){
        //pass
    }
    try {
        bool isInBandPosition = isInCommandedBandPosition();
        flagNotInCommandedBandPosition(isInBandPosition);
    } catch (...) {
        //pass
    }
    try {
        bool isInLoadPosition = isInCommandedLoadPosition();
        flagNotInCommandedLoadPosition(isInLoadPosition);
    } catch (...) {
        //pass
    }
    try {
        ACS::Time ts;
        bool isinposition = getStatusInPos(ts);
        flagNotInPosition(isinposition);
    } catch (...) {
        //pass
    }
}

bool ACDImpl::isInCommandedLoadPosition()
{
    bool inPosition = false;
    try {
        if (lastCommandedLoad_m == getCurrentLoadInternal()) {
            inPosition = true;
        } else {
            ostringstream msg;
            msg << "ACD has the wrong load: " << getCurrentLoadInternal()
                    << " instead of " << lastCommandedLoad_m;
            //LOG_TO_DEVELOPER(LM_NOTICE, msg.str()); //FIXME: uncomment when ready.
        }
    } catch (ControlExceptions::INACTErrorExImpl& ex) {

    } catch (ControlExceptions::CAMBErrorExImpl& ex) {

    } catch (ControlExceptions::HardwareErrorExImpl& ex) {

    } catch (...) {
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();

    }
    return inPosition;
}

bool ACDImpl::isInCommandedBandPosition()
{
    bool inPosition = false;
    try {
        if (lastCommandedBand_m == getCurrentCartrigeInternal()) {
            inPosition = true;
        }
        else {
        ostringstream msg;
        msg<<"ACD over wrong band, it is on: "<<getCurrentCartrigeInternal()
           <<" instead of "<<lastCommandedBand_m;
    //    LOG_TO_DEVELOPER(LM_NOTICE, msg.str());
        }
    } catch (ControlExceptions::IllegalParameterErrorExImpl &ex) {
        //This means the ACD is not responding something valid.
    } catch (ControlExceptions::INACTErrorExImpl& ex) {

    } catch (ControlExceptions::CAMBErrorExImpl& ex) {

    } catch (ControlExceptions::HardwareErrorExImpl& ex) {

    } catch (...) {
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
    }
    return inPosition;
}

void  ACDImpl::flagNotInCommandedBandPosition(bool isOnPosition) {
    if (wasBandInPosition_m == isOnPosition)
        return;


    ACS::Time now = ::getTimeStamp();
    maci::SmartPtr<Control::ControlDevice> parent = getParentReference();
    if (parent.isNil()) return;

    Control::HardwareController_var frontEnd =
        Control::HardwareController::_narrow(&*parent);
    if (CORBA::is_nil(frontEnd)) return;

    CORBA::String_var componentName = name();
    try {
        frontEnd->flagData(!isOnPosition, now, componentName.in(),
                          "ACD is not over commanded band cartridge");
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named "
            << ex._name() << " while trying to "
            << ((isOnPosition) ? "unflag": "flag") << " data."
            << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }

    wasBandInPosition_m = isOnPosition;

        ostringstream msg;
        msg << "The ACD is"
            << " trying to "
            << ((isOnPosition) ? "unflag": "flag") << " data. Regarding the band position";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

void  ACDImpl::flagNotInCommandedLoadPosition(bool isOnPosition) {
    if (wasLoadInPosition_m == isOnPosition)
        return;


    ACS::Time now = ::getTimeStamp();
    maci::SmartPtr<Control::ControlDevice> parent = getParentReference();
    if (parent.isNil()) return;

    Control::HardwareController_var frontEnd =
        Control::HardwareController::_narrow(&*parent);
    if (CORBA::is_nil(frontEnd)) return;

    CORBA::String_var componentName = name();
    try {
        frontEnd->flagData(!isOnPosition, now, componentName.in(),
                          "ACD does not have the correct load in position");
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named "
            << ex._name() << " while trying to "
            << ((isOnPosition) ? "unflag": "flag") << " data."
            << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }

    wasLoadInPosition_m = isOnPosition;

        ostringstream msg;
        msg << "The ACD is"
            << " trying to "
            << ((isOnPosition) ? "unflag": "flag") << " data. Regarding the load position";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

void  ACDImpl::flagNotInPosition(bool isOnPosition) {
    if (wasInPosition_m == isOnPosition)
        return;


    ACS::Time now = ::getTimeStamp();
    maci::SmartPtr<Control::ControlDevice> parent = getParentReference();
    if (parent.isNil()) return;

    Control::HardwareController_var frontEnd =
        Control::HardwareController::_narrow(&*parent);
    if (CORBA::is_nil(frontEnd)) return;

    CORBA::String_var componentName = name();
    try {
        frontEnd->flagData(!isOnPosition, now, componentName.in(),
                          "ACD motors are not in position.");
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named "
            << ex._name() << " while trying to "
            << ((isOnPosition) ? "unflag": "flag") << " data."
            << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }

    wasInPosition_m = isOnPosition;

        ostringstream msg;
        msg << "The ACD is"
            << " trying to "
            << ((isOnPosition) ? "unflag": "flag") << " data. Regarding motor position";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}
/* Thread monitor constructor */
ACDImpl::UpdateThread::UpdateThread(const ACE_CString& name, ACDImpl& ACD) :
  ACS::Thread(name),
  acdDevice_p(ACD)
{
  ACS_TRACE(__func__);
}

/* runLoop implementation */
void ACDImpl::UpdateThread::runLoop()
{
  ACS_TRACE(__func__);
  try {
  acdDevice_p.updateThreadAction();
  }catch (...) {
  }
}

//-----------------------------------------------------------------------------
// Error Handling Methods
//-----------------------------------------------------------------------------
/*
  handleSetError and handleResetErrorFlag abstract methods on the
  ErrorStateHelper.h
*/
void ACDImpl::handleSetErrorFlag(ACDErrorCondition newErrorFlag) {
  ACS_TRACE(__func__);

  /* Turn on the Alarm */
  sendAlarm(true, newErrorFlag);
  ACDBase::setError(createErrorMessage());

}

void ACDImpl::handleResetErrorFlag(ACDErrorCondition newErrorFlag) {
  ACS_TRACE("ACDImpl::handleResetErrorFlag");

  /* Turn off the Alarm */
  sendAlarm(false, newErrorFlag);

  if (errorFlagSet()) {
    ACDBase::setError(createErrorMessage());
  } else {
    ACDBase::clearError();
  }
}

string ACDImpl::createErrorMessage()
{
  ostringstream errMsg;
  bool firstMessage = true;


  if (errorFlagSet(HotLoadUnderDefaultValue)) {
    if (firstMessage)
      firstMessage = false;
    else
      errMsg << "\n";
    errMsg << "Current hot load temperature is below of a default value: " <<
      DEFAULT_HL_TEMPERATURE;
  }
  if (errorFlagSet(HotLoadOutOfRange)) {
    if (firstMessage)
      firstMessage = false;
    else
      errMsg << "\n";
    errMsg << "Set up value of heater is not inside range allowed";
  }

  return errMsg.str();
}


ACS::TimeIntervalSeq* ACDImpl::timeToTune(const Control::ACD::CalibrationDeviceSeq& positions)
{
    const unsigned int size(positions.length());
    ACS::TimeIntervalSeq_var ret(new ACS::TimeIntervalSeq);
    ret->length(size);
    for (unsigned int i=0;i<size;i++) {
        //FIXME: Do actual calculation.
        //2 seconds is the last value provided by scorder
        ret[i] = 2;
    }
    return ret._retn();
}
//-----------------------------------------------------------------------------
// ACD alarm methods
//-----------------------------------------------------------------------------
void ACDImpl::sendAlarm (bool isActive, int alarmType)
{
  ACS_TRACE(__func__);

  //Get antenna name from component name
  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
  const std::string antName(HardwareDeviceImpl::componentToAntennaName(
      compName.in()));

  auto_ptr<acsalarm::FaultState> faultState =
    ACSAlarmSystemInterfaceFactory::
    createFaultState("ACD",antName, alarmType);

  faultState->setUserTimestamp(auto_ptr<acsalarm::Timestamp>
			       (new acsalarm::Timestamp()));
  if (isActive) {
    faultState->setDescriptor(faultState::ACTIVE_STRING);
  } else {
    faultState->setDescriptor(faultState::TERMINATE_STRING);
  }

  // Send the fault. We must use the "ALARM_SYSTEM_SOURCES" name.
  ACSAlarmSystemInterfaceFactory::
    createSource("ALARM_SYSTEM_SOURCES")->push(*faultState);

}

void ACDImpl::clearAlarm ()
{
  ACS_TRACE(__func__);

  sendAlarm (false, HotLoadOutOfRange);
  sendAlarm (false, HotLoadUnderDefaultValue);

}

//-----------------------------------------------------------------------------
// Internal methods
//-----------------------------------------------------------------------------
void ACDImpl::msecSleep(CORBA::Long timeout)
{
  ACS_TRACE(__func__);

  time_t sec  = (int)(timeout/1000);
  time_t msec = timeout - (sec*1000);
  time_t nsec = (int)(msec*1000000L);

  struct timespec delay = {sec, nsec};
  struct timespec rsp = {0};

  nanosleep(&delay, &rsp);

}

/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(ACDImpl)
