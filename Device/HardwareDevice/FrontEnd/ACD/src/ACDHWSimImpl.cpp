/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ACDHWSimImpl.cpp
 */

/* In this file we should override the base-setters, to associate also
 * the status of the device (getter) after the command is sent (setter).
 */
/*
 * For the *ToData() and dataTo*() functions used in here, see
 * CONTROL/Common/HWDeviceSim/include/AMBUtil.h
 */
#include "ACDHWSimImpl.h"

using namespace AMB;

// Constructor (default initializations)
// ----------------------------------------------------------------------------
ACDHWSimImpl::ACDHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber)
    : ACDHWSimBase(node,serialNumber)
{
    const char* __METHOD__ = "ACDHWSimImpl::ACDHWSimImpl";
    std::cerr << __METHOD__ << std::endl;

    long long raw(0LL);
    std::vector< CAN::byte_t >* vvalue(new std::vector< CAN::byte_t >);
    raw = static_cast< long long >(ACD_HL_CONTROLLER_ACTIVE);
    AMB::TypeConversion::valueToData(*vvalue, static_cast< unsigned char >(raw), 1U);
    if(state_m.find(monitorPoint_HL_STATUS) != state_m.end())
        *(state_m.find(monitorPoint_HL_STATUS)->second) = *vvalue;
    else
        state_m.insert(std::make_pair(monitorPoint_HL_STATUS, vvalue));
    std::vector<CAN::byte_t> data;
    data.push_back(static_cast<CAN::byte_t>(1U));
    setControlPark0Loads(data);

}

void ACDHWSimImpl::setControlInit(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_INIT");
    if (state_m.find(controlPoint_INIT) != state_m.end())
	{
	// After INIT, motors are in park1 position (1st byte, bits 0-3: 0xD, bits 4-7: 0x8)

	state_m[monitorPoint_STATUS]->clear();
	unsigned char aux[2] = {ACD_IN_POSITION | ACD_PARK1, 0x00};
	std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
	// For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
	*(state_m[monitorPoint_STATUS]) = result;
	// should set in principle encoder position as well
	}
    else
	throw CAN::Error("Trying to set controlPoint_INIT. Member not found.");
}

void ACDHWSimImpl::setControlReset(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_RESET");
    if (state_m.find(controlPoint_RESET) != state_m.end())
	{
	// After RESET, motors are left where they were before, but no movement possible anymore
	}
    else
	throw CAN::Error("Trying to set controlPoint_RESET. Member not found.");
}

void ACDHWSimImpl::setControlSetHotLoad(const std::vector<CAN::byte_t>& data)
{
  checkSize(data,1,"controlPoint_HOT_LOAD");
  if (state_m.find(controlPoint_HOT_LOAD) != state_m.end())
    {
      ACDHWSimBase::setControlSetHotLoad(data);
      state_m[monitorPoint_STATUS]->clear();
      unsigned char aux[2] = {ACD_IN_POSITION | ACD_HOT_LOAD | data.at(0), 0x00};
      std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
      // For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
      *(state_m[monitorPoint_STATUS]) = result;
      // should set in principle encoder position as well
    }
  else
    throw CAN::Error("Trying to set controlPoint_HOT_LOAD. Member not found.");
}

void ACDHWSimImpl::setControlSetAmbientLoad(const std::vector<CAN::byte_t>& data)
{
  checkSize(data,1,"controlPoint_AMBIENT_LOAD");
  if (state_m.find(controlPoint_AMBIENT_LOAD) != state_m.end())
    {
      ACDHWSimBase::setControlSetAmbientLoad(data);
      state_m[monitorPoint_STATUS]->clear();
      unsigned char aux[2] = { ACD_IN_POSITION | ACD_AMBIENT_LOAD | data.at(0), 0x00};
      std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
      // For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
      *(state_m[monitorPoint_STATUS]) = result;
      // should set in principle encoder position as well
    }
  else
    throw CAN::Error("Trying to set controlPoint_AMBIENT_LOAD. Member not found.");
}

void ACDHWSimImpl::setControlSetSolarFilter(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_SOLAR_FILTER");
    if (state_m.find(controlPoint_SOLAR_FILTER) != state_m.end())
	{
	  ACDHWSimBase::setControlSetSolarFilter(data);
	state_m[monitorPoint_STATUS]->clear();
        unsigned char aux[2] = {ACD_SOLAR_FILTER | ACD_IN_POSITION | data.at(0), 0x00};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
	// For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
	*(state_m[monitorPoint_STATUS]) = result;
	// should set in principle encoder position as well
	}
    else
	throw CAN::Error("Trying to set controlPoint_SOLAR_FILTER. Member not found.");
}

void ACDHWSimImpl::setControlPark0Loads(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_PARK0_LOADS");
    if (state_m.find(controlPoint_PARK0_LOADS) != state_m.end())
	{
	state_m[monitorPoint_STATUS]->clear();
	// Not clear from ICD which load will be reported after PARK0_LOADS cmd
        unsigned char aux[2] = {ACD_PARK0 | ACD_AMBIENT_LOAD | ACD_IN_POSITION, 0x00};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
	// For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
	*(state_m[monitorPoint_STATUS]) = result;
	// should set in principle encoder position as well
	}
    else
	throw CAN::Error("Trying to set controlPoint_PARK0_LOADS. Member not found.");
}

void ACDHWSimImpl::setControlPark1Loads(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_PARK1_LOADS");
    if (state_m.find(controlPoint_PARK1_LOADS) != state_m.end())
	{
	state_m[monitorPoint_STATUS]->clear();
	// Not clear from ICD which load will be reported after PARK1_LOADS cmd
        unsigned char aux[2] = {ACD_PARK1 | ACD_AMBIENT_LOAD | ACD_IN_POSITION, 0x00};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
	// For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
	*(state_m[monitorPoint_STATUS]) = result;
	// should set in principle encoder position as well
	}
    else
	throw CAN::Error("Trying to set controlPoint_PARK1_LOADS. Member not found.");
}

void ACDHWSimImpl::setControlStowLoads(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_STOW_LOADS");
    if (state_m.find(controlPoint_STOW_LOADS) != state_m.end())
	{
	state_m[monitorPoint_STATUS]->clear();
	// Not clear from ICD which load will be reported after PARK1_LOADS cmd
        unsigned char aux[2] = {ACD_STOW | ACD_AMBIENT_LOAD | ACD_IN_POSITION, 0x00};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
	// For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
	*(state_m[monitorPoint_STATUS]) = result;
	// should set in principle encoder position as well
	}
    else
	throw CAN::Error("Trying to set controlPoint_STOW_LOADS. Member not found.");
}

void ACDHWSimImpl::setControlSetHeat(const std::vector<CAN::byte_t>& data)
{
  checkSize(data,2,"controlPoint_HEAT");
  std::vector< CAN::byte_t > vvalue = ACDHWSimBase::getMonitorHlStatus(); 

  if (state_m.find(controlPoint_HEAT) != state_m.end())
    {
      if (vvalue.at(0) & ACD_HL_CONTROLLER_ACTIVE ) 
	{
	  state_m[monitorPoint_TEMP20]->clear();
	  state_m[monitorPoint_TEMP21]->clear();
	  state_m[monitorPoint_TEMP22]->clear();
	  *(state_m[monitorPoint_TEMP20]) = data;
	  *(state_m[monitorPoint_TEMP21]) = data;
	  *(state_m[monitorPoint_TEMP22]) = data;
	}
    }
  else
    throw CAN::Error("Trying to set controlPoint_HEAT. Member not found.");
}

void ACDHWSimImpl::setControlSetLoad0Xy(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,8,"controlPoint_LOAD0_XY");
    if (state_m.find(controlPoint_LOAD0_XY) != state_m.end())
	{
	//state_m[monitorPoint_LOAD0_XY]->clear();

	//*(state_m.find(monitorPoint_LOAD0_XY)->second) = data;
	*(state_m[monitorPoint_LOAD0_XY]) = data;
	}
    else
	throw CAN::Error("Trying to set controlPoint_LOAD0_XY. Member not found.");
}

void ACDHWSimImpl::setControlSetLoad1Xy(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,8,"controlPoint_LOAD1_XY");
    if (state_m.find(controlPoint_LOAD1_XY) != state_m.end())
	{
	//state_m[monitorPoint_LOAD1_XY]->clear();
	*(state_m[monitorPoint_LOAD1_XY]) = data;
	}
    else
	throw CAN::Error("Trying to set controlPoint_LOAD1_XY. Member not found.");
}

void ACDHWSimImpl::setControlSetLoad2Xy(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,8,"controlPoint_LOAD2_XY");
    if (state_m.find(controlPoint_LOAD2_XY) != state_m.end())
	{
	//state_m[monitorPoint_LOAD2_XY]->clear();
	*(state_m[monitorPoint_LOAD2_XY]) = data;
	}
    else
	throw CAN::Error("Trying to set controlPoint_LOAD2_XY. Member not found.");
}

void ACDHWSimImpl::setControlSetLoad0Dxdy(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,8,"controlPoint_LOAD0_dXdY");
    if (state_m.find(controlPoint_LOAD0_dXdY) != state_m.end())
	{
	std::vector<CAN::byte_t>currentPos = *(state_m[monitorPoint_LOAD0_XY]);
	std::vector<CAN::byte_t>deltaPos = data;
		
	// dataToFloat() *removes* first 4 elements of byte-vector.
	float x = dataToFloat(currentPos) + dataToFloat(deltaPos);
	float y = dataToFloat(currentPos) + dataToFloat(deltaPos);

	// Note: doubleToData appends nr of bytes of 2nd arg to *end* 
	// of vector passed as 1st arg (with the help of the vector-function
	// push_back())
	double *d = (double *)&y;
        std::vector<CAN::byte_t>newPos = doubleToData(floatToData(x), *d, 4);
	*(state_m[monitorPoint_LOAD0_XY]) = newPos;
	}
    else
	throw CAN::Error("Trying to set controlPoint_LOAD0_dXdY. Member not found.");
}

void ACDHWSimImpl::setControlSetLoad1Dxdy(const std::vector<CAN::byte_t>& data)
{
    // Check setControlSetLoad0dXdY
    checkSize(data,8,"controlPoint_LOAD1_dXdY");
    if (state_m.find(controlPoint_LOAD1_dXdY) != state_m.end())
	{
	std::vector<CAN::byte_t>currentPos = *(state_m[monitorPoint_LOAD1_XY]);
	std::vector<CAN::byte_t>deltaPos = data;
		
	float x = dataToFloat(currentPos) + dataToFloat(deltaPos);
	float y = dataToFloat(currentPos) + dataToFloat(deltaPos);

	double *d = (double *)&y;
        std::vector<CAN::byte_t>newPos = doubleToData(floatToData(x), *d, 4);
	*(state_m[monitorPoint_LOAD1_XY]) = newPos;
	}
    else
	throw CAN::Error("Trying to set controlPoint_LOAD1_dXdY. Member not found.");
}

void ACDHWSimImpl::setControlSetLoad2Dxdy(const std::vector<CAN::byte_t>& data)
{
    // Check setControlSetLoad0dXdY
    checkSize(data,8,"controlPoint_LOAD2_dXdY");
    if (state_m.find(controlPoint_LOAD2_dXdY) != state_m.end())
	{
	std::vector<CAN::byte_t>currentPos = *(state_m[monitorPoint_LOAD2_XY]);
	std::vector<CAN::byte_t>deltaPos = data;
		
	float x = dataToFloat(currentPos) + dataToFloat(deltaPos);
	float y = dataToFloat(currentPos) + dataToFloat(deltaPos);

	double *d = (double *)&y;
        std::vector<CAN::byte_t>newPos = doubleToData(floatToData(x), *d, 4);
	*(state_m[monitorPoint_LOAD2_XY]) = newPos;
	}
    else
	throw CAN::Error("Trying to set controlPoint_LOAD2_dXdY. Member not found.");
}

void ACDHWSimImpl::setControlSetArm0(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,4,"controlPoint_ARM0");
    if (state_m.find(controlPoint_ARM0) != state_m.end())
	{
	state_m[monitorPoint_ARM0]->clear();
	*(state_m[monitorPoint_ARM0]) = data;
	}
    else
	throw CAN::Error("Trying to set controlPoint_ARM0. Member not found.");
}

void ACDHWSimImpl::setControlSetArm1(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,4,"controlPoint_ARM1");
    if (state_m.find(controlPoint_ARM1) != state_m.end())
	{
	state_m[monitorPoint_ARM1]->clear();
	*(state_m[monitorPoint_ARM1]) = data;
	}
    else
	throw CAN::Error("Trying to set controlPoint_ARM1. Member not found.");
}

void ACDHWSimImpl::setControlSetArm2(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,4,"controlPoint_ARM2");
    if (state_m.find(controlPoint_ARM2) != state_m.end())
	{
	state_m[monitorPoint_ARM2]->clear();
	*(state_m[monitorPoint_ARM2]) = data;
	}
    else
	throw CAN::Error("Trying to set controlPoint_ARM2. Member not found.");
}

void ACDHWSimImpl::setControlSetQuarterwavePlate(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_QUARTERWAVE_PLATE");
    if (state_m.find(controlPoint_QUARTERWAVE_PLATE) != state_m.end())
	{
	state_m[monitorPoint_STATUS]->clear();
	// should set in principle encoder position as well
	*(state_m[monitorPoint_STATUS]) = charToData(data, ACD_BAND7 | ACD_QWP | ACD_IN_POSITION);
	}
    else
	throw CAN::Error("Trying to set controlPoint_QUARTERWAVE_PLATE. Member not found.");
}

void ACDHWSimImpl::setControlSetHlStatus(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"monitorPoint_HL_STATUS");
    if (state_m.find(monitorPoint_HL_STATUS) != state_m.end())
	{
	state_m[monitorPoint_HL_STATUS]->clear();
        unsigned char aux[1] = {ACD_HL_CONTROLLER_ACTIVE};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
    sleep(5);
	// For conversion, see also CONTROL/Common/HWDeviceSim/include/TypeConversion.h
	*(state_m[monitorPoint_HL_STATUS]) = result;
	// should set in principle encoder position as well
	}
    else
	throw CAN::Error("Trying to set monitorPoint_HL_STATUS. Member not found.");
}
