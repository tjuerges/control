# Redirect both stdout and stderr to /dev/null
# (stdout to avoid that exec returns an error when output is
# sent to stderr; stdin to avoid that loadLkmModule interrogates
# the device as if it were a tty)
exec loadLkmModule CAMBServerSim.lkm >>& /dev/null
set pid1 [exec AMBLBSimulator &]
set pid2 [exec ambdaemon &]
# wait some time for these 2 processes to come up
puts "Before wait"
after 7000

puts "After wait"
# Don't use "write_file" as that would append a trailing newline.
set fp [open pidsFile w]
puts -nonewline $fp [list $pid1 $pid2]
close $fp

puts "Before exit"
exit
