set fp [open pidsFile r]
gets $fp pids
close $fp
eval exec kill $pids

# Redirect both stdout and stderr to /dev/null
# (stdout to avoid that exec returns an error when output is
# sent to stderr; stdin to avoid that unloadLkmModule interrogates
# the device as if it were a tty)
exec unloadLkmModule CAMBServerSim.lkm >>& /dev/null
file delete -force -- pidsFile
