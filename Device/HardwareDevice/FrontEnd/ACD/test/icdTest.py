#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#
# Phase 2 test of ALMA Calibration Device
#
# who       when        what
# --------  ----------  ----------------------------------------------
# eallaert  2008-06-13  created
#

import sys
import unittest
import struct
import ControlExceptions
import ACDDef
from time import sleep
from time import time

# Cartridge- and load-names: see STATUS byte 1 (bits 0-3 resp 4-5)
cartridgeNames = ['STOW', 'band 1', 'band 2', 'band 3', 'band 4', 'band 5', 'band 6',
                  'band 7', 'band 8', 'band 9', 'band 10', 'WVR', 'PARK0', 'PARK1',
                  'not aligned', 'UNDEFINED']
loadNames = ['ambient load', 'hot load', 'solar filter', 'quarter wave plate']
# The software servo-loop tolerance seems to be substantially less than the
# specifications (~0.1 mm vs 1mm) - see 4.4.4 in FEND-40.06.00.00-009-B-SPE.pdf.
# Note that the effective x/y tolerance depends on the load and band (conversion
# of *radial* tolerance to x/y coordinates)
tolerance = 0.25
# Delay (in seconds) to wait before checking for in-position flag when
# a movement command is given.
inPositionLatency = 0.125


byteLength = {}
byteLength['ubyte']=1
byteLength['int8']=1
byteLength['uint8']=1
byteLength['int16']=2
byteLength['uint16']=1
byteLength['int24']=3
byteLength['uint24']=3
byteLength['int32']=4
byteLength['uint32']=4
byteLength['int48']=6
byteLength['uint48']=6
byteLength['int64']=8
byteLength['uint64']=8
byteLength['float']=4
byteLength['double']=8

def testProperty(compRef, rca, testName, propName, propLen, format, minVal, maxVal):
    '''
    Tests a single BACI property
    '''
    compRef.startTestLog("Test " + testName + ": " + propName + " Test")

    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, rca)
    errStr = str(propName) + " returned incorrect length value, actual = "
    errStr += str(len(r)) + " bytes, expected " + str(propLen) + " bytes"
    compRef.failUnless(len(r) == propLen, errStr)
    (resp,)= struct.unpack(format,r)
    compRef.log(str(propName) + " returns value " + str(resp))
    errStr = str(propName) + " returned value out of bounds, value = " + str(resp)
    compRef.failUnless((resp <= maxVal) and (resp >= minVal), errStr)

    compRef.endTestLog("Test " + testName + ": " + propName + " Test")
    return

def printStatus(compRef):
    '''
    Get the status monitor point, analyse the bits and print the status
    '''
    status = getStatus(compRef)
    cartNr = status['cartridge']
    cartName = cartridgeNames[cartNr]
    loadNr = status['load'] / 16
    loadName = loadNames[loadNr]
    compRef.log ("ACD status: cartridge %d = '%s', load %d = '%s'" %
                 (cartNr, cartName, loadNr, loadName))
    if status['inPosition'] :
        compRef.log("            all motors in position")
    else:
        compRef.log("            at least 1 motor not in position")
        
    if status['xyError'] :
        compRef.log("            error on X/Y position")
    if status['setLoadiXYOOR'] :
        compRef.log("            SET_LOADi_XY attempted a displacement out-of-range")
    if status['setLoadidXdYOOR'] :
        compRef.log("            SET_LOADi_dXdY attempted a displacement out-of-range")
    if status['setArmiOOR'] :
        compRef.log("            SET_ARMi attempted a displacement out-of-range")
    if status['canError'] :
        compRef.log("            errors in last CAN communication")
    if status['armError'] :
        compRef.log("            arm motor not in position")
    if status['wheelError'] :
        compRef.log("            wheel motor not in position")
    if status['qwpError'] :
        compRef.log("            QWP motor not in position")
    if status['wrongNrBytes'] :
        compRef.log("            last command given with wrong number of bytes")


    return status
    
def getStatus(compRef):
    '''
    Get the status monitor point and analyse the bits
    '''
    # Status Byte 1:
    #  bits 0-3 = cartridge number
    #  bits 4-5 = address of the loads
    #  bit 6    = error on X/Y position
    #  bit 7    = in-position bit
    cartridgeMask  = 0x0F
    loadMask = 0x30
    xyErrorMask = 0x40
    inPositionMask = 0x80

    # Status Byte 2:
    #  bit 0: set if SET_LOADi_XY attempted a displacement out-of-range last time it was executed
    #  bit 1: set if SET_LOADi_dXdY attempted a displacement out-of-range last time it was executed
    #  bit 2: set if SET_ARMi attempted a displacement out-of-range last time it was executed
    #  bit 3: set if CAN error on last communication
    #  bit 4: set if arm motor not in position
    #  bit 5: set if wheel motor not in position
    #  bit 6: set if qwp motor not in position
    #  bit 7: set if last command was given with wrong number of bytes
    setLoadiXYOORMask = 0x01
    setLoadidXdYOORMask = 0x02
    setArmiOORMask = 0x04
    canErrorMask = 0x08
    armErrorMask = 0x10
    wheelErrorMask = 0x20
    qwpErrorMask = 0x40
    wrongNrBytesMask = 0x80

    (r,t) = compRef.manager.monitor(compRef.channel, compRef.node, 0x01)
    (byte1, byte2) = struct.unpack("!2B", r)

    cartridge = byte1 & cartridgeMask
    load = byte1 & loadMask
    xyError = byte1 & xyErrorMask
    #inPosition = byte1 & inPositionMask
    if (byte1 & inPositionMask) == inPositionMask :
        inPosition = True
    else:
        inPosition = False
    
    setLoadiXYOOR = byte2 & setLoadiXYOORMask
    setLoadidXdYOOR = byte2 & setLoadidXdYOORMask
    setArmiOOR = byte2 & setArmiOORMask
    canError = byte2 & canErrorMask
    armError = byte2 & armErrorMask
    wheelError = byte2 & wheelErrorMask
    qwpError = byte2 & qwpErrorMask
    wrongNrBytes = byte2 & wrongNrBytesMask

    status = {'cartridge': cartridge, 'load' : load, 'xyError' : xyError,
              'inPosition' : inPosition, 'setLoadiXYOOR' : setLoadiXYOOR,
              'setLoadidXdYOOR' : setLoadidXdYOOR, 'setArmiOOR' : setArmiOOR,
              'canError' : canError, 'armError' : armError, 'wheelError' : wheelError,
              'qwpError' : qwpError, 'wrongNrBytes' : wrongNrBytes}


    return status
    
def waitTillInPosition(compRef, timeout):
    '''
    Wait till the in-position bit of the status monitor point is set
    '''
    startTime = time()
    compRef.log("Waiting till in position (timeout = " + str(timeout) + "s)")
    
    sleep(inPositionLatency)

    sleepTime = 0.03125
    elapsedTime = time() - startTime
    status = getStatus(compRef)
    while (not status['inPosition']) and (elapsedTime < timeout) :
        sleep(sleepTime)
        elapsedTime = time() - startTime
        status = getStatus(compRef)
        
    if elapsedTime < timeout :
        compRef.log("Motors in position within %.3fs." % elapsedTime)
    else:
        compRef.log("Motors not in position within %.3fs." % elapsedTime)
        printStatus(compRef)
        compRef.fail("Motors not in position within %.3fs." % elapsedTime)
    
    return elapsedTime

def getRcaAndNrBytes(pointName):
    '''
    function to extract the RCA and the number of bytes from a point
    as defined in ACDDef.py, i.e. ACD[pointName].
    '''
    rca = ACD[pointName][0]
    dataType = ACD[pointName][1]
    return (ACD[pointName][0], ACD[pointName][2]*byteLength[ACD[pointName][1]])

# The list/characteristics of points comes as a dictionary. Format:
# ACD = {'cmd/monName' : [rca, 'dataType', dimension], ...}
ACD = ACDDef.load()

# In PyUnit, test cases are represented by the TestCase class in the unittest module.
# Our own test cases must be subclasses of TestCase.
class Automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        unittest.TestCase.__init__(self,methodName)
        self.out = None
        if antenna == 'daemon':
            import AmbDaemonAdapter
            self.manager = AmbDaemonAdapter.AmbDaemonAdapter('localhost', 0, node)
        else :
            import CCL.AmbManager
            # For some reason ACS has problems unless I do this
            from Acspy.Clients.SimpleClient import PySimpleClient
            client = PySimpleClient.getInstance()
            self.manager = CCL.AmbManager.AmbManager(antenna)
            
        self.node = node
        self.channel = 0

        (self.rcaRESET,b)               = getRcaAndNrBytes('SET_RESET')
        (self.rcaINIT,b)                = getRcaAndNrBytes('SET_INIT')
        (self.rcaSET_HOT_LOAD,b)        = getRcaAndNrBytes('SET_HOT_LOAD')
        (self.rcaSET_AMBIENT_LOAD,b)    = getRcaAndNrBytes('SET_AMBIENT_LOAD')
        (self.rcaSET_SOLAR_FILTER,b)    = getRcaAndNrBytes('SET_SOLAR_FILTER')
        (self.rcaPARK0_LOADS,b)         = getRcaAndNrBytes('SET_PARK0_LOADS')
        (self.rcaPARK1_LOADS,b)         = getRcaAndNrBytes('SET_PARK1_LOADS')
        (self.rcaSTOW_LOADS,b)          = getRcaAndNrBytes('SET_STOW_LOADS')
        (self.rcaSET_HEAT,b)            = getRcaAndNrBytes('SET_HEAT')
        (self.rcaSET_LOAD0_XY,b)        = getRcaAndNrBytes('SET_LOAD0_XY')
        (self.rcaSET_LOAD1_XY,b)        = getRcaAndNrBytes('SET_LOAD1_XY')
        (self.rcaSET_LOAD2_XY,b)        = getRcaAndNrBytes('SET_LOAD2_XY')
        (self.rcaSET_LOAD0_dXdY,b)      = getRcaAndNrBytes('SET_LOAD0_dXdY')
        (self.rcaSET_LOAD1_dXdY,b)      = getRcaAndNrBytes('SET_LOAD1_dXdY')
        (self.rcaSET_LOAD2_dXdY,b)      = getRcaAndNrBytes('SET_LOAD2_dXdY')
        (self.rcaSET_ARM0,b)            = getRcaAndNrBytes('SET_ARM0')
        (self.rcaSET_ARM1,b)            = getRcaAndNrBytes('SET_ARM1')
        (self.rcaSET_ARM2,b)            = getRcaAndNrBytes('SET_ARM2')
        (self.rcaSET_QWP,b)             = getRcaAndNrBytes('SET_QUARTERWAVE_PLATE')
        (self.rcaSTOW_QWP,b)            = getRcaAndNrBytes('SET_STOW_QUARTERWAVE_PLATE')
        (self.rcaCAL_QWP_SET,b)         = getRcaAndNrBytes('SET_CAL_QWP_SET')
        (self.rcaCAL_QWP_STOW,b)        = getRcaAndNrBytes('SET_CAL_QWP_STOW')
        (self.rcaCAL_LOADi_BANDj,b)     = getRcaAndNrBytes('SET_CAL_LOADi_BANDj')
        (self.rcaCAL_PARK0,b)           = getRcaAndNrBytes('SET_CAL_PARK0')
        (self.rcaCAL_PARK1,b)           = getRcaAndNrBytes('SET_CAL_PARK1')
        (self.rcaCAL_STOW,b)            = getRcaAndNrBytes('SET_CAL_STOW')
        (self.rcaSET_X0_Y0,b)           = getRcaAndNrBytes('SET_X0_Y0')
        (self.rcaRESTORE_DEFAULT_CAL,b) = getRcaAndNrBytes('SET_RESTORE_DEFAULT_CAL')
        (self.rcaMAINTENANCE_FUNC1,b)   = getRcaAndNrBytes('SET_MAINTENANCE_FUNCTION1')
        (self.rcaSET_REG_TO_MONITOR,b)  = getRcaAndNrBytes('SET_REG_TO_MONITOR')
        (self.rcaMAINTENANCE_FUNC2,b)   = getRcaAndNrBytes('SET_MAINTENANCE_FUNCTION2')

        # Monitor points: {<key=name>:(<rca>,<nrBytesReturned>), ...}
        self.monitor = {'GET_STATUS'   : getRcaAndNrBytes('STATUS'),
                        'GET_HL_STATUS': getRcaAndNrBytes('HL_STATUS'),
                        'GET_TEMP01'   : getRcaAndNrBytes('TEMP01'),
                        'GET_TEMP02'   : getRcaAndNrBytes('TEMP02'),
                        'GET_TEMP11'   : getRcaAndNrBytes('TEMP11'),
                        'GET_TEMP12'   : getRcaAndNrBytes('TEMP12'),
                        'GET_TEMP20'   : getRcaAndNrBytes('TEMP20'),
                        'GET_TEMP21'   : getRcaAndNrBytes('TEMP21'),
                        'GET_TEMP22'   : getRcaAndNrBytes('TEMP22'),
                        'GET_TEMPLC'   : getRcaAndNrBytes('TEMPLC'),
                        'GET_LOAD0_XY' : getRcaAndNrBytes('LOAD0_XY'),
                        'GET_LOAD1_XY' : getRcaAndNrBytes('LOAD1_XY'),
                        'GET_LOAD2_XY' : getRcaAndNrBytes('LOAD2_XY'),
                        'GET_ARM0'     : getRcaAndNrBytes('ARM0'),
                        'GET_ARM1'     : getRcaAndNrBytes('ARM1'),
                        'GET_ARM2'     : getRcaAndNrBytes('ARM2'),
                        'GET_REG0'     : getRcaAndNrBytes('REG0'),
                        'GET_REG1'     : getRcaAndNrBytes('REG1'),
                        'GET_REG2'     : getRcaAndNrBytes('REG2'),
                        'GET_REG3'     : getRcaAndNrBytes('REG3'),
                        'GET_REG4'     : getRcaAndNrBytes('REG4'),
                        'GET_REG5'     : getRcaAndNrBytes('REG5'),
                        'GET_REG6'     : getRcaAndNrBytes('REG6'),
                        'GET_REG7'     : getRcaAndNrBytes('REG7'),
                        'GET_PROTOCOL_REV_LEVEL' : (0x30000, 3),
                        'GET_CAN_ERROR'          : (0x30001, 4),
                        'GET_TRANS_NUM'          : (0x30002, 4),
                        'GET_AMBIENT_TEMPERATURE': (0x30003, 4),
                        'GET_SW_REV_LEVEL'       : (0x30004, 3) }
        
        self.rcaRESET_DEVICE = 0x31001

        # command: {<key=RCA>:(<nrBytes>,<rcaMonitor>,'<testName>'), ...}
        self.command = {self.rcaSET_LOAD0_XY:(self.monitor['GET_LOAD0_XY'][0],
                                              self.monitor['GET_LOAD0_XY'][1],
                                              "SET_LOAD0_XY"),
                        self.rcaSET_LOAD1_XY:(self.monitor['GET_LOAD1_XY'][0],
                                              self.monitor['GET_LOAD1_XY'][1],
                                              "SET_LOAD1_XY"),
                        self.rcaSET_LOAD2_XY:(self.monitor['GET_LOAD2_XY'][0],
                                              self.monitor['GET_LOAD2_XY'][1],
                                              "SET_LOAD2_XY"),
                        }
        self.genericMonitor = {'GET_SERIAL_NUMBER'      : (0x00000, 8),
                               'GET_PROTOCOL_REV_LEVEL' : (0x30000, 3),
                               'GET_CAN_ERROR'          : (0x30001, 4),
                               'GET_TRANS_NUM'          : (0x30002, 4),
                               'GET_AMBIENT_TEMPERATURE': (0x30003, 4),
                               'GET_SW_REV_LEVEL'       : (0x30004, 3) }
        # genericMonitorRCAs: {<key=RCA>:<nrBytes>, ...}
        self.genericMonitorRCAs = dict(self.genericMonitor.values())
        # monitorRCAs: {<key=RCA>:<nrBytes>, ...}
        self.monitorRCAs = dict(self.monitor.values())

        self.out = ""
    def __del__(self):
        if self.out != None:
            print self.out
        
        del(self.manager)


    def startTestLog(self, testDescription):
        self.log("\n")
        self.log("-------------------------------------------------------")
        callerName = sys._getframe(1).f_code.co_name
        self.log(callerName + ": start " + testDescription + "\n")

    def endTestLog(self, testDescription):
        callerName = sys._getframe(1).f_code.co_name
        self.log("\n" + callerName + ": end " + testDescription)
        self.log("-------------------------------------------------------")
                 
    def log(self, msg):
        #!# I wonder why you'd want to collect output for printing only
        #!# when the test is finished, instead of printing as the test goes.
        #self.out += msg + "\n"
        print msg
        # In case the output is re-directed to a file (e.g. by tat), these prints
        # may get intermixed with the output from pyunit, due to buffering. To
        # avoid that, do a flush.
        sys.stdout.flush()
        
    def getConfirmationOrWait(self, prompt, errStr):
        '''
        Either prompt and wait for acknowledge or wait a number of seconds.
        This method is intended to be used within methods that move motors
        to different position, where each new position must be verified
        (in this case there will simply be a pause).
        '''
        self.log("Will continue after sleeping %.2fs" % confDelay)
        sleep (confDelay)

    ############################################################
    # Test Definitions
    def broadcast(self):
        '''
        Verify that the device correctly responds to broadcast messages
        and return the serial number when requested by RCA 0x0.

        For recordkeeping the serial number is returned.
        '''
        self.startTestLog("Broadcast Response test")
        sleep(1)
        
        try:
            (r,t) = self.manager.getNodes(self.channel)
        except:
            self.fail("Unable to get nodes on bus")

        foundNode = False
        for node in r:
            if node.node == self.node:
                if not foundNode:
                    foundNode = True
                    self.log("Device on node 0x%.4x serial number: 0x%x = %d" %
                             (node.node, node.sn, node.sn))
                else:
                    self.fail("Node 0x%x responded at least twice" % self.node)
            else:
                self.log("(Device on node 0x%.4x replied, serial Number 0x%x)" %
                         (node.node, node.sn))
                pass

        self.failUnless(foundNode, "Node %d did not respond" % self.node)

        try:
            (sn, time) = self.manager.findSN(self.channel, self.node)
        except:
            self.fail("Exception retrieving serial number from node")

        self.endTestLog("Broadcast Response test")

    # --------------------------------------------------
    def genericMonitors(self):
        '''
        Verify that all mandatory generic monitor points are implemented.
        '''
        self.startTestLog("Mandatory Generic Monitor Points test")
        # The keys() method returns an *unsorted* list!
        for name in sorted(self.genericMonitor.keys()):
            rca = self.genericMonitor[name][0]
            try:
                (r,t) = self.manager.monitor(self.channel, self.node, rca)
                self.failUnless(len(r) == self.genericMonitorRCAs[rca],
                                "Response-length for %s (0x%.5x) should be %d but is %d"
                                % (name, rca, self.genericMonitorRCAs[rca], len(r)))
                
                if len(r) == 8:
                    # Must be request for serial number
                    (sn,) = struct.unpack("!Q", r)
                    self.log("Reply on %s (0x%.5x): 0x%.8x = %d" % (name, rca, sn, sn))
                elif len(r) == 4:
                    # must be number of CAN errors, number of transmissions or ambient temperature
                    if name == 'GET_AMBIENT_TEMPERATURE' :
                        # Note: this temperature can also be obtained via
                        #    self.manager.getTemperature() 
                        # For DS1820 on AMBSI-2, MSByte of temperature can only be 0x00 or 0xFF
                        # (see DS1820 data sheet pages 3 & 4 for details)
                        if r[0] == '\xFF' or r[0] == '\x00':
                            # Big-endian order ( = network order) - how it should be
                            (temp1, cRemain, cPerDegree) = struct.unpack(">hBB", r)
                        else:
                            # Little-endian order or degenerate case (bug in AMBSI-2)
                            (temp1, cRemain, cPerDegree) = struct.unpack("<hBB", r)
                        # See DS1820 data sheet pages 3 & 4 for details
                        if cPerDegree == 0 :
                            # This happens e.g. when using AMBLBsimulator.
                            # Avoid division by 0
                            temp = 0.0
                        else :
                            temp = temp1/2 - 0.25 + float(cPerDegree - cRemain)/cPerDegree

                        self.log("Reply on %s (0x%.5x): %.2f degrees C (%d, %d, %d)" % (name, rca, temp, temp1, cRemain, cPerDegree))
                    else:    
                        (val,)  = struct.unpack("!I", r)
                        self.log("Reply on %s (0x%.5x): %d" % (name, rca, val))
                elif len(r) == 3:
                    # must be release number major.minor.patchLevel
                    (major, minor, patch)  = struct.unpack("!3B", r)
                    self.log("Reply on %s (0x%.5x): %d.%d.%d" % (name, rca, major, minor, patch))
                
            except ControlExceptions.CAMBErrorEx:
                self.fail("Exception generated, monitoring point 0x%.5x" % rca)

        self.endTestLog("Mandatory Generic Monitor Points test")


    # --------------------------------------------------
    def replyLength(self):
        '''
        This test ensures that monitors documented in the ICD respond with
        the correct number of bytes.
        '''
        self.startTestLog("Monitor reply length test")
        # The keys() method returns an *unsorted* list!
        for name in sorted(self.monitor.keys()):
            rca = self.monitor[name][0]
            (r,t) = self.manager.monitor(self.channel, self.node, rca)
            self.failIf(len(r) != self.monitor[name][1],
                        "RCA 0x%.5x (%s) returned %d bytes (expected %d)" %
                        (rca, name, len(r),self.monitor[name][1]))

        self.endTestLog("Monitor reply length test")

    # --------------------------------------------------
    def status(self):
        '''
        Verify an appropiate response from the GET_STATUS monitor point.
        Note that error-bits can be set if ACD has just been powered on.
        #!#NOTE2: this test can be removed - it is part of "replyLength"
        '''
        self.startTestLog("GET_STATUS tests")
        (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_STATUS'][0])
        errStr = "GET_STATUS returned incorrect length value, actual = "
        errStr += str(len(r)) + " bytes, expected 2 bytes"
        self.failUnless(len(r) == 2, errStr)

        status = printStatus(self)
        self.endTestLog("GET_STATUS tests")

    # --------------------------------------------------
    def nodeId(self):
        '''
        This test verifies if the device responds to a wrong node-ID,
        for monitors documented in the ICD.
        '''
        self.startTestLog("Node-ID test")
        # The keys() method returns an *unsorted* list!
        nodeId = self.node
        for name in sorted(self.monitor.keys()):
            rca = self.monitor[name][0]
            nodeId = nodeId + 1
            try:
                (r,t) = self.manager.monitor(self.channel, nodeId, rca)
                self.fail("Got a reply for node-id 0x%.2x, RCA 0x%.5x (%s)" % (nodeId, rca, name))
                
            except Exception, e:
                #print("---> exception " + str(e))
                pass
            

        self.endTestLog("Node-ID test")

    # --------------------------------------------------
    def resetAndInit(self):
        '''
        Verify the RESET plus INIT control points, and an appropiate response
        from the GET_STATUS monitor point.
        '''
        self.startTestLog("RESET/INIT tests")

        self.log ("Before RESET:")
        printStatus(self)
        
        # Do a RESET
        #!# Note: qwpError, armError, wheelError can be set after RESET?
        #!# (inPosition is reset, but that is OK).
        data = 0x00
        self.log("testResetAndInit - sending RESET")
        t = self.manager.command(self.channel, self.node, self.rcaRESET,
                                 struct.pack("!B", data))
        sleep(1.5)

        self.log ("After RESET, just before INIT::")
        printStatus(self)

        # Do an INIT and wait till in Position
        self.log("testResetAndInit - sending INIT")
        t = self.manager.command(self.channel, self.node, self.rcaINIT,
                                 struct.pack("!B", data))
        
        sleep(1.5)
        self.log ("1.5 s after INIT:")
        printStatus(self)

        elapsedTime = waitTillInPosition (self, 27.0)
        
        self.log ("After INIT is ready:")
        printStatus(self)

        self.endTestLog("RESET/INIT tests")

    # --------------------------------------------------
    def setHotLoad(self):
        '''
        Verify the SET_HOT_LOAD control point.
        '''
        self.startTestLog("SET_HOT_LOAD tests")
        #!# Note1: cartridge number starts counting from 1 for this cmd.
        #!# Note2: ICD does not say what happens if cartridge number is out-of-range
        #!#   (looks like no action is taken, no error).
        for data in range(0x00, len(cartridgeNames)) :
            self.log("\nMoving hotload to position %d/ '%s'" % (data, cartridgeNames[data]))
            t = self.manager.command(self.channel, self.node, self.rcaSET_HOT_LOAD,
                                     struct.pack("!B", data))
            elapsedTime = waitTillInPosition (self, 5.0)
            printStatus(self)
            self.getConfirmationOrWait("", "User requested to quit.")

        self.endTestLog("SET_HOT_LOAD tests")

    # --------------------------------------------------
    def setAmbientLoad(self):
        '''
        Verify the SET_AMBIENT_LOAD control point.
        '''
        self.startTestLog("SET_AMBIENT_LOAD tests")
        #!# Note1: cartridge number starts counting from 1 for this cmd.
        #!# Note2: ICD does not say what happens if cartridge number is out-of-range
        #!#   (looks like no action is taken, no error).
        for data in range(0x00, len(cartridgeNames)) :
            self.log("\nMoving ambient load to position %d/ '%s'" % (data, cartridgeNames[data]))
            t = self.manager.command(self.channel, self.node, self.rcaSET_AMBIENT_LOAD,
                                     struct.pack("!B", data))
            elapsedTime = waitTillInPosition (self, 5.0)
            printStatus(self)
            self.getConfirmationOrWait("", "User requested to quit.")

        self.endTestLog("SET_AMBIENT_LOAD tests")

    # --------------------------------------------------
    def setSolarFilter(self):
        '''
        Verify the SET_SOLAR_FILTER control point.
        '''
        self.startTestLog("SET_SOLAR_FILTER tests")
        #!# Note1: cartridge number starts counting from 1 for this cmd.
        #!# Note2: ICD does not say what happens if cartridge number is out-of-range
        #!#   (looks like no action is taken, no error).
        for data in range(0x00, len(cartridgeNames)) :
            self.log("\nMoving solar filter to position %d/ '%s'" % (data, cartridgeNames[data]))
            t = self.manager.command(self.channel, self.node, self.rcaSET_SOLAR_FILTER,
                                     struct.pack("!B", data))
            elapsedTime = waitTillInPosition (self, 5.0)
            printStatus(self)
            self.getConfirmationOrWait("", "User requested to quit.")
            
        self.endTestLog("SET_SOLAR_FILTER tests")

    # --------------------------------------------------
    def park0Loads(self):
        '''
        Verify the PARK0_LOADS control point.
        '''
        self.startTestLog("PARK0_LOADS tests")
        data = 0x00
        t = self.manager.command(self.channel, self.node, self.rcaPARK0_LOADS,
                                 struct.pack("!B", data))
        elapsedTime = waitTillInPosition (self, 5.0)
        printStatus(self)
            
        self.endTestLog("PARK0_LOADS tests")

    # --------------------------------------------------
    def park1Loads(self):
        '''
        Verify the PARK1_LOADS control point.
        '''
        self.startTestLog("PARK1_LOADS tests")
        data = 0x00
        t = self.manager.command(self.channel, self.node, self.rcaPARK1_LOADS,
                                 struct.pack("!B", data))
        elapsedTime = waitTillInPosition (self, 5.0)
        printStatus(self)
            
        self.endTestLog("PARK1_LOADS tests")

    # --------------------------------------------------
    def stowLoads(self):
        '''
        Verify the STOW_LOADS control point.
        '''
        self.startTestLog("STOW_LOADS Tests")
        data = 0x00
        t = self.manager.command(self.channel, self.node, self.rcaSTOW_LOADS,
                                 struct.pack("!B", data))
        elapsedTime = waitTillInPosition (self, 5.0)
        printStatus(self)
            
        self.endTestLog("STOW_LOADS tests")

    # --------------------------------------------------
    def setHeat(self):
        '''
        Verify the SET_HEAT control point.
        '''
        self.startTestLog("SET_HEAT tests")
        data = 656
        self.log("Setting the hot load temperature to %.1f degrees C" % (data/10.,))
        t = self.manager.command(self.channel, self.node, self.rcaSET_HEAT,
                                 struct.pack("!H", data))
            
        self.endTestLog("SET_HEAT tests")

    # --------------------------------------------------
    def setLoadiXY(self):
        '''
        Verify the SET_LOADi_XY control point and the GET_LOADi_XY monitor point.
        '''
        self.startTestLog("SET_LOADi_XY/GET_LOADi_XY tests")

        self.log("First INIT, to start from known position.")
        data = 0x00
        t = self.manager.command(self.channel, self.node, self.rcaINIT,
                                 struct.pack("!B", data))
        elapsedTime = waitTillInPosition (self, 25.0)
        printStatus(self)

        control = [self.rcaSET_LOAD0_XY,
                   self.rcaSET_LOAD1_XY,
                   self.rcaSET_LOAD2_XY]
        monitor = [self.monitor['GET_LOAD0_XY'][0],
                   self.monitor['GET_LOAD1_XY'][0],
                   self.monitor['GET_LOAD2_XY'][0]]
        for i in range(3) :
            self.log("............")
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            errStr = "GET_XY_LOAD" + str(i) +" returned incorrect length value, actual = "
            errStr += str(len(r)) + " bytes, expected 8 bytes (2 floats)"
            self.failUnless(len(r) == 8, errStr)
            (x0,y0) = struct.unpack("!2f" ,r)
            x = x0 - 15.0
            y = y0 + 15.0
            self.log("LOAD %d currently at X/Y = %.2f/%.2f, will move to %.2f/%.2f"
                     % (i, x0, y0, x, y))
            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!2f", x, y))
            #!# it can take > 0.25s for the in-position bit to get cleared!
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (x1,y1) = struct.unpack("!2f", r)

            errStr = "LOAD %d position not consistent with position set.\n" % i
            errStr += "Set: %.2f/%.2f" % (x,y)
            errStr += " <--> Status: %.2f/%.2f" % (x1,y1)
            #!# Note: not clear what tolerances are
            self.failIf(abs(x1 - x) > tolerance, errStr)
            self.failIf(abs(y1 - y) > tolerance, errStr)

            self.log ("LOAD %d in position, X/Y = %.2f/%.2f, attempt to move out of range"
                      % (i, x1,y1))
            x =  500.0
            y = -500.0
            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!2f", x, y))
            elapsed = waitTillInPosition(self, 5.0)
            status = printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (x1,y1) = struct.unpack("!2f", r)
            errStr = "SET_LOAD%d_XY X/Y = %.2f/%.2f did not set out-of-range bit.\n" % (i, x, y)
            errStr += "Current position: %.2f/%.2f" % (x1,y1)
            self.failUnless(status['setLoadiXYOOR'], errStr)

            self.log ("LOAD %d in position, X/Y = %.2f/%.2f, back to original position"
                      % (i, x1, y1))

            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!2f", x0, y0))
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (x2,y2) = struct.unpack("!2f", r)
            
            errStr = "LOAD %d position not consistent with position set.\n" % i
            errStr += "Set: %.2f/%.2f" % (x0,y0)
            errStr += " <--> Status: %.2f/%.2f" % (x2,y2)
            self.failIf(abs(x2 - x0) > tolerance, errStr)
            self.failIf(abs(y2 - y0) > tolerance, errStr)

            self.log ("LOAD %d in position, X/Y = %.2f/%.2f"
                      % (i, x2, y2))

            self.getConfirmationOrWait("", "User requested to quit.")
            
        self.endTestLog("SET_LOADi_XY/GET_LOADi_XY tests")

    # --------------------------------------------------
    def setLoadidXdY(self):
        '''
        Verify the SET_LOADi_dXdY control point and the GET_LOADi_XY monitor point.
        '''
        self.startTestLog("SET_LOADi_dXdY/GET_LOADi_XY tests")

        control = [self.rcaSET_LOAD0_dXdY,
                   self.rcaSET_LOAD1_dXdY,
                   self.rcaSET_LOAD2_dXdY]
        monitor = [self.monitor['GET_LOAD0_XY'][0],
                   self.monitor['GET_LOAD1_XY'][0],
                   self.monitor['GET_LOAD2_XY'][0]]
        for i in range(3) :
            self.log("............")
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            errStr = "GET_XY_LOAD" + str(i) +" returned incorrect length value, actual = "
            errStr += str(len(r)) + " bytes, expected 8 bytes (2 floats)"
            self.failUnless(len(r) == 8, errStr)
            (x0,y0) = struct.unpack("!2f" ,r)
            dx = -15.0
            dy =  15.0
            self.log("LOAD %d currently at X/Y = %.2f/%.2f, will move %.2f/%.2f"
                     % (i, x0, y0, dx, dy))
            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!2f", dx, dy))
            #!# it can take > 0.25s for the in-position bit to get cleared!
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (x1,y1) = struct.unpack("!2f", r)

            errStr = "LOAD %d position not consistent with position set.\n" % i
            errStr += "Set: %.2f/%.2f" % (x0 + dx,y0 + dy)
            errStr += " <--> Status: %.2f/%.2f" % (x1,y1)
            #!# Note: not clear what tolerances are
            self.failIf(abs(x1 - x0 - dx) > tolerance, errStr)
            self.failIf(abs(y1 - y0 - dy) > tolerance, errStr)

            self.log ("LOAD %d in position, X/Y = %.2f/%.2f, attempt to move out of range"
                      % (i, x1,y1))

            dx1 =  501.0 - x1
            dy1 = -501.0 - y1
            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!2f", dx1, dy1))
            elapsed = waitTillInPosition(self, 5.0)
            #!# it seems to take a while for the out-of-range bit to get set!
            sleep (0.25)
            status = printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (x1,y1) = struct.unpack("!2f", r)
            errStr = "SET_LOAD%d_dXdY dX/dY = %.2f/%.2f did not set out-of-range bit.\n" % (i, dx1, dy1)
            errStr += "Current position: %.2f/%.2f" % (x1,y1)
            self.failUnless(status['setLoadidXdYOOR'], errStr)
            
            self.log ("LOAD %d in position, X/Y = %.2f/%.2f, back to original position"
                      % (i, x1, y1))

            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!2f", -dx, -dy))
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (x2,y2) = struct.unpack("!2f", r)
            
            errStr = "LOAD %d position not consistent with position set.\n" % i
            errStr += "Set: %.2f/%.2f" % (x0,y0)
            errStr += " <--> Status: %.2f/%.2f" % (x2,y2)
            self.failIf(abs(x2 - x1 + dx) > tolerance, errStr)
            self.failIf(abs(y2 - y1 + dy) > tolerance, errStr)

            self.log ("LOAD %d in position, X/Y = %.2f/%.2f"
                      % (i, x2, y2))

            self.getConfirmationOrWait("", "User requested to quit.")
            
        self.endTestLog("SET_LOADi_dXdY/GET_LOADi_XY tests")

    # --------------------------------------------------
    def setArmi(self):
        '''
        Verify the SET_ARMi control point and the GET_ARMi_ monitor point.
        '''
        self.startTestLog("SET_ARMi/GET_ARMi tests")

        control = [self.rcaSET_ARM0,
                   self.rcaSET_ARM1,
                   self.rcaSET_ARM2]
        monitor = [self.monitor['GET_ARM0'][0],
                   self.monitor['GET_ARM1'][0],
                   self.monitor['GET_ARM2'][0]]
        if qwpInstalled :
            arms = [0, 1, 2]
        else :
            arms = [0, 1]
            self.log("QWP not installed - will skip SET_ARM2/GET_ARM2 tests")
                
        for i in arms :
            self.log("............")
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            errStr = "GET_ARM" + str(i) +" returned incorrect length value, actual = "
            errStr += str(len(r)) + " bytes, expected 4 bytes (1 integer)"
            self.failUnless(len(r) == 4, errStr)
            (pos0,) = struct.unpack("!i" ,r)
            pos  = pos0 + 1000
            self.log("ARM%d currently at position %d, will move to %d"
                     % (i, pos0, pos))
            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!i", pos))
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (pos1,) = struct.unpack("!i", r)

            errStr = "ARM%d position not consistent with position set.\n" % i
            errStr += "Set: %d <--> Status: %d" % (pos, pos1)
            #!# Note: not clear what tolerances are
            self.failIf(abs(pos1 - pos) > 100, errStr)

            self.log ("ARM%d in position (%d), attempt to move out of range" % (i, pos))

            pos = 300000
            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!i", pos))
            elapsed = waitTillInPosition(self, 5.0)
            status = printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (pos1,) = struct.unpack("!i", r)
            errStr = "SET_ARM%d pos = %d did not set out-of-range bit.\n" % (i, pos)
            errStr += "Current position: %d" % pos1
            self.failUnless(status['setArmiOOR'], errStr)
            
            self.log ("ARM%d in position, (%d), back to original position"
                      % (i, pos1))

            t = self.manager.command(self.channel, self.node, control[i],
                                     struct.pack("!i", pos0))
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            (r,t) = self.manager.monitor(self.channel, self.node, monitor[i])
            (pos2,) = struct.unpack("!i", r)
            
            errStr = "ARM%d position not consistent with position set.\n" % i
            errStr += "Set: %d <--> Status: %d" % (pos0, pos2)
            self.failIf(abs(pos2 - pos0) > 100, errStr)

            self.log ("ARM%d in position (%d)" % (i, pos2))

            self.getConfirmationOrWait("", "User requested to quit.")
            
        self.endTestLog("SET_LOADi_XY/GET_LOADi_XY tests")

    # --------------------------------------------------
    def setQWP(self):
        '''
        Verify the SET_QUARTERWAVE_PLATE/STOW_QUARTERWAVE_PLATE control points.
        '''
        self.startTestLog("SET_QUARTERWAVE_PLATE/STOW_QUARTERWAVE_PLATE tests")

        if qwpInstalled :
            data = 0x00
            t = self.manager.command(self.channel, self.node, self.rcaSET_QWP, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            
            t = self.manager.command(self.channel, self.node, self.rcaSTOW_QWP, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)

        else:
            self.log("QWP not installed - skipping tests")

        self.endTestLog("SET_QUARTERWAVE_PLATE/STOW_QUARTERWAVE_PLATE tests")

    # --------------------------------------------------
    def calQWP(self):
        '''
        Verify the CAL_QWP_* commands.
        Relies on the SET_QUARTERWAVE_PLATE/STOW_QUARTERWAVE_PLATE control points.
        '''
        self.startTestLog("CAL_QWP_* tests")

        if not qwpInstalled :
            self.log("QWP not installed - skipping tests")
            self.endTestLog("CAL_QWP_* tests")
            return

        # <key=SET|STOW>:(RCA SET_QWP|STOW_QWP, RCA CAL_QWP_SET|STOW, RCA STOW_QWP|SET_QWP,
        rcaList = {'SET':(self.rcaSET_QWP, self.rcaCAL_QWP_SET, self.rcaSTOW_QWP) , 'STOW': (self.rcaSTOW_QWP, self.rcaCAL_QWP_STOW, self.rcaSET_QWP)}

        for cmd in ('SET', 'STOW') :
            self.log("\nCAL_QWP_" + cmd + " test ............")
            data = 0x00
            # SET|STOW_QUARTERWAVE_PLATE
            RCA = rcaList[cmd][0]
            t = self.manager.command(self.channel, self.node, RCA, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 5.0)
            printStatus(self)
            # Extra wait to let motors fine-position.
            sleep(0.25)
        
            # GET_ARM2 - get position corresponding to actual calibration
            #!# Note: this is only right within margins of tolerance, i.e. there
            #!# is a monitor point missing to read back stored calibration values.
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM2'][0])
            (pos1,) = struct.unpack("!i", r)
            self.log ("Current calibrated position: arm2 = %d" % pos1)
        
            # SET_ARM2 - move to new position that will serve as calibrated position
            pos2 = pos1 + 567
            t = self.manager.command(self.channel, self.node, self.rcaSET_ARM2, struct.pack("!i", pos2))
            elapsed = waitTillInPosition(self, 5.0)
            # Extra wait to let motors fine-position.
            sleep (0.25)
            self.log ("Moved to new calibration position: arm2 = %d" % pos2)

            # CAL_QWP_SET|STOW - store new calibration value
            RCA = rcaList[cmd][1]
            t = self.manager.command(self.channel, self.node, RCA, struct.pack("!B", data))
            sleep (0.01)
            # STOW|SET_QUARTERWAVE_PLATE/SET|STOW_QUARTERWAVE_PLATE - move away and
            # back to new calibration value
            self.log ("Moving away to SET/STOW position ...")
            RCA = rcaList[cmd][2]
            t = self.manager.command(self.channel, self.node, RCA, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 2.5)
            self.log ("Moving back to newly calibrated %s position ..." % cmd)
            RCA = rcaList[cmd][0]
            t = self.manager.command(self.channel, self.node, RCA, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 2.5)
            # Extra wait to let motors fine-position.
            sleep (0.25)

            # GET_ARM2 - get position corresponding to new calibration
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM2'][0])
            (pos,) = struct.unpack("!i", r)
            self.log ("Gone to newly calibrated position: arm2 = %d" % pos)
        
            errStr = "QWP position not consistent with new %s calibration value.\n" % cmd
            errStr += "Set: %d <--> Status: %d" % (pos2, pos)
            #!# Note: not clear what tolerances are
            self.failIf(abs(pos2 - pos) > 50, errStr)
                                     
            # RESTORE_DEFAULT_CAL - restore original condition
            #!# Actually, we should set pos1 back as calibration value, but pos1
            #!# is not 100% identical to the originally stored value (see above).
            #!# Note also that this command seems to take some time to process, i.e.
            #!# the ACD is not ready to move till ~3s are passed.
            t = self.manager.command(self.channel, self.node, self.rcaRESTORE_DEFAULT_CAL, struct.pack("!B", data))
            sleep (5.0)
        
            # SET_QUARTERWAVE_PLATE - move to original calibration value
            RCA = rcaList[cmd][0]
            t = self.manager.command(self.channel, self.node, RCA, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 2.5)
            # Extra wait to let motors fine-position.
            sleep (0.25)
            # GET_ARM2 - get position corresponding to original calibration
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM2'][0])
            (pos,) = struct.unpack("!i", r)
            self.log ("Gone to original calibrated position: arm2 = %d" % pos)
        
            errStr = "QWP position not consistent with original %s calibration value.\n" % cmd
            errStr += "Set: %d <--> Status: %d" % (pos1, pos)
            #!# Note: not clear what tolerances are
            self.failIf(abs(pos1 - pos) > 50, errStr)
        
        self.endTestLog("CAL_QWP_* tests")

    # --------------------------------------------------
    def calLoadiBandj(self):
        '''
        Verify the CAL_LOADi_BANDj command.
        Relies on the SET_ARM0/1 and GET_ARM0/1 points.
        '''
        self.startTestLog("CAL_LOADi_BANDj test")

        # SET_<LOAD> control point RCAs in order: ambient load, hot load, solar filter
        rcaList = (self.rcaSET_AMBIENT_LOAD,  self.rcaSET_HOT_LOAD, self.rcaSET_SOLAR_FILTER)

        #!# should test for load 3 as well
        for load in range(0,3) :
            loadName = loadNames[load]
            rcaSetLoad = rcaList[load]
            #!# should test for bands 0, 12, 13, 14 and 15 as well.
            for band in range(1,12) :
                self.log("\nCalibrating %s for %s ............." % (loadName, cartridgeNames[band]))
                # SET_<LOAD>
                t = self.manager.command(self.channel, self.node, rcaSetLoad, struct.pack("!B", band))
                elapsed = waitTillInPosition(self, 5.0)
                printStatus(self)
                # Extra wait to let motors fine-position.
                sleep (0.25)
        
                # GET_ARM0/1 - get position corresponding to actual calibration
                #!# Note: this is only right within margins of tolerance, i.e. there
                #!# is a monitor point missing to read back stored calibration values.
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM0'][0])
                (arm0Pos1,) = struct.unpack("!i", r)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM1'][0])
                (arm1Pos1,) = struct.unpack("!i", r)
                self.log ("Current calibrated positions: arm0 = %d, arm1 = %d" % (arm0Pos1, arm1Pos1))
        
                # SET_ARM0/1 - move to new position that will serve as calibrated position
                arm0Pos2 = arm0Pos1 + 456
                t = self.manager.command(self.channel, self.node, self.rcaSET_ARM0, struct.pack("!i", arm0Pos2))
                elapsed = waitTillInPosition(self, 5.0)
                arm1Pos2 = arm1Pos1 + 567
                t = self.manager.command(self.channel, self.node, self.rcaSET_ARM1, struct.pack("!i", arm1Pos2))
                elapsed = waitTillInPosition(self, 5.0)
                self.log ("Moved to new calibration positions: arm0 = %d, arm1 = %d" % (arm0Pos2, arm1Pos2))

                # CAL_LOADi_BANDj - store new calibration value
                data = 0x00
                t = self.manager.command(self.channel, self.node, self.rcaCAL_LOADi_BANDj, struct.pack("!2B", load, band))
                sleep (0.01)
                # move away and back to new calibration value
                nextBand = (band + 1) % 11
                self.log ("Moving away to band %d ..." % nextBand)
                t = self.manager.command(self.channel, self.node, rcaSetLoad, struct.pack("!B", nextBand))
                elapsed = waitTillInPosition(self, 2.5)
                self.log ("Moving back to newly calibrated band %d ..." % band)
                t = self.manager.command(self.channel, self.node, rcaSetLoad, struct.pack("!B", band))
                elapsed = waitTillInPosition(self, 2.5)
                # Extra wait to let motors fine-position.
                sleep(0.25)
                # GET_ARM0/1 - get position corresponding to actual (new) calibration
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM0'][0])
                (arm0Pos,) = struct.unpack("!i", r)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM1'][0])
                (arm1Pos,) = struct.unpack("!i", r)
                self.log ("Gone to new calibrated positions: arm0 = %d, arm1 = %d" % (arm0Pos, arm1Pos))
                
                errStr = "%s position not consistent with new calibration values.\n" % loadName
                errStr += "Set: %d/%d <--> Status: %d/%d" % (arm0Pos2, arm1Pos2, arm0Pos, arm1Pos)
                #!# Note: not clear what tolerances are
                self.failIf(abs(arm0Pos2 - arm0Pos) > 50 or abs(arm1Pos2 - arm1Pos) > 50, errStr)
                # RESTORE_DEFAULT_CAL - restore original condition
                #!# Actually, we should set armxPos1 back as calibration value, but
                #!# it is not 100% identical to the originally stored value (see above).
                #!# Note also that this command seems to take some time to process, i.e.
                #!# the ACD is not ready to move till ~3s are passed.
                t = self.manager.command(self.channel, self.node, self.rcaRESTORE_DEFAULT_CAL, struct.pack("!B", data))
                sleep (5.0)
        
                # SET_<LOAD> - move to original calibration value
                t = self.manager.command(self.channel, self.node, rcaSetLoad, struct.pack("!B", band))
                elapsed = waitTillInPosition(self, 2.5)
                # Extra wait to let motors fine-position.
                sleep (0.25)
                # GET_ARM0/1 - get position corresponding to original calibration
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM0'][0])
                (arm0Pos,) = struct.unpack("!i", r)
                (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM1'][0])
                (arm1Pos,) = struct.unpack("!i", r)
                self.log ("Gone to original calibrated positions: arm0 = %d, arm1 = %d" % (arm0Pos, arm1Pos))

                errStr = "%s position not consistent with original calibration values.\n" % loadName
                errStr += "Set: %d/%d <--> Status: %d/%d" % (arm0Pos1, arm1Pos1, arm0Pos, arm1Pos)
                #!# Note: not clear what tolerances are
                self.failIf(abs(arm0Pos1 - arm0Pos) > 50 or abs(arm1Pos1 - arm1Pos) > 50, errStr)
        
        self.endTestLog("CAL_LOADi_BANDj test")

    # --------------------------------------------------
    def calPark(self):
        '''
        Verify the CAL_PARK0/1 and CAL_STOW commands.
        Relies on the PARK0/1_LOADS, STOW_LOADS, SET_ARMi control points, plus the
        GET_ARMi monitor points.
        '''
        self.startTestLog("CAL_PARKx/CAL_STOW test")

        # <key=position>:(RCA CAL_position, RCA position_LOADS, RCA positionAway_LOADS)
        rcaList = {'PARK0':(self.rcaCAL_PARK0, self.rcaPARK0_LOADS, self.rcaPARK1_LOADS),
                   'PARK1':(self.rcaCAL_PARK1, self.rcaPARK1_LOADS, self.rcaSTOW_LOADS),
                   'STOW': (self.rcaCAL_STOW, self.rcaSTOW_LOADS,  self.rcaPARK0_LOADS)}

        for position in sorted(rcaList.keys()):
            self.log("\nCalibrating %s position ............." % position)
            rcaCal  = rcaList[position][0]
            rcaPark = rcaList[position][1]
            rcaAway = rcaList[position][2]
            # PARK0/1_LOADS or STOW_LOADS
            data = 0x00
            t = self.manager.command(self.channel, self.node, rcaPark,
                                     struct.pack("!B", data))
            elapsedTime = waitTillInPosition (self, 5.0)
            printStatus(self)
            # Extra wait to let motors fine-position.
            sleep (0.25)
        
            # GET_ARM0/1 - get position corresponding to actual calibration
            #!# Note: this is only right within margins of tolerance, i.e. there
            #!# is a monitor point missing to read back stored calibration values.
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM0'][0])
            (arm0Pos1,) = struct.unpack("!i", r)
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM1'][0])
            (arm1Pos1,) = struct.unpack("!i", r)
            self.log ("Current calibrated positions: arm0 = %d, arm1 = %d" % (arm0Pos1, arm1Pos1))

            # SET_ARM0/1 - move to new position that will serve as calibrated position
            if position == 'PARK0' :
                delta0 = 456
                delta1 = 567
            elif position == 'PARK1':
                delta0 = -456
                delta1 = 567
            elif position == 'STOW':
                delta0 = -456
                delta1 = 567
            
            arm0Pos2 = arm0Pos1 + delta0
            t = self.manager.command(self.channel, self.node, self.rcaSET_ARM0, struct.pack("!i", arm0Pos2))
            elapsed = waitTillInPosition(self, 5.0)
            arm1Pos2 = arm1Pos1 + delta1
            t = self.manager.command(self.channel, self.node, self.rcaSET_ARM1, struct.pack("!i", arm1Pos2))
            elapsed = waitTillInPosition(self, 5.0)
            self.log ("Moved to new calibration positions: arm0 = %d, arm1 = %d" % (arm0Pos2, arm1Pos2))

            # CAL_PARK0/1 or CAL_STOW - store new calibration value
            data = 0x00
            t = self.manager.command(self.channel, self.node, rcaCal, struct.pack("!B", data))
            sleep (0.01)
            # move away and back to new calibration value
            self.log ("Moving away to other park/stow position ...")
            t = self.manager.command(self.channel, self.node, rcaAway, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 2.5)
            self.log ("Moving back to newly calibrated %s position ..." % position)
            t = self.manager.command(self.channel, self.node, rcaPark, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 2.5)
            # Extra wait to let motors fine-position.
            sleep (0.25)
            # GET_ARM0/1 - get position corresponding to actual (new) calibration
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM0'][0])
            (arm0Pos,) = struct.unpack("!i", r)
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM1'][0])
            (arm1Pos,) = struct.unpack("!i", r)
            self.log ("Gone to new calibrated positions: arm0 = %d, arm1 = %d" % (arm0Pos, arm1Pos))
                
            errStr = "%s position not consistent with new calibration values.\n" % position
            errStr += "Set: %d/%d <--> Status: %d/%d" % (arm0Pos2, arm1Pos2, arm0Pos, arm1Pos)
            #!# Note: not clear what tolerances are
            self.failIf(abs(arm0Pos2 - arm0Pos) > 50 or abs(arm1Pos2 - arm1Pos) > 50, errStr)
            # RESTORE_DEFAULT_CAL - restore original condition
            #!# Actually, we should set armxPos1 back as calibration value, but
            #!# it is not 100% identical to the originally stored value (see above).
            #!# Note also that this command seems to take some time to process, i.e.
            #!# the ACD is not ready to move till ~3s are passed.
            t = self.manager.command(self.channel, self.node, self.rcaRESTORE_DEFAULT_CAL, struct.pack("!B", data))
            sleep (5.0)
        
            # PARK0/1_LOADS or STOW_LOADS - move to original calibration value
            t = self.manager.command(self.channel, self.node, rcaPark, struct.pack("!B", data))
            elapsed = waitTillInPosition(self, 2.5)
            # Extra wait to let motors fine-position.
            sleep (0.25)
            # GET_ARM0/1 - get position corresponding to original calibration
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM0'][0])
            (arm0Pos,) = struct.unpack("!i", r)
            (r,t) = self.manager.monitor(self.channel, self.node, self.monitor['GET_ARM1'][0])
            (arm1Pos,) = struct.unpack("!i", r)
            self.log ("Gone to original calibrated positions: arm0 = %d, arm1 = %d" % (arm0Pos, arm1Pos))

            errStr = "%s position not consistent with original calibration values.\n" % position
            errStr += "Set: %d/%d <--> Status: %d/%d" % (arm0Pos1, arm1Pos1, arm0Pos, arm1Pos)
            #!# Note: not clear what tolerances are
            self.failIf(abs(arm0Pos1 - arm0Pos) > 50 or abs(arm1Pos1 - arm1Pos) > 50, errStr)
        
        self.endTestLog("CAL_PARKx/CAL_STOW test")

    # --------------------------------------------------
    def monitorPoints(self):
        '''
        This test verifies all the other monitor points that are not tested elsewhere.
        '''
        self.startTestLog("Other Monitor Points tests")
        # testProperty(compRef, rca, testName, propName, propLen, format, minVal, maxVal)
        testProperty(self, self.monitor['GET_TEMP01'][0], 'GET_TEMPi', 'TEMP01', 2, '!H', 100, 300)
        #!# max should be 300
        testProperty(self, self.monitor['GET_TEMP02'][0], 'GET_TEMPi', 'TEMP02', 2, '!H', 100, 350)
        #!# min should be 100
        testProperty(self, self.monitor['GET_TEMP11'][0], 'GET_TEMPi', 'TEMP11', 2, '!H',   0, 300)
        testProperty(self, self.monitor['GET_TEMP12'][0], 'GET_TEMPi', 'TEMP12', 2, '!H', 100, 300)
        testProperty(self, self.monitor['GET_TEMP20'][0], 'GET_TEMPi', 'TEMP20', 2, '!H', 100, 1000)
        testProperty(self, self.monitor['GET_TEMP21'][0], 'GET_TEMPi', 'TEMP21', 2, '!H', 100, 1000)
        testProperty(self, self.monitor['GET_TEMP22'][0], 'GET_TEMPi', 'TEMP22', 2, '!H', 100, 1000)
        testProperty(self, self.monitor['GET_TEMPLC'][0], 'GET_TEMPi', 'TEMPLC', 2, '!H', 100, 500)
        testProperty(self, self.monitor['GET_REG0'][0], 'GET_REGi', 'REG0', 4, '!I', 0, 0xFFFFFFFF)
        testProperty(self, self.monitor['GET_REG1'][0], 'GET_REGi', 'REG1', 4, '!I', 0, 0xFFFFFFFF)
        testProperty(self, self.monitor['GET_REG2'][0], 'GET_REGi', 'REG2', 4, '!I', 0, 0xFFFFFFFF)
        testProperty(self, self.monitor['GET_REG3'][0], 'GET_REGi', 'REG3', 4, '!I', 0, 0xFFFFFFFF)
        testProperty(self, self.monitor['GET_REG4'][0], 'GET_REGi', 'REG4', 4, '!I', 0, 0xFFFFFFFF)
        testProperty(self, self.monitor['GET_REG5'][0], 'GET_REGi', 'REG5', 4, '!I', 0, 0xFFFFFFFF)
        testProperty(self, self.monitor['GET_REG6'][0], 'GET_REGi', 'REG6', 4, '!I', 0, 0xFFFFFFFF)
        testProperty(self, self.monitor['GET_REG7'][0], 'GET_REGi', 'REG7', 4, '!I', 0, 0xFFFFFFFF)
        self.endTestLog("Other Monitor Points tests")

    # --------------------------------------------------
    def resetDevice(self):
        '''
        This test verifies the RESET_DEVICE control point
        '''
        self.startTestLog("RESET_DEVICE test")
        t = self.manager.command(self.channel, self.node, self.rcaRESET_DEVICE,
                                 struct.pack("!B", 1))
        sleep (1.)
        printStatus(self)
        
        self.endTestLog("RESET_DEVICE test")

    # --------------------------------------------------
    def resetDevice2(self):
        '''
        This test issues a RESET command as defined in the ICD
        '''
        self.startTestLog("RESET command")
        t = self.manager.command(self.channel, self.node, self.rcaRESET,
                                 struct.pack("!B", 1))
        sleep (1.)
        printStatus(self)
        
        self.endTestLog("RESET command")

    # --------------------------------------------------
    def calibrationCycle(self):
        '''
        This test performs a calibration cycle and measures the timing
        '''
        self.startTestLog("Calibration cycle")
        perfMsg = ""

        for band in range (1,12) :
                
            for park in [self.rcaPARK0_LOADS, self.rcaPARK1_LOADS] :
                # go to PARKx
                data = 0x00
                t = self.manager.command(self.channel, self.node, park,
                                         struct.pack("!B", data))
                elapsedTime = waitTillInPosition (self, 5.0)
                printStatus(self)

                # Ambient load to band x
                self.log("\nStarting timer and moving ambient load to position %d / '%s'" % (band, cartridgeNames[band]))
                startTime = time()
                t = self.manager.command(self.channel, self.node, self.rcaSET_AMBIENT_LOAD,
                                         struct.pack("!B", band))
                elapsedTime = waitTillInPosition (self, 5.0)
                printStatus(self)
                
                currentTime = time()
                self.log("\nElapsed time: %.3fs. Will sleep for 2 seconds now" % (currentTime - startTime))

                sleep (2.0)

                # Hot load to band x
                self.log("\nMoving hotload to position %d / '%s'" % (band, cartridgeNames[band]))
                t = self.manager.command(self.channel, self.node, self.rcaSET_HOT_LOAD,
                                         struct.pack("!B", band))
                elapsedTime = waitTillInPosition (self, 5.0)
                printStatus(self)

                currentTime = time()
                self.log("\nElapsed time: %.3fs. Will sleep for 2 seconds now" % (currentTime - startTime))
                
                sleep (2.0)

                # go back to PARKx
                data = 0x00
                t = self.manager.command(self.channel, self.node, park,
                                         struct.pack("!B", data))
                elapsedTime = waitTillInPosition (self, 5.0)
                printStatus(self)

                endTime = time()

                duration = endTime - startTime
                if park == self.rcaPARK0_LOADS :
                    durationPark0 = duration
                else:
                    durationPark1 = duration
                self.log("\nNeeded %.3fs for entire cycle, incl. 2x2 seconds waiting with loads positioned." % duration)
                self.log("-----------------------------------------------------------------------" )

            #
            perfMsg += "Band %2d / '%7s': %6.3fs from PARK0 <-> %6.3fs from PARK1.\n" % (band, cartridgeNames[band], durationPark0, durationPark1)
            self.log("-----------------------------------------------------------------------" )

        self.log("\nPositioning timing data (incl. 2x2 seconds waiting with loads positioned):") 
        self.log("==========================================================================")
        self.log(perfMsg)
        self.log("==========================================================================")
        self.endTestLog("Calibration cycle")

        
class Interactive(Automated):
    def __init__(self, methodName="runTest"):
        Automated.__init__(self,methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg
        # In case the output is re-directed to a file (e.g. by tat), these prints
        # may get intermixed with the output from pyunit, due to buffering. To
        # avoid that, do a flush.
        sys.stdout.flush()

    def getConfirmationOrWait(self, prompt, errStr):
        '''
        Prompt and wait for acknowledge.
        This method is intended to be used within methods that move motors
        to different position, where each new position must be verified
        (in this case the test will wait for an acknowledge via the keyboard).
        '''
        rv = raw_input("\n" + prompt + "Press <Enter> to proceed or <a> to abort: ")
        self.failIf (len(rv) != 0 and rv == "abort"[0:len(rv)], errStr)

    ########################################################
    # Test Definitions
    def monitorTiming(self):
        '''
        This test does a CAN monitor at each of the monitor points in
        the ICD.  The timing from the end of the request to the beginning of
        the response should be measured using an oscilloscope to verify that
        the delay is less than 150 us.
        '''
        self.startTestLog("Monitor Timing test")
        self.log('''Connect CAN_L and CAN_H signals (AMB D-sub connector pins 2 resp 7) to
oscilloscope and set for single trigger.  Time base should be approximatly
100 microseconds.
           
Press <enter> to generate monitor request,
a[utomatic] to generate 1 monitor request per second and then process next,
n[ext] to process next monitor point,
e[xit] to exit.
                 ''')

        rv = ''
        # The keys() method returns an *unsorted* list!
        for name in sorted(self.monitor.keys()):
            rca = self.monitor[name][0]
            if len(rv) == 0 or rv != "automatic"[0:len(rv)] :
                rv = raw_input("Ready for %s (RCA 0x%.2x). Press <Enter> to begin ('e' to exit, ...) " %
                               (name, rca))
             
            while len(rv) == 0 or (rv != "next"[0:len(rv)] and rv != "exit"[0:len(rv)]):
                try:
                    (r,t) = self.manager.monitor(self.channel, self.node, rca)
                    self.failUnless(len(r)==self.monitor[rca],
                                    "Length of response incorrect for %s (RCA 0x%.2x)."
                                    % (name, rca))
                    if len(rv) != 0 and rv == "automatic"[0:len(rv)] :
                        self.log("CAN request sent for %s (RCA 0x%.2x)" % (name, rca))
                        sleep(1.0)
                        break
                    else:
                        rv = raw_input("CAN request sent to RCA: 0x%.2x. Press <Enter> to continue ('e' to exit, ...) " % rca)
                        
                except ControlExceptions.CAMBErrorEx:
                    self.fail("Exception generated monitoring %s (RCA 0x%.2x) " % (name, rca))
                    
            if rv == "exit"[0:len(rv)]:
                break
            
        self.endTestLog("Test 2: Monitor Timing Test")

    # --------------------------------------------------
    def ambReset(self):
        '''
        This test toggles the reset wires.  It should be verified that the
        reset pulse is detected and the AMBSI resets.  In addition
        the levels on these lines should be checked using an oscilliscope
        '''
        self.startTestLog("AMB Reset test")
        self.log('''Connect Reset wires (AMB D-sub connector pins 1 and 6) to oscilliscope and
set for single trigger.  Time base should be approximatly 100 us.

                 ''')

        rv = raw_input("Press <Enter> to generate reset request, type e[xit] to exit ")
        while  len(rv) == 0 or rv != "exit"[0:len(rv)]:
            t = self.manager.reset(self.channel)
            rv=raw_input("Reset Generated ")

        self.endTestLog("AMB Reset test")

    # --------------------------------------------------
    def commandLength(self):
        '''
        Verifies that the device only responds to commands of the
        correct length.
        Note that this requires the ACD to be initialized, as there is an
        attempt to move motors.
        '''
        self.startTestLog("Ill formed command test")
        self.log ("\nNote: this test requires the ACD to be initialized,"
                  " as there is an attempt to move motors.\n")
        for rca in self.command.keys():
            self.log("rca = 0x%.2x, monitor = 0x%.2x" % (rca, (self.command[rca][0])))
            (r,t) = self.manager.monitor(self.channel, self.node, self.command[rca][0])
            (x,y) = struct.unpack("!2f", r)

            
            bytes = [0x01]
            self.log ("...............")
            while len(bytes) <= 8:
                if len(bytes) != self.command[rca][1]:
                    self.log("sending %d bytes to RCA 0x%.2x" % (len(bytes), rca))
                    t = self.manager.command(self.channel, self.node, rca,
                                             struct.pack("!%dB"%len(bytes),*bytes))
                    elapsedTime = waitTillInPosition(self, 3.0)
                    (r,t) = self.manager.monitor(self.channel, self.node,
                                                 self.command[rca][0])
                    (x1, y1) = struct.unpack("!2f",r)
                    self.log ("(x,y)/(x1,y1) = (%.2f,%.2f)/(%.2f,%.2f, elapsedTime = %.3fs" % (x, y, x1, y1, elapsedTime))
                    errStr = "%s acted with length %d bytes.\n" % (self.command[rca][2], len(bytes))
                    errStr += "    original position (%.2f/%.2f), now at  (%.2f/%.2f)" % (x,y, x1, y1)
                    #!# Note: not clear what tolerances are
                    self.failIf(abs(x1 - x) > tolerance, errStr)
                    self.failIf(abs(y1 - y) > tolerance, errStr)
                bytes.append(0x01)
            
        self.endTestLog("Ill formed command test")

    # --------------------------------------------------
    def unknownMonitors(self):
        '''
        This test monitors all RCAs between 1 and 0x3FFFF (the full
        range allocated to each device).  It ensures that only
        monitors documented in the ICD respond, and that the response
        is valid (correct number of bytes).
        '''
        self.startTestLog("Monitor Survey test")
        self.log('''The test can be interrupted by typing ^C''')
        errorList = {}
        ##for RCA in range (0x1,0x3FFFF +1):
        for RCA in range (0x00001, 0x3FFFF + 1):
            # For time being, avoid accessing monitors corresponding to the
            # control points MAINTENANCE_FUNCTION1 and MAINTENANCE_FUNCTION2
            # EAL - 2008-05-30
            if RCA != self.rcaMAINTENANCE_FUNC1 and RCA != self.rcaMAINTENANCE_FUNC2 :
                try:
                    if (RCA % 0x1000) == 0:
                        self.log("Sending monitor request 0x%.5x" % RCA)

                    (r,t) = self.manager.monitor(self.channel, self.node, RCA)
                    if self.monitorRCAs.has_key(RCA) :
                        if len(r) != self.monitorRCAs[RCA]:
                            #self.log("expected %d, got %d bytes" % (self.monitorRCAs[RCA], len(r)))
                            errorList[RCA]=len(r)
                    else :
                        #self.log("expected no reply, got %d bytes" % len(r))
                        errorList[RCA]=len(r)
                        
                except KeyboardInterrupt, e:
                    break
                except Exception, e:
                    #print("---> exception " + str(e))
                    pass
                                
            # If introducing a sleep, the likelihood of seeing a Ctrl-C
            # becomes higher (at least in simulation).
            #!# Without this sleep, there are CAMB errors - rate too high?
            #sleep (0.001953125)
            sleep (0.002)
            
        if len(errorList) != 0:
            if len(errorList) > 50:
                self.log("%d offending responses, will list first 50:" % len(errorList))
            else :
                self.log("%d offending responses" % len(errorList))

            count = 1
            # The keys() method returns an *unsorted* list!
            for idx in sorted(errorList.keys()):
                if self.monitorRCAs.has_key(RCA):
                    self.log("RCA 0x%.5x returned %d bytes (%d expected)" % \
                             (idx, errorList[idx], self.monitorRCAs[RCA]))
                else:
                    self.log("RCA 0x%.5x returned %d bytes" % \
                             (idx, errorList[idx]))
                count = count + 1
                if count > 50 :
                    break

            self.fail("%d Monitor points not in ICD Responded" %
                      len(errorList))

        self.endTestLog("Monitor Survey test")

# Define the test suites
# automatedSuite: contains all tests in the class Automated, except the calibration tests
def automatedSuite():
    suite = unittest.TestSuite()
    suite.addTest(Automated("broadcast"))
    suite.addTest(Automated("genericMonitors"))
    suite.addTest(Automated("replyLength"))
    suite.addTest(Automated("status"))
    suite.addTest(Automated("nodeId"))
    suite.addTest(Automated("resetAndInit"))
    suite.addTest(Automated("setHotLoad"))
    suite.addTest(Automated("setAmbientLoad"))
    suite.addTest(Automated("setSolarFilter"))
    suite.addTest(Automated("park0Loads"))
    suite.addTest(Automated("park1Loads"))
    suite.addTest(Automated("stowLoads"))
    suite.addTest(Automated("setHeat"))
    suite.addTest(Automated("setLoadiXY"))
    suite.addTest(Automated("setLoadidXdY"))
    suite.addTest(Automated("setArmi"))
    suite.addTest(Automated("setQWP"))
    suite.addTest(Automated("monitorPoints"))
    suite.addTest(Automated("calibrationCycle"))
    suite.addTest(Automated("resetDevice"))
    
    return suite

# calibrationSuite: contains the calibration tests
def calibrationSuite():
    suite = unittest.TestSuite()
    suite.addTest(Automated("calQWP"))
    suite.addTest(Automated("calLoadiBandj"))
    suite.addTest(Automated("calPark"))
    
    return suite

# tatSuite: subset of automatedSuite, containing tests that do not
# involve motor movements (as this would timeout and fail)
def tatSuite():
    suite = unittest.TestSuite()
    suite.addTest(Automated("broadcast"))
    suite.addTest(Automated("genericMonitors"))
    suite.addTest(Automated("replyLength"))
    # Automated.status returns with ambdaemon a status which depends
    # on the previous use of ambdaemon (i.e. on previous tests)
    ##suite.addTest(Automated("status"))
    suite.addTest(Automated("nodeId"))
    suite.addTest(Automated("setHeat"))
    suite.addTest(Automated("monitorPoints"))
    # Automated.resetDevice returns with ambdaemon a status which depends
    # on the previous use of ambdaemon (i.e. on previous tests)
    ##suite.addTest(Automated("resetDevice"))
    
    return suite

# interactiveSuite: contains all tests of the Interactive class
def interactiveSuite():
    suite = unittest.TestSuite()
    suite.addTest(Interactive("monitorTiming"))
    suite.addTest(Interactive("ambReset"))
    suite.addTest(Interactive("commandLength"))
    suite.addTest(Interactive("unknownMonitors"))
    
    return suite

def all():
    return unittest.TestSuite((interactiveSuite, automatedSuite, calibrationSuite))

# The next is to run some tests defined in the Automated
# class as objects from the Interactive class. The latter
# class redefines the method getConfirmationOrWait() to
# prompt the user for acknowledgement, rather than waiting
# for a number of seconds.
def checkPositions():
    suite = unittest.TestSuite()
    suite.addTest(Interactive("resetAndInit"))
    suite.addTest(Interactive("setHotLoad"))
    suite.addTest(Interactive("setAmbientLoad"))
    suite.addTest(Interactive("setSolarFilter"))
    suite.addTest(Interactive("setLoadiXY"))
    suite.addTest(Interactive("setLoadidXdY"))
    suite.addTest(Interactive("setArmi"))

    return suite

# **************************************************
# MAIN Program
if __name__ == '__main__':

    runstring = sys.argv[0]
    for i in range(1,len(sys.argv)) :
        runstring += " " + sys.argv[i]

    print "Original runstring:\n    " + runstring
    
    # In case the output is re-directed to a file (e.g. by tat), these prints
    # may get intermixed with the output from pyunit, due to buffering. To
    # avoid that, do a flush.
    sys.stdout.flush()

    # First argument is for unittest (testcase/suite).
    # Try --help for unittest help on arguments.
    if len(sys.argv) == 1:
        # No arguments passed defaults to the "all" test suite.
        # See suites defined above: "automatedSuite", "interactiveSuite",
        # "all" and "checkPositions", or execute individual tests
        # (e.g. "Automated.testBroadcast")
        sys.argv.append("all")

    # Second argument is antenna name
    if len(sys.argv) == 2:
        sys.argv.append("DV01")

    # Third argument is CAN node number.
    #!# Default should be 0x28 (40 decimal)
    if len(sys.argv) == 3:
        sys.argv.append("%d" % 0x28)

    # Fourth argument is flags if QWP-stage is installed and connected (0 = no (default), 1 = yes)
    if len(sys.argv) == 4:
        sys.argv.append("0")
    
    # Fifth argument is "confirmation delay" for positioning tests (interactive)
    if len(sys.argv) == 5:
        sys.argv.append("2.0")

    runstring = sys.argv[0]
    for i in range(1,len(sys.argv)) :
        runstring += " " + sys.argv[i]

    print "Converted into:\n    " + runstring + "\n"
    sys.stdout.flush()

    confDelay = float(sys.argv[5])
    sys.argv.remove(sys.argv[5])

    if int(sys.argv[4]) == 0 :
        qwpInstalled = False
    elif int(sys.argv[4]) == 1 :
        qwpInstalled = True
    sys.argv.remove(sys.argv[4])

    node = int(sys.argv[3])
    sys.argv.remove(sys.argv[3])

    antenna = sys.argv[2]
    sys.argv.remove(sys.argv[2])

    unittest.main(defaultTest='all')
