#
# Remove ISO time-stamps by symbolic value
/[0-9][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]T[0-5][0-9]:[0-5][0-9]:[0-5][0-9][.0-9]*/d
s/[0-9][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]T[0-5][0-9]:[0-5][0-9]:[0-5][0-9][.0-9]*/<timestamp>/g
#
#
# Replace number of CAN transactions by generic number
s/Reply on GET_TRANS_NUM (0x30002): [0-9]*/Reply on GET_TRANS_NUM (0x30002): <#transactions>/g
#
# Replace timing on execution of tests
s/Ran 6 tests in [0-9.]*s/Ran 6 tests in <#seconds>/g
s/.*<timestamp>.*//g
s/\s+//g
