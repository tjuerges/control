#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$

#import sys
import unittest
#import struct
#import ControlExceptions
#from ambManager import *
#from time import sleep
#from Acspy.Clients.SimpleClient import PySimpleClient
#from Acspy.Common.QoS import setObjectTimeout
#import Acspy.Util
from CCL.ACD import ACD
from Control import HardwareDevice

class cclTest(unittest.TestCase):
    
    def setUp(self):
        self.acd = ACD("DA41",stickyFlag=True)
        self.failIf(self.acd == None, "Failed to activate component")
        self.acd.hwStop()
        self.failUnlessEqual(self.acd.getHwState(), HardwareDevice.Stop,
                             "Unable to get ACD to Stop State")

        self.acd.hwStart() 
        self.failUnlessEqual(self.acd.getHwState(), HardwareDevice.Start,
                             "Unable to get ACD to Start State")
        self.acd.hwConfigure()
        self.failUnlessEqual(self.acd.getHwState(), HardwareDevice.Configure,
                             "Unable to get ACD to Configure State")
        self.acd.hwInitialize()
        self.failUnlessEqual(self.acd.getHwState(), HardwareDevice.Initialize,
                             "Unable to get ACD to Initialize State")
        self.acd.hwOperational()
        self.failUnlessEqual(self.acd.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get ACD to Operational State")

    def tearDown(self):
        self.acd.hwStop() 
        self.failUnlessEqual(self.acd.getHwState(), HardwareDevice.Stop,
                             "Unable to get ACD to Stop State")

    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Hardware Lifecycle test.  Move the device to operational and
        Ensure that the device is in operational mode.
        '''
        self.failUnlessEqual(self.acd.getHwState(),
                             HardwareDevice.Operational,
                             "Unable to get ACD to Operational State")
        
    def test02(self):
        '''
        Check that we can properly get and set the loads
        '''
        [setX, setY] = [0.123, 0.432]
        self.acd.SET_LOAD0_XY([setX, setY])
        [getX, getY] = self.acd.GET_LOAD0_XY()[0]
        self.failIf(abs(getX - setX) > 1E-3 or abs(getY - setY) > 1E-3,
                    "Returned position of ambient load (%.3f, %.3f) does not match set position (%.3f, %.3f)."
                    % (getX, getY, setX, setY))
        [deltaX, deltaY] = [0.111, -0.111]
        self.acd.SET_LOAD0_dXdY([deltaX, deltaY])
        [getX, getY] = self.acd.GET_LOAD0_XY()[0]
        self.failIf(abs(getX - setX - deltaX) > 1E-3 or abs(getY - setY - deltaY) > 1E-3,
                    "Returned position of ambient load (%.3f, %.3f) does not match offset position (%.3f + %.3f, %.3f + %.3f)."
                    % (getX, getY, setX, deltaX, setY, deltaY))

        [setX, setY] = [0.234, 0.321]
        self.acd.SET_LOAD1_XY([setX, setY])
        [getX, getY] = self.acd.GET_LOAD1_XY()[0]
        self.failIf(abs(getX - setX) > 1E-3 or abs(getY - setY) > 1E-3,
                    "Returned position of hot load (%.3f, %.3f) does not match set position (%.3f, %.3f)."
                    % (getX, getY, setX, setY))
        [deltaX, deltaY] = [-0.111, 0.111]
        self.acd.SET_LOAD1_dXdY([deltaX, deltaY])
        [getX, getY] = self.acd.GET_LOAD1_XY()[0]
        self.failIf(abs(getX - setX - deltaX) > 1E-3 or abs(getY - setY - deltaY) > 1E-3,
                    "Returned position of hot load (%.3f, %.3f) does not match offset position (%.3f + %.3f, %.3f + %.3f)."
                    % (getX, getY, setX, deltaX, setY, deltaY))
        
        [setX, setY] = [0.111, 0.222]
        self.acd.SET_LOAD2_XY([setX, setY])
        [getX, getY] = self.acd.GET_LOAD2_XY()[0]
        self.failIf(abs(getX - setX) > 1E-3 or abs(getY - setY) > 1E-3,
                    "Returned position of solar filter (%.3f, %.3f) does not match set position (%.3f, %.3f)."
                    % (getX, getY, setX, setY))
        [deltaX, deltaY] = [0.111, -0.111]
        self.acd.SET_LOAD2_dXdY([deltaX, deltaY])
        [getX, getY] = self.acd.GET_LOAD2_XY()[0]
        self.failIf(abs(getX - setX - deltaX) > 1E-3 or abs(getY - setY - deltaY) > 1E-3,
                   "Returned position of solar filter load (%.3f, %.3f) does not match offset position (%.3f + %.3f, %.3f + %.3f)."
                    % (getX, getY, setX, deltaX, setY, deltaY))

# **************************************************
# MAIN Program
if __name__ == '__main__':
    unittest.main()
