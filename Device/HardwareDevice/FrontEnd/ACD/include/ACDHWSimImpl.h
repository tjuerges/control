/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ACDHWSimImpl.h
 */

#ifndef ACDHWSimImpl_H
#define ACDHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "ACDHWSimBase.h"

// STATUS - "cartridge" positions for PARK, STOW and QWP commands
#define ACD_STOW        0x00
#define ACD_BAND1       0x01
#define ACD_BAND2       0x02
#define ACD_BAND3       0x03
#define ACD_BAND4       0x04
#define ACD_BAND5       0x05
#define ACD_BAND6       0x06
#define ACD_BAND7       0x07
#define ACD_BAND8       0x08
#define ACD_BAND9       0x09
#define ACD_WVR         0x0B
#define ACD_PARK0       0x0C
#define ACD_PARK1       0x0D
#define ACD_NOT_ALIGNED 0x0E

// STATUS - address of the loads
#define ACD_AMBIENT_LOAD 0x00
#define ACD_HOT_LOAD     0x10
#define ACD_SOLAR_FILTER 0x20
#define ACD_QWP          0x30

// STATUS - "in position bit
#define ACD_ERROR_XY     0x40
#define ACD_IN_POSITION  0x80

// HL_STATUS
#define ACD_HL_CONTROLLER_IDLE           0x01
#define ACD_HL_CONTROLLER_ACTIVE         0x02
#define ACD_HL_CONTROLLER_RESET          0x04
#define ACD_HL_CONTROLLER_ERROR          0x08
#define ACD_HL_CONTROLLER_INPUT_OUTRANGE 0x10

namespace AMB
{
  /**
   * The ACDHWSimImpl class is the base class for the
   * ACD Unit hardware simulator.
   * <ul>
   * <li> Device:   ACD Unit
   * <li> Assembly: ACD
   * <li> Parent:   root
   * <li> Node:     0x200
   * <li> Channel:  1 
   * </ul>
   */
  class ACDHWSimImpl : public ACDHWSimBase
  {
  public :
	/** Constructor
	 ** @param node Node ID of this device
	 ** @param serialNumber S/N of this device
	 */
	ACDHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);

  protected :
      virtual void setControlReset(const std::vector<CAN::byte_t>& data);
      virtual void setControlInit(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetHotLoad(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetAmbientLoad(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetSolarFilter(const std::vector<CAN::byte_t>& data);
      virtual void setControlPark0Loads(const std::vector<CAN::byte_t>& data);
      virtual void setControlPark1Loads(const std::vector<CAN::byte_t>& data);
      virtual void setControlStowLoads(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetHeat(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetLoad0Xy(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetLoad1Xy(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetLoad2Xy(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetLoad0Dxdy(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetLoad1Dxdy(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetLoad2Dxdy(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetArm0(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetArm1(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetArm2(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetQuarterwavePlate(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetHlStatus(const std::vector<CAN::byte_t>& data);

  }; // class ACDHWSimImpl
};
#endif
