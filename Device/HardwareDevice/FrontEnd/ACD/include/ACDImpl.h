/* @(#) $Id$
 *
 * Copyright (C) 2001
 * Associated Universities, Inc. Washington DC, USA.
 *
 * Produced for the ALMA project
 *
 * This library is free software; you can redistribute it and/or modify it it 
 * under the terms of the GNU Library General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public 
 * License for more details.
 *
 * You should have received a copy of the GNU Library General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 675 Massachusetts Ave, Cambridge, MA, 02139, USA.
 *
 * Correspondence concerning ALMA should be addressed as follows:
 * Internet email: alma-sw-admin@nrao.edu
 */
#ifndef ACDIMPL_H
#define ACDIMPL_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <ACDBase.h>
#include <ACDS.h>
#include <AntennaStatusReportingC.h>
/* Allows managing alarm state flags not covered by BACI Alarms */
#include <errorStateHelper.h>

// Forward Declarations
#include <ACSAlarmSystemInterfaceFactory.h>

/* acsalarm::FaultState */
#include <FaultState.h>
#include <faultStateConstants.h>
#include <RepeatGuard.h>
#include <DataCaptureInterfacesC.h> // For CalDeviceData


using namespace baci;


enum ACDErrorCondition {
  HotLoadUnderDefaultValue = 0x01,
  HotLoadOutOfRange = 0x02
};

class ACDImpl : public virtual POA_Control::ACD,
                public ACDBase, public ErrorStateHelper<ACDErrorCondition>
{
  public:
    
    ACDImpl(const ACE_CString& name, maci::ContainerServices* pCS);
    
    ~ACDImpl();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize ();
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp ();
    /// \exception CORBA::SystemException
    /// \exception ControlDeviceExceptions::HwLifecycleEx
    virtual void hwOperational ();
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void hwStopAction ();
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void hwInitializeAction ();

    /// \exception CORBA::SystemException
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void lookup();

    virtual void RESET();

    /// This method is overloaded, because when it is called
    /// a state variable of heater will be enabled
    /// \exception ControlExceptions::CAMBErrorEx 
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_HEAT(CORBA::Float world);

    virtual CORBA::Long waitTillInPosition(const CORBA::Double timeout);

    /// Initialization and set hot load temperature.
    /// \exception CORBA::SystemException
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void setHotLoadTemperature (CORBA::Float temp);

    /// \exception CORBA::SystemException
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual bool areMotorsBlocked ();

    /// \exception CORBA::SystemException
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual bool isInResetState ();

    /// \exception CORBA::SystemException
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void stopHeat();

    /* See the IDL file for a description of this method */
    virtual void enableCalDeviceDataReporting(bool enable);

    /* See the IDL file for a description of this method */

    virtual bool calDeviceDataReportingEnabled();

    ReceiverBandMod::ReceiverBand getCurrentCartrige();

    CalibrationDeviceMod::CalibrationDevice getCurrentLoad();

    void setCalibrationDeviceBand(CalibrationDeviceMod::CalibrationDevice cd,
        ReceiverBandMod::ReceiverBand band);

    ACS::TimeIntervalSeq* timeToTune(const Control::ACD::CalibrationDeviceSeq& positions);

  protected: 
    /// \exception ControlExceptions::CAMBErrorExImpl
    virtual void getDeviceUniqueId(std::string& deviceID);
    virtual void msecSleep(CORBA::Long timeout);

    /* This method will report to it's parent if either the Ambient
       or hot load temperature has varied by more than <threshold> 
       degrees*/
    virtual void reportCalDeviceData();

    /* ------------------ Error State Methods ---------------------*/
    virtual void handleSetErrorFlag(ACDErrorCondition newFlag);
    virtual void handleResetErrorFlag(ACDErrorCondition newFlag);
    virtual std::string createErrorMessage();

    /* ---------------- Writer Thread Class  -----------------------*/
    class UpdateThread: public ACS::Thread 
      {
      public:
	UpdateThread (const ACE_CString& name, ACDImpl& ACD);
	virtual void runLoop();
      protected:
	ACDImpl& acdDevice_p;
      };
    void updateThreadAction ();

  public:
    friend class ACDImpl::UpdateThread;

    
 private:
    ReceiverBandMod::ReceiverBand getCurrentCartrigeInternal();

    CalibrationDeviceMod::CalibrationDevice getCurrentLoadBareInternal();

    CalibrationDeviceMod::CalibrationDevice getCurrentLoadInternal();

    Control::AntennaStatusReporting_ptr getAntennaStatusReportingReference();

    void setCalibrationDeviceBandInternal(CalibrationDeviceMod::CalibrationDevice cd,
        ReceiverBandMod::ReceiverBand band);

    void flagNotInCommandedBandPosition(bool flag);
    void flagNotInCommandedLoadPosition(bool flag);
    void flagNotInPosition(bool flag);

    bool isInCommandedBandPosition();
    bool isInCommandedLoadPosition();

    virtual void sendAlarm (bool isActive, int alarmType);
    virtual void clearAlarm ();
    virtual void checkHLTemperature ();

    // copy and assignment are not allowed
    ACDImpl(const ACDImpl&);
    void operator= (const ACDImpl&);

   //Byte 1 Status Mask
   // bits 0-3 = cartridge number
   // bits 4-5 = address of the loads
   // bit 6    = error on X/Y position
   // bit 7    = in-position bit
   static const unsigned char cartridgeMASK  = 0x0F;
   static const unsigned char loadMASK       = 0x30;
   static const unsigned char xyErrorMASK    = 0x40;
   static const unsigned char inPositionMASK = 0x80;

   //Byte 2 Status Mask
   // bit 0: set if SET_LOADi_XY attempted a displacement out-of-range last 
   //        time it was executed
   // bit 1: set if SET_LOADi_dXdY attempted a displacement out-of-range last 
   //        time it was executed
   // bit 2: set if SET_ARMi attempted a displacement out-of-range last 
   //        time it was executed
   // bit 3: set if CAN error on last communication
   // bit 4: set if arm motor not in position
   // bit 5: set if wheel motor not in position
   // bit 6: set if qwp motor not in position
   // bit 7: set if last displacement attempt was made while motor was not 
   //        in position (FW 2.7)
   // set if last command was given with wrong number of bytes (FW 2.9)
   static const unsigned char setLoadiXYOORMASK   = 0x01;
   static const unsigned char setLoadidXdYOORMASK = 0x02;
   static const unsigned char setArmiOORMASK      = 0x04;
   static const unsigned char canErrorMASK        = 0x08;
   static const unsigned char armErrorMASK        = 0x10;
   static const unsigned char wheelErrorMASK      = 0x20;
   static const unsigned char qwpErrorMASK        = 0x40;
   static const unsigned char wrongNrBytesMASK    = 0x80;

   // counter of thread cycles
   static const unsigned int CYCLE_TIME_DURATION;
   static const float DEFAULT_HL_TEMPERATURE;
   float hotLoadTemp_m;
   bool heatEnabled_m;
   bool startCheckTempAlarm_m;
   bool wasBandInPosition_m;
   bool wasLoadInPosition_m;
   bool wasInPosition_m;

   sem_t updateThreadSem_m;
   UpdateThread* updateThread_p;

   // thread default cycle time 0.5 sec, 5000000 [100ns]
   static const long long THREAD_CYCLE_TIME = 5000000;
   const ACS::Time updateThreadCycleTime_m;

   // counter of number thread cycles done
   long long threadCycleCounter_m;

   bool calDeviceReportingEnabled;
   Control::CalDeviceData calDeviceData_m;
   /* This is how much the temperature must change by before we put an 
      entry in the ASDM */
   static const float updateThreshold;
   RepeatGuard  calDeviceDataGuard_m;

   ReceiverBandMod::ReceiverBand lastCommandedBand_m;
   CalibrationDeviceMod::CalibrationDevice lastCommandedLoad_m;

};

#endif /* ! ACDIMPL_H */
