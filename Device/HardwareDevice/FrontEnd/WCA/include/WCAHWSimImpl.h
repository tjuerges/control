
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WCAHWSimImpl.h
 *
 * $Id$
 */
#ifndef WCAHWSimImpl_H
#define WCAHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "WCAHWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * WCAHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class WCAHWSimImpl : public WCAHWSimBase
  {
  public :
	WCAHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);

  protected:
	/// Specific Control set helper functions
        /// OVERLOADING the one on the base class

	virtual void setControlSetLoYtoCoarseTune(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPhotomixerOn(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPhotomixerOff(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPllClearUnlockDetectLatch(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPllLoopBandwidthDefault(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetLoPllLoopBandwidthAlt(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPllSbLockPolarityLower(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetLoPllSbLockPolarityUpper(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPllNullLoopIntegratorOperate(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetLoPllNullLoopIntegratorNull(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoAmcDrainBVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoAmcMultiplierDVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoAmcGateEVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoAmcDrainEVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPaPol0GateVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPaPol1GateVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPaPol0DrainVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetLoPaPol1DrainVoltage(const std::vector<CAN::byte_t>& data);

  }; // class WCAHWSimImpl

} // namespace AMB

#endif // WCAHWSimImpl_H
