#ifndef WCAImpl_H
#define WCAImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WCAImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <WCABase.h>

// Exceptions
#include <WCAExceptions.h>

// Band and SideBand Enumeration
#include <NetSideband.h>
#include <ReceiverBand.h>

//CORBA Servants
#include "WCAS.h"

static const int NOLOCK = 1;
static const int HIGHCURRENT = 2;
static const int LOWCURRENT = 3;
static const int PHOTOMIXEROFF = 4;
static const int HIGHTEMP= 5;

class WCAImpl: public WCABase,
    public virtual POA_Control::WCA
{
    public:
    //friend class WCAImpl::WCAAlarmThread;
    /**
     * Constructor
     */
    WCAImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~WCAImpl();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize ();
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp ();
    
    /* IDL SPECIFIED INTERFACE */
    void setSelected(bool select);
    /**
     * void SetFrequency(in float nominalFrequency, in boolean lock)
     * This method will set the WCA frequency to the specified value.
     * The WCA will verify if the frequency is valid, and throw an
     * exception if not.
     * @param nominalFrequency frequency in Hz.
     * @param lock boolean variable that indicated if a lock should be
     * attempted at this frequency.
     * @except ControlExceptions::CAMBErrorEx
     * @except ControlExceptions::INACTErrorEx
     * @except ControlExceptions::IllegalParameterErrorEx
     * @except WCAExceptions::LockFailedEx
     * @except WCAExceptions::LockLostEx
     * @except WCAExceptions::AdjustPllFailedEx
     */
    void SetFrequency(float nominalFrequency, bool lock);

    /**
     * void AdjustPll(in float voltage)
     * Attempt to adjust the FM coil voltage to the specified voltage.
     * @except ControlExceptions::CAMBErrorEx
     * @except ControlExceptions::INACTErrorEx
     * @except WCAExceptions::LockLostEx
     * @except WCAExceptions::AdjustPllFailedEx
    */
    void AdjustPll(float voltage);

    /**
     * void ReLock()
     * This will attempt to relock the WCA a the same frequency it is
     * configured.
     * @except ControlExceptions::CAMBErrorEx
     * @except ControlExceptions::INACTErrorEx
     * @except WCAExceptions::LockFailedEx
     * @except WCAExceptions::LockLostEx
     * @except WCAExceptions::AdjustPllFailedEx
     */
    void ReLock();

    /**
     * float GetFrequency()
     * Return the current configured frequency
     * @return current freq in Hz
     * @except ControlExceptions::INACTErrorEx
     */
    float GetFrequency();

    /**
     * void SetLOPowerAmps(in boolean enable)
     * Enable/disable the LO power amplifiers. When enabling the PA the WCA
     * will search for the proper setting in the TMCDB, if no setting is
     * available for the current FREQ it will linearly interpolate the to
     * nearest setting.
     * @except ControlExceptions::CAMBErrorEx
     * @except ControlExceptions::INACTErrorEx
     */
    void SetLOPowerAmps(bool enable);

    /**
     * bool IsLocked()
     * Return the locked status of the WCA PLL
     * @return boolean - lock status
     * @except ControlExceptions::CAMBErrorEx
     * @except ControlExceptions::INACTErrorEx
     */
    bool IsLocked();
   
    /**
     * bool isPACacheHit()
     * Return true if the power amplifier optimum settings are in the cache 
     * table.
     * @return boolean - cache hit
     */
    bool isPACacheHit();

    /**
     * void setPACacheHit(double VD1, double VD2)
     * Set the cache table with the following settings.
     * @param double - Darin voltage 1
     * @param double - Darin voltage 2
     */
    void setPACache(double VD1, double VD2);

    /**
     * void setPAFromCache()
     * Set drain voltages using the cache table.
     */
    void setPAFromCache();


    /**
     * double getTargetVD1()
     * Get the target drain voltage 1 from the Lookup tables.
     */
    double getTargetVD1();

    /**
     * double getTargetVD2()
     * Get the target drain voltage 2 from the Lookup tables.
     */

    double getTargetVD2();

    /**
     * void setPllDebug(in boolean debug);
     * That sets the PLL debug flag in the PLL.  if set to true then by
     * default when locking the LO PLL no Voltage correction adjustment
     * will be tried.
     */
    void setPllDebug(bool debug);

    /**
     * boolean getPllDebug();
     * get the PLL debug flag
     */
    bool getPllDebug();

    /**
     * double getTargetDYTOFrequency()
     * return the targeted DYTO frequency.
     * This is a nominal value.
     * @return double - yigfreq
     */
    double getTargetDYTOFrequency();

    /**
     * double getDerivedDYTOFrequency()
     * return the DYTO frequency calculated from
     * the COARSE_TUNE values.
     * @return double 
     * @except ControlExceptions::CAMBErrorEx
     * @except ControlExceptions::INACTErrorEx
     */
    double getDerivedDYTOFrequency();

    /**
     * double getHiDYTOLimit()
     * return the HiYiglimit.
     * @return double
     */
    double getHiDYTOLimit();

    /**
     * double getLoDYTOLimit()
     * return the LoYiglimit.
     * @return double
     */
    double getLoDYTOLimit();

    /* END OF IDL SPECIFIED INTERFACE */

    /**
    * @override WCABase::hwOperationalAction 
    * @except ControlDeviceExceptions::HwLifecycleEx
    */
    virtual void hwOperationalAction();
    
    /**
    * @override WCABase::hwInitializeAction 
    * @except ControlDeviceExceptions::HwLifecycleEx
    */
    virtual void hwInitializeAction();

    /**
    * @override WCABase::hwStopAction
    * @except ControlDeviceExceptions::HwLifecycleEx
    */
    virtual void hwStopAction();
    
    protected:
    /* ---------------- WCA Alarm Thread Class  -----------------------*/
    class WCAAlarmThread: public ACS::Thread {
      public:
        WCAAlarmThread (const ACE_CString& name, WCAImpl& wca);
        virtual void runLoop();
      protected:
        WCAImpl& wca_p;
    };
    void updateThreadAction();

//  public:
    friend class WCAImpl::WCAAlarmThread;


    WCAAlarmThread *alarmThread_m;  
    
    void  initializeAlarms();
   
    /**
     * std::vector<double> getConfigPowerAmp();
     * Return a vector with the voltages to be set in the
     * power amplifiers, interpolated from the XML configuration file
     * @return std::vector<double>:  power amplifiers voltages to be set
     * @except ControlExceptions::XmlParserErrorExImpl
     */
    std::vector<double> getConfigPowerAmp();

    /**
     * double getConfigTargetPhotoMixerCurrent();
     * Return the target photomixer current to be optimized
     * during the first phase of the locking process.
     * @return double:  target photomixer current
     * @except ControlExceptions::XmlParserErrorEx
     */
    double getConfigTargetPhotoMixerCurrent();

    /**
     * bool isLocked()
     * Return the locked status of the WCA PLL according to the criteria
     * specified in FEND-40.00.00.00-089-A-MAN section 10.6.
     * @return boolean - lock status
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     */
    bool isLocked();

    /**
     * void lockPll()
     * Lock the WCA PLL according to the procedure specified in 
     * FEND-40.00.00.00-089-A-MAN section 10.2
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except WCAExceptions::InsufficientPowerExImpl
     * @except WCAExceptions::LockFailedExImpl
     */
    void lockPll();

    /**
     * void adjustPll()
     * Adjust the FM coil voltage according to the procedure 
     * specified in FEND-40.00.00.00-089-A-MAN section 10.5
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except WCAExceptions::LockLostExImpl
     * @except WCAExceptions::AdjustPllFailedExImpl
     */
    void adjustPll(float targetCorrcoltage);
 
    /**
     * void setLOPowerAmps(bool enable)
     * Set the WCA power amplifiers according to the values
     * from the lookup table.
     * @param bool - True to enable, False to set all 0.0
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     */
    void setLOPowerAmps(bool enable);

    void flagIsLocked(bool lock);

    void flagDataAsNeeded();

    int pllLoopBwMultiplier_m;
    int coldMultiplier_m;
    float yigFreqMin_m;
    float yigFreqMax_m;
    float lockAcquireTime_m;
    float nominalFrequency_m;
    float tagretDYTOFrequency_m;
    unsigned short coarseYIG_m;
    ReceiverBand band_m;
    ConfigDataAccessor *configData;
    std::map<int, std::vector<double> > paCache_m;
    bool pllDebug_m;
    bool selected_m;
    bool wasLockedState_m;

    Control::AlarmSender *alarmSender_m;

   // thread default cycle time 1 sec, 10000000000 ns/100 (unit=100 ns)
   static const long long THREAD_CYCLE_TIME = 10000000;
   const ACS::Time alarmThreadCycleTime_m;

    int m_mutexIndex;
    pthread_mutex_t    m_mutex[10];//HACK

    /**HACK HACK HACK 
8.2.  Photomixer monitoring 
A timing problem has recently been discovered with the LO hardware for
monitoring (among other things) the photomixer current. This will be
addressed eventually by upgrading the WCAs to a new electronic design. Since
the current needs to be known with fairly high precision, use this procedure
whenever monitoring the photomixer current: 
1) Send the photomixer monitor request. 
2) Wait 2 milliseconds without monitoring any other point in the
   WCA. Other items in the cartridge or front end may safely be monitored
   during this interval. 
3) Send the monitor request again and use the second value.

What i have to do (I hate the guts of John FrontEnd) is override every single
analog monitor point in the WCA so it blocks if getLoPhotomixerCurrent is
being executed

all of the following @except the same:
@except ControlExceptions::CAMBErrorExImpl
@except ControlExceptions::INACTErrorExImpl
**/
    virtual float getLoPhotomixerVoltage(ACS::Time& timestamp);
    virtual float getLoPhotomixerCurrent(ACS::Time& timestamp);
    virtual float getLoPllLockDetectVoltage(ACS::Time& timestamp);
    virtual float getLoPllCorrectionVoltage(ACS::Time& timestamp);
    virtual float getLoPllAssemblyTemp(ACS::Time& timestamp);
    virtual float getLoYigHeaterCurrent(ACS::Time& timestamp);
    virtual float getLoPllRefTotalPower(ACS::Time& timestamp);
    virtual float getLoPllIfTotalPower(ACS::Time& timestamp);
    virtual float getLoAmcGateAVoltage(ACS::Time& timestamp);
    virtual float getLoAmcDrainAVoltage(ACS::Time& timestamp);
    virtual float getLoAmcDrainACurrent(ACS::Time& timestamp);
    virtual float getLoAmcGateBVoltage(ACS::Time& timestamp);
    virtual float getLoAmcDrainBVoltage(ACS::Time& timestamp);
    virtual float getLoAmcDrainBCurrent(ACS::Time& timestamp);
    virtual float getLoAmcGateEVoltage(ACS::Time& timestamp);
    virtual float getLoAmcDrainEVoltage(ACS::Time& timestamp);
    virtual float getLoAmcDrainECurrent(ACS::Time& timestamp);
    virtual float getLoAmcMultiplierDCurrent(ACS::Time& timestamp);
    virtual float getLoPaPol0GateVoltage(ACS::Time& timestamp);
    virtual float getLoPaPol1GateVoltage(ACS::Time& timestamp);
    virtual float getLoPaPol0DrainVoltage(ACS::Time& timestamp);
    virtual float getLoPaPol1DrainVoltage(ACS::Time& timestamp);
    virtual float getLoPaPol0DrainCurrent(ACS::Time& timestamp);
    virtual float getLoPaPol1DrainCurrent(ACS::Time& timestamp);
    virtual float getLoAmcSupplyVoltage5v(ACS::Time& timestamp);
    virtual float getLoPaSupplyVoltage3v(ACS::Time& timestamp);
    virtual float getLoPaSupplyVoltage5v(ACS::Time& timestamp);
    virtual unsigned char getLoPllUnlockDetectLatch(ACS::Time& timestamp);
};
#endif // WCAImpl_H
