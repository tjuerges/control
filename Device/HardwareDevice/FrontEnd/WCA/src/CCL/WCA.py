#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a WCA device.
"""

import CCL.WCABase
from CCL import StatusHelper

class WCA(CCL.WCABase.WCABase):
    '''
    The WCA class inherits from the code generated WCABase
    class and adds specific methods. This class is thought
    as a generic implementation for WCAs, to be used within
    specific WCAx implementations.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        CCL.WCABase.WCABase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.WCABase.WCABase.__del__(self)

    def SetFrequency(self, nominalFrequency, lock = True):
        '''
        Set the frecuency of the WCA and lock by default. Its posible to set
        the frequency without locking by seting the lock parameter to False.
        param nominalFrequency: WCA frecuency in hz
        param lock: (by default True) indicates if the WCA should try to lock.
        raises ControlExceptions::CAMBErrorEx,
               ControlExceptions::INACTErrorEx,
               ControlExceptions::IllegalParameterErrorEx,
               WCAExceptions::InsufficientPowerEx,
               WCAExceptions::LockFailedEx,
               WCAExceptions::LockLostEx,
               WCAExceptions::AdjustPllFailedEx;

        ex. With locking.
           wca3 = CCL.WCA3.WCA3("DA41")
           wca3.STATUS()
           wca3.SetFrequency(100E9)
           wca3.STATUS()

        ex. without locking.
           wca3 = CCL.WCA6.WCA6("DA41")
           wca3.STATUS()
           wca3.SetFrequency(100E9, False)
           wca3.STATUS()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].SetFrequency(nominalFrequency, lock)
        
    def AdjustPll(self, targetVoltage):
        '''
        Attempt to adjust the FM coil voltage to the specified voltage.
        raised ControlExceptions::CAMBErrorEx
               ControlExceptions::INACTErrorEx
               WCAExceptions::LockLostEx
               WCAExceptions::AdjustPllFailedEx
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].AdjustPll(targetVoltage)

    def ReLock(self):
        '''
        Reaquire the lock of the WCA.
        raises ControlExceptions::CAMBErrorEx,
               ControlExceptions::INACTErrorEx,
               WCAExceptions::InsufficientPowerEx,
               WCAExceptions::LockFailedEx,
               WCAExceptions::LockLostEx,
               WCAExceptions::AdjustPllFailedEx;
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].ReLock()
            
    def SetLOPowerAmps(self, enable = True):
        '''
        Set the LO power amplifier settings.
        Shut down the LO amplifiers if set to false.
        raises ControlExceptions::CAMBErrorEx,
               ControlExceptions::INACTErrorEx,
        
        For setting the LO power amplifier correctly first a frequency has to
        be set. Then the SetLOPowerAmps should be called in order to have the 
        proper LO PA setting.
        ex.
           wca3 = CCL.WCA3.WCA3("DA41")
           wca3.STATUS()
           wca3.SetFrequency(100E9)
           wca3.SetLOPowerAmps()
           wca3.STATUS()
           wca3.SetLOPowerAmps(False)
           wca3.STATUS()

        ex.
           wca3 = CCL.WCA3.WCA3("DA41")
           wca3.STATUS()
           wca3.SetFrequency(100E9, False)
           wca3.SetLOPowerAmps()
           wca3.STATUS()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].SetLOPowerAmps(enable)

    def GetFrequency(self):
        '''
        Returns the currently set frequency.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GetFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def IsLocked(self):
        '''
        Returns if the WCA is currently locked
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].IsLocked()
        if len(self._devices) == 1:
            return result.values()[0]
        return result        

    def isPACacheHit(self):
        '''
        Check if there is a Power amplifier setting in cache.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].isPACacheHit()
        if len(self._devices) == 1:
            return result.values()[0]
        return result        

    def setPACache(self, VD1, VD2):
        '''
        Set the Powe amplifier cache for the actual frecuency.
        This method takes as argument the drain voltages and
        will store them in the cache table. Subsecuent calls to
        isPACacheHit should return true for the actual frequency.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].setPACache(VD1, VD2)


    def setPAFromCache(self):
        '''
        This metod will set the PA with the values in the cache table
        for the actual frequency. If no values are found an error is
        logged and the PA are not changed.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].setPAFromCache()

    def setPllDebug(self, debug):
        '''
        This method sets the debug flag for the adjust PLL
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].setPllDebug(debug)

    def getPllDebug(self):
        '''
        This method gets the debug flag for the adjust PLL
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getPllDebug()
        if len(self._devices) == 1:
            return result.values()[0]
        return result        
        
    def getTargetDYTOFrequency(self):
        '''
        return the tageted DYTO frequency.
        This is a nominal value.
        @return double - yigfreq
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getTargetDYTOFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result        

    def getDerivedDYTOFrequency(self):
        '''
        return the DYTO frequncy calculated from
        the COARSE_TUNE values.
        @return double 
        @except ControlExceptions::CAMBErrorEx
        @except ControlExceptions::INACTErrorEx
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getDerivedDYTOFrequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result        

    def getHiDYTOLimit(self):
        '''
        return the HiYiglimit.
        @return double
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getHiDYTOLimit()
        if len(self._devices) == 1:
            return result.values()[0]
        return result        
    
    def getLoDYTOLimit(self):
        '''
        return the LoYiglimit.
        @return double
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getLoDYTOLimit()
        if len(self._devices) == 1:
            return result.values()[0]
        return result        

    def __STATUSHelper(self, key):
        '''
        This method provides the status part that is common to all WCAs.
        A list of elements is returned that should be used within higher
        level STATUS() methods. 
        '''
        # List of status elements
        elements = []

        ############################### General ###################################################
        elements.append( StatusHelper.Separator("General") )

        # GetFrequency()
        try:
            value = self._devices[key].GetFrequency()
            value = "%.6f" % (value / 1.0E9)
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Frequency",
                                           [StatusHelper.ValueUnit(value, "GHz")]) )

        # IsLocked()
        try:
            value = self._devices[key].IsLocked()
            if value == True:
                value_t = "Yes"
            else:
                value_t = "NO"
        except:
            value_t = "N/A"
        elements.append( StatusHelper.Line("Locked",
                                           [StatusHelper.ValueUnit(value_t)]) )

        ############################### LO PLL ###################################################
        elements.append( StatusHelper.Separator("PLL") )
        
        # GET_LO_PLL_LOCK_DETECT_VOLTAGE
        try:
            value = self._devices[key].GET_LO_PLL_LOCK_DETECT_VOLTAGE()[0]
            value = "%.4f" % value
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Lock voltage",
                                           [StatusHelper.ValueUnit(value, "V")]) )

        # GET_LO_PLL_UNLOCK_DETECT_LATCH
        try:
            value = self._devices[key].GET_LO_PLL_UNLOCK_DETECT_LATCH()[0]
            if value == 0:
                value_t = "PLL lock ok"
            elif value == 1:
                value_t = "PLL UNLOCK"
            else:
                value_t = "UNKNOWN"
        except:
            value_t = "N/A"
        elements.append( StatusHelper.Line("Unlock latch",
                                           [StatusHelper.ValueUnit(value_t)]) )

        # GET_LO_PLL_SB_LOCK_POLARITY_SELECT
        try:
            value = self._devices[key].GET_LO_PLL_SB_LOCK_POLARITY_SELECT()[0]
            if value == 0:
                value_t = "Lock below reference (LSB)"
            elif value == 1:
                value_t = "Lock above reference (USB)"
            else:
                value_t = "UNKNOWN"
        except:
            value_t = "N/A"
        elements.append( StatusHelper.Line("Sideband polarity",
                                           [StatusHelper.ValueUnit(value_t)]) )
        
        # GET_LO_PLL_CORRECTION_VOLTAGE
        try:
            value = self._devices[key].GET_LO_PLL_CORRECTION_VOLTAGE()[0]
            value = "%.2f" % value
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Correction voltage",
                                           [StatusHelper.ValueUnit(value, "V")]) )

        # GET_LO_PLL_REF_TOTAL_POWER
        try:
            value = self._devices[key].GET_LO_PLL_REF_TOTAL_POWER()[0]
            value = "%.4f" % value
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Ref. total power",
                                           [StatusHelper.ValueUnit(value, "V")]) )

        # GET_LO_PLL_IF_TOTAL_POWER
        try:
            value = self._devices[key].GET_LO_PLL_IF_TOTAL_POWER()[0]
            value = "%.4f" % value
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("IF total power",
                                           [StatusHelper.ValueUnit(value, "V")]) )

        # GET_LO_PLL_NULL_LOOP_INTEGRATOR
        try:
            value = self._devices[key].GET_LO_PLL_NULL_LOOP_INTEGRATOR()[0]
            if value == 0:
                value_t = "Zeroing disabled, normal PLL operation"
            elif value == 1:
                value_t = "ZEROING ENABLED, INTEGRATOR DUMPED"
            else:
                value_t = "UNKNOWN"
        except:
            value_t = "N/A"
        elements.append( StatusHelper.Line("Null-loop integrator",
                                           [StatusHelper.ValueUnit(value_t)]) ) 

        # GET_LO_PLL_ASSEMBLY_TEMP
        try:
            value = self._devices[key].GET_LO_PLL_ASSEMBLY_TEMP()[0]
            valueCelcius = "%.2f" % (value - 273.15)
            value = "%.2f" % value
        except:
            value = "N/A"
            valueCelcius = "N/A"
        elements.append( StatusHelper.Line("Assembly temperature",
                                           [StatusHelper.ValueUnit(value, "K"),
                                           StatusHelper.ValueUnit(valueCelcius, "C")]) )

        # GET_LO_YIG_HEATER_CURRENT
        try:
            value = self._devices[key].GET_LO_YIG_HEATER_CURRENT()[0]
            value = "%.2f" % (value*1000)
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Supply voltage",
                                           [StatusHelper.ValueUnit(value, "mA")]) )

        ############################### AMC ###################################################
        elements.append( StatusHelper.Separator("Active multiplier chain") )
        
        # AMC A
        try:
            Vd = self._devices[key].GET_LO_AMC_DRAIN_A_VOLTAGE()[0]
            Vd = "%.4f" % Vd
            Id = self._devices[key].GET_LO_AMC_DRAIN_A_CURRENT()[0]
            Id = "%.2f" % (Id * 1000.0)
            Vg = self._devices[key].GET_LO_AMC_GATE_A_VOLTAGE()[0]
            Vg = "%.4f" % Vg         
        except:
            Vd = "N/A"
            Id = "N/A"
            Vg = "N/A"
        elements.append( StatusHelper.Line("AMC A",
                                           [StatusHelper.ValueUnit(Vd, "V", label="Vd"),
                                            StatusHelper.ValueUnit(Id, "mA", label="Id"),
                                            StatusHelper.ValueUnit(Vg, "V", label="Vg")]) )

        # AMC B
        try:
            Vd = self._devices[key].GET_LO_AMC_DRAIN_B_VOLTAGE()[0]
            Vd = "%.4f" % Vd
            Id = self._devices[key].GET_LO_AMC_DRAIN_B_CURRENT()[0]
            Id = "%.2f" % (Id * 1000.0)
            Vg = self._devices[key].GET_LO_AMC_GATE_B_VOLTAGE()[0]
            Vg = "%.4f" % Vg         
        except:
            Vd = "N/A"
            Id = "N/A"
            Vg = "N/A"
        elements.append( StatusHelper.Line("AMC B",
                                           [StatusHelper.ValueUnit(Vd, "V", label="Vd"),
                                            StatusHelper.ValueUnit(Id, "mA", label="Id"),
                                            StatusHelper.ValueUnit(Vg, "V", label="Vg")]) )

        # AMC E
        try:
            Vd = self._devices[key].GET_LO_AMC_DRAIN_E_VOLTAGE()[0]
            Vd = "%.4f" % Vd
            Id = self._devices[key].GET_LO_AMC_DRAIN_E_CURRENT()[0]
            Id = "%.2f" % (Id * 1000.0)
            Vg = self._devices[key].GET_LO_AMC_GATE_E_VOLTAGE()[0]
            Vg = "%.4f" % Vg         
        except:
            Vd = "N/A"
            Id = "N/A"
            Vg = "N/A"
        elements.append( StatusHelper.Line("AMC E",
                                           [StatusHelper.ValueUnit(Vd, "V", label="Vd"),
                                            StatusHelper.ValueUnit(Id, "mA", label="Id"),
                                            StatusHelper.ValueUnit(Vg, "V", label="Vg")]) )

        # GET_LO_AMC_MULTIPLIER_D_VOLTAGE
        try:
            value = self._devices[key].GET_LO_AMC_MULTIPLIER_D_VOLTAGE()[0]
            value = "%.2f" % value
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Multiplier D Voltage",
                                           [StatusHelper.ValueUnit(value, "V")]) )

        # GET_LO_AMC_MULTIPLIER_D_CURRENT
        try:
            value = self._devices[key].GET_LO_AMC_MULTIPLIER_D_CURRENT()[0]
            value = "%.2f" % (value*1000)
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Multiplier D Current",
                                           [StatusHelper.ValueUnit(value, "mA")]) )

        # GET_LO_AMC_SUPPLY_VOLTAGE_5V
        try:
            value = self._devices[key].GET_LO_AMC_SUPPLY_VOLTAGE_5V()[0]
            value = "%.2f" % value
        except:
            value = "N/A"
        elements.append( StatusHelper.Line("Supply voltage",
                                           [StatusHelper.ValueUnit(value, "V", label="5V")]) )

        ############################### PA ###################################################
        elements.append( StatusHelper.Separator("Power amplifier") )
        
        # Pol 0
        try:
            Vd = self._devices[key].GET_LO_PA_POL0_DRAIN_VOLTAGE()[0]
            Vd = "%.4f" % Vd
            Id = self._devices[key].GET_LO_PA_POL0_DRAIN_CURRENT()[0]
            Id = "%.2f" % (Id * 1000.0)
            Vg = self._devices[key].GET_LO_PA_POL0_GATE_VOLTAGE()[0]
            Vg = "%.4f" % Vg         
        except:
            Vd = "N/A"
            Id = "N/A"
            Vg = "N/A"
        elements.append( StatusHelper.Line("A: Pol0",
                                           [StatusHelper.ValueUnit(Vd, "V", label="Vd"),
                                            StatusHelper.ValueUnit(Id, "mA", label="Id"),
                                            StatusHelper.ValueUnit(Vg, "V", label="Vg")]) )
        
        # Pol 1
        try:
            Vd = self._devices[key].GET_LO_PA_POL1_DRAIN_VOLTAGE()[0]
            Vd = "%.4f" % Vd
            Id = self._devices[key].GET_LO_PA_POL1_DRAIN_CURRENT()[0]
            Id = "%.2f" % (Id * 1000.0)
            Vg = self._devices[key].GET_LO_PA_POL1_GATE_VOLTAGE()[0]
            Vg = "%.4f" % Vg         
        except:
            Vd = "N/A"
            Id = "N/A"
            Vg = "N/A"
        elements.append( StatusHelper.Line("B: Pol1",
                                           [StatusHelper.ValueUnit(Vd, "V", label="Vd"),
                                            StatusHelper.ValueUnit(Id, "mA", label="Id"),
                                            StatusHelper.ValueUnit(Vg, "V", label="Vg")]) )

        # GET_LO_PA_SUPPLY_VOLTAGE_3V and 5V
        try:
            value_3V = self._devices[key].GET_LO_PA_SUPPLY_VOLTAGE_3V()[0]
            value_3V = "%.2f" % value_3V
            value_5V = self._devices[key].GET_LO_PA_SUPPLY_VOLTAGE_5V()[0]
            value_5V = "%.2f" % value_5V
        except:
            value_3V = "N/A"
            value_5V = "N/A"
        elements.append( StatusHelper.Line("Supply voltage",
                                           [StatusHelper.ValueUnit(value_3V, "V", label="3V"),
                                            StatusHelper.ValueUnit(value_5V, "V", label="5V")]) )

        ############################### Photomixer ################################################
        elements.append( StatusHelper.Separator("Photomixer") )

         # GET_LO_PHOTOMIXER_ENABLE
        try:
            value = self._devices[key].GET_LO_PHOTOMIXER_ENABLE()[0]
            if value == 0:
                value_t = "OFF"
            elif value == 1:
                value_t = "On"
            else:
                value_t = "UNKNOWN"
            value_V = self._devices[key].GET_LO_PHOTOMIXER_VOLTAGE()[0]
            value_V = "%.2f" % value_V
            value_I = self._devices[key].GET_LO_PHOTOMIXER_CURRENT()[0]
            value_I = "%.2f" % (value_I * 1000.0)           
        except:
            value_t = "N/A"
            value_V = "N/A"
            value_I = "N/A"
        elements.append( StatusHelper.Line("Photomixer",
                                           [StatusHelper.ValueUnit(value_t, label="State"),
                                            StatusHelper.ValueUnit(value_V, "V", label="Volt."),
                                            StatusHelper.ValueUnit(value_I, "mA", label="Curr.")]) )
        
        return elements

    def PADrainSTATUS(self):
        # Pol 1
        try:
            Vd0 = self.GET_LO_PA_POL0_DRAIN_VOLTAGE()[0]
            Vd0 = "%.4f" % Vd0
            Vd1 = self.GET_LO_PA_POL1_DRAIN_VOLTAGE()[0]
            Vd1 = "%.4f" % Vd1
        except:
            Vd0 = "N/A"
            Vd1 = "N/A"
        return StatusHelper.Line("LO PA Drain Voltage",
                                           [StatusHelper.ValueUnit(Vd0, "V", label="Pol 0"),
                                            StatusHelper.ValueUnit(Vd1, "V", label="Pol 1")])
