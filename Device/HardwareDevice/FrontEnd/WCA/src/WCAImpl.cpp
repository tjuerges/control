/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WCAImpl.cpp
 *
 * $Id$
 */

#include <WCAImpl.h>
#include <math.h>
#include <loggingMACROS.h>
#include <HardwareControllerC.h>

/* Constant variable declarations */

/**
 *-----------------------
 * WCA Constructor
 *-----------------------
 */
//static const int NOLOCK = 1;
//static const int HIGHCURRENT = 2;
//static const int LOWCURRENT =3;
//static const int PHOTOMIXEROFF 4;

using Control::AlarmSender;
using Control::AlarmInformation;


WCAImpl::WCAImpl(const ACE_CString& name,
        maci::ContainerServices* cs):
    WCABase(name,cs),alarmThreadCycleTime_m( THREAD_CYCLE_TIME ) //1 sec
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    coldMultiplier_m = -1;
    pllLoopBwMultiplier_m = -1;
    yigFreqMin_m = -1;
    yigFreqMax_m = -1;
    lockAcquireTime_m = 0.010;
    nominalFrequency_m = 0.0;
    tagretDYTOFrequency_m = 0.0;
    coarseYIG_m = 0;
    band_m = ReceiverBandMod::UNSPECIFIED;

    // m_mutexIndex initialized in inherited classes
    //From FEMC
    hasEsn_m = true;
    assembly_m = "WCA";
    pllDebug_m = false;
    selected_m = false;
    wasLockedState_m = true;
    alarmSender_m = NULL;
    alarmThread_m = NULL;

    //pthread_mutex_init(&m_mutex, 0);//HACK

}

/**
 *-----------------------
 * WCA Destructor
 *-----------------------
 */
WCAImpl::~WCAImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    //    pthread_mutex_destroy(&m_mutex);//HACK
}

void WCAImpl::initialize()
{
  ACS_TRACE(__PRETTY_FUNCTION__);
  alarmSender_m = new AlarmSender();
  try {
    // Initialize Base class
    WCABase::initialize();
  } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__, __PRETTY_FUNCTION__);
  }

  CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
  std::string threadName(compName.in());
  threadName += "MonitoringThread";
  //Get component thread manager to create thread
  alarmThread_m = getContainerServices()->
    getThreadManager()->
    create<WCAAlarmThread, WCAImpl>
    (threadName.c_str(), *this);

  // Thread sleep time 1 sec
  alarmThread_m->setSleepTime(alarmThreadCycleTime_m);
}

void WCAImpl::cleanUp()
{
  ACS_TRACE(__PRETTY_FUNCTION__);

  /* Shutdown thread, thread was suspended by hwStopAction baseclass call */
  /* Terminates thread                                                    */
  alarmThread_m->terminate();
  alarmSender_m->forceTerminateAllAlarms();
  delete alarmSender_m;
  
  try {
    WCABase::cleanUp();
  } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
    throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__, __PRETTY_FUNCTION__);
  }
}
void WCAImpl::initializeAlarms()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<AlarmInformation> aivector;
    {
        AlarmInformation ai;
        ai.alarmCode = NOLOCK;
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = HIGHCURRENT;
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = LOWCURRENT;
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = PHOTOMIXEROFF;
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = HIGHTEMP;
        aivector.push_back(ai);
    }
    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    alarmSender_m->initializeAlarms("WCA", compName.in(), aivector);
}
/**
 * ------------------------------------------------------------------------------------------------
 *  Hardware Lifecycle Methods
 * ------------------------------------------------------------------------------------------------
 */

void WCAImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    WCABase::hwOperationalAction();

    alarmThread_m->resume();
}

void WCAImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    WCABase::hwInitializeAction();
    /* Suspend execution of Thread */
    alarmThread_m ->suspend();
    try {
        std::auto_ptr<const ControlXmlParser> tmp = configData_m->getElement("FLOYIG");
        yigFreqMin_m = (*tmp).getDoubleAttribute("value");
        tmp = configData_m->getElement("FHIYIG");
        yigFreqMax_m = (*tmp).getDoubleAttribute("value");
        tmp = configData_m->getElement("ColdMultiplier");
        coldMultiplier_m = (*tmp).getLongAttribute("value");
        tmp = configData_m->getElement("PLLLoopBwMultiplier");
        pllLoopBwMultiplier_m = (*tmp).getLongAttribute("value");

    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,__FILE__, __LINE__,
                "WCAImpl::hwConfigureAction");
        std::string msg = "Could not access config data";
        nex.addData("Detail", msg);
        throw nex.getHwLifecycleEx();
    }

    /* This method is only expected to throw
     * ControlDeviceExceptions::HwLifecycleEx) we must chew up everything else*/
    try {
        /*As specified by front end LO Power Amps must be shut off when the
         * band is, this will do exactly that.*/
        setLOPowerAmps(false);
    }  catch (ControlExceptions::CAMBErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not power off the LO Power Amplifiers");
        nex.log(LM_WARNING);
        // Will chew on the exception since it is not fatal
    } catch(ControlExceptions::INACTErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not power off the LO Power Amplifiers");
        nex.log(LM_WARNING);
        // Will chew on the exception since it is not fatal
    }
    initializeAlarms();
    alarmSender_m->forceTerminateAllAlarms();
}

void WCAImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    /*if the hardware is stopped, don't even try*/
    if(getHwState()==Control::HardwareDevice::Stop) return;
    // Suspend the threads
    alarmThread_m->suspend(); 
    
    //disable alarms since we will shutdown. 10 minutes should be enough
    alarmSender_m->inhibitAlarms(static_cast<ACS::TimeInterval>(600*10E7));
  
    /*Only attempt to poweroff the PA when software if operational*/
    if(getHwState()==Control::HardwareDevice::Operational){
        try {
            /*As specified by front end LO Power Amps must be shut off when the
             * band is, this will do exactly that.*/
            setLOPowerAmps(false);
        }  catch (ControlExceptions::CAMBErrorExImpl &ex){
            ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                    __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail","Could not power off the LO Power Amplifiers");
            nex.log();
            // Will chew on the exception since it is not fatal
        } catch(ControlExceptions::INACTErrorExImpl &ex){
            ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                    __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail","Could not power off the LO Power Amplifiers");
            nex.log();
            // Will chew on the exception since it is not fatal
        }
    }
    WCABase::hwStopAction();
}

///////////////////////////////////////
// IMPLEMENTATION OF IDL INTERFACE
///////////////////////////////////////
void WCAImpl::SetFrequency(float nominalFrequency, bool lock=true)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    float refFrequency = nominalFrequency / coldMultiplier_m;
    float yigFreq = refFrequency / pllLoopBwMultiplier_m;

    if (yigFreq < yigFreqMin_m || yigFreq > yigFreqMax_m) {
        ostringstream message;
        message << "Cannot set the WCA frequency as the specified value is"
            << " outside the allowed range. The specified value is "
            << nominalFrequency << "Hz and the allowed limits are from "
            << yigFreqMin_m * pllLoopBwMultiplier_m * coldMultiplier_m << "Hz to "
            << yigFreqMax_m * pllLoopBwMultiplier_m * coldMultiplier_m << "Hz.";
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,__LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    try{
        setCntlLoPhotomixerOn();//Check if it is still needed since its done
        //in the FE.
        LOG_TO_AUDIENCE(LM_DEBUG, __FUNCTION__, "Photomixer ON", " ");
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        LOG_TO_AUDIENCE(LM_WARNING, __FUNCTION__, "Failed to turn Photomixer ON", " ");
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        LOG_TO_AUDIENCE(LM_WARNING, __FUNCTION__, "Failed to turn Photomixer ON", " ");
    }

    nominalFrequency_m = nominalFrequency;
    tagretDYTOFrequency_m = yigFreq;

    coarseYIG_m = (unsigned short) ((yigFreq - yigFreqMin_m) /
            (yigFreqMax_m - yigFreqMin_m) * 4095.0);

    try{

        
        //The FrontEnd behaves erratically at best when stepping frequencies
        //downwards, so to avoid this set the coarseTune to zero. This will
        //move the magnet all the way to the start, then we wait (its a magnet)
        //and start setting the real coarse tune.
        //2010-08-17 aditional if added to adress http://jira.alma.cl/browse/AIV-2988
        ACS::Time timestamp;
        if (coarseYIG_m != getLoYtoCoarseTune(timestamp)){
            ostringstream message;
            message << "Trying to set a CoarseTune different from what is already in place."
                    << "Current Course Tune: "<<getLoYtoCoarseTune(timestamp)
                    << " New Coase tune: "<<coarseYIG_m;
            LOG_TO_AUDIENCE(LM_WARNING, __FUNCTION__, message.str().c_str(), " ");
//            setCntlLoYtoCoarseTune(0);
//            usleep(static_cast<int>(250 * 1E3));//250ms//FIXME
            setCntlLoYtoCoarseTune(coarseYIG_m);
            usleep(10000);
        }

        ostringstream message;
        message<<"WCA Frequency set to: "<< nominalFrequency <<endl;
        LOG_TO_AUDIENCE(LM_INFO, __FUNCTION__, message.str(), " ");

        if(!lock)//if locked  we should not try to lock, just return.
            return;

        lockPll();
        LOG_TO_AUDIENCE(LM_INFO, __FUNCTION__, "WCA Lock Acquired", " ");

        if(!pllDebug_m){
            //Stuff is still adjusting here. If we give it time it stabilizes.
            usleep(static_cast<int>(150 * 1E3));//150ms//FIXME
            adjustPll(0.0);
            LOG_TO_AUDIENCE(LM_INFO, __FUNCTION__, "WCA PLL adjust to near zero", " ");
        }else{
            LOG_TO_AUDIENCE(LM_NOTICE, __FUNCTION__, "WCA PLL will not be adjust, we are set to debug mode", " ");
        }
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const WCAExceptions::InsufficientPowerExImpl& ex) {
        WCAExceptions::InsufficientPowerExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getInsufficientPowerEx();
    } catch(const WCAExceptions::LockFailedExImpl& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const WCAExceptions::LockLostExImpl& ex) {
        WCAExceptions::LockLostExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockLostEx();
    } catch(const WCAExceptions::AdjustPllFailedExImpl& ex) {
        WCAExceptions::AdjustPllFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getAdjustPllFailedEx();
    }
}


void WCAImpl::AdjustPll(float voltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        adjustPll(voltage);
    }catch(ControlExceptions::CAMBErrorExImpl &ex){
        throw ex.getCAMBErrorEx();
    }catch(ControlExceptions::INACTErrorExImpl &ex){
        throw ex.getINACTErrorEx();
    }catch(WCAExceptions::LockLostExImpl &ex){
        throw ex.getLockLostEx();
    }catch(WCAExceptions::AdjustPllFailedExImpl &ex){
        throw ex.getAdjustPllFailedEx();
    }
}

void WCAImpl::ReLock()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        lockPll();
        LOG_TO_AUDIENCE(LM_INFO, __FUNCTION__, "WCA ReLock Acquired", " ");
        if(!pllDebug_m){
            adjustPll(0.0);
            LOG_TO_AUDIENCE(LM_INFO, __FUNCTION__, "WCA PLL adjust to near zero", " ");
        }else{
            LOG_TO_AUDIENCE(LM_NOTICE, __FUNCTION__, "WCA PLL will not be adjust, we are set to debug mode", " ");
        }
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const WCAExceptions::InsufficientPowerExImpl& ex) {
        WCAExceptions::InsufficientPowerExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getInsufficientPowerEx();
    } catch(const WCAExceptions::LockFailedExImpl& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const WCAExceptions::LockLostExImpl& ex) {
        WCAExceptions::LockLostExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockLostEx();
    } catch(const WCAExceptions::AdjustPllFailedExImpl& ex) {
        WCAExceptions::AdjustPllFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getAdjustPllFailedEx();
    }
}

float WCAImpl::GetFrequency()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        throw ex.getINACTErrorEx();
    }
    return nominalFrequency_m;
}


void WCAImpl::SetLOPowerAmps(bool enable)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        setLOPowerAmps(enable);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    }
}

bool WCAImpl::IsLocked()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        return isLocked();
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    }
}

double WCAImpl::getTargetDYTOFrequency()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return tagretDYTOFrequency_m;
}

double WCAImpl::getDerivedDYTOFrequency()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    double derivedFreqYIG = -1;
    try {
        int coarseTune = getLoYtoCoarseTune(timestamp);
        derivedFreqYIG = yigFreqMin_m + (( yigFreqMax_m - yigFreqMin_m)*
                coarseTune)/4095.0;
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    }
    return derivedFreqYIG;
}

double WCAImpl::getHiDYTOLimit()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return yigFreqMax_m;
}

double WCAImpl::getLoDYTOLimit()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return yigFreqMin_m;
}

/********************
 * Internal Methods *
 ********************/

std::vector<double> WCAImpl::getConfigPowerAmp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    double freq = nominalFrequency_m;
    int index=-1;
    std::vector<double> retVal(0);
    try {
        //get the list of PowerAmp configurations
        std::auto_ptr<std::vector<ControlXmlParser> > list = configData_m->getElements("PowerAmp");
        //if we have only one row don't bother searching.
        if((*list).size() == 1) {
            index=0;
        }
        //Check for border conditions.
        if(freq < (*list)[0].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=0;
        }
        if(freq > (*list)[(*list).size()-1].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=(*list).size()-1;
        }
        //not a single row and not a border condition. This gets messy since
        //we have to interpolate.
        for(unsigned int i=1; i < (*list).size(); i++){
            if (index!=-1)break;
            if(freq == (*list)[i].getDoubleAttribute("FreqLO")){
                //found an exact match.
                index=i;
                break;
            }
            if(freq < (*list)[i].getDoubleAttribute("FreqLO")){
                //OK. we have to interpolate this row with the previous one
                double freqSpread = -1;
                double freqE = 0;
                double slope, desp;
                freqSpread = ((*list)[i].getDoubleAttribute("FreqLO") - (*list)[i -1 ].getDoubleAttribute("FreqLO"));
                freqE = (*list)[i].getDoubleAttribute("FreqLO");
                //FreqLO="614E9" VD0="1.96" VD1="1.63" VG0="-0.28" VG1="-0.28"
                //VD0
                slope = ((*list)[i].getDoubleAttribute("VD0") - (*list)[i-1].getDoubleAttribute("VD0")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VD0") - freqE * slope;
                retVal.push_back(freq * slope + desp);
                //VD1
                slope = ((*list)[i].getDoubleAttribute("VD1") - (*list)[i-1].getDoubleAttribute("VD1")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VD1") - freqE * slope;
                retVal.push_back(freq * slope + desp);
                //VG0
                slope = ((*list)[i].getDoubleAttribute("VG0") - (*list)[i-1].getDoubleAttribute("VG0")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VG0") - freqE * slope;
                retVal.push_back(freq * slope + desp);
                //VG1
                slope = ((*list)[i].getDoubleAttribute("VG1") - (*list)[i-1].getDoubleAttribute("VG1")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VG1") - freqE * slope;
                retVal.push_back(freq * slope + desp);
                break;
            }
        }
        if(index!=-1){
            retVal.push_back((*list)[index].getDoubleAttribute("VD0"));
            retVal.push_back((*list)[index].getDoubleAttribute("VD1"));
            retVal.push_back((*list)[index].getDoubleAttribute("VG0"));
            retVal.push_back((*list)[index].getDoubleAttribute("VG1"));
        }
    } catch(ControlExceptions::XmlParserErrorExImpl &ex) {
        ControlExceptions::XmlParserErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex;
    }
    return retVal;
}

double WCAImpl::getConfigTargetPhotoMixerCurrent()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    double freq = nominalFrequency_m;
    int index=-1;
    double retVal=0.0;
    try {
        //get the list of OptimizationTargets configurations
        std::auto_ptr<std::vector<ControlXmlParser> > list = configData_m->getElements("OptimizationTargets");

         // if we cannot found an PM current target, we set a default value and warn the user
        if((*list).size() < 1)
        {
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,(LM_ERROR,"Could not find any PM current target in the configuration file, using -0.4mA instead"));
            return 0.4E-3;
        }

        //if we have only one row don't bother searching.
        if((*list).size() == 1) {
            index=0;
        }
        //Check for border conditions.
        else if(freq < (*list)[0].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=0;
        }
        else if(freq > (*list)[(*list).size()-1].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=(*list).size()-1;
        }
        //not a single row and not a border condition. This gets messy since
        //we have to interpolate.
        else {
            for(unsigned int i=1; i < (*list).size(); i++){
                if (index!=-1)break;
                if(freq == (*list)[i].getDoubleAttribute("FreqLO")){
                    //found an exact match.
                    index=i;
                    break;
                }
                if(freq < (*list)[i].getDoubleAttribute("FreqLO")){
                    //OK. we have to interpolate this row with the previous one
                    double freqSpread = -1;
                    double freqE = 0;
                    double slope, desp;
                    freqSpread = ((*list)[i].getDoubleAttribute("FreqLO") - (*list)[i -1 ].getDoubleAttribute("FreqLO"));
                    freqE = (*list)[i].getDoubleAttribute("FreqLO");
                    //FreqLO="614E9" VD0="1.96" VD1="1.63" VG0="-0.28" VG1="-0.28"
                    //PhotoMixerCurrent Target
                    slope = ( fabs((*list)[i].getDoubleAttribute("PhotoMixerCurrent")) - fabs((*list)[i-1].getDoubleAttribute("PhotoMixerCurrent"))) / freqSpread;
                    desp =  fabs((*list)[i].getDoubleAttribute("PhotoMixerCurrent")) - freqE * slope;
                    return (freq * slope + desp);
                }
            }
        }
        if(index!=-1){
            return fabs((*list)[index].getDoubleAttribute("PhotoMixerCurrent"));
        }
    } catch(ControlExceptions::XmlParserErrorExImpl &ex) {
        ControlExceptions::XmlParserErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getXmlParserErrorEx();
    }
    return retVal;
}

void WCAImpl::lockPll()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    ostringstream message;
    /*Check that we have sufficient reference total power to lock the PLL */
    float referenceTotalPower;
    int delay = 0;
    bool isFinish = false;
    while (!isFinish) {
        try {
            referenceTotalPower = getLoPllRefTotalPower(timestamp);
        } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
            throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        } catch(const ControlExceptions::INACTErrorExImpl& ex) {
            throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        //referenceTotalPower is in VOLTS
        if (fabs(referenceTotalPower) < 0.5) {
            // JIRA:AIV-4285, Add a delay of 250 ms to wait the FLOOG is set.
            if (delay >= 250) {
                /* There is insufficient power to lock the PLL */
                message.str("");
                message<<"Insufficient power to lock WCA PLL: "<<referenceTotalPower<<" volt";
                WCAExceptions::InsufficientPowerExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
                ex.addData("Additional", message.str());
                ex.log();
                throw ex;
            }
            usleep(50000);//50ms
            delay = delay + 50;
        } else {
            isFinish = true;
        }
    }

    /* Start Phase Lock */

    //float pllLockVoltage;
    // lock search window is is +/- 50 MHz (was 30):
    // As of 2010-09-14, requested in http://jira.alma.cl/browse/AIV-3171
    const double window = 50E6;
    // compute YIG step size:
    double stepYIG = (yigFreqMax_m - yigFreqMin_m) / 4095.0;
    message.str("");
    message<<"stepYig "<<stepYIG<<endl;
    LOG_TO_AUDIENCE(LM_DEBUG, __FUNCTION__, message.str()," ");
    // lock search window, in YIG steps:
    int windowSteps = (int) (window / stepYIG) + 1;
    message.str("");
    message<<"windowSteps "<<windowSteps<<endl;
    LOG_TO_AUDIENCE(LM_DEBUG, __FUNCTION__, message.str()," ");
    // lock step size in YIG steps is windowSteps / 10:
    int stepSize = (int) windowSteps / 10;
    message.str("");
    message<<"stepSize "<<stepSize<<endl;
    LOG_TO_AUDIENCE(LM_DEBUG, __FUNCTION__, message.str()," ");
    int step = stepSize;
    message.str("");
    message<<"step "<<step<<endl;
    LOG_TO_AUDIENCE(LM_DEBUG, __FUNCTION__, message.str()," ");


    if (step<1) {
        message.str("");
        message << "Step size is not valid. If zero i will loop for ever. Step: "<<step;
        WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }
    int loLimit = coarseYIG_m - windowSteps;
    if (loLimit < 0)
        loLimit = 0;
    int hiLimit = coarseYIG_m + windowSteps;
    if (hiLimit > 4095)
        hiLimit = 4095;

    int tryCoarseYIG = coarseYIG_m;
    //No alarms for the next 2 seconds
    alarmSender_m->inhibitAlarms(static_cast<ACS::TimeInterval>(2*10E7));
    try {
        //pllLockVoltage = getLoPllLockDetectVoltage(timestamp);
        //while (pllLockVoltage<3.0) {
        usleep(100000);//100ms
        while (getLoPllLockDetectVoltage(timestamp) <= 3.0) {
            tryCoarseYIG = coarseYIG_m + step;
            if (tryCoarseYIG >= loLimit && tryCoarseYIG <= hiLimit) {
                /* set the integrator zero bit */
                setCntlLoPllNullLoopIntegratorNull();

                usleep(1000);  // 1ms delay
                /* Set the frequency */
                message.str("");
                message<<"Trying to Course tune: "<<tryCoarseYIG;
                //FIXME: COMP-2376
                LOG_TO_AUDIENCE(LM_DEBUG,__FUNCTION__,message.str()," ");
                setCntlLoYtoCoarseTune(tryCoarseYIG);

                usleep(2000);  // 2ms delay
                /* reset the integrator zero bit */
                setCntlLoPllNullLoopIntegratorOperate();

                /* wait a short time before reading the voltage again */
                usleep(static_cast<int>(lockAcquireTime_m * 1E6));
                //pllLockVoltage = getLoPllLockDetectVoltage(timestamp);
            }
            message.str("");
            message<<"Step: "<<step<<" Window: "<<windowSteps;
            LOG_TO_AUDIENCE(LM_DEBUG,__FUNCTION__,message.str()," ");
            if (step > 0)
                step = -step;
            else {
                step = -step + stepSize;
                if (step > windowSteps){
                    /* We have exceeded our maximum range, log the error and throw
                       exception
                     */

                    float pm = -99.99;
                    try {
                        pm = getLoPhotomixerCurrent(timestamp);
                    } catch (...){
                       //do nothing.
                    }
                    message.str("");
                    message << "No Lock found within "<< window << " Hz range."<<endl;
                    message << "PM Current: "<< pm ;
                    WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                            __PRETTY_FUNCTION__);
                    ex.addData("Detail", message.str());
                    ex.log();
                    throw ex;
                }
            }
        }//while
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
    }

    if(!isLocked()){
        float pm = -99.99;
        float ifp = -99.99;
        try {
            pm = getLoPhotomixerCurrent(timestamp);
            ifp = getLoPllIfTotalPower(timestamp);
        } catch(...) {
           //do nothing.
        }
        message.str("");
        message << "The WCA didn't lock properly. Look at the IF Power"<<endl;
        message << "IFTotal power: "<<ifp<<endl;
        message << "PM Current: "<<pm<<endl;
        WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    usleep(static_cast<int>(50000)); // 50ms
    setCntlLoPllClearUnlockDetectLatch();

    //We are done so re-enable alarms if they are inhibited
    alarmSender_m->resetAlarmsInhibitionTime();
    coarseYIG_m = tryCoarseYIG;
    flagIsLocked(true);
}

void WCAImpl::adjustPll(float targetCorrVoltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp;
    int maxDist = 50;
    int dist = 0;
    //double slope = 0.0025;
    double slope = 2.5E6; // This should be in Hz/v
    double window = 0.25;
    double hiThreshold = targetCorrVoltage + window;
    double loThreshold = targetCorrVoltage - window;

    float correctionVoltage;
    //float pllLockVoltage;
    float voltageError;
    double tryFreqYIG;
    int tryCoarseYIG;

    try {
        correctionVoltage = getLoPllCorrectionVoltage(timestamp);
        voltageError = correctionVoltage - targetCorrVoltage;
        /*Transform CoarseYIG to freq.*/
        tryFreqYIG = yigFreqMin_m + (( yigFreqMax_m - yigFreqMin_m)*
                coarseYIG_m)/4095.0;
        tryFreqYIG = tryFreqYIG + (voltageError * slope);
        /*Transform Freq to coarse */
        //FIXME: check that tryFreqYIG is within range.
        tryCoarseYIG = (unsigned short) ((tryFreqYIG - yigFreqMin_m) /
                (yigFreqMax_m - yigFreqMin_m) * 4095.0);

        setCntlLoYtoCoarseTune(tryCoarseYIG);
        usleep(static_cast<int>(lockAcquireTime_m * 1E6));
        correctionVoltage = getLoPllCorrectionVoltage(timestamp);
        //pllLockVoltage = getLoPllLockDetectVoltage(timestamp);
        int oscdetect[3];
        oscdetect[0]=tryCoarseYIG;
        oscdetect[1]=0;
        oscdetect[2]=0;
        while((correctionVoltage-targetCorrVoltage) <= loThreshold ||
                (correctionVoltage-targetCorrVoltage) >= hiThreshold) {
            if(!isLocked()) {
                /* we've lost the lock. Wrap and throw an exception*/
                WCAExceptions::LockLostExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
                ostringstream message;
                message << "Lost lock while adjusting PLL FM coil voltage."
                    << "Tried setting new freq: "<< tryFreqYIG << "with coarse:"
                    << tryCoarseYIG << "Previous coarse was: "<< oscdetect[1];
                ex.addData("Detail", message.str());
                ex.log();
                throw ex;
            }
            if(oscdetect[0]==oscdetect[2]){
                ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_NOTICE,"adjustPll is oscillating. Terminating optimization"));
                return;
            }else{
                oscdetect[0]=oscdetect[1];
                oscdetect[1]=oscdetect[2];
            }
            voltageError = correctionVoltage - targetCorrVoltage;
            tryCoarseYIG += (voltageError > 0) ? 1 : -1;
            oscdetect[2]=tryCoarseYIG;
            dist = abs(tryCoarseYIG - coarseYIG_m);
            if (dist > maxDist || tryCoarseYIG < 0 || tryCoarseYIG > 4095){
                /* we've gone out of range. Wrap and trow an exception*/
                WCAExceptions::AdjustPllFailedExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
                ostringstream message;
                message << "Failed with distance:" << dist << "of " << maxDist <<" ."
                    << "Calculated Coarse YIG:"<<tryCoarseYIG;
                ex.addData("Detail", message.str());
                ex.log();
                throw ex;
            }
            setCntlLoYtoCoarseTune(tryCoarseYIG);
            ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"Setting Coarse to %d", tryCoarseYIG));
            /* wait a short time before reading the voltage again */
            usleep(static_cast<int>(lockAcquireTime_m * 1E6));
            correctionVoltage = getLoPllCorrectionVoltage(timestamp);
            //correctionVoltage = getLoPllLockDetectVoltage(timestamp);
        }//while
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
    }
    coarseYIG_m = tryCoarseYIG;
}


void WCAImpl::setLOPowerAmps(bool enable)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (enable) {
        double VDP0, VDP1, VGP0, VGP1;
        std::vector<double> pa = getConfigPowerAmp();
        VDP0 = pa[0];
        VDP1 = pa[1];
        VGP0 = pa[2];
        VGP1 = pa[3];
        ostringstream message;
        message<<"Tuning Values: VDP0="<<VDP0<<" VDP1="<<VDP1;
        message<<"VGP0="<<VGP0<<" VGP1"<<VGP1;
        LOG_TO_AUDIENCE(LM_DEBUG, __FUNCTION__, message.str() ," ");
        setCntlLoPaPol0DrainVoltage(VDP0);
        setCntlLoPaPol1DrainVoltage(VDP1);
        setCntlLoPaPol0GateVoltage(VGP0);
        setCntlLoPaPol1GateVoltage(VGP1);
    }else{
        setCntlLoPaPol0DrainVoltage(0.0);
        setCntlLoPaPol1DrainVoltage(0.0);
        setCntlLoPaPol0GateVoltage(0.0);
        setCntlLoPaPol1GateVoltage(0.0);
    }
}

bool WCAImpl::isLocked()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    double ldv = getLoPllLockDetectVoltage(timestamp);
    double rtp = getLoPllRefTotalPower(timestamp);
    double itp = getLoPllIfTotalPower(timestamp);
    if ((ldv > 3.0) && (fabs(rtp) > 0.5) && (fabs(itp) > 0.5))
        return true;
    return false;
}


//External method that support configuration cache.
bool WCAImpl::isPACacheHit()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(paCache_m.find(int(nominalFrequency_m / 1E9)) != paCache_m.end())
        return true;
    return false;
}

void WCAImpl::setPACache(double VD1, double VD2)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<double> vec;
    vec.push_back(VD1);
    vec.push_back(VD2);
    paCache_m[int(nominalFrequency_m / 1E9)] = vec;
}

void WCAImpl::setPAFromCache()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<double> vec;
    std::map<int, std::vector<double> >::iterator it;
    it = paCache_m.find(int(nominalFrequency_m / 1E9));
    //safe guard
    if(it == paCache_m.end()) {
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,(LM_ERROR,"No values in cache table, PA not modified"));
        return;
    }
    vec = it->second;
    try {
        setCntlLoPaPol0DrainVoltage(vec[0]);
        setCntlLoPaPol1DrainVoltage(vec[1]);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_ERROR,"Could not set PA"));
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_ERROR,"Could not set PA"));
    }
}

double WCAImpl::getTargetVD1()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<double> row;
    row = getConfigPowerAmp();
    return row[0];
}

double WCAImpl::getTargetVD2()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<double> row;
    row = getConfigPowerAmp();
    return row[1];
}

void WCAImpl::setPllDebug(bool debug)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pllDebug_m = debug;
}

bool WCAImpl::getPllDebug()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return pllDebug_m;
}

void WCAImpl::setSelected(bool select)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    selected_m = select;
 //   if (select == false ) {
//        //Stop flagging.
//        flagIsLocked(true);
//    }
}

  /* ------------ AlarmThread class -------------- */
WCAImpl::WCAAlarmThread::WCAAlarmThread(const ACE_CString& name, WCAImpl& wca) :
  ACS::Thread(name),
  wca_p(wca)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);
}

/* runLoop implementation */
void WCAImpl::WCAAlarmThread::runLoop()
{
  ACS_TRACE(__PRETTY_FUNCTION__);
  wca_p.updateThreadAction();
//  wca_p.flagDataAsNeeded();
}


void WCAImpl::updateThreadAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;

        if(selected_m){
            try{
                float pcurrent = getLoPhotomixerCurrent(timestamp);
                if (pcurrent <= -1.0E-3) {
                    alarmSender_m->activateAlarm(HIGHCURRENT);
                }else{
                    alarmSender_m->deactivateAlarm(HIGHCURRENT);
                }
            }catch(...){
                //do nothing.
            }

            try{
                if(getLoPhotomixerEnable(timestamp))
                    alarmSender_m->deactivateAlarm(PHOTOMIXEROFF);
                else
                    alarmSender_m->activateAlarm(PHOTOMIXEROFF);
            }catch(...){
                //do nothing.
            }

            try{
                if(isLocked()) {
                    alarmSender_m->deactivateAlarm(NOLOCK);
                    flagIsLocked(true);
                }else{
                    alarmSender_m->activateAlarm(NOLOCK);
                    flagIsLocked(false);
                }
            }catch(...){
                //do nothing.
            }
        } else {
            //DEACTIVATE certain alarms and monitor the rest.
            alarmSender_m->deactivateAlarm(NOLOCK);
            alarmSender_m->deactivateAlarm(PHOTOMIXEROFF);
            alarmSender_m->deactivateAlarm(HIGHCURRENT);

        }
        try {
            if(getLoPllAssemblyTemp(timestamp) > 55 + 273.15) {
                alarmSender_m->activateAlarm(HIGHTEMP);
            } else {
                alarmSender_m->deactivateAlarm(HIGHTEMP);
            }
        }catch(...){
            //do nothing.
        }
}


void WCAImpl::flagDataAsNeeded()
{
//    if (selected_m == false) {
//        return;
//    }
//
//    try{
//        bool isLockedState = isLocked();
//
//
//        wasLockedState_m = isLockedState;
//
//            if(isLockedState)
//                flagIsLocked(true);
//            else
//                flagIsLocked(false);
//    }catch(...){
//        flagIsLocked(false);
//        LOG_TO_OPERATOR(LM_WARNING, "Could not determine lock. Flagging data");
//    }
//
}

void  WCAImpl::flagIsLocked(bool lock) {

    if (wasLockedState_m == lock) {
        return;
    }

    ACS::Time now = ::getTimeStamp();
    maci::SmartPtr<Control::ControlDevice> parent = getParentReference();
    if (parent.isNil()) return;

    Control::HardwareController_var frontEnd =
        Control::HardwareController::_narrow(&*parent);
    if (CORBA::is_nil(frontEnd)) return;

    CORBA::String_var componentName = name();
    try {
        frontEnd->flagData(!lock, now, componentName.in(),
                          "The WCA is not locked.");
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named "
            << ex._name() << " while trying to "
            << ((lock) ? "unflag": "flag") << " data."
            << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }

    wasLockedState_m = lock;

        ostringstream msg;
        msg << "WCA change lock state"
            << "trying to "
            << ((lock) ? "unflag": "flag") << " data.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
}

/** implementation of HACK described in the .h*/
/* revert the hack until i find out why it crashes*/
float WCAImpl::getLoPhotomixerCurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    float ret_val;
    try {
        pthread_mutex_lock(&m_mutex[m_mutexIndex]);
        ret_val = WCABase::getLoPhotomixerCurrent(timestamp);
        usleep(static_cast<int>(2 * 1E3));//2ms
        ret_val = WCABase::getLoPhotomixerCurrent(timestamp);
        pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    }catch(ControlExceptions::CAMBErrorExImpl &ex){
        pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
        throw ex;
    }catch(ControlExceptions::INACTErrorExImpl &ex){
        pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
        throw ex;
    }
    return ret_val;
}

float WCAImpl::getLoPhotomixerVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPhotomixerVoltage(timestamp);
}

float WCAImpl::getLoPllLockDetectVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPllLockDetectVoltage(timestamp);
}

float WCAImpl::getLoPllCorrectionVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPllCorrectionVoltage(timestamp);
}
float WCAImpl::getLoPllAssemblyTemp(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPllAssemblyTemp(timestamp);
}

float WCAImpl::getLoYigHeaterCurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoYigHeaterCurrent(timestamp);
}

float WCAImpl::getLoPllRefTotalPower(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPllRefTotalPower(timestamp);
}

float WCAImpl::getLoPllIfTotalPower(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPllIfTotalPower(timestamp);
}

float WCAImpl::getLoAmcGateAVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcGateAVoltage(timestamp);
}

float WCAImpl::getLoAmcDrainAVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcDrainAVoltage(timestamp);
}

float WCAImpl::getLoAmcDrainACurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcDrainACurrent(timestamp);
}

float WCAImpl::getLoAmcGateBVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcGateBVoltage(timestamp);
}

float WCAImpl::getLoAmcDrainBVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcDrainBVoltage(timestamp);
}

float WCAImpl::getLoAmcDrainBCurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcDrainBCurrent(timestamp);
}

float WCAImpl::getLoAmcGateEVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcGateEVoltage(timestamp);
}

float WCAImpl::getLoAmcDrainEVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcDrainEVoltage(timestamp);
}

float WCAImpl::getLoAmcDrainECurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcDrainECurrent(timestamp);
}

float WCAImpl::getLoAmcMultiplierDCurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcMultiplierDCurrent(timestamp);
}

float WCAImpl::getLoPaPol0GateVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaPol0GateVoltage(timestamp);
}

float WCAImpl::getLoPaPol1GateVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaPol1GateVoltage(timestamp);
}

float WCAImpl::getLoPaPol0DrainVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaPol0DrainVoltage(timestamp);
}

float WCAImpl::getLoPaPol1DrainVoltage(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaPol1DrainVoltage(timestamp);
}

float WCAImpl::getLoPaPol0DrainCurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaPol0DrainCurrent(timestamp);
}

float WCAImpl::getLoPaPol1DrainCurrent(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaPol1DrainCurrent(timestamp);
}

float WCAImpl::getLoAmcSupplyVoltage5v(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoAmcSupplyVoltage5v(timestamp);
}

float WCAImpl::getLoPaSupplyVoltage3v(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaSupplyVoltage3v(timestamp);
}

float WCAImpl::getLoPaSupplyVoltage5v(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPaSupplyVoltage5v(timestamp);
}

unsigned char WCAImpl::getLoPllUnlockDetectLatch(ACS::Time& timestamp)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_lock(&m_mutex[m_mutexIndex]);
    pthread_mutex_unlock(&m_mutex[m_mutexIndex]);
    return WCABase::getLoPllUnlockDetectLatch(timestamp);
}
/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(WCAImpl)
