
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WCAHWSimImpl.cpp
 *
 * $Id$
 */

#include "WCAHWSimImpl.h"

using namespace AMB;

/* Please use this class to implement complex functionality for the
 * WCAHWSimImpl helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

WCAHWSimImpl::WCAHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: WCAHWSimBase::WCAHWSimBase(node, serialNumber)
{
    initialize(node, serialNumber);
}

// Specific Control set helpers
// ----------------------------------------------------------------------------

void WCAHWSimImpl::setControlSetLoYtoCoarseTune(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoYtoCoarseTune(data);
    setMonitorLoYtoCoarseTune(data);
}

void WCAHWSimImpl::setControlSetLoPhotomixerOn(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPhotomixerOff(data);
    WCAHWSimBase::setControlSetLoPhotomixerOn(data);
    setMonitorLoPhotomixerEnable(data);
}

void WCAHWSimImpl::setControlSetLoPhotomixerOff(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPhotomixerOff(data);
    WCAHWSimBase::setControlSetLoPhotomixerOn(data);
    setMonitorLoPhotomixerEnable(data);
}

void WCAHWSimImpl::setControlSetLoPllClearUnlockDetectLatch(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPllClearUnlockDetectLatch(data);
    setMonitorLoPllUnlockDetectLatch(data);
}

void WCAHWSimImpl::setControlSetLoPllLoopBandwidthDefault(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPllLoopBandwidthDefault(data);
    WCAHWSimBase::setControlSetLoPllLoopBandwidthAlt(data);
    setControlSetLoPllLoopBandwidthDefault(data);
}

void WCAHWSimImpl::setControlSetLoPllLoopBandwidthAlt(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPllLoopBandwidthDefault(data);
    WCAHWSimBase::setControlSetLoPllLoopBandwidthAlt(data);
    setControlSetLoPllLoopBandwidthDefault(data);
}

void WCAHWSimImpl::setControlSetLoPllSbLockPolarityUpper(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPllSbLockPolarityUpper(data);
    setMonitorLoPllSbLockPolaritySelect(data);
}

void WCAHWSimImpl::setControlSetLoPllSbLockPolarityLower(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPllSbLockPolarityLower(data);
    setMonitorLoPllSbLockPolaritySelect(data);
}

void WCAHWSimImpl::setControlSetLoPllNullLoopIntegratorOperate(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPllNullLoopIntegratorOperate(data);
    WCAHWSimBase::setControlSetLoPllNullLoopIntegratorNull(data);
    setMonitorLoPllNullLoopIntegrator(data);
}

void WCAHWSimImpl::setControlSetLoPllNullLoopIntegratorNull(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPllNullLoopIntegratorOperate(data);
    WCAHWSimBase::setControlSetLoPllNullLoopIntegratorNull(data);
    setMonitorLoPllNullLoopIntegrator(data);
}

void WCAHWSimImpl::setControlSetLoAmcDrainBVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoAmcDrainBVoltage(data);
    setMonitorLoAmcDrainBVoltage(data);
}

void WCAHWSimImpl::setControlSetLoAmcMultiplierDVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoAmcMultiplierDVoltage(data);
    setMonitorLoAmcMultiplierDVoltage(data);
}

void WCAHWSimImpl::setControlSetLoAmcGateEVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoAmcGateEVoltage(data);
    setMonitorLoAmcGateEVoltage(data);
}

void WCAHWSimImpl::setControlSetLoAmcDrainEVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoAmcDrainEVoltage(data);
    setMonitorLoAmcDrainEVoltage(data);
}

void WCAHWSimImpl::setControlSetLoPaPol0GateVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPaPol0GateVoltage(data);
    setMonitorLoPaPol0GateVoltage(data);
}

void WCAHWSimImpl::setControlSetLoPaPol1GateVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPaPol1GateVoltage(data);
    setMonitorLoPaPol1GateVoltage(data);
}

void WCAHWSimImpl::setControlSetLoPaPol0DrainVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPaPol0DrainVoltage(data);
    setMonitorLoPaPol0DrainVoltage(data);
}

void WCAHWSimImpl::setControlSetLoPaPol1DrainVoltage(const std::vector<CAN::byte_t>& data)
{
    WCAHWSimBase::setControlSetLoPaPol1DrainVoltage(data);
    setMonitorLoPaPol1DrainVoltage(data);
}
