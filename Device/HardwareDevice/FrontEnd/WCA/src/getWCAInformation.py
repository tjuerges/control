#!/bin/env python
import csv
from optparse import OptionParser
from xml.dom.minidom import parseString
import sys




__revision__ = "@(#) $Id$"
#$Source$


        
       
if __name__ == "__main__":
    parser = OptionParser(usage="%prog -f inifile [-o outputfile ]", version=__revision__)
    parser.add_option("-f", "--file", dest = "infile", type = "str",
                      default = None,
                      help = "CSV file with the FE mapping",
                      metavar = "FILE")

    parser.add_option("-o", "--outputfile", default=None, type="str", 
                      dest="outfile",
                      help="CSV file with the WCA TMCDB information")
    (options, args) = parser.parse_args()
    if options.infile is None:
        parser.error("An input file must be suplied")
    
    if options.outfile is None:
        writer = csv.writer(sys.stdout)
    else:
        writer = csv.writer(open(options.outfile, "wb"))
        
        
            
    reader = csv.reader(open(options.infile, "rb"))
    writer.writerow(["ESN","Assigned Configuration", "Antenna", "FE#", "LOYig", "HIYig"])
    
    import Acspy.Clients.SimpleClient
    client = Acspy.Clients.SimpleClient.PySimpleClient("WCAInformation") 
#    tmcdb = client.getDefaultComponent("IDL:alma/TMCDB/Access:1.0")
    tmcdb = client.getComponentNonSticky("TMCDBNEW")

    for row in reader:
        FEcomponent = row[1]
        if (FEcomponent.find("WCA",0,3) == 0):
            outrow = row[0:4]
            try:
            #    print "get assembly ", str(row[0])
            #    continue; 
                assembly = tmcdb.getAssemblyConfigData(str(row[0]).strip())
            except Exception ,ex:#TmcdbNoSuchRowEx:
                #print ex
                continue;
            try:
                dom = parseString(assembly.xmlDoc)

                FLOYIG = dom.getElementsByTagName("FLOYIG")[0]
                FHIYIG = dom.getElementsByTagName("FHIYIG")[0]
                outrow.append(FLOYIG.getAttribute("value"))
                outrow.append(FHIYIG.getAttribute("value"))
                writer.writerow(outrow)
            except Exception, ex:
                print ex
                print str(row[0]).strip()
                print assembly.xmlDoc
            




