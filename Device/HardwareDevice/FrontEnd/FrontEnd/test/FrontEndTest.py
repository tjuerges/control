#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The FrontEndTest provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
from Acspy.Clients.SimpleClient import PySimpleClient

# Other imports
from time import sleep

# Exceptions
import ControlDeviceExceptions
import FrontEndExceptions
import exceptions

# For Control and band enums
import Control
from almaEnumerations_IF_idl import _0_ReceiverBandMod

# Automated test class
class automated( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()

    def tearDown(self):

        self.client.disconnect()
        del(self.client)

    def initFrontEnd(self):

        # Define the component name to be used
        self.componentName = "CONTROL/DA41/FrontEnd"
        print "Instantiating FrontEnd component..."

        try:
            # Get the component reference
            self.fe = self.client.getComponent(self.componentName)

        except Exception, e:
            print "Exception: ", str(e)

    def test01(self):
        '''
        Component instantiation and intialization test.
        '''
        self.initFrontEnd()
        sleep(2)
	print "Releasing FrontEnd component..."
        self.client.releaseComponent(self.componentName)
	sleep(2)

    def test02(self):
        '''
        Functional test
        '''
        self.initFrontEnd()

        try:
            # Configure the FrontEnd
            devicelist = []
 	    devicelist.append(Control.DeviceConfig("WCA3", []))
            devicelist.append(Control.DeviceConfig("PowerDist3", []))
            devicelist.append(Control.DeviceConfig("ColdCart3", []))
 	    devicelist.append(Control.DeviceConfig("WCA6", []))
            devicelist.append(Control.DeviceConfig("PowerDist6", []))
            devicelist.append(Control.DeviceConfig("ColdCart6", []))
 	    devicelist.append(Control.DeviceConfig("WCA7", []))
            devicelist.append(Control.DeviceConfig("PowerDist7", []))
            devicelist.append(Control.DeviceConfig("ColdCart7", []))
 	    devicelist.append(Control.DeviceConfig("WCA9", []))
            devicelist.append(Control.DeviceConfig("PowerDist9", []))
            devicelist.append(Control.DeviceConfig("ColdCart9", []))
            devicelist.append(Control.DeviceConfig("IFSwitch", []))
            devicelist.append(Control.DeviceConfig("LPR", []))

           
            feConfig = Control.DeviceConfig("FrontEnd", devicelist)  

            self.fe.createSubdevices(feConfig, "CONTROL/DA41/FrontEnd")
            print "FrontEnd configured."
            sleep(2)
             
            # Bring controller to operational
	    print "Bringing controller to operational..."
	    self.fe.controllerOperational()

            # Check available bands
            availableBands = self.fe.availableBands()
            print "availableBands = ", availableBands
            self.assertEquals(availableBands, [_0_ReceiverBandMod.ALMA_RB_03])

            # Power up one band ALMA_RB_03
            self.fe.powerUpBand(_0_ReceiverBandMod.ALMA_RB_03)
            print "ALMA_RB_03 powered up."
            sleep(3)

            # Check powered up bands
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [_0_ReceiverBandMod.ALMA_RB_03])

            # Select band ALMA_RB_03
            self.fe.selectBand(_0_ReceiverBandMod.ALMA_RB_03)

            # Check selected band
            selectedBand = self.fe.getCurrentBand()
            print "selectedBand = ", selectedBand
            self.assertEquals(poweredBands, [_0_ReceiverBandMod.ALMA_RB_03])

            # Power off band ALMA_RB_03
            self.fe.powerOffBand(_0_ReceiverBandMod.ALMA_RB_03)
            print "ALMA_RB_03 powered off."
            sleep(3)

            # Check powered up bands again
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [])
          
            # Releasing the component
            # Note: we force the FE component to release its references first
	    print "Shutting down controller..."
	    self.fe.controllerShutdown()
	    print "Releasing FrontEnd component..."
            self.fe.releaseSubdevices()
            self.client.releaseComponent(self.componentName)
	    sleep(1)
                    
        except Exception, e:
            print "Exception: ", str(e)
            

    def test03(self):
        '''
        Error condition tests
        '''
        self.initFrontEnd()

        try:
            # Try to define an incorrect component name:
            # CONTROL/DA41/FrontEnd/MyDevice3
            wcaConfig = Control.DeviceConfig("WCA3", [])
            pdConfig = Control.DeviceConfig("PowerDist3", [])
            ccConfig = Control.DeviceConfig("ColdCart3", [])
            fecConfig = Control.DeviceConfig("FEC", [])
            fedConfig = Control.DeviceConfig("MyDevice3", [])
            feConfig = Control.DeviceConfig("FrontEnd", [wcaConfig, pdConfig, ccConfig, fecConfig, fedConfig])

            self.fe.createSubdevices(feConfig, "CONTROL/DA41/FrontEnd")

        except (ControlDeviceExceptions.IllegalConfigurationEx), ex:
            print "Expected exception (IllegalConfigurationEx) catched. Going on."
            
        try:
            # Try to define an incorrect band number:
            # CONTROL/DA41/FrontEnd/WCA17
            wcaConfig = Control.DeviceConfig("WCA17", [])
            pdConfig = Control.DeviceConfig("PowerDist3", [])
            ccConfig = Control.DeviceConfig("ColdCart3", [])
            fecConfig = Control.DeviceConfig("FEC", [])
            fedConfig = Control.DeviceConfig("FED", [])
            feConfig = Control.DeviceConfig("FrontEnd", [wcaConfig, pdConfig, ccConfig, fecConfig, fedConfig])

            self.fe.createSubdevices(feConfig, "CONTROL/DA41/FrontEnd")

        except (ControlDeviceExceptions.IllegalConfigurationEx), ex:
            print "Expected exception (IllegalConfigurationEx) catched. Going on."

        # For the next test: configure the FrontEnd
        # ... but clean up first
	self.fe.controllerShutdown()
	self.fe.releaseSubdevices()

        wcaConfig = Control.DeviceConfig("WCA3", [])
        pdConfig = Control.DeviceConfig("PowerDist3", [])
        ccConfig = Control.DeviceConfig("ColdCart3", [])
        fecConfig = Control.DeviceConfig("FEC", [])
        fedConfig = Control.DeviceConfig("FED", [])
        feConfig = Control.DeviceConfig("FrontEnd", [wcaConfig, pdConfig, ccConfig, fecConfig, fedConfig])

        self.fe.createSubdevices(feConfig, "CONTROL/DA41/FrontEnd")
	self.fe.controllerOperational()
        sleep(1)
            
        try:
            # Try to power up a band that is not available
            self.fe.powerUpBand(_0_ReceiverBandMod.ALMA_RB_01)
        except (FrontEndExceptions.BandNotAvailableEx), ex:
            print "Expected exception (BandNotAvailableEx) catched. Going on."

        # For the next tests: power up one band ALMA_RB_03
        self.fe.powerUpBand(_0_ReceiverBandMod.ALMA_RB_03)
        sleep(1)

        try:
            # Try to select a band that is not powered up
            self.fe.selectBand(_0_ReceiverBandMod.ALMA_RB_01)
        except (FrontEndExceptions.BandNotPoweredEx), ex:
            print "Expected exception (BandNotPoweredEx) catched. Going on."
            
        try:
            # Try to power off a band that is not powered on
            self.fe.powerOffBand(_0_ReceiverBandMod.ALMA_RB_01)
        except (FrontEndExceptions.BandNotPoweredEx), ex:
            print "Expected exception (BandNotPoweredEx) catched. Going on."

        # For the next tests: power uff band ALMA_RB_03
        self.fe.powerOffBand(_0_ReceiverBandMod.ALMA_RB_03)
        sleep(1)
            
        try:
            # Release FrontEnd component
            # Note: we force the FE component to release its references first
	    self.fe.controllerShutdown()
            self.fe.releaseSubdevices()
            print "Releasing FrontEnd component..."
            self.client.releaseComponent(self.componentName)
            sleep(1)
        except Exception, e:
            print "Exception: ", str(e)


# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()


