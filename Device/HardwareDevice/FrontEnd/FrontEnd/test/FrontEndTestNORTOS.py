#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The FrontEndTest provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
from Acspy.Clients.SimpleClient import PySimpleClient


# Other imports
from time import sleep

# Exceptions
import ControlDeviceExceptions
import ControlExceptions
import FrontEndExceptions
import exceptions

# For Control and band enums
import Control

from PyDataModelEnumeration import PyReceiverBand
from PyDataModelEnumeration import PyCalibrationDevice

# Front End Test Class
class FrontEndTestNoRTOS( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()
        self.initFrontEnd()

    def tearDown(self):

	print "Shutting down controller..."
	self.fe.controllerShutdown()
	print "Releasing FrontEnd component..."
        self.fe.releaseSubdevices()
        self.client.releaseComponent(self.componentName)
	sleep(1)
        self.client.disconnect()
        del(self.client)

    def initFrontEnd(self):

        # Define the component name to be used
        self.componentName = "CONTROL/DA41/FrontEnd"
        print "Instantiating FrontEnd component..."

        try:
            # Get the component reference
            self.fe = self.client.getComponent(self.componentName)

 	    wca3Config = Control.DeviceConfig("WCA3", [])
            pd3Config = Control.DeviceConfig("PowerDist3", [])
            cc3Config = Control.DeviceConfig("ColdCart3", [])
 	    wca4Config = Control.DeviceConfig("WCA4", [])
            pd4Config = Control.DeviceConfig("PowerDist4", [])
            cc4Config = Control.DeviceConfig("ColdCart4", [])
 	    wca6Config = Control.DeviceConfig("WCA6", [])
            pd6Config = Control.DeviceConfig("PowerDist6", [])
            cc6Config = Control.DeviceConfig("ColdCart6", [])
 	    wca7Config = Control.DeviceConfig("WCA7", [])
            pd7Config = Control.DeviceConfig("PowerDist7", [])
            cc7Config = Control.DeviceConfig("ColdCart7", [])
 	    wca8Config = Control.DeviceConfig("WCA8", [])
            pd8Config = Control.DeviceConfig("PowerDist8", [])
            cc8Config = Control.DeviceConfig("ColdCart8", [])
 	    wca9Config = Control.DeviceConfig("WCA9", [])
            pd9Config = Control.DeviceConfig("PowerDist9", [])
            cc9Config = Control.DeviceConfig("ColdCart9", [])
            cryoConfig = Control.DeviceConfig("Cryostat", [])
            ifsConfig = Control.DeviceConfig("IFSwitch", [])
            lprConfig = Control.DeviceConfig("LPR", []) 
            acdConfig = Control.DeviceConfig("ACD", []) 
            feConfig = Control.DeviceConfig("FrontEnd", [cryoConfig, ifsConfig, lprConfig, acdConfig, wca3Config, pd3Config, cc3Config,wca4Config, pd4Config, cc4Config, wca6Config, pd6Config, cc6Config,wca7Config, pd7Config, cc7Config,wca8Config, pd8Config, cc8Config, wca9Config, pd9Config, cc9Config])  

            self.fe.createSubdevices(feConfig, "CONTROL/DA41/FrontEnd")
            print "FrontEnd configured."
            sleep(2)
            
            # Bring controller to operational
	    print "Bringing controller to operational..."
	    self.fe.controllerOperational()
        except Exception, e:
            print "Exception: ", str(e)

    def cleanup(self):
            poweredBands = self.fe.getPoweredBands()
            for p in poweredBands:
                self.fe.powerOffBand(p)

    def test01(self):
        '''
        Functional test
        '''

        try:

            # Check available bands
            availableBands = self.fe.availableBands()
            print "availableBands = ", availableBands

            # Power up one band ALMA_RB_03
            self.fe.powerUpBand(PyReceiverBand.ALMA_RB_03)
            print "ALMA_RB_03 powered up."
            sleep(3)

            # Check powered up bands
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [PyReceiverBand.ALMA_RB_03])

            # Select band ALMA_RB_03
            self.fe.selectBand(PyReceiverBand.ALMA_RB_03)

            # Check selected band
            selectedBand = self.fe.getCurrentBand()
            print "selectedBand = ", selectedBand
            self.assertEquals(poweredBands, [PyReceiverBand.ALMA_RB_03])

            # Power off band ALMA_RB_03
            self.fe.powerOffBand(PyReceiverBand.ALMA_RB_03)
            print "ALMA_RB_03 powered off."
            sleep(3)

            # Check powered up bands again
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [])
          
            # Power up one band ALMA_RB_03
            self.fe.powerUpBand(PyReceiverBand.ALMA_RB_03)
            print "ALMA_RB_03 powered up. Second Time"
            sleep(3)

            # Check powered up bands
            poweredBands = self.fe.getPoweredBands()
            print "Second Time poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [PyReceiverBand.ALMA_RB_03])

            # Select band ALMA_RB_03
            self.fe.selectBand(PyReceiverBand.ALMA_RB_03)

            # Check selected band
            selectedBand = self.fe.getCurrentBand()
            print "Second Time selectedBand = ", selectedBand
            self.assertEquals(poweredBands, [PyReceiverBand.ALMA_RB_03])

            # Power off band ALMA_RB_03
            self.fe.powerOffBand(PyReceiverBand.ALMA_RB_03)
            print "ALMA_RB_03 powered off. Second Time"
            sleep(3)

            # Check powered up bands again
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [])
            # Releasing the component
            # Note: we force the FE component to release its references first
                    
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()
            
    def test02(self):
        '''
        Functional test
        '''

        try:
             
            # Check available bands
            availableBands = self.fe.availableBands()
            print "availableBands = ", availableBands

            # Power up band ALMA_RB_03 (should already be powered up)
            self.fe.powerUpBand(PyReceiverBand.ALMA_RB_03)
            print "ALMA_RB_03 powered up."
            sleep(2)

            # Power up band ALMA_RB_06
            self.fe.powerUpBand(PyReceiverBand.ALMA_RB_06)
            print "ALMA_RB_06 powered up."
            sleep(2)
 
            # Power up band ALMA_RB_09
            self.fe.powerUpBand(PyReceiverBand.ALMA_RB_09)
            print "ALMA_RB_09 powered up."
            sleep(2)

            
            # Check powered up bands
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [PyReceiverBand.ALMA_RB_03, PyReceiverBand.ALMA_RB_06, PyReceiverBand.ALMA_RB_09])

            # Select band ALMA_RB_03
            self.fe.selectBand(PyReceiverBand.ALMA_RB_03)

            # Power up one band ALMA_RB_07
            try:
                self.fe.powerUpBand(PyReceiverBand.ALMA_RB_07)
                print "ALMA_RB_07 powered up."
                sleep(2)
            except (FrontEndExceptions.PowerUpLimitReachedEx), ex:
                print "Expected PowerUpLimitReachedEx exception"
            else:
                self.fail()

            # Power off all bands
            self.fe.powerOffAllBands()
            print "All bands powered off."
            sleep(2)
             
            # Check powered band
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [])

            # Power on band ALMA_RB_07
	    self.fe.powerUpBand(PyReceiverBand.ALMA_RB_07)
            print "ALMA_RB_07 powered up."
            sleep(2)
            
            # Select band ALMA_RB_07
            self.fe.selectBand(PyReceiverBand.ALMA_RB_07)

            # Check selected band
            selectedBand = self.fe.getCurrentBand()
            print "selectedBand = ", selectedBand
            self.assertEquals(selectedBand, PyReceiverBand.ALMA_RB_07)
            
            # Power off band ALMA_RB_07
            self.fe.powerOffBand(PyReceiverBand.ALMA_RB_07)
            print "ALMA_RB_07 powered off."
            sleep(2)

            # Check selected band
            selectedBand = self.fe.getCurrentBand()
            print "selectedBand = ", selectedBand
            self.assertEquals(selectedBand, PyReceiverBand.UNSPECIFIED)
            
            # Check powered up bands again
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [])
          
                    
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()

    def test03(self):
        '''test for FrontEndExceptions.BandNotAvailableEx using a not available band'''
        try:        
            # Power up one band ALMA_RB_01
            self.fe.powerUpBand(PyReceiverBand.ALMA_RB_01)
            print "ALMA_RB_01 powered up."
            sleep(2)
        except (FrontEndExceptions.BandNotAvailableEx), e:
            print "Expected exception FrontEndExceptions.BandNotAvailableEx"
        except Exception, e:
            print "Exception: ", str(e)
            self.fail()
        else:
            self.fail()
        finally:
            self.cleanup()

    def test04(self):
        '''test for FrontEndExceptions.BandNotPoweredEx using an available band'''
        try:        
            self.fe.selectBand(PyReceiverBand.ALMA_RB_06)
        except (FrontEndExceptions.BandNotPoweredEx), e:
            print "Expected exception FrontEndExceptions.BandNotPoweredEx"
        except Exception, e:
            print "Exception: ", str(e)
            self.fail()
        else:
            pass
            #Disabling test, since a not powered band will be powered up
            #upon selection.
            #self.fail()
        finally:
            self.cleanup()
            
    def test05(self):
        '''test for FrontEndExceptions.BandNotPoweredEx using a not available band'''
        try:        
            self.fe.selectBand(PyReceiverBand.ALMA_RB_02)
        except (FrontEndExceptions.BandNotPoweredEx), e:
            print "Expected exception FrontEndExceptions.BandNotPoweredEx"
        except Exception, e:
            print "Exception: ", str(e)
            self.fail()
        else:
            self.fail()
        finally:
            self.cleanup()

    def test06(self):
        '''selectBand/getCurrentBand test'''
        try: 
            # Power up band ALMA_RB_03
            self.fe.powerUpBand(PyReceiverBand.ALMA_RB_03)
            print "ALMA_RB_03 powered up."
            sleep(2)
            # Select band ALMA_RB_03
            # This will set the IFSwitch and LPRs
            self.fe.selectBand(PyReceiverBand.ALMA_RB_03)
            band = self.fe.getCurrentBand()
            print "Current band is", band
        except Exception, e:
            print "Exception: ", str(e)
            self.fail()
        finally:
            self.cleanup()    

    def test07(self):
        '''The purpose of this test is to verify that when the FE controller becomes operational, so does band 3.'''

        try:

            # Check available bands
            availableBands = self.fe.availableBands()
            print "availableBands = ", availableBands

            # Check powered up bands
            poweredBands = self.fe.getPoweredBands()
            print "poweredBands = ", poweredBands
            self.assertEquals(poweredBands, [PyReceiverBand.ALMA_RB_03])

        except Exception, e:
            print "Exception: ", str(e)
            self.fail()
        finally:
            self.cleanup()    

    def testCalibrationDevice(self):
        '''calibration device test'''

        # We need to power up and select a band
        self.fe.powerUpBand(PyReceiverBand.ALMA_RB_03)
        self.fe.powerUpBand(PyReceiverBand.ALMA_RB_06)
        
        sleep(2) # Give the band time to power up
        self.fe.selectBand(PyReceiverBand.ALMA_RB_03)

        self.fe.setCalibrationDevice(PyCalibrationDevice.NONE)

        calDevice = self.fe.getCurrentCalibrationDevice()
        if calDevice != PyCalibrationDevice.NONE:
            self.fail("Incorrect Calibration Device Returned (" +
                      str(calDevice) + ") expected NONE.");

                
        # Set the AMBIENT LOAD on Band 3
        self.fe.setCalibrationDevice(PyCalibrationDevice.AMBIENT_LOAD)

        
        calDevice = self.fe.getCurrentCalibrationDevice()
        if calDevice != PyCalibrationDevice.AMBIENT_LOAD:
            self.fail("Incorrect Calibration Device Returned (" +
                      str(calDevice) + ") expected AMBIENT_LOAD.");

        # Set the Hot Load 
        self.fe.setCalibrationDevice(PyCalibrationDevice.HOT_LOAD)

        calDevice = self.fe.getCurrentCalibrationDevice()
        if calDevice != PyCalibrationDevice.HOT_LOAD:
            self.fail("Incorrect Calibration Device Returned (" +
                      str(calDevice) + ") expected HOT_LOAD.");

        # Move the Cal device to another band
        self.fe.setCalibrationDeviceBand(PyCalibrationDevice.HOT_LOAD,
                                         PyReceiverBand.ALMA_RB_06)

        try:
           calDevice = self.fe.getCurrentCalibrationDevice()
        except ControlExceptions.HardwareErrorEx:
            pass
        else:
            self.fail("Failed to get exception when other band was specified");
    
        self.fe.selectBand(PyReceiverBand.ALMA_RB_06)

        calDevice = self.fe.getCurrentCalibrationDevice()
        #if calDevice != PyCalibrationDevice.NONE: #it should be this. But the resetung feature was disabled.
        if calDevice != PyCalibrationDevice.HOT_LOAD:
            self.fail("Incorrect Calibration Device Returned (" +
                      str(calDevice) + ") expected NONE.");




# MAIN Program
if __name__ == '__main__':
    suite = unittest.makeSuite(FrontEndTestNoRTOS)
    unittest.TextTestRunner(verbosity=2).run(suite)


