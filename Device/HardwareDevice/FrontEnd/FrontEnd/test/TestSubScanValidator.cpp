/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * $Id$
 *
 */

//#include <limits>
//#include <vector>
#include <cppunit/extensions/HelperMacros.h>
//#include <TypeConversion.h>
#include <SubScanValidator.h>

/**
 * A test case for the WCAHWSimBase class
 *
 */
class SubScanValidatorTestCase: public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(SubScanValidatorTestCase);
    CPPUNIT_TEST(test_checkParameterTime);
    CPPUNIT_TEST(test_checkParameterBand);
  //  CPPUNIT_TEST(test_checkParameterFrequencyForBand);
    CPPUNIT_TEST(test_checkParameterCalibrationDevice);
    CPPUNIT_TEST_SUITE_END();

    public:
    void setUp();
    void tearDown();

    protected:
    
    void test_checkParameterTime();
    
    void test_checkParameterBand();
    
    void test_checkParameterFrequencyForBand();
    
    void test_checkParameterCalibrationDevice();

    SubScanValidator *validator;
};

CPPUNIT_TEST_SUITE_REGISTRATION(SubScanValidatorTestCase);


void SubScanValidatorTestCase::setUp()
{
    std::set<ReceiverBandMod::ReceiverBand> poweredBands;
    poweredBands.insert(ReceiverBandMod::ALMA_RB_01);
    poweredBands.insert(ReceiverBandMod::ALMA_RB_03);
    poweredBands.insert(ReceiverBandMod::ALMA_RB_07);

    validator = new SubScanValidator(poweredBands);
}

void SubScanValidatorTestCase::tearDown()
{
    delete validator;
}

void SubScanValidatorTestCase::test_checkParameterTime() 
{
    ACS::Time now = ::getTimeStamp();
    CPPUNIT_ASSERT_NO_THROW(validator->checkParameterTime(now * 2));
    CPPUNIT_ASSERT_THROW(validator->checkParameterTime(now / 2), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_THROW(validator->checkParameterTime(0), ControlExceptions::IllegalParameterErrorExImpl);
}

void SubScanValidatorTestCase::test_checkParameterBand()
{
    CPPUNIT_ASSERT_THROW(validator->checkParameterBand((ReceiverBandMod::ReceiverBand)1000), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_NO_THROW(validator->checkParameterBand(ReceiverBandMod::ALMA_RB_01));
    CPPUNIT_ASSERT_NO_THROW(validator->checkParameterBand(ReceiverBandMod::ALMA_RB_03));
    CPPUNIT_ASSERT_NO_THROW(validator->checkParameterBand(ReceiverBandMod::ALMA_RB_07));
    CPPUNIT_ASSERT_THROW(validator->checkParameterBand(ReceiverBandMod::ALMA_RB_09), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_THROW(validator->checkParameterBand(ReceiverBandMod::UNSPECIFIED), ControlExceptions::IllegalParameterErrorExImpl);
}

void SubScanValidatorTestCase::test_checkParameterFrequencyForBand()
{
    CPPUNIT_ASSERT_NO_THROW(validator->checkParameterFrequencyForBand( 30E9 , ReceiverBandMod::ALMA_RB_01));
    CPPUNIT_ASSERT_THROW(validator->checkParameterFrequencyForBand( 30E9 , ReceiverBandMod::ALMA_RB_03), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_THROW(validator->checkParameterFrequencyForBand( 30E9 , ReceiverBandMod::ALMA_RB_09), ControlExceptions::IllegalParameterErrorExImpl);
}

void SubScanValidatorTestCase::test_checkParameterCalibrationDevice()
{
   CPPUNIT_ASSERT_NO_THROW(validator->checkParameterCalibrationDevice(CalibrationDeviceMod::AMBIENT_LOAD));
   CPPUNIT_ASSERT_NO_THROW(validator->checkParameterCalibrationDevice(CalibrationDeviceMod::HOT_LOAD));
   CPPUNIT_ASSERT_NO_THROW(validator->checkParameterCalibrationDevice(CalibrationDeviceMod::NONE));
   CPPUNIT_ASSERT_NO_THROW(validator->checkParameterCalibrationDevice(CalibrationDeviceMod::SOLAR_FILTER));
   CPPUNIT_ASSERT_THROW(validator->checkParameterCalibrationDevice(CalibrationDeviceMod::QUARTER_WAVE_PLATE), ControlExceptions::IllegalParameterErrorExImpl);
   CPPUNIT_ASSERT_THROW(validator->checkParameterCalibrationDevice(CalibrationDeviceMod::COLD_LOAD), ControlExceptions::IllegalParameterErrorExImpl);
   CPPUNIT_ASSERT_THROW(validator->checkParameterCalibrationDevice(CalibrationDeviceMod::NOISE_TUBE_LOAD), ControlExceptions::IllegalParameterErrorExImpl);
   CPPUNIT_ASSERT_THROW(validator->checkParameterCalibrationDevice((CalibrationDeviceMod::CalibrationDevice)999999), ControlExceptions::IllegalParameterErrorExImpl);
}


/**
 * Main function running the tests
 */
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main(int argc, char* argv[])
{
    // Create the event manager and test controller
    CPPUNIT_NS::TestResult controller;

    // Add a listener that colllects test result
    CPPUNIT_NS::TestResultCollector result;
    controller.addListener(&result);

    // Add a listener that print dots as test run.
    CPPUNIT_NS::BriefTestProgressListener progress;
    controller.addListener(&progress);

    // Add the top suite to the test runner
    CPPUNIT_NS::TestRunner runner;
    runner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    runner.run(controller);

    // Print test in a compiler compatible format.
    CPPUNIT_NS::CompilerOutputter outputter(&result, std::cerr);
    outputter.write();

    return result.wasSuccessful() ? 0 : 1;
}
