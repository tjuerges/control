/*
 * CalibrationDevice.cpp
 *
 *  Created on: Mar 7, 2011
 *      Author: ntroncos
 */

#include <FrontEndImpl.h>
#include "FrontEndUtils.h"

using namespace ReceiverBandMod;
using namespace CalibrationDeviceMod;

using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::TimeoutExImpl;

void FrontEndImpl::setCalibrationDevice(
        CalibrationDeviceMod::CalibrationDevice cd)
{
    setCalibrationDeviceBand(cd, currentBand_m);
}

void FrontEndImpl::setCalibrationDeviceBand(
        CalibrationDeviceMod::CalibrationDevice cd,
        ReceiverBandMod::ReceiverBand band)
{
    try {

        setCalibrationDeviceBandInternal(cd, band);

    } catch (ControlExceptions::HardwareErrorExImpl& ex) {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getHardwareErrorEx();
    } catch (INACTErrorExImpl& ex) {
        INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (IllegalParameterErrorExImpl& ex) {
        IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getIllegalParameterErrorEx();
    } catch (TimeoutExImpl& ex) {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        std::cout << "ACD Movement: Time: TIMEOUT  To: " << band << " Set As: "
                << cd << std::endl;
        nex.log();
        throw nex.getTimeoutEx();
    } catch(...){
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getHardwareErrorEx();
    }
}

void FrontEndImpl::setCalibrationDeviceBandInternal(
        CalibrationDeviceMod::CalibrationDevice cd,
        ReceiverBandMod::ReceiverBand band)
{

    ACS::Time starttime = getTimeStamp();
    ACS::Time endtime;

    Control::ACD_ptr acd_ptr = getSubdeviceReference<Control::ACD> (
            FrontEndUtils::ACD_STRING);

    acd_ptr->setCalibrationDeviceBand(cd, band);
    acd_ptr->waitTillInPosition(FrontEndImpl::ACD_TIMEOUT);

    endtime = getTimeStamp();

    std::cout << "ACD Movement: Time: " << endtime - starttime << " To: "
            << band << " Set As: " << cd << std::endl;
    std::cout << "ACDCSV," << endtime - starttime << "," << band << "," << cd
            << std::endl;

}

CalibrationDeviceMod::CalibrationDevice FrontEndImpl::getCurrentCalibrationDevice()
{
    try {

        return getCurrentCalibrationDeviceInternal();

    } catch (HardwareErrorExImpl &ex) {
        HardwareErrorExImpl  nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getHardwareErrorEx();
    } catch (IllegalParameterErrorExImpl &ex) {
        IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getIllegalParameterErrorEx();
    } catch (ControlExceptions::INACTErrorExImpl& ex) {
        INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch(...){
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getHardwareErrorEx();
    }

}
CalibrationDeviceMod::CalibrationDevice FrontEndImpl::getCurrentCalibrationDeviceInternal()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

        throwIfNoBandSelected(__FILE__,__LINE__, __PRETTY_FUNCTION__);

        Control::ACD_ptr acd_ptr = getSubdeviceReference<Control::ACD> (
                FrontEndUtils::ACD_STRING);

        ReceiverBand cartrigeonband = acd_ptr->getCurrentCartrige();

        if ( cartrigeonband == ReceiverBandMod::UNSPECIFIED) {
            /* The ACD is in a park position return None */
            return CalibrationDeviceMod::NONE;
        }

        if (cartrigeonband != currentBand_m) {
            HardwareErrorExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            string msg("The ACD is in front of a different band");
            ex.addData("Error Message", msg);
            ex.addData("ACD Cartridge: ", FrontEndUtils::bandToInt(cartrigeonband));
            ex.addData("Current band: ", FrontEndUtils::bandToInt(currentBand_m));
            ex.log();
            throw ex;
        }

        /* At this point we know we are talking about the current band */
        return acd_ptr->getCurrentLoad();
}

void FrontEndImpl::enableCalDeviceDataReporting(bool enable)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::ACD_ptr acd_ptr = getSubdeviceReference<Control::ACD> (
            FrontEndUtils::ACD_STRING);


    acd_ptr->enableCalDeviceDataReporting(enable);

}

bool FrontEndImpl::calDeviceDataReportingEnabled()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::ACD_ptr acd_ptr = getSubdeviceReference<Control::ACD> (
            FrontEndUtils::ACD_STRING);

    return acd_ptr->calDeviceDataReportingEnabled();
}
