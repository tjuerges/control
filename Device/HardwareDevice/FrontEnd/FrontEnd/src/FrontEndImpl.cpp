/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FrontEndImpl.cpp
 *
 * $Id$
 */

#include <FrontEndImpl.h>

#include "SubScanValidator.h"
#include "FrontEndUtils.h"

#include <algorithm> //std::min

#include <TETimeUtil.h>


// Namespaces
using namespace ReceiverBandMod;
using namespace CalibrationDeviceMod;


using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::TimeoutExImpl;
using FrontEndUtils::bandToInt;
using FrontEndUtils::getColdMultiplier;

/**
 *-----------------------
 * Constants
 *-----------------------
 */
// Only two receiver bands and band 3 can be powered up at the same time:
const unsigned int FrontEndImpl::MAX_POWERED_BANDS = 3;

// Up to 10 receiver bands should be considered:
const unsigned int FrontEndImpl::NUM_RECEIVER_BANDS = 10;

/* This is the maximum time that the Front End will wait for the ACD to
 get in the correct position. */
const double FrontEndImpl::ACD_TIMEOUT = 10.0;

/**
 *-----------------------
 * FrontEnd Constructor
 *-----------------------
 */
FrontEndImpl::FrontEndImpl(const ACE_CString& name, maci::ContainerServices* cs) :
    Control::HardwareControllerImpl(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // Initialize the lists
    availableBandsList_m.clear();
    poweredBandsList_m.clear();

    // Initialize the currently selected band
    currentBand_m = ReceiverBandMod::UNSPECIFIED;
    enableLPRTunning_m = true;
    useCache_m = false;
    nominalFrequency_m = -1;
    cleanUpCalled = false;
}

/**
 *-----------------------
 * FrontEnd Destructor
 *-----------------------
 */
FrontEndImpl::~FrontEndImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}


void FrontEndImpl::cleanUp()
{
    /** 2010-04-08 ntroncos: note to the future maintainer. What is the purpose of this cleanUp.
     * The issue is that the cleanUp process is not thread safe. When
     * Control::HardwareControllerImpl::cleanUp() is called, Controller
     * shutdown will be called, so if any thread was working with the devices everything will
     * turn into a bag of crap. To avoid this, first set the controller to Shutdown State, this will prevent
     * any incoming request from executing, since  external method check for this. Then
     * acquire a write mutex, that will block, until every other method has finished
     * then proceed with the shutdown.
     * Every relevant method check for the shutdown state afeter acquiring the mutex. The one
     * that cannot due to the design check for cleanUpCalled, and return if true.
     */
    setControllerState(Control::HardwareController::Shutdown);
    cleanUpCalled = true;
    ACE_Write_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    Control::HardwareControllerImpl::cleanUp();
}
/**
 *-------------------------------------------------
 * External Interface: General Controller Interface
 *-------------------------------------------------
 */
void FrontEndImpl::createSubdevices(const Control::DeviceConfig& config,
        const char* parentName)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    availableBandsList_m.clear();
    // As we are going to re-take references, which might fail, we
    // also reset the currently selected band:
    currentBand_m = ReceiverBandMod::UNSPECIFIED;

    std::set<ReceiverBandMod::ReceiverBand> coldCartList;
    std::set<ReceiverBandMod::ReceiverBand> powerdistList;
    std::set<ReceiverBandMod::ReceiverBand> wcaList;

    try {

        // Obtain the references to the subdevices
        Control::HardwareControllerImpl::createSubdevices(config, parentName);

        // -----------------------------------------------------------------------
        // Go through the sequence of subdevices and parse the type and band number
        for (unsigned int idx = 0; idx < config.subdevice.length(); idx++) {

            string componentName = string(config.subdevice[idx].Name);

            int integerInName;
            try {
                integerInName = FrontEndUtils::getIntFromName(componentName);
            } catch(const IllegalParameterErrorExImpl &ex) {
                continue;
            }

            if (componentName.find(FrontEndUtils::COLDCART_STRING)
                    != string::npos) {
                coldCartList.insert(FrontEndUtils::intToBand(integerInName));
            } else if (componentName.find(FrontEndUtils::POWERDIST_STRING)
                    != string::npos) {
                powerdistList.insert(FrontEndUtils::intToBand(integerInName));
            } else if (componentName.find(FrontEndUtils::WCA_STRING)
                    != string::npos) {
                wcaList.insert(FrontEndUtils::intToBand(integerInName));
            }

        }

        // Determine available bands
        std::set<ReceiverBandMod::ReceiverBand>::iterator it;
        for (it = coldCartList.begin(); it != coldCartList.end(); it++) {
            // We need one WCA, one PowerDist and one ColdCart in order
            // to say that a band is available:
            if (powerdistList.find(*it) != powerdistList.end() && wcaList.find(
                    *it) != wcaList.end()) {
                availableBandsList_m.insert(*it);
            }
        }

    } catch(const ControlDeviceExceptions::IllegalConfigurationExImpl& ex) {
        ControlDeviceExceptions::IllegalConfigurationExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalConfigurationEx();
    }
}

void FrontEndImpl::controllerNotBandOperational()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    for (std::map<std::string, Control::ControlDevice_ptr>::iterator iter(
            subdevice_m.begin()); iter != subdevice_m.end(); ++iter) {
        if (FrontEndUtils::isBandComponent((*iter).first))
            continue;

        if (CORBA::is_nil((*iter).second) == true) {
            // Add this device to the degraded list and go to degraded state.
            setSubdeviceError((*iter).first.c_str());
        } else {
            setDeviceOperational(iter);
        }
    }

}

bool FrontEndImpl::isCartrigeEnabled(const std::map<std::string, Control::ControlDevice_ptr>::iterator &subdeviceIterator)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return false;
    bool retVal = true;
    int enabled_flag = -1;
    ACS::Time timestamp;

    try {
            Control::PowerDist_ptr powerdist =
                    getSubdeviceReference<Control::PowerDist> (
                            (*subdeviceIterator).first);

        enabled_flag = powerdist->GET_CARTRIDGE_ENABLE(timestamp);
        if(enabled_flag != 1){
            retVal=false;
        }

    } catch(const ControlExceptions::CAMBErrorEx& ex) {
        ControlExceptions::CAMBErrorExImpl newEx(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        string msg =
                "CAMB error when reading state information from the power distributor.";
        newEx.addData("Detail", msg);
        newEx.log();

        setSubdeviceError((*subdeviceIterator).first.c_str());
        retVal=false;

    } catch(const ControlExceptions::INACTErrorEx& ex) {
        INACTErrorExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "State error when accessing power distributor.";
        newEx.addData("Detail", msg);
        newEx.log();
        setSubdeviceError((*subdeviceIterator).first.c_str());
        retVal=false;

    }
    return retVal;
}

void FrontEndImpl::controllerBandOperational()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    set<ReceiverBandMod::ReceiverBand>::iterator it;
    for (it = availableBandsList_m.begin(); it != availableBandsList_m.end(); it++) {

        string componentName(FrontEndUtils::getNameWithInt(FrontEndUtils::POWERDIST_STRING, *it));

        std::map<std::string, Control::ControlDevice_ptr>::iterator
                subdeviceIterator;

        subdeviceIterator = subdevice_m.find(componentName);
        Control::HardwareControllerImpl::setDeviceOperational(subdeviceIterator);

        if (!isCartrigeEnabled(subdeviceIterator)) {
            continue;
        }

        try {
            powerUpBandInternal(*it);
        }catch(...){
            ostringstream message;
            message << "Receiver band #" << FrontEndUtils::bandToInt(*it) << " was NOT powered on.";
            LOG_TO_AUDIENCE(LM_WARNING, __PRETTY_FUNCTION__, message.str(), "Operator");
            continue;
        }
    }

    try {
        powerUpBandInternal(ReceiverBandMod::ALMA_RB_03);
    } catch(...){
        ostringstream message;
        message << "Receiver band #" << FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_03) << " was NOT powered on.";
        LOG_TO_AUDIENCE(LM_WARNING, __PRETTY_FUNCTION__, message.str(), "Operator");
    }
}

void FrontEndImpl::controllerOperational()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    // Protect the controller lifecycle methods from being called synchronous.
    ACE_Guard < ACE_Mutex > guard(controllerLifecycleMutex);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    controllerNotBandOperational();
    controllerBandOperational();
    Control::HardwareControllerImpl::setParentError();
}

void FrontEndImpl::enableCartrige(ReceiverBandMod::ReceiverBand  band)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    std::string powerdistName = FrontEndUtils::getPowerDistName(band);
    Control::PowerDist_ptr powerdist =
            getSubdeviceReference<Control::PowerDist> (powerdistName);
    powerdist->SET_CARTRIDGE_ON();
    sleep(1); //wait for a sec until the hardware is powered on
}

void FrontEndImpl::disableCartrige(ReceiverBandMod::ReceiverBand band)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    std::string powerdistName = FrontEndUtils::getPowerDistName(band);
    Control::PowerDist_ptr powerdist =
            getSubdeviceReference<Control::PowerDist> (powerdistName);
    powerdist->SET_CARTRIDGE_OFF();
    sleep(1); //wait for a sec until the hardware is powered off
}

void FrontEndImpl::powerUpBandInternal(ReceiverBandMod::ReceiverBand band)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    if (poweredBandsList_m.find(band) != poweredBandsList_m.end()) {
        return;
    }

    if (poweredBandsList_m.size() >= MAX_POWERED_BANDS) {
        FrontEndExceptions::PowerUpLimitReachedExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        stringstream msg;
        msg<<"Only " <<MAX_POWERED_BANDS
           << " bands can be powered up at the same time";
        ex.addData("Detail", msg.str());
        throw ex;
    }

    if (availableBandsList_m.find(band) == availableBandsList_m.end()) {
        FrontEndExceptions::BandNotAvailableExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        stringstream msg;
        msg << "Band "<< FrontEndUtils::bandToInt(band) <<" is not available";
        ex.addData("Detail", msg.str());
        throw ex;
    }

    enableCartrige(band);

    std::map<std::string, Control::ControlDevice_ptr>::iterator
            subdeviceIterator;

    std::string componentName(FrontEndUtils::getColdCartName(band));
    subdeviceIterator = subdevice_m.find(componentName);
    Control::HardwareControllerImpl::setDeviceOperational(subdeviceIterator);

    componentName = FrontEndUtils::getWCAName(band);
    subdeviceIterator = subdevice_m.find(componentName);
    Control::HardwareControllerImpl::setDeviceOperational(subdeviceIterator);

    poweredBandsList_m.insert(band);

    ostringstream message;
    message << "Receiver band #" << FrontEndUtils::bandToInt(band) << " powered on.";
    LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, message.str(), "Operator");
}

void FrontEndImpl::powerOffBandInternal(ReceiverBandMod::ReceiverBand band)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    if (cleanUpCalled == true)
        return;
    if (poweredBandsList_m.find(band) == poweredBandsList_m.end()) {
        return;
    }

    Control::WCA_ptr wca = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(band));
    wca->hwStop();

    Control::ColdCart_ptr coldcart = getSubdeviceReference<Control::ColdCart> (
            FrontEndUtils::getColdCartName(band));
    coldcart->hwStop();

    disableCartrige(band);

    poweredBandsList_m.erase(band);

    if (band == currentBand_m) {
        currentBand_m = ReceiverBandMod::UNSPECIFIED;
    }

    ostringstream message;
    message << "Receiver band #" << FrontEndUtils::bandToInt(band) << " powered off";
    LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, message.str(), "Operator");

}

void FrontEndImpl::powerOffAllBandsInternal()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
    // Make a snapshot of the currently powered bands to be powered off
    set<ReceiverBandMod::ReceiverBand> powered_bands = poweredBandsList_m;
    set<ReceiverBandMod::ReceiverBand>::iterator it;


    for (it = powered_bands.begin(); it != powered_bands.end(); it++) {
        try {
            powerOffBandInternal(*it);
        } catch(...) {

        }
    }
}

Control::FrontEnd::ReceiverBandSeq* FrontEndImpl::getPoweredBandsInternal()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    // Convert list into CORBA sequence
    Control::FrontEnd::ReceiverBandSeq_var powered_bands =
            new Control::FrontEnd::ReceiverBandSeq();
    try {
        throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        powered_bands->length(poweredBandsList_m.size());
        set<ReceiverBandMod::ReceiverBand>::iterator it;
        unsigned int index = 0;
        for (it = poweredBandsList_m.begin(); it != poweredBandsList_m.end(); it++, index++) {
            powered_bands[index] = *it;
        }
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        powered_bands->length(0);
    }
    return powered_bands._retn();
}

void FrontEndImpl::mixerDefluxInternal(ReceiverBandMod::ReceiverBand band,
        float magnetCurrentPulseLenght, float Imax, float heatherPulseLenght)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    if (band != ReceiverBandMod::ALMA_RB_07 &&
        band != ReceiverBandMod::ALMA_RB_09) {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Additional",
                "mixerDeflux may only be executed on bands 7 and 9");
        throw ex;
    }

    powerUpBandInternal(band);

    if (band == ReceiverBandMod::ALMA_RB_09)
        setLO1FrequencyAction(band, 691.473076e9, NetSidebandMod::USB, false);
    else
        setLO1FrequencyAction(band, 345.795990e9, NetSidebandMod::LSB, false);

    Control::WCA_ptr wca_ptr =
            getSubdeviceReference<Control::WCA>(FrontEndUtils::getWCAName(band));

    wca_ptr->SetLOPowerAmps(false);

    Control::ColdCart_ptr cc_ptr =
            getSubdeviceReference<Control::ColdCart>(FrontEndUtils::getColdCartName(band));

    cc_ptr->mixerDeflux(magnetCurrentPulseLenght, Imax, heatherPulseLenght);
}

double FrontEndImpl::getLO1FrequencyInternal()
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    throwIfNoBandSelected(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(currentBand_m));
    return wca_ptr->GetFrequency();
}

double FrontEndImpl::getStandbyLO1FrequencyInternal(
        ReceiverBandMod::ReceiverBand band)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    throwIfBandNotPowered(band, __FILE__, __LINE__, __PRETTY_FUNCTION__);

    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(band));
    return wca_ptr->GetFrequency();
}


void FrontEndImpl::selectBandInternal(ReceiverBand band)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    if (poweredBandsList_m.find(band) == poweredBandsList_m.end()) {
        FrontEndExceptions::BandNotPoweredExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        stringstream msg;
        msg << "Band #" << FrontEndUtils::bandToInt(band)
                << " is not powered up";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }

    Control::IFSwitch_ptr ifswitch_ptr = getSubdeviceReference<
            Control::IFSwitch> (FrontEndUtils::IFSWITCH_STRING);

    ifswitch_ptr->selectBand(band);

    Control::LPR_ptr lpr_ptr = getSubdeviceReference<Control::LPR> (
            FrontEndUtils::LPR_STRING);

    lpr_ptr->selectBand(band);

    //Inform the current WCA that it is being unselected so it stops certain alarms.
    if (currentBand_m != ReceiverBandMod::UNSPECIFIED) {
        Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
                FrontEndUtils::getWCAName(currentBand_m));
        wca_ptr->setSelected(false);
    }

    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(band));
    wca_ptr->setSelected(true);

    currentBand_m = band;

    ostringstream message;
    message << "Receiver band  #" << FrontEndUtils::bandToInt(band)
            << " has been selected";
    LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, message.str(), "Operator");
}



bool FrontEndImpl::isLockedInternal()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    throwIfNoBandSelected(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(currentBand_m));
    return wca_ptr->IsLocked();
}

void FrontEndImpl::relockInternal()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    throwIfNoBandSelected(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(currentBand_m));

    wca_ptr->ReLock();
}

NetSidebandMod::NetSideband FrontEndImpl::getSidebandInternal()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    throwIfNoBandSelected(__FILE__, __LINE__, __PRETTY_FUNCTION__);


    long sideband = -1;
    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(currentBand_m));

    ACS::Time timestamp;
    sideband = wca_ptr->GET_LO_PLL_SB_LOCK_POLARITY_SELECT(timestamp);

    if (sideband == 0) {
        return NetSidebandMod::LSB;
    } else if (sideband == 1) {
        return NetSidebandMod::USB;
    } else {
        FrontEndExceptions::UnknownFailureExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        string msg = "WCA returned incorrect sideband value";
        ex.addData("Detail", msg);
        throw ex;
    }
}
void FrontEndImpl::setLO1FrequencyAction(ReceiverBand band,
        CORBA::Double frontEndFreq, NetSidebandMod::NetSideband sideband,
        bool lockFlag)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ACS::Time timestamp;
    try {
        Control::WCA_ptr wca_ptr =
                getSubdeviceReference<Control::WCA>(FrontEndUtils::getWCAName(band));

        Control::ColdCart_ptr coldcart_ptr =
                getSubdeviceReference<Control::ColdCart>(FrontEndUtils::getColdCartName(band));

        Control::LPR_ptr lpr_ptr =
                        getSubdeviceReference<Control::LPR>(FrontEndUtils::LPR_STRING);

        switch (sideband) {
        case NetSidebandMod::USB:
            wca_ptr->SET_LO_PLL_SB_LOCK_POLARITY_UPPER();
            break;
        case NetSidebandMod::LSB:
            wca_ptr->SET_LO_PLL_SB_LOCK_POLARITY_LOWER();
            break;
        default:
            IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            ex.addData("Detail",
                    "Suplied sideband invalid. not NetSidebandMod::USB nor NetSidebandMod::LSB");
            ex.log();
            throw ex;
        }

        wca_ptr->SetFrequency(frontEndFreq, false);

        wca_ptr->SetLOPowerAmps(true);
        //Setting the frontEndFreq internally in the ColdCart Hardware Controller
        //The frequency is needed to choose the proper setting for the LNA bias
        //, SIS bias and Sis magnet.
        coldcart_ptr->setConfigFreq(frontEndFreq);
        coldcart_ptr->setBandLNABias();
        coldcart_ptr->setBandEnableLNABias(true);
        coldcart_ptr->setBandSISBias(true);
        coldcart_ptr->setBandSISMagnet(true);
        double targetPhotomixerCurrent = 0.0;
        if (lockFlag) {
            wca_ptr->SET_LO_PLL_NULL_LOOP_INTEGRATOR_NULL();
            if (enableLPRTunning_m)
                lpr_ptr->SET_MODULATION_INPUT_VALUE(0.00);
            try {
                // trying to read the target photomixer current from the configuration file
                targetPhotomixerCurrent
                        = wca_ptr->getConfigTargetPhotoMixerCurrent();
            } catch(const ControlExceptions::XmlParserErrorEx &ex) {
                // Found problems reading the target photomixer current
                // but we will try to lock with to default value (0.6mA)
                targetPhotomixerCurrent = 0.6E-3;
                ACS_LOG(
                        LM_SOURCE_INFO,
                        __PRETTY_FUNCTION__,
                        (LM_WARNING, "There was a problem reading the target photomixer current"
                            " from the configuration file. Using the default: %lf mA.", targetPhotomixerCurrent));
            }
            tuneLPRPhotomixer(targetPhotomixerCurrent);//Amps
            //debugdata
            float mv = lpr_ptr->GET_MODULATION_INPUT_VALUE(timestamp);
            ostringstream msg;
            msg << "Setting LPR EDFA_V to 0.0. Previous value: " << mv;

            ACS_LOG(
                    LM_SOURCE_INFO,
                    __PRETTY_FUNCTION__,
                    (LM_INFO, "Attempting lock with EDFA value %lf, and target PM current %lf", lpr_ptr->GET_MODULATION_INPUT_VALUE(
                            timestamp), targetPhotomixerCurrent));
            usleep(200000);//200ms

            try {
                wca_ptr->SetFrequency(frontEndFreq, true);
            } catch(const WCAExceptions::InsufficientPowerEx& ex) {
                WCAExceptions::InsufficientPowerExImpl nex(ex, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);

                nex.addData("Additional", msg.str());
                nex.log();
                lpr_ptr->SET_MODULATION_INPUT_VALUE(0.00);
                throw nex;
            } catch(const WCAExceptions::LockFailedEx& ex) {
                WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
                nex.addData("Additional", msg.str());
                nex.log();
                lpr_ptr->SET_MODULATION_INPUT_VALUE(0.00);
                throw nex;
            } catch(const WCAExceptions::LockLostEx& ex) {
                WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
                nex.addData("Additional", msg.str());
                nex.log();
                lpr_ptr->SET_MODULATION_INPUT_VALUE(0.00);
                throw nex;
            } catch(const WCAExceptions::AdjustPllFailedEx& ex) {
                WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
                nex.addData("Additional", msg.str());
                nex.log();
                lpr_ptr->SET_MODULATION_INPUT_VALUE(0.00);
                throw nex;
            }
            tuneLPRPIFTotalpower(2.5);//Volts
            ACS_LOG(
                    LM_SOURCE_INFO,
                    __PRETTY_FUNCTION__,
                    (LM_INFO, "Optimized EDFA value %lf, for a target voltage of 2.5V", lpr_ptr->GET_MODULATION_INPUT_VALUE(
                            timestamp)));
            if (wca_ptr->IsLocked() == false) {
                WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
                ex.addData("Detail",
                        "Lost lock after tuneLPRPIFTotalpower(2.5)");
                ex.log();
                throw ex;
            }

            wca_ptr->SET_LO_PLL_NULL_LOOP_INTEGRATOR_NULL();
            wca_ptr->SET_LO_PLL_NULL_LOOP_INTEGRATOR_OPERATE();
            if (wca_ptr->IsLocked() == false) {
                WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
                ex.addData("Detail", "Lock appear to have been a false lock");
                ex.log();
                throw ex;
            }
            setCache(lpr_ptr->GET_MODULATION_INPUT_VALUE(timestamp),
                    wca_ptr->GET_LO_YTO_COARSE_TUNE(timestamp));
            try {
                optimizeLOAmpInternal(band);
                //FIXME: Clear the alarm
            } catch(const FrontEndExceptions::OptimizationFailedExImpl &ex) {
                FrontEndExceptions::OptimizationFailedExImpl nex(ex, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                nex.addData("Detail",
                        "Failed to optimize LO amp while setting Frequency.");
                nex.log(LM_WARNING);
                //FIXME: Raise an alarm.
            }
        }

    } catch(const ControlExceptions::CAMBErrorEx& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const ControlExceptions::INACTErrorEx& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const ControlExceptions::IllegalParameterErrorEx& ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::InsufficientPowerEx& ex) {
        WCAExceptions::InsufficientPowerExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::LockFailedEx& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::LockLostEx& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::AdjustPllFailedEx& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }

}

void FrontEndImpl::setLO1FrequencyFromCache(ReceiverBand band,
        CORBA::Double frontEndFreq, NetSidebandMod::NetSideband sideband,
        bool lockFlag)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    try {
        Control::WCA_ptr wca_ptr =
                getSubdeviceReference<Control::WCA>(FrontEndUtils::getWCAName(band));

        Control::ColdCart_ptr coldcart_ptr =
                getSubdeviceReference<Control::ColdCart>(FrontEndUtils::getColdCartName(band));

        Control::LPR_ptr lpr_ptr =
                        getSubdeviceReference<Control::LPR>(FrontEndUtils::LPR_STRING);

        //SET SB from cache
        //disable the PLL
        //reset LPR modulation value to safe level 0V TBC
        //switch to band required (if necessary)
        //set DYTO coarse tune to cached value
        //set LPR modulation value to cached value
        //set LO drain voltage to cached value
        //set FE biases according to LUT (section 10.8 & 10.9, RD-01)
        //wait 200ms (TBC)
        //verify that the photonic reference frequency is set correctly
        //check for lock, including check whether IF power is within range
        //(-1.0 to -3.5 V) if lock not acquired abort and raise exception
        //optimize coarse tune to bring correction voltage to target value
        //within tolerance (in case there has been drift) (section 10.5 RD-01)
        //we are done!

        std::vector<double> cache = getCache();
        wca_ptr->SET_LO_PLL_NULL_LOOP_INTEGRATOR_NULL();
        lpr_ptr->SET_MODULATION_INPUT_VALUE(0.00);
        wca_ptr->SET_LO_YTO_COARSE_TUNE(int(cache[0]));
        lpr_ptr->SET_MODULATION_INPUT_VALUE(cache[1]);
        try {
            optimizeLOAmpInternal(band);
            //FIXME: Clear the alarm
        } catch(const FrontEndExceptions::OptimizationFailedExImpl &ex) {
            FrontEndExceptions::OptimizationFailedExImpl nex(ex, __FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail",
                    "Failed to optimize LO amp while setting Frequency.");
            nex.log(LM_WARNING);
            //FIXME: Raise an alarm.
        }

        coldcart_ptr->setConfigFreq(frontEndFreq);
        coldcart_ptr->setBandLNABias();
        coldcart_ptr->setBandEnableLNABias(true);
        coldcart_ptr->setBandSISBias(true);
        coldcart_ptr->setBandSISMagnet(true);
        usleep(200000);//200ms
        wca_ptr->SET_LO_PLL_NULL_LOOP_INTEGRATOR_NULL();
        wca_ptr->SET_LO_PLL_NULL_LOOP_INTEGRATOR_OPERATE();
        if (wca_ptr->IsLocked() == false) {
            WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            ex.addData("Detail", "Lock appear to have been a false lock");
            ex.log();
            throw ex;
        }
        //check  IF power is within range (-1.0 to -3.5 V)
        float ift = fabs(wca_ptr->GET_LO_PLL_IF_TOTAL_POWER(timestamp));
        if (ift < 1.0 || ift > 3.5) {
            FrontEndExceptions::OptimizationFailedExImpl nex(__FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", "WCA IF total power not at optimal range.");
            nex.log(LM_WARNING);
        }
        wca_ptr->AdjustPll(0.0);

        setCache(lpr_ptr->GET_MODULATION_INPUT_VALUE(timestamp),
                wca_ptr->GET_LO_YTO_COARSE_TUNE(timestamp));

    } catch(const ControlExceptions::CAMBErrorEx& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const ControlExceptions::INACTErrorEx& ex) {
        INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const ControlExceptions::IllegalParameterErrorEx& ex) {
        IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::InsufficientPowerEx& ex) {
        WCAExceptions::InsufficientPowerExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::LockFailedEx& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::LockLostEx& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(const WCAExceptions::AdjustPllFailedEx& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }

}

void FrontEndImpl::setCache(float EDFA_V, int coarseYTO)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<double> vec;
    vec.push_back(coarseYTO);
    vec.push_back(EDFA_V);
    lockCache_m[(int(nominalFrequency_m / 1E9))] = vec;
}

bool FrontEndImpl::isCacheHit()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (lockCache_m.find(int(nominalFrequency_m / 1E9)) != lockCache_m.end())
        return true;
    return false;
}

std::vector<double> FrontEndImpl::getCache()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::map<int, std::vector<double> >::iterator it;
    it = lockCache_m.find(int(nominalFrequency_m / 1E9));
    return it->second;
}

void FrontEndImpl::tuneLPRPIFTotalpower(float ift_input_target)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (!enableLPRTunning_m)
        return;
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    Control::WCA_ptr wca =
            getSubdeviceReference<Control::WCA>(FrontEndUtils::getWCAName(currentBand_m));


    Control::LPR_ptr lpr =
                    getSubdeviceReference<Control::LPR>(FrontEndUtils::LPR_STRING);

    ACS::Time timestamp;
    float pm_now = 0.0;
    float ift_last = 0.0;
    float ift_now = 0.0;
    float ift_tolerance = 0.5;
    float ift_target = 0.1;
    float edfa_last = 0.0;
    float edfa_now = 0.0;
    float edfa_new = 0.0;
    //Modified to 0.005 and 0.0001 according to Eric Breyton
    float edfa_step = 0.005;
    int maxiter = 80;

    pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));
    ift_last = ift_now = fabs(wca->GET_LO_PLL_IF_TOTAL_POWER(timestamp));
    edfa_last = edfa_now = lpr->GET_MODULATION_INPUT_VALUE(timestamp);

    if (pm_now > 2.5E-3) {
        //Emergency abort CURRENT to high
        lpr->SET_MODULATION_INPUT_VALUE(0.00);
        WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        stringstream msg;
        msg << "Emergency abort, photomixer current ";
        msg << setprecision(3) << pm_now * 1000;
        msg << "mA is to high. EDFA voltage was: ";
        msg << edfa_last << "Now its been set to 0.0.";
        ex.addData("Detail", msg.str().c_str());
        ex.log();
        throw ex;
    }

    ift_target = ift_input_target;
    // ift_tolerance = std::min(0.5, ift_target*0.1);
    ift_tolerance = 0.25;

    if (fabs(ift_now - ift_target) < ift_tolerance) {
        std::cout << "DBGOptmz: IF Total power already within tolerence"
                << ift_now << std::endl;
        return;
    }

    while ((ift_now - ift_target) * (ift_last - ift_target) > 0 && (maxiter--
            > 0)) {
        std::cout << "DBGOptmz: IFT Last:" << ift_last << std::endl;
        std::cout << "DBGOptmz: IFT Now:" << ift_now << std::endl;
        std::cout << "DBGOptmz: IFT Target:" << ift_target << std::endl;
        std::cout << "DBGOptmz: EDFA last:" << edfa_last << std::endl;
        std::cout << "DBGOptmz: EDFA now:" << edfa_now << std::endl;
        std::cout << "DBGOptmz: PM now:" << pm_now << std::endl;
        std::cout << "DBGOptmz: LPR photodetector current:"
                << lpr->GET_PHOTO_DETECT_CURRENT(timestamp) << std::endl;
        std::cout << "DBGOptmz: LPR photodetector power:"
                << lpr->GET_PHOTO_DETECT_POWER(timestamp) << std::endl;

        if (pm_now > 2.5E-3) {
            //Emergency abort CURRENT to high
            lpr->SET_MODULATION_INPUT_VALUE(0.00);
            WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            stringstream msg;
            msg << "Emergency abort, photomixer current ";
            msg << setprecision(3) << pm_now * 1000;
            msg << "mA is to high. EDFA voltage was: ";
            msg << edfa_last << "Now its been set to 0.0.";
            ex.addData("Detail", msg.str().c_str());
            ex.log();
            throw ex;
        }

        if (edfa_now > 1.105) // (1.5 - 0.3 + 0.005) -> changed to (1.4 - 0.3 + 0.005)
            edfa_step = std::min((float) 0.005, edfa_step);
        if (fabs(ift_now - ift_target) < ift_tolerance)
            edfa_step = 0.0001;
        edfa_last = edfa_now;
        if (ift_last < ift_target)
            edfa_new = edfa_now + edfa_step;
        if (ift_last > ift_target)
            edfa_new = edfa_now - edfa_step;
        if (edfa_new < 0.0) {
            std::cout << "DBGOptmz: interpolation yielded an edfa value of:"
                    << edfa_now << ". Reseting to 0.0";
            edfa_new = 0.0;
            lpr->SET_MODULATION_INPUT_VALUE(edfa_new);
            return;
        }
        ift_last = ift_now;
        edfa_last = edfa_now;
        lpr->SET_MODULATION_INPUT_VALUE(edfa_new);
        usleep(48000);

        pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));
        ift_now = fabs(wca->GET_LO_PLL_IF_TOTAL_POWER(timestamp));
        edfa_now = lpr->GET_MODULATION_INPUT_VALUE(timestamp);
    }

    std::cout << "DBGOptmz: FINAL IFT Last:" << ift_last << std::endl;
    std::cout << "DBGOptmz: FINAL IFT Now:" << ift_now << std::endl;
    std::cout << "DBGOptmz: FINAL IFT Target:" << ift_target << std::endl;
    std::cout << "DBGOptmz: FINAL EDFA last:" << edfa_last << std::endl;
    std::cout << "DBGOptmz: FINAL EDFA now:" << edfa_now << std::endl;
    std::cout << "DBGOptmz: FINAL PM now:" << pm_now << std::endl;

    if (maxiter != -1) {
        //Set V_EDFA_new = V_EDFA_last*((I_PM_new - I_PM_target)-(I_PM_last - I_PM_target)/(I_OM_now - I_PM_last)
        //interpolate
        //edfa_new = edfa_last * ((ift_now - ift_target) - (ift_last - ift_target))/(ift_now - ift_last);
        edfa_new = ((ift_now - ift_target) * edfa_last
                + (ift_target - ift_last) * edfa_now) / (ift_now - ift_last);
        std::cout << "DBGOptmz: Interpolated EDFA:" << edfa_new << std::endl;
        lpr->SET_MODULATION_INPUT_VALUE(edfa_new);
    } else {
        std::cout
                << "DBGOptmz: Warning: the target could not be reached before the maximum iteration count. We will keep EDFA="
                << edfa_new << std::endl;
    }

    pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));
    ift_now = fabs(wca->GET_LO_PLL_IF_TOTAL_POWER(timestamp));

    std::cout << "DBGOptmz: EDFA applied. PM Current:" << pm_now << std::endl;
    std::cout << "DBGOptmz: EDFA applied. IF total power:" << ift_now
            << std::endl;

    if (pm_now > 2.5E-3) {
        //Emergency abort CURRENT to high
        lpr->SET_MODULATION_INPUT_VALUE(0.00);
        WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        stringstream msg;
        msg << "Emergency abort, photomixer current ";
        msg << setprecision(3) << pm_now * 1000;
        msg << "mA is to high. EDFA voltage was: ";
        msg << edfa_last << "Now its been set to 0.0.";
        ex.addData("Detail", msg.str().c_str());
        ex.log();
        throw ex;
    }

    if (fabs(ift_now - ift_target) > ift_tolerance) {
        ACS_LOG(
                LM_SOURCE_INFO,
                __PRETTY_FUNCTION__,
                (LM_WARNING, "Target IF Total power %f V was not reached. Actual value is %f V", ift_target, ift_now));
    }
}

void FrontEndImpl::tuneLPRPhotomixer(float pm_input_target)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if (!enableLPRTunning_m)
        return;
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    if (pm_input_target > 2.5E-3 || pm_input_target < 0.1E-3) {
        //Emergency abort CURRENT to high
        WCAExceptions::LockFailedExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        stringstream msg;
        msg << "Target photomixer current out of spec:";
        msg << setprecision(3) << pm_input_target * 1000;
        msg << "mA. Limits [0.1 2.5]";
        ex.addData("Detail", msg.str().c_str());
        ex.log();
        throw ex;
    }

    Control::WCA_ptr wca =
            getSubdeviceReference<Control::WCA>(FrontEndUtils::getWCAName(currentBand_m));


    Control::LPR_ptr lpr =
                    getSubdeviceReference<Control::LPR>(FrontEndUtils::LPR_STRING);


    ACS::Time timestamp;
    float pm_last = 0.0;
    float pm_now = 0.0;
    float pm_tolerance = 0.5;
    float pm_target = 0.1;
    float edfa_last = 0.0;
    float edfa_now = 0.0;
    float edfa_new = 0.0; // real initialization is done later
    float edfa_step = 0.3;
    int maxiter = 40;
    int interpolIter;
    //Empirical offset for the target pm. If you want target, you must adjust target + offset
    //The current will drift by this much at least.. so we over shoot a little.
    float empOffset = 0.005E-3;

    pm_last = pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));
    edfa_last = edfa_now = lpr->GET_MODULATION_INPUT_VALUE(timestamp);

    if (pm_last > 2.5E-3) {
        //Emergency abort CURRENT to high
        lpr->SET_MODULATION_INPUT_VALUE(0.00);
        WCAExceptions::LockFailedExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        stringstream msg;
        msg << "Emergency abort, photomixer current ";
        msg << setprecision(3) << pm_last*1000;
        msg <<"mA is to high. EDFA voltage was: ";
        msg <<pm_last<<"Now its been set to 0.0.";
        ex.addData("Detail", msg.str().c_str());
        ex.log();
        throw ex;
    }

    //pm_target = wca->getTargetPM();
    pm_target = pm_input_target + empOffset;
    pm_tolerance = std::min(0.005E-3, pm_target*0.1);
    std::cout<<"DBGOptmz: PM Target (=requested target + drift offset(0.005E-3)) "<<pm_target<<" Tolerance: "<< pm_tolerance  <<std::endl;

   if(fabs(pm_now - pm_target) < pm_tolerance){
        std::cout<<"DBGOptmz: PM Current already within tolerance "<<pm_now<<std::endl;
        return;
    }

   std::cout<<"DBGOptmz: current PM value:"<<pm_last<<std::endl;
   std::cout<<"DBGOptmz: current EDFA value:"<<edfa_last<<std::endl;
   // Setting initial values
   //if (edfa_last < 0.1 ) // probably set to 0 after changing the band or loosing the lock
   //    edfa_new=1.0;     // 1.0V is a good value for starting in any band. We have to revisit this later.
   //else
   //    edfa_new=edfa_last;
   interpolIter=3;  // max number of interpolation iteration
   for(int iter=0;iter < maxiter ; iter++ ){
       // Setting edfa value and reading the results
       // 1.2 edfa low limit reached
       if (edfa_new < 0.0) {
           std::cout<<"DBGOptmz: interpolation yielded an edfa value of:"<<edfa_now<<". Reseting to 0.0" <<  std::endl;
           edfa_new = 0.0;
           ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                   (LM_WARNING,"EDFA LOW limit 0.0 reached. Breaking out of loop."));
           break;
       }
       // 1.2 edfa high limit reached
       if (edfa_new > 3.5) {
           std::cout<<"DBGOptmz: interpolation yielded an edfa value of:"<<edfa_now<<". Reseting to 3.5" <<  std::endl;
           edfa_new = 3.5;
           ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                   (LM_WARNING,"EDFA HIGH limit 3.5 reached. Breaking out of loop."));
           break;
       }

       lpr->SET_MODULATION_INPUT_VALUE(edfa_new);
       usleep(48000);//100ms
       pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));
       pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));//we are having jumps.
       edfa_now = lpr->GET_MODULATION_INPUT_VALUE(timestamp);

       // printing the results in the container log
       std::cout<<"DBGOptmz: Iter:"<<iter<<std::endl;
       std::cout<<"DBGOptmz: PM Now:"<<pm_now<<std::endl;
       std::cout<<"DBGOptmz: PM Target:"<<pm_target-empOffset<<std::endl;
       std::cout<<"DBGOptmz: EDFA last:"<<edfa_last<<std::endl;
       std::cout<<"DBGOptmz: EDFA now:"<<edfa_now<<std::endl;
       std::cout<<"DBGOptmz: LPR photodetector current:"<<lpr->GET_PHOTO_DETECT_CURRENT(timestamp)<<std::endl;
       std::cout<<"DBGOptmz: LPR photodetector power:"<<lpr->GET_PHOTO_DETECT_POWER(timestamp)<<std::endl;
       // Step 0, verify if we reached the target
       if(fabs(pm_now - pm_input_target) < pm_tolerance){
           std::cout<<"DBGOptmz: PM Current within tolerance: "<<pm_now<<std::endl;
           return;
       }

       // Now we start checking the conditions
       // 1 - First, we check for problems!
       // 1.1 Photomixer current too high
       if (pm_now > 2.5E-3) {
           //Emergency abort CURRENT to high
           lpr->SET_MODULATION_INPUT_VALUE(0.00);
           WCAExceptions::LockFailedExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
           stringstream msg;
           msg << "Emergency abort, photomixer current ";
           msg << setprecision(3) << pm_last*1000;
           msg <<"mA is to high. EDFA voltage was: ";
           msg <<edfa_last<<"Now its been set to 0.0.";
           ex.addData("Detail", msg.str().c_str());
           ex.log();
           throw ex;
       }

       // 2- Now we look for 'normal conditions'
       // 2.1 pm over or below the target (moving the edfa value without reach the PM target yet)
       if ( (pm_now - pm_target) * (pm_last - pm_target) > 0 )  {
           // In this case, we just move on step
           if (edfa_now > 1.2 ) // (1.5 - 0.3 + 0.1) --> changed to 1.4 - 0.3 + 0.1)
               edfa_step = std::min((float)0.1, edfa_step);
           edfa_last = edfa_now;
           // step up or down the edfa
           if (pm_now < pm_target) edfa_new = edfa_now + edfa_step;
           if (pm_now > pm_target) edfa_new = edfa_now - edfa_step;
           pm_last = pm_now;
           edfa_last = edfa_now;
         // and we continue to the next iteration
       } else {
           // 2.2 the target is between the last and the last value, therefore, we will interpolate to
           // try to get a better aproximation
               if (interpolIter) {  // unterpolating for just a limited number of iteration, in case is oscillating
                   interpolIter--;
                   edfa_new = ((pm_now - pm_target)*edfa_last + (pm_target - pm_last)*edfa_now)/(pm_now - pm_last);
                   if (( edfa_new < std::min(edfa_now, edfa_last)) || ( edfa_new > std::max(edfa_now, edfa_last))) {
                       // check that we're not extrapolating
                       std::cout<<"DBGOptmz: Warning: Trying to extrapolate! edfa_last:"<< edfa_last << ", edfa_now:"<<edfa_now<< ", edfa_new:" <<edfa_new <<  std::endl;
                       ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                           (LM_WARNING,"FE Algorithm tried to extrapolate a value. Using the old value and exiting"));
                       edfa_new=edfa_now;
                       break;
                   }

               }
            }
       }
       // at this point, we were not able to reach the target, therefore, we set the best value get got
       lpr->SET_MODULATION_INPUT_VALUE(edfa_new);

       pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));
       pm_now = fabs(wca->GET_LO_PHOTOMIXER_CURRENT(timestamp));//we are having jumps.
       edfa_now = lpr->GET_MODULATION_INPUT_VALUE(timestamp);
       if (pm_now > 2.5E-3) {
          //Emergency abort CURRENT to high
          lpr->SET_MODULATION_INPUT_VALUE(0.00);
          WCAExceptions::LockFailedExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
          stringstream msg;
          msg << "Emergency abort, photomixer current ";
          msg << setprecision(3) << pm_last*1000;
          msg <<"mA is to high. EDFA voltage was: ";
          msg <<edfa_last<<"Now its been set to 0.0.";
          ex.addData("Detail", msg.str().c_str());
          ex.log();
          throw ex;
       }
       std::cout<<"DBGOptmz: Target photomixe current not reached, using edfa:"<<edfa_now<<", pm current: "<<pm_now<<std::endl;
       ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
          (LM_WARNING,"Target photomixer current %f was not reached. Actual value is %f usinf edfa %f",
          pm_target, pm_now,edfa_now));

}



void FrontEndImpl::optimizeLOAmpInternal(ReceiverBand band)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__,__LINE__,__PRETTY_FUNCTION__);
    throwIfBandNotPowered(band, __FILE__,__LINE__,__PRETTY_FUNCTION__);
    bool cache = false;

    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(band));

    if (wca_ptr->isSimulated() == true)
        return;

    Control::ColdCart_ptr coldcart_ptr = getSubdeviceReference<
            Control::ColdCart> (FrontEndUtils::getColdCartName(band));

    if (coldcart_ptr->isSimulated() == true)
        return;

    //jreveco:
    //cache = wca_ptr->isPACacheHit();
    // disable cache since I suspect is causing the problems reported on AIV-3857
    // after engineering check, I will re-enable it or reimpleted it
    cache = false;
    if (cache == false) {
        //Cache miss
        //try to optimize. if it fails for any polarization no cache is
        //setup.
        float IJ[2] = { 0.0, 0.0 };
        float VD[2] = { 0.0, 0.0 };
        IJ[0] = coldcart_ptr->getTargetIJ1();
        IJ[1] = coldcart_ptr->getTargetIJ2();
        VD[0] = wca_ptr->getTargetVD1();
        VD[1] = wca_ptr->getTargetVD2();
        optimizeSinglePolInternal(band, 0, VD[0], IJ[0]);
        optimizeSinglePolInternal(band, 1, VD[1], IJ[1]);
        ACS_LOG(
                LM_SOURCE_INFO,
                __PRETTY_FUNCTION__,
                (LM_TRACE, "Setting LO_PA_POL0_DRAIN_VOLTAGE to VD1=%f, VD2=%f, IJ0=%f, IJ1=%f", VD[0], VD[1], IJ[0], IJ[1]));

        // cout << "Setting LO_PA_POL0_DRAIN_VOLTAGE to VD1=" << VD[0] <<", VD2="<<  VD[1] << endl;
        //VD1 = wca_ptr->GET_LO_PA_POL0_DRAIN_VOLTAGE(timestamp);
        //VD2 = wca_ptr->GET_LO_PA_POL1_DRAIN_VOLTAGE(timestamp);
        wca_ptr->setPACache(VD[0], VD[1]);
    } else {
        wca_ptr->setPAFromCache();
    }
}



void FrontEndImpl::optimizeSinglePolInternal(ReceiverBand band, int pol,
        float& VD, float& targetIJ1)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    throwIfBandNotPowered(band, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    // Constants governing the range of possible settings:
    //float VD = 2.5;//<--getFrom database.
    //float targetIJ1 = getConfigTagetIJ();
    //float targetIJ1 = 0.5; //hardcode.
    const float VDmax = 2.5; // V
    const float step = VDmax / 255;
    const float window = 5.0E-6; // A

    // Set a maximum iteration count to guard against pathological
    //inputs:
    const int iterLimit = 300;

    stringstream outputlog;
    outputlog << "OptimizeLOPowerAmp: pol=" << pol << fixed << setprecision(4)
            << " VD=" << VD << " step=" << step << setprecision(7)
            << " V targetIJ1=" << targetIJ1 << " +/- " << window << " A"
            << endl;
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_TRACE, outputlog.str().c_str()));
    outputlog.str("");

    // Define the window of IJ values near the target
    // It is an error if we cannot move IJ1 and IJ2 to within these limits:
    float loThreshold = fabsf(targetIJ1) - window;
    if (loThreshold < 0)
        loThreshold = 0;
    float hiThreshold = fabsf(targetIJ1) + window;

    // Initialize variables used within the loop:
    bool done = false;
    bool error = false;
    int iter = 0;
    bool foundMin = false; // true if a minimum has been seen
    float minErrorIJ1 = 999; // starting smallest errorIJ1 values seen
    float minErrorVD1 = 0; // VD setting where smallest errorIJ1 was seen
    float VJ1; // instantaneous VJ reading
    float IJ1; // instantaneous IJ reading
    float errorIJ1; // IJ error abs(targetIJ - IJ)
    int signErrorIJ1; // sign of the error
    ACS::Time timestamp;

    Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
            FrontEndUtils::getWCAName(band));

    Control::ColdCart_ptr coldcart_ptr = getSubdeviceReference<
            Control::ColdCart> (FrontEndUtils::getColdCartName(band));

    //If VD is zero, use what is set in the WCA
    if (VD == 0) {
        if (pol == 0) {
            VD = wca_ptr->GET_LO_PA_POL0_DRAIN_VOLTAGE(timestamp);
        } else {
            VD = wca_ptr->GET_LO_PA_POL1_DRAIN_VOLTAGE(timestamp);
        }
    }

    while (!done && !error) {
        // Check for maximum iterations or out of range:
        if (iter++ > iterLimit || VD < 0 || VD > VDmax) {
            outputlog << "Max iterations exceeded or VD out of range.iter="
                    << iter << fixed << setprecision(4) << " VD=" << VD
                    << " VJ1=" << VJ1 << setprecision(7) << " IJ1=" << IJ1
                    << endl;
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                    (LM_TRACE, outputlog.str().c_str()));
            outputlog.str("");

            error = true;
            continue;
        } else {
            // Set the new (or starting) VD and get the instantaneous VJ & IJ reading:
            if (pol == 0) {
                wca_ptr->SET_LO_PA_POL0_DRAIN_VOLTAGE(VD);
                usleep(48000);
                VJ1 = coldcart_ptr->GET_POL0_SB1_SIS_VOLTAGE(timestamp);
                IJ1 = fabsf(coldcart_ptr->GET_POL0_SB1_SIS_CURRENT(timestamp));
            } else {
                wca_ptr->SET_LO_PA_POL1_DRAIN_VOLTAGE(VD);
                usleep(48000);
                VJ1 = coldcart_ptr->GET_POL1_SB1_SIS_VOLTAGE(timestamp);
                IJ1 = fabsf(coldcart_ptr->GET_POL1_SB1_SIS_CURRENT(timestamp));
            }

            // If IJ is below the low threshold, step up:
            if (IJ1 < loThreshold) {
                VD += step;
                outputlog << fixed << "Stepping up. VD=" << setprecision(4)
                        << VD << " IJ1=" << setprecision(7) << IJ1 << endl;
                ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                        (LM_TRACE, outputlog.str().c_str()));
                outputlog.str("");

                // If it is above the high threshold step down:
            } else if (IJ1 > hiThreshold) {
                VD -= step;
                outputlog << fixed << "Stepping down. VD=" << setprecision(4)
                        << VD << " IJ1=" << setprecision(7) << IJ1 << endl;
                ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                        (LM_TRACE, outputlog.str().c_str()));
                outputlog.str("");
            } else {
                // We are within the window.
                outputlog << fixed << "Within window. VD=" << setprecision(4)
                        << VD << " IJ1=" << setprecision(7) << IJ1 << endl;
                ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                        (LM_TRACE, outputlog.str().c_str()));
                outputlog.str("");

                // Compute the IJ error and sign:
                // We pretend zero error is positive to simplify.
                errorIJ1 = targetIJ1 - IJ1;
                if (errorIJ1 >= 0)
                    signErrorIJ1 = 1;
                else {
                    signErrorIJ1 = -1;
                    errorIJ1 = -errorIJ1;
                }

                // Find the minimum error...

                // If the absolute error is smaller than the smallest seen,
                //  save the new smallest and the corresponding VD:
                if (!foundMin) {
                    if (errorIJ1 <= minErrorIJ1) {
                        minErrorIJ1 = errorIJ1;
                        minErrorVD1 = VD;
                        // Otherwise the error has started to increase; stop here:
                    } else {
                        VD = minErrorVD1;
                        outputlog << "Found LO PA voltage. VD=" << VD << endl;
                        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                                (LM_TRACE, outputlog.str().c_str()));
                        outputlog.str("");

                        done = foundMin = true;
                    }
                }
                if (!foundMin)
                    VD += signErrorIJ1 * step;
            }
        }
    }
    // If error disable the PA:
    if (error) {
        outputlog << "Failed searching for LO PA voltage." << endl;
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_TRACE, outputlog.str().c_str()));
        outputlog.str("");
        VD = 0;
    }

    // Set the final VD:
    if (pol == 0) {
        outputlog << "Setting LO_PA_POL0_DRAIN_VOLTAGE to " << VD << endl;
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_TRACE, outputlog.str().c_str()));
        outputlog.str("");
        wca_ptr->SET_LO_PA_POL0_DRAIN_VOLTAGE(VD);
        //paParams_m.set(freqLO_m, PowerAmpParams::VD0, VD);
    } else {
        outputlog << "Setting LO_PA_POL1_DRAIN_VOLTAGE to " << VD << endl;
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_TRACE, outputlog.str().c_str()));
        outputlog.str("");
        wca_ptr->SET_LO_PA_POL1_DRAIN_VOLTAGE(VD);
        //paParams_m.set(freqLO_m, PowerAmpParams::VD1, VD);
    }
    if (error) {
        FrontEndExceptions::OptimizationFailedExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        string msg = "Failed searching for LO PA voltage.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }
}

void FrontEndImpl::lockFrontEndInternal()
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__,__LINE__,__PRETTY_FUNCTION__);

    Control::FrontEnd::SubscanInformation currentSubScanInfo =
            getCurrentSubScanInfo();
    //TODO The following method is only temporary, since it duplicated information.
    //It should be changed to we directly ask the WCA.
    int coldMultiplier = getColdMultiplier(currentSubScanInfo.band);
    selectBandInternal(currentSubScanInfo.band);
    setCalibrationDeviceBandInternal(currentSubScanInfo.acdState,
            currentSubScanInfo.band); //Talk this with Ralph.
    //FIXME: Modify the WCA so it understands subscanInfo.
    NetSidebandMod::NetSideband sideband = NetSidebandMod::LSB;
    if (currentSubScanInfo.addFLOOG == true) {
        sideband = NetSidebandMod::USB;
    }

    bool lock = isLocked();

    if (lock == true &&  currentSubScanInfo.photoRefFreq * coldMultiplier == nominalFrequency_m) {
        ostringstream message;
        message << "Receiver band was already locked at " << std::scientific
                << nominalFrequency_m;
        LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, message.str(), "Operator");
        return;
    }

    nominalFrequency_m = currentSubScanInfo.photoRefFreq * coldMultiplier;

    if (isCacheHit() && useCache_m) {

        setLO1FrequencyFromCache(currentBand_m, nominalFrequency_m, sideband, true);

        ostringstream message;
        message << "FrontEnd band# " << FrontEndUtils::bandToInt(currentBand_m) << " locked at "
                << std::scientific << nominalFrequency_m;
        LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, message.str(),
                "Operator");
    } else {

        setLO1FrequencyAction(currentBand_m, nominalFrequency_m, sideband, true);

        ostringstream message;
        message << "FrontEnd band# " << FrontEndUtils::bandToInt(currentBand_m)<< " locked at "
                << std::scientific << nominalFrequency_m;
        LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, message.str(),
                "Operator");
    }

}

void FrontEndImpl::queueFrequenciesInternal(
        const Control::FrontEnd::SubscanInformationSeq &frequencies)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    SubScanValidator validator(poweredBandsList_m);
    std::vector<Control::FrontEnd::SubscanInformation>
            subscanInformationVectorTmp;
    for (unsigned int i = 0; i < frequencies.length(); i++) {
        validator.checkParameterTime(frequencies[i].startTime);
        validator.checkParameterBand(frequencies[i].band);
        validator.checkParameterFrequencyForBand(frequencies[i].photoRefFreq,
                frequencies[i].band);
        validator.checkParameterCalibrationDevice(frequencies[i].acdState);
        subscanInformationVectorTmp.push_back(frequencies[i]);
    }
    //Using a temp vector guarantees that we always have a valid vector,
    //Even if this method fails because we were fed rubbish.
    ACE_Guard<ACE_Mutex> guard(subscanInformationVectorMutex);
    subscanInformationVector.clear();
    subscanInformationVector = subscanInformationVectorTmp;
}

void FrontEndImpl::abortFrequencyChangesInternal()
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
    ACE_Guard < ACE_Mutex > guard(subscanInformationVectorMutex);
    std::vector<Control::FrontEnd::SubscanInformation>::iterator it;
    it = subscanInformationVector.begin();
    it++;
    subscanInformationVector.erase(it,subscanInformationVector.end());
}

ACS::TimeIntervalSeq* FrontEndImpl::timeToTuneInternal(
        const Control::FrontEnd::SubscanInformationSeq &frequencies)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    const unsigned int size(frequencies.length());

    bool isSimulated = false;

    ACS::TimeIntervalSeq_var ret(new ACS::TimeIntervalSeq);
    ret->length(size);
    SubScanValidator validator(poweredBandsList_m);

    for (unsigned int i = 0; i < size; i++) {

        validator.checkParameterBand(frequencies[i].band);
        validator.checkParameterFrequencyForBand(frequencies[i].photoRefFreq,
                frequencies[i].band);
        validator.checkParameterCalibrationDevice(frequencies[i].acdState);

        Control::WCA_ptr wca_ptr = getSubdeviceReference<Control::WCA> (
                FrontEndUtils::getWCAName(frequencies[i].band));

        isSimulated = wca_ptr->isSimulated();
        int coldmutiplier = FrontEndUtils::getColdMultiplier(
                frequencies[i].band);
        if (i == 0) {
            if (isSimulated == true) {
                if (nominalFrequency_m == coldmutiplier
                        * frequencies[i].photoRefFreq) {
                    ret[i] = static_cast<ACS::TimeInterval> (0.5
                            * TETimeUtil::ACS_ONE_SECOND);
                } else {
                    ret[i] = static_cast<ACS::TimeInterval> (3
                            * TETimeUtil::ACS_ONE_SECOND);
                }
            } else {
                //TODO: Check this case..for real hardware.
                ret[i] = static_cast<ACS::TimeInterval> (5
                        * TETimeUtil::ACS_ONE_SECOND);
            }
        } else {
            if (frequencies[i].photoRefFreq * FrontEndUtils::getColdMultiplier(
                    frequencies[i].band) != frequencies[i - 1].photoRefFreq
                    * FrontEndUtils::getColdMultiplier(frequencies[i].band)) {
                if (isSimulated == true) {
                    ret[i] = static_cast<ACS::TimeInterval> (3
                            * TETimeUtil::ACS_ONE_SECOND);

                } else {
                    ret[i] = static_cast<ACS::TimeInterval> (5
                            * TETimeUtil::ACS_ONE_SECOND);
                }
            } else {
                ret[i] = static_cast<ACS::TimeInterval> (1
                        * TETimeUtil::ACS_ONE_SECOND);
            }
        }

    }

    return ret._retn();
}

ACS::TimeIntervalSeq* FrontEndImpl::timeToTuneACDInternal(
        const Control::FrontEnd::SubscanInformationSeq &frequencies)
{
    ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
    throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    const unsigned int size(frequencies.length());

    Control::ACD_ptr acd_ptr =
              getSubdeviceReference<Control::ACD>(FrontEndUtils::ACD_STRING);

    Control::ACD::CalibrationDeviceSeq_var positions= new Control::ACD::CalibrationDeviceSeq();
    positions->length(size);

    for (unsigned int i = 0; i < size; i++) {
        positions[i] = frequencies[i].acdState;
    }

    return acd_ptr->timeToTune(positions);
}
/*
Control::FrontEnd::SubscanInformation FrontEndImpl::getCurrentSubScanInfo()
{
    ACS::Time now = ::getTimeStamp();
    std::vector<Control::FrontEnd::SubscanInformation>::iterator it;
    std::vector<Control::FrontEnd::SubscanInformation>::iterator itnext;
    if (subscanInformationVector.begin() == subscanInformationVector.end()) {
        ControlExceptions::IllegalParameterErrorExImpl ex( __FILE__, __LINE__,
        __PRETTY_FUNCTION__);
        ex.addData("Additional", "Queue vector is empty");
        throw ex;
    }

    Control::FrontEnd::SubscanInformation returnSubCanInformation = subscanInformationVector.front();

    for (it = subscanInformationVector.begin(); it
            < subscanInformationVector.end(); it++) {
        itnext = it;
        itnext++;

        returnSubCanInformation = *it;

        if (itnext == subscanInformationVector.end()) {
            break;
        }

        if (now < (*it).startTime && (*it).startTime!=0) {
            //TODO Throw an Illegal parameter exception.

        } else if (now >= (*it).startTime && now < (*itnext).startTime) {
            returnSubCanInformation = *it;
            break;
        } else if (now >= (*itnext).startTime) {
            subscanInformationVector.erase(it);
        }
    }
    return returnSubCanInformation;
}*/

    Control::FrontEnd::SubscanInformation FrontEndImpl::getCurrentSubScanInfo()
    {
        //VERY SIMPLE and naive FIXME.
        Control::FrontEnd::SubscanInformation returnSubCanInformation;
        ACE_Read_Guard< ACE_RW_Mutex > guardCleanUp(cleanUpMutex_m);
        throwIfInShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ACE_Guard < ACE_Mutex > guard(subscanInformationVectorMutex);


        cout<<"Vector Size: " << subscanInformationVector.size();
        if (subscanInformationVector.size() == 0) {
            //Exception.
        }

        returnSubCanInformation = subscanInformationVector.front();

        if (subscanInformationVector.size() > 1) {
            subscanInformationVector.erase(subscanInformationVector.begin());
        }


              std::cout<<std::endl
                <<"Starttime: "<<returnSubCanInformation.startTime<<std::endl
                <<"Band: "<<returnSubCanInformation.band<<std::endl
                <<"Freq: "<<returnSubCanInformation.photoRefFreq<<std::endl
                <<"Caldevice "<<returnSubCanInformation.acdState<<std::endl;
        return returnSubCanInformation;
    }




void FrontEndImpl::throwIfInShutdown(const char *file, const int line, const char *function)
{
    if (getState() == Control::HardwareController::Shutdown) {
        ControlExceptions::INACTErrorExImpl ex(file, line, function);
        std::string msg = "The FrontEnd is in Shutdown. No operations allowed.";
        ex.addData("Additional", msg);
        throw ex;
    }
}

void FrontEndImpl::throwIfNoBandSelected(const char *file, const int line, const char *function)
{
    if (currentBand_m == ReceiverBandMod::UNSPECIFIED) {
        ControlExceptions::IllegalParameterErrorExImpl ex(file, line, function);
        std::string msg = "The FrontEnd has no selected receiver band.  Select a band and retry the operation.";
        ex.addData("Additional", msg);
        throw ex;
    }
}

void FrontEndImpl::throwIfBandNotPowered(ReceiverBandMod::ReceiverBand band, const char *file, const int line, const char *function)
{
    if (poweredBandsList_m.find(band) == poweredBandsList_m.end()) {
        FrontEndExceptions::BandNotPoweredExImpl ex(file, line, function);
        std::stringstream msg ;
        msg<<"Band "<<FrontEndUtils::bandToInt(band)<<" is not powered.";
        ex.addData("Additional", msg.str());
        throw ex;
    }
}
/* --------------- [ MACI DLL support functions ] -----------------*/
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(FrontEndImpl)
/* ----------------------------------------------------------------*/
