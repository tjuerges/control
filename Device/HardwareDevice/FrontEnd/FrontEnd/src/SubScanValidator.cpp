/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FrontEndImpl.cpp
 *
 * $Id$
 */

#include "SubScanValidator.h"
#include "FrontEndUtils.h"

#include <ControlExceptions.h>

using namespace ReceiverBandMod;
using namespace CalibrationDeviceMod;

using ControlExceptions::IllegalParameterErrorExImpl;
using std::vector;
using std::stringstream;

SubScanValidator::SubScanValidator(
        set<ReceiverBandMod::ReceiverBand> poweredBandsIn)
{
    poweredBands = poweredBandsIn;
}

void SubScanValidator::checkParameterTime(const ACS::Time startTime)
{
    ACS::Time now = ::getTimeStamp();
    if (now > startTime) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        stringstream msg;
        msg << "Subscan start time is in the past: Time: " << startTime;
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }
}

void SubScanValidator::checkParameterBand(
        const ReceiverBandMod::ReceiverBand band)
{
    int bandNumber = -1;
        bandNumber = FrontEndUtils::bandToInt(band);


    if (poweredBands.find(band) != poweredBands.end()) {
        return;
    }

    IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    stringstream msg;
    msg << "FrontEnd Band:" << bandNumber
            << " is not powered up, hence not available.";
    ex.addData("Detail", msg.str());
    ex.log();
    throw ex;
}

void SubScanValidator::checkParameterFrequencyForBand(
        const double photoRefFreq, const ReceiverBandMod::ReceiverBand band)
{
    //TODO This will remain a stub until I sort out the component reference handling
    //in the Front End Controller
}

void SubScanValidator::checkParameterCalibrationDevice(
        const CalibrationDeviceMod::CalibrationDevice acdState)
{
    switch (acdState) {
    case AMBIENT_LOAD:
        break;
    case HOT_LOAD:
        break;
    case NONE:
        break;
    case SOLAR_FILTER:
        break;
    case QUARTER_WAVE_PLATE:
    case COLD_LOAD:
    case NOISE_TUBE_LOAD:
    default:
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        stringstream msg;
        msg << "The requested calibration device is not supported. Mode:"
                << acdState;
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }
}

