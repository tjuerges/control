/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2010
 * (c) Associated Universities Inc., 2010
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FrontEndImpl.cpp
 *
 * $Id$
 */

#include <FrontEndImpl.h>
#include <FrontEndUtils.h>
#include <SubScanValidator.h>
#include<TETimeUtil.h>

void FrontEndImpl::setLO1Frequency(CORBA::Double frontEndFreq,
        NetSidebandMod::NetSideband sideband)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try {
            throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
            setLO1FrequencyAction(currentBand_m, frontEndFreq, sideband, true);

        } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
            ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getIllegalParameterErrorEx();
        } catch(const ControlExceptions::InvalidRequestExImpl &ex) {
            ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getInvalidRequestEx();
        } catch(const ControlExceptions::CAMBErrorExImpl &ex) {
            ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getCAMBErrorEx();
        } catch(const ControlExceptions::INACTErrorExImpl &ex) {
            ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            throw nex.getINACTErrorEx();
        } catch(const WCAExceptions::InsufficientPowerExImpl &ex) {
            ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getINACTErrorEx();
        } catch(const WCAExceptions::LockFailedExImpl &ex) {
            WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getLockFailedEx();
        } catch(const FrontEndExceptions::BandNotSupportedExImpl &ex) {
            FrontEndExceptions::BandNotSupportedExImpl nex(ex, __FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getBandNotSupportedEx();
        } catch(const FrontEndExceptions::OptimizationFailedExImpl &ex) {
            FrontEndExceptions::OptimizationFailedExImpl nex(ex, __FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
            nex.log();
            throw nex.getOptimizationFailedEx();
        } catch(...) {
            ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
            ex.addData("Additional", "An UNKOWN Exception was caught.");
            ex.log();
            throw ex.getINACTErrorEx();
        }



}

void FrontEndImpl::setStandbyLO1Frequency(ReceiverBandMod::ReceiverBand band,
        CORBA::Double frontEndFreq, NetSidebandMod::NetSideband sideband)
{

    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        setLO1FrequencyAction(band, frontEndFreq, sideband, false);

    } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(const ControlExceptions::InvalidRequestExImpl &ex) {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getInvalidRequestEx();
    } catch(const ControlExceptions::CAMBErrorExImpl &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const WCAExceptions::InsufficientPowerExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const WCAExceptions::LockFailedExImpl &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const FrontEndExceptions::BandNotPoweredExImpl &ex) {
        FrontEndExceptions::BandNotPoweredExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getBandNotPoweredEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }

}

double FrontEndImpl::getLO1Frequency()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        return getLO1FrequencyInternal();

    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    }catch (const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const FrontEndExceptions::NoBandSelectedExImpl &ex) {
        FrontEndExceptions::NoBandSelectedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getNoBandSelectedEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

double FrontEndImpl::getStandbyLO1Frequency(ReceiverBandMod::ReceiverBand band)
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        return getStandbyLO1FrequencyInternal(band);

    }catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    }catch (const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const FrontEndExceptions::BandNotPoweredExImpl &ex) {
        FrontEndExceptions::BandNotPoweredExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getBandNotPoweredEx();
    }  catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }

}

bool FrontEndImpl::isLocked()
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        return isLockedInternal();

    } catch (const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const FrontEndExceptions::NoBandSelectedExImpl &ex) {
        FrontEndExceptions::NoBandSelectedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getNoBandSelectedEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

void FrontEndImpl::relock()
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        relockInternal();

    } catch (const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const FrontEndExceptions::NoBandSelectedExImpl &ex) {
        FrontEndExceptions::NoBandSelectedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getNoBandSelectedEx();
    } catch (const WCAExceptions::InsufficientPowerEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch (const WCAExceptions::LockLostEx &ex ) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch (const WCAExceptions::AdjustPllFailedEx &ex){
        WCAExceptions::LockFailedExImpl nex(ex,__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

NetSidebandMod::NetSideband FrontEndImpl::getSideband()
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        return getSidebandInternal();

    }catch (const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const FrontEndExceptions::UnknownFailureEx &ex) {
        FrontEndExceptions::UnknownFailureExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getUnknownFailureEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

Control::FrontEnd::ReceiverBandSeq* FrontEndImpl::availableBands()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    Control::FrontEnd::ReceiverBandSeq_var available_bands =
            new Control::FrontEnd::ReceiverBandSeq();
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        available_bands->length(availableBandsList_m.size());
        set<ReceiverBandMod::ReceiverBand>::iterator it;
        unsigned int index=0;
        for (it = availableBandsList_m.begin(); it != availableBandsList_m.end(); it++, index++) {
            available_bands[index] = *it;
        }
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        available_bands->length(0);
    } catch (...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        available_bands->length(0);
    }

    return available_bands._retn();
}

void FrontEndImpl::powerUpBand(ReceiverBandMod::ReceiverBand band)
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        powerUpBandInternal(band);

    } catch (const FrontEndExceptions::BandNotAvailableExImpl &ex) {
        FrontEndExceptions::BandNotAvailableExImpl nex(ex,__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getBandNotAvailableEx();
    } catch (const FrontEndExceptions::PowerUpLimitReachedExImpl &ex) {
        FrontEndExceptions::PowerUpLimitReachedExImpl nex(ex,__FILE__, __LINE__,
        __PRETTY_FUNCTION__);
        throw nex.getPowerUpLimitReachedEx();
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

void FrontEndImpl::powerOffBand(ReceiverBandMod::ReceiverBand band)
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        powerOffBandInternal(band);

    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                        __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

void FrontEndImpl::powerOffAllBands()
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        powerOffAllBandsInternal();

    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex,__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    }
}

Control::FrontEnd::ReceiverBandSeq* FrontEndImpl::getPoweredBands()
{

    Control::FrontEnd::ReceiverBandSeq_var powered_bands(getPoweredBandsInternal());
    return powered_bands._retn();

}

void FrontEndImpl::selectBand(ReceiverBandMod::ReceiverBand band)
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        selectBandInternal(band);

    } catch(ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,__PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const ControlExceptions::HardwareErrorExImpl &ex) {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,__PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    } catch(const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,__PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(const FrontEndExceptions::BandNotPoweredExImpl &ex ) {
        FrontEndExceptions::BandNotPoweredExImpl nex(ex, __FILE__, __LINE__,__PRETTY_FUNCTION__);
        throw nex.getBandNotPoweredEx();
    } catch (...) {
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getHardwareErrorEx();
    }

}

void FrontEndImpl::mixerDeflux(ReceiverBandMod::ReceiverBand band,
        float magnetCurrentPulseLenght, float Imax, float heatherPulseLenght)
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        mixerDefluxInternal(band, magnetCurrentPulseLenght, Imax, heatherPulseLenght);

    } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch (const ControlExceptions::IllegalParameterErrorEx &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(const ControlExceptions::InvalidRequestExImpl &ex) {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getInvalidRequestEx();
    } catch(const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::CAMBErrorExImpl &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const WCAExceptions::InsufficientPowerExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const WCAExceptions::LockFailedExImpl &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const FrontEndExceptions::BandNotPoweredExImpl &ex) {
        FrontEndExceptions::BandNotPoweredExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getBandNotPoweredEx();
    } catch (const FrontEndExceptions::PowerUpLimitReachedExImpl &ex) {
        FrontEndExceptions::PowerUpLimitReachedExImpl nex(ex,__FILE__, __LINE__,
        __PRETTY_FUNCTION__);
        throw nex.getPowerUpLimitReachedEx();
    }  catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}



void FrontEndImpl::enableCache(bool enable)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    useCache_m = enable;
    if (enable) {
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_NOTICE, "Cache usage has been enabled."));
    } else {
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                (LM_NOTICE, "Cache usage has been disabled."));
    }
}

void FrontEndImpl::clearCache()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    lockCache_m.clear();
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
            (LM_NOTICE, "FrontEnd cache has been cleared."));

}

void FrontEndImpl::dumpCache()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::map<int, std::vector<double> >::iterator it;
    for (it = lockCache_m.begin(); it != lockCache_m.end(); it++) {
        std::cout << "Freq:" << it->first << " ";
        std::cout << "CoarseYTO" << it->second[0] << " ";
        std::cout << "EDFA_V" << it->second[1] << " " << std::endl;

    }

}

void FrontEndImpl::optimizeLOAmp(ReceiverBandMod::ReceiverBand band)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        optimizeLOAmpInternal(band);

    } catch(const ControlExceptions::CAMBErrorExImpl &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const FrontEndExceptions::OptimizationFailedExImpl &ex) {
        FrontEndExceptions::OptimizationFailedExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getOptimizationFailedEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

void FrontEndImpl::optimizeSinglePol(ReceiverBandMod::ReceiverBand band, int pol, float VD,
        float targetIJ1)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        optimizeSinglePolInternal(band, pol, VD, targetIJ1);

    } catch(const ControlExceptions::CAMBErrorExImpl &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::CAMBErrorEx &ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const ControlExceptions::INACTErrorEx &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(const FrontEndExceptions::OptimizationFailedExImpl &ex) {
        FrontEndExceptions::OptimizationFailedExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getOptimizationFailedEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

void FrontEndImpl::queueFrequencies(
        const Control::FrontEnd::SubscanInformationSeq &frequencies)
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        queueFrequenciesInternal(frequencies);

    } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }

}

void FrontEndImpl::lockFrontEnd()
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        lockFrontEndInternal();

    } catch(const ControlExceptions::INACTErrorExImpl &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const ControlExceptions::HardwareErrorExImpl &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const FrontEndExceptions::BandNotPoweredExImpl &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const ControlExceptions::TimeoutExImpl& ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const ControlExceptions::IllegalParameterErrorEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const ControlExceptions::InvalidRequestEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const ControlExceptions::CAMBErrorEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const ControlExceptions::INACTErrorEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const WCAExceptions::InsufficientPowerEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const WCAExceptions::LockFailedEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const FrontEndExceptions::BandNotPoweredEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch(const FrontEndExceptions::BandNotSupportedEx &ex) {
        WCAExceptions::LockFailedExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getLockFailedEx();
    } catch (...) {
        WCAExceptions::LockFailedExImpl ex( __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getLockFailedEx();
    }

}

void FrontEndImpl::abortFrequencyChanges()
{
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        abortFrequencyChangesInternal();

    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
    }
}


ACS::TimeIntervalSeq* FrontEndImpl::timeToTune(
        const Control::FrontEnd::SubscanInformationSeq &frequencies)
{
    ACS::TimeIntervalSeq_var ret;
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
         ret= timeToTuneInternal(frequencies);
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
    return ret._retn();
}

ACS::TimeIntervalSeq* FrontEndImpl::timeToTuneACD(
        const Control::FrontEnd::SubscanInformationSeq &frequencies)
{
    ACS::TimeIntervalSeq_var ret;
    try {
        throwIfInShutdown(__FILE__, __LINE__,__PRETTY_FUNCTION__);
         ret= timeToTuneACDInternal(frequencies);
    } catch (const ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(...) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getINACTErrorEx();
    }
    return ret._retn();
}

ReceiverBandMod::ReceiverBand FrontEndImpl::getCurrentBand()
{
    return currentBand_m;
}

void FrontEndImpl::setLPRTunning(bool enable)
{
    enableLPRTunning_m = enable;
}

bool FrontEndImpl::getLPRTunning()
{
    return enableLPRTunning_m;
}
