#!/usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# @(#) $Id$
'''
FELOLocking
'''

from CCL.FrontEnd import FrontEnd
from CCL.WCA3 import WCA3
from CCL.WCA6 import WCA6
from CCL.WCA7 import WCA7
from CCL.WCA9 import WCA9
from CCL.LPR import LPR
from CCL.Global import setArrayName
from CCL.Global import getArray

     
class FELOLocking:
    '''
    '''
    def __init__(self, antenna, arrayName, pllDiagnostic=False):
        '''
        Common environment setup.
        '''
        self.antenna = antenna
        self.arrayName = arrayName

        # Getting device instances
        self.fe = FrontEnd(self.antenna)
        self.wca3 = WCA3(self.antenna)
        self.wca6 = WCA6(self.antenna)
        self.wca7 = WCA7(self.antenna)
        self.wca9 = WCA9(self.antenna)
        self.lpr = LPR(self.antenna)

        setArrayName(self.arrayName)
        self.array = getArray()
        self.tp = self.array.getTotalPowerObservingMode()

        # Set debug
        if pllDiagnostic:
            self.wca3.setPllDebug(True)
            self.wca6.setPllDebug(True)
            self.wca7.setPllDebug(True)
            self.wca9.setPllDebug(True)

    def __del__(self):
        '''
        Common environment cleanup.
        '''
        del self.fe
        del self.wca3
        del self.wca6
        del self.wca7
        del self.wca9
        del self.lpr
        if vars(self).has_key('wca'):
            del self.wca

    def lockLoop(self, freqTable = None):
        '''
        '''
        result = {}
        for i in freqTable:
            try:
                self.tp.setFrequency(i)
                sol = [True, None]
            except Exception, ex:
                sol = [False, ex]
            result[i] = sol
        return result

    def diagnosticLockLoop(self, freqTable, adjustPLL=False, correctionVoltage=0.0):
        '''
        retunrs a hash map Freq=>list
        list = [Tune, Exception, lprEDFA, wcaPhotomixer, loSolution, SB polarity, Correction voltage, CorseTune, (after adjust )Correction voltage, (after adjust )CorseTune]
        '''
        result = {}
        for i in freqTable:
            sol = []
            try:
                self.tp.setFrequency(i)
                sol.append(True)    #succeded LOCK
                sol.append(None)    #No exception
                self.__setWCA()     #convinience method to get Actual WCA
                sol.append(self.lpr.GET_MODULATION_INPUT_VALUE()[0])
                sol.append(self.wca.GET_LO_PHOTOMIXER_CURRENT()[0])
                solutionN = self.tp.getLOObservingMode().preferredTuningSolution()
                sol.append(self.tp.getLOObservingMode().getTuningSolution(solutionN))
                sol.append(self.wca.GET_LO_PLL_SB_LOCK_POLARITY_SELECT()[0])
                sol.append(self.wca.GET_LO_PLL_CORRECTION_VOLTAGE()[0])
                sol.append(self.wca.GET_LO_YTO_COARSE_TUNE()[0])
                if adjustPLL is True:
                    try:
                        self.wca.AdjustPll(correctionVoltage)
                        sol.append(self.wca.GET_LO_PLL_CORRECTION_VOLTAGE()[0])
                        sol.append(self.wca.GET_LO_YTO_COARSE_TUNE()[0])
                    except Exception, ex:
                        sol[0] = False
                        sol[1] = str(ex)
                        print ex
                        sol.append(None)
                        sol.append(None)
                else:
                    sol.append(None)
                    sol.append(None)        
            except Exception, ex:
                if len(sol) < 2:
                    sol.append(False)   #succeded LOCK
                    sol.append(None)    #No exception
                print ex
                sol[1] = str(ex)
            result[i] = sol
        #freq => Tune, Exception, lprEDFA, wcaPhotomixer, loSolution, SB polarity, Correction voltage, CorseTune, (after adjust )Correction voltage, (after adjust )CorseTune
        return result

    def __setWCA(self):
        '''
        '''
        if self.fe.getCurrentBand() == FrontEnd.Band.ALMA_RB_03:
            self.wca = WCA3(self.antenna)
        elif self.fe.getCurrentBand() == FrontEnd.Band.ALMA_RB_06:
            self.wca = WCA6(self.antenna)
        elif self.fe.getCurrentBand() == FrontEnd.Band.ALMA_RB_07:
            self.wca = WCA7(self.antenna)
        elif self.fe.getCurrentBand() == FrontEnd.Band.ALMA_RB_09:
            self.wca = WCA9(self.antenna)


def prettyPrint(hash_table, filename=None):
    '''
    '''
    print "Sky Freq\tLock\tLprEDFA\twcaPM\tSB\tCvolt\tCtune\tCvoltAf\tCTuneA"
    if filename is not None:
        f = open(filename, "w")
        print >> f, "Sky Freq,Lock,LprEDFA,wcaPhotmixer,SB,Correction volt,Coarse tune,Correction volt After,Coarse Tune After,tuning solution,Exception"
    for key in hash_table:
        arr = []
        arr.append(key)
        values = hash_table[key]
        if len(values) == 2:
            print "%s\t%s\tNone\tNone\tNone\tNone\tNone\tNone\tNone" % (key, values[0])
            if filename is not None:
                print >> f, "%s,%s,,,,,,,,,%s" % (key, values[0], values[1])
                f.flush()
        else:
            print "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (key, values[0], values[2], values[3], values[5], values[6], values[7], values[8], values[9])
            if filename is not None:
                print >> f, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (key, values[0], values[2], values[3], values[5], values[6], values[7], values[8], values[9], values[4], values[1])
                f.flush()
    if filename is not None:
        f.close()


if __name__ == "__main__":
    unittest.main()

