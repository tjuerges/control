#! /usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# File FrontEnd.py
"""
This module defines the FrontEnd Control Command Language (CCL)
base class.It provides access to all set monitor, control points and
device methods.
"""

import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import Acspy.Common.Err
from CCL import StatusHelper
from CCL.EnumerationHelper import getEnumeration
import os

from ScriptExec.rm import getComponent
from ScriptExec.rm import getLogger
from ScriptExec.rm import getComponentNonSticky

from PyDataModelEnumeration import PyReceiverBand
from PyDataModelEnumeration import PyNetSideband
from PyDataModelEnumeration import PyCalibrationDevice

class FrontEnd():
    '''
    The FrontEnd Controller handles the available FrontEnd devices,
    p.e. powers them up, turnos them off and permits their tuning.
    '''

    # Import the receiver band enum types
    from almaEnumerations_IF_idl import _0_ReceiverBandMod as Band
    from almaEnumerations_IF_idl import _0_NetSidebandMod as Sideband
    from almaEnumerations_IF_idl import _0_CalibrationDeviceMod as CalibrationDevice
    
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        The constructor creates a FrontEnd object.

        If the antennaName is defined then this constructor references
        the component running on that antenna. Alternativly the full
        componentName can be specified if desired.

        The FrontEndBase class is a python proxy to the FrontEnd
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponent.

        An exception is thrown if there is a problem creating
        this component, establishing the connection to the
        previously mentioned hardware components, or if either
        both or neither antennaName and componentName are specified.

        The front end class allows Bands to be specified as integers,
        strings "ALMA_RB_03", or enumerations.

        EXAMPLE:
        from CCL.FrontEnd import FrontEnd
        fe = FrontEnd("DV01")
        import Control
        wcaConfig = Control.DeviceConfig("WCA3", [])
        pdConfig  = Control.DeviceConfig("PowerDist3", [])
        ccConfig  = Control.DeviceConfig("ColdCart3", [])
        fecConfig = Control.DeviceConfig("FEC", [])
        fedConfig = Control.DeviceConfig("FED", [])
        feConfig  = Control.DeviceConfig("FrontEnd", [wcaConfig, pdConfig, ccConfig, fecConfig, fedConfig])
        fe.createSubdevices(feConfig, "CONTROL/DV01/FrontEnd")
        fe.controllerOperational()
        fe.powerUpBand(FrontEnd.Band.ALMA_RB_03)
        fe.selectBand(FrontEnd.Band.ALMA_RB_03)
        fe.setLO1Frequency(102.5E9, FrontEnd.Sideband.LSB)
        fe.controllerShutdown()
        fe.releaseSubdevices()
        '''
        # Initialize the base class
        # Dictionary of device instances
        self._devices = {}

        # Group of antenna or component names.
        if ((isinstance(antennaName,list) == True) |
            (isinstance(componentName,list) == True)):
            #antenna names
            if (isinstance(antennaName,list) == True):
                if (len(antennaName) != 0):
                    for idx in range (0,len(antennaName)):
                        self._devices["CONTROL/"+antennaName[idx]+"/FrontEnd"]=""
            #component names
            if (isinstance(componentName,list) == True):
                if (len(componentName) != 0):
                    for idx in range (0,len(componentName)):
                        self._devices[componentName[idx]]=""
        # One component
        else:
            if not((antennaName == None) ^ (componentName == None)):
                raise NameError, "missing antennaName or componentName"
            if antennaName != None:
                self._devices["CONTROL/"+antennaName+"/FrontEnd"]=""

            if componentName != None:
                self._devices[componentName] = ""

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.__stickyFlag = stickyFlag

        # Initialize the instances
        for key, val in self._devices.iteritems():

            if self.__stickyFlag:
                self.__fe = getComponent(key)
            else:
                self.__fe = getComponentNonSticky(key)
            self._devices[key]= self.__fe

        self.__logger = getLogger()


    def __del__(self):
        '''
        Destructor
        '''
        for key, val in self._devices.iteritems():
            instance = self._devices[key]
            del(instance)

    def __bandInputToEnumeration(self, inputBand):
        '''
        This method allows the user to input the band as either an
        integer [1-10], a string "ALMA_RB_03" or as an enumeration
        and returns the enumerated value.

        Helper routine used through out the rest of the class where
        bands are input.
        '''
        if isinstance(inputBand, int):
            inputBand = "ALMA_RB_%02d" % inputBand
        return getEnumeration(inputBand, PyReceiverBand)
        
    #
    # --------------------- General Methods ---------------------
    #


    #
    # --------------------- Monitor Points ----------------------
    #

    
    # --------------------- Control Points ---------------------


 
    #
    # setLO1Frequency
    #
    def setLO1Frequency(self, frontEndFreq, sideband):
        '''
        Sets the overall frequency and the sideband of the FrontEnd.
        
        Example:
        fe.setLO1Frequency(102.5E9, FrontEnd.Sideband.LSB)
        or
        fe.setLO1Frequency(102.5E9, "LSB")
        '''
        sideband = getEnumeration(sideband,PyNetSideband)
        
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].setLO1Frequency(frontEndFreq, sideband)
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    #
    # setStandbyLO1Frequency
    #
    def setStandbyLO1Frequency(self, band, frontEndFreq, sideband):
        '''
        Sets the overall frequency and the sideband for a specified
        band that is currently in standby-mode.
        
        Example:
        fe.setStandbyLO1Frequency(FrontEnd.Band.ALMA_RB_06, 245.0E9, FrontEnd.Sideband.LSB)
        or
        fe.setStandbyLO1Frequency("ALMA_RB_06", 245.0E9, "LSB")
        '''
        band = self.__bandInputToEnumeration(band)
        sideband = getEnumeration(sideband, PyNetSideband)
        
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].setStandbyLO1Frequency(band, frontEndFreq, sideband)
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    #
    # getLO1Frequency
    #
    def getLO1Frequency(self):
        '''
        Returns the currently set FrontEnd frequency.
        
        Example:
        freq = fe.getLO1Frequency()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getLO1Frequency()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    #
    # getStandbyLO1Frequency
    #
    def getStandbyLO1Frequency(self, band):
        '''
        Returns the currently frequency for the specified band that is
        currently in standby-mode.
        
        Example:
        freq = fe.getStandbyLO1Frequency(FrontEnd.Band.ALMA_RB_06)
        '''
        band = self.__bandInputToEnumeration(band)
        
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getStandbyLO1Frequency(band)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # isLocked
    #
    def isLocked(self):
        '''
        Returns the lock state of the currently selected band.
        
        Example:
        lock = fe.isLocked()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].isLocked()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    #
    # relock
    #
    def relock(self):
        '''
        Attempts to re-lock the WCA after a band switch.
        
        Example:
        fe.relock()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].relock()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    #
    # getSideband
    #
    def getSideband(self):
        '''
        Returns the currently set FrontEnd sideband.
        
        Example:
        sideband = fe.getSideband()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getSideband() 
        if len(self._devices) == 1:
            return result.values()[0]
        return result


    #
    # createSubdevices
    #
    def createSubdevices(self, config, parentName):
        '''
        Configures the FE controller by providing a list of all
        available FrontEnd components.
        
        Example:
        wcaConfig = Control.DeviceConfig("WCA3", [])
        pdConfig  = Control.DeviceConfig("PowerDist3", [])
        ccConfig  = Control.DeviceConfig("ColdCart3", [])
        fecConfig = Control.DeviceConfig("FEC", [])
        fedConfig = Control.DeviceConfig("FED", [])
        feConfig  = Control.DeviceConfig("FrontEnd", [wcaConfig, pdConfig, ccConfig, fecConfig, fedConfig])
        fe.createSubdevices(feConfig, "CONTROL/DV01/FrontEnd")
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].createSubdevices(config, parentName) 
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # releaseSubdevices
    #
    def releaseSubdevices(self):
        '''
	Release all references to the FE controller subdevices.

        Example:
        fe.releaseSubdevices()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].releaseSubdevices()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # getSubdeviceList
    #
    def getSubdeviceList(self):
        '''
        Returns the list of the currently configured FE subdevices.

        Example:
        fe.getSubdeviceList()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getSubdeviceList()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # controllerOperational
    #
    def controllerOperational(self):
        '''
        Brings the FE controller to operational. This brings to
        operational all common subdevices (e.g. cryostat), but not
        those belonging to a specific receiver band.

        Example:
        fe.controllerOperational()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].controllerOperational()  
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # controllerShutdown
    #
    def controllerShutdown(self):
        '''
        Shuts down the FE controller. 

        Example:
        fe.controllerShutdown()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].controllerShutdown() 
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # availableBands
    #
    def availableBands(self):
        '''
        Returns a list of all currently available bands.
        
        Example:
        bands = fe.availableBands()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].availableBands() 
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # powerUpBand
    #
    def powerUpBand(self, band):
        '''
        Powers up the specified band. Throws an exception if the specified
        band is not available.
        
        Example:
        fe.powerUpBand(FrontEnd.Band.ALMA_RB_03)
        '''

        band = self.__bandInputToEnumeration(band)
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].powerUpBand(band)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # powerOffBand
    #
    def powerOffBand(self, band):
        '''
        Powers off the specified band. Throws an exception if the specified
        band is not powered on.
        
        Example:
        fe.powerOffBand(FrontEnd.Band.ALMA_RB_03)
        '''
        band = self.__bandInputToEnumeration(band)
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].powerOffBand(band)
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    #
    # powerOffAllBands
    #
    def powerOffAllBands(self):
        '''
        Powers off all currently powered on bands.
        
        Example:
        fe.powerOffAllBands()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].powerOffAllBands()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    #
    # getPoweredBands
    #
    def getPoweredBands(self):
        '''
        Returns a list of all currently available bands.
        
        Example:
        bands = fe.getPoweredBands()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getPoweredBands()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # selectBand
    #
    def selectBand(self, band):
        '''
        Selects the specified receiver band. Throws an exception if
        the specified band is not powered on.
        
        Example:
        fe.selectBand(FrontEnd.Band.ALMA_RB_03)
        '''
        band = self.__bandInputToEnumeration(band)
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].selectBand(band)
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    #
    # getCurrentBand
    #
    def getCurrentBand(self):
        '''
        Returns the currently selected receiver band.
        
        Example:
        band = fe.getCurrentBand()
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getCurrentBand() 
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def optimizeLOAmp(self, band):
        '''
        Optimize the Power Amp for the selected band.
        
        Example:
        band = fe.optimeLOAmp(FrontEnd.Band.ALMA_RB_09)
        '''
        band = self.__bandInputToEnumeration(band)
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            self._devices[key].optimizeLOAmp(band) 

    def optimizeSinglePol(self, band , pol, VD, tagetIJ):
        '''
        Optimize the Power Amp for the selected band and polarization.
        VD in volts
        tagetIJ in Ampere 
        Example:
        band = fe.optimizeSinglePol(FrontEnd.Band.ALMA_RB_09, 0, 1.5, 20.5E-6)
        '''
        band = self.__bandInputToEnumeration(band)
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            self._devices[key].optimizeSinglePol(band , pol, VD, tagetIJ) 

    def setLPRTunning(self, enable):
        '''
        Set to enable/disable the LPR EDFA tunning procedure
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            self._devices[key].setLPRTunning(enable) 

    def getLPRTunning(self):
        '''
        get the LPR EDFA tunning procedure state.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getLPRTunning() 
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def setCalibrationDeviceBand(self,calibrationDevice, band):
        '''
        Set the calibration device for the specified Band
        The possible options are: AMBIENT_LOAD, COLD_LOAD,
        HOT_LOAD, NOISE_TUBE_LOAD, QUARTER_WAVE_PLATE,
        SOLAR_FILTER and NONE (park), but currently only
        AMBIENT_LOAD, HOT_LOAD and NONE are actually implemented.

        Example:
        fe.setCalibrationDeviceBand(fe.CalibrationDevice.HOT_LOAD,
                                    fe.Band.ALMA_RB_03)
        '''
        band = self.__bandInputToEnumeration(band)
        calibrationDevice = getEnumeration(calibrationDevice,
                                           PyCalibrationDevice)
        for key, val in self._devices.iteritems():
            self._devices[key].setCalibrationDeviceBand(calibrationDevice,
                                                        band) 

    def setCalibrationDevice(self,calibrationDevice):
        '''
        Set the calibration device for the current Band
        The possible options are: AMBIENT_LOAD, COLD_LOAD,
        HOT_LOAD, NOISE_TUBE_LOAD, QUARTER_WAVE_PLATE,
        SOLAR_FILTER and NONE (park), but currently only
        AMBIENT_LOAD, HOT_LOAD and NONE are actually implemented.

        Example:
          fe.setCalibrationDevice(fe.CalibrationDevice.HOT_LOAD)
          or
          fe.setCalibrationDevice("HOT_LOAD")
        '''
        calibrationDevice = getEnumeration(calibrationDevice,
                                           PyCalibrationDevice)
        for key, val in self._devices.iteritems():
            self._devices[key].setCalibrationDevice(calibrationDevice) 
            
    def getCurrentCalibrationDevice(self):
        '''
        Return the calibration device currently in from of the current band
        if the current band is not set or the ACD is positioned in front of
        a different band an exception is thrown.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getCurrentCalibrationDevice()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def mixerDeflux(self, band, magnetCurrentPulseLength = 0.1,
                    Imax = 50,
                    heatherPulseLength = 0.5):
        '''
        This method executes the deflux procedure for a specific band
        in this Front end according to section 4.4 of
        FEND-40.02.09.00-048-A3-MAN.
       
        @param ReceiverBandMod::ReceiverBand band
        @param float magnetCurrentPulseLenght in seconds
        @param float Imax in mA
        @param float heatherPulseLenght in seconds
        @except ControlExceptions::CAMBErrorEx
        @except ControlExceptions::INACTErrorEx
        @except ControlExceptions::IllegalParameterErrorEx
        @except FrontEndExceptions::BandNotPoweredEx
        @except FrontEndExceptions::UnknownFailureEx
        @except FrontEndExceptions::PowerOffFailureEx
        @except ControlExceptions::InvalidRequestEx
        @except WCAExceptions::InsufficientPowerEx
        @except WCAExceptions::LockFailedEx
        @except FrontEndExceptions::NoBandSelectedEx
        @except FrontEndExceptions::BandNotSupportedEx
        
        Example:
        band = fe.mixerDeflux(FrontEnd.Band.ALMA_RB_09)
        '''
        band = self.__bandInputToEnumeration(band)
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            self._devices[key].mixerDeflux(band,
                                           magnetCurrentPulseLength,
                                           Imax, heatherPulseLength) 

    #
    # STATUS
    #
    def STATUS(self):
        '''
        This method displays the current status of the FrontEnd
        '''
        os.system("clear")
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            # List of status elements
            elements = []

            ################## Controller ###########################
            elements.append( StatusHelper.Separator("Controller") )

            # State
            try:
                value = self._devices[key].getState()                
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("State",
                                               [StatusHelper.ValueUnit(value)]) )   

            ############################### Bands ###################################################
            elements.append( StatusHelper.Separator("Bands") )
            
            # Available bands
            try:
                value = self._devices[key].availableBands()
                orig = value
                if len(value) > 4:
                    value = orig[:4]
                    elements.append( StatusHelper.Line("Available bands",
                                               [StatusHelper.ValueUnit(value)]) )
                if len(orig) > 8:
                    value = orig[4:8]
                    elements.append( StatusHelper.Line("Available bands",
                                               [StatusHelper.ValueUnit(value)]) )
                    value = orig[8:]
                    elements.append( StatusHelper.Line(" ",
                                               [StatusHelper.ValueUnit(value)]) )
                else:
                    value = orig
                    elements.append( StatusHelper.Line("Available bands",
                                               [StatusHelper.ValueUnit(value)]) )

                    
            except Exception, e:
                print e
                value = "N/A";
                elements.append( StatusHelper.Line("Available bands",
                                               [StatusHelper.ValueUnit(value)]) )

            # Powered-up bands
            try:
                value = self._devices[key].getPoweredBands()
            except:
                value = "N/A";
            elements.append( StatusHelper.Line("Powered bands",
                                               [StatusHelper.ValueUnit(value)]) )

            ############################### Selected Band #############################################
            elements.append( StatusHelper.Separator("Selected band") )

            # Selected band
            try:
                value = self._devices[key].getCurrentBand()
                band = value
            except:
                value = "N/A";
            elements.append( StatusHelper.Line("Selected band",
                                               [StatusHelper.ValueUnit(value)]) )

            #FIXME change this for != than unspecified band.
            if band != FrontEnd.Band.UNSPECIFIED:
           # if band == FrontEnd.Band.ALMA_RB_03 or band == FrontEnd.Band.ALMA_RB_04 or band == FrontEnd.Band.ALMA_RB_06 or band == FrontEnd.Band.ALMA_RB_07 or band == FrontEnd.Band.ALMA_RB_08 or band == FrontEnd.Band.ALMA_RB_09:
                # LO1 Frequency
                try:
                    value = self._devices[key].getLO1Frequency()
                    value = "%.6f" % (value / 1.0E9)
                except:
                    value = "N/A"
                elements.append( StatusHelper.Line("LO1 frequency",
                                                   [StatusHelper.ValueUnit(value, "GHz")]) )

                # Sideband
                try:
                    value = self._devices[key].getSideband()
                except:
                    value = "N/A";
                elements.append( StatusHelper.Line("Sideband",
                                                   [StatusHelper.ValueUnit(value)]) )
                
                # isLocked
                try:
                    value = self._devices[key].isLocked()
                    if value == True:
                        value_t = "Yes"
                    else:
                        value_t = "NO"
                except:
                    value_t = "N/A";
                elements.append( StatusHelper.Line("Locked",
                                                   [StatusHelper.ValueUnit(value_t)]) )
            else:
                elements.append( StatusHelper.Line("LO1 frequency",
                                                   [StatusHelper.ValueUnit("N/A", "GHz")]) )
                elements.append( StatusHelper.Line("Sideband",
                                                   [StatusHelper.ValueUnit("N/A")]) )
                elements.append( StatusHelper.Line("Locked",
                                                   [StatusHelper.ValueUnit("N/A")]) )
                
            try:
                band = self._devices[key].getCurrentBand()
                if band == FrontEnd.Band.ALMA_RB_01:
                    import CCL.ColdCart1
                    cc = CCL.ColdCart1.ColdCart1(antName)
                elif band == FrontEnd.Band.ALMA_RB_02:
                    import CCL.ColdCart2
                    cc = CCL.ColdCart2.ColdCart2(antName)
                elif band == FrontEnd.Band.ALMA_RB_03:
                    import CCL.ColdCart3
                    cc = CCL.ColdCart3.ColdCart3(antName)
                elif band == FrontEnd.Band.ALMA_RB_04:
                    import CCL.ColdCart4
                    cc = CCL.ColdCart4.ColdCart4(antName)
                elif band == FrontEnd.Band.ALMA_RB_05:
                    import CCL.ColdCart5
                    cc = CCL.ColdCart5.ColdCart5(antName)
                elif band == FrontEnd.Band.ALMA_RB_06:
                    import CCL.ColdCart6
                    cc = CCL.ColdCart6.ColdCart6(antName)
                elif band == FrontEnd.Band.ALMA_RB_07:
                    import CCL.ColdCart7
                    cc = CCL.ColdCart7.ColdCart7(antName)
                elif band == FrontEnd.Band.ALMA_RB_08:
                    import CCL.ColdCart8
                    cc = CCL.ColdCart8.ColdCart8(antName)
                elif band == FrontEnd.Band.ALMA_RB_09:
                    import CCL.ColdCart9
                    cc = CCL.ColdCart9.ColdCart9(antName)
                elif band == FrontEnd.Band.ALMA_RB_10:
                    import CCL.ColdCart10
                    cc = CCL.ColdCart10.ColdCart10(antName)

                sisStat = cc.SISSTATUS()
                elements.append(StatusHelper.Group("SIS Mixers",sisStat))
                   
            except:
                elements.append( StatusHelper.Line("SIS Mixers",[StatusHelper.ValueUnit("N/A")]) )

            try:
                band = self._devices[key].getCurrentBand()
                if band == FrontEnd.Band.ALMA_RB_01:
                    import CCL.WCA1
                    wca = CCL.WCA1.WCA1(antName)
                elif band == FrontEnd.Band.ALMA_RB_02:
                    import CCL.WCA2
                    wca = CCL.WCA2.WCA2(antName)
                elif band == FrontEnd.Band.ALMA_RB_03:
                    import CCL.WCA3
                    wca = CCL.WCA3.WCA3(antName)
                elif band == FrontEnd.Band.ALMA_RB_04:
                    import CCL.WCA4
                    wca = CCL.WCA4.WCA4(antName)
                elif band == FrontEnd.Band.ALMA_RB_05:
                    import CCL.WCA5
                    wca = CCL.WCA5.WCA5(antName)
                elif band == FrontEnd.Band.ALMA_RB_06:
                    import CCL.WCA6
                    wca = CCL.WCA6.WCA6(antName)
                elif band == FrontEnd.Band.ALMA_RB_07:
                    import CCL.WCA7
                    wca = CCL.WCA7.WCA7(antName)
                elif band == FrontEnd.Band.ALMA_RB_08:
                    import CCL.WCA8
                    wca = CCL.WCA8.WCA8(antName)
                elif band == FrontEnd.Band.ALMA_RB_09:
                    import CCL.WCA9
                    wca = CCL.WCA9.WCA9(antName)
                elif band == FrontEnd.Band.ALMA_RB_10:
                    import CCL.WCA10
                    wca = CCL.WCA10.WCA10(antName)

                try:
                    elements.append(wca.PADrainSTATUS())
                except:
                    elements.append( StatusHelper.Line("Drain Voltage",[StatusHelper.ValueUnit("N/A")]) )
                try:
                    value = wca.GET_LO_PHOTOMIXER_CURRENT()[0]
                    value = "%.2f" % (value * 1000.0)
                    elements.append(StatusHelper.Line( "Photomixer Current", [StatusHelper.ValueUnit(value, "mA")]))
                except:
                    elements.append( StatusHelper.Line("Photomixer Current",[StatusHelper.ValueUnit("N/A")]) )
            except:
                elements.append( StatusHelper.Line("Drain Voltage",[StatusHelper.ValueUnit("N/A")]) )
                elements.append( StatusHelper.Line("Photomixer Current",[StatusHelper.ValueUnit("N/A")]) )

            elements.append( StatusHelper.Separator("LPR") )
            try:
                import CCL.LPR
                lpr = CCL.LPR.LPR(antName)
                value = lpr.GET_MODULATION_INPUT_VALUE()[0]
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Modulation Voltage",
                                               [StatusHelper.ValueUnit(value, "V")]) )
               
            
            elements.append( StatusHelper.Separator("IFProc Sideband Power") )
            try:
                import CCL.IFProc
                ifp0 = CCL.IFProc.IFProc(antName, 0)
                value = ifp0.getSBPower()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1]];
            except:
                value = ["N/A"]*2;
            elements.append( StatusHelper.Line("IFProc 0 SB Power" , [ StatusHelper.ValueUnit(value[0], "dbm", "USB"),
                                                                    StatusHelper.ValueUnit(value[1], "dbm", "LSB")]) )

            try:
                import CCL.IFProc
                ifp1 = CCL.IFProc.IFProc(antName, 1)
                value = ifp1.getSBPower()[0];
                value = ["%+.3f"%value[0], "%+.3f"%value[1]];
            except:
                value = ["N/A"]*2;
            elements.append( StatusHelper.Line("IFProc 1 SB Power" , [ StatusHelper.ValueUnit(value[0], "dbm", "USB"),
                                                                    StatusHelper.ValueUnit(value[1], "dbm", "LSB")]) )

            elements.append( StatusHelper.Separator("Cryostat") )
            try:
                import CCL.Cryostat
                cryo = CCL.Cryostat.Cryostat(antName)
                cryoTemps = cryo.CompactTempSTATUS() 
                elements.append(StatusHelper.Group("Temperature",cryoTemps))
                
                cryoPre = cryo.CompactPressureSTATUS()
                elements.append(cryoPre)
            except:
                elements.append( StatusHelper.Line("Temperature",[StatusHelper.ValueUnit("N/A")]) )
                elements.append( StatusHelper.Line("Pressure",[StatusHelper.ValueUnit("N/A")]) )
                
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
   
