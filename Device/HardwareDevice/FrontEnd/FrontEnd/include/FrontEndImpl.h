#ifndef FrontEndImpl_H
#define FrontEndImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FrontEndImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// C++
#include <vector>
#include <set>

// Base class(es)
#include <baciCharacteristicComponentImpl.h>

// CORBA servant header generated from IDL.
#include <FrontEndS.h>


// ALMA Enum types
#include <Enum.hpp>

// Hardware Controller
#include <hardwareControllerImpl.h>

// ACD
#include <ACDC.h>


// FE components that need to be accessed via their interface
#include <WCAC.h>
#include <ColdCartC.h>
#include <PowerDistC.h>
#include <IFSwitchC.h>
#include <LPRC.h>

// Exceptions
#include <WCAExceptions.h>
#include <ColdCartExceptions.h>
#include <FrontEndExceptions.h>

// Forward declarations for classes that this component uses
namespace maci {
  class ContainerServices;
}

/// This class coordinates the configuration and startup/shutdown of
/// the different receiver bands. Restrictions are that not more than
/// two bands can be powered on simultaneously. It also allows to
/// set/get the frequency.
///
class FrontEndImpl : public Control::HardwareControllerImpl,
		     public virtual POA_Control::FrontEnd
{
    public:
    /**
     * Constructor
     */
    FrontEndImpl(const ACE_CString&       name,
		 maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~FrontEndImpl();

    void cleanUp();

    /* ----------------- Constants & Enums -----------------------*/
    static const unsigned int MAX_POWERED_BANDS;
    static const unsigned int NUM_RECEIVER_BANDS;
    static const double       ACD_TIMEOUT;


    /* ----------------- External Interface ----------------------*/

    /*
     * General Controller Interface
     */
    void tuneLPRPIFTotalpower(float ift_input_target);
    void tuneLPRPhotomixer(float pm_input_target);

    /**
     * virtual voidcreateSubdevices(const Control::DeviceConfig& configuration,
     * const char* parentName)
     * Receives the FE controller configuration information and
     * creates the respective subdevices
     * @override HardwareControllerImpl::createSubdevices
     * @param const Control::DeviceConfig& configuration
     * @param const char* parentName
     * @except ControlDeviceExceptions::IllegalConfigurationEx
     */
    virtual void
    createSubdevices(const Control::DeviceConfig& configuration,
                     const char *parentName);

    /**
     * virtual void controllerOperational()
     * Brings the FE controller to operational state
     * @override HardwareControllerImpl::controllerOperational
     */
    virtual void controllerOperational();


    /* 
     * FE Controller Interface
     * See the IDL-file for descriptions.
     */

      /**
       * void setLO1Frequency(in double frontEndFreq, in NetSidebandMod::NetSideband sideband)
       * Set the overall frequency of the frontEnd (this does not include
       *the contribution from the FLOOG).  And if the FLOOG is on the upper
       * or lower sideband.
       * Note: Only NetSideband.LSB and NetSideband.USB are allowed calls
       *       to this method since this controls if the FLOOG is added to or
       *       subtracted from the First LO.
       * Notes: If this ends up taking a long time, we may want to make this
       *	a oneway call
       * @param double frontEndFreq
       * @param NetSidebandMod::NetSideband sideband
       * @except ControlExceptions::IllegalParameterErrorEx
       * @except ControlExceptions::InvalidRequestEx
       * @except ControlExceptions::CAMBErrorEx
       * @except ControlExceptions::INACTErrorEx
       * @except WCAExceptions::InsufficientPowerEx
       * @except WCAExceptions::LockFailedEx
       * @except FrontEndExceptions::NoBandSelectedEx
       * @except FrontEndExceptions::BandNotSupportedEx
      */      
    virtual void setLO1Frequency(CORBA::Double frontEndFreq,
				  NetSidebandMod::NetSideband sideband);

      /**
       * void setStandbyLO1Frequency(in ReceiverBandMod::ReceiverBand band,
       *                              in double frontEndFreq,
       *                              in NetSidebandMod::NetSideband sideband) 
       * Same as setLO1Frequency(), but no lock is acquired. This method
       * can be used to set the frequency for those bands that are currently
       * in standby mode.
       * @param ReceiverBandMod::ReceiverBand band
       * @param double frontEndFreq
       * @param NetSidebandMod::NetSideband sideband
       * @except ControlExceptions::IllegalParameterErrorEx
       * @except ControlExceptions::InvalidRequestEx
       * @except ControlExceptions::CAMBErrorEx
       * @except ControlExceptions::INACTErrorEx
       * @except WCAExceptions::InsufficientPowerEx
       * @except WCAExceptions::LockFailedEx
       * @except FrontEndExceptions::NoBandSelectedEx
       * @except FrontEndExceptions::BandNotSupportedEx
      */      
    virtual void setStandbyLO1Frequency(ReceiverBandMod::ReceiverBand band,
					 CORBA::Double frontEndFreq,
					 NetSidebandMod::NetSideband sideband);
    
      /**
       * double getLO1Frequency() 
       * Get the current Frequency of the front end (again minus the FLOOG 
       * Contribution
       * @return double LO1Frequency value.
       * @except ControlExceptions::INACTErrorEx
       * @except FrontEndExceptions::NoBandSelectedEx
       * @except FrontEndExceptions::BandNotSupportedEx
      */
    virtual double getLO1Frequency();

      /**
       * double getStandbyLO1Frequency(in ReceiverBandMod::ReceiverBand band)
       * Same as getLO1Frequency(), but the frequency of a specified
       * band (in standby mode) can be retrieved.
       * @param ReceiverBandMod::ReceiverBand band
       * @return double standby LO1Frequency value.
       * @except ControlExceptions::INACTErrorEx
       * @except FrontEndExceptions::NoBandSelectedEx
       * @except FrontEndExceptions::BandNotSupportedEx
      */
    virtual double getStandbyLO1Frequency(ReceiverBandMod::ReceiverBand band);

      /**
       * boolean isLocked()
       * Retrieves the current lock status of the WCA
       * @except ControlExceptions::INACTErrorEx
       * @except ControlExceptions::CAMBErrorEx
       * @except FrontEndExceptions::NoBandSelectedEx
       * @except FrontEndExceptions::BandNotSupportedEx
       */
    virtual bool isLocked();

      /**
       * void relock()
       * Permits to relock the WCA after a band switch, if its frequency
       * settings have been set before or did not change.
       * @except ControlExceptions::CAMBErrorEx
       * @except ControlExceptions::INACTErrorEx
       * @except WCAExceptions::InsufficientPowerEx
       * @except WCAExceptions::LockFailedEx
       * @except WCAExceptions::LockLostEx
       * @except WCAExceptions::AdjustPllFailedEx,
       * @except FrontEndExceptions::NoBandSelectedEx
       * @except FrontEndExceptions::BandNotSupportedEx
      */      
    virtual void relock();

      /** 
       * NetSidebandMod::NetSideband getSideband()
       * Get the currently used subband 
       * Note: Hopefully the sideband selection can be made an enum in 
       * parallel with the SFI-ASDM changes.
       * @return NetSidebandMod::NetSideband selected side band
       * @except ControlExceptions::CAMBErrorEx
       * @except ControlExceptions::INACTErrorEx
       * @except FrontEndExceptions::UnknownFailureEx,
       * @except FrontEndExceptions::NoBandSelectedEx
      */
    virtual NetSidebandMod::NetSideband getSideband();


      /**
       * ReceiverBandSeq availableBands()
       * Obtain a sequence of the currently available bands
       * @return ReceiverBandSeq
       */
    virtual Control::FrontEnd::ReceiverBandSeq* availableBands();

      /**
       * void powerUpBand(in ReceiverBandMod::ReceiverBand band)
       * Power up a specified receiver band
       * @param ReceiverBandMod::ReceiverBand band
       * @except FrontEndExceptions::PowerUpLimitReachedEx
       * @except FrontEndExceptions::BandNotAvailableEx
       * @except FrontEndExceptions::PowerUpFailureEx
       */
    virtual void powerUpBand(ReceiverBandMod::ReceiverBand band);

      /**
       * void powerOffBand(in ReceiverBandMod::ReceiverBand band)
       * Power off a specified receiver band
       * @param ReceiverBandMod::ReceiverBand band
       * @except FrontEndExceptions::BandNotPoweredEx
       * @except FrontEndExceptions::UnknownFailureEx
       * @except FrontEndExceptions::PowerOffFailureEx
       */ 
    virtual void powerOffBand(ReceiverBandMod::ReceiverBand band);

      /**
       * void powerOffAllBands()
       * Power off all currently powered bands
       * @except FrontEndExceptions::UnknownFailureEx
       * @except FrontEndExceptions::PowerOffFailureEx
       */ 
    virtual void powerOffAllBands();

      /**
       * ReceiverBandSeq getPoweredBands()
       * Obtain a sequence of the currently powered on bands
       * @return ReceiverBandSeq
       */
    virtual Control::FrontEnd::ReceiverBandSeq* getPoweredBands();

      /**
       * void selectBand(in ReceiverBandMod::ReceiverBand band)
       * Select a specific powered band
       * @param ReceiverBandMod::ReceiverBand band
       * @except FrontEndExceptions::BandNotPoweredEx
       * @except FrontEndExceptions::IfSwitchFailureEx
       * @except FrontEndExceptions::LprFailureEx
       */
    virtual void selectBand(ReceiverBandMod::ReceiverBand band);

      /**
       * ReceiverBandMod::ReceiverBand getCurrentBand()
       * Obtain the currently selected band
       * @return ReceiverBandMod::ReceiverBand
       */
    virtual ReceiverBandMod::ReceiverBand getCurrentBand();

      /**
       * void optimizeSinglePol(in ReceiverBandMod::ReceiverBand band , 
       *                        in long pol, in float VD, in float tagetIJ)
       * Optimize the LO PA according to the parameters for a single polarization.
       * @param ReceiverBandMod::ReceiverBand band
       * @param long pol - polarization
       * @param float VD - Drain voltage
       * @param float tagetIJ - target junction current
       * @except ControlExceptions::CAMBErrorEx
       * @except ControlExceptions::INACTErrorEx
       * @except FrontEndExceptions::OptimizationFailedEx
       */
    void optimizeSinglePol(ReceiverBandMod::ReceiverBand band, 
                           int pol, 
                           float VD, 
                           float tagertIJ);

      /**
       * void optimizeLOAmp(in ReceiverBandMod::ReceiverBand band)
       * Optimize the LO PA for both polarization according to the values
       * found in the TMCDB.
       * @param ReceiverBandMod::ReceiverBand band
       * @except ControlExceptions::CAMBErrorEx
       * @except ControlExceptions::INACTErrorEx
       * @except FrontEndExceptions::OptimizationFailedEx
       */
    virtual void optimizeLOAmp(ReceiverBandMod::ReceiverBand band);

      /**
       * void setLPRTunning(in boolean enable)
       * Set the status of the LPR optimization flag. By default its false
       * which means there is no LPR tuning.
       * @param boolean enable
       */
    void setLPRTunning(bool enable);

      /**
       * boolean getLPRTunning()
       * Get the status of the LPR tuning flag.
       * @return boolean
       */
    bool getLPRTunning();

    /// See the IDL for description of this method
    void setCalibrationDeviceBand(CalibrationDeviceMod::CalibrationDevice cd,
                                  ReceiverBandMod::ReceiverBand band);

    /// See the IDL for description of this method
    void setCalibrationDevice(CalibrationDeviceMod::CalibrationDevice cd);

    /// See the IDL for description of this method
    CalibrationDeviceMod::CalibrationDevice getCurrentCalibrationDevice();

    /// See the IDL for description of this method
    virtual void enableCalDeviceDataReporting(bool enable);

    /// See the IDL for description of this method
    bool calDeviceDataReportingEnabled();

    /// See the IDL for description of this method
    void mixerDeflux(ReceiverBandMod::ReceiverBand band,
                     float magnetCurrentPulseLenght,
                     float Imax,
                     float heatherPulseLenght);

    void clearCache();
    void dumpCache();
    void enableCache(bool enable);

    void queueFrequencies(const Control::FrontEnd::SubscanInformationSeq &frequencies);

    void lockFrontEnd();

    void abortFrequencyChanges();

        /// See the IDL file for a description of this function.
    ACS::TimeIntervalSeq* timeToTune(const Control::FrontEnd::SubscanInformationSeq &frequencies);

    /// See the IDL file for a description of this function.
    ACS::TimeIntervalSeq* timeToTuneACD(const Control::FrontEnd::SubscanInformationSeq &frequencies);

    /* -------------------- Helper methods ----------------------*/

    private:
    void enableCartrige(ReceiverBandMod::ReceiverBand band);

    void disableCartrige(ReceiverBandMod::ReceiverBand band);


    double getLO1FrequencyInternal();

    double getStandbyLO1FrequencyInternal(ReceiverBandMod::ReceiverBand band);


    /**
     * void setLO1FrequencyAction(ReceiverBand band,
     *                             CORBA::Double frontEndFreq,
     *                             NetSidebandMod::NetSideband sideband,
     *                             bool lockFlag)
     * Sets the LO1 frequency of a band, optionally lock
     * @param ReceiverBand band
     * @param CORBA::Double frontEndFreq
     * @param NetSidebandMod::NetSideband sideband
     * @param bool lockFlag
     * @except ControlExceptions::IllegalParameterErrorExImpl
     * @except ControlExceptions::InvalidRequestExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except ControlExceptions::CAMBErrorExImpl
     * @except WCAExceptions::InsufficientPowerExImpl
     * @except WCAExceptions::LockFailedExImpl
     * @except FrontEndExceptions::BandNotSupportedExImpl
     * @except FrontEndExceptions::OptimizationFailedExImpl
     */
    void setLO1FrequencyAction(ReceiverBandMod::ReceiverBand band,
				CORBA::Double frontEndFreq,
				NetSidebandMod::NetSideband sideband,
				bool lockFlag);

    /**
     * void setLO1FrequencyFromCache(ReceiverBand band,
     *                             CORBA::Double frontEndFreq,
     *                             NetSidebandMod::NetSideband sideband,
     *                             bool lockFlag)
     * Sets the LO1 frequency of a band from a cache, optionally lock
     * @param ReceiverBand band
     * @param CORBA::Double frontEndFreq
     * @param NetSidebandMod::NetSideband sideband
     * @param bool lockFlag
     * @except ControlExceptions::IllegalParameterErrorExImpl
     * @except ControlExceptions::InvalidRequestExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except ControlExceptions::CAMBErrorExImpl
     * @except WCAExceptions::InsufficientPowerExImpl
     * @except WCAExceptions::LockFailedExImpl
     * @except FrontEndExceptions::BandNotSupportedExImpl
     * @except FrontEndExceptions::OptimizationFailedExImpl
     */
    void setLO1FrequencyFromCache(ReceiverBandMod::ReceiverBand band,
				CORBA::Double frontEndFreq,
				NetSidebandMod::NetSideband sideband,
				bool lockFlag);
    /**
     * void optimizeLOAmpInternal(ReceiverBandMod::ReceiverBand band)
     * protected method to optimize the LO amplifier
     * @param ReceiverBandMod::ReceiverBand band
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except FrontEndExceptions::OptimizationFailedExImpl
     */
    void optimizeLOAmpInternal(ReceiverBandMod::ReceiverBand band);

    /**
     * void optimizeSinglePolInternal(ReceiverBandMod::ReceiverBand band , int pol ,
     *                              float& VD, float& targetIJ)
     * optimize the LO power amplifier drain voltage setting to achieve a 
     * target junction voltage.
     * 'pol' controls which polarization to optimize
     * 'VD' is the drain voltage to adjust. If VD is zero, it will read what is
     * set in the WCA and use that value.
     * 'targetIJ1' is the optimal junction current for SIS1.  SIS2 is 
     * allowed to fall where it may.
     * @param ReceiverBandMod::ReceiverBand band
     * @param int pol
     * @param float& VD
     * @param float& targetIJ
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except FrontEndExceptions::OptimizationFailedExImpl
     */
    void optimizeSinglePolInternal(ReceiverBandMod::ReceiverBand band , int pol , float& VD, float& targetIJ);


    void setCache(float EDFA_V, int coarseYTO);
    bool isCacheHit();
    std::vector<double> getCache();

    Control::FrontEnd::SubscanInformation getCurrentSubScanInfo();


    void controllerNotBandOperational();

    void controllerBandOperational();

    bool isCartrigeEnabled(const std::map<std::string, Control::ControlDevice_ptr>::iterator &subdeviceIterator);

    void setCalibrationDeviceBandInternal(
            CalibrationDeviceMod::CalibrationDevice cd,
            ReceiverBandMod::ReceiverBand band);

    void selectBandInternal(ReceiverBandMod::ReceiverBand);

    CalibrationDeviceMod::CalibrationDevice getCurrentCalibrationDeviceInternal();

    void lockFrontEndInternal();

    bool isLockedInternal();

    void relockInternal();

    NetSidebandMod::NetSideband getSidebandInternal();

    void powerUpBandInternal(ReceiverBandMod::ReceiverBand band);

    void powerOffBandInternal(ReceiverBandMod::ReceiverBand band);

    void powerOffAllBandsInternal();

    void throwIfNoBandSelected(const char *file, const int line, const char *function);

    void throwIfInShutdown(const char *file, const int line, const char *function);

    void throwIfBandNotPowered(ReceiverBandMod::ReceiverBand band, const char *file, const int line, const char *function);

    Control::FrontEnd::ReceiverBandSeq* getPoweredBandsInternal();

    void mixerDefluxInternal(ReceiverBandMod::ReceiverBand band,
            float magnetCurrentPulseLenght, float Imax, float heatherPulseLenght);

    void queueFrequenciesInternal(
            const Control::FrontEnd::SubscanInformationSeq &frequencies);

    void abortFrequencyChangesInternal();

    ACS::TimeIntervalSeq* timeToTuneInternal(
            const Control::FrontEnd::SubscanInformationSeq &frequencies);

    ACS::TimeIntervalSeq* timeToTuneACDInternal(
            const Control::FrontEnd::SubscanInformationSeq &frequencies);
    private :

    // Currently available receiver bands
    set<ReceiverBandMod::ReceiverBand>   availableBandsList_m;

    // Currently powered-up receiver bands
    set<ReceiverBandMod::ReceiverBand>   poweredBandsList_m;

    // Currently selected band
    ReceiverBandMod::ReceiverBand           currentBand_m;

    std::vector<Control::FrontEnd::SubscanInformation> subscanInformationVector;

    bool enableLPRTunning_m;

    bool useCache_m;

    double nominalFrequency_m;

    std::map<int, std::vector<double> > lockCache_m;

    ACE_Mutex subscanInformationVectorMutex;

    ACE_RW_Mutex cleanUpMutex_m;

    bool cleanUpCalled;

};

#endif // FrontEndImpl_H
