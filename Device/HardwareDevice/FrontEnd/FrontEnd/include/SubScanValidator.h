/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FrontEndImpl.cpp
 *
 * $Id$
 */
#ifndef SUBSCANVALIDATOR_H_
#define SUBSCANVALIDATOR_H_

#include <vector>

#include "FrontEndImpl.h"
#include <almaEnumerations_IFC.h>
#include <acstime.h>


class SubScanValidator {
public:
    SubScanValidator(std::set<ReceiverBandMod::ReceiverBand>   poweredBands);
    void checkParameterTime(const ACS::Time startTime);
    void checkParameterBand(const ReceiverBandMod::ReceiverBand band);
    void checkParameterFrequencyForBand(const double photoRefFreq, const ReceiverBandMod::ReceiverBand band);
    void checkParameterCalibrationDevice(const CalibrationDeviceMod::CalibrationDevice acdState);

private:
    SubScanValidator();

    std::set<ReceiverBandMod::ReceiverBand> poweredBands;

};

#endif /* SUBSCANVALIDATOR_H_ */
