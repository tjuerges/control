
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCart7SimImpl.cpp
 *
 * $Id$
 */

#include "ColdCart7SimImpl.h"
#include <logging.h>

/**
 *-----------------------
 * ColdCart7 Constructor
 *-----------------------
 */
ColdCart7SimImpl::ColdCart7SimImpl(const ACE_CString& name, maci::ContainerServices* cs):
    ColdCart7SimBase::ColdCart7SimBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 *-----------------------
 * ColdCart7 Destructor
 *-----------------------
 */
ColdCart7SimImpl::~ColdCart7SimImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void ColdCart7SimImpl::hwSimulationAction()
    throw(CORBA::SystemException, ControlDeviceExceptions::HwLifecycleEx)
{
    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "ColdCart7SimImpl::hwSimulationAction"));

    ColdCart7Impl::hwOperationalAction();
}

///////////////////////////////////////
// Additional methods for ColdCart7
///////////////////////////////////////




















































































































































/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ColdCart7Impl)
