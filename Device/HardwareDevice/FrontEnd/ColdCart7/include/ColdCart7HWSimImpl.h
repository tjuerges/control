
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File ColdCart7HWSimImpl.h
 *
 * $Id$
 */
#ifndef ColdCart7HWSimImpl_H
#define ColdCart7HWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "ColdCart7HWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * ColdCart7HWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class ColdCart7HWSimImpl : public ColdCart7HWSimBase
  {
  public :
	ColdCart7HWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);
	
  }; // class ColdCart7HWSimImpl

} // namespace AMB

#endif // ColdCart7HWSimImpl_H
