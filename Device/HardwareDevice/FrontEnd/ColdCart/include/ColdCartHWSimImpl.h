
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCartHWSimImpl.h
 *
 * $Id$
 */
#ifndef ColdCartHWSimImpl_H
#define ColdCartHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "ColdCartHWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * ColdCartHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class ColdCartHWSimImpl : public ColdCartHWSimBase
  {
  public :
	ColdCartHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);


  protected:
    std::vector<CAN::byte_t> plusone(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1SisVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1SisVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1SisOpenLoop(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1SisOpenLoop(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1LnaEnable(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1LnaEnable(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1Lna1DrainVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1Lna1DrainVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1Lna1DrainCurrent(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1Lna1DrainCurrent(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1Lna2DrainVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1Lna2DrainVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1Lna2DrainCurrent(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1Lna2DrainCurrent(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1Lna3DrainVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1Lna3DrainVoltage(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0Sb1Lna3DrainCurrent(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1Sb1Lna3DrainCurrent(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol0LnaLedEnable(const std::vector<CAN::byte_t>& data);

    virtual void setControlSetPol1LnaLedEnable(const std::vector<CAN::byte_t>& data);

    //Temperature sensors

    virtual std::vector<CAN::byte_t> getMonitorTemperatureSensor0() const;

    virtual std::vector<CAN::byte_t> getMonitorTemperatureSensor1() const;

    virtual std::vector<CAN::byte_t> getMonitorTemperatureSensor2() const;

    virtual std::vector<CAN::byte_t> getMonitorTemperatureSensor3() const;

    virtual std::vector<CAN::byte_t> getMonitorTemperatureSensor4() const;

    virtual std::vector<CAN::byte_t> getMonitorTemperatureSensor5() const;

    private:
    unsigned int monitorPoint_m;
    unsigned int tcount_m;

    static const unsigned int TEMP_SENSOR0;
    static const unsigned int TEMP_SENSOR1;
    static const unsigned int TEMP_SENSOR2;
    static const unsigned int TEMP_SENSOR3;
    static const unsigned int TEMP_SENSOR4;
    static const unsigned int TEMP_SENSOR5;
    static const unsigned int TEMP_RETRY;

    float simulateTemp(unsigned int stage);
  }; // class ColdCartHWSimImpl

} // namespace AMB

#endif // ColdCartHWSimImpl_H
