#ifndef ColdCartImpl_H
#define ColdCartImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCartImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <ColdCartBase.h>

// Exceptions
#include <ColdCartExceptions.h>

// Band Enumeration
#include <ReceiverBand.h>

//CORBA Servants
#include <ColdCartS.h>

//FE Config
#include<configDataAccess.h>

class ColdCartImpl: public ColdCartBase,
                    virtual POA_Control::ColdCart
{
    public:
        /**
         * Constructor
         */
        ColdCartImpl(const ACE_CString& name, maci::ContainerServices* cs);


        /**
         * Destructor
         */
        virtual ~ColdCartImpl();
   
        /// IDL IMPLEMENTATION ///
        
        //@see IDL for this method description.
        void setConfigFreq(float freq_LO);

        //@see IDL for this method description.
        float getConfigFreq();

        //@see IDL for this method description.
        virtual void setBandLNABias();

        //@see IDL for this method description.
        virtual void setBandEnableLNABias(bool enable) ;

        //@see IDL for this method description.
        virtual void setBandSISBias(bool enable);

        //@see IDL for this method description.
        virtual void setBandSISMagnet(bool enable);

        //@see IDL for this method description.
        virtual void setBandOpenLoop(bool openLoop);

        //@see IDL for this method description.
        Control::ColdCart::IVData* getIVData(CORBA::Double startVoltage,
					     CORBA::Double endVoltage,
					     CORBA::Double step);

        //@see IDL for this method description.
        virtual float getTargetIJ1();

        //@see IDL for this method description.
        virtual float getTargetIJ2();

        //@see IDL for this method description.
        virtual void mixerDeflux(float magnetCurrentPulseLenght, 
                                 float Imax,
                                 float heatherPulseLenght);

    protected:
        /**
         * get the config for the LNA and store it in *row its a 6 float array
         * These values are acquired from the lookup tables.
         * @param int pol polarization
         * @param int sb side band
         * @return std::vector<double> 
         */
        std::vector<double> getConfigLNA(int pol, int sb);

        /**
         * get the config for the SIS Bias and store it in *row its a 4 float array
         * These values are acquired from the lookup tables.
         * @return std::vector<double> 
         */
        std::vector<double> getConfigSISBias();

        /**
         * < get the config for the SIS Magnets. Fill *row, its a 4 float array
         * These values are acquired from the lookup tables.
         * @return std::vector<double> 
         */
        std::vector<double> getConfigSisMag();

        /**
         * real action of defluxing the mixers.
         * Read the temperatures before starting, and zero out
         * the SIS mixers and magnets.
         * This method is only invoked for band 7 and 9
         * read section 4.4 of FEND-40.02.09.00-048-A3-MAN.
         * @see mixerDeflux verifies the band before invoking this action.
         * @param float magnetCurrentPulseLenght in seconds
         * @param float Imax in mA
         * @param float heatherPulseLenght in seconds
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */
        virtual void mixerDefluxAction(float magnetCurrentPulseLenght = 0.1,
                                       float Imax = 50,
                                       float heatherPulseLenght = 0.5);
        /**
         * Demagnetize
         * read section 4.4 of FEND-40.02.09.00-048-A3-MAN.
         * This method must be overloaded by the implementing band
         * 7 and 9 currently. Since 7 has 2 side band  and 9 has only one
         * The actual implementation is delegated to each coldcart implementation.
         * This causes some code duplication, but keeps the interface clean
         * and lean.
         * @param float magnetCurrentPulseLenght in seconds.
         * @param float Imax in mA
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */ 
        virtual void demagnetize(float magnetCurrentPulseLenght, float Imax);

        /**
         * Deflux. This does heat pulses and the waits for the ColdCart
         * to cool down again.
         * read section 4.4 of FEND-40.02.09.00-048-A3-MAN.
         * This method must be overloaded by the implementing band
         * 7 and 9 currently. Since 7 has 2 side band  and 9 has only one
         * The actual implementation is delegated to each coldcart implementation.
         * This causes some code duplication, but keeps the interface clean
         * and lean.
         * @param float heatherPulseLenght in seconds.
         * @param float initialTempPol0 in K.
         * @param float initialTempPol1 in K.
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */ 
        virtual void defluxPulse(float heatherPulseLenght, float initialTempPol0, 
                                 float initialTempPol1);
        /**
         * Set the LNA bias for a given pol and sb or for all LNAs simultaneously.
         * The corresponding setting is used from the configuration database (lookup
         * table). 
         * If either pol or sb is -1, then all LNAs in the cartridge will be set.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param int pol polarization [0:1]
         * @param int sb sideband [1:2]
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         * @exception ColdCartExceptions::MustOverloadExImpl
         */
        virtual void setLNABias(int pol = -1, int sb = -1);


        /**
         * Enable or disable the LNA for a given pol and sb or for all LNAs simultaneously.
         * If either pol or sb is -1, all LNAs are simultaneously affected.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param int pol polarization [0:1]
         * @param int sb sideband [1:2]
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         * @exception ColdCartExceptions::MustOverloadExImpl
         */
        virtual void setEnableLNABias(bool enable = false, int pol = -1, int sb = -1);


        /**
         * Enable/disable and set the SIS bias for a given pol and sb or for all 
         * SIS mixers simultaneously from the configuration database.
         * If openLoop is -1, no change is made to the SIS openLoop state. For 
         * openLoop = 1 the SIS openLoop state is enables for openLoop = 0 its disabled.
         * If either pol or sb is -1, then all junctions are biased according 
         * to the database.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param bool enable
         * @param int pol polarization [0:1]
         * @param int sb sideband [1:2]
         * @param int openLoop
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         * @exception ColdCartExceptions::MustOverloadExImpl
         */
        virtual void setSISBias(bool enable = false, int pol = -1, int sb = -1, 
                                int openLoop = -1);

        /**
         * Enable/disable and set the SIS magnet bias for a given pol and sb or for 
         * all SIS magnets simultaneously using the configuration database.
         * If either pol or sb is -1, then all magnets are biased according to the database.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands, and some don't have magnets.
         * @param bool enable
         * @param int pol polarization [0:1]
         * @param int sb sideband [1:2]
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         * @exception ColdCartExceptions::MustOverloadExImpl
         */
        virtual void setSISMagnet(bool enable = false, int pol = -1, int sb = -1);


        /**
         * Optimize the SIS magnet bias current for a given pol and sb or 
         * for all SIS magnets simultaneously.
         * If VJWindow is non-zero, an I-V curve will be swept for VJ in 
         * (-VJWindow...+VJwindow) to find the minimum Josephson current.  
         * Otherwise VJ will be set to 0.
         * If either pol or sb is -1, then all magnets are optimized in sequence.
         * TODO: This routine needs more testing and improvement.  It has only 
         * been seen to work once on band 7. 
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands, and some don't have magnets.
         * @param int pol polarization [0:1]
         * @param int sb sideband [1:2]
         * @param float VJWindow in volts
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         * @exception ColdCartExceptions::MustOverloadExImpl
         */
        virtual void adjustSISMagnet(int pol = -1, int sb = -1, float VJWindow = 2.0);

        
        /**
         * Set all SIS voltages; both polarizations and bot sidebands to the specified 
         * values.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param float voltage in volts.
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */
        virtual void setSisVoltage(const float voltage);
         
        /**
         * Set all SIS voltages; both polarizations and bot sidebands to the values
         * specified int the vector. vector[0] pol0 sb 1, vector [1] pol0 sb 2.. etc.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param std::vector< float >& voltage.
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */
        virtual void setSisVoltage(const std::vector< float >& voltage);
         
        /**
         * Get all SIS voltages; both polarizations and bot sidebands values.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param std::vector< float >& voltage with the read values.
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */
        virtual void getSisVoltage(std::vector< float >& voltage);
         
        /**
         * Set all SIS Magnet currents; both polarizations and bot sidebands to the specified 
         * value.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands, and some don't have magnets.
         * @param float current in Amps.
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */
        virtual void setSisMagnetCurrent(const float current);
         
        /**
         * Set all SIS Magnet currents; both polarizations and bot sidebands to the values
         * specified int the vector. vector[0] pol0 sb 1, vector [1] pol0 sb 2.. etc.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param std::vector< float >& current.
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */
        virtual void setSisMagnetCurrent(const std::vector< float >& current);

        /**
         * Get all SIS currents; both polarizations and bot sidebands values.
         * This method must be overridden by the implementing coldcarts since
         * the have different amount of sidebands.
         * @param std::vector< float >& currents with the read values.
         * @exception ControlExceptions::CAMBErrorExImpl
         * @exception ControlExceptions::INACTErrorExImpl
         * @exception ControlExceptions::IllegalParameterErrorExImpl
         */
        virtual void getSisCurrent(std::vector< float >& current);
         
        //HACK that overload the temperature readout until FEND firmware gets it together.
        virtual float getTemperatureSensor0(ACS::Time& timestamp);

        virtual float getTemperatureSensor1(ACS::Time& timestamp);
            
        virtual float getTemperatureSensor2(ACS::Time& timestamp);

        virtual float getTemperatureSensor3(ACS::Time& timestamp);

        virtual float getTemperatureSensor4(ACS::Time& timestamp);
            
        virtual float getTemperatureSensor5(ACS::Time& timestamp);
            
        //Receiver band cache variable. 
        ReceiverBand band_m;
        float nominalFrequency_m;

        
        //HACK that overload the temperature readout until FEND firmware gets it together.
        float temp0cache_m;
        float temp1cache_m;
        float temp2cache_m;
        float temp3cache_m;
        float temp4cache_m;
        float temp5cache_m;

};
#endif // ColdCartImpl_H
