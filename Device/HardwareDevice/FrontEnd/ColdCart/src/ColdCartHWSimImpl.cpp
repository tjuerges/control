
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCartHWSimImpl.cpp
 *
 * $Id$
 */

#include "ColdCartHWSimImpl.h"
#include <cstdlib>

using namespace AMB;

const unsigned int ColdCartHWSimImpl::TEMP_SENSOR0 = 0x01;
const unsigned int ColdCartHWSimImpl::TEMP_SENSOR1 = 0x02;
const unsigned int ColdCartHWSimImpl::TEMP_SENSOR2 = 0x04;
const unsigned int ColdCartHWSimImpl::TEMP_SENSOR3 = 0x08;
const unsigned int ColdCartHWSimImpl::TEMP_SENSOR4 = 0x10;
const unsigned int ColdCartHWSimImpl::TEMP_SENSOR5 = 0x20;
const unsigned int ColdCartHWSimImpl::TEMP_RETRY = 0; //Change to > 0 to enable broken behavoir

/* Please use this class to implement complex functionality for the
 * ColdCartHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

ColdCartHWSimImpl::ColdCartHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: ColdCartHWSimBase::ColdCartHWSimBase(node, serialNumber),
        monitorPoint_m(0),
        tcount_m(0)
{
    initialize(node, serialNumber);
}

std::vector<CAN::byte_t> ColdCartHWSimImpl::plusone(const std::vector<CAN::byte_t>& data){
   std::vector<CAN::byte_t> data2(data);
   data2.push_back(0);
   return data2;   
}
// Specific Control set helpers
// ----------------------------------------------------------------------------

void ColdCartHWSimImpl::setControlSetPol0Sb1SisVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB1_SIS_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB1_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_SIS_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_SIS_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_SIS_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_SIS_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1SisVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB1_SIS_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB1_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_SIS_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_SIS_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_SIS_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_SIS_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1SisOpenLoop(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL0_SB1_SIS_OPEN_LOOP");
	if (state_m.find(controlPoint_POL0_SB1_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_SIS_OPEN_LOOP)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_SIS_OPEN_LOOP. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_SIS_OPEN_LOOP)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_SIS_OPEN_LOOP. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1SisOpenLoop(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL1_SB1_SIS_OPEN_LOOP");
	if (state_m.find(controlPoint_POL1_SB1_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_SIS_OPEN_LOOP)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_SIS_OPEN_LOOP. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_SIS_OPEN_LOOP)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_SIS_OPEN_LOOP. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1LnaEnable(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL0_SB1_LNA_ENABLE");
	if (state_m.find(controlPoint_POL0_SB1_LNA_ENABLE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_LNA_ENABLE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_LNA_ENABLE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_LNA_ENABLE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_LNA_ENABLE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_LNA_ENABLE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1LnaEnable(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL1_SB1_LNA_ENABLE");
	if (state_m.find(controlPoint_POL1_SB1_LNA_ENABLE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_LNA_ENABLE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_LNA_ENABLE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_LNA_ENABLE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_LNA_ENABLE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_LNA_ENABLE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1Lna1DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB1_LNA1_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB1_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_LNA1_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_LNA1_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_LNA1_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_LNA1_DRAIN_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1Lna1DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB1_LNA1_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB1_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_LNA1_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_LNA1_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_LNA1_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_LNA1_DRAIN_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1Lna1DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB1_LNA1_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL0_SB1_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_LNA1_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_LNA1_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_LNA1_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_LNA1_DRAIN_CURRENT. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1Lna1DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB1_LNA1_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL1_SB1_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_LNA1_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_LNA1_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_LNA1_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_LNA1_DRAIN_CURRENT. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1Lna2DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB1_LNA2_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB1_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_LNA2_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_LNA2_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_LNA2_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_LNA2_DRAIN_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1Lna2DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB1_LNA2_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB1_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_LNA2_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_LNA2_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_LNA2_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_LNA2_DRAIN_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1Lna2DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB1_LNA2_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL0_SB1_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_LNA2_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_LNA2_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_LNA2_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_LNA2_DRAIN_CURRENT. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1Lna2DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB1_LNA2_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL1_SB1_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_LNA2_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_LNA2_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_LNA2_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_LNA2_DRAIN_CURRENT. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1Lna3DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB1_LNA3_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB1_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_LNA3_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_LNA3_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_LNA3_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_LNA3_DRAIN_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1Lna3DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB1_LNA3_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB1_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_LNA3_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_LNA3_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_LNA3_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_LNA3_DRAIN_VOLTAGE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0Sb1Lna3DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB1_LNA3_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL0_SB1_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB1_LNA3_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB1_LNA3_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB1_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB1_LNA3_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB1_LNA3_DRAIN_CURRENT. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1Sb1Lna3DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB1_LNA3_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL1_SB1_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB1_LNA3_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB1_LNA3_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB1_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB1_LNA3_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB1_LNA3_DRAIN_CURRENT. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol0LnaLedEnable(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL0_LNA_LED_ENABLE");
	if (state_m.find(controlPoint_POL0_LNA_LED_ENABLE) != state_m.end())
		*(state_m.find(controlPoint_POL0_LNA_LED_ENABLE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_LNA_LED_ENABLE. Member not found.");
	if (state_m.find(monitorPoint_POL0_LNA_LED_ENABLE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_LNA_LED_ENABLE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_LNA_LED_ENABLE. Member not found.");
}

void ColdCartHWSimImpl::setControlSetPol1LnaLedEnable(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL1_LNA_LED_ENABLE");
	if (state_m.find(controlPoint_POL1_LNA_LED_ENABLE) != state_m.end())
		*(state_m.find(controlPoint_POL1_LNA_LED_ENABLE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_LNA_LED_ENABLE. Member not found.");
	if (state_m.find(monitorPoint_POL1_LNA_LED_ENABLE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_LNA_LED_ENABLE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_LNA_LED_ENABLE. Member not found.");
}

/**
* Temperature behaviour, this means you must retry 10 times before getting a reading.
*/

/**
* Stage 4k temperature sensor.
*/
float ColdCartHWSimImpl::simulateTemp(unsigned int stage){
    float origin = 0.0;
    float span = 0.0;
    switch(stage){
        case TEMP_SENSOR0:
            span = (4.0 - 1.6);
            origin = 1.6;
            break;
        case TEMP_SENSOR1:
            span = (130.0 - 70.0);
            origin = 70.0;
            break;
        case TEMP_SENSOR2:
            span = (4.5 - 1.6);
            origin = 1.6;
            break;
        case TEMP_SENSOR3:
            span = 0;
            origin =  0;
            break;
        case TEMP_SENSOR4:
            span = (18.0 - 10.0);
            origin =  10.0;
            break;
        case TEMP_SENSOR5:
            span = (4.5 - 1.6);
            origin = 1.6;
            break;
    }
    float jitter = float(rand()) / (float(RAND_MAX) + 1.0);
    return jitter*span + origin;

}

std::vector<CAN::byte_t> ColdCartHWSimImpl::getMonitorTemperatureSensor0() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    ColdCartHWSimImpl* cch = const_cast<ColdCartHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP_SENSOR0);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR0.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP_SENSOR0) == 0){
        cch->monitorPoint_m = TEMP_SENSOR0;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMPERATURE_SENSOR0) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMPERATURE_SENSOR0)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMPERATURE_SENSOR0. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> ColdCartHWSimImpl::getMonitorTemperatureSensor1() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    ColdCartHWSimImpl* cch = const_cast<ColdCartHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP_SENSOR1);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR1.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP_SENSOR1) == 0){
        cch->monitorPoint_m = TEMP_SENSOR1;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMPERATURE_SENSOR1) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMPERATURE_SENSOR1)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMPERATURE_SENSOR1. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> ColdCartHWSimImpl::getMonitorTemperatureSensor2() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    ColdCartHWSimImpl* cch = const_cast<ColdCartHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP_SENSOR2);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR2.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP_SENSOR2) == 0){
        cch->monitorPoint_m = TEMP_SENSOR2;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMPERATURE_SENSOR2) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMPERATURE_SENSOR2)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMPERATURE_SENSOR2. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> ColdCartHWSimImpl::getMonitorTemperatureSensor3() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
     /* This sensor is the spare an should return erro 251 */
    ColdCartHWSimImpl* cch = const_cast<ColdCartHWSimImpl*> (this);
    //float temp = cch->simulateTemp(TEMP_SENSOR3);
    long long raw = 0LL;
    raw = static_cast<long long>(0);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR3.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
/*    if((monitorPoint_m & TEMP_SENSOR3) == 0){
        cch->monitorPoint_m = TEMP_SENSOR3;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }*/
    vvalue->push_back(251);//Hardware Convertion Error -5. (complement 2)
    if (state_m.find(monitorPoint_TEMPERATURE_SENSOR3) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMPERATURE_SENSOR3)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMPERATURE_SENSOR3. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> ColdCartHWSimImpl::getMonitorTemperatureSensor4() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    ColdCartHWSimImpl* cch = const_cast<ColdCartHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP_SENSOR4);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR4.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP_SENSOR4) == 0){
        cch->monitorPoint_m = TEMP_SENSOR4;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMPERATURE_SENSOR4) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMPERATURE_SENSOR4)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMPERATURE_SENSOR4. Member not found.");
    return *vvalue;
}

std::vector<CAN::byte_t> ColdCartHWSimImpl::getMonitorTemperatureSensor5() const {
    std::vector<CAN::byte_t>* vvalue = new std::vector<CAN::byte_t>();
    /**
    * Ninja stuff. Sombody had the bright idea to do this method const, but i
    * need to fidle with stuff to do proper simulation. To achieve that a need
    * a pointer to this, which is not const and operate over it when necesary.
    */
    ColdCartHWSimImpl* cch = const_cast<ColdCartHWSimImpl*> (this);
    float temp = cch->simulateTemp(TEMP_SENSOR5);
    long long raw = 0LL;
    raw = static_cast<long long>(temp);
    TypeConversion::valueToData(*vvalue, static_cast<float>(raw), 4);
    /**
    * monitorPoint_m will be masked against TEMP_SENSOR5.  If this monitor
    * moint was not the last to be monitored reset the counter. We have to
    * simulate the broken hardware, this mean the software MUST poll 10 times a
    * single temperature, before geting a result, and before moving on to the
    * next sensor.
    **/
    if((monitorPoint_m & TEMP_SENSOR5) == 0){
        cch->monitorPoint_m = TEMP_SENSOR5;
        cch->tcount_m = 0;
    }
    if(tcount_m < TEMP_RETRY){
        cch->tcount_m++;
        vvalue->push_back(250);//Retry code -6. (complement 2)
    }else{
        vvalue->push_back(0x0); //no error
    }
    if (state_m.find(monitorPoint_TEMPERATURE_SENSOR5) != state_m.end())
        *(cch->state_m.find(monitorPoint_TEMPERATURE_SENSOR5)->second) = *vvalue;
    else
        throw CAN::Error("monitorPoint_TEMPERATURE_SENSOR5. Member not found.");
    return *vvalue;
}

