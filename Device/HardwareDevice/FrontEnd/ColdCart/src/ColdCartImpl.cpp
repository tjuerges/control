/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCartImpl.cpp
 *
 * $Id$
 */

#include <ColdCartImpl.h>

/**
 *-----------------------
 * ColdCart Constructor
 *-----------------------
 */
ColdCartImpl::ColdCartImpl(const ACE_CString& name,
        maci::ContainerServices* cs):
    ColdCartBase(name,cs),
    band_m(ReceiverBandMod::UNSPECIFIED),
    nominalFrequency_m(0.0),
    temp0cache_m(-1.0),
    temp1cache_m(-1.0),
    temp2cache_m(-1.0),
    temp3cache_m(-1.0),
    temp4cache_m(-1.0),
    temp5cache_m(-1.0)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    assembly_m = "ColdCart";
    hasEsn_m = true;
}


/**
 *-----------------------
 * ColdCart Destructor
 *-----------------------
 */
ColdCartImpl::~ColdCartImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

///////////////////////////////////////
// Additional methods for ColdCart
///////////////////////////////////////

void ColdCartImpl::setConfigFreq(float freq_LO){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    nominalFrequency_m = freq_LO;
}

float ColdCartImpl::getConfigFreq(){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    return nominalFrequency_m;
}

std::vector<double> ColdCartImpl::getConfigLNA(int pol, int sb) {

    AUTO_TRACE(__PRETTY_FUNCTION__);
    double freq = nominalFrequency_m;
    int index=-1;
    std::vector<double> retVal(0);
    std::stringstream tmp;
    tmp<<"PreampParamsPol"<<pol<<"Sb"<<sb;
    try {
        //get the list of PowerAmp configurations
        std::auto_ptr<std::vector<ControlXmlParser> > list = configData_m->getElements(tmp.str().c_str());
        //if we have only one row don't bother searching.
        if((*list).size() == 1) {
            index=0;
        }
        //Check for border conditions.
        if(freq < (*list)[0].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=0;
        }
        if(freq > (*list)[(*list).size()-1].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=(*list).size()-1;
        }
        //not a single row and not a border condition. This gets messy since
        //we have to interpolate.
        for(unsigned int i=1; i < (*list).size(); i++){
            if (index!=-1)break;
            if(freq == (*list)[i].getDoubleAttribute("FreqLO")){
                //found an exact match.
                index=i;
                break;
            }
            if(freq < (*list)[i].getDoubleAttribute("FreqLO")){
                //OK. we have to interpolate this row with the previous one
                double freqSpread = -1;
                double freqE = 0;
                double slope, desp;
                freqSpread = ((*list)[i].getDoubleAttribute("FreqLO") - (*list)[i -1 ].getDoubleAttribute("FreqLO"));
                freqE = (*list)[i].getDoubleAttribute("FreqLO");
//<PreampParamsPol0Sb1 FreqLO="283" VD1="0.6" VD2="0.5" VD3="1.2" ID1="4" ID2="3" ID3="4" VG1="-1.87" VG2="-1.58" VG3="-2.56" /
                //VD1
                slope = ((*list)[i].getDoubleAttribute("VD1") - (*list)[i-1].getDoubleAttribute("VD1")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VD1") - (freqE * slope);
                retVal.push_back(freq * slope + desp);
                //VD2
                slope = ((*list)[i].getDoubleAttribute("VD2") - (*list)[i-1].getDoubleAttribute("VD2")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VD2") - (freqE * slope);
                retVal.push_back(freq * slope + desp);
                //VD3
                slope = ((*list)[i].getDoubleAttribute("VD3") - (*list)[i-1].getDoubleAttribute("VD3")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VD3") - (freqE * slope);
                retVal.push_back(freq * slope + desp);
                //ID1
                slope = ((*list)[i].getDoubleAttribute("ID1") - (*list)[i-1].getDoubleAttribute("ID1")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("ID1") - (freqE * slope);
                retVal.push_back((freq * slope + desp) * 1.0E-3 );
                //ID2
                slope = ((*list)[i].getDoubleAttribute("ID2") - (*list)[i-1].getDoubleAttribute("ID2")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("ID2") - (freqE * slope);
                retVal.push_back((freq * slope + desp) * 1.0E-3);
                //ID3
                slope = ((*list)[i].getDoubleAttribute("ID3") - (*list)[i-1].getDoubleAttribute("ID3")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("ID3") - (freqE * slope);
                retVal.push_back((freq * slope + desp) * 1E-3);
                //VG1
                slope = ((*list)[i].getDoubleAttribute("VG1") - (*list)[i-1].getDoubleAttribute("VG1")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VG1") - (freqE * slope);
                retVal.push_back(freq * slope + desp);
                //VG2
                slope = ((*list)[i].getDoubleAttribute("VG2") - (*list)[i-1].getDoubleAttribute("VG2")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VG2") - (freqE * slope);
                retVal.push_back(freq * slope + desp);
                //VG3
                slope = ((*list)[i].getDoubleAttribute("VG3") - (*list)[i-1].getDoubleAttribute("VG3")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VG3") - (freqE * slope);
                retVal.push_back(freq * slope + desp);
                break;
            }
        }
        if(index!=-1){
            retVal.push_back((*list)[index].getDoubleAttribute("VD1"));
            retVal.push_back((*list)[index].getDoubleAttribute("VD2"));
            retVal.push_back((*list)[index].getDoubleAttribute("VD3"));
            retVal.push_back((*list)[index].getDoubleAttribute("ID1")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("ID2")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("ID3")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("VG1"));
            retVal.push_back((*list)[index].getDoubleAttribute("VG2"));
            retVal.push_back((*list)[index].getDoubleAttribute("VG3"));
        }
    } catch(ControlExceptions::XmlParserErrorExImpl &ex) {
        //more data?
        throw ex;
    }
    return retVal;
}

std::vector<double> ColdCartImpl::getConfigSISBias(){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    double freq = nominalFrequency_m;
    int index=-1;
    std::vector<double> retVal(0);
    try {
        //get the list of Mixer configurations
        std::auto_ptr<std::vector<ControlXmlParser> > list = configData_m->getElements("MixerParams");
        //if we have only one row don't bother searching.
        if((*list).size() == 1) {
            index=0;
        }
        //Check for border conditions.
        if(freq < (*list)[0].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=0;
        }
        if(freq > (*list)[(*list).size()-1].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=(*list).size()-1;
        }
        //not a single row and not a border condition. This gets messy since
        //we have to interpolate.
        for(unsigned int i=1; i < (*list).size(); i++){
            if (index!=-1)break;
            if(freq == (*list)[i].getDoubleAttribute("FreqLO")){
                //found an exact match.
                index=i;
                break;
            }
            if(freq < (*list)[i].getDoubleAttribute("FreqLO")){
                //OK. we have to interpolate this row with the previous one
                double freqSpread = -1;
                double freqE = 0;
                double slope, desp;
                freqSpread = ((*list)[i].getDoubleAttribute("FreqLO") - (*list)[i -1 ].getDoubleAttribute("FreqLO"));
                freqE = (*list)[i].getDoubleAttribute("FreqLO");
                 //FreqLO VJ01 VJ02 VJ11 VJ12 IJ01 IJ02 IJ11 IJ12
                //VJ01
                slope = ((*list)[i].getDoubleAttribute("VJ01") - (*list)[i-1].getDoubleAttribute("VJ01")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VJ01") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);
                //VJ02
                slope = ((*list)[i].getDoubleAttribute("VJ02") - (*list)[i-1].getDoubleAttribute("VJ02")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VJ02") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);
                //VJ11
                slope = ((*list)[i].getDoubleAttribute("VJ11") - (*list)[i-1].getDoubleAttribute("VJ11")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VJ11") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);
                //VJ12
                slope = ((*list)[i].getDoubleAttribute("VJ12") - (*list)[i-1].getDoubleAttribute("VJ12")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("VJ12") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);

                //IJ01
                slope = ((*list)[i].getDoubleAttribute("IJ01") - (*list)[i-1].getDoubleAttribute("IJ01")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IJ01") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-6);
                //IJ02
                slope = ((*list)[i].getDoubleAttribute("IJ02") - (*list)[i-1].getDoubleAttribute("IJ02")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IJ02") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-6);
                //IJ11
                slope = ((*list)[i].getDoubleAttribute("IJ11") - (*list)[i-1].getDoubleAttribute("IJ11")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IJ11") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-6);
                //IJ12
                slope = ((*list)[i].getDoubleAttribute("IJ12") - (*list)[i-1].getDoubleAttribute("IJ12")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IJ12") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-6);
                break;
            }
        }
        if(index!=-1){
                 //FreqLO VJ01 VJ02 VJ11 VJ12 IJ01 IJ02 IJ11 IJ12
            retVal.push_back((*list)[index].getDoubleAttribute("VJ01")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("VJ02")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("VJ11")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("VJ12")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("IJ01")*1E-6);
            retVal.push_back((*list)[index].getDoubleAttribute("IJ02")*1E-6);
            retVal.push_back((*list)[index].getDoubleAttribute("IJ11")*1E-6);
            retVal.push_back((*list)[index].getDoubleAttribute("IJ12")*1E-6);
        }
    } catch(ControlExceptions::XmlParserErrorExImpl &ex) {
        //more data?
        throw ex;
    }
    return retVal;
}

std::vector<double> ColdCartImpl::getConfigSisMag(){
    AUTO_TRACE(__PRETTY_FUNCTION__);
    double freq = nominalFrequency_m;
    int index=-1;
    std::vector<double> retVal(0);
    try {
        //get the list of PowerAmp configurations
        std::auto_ptr<std::vector<ControlXmlParser> > list = configData_m->getElements("MagnetParams");
        //if we have only one row don't bother searching.
        if((*list).size() == 1) {
            index=0;
        }
        //Check for border conditions.
        if(freq < (*list)[0].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=0;
        }
        if(freq > (*list)[(*list).size()-1].getDoubleAttribute("FreqLO")) {
            //no search needed, return index.
            index=(*list).size()-1;
        }
        //not a single row and not a border condition. This gets messy since
        //we have to interpolate.
        for(unsigned int i=1; i < (*list).size(); i++){
            if (index!=-1)break;
            if(freq == (*list)[i].getDoubleAttribute("FreqLO")){
                //found an exact match.
                index=i;
                break;
            }
            if(freq < (*list)[i].getDoubleAttribute("FreqLO")){
                //OK. we have to interpolate this row with the previous one
                double freqSpread = -1;
                double freqE = 0;
                double slope, desp;
                freqSpread = ((*list)[i].getDoubleAttribute("FreqLO") - (*list)[i -1 ].getDoubleAttribute("FreqLO"));
                freqE = (*list)[i].getDoubleAttribute("FreqLO");
                //FreqLO, IMag01, IMag02, IMag11, IMag12
                //IMag01
                slope = ((*list)[i].getDoubleAttribute("IMag01") - (*list)[i-1].getDoubleAttribute("IMag01")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IMag01") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);
                //IMag02
                slope = ((*list)[i].getDoubleAttribute("IMag02") - (*list)[i-1].getDoubleAttribute("IMag02")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IMag02") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);
                //IMag11
                slope = ((*list)[i].getDoubleAttribute("IMag11") - (*list)[i-1].getDoubleAttribute("IMag11")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IMag11") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);
                //IMag12
                slope = ((*list)[i].getDoubleAttribute("IMag12") - (*list)[i-1].getDoubleAttribute("IMag12")) / freqSpread;
                desp =  (*list)[i].getDoubleAttribute("IMag12") - freqE * slope;
                retVal.push_back((freq * slope + desp)*1E-3);
                break;
            }
        }
        if(index!=-1){
            retVal.push_back((*list)[index].getDoubleAttribute("IMag01")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("IMag02")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("IMag11")*1E-3);
            retVal.push_back((*list)[index].getDoubleAttribute("IMag12")*1E-3);
        }
    } catch(ControlExceptions::XmlParserErrorExImpl &ex) {
        //more data?
        throw ex;
    }
    return retVal;
}


void ColdCartImpl::setBandLNABias()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        setLNABias(0,1);
        setLNABias(0,2);
        setLNABias(1,1);
        setLNABias(1,2);
    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (const ColdCartExceptions::MustOverloadExImpl& ex){
        ColdCartExceptions::MustOverloadExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getMustOverloadEx();
    }
}

void ColdCartImpl::setBandEnableLNABias(bool enable)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        setEnableLNABias(enable,0,1);
        setEnableLNABias(enable,0,2);
        setEnableLNABias(enable,1,1);
        setEnableLNABias(enable,1,2);
    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (const ColdCartExceptions::MustOverloadExImpl& ex){
        ColdCartExceptions::MustOverloadExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getMustOverloadEx();
    }
}

void ColdCartImpl::setBandSISBias(bool enable)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        setSISBias(enable, 0, 1, 0);//POL0, SB1, Close Loop
        setSISBias(enable, 0, 2, 0);//POL0, SB2, Close Loop
        setSISBias(enable, 1, 1, 0);//POL1, SB1, Close Loop
        setSISBias(enable, 1, 2, 0);//POL1, SB2, Close Loop
    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (const ColdCartExceptions::MustOverloadExImpl& ex){
        ColdCartExceptions::MustOverloadExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getMustOverloadEx();
    }
}

void ColdCartImpl::setBandSISMagnet(bool enable)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try{
        setSISMagnet(enable, 0, 1);
        setSISMagnet(enable, 0, 2);
        setSISMagnet(enable, 1, 1);
        setSISMagnet(enable, 1, 2);
    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (const ColdCartExceptions::MustOverloadExImpl& ex){
        ColdCartExceptions::MustOverloadExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getMustOverloadEx();
    }
}

void ColdCartImpl::setBandOpenLoop(bool openLoop)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try{
        setCntlPol0Sb1SisOpenLoop(openLoop);
        //setCntlPol0Sb2SisOpenLoop(openLoop);
        setCntlPol1Sb1SisOpenLoop(openLoop);
        //setCntlPol1Sb2SisOpenLoop(openLoop);
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
}

Control::ColdCart::IVData*
ColdCartImpl::getIVData(CORBA::Double startVoltage,
                        CORBA::Double endVoltage,
                        CORBA::Double step)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Note: for details on this measurement please refer to the
    // document "FE Control Software Procedures and Algorithms"
    // (FEND-40.00.00.00-089-A-MAN)

    int counter = 0;
    // Prepare output vectors
    std::vector< float > bias_voltage;
    std::vector< float > pol0_sb1_sis_voltage;
    std::vector< float > pol1_sb1_sis_voltage;
    std::vector< float > pol0_sb2_sis_voltage;
    std::vector< float > pol1_sb2_sis_voltage;
    std::vector< float > pol0_sb1_sis_current;
    std::vector< float > pol1_sb1_sis_current;
    std::vector< float > pol0_sb2_sis_current;
    std::vector< float > pol1_sb2_sis_current;

    try {
        // Store the current voltages in order to set the values
        // back at the end
        std::vector< float > sis_voltage_init;
        getSisVoltage(sis_voltage_init);

        // We check briefly that the parameters are within range:
        // -20 mV and 20 mV, step minimum 0.005 mV
        if (((startVoltage < -0.02) || (startVoltage > 0.02))
            || ((endVoltage < -0.02) || (endVoltage > 0.02))
            || (startVoltage >= endVoltage)
            || (step < 0.000005)) {
            ControlExceptions::IllegalParameterErrorExImpl
                ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            string msg = "One or more of the given parameters is out of range";
            ex.addData("Detail", msg);
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }

        bool firstTime = true;
        // Step over given voltage range in POSITIVE direction first
        float voltage = (float)startVoltage;
        while (voltage <= (float)endVoltage) {
            // Set all the SIS voltages
            setSisVoltage(voltage);
            // We give some time to stabilize (1ms)
            if (firstTime){
                usleep(100000);
                firstTime = false;
            } else {
                usleep(1000);
            }
            std::vector< float > sis_voltage_value;
            std::vector< float > sis_current_value;

            // Read back the voltages to make sure to get real values
            getSisVoltage(sis_voltage_value);
            // Read the SIS currents
            getSisCurrent(sis_current_value);

            // Add the values to the vectors
            bias_voltage.push_back(voltage);
            pol0_sb1_sis_voltage.push_back(sis_voltage_value[0]);
            pol1_sb1_sis_voltage.push_back(sis_voltage_value[1]);
            pol0_sb2_sis_voltage.push_back(sis_voltage_value[2]);
            pol1_sb2_sis_voltage.push_back(sis_voltage_value[3]);
            pol0_sb1_sis_current.push_back(sis_current_value[0]);
            pol1_sb1_sis_current.push_back(sis_current_value[1]);
            pol0_sb2_sis_current.push_back(sis_current_value[2]);
            pol1_sb2_sis_current.push_back(sis_current_value[3]);

            // Increase the voltage by "step"
            voltage = voltage + step;
            counter++;
        }

        // Now step over given voltage range in NEGATIVE direction
        voltage = (float)endVoltage;
        while (voltage >= (float)startVoltage) {
            // Set all the SIS voltages
            setSisVoltage(voltage);
            // We give some time to stabilize (1ms)
            usleep(1000);

            std::vector< float > sis_voltage_value;
            std::vector< float > sis_current_value;

            // Read back the voltages to make sure to get real values
            getSisVoltage(sis_voltage_value);
            // Read the SIS currents
            getSisCurrent(sis_current_value);

            // Add the values to the vectors
            bias_voltage.push_back(voltage);
            pol0_sb1_sis_voltage.push_back(sis_voltage_value[0]);
            pol1_sb1_sis_voltage.push_back(sis_voltage_value[1]);
            pol0_sb2_sis_voltage.push_back(sis_voltage_value[2]);
            pol1_sb2_sis_voltage.push_back(sis_voltage_value[3]);
            pol0_sb1_sis_current.push_back(sis_current_value[0]);
            pol1_sb1_sis_current.push_back(sis_current_value[1]);
            pol0_sb2_sis_current.push_back(sis_current_value[2]);
            pol1_sb2_sis_current.push_back(sis_current_value[3]);

            // Decrease the voltage by "step"
            voltage = voltage - step;
            counter++;
        }

        // Set the voltage values back to the initial ones
        setSisVoltage(sis_voltage_init);

    } catch (ControlExceptions::CAMBErrorExImpl &ex) {
        ControlExceptions::CAMBErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "CAMB error during I-V curve measurement, interrupting.";
        newEx.addData("Detail", msg);
        throw newEx.getCAMBErrorEx();
    } catch (ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl
          newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "State error during I-V curve measurement, interrupting.";
        newEx.addData("Detail", msg);
        throw newEx.getINACTErrorEx();
    }

  // Package the data into the IVData structure
    Control::ColdCart::IVData_var ivData =
    new Control::ColdCart::IVData;
    (*ivData).V.length(counter);
    (*ivData).IPo0Sb1.length(counter);
    (*ivData).VPo0Sb1.length(counter);
    (*ivData).IPo1Sb1.length(counter);
    (*ivData).VPo1Sb1.length(counter);
    (*ivData).IPo0Sb2.length(counter);
    (*ivData).VPo0Sb2.length(counter);
    (*ivData).IPo1Sb2.length(counter);
    (*ivData).VPo1Sb2.length(counter);

    for (int i = 0; i < counter; i++) {
        (*ivData).V[i] = bias_voltage[i];
        (*ivData).IPo0Sb1[i] = pol0_sb1_sis_current[i];
        (*ivData).VPo0Sb1[i] = pol0_sb1_sis_voltage[i];
        (*ivData).IPo1Sb1[i] = pol1_sb1_sis_current[i];
        (*ivData).VPo1Sb1[i] = pol1_sb1_sis_voltage[i];
        (*ivData).IPo0Sb2[i] = pol0_sb2_sis_current[i];
        (*ivData).VPo0Sb2[i] = pol0_sb2_sis_voltage[i];
        (*ivData).IPo1Sb2[i] = pol1_sb2_sis_current[i];
        (*ivData).VPo1Sb2[i] = pol1_sb2_sis_voltage[i];
    }
    return ivData._retn();
}

float ColdCartImpl::getTargetIJ1()
{
    std::vector<double> row;
    row = getConfigSISBias();
    //return the target Current for SIS SB1 in pol 0
    return row[4];
}

float ColdCartImpl::getTargetIJ2()
{
        std::vector<double> row;
    row = getConfigSISBias();
    //return the target Current for SIS SB1 in pol 1
    return row[6];
}

void ColdCartImpl::mixerDeflux (float magnetCurrentPulseLenght,
                                float Imax,
                                float heatherPulseLenght)
{
    try {
        //I hate doing this, but it will avoid code duplication.
        //This is only valid for band 7 and 9. Any other band
        //Will just return.
        switch(band_m) {
            case ReceiverBandMod::ALMA_RB_07:
            case ReceiverBandMod::ALMA_RB_09:
                mixerDefluxAction(magnetCurrentPulseLenght, Imax, heatherPulseLenght);
                break;
            default:
                //LOG something.
                break;
        }
    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (const ColdCartExceptions::MustOverloadExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
}

//PROTECTED METHODS
void ColdCartImpl::mixerDefluxAction(float magnetCurrentPulseLenght,
                                     float Imax,
                                     float heatherPulseLenght)
{
    float initialTempPol0 = -1;
    float initialTempPol1 = -1;
    ACS::Time  timestamp;

    try {
        //# getting initial temperatures
        initialTempPol0 = getTemperatureSensor0(timestamp);
        initialTempPol1 = getTemperatureSensor5(timestamp);
        //# setting the SIS Voltages to 0
        setSisVoltage(0.0);
        //# setting magnet current to 0
        setSisMagnetCurrent(0.0);

        //# --- Demagnetization  ---
        demagnetize(magnetCurrentPulseLenght, Imax);
        // Applying Defluxing pulse
        defluxPulse(heatherPulseLenght, initialTempPol0, initialTempPol1);

    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch (const ColdCartExceptions::MustOverloadExImpl& ex){
        ColdCartExceptions::MustOverloadExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void ColdCartImpl::demagnetize(float magnetCurrentPulseLenght, float Imax)
{
//Only interface. The specific band must overload.
//If not overloaded it will do nothing.
}

void ColdCartImpl::defluxPulse(float heatherPulseLenght, float initialTempPol0, float initialTempPol1)
{
//Only interface. The specific band must overload.
//If not overloaded it will do nothing.
}

void ColdCartImpl::setSisVoltage(const float voltage)
{
//Only interface. The specific band must overload.
}

void ColdCartImpl::setSisVoltage(const std::vector< float >& voltage)
{
//Only interface. The specific band must overload.
}

void ColdCartImpl::getSisVoltage(std::vector< float >& voltage)
{
//Only interface. The specific band must overload.
}

void ColdCartImpl::setSisMagnetCurrent(const float current)
{
//Only interface. The specific band must overload.
}

void ColdCartImpl::setSisMagnetCurrent(const std::vector< float >& current)
{
//Only interface. The specific band must overload.
}

void ColdCartImpl::getSisCurrent(std::vector< float >& current)
{
//Only interface. The specific band must overload.
}

void ColdCartImpl::setLNABias(int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ColdCartExceptions::MustOverloadExImpl ex(__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    ex.log();
    throw ex;
}

void ColdCartImpl::setEnableLNABias(bool enable, int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ColdCartExceptions::MustOverloadExImpl ex(__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    ex.log();
    throw ex;
}

void ColdCartImpl::setSISBias(bool enable, int pol, int sb, int openLoop)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ColdCartExceptions::MustOverloadExImpl ex(__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    ex.log();
    throw ex;
}

void ColdCartImpl::setSISMagnet(bool enable, int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ColdCartExceptions::MustOverloadExImpl ex(__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    ex.log();
    throw ex;
}

void ColdCartImpl::adjustSISMagnet(int pol, int, float VJWindow)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ColdCartExceptions::MustOverloadExImpl ex(__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    ex.log();
    throw ex;
}

//The following overload has to be done because the FE is broke and you have to
//try multiple times before you get a proper reading for the temperatures.
float ColdCartImpl::getTemperatureSensor0(ACS::Time& timestamp)
{
    int retry = 11;
    while(retry>0){
        try{
            temp0cache_m=ColdCartBase::getTemperatureSensor0(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250") && error!=ACE_CString("251")){
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                   __PRETTY_FUNCTION__);
                throw nex;
            }
            if(retry>0){
                //Chew the exception and read again
                retry--;
                continue;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp0cache_m;
}

float ColdCartImpl::getTemperatureSensor1(ACS::Time& timestamp)
{
    int retry = 11;
    while(retry>0){
        try{
            temp1cache_m=ColdCartBase::getTemperatureSensor1(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250") && error!=ACE_CString("251")){
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                   __PRETTY_FUNCTION__);
                throw nex;
            }
            if(retry>0){
                //Chew the exception and read again
                retry--;
                continue;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp1cache_m;
}

float ColdCartImpl::getTemperatureSensor2(ACS::Time& timestamp)
{
    int retry = 11;
    while(retry>0){
        try{
            temp2cache_m=ColdCartBase::getTemperatureSensor2(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250") && error!=ACE_CString("251")){
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                   __PRETTY_FUNCTION__);
                throw nex;
            }
            if(retry>0){
                //Chew the exception and read again
                retry--;
                continue;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp2cache_m;
}

//This sensor is a spare and return HW error 251. We wont poll it
float ColdCartImpl::getTemperatureSensor3(ACS::Time& timestamp)
{
//    int retry = 11;
//    while(retry>0){
//        try{
//            temp3cache_m=ColdCartBase::getTemperatureSensor3(timestamp);
//        }catch(ControlExceptions::CAMBErrorExImpl &ex){
//            ACE_CString error = ex.getData("ErrorCode");
//            if(error!=ACE_CString("250") && error!=ACE_CString("251")){
//                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
//                   __PRETTY_FUNCTION__);
//                throw nex;
//            }
//            if(retry>0){
//                //Chew the exception and read again
//                retry--;
//                continue;
//            }
//        }
//        //if everything is OK. break the loop.
//        break;
//    }
    return temp3cache_m;
}

float ColdCartImpl::getTemperatureSensor4(ACS::Time& timestamp)
{
    int retry = 11;
    while(retry>0){
        try{
            temp4cache_m=ColdCartBase::getTemperatureSensor4(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250") && error!=ACE_CString("251")){
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                   __PRETTY_FUNCTION__);
                throw nex;
            }
            if(retry>0){
                //Chew the exception and read again
                retry--;
                continue;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp4cache_m;
}

float ColdCartImpl::getTemperatureSensor5(ACS::Time& timestamp)
{
    int retry = 11;
    while(retry>0){
        try{
            temp5cache_m=ColdCartBase::getTemperatureSensor5(timestamp);
        }catch(ControlExceptions::CAMBErrorExImpl &ex){
            ACE_CString error = ex.getData("ErrorCode");
            if(error!=ACE_CString("250") && error!=ACE_CString("251")){
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                   __PRETTY_FUNCTION__);
                throw nex;
            }
            if(retry>0){
                //Chew the exception and read again
                retry--;
                continue;
            }
        }
        //if everything is OK. break the loop.
        break;
    }
    return temp5cache_m;
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ColdCartImpl)
