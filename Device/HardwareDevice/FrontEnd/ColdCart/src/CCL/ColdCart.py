#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a ColdCart device.
"""

import CCL.ColdCartBase
import StatusHelper
import os

class ColdCart(CCL.ColdCartBase.ColdCartBase):
    '''
    The ColdCart class inherits from the code generated ColdCartBase
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        CCL.ColdCartBase.ColdCartBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.ColdCartBase.ColdCartBase.__del__(self)

    def setConfigFreq(self, freq_LO):
        for key, val in self._devices.iteritems():
            try:
                self._devices[key].setConfigFreq(freq_LO)
            except:
                print "Couldn't apply setConfigFreq("+str(freq_LO)+") to "+key
    def setBandLNABias(self):
        for key, val in self._devices.iteritems():
            try:
                self._devices[key].setBandLNABias()
            except:
                print "Couldn't apply setBandLNABias() to "+key
    def setBandEnableLNABias(self, enable):
        for key, val in self._devices.iteritems():
            try:
                self._devices[key].setBandEnableLNABias(enable)
            except:
                print "Couldn't apply setBandEnableLNABias("+str(enable)+") to "+key
    def setBandSISBias(self, enable):
        for key, val in self._devices.iteritems():
            try:
                self._devices[key].setBandSISBias(enable)
            except:
                print "Couldn't apply setBandSISBias("+str(enable)+") to "+key
    def setBandSISMagnet(self, enable):
        for key, val in self._devices.iteritems():
            try:
                self._devices[key].setBandSISMagnet(enable)
            except:
                print "Couldn't apply setBandSISMagnet("+str(enable)+") to "+key

    def getIVData(self, startVoltage, endVoltage, step, outputFileName=None):
        '''
        This method returns an I-V dataset for the available polarizations and
        sidebands. The bias voltage is moved between the given startVoltage and
        endVoltage, increasing the voltage every time by the given step. Then
        this is repeated going in the negative direction in order to consider
        the hysteresis. Note that voltages should be within the range +/- 0.020 [V].

        Optionally the data can be also written to a text file, for this give
        the file name at the parameter outputFileName.

        This is the data structure that is returned:
        struct IVData
        {
          DoubleSeq V;
          DoubleSeq IPo0Sb1;
          DoubleSeq VPo0Sb1;
          DoubleSeq IPo1Sb1;
          DoubleSeq VPo1Sb1;
          DoubleSeq IPo0Sb2;
          DoubleSeq VPo0Sb2;
          DoubleSeq IPo1Sb2;
          DoubleSeq VPo1Sb2;
        };

        Examples of usage:
        >>> cc6=ColdCart6("DA41")
        >>> data=cc6.getIVData(-0.020, 0.020, 0.0001)
        >>> data=cc6.getIVData(-0.010, 0.010, 0.00001, outputFileName="myfile.txt")
        The respective I-V datasets can be accessed by e.g.:
        >>> data.IPo0Sb1
        >>> data.VPo0Sb1
        '''
        result = {}
        if outputFileName != None:
            out_file = open(outputFileName, "w")
            out_file.write("I-V Curve  startVoltage=%8.4f [V] endVoltage=%8.4f [V] step = %8.6f [V]\n" % (startVoltage, endVoltage, step))
            out_file.write("V,IPo0Sb1,VPo0Sb1,IPo1Sb1,VPo1Sb1,IPo0Sb2,VPo0Sb2,IPo1Sb2,VPo1Sb2\n")

        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getIVData(startVoltage, endVoltage, step)
            if outputFileName != None:
                for i in range(0, len(result[antName].VPo0Sb1)):
                    output_str = "%e,%e,%e,%e,%e,%e,%e,%e,%e\n" % (result[antName].V[i], result[antName].IPo0Sb1[i], result[antName].VPo0Sb1[i], result[antName].IPo1Sb1[i], result[antName].VPo1Sb1[i], result[antName].IPo0Sb2[i], result[antName].VPo0Sb2[i], result[antName].IPo1Sb2[i], result[antName].VPo1Sb2[i])
                    out_file.write(output_str)

        if outputFileName != None:
            out_file.close()
            print "Data has been written to file", outputFileName

        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def __SISPol0USB(self, key):
        try:
            value = self._devices[key].GET_POL0_SB1_SIS_OPEN_LOOP()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Closed Loop"
            elif value == 1:
                value_t = "Open Loop"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        state=StatusHelper.ValueUnit(value_t,label="Mode")
        
        try:
            value1 = self._devices[key].GET_POL0_SB1_SIS_VOLTAGE()[0]
            value1 = "%.4f" % (value1*1000)
        except:
            value1 = "N/A"
        voltage=StatusHelper.ValueUnit(value1,"mV","V") 
        try:
            value2 = self._devices[key].GET_POL0_SB1_SIS_CURRENT()[0]
            value2 = "%.4f" % (value2*1000000)
        except:
            value2 = "N/A"
        current=StatusHelper.ValueUnit(value2,"uA","I")

        return [state, voltage, current]

    def __SISPol1USB(self, key):
        try:
            value = self._devices[key].GET_POL1_SB1_SIS_OPEN_LOOP()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Closed Loop"
            elif value == 1:
                value_t = "Open Loop"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        state=StatusHelper.ValueUnit(value_t,label="Mode")
        
        try:
            value1 = self._devices[key].GET_POL1_SB1_SIS_VOLTAGE()[0]
            value1 = "%.4f" % (value1*1000)
        except:
            value1 = "N/A"
        voltage=StatusHelper.ValueUnit(value1,"mV","V") 
        try:
            value2 = self._devices[key].GET_POL1_SB1_SIS_CURRENT()[0]
            value2 = "%.4f" % (value2*1000000)
        except:
            value2 = "N/A"
        current=StatusHelper.ValueUnit(value2,"uA","I")

        return [state, voltage, current]

    def __SISPol0LSB(self, key):
        try:
            value = self._devices[key].GET_POL0_SB2_SIS_OPEN_LOOP()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Closed Loop"
            elif value == 1:
                value_t = "Open Loop"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        state=StatusHelper.ValueUnit(value_t,label="Mode")
        
        try:
            value1 = self._devices[key].GET_POL0_SB2_SIS_VOLTAGE()[0]
            value1 = "%.4f" % (value1*1000)
        except:
            value1 = "N/A"
        voltage=StatusHelper.ValueUnit(value1,"mV","V") 
        try:
            value2 = self._devices[key].GET_POL0_SB2_SIS_CURRENT()[0]
            value2 = "%.4f" % (value2*1000000)
        except:
            value2 = "N/A"
        current=StatusHelper.ValueUnit(value2,"uA","I")

        return [state, voltage, current]

    def __SISPol1LSB(self, key):
        try:
            value = self._devices[key].GET_POL1_SB2_SIS_OPEN_LOOP()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Closed Loop"
            elif value == 1:
                value_t = "Open Loop"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        state=StatusHelper.ValueUnit(value_t,label="Mode")
        
        try:
            value1 = self._devices[key].GET_POL1_SB2_SIS_VOLTAGE()[0]
            value1 = "%.4f" % (value1*1000)
        except:
            value1 = "N/A"
        voltage=StatusHelper.ValueUnit(value1,"mV","V") 
        try:
            value2 = self._devices[key].GET_POL1_SB2_SIS_CURRENT()[0]
            value2 = "%.4f" % (value2*1000000)
        except:
            value2 = "N/A"
        current=StatusHelper.ValueUnit(value2,"uA","I")

        return [state, voltage, current]

    def __STATUSHelper(self, key):
        '''
        This method provides the status part that is common to all ColdCarts.
        A list of elements is returned that should be used within higher
        level STATUS() methods.
        '''
        os.system("clear")

        # List of status elements for polarization 0
        pol0 = []

        pol0.append(StatusHelper.Separator("Polarization 0"))
        try:
            value = self._devices[key].GET_POL0_LNA_LED_ENABLE()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Disabled"
            elif value == 1:
                value_t = "Enabled"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        pol0.append( StatusHelper.Line("HEMT LED",[StatusHelper.ValueUnit(value_t)]) )
        
        #######################LNA USB################################
        try:
            value = self._devices[key].GET_POL0_SB1_LNA_ENABLE()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Disabled"
            elif value == 1:
                value_t = "Enabled"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        state=StatusHelper.Line("State",[StatusHelper.ValueUnit(value_t)])
        
        try:
            value1 = self._devices[key].GET_POL0_SB1_LNA1_DRAIN_VOLTAGE()[0]
            value1 = "%.4f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_POL0_SB1_LNA1_DRAIN_CURRENT()[0]
            value2 = "%.4f" % (value2*1000)
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_POL0_SB1_LNA1_GATE_VOLTAGE()[0]
            value3 = "%.4f" % value3
        except:
            value3 = "N/A"
        stage1=StatusHelper.Line("Stage 1",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")]) 
        
        try:
            value1 = self._devices[key].GET_POL0_SB1_LNA2_DRAIN_VOLTAGE()[0]
            value1 = "%.4f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_POL0_SB1_LNA2_DRAIN_CURRENT()[0]
            value2 = "%.4f" % (value2*1000)
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_POL0_SB1_LNA2_GATE_VOLTAGE()[0]
            value3 = "%.4f" % value3
        except:
            value3 = "N/A"
        stage2=StatusHelper.Line("Stage 2",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")]) 

        try:
            value1 = self._devices[key].GET_POL0_SB1_LNA3_DRAIN_VOLTAGE()[0]
            value1 = "%.4f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_POL0_SB1_LNA3_DRAIN_CURRENT()[0]
            value2 = "%.4f" % (value2*1000)
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_POL0_SB1_LNA3_GATE_VOLTAGE()[0]
            value3 = "%.4f" % value3
        except:
            value3 = "N/A"
        stage3=StatusHelper.Line("Stage 3",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")]) 

        
        pol0.append(StatusHelper.Group("LNA USB",[state,stage1,stage2,stage3]))

        ################################SIS USB###########################
        temp = self.__SISPol0USB(key)

        pol0.append(StatusHelper.Line("SIS USB", temp))

        # List of status elements for polarization 1
        pol1 = []

        pol1.append(StatusHelper.Separator("Polarization 1"))
        try:
            value = self._devices[key].GET_POL1_LNA_LED_ENABLE()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Disabled"
            elif value == 1:
                value_t = "Enabled"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        pol1.append( StatusHelper.Line("HEMT LED",[StatusHelper.ValueUnit(value_t)]) )
        
        #######################LNA USB################################
        try:
            value = self._devices[key].GET_POL1_SB1_LNA_ENABLE()[0]
            #value = "%.2f" % value
            if value == 0:
                value_t = "Disabled"
            elif value == 1:
                value_t = "Enabled"
            else:
                value_t = "Unknown"
        except:
            value_t = "N/A"
        state=StatusHelper.Line("State",[StatusHelper.ValueUnit(value_t)])
        
        try:
            value1 = self._devices[key].GET_POL1_SB1_LNA1_DRAIN_VOLTAGE()[0]
            value1 = "%.4f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_POL1_SB1_LNA1_DRAIN_CURRENT()[0]
            value2 = "%.4f" % (value2*1000)
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_POL1_SB1_LNA1_GATE_VOLTAGE()[0]
            value3 = "%.4f" % value3
        except:
            value3 = "N/A"
        stage1=StatusHelper.Line("Stage 1",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")]) 
        
        try:
            value1 = self._devices[key].GET_POL1_SB1_LNA2_DRAIN_VOLTAGE()[0]
            value1 = "%.4f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_POL1_SB1_LNA2_DRAIN_CURRENT()[0]
            value2 = "%.4f" % (value2*1000)
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_POL1_SB1_LNA2_GATE_VOLTAGE()[0]
            value3 = "%.4f" % value3
        except:
            value3 = "N/A"
        stage2=StatusHelper.Line("Stage 2",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")]) 

        try:
            value1 = self._devices[key].GET_POL1_SB1_LNA3_DRAIN_VOLTAGE()[0]
            value1 = "%.4f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_POL1_SB1_LNA3_DRAIN_CURRENT()[0]
            value2 = "%.4f" % (value2*1000)
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_POL1_SB1_LNA3_GATE_VOLTAGE()[0]
            value3 = "%.4f" % value3
        except:
            value3 = "N/A"
        stage3=StatusHelper.Line("Stage 3",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")]) 

        
        pol1.append(StatusHelper.Group("LNA USB",[state,stage1,stage2,stage3]))

        ################################SIS USB###########################
        temp = self.__SISPol1USB(key)

        pol1.append(StatusHelper.Line("SIS USB", temp))
        
        ##################Temperatures################
        temps = []
        temps.append(StatusHelper.Separator("Cartridge Temperatures"))
        try:
            value1 = self._devices[key].GET_TEMPERATURE_SENSOR0()[0]
            value1 = "%.2f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_TEMPERATURE_SENSOR1()[0]
            value2 = "%.2f" % value2
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_TEMPERATURE_SENSOR2()[0]
            value3 = "%.2f" % value3
        except:
            value3 = "N/A"
        temp=StatusHelper.Line("Cartridge Temperatures",[StatusHelper.ValueUnit(value1,"K","4K"),StatusHelper.ValueUnit(value2,"K","110K"),StatusHelper.ValueUnit(value3,"K","Mix pol0")]) 
        
        temps.append(temp)
        try:
            value1 = self._devices[key].GET_TEMPERATURE_SENSOR3()[0]
            value1 = "%.2f" % value1
        except:
            value1 = "N/A"
        try:
            value2 = self._devices[key].GET_TEMPERATURE_SENSOR4()[0]
            value2 = "%.2f" % value2
        except:
            value2 = "N/A"
        try:
            value3 = self._devices[key].GET_TEMPERATURE_SENSOR5()[0]
            value3 = "%.2f" % value3
        except:
            value3 = "N/A"
        temp=StatusHelper.Line("Cartridge Temperatures",[StatusHelper.ValueUnit(value1,"K","Spare"),StatusHelper.ValueUnit(value2,"K","15K"),StatusHelper.ValueUnit(value3,"K","Mix pol1")]) 
        
        temps.append(temp)
        return [pol0,pol1,temps]

