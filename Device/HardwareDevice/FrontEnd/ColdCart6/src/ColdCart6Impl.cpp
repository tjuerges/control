/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCart6Impl.cpp
 *
 * $Id$
 */

#include <ColdCart6Impl.h>

/**
 *-----------------------
 * ColdCart6 Constructor
 *-----------------------
 */
ColdCart6Impl::ColdCart6Impl(const ACE_CString& name,
    maci::ContainerServices* cs):
    ColdCart6Base(name,cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    band_m = ReceiverBandMod::ALMA_RB_06;
    ColdCartBase::setBaseAddress(getBaseAddress());
    assembly_m = "ColdCart6";
}

/**
 *-----------------------
 * ColdCart6 Destructor
 *-----------------------
 */
ColdCart6Impl::~ColdCart6Impl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

///////////////////////////////////////
// Additional methods for ColdCart6
///////////////////////////////////////

void ColdCart6Impl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ColdCart6Base::hwInitializeAction();
    try {
        setCntlPol0Sb1SisOpenLoop(false);
        setCntlPol0Sb2SisOpenLoop(false);
        setCntlPol1Sb1SisOpenLoop(false);
        setCntlPol1Sb2SisOpenLoop(false);

        setCntlPol0SisHeaterEnable(false);
        setCntlPol1SisHeaterEnable(false);
    } catch (ControlExceptions::CAMBErrorExImpl &ex) {
        ex.log();
    }

}


void  ColdCart6Impl::setLNABias(int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    bool setAll = (pol < 0 || sb < 1);
    //Data has 9 point, we only use 6
    std::vector<double> row;

    if (setAll || (pol == 0 && sb == 1)) {
        row = getConfigLNA(0,1);
        setCntlPol0Sb1Lna1DrainVoltage(row[0]);
        setCntlPol0Sb1Lna2DrainVoltage(row[1]);
        setCntlPol0Sb1Lna3DrainVoltage(row[2]);
        setCntlPol0Sb1Lna1DrainCurrent(row[3]);
        setCntlPol0Sb1Lna2DrainCurrent(row[4]);
        setCntlPol0Sb1Lna3DrainCurrent(row[5]);
    }
    if (setAll || (pol == 0 && sb == 2)) {
        row = getConfigLNA(0,2);
        setCntlPol0Sb2Lna1DrainVoltage(row[0]);
        setCntlPol0Sb2Lna2DrainVoltage(row[1]);
        setCntlPol0Sb2Lna3DrainVoltage(row[2]);
        setCntlPol0Sb2Lna1DrainCurrent(row[3]);
        setCntlPol0Sb2Lna2DrainCurrent(row[4]);
        setCntlPol0Sb2Lna3DrainCurrent(row[5]);
    }
    if (setAll || (pol == 1 && sb == 1)) {
        row = getConfigLNA(1,1);
        setCntlPol1Sb1Lna1DrainVoltage(row[0]);
        setCntlPol1Sb1Lna2DrainVoltage(row[1]);
        setCntlPol1Sb1Lna3DrainVoltage(row[2]);
        setCntlPol1Sb1Lna1DrainCurrent(row[3]);
        setCntlPol1Sb1Lna2DrainCurrent(row[4]);
        setCntlPol1Sb1Lna3DrainCurrent(row[5]);
    }
    if (setAll || (pol == 1 && sb == 2)) {
        row = getConfigLNA(1,2);
        setCntlPol1Sb2Lna1DrainVoltage(row[0]);
        setCntlPol1Sb2Lna2DrainVoltage(row[1]);
        setCntlPol1Sb2Lna3DrainVoltage(row[2]);
        setCntlPol1Sb2Lna1DrainCurrent(row[3]);
        setCntlPol1Sb2Lna2DrainCurrent(row[4]);
        setCntlPol1Sb2Lna3DrainCurrent(row[5]);
    }
}

void ColdCart6Impl::setEnableLNABias(bool enable, int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    bool setAll = (pol < 0 || sb < 1);

    if(setAll || (pol == 0 && sb == 1))
        setCntlPol0Sb1LnaEnable(enable);
    if(setAll || (pol == 0 && sb == 2))
        setCntlPol0Sb2LnaEnable(enable);
    if(setAll || (pol == 1 && sb == 1))
        setCntlPol1Sb1LnaEnable(enable);
    if(setAll || (pol == 1 && sb == 2))
        setCntlPol1Sb2LnaEnable(enable);
}

void ColdCart6Impl::setSISBias(bool enable,
                                int pol, int sb, int _openLoop)
{
    bool setAll = (pol < 0 || sb < 1);
    bool openLoop = (_openLoop == 1);

    std::vector<double> row = getConfigSISBias();

    setCntlPol0SisHeaterEnable(false);
    setCntlPol1SisHeaterEnable(false);

    if (enable) {
        if (setAll || (pol == 0 && sb == 1)) {
            setCntlPol0Sb1SisVoltage(row[0]);
            if (_openLoop >= 0) {
                setCntlPol0Sb1SisOpenLoop(openLoop);
            }
        }
        if (setAll || (pol == 0 && sb == 2)) {
            setCntlPol0Sb2SisVoltage(row[1]);
            if (_openLoop >= 0) {
                setCntlPol0Sb2SisOpenLoop(openLoop);
            }
        }
        if (setAll || (pol == 1 && sb == 1)) {
            setCntlPol1Sb1SisVoltage(row[2]);
            if (_openLoop >= 0) {
                setCntlPol1Sb1SisOpenLoop(openLoop);
            }
        }
        if (setAll || (pol == 1 && sb == 2)) {
            setCntlPol1Sb2SisVoltage(row[3]);
            if (_openLoop >= 0) {
                setCntlPol1Sb2SisOpenLoop(openLoop);
            }
        }
    }
}

void ColdCart6Impl::setBandOpenLoop(bool openLoop)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try{
        setCntlPol0Sb1SisOpenLoop(openLoop);
        setCntlPol0Sb2SisOpenLoop(openLoop);
        setCntlPol1Sb1SisOpenLoop(openLoop);
        setCntlPol1Sb2SisOpenLoop(openLoop);
    } catch (const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    } catch (const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    }
}


void ColdCart6Impl::setSISMagnet(bool enable, int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    bool setAll = (pol < 0 || sb < 1);

    std::vector<double> row(4,0.0);
    //disabling the magnet is achieved by setting 0.0 to its values.
    if(enable)
        row = getConfigSisMag();

    if (setAll || (pol == 0 && sb == 1)) {
        setCntlPol0Sb1SisMagnetCurrent(row[0]);
    }
   // if (setAll || (pol == 0 && sb == 2)) {
        //BAND6 has no magnet in SB2 Throw exception
       // setCntlPol0Sb2SisMagnetCurrent(row[1]);
   // }
    if (setAll || (pol == 1 && sb == 1)) {
        setCntlPol1Sb1SisMagnetCurrent(row[2]);
    }
    //if (setAll || (pol == 1 && sb == 2)) {
        //BAND6 has no magnet in SB2 Throw exception
        //setCntlPol1Sb2SisMagnetCurrent(row[3]);
    //}
}

void ColdCart6Impl::setSisVoltage(const float voltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    setCntlPol0Sb1SisVoltage(voltage);
    setCntlPol1Sb1SisVoltage(voltage);
    setCntlPol0Sb2SisVoltage(voltage);
    setCntlPol1Sb2SisVoltage(voltage);
}

void ColdCart6Impl::setSisVoltage(const std::vector< float >& voltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(voltage.size()<4) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Details","Voltage vector is to short.");
        throw ex;
    }
    setCntlPol0Sb1SisVoltage(voltage[0]);
    setCntlPol1Sb1SisVoltage(voltage[1]);
    setCntlPol0Sb2SisVoltage(voltage[2]);
    setCntlPol1Sb2SisVoltage(voltage[3]);
}

void ColdCart6Impl::getSisVoltage(std::vector< float >& voltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    voltage.clear();
    voltage.push_back(getPol0Sb1SisVoltage(timestamp));
    voltage.push_back(getPol1Sb1SisVoltage(timestamp));
    voltage.push_back(getPol0Sb2SisVoltage(timestamp));
    voltage.push_back(getPol1Sb2SisVoltage(timestamp));
}

void ColdCart6Impl::setSisMagnetCurrent(const float current)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    setCntlPol0Sb1SisMagnetCurrent(current);
    setCntlPol1Sb1SisMagnetCurrent(current);
}

void ColdCart6Impl::setSisMagnetCurrent(const std::vector< float >& current)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(current.size()>2) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Details","Current vector is to long.");
        throw ex;
    }
    setCntlPol0Sb1SisMagnetCurrent(current[0]);
    setCntlPol1Sb1SisMagnetCurrent(current[1]);
}

void ColdCart6Impl::getSisCurrent(std::vector< float >& current)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    current.clear();
    current.push_back(getPol0Sb1SisCurrent(timestamp));
    current.push_back(getPol1Sb1SisCurrent(timestamp));
    current.push_back(getPol0Sb2SisCurrent(timestamp));
    current.push_back(getPol1Sb2SisCurrent(timestamp));
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ColdCart6Impl)
