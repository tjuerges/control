#! /usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a PowerDist9 device.
"""

import CCL.PowerDist9Base
from CCL import StatusHelper

class PowerDist9(CCL.PowerDist9Base.PowerDist9Base):
    '''
    The PowerDist9 class inherits from the code generated PowerDist9Base
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/PowerDist9"
            antennaName = None
        CCL.PowerDist9Base.PowerDist9Base.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.PowerDist9Base.PowerDist9Base.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the PowerDist9
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]
        
            # Invoke the generic PowerDist status helper
            elements = self._PowerDist__STATUSHelper(key)
            
            '''
            ############################### PowerDist9 specific ###################################################
            elements.append( StatusHelper.Separator("PowerDist9 specific") )
            
            # GET_LO_PLL_ASSEMBLY_TEMP
            try:
                value = self._devices[key].GET_LO_PLL_ASSEMBLY_TEMP()[0]
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("LO Pll Assembly Temp",
                                               [StatusHelper.ValueUnit(value, "K")]) )    
            '''   
            try:
                value = self._devices[key].GET_CARTRIDGE_ENABLE()[0]
                if value==0:
                    value_t="Disabled"
                else:
                    value_t="Enabled"
            except:
                value_t = "N/A"
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName + "   Cartridge: " + value_t, elements)
            statusFrame.printScreen()
