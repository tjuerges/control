
#ifndef PowerDist9SimImpl_H
#define PowerDist9SimImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PowerDist9SimImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include "PowerDist9SimBase.h"
#include <maciContainerServices.h>
#include <ControlDeviceExceptionsC.h>


class PowerDist9SimImpl: public PowerDist9SimBase
{
    public:
    /**
     * Constructor
     */
    PowerDist9SimImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~PowerDist9SimImpl();


    

    

    

    

    

    

    

    

    

    

    

    

    



    

    



    virtual void hwSimulationAction()
        throw(CORBA::SystemException, ControlDeviceExceptions::HwLifecycleEx);

    private:
    /**
     * No default constructor.
     */
    PowerDist9SimImpl();

    /**
     * ALMA coding standards: copy constructor is disabled.
     */
    PowerDist9SimImpl(const PowerDist9SimImpl&);

    /**
     * ALMA coding standards: assignment operator is disabled.
     */
    PowerDist9SimImpl& operator=(const PowerDist9SimImpl&);

};
#endif // PowerDist9SimImpl_H
