#! /usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a ColdCart4 device.
"""

import CCL.ColdCart4Base
from CCL import StatusHelper

class ColdCart4(CCL.ColdCart4Base.ColdCart4Base):
    '''
    The ColdCart4 class inherits from the code generated ColdCart4Base
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/ColdCart4"
            antennaName = None
        CCL.ColdCart4Base.ColdCart4Base.__init__(self, antennaName, componentName, stickyFlag)
    
    def __del__(self):
        CCL.ColdCart4Base.ColdCart4Base.__del__(self)

    def SISSTATUS(self):
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            retval = []
            retval.append(StatusHelper.Line("SIS Pol 0 USB", self._ColdCart__SISPol0USB(key)))
            retval.append(StatusHelper.Line("SIS Pol 0 LSB", self._ColdCart__SISPol0LSB(key)))
            retval.append(StatusHelper.Line("SIS Pol 1 USB", self._ColdCart__SISPol1USB(key)))
            retval.append(StatusHelper.Line("SIS Pol 1 LSB", self._ColdCart__SISPol1LSB(key)))
            result[antName] = retval
        if len(self._devices) == 1:
            return result.values()[0]
        return result
        

    def STATUS(self):
        '''
        Displays the current status of the ColdCart4
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]
        
            # Invoke the generic ColdCart status helper
            temp = self._ColdCart__STATUSHelper(key)
            pol0 = temp[0]
            pol1 = temp[1]
            temps= temp[2]
            
            '''
            ############################### ColdCart4 specific ###################################################
            elements.append( StatusHelper.Separator("ColdCart4 specific") )
            
            # GET_LO_PLL_ASSEMBLY_TEMP
            try:
                value = self._devices[key].GET_LO_PLL_ASSEMBLY_TEMP()[0]
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("LO Pll Assembly Temp",
                                               [StatusHelper.ValueUnit(value, "K")]) )    
            '''   
            #######################Polarization 0########################
            #######################LNA LSB################################
            try:
                value = self._devices[key].GET_POL0_SB2_LNA_ENABLE()[0]
                #value = "%.2f" % value
                if value == 0:
                    value_t = "Disabled"
                elif value == 1:
                    value_t = "Enabled"
                else:
                    value_t = "Unknown"
            except:
                value_t = "N/A"
            state=StatusHelper.Line("State",[StatusHelper.ValueUnit(value_t)])

            try:
                value1 = self._devices[key].GET_POL0_SB2_LNA1_DRAIN_VOLTAGE()[0]
                value1 = "%.4f" % value1
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL0_SB2_LNA1_DRAIN_CURRENT()[0]
                value2 = "%.4f" % (value2*1000)
            except:
                value2 = "N/A"
            try:
                value3 = self._devices[key].GET_POL0_SB2_LNA1_GATE_VOLTAGE()[0]
                value3 = "%.4f" % value3
            except:
                value3 = "N/A"
            stage1=StatusHelper.Line("Stage 1",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")])

            try:
                value1 = self._devices[key].GET_POL0_SB2_LNA2_DRAIN_VOLTAGE()[0]
                value1 = "%.4f" % value1
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL0_SB2_LNA2_DRAIN_CURRENT()[0]
                value2 = "%.4f" % (value2*1000)
            except:
                value2 = "N/A"
            try:
                value3 = self._devices[key].GET_POL0_SB2_LNA2_GATE_VOLTAGE()[0]
                value3 = "%.4f" % value3
            except:
                value3 = "N/A"
            stage2=StatusHelper.Line("Stage 2",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")])

            try:
                value1 = self._devices[key].GET_POL0_SB2_LNA3_DRAIN_VOLTAGE()[0]
                value1 = "%.4f" % value1
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL0_SB2_LNA3_DRAIN_CURRENT()[0]
                value2 = "%.4f" % (value2*1000)
            except:
                value2 = "N/A"
            try:
                value3 = self._devices[key].GET_POL0_SB2_LNA3_GATE_VOLTAGE()[0]
                value3 = "%.4f" % value3
            except:
                value3 = "N/A"
            stage3=StatusHelper.Line("Stage 3",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")])
    
            pol0.append(StatusHelper.Group("LNA LSB",[state,stage1,stage2,stage3]))
            
           ################################SIS LSB###########################
            tempSIS = self._ColdCart__SISPol0LSB(key) 
            pol0.append(StatusHelper.Line("SIS LSB", tempSIS))


            #######################Polarization 1########################
            #######################LNA LSB################################
            try:
                value = self._devices[key].GET_POL1_SB2_LNA_ENABLE()[0]
                #value = "%.2f" % value
                if value == 0:
                    value_t = "Disabled"
                elif value == 1:
                    value_t = "Enabled"
                else:
                    value_t = "Unknown"
            except:
                value_t = "N/A"
            state=StatusHelper.Line("State",[StatusHelper.ValueUnit(value_t)])

            try:
                value1 = self._devices[key].GET_POL1_SB2_LNA1_DRAIN_VOLTAGE()[0]
                value1 = "%.4f" % value1
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL1_SB2_LNA1_DRAIN_CURRENT()[0]
                value2 = "%.4f" % (value2*1000)
            except:
                value2 = "N/A"
            try:
                value3 = self._devices[key].GET_POL1_SB2_LNA1_GATE_VOLTAGE()[0]
                value3 = "%.4f" % value3
            except:
                value3 = "N/A"
            stage1=StatusHelper.Line("Stage 1",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")])

            try:
                value1 = self._devices[key].GET_POL1_SB2_LNA2_DRAIN_VOLTAGE()[0]
                value1 = "%.4f" % value1
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL1_SB2_LNA2_DRAIN_CURRENT()[0]
                value2 = "%.4f" % (value2*1000)
            except:
                value2 = "N/A"
            try:
                value3 = self._devices[key].GET_POL1_SB2_LNA2_GATE_VOLTAGE()[0]
                value3 = "%.4f" % value3
            except:
                value3 = "N/A"
            stage2=StatusHelper.Line("Stage 2",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")])

            try:
                value1 = self._devices[key].GET_POL1_SB2_LNA3_DRAIN_VOLTAGE()[0]
                value1 = "%.4f" % value1
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL1_SB2_LNA3_DRAIN_CURRENT()[0]
                value2 = "%.4f" % (value2*1000)
            except:
                value2 = "N/A"
            try:
                value3 = self._devices[key].GET_POL1_SB2_LNA3_GATE_VOLTAGE()[0]
                value3 = "%.4f" % value3
            except:
                value3 = "N/A"
            stage3=StatusHelper.Line("Stage 3",[StatusHelper.ValueUnit(value1,"V","Vd"),StatusHelper.ValueUnit(value2,"mA","Id"),StatusHelper.ValueUnit(value3,"V","Vg")])
    
            pol1.append(StatusHelper.Group("LNA LSB",[state,stage1,stage2,stage3]))
            
           ################################SIS LSB###########################
            tempSIS = self._ColdCart__SISPol1LSB(key) 
            pol1.append(StatusHelper.Line("SIS LSB", tempSIS))
            
            elements = pol0+pol1+temps
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
