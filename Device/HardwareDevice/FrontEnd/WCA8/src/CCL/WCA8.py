#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a WCA8 device.
"""

import CCL.WCA8Base
from CCL import StatusHelper

class WCA8(CCL.WCA8Base.WCA8Base):
    '''
    The WCA8 class inherits from the code generated WCA8Base
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor

        Example:
        wca7 = WCA8("DA41")
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/WCA8"
            antennaName = None
        CCL.WCA8Base.WCA8Base.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.WCA8Base.WCA8Base.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the WCA8
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]
        
            # Invoke the generic WCA status helper
            elements = self._WCA__STATUSHelper(key)
            
            ############################### WCA8 specific ###################################################
            #elements.append( StatusHelper.Separator("WCA8 specific") )
                  
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()

