#ifndef FEMCImpl_H
#define FEMCImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FEMCImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <FEMCBase.h>

//CORBA Servants
#include "FEMCS.h"

//FEMC Exceptions
#include <FEMCExceptions.h>

#include<configDataAccess.h>

#include <xmlTmcdbComponent.h>
#include <xmlTmcdbComponent.h>
#include <controlAlarmSender.h>

class FEMCImpl: public FEMCBase,
    public virtual POA_Control::FEMC
{
    public:
    /**
     * Constructor
     */
    FEMCImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~FEMCImpl();

    protected:
    /**
    * void setupFEMC()
    * cal SETUP_INFO. This will initialize the ARCOM board if it was not. If
    * the board is not initialized no CAN message will be acknowledged.
    * @except ControlExceptions::CAMBErrorExImpl
    * @except ControlExceptions::INACTErrorExImpl
    * @except FEMCExceptions::SetupFailedExImpl
    */
    void setupFEMC();

    /**
    * voids setupEsns()
    * Readout all the ESNs from the FEMC and store the in a vector for
    * later usage.
    * @except ControlExceptions::CAMBErrorExImpl
    * @except ControlExceptions::INACTErrorExImpl
    * @except FEMCExceptions::NoEsnsExImpl
    */
    void setupEsns();

    /**
    * void checkEsn(unsigned long long esn)
    * Check for ERROR_ESN = ~0x0ULL since it
    * indicates a problem in the ESNS. If there
    * is a problem throw and exception.
    * @param unsigned long long esn
    * @except FEMCExceptions::NoEsnsExImpl
    */
    void checkEsn(unsigned long long esn);

    /**
    * @override FEMCBase::hwInitializeAction
    * @except ControlDeviceExceptions::HwLifecycleEx
    */
    virtual void hwInitializeAction();

    /**
    * @override FEMCBase::hwConfigureAction
    * @except ControlDeviceExceptions::HwLifecycleEx
    */
    virtual void hwConfigureAction();
    
   bool hasEsn_m;
   std::string assembly_m;
   std::vector< unsigned long long > esns_m;
   ConfigDataAccessor *configData_m;

   private:
   XmlTmcdbComponent *tmcdb_m;
   Control::AlarmSender *alarmSender_m;


};
#endif // FEMCImpl_H
