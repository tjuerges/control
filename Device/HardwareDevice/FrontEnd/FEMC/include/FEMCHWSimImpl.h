
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FEMCHWSimImpl.h
 *
 * $Id$
 */
#ifndef FEMCHWSimImpl_H
#define FEMCHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "FEMCHWSimBase.h"
#include <Utils.h>
#include <boost/lexical_cast.hpp>  //for boost::lexical_cast
//FEMC Exceptions
#include <FEMCExceptions.h>

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * FEMCHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class FEMCHWSimImpl : public FEMCHWSimBase
  {
  public :
    FEMCHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);

  void setup();
//  void setup(const std::vector<CAN::byte_t>& serialNumber);

  virtual std::vector< CAN::byte_t > getMonitorEsnsFound() const;
  virtual std::vector< CAN::byte_t > getMonitorEsns() const;
  std::vector< CAN::byte_t > getMonitorEsnsNoConst();

  private:
       std::vector< std::vector< CAN::byte_t >  > esns_cb;
       std::vector< std::vector< CAN::byte_t >  >::iterator cb_pos;
  }; // class FEMCHWSimImpl

} // namespace AMB

#endif // FEMCHWSimImpl_H
