
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FEMCHWSimImpl.cpp
 *
 * $Id$
 */

#include "FEMCHWSimImpl.h"
#include <SimulatedSerialNumber.h>

using namespace AMB;

/* Please use this class to implement complex functionality for the
 * FEMCHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

FEMCHWSimImpl::FEMCHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
    : FEMCHWSimBase::FEMCHWSimBase(node, serialNumber)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    setup();
}

//Setup the electronic serial numbers ESN.
void FEMCHWSimImpl::setup()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    //create ESNS
    // 1-10 ColdCart1..ColdCart10
    // 11-20 WCA1..WCA10
    // 21 LPR
    // 22 IFSwitch
    // 23 Cryostat


    std::vector< CAN::byte_t >* vvalue(new std::vector< CAN::byte_t >);

    for(unsigned long long i=1; i<=23; i++) {
        AMB::TypeConversion::valueToData(*vvalue, static_cast< unsigned long long >(i), 8U);
        esns_cb.push_back(*vvalue);
    }
    AMB::TypeConversion::valueToData(*vvalue, static_cast< unsigned long long >(0ULL), 8U);
    esns_cb.push_back(*vvalue);
    cb_pos = esns_cb.begin();
}

std::vector< CAN::byte_t > AMB::FEMCHWSimImpl::getMonitorEsnsFound() const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    long long raw(0LL);
    int size = esns_cb.size();
    if(size<=1){
        size=0;
    } else {
        size--;
    }
    raw = static_cast< long long >(size);
    std::vector< CAN::byte_t >* vvalue(new std::vector< CAN::byte_t >);
    AMB::TypeConversion::valueToData(*vvalue, static_cast< unsigned char >(raw), 1U);
    return *vvalue;
}

std::vector< CAN::byte_t > AMB::FEMCHWSimImpl::getMonitorEsns() const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    FEMCHWSimImpl* femc = const_cast<FEMCHWSimImpl*>(this);
    return femc->getMonitorEsnsNoConst();
}

std::vector< CAN::byte_t > AMB::FEMCHWSimImpl::getMonitorEsnsNoConst()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    cb_pos++;
    if(cb_pos == esns_cb.end())
        cb_pos = esns_cb.begin();
    return *cb_pos;
}
