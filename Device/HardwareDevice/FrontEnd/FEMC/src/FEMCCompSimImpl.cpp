
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * File FEMCCompSimImpl.cpp
 */

#include "FEMCCompSimImpl.h"
#include <FEMCHWSimImpl.h>
#include <maciACSComponentDefines.h>
#include <TypeConversion.h>
#include <SimulatedSerialNumber.h>

using namespace maci;

/* Please use this class to implement alternative component, extending
 * the FEMCCompSimBase class. */

FEMCCompSimImpl::FEMCCompSimImpl(const ACE_CString& name, ContainerServices* pCS)
    : FEMCCompSimBase(name,pCS)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::string componentName(name.c_str());
    //We construct the serial number for the device.
    //Use the component name and the assembly name.
    //For example for the LPR in DV02 it would use:
    //CONTROL/DV02/LPR , LPR
    //This will guarantee a unique serialnumber for the TMCDB monitoring
    hashed_sn_m=AMB::Utils::getSimSerialNumber(componentName, assembly_m);
    std::ostringstream msg;
    msg<< "simSerialNumber for  " << name.c_str() << "  with assembly name " << "FEMC" << " is " << std::hex << "0x" << hashed_sn_m << std::endl;
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, msg.str().c_str()));

    std::vector< CAN::byte_t > sn;
    AMB::node_t node = 0x00;
    AMB::TypeConversion::valueToData(sn,hashed_sn_m, 8U);

    simulationIf_m.setSimObj(new AMB::FEMCHWSimImpl(node,sn));
}

void FEMCCompSimImpl::hwInitializeAction()
{
    FEMCImpl::hwInitializeAction();
    std::stringstream sn;
    sn << hashed_sn_m;
    //Set the serial number for simulation. This MUST be done since the implementation will
    //set the serial number depending on the ESN. Simulation does not have different ESN
    //For different antennas, they all have the same.
    setSerialNumber(sn.str().c_str());
}
/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(FEMCCompSimImpl)
