#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a FEMC device.
"""

import CCL.FEMCBase
from CCL import StatusHelper

class FEMC(CCL.FEMCBase.FEMCBase):
    '''
    The FEMC class inherits from the code generated FEMCBase
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        CCL.FEMCBase.FEMCBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.FEMCBase.FEMCBase.__del__(self)

    def __STATUSHelper(self, key):
        '''
        This method provides the status part that is common to all FE Devices.
        A list of elements is returned that should be used within higher
        level STATUS() methods. 
        '''
        # List of status elements
        elements = []
        return elements
