/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File FEMCImpl.cpp
 *
 * $Id$
 */

#include <FEMCImpl.h>
#include <SimulatedSerialNumber.h>

using std::string;
using Control::AlarmSender;

/**
 *-----------------------
 * FEMC Constructor
 *-----------------------
 */
FEMCImpl::FEMCImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    FEMCBase(name,cs),
    hasEsn_m(false)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    configData_m = NULL;
    tmcdb_m = NULL;
    alarmSender_m = NULL;
}

/**
 *-----------------------
 * FEMC Destructor
 *-----------------------
 */
FEMCImpl::~FEMCImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(configData_m != NULL)
        delete configData_m;
    if(tmcdb_m != NULL)
        delete tmcdb_m;
    if(alarmSender_m != NULL)
        delete alarmSender_m;
}

///////////////////////////////////////
// Additional methods for FEMC
///////////////////////////////////////

void FEMCImpl::setupFEMC()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    /* cal SETUP_INFO. This will initialize the ARCOM board if it was not. If
    * the board is not initialized no CAN message will be acknowledged.
    * Return values for setup are:
    * 0x00 no error
    * 0x01 error in registering the special monitor functions
    * 0x02 error in registering the special control functions
    * 0x03 error in registering the monitor functions
    * 0x04 error in registering the control functions
    * 0x05 warning: this function has already been called
    * 0x06 communication between ARCOM and AMBSI not yet established
    * 0x07 Timeout while forwarding CAN message to the ARCOM board
    * 0x08 The function is being accessed by the automatic startup mode
    */
    unsigned char setup = 0XFF;
    ACS::Time timestamp;
    setup = getSetupInfo(timestamp);
    //check for normal condition and return.
    if(setup == 0x05 || setup == 0x00 )
        return;

    //We are in trouble. Start diagnose. and throw a
    // FEMCExceptions::SetupFailedExImpl
    std::string msg;
    switch(setup) {
        case 0x01:
            msg = "error in registering the special monitor functions";
            break;
        case 0x02:
            msg = "error in registering the special control functions";
            break;
        case 0x03:
            msg = "error in registering the monitor functions";
            break;
        case 0x04:
            msg = "error in registering the control functions";
            break;
        case 0x06:
            msg = "communication between ARCOM and AMBSI not yet established";
            break;
        case 0x07:
            msg = "Timeout while forwarding CAN message to the ARCOM board";
            break;
        case 0x08:
            msg = "The function is being accessed by the automatic startup mode";
            break;
        default:
            msg = "Unknown return value";
            break;
    }
    FEMCExceptions::SetupFailedExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
    ex.addData("Detail", msg.c_str());
    throw ex;
}


void FEMCImpl::checkEsn(unsigned long long esn)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    //FIXME: This is not in the ICD yet.
    const unsigned long long ERROR_ESN = ~0x0ULL;

    if (esn == ERROR_ESN) {
        FEMCExceptions::NoEsnsExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Detail", "Got the error message when trying to get the ESNS");
        throw ex;
    }
}

void FEMCImpl::setupEsns()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    unsigned int esns = 0;
    esns = getEsnsFound(timestamp);
    if (esns<1) {
        FEMCExceptions::NoEsnsExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        //ex.addData("Detail", "No ESNS found in by the FEMC");
        //throw ex;
        std::stringstream msg;
        msg<<"No ESNS found in by the FEMC"<<std::endl
           <<"This is a BUG in the FrontEnd Firmware."<<std::endl
           <<"Check the status of http://jira.alma.cl/browse/FE-47"<<std::endl;
        ex.addData("Details", msg.str());
        ex.log();
        throw ex;
    }

    //Read the ESN and check if we get an error msg
    unsigned long long esn;
    //Cycle through the ESNs buffer until we are at the beginning.
    //We will be at the end of the buffer when the ESN is all 0x00.
    esn = getEsns(timestamp);
    checkEsn(esn);
    while(esn) {
        esn = getEsns(timestamp);
        checkEsn(esn);
    }

    //Now we are certain to be at the beginning. Store the ESNs.
    esn = getEsns(timestamp);
    checkEsn(esn);
    do {
        esns_m.push_back(esn);
        esn = getEsns(timestamp);
        checkEsn(esn);
    } while(esn);
}


void FEMCImpl::hwConfigureAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    FEMCBase::hwConfigureAction();
    alarmSender_m = new AlarmSender();

    // If this Device has ESN requirements keep on. If not return immediately
    // we do not want unneeded references to TMCDBComponent.
    if (!hasEsn_m)
        return;

    if (tmcdb_m == NULL) 
        tmcdb_m = new XmlTmcdbComponent(getContainerServices());
}

void FEMCImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    FEMCBase::hwInitializeAction();
    esns_m.clear();
    // ----------------------------------------------------------------------
    // Read the FE monitor point GET_SETUP_INFO as this will turn
    // on the FE controller if it is not started up
    try {
        setupFEMC();
    }  catch (ControlExceptions::CAMBErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not setup the FEMC");
        throw nex.getHwLifecycleEx();
    } catch(ControlExceptions::INACTErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not setup the FEMC");
        throw nex.getHwLifecycleEx();
    } catch(FEMCExceptions::SetupFailedExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not setup the FEMC");
        throw nex.getHwLifecycleEx();
    }

   //Turn of the console in the ARCOM board. It adds 50us to can commands.
    try {
        setCntlConsoleEnable(false);
    }  catch (ControlExceptions::CAMBErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not turn console off");
        nex.log(LM_WARNING);
    } catch(ControlExceptions::INACTErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not turn console off");
        nex.log(LM_WARNING);
    }

    //Now we retrieve the ESNs available and store them in a vector This will
    //avoid the need to do this processing in the code of all other FE
    //devices.
    try {
        setupEsns();
    }  catch (ControlExceptions::CAMBErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not get ESNS list");
        throw nex.getHwLifecycleEx();
    } catch(ControlExceptions::INACTErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not get ESNS list");
        throw nex.getHwLifecycleEx();
    } catch(FEMCExceptions::NoEsnsExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not get ESN list");
        throw nex.getHwLifecycleEx();
    }

    // If this Device has ESN requirements keep on. If not return immediately
    if (!hasEsn_m)
        return;

    std::vector< unsigned long long >::iterator it;
    std::stringstream sesn;//String version of the ESN
    for(it = esns_m.begin(); it< esns_m.end(); it++) {
        sesn.str("");
        sesn<<*it;
        std::string msg = "Looking for TMCDB configuration for SESN ";
        msg += sesn.str();
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_TRACE, msg.c_str()));
        std::string xml = tmcdb_m->getConfigXml(sesn.str());
        std::string xsd = tmcdb_m->getConfigXsd(sesn.str());
        LOG_TO_AUDIENCE(LM_TRACE, __PRETTY_FUNCTION__, sesn.str(), "Developer");
        std::stringstream debug;
        debug<<"XML = "<<xml<<std::endl;
        LOG_TO_AUDIENCE(LM_TRACE, __PRETTY_FUNCTION__, debug.str(), "Developer");
        debug.str("");
        debug<<"XSD = "<<xsd<<std::endl;
        LOG_TO_AUDIENCE(LM_TRACE, __PRETTY_FUNCTION__, debug.str(), "Developer");
        if(xml.size() == 0 || xsd.size() == 0) {
            LOG_TO_AUDIENCE(LM_TRACE, __PRETTY_FUNCTION__, "XML or XSD data has zero length", "Developer");
            continue;
        }

        std::string assembly_tmp;
        try {
            configData_m = new ConfigDataAccessor(xml, xsd);
            std::auto_ptr<const ControlXmlParser> tmp = configData_m->getElement("ASSEMBLY");
            assembly_tmp = (*tmp).getStringAttribute("value");
        } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
            ControlExceptions::XmlParserErrorExImpl nex(ex,__FILE__, __LINE__,"FEMCImpl::hwInitializeAction");
            std::string msg = "Could not access config data";
            nex.addData("Detail", msg);
            nex.log(LM_WARNING);
        }

        if(assembly_tmp == assembly_m){
            // set the serial number for TMCDB Monitoring
            setSerialNumber(sesn.str().c_str());
            std::string tmpstr = sesn.str();
            sesn.str("");
            sesn<<"Found configuration file for assembly: "<<assembly_tmp;
            sesn<<" using ESN: "<<tmpstr;
            LOG_TO_AUDIENCE(LM_TRACE, __PRETTY_FUNCTION__, sesn.str(), "Developer");
            return;
        } else {
            delete configData_m;
            configData_m = NULL;
        }
    }

    if(configData_m == NULL) {
    ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    std::string msg;
    msg = "no configuration data found for this device";
    ex.addData("Detail", msg);
    throw ex.getHwLifecycleEx();
    }
}
