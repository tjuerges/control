#!/bin/env python
import ConfigParser, os
from optparse import OptionParser


__revision__ = "@(#) $Id$"
#$Source$

class FEini2xml():
    def __init__(self):
        self.parser = ConfigParser.ConfigParser()

    def setUp(self, file):
        self.parser.read(file)
        self.sections = self.parser.sections()
        
    def getList(self, option, itemname, numitems, shift=1):
        itemlist = []
        for i in range(numitems):
            n = shift+i
            if n<10:
                n = str(n).zfill(2)
            else:
                n = str(n)
            itemlist.append(self.parser.get(option, itemname+n))
        return itemlist

    def processWCA(self, name):
        wca=None
        for i in self.sections:
            if i.find(name) != -1:
                wca = i
                print "--> found "+name
                break
        if wca == None:
          raise UnboundLocalError
        loparams = self.parser.getint(wca, 'loparams')
        esn = self.parser.get(wca, 'esn')
        fhiyig = self.parser.get(wca, 'fhiyig')
        floyig = self.parser.get(wca, 'floyig')
        loparamsList = []
        loparamsList = self.getList(wca, 'loparam', loparams)
        return [esn, floyig, fhiyig, loparamsList]
        


    def processColdCart(self, name):
        cc=None
        for i in self.sections:
            if i.find(name) != -1:
                cc = i
                print "--> found "+name
                break
        if cc == None:
          raise UnboundLocalError
        band = self.parser.getint(cc, 'band')
        mixerparams = self.parser.getint(cc, 'mixerparams')
        magnetparams = self.parser.getint(cc, 'magnetparams')
        preampparams = self.parser.getint(cc, 'preampparams')
        esn = self.parser.get(cc, 'esn')

        if band == 9:
            preampparamsPol = preampparams/2
        else:
            preampparamsPol = preampparams/4
        mixerparamsList = []
        magnetparamsList = []
        PreampParamsPol0Sb1 = []
        PreampParamsPol0Sb2 = []
        PreampParamsPol1Sb1 = []
        PreampParamsPol1Sb2 = []

        mixerparamsList = self.getList(cc, 'mixerparam', mixerparams)
        magnetparamsList = self.getList(cc, 'magnetparam', magnetparams)

        newShift = 1
        PreampParamsPol0Sb1 = self.getList(cc, 'preampparam', preampparamsPol, newShift)

        if band != 9:
            newShift = newShift + 1*preampparamsPol
            PreampParamsPol0Sb2 = self.getList(cc, 'preampparam', preampparamsPol, newShift)

        newShift = newShift + 1*preampparamsPol
        PreampParamsPol1Sb1 = self.getList(cc, 'preampparam', preampparamsPol, newShift)

        if band != 9:
            newShift = newShift + 1*preampparamsPol
            PreampParamsPol1Sb2 = self.getList(cc, 'preampparam', preampparamsPol, newShift)

        return [esn, mixerparamsList, magnetparamsList, PreampParamsPol0Sb1, PreampParamsPol0Sb2, PreampParamsPol1Sb1, PreampParamsPol1Sb2]

    def wcaXml(self, assembly, Cold, Pll, data):
        output = ""
        preampxml = "    <PowerAmp FreqLO=\"%sE9\" VD0=\"%s\" VD1=\"%s\" VG0=\"%s\" VG1=\"%s\" />\n"
        header = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n\
<ConfigData\n\
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\
xsi:noNamespaceSchemaLocation=\"membuffer.xsd\">\n"
        output = output + header
        output = output + ("    <ASSEMBLY value=\"%s\" />\n" % assembly.upper() )
        output = output + ("    <ColdMultiplier value=\"%d\" />\n" % Cold)
        output = output + ("    <PLLLoopBwMultiplier value=\"%d\" />\n" % Pll)
        output = output + ("    <FLOYIG value=\"%sE9\" />\n" % data[1])
        output = output + ("    <FHIYIG value=\"%sE9\" />\n" % data[2])
        for i in data[3]:
            output = output + (preampxml % tuple(self.strip(i.split(','))))

        output = output + "    <OptimizationTargets FreqLO=\"0.0\" PhotoMixerCurrent=\"-0.4E-3\" />\n"
        output = output + "</ConfigData>"
        return output


    def coldcartXml(self, assembly, data):
        output = ""
        mixerxml = "    <MixerParams FreqLO=\"%sE9\" VJ01=\"%s\" VJ02=\"%s\" VJ11=\"%s\" VJ12=\"%s\" IJ01=\"%s\" IJ02=\"%s\" IJ11=\"%s\" IJ12=\"%s\" />\n" 
        magnetxml = "    <MagnetParams FreqLO=\"%sE9\" IMag01=\"%s\" IMag02=\"%s\" IMag11=\"%s\" IMag12=\"%s\" />\n"
        preampxml = " FreqLO=\"%sE9\" VD1=\"%s\" VD2=\"%s\" VD3=\"%s\" ID1=\"%s\" ID2=\"%s\" ID3=\"%s\" VG1=\"%s\" VG2=\"%s\" VG3=\"%s\" />\n"
        preampbasexml = "    <PreampParamsPol%dSb%d"
        header = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n\
<ConfigData\n\
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\
xsi:noNamespaceSchemaLocation=\"membuffer.xsd\">\n"
        output = output + header
        output = output + ("    <ASSEMBLY value=\"%s\" />\n" % assembly )
        for i in data[1]:
            output = output + (mixerxml % tuple(self.strip(i.split(','))))
        if data[2].__len__() == 0:
            output = output + "    <MagnetParams FreqLO=\"0.0\" />\n"
        else:
            for i in data[2]:
                output = output + (magnetxml % tuple(self.strip(i.split(','))))

        preampbasexmlT = preampbasexml % (0,1)
        for i in data[3]:
            chopped = i.split(',')[0:1] + i.split(',')[3:]
            output = output + preampbasexmlT + (preampxml % tuple(self.strip(chopped)))
        
        preampbasexmlT = preampbasexml % (0,2)
        if data[4].__len__() == 0:
            output = output + preampbasexmlT + " FreqLO=\"0.0\" />\n"
        else:
            for i in data[4]:
                chopped = i.split(',')[0:1] + i.split(',')[3:]
                output = output + preampbasexmlT + (preampxml % tuple(self.strip(chopped)))

        preampbasexmlT = preampbasexml % (1,1)
        for i in data[5]:
            chopped = i.split(',')[0:1] + i.split(',')[3:]
            output = output + preampbasexmlT + (preampxml % tuple(self.strip(chopped)))

        preampbasexmlT = preampbasexml % (1,2)
        if data[6].__len__() == 0:
            output = output + preampbasexmlT + " FreqLO=\"0.0\" />\n"
        else:
            for i in data[6]:
                chopped = i.split(',')[0:1] + i.split(',')[3:]
                output = output + preampbasexmlT + (preampxml % tuple(self.strip(chopped)))

        output = output + "</ConfigData>"
        return output

    def strip(self, par):
        retval = []
        for i in par:
            retval.append(i.lstrip().rstrip())
        return retval

        
       
if __name__ == "__main__":
    parser = OptionParser(usage="%prog -f inifile [ -e esn ]", version=__revision__)
    parser.add_option("-f", "--file", dest = "file", type = "str",
                      default = None,
                      help = "ini file with FE characteristics",
                      metavar = "FILE")

    parser.add_option("-e", "--esn", default=None, type="int", 
                      dest="esn",
                      help="Ignore ESN and number the from this")
    (options, args) = parser.parse_args()
    if options.file is None:
        parser.error("A file must be suplied")
    
    p = FEini2xml()
    p.setUp(options.file)
    bands = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    coldMult = {1:1, 2:1, 3:1, 4:2, 5:6, 6:3, 7:3, 8:6, 9:9, 10:9}
    pll =      {1:1, 2:6, 3:6, 4:3, 5:2, 6:6, 7:6, 8:3, 9:3, 10:6}
    for i in bands:
        cc = "ColdCart%d" % i
        try:
           data = p.processColdCart(cc)
        except UnboundLocalError:
           print "%s is not defined in the file skipping" % cc
           continue
        if options.esn is not None:
            outputname = ("%d.xml" % options.esn)
            options.esn = options.esn + 1
        else:
            hesn = "0x"+data[0].replace(' ','')
            outputname = ("%d.xml" % int(hesn,16))
        output = p.coldcartXml(cc, data)
        f = open(outputname, 'w')
        f.write(output)
        f.close()
        
    for i in bands:
        wca = "WCA%d" % i
        try:
            data = p.processWCA(wca)
        except UnboundLocalError:
           print "%s is not defined in the file skipping" % wca
           continue
        if options.esn is not None:
            outputname = ("%d.xml" % options.esn)
            options.esn = options.esn + 1
        else:
            hesn = "0x"+data[0].replace(' ','')
            outputname = ("%d.xml" % int(hesn,16))
        output = p.wcaXml(wca, coldMult[i], pll[i], data)
        f = open(outputname, 'w')
        f.write(output)
        f.close()
