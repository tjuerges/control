#!/usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
'''
This script contains a class useful to retrieve information about
the Serial Numbers configured in a STE 
'''
from CCL.Cryostat import Cryostat
from Acspy.Clients import SimpleClient
from xml.dom.minidom import parseString
from optparse import OptionParser

# defining global vars
ValidFEDevices=[]
for i in range(10):
    ValidFEDevices.append('WCA'+str(i+1))
    ValidFEDevices.append('ColdCart'+str(i+1))
    ValidFEDevices.append('PowerDist'+str(i+1))
ValidFEDevices.append('Cryostat')
ValidFEDevices.append('IFSwitch')
ValidFEDevices.append('LPR')

# FE number will be determined from its FEMC S/N
FeNumber={}
FeNumber['1186321077283324034']='12'
FeNumber['1206516576149831703']='7'
FeNumber['1183227051562762256']='5'
FeNumber['1213823930428031102']='9'
FeNumber['1195657799313915993']='3'
FeNumber['1187741315593928732']='1'
FeNumber['1200126214569197733']='6'
FeNumber['1206864352536690858']='11'
FeNumber['1204614751746261128']='10'
FeNumber['1159622405225185394']='4'
FeNumber['1181806482539675841']='19'
FeNumber['1173609292642123981']='17'




class FEAntennaData:
    def __init__(self,antennaName):
        # deviceList: dictionary referenced by its serial number
        self.validDevices={}
        # xmlList: dictionary with XML configurations referenced by the device name 
        self.xmls={}
        self.antennaName=antennaName
        self.serialNumbersDeviceMap={}
        self.feNumber='unknown'
 
    def addDevice(self,serialNumber,deviceName, xmlConfiguration):
        if FeNumber.has_key(serialNumber):
            self.feNumber=FeNumber[serialNumber]
        if (ValidFEDevices.count(deviceName)>0):
            self.validDevices[serialNumber]=deviceName
        self.xmls[serialNumber]=xmlConfiguration
        self.serialNumbersDeviceMap[serialNumber]=deviceName
    def getDeviceList(self):
        return self.validDevices.values()
    def getSerialNumberList(self):
        return self.xmls.keys()
    def getXML(self,serialNumber):
        return self.xmls[serialNumber]
    def getXMLfromDeviceName(self,deviceName):
        for sn,dev in self.serialNumbersDeviceMap.iteritems():
            if dev == deviceName:
                return self.xmls[sn]
    def getDeviceName(self,serialNumber):
        return self.serialNumbersDeviceMap[serialNumber]
    

        
class FrontEndInfo:
    def __init__(self):
        self.FEDataList={}
        sc=SimpleClient.PySimpleClient()
        self.tmcdb=sc.getComponent(comp_idl_type='IDL:alma/TMCDB/Access:1.0')
        startAntConf=self.tmcdb.getStartupAntennasInfo()
        self.antennaList=[]
        for antennaConf in startAntConf:
            antennaName=antennaConf.antennaName
            try:
                cryo=Cryostat(antennaName)
            except Exception: 
                    print "Could not get an instance of "+antennaName+" Cryostat. Skipping antenna"
                    continue
            snlist=[]
            try:
                for i in range(cryo.GET_ESNS_FOUND()[0]+2):
                    snlist.append(str(cryo.GET_ESNS()[0]))
                snlist.remove('0')
                fe=FEAntennaData(antennaName)
            except Exception:
                print "Could not read ESNS for "+antennaName+" FrontEnd. Skipping antenna"
                continue  
            self.antennaList.append(antennaName)
            for sn in snlist:
                try:
                    assemblyConf=self.tmcdb.getAssemblyConfigData(sn)
                    xml=assemblyConf.xmlDoc
                    dom=parseString(xml)
                except:
                    xml='None'
                    deviceName='Not assigned'
                try:
                    if (xml != 'None'):
                        deviceName=str(dom.getElementsByTagName('ASSEMBLY')[0].getAttribute('value'))
                except:
                    print "Invalid XML"
                    xml='Invalid XML'
                    deviceName='Not assigned'
                fe.addDevice(sn, deviceName, xml)
                self.FEDataList[antennaName]=fe
    def printFEInfo(self,antennaName):
        fe=self.FEDataList[antennaName]
        print "| *Serial number* | *FE #* | *antennaName* | *Device* |"
        for sn in fe.getSerialNumberList():
            print " | "+sn+" | "+fe.feNumber+" | "+antennaName+" | !"+fe.getDeviceName(sn)+" |"
    def printAllFEInfo(self):
        for antenna in self.antennaList:
            self.printFEInfo(antenna)
    def saveXmlToFile(self,antennaName,basedir):
        fe=self.FEDataList[antennaName]
        devices=fe.getDeviceList()
        for dev in devices:
            file=open(basedir+'/'+antennaName+"_"+dev+".xml","w")
            file.write(fe.getXMLfromDeviceName(dev))
            file.close()
    def saveAllXmlsToFile(self,basedir):
        for antenna in self.antennaList:
            self.saveXmlToFile(antenna, basedir)
        
            
if __name__ == '__main__':
    # Options section
    parser = OptionParser()
    parser.set_usage("""
    getFEConfigurations [options]
    
    This script retrieves the FE configuration for the antennas installed in the STE.
    It requiresn operational system to work""")
    parser.add_option("-a","--antennaName", dest="antennaName", metavar="AntennaName",
                  default='all',help="To look only for 'antennaName' configuration. If not specified, it will look for all")
    parser.add_option("-d","--dir", dest="basedir", metavar="BaseDir", default="./",
                  help="Specify the directory where the XML files will be save when using -s option, by default, the current dir")
    parser.add_option("-s","--save",dest="savetofile", metavar="Save to file",
                  action = "store_true", default = False,
                  help="If present, it will save XML the configuration to files called <antenna>_<device.xml>")
    (options, args) = parser.parse_args()
    feinfo=FrontEndInfo()
    if options.antennaName != 'all':
        if feinfo.antennaList.count(options.antennaName) == 0:
            print options.antennaName +" is  not running in this STE"
            exit(0)
        if options.savetofile:
            feinfo.saveXmlToFile(options.antennaName, options.basedir)
        else:
            feinfo.printFEInfo(options.antennaName)
    else:
        if options.savetofile:
            feinfo.saveAllXmlsToFile(options.basedir)
        else:
            feinfo.printAllFEInfo()


