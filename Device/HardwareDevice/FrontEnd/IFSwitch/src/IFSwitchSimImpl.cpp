
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File IFSwitchSimImpl.cpp
 *
 * $Id$
 */

#include "IFSwitchSimImpl.h"
#include <logging.h>

/**
 *-----------------------
 * IFSwitch Constructor
 *-----------------------
 */
IFSwitchSimImpl::IFSwitchSimImpl(const ACE_CString& name, maci::ContainerServices* cs):
    IFSwitchSimBase::IFSwitchSimBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 *-----------------------
 * IFSwitch Destructor
 *-----------------------
 */
IFSwitchSimImpl::~IFSwitchSimImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void IFSwitchSimImpl::hwSimulationAction()
    throw(CORBA::SystemException, ControlDeviceExceptions::HwLifecycleEx)
{
    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, "IFSwitchSimImpl::hwSimulationAction"));

    IFSwitchImpl::hwOperationalAction();
}

///////////////////////////////////////
// Additional methods for IFSwitch
///////////////////////////////////////












































































/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(IFSwitchImpl)
