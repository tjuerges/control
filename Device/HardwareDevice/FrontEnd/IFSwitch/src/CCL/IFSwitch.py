#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a IFSwitch device.
"""
import StatusHelper
import CCL.IFSwitchBase

class IFSwitch(CCL.IFSwitchBase.IFSwitchBase):
    '''
    The IFSwitch class inherits from the code generated IFSwitchBase
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        # FrontEnd subcomponents also have the prefix "FrontEnd/" in their
        # name, so we prepare always the componentName:
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/IFSwitch"
            antennaName = None
        CCL.IFSwitchBase.IFSwitchBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.IFSwitchBase.IFSwitchBase.__del__(self)

    def STATUS(self):
        '''
        This method displays the current status of the IFSwitch
        '''
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]

            elements = []
            try:
                value = self._devices[key].GET_CARTRIDGE()[0]
                value += 1
            except:
                value = 'N/A'
            elements.append(StatusHelper.Line("Selected Cartridge",[StatusHelper.ValueUnit(value)]))

            elements.append( StatusHelper.Separator("Polarization 0, Upper Sideband"))
            try:
                value = self._devices[key].GET_CHANNEL01_ASSEMBLY_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
                value = "%.2f" % value
            except:
                value = 'N/A'
                valueCelcius = 'N/A'
            elements.append(StatusHelper.Line("Temperature",[StatusHelper.ValueUnit(value,"K"),
                                                             StatusHelper.ValueUnit(valueCelcius,"C")]))
            try:
                value = self._devices[key].GET_CHANNEL01_ATTENUATION()[0]
                value = "%.2f" % value
            except:
                value = 'N/A'
            elements.append(StatusHelper.Line("Attenuation",[StatusHelper.ValueUnit(value,"dB")]))
            try:
                value = self._devices[key].GET_CHANNEL01_TEMP_SERVO_ENABLE()[0]
                if value==0:
                    value_t='OFF'
                elif value==1:
                    value_t='ON'
                else:
                    value_t='UNKNOWN'
            except:
                value_t = 'N/A'
            elements.append(StatusHelper.Line("Servo State",[StatusHelper.ValueUnit(value_t)]))

            elements.append( StatusHelper.Separator("Polarization 0, Lower Sideband"))
            try:
                value = self._devices[key].GET_CHANNEL02_ASSEMBLY_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
                value = "%.2f" % value
            except:
                value = 'N/A'
                valueCelcius = 'N/A'
            elements.append(StatusHelper.Line("Temperature",[StatusHelper.ValueUnit(value,"K"),
                                                             StatusHelper.ValueUnit(valueCelcius,"C")]))
            try:
                value = self._devices[key].GET_CHANNEL02_ATTENUATION()[0]
                value = "%.2f" % value
            except:
                value = 'N/A'
            elements.append(StatusHelper.Line("Attenuation",[StatusHelper.ValueUnit(value,"dB")]))
            try:
                value = self._devices[key].GET_CHANNEL02_TEMP_SERVO_ENABLE()[0]
                if value==0:
                    value_t='OFF'
                elif value==1:
                    value_t='ON'
                else:
                    value_t='UNKNOWN'
            except:
                value_t = 'N/A'
            elements.append(StatusHelper.Line("Servo State",[StatusHelper.ValueUnit(value_t)]))
            
            elements.append( StatusHelper.Separator("Polarization 1, Upper Sideband"))
            try:
                value = self._devices[key].GET_CHANNEL11_ASSEMBLY_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
                value = "%.2f" % value
            except:
                value = 'N/A'
                valueCelcius = 'N/A'
            elements.append(StatusHelper.Line("Temperature",[StatusHelper.ValueUnit(value,"K"),
                                                             StatusHelper.ValueUnit(valueCelcius,"C")]))
            try:
                value = self._devices[key].GET_CHANNEL11_ATTENUATION()[0]
                value = "%.2f" % value
            except:
                value = 'N/A'
            elements.append(StatusHelper.Line("Attenuation",[StatusHelper.ValueUnit(value,"dB")]))
            try:
                value = self._devices[key].GET_CHANNEL11_TEMP_SERVO_ENABLE()[0]
                if value==0:
                    value_t='OFF'
                elif value==1:
                    value_t='ON'
                else:
                    value_t='UNKNOWN'
            except:
                value_t = 'N/A'
            elements.append(StatusHelper.Line("Servo State",[StatusHelper.ValueUnit(value_t)]))
            
            elements.append( StatusHelper.Separator("Polarization 1, Lower Sideband"))
            try:
                value = self._devices[key].GET_CHANNEL12_ASSEMBLY_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
                value = "%.2f" % value
            except:
                value = 'N/A'
                valueCelcius = 'N/A'
            elements.append(StatusHelper.Line("Temperature",[StatusHelper.ValueUnit(value,"K"),
                                                             StatusHelper.ValueUnit(valueCelcius,"C")]))
            try:
                value = self._devices[key].GET_CHANNEL12_ATTENUATION()[0]
                value = "%.2f" % value
            except:
                value = 'N/A'
            elements.append(StatusHelper.Line("Attenuation",[StatusHelper.ValueUnit(value,"dB")]))
            try:
                value = self._devices[key].GET_CHANNEL12_TEMP_SERVO_ENABLE()[0]
                if value==0:
                    value_t='OFF'
                elif value==1:
                    value_t='ON'
                else:
                    value_t='UNKNOWN'
            except:
                value_t = 'N/A'
            elements.append(StatusHelper.Line("Servo State",[StatusHelper.ValueUnit(value_t)]))

            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
                                

        
