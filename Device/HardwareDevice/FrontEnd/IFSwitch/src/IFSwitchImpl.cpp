/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File IFSwitchImpl.cpp
 *
 * $Id$
 */

#include <IFSwitchImpl.h>
using std::string;

/**
 *-----------------------
 * IFSwitch Constructor
 *-----------------------
 */
IFSwitchImpl::IFSwitchImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    IFSwitchBase(name,cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    assembly_m = "IFSwitch";
    hasEsn_m = true;
}

/**
 *-----------------------
 * IFSwitch Destructor
 *-----------------------
 */
IFSwitchImpl::~IFSwitchImpl()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 * ------------------------------------------------------------------------------------------------
 *  Hardware Lifecycle Methods
 * ------------------------------------------------------------------------------------------------
 */



void IFSwitchImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    IFSwitchBase::hwInitializeAction();
    /* This method is only expected to throw
 * ControlDeviceExceptions::HwLifecycleEx) we must chew up everything else*/
    try {
	/*As specified by front end LO Power Amps must be shut off when the
 * band is, this will do exactly that.*/
        setCntlChannel01TempServoOn();
        setCntlChannel11TempServoOn();
        setCntlChannel02TempServoOn();
        setCntlChannel12TempServoOn();
    }  catch (ControlExceptions::CAMBErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                                              __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not power on the Temp Servo.");
        nex.log(LM_WARNING);
        // Will chew on the exception
        //throw nex.getHwLifecycleEx();
    } catch(ControlExceptions::INACTErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                                              __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not power on the Temp Servo.");
        nex.log(LM_WARNING);
        // Will chew on the exception
        //throw nex.getHwLifecycleEx();
    }
}


void IFSwitchImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    /*if the hardware is stopped, don't even try*/
    if(getHwState()==Control::HardwareDevice::Stop) return;
    /*Only attempt to poweroff the PA when software if operational*/
    if(getHwState()==Control::HardwareDevice::Operational){
        try {
    	/*As specified by front end LO Power Amps must be shut off when the
     * band is, this will do exactly that.*/
            setCntlChannel01TempServoOff();
            setCntlChannel11TempServoOff();
            setCntlChannel02TempServoOff();
            setCntlChannel12TempServoOff();
        }  catch (ControlExceptions::CAMBErrorExImpl &ex){
            ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                                                  __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail","Could not power on the Temp Servo.");
            nex.log(LM_WARNING);
            // Will chew on the exception
            //throw nex.getHwLifecycleEx();
        } catch(ControlExceptions::INACTErrorExImpl &ex){
            ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                                                  __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail","Could not power on the Temp Servo.");
            nex.log(LM_WARNING);
            // Will chew on the exception
            //throw nex.getHwLifecycleEx();
        }
    }
    IFSwitchBase::hwStopAction();
}

void IFSwitchImpl::selectBand(ReceiverBandMod::ReceiverBand band)
{
    try {

        selectBandInternal(band);

    } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch (ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl &ex) {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    } catch (...) {
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getHardwareErrorEx();
    }
}


void IFSwitchImpl::selectBandInternal(ReceiverBandMod::ReceiverBand band)
{
    switch (band) {
    case ReceiverBandMod::ALMA_RB_01:
        setCntlCartridge1();
        break;
    case ReceiverBandMod::ALMA_RB_02:
        setCntlCartridge2();
        break;
    case ReceiverBandMod::ALMA_RB_03:
        setCntlCartridge3();
        break;
    case ReceiverBandMod::ALMA_RB_04:
        setCntlCartridge4();
        break;
    case ReceiverBandMod::ALMA_RB_05:
        setCntlCartridge5();
        break;
    case ReceiverBandMod::ALMA_RB_06:
        setCntlCartridge6();
        break;
    case ReceiverBandMod::ALMA_RB_07:
        setCntlCartridge7();
        break;
    case ReceiverBandMod::ALMA_RB_08:
        setCntlCartridge8();
        break;
    case ReceiverBandMod::ALMA_RB_09:
        setCntlCartridge9();
        break;
    case ReceiverBandMod::ALMA_RB_10:
        setCntlCartridge10();
        break;
    default:
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        std::string msg("Band not valid.");
        ex.addData("Additional", msg);
        ex.log();
        throw ex;
    }
}
///////////////////////////////////////
// Additional methods for IFSwitch
///////////////////////////////////////

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(IFSwitchImpl)
