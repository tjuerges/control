#ifndef IFSwitchImpl_H
#define IFSwitchImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File IFSwitchImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <IFSwitchBase.h>
#include <IFSwitchS.h>
#include <almaEnumerations_IFC.h>

class IFSwitchImpl: public IFSwitchBase,
                    virtual public POA_Control::IFSwitch
{
    public:
    /**
     * Constructor
     */
    IFSwitchImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~IFSwitchImpl();

    /**
     * void hwInitializeAction()
     * @override IFSwitchBase::hwInitializeAction()
     * @except ControlDeviceExceptions::HwLifecycleEx
     */
    virtual void hwInitializeAction();

    /**
     * void hwStopAction()
     * @override IFSwitchBase::hwStopAction()
     * @except ControlDeviceExceptions::HwLifecycleEx
     */
    virtual void hwStopAction();

    /**
     * void selectBand
     * @param band
     * @except ControlExceptions::IllegalParameterErrorEx
     * @except ControlExceptions::HardwareErrorEx
     * @except ControlExceptions::INACTErrorEx
     */
    void selectBand(ReceiverBandMod::ReceiverBand band);

    private:
    void selectBandInternal(ReceiverBandMod::ReceiverBand band);

};
#endif // IFSwitchImpl_H
