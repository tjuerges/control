#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
# $Source$


"""
This module is part of the Control Command Language.
It contains complementary functionality for a MPR device.
"""


import CCL.HardwareDevice
import CCL.logging
import CCL.FEPSBase
from CCL import StatusHelper


class FEPS(CCL.FEPSBase.FEPSBase):
    '''
    The FEPS class inherits from the code generated FEPSBase
    class and adds specific methods.
    '''
    def __init__(self, \
            antennaName = None, \
            componentName = None, \
            stickyFlag = False):
        '''
        The constructor creates a FEPS object using FEPSBase constructor.
        EXAMPLE:
        from CCL.FEPS import FEPS
        obj = FEPS("DA41")

        '''
        # Dictionary of device instances
        self._devices = {}

        # Group of antenna or component names.
        if ((isinstance(antennaName,list) == True)
            or (isinstance(componentName,list) == True)):
            #antenna names
            if isinstance(antennaName,list) == True:
                if len(antennaName) != 0:
                    for idx in range (0,len(antennaName)):
                        self._devices[ \
                            "CONTROL/" + antennaName[idx] + "/FEPS"] = ""
            #component names
            if isinstance(componentName,list) == True:
                if len(componentName) != 0:
                    for idx in range (0,len(componentName)):
                        self._devices[componentName[idx]] = ""
        # One component
        else:
            if not((antennaName == None) ^ (componentName == None)):
                raise NameError, "missing antennaName or componentName"

            if antennaName != None:
                self._devices["CONTROL/" + antennaName + "/FEPS"] = ""

            if componentName != None:
                self._devices[componentName] = ""

        # Initialize the base class
        for key, val in self._devices.iteritems():
            CCL.HardwareDevice.HardwareDevice.__init__(self, \
                key, \
                stickyFlag);
            self._devices[key] = self._HardwareDevice__hw

        self.__logger = CCL.logging.getLogger()

    def __del__(self):

        for key, val in self._devices.iteritems():
            instance = self._devices[key]
            del(instance)
        CCL.FEPSBase.FEPSBase.__del__(self)

    # Values shown according JIRA:AIV-4683
    # 
    def STATUS(self):
        '''
        This method print the Status of the FEPS. 
        '''
        
        #result = {}
        for key, val in self._devices.iteritems():
            print key, val
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            elements = []
            
            ############################### FEPS SN ###################################################
            elements.append( StatusHelper.Separator("General") )
            try:
                value = "%s" % self._devices[key].GET_ID_SER_NUM()[0]
            except:
                value = "N/A"
            
            elements.append( StatusHelper.Line("FEPS SN",
                                               [StatusHelper.ValueUnit(value)]) )
            
            
            ############################### DC Power Supply Output ###################################################
            elements.append( StatusHelper.Separator("DC Power Supply Output") )
            
            try:
                Vmon = "%.3f" % self._devices[key].GET_PLUS_5V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_PLUS_5V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_PLUS_5V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("+5V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )
        
            try:
                Vmon = "%.3f" % self._devices[key].GET_PLUS_16V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_PLUS_16V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_PLUS_16V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("+16V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )
    
            try:
                Vmon = "%.3f" % self._devices[key].GET_PLUS_9V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_PLUS_9V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_PLUS_9V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("+9V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )

            try:
                Vmon = "%.3f" % self._devices[key].GET_PLUS_6V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_PLUS_6V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_PLUS_6V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("+6V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )
        
    
            try:
                Vmon = "%.3f" % self._devices[key].GET_MINUS_7V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_MINUS_7V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_MINUS_7V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("-7V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )
        
            try:
                Vmon = "%.3f" % self._devices[key].GET_PLUS_7V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_PLUS_7V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_PLUS_7V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("+7V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )
        
    
            try:
                Vmon = "%.3f" % self._devices[key].GET_MINUS_16V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_MINUS_16V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_MINUS_16V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("-16V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )
            
            try:
                Vmon = "%.3f" % self._devices[key].GET_PLUS_25V_VOLTAGE()[0]
                Imon = "%.3f" % self._devices[key].GET_PLUS_25V_CURRENT()[0]
                Temp = "%.1f" % self._devices[key].GET_PLUS_25V_TEMP()[0]
            except:
                Vmon, Imon, Temp = ("N/A", "N/A", "N/A")
                
            elements.append( StatusHelper.Line("+25V",
                                               [StatusHelper.ValueUnit(Vmon, "V", label="Vmon"),
                                                StatusHelper.ValueUnit(Imon, "mA", label="Imon"),
                                                StatusHelper.ValueUnit(Temp, "C", label="Temp")]) )
            
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName,  elements )
            statusFrame.printScreen()
