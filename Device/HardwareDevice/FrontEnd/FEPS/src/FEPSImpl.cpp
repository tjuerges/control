//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <FEPSImpl.h>
#include <acsContainerServices.h>


FEPSImpl::FEPSImpl(const ACE_CString& name, maci::ContainerServices* cs):
    FEPSBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

FEPSImpl::~FEPSImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(FEPSImpl)
