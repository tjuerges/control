
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCart3SimImpl.cpp
 *
 * $Id$
 */

#include "ColdCart3SimImpl.h"
#include <logging.h>

/**
 *-----------------------
 * ColdCart3 Constructor
 *-----------------------
 */
ColdCart3SimImpl::ColdCart3SimImpl(const ACE_CString& name, maci::ContainerServices* cs):
    ColdCart3SimBase::ColdCart3SimBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 *-----------------------
 * ColdCart3 Destructor
 *-----------------------
 */
ColdCart3SimImpl::~ColdCart3SimImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void ColdCart3SimImpl::hwSimulationAction()
    throw(CORBA::SystemException, ControlDeviceExceptions::HwLifecycleEx)
{
    ACS_LOG(LM_SOURCE_INFO, __FUNCTION__, (LM_INFO, __PRETTY_FUNCTION__));

    ColdCart3Impl::hwOperationalAction();
}

///////////////////////////////////////
// Additional methods for ColdCart3
///////////////////////////////////////












































































































































































































































/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ColdCart3Impl)
