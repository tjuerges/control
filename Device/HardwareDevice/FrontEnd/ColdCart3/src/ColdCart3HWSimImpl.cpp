
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCart3HWSimImpl.cpp
 *
 * $Id$
 */

#include "ColdCart3HWSimImpl.h"

using namespace AMB;

/* Please use this class to implement complex functionality for the
 * ColdCart3HWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

ColdCart3HWSimImpl::ColdCart3HWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: ColdCart3HWSimBase::ColdCart3HWSimBase(node, serialNumber)
{

}

// Specific Control set helpers
// ----------------------------------------------------------------------------
void ColdCart3HWSimImpl::setControlSetPol0Sb2SisVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB2_SIS_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB2_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_SIS_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_SIS_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_SIS_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_SIS_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2SisVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB2_SIS_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB2_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_SIS_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_SIS_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_SIS_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_SIS_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_SIS_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2SisOpenLoop(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL0_SB2_SIS_OPEN_LOOP");
	if (state_m.find(controlPoint_POL0_SB2_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_SIS_OPEN_LOOP)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_SIS_OPEN_LOOP. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_SIS_OPEN_LOOP)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_SIS_OPEN_LOOP. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2SisOpenLoop(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL1_SB2_SIS_OPEN_LOOP");
	if (state_m.find(controlPoint_POL1_SB2_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_SIS_OPEN_LOOP)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_SIS_OPEN_LOOP. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_SIS_OPEN_LOOP) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_SIS_OPEN_LOOP)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_SIS_OPEN_LOOP. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2LnaEnable(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL0_SB2_LNA_ENABLE");
	if (state_m.find(controlPoint_POL0_SB2_LNA_ENABLE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_LNA_ENABLE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_LNA_ENABLE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_LNA_ENABLE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_LNA_ENABLE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_LNA_ENABLE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2LnaEnable(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,1,"controlPoint_POL1_SB2_LNA_ENABLE");
	if (state_m.find(controlPoint_POL1_SB2_LNA_ENABLE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_LNA_ENABLE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_LNA_ENABLE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_LNA_ENABLE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_LNA_ENABLE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_LNA_ENABLE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2Lna1DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB2_LNA1_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB2_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_LNA1_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_LNA1_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_LNA1_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_LNA1_DRAIN_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2Lna1DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB2_LNA1_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB2_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_LNA1_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_LNA1_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_LNA1_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_LNA1_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_LNA1_DRAIN_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2Lna1DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB2_LNA1_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL0_SB2_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_LNA1_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_LNA1_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_LNA1_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_LNA1_DRAIN_CURRENT. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2Lna1DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB2_LNA1_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL1_SB2_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_LNA1_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_LNA1_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_LNA1_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_LNA1_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_LNA1_DRAIN_CURRENT. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2Lna2DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB2_LNA2_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB2_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_LNA2_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_LNA2_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_LNA2_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_LNA2_DRAIN_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2Lna2DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB2_LNA2_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB2_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_LNA2_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_LNA2_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_LNA2_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_LNA2_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_LNA2_DRAIN_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2Lna2DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB2_LNA2_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL0_SB2_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_LNA2_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_LNA2_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_LNA2_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_LNA2_DRAIN_CURRENT. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2Lna2DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB2_LNA2_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL1_SB2_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_LNA2_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_LNA2_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_LNA2_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_LNA2_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_LNA2_DRAIN_CURRENT. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2Lna3DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB2_LNA3_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL0_SB2_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_LNA3_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_LNA3_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_LNA3_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_LNA3_DRAIN_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2Lna3DrainVoltage(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB2_LNA3_DRAIN_VOLTAGE");
	if (state_m.find(controlPoint_POL1_SB2_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_LNA3_DRAIN_VOLTAGE)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_LNA3_DRAIN_VOLTAGE. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_LNA3_DRAIN_VOLTAGE) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_LNA3_DRAIN_VOLTAGE)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_LNA3_DRAIN_VOLTAGE. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol0Sb2Lna3DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL0_SB2_LNA3_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL0_SB2_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL0_SB2_LNA3_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL0_SB2_LNA3_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL0_SB2_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL0_SB2_LNA3_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL0_SB2_LNA3_DRAIN_CURRENT. Member not found.");
}

void ColdCart3HWSimImpl::setControlSetPol1Sb2Lna3DrainCurrent(const std::vector<CAN::byte_t>& data)
{
	checkSize(data,4,"controlPoint_POL1_SB2_LNA3_DRAIN_CURRENT");
	if (state_m.find(controlPoint_POL1_SB2_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(controlPoint_POL1_SB2_LNA3_DRAIN_CURRENT)->second) = data;
	else
		throw CAN::Error("Trying to set controlPoint_POL1_SB2_LNA3_DRAIN_CURRENT. Member not found.");
	if (state_m.find(monitorPoint_POL1_SB2_LNA3_DRAIN_CURRENT) != state_m.end())
		*(state_m.find(monitorPoint_POL1_SB2_LNA3_DRAIN_CURRENT)->second) = plusone(data);
	else
		throw CAN::Error("Trying to set monitorPoint_POL1_SB2_LNA3_DRAIN_CURRENT. Member not found.");
}
