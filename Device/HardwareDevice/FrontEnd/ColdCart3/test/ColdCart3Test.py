#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
from Acspy.Clients.SimpleClient import PySimpleClient

# Other imports
from time import sleep

# Exceptions
import ControlDeviceExceptions
import exceptions

# For Control
import Control

# Automated test class
class automated( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()
        # Define the component name to be used
        self.componentName = "CONTROL/DA41/FrontEnd/ColdCart3"
        try:
            # Get the component reference
            self.coldcart = self.client.getComponent(self.componentName)
        except Exception, e:
            print "Exception: ", str(e)

    def tearDown(self):
       del(self.coldcart)
       self.client.releaseComponent(self.componentName)
       sleep(2)
       self.client.disconnect()
       del(self.client)

    def initColdCart(self):
        self.coldcart.hwConfigure()
	self.coldcart.hwInitialize()
        self.coldcart.hwOperational()

    def cleanup(self):
            print "Cleaning up..."
            # Nothing to be done for now
            
    def test01(self):
        '''
       Life Cycle 
        '''
        try:
            self.initColdCart()
            self.coldcart.hwStop()
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()
            
    def test02(self):
        '''
       Test setBandLNABias(); 
        '''
        try:
            self.initColdCart()
            self.coldcart.setConfigFreq(92E9);
            self.coldcart.setBandLNABias();
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()
            
    def test03(self):
        '''
       Test setBandEnableLNABias(in boolean enable)
        '''
        try:
            self.initColdCart()
            self.coldcart.setConfigFreq(92E9);
            self.coldcart.setBandEnableLNABias(True);
            self.coldcart.setBandEnableLNABias(False);
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()
            
    def test04(self):
        '''
       Test setBandSISBias(in boolean enable)
        '''
        try:
            self.initColdCart()
            self.coldcart.setConfigFreq(92E9);
            self.coldcart.setBandSISBias(True);
            self.coldcart.setBandSISBias(False);
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()

    def test05(self):
        '''
       Test setBandSISMagnet(in boolean enable)
        '''
        try:
            self.initColdCart()
            self.coldcart.setConfigFreq(92E9);
            self.coldcart.setBandSISMagnet(True);
            self.coldcart.setBandSISMagnet(False);
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()

    def test06(self):
        '''
       Test getIVData
        '''
        try:
            self.initColdCart()
            self.coldcart.getIVData(0.01,0.02,0.005)
        except Exception, e:
            print "Exception: ", str(e)
            self.cleanup()
            self.fail()


# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()


