
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 * 
 * File ColdCart3HWSimImpl.h
 *
 * $Id$
 */
#ifndef ColdCart3HWSimImpl_H
#define ColdCart3HWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "ColdCart3HWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * ColdCart3HWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class ColdCart3HWSimImpl : public ColdCart3HWSimBase
  {
  public :
	ColdCart3HWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);
	
// Specific Control set helpers
// ----------------------------------------------------------------------------
	virtual void setControlSetPol0Sb2SisVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2SisVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2SisOpenLoop(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2SisOpenLoop(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2LnaEnable(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2LnaEnable(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2Lna1DrainVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2Lna1DrainVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2Lna1DrainCurrent(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2Lna1DrainCurrent(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2Lna2DrainVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2Lna2DrainVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2Lna2DrainCurrent(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2Lna2DrainCurrent(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2Lna3DrainVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2Lna3DrainVoltage(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol0Sb2Lna3DrainCurrent(const std::vector<CAN::byte_t>& data);

	virtual void setControlSetPol1Sb2Lna3DrainCurrent(const std::vector<CAN::byte_t>& data);
  }; // class ColdCart3HWSimImpl

} // namespace AMB

#endif // ColdCart3HWSimImpl_H
