#ifndef ColdCart3Impl_H
#define ColdCart3Impl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCart3Impl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <ColdCart3Base.h>
#include <ColdCart3S.h>

//Exceptions
#include <ColdCartExceptions.h>

class ColdCart3Impl: public ColdCart3Base,
                     virtual public POA_Control::ColdCart3
{
    public:
    /**
     * Constructor
     */
    ColdCart3Impl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~ColdCart3Impl();

    protected:

    //See ColdCartImpl.h for the description of this method.
    void setLNABias(int pol = -1, int sb = -1);


    //See ColdCartImpl.h for the description of this method.
    void setEnableLNABias(bool enable = false, int pol = -1, int sb = -1);

    //See ColdCartImpl.h for the description of this method.
    void setSISBias(bool enable = false, int pol = -1, int sb = -1, int openLoop = -1);

    //See ColdCartImpl.h for the description of this method.
    void setBandOpenLoop(bool openLoop);

    //See ColdCartImpl.h for the description of this method.
    void setSISMagnet(bool enable = false, int pol = -1, int sb = -1){/*NOOP for BAND 3*/};

    //See ColdCartImpl.h for the description of this method.
    virtual void hwInitializeAction();

    //See ColdCartImpl.h for the description of this method.
    virtual void setSisVoltage(const float voltage);
     
    //See ColdCartImpl.h for the description of this method.
    virtual void setSisVoltage(const std::vector< float >& voltage);
     
    //See ColdCartImpl.h for the description of this method.
    virtual void getSisVoltage(std::vector< float >& voltage);
     
    //See ColdCartImpl.h for the description of this method.
    virtual void setSisMagnetCurrent(const float current){/*NOOP for BAND 3*/};
     
    //See ColdCartImpl.h for the description of this method.
    virtual void setSisMagnetCurrent(const std::vector< float >& current){/*NOOP for BAND 3*/};

    //See ColdCartImpl.h for the description of this method.
    virtual void getSisCurrent(std::vector< float >& current);
};
#endif // ColdCart3Impl_H
