#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a WCA6 device.
"""

import CCL.WCA6Base
from CCL import StatusHelper

class WCA6(CCL.WCA6Base.WCA6Base):
    '''
    The WCA6 class inherits from the code generated WCA6Base
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor

        Example:
        wca6 = WCA6("DA41")
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/WCA6"
            antennaName = None
        CCL.WCA6Base.WCA6Base.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.WCA6Base.WCA6Base.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the WCA6
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]
        
            # Invoke the generic WCA status helper
            elements = self._WCA__STATUSHelper(key)
            
            ############################### WCA6 specific ###################################################
            #elements.append( StatusHelper.Separator("WCA6 specific") )
                  
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
