/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WCA6Impl.cpp
 *
 * $Id$
 */

#include <WCA6Impl.h>

/**
 *-----------------------
 * WCA6 Constructor
 *-----------------------
 */
WCA6Impl::WCA6Impl(const ACE_CString& name,
        maci::ContainerServices* cs):
    WCA6Base(name,cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    band_m = ReceiverBandMod::ALMA_RB_06;
    WCABase::setBaseAddress(getBaseAddress());
    m_mutexIndex=5;
    pthread_mutex_init(&m_mutex[m_mutexIndex], NULL);
    //From FEMC
    assembly_m = "WCA6";
}

/**
 *-----------------------
 * WCA6 Destructor
 *-----------------------
 */
WCA6Impl::~WCA6Impl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_destroy(&m_mutex[m_mutexIndex]);
}

///////////////////////////////////////
// Additional methods for WCA6
///////////////////////////////////////

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(WCA6Impl)
