#ifndef LPR_IDL
#define LPR_IDL
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LPR.idl
 *
 * $Id$
 */

/**
 * External device interface to LPR.
 */

#include <LPRBase.idl>
#include <almaEnumerations_IF.idl>

#pragma prefix "alma"

module Control
{
    interface LPR: Control::LPRBase
    {
        /**
        * void prepEDFAModulationVoltage()
        * Step the EDFA Moulation voltage until 
        * the power is of 2mW.
        * @throw ControlExceptions::CAMBErrorEx
        * @throw ControlExceptions::INACTErrorEx
        */
        void prepEDFAModulationVoltage() 
            raises (ControlExceptions::CAMBErrorEx,
                    ControlExceptions::INACTErrorEx);

        /**
        * void photonicStandby()
        * Set EDFA Moulation voltage to cero and put
        * the caps on the lasers
        * @throw ControlExceptions::CAMBErrorEx
        * @throw ControlExceptions::INACTErrorEx
        */
        void photonicStandby() 
            raises (ControlExceptions::CAMBErrorEx,
                    ControlExceptions::INACTErrorEx);

        /**
        * float getLastEDFA()
        * retrun the last commanded EDFA voltage that was different to 0.
        */
        float getLastEDFA();

        void selectBand(in ReceiverBandMod::ReceiverBand band)
             raises (ControlExceptions::INACTErrorEx,
                     ControlExceptions::HardwareErrorEx,
                     ControlExceptions::IllegalParameterErrorEx);
    };
};
#endif /* LPR_IDL */
