#ifndef LPRImpl_H
#define LPRImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LPRImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

// Base class
#include <LPRBase.h>
#include <LPRS.h>

#include <almaEnumerations_IFC.h>

class LPRImpl: public LPRBase,
               virtual public POA_Control::LPR
{
    public:
    /**
     * Constructor
     */
    LPRImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~LPRImpl();
    /* IDL SPECIFIED INTERFACE */

    /**
    * void prepEDFAModulationVoltage()
    * Step the EDFA Modulation voltage until 
    * the power is of 2mW.
    * @except ControlExceptions::CAMBErrorEx
    * @except ControlExceptions::INACTErrorEx
    */
    void prepEDFAModulationVoltage();

    /**
    * void photonicStandby()
    * Set EDFA Moulation voltage to zero and put
    * the caps on the lasers
    * @except ControlExceptions::CAMBErrorEx
    * @except ControlExceptions::INACTErrorEx
    */
    void photonicStandby();

    /**
    * float getLastEDFA()
    * return the last commanded EDFA voltage that was different to 0.
    */
    float getLastEDFA();

    /* END IDL SPECIFIED INTERFACE */
    /**
     * @override LPRBase::hwInitializeAction
     * @except ControlDeviceExceptions::HwLifecycleEx
     */
    virtual void hwInitializeAction();

    void selectBand(ReceiverBandMod::ReceiverBand band);

    private:
    void selectBandInternal(ReceiverBandMod::ReceiverBand band);

    /* Hack for patched LPR */
    /////////// ControlPoint: OPT_SWITCH_PORT
    /**
     * @override LPRBase::setCntlOptSwitchPort
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     */
    virtual void setCntlOptSwitchPort(const unsigned char world);

    /////////// ControlPoint: MODULATION_INPUT_VOLTAGE
    /**
     * @override LPRBase::setCntlModulationInputValue
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     */
    virtual void setCntlModulationInputValue(const float world);


    
    /**
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except LPRExceptions::DamageThresholdExImpl
     */
    virtual void checkBurnoutPower(const float world);

    /**
     * @override LPRBase::setCntlModulationInputValue
     * @except ControlExceptions::CAMBErrorExImpl
     * @except ControlExceptions::INACTErrorExImpl
     * @except LPRExceptions::SafeThresholdExImpl
     */
    virtual void checkLimitPower(const float world); 



    bool isPatched_m;
    float edfaLast_m; //Keep record of the last commanded value != 0.

};
#endif // LPRImpl_H
