/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LPRImpl.cpp
 *
 * $Id$
 */

#include <LPRImpl.h>
#include <LPRExceptions.h>
#include <FrontEndUtils.h>

/**
 *-----------------------
 * LPR Constructor
 *-----------------------
 */
LPRImpl::LPRImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    LPRBase(name,cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    configData_m=NULL;
    assembly_m = "LPR";
    hasEsn_m = true;
    //HACK for Broken LPR
    isPatched_m = false;
    edfaLast_m = -1.0;
}

/**
 *-----------------------
 * LPR Destructor
 *-----------------------
 */
LPRImpl::~LPRImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

///////////////////////////////////////
// Additional methods for LPR
///////////////////////////////////////
void LPRImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    double edfaVoltage = 0.0;
    LPRBase::hwInitializeAction();
    if (configData_m == NULL) {
        ControlDeviceExceptions::HwLifecycleExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        std::string msg = "there is no config data for LPR";
        ex.addData("Detail", msg);
        ex.log(LM_WARNING);
        return;
    }
    try {
        std::auto_ptr<const ControlXmlParser> tmp = configData_m->getElement("EDFAVoltage");
        edfaVoltage = (*tmp).getDoubleAttribute("value");
    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,__FILE__, __LINE__, __PRETTY_FUNCTION__);
        std::string msg = "Could not access config data";
        nex.addData("Detail", msg);
        nex.log(LM_WARNING);
        // Will chew on the exception since it is not fatal
    }

    /* This method is only expected to throw
     * ControlDeviceExceptions::HwLifecycleEx) we must chew up everything else*/
    try {
        /* the EDFA voltage is set during the initialization. This value is different
         * for every FrontEnd and is determined empirically
         */
        setCntlModulationInputValue(edfaVoltage);
    }  catch (ControlExceptions::CAMBErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not set the modulation input value (EDFA voltage)");
        nex.log(LM_WARNING);
        // Will chew on the exception since it is not fatal
    } catch(ControlExceptions::INACTErrorExImpl &ex){
        ControlDeviceExceptions::HwLifecycleExImpl nex(ex,
                __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail","Could not set the modulation input value (EDFA voltage)");
        nex.log(LM_WARNING);
        // Will chew on the exception since it is not fatal
    }
    /*  HACK
        This hack is for DV01'FE LPR. It will move the switch to Band 5 instead 3 when Band 3 will be required
     */
    try {
        std::auto_ptr<const ControlXmlParser> tmp = configData_m->getElement("isPatched");
        isPatched_m = (*tmp).getBoolAttribute("value");
    } catch (ControlExceptions::XmlParserErrorExImpl &ex) {
        /* We expect to catch an exception here, the usual behavior is do not have the 'isPatched' attribute */
    }
    /* End of Catch */

}

void LPRImpl::setCntlOptSwitchPort(const unsigned char world)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    unsigned char currentport;
    /* HACK --  LPR. Delete this method after the DV01 LPR is fixed  */
    unsigned char world2=world;
    if ( isPatched_m && (world2==2)) {   // if Band 3 is requested
        world2=4;   // we will set the band to Band 5
    }
    currentport = getOptSwitchPort(timestamp);
    //Only send the command if we are actually change ports
    if (currentport != world2) {
        //Set EDFA_voltage to zero, then switch band.
        setCntlModulationInputValue(0.0);
        ACS_LOG(LM_SOURCE_INFO, "setCntlOptSwitchPort",
                (LM_INFO,"EDFA voltage set to 0 when changing from band %i to %i",currentport,world2));
        LPRBase::setCntlOptSwitchPort(world2);
    }
}

void LPRImpl::setCntlModulationInputValue(const float world)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    float edfaVoltage = 3.51;//Initialize at maxed out values
    ACS::Time timestamp;
    //We could just leave the BASE implementation deal with out of range values,
    //but i prefer to slap the face of the user right away. Otherwise this routine
    // will drive the voltage to 0.0 of 3.5 before noticing the input data is rubbish.
    if(world < static_cast< float >(0.0)) {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The requested value ("
            << world
            << ") for this Control Point ("
            "MODULATION_INPUT_VALUE) is less than the MinRange (0.0) which has been "
            "defined in the spreadsheet!, and overridden in the implementation";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }

    if(world > static_cast< float >(3.5)) {
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The requested value ("
            << world
            << ") for this Control Point ("
            "MODULATION_INPUT_VALUE) is greater than the MaxRange (3.5) which has been "
            "defined in the spreadsheet!, and overridden in the implementation";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }
    edfaVoltage = getModulationInputValue(timestamp);

    if (edfaVoltage == world) return; //do nothing if setting the same value

    if(world < edfaVoltage) {
        //The new value is lower than the current value.
        // Go ahead an set the value
        LPRBase::setCntlModulationInputValue(world);
        usleep(50000);//50ms
        //wait 50ms for settling, this was suggested in a verbal
        //communication by the LPR designer.  There is no direct mention of
        //this specification in ALMA-56.05.00.00-023-A-SPE 2009-09-29.
        edfaVoltage = getModulationInputValue(timestamp);
        if(world!=0.0)
            edfaLast_m = edfaVoltage;
        try {
            checkBurnoutPower(world);
        } catch (LPRExceptions::DamageThresholdExImpl &ex) {
            ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.log();
            throw nex;
        }
    } else {
        // Driving more power, so take precautions.
        //Check safe guard with current voltage.
        try {
            checkBurnoutPower(edfaVoltage);
            checkLimitPower(edfaVoltage);
        } catch (LPRExceptions::DamageThresholdExImpl &ex) {
            ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
            nex.log();
            throw nex;
        } catch (LPRExceptions::SafeThresholdExImpl &ex) {
            ex.log(LM_WARNING);
            return;
        }
        float maxstep = 0.3;
        float nextVoltage = edfaVoltage;
        while (edfaVoltage != world ) {
            if(edfaVoltage < 1.2)
                maxstep = 0.3;
            else
                maxstep = 0.1;

            //Check we do not over step
            if ((world - edfaVoltage) >= maxstep)
                nextVoltage = edfaVoltage + maxstep;
            else
                nextVoltage = world;
            //Apply the voltage
            LPRBase::setCntlModulationInputValue(nextVoltage);
            usleep(50000);//50ms
            //wait 50ms for settling, this was suggested in a verbal
            //communication by the LPR designer.  There is no direct mention of
            //this specification in ALMA-56.05.00.00-023-A-SPE 2009-09-29.
            edfaVoltage = getModulationInputValue(timestamp);
            if(world!=0.0)
                edfaLast_m = edfaVoltage;
            try {
                checkBurnoutPower(nextVoltage);
                checkLimitPower(nextVoltage);
            } catch (LPRExceptions::DamageThresholdExImpl &ex) {
                ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
                nex.log();
                throw nex;
            } catch (LPRExceptions::SafeThresholdExImpl &ex) {
                ex.log(LM_WARNING);
                return;
            }
        }

    }
}

void LPRImpl::checkBurnoutPower(const float world)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    float photodetector = getPhotoDetectPower(timestamp);
    if (photodetector >= 8.0E-3 ) {
        //We are in the abort threshold
        LPRExceptions::DamageThresholdExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The requested value ("
            << world
            << ") for this Control Point ("
            "MODULATION_INPUT_VALUE) drove the photodetector power to the burn threshold"
            "Turning the VOLTAGE off as safe guard";
        ex.addData("Detail", msg.str());
        LPRBase::setCntlModulationInputValue(0.0);
        throw ex;
    }
}

void LPRImpl::checkLimitPower(const float world)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    float photodetector = getPhotoDetectPower(timestamp);
    if (photodetector >= 5.0E-3 ) {
        //We are in the abort threshold
        LPRExceptions::SafeThresholdExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The requested value ("
            << world
            << ") for this Control Point ("
            "MODULATION_INPUT_VALUE) drove the photodetector power to limit 5mW"
            "Aborting routines";
        ex.addData("Detail", msg.str());
        throw ex;
    }
}

void LPRImpl::prepEDFAModulationVoltage()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    float edfaVoltage = 0.0;
    float photodetector = 0.0;
    try {
        photodetector = getPhotoDetectPower(timestamp);
        edfaVoltage = getModulationInputValue(timestamp);
        while(photodetector < 2.0E-3){
            edfaVoltage+=0.1;
            //safe guard
            if(edfaVoltage>=3.5){
                return;
            }
            setCntlModulationInputValue(edfaVoltage);
            usleep(10000);//10ms
            photodetector = getPhotoDetectPower(timestamp);
        }
    } catch(ControlExceptions::CAMBErrorExImpl &ex){
        throw ex.getCAMBErrorEx();
    } catch(ControlExceptions::INACTErrorExImpl &ex) {
        throw ex.getINACTErrorEx();
    }
}

void LPRImpl::photonicStandby()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    try {
        setCntlModulationInputValue(0.0);
        setCntlOptSwitchShutter();
    } catch(ControlExceptions::CAMBErrorExImpl &ex){
        throw ex.getCAMBErrorEx();
    } catch(ControlExceptions::INACTErrorExImpl &ex) {
        throw ex.getINACTErrorEx();
    }
}

float LPRImpl::getLastEDFA()
{
    return edfaLast_m;
}

void LPRImpl::selectBand(ReceiverBandMod::ReceiverBand band)
{
    try {

        selectBandInternal(band);

    }catch(ControlExceptions::CAMBErrorExImpl &ex){
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    } catch(ControlExceptions::INACTErrorExImpl &ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getINACTErrorEx();
    } catch (ControlExceptions::TimeoutExImpl &ex) {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    } catch (ControlExceptions::IllegalParameterErrorExImpl &ex) {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch(...){
        ControlExceptions::HardwareErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Additional", "An UNKOWN Exception was caught.");
        ex.log();
        throw ex.getHardwareErrorEx();
    }
}

void LPRImpl::selectBandInternal(ReceiverBandMod::ReceiverBand band)
{
    ACS::Time timestamp;
    int iterations = 0;

    FrontEndUtils::bandToInt(band);//Throws if not valid band

    setCntlOptSwitchPort(band);
    while (getOptSwitchBusy(timestamp) && ++iterations > 1200) {
        usleep(50000);//50ms
    }
    if (iterations >= 1200 ) {
        ControlExceptions::TimeoutExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        double seconds = 50 * iterations / 1000.0;
        std::stringstream msg;
        msg <<"Switching the LPR optical port took more than "<<seconds<<" seconds.";
        ex.addData("Additional", msg.str());
        throw ex;
    }
}
/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LPRImpl)
