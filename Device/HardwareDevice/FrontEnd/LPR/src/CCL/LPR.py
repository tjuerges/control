#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a LPR device.
"""

import CCL.LPRBase
from CCL import StatusHelper

class LPR(CCL.LPRBase.LPRBase):
    '''
    The LPR class inherits from the code generated LPRBase
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor

        Example:
        from CCL.LPR import LPR
        lpr=LPR("DA41")
        '''
        # FrontEnd subcomponents also have the prefix "FrontEnd/" in their
        # name, so we prepare always the componentName:
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/LPR"
            antennaName = None
        CCL.LPRBase.LPRBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.LPRBase.LPRBase.__del__(self)

    def prepEDFAModulationVoltage(self):
        '''
        This method prepares the EDFA modulation voltage to an local optimum
        before the LO locking in the WCA.
        2) Set the EDFA modulation voltage to zero:
        4) Increase the EDFA modulation voltage in steps of 0.1 V until the
        LPR Photodetector Power is about 2.00 mW

        Example:
        from CCL.LPR import LPR
        lpr=LPR("DA41")
        lpr.prepEDFAModulationVoltage()
        '''
        for key, val in self._devices.iteritems():
            try:
                self._devices[key].prepEDFAModulationVoltage()
            except e:
                print "Couldn't apply prepEDFAModulationVoltage to "+key
                print e

    def photonicStandby(self):
        '''
        This method takes the LPR to a photonic standby state.
        EDFA Modulation voltage = 0
        Shutter caps on.

        Example:
        from CCL.LPR import LPR
        lpr=LPR("DA41")
        lpr.photonicStandby()
        '''
        for key, val in self._devices.iteritems():
            try:
                self._devices[key].photonicStandby()
            except e:
                print "Couldn't apply photonicStandby to "+key
                print e

    def STATUS(self):
        '''
        This method displays the current status of the LPR
        '''

        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]

            # List of status elements
            elements = []

            ############################### Optical switch ###################################################
            elements.append( StatusHelper.Separator("Optical switch") )

            # Optical switch state & busy
            try:
                value = self._devices[key].GET_OPT_SWITCH_STATE()[0]
                if value == 0:
                    value_t = "Ok"
                elif value == 1:
                    value_t = "ERROR"
                else:
                    value_t = "UNKNOWN"
                value_busy = self._devices[key].GET_OPT_SWITCH_BUSY()[0]
                if value_busy == 0:
                    value_busy_t = "Idle"
                elif value_busy == 1:
                    value_busy_t = "Switching"
                else:
                    value_busy_t = "UNKNOWN"                    
            except:
                value_t = "N/A"
                value_busy_t = "N/A"
            elements.append( StatusHelper.Line("Optical switch",
                                               [StatusHelper.ValueUnit(value_t, label="State"),
                                                StatusHelper.ValueUnit(value_busy_t, label="Busy")]) )   

            # Optical switch port
            try:
                value = self._devices[key].GET_OPT_SWITCH_PORT()[0]
                if value == 0xFF:
                    value_t = "SHUTTER ON (OUTPUT DISABLED)"
                else:
                    value_t = "Band %d (port %d)" % (value + 1,value)
                    
            except:
                value_t = "N/A";
            elements.append( StatusHelper.Line("Selected port",
                                               [StatusHelper.ValueUnit(value_t)]) )


            # Optical switch shutter
            try:
                value = self._devices[key].GET_OPT_SWITCH_SHUTTER()[0]
                if value == 0:
                    value_t = "Shutter off (laser enabled)"
                elif value == 1:
                    value_t = "SHUTTER ON (LASER DISABLED)"
                else:
                    value_t = "UNKNOWN"
                    
            except:
                value_t = "N/A";
            elements.append( StatusHelper.Line("Shutter",
                                               [StatusHelper.ValueUnit(value_t)]) )
            
            ################################### Modulation ##################################################
            elements.append( StatusHelper.Separator("Modulation") )

            # MODULATION_INPUT_VALUE
            try:
                value = self._devices[key].GET_MODULATION_INPUT_VALUE()[0]
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Input port value",
                                               [StatusHelper.ValueUnit(value, "V")]) )
            
            ########################################### EDFA ###############################################
            elements.append( StatusHelper.Separator("EDFA") )

            # EDFA_PUMP_TEMP
            try:
                value = self._devices[key].GET_EDFA_PUMP_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
                value = "%.2f" % value
            except:
                value = 'N/A'
                valueCelcius = 'N/A'
            elements.append(StatusHelper.Line("Pump temperature",[StatusHelper.ValueUnit(value,"K"),
                                                             StatusHelper.ValueUnit(valueCelcius,"C")]))

            # EDFA_LASER_DRIVE_CURRENT
            try:
                value = self._devices[key].GET_EDFA_LASER_DRIVE_CURRENT()[0]
                value = "%.5f" % (value*1000)
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Laser drive current",
                                               [StatusHelper.ValueUnit(value, "mA")]) )

            # EDFA_LASER_PHOTO_DETECT_CURRENT
            try:
                value = self._devices[key].GET_EDFA_LASER_PHOTO_DETECT_CURRENT()[0]
                value = "%.5f" % (value*1000)
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Photo detector current",
                                               [StatusHelper.ValueUnit(value, "mA")]) )

            
            ################################### Photo detector ###################################################
            elements.append( StatusHelper.Separator("Photo detector") )

            # PHOTO_DETECT_CURRENT
            try:
                value = self._devices[key].GET_PHOTO_DETECT_CURRENT()[0]
                value = "%.6f" % (value*1000)
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Photo detector current",
                                               [StatusHelper.ValueUnit(value, "mA")]) )          

            # PHOTO_DETECT_POWER
            try:
                value = self._devices[key].GET_PHOTO_DETECT_POWER()[0]
                value = "%.6f" % (value*1000)
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("Photo detector power",
                                               [StatusHelper.ValueUnit(value, "mW")]) )   
           
            ################################### Temperature ######################################################
            elements.append( StatusHelper.Separator("Temperature") )

            # TEMP0
            try:
                value = self._devices[key].GET_TEMP0_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
                value = "%.2f" % value
            except:
                value = 'N/A'
                valueCelcius = 'N/A'
            elements.append(StatusHelper.Line("0:",[StatusHelper.ValueUnit(value,"K"),
                                                             StatusHelper.ValueUnit(valueCelcius,"C")]))

            # TEMP1
            try:
                value = self._devices[key].GET_TEMP1_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
                value = "%.2f" % value
            except:
                value = 'N/A'
                valueCelcius = 'N/A'
            elements.append(StatusHelper.Line("1:",[StatusHelper.ValueUnit(value,"K"),
                                                             StatusHelper.ValueUnit(valueCelcius,"C")]))
                  
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
