
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * File WCA3CompSimImpl.cpp
 */

#include "WCA3CompSimImpl.h"
#include <WCA3HWSimImpl.h>
#include <maciACSComponentDefines.h>
#include <TypeConversion.h>
#include <SimulatedSerialNumber.h>

using namespace maci;

/* Please use this class to implement alternative component, extending
 * the WCA3CompSimBase class. */

WCA3CompSimImpl::WCA3CompSimImpl(const ACE_CString& name, ContainerServices* pCS)
: WCA3CompSimBase(name,pCS)
{
    // note in simulation, WCA3 is device number 7
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::string componentName(name.c_str());

    std::string subComponentName=componentName.substr(0, componentName.find_last_of('/'));

    unsigned long long hashed_sn=AMB::Utils::getSimSerialNumber(subComponentName,boost::lexical_cast<std::string>(7));
    std::ostringstream msg;
    msg<< "simSerialNumber for  " << name.c_str() << "  with assembly name " << "WCA3" << " is " << std::hex << "0x" << hashed_sn << std::endl;
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG, msg.str().c_str()));

    std::vector< CAN::byte_t > sn;
    AMB::node_t node = 0x00;
    AMB::TypeConversion::valueToData(sn,hashed_sn, 8U);
    simulationIf_m.setSimObj(new AMB::WCA3HWSimImpl(node,sn));
}

/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(WCA3CompSimImpl)
