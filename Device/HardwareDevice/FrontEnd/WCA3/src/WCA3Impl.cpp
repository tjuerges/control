/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File WCA3Impl.cpp
 *
 * $Id$
 */

#include <WCA3Impl.h>

/**
 *-----------------------
 * WCA3 Constructor
 *-----------------------
 */
WCA3Impl::WCA3Impl(const ACE_CString& name,
        maci::ContainerServices* cs):
    WCA3Base(name,cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    band_m = ReceiverBandMod::ALMA_RB_03;
    WCABase::setBaseAddress(getBaseAddress());
    m_mutexIndex=2;
    pthread_mutex_init(&m_mutex[m_mutexIndex], NULL);
    //From FEMC
    assembly_m = "WCA3";
}

/**
 *-----------------------
 * WCA3 Destructor
 *-----------------------
 */
WCA3Impl::~WCA3Impl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    pthread_mutex_destroy(&m_mutex[m_mutexIndex]);
}

///////////////////////////////////////
// Additional methods for WCA3
///////////////////////////////////////

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(WCA3Impl)
