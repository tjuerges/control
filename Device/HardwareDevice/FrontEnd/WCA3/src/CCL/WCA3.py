#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a WCA3 device.
"""

import CCL.WCA3Base
from CCL import StatusHelper

class WCA3(CCL.WCA3Base.WCA3Base):
    '''
    The WCA3 class inherits from the code generated WCA3Base
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/WCA3"
            antennaName = None
        CCL.WCA3Base.WCA3Base.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.WCA3Base.WCA3Base.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the WCA3
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]
        
            # Invoke the generic WCA status helper
            elements = self._WCA__STATUSHelper(key)
            
            ############################### WCA3 specific ###################################################
            #elements.append( StatusHelper.Separator("WCA3 specific") )
                  
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
