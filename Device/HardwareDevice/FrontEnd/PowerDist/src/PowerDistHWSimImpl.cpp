
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PowerDistHWSimImpl.cpp
 *
 * $Id$
 */

#include "PowerDistHWSimImpl.h"

using namespace AMB;

/* Please use this class to implement complex functionality for the
 * PowerDistHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

PowerDistHWSimImpl::PowerDistHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: PowerDistHWSimBase::PowerDistHWSimBase(node, serialNumber)
{
    initialize(node,serialNumber);
}

// Monitor and Control Points getters
// ----------------------------------------------------------------------------
std::vector< CAN::byte_t > AMB::PowerDistHWSimImpl::monitor(
    AMB::rca_t rca) const
{
    if (rca == monitorPoint_POWERED_MODULES) {
        return PowerDistHWSimBase::monitor(rca + getBaseAddress());
    }
    return PowerDistHWSimBase::monitor(rca);
}


