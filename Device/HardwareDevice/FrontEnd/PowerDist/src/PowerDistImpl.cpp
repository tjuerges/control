/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PowerDistImpl.cpp
 *
 * $Id$
 */

#include <PowerDistImpl.h>

/**
 *-----------------------
 * PowerDist Constructor
 *-----------------------
 */
PowerDistImpl::PowerDistImpl(const ACE_CString& name,
        maci::ContainerServices* cs):
    PowerDistBase(name,cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 *-----------------------
 * PowerDist Destructor
 *-----------------------
 */
PowerDistImpl::~PowerDistImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

///////////////////////////////////////
// Additional methods for PowerDist
///////////////////////////////////////
void PowerDistImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    PowerDistBase::hwInitializeAction();
    /* Because of http://jira.alma.cl/browse/COMP-3257
     * and because this monitor point is the same for all
     * PowerdDist independent of the base address, we
     * have to spike the RCA to:
     * RCA - BASEADDRESS so when the monitor point is polled
     * monitor (RCA + BASEADRESS)
     * We will monitor a constant monitor point independent
     * of the baseaddress
     */
    ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_INFO,"Original RCA for GET_POWERED_MODULES: %x",getMonitorRCAPoweredModules()));
    setMonitorRCAPoweredModules(0x0a0a0 - getBaseAddress());
    ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_INFO,"New RCA for GET_POWERED_MODULES: %x",getMonitorRCAPoweredModules()));
}
/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(PowerDistImpl)
