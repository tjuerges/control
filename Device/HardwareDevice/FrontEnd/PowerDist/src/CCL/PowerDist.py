#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a PowerDist device.
"""

import CCL.PowerDistBase
import StatusHelper

class PowerDist(CCL.PowerDistBase.PowerDistBase):
    '''
    The PowerDist class inherits from the code generated PowerDistBase
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        CCL.PowerDistBase.PowerDistBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.PowerDistBase.PowerDistBase.__del__(self)

    def __STATUSHelper(self, key):
        '''
        This method provides the status part that is common to all PowerDists.
        A list of elements is returned that should be used within higher
        level STATUS() methods.
        '''
        # List of status elements
        elements = []

        try:
            current = self._devices[key].GET_CARTRIDGE_CHANNEL0_CURRENT()[0]
            current = "%.4f" % current
        except:
            current = "N/A"
        try:
            voltage = self._devices[key].GET_CARTRIDGE_CHANNEL0_VOLTAGE()[0]
            voltage = "%.2f" % voltage
        except:
            voltage = "N/A"
        elements.append( StatusHelper.Line("6V output:",
                                           [StatusHelper.ValueUnit(voltage,'V'),StatusHelper.ValueUnit(current,'A')]) )
        try:
            current = self._devices[key].GET_CARTRIDGE_CHANNEL1_CURRENT()[0]
            current = "%.4f" % current
        except:
            current = "N/A"
        try:
            voltage = self._devices[key].GET_CARTRIDGE_CHANNEL1_VOLTAGE()[0]
            voltage = "%.2f" % voltage
        except:
            voltage = "N/A"
        elements.append( StatusHelper.Line("-6V output:",
                                           [StatusHelper.ValueUnit(voltage,'V'),StatusHelper.ValueUnit(current,'A')]) )
        try:
            current = self._devices[key].GET_CARTRIDGE_CHANNEL2_CURRENT()[0]
            current = "%.4f" % current
        except:
            current = "N/A"
        try:
            voltage = self._devices[key].GET_CARTRIDGE_CHANNEL2_VOLTAGE()[0]
            voltage = "%.2f" % voltage
        except:
            voltage = "N/A"
        elements.append( StatusHelper.Line("15V output:",
                                           [StatusHelper.ValueUnit(voltage,'V'),StatusHelper.ValueUnit(current,'A')]) )
        try:
            current = self._devices[key].GET_CARTRIDGE_CHANNEL3_CURRENT()[0]
            current = "%.4f" % current
        except:
            current = "N/A"
        try:
            voltage = self._devices[key].GET_CARTRIDGE_CHANNEL3_VOLTAGE()[0]
            voltage = "%.2f" % voltage
        except:
            voltage = "N/A"
        elements.append( StatusHelper.Line("-15V output:",
                                           [StatusHelper.ValueUnit(voltage,'V'),StatusHelper.ValueUnit(current,'A')]) )
        try:
            current = self._devices[key].GET_CARTRIDGE_CHANNEL4_CURRENT()[0]
            current = "%.4f" % current
        except:
            current = "N/A"
        try:
            voltage = self._devices[key].GET_CARTRIDGE_CHANNEL4_VOLTAGE()[0]
            voltage = "%.2f" % voltage
        except:
            voltage = "N/A"
        elements.append( StatusHelper.Line("24V output:",
                                           [StatusHelper.ValueUnit(voltage,'V'),StatusHelper.ValueUnit(current,'A')]) )
        try:
            current = self._devices[key].GET_CARTRIDGE_CHANNEL5_CURRENT()[0]
            current = "%.4f" % current
        except:
            current = "N/A"
        try:
            voltage = self._devices[key].GET_CARTRIDGE_CHANNEL5_VOLTAGE()[0]
            voltage = "%.2f" % voltage
        except:
            voltage = "N/A"
        elements.append( StatusHelper.Line("8V output:",
                                           [StatusHelper.ValueUnit(voltage,'V'),StatusHelper.ValueUnit(current,'A')]) )

        return elements
