
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PowerDistHWSimImpl.h
 *
 * $Id$
 */
#ifndef PowerDistHWSimImpl_H
#define PowerDistHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "PowerDistHWSimBase.h"

namespace AMB
{
  /* Please use this class to implement complex functionality for the
   * PowerDistHWSimBase helper functions. Use AMB::TypeConvertion methods
   * for convenient type convertions. */
  class PowerDistHWSimImpl : public PowerDistHWSimBase
  {
  public :
	PowerDistHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber);

	    /// Monitor interface ('get' functions for monitor points)
        /// \param rca CAN node RCA
        /// \return std::vector of bytes containing monitor info. Each response
        /// is sized to what's specified in the ICD.
        /// \exception CAN::Error
        virtual std::vector< CAN::byte_t > monitor(AMB::rca_t rca) const;

  }; // class PowerDistHWSimImpl

} // namespace AMB

#endif // PowerDistHWSimImpl_H
