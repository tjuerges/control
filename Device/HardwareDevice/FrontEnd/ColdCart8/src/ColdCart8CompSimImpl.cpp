
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2009
 * (c) Associated Universities Inc., 2009
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 * Source: $
 *
 */


#include <ColdCart8CompSimImpl.h>
#include <ColdCart8HWSimImpl.h>
#include <string>
#include <sstream>
// For audience logs.
#include <LogToAudience.h>
#include <maciContainerServices.h>
#include <TypeConversion.h>
#include <Utils.h>
#include <SimulatedSerialNumber.h>

#include <boost/lexical_cast.hpp>


/**
 * Please use this class to implement alternative component, extending
 * the ColdCart8CompSimBase class.
 */


ColdCart8CompSimImpl::ColdCart8CompSimImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    ColdCart8CompSimBase(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string componentName(name.c_str());
    
    const std::string subComponentName(
        componentName.substr(0, componentName.find_last_of('/')));
    const unsigned long long hashed_sn(AMB::Utils::getSimSerialNumber(
        subComponentName, boost::lexical_cast< std::string >(8)));
    
    std::ostringstream msg;
    msg << "simSerialNumber for \""
        << name.c_str()
        << "\" with assembly name \"ColdCart8\" is "
        << std::hex
        << "0x"
        << hashed_sn
        << "\n";
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    
    std::vector< CAN::byte_t > sn;
    AMB::TypeConversion::valueToData(sn, hashed_sn, 8U);

    const AMB::node_t node(0x00U);

    simulationObject.reset(new AMB::ColdCart8HWSimImpl(node, sn));
    simulationIf_m.setSimObj(simulationObject.get());
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ColdCart8CompSimImpl)
