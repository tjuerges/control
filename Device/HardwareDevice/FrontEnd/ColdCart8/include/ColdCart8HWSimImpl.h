
#ifndef ColdCart8HWSimImpl_h
#define ColdCart8HWSimImpl_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 *
 */


#include <ColdCart8HWSimBase.h>
#include <vector>
// For AMB::node_t and CAN::byte_t
#include <CANTypes.h>


namespace AMB
{
    /**
     * Please use this class to implement complex functionality for the
     * ColdCart8HWSimBase helper functions. Use AMB::TypeConvertion methods
     * for convenient type convertions.
     */
    class ColdCart8HWSimImpl: public ColdCart8HWSimBase
    {
        public:
        /// Constructor.
        ColdCart8HWSimImpl(AMB::node_t node,
            const std::vector< CAN::byte_t >& serialNumber);

        /// Destructor.
        virtual ~ColdCart8HWSimImpl()
        {
        };


        private:
        /// No copy constructor.
        ColdCart8HWSimImpl(const ColdCart8HWSimImpl&);

        /// No assignment operator.
        ColdCart8HWSimImpl& operator=(const ColdCart8HWSimImpl&);
    };
}
#endif
