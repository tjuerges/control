/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File ColdCart9Impl.cpp
 *
 * $Id$
 */

#include <ColdCart9Impl.h>

/**
 *-----------------------
 * ColdCart9 Constructor
 *-----------------------
 */
ColdCart9Impl::ColdCart9Impl(const ACE_CString& name,
    maci::ContainerServices* cs):
    ColdCart9Base(name,cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    band_m = ReceiverBandMod::ALMA_RB_09;
    ColdCartBase::setBaseAddress(getBaseAddress());
    assembly_m = "ColdCart9";
}

/**
 *-----------------------
 * ColdCart9 Destructor
 *-----------------------
 */
ColdCart9Impl::~ColdCart9Impl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

///////////////////////////////////////
// Additional methods for ColdCart9
///////////////////////////////////////
void ColdCart9Impl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ColdCart9Base::hwInitializeAction();
    try {
        setCntlPol0Sb1SisOpenLoop(false);
      //  setCntlPol0Sb2SisOpenLoop(false);
        setCntlPol1Sb1SisOpenLoop(false);
      //  setCntlPol1Sb2SisOpenLoop(false);

        setCntlPol0SisHeaterEnable(false);
        setCntlPol1SisHeaterEnable(false);
    } catch (ControlExceptions::CAMBErrorExImpl &ex) {
        ex.log();
    }
}

void ColdCart9Impl::demagnetize(float magnetCurrentPulseLenght, float Imax)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "Demagnetizing ColdCart in Band 9"));
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "...Demagnetizing Side Band 1, Polarization 0 "));
    for (int i = 50; i<0 ; i--){
        float Imix = 1.0E-3 * Imax * i /50;
        setCntlPol0Sb1SisMagnetCurrent(Imix);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
        setCntlPol0Sb1SisMagnetCurrent(0.0);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
        setCntlPol0Sb1SisMagnetCurrent(Imix);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
        setCntlPol0Sb1SisMagnetCurrent(0.0);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
    }
    ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "...Demagnetizing Side Band 1, Polarization 1 "));
    for (int i = 50; i<0 ; i--){
        float Imix = 1.0E-3 * Imax * i /50;
        setCntlPol1Sb1SisMagnetCurrent(Imix);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
        setCntlPol1Sb1SisMagnetCurrent(0.0);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
        setCntlPol1Sb1SisMagnetCurrent(Imix);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
        setCntlPol1Sb1SisMagnetCurrent(0.0);
        usleep((int)(magnetCurrentPulseLenght * 1E6));
    }

}

void ColdCart9Impl::defluxPulse(float heatherPulseLenght, float initialTempPol0,
                                 float initialTempPol1)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    float currentTempPol0 = 5000;
    float currentTempPol1 = 5000;
    bool tempPol0reached = false;
    bool tempPol1reached = false;
    ACS::Time  timestamp;

    setCntlPol0SisHeaterEnable(true);
    usleep((int)(heatherPulseLenght * 1E6));
    setCntlPol0SisHeaterEnable(false);

    for(int i=0; i<50; i++) {
        currentTempPol0 = getTemperatureSensor0(timestamp);
        if (currentTempPol0 < initialTempPol0 + 0.5){
            break;
            tempPol0reached = true;
        }
        sleep((int)(0.1 * 1E6));
    }
    if(!tempPol0reached){
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_NOTICE, "Initial temp %f was not reached. Actual %f", initialTempPol0, currentTempPol0));
    }

    for(int i=0; i<50; i++) {
        currentTempPol1 = getTemperatureSensor5(timestamp);
        if (currentTempPol1 < initialTempPol1 + 0.5){
            break;
            tempPol1reached = true;
        }
        sleep((int)(0.1 * 1E6));
    }
    if(!tempPol1reached){
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_NOTICE, "Initial temp %f was not reached. Actual %f", initialTempPol1, currentTempPol1));
    }
}

void  ColdCart9Impl::setLNABias(int pol, int sb)
{
    bool setAll = (pol < 0 || sb < 1);
    //data has 9 points we use only 6
    std::vector<double> row;

    if (setAll || (pol == 0 && sb == 1)) {
        row = getConfigLNA(0,1);
        setCntlPol0Sb1Lna1DrainVoltage(row[0]);
        setCntlPol0Sb1Lna2DrainVoltage(row[1]);
        setCntlPol0Sb1Lna3DrainVoltage(row[2]);
        setCntlPol0Sb1Lna1DrainCurrent(row[3]);
        setCntlPol0Sb1Lna2DrainCurrent(row[4]);
        setCntlPol0Sb1Lna3DrainCurrent(row[5]);
    }
    if (setAll || (pol == 0 && sb == 2)) {
        //BAND 9 has no sideband 2
        //row = getConfigLNA(0,2);
        //setCntlPol0Sb2Lna1DrainVoltage(row[0]);
        //setCntlPol0Sb2Lna2DrainVoltage(row[1]);
        //setCntlPol0Sb2Lna3DrainVoltage(row[2]);
        //setCntlPol0Sb2Lna1DrainCurrent(row[3]);
        //setCntlPol0Sb2Lna1DrainCurrent(row[4]);
        //setCntlPol0Sb2Lna1DrainCurrent(row[5]);
    }
    if (setAll || (pol == 1 && sb == 1)) {
        row = getConfigLNA(1,1);
        setCntlPol1Sb1Lna1DrainVoltage(row[0]);
        setCntlPol1Sb1Lna2DrainVoltage(row[1]);
        setCntlPol1Sb1Lna3DrainVoltage(row[2]);
        setCntlPol1Sb1Lna1DrainCurrent(row[3]);
        setCntlPol1Sb1Lna2DrainCurrent(row[4]);
        setCntlPol1Sb1Lna3DrainCurrent(row[5]);
    }
    if (setAll || (pol == 1 && sb == 2)) {
        //row = getConfigLNA(1,2);
        //setCntlPol1Sb2Lna1DrainVoltage(row[0]);
        //setCntlPol1Sb2Lna2DrainVoltage(row[1]);
        //setCntlPol1Sb2Lna3DrainVoltage(row[2]);
        //setCntlPol1Sb2Lna1DrainCurrent(row[3]);
        //setCntlPol1Sb2Lna2DrainCurrent(row[4]);
        //setCntlPol1Sb2Lna3DrainCurrent(row[5]);
    }

}

void ColdCart9Impl::setEnableLNABias(bool enable, int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    bool setAll = (pol < 0 || sb < 1);

    if(setAll || (pol == 0 && sb == 1))
        setCntlPol0Sb1LnaEnable(enable);
   // if(setAll || (pol < 0 && sb < 2))
        //BAND 9 has no SB2 throw exception
        //setCntlPol0Sb2LnaEnable(enable);
    if(setAll || (pol == 1 && sb == 1))
        setCntlPol1Sb1LnaEnable(enable);
 //   if(setAll || (pol < 1 && sb < 2))
        //BAND 9 has no SB2 throw exception
        //setCntlPol1Sb2LnaEnable(enable);
}

void ColdCart9Impl::setSISBias(bool enable,
                              int pol, int sb, int _openLoop)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    bool setAll = (pol < 0 || sb < 1);
    bool openLoop = (_openLoop == 1);

    std::vector<double> row = getConfigSISBias();

    setCntlPol0SisHeaterEnable(false);
    setCntlPol1SisHeaterEnable(false);

    if (enable) {
        if (setAll || (pol == 0 && sb == 1)) {
            setCntlPol0Sb1SisVoltage(row[0]);
            if (_openLoop >= 0) {
                setCntlPol0Sb1SisOpenLoop(openLoop);
            }
        }
        if (setAll || (pol == 0 && sb == 2)) {
            //BAND 9 has no SB2 throw exception
            //setCntlPol0Sb2SisVoltage(row[1]);
            //if (_openLoop >= 0) {
            //    setCntlPol0Sb2SisOpenLoop(openLoop);
           // }
        }
        if (setAll || (pol == 1 && sb == 1)) {
            setCntlPol1Sb1SisVoltage(row[2]);
            if (_openLoop >= 0) {
                setCntlPol1Sb1SisOpenLoop(openLoop);
            }
        }
        if (setAll || (pol == 1 && sb == 2)) {
            //BAND 9 has no SB2 throw exception
            //setCntlPol1Sb2SisVoltage(row[3]);
            //if (_openLoop >= 0) {
            //    setCntlPol1Sb2SisOpenLoop(openLoop);
            //}
        }
    }
}

void ColdCart9Impl::setSISMagnet(bool enable, int pol, int sb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    bool setAll = (pol < 0 || sb < 1);

    std::vector<double> row(4,0.0);
    //disabling the magnet is achieved by setting 0.0 to its values.
    if(enable)
        row = getConfigSisMag();

    if (setAll || (pol == 0 && sb == 1)) {
        setCntlPol0Sb1SisMagnetCurrent(row[0]);
    }
    if (setAll || (pol == 0 && sb == 2)) {
        //BAND 9 has no SB2 throw exception
        //setCntlPol0Sb2SisMagnetCurrent(row[1]);
    }
    if (setAll || (pol == 1 && sb == 1)) {
        setCntlPol1Sb1SisMagnetCurrent(row[2]);
    }
    if (setAll || (pol == 1 && sb == 2)) {
        //BAND 9 has no SB2 throw exception
        //setCntlPol1Sb2SisMagnetCurrent(row[3]);
    }
}

void ColdCart9Impl::setSisVoltage(const float voltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    setCntlPol0Sb1SisVoltage(voltage);
    setCntlPol1Sb1SisVoltage(voltage);
//no SB2 for CC9
//    setCntlPol0Sb2SisVoltage(voltage);
//    setCntlPol1Sb2SisVoltage(voltage);
}

void ColdCart9Impl::setSisVoltage(const std::vector< float >& voltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(voltage.size()<2) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__,__LINE__,"ColdCart9Impl::setSisVoltage");
        ex.addData("Details","Voltage vector is to short.");
        throw ex;
    }
    setCntlPol0Sb1SisVoltage(voltage[0]);
    setCntlPol1Sb1SisVoltage(voltage[1]);
//no sb2 for CC9
//    setCntlPol0Sb2SisVoltage(voltage[2]);
 //   setCntlPol1Sb2SisVoltage(voltage[3]);
}

void ColdCart9Impl::getSisVoltage(std::vector< float >& voltage)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    voltage.clear();
    voltage.push_back(getPol0Sb1SisVoltage(timestamp));
    voltage.push_back(getPol1Sb1SisVoltage(timestamp));
    voltage.push_back(0.0);
    voltage.push_back(0.0);
}

void ColdCart9Impl::setSisMagnetCurrent(const float current)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    setCntlPol0Sb1SisMagnetCurrent(current);
    setCntlPol1Sb1SisMagnetCurrent(current);
}

void ColdCart9Impl::setSisMagnetCurrent(const std::vector< float >& current)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    if(current.size()<4) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Details","MagnetCurrent vector is to short.");
        throw ex;
    }
    setCntlPol0Sb1SisMagnetCurrent(current[0]);
    setCntlPol1Sb1SisMagnetCurrent(current[1]);
}

void ColdCart9Impl::getSisCurrent(std::vector< float >& current)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    ACS::Time timestamp;
    current.clear();
    current.push_back(getPol0Sb1SisCurrent(timestamp));
    current.push_back(getPol1Sb1SisCurrent(timestamp));
    current.push_back(0.0);
    current.push_back(0.0);
}


/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(ColdCart9Impl)
