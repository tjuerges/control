#! /usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a ColdCart9 device.
"""

import CCL.ColdCart9Base
from CCL import StatusHelper

class ColdCart9(CCL.ColdCart9Base.ColdCart9Base):
    '''
    The ColdCart9 class inherits from the code generated ColdCart9Base
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/ColdCart9"
            antennaName = None
        CCL.ColdCart9Base.ColdCart9Base.__init__(self, antennaName, componentName, stickyFlag)
    
    def __del__(self):
        CCL.ColdCart9Base.ColdCart9Base.__del__(self)

    def SISSTATUS(self):
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            retval = []
            retval.append(StatusHelper.Line("SIS Pol 0 USB", self._ColdCart__SISPol0USB(key)))
            retval.append(StatusHelper.Line("SIS Pol 1 USB", self._ColdCart__SISPol1USB(key)))
            result[antName] = retval
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def STATUS(self):
        '''
        Displays the current status of the ColdCart9
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]
        
            # Invoke the generic ColdCart status helper
            temp = self._ColdCart__STATUSHelper(key)
            pol0 = temp[0]
            pol1 = temp[1]
            temps= temp[2]
            
            '''
            ############################### ColdCart9 specific ###################################################
            elements.append( StatusHelper.Separator("ColdCart9 specific") )
            
            # GET_LO_PLL_ASSEMBLY_TEMP
            try:
                value = self._devices[key].GET_LO_PLL_ASSEMBLY_TEMP()[0]
                value = "%.2f" % value
            except:
                value = "N/A"
            elements.append( StatusHelper.Line("LO Pll Assembly Temp",
                                               [StatusHelper.ValueUnit(value, "K")]) )    
            '''   
            #######################Polarization 0########################
            #######################SIS USB Magnet########################
            try:
                value1 = self._devices[key].GET_POL0_SB1_SIS_MAGNET_CURRENT()[0]
                value1 = "%.4f" % (value1 * 1000)
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL0_SB1_SIS_MAGNET_VOLTAGE()[0]
                value2 = "%.4f" % value2
            except:
                value2 = "N/A"
            pol0.append(StatusHelper.Line("SIS USB Magnet",[StatusHelper.ValueUnit(value2,"V","V"),StatusHelper.ValueUnit(value1,"mA","I")]))
            
           ################################SIS HEATER###########################
            try:
                value1 = self._devices[key].GET_POL0_SIS_HEATER_CURRENT()[0]
                value1 = "%.4f" % (value1*1000)
            except:
                value1 = "N/A"
    
            pol0.append(StatusHelper.Line("SIS Heater",[StatusHelper.ValueUnit(value1,"mA","I")]))


            #######################Polarization 1########################
            #######################SIS USB Magnet########################
            try:
                value1 = self._devices[key].GET_POL1_SB1_SIS_MAGNET_CURRENT()[0]
                value1 = "%.4f" % (value1 * 1000)
            except:
                value1 = "N/A"
            try:
                value2 = self._devices[key].GET_POL1_SB1_SIS_MAGNET_VOLTAGE()[0]
                value2 = "%.4f" % value2
            except:
                value2 = "N/A"
            pol1.append(StatusHelper.Line("SIS USB Magnet",[StatusHelper.ValueUnit(value2,"V","V"),StatusHelper.ValueUnit(value1,"mA","I")]))
            

           ################################SIS HEATER###########################
            try:
                value1 = self._devices[key].GET_POL1_SIS_HEATER_CURRENT()[0]
                value1 = "%.4f" % (value1*1000)
            except:
                value1 = "N/A"
    
            pol1.append(StatusHelper.Line("SIS Heater",[StatusHelper.ValueUnit(value1,"mA","I")]))


            
            elements = pol0+pol1+temps
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
