#! /usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a FETIM device.
"""

import CCL.FETIMBase
from CCL import StatusHelper

class FETIM(CCL.FETIMBase.FETIMBase):
    '''
    The FETIM class inherits from the code generated FETIMBase
    class and adds specific methods.
    '''
    
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        Constructor

        Example:
        from CCL.FETIM import FETIM
        fetim = FETIM("DA55")
        fetim.STATUS()
        '''
        if antennaName != None:
            componentName = "CONTROL/" + antennaName + "/FrontEnd/FETIM"
            antennaName = None
        CCL.FETIMBase.FETIMBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.FETIMBase.FETIMBase.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the FETIM
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[3]

            elements = []
        
            elements.append( StatusHelper.Separator("Interlock") )

            try:
                value = self._devices[key].GET_INTRLK_SENS_INT_TEMP0_TEMP()[0]
                valueCelcius = "%.2f" %  (value - 273.15)
            except:
                value = "N/A"
                valueCelcius = "N/A"

            elements.append( StatusHelper.Line("Temp Sensor 0:", 
                                                [StatusHelper.ValueUnit(value, "K"),
                                                 StatusHelper.ValueUnit(valueCelcius, "C")]))


            try:
                value = self._devices[key].GET_INTRLK_SENS_INT_TEMP1_TEMP()[0]
                valueCelcius = "%.2f" %  (value - 273.15)
            except:
                value = "N/A"
                valueCelcius = "N/A"

            elements.append( StatusHelper.Line("Temp Sensor 1:", 
                                                [StatusHelper.ValueUnit(value, "K"),
                                                 StatusHelper.ValueUnit(valueCelcius, "C")]))
            try:
                value = self._devices[key].GET_INTRLK_SENS_INT_TEMP2_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
            except:
                value = "N/A"
                valueCelcius = "N/A"

            elements.append( StatusHelper.Line("Temp Sensor 2:", 
                                                [StatusHelper.ValueUnit(value, "K"),
                                                 StatusHelper.ValueUnit(valueCelcius, "C")]))
            try:
                value = self._devices[key].GET_INTRLK_SENS_INT_TEMP3_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
            except:
                value = "N/A"
                valueCelcius = "N/A"

            elements.append( StatusHelper.Line("Temp Sensor 3:", 
                                                [StatusHelper.ValueUnit(value, "K"),
                                                 StatusHelper.ValueUnit(valueCelcius, "C")]))

            try:
                value = self._devices[key].GET_INTRLK_SENS_FLOW_SENS0_FLOW()[0]
                if value == 0:
                    valueTmp = "Error"
                elif value == 5:
                    valueTmp = "OK"
                else:
                    valueTmp = "Unknown"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Flow Sensor 0:", 
                                                [StatusHelper.ValueUnit(value, "V"),
                                                 StatusHelper.ValueUnit(valueTmp)]))

            try:
                value = self._devices[key].GET_INTRLK_SENS_FLOW_SENS1_FLOW()[0]
                if value == 0:
                    valueTmp = "Error"
                elif value == 5:
                    valueTmp = "OK"
                else:
                    valueTmp = "Unknown"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Flow Sensor 1:", 
                                                [StatusHelper.ValueUnit(value, "V"),
                                                 StatusHelper.ValueUnit(valueTmp)]))


            try:
                value = self._devices[key].GET_INTRLK_SENS_SINGLE_FAIL()[0]
                if value == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Error"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Sensor Failure:", 
                                                [StatusHelper.ValueUnit(valueTmp)]))

            elements.append( StatusHelper.Separator("Interlock State") )
            try:
                value = self._devices[key].GET_INTRLK_STATE_GLITCH_VALUE()[0]
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Glitch Value:", 
                                                [StatusHelper.ValueUnit(value), '%']))

            try:
                value = self._devices[key].GET_INTRLK_STATE_GLITCH_CNT_TRIG()[0]
                if value == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Triggered"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Glitch integrator counter:", 
                                                [StatusHelper.ValueUnit(valueTmp)]))

            try:
                value = self._devices[key].GET_INTRLK_STATE_SENS_MULT_FAIL()[0]
                if value == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Triggered"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Multiple sensor failure interlock:", 
                                                [StatusHelper.ValueUnit(valueTmp)]))


            try:
                value = self._devices[key].GET_INTRLK_STATE_SENS_INT_TEMP_ORR()[0]
                if value == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Error"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Thermal interlock out of range:", 
                                                [StatusHelper.ValueUnit(valueTmp)]))

            try:
                value = self._devices[key].GET_INTRLK_STATE_DELAY_TRIG()[0]
                if value == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Triggered"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Delay counter:", 
                                                [StatusHelper.ValueUnit(valueTmp)]))

            try:
                value = self._devices[key].GET_INTRLK_STATE_SHUTDOWN_TRIG()[0]
                if value == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Triggered"
            except:
                value = "N/A"
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Shutdown counter:", 
                                                [StatusHelper.ValueUnit(valueTmp)]))

            elements.append( StatusHelper.Separator("Compressor Sensors") )


            try:
                value = self._devices[key].GET_COMP_EXT_TEMP_SENS0_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
            except:
                value = "N/A"
                valueCelcius = "N/A"

            try:
                valueTmp = self._devices[key].GET_COMP_EXT_TEMP_SENS0_OOR()[0]
                if valueTmp == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Error"
            except:
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Temp Sensor 0:", 
                                                [StatusHelper.ValueUnit(value, "K"),
                                                 StatusHelper.ValueUnit(valueCelcius, "C"),
                                                 StatusHelper.ValueUnit(valueTmp)]))

            try:
                value = self._devices[key].GET_COMP_EXT_TEMP_SENS1_TEMP()[0]
                valueCelcius = "%.2f" % (value - 273.15)
            except:
                value = "N/A"
                valueCelcius = "N/A"

            try:
                valueTmp = self._devices[key].GET_COMP_EXT_TEMP_SENS1_OOR()[0]
                if valueTmp == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Error"
            except:
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Temp Sensor 1:", 
                                                [StatusHelper.ValueUnit(value, "K"),
                                                 StatusHelper.ValueUnit(valueCelcius, "C"),
                                                 StatusHelper.ValueUnit(valueTmp)]))

            try:
                value = self._devices[key].GET_COMP_HE2_PRESS_PRESS()[0]
                valuemPa = "%.2f" % (value * 1E3)
            except:
                value = "N/A"
                valuemPa = "N/A"

            try:
                valueTmp = self._devices[key].GET_COMP_HE2_PRESS_OOR()[0]
                if valueTmp == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Error"
            except:
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("He2 Pressure Sensor:", 
                                                [StatusHelper.ValueUnit(value, "Pa"),
                                                 StatusHelper.ValueUnit(valuemPa, "mPa"),
                                                 StatusHelper.ValueUnit(valueTmp)]))

            try:
                valueTmp = self._devices[key].GET_COMP_FE_STATUS()[0]
                if valueTmp == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "NOT READY"
            except:
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Compressor state:", 
                                                 [StatusHelper.ValueUnit(valueTmp)]))

            try:
                valueTmp = self._devices[key].GET_COMP_INTRLK_STATUS()[0]
                if valueTmp == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Triggered"
            except:
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Compressor interlock:", 
                                                 [StatusHelper.ValueUnit(valueTmp)]))
            try:
                valueTmp = self._devices[key].GET_COMP_CABLE_STATUS()[0]
                if valueTmp == 0:
                    valueTmp = "OK"
                else:
                    valueTmp = "Error"
            except:
                valueTmp = "N/A"

            elements.append( StatusHelper.Line("Compressor comunication:", 
                                                 [StatusHelper.ValueUnit(valueTmp)]))

            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
