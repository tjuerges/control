/*
 * FrontEndUtils.h
 *
 *  Created on: Feb 24, 2011
 *      Author: ntroncos
 */

#ifndef FRONTENDUTILS_H_
#define FRONTENDUTILS_H_

#include <almaEnumerations_IFC.h>
#include<string>

namespace FrontEndUtils {
    /**
     * int bandToInt(ReceiverBand band);
     * As the name suggests it transforms a Band into an integer from
     * 1 to 10
     * @param ReceiverBand band
     * @except ControlExceptions::IllegalParameterErrorExImpl
     */

    const static std::string POWERDIST_STRING("PowerDist");
    const static std::string COLDCART_STRING("ColdCart");
    const static std::string WCA_STRING("WCA");
    const static std::string IFSWITCH_STRING("IFSwitch");
    const static std::string LPR_STRING("LPR");
    const static std::string CRYOSTAT_STRING("Cryostat");
    const static std::string ACD_STRING("ACD");

    int bandToInt(const ReceiverBandMod::ReceiverBand band);

    ReceiverBandMod::ReceiverBand intToBand(const int intband);

    int getIntFromName(const std::string name);

    std::string getNameWithInt(const std::string name, const ReceiverBandMod::ReceiverBand band);

    std::string getPowerDistName(const ReceiverBandMod::ReceiverBand band);

    std::string getColdCartName(const ReceiverBandMod::ReceiverBand band);

    std::string getWCAName(const ReceiverBandMod::ReceiverBand band);

    bool isBandComponent(const std::string name);

    int  getColdMultiplier(const ReceiverBandMod::ReceiverBand band);

}

#endif /* FRONTENDUTILS_H_ */
