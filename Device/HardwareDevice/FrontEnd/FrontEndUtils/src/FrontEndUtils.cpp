/*
 * FrontEndUtils.cpp
 *
 *  Created on: Feb 24, 2011
 *      Author: ntroncos
 */

#include "FrontEndUtils.h"

#include <ControlExceptions.h>
#include <iostream>


using namespace ReceiverBandMod;

using ControlExceptions::IllegalParameterErrorExImpl;
using std::string;
using std::istringstream;
using std::stringstream;

int FrontEndUtils::bandToInt(const ReceiverBand band)
{

    int returnValue = 0;
    string msg = "";
    switch(band) {
        case ReceiverBandMod::ALMA_RB_01:
            returnValue = 1;
            break;
        case ReceiverBandMod::ALMA_RB_02:
            returnValue = 2;
            break;
        case ReceiverBandMod::ALMA_RB_03:
            returnValue = 3;
            break;
        case ReceiverBandMod::ALMA_RB_04:
            returnValue = 4;
            break;
        case ReceiverBandMod::ALMA_RB_05:
            returnValue = 5;
            break;
        case ReceiverBandMod::ALMA_RB_06:
            returnValue = 6;
            break;
        case ReceiverBandMod::ALMA_RB_07:
            returnValue = 7;
            break;
        case ReceiverBandMod::ALMA_RB_08:
            returnValue = 8;
            break;
        case ReceiverBandMod::ALMA_RB_09:
            returnValue = 9;
            break;
        case ReceiverBandMod::ALMA_RB_10:
            returnValue = 10;
            break;
        case ReceiverBandMod::UNSPECIFIED:
            {
                IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
                msg = "The FrontEnd has no selected receiver band."
                      "This exception should have not happened and it is most likely"
                      " and oversight by the developer. Please report this issue in a"
                      "Jira ticket. To work around the problem "
                      "select a band and retry the operation.";
                ex.addData("Additional", msg);
                throw ex;
            }
            break;
        default:
            //This should never happen unless you supply something that is not a
            //BAND
            IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            msg = "The requested band is unrecognized";
            ex.addData("Additional", msg);
            throw ex;
    }
    return returnValue;
}

ReceiverBand FrontEndUtils::intToBand(const int intband)
{
        ReceiverBand returnValue;

        switch(intband) {
            case 1:
                returnValue = ReceiverBandMod::ALMA_RB_01;
                break;
            case 2:
                returnValue = ReceiverBandMod::ALMA_RB_02;
                break;
            case 3:
                returnValue = ReceiverBandMod::ALMA_RB_03;
                break;
            case 4:
                returnValue = ReceiverBandMod::ALMA_RB_04;
                break;
            case 5:
                returnValue = ReceiverBandMod::ALMA_RB_05;
                break;
            case 6:
                returnValue = ReceiverBandMod::ALMA_RB_06;
                break;
            case 7:
                returnValue = ReceiverBandMod::ALMA_RB_07;
                break;
            case 8:
                returnValue = ReceiverBandMod::ALMA_RB_08;
                break;
            case 9:
                returnValue = ReceiverBandMod::ALMA_RB_09;
                break;
            case 10:
                returnValue = ReceiverBandMod::ALMA_RB_10;
                break;
            default:
                //This should never happen unless you supply something that is not a
                //BAND
                IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
                string msg = "The requested band number is unrecognized";
                ex.addData("Additional", msg);
                throw ex;

        }
        return returnValue;
    }

int FrontEndUtils::getIntFromName(const string name)
{
    size_t found;
    found=name.find_first_of("123456789");

    if (found==string::npos){
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "The string does not contain numbers.";
        ex.addData("Additional", msg);
        throw ex;
    }


    string substring = name.substr(found, string::npos);
    found = substring.find_first_not_of("1234567890");

    if (found!=string::npos) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "The string mixes numbers and letters. Something like WCA9 is expected";
        ex.addData("Additional", msg);
        throw ex;
    }

    // Convert string into a number
    istringstream iss(substring);
    int returnInt;
    iss >> returnInt;
    return returnInt;
}

std::string FrontEndUtils::getNameWithInt(const std::string name, const ReceiverBandMod::ReceiverBand band)
{
    int bandNum = bandToInt(band);
    stringstream ss;
    ss << name << bandNum;
    return ss.str();
}

std::string FrontEndUtils::getPowerDistName(const ReceiverBandMod::ReceiverBand band)
{
    return getNameWithInt(FrontEndUtils::POWERDIST_STRING, band);
}

std::string FrontEndUtils::getColdCartName(const ReceiverBandMod::ReceiverBand band)
{
    return getNameWithInt(FrontEndUtils::COLDCART_STRING, band);
}

std::string FrontEndUtils::getWCAName(const ReceiverBandMod::ReceiverBand band)
{
    return getNameWithInt(FrontEndUtils::WCA_STRING, band);
}


bool FrontEndUtils::isBandComponent(const std::string name)
{

    if (name.find("WCA") != string::npos)
        return true;
    if (name.find("ColdCart") != string::npos)
        return true;
    if (name.find("PowerDist") != string::npos)
        return true;
    return false;
}

int FrontEndUtils::getColdMultiplier(const ReceiverBand band)
{

    int returnValue = 0;
    string msg = "";
    switch(band) {
        case ReceiverBandMod::ALMA_RB_01:
            returnValue = 1;
            break;
        case ReceiverBandMod::ALMA_RB_02:
            returnValue = 1;
            break;
        case ReceiverBandMod::ALMA_RB_03:
            returnValue = 1;
            break;
        case ReceiverBandMod::ALMA_RB_04:
            returnValue = 2;
            break;
        case ReceiverBandMod::ALMA_RB_05:
            returnValue = 6;
            break;
        case ReceiverBandMod::ALMA_RB_06:
            returnValue = 3;
            break;
        case ReceiverBandMod::ALMA_RB_07:
            returnValue = 3;
            break;
        case ReceiverBandMod::ALMA_RB_08:
            returnValue = 6;
            break;
        case ReceiverBandMod::ALMA_RB_09:
            returnValue = 9;
            break;
        case ReceiverBandMod::ALMA_RB_10:
            returnValue = 9;
            break;
        default:
            returnValue = 1;
    }
    return returnValue;
}
