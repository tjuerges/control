/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * $Id$
 *
 */

//#include <limits>
//#include <vector>
#include <cppunit/extensions/HelperMacros.h>
//#include <TypeConversion.h>
#include <FrontEndUtils.h>
#include <ControlExceptions.h>

/**
 * A test case for the WCAHWSimBase class
 *
 */
class FrontEndUtilsTestCase: public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(FrontEndUtilsTestCase);
    CPPUNIT_TEST(test_bandToInt);
    CPPUNIT_TEST(test_intToBand);
    CPPUNIT_TEST(test_getIntFromName);
    CPPUNIT_TEST(test_getNameWithInt);
    CPPUNIT_TEST(test_getPowerDistName);
    CPPUNIT_TEST(test_getColdCartName);
    CPPUNIT_TEST(test_getWCAName);
    CPPUNIT_TEST(test_isBandComponent);
    CPPUNIT_TEST_SUITE_END();

    public:
//    void setUp();
//    void tearDown();

    protected:
    
    void test_bandToInt();
    void test_intToBand();
    void test_getIntFromName();
    void test_getNameWithInt();
    void test_getPowerDistName();
    void test_getColdCartName();
    void test_getWCAName();
    void test_isBandComponent();
    void test_getColdMultiplier();

};

CPPUNIT_TEST_SUITE_REGISTRATION(FrontEndUtilsTestCase);


//void FrontEndUtilsTestCase::setUp()
//{
//}
//
//void FrontEndUtilsTestCase::tearDown()
//{
//}

void FrontEndUtilsTestCase::test_bandToInt() 
{
    
    CPPUNIT_ASSERT_THROW(FrontEndUtils::bandToInt((ReceiverBandMod::ReceiverBand)1000), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_THROW(FrontEndUtils::bandToInt(ReceiverBandMod::UNSPECIFIED), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_EQUAL(1, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_01));
    CPPUNIT_ASSERT_EQUAL(2, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_02));
    CPPUNIT_ASSERT_EQUAL(3, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_03));
    CPPUNIT_ASSERT_EQUAL(4, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_04));
    CPPUNIT_ASSERT_EQUAL(5, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_05));
    CPPUNIT_ASSERT_EQUAL(6, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_06));
    CPPUNIT_ASSERT_EQUAL(7, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_07));
    CPPUNIT_ASSERT_EQUAL(8, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_08));
    CPPUNIT_ASSERT_EQUAL(9, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_09));
    CPPUNIT_ASSERT_EQUAL(10, FrontEndUtils::bandToInt(ReceiverBandMod::ALMA_RB_10));

}


void FrontEndUtilsTestCase::test_intToBand() 
{
    CPPUNIT_ASSERT_THROW(FrontEndUtils::intToBand(1000), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_01, FrontEndUtils::intToBand(1));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_02, FrontEndUtils::intToBand(2));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_03, FrontEndUtils::intToBand(3));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_04, FrontEndUtils::intToBand(4));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_05, FrontEndUtils::intToBand(5));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_06, FrontEndUtils::intToBand(6));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_07, FrontEndUtils::intToBand(7));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_08, FrontEndUtils::intToBand(8));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_09, FrontEndUtils::intToBand(9));
    CPPUNIT_ASSERT_EQUAL(ReceiverBandMod::ALMA_RB_10, FrontEndUtils::intToBand(10));
}

void FrontEndUtilsTestCase::test_getIntFromName()
{
    CPPUNIT_ASSERT_THROW(FrontEndUtils::getIntFromName("WCA"), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_THROW(FrontEndUtils::getIntFromName("WCA1B9"), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_EQUAL(1, FrontEndUtils::getIntFromName("WCA1"));
    CPPUNIT_ASSERT_EQUAL(10, FrontEndUtils::getIntFromName("WCA10"));
    CPPUNIT_ASSERT_EQUAL(990, FrontEndUtils::getIntFromName("WCA990"));
}

void FrontEndUtilsTestCase::test_getNameWithInt()
{
    CPPUNIT_ASSERT_THROW(FrontEndUtils::getNameWithInt("WCA", (ReceiverBandMod::ReceiverBand)18), ControlExceptions::IllegalParameterErrorExImpl);
    std::string expected("WCA9");
    CPPUNIT_ASSERT_EQUAL(expected, FrontEndUtils::getNameWithInt("WCA", ReceiverBandMod::ALMA_RB_09));
}

void FrontEndUtilsTestCase::test_getPowerDistName()
{
    std::string expected("PowerDist9");
    CPPUNIT_ASSERT_THROW(FrontEndUtils::getPowerDistName((ReceiverBandMod::ReceiverBand)18), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_EQUAL(expected, FrontEndUtils::getPowerDistName(ReceiverBandMod::ALMA_RB_09));

}

void FrontEndUtilsTestCase::test_getColdCartName()
{
    std::string expected("ColdCart9");
    CPPUNIT_ASSERT_THROW(FrontEndUtils::getColdCartName((ReceiverBandMod::ReceiverBand)18), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_EQUAL(expected, FrontEndUtils::getColdCartName(ReceiverBandMod::ALMA_RB_09));

}

void FrontEndUtilsTestCase::test_getWCAName()
{
    std::string expected("WCA9");
    CPPUNIT_ASSERT_THROW(FrontEndUtils::getWCAName((ReceiverBandMod::ReceiverBand)18), ControlExceptions::IllegalParameterErrorExImpl);
    CPPUNIT_ASSERT_EQUAL(expected, FrontEndUtils::getWCAName(ReceiverBandMod::ALMA_RB_09));

}

void FrontEndUtilsTestCase::test_isBandComponent()
{
    CPPUNIT_ASSERT_EQUAL(true, FrontEndUtils::isBandComponent("WCA1"));
    CPPUNIT_ASSERT_EQUAL(true, FrontEndUtils::isBandComponent("ColdCart1"));
    CPPUNIT_ASSERT_EQUAL(true, FrontEndUtils::isBandComponent("PowerDist1"));
    CPPUNIT_ASSERT_EQUAL(true, FrontEndUtils::isBandComponent("PowerDist11"));
    CPPUNIT_ASSERT_EQUAL(false, FrontEndUtils::isBandComponent("Cryostat"));
    CPPUNIT_ASSERT_EQUAL(false, FrontEndUtils::isBandComponent("ACD"));

}
/**
 * Main function running the tests
 */
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main(int argc, char* argv[])
{
    // Create the event manager and test controller
    CPPUNIT_NS::TestResult controller;

    // Add a listener that colllects test result
    CPPUNIT_NS::TestResultCollector result;
    controller.addListener(&result);

    // Add a listener that print dots as test run.
    CPPUNIT_NS::BriefTestProgressListener progress;
    controller.addListener(&progress);

    // Add the top suite to the test runner
    CPPUNIT_NS::TestRunner runner;
    runner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    runner.run(controller);

    // Print test in a compiler compatible format.
    CPPUNIT_NS::CompilerOutputter outputter(&result, std::cerr);
    outputter.write();

    return result.wasSuccessful() ? 0 : 1;
}
