#!/usr/bin/env python
# $Id$
#
# Copyright (C) 2004
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu
#
"""

Definition of a pyunit (or unittest) test case class for the Local
Oscillator Reference Receiver (LORR) hardware testing using only the
AmbManager for AMB transactions, and a script to run the tests from
the command line. 

An example of how to execute the test script on devices connected to
the ABM named 'ABM001' on channel 2 with node ids 64, 65, and 66:

./LORRAMBTest.py ABM001 0 34
"""
__version__ = '$Id$'

import AMBMCTest
import struct

class TestCase(AMBMCTest.TestCase):
    """Class for implementing LORR device hardware 'unit tests'.
    """
    def test_EFC_25_MHZ(self):
        "Test the LORR EFC_25_MHZ monitor point"
        (data, ts) = self.monitorSizeTest(0x02, 2)
        value = struct.unpack('!h', data)
        result = value[0]*20./4095.
        self.assert_((result > 0.0) & (result < 9.0))
        return
    def test_EFC_125_MHZ(self):
        "Test the LORR EFC_125_MHZ monitor point"
        (data, ts) = self.monitorSizeTest(0x03, 2)
        value = struct.unpack('!h', data)
        result = value[0]*20./4095.
        self.assert_((result > 1.0) & (result < 9.0))
        return
    def test_PWR_25_MHZ(self):
        "Test the LORR PWR_25_MHZ monitor point"
        (data, ts) = self.monitorSizeTest(0x04, 2)
        value = struct.unpack('!h', data)
        result = value[0]*20./4095.
        self.assert_((result > 0.0) & (result < 10.0))
        return
    def test_PWR_125_MHZ(self):
        "Test the LORR PWR_125_MHZ monitor point"
        (data, ts) = self.monitorSizeTest(0x05, 2)
        value = struct.unpack('!h', data)
        result = value[0]*20./4095.
        self.assert_((result > 0.0) & (result < 10.0))
        return
    def test_PWR_2_GHZ(self):
        "Test the LORR PWR_2_GHZ monitor point"
        (data, ts) = self.monitorSizeTest(0x06, 2)
        value = struct.unpack('!h', data)
        result = value[0]*20./4095.
        self.assert_((result > 0.0) & (result < 10.0))
        return
    def test_RX_OPT_PWR(self):
        "Test the LORR RX_OPT_PWR monitor point"
        (data, ts) = self.monitorSizeTest(0x07, 2)
        value = struct.unpack('!h', data)
        result = value[0]*20./4095.
        self.assert_((result > 0.025) & (result < 5.0))
        return
    def test_VDC_12(self):
        "Test the LORR VDC_12 monitor point"
        (data, ts) = self.monitorSizeTest(0x09, 2)
        value = struct.unpack('!h', data)
        result = value[0]*40./4095.
        self.assert_((result > 11.5) & (result < 12.5))
        return
    def test_VDC_15(self):
        "Test the LORR VDC_15 monitor point"
        (data, ts) = self.monitorSizeTest(0x0A, 2)
        value = struct.unpack('!h', data)
        result = value[0]*40./4095.
        self.assert_((result > 14.5) & (result < 15.5))
        return

from Acspy.Clients.SimpleClient import PySimpleClient

if __name__ == '__main__':
    client = PySimpleClient()
    # This line is added to make an error mesage go away. It should
    # not be necessary and uggests a problem with reference counting
    # that I do not have time to track down.
    AMBMCTest.main(TestCase, __file__)

