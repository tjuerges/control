#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#
# @(#) $Id$

import sys
import unittest
import struct
import ControlExceptions
import CCL.Antenna
import CCL.AmbManager
from time import sleep

from Acspy.Common.QoS import setObjectTimeout

import Acspy.Util

class automated(unittest.TestCase):
	def __init__(self, methodName="runTest"):
		unittest.TestCase.__init__(self, methodName)
		setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)
		self._antenna = CCL.Antenna.Antenna(antenna)
		self._antenna.controllerOperational()
		if self._antenna.isAmbManagerRunning() == False:
			self._antenna.startAmbManager()
		self.manager = CCL.AmbManager.AmbManager(antenna)
		setObjectTimeout(self.manager._AmbManager__mgrRef, 20000)
		self.node = 0x22
		self.channel = 0
		self.monitor = {0x01:1, 0x02:2, 0x03:2, 0x04:2, 0x05:2, 0x06:2,
						 0x07:2, 0x09:2, 0x0a:2,
						 0x30000:3, 0x30001:4, 0x30002:4, 0x30003:4, 0x30004:3,
						 0x30005:2}
		self.command = {0x31001:1}
		self.out = ""

		self.invalidNodes = []
		for point in range (2, 16):
			self.invalidNodes.append(point)
		self.invalidNodes.append(22)
		for point in range (24, 27):
			self.invalidNodes.append(point)
		for point in range (73, 80):
			self.invalidNodes.append(point)
		for point in range (88, 96):
			self.invalidNodes.append(point)
		for point in range (112, 156):
			self.invalidNodes.append(point)
		for point in range (576, 640):
			self.invalidNodes.append(point)
		for point in range (768, 2048):
			self.invalidNodes.append(point)

	def __del__(self):
		del self._antenna
		if self.out != None:
			print self.out
		del(self.manager)


	def startTestLog(self, testName):
		self.log("\n")
		self.log("-------------------------------------------------------")
		self.log("Begin " + testName)

	def endTestLog(self, testname):
		self.log("End" + testname)
		self.log("-------------------------------------------------------")

	def log(self, msg):
		self.out += msg + "\n"

	# --------------------------------------------------
	# Test Definitions

	def test01(self):
		'''
		This test verifies the PWR_25_MHZ, PWR_125_MHZ and PWR_2_GHZ monitor
		points are properly returned and are in the allowed range [0; 10]V.
		'''
		self.startTestLog("Test 1: PWR_25_MHZ, PWR_125_MHZ and PWR_2_GHZ tests")
		(r, t) = self.manager.monitor(self.channel, self.node, 0x04)
		self.failUnless(len(r) == 2, "PWR_25_MHZ returned incorrect length value")

		(resp,) = struct.unpack("!h", r)
		self.log("The PWR_25_MHZ value is %4.2f" % (resp * 20.0 / 4095.0)) 
		self.failUnless(((resp * 20.0 / 4095.0) > 0.0) and ((resp * 20.0 / 4095.0) < 10.0), "The PWR_25_MHZ value is not in the allowed range. Value=%4.2f" % (resp * 20.0 / 4095.0))

 		(r, t) = self.manager.monitor(self.channel, self.node, 0x05)
 		self.failUnless(len(r) == 2, "PWR_125_MHZ returned incorrect length value")

 		(resp,) = struct.unpack("!h", r)
		self.log("The PWR_125_MHZ value is %4.2f" % (resp * 20.0 / 4095.0)) 		
 		self.failUnless(((resp * 20.0 / 4095.0) > 0.0) and ((resp * 20.0 / 4095.0) < 10.0), "The PWR_125_MHZ value is not in the allowed range. Value=%4.2f" % (resp * 20.0 / 4095.0))

 		(r, t) = self.manager.monitor(self.channel, self.node, 0x06)
 		self.failUnless(len(r) == 2, "PWR_2_GHZ returned incorrect length value")

 		(resp,) = struct.unpack("!h", r)
		self.log("The PWR_2_GHZ value is %4.2f" % (resp * 20.0 / 4095.0)) 		
 		self.failUnless(((resp * 20.0 / 4095.0) > 0.0) and ((resp * 20.0 / 4095.0) < 10.0), "The PWR_2_GHZ value is not in the allowed range. Value=%4.2f" % (resp * 20.0 / 4095.0))

		self.endTestLog("Test 1: PWR_25_MHZ, PWR_125_MHZ and PWR_2_GHZ tests")

	def test02(self):
		'''
		Verify correct response from RX_OPT_PWR monitor point.
		'''
		self.startTestLog("Test 2: RX_OPT_PWR test")
		(r, t) = self.manager.monitor(self.channel, self.node, 0x07)
		self.failUnless(len(r) == 2,
						"RX_OPT_PWR returned incorrect length value")

		(resp,) = struct.unpack("!h", r)
		self.log("The RX_OPT_PWR value is %5.3f" % (resp * 20.0 / 4095.0)) 		
		self.failUnless(((resp * 20.0 / 4095.0) > 0.025) and ((resp * 20.0 / 4095.0) < 1.0),
						 "The RX_OPT_PWR value is not in the allowed range Value=%5.3f" % (resp * 20.0 / 4095.0))

		self.endTestLog("Test 2: RX_OPT_PWR test")

	def test03(self):
		'''
		Verify correct response from the VDC_12 and VDC_15 monitor points.
		'''
		self.startTestLog("Test 3: VDC_12 and VDC_15 tests")
		(r, t) = self.manager.monitor(self.channel, self.node, 0x09)
		self.failUnless(len(r) == 2, "VDC_12 returned incorrect length value")

		(resp,) = struct.unpack("!h", r)
		self.log("The VDC_12 value is %5.2f" % (resp * 40.0 / 4095.0)) 		
		self.failUnless(((resp * 40.0 / 4095.0) > 10.5) and ((resp * 40.0 / 4095.0) < 11.5),
							 "The VDC_12 value is not in the allowed range. Value=%5.2f" % (resp * 40.0 / 4095.0))

		(r, t) = self.manager.monitor(self.channel, self.node, 0x0a)
		self.failUnless(len(r) == 2,
							"VDC_15 returned incorrect length value")

		(resp,) = struct.unpack("!h", r)
		self.log("The VDC_15 value is %5.2f" % (resp * 40.0 / 4095.0)) 			
		self.failUnless(((resp * 40.0 / 4095.0) > 13.0) and ((resp * 40.0 / 4095.0) < 14.0),
							 "The VDC_15 value is not in the allowed range. Value=%5.2f" % (resp * 40.0 / 4095.0))


		self.endTestLog("Test 3: VDC_12 and VDC_15 tests")
		

	def test07(self):
		'''
		This test verifies an appropiate response from the GET_CAN_ERROR monitor point.
		'''
		self.startTestLog("Test 7: GET_CAN_ERROR test")
		
		try:
			(r, t) = self.manager.monitor(self.channel, self.node, 0x30001)
			self.failUnless(len(r) == 4, "STATUS returned incorrect length value")
		except:
			self.fail("Exception generated monitor point 0x30001")
			
		resp = struct.unpack("!HBB", r)
		self.failUnless(resp[0] == 0x0000, "Bytes 0 and 1 are 0x%x" % resp[0])
		self.failUnless(resp[1] == 0x00, "Byte 2 is 0x%x" % resp[1])
		self.failUnless((resp[2] == 0x18 or resp[2] == 0x38), "Byte 3 is 0x%x" % resp[2])

		self.endTestLog("Test 7: GET_CAN_ERROR test")

class interactive(automated):
	def __init__(self, methodName="runTest"):
		automated.__init__(self, methodName)

	def tearDown(self):
		self.out = None

	def log(self, msg):
		print msg

	# --------------------------------------------------
	# Test Definitions


	def test04(self):
		'''
		This test monitors all RCAs between 1 and 0x3FFFF (the full
		range allocated to each device).  It ensures that only
		monitors documented in the ICD respond, and that the response
		is valid (correct number of bytes).
		Although this test is automated, it is in the interactive section
		because it takes a long time to run.
		'''
		self.startTestLog("Test 4: Monitor Survey test")
		errorList = {}
		for RCA in range (0x1, 0x40000):
			try:
				(r, t) = self.manager.monitor(self.channel, self.node, RCA)
				if self.monitor.has_key(RCA):
					if len(r) != self.monitor[RCA][2]:
						errorList[RCA] = len(r)
				else:
						errorList[RCA] = len(r)
			except:
				pass

		if len(errorList) != 0:
			self.log("%d offending responses" % len(errorList))
			if len(errorList) < 10:
				for idx in errorList.keys():
					if self.monitor.has_key(RCA):
						self.log("RCA 0x%x returned %d bytes (%d expected)" % \
							(idx, errorList[idx], self.monitor[RCA][2]))
					else:
						self.log("RCA 0x%x returned %d bytes" % \
							(idx, errorList[idx]))
			self.fail("%d Monitor points not in ICD Responeded" % 
				len(errorList))
			self.endTestLog("Test 4: Monitor Survey test")	

	def test05(self):
		'''
		This test monitors all correct RCAs for incorrect node addresses.
		It ensures that the LORR does not respond to any of these monitors.
		Although this is an automated test, it is in the interactive section
		because it takes a long time to run.
		'''
		self.startTestLog("Test 5: Monitor Survey test pt. 2")
		# errorList = {}
		for node in self.invalidNodes:
			for RCA in self.monitor.keys():
				try:
					(r, t) = self.manager.monitor(self.channel, node , RCA)
					self.fail("LORR responded at 0x%x RCA for node 0x%x" % (RCA, node))
				except:
					pass
		self.endTestLog("Test 5: Monitor Survey test pt. 2")

	def test06(self):
		'''
		This tests control points at all RCAs between 1 and 0x3FFFF (the full
		range allocated to each device).  It ensures that only
		controls documented in the ICD respond.
		Although this is an automated test, it is in the interactive section
		because it takes a long time to run.
		'''
		self.startTestLog("Test 6: Control Survey test")
		self.log('''Verify that the FPGA and AMBSI2 do not reset throughout this test.''')
		#rv = raw_input("Press <enter> to begin.")
		cmd = [0x01]
		for RCA in range (0x1, 0x40000):
			if self.command.has_key (RCA):
				pass
			else:
				try:
					t = self.manager.command(self.channel, self.node, RCA, struct.pack("!%dB" % len(cmd), *cmd))
				except:
					self.fail("Exception generated control point 0x%x" % RCA)

				tmprca = 0x30002
				try:
					(r, t) = self.manager.monitor(self.channel, self.node, tmprca)
					self.failUnless(len(r) == self.monitor[tmprca],
							 "Length of response incorrect for RCA:0x%x" % tmprca)
				except ControlExceptions.CAMBErrorEx:
					self.fail("Exception generated monitoring point 0x%x" % tmprca)
		
				resp = struct.unpack("!I", r)
				self.failUnless(resp > 0, "Reset occurrred at control point %x" % RCA)		

		self.endTestLog("Test 6: Control Survey test")

	def test08(self):
		'''
		This test verifies that the device correctly responds to
		broadcast messages and returns the serial number when requested
		by RCA 0x0.  Also, use an oscilloscope to verify that the time between
		end of broadcast and beginning of response is no more than a few
		milliseconds.

		For recordkeeping the serial number is returned.
		'''
		self.startTestLog("Test 8: Broadcast Response Test")
		self.log('''Usxe the oscilloscope to verify that the time between end of broadcast and beginning of response is no more than a few milliseconds.''')
		rv = raw_input("Press <enter> to begin.")
		try:
			(r, t) = self.manager.getNodes(self.channel)
		except:
			self.fail("Unable to get nodes on bus")

		foundNode = False
		for node in r:
			if node.node == self.node:
				if not foundNode:
					foundNode = True
					self.log("Device Serial Number: 0x%x" % node.sn)
				else:
					self.fail("Node 0x%x responded at least twice" % self.node)

		self.failUnless(foundNode, "Node %d did not respond" % self.node)

		try:
			(sn, time) = self.manager.findSN(self.channel, self.node)
		except:
			self.fail("Exception retrieving serial number from node")

		self.endTestLog("Test 8: Broadcast Response Test")
	
	def test09(self):
		'''
		This test does a CAN monitor at each of the monitor points in
		the ICD.  The timing from the end of the request to the beginning of
		the response should be measured using an oscilloscope to verify that
		the delay is less than 150 us.
		'''
		self.startTestLog("Test 9: Monitor Timing Test")
		self.log('''Connect CAN wires (AMB pin 2 and 7) to oscilliscope and
set for single trigger.  Time base should be approximatly
200 us.
Press <enter> to generate monitor request,
n[ext] to process next monitor point.''')

		rv = raw_input("press <enter> to begin:")
		for rca in self.monitor.keys():
			rv = ''
			while len(rv) == 0 or rv != "n"[0:len(rv)]:
				try:
					(r, t) = self.manager.monitor(self.channel, self.node, rca)
					self.failUnless(len(r) == self.monitor[rca],
						"Length of response incorrect for RCA:0x%x"	 % rca)
					rv = raw_input("CAN request sent to RCA: 0x%x " % rca)
				except ControlExceptions.CAMBErrorEx:
					self.fail("Exception generated monitoring point 0x%x" % rca)
		self.endTestLog("Test 9: Monitor Timing Test")

	def test10(self):
		self.log("\nThis test does not require software. Test the TE levels on TIMA and TIMB \
with differential probe. Then measure the duty cycle (6ms out of 48ms?). Then, with a \
single ended probe, measure voltage between the AMB connector and TIMA/TIMB.")

	def test11(self):
		'''
		This test toggles the reset wires.  It should be verified that the
		reset pulse is detected and the AMBSI resets.  In addition
		the levels on these lines should be checked using an oscilliscope
		'''
		self.startTestLog("Test 11: AMB Reset Test")
		self.log('''Connect Reset wires (AMB pin 1 and 6) to oscilliscope
and set for single trigger.  Time base should be
approximatly 200 us''')

		rv = raw_input("Press <enter> to generate monitor point.")
		tmprca = 0x30002
		try:
			(r, t) = self.manager.monitor(self.channel, self.node, tmprca)
			self.failUnless(len(r) == self.monitor[tmprca],
					 "Length of response incorrect for RCA:0x%x" % tmprca)
		except ControlExceptions.CAMBErrorEx:
			self.fail("Exception generated monitoring point 0x%x" % tmprca)
		
		resp = struct.unpack("!I", r)
		print "Number of transactions since power-up: %d" % resp

		rv = raw_input("Press <enter> to generate reset request, type e[xit] to exit ")

		while len(rv) == 0 or rv != "e"[0:len(rv)]:
			t = self.manager.reset(self.channel)
			rv = raw_input("Reset Generated ")

		rv = raw_input("Press <enter> to generate monitor point.")
		tmprca = 0x30002
		try:
			(r, t) = self.manager.monitor(self.channel, self.node, tmprca)
			self.failUnless(len(r) == self.monitor[tmprca],
					 "Length of response incorrect for RCA:0x%x" % tmprca)
		except ControlExceptions.CAMBErrorEx:
			self.fail("Exception generated monitoring point 0x%x" % tmprca)
		
		resp = struct.unpack("!I", r)
		print "Number of transactions since power-up: %d" % resp

		self.endTestLog("Test 11: AMB Reset Test")

	def test12(self):
		'''
		This test verifies an appropiate response from the STATUS monitor point,
		as well as that bit 5 is not toggling.
		'''
		self.startTestLog("Test 12: STATUS test")
	        self.log('''Generate a monitor point.  Wait 10 seconds, then generate another monitor point.''')

		for x in range (1, 3):
			rv = raw_input("Press <enter> to generate a monitor point.")
			(r, t) = self.manager.monitor(self.channel, self.node, 0x01)
			self.failUnless(len(r) == 1, "STATUS returned incorrect length value")

			resp = struct.unpack("!1B", r)
		        self.log("The STATUS value is 0x%x" % resp[0]) 	
			self.failUnless(resp[0] == 0x60, "STATUS has the wrong bits set (0x%x)." % resp[0])

		self.endTestLog("Test 12: STATUS test")

	def test13(self):
		'''
		This test verifies the EFC_25_MHZ and EFC_2_GHZ monitor points are properly
		returned and are in the allowed range [1; 9]V.
			'''
		self.startTestLog("Test 13: EFC_25_MHZ and EFC_2_GHZ tests")
		self.log('''Verify that the voltages returned by these monitor points do not fluctuate''')
		rv = raw_input("Press <enter> to generate EFC_25_MHZ request, type e[xit] to continue to next monitor point ")
		while len(rv) == 0 or rv != "e"[0:len(rv)]:
			(r, t) = self.manager.monitor(self.channel, self.node, 0x02)
			self.failUnless(len(r) == 2, "EFC_25_MHZ returned incorrect length value")

			(resp,) = struct.unpack("!h", r)
			self.log("The EFC_25_MHZ value is %f" % (resp * 20.0 / 4095.0)) 				
			self.failUnless(((resp * 20.0 / 4095.0) > 1.0) and ((resp * 20.0 / 4095.0) < 9.0),
							 "The EFC_25_MHZ value is not in the allowed range. Value=%f" % (resp * 20.0 / 4095.0))
			rv = raw_input("Monitor Point = %f" % (resp * 20.0 / 4095.0))

		rv = raw_input("Press <enter> to generate EFC_2_GHZ request, type e[xit] to exit ")
		while len(rv) == 0 or rv != "e"[0:len(rv)]:
			(r, t) = self.manager.monitor(self.channel, self.node, 0x03)
			self.failUnless(len(r) == 2, "EFC_2_GHZ returned incorrect length value")

			(resp,) = struct.unpack("!h", r)
		        self.log("The EFC_2_GHZ value is %f" % (resp * 20.0 / 4095.0)) 
			self.failUnless(((resp * 20.0 / 4095.0) > 1.0) and ((resp * 20.0 / 4095.0) < 9.0),
						 "The EFC_2_GHZ value is not in the allowed range. Value=%f" % (resp * 20.0 / 4095.0))
			rv = raw_input("Monitor Point = %f" % (resp * 20.0 / 4095.0))
					
		self.endTestLog("Test 13: EFC_25_MHZ and EFC_2_GHZ tests")

	def test14(self):
		'''
		This test tests the control point for the LORR.
		'''
		self.startTestLog("Test 14: Control point test")
		self.log('''Generate control point and verify that the FPGA and AMBSI2 reset.''')
		self.log('''Connect CAN wires (AMB pin 2 and 7) to oscilliscope and
set for single trigger.  Time base should be approximatly
200 us.''') 

		rv = raw_input("Press <enter> to generate monitor point.")
		tmprca = 0x30002
		try:
			(r, t) = self.manager.monitor(self.channel, self.node, tmprca)
			self.failUnless(len(r) == self.monitor[tmprca],
					 "Length of response incorrect for RCA:0x%x" % tmprca)
		except ControlExceptions.CAMBErrorEx:
			self.fail("Exception generated monitoring point 0x%x" % tmprca)
		
		resp = struct.unpack("!I", r)
		print "Number of transactions since power-up: %d" % resp

		self.log('''Verify that the oscilloscope detects the control point.''')
		rv = raw_input("press <enter> to generate control point.:")
		rv = ''
		cmd = [0x01]
		rca = 0x31001
	
		try:
		#	t = self.manager.command( self.channel, self.node, rca, struct.pack( "!%dB"%len( cmd ), *cmd ) )
			t = self.manager.command(self.channel, self.node, rca, struct.pack("!B", *cmd))
		except:
			self.fail("Exception generated control point 0x%x" % rca)
	
		self.log('''Verify that the FPGA and AMBSI2 reset''')

		rv = raw_input("Press <enter> to generate monitor point.")
		tmprca = 0x30002
		try:
			(r, t) = self.manager.monitor(self.channel, self.node, tmprca)
			self.failUnless(len(r) == self.monitor[tmprca],
					 "Length of response incorrect for RCA:0x%x" % tmprca)
		except ControlExceptions.CAMBErrorEx:
			self.fail("Exception generated monitoring point 0x%x" % tmprca)
		
		resp = struct.unpack("!I", r)
		print "Number of transactions since power-up: %d" % resp
					
		self.endTestLog("Test 14: Control point test")

	def test15(self):
		'''
		This test verifies that the device only responds to control commands of the correct length
                '''

		self.startTestLog("Test 15: Ill formed control test")
		cmd = [0x01]
		while len(cmd) < 8:
			if len(cmd) != self.command[key]:
				t = self.manager.command(self.channel, self.node, key, struct.pack("!%dB" % len(cmd), *cmd))
				self.log('''Verify that the FPGA and AMBSI2 did not reset.  If they did reset, then the test failed.''')
				rv = raw_data("press <enter> to continue.")
			cmd.append(0x01)
			
		self.endTestLog("Test 15: Ill formed command test")

	def test16(self):
		'''
		This test tests that the CAN_H and CAN_L signals are at the correct voltages.
		'''
		self.startTestLog("Test 16: CAN signal test")
		self.log('''Connect CAN wires (AMB pin 2 and 7) to oscilliscope and
set for single trigger.  Time base should be approximatly
200 us.  Verify that the differential voltage is 0V.  Then,
with a single ended probe, measure the voltage between the
AMB connector and CAN_L/CAN_H.  It should be 2V.  Press
<enter> to generate monitor request.  Verify that CAN_H and
CAN_L are in anti-phase and should be at 1.5V and 3.5V.''')

		rv = raw_input("press <enter> to generate monitor request, and press e[xit] to exit.:")
		rv = ''
		rca = 0x01
		while len(rv) == 0 or rv != "e"[0:len(rv)]:
			try:
				(r, t) = self.manager.monitor(self.channel, self.node, rca)
				self.failUnless(len(r) == self.monitor[rca],
						 "Length of response incorrect for RCA:0x%x"	 % rca)
				rv = raw_input("CAN request sent to RCA: 0x%x " % rca)
			except ControlExceptions.CAMBErrorEx:
				self.fail("Exception generated monitoring point 0x%x" % rca)
		self.endTestLog("Test 16: CAN signal test")			

	def test17(self):
		'''
		This tests that the STATUS monitor has the correct bits set
		when the fiber-optic cable is unplugged.
		'''
		self.startTestLog("Test 17: Fiber-optic cable test")
		self.log('''Unplug the fiber-optic cable.''')
		rv = raw_input("Press <enter> when ready.")
		(r, t) = self.manager.monitor(self.channel, self.node, 0x01)
		self.failUnless(len(r) == 1, "STATUS returned incorrect length value")

		resp = struct.unpack("!1B", r)
		self.failUnless(resp[0] == 0x38, "STATUS has the wrong bits set (x0%x)." % (resp[0]))
		self.endTestLog("Test 17: Fiber-optic cable test")

	def test18(self):
		'''
		This test verifies an appropiate response from the GET_PROTOCOL_REV_LEVEL monitor point.
		'''
		self.startTestLog("Test 18: GET_PROTOCOL_REV_LEVEL test")
		self.log('''Verify that the correct revision level is printed to the screen.''')

		(r, t) = self.manager.monitor(self.channel, self.node, 0x30000)
		self.failUnless(len(r) == 3, "STATUS returned incorrect length value")

		resp = struct.unpack("!3B", r)

		print "V%x.%x.%x" % (resp[0], resp[1], resp[2])

		
		self.endTestLog("Test 18: GET_PROTOCOL_REV_LEVEL test")

	def test19(self):
		'''
		This test verifies an appropiate response from the GET_TRANS_NUM monitor point.
		'''
		self.startTestLog("Test 19: GET_TRANS_NUM test")
		self.log('''Verify that the correct number of transactions handled by the LORR since power-up is printed to the screen.''')

		(r, t) = self.manager.monitor(self.channel, self.node, 0x30002)
		self.failUnless(len(r) == 4, "STATUS returned incorrect length value")

		resp = struct.unpack("!I", r)

		print "Number of transactions since power-up: %d" % resp
		
		self.endTestLog("Test 19: GET_TRANS_NUM test")

	def test20(self):
		'''
		This test verifies an appropiate response from the GET_AMBIENT_TEMPERATURE monitor point.
		'''
		self.startTestLog("Test 20: GET_AMBIENT_TEMPERATURE test")
		self.log('''Verify that the correct ambient temperature measured by DS1820 on M&C interface board is printed to the screen.''')

		(r, t) = self.manager.monitor(self.channel, self.node, 0x30003)
		self.failUnless(len(r) == 4, "STATUS returned incorrect length value")

		resp = struct.unpack("!HBB", r)

		print "Bytes 0-1 = 0x%x, Byte 2 = 0x%x. Byte 3 = 0x%x" % (resp[0], resp[1], resp[2])
		
		self.endTestLog("Test 20: GET_AMBIENT_TEMPERATURE test")

	def test21(self):
		'''
		This test verifies an appropiate response from the GET_SW_REV_LEVEL monitor point.
		'''
		self.startTestLog("Test 21: GET_SW_REV_LEVEL test")
		self.log('''Verify that the correct revision level of the embedded code is printed to the screen.''')

		(r, t) = self.manager.monitor(self.channel, self.node, 0x30004)
		self.failUnless(len(r) == 3, "STATUS returned incorrect length value")

		resp = struct.unpack("!3B", r)

		print "V%x.%x.%x" % (resp[0], resp[1], resp[2])

		
		self.endTestLog("Test 21: GET_SW_REV_LEVEL test")		

# **************************************************
# MAIN Program
from Control import DeviceConfig

if __name__ == '__main__':
	if len(sys.argv) == 1:
		sys.argv.append("interactive")

	if len(sys.argv) == 2:
		sys.argv.append("DV01")

	antenna = sys.argv[2]
	sys.argv.remove(sys.argv[2])

	unittest.main()
