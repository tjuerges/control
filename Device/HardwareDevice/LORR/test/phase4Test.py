#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#
# @(#) $Id$

import sys
import unittest
import struct
import Control
import ControlExceptions
import time
import TETimeUtil
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.TimeHelper import TimeUtil

from Acspy.Common.QoS import setObjectTimeout

import Acspy.Util

class automated(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self._client = PySimpleClient.getInstance()
        setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 50000)
        self.out = ""
        self.tu = TimeUtil()
        unittest.TestCase.__init__(self, methodName)

    def __del__(self):
        if self.out != None:
            print self.out
        self._client.disconnect()


    def setUp(self):
        self._ref = self._client.getComponent("CONTROL/" + antenna + "/LORR")
        setObjectTimeout(self._ref, 20000)
        self._ref.hwStart()
        self._ref.hwConfigure()
        self._ref.hwInitialize()
        self._ref.hwOperational()
        
    def tearDown(self):
        self._ref.hwStop()
        self._client.releaseComponent("CONTROL/" + antenna + "/LORR")

    def startTestLog(self, testName):
        self.log("\n")
        self.log("-------------------------------------------------------")
        self.log("Begin " + testName)

    def endTestLog(self, testname):
        self.log("End" + testname)
        self.log("-------------------------------------------------------")

    def log(self, msg):
        self.out += msg + "\n"


    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Test 1 Verifies an appropiate response from the STATUS monitor point.
        '''
        self.startTestLog("Test 1: STATUS test")

        ro_prop = self._ref._get_STATUS()
        (r, c) = ro_prop.get_sync()
        self.failIf((r & 0x31) != 0x01, "STATUS has the wrong bits set "
		      "(x0%x)." % (r & 0x31))

        self.endTestLog("Test 1: STATUS test")
        pass

    def test02(self):
        '''
        This test verifies the EFC_125_MHZ monitor point is properly
        returned and is in the allowed range [1; 9]V.
            '''
        self.startTestLog("Test 2: EFC_125_MHZ test")

        ro_prop = self._ref._get_EFC_125_MHZ()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 1.0) and (r < 9.0),
                 "The EFC_125_MHZ value is not in the allowed range. Value=%f"
                 % r)

        self.endTestLog("Test 2: EFC_125_MHZ test")
        pass

    def test03(self):
        '''
        This test verifies the EFC_COMB_LINE_PLL monitor point is properly
        returned and is in the allowed range [1; 9]V.
            '''
        self.startTestLog("Test 3: EFC_COMB_LINE_PLL test")

        ro_prop = self._ref._get_EFC_COMB_LINE_PLL()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 1.0) and (r < 9.0),
                             "The EFC_COMB_LINE_PLL value is not in the" 
                             "allowed range. Value=%f" % r)

        self.endTestLog("Test 3: EFC_COMB_LINE_PLL test")
        pass

    def test04(self):
        '''
        This test verifies the PWR_25_MHZ monitor point is properly
        returned and is in the allowed range [0; 10]V.
            '''
        self.startTestLog("Test 4: PWR_25_MHZ test")

        ro_prop = self._ref._get_PWR_25_MHZ()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 0.0) and (r < 10.0),
                             "The PWR_25_MHZ value is not in the allowed "
                             "range. Value=%f" % r)

        self.endTestLog("Test 4: PWR_25_MHZ test")
        pass

    def test05(self):
        '''
        This test verifies the PWR_125_MHZ monitor point is properly
        returned and is in the allowed range [0; 10]V.
            '''
        self.startTestLog("Test 5: PWR_125_MHZ test")

        ro_prop = self._ref._get_PWR_125_MHZ()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 0.0) and (r < 10.0),
                             "The PWR_125_MHZ value is not in the allowed "
                             "range. Value=%f" % r)

        self.endTestLog("Test 5: PWR_125_MHZ test")
        pass

    def test06(self):
        '''
        This test verifies the PWR_2_GHZ monitor point is properly
        returned and is in the allowed range [0; 10]V.
            '''
        self.startTestLog("Test 6: PWR_2_GHZ test")

        ro_prop = self._ref._get_PWR_25_MHZ()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 0.0) and (r < 10.0),
                             "The PWR_2_GHZ value is not in the allowed "
                             "range. Value=%f" % r)

        self.endTestLog("Test 6: PWR_2_GHZ test")
        pass

    def test07(self):
        '''
        This test verifies the RX_OPT_PWR monitor point is properly
        returned and is in the allowed range [0.025; 1.0]V.
            '''
        self.startTestLog("Test 7: RX_OPT_PWR test")

        ro_prop = self._ref._get_RX_OPT_PWR()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 0.000025) and (r < 0.00025),
                             "The RX_OPT_PWR value is not in the allowed "
                             "range. Value=%f" % r)

        self.endTestLog("Test 7: RX_OPT_PWR test")
        pass

    def test08(self):
        '''
        This test verifies the VDC_12 monitor point is properly
        returned and is in the allowed range [11.5; 12.5]V.
            '''
        self.startTestLog("Test 8: VDC_12 test")

        ro_prop = self._ref._get_VDC_12()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 10.0) and (r < 12.5),
                             "The VDC_12 value is not in the allowed range. "
                             "Value=%f" % r)

        self.endTestLog("Test 8: VDC_12 test")
        pass

    def test09(self):
        '''
        This test verifies the VDC_15 monitor point is properly
        returned and is in the allowed range [14.5; 15.5]V.
            '''
        self.startTestLog("Test 9: VDC_15 test")

        ro_prop = self._ref._get_VDC_15()
        (r, c) = ro_prop.get_sync()
        self.failUnless(c.code == 0 ,
                "Property Monitor Failed");
        self.failUnless((r > 12.0) and (r < 15.5),
                             "The VDC_15 value is not in the allowed range. "
                             "Value=%f" % r)

        self.endTestLog("Test 9: VDC_15 test")
        pass


class interactive(automated):
    def __init__(self, methodName="runTest"):
        automated.__init__(self, methodName)

    def tearDown(self):
        self.out = None

    def log(self, msg):
        print msg


    # --------------------------------------------------
    # Test Definitions
# **************************************************
# MAIN Program
if __name__ == '__main__':
	if len(sys.argv) == 1:
		sys.argv.append("interactive")

	if len(sys.argv) == 2:
		sys.argv.append("DV01")

	antenna = sys.argv[2]
	sys.argv.remove(sys.argv[2])

	unittest.main()
