#!/usr/bin/env python
# $Id$
#
# Copyright (C) 2004
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu
#
"""

Definition of a pyunit (or unittest) test case class for the Local
Oscillator Reference Receiver (LORR) device. .This test will work
correctly only with the LORR simulator. To test the LORR hardware use
the AMBTest (in the same directory as this test).

"""

import unittest
import Devices

class TestCase(unittest.TestCase):
    """Class for implementing LORR device 'unit tests'.
    """
    def test_EFC_25_MHZ(self):
        "Test the LORR EFC_25_MHZ monitor point"
        (val, ts) = lorr.EFC_25_MHZ
        self.assertAlmostEqual(val, 0.13, 2)
        return
    def test_EFC_125_MHZ(self):
        "Test the LORR EFC_2_GHZ monitor point"
        (val, ts) = lorr.EFC_2_GHZ
        self.assertAlmostEqual(val, 5.1, 1)
        return
    def test_PWR_25_MHZ(self):
        "Test the LORR PWR_25_MHZ monitor point"
        (val, ts) = lorr.PWR_25_MHZ
        self.assertAlmostEqual(val, 1.9, 1)
        return
    def test_PWR_125_MHZ(self):
        "Test the LORR PWR_125_MHZ monitor point"
        (val, ts) = lorr.PWR_125_MHZ
        self.assertAlmostEqual(val, 2.5, 1)
        return
    def test_PWR_2_GHZ(self):
        "Test the LORR PWR_2_GHZ monitor point"
        (val, ts) = lorr.PWR_2_GHZ
        self.assertAlmostEqual(val, 3.1, 1)
        return
    def test_RX_OPT_PWR(self):
        "Test the LORR RX_OPT_PWR monitor point"
        (val, ts) = lorr.RX_OPT_PWR
        self.assertAlmostEqual(val, 5.0, 1)
        return
    def test_VDC_12(self):
        "Test the LORR VDC_12 monitor point"
        (val, ts) = lorr.VDC_12
        self.assertAlmostEqual(val, 11.7, 1)
        return
    def test_VDC_15(self):
        "Test the LORR VDC_15 monitor point"
        (val, ts) = lorr.VDC_15
        self.assertAlmostEqual(val, 15.1, 1)
        return
    
from Acspy.Clients.SimpleClient import PySimpleClient

if __name__ == '__main__':
    client = PySimpleClient()
    lorr = Devices.getDeviceByNode('0x22');
    suite = unittest.makeSuite(TestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
    del (lorr)
