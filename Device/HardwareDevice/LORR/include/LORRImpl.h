#ifndef LORRImpl_H
#define LORRImpl_H
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LORRImpl.h
 *
 * $Id$
 */
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


// Base class
#include <LORRS.h>
#include <LORRBase.h>
/* Allows managing alarm state flags not covered by BACI Alarms */
#include <controlAlarmHelper.h>
#include <monitorHelper.h>
#include <RepeatGuard.h>
#include <acstimeTimeUtil.h>


/// Error State Enumeration
enum LORRErrorCondition
{
    DCMUnLocked = 0x001,
    PS12VOutOfRange = 0x002,
    PS15VOutOfRange = 0x004,
    BadOpticalPower = 0x008,
    Bad2GHZSync = 0x010,
    Bad125MHzSync = 0x020,
    TELongFlag = 0x040,
    TEShortFlag = 0x080,
    CommunicationError = 0x100
};


class LORRImpl: public LORRBase, 
                public MonitorHelper,
                public Control::AlarmHelper,
                virtual public POA_Control::LORR
{
    protected:
    /// Writer Thread Class
    class UpdateThread: public ACS::Thread
    {
        public:
        UpdateThread(const ACE_CString& name, LORRImpl& LORR);
        virtual void runLoop();


        protected:
        LORRImpl& lorrDevice_p;
    };


    public:
    friend class LORRImpl::UpdateThread;

    /// Constructor
    LORRImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /// Destructor
    virtual ~LORRImpl();

    // ---------------- Component Lifecycle -----------------------
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();


    protected:
    // ---------------- Hardware Lifecycle Interface ---------------
    virtual void hwInitializeAction();

    virtual void hwOperationalAction();

    virtual void hwStopAction();

    // ------------------ Monitor Helper Methods -------------------
    virtual void processRequestResponse(const AMBRequestStruct& response);


    /* ------------------ Alarm Helper (Error State) Methods ---------------------*/
    virtual void handleActivateAlarm(int newFlag);
    virtual void handleDeactivateAlarm(int newFlag);


    // ---------------- Error State Methods ------------------------
    void updateThreadAction();


    private:
    std::vector<Control::AlarmInformation> createAlarmVector();

    /// Hardware queuing methods
    virtual void queueStatusMonitors();

    /// Hardware Processing Methods
    virtual void processLORRStatusMonitor(
        const MonitorHelper::AMBRequestStruct&);

    /// This semaphore is used to tell when the update thread is really
    /// running and when it is not
    sem_t updateThreadSem_m;

    /// Pointer for the monitoring update thread.
    UpdateThread* updateThread_p;

    /// Time stamp of the last monitor update.
    ACS::Time lastStatusMonitorTime_m;

    /// Repeat guard for avoid logging flooding.
    RepeatGuard guard;

    const ACS::Time updateThreadCycleTime_m;
};
#endif // LORRImpl_H
