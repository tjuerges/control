/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * File LORRCompSimImpl.cpp
 */


#include "LORRCompSimImpl.h"
#include <LORRHWSimImpl.h>
#include <string>
#include <boost/shared_ptr.hpp>
#include <TypeConversion.h>
#include <maciContainerServices.h>
#include <SimulatedSerialNumber.h>


/**
 * Please use this class to implement alternative component, extending
 * the LORRCompSimBase class.
 */


LORRCompSimImpl::LORRCompSimImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    LORRCompSimBase(name, cs)
{
    const std::string fnName("LORRCompSimImpl::LORRCompSimImpl");
    AUTO_TRACE(fnName);
    std::string componentName(name.c_str());
    unsigned long long hashed_sn=AMB::Utils::getSimSerialNumber(componentName,"LORR");
    std::ostringstream msg;
    msg<< "simSerialNumber for  " << name.c_str() << "  with assembly name " << "LORR" << " is " << std::hex << "0x" << hashed_sn << std::endl;
    ACS_LOG(LM_SOURCE_INFO, fnName, (LM_DEBUG, msg.str().c_str()));

    std::vector< CAN::byte_t > sn;
    AMB::node_t node = 0x00;
    AMB::TypeConversion::valueToData(sn,hashed_sn, 8U);
 /*   const std::string fnName("LORRCompSimImpl::LORRCompSimImpl");
    ACS_TRACE(fnName);

	std::vector< CAN::byte_t > sn;
    AMB::TypeConversion::valueToData(sn, 0x000000000023bf03LL, 8U);
	AMB::node_t node(0x00);*/


    simulationObject.reset(new AMB::LORRHWSimImpl(node, sn));
    simulationIf_m.setSimObj(simulationObject.get());
}

LORRCompSimImpl::~LORRCompSimImpl()
{
    const std::string fnName("LORRCompSimImpl::~LORRCompSimImpl");
    ACS_TRACE(fnName);
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LORRCompSimImpl)
