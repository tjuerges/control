#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when          what
# --------  ------------  ----------------------------------------------
# raraya    MAR 04, 2008  created
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for the Local Oscillator Reference Receiver (LORR) device.
"""

import CCL.LORRBase
from CCL import StatusHelper
import os
import math
class LORR(CCL.LORRBase.LORRBase):
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):

        '''
        The constructor creates a LORR object by making use of
        the LORRBase constructor.
        '''

        CCL.LORRBase.LORRBase.__init__(self, antennaName, componentName, stickyFlag)

    def __del__(self):
        CCL.LORRBase.LORRBase.__del__(self)

    def STATUS(self):
        '''
        This method print the Status of the LORR
        '''

        os.system("clear")
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]


            elements = []

            # Device Status
            try:
                value = self._devices[key].GET_STATUS()[0];
            except:
                value = -1;
                tmp = "N/A"
    
            # Byte 0
            if value!=-1:
              if (value[0] & 1):
                 tmp = "locked"
              else:
                 tmp = "NOT LOCKED"
            status1_l = StatusHelper.Line("DCM", [ StatusHelper.ValueUnit(tmp)])
    
            if value!=-1:
              if (value[0] & 2):
                 tmp = "OUT OF RANGE"
              else:
                 tmp = "ok"
            status2_l = StatusHelper.Line("12V", [ StatusHelper.ValueUnit(tmp)])
    
            if value!=-1:
              if (value[0] & 4):
                 tmp = "OUT OF RANGE"
              else:
                 tmp = "ok"
            status3_l = StatusHelper.Line("15V", [ StatusHelper.ValueUnit(tmp)])
    
            if value!=-1:
              if (value[0] & 8):
                 tmp = "INADEQUATE"
              else:
                 tmp = "ok"
            status4_l = StatusHelper.Line("Opt PWR", [ StatusHelper.ValueUnit(tmp)])
    
            if value!=-1:
              if (value[0] & 16):
                 tmp = "DETECTED"
              else:
                 tmp = "ok"
            status5_l = StatusHelper.Line("2Ghz out of Lock flag", [ StatusHelper.ValueUnit(tmp)])
    
            if value!=-1:
              if (value[0] & 32):
                 tmp = "DETECTED"
              else:
                 tmp = "ok"
            status6_l = StatusHelper.Line("125Mhz out of Lock flag", [ StatusHelper.ValueUnit(tmp)])
             
              # Byte 1
            if value!=-1:
              if (value[1] & 2):
                 tmp = "DETECTED"
              else:
                 tmp = "ok"
            status7_l = StatusHelper.Line("TE short flag", [ StatusHelper.ValueUnit(tmp)])
    
            if value!=-1:
              if (value[1] & 4):
                 tmp = "DETECTED"
              else:
                 tmp = "ok"
            status8_l = StatusHelper.Line("TE long flag", [ StatusHelper.ValueUnit(tmp)])
    
   


 
            ################################### Reference Signals ########################################    
            elements.append( StatusHelper.Separator("Reference Signals") )
    
            # TE_LENGTH
            try:
                value = self._devices[key].GET_TE_LENGTH()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1:
              tmp = value
            elements.append( StatusHelper.Line("TE LENGTH", [ StatusHelper.ValueUnit(tmp)]) )

    
            # TE_OFFSET_COUNTER
            try:
                value = self._devices[key].GET_TE_OFFSET_COUNTER()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1:
              tmp = value
            elements.append( StatusHelper.Line("TE OFFSET COUNTER", [ StatusHelper.ValueUnit(tmp,)]) )


            ################################### Operative Data ########################################    
            elements.append( StatusHelper.Separator("Operative Data") )

    
            # DCM Status
            elements.append( status1_l ) 

            # 12V Status
            elements.append( status2_l )

            # 15V Status
            elements.append( status3_l )

            # Opticat Power Status
            elements.append( status4_l )

            # 2Ghz out of Lock Flag
            elements.append( status5_l )

            # 125Mhz out of Lock Flag
            elements.append( status6_l )

            # TE short Flag
            elements.append( status7_l )

            # TE long Flag
            elements.append( status8_l )


            # EFC_125_MHZ
            try:
                value = self._devices[key].GET_EFC_125_MHZ()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1: tmp = "%+.3f"%value
            elements.append( StatusHelper.Line("EFC 125MHZ", [ StatusHelper.ValueUnit(tmp,"V")]) )
    
    
            # EFC_COMB_LINE_PLL
            try:
                value = self._devices[key].GET_EFC_COMB_LINE_PLL()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1: tmp = "%+.3f"%value
            elements.append( StatusHelper.Line("EFC COMB LINE PLL", [ StatusHelper.ValueUnit(tmp,"V")]) )
    
    
            # PWR_125_MHZ
            try:
                value = self._devices[key].GET_PWR_125_MHZ()[0];
            except:
                value = -1;
                tmp = "N/A"
                tmp_dbm = "N/A"
            if value!=-1: 
                tmp = value
                tmp_dbm = 10*math.log10(((tmp**2)*1000)/50.0)
                tmp_dbm = "%3.2f"%tmp_dbm 
            elements.append( StatusHelper.Line("PWR 125MHZ", [ StatusHelper.ValueUnit(tmp,"V"),StatusHelper.ValueUnit(tmp_dbm,"dBm")]) )
    
    
            # PWR_25_MHZ
            try:
                value = self._devices[key].GET_PWR_25_MHZ()[0];
            except:
                value = -1;
                tmp = "N/A"
                tmp_dbm = "N/A"
            if value!=-1: 
                tmp = value
                tmp_dbm = 10*math.log10(((tmp**2)*1000)/50.0)
                tmp_dbm = "%3.2f"%tmp_dbm 
            elements.append( StatusHelper.Line("PWR 25MHZ", [ StatusHelper.ValueUnit(tmp,"V"),StatusHelper.ValueUnit(tmp_dbm,"dBm")]) )
    
    
            # PWR_2_GHZ
            try:
                value = self._devices[key].GET_PWR_2_GHZ()[0];
            except:
                value = -1;
                tmp = "N/A"
                tmp_dbm = "N/A"
            if value!=-1:
                tmp = value
                tmp_dbm = 10*math.log10(((tmp**2)*1000)/50.0)
                tmp_dbm = "%3.2f"%tmp_dbm 
            elements.append( StatusHelper.Line("PWR 2GHZ", [ StatusHelper.ValueUnit(tmp,"V"),StatusHelper.ValueUnit(tmp_dbm,"dBm")]) )
   
 
            # RX_OPT_PWR
            try:
                value = self._devices[key].GET_RX_OPT_PWR()[0];
            except:
                value = -1;
                tmp = "N/A"
                tmp_dbm = "N/A"
            if value!=-1:
              tmp = value
              tmp_dbm = math.log10((tmp * 1000))*10
              tmp_dbm = "%3.2f"%tmp_dbm
            elements.append( StatusHelper.Line("RX OPT PWR", [ StatusHelper.ValueUnit(tmp,"W"),StatusHelper.ValueUnit(tmp_dbm,"dBm")] ) )

    
            # VDC_12
            try:
                value = self._devices[key].GET_VDC_12()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1:
              tmp = value
            elements.append( StatusHelper.Line("VDC 12", [ StatusHelper.ValueUnit(tmp,"V")]) )

    
            # VDC_15
            try:
                value = self._devices[key].GET_VDC_15()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1:
              tmp = value
            elements.append( StatusHelper.Line("VDC 15", [ StatusHelper.ValueUnit(tmp,"V")]) )

    
            # VDC_7
            try:
                value = self._devices[key].GET_VDC_7()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1:
              tmp = value
            elements.append( StatusHelper.Line("VDC 7", [ StatusHelper.ValueUnit(tmp,"V")]) )

    
            # VDC_MINUS_7
            try:
                value = self._devices[key].GET_VDC_MINUS_7()[0];
            except:
                value = -1;
                tmp = "N/A"
            if value!=-1:
              tmp = value
            elements.append( StatusHelper.Line("VDC -7", [ StatusHelper.ValueUnit(tmp,"V")]) )


            ################################### Firmware ########################################    
            elements.append( StatusHelper.Separator("Firmware") )

            # READ_MODULE_CODES
            try:
                value = self._devices[key].GET_READ_MODULE_CODES()[0];
                value[0] = "%.2x"%value[0]
                value[1] = "%.2x"%value[1]
                value[2] = str(value[2]>>4)+'.'+str(((value[2]<<4)&0xFF)>>4) #Revision
                value[3] = '20'+"%.2d"%value[5]+'-'+"%.2d"%value[4]+'-'+"%.2d"%value[3]
                value[4] = value[6]<<8 | value[7]
            except:
                value = ["N/A"]*5

            elements.append( StatusHelper.Line("Module Codes", [ StatusHelper.ValueUnit(value[0], label="code"),
                                                                 StatusHelper.ValueUnit(value[1], label=" IFMC"),
                                                                 StatusHelper.ValueUnit(value[2], label=" rev"),
                                                                 StatusHelper.ValueUnit(value[3], label=" date"),
                                                                 StatusHelper.ValueUnit(value[4], label=" SN") ]) )


 
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName,  elements )
            statusFrame.printScreen()

