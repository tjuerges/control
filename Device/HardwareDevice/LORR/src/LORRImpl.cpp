/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LORRImpl.cpp
 *
 * $Id$
 */


#include <LORRImpl.h>
#include <memory> // For std::auto_ptr.
#include <TETimeUtil.h>
#include <acstimeTimeUtil.h>

using Control::AlarmInformation;

/**
 *-----------------------
 * LORR Constructor
 *-----------------------
 */
LORRImpl::LORRImpl(const ACE_CString& name, maci::ContainerServices* cs):
    LORRBase(name, cs),
    MonitorHelper(),
    guard(10000000ULL * 10ULL, 0),
    updateThreadCycleTime_m(5 * TETimeUtil::TE_PERIOD_ACS)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

/**
 *-----------------------
 * LORR Destructor
 *-----------------------
 */
LORRImpl::~LORRImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    sem_destroy( &updateThreadSem_m);
}

///////////////////////////////////////
// Additional methods for LORR
///////////////////////////////////////

/**
 *------------------------
 * Component Lifecycle Methods
 *------------------------
 */
void LORRImpl::initialize()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());

    try
    {
        LORRBase::initialize();
        MonitorHelper::initialize(compName.in(), getContainerServices());
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    }

    if(sem_init(&updateThreadSem_m, 0, 1))
    {
        std::ostringstream msg;
        msg << "Error initialising semaphore";
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__,__LINE__,
            __PRETTY_FUNCTION__);
        ex.log();
        throw ex;
    }

    std::string threadName(compName.in());
    threadName += "WriterThread";
    updateThread_p = getContainerServices()->getThreadManager()-> create<
        UpdateThread, LORRImpl >(threadName.c_str(), *this);
    updateThread_p->setSleepTime(updateThreadCycleTime_m);
    //register alarms.
    initializeAlarms("LORR", compName.in(), createAlarmVector());
}

void LORRImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Shutdown the threads, the threads were suspended by the
    // baseclass call to hwStopAction so we just need to terminate them
    // here.
    updateThread_p->terminate();

    try
    {
        MonitorHelper::cleanUp();
        LORRBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__,__LINE__,
            __PRETTY_FUNCTION__);
    }

    forceTerminateAllAlarms();
}

// ---------------- Hardware Lifecycle Methods --------------
void LORRImpl::hwInitializeAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LORRBase::hwInitializeAction();

    MonitorHelper::resume();
    updateThread_p ->suspend();

    // Clear all the alarms
    forceTerminateAllAlarms();

}

void LORRImpl::hwOperationalAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LORRBase::hwOperationalAction();

    acstime::Epoch startTime(TETimeUtil::unix2epoch(time(0)));
    lastStatusMonitorTime_m = TETimeUtil::rtMonitorTime(startTime, 5).value;

    updateThread_p->resume();
}

void LORRImpl::hwStopAction()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushTime(0ULL);

    // Suspend the threads
    updateThread_p->suspend();

    // Flush all commands
    flushNode(0, &flushTime, &flushStatus);
    if(flushStatus != AMBERR_NOERR)
    {
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "Communication failure "
            "flushing commands to device"));
    }
    else
    {
        ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG,"All commands and monitors "
            "flushed at: %lld",flushTime));
    }

    MonitorHelper::suspend();
    LORRBase::hwStopAction();
    forceTerminateAllAlarms();
}

// ================ Monitoring Dispatch Method ================
void LORRImpl::processRequestResponse(const AMBRequestStruct& response)
{

    if(response.Status == AMBERR_FLUSHED)
    {
        // Nothing to do, just return
        return;
    }

    if(response.RCA == getMonitorRCAStatus())
    {
        // Response from LORR Status Monitor
        processLORRStatusMonitor(response);
        return;
    }
}

// ================ Error Handling Methods ==================
void LORRImpl::handleActivateAlarm(int newErrorFlag)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    LORRBase::setError(createErrorMessage());

}

void LORRImpl::handleDeactivateAlarm(int  newErrorFlag)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (isAlarmSet()) {
        LORRBase::setError(createErrorMessage());
    } else {
        LORRBase::clearError();
    }
}

std::vector<AlarmInformation> LORRImpl::createAlarmVector()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    std::vector<AlarmInformation> aivector;
    {
        AlarmInformation ai;
        ai.alarmCode = DCMUnLocked;
        ai.alarmDescription = "The DCM is UNlocked";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = PS12VOutOfRange;
        ai.alarmDescription = "12V power supply is out of range.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = PS15VOutOfRange;
        ai.alarmDescription = "15V power supply is out of range.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = BadOpticalPower;
        ai.alarmDescription = "Inadequate received optical power.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = Bad2GHZSync;
        ai.alarmDescription = "LORR is having trouble synchronizing.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = Bad125MHzSync;
        ai.alarmDescription = "LORR is having trouble synchronizing.";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = TEShortFlag;
        ai.alarmDescription = "Fewer TE than expected on the composite";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = TELongFlag;
        ai.alarmDescription = "More TE than expected on the composite";
        aivector.push_back(ai);
    }
    {
        AlarmInformation ai;
        ai.alarmCode = CommunicationError;
        ai.alarmDescription = "CAN BUS Communication Error.";
        aivector.push_back(ai);
    }
    return aivector;
}

// ================ Hardware Queueing Methods  ================= */

// Not TE related
void LORRImpl::queueStatusMonitors()
{
    MonitorHelper::AMBRequestStruct* monReq(0);

    while((TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value
        + (updateThreadCycleTime_m * 2)) > lastStatusMonitorTime_m)
    {
        lastStatusMonitorTime_m += TETimeUtil::TE_PERIOD_ACS;

        monReq = getRequestStruct();

        monReq->RCA = getMonitorRCAStatus();
        monReq->TargetTime = lastStatusMonitorTime_m;

        monitorTE(monReq->TargetTime, monReq->RCA, monReq->DataLength,
            monReq->Data, monReq->SynchLock, &(monReq->Timestamp),
            &(monReq->Status));

        queueRequest(monReq);
    }
}

// ================ Hardware Processing Methods =================
void LORRImpl::processLORRStatusMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{

    // Check Status
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_WARNING, "LORR Status Monitor "
                "Failed (Status = %d)", response.Status));
        }

        activateAlarm(CommunicationError);
    }
    else
    {
        deactivateAlarm(CommunicationError);
    }

    // Check Time
    if((response.Timestamp - response.TargetTime)
        > (TETimeUtil::TE_PERIOD_ACS / 2))
    {
        if(guard.checkAndIncrement() == true)
        {
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_DEBUG,"LORR Status Monitor at "
                "incorrect time ( %f s off)",
                (response.Timestamp - response.TargetTime) * 1E-7));
        }
    }

    // Byte 1
    if((response.Data[0] & 0x01) == 0x00)
    {
        activateAlarm(DCMUnLocked);
    }
    else
    {
        deactivateAlarm(DCMUnLocked);
    }

    if((response.Data[0] & 0x02) == 0x02)
    {
        activateAlarm(PS12VOutOfRange);
    }
    else
    {
        deactivateAlarm(PS12VOutOfRange);
    }

    if((response.Data[0] & 0x04) == 0x04)
    {
        activateAlarm(PS15VOutOfRange);
    }
    else
    {
        deactivateAlarm(PS15VOutOfRange);
    }

    if((response.Data[0] & 0x08) == 0x08)
    {
        activateAlarm(BadOpticalPower);
    }
    else
    {
        deactivateAlarm(BadOpticalPower);
    }

    if((response.Data[0] & 0x10) != 0x00)
    {
        activateAlarm(Bad2GHZSync);
    }
    else
    {
        deactivateAlarm(Bad2GHZSync);
    }

    if((response.Data[0] & 0x20) != 0x00)
    {
        activateAlarm(Bad125MHzSync);
    }
    else
    {
        deactivateAlarm(Bad125MHzSync);
    }

    // Byte 2
    if((response.Data[1] & 0x02) == 0x02)
    {
        activateAlarm(TEShortFlag);
        ///
        /// TODO
        /// Thomas, Mar 3, 2009
        /// FIX: queueClearTEFlags();
    }
    else
    {
        deactivateAlarm(TEShortFlag);
    }

    if((response.Data[1] & 0x04) == 0x04)
    {
        activateAlarm(TELongFlag);
        ///
        /// TODO
        /// Thomas, Mar 3, 2009
        /// FIX: queueClearTEFlags();
    }
    else
    {
        deactivateAlarm(TELongFlag);
    }
}


// ===================  Threads   =========================
void LORRImpl::updateThreadAction()
{
    // Queue Status Monitor
    queueStatusMonitors();
}

LORRImpl::UpdateThread::UpdateThread(const ACE_CString& name, LORRImpl& LORR) :
    ACS::Thread(name), lorrDevice_p(LORR)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void LORRImpl::UpdateThread::runLoop()
{
    lorrDevice_p.updateThreadAction();
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(LORRImpl)
