/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File LORRHWSimImpl.cpp
 *
 * $Id$
 */


#include "LORRHWSimImpl.h"
#include <vector>
#include <Basic_Types.h>
#include <TypeConversion.h>
#include <ControlExceptions.h>


/**
 * Please use this class to implement complex functionality for the
 * LORRHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions.
 */


AMB::LORRHWSimImpl::LORRHWSimImpl(node_t node,
    const std::vector< CAN::byte_t >& serialNumber):
    LORRHWSimBase::LORRHWSimBase(node, serialNumber)
{
    const std::string fnName("AMB::LORRHWSimImpl::LORRHWSimImpl");
    AUTO_TRACE(fnName);

    // Set up of simulated values.
    std::vector< CAN::byte_t > data;
    AMB::TypeConversion::valueToData(data,
        ACE_UINT64(0x5540000202021234ULL));
    try
    {
        monitorSetter(AMB::LORRHWSimBase::monitorPoint_READ_MODULE_CODES, data);
    }
    catch(const CAN::Error& ex)
    {
        std::ostringstream msg;
        msg << "Tried to set up the RCA monitorPoint_READ_MODULE_CODES for simulation "
            "but an exception was thrown:\n"
            << ex
            << "\nContinuing with normal operation.";
        ControlExceptions::IllegalParameterErrorExImpl nex(__FILE__, __LINE__,
            fnName.c_str());
        nex.addData("Detail", msg.str());
        nex.log();
    }
}

AMB::LORRHWSimImpl::~LORRHWSimImpl()
{
    const std::string fnName("AMB::LORRHWSimImpl::~LORRHWSimImpl");
    AUTO_TRACE(fnName);
}
