/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LORR;

import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Implement the ChessboardDetailsPluginFactory interface.  This creates a class that supplies a class that 
 * provides the status of this device in each antenna to a OmcPlugin for display to the user.  
 *
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington  sharring@nrao.edu
 * @since   ALMA 5.0.3
 */
public class DetailsPluginFactory implements ChessboardDetailsPluginFactory {

    /**
     * @see alma.common.gui.chessboard.ChessboardDetailsPluginFactory
     */
    public ChessboardDetailsPlugin[] instantiateDetailsPlugin(
        String[] names,
        PluginContainerServices containerServices,
        ChessboardDetailsPluginListener listener, 
        String UID,
        String selection
    ) {
        ChessboardDetailsPlugin[] detailsPlugin = new ChessboardDetailsPlugin[INSTANCES];
        if (null != names && names.length > 0) {
            detailsPlugin[0] = new DeviceChildPlugin(
                names, 
                listener, 
                containerServices, 
                UID, 
                selection
            );
        }
        return detailsPlugin;
    }
    
    private static final int INSTANCES = 1;
}

//
// O_o
        
