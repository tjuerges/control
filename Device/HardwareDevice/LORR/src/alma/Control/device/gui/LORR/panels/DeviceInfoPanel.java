/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LORR.panels;

import alma.Control.device.gui.LORR.IcdControlPoints;
import alma.Control.device.gui.LORR.IcdMonitorPoints;
import alma.Control.device.gui.LORR.IdlMonitorPoints;
import alma.Control.device.gui.LORR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.panels.AmbsiPanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel contains information on the LORR module. It loads the Ambsi Panel and the Version Info Panel.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 5.0.3
 */

public class DeviceInfoPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DeviceInfoPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(ambsiPanel, c);
        c.gridx++;
//TODO: create a version info panel, then add it.
//        add(versionInfoPanel, c);

        setVisible(true);
    }
    
    /**
     * It is necessary to build all sub-panels before buildPanel() is called.
     * 
     * @param logger to be used by the sub-panels.
     * @param dPM the device presentation model for the device containing the panel.
     */
    private void buildSubPanels(Logger logger, DevicePM dPM) {
        ambsiPanel = new AmbsiPanel(logger, dPM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.SERIAL_NUMBER),
                devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.TRANS_NUM),
                devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_SLAVE_ERROR_CODE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.CAN_ERROR_COUNT),
                devicePM.getMonitorPointPM(IcdMonitorPoints.ERROR_CODE_LAST_CAN_ERROR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PATCH_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_PATCH_LEVEL),
                devicePM.getControlPointPM(IcdControlPoints.RESET_AMBSI));
//        versionInfoPanel = new VersionInfoPanel(logger, dPM);
    }

    private AmbsiPanel ambsiPanel;
//    private VersionInfoPanel versionInfoPanel;
}

//
// O_o

