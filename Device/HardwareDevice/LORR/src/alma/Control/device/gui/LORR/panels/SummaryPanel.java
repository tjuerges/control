/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LORR.panels;

import alma.Control.device.gui.LORR.presentationModels.DevicePM;
import alma.Control.device.gui.LORR.IcdMonitorPoints;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to keep
 * related data together and separate unrelated data.
 * 
 * This class contains the Summary data.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public SummaryPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(
                devicePM.getDeviceDescription()),BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 100;
        c.weighty = 100;
        c.gridx = 1;
        c.gridy = 0;
        addLeftLabel("DCM Lock", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.DCM_LOCKED),
                Status.FAILURE, "Error: DCM is out of lock",
                Status.GOOD, "DCM locked"), c);
        c.gridy++;
        addLeftLabel("Optical Power", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.OPTICAL_POWER_OFF),
                Status.FAILURE, "Error: inadequate optical power received",
                Status.GOOD, "Optical power OK"), c);
        c.gridx+=2;
        c.gridy=0;
        addLeftLabel("12 Volt", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.POWER_SUPPLY_12V_NOT_OK),
                Status.FAILURE, "12V power error",
                Status.GOOD, "12V power OK"), c);
        c.gridy++;
        addLeftLabel("15 Volt", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.POWER_SUPPLY_15V_NOT_OK),
                Status.FAILURE, "15 Volt power error",
                Status.GOOD, "15V power OK"), c);
        c.gridx+=2;
        c.gridy=0;
        addLeftLabel("2GHz PLL", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.OUTPUT_2GHZ_LOCKED),
                Status.FAILURE, "2GHz PLL out of lock",
                Status.GOOD, "2GHz PLL locked"), c);
        c.gridy++;
        addLeftLabel("125MHz PLL", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.OUTPUT_125MHZ_LOCKED),
                Status.FAILURE, "125MHz PLL out of lock",
                Status.GOOD, "125MHz PLL locked"), c);
        c.gridx+=2;
        c.gridy=0;
        addLeftLabel("Too Few Hz/TE", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.TE_SHORT_FLAG_SET),
                Status.FAILURE, "Error: there are fewer than 6000000 cycles per TE",
                Status.GOOD, "There are at least 6000000 cycles per TE"), c);
        c.gridy++;
        addLeftLabel("Too Many Hz/TE", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.TE_LONG_FLAG_SET),
                Status.FAILURE, "Error: there are more than 6000000 cycles per TE",
                Status.GOOD, "There are no more than 6000000 cycles per TE"), c);

        this.setVisible(true);
    }
    protected void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        c.anchor=GridBagConstraints.EAST;
        add(new JLabel(s), c);
        c.anchor=GridBagConstraints.WEST;
        c.gridx++;
    }
}
//
// O_o
