/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LORR.panels;

import alma.Control.device.gui.LORR.IcdControlPoints;
import alma.Control.device.gui.LORR.IcdMonitorPoints;
import alma.Control.device.gui.LORR.presentationModels.DevicePM;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel contains most of the information on the LORR. The device info panels go in the device info tab,
 * and the status bits should be displayed in the status tab.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class StatusPanel extends DevicePanel {

    public StatusPanel(Logger logger, DevicePM devicePM) {
        super(logger, devicePM);
        buildSubPanels();
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
//        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        c.gridheight=2;
        add(powerPanel, c);
        c.gridheight=1;
        c.gridy+=2;
        add(voltagePanel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(tePanel, c);
        c.gridy++;
        add(efcPanel, c);
        c.gridy++;
        add(controlPanel, c);

        setVisible(true);
    }
    
    private void buildSubPanels(){
        controlPanel = new ControlPanel(logger, devicePM);
        powerPanel = new PowerPanel(logger, devicePM);
        tePanel = new TePanel(logger, devicePM);
        voltagePanel = new VoltagePanel(logger, devicePM);;
        efcPanel = new EFCPanel(logger, devicePM);
    }

    /*
     * This panel contains all the power readouts from the LORR.
     */
    private class PowerPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public PowerPanel(Logger logger, DevicePresentationModel devicePM) {
            super(logger, devicePM);
            buildPanel();
        }

        protected void buildPanel() {

            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("I/O Power"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            addLeftLabel("25 MHz:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.PWR_25_MHZ),
                    true, 4), c); 
            c.gridy++;
            addLeftLabel("125 MHz:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.PWR_125_MHZ),
                    true, 4), c); 
            c.gridy++;
            addLeftLabel("2 GHz:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.PWR_2_GHZ),
                    true, 4), c); 
            c.gridy++;
            addLeftLabel("Optical in:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.RX_OPT_PWR),
                    true, 4), c); 
        }
    }

    /*
     * This sub-pane contains the voltage readouts.
     */
    private class VoltagePanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public VoltagePanel(Logger logger, DevicePresentationModel devicePM) {
            super(logger, devicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Voltage"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            addLeftLabel("7 VDC:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.VDC_7),
                    true, 4), c); 
            c.gridy++;
            addLeftLabel("12 VDC:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.VDC_12),
                    true, 4), c); 
            c.gridy++;
            addLeftLabel("15 VDC:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.VDC_15),
                    true, 4), c); 
            c.gridy++;
            addLeftLabel("-7 VDC:", c);
            add (new FloatMonitorPointWidget(logger, devicePM.getMonitorPointPM(IcdMonitorPoints.VDC_MINUS_7),
                    true, 4), c); 
        }
    }

    /*
     * This sub-panel contains the TE related monitor points.
     */
    private class TePanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public TePanel(Logger logger, DevicePresentationModel devicePM) {
            super(logger, devicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("TE Status"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("TE Length:", c);
            add (new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TE_LENGTH)), c); 
            c.gridy++;
            addLeftLabel("TE Offset Counter:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TE_OFFSET_COUNTER), true, 4), c); 
        }
    }
    
    /*
     * This sub-panel contains the EFC (Electronic Frequency Control) related monitor points.
     */
    private class EFCPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public EFCPanel(Logger logger, DevicePresentationModel devicePM) {
            super(logger, devicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("EFC Status"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("125 MHz:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.EFC_125_MHZ), true, 4), c); 
            c.gridy++;
            addLeftLabel("Line PLL:", c);
            add (new FloatMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.EFC_COMB_LINE_PLL), true, 4), c); 
        }
    }

    /*
     * This sub-panel contains the 3 control buttons.
     */
    private class ControlPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public ControlPanel(Logger logger, DevicePresentationModel devicePM) {
            super(logger, devicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            c.weightx=1;
            c.weighty=1;

            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.CLEAR_FLAGS), "Clear Flags", 0x01), c);
            c.gridy++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.RESYNC_TE), "Resync TE", 0x01), c);
            c.gridy++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.FPGA_LOGIC_RESET), "Reset FPGAs"), c);
        }
    }

    private ControlPanel controlPanel;
    private PowerPanel powerPanel;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private TePanel tePanel;
    private VoltagePanel voltagePanel;
    private EFCPanel efcPanel;

}

//
// O_o
