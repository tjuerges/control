/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.LORR.panels;

import alma.Control.device.gui.LORR.IdlMonitorPoints;
import alma.Control.device.gui.LORR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Detail data.
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DetailsPanel extends DevicePanel {
    
    public DetailsPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        customizeMonitorPoints();
        buildSubPanels(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"),
                        BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weighty=1;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(new ShowHideButton(), c);
        c.weighty=100;
        tabs = new JTabbedPane();
        tabs.add("Status", new JScrollPane(statusPanel));
        tabs.add("Device Info", new JScrollPane(deviceInfoPanel));
        c.gridy++;
        add(tabs, c);
        this.setVisible(true);
    }
    
    /**
     * It is necessary to build all sub-panels before buildPanel() is called.
     * 
     * @param logger to be used by the sub-panels.
     * @param dPM the device presentation model for the device containing the panel.
     */
    private void buildSubPanels(Logger logger, DevicePM dPM) {
        deviceInfoPanel = new DeviceInfoPanel(logger, dPM);
        statusPanel = new StatusPanel(logger, dPM);
    }
    
    /**
     * This function sets any customizations needed for displaying the monitor data in a user friendly format.
     * E.g degree C instead of K.
     */
    private void customizeMonitorPoints() {
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayUnits("\u00B0C");
    }
    
    private class ShowHideButton
    extends JButton
    implements ActionListener {

        private String hideActionCommand;
        private String hideButtonText;
        private String showActionCommand;
        private String showButtonText;

        public ShowHideButton() {
            hideButtonText = "Hide Details";
            showButtonText = "Show Details";
            hideActionCommand = "HIDE_DETAILS";
            showActionCommand = "SHOW_DETAILS";

            setText(hideButtonText);
            setActionCommand(hideActionCommand);
            addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == hideActionCommand) {
                tabs.setVisible(false);
                setText(showButtonText);
                setActionCommand(showActionCommand);
            } else if (e.getActionCommand() == showActionCommand) {
                tabs.setVisible(true);
                setText(hideButtonText);
                setActionCommand(hideActionCommand);
            }
        }
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

    }

    private DeviceInfoPanel deviceInfoPanel;
    private StatusPanel statusPanel;
    private JTabbedPane tabs;

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
