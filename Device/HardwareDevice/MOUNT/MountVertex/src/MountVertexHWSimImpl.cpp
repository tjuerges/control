/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MountVertexHWSimImpl.cpp
 */

#include "MountVertexHWSimImpl.h"
using namespace AMB;
//using namespace VERTEX;

const double MountVertexHWSimImpl::bitsPerDegree = (static_cast<double>(0x40000000) / 180.0);
const double MountVertexHWSimImpl::degreesPerBit = (180.0 / static_cast<double>(0x40000000));
const double MountVertexHWSimImpl::encoderOffset = 0.0;

MountVertexHWSimImpl::MountVertexHWSimImpl(node_t node, 
					   const std::vector<CAN::byte_t> &serialNumber,
					   int antenna,
					   SHARED_SIMULATOR::SharedSimulator_var ss)
  : MountVertexHWSimBase::MountVertexHWSimBase(node, serialNumber),
    ssi_m(NULL)
{
  azTransf_m.initialize(bitsPerDegree, degreesPerBit, encoderOffset);
  elTransf_m.initialize(bitsPerDegree, degreesPerBit, encoderOffset);
  ss_m = ss;
  if(!CORBA::is_nil(ss.in()))
    ssi_m = new SharedSimWrapper(antenna, ss_m);
  drive_m.setSharedSimWrapper(ssi_m);
}

MountVertexHWSimImpl::~MountVertexHWSimImpl()
{
  if (!ssi_m) delete ssi_m;
}

void MountVertexHWSimImpl::start()
{
 drive_m.start();
}

void MountVertexHWSimImpl::stop()
{
 drive_m.stop();
}

void MountVertexHWSimImpl::setControlSetAls(const std::vector<CAN::byte_t>& data) 
{
  const char* __METHOD__ = "MountVertexHWSimImpl::MountVertexHWSimImpl";
  ACS_TRACE(__METHOD__);
  MountVertexHWSimBase::setControlSetAls(data);
  
  std::vector<CAN::byte_t> data_tmp;
  std::vector<CAN::byte_t> status; 
  std::vector<CAN::byte_t> currentStatus; 
  currentStatus = getMonitorAlsStatus();
  
  unsigned char val;
  char byte0 = currentStatus[0];
  char byte1 = currentStatus[1];
  
  data_tmp = data;
  
  /* 
   * update state for monitorPoint_ALS_STATUS
   */
  val = AMB::dataToChar(data_tmp);
  
  bool az = static_cast<bool>(val & 0x1);
  bool el = static_cast<bool>((val & 0x2)>>1);
  
  if(az)
    byte0 = 0x10;
  if(el)
    byte1 = 0x10;
  
  status.push_back(byte0);
  status.push_back(byte1);
  setMonitorAlsStatus(status);	
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAcuModeRsp() const {

  std::vector<char> mode = drive_m.getACUMode();
  (state_m.find(monitorPoint_ACU_MODE_RSP)->second)->clear();
  for(unsigned short index=0; index<static_cast<unsigned short>(mode.size()); index++)
    (state_m.find(monitorPoint_ACU_MODE_RSP)->second)->push_back(static_cast<CAN::byte_t>(mode[index]));
  return MountVertexHWSimBase::getMonitorAcuModeRsp();
}

void MountVertexHWSimImpl::setControlAcuModeCmd(const std::vector<CAN::byte_t>& data)
{

  MountVertexHWSimBase::setControlAcuModeCmd(data);
  drive_m.setACUMode(static_cast<unsigned char>(data[0]));
}

void MountVertexHWSimImpl::setMonitorAcuModeRsp(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setMonitorAcuModeRsp(data);
  std::vector<unsigned char> mode;
  for(unsigned short index=0; index<static_cast<unsigned short>(data.size()); index++)
    mode.push_back(static_cast<unsigned char>(data[index]));
  drive_m.setACUMode(mode[0]);
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzPosnRsp() const
{
  double pos, vel;
  long raw;
  drive_m.getAzPsn(pos,vel);
  azTransf_m.degreesToBits(pos,raw);
  unsigned long long position = static_cast<unsigned long long>(raw);
  position += (position << 32);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, position);
  associate(monitorPoint_AZ_POSN_RSP, data);
  return MountVertexHWSimBase::getMonitorAzPosnRsp();
} 

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElPosnRsp() const
{
  double pos, vel;
  long raw;
  drive_m.getElPsn(pos,vel);
  elTransf_m.degreesToBits(pos,raw);
  unsigned long long position = static_cast<unsigned long long>(raw);
  position += (position << 32);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, position);
  associate(monitorPoint_EL_POSN_RSP, data);
  return MountVertexHWSimBase::getMonitorElPosnRsp();
} 

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzEnc() const
{
  double pos, vel;
  long raw;
  drive_m.getAzPsn(pos,vel);
  azTransf_m.degreesToEncoder(pos,raw);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_ENC, data);
  return MountVertexHWSimBase::getMonitorAzEnc();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElEnc() const
{
  double pos, vel;
  long raw;
  drive_m.getElPsn(pos,vel);
  elTransf_m.degreesToEncoder(pos,raw);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_ENC, data);
  return MountVertexHWSimBase::getMonitorElEnc();
}

void MountVertexHWSimImpl::setControlAzTrajCmd(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlAzTrajCmd(data);
  std::vector<CAN::byte_t> pos;
  std::vector<CAN::byte_t> vel;

  const unsigned short longSize = sizeof(long);
  for(unsigned short index=0; index <longSize; index++)
    {
      pos.push_back(data[index]);
      vel.push_back(data[index+longSize]);
    }
  long rawPos, rawVel;
  double position, velocity;
  AMB::TypeConversion::dataToValue(pos, rawPos);
  AMB::TypeConversion::dataToValue(vel, rawVel);
  azTransf_m.bitsToDegrees(position,rawPos);
  azTransf_m.bitsToDegrees(velocity,rawVel);
  drive_m.setAzPsn(position,velocity);
}

void MountVertexHWSimImpl::setControlElTrajCmd(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlElTrajCmd(data);
  std::vector<CAN::byte_t> pos;
  std::vector<CAN::byte_t> vel;

  const unsigned short longSize = sizeof(long);
  for(unsigned short index=0; index <longSize; index++)
    {
      pos.push_back(data[index]);
      vel.push_back(data[index+longSize]);
    }
  long rawPos, rawVel;
  double position, velocity;
  AMB::TypeConversion::dataToValue(pos, rawPos);
  AMB::TypeConversion::dataToValue(vel, rawVel);
  elTransf_m.bitsToDegrees(position,rawPos);
  elTransf_m.bitsToDegrees(velocity,rawVel);
  drive_m.setElPsn(position,velocity);
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzBrake() const
{
  CAN::byte_t raw = static_cast<CAN::byte_t>(drive_m.Az->get_brake());
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_BRAKE, data);
  return MountVertexHWSimBase::getMonitorAzBrake();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElBrake() const
{
  CAN::byte_t raw = static_cast<CAN::byte_t>(drive_m.El->get_brake());
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_BRAKE, data);
  return MountVertexHWSimBase::getMonitorElBrake();
}

void MountVertexHWSimImpl::setControlSetAzBrake(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzBrake(data);
  unsigned char raw;
  AMB::TypeConversion::dataToValue(data, raw);
  drive_m.Az->set_brake(static_cast<AZEL_Brake_t>(raw));
}

void MountVertexHWSimImpl::setControlSetElBrake(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElBrake(data);
  unsigned char raw;
  AMB::TypeConversion::dataToValue(data, raw);
  drive_m.El->set_brake(static_cast<AZEL_Brake_t>(raw));
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff0() const
{
  double raw = drive_m.Az->servo_coefficients[0];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_0, data); 
  return MountVertexHWSimBase::getMonitorAzServoCoeff0();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff1() const
{
  double raw = drive_m.Az->servo_coefficients[1];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_1, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff1();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff2() const
{
  double raw = drive_m.Az->servo_coefficients[2];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_2, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff2();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff3() const
{
  double raw = drive_m.Az->servo_coefficients[3];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_3, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff3();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff4() const
{
  double raw = drive_m.Az->servo_coefficients[4];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_4, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff4();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff5() const
{
  double raw = drive_m.Az->servo_coefficients[5];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_5, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff5();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff6() const
{
  double raw = drive_m.Az->servo_coefficients[6];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_6, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff6();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff7() const
{
  double raw = drive_m.Az->servo_coefficients[7];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_7, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff7();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff8() const
{
  double raw = drive_m.Az->servo_coefficients[8];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_8, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff8();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeff9() const
{
  double raw = drive_m.Az->servo_coefficients[9];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_9, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeff9();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeffA() const
{
  double raw = drive_m.Az->servo_coefficients[10];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_A,data);
  return MountVertexHWSimBase::getMonitorAzServoCoeffA();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeffB() const
{
  double raw = drive_m.Az->servo_coefficients[11];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_B, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeffB();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorAzServoCoeffC() const
{
  double raw = drive_m.Az->servo_coefficients[12];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_SERVO_COEFF_C, data);
  return MountVertexHWSimBase::getMonitorAzServoCoeffC();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff0() const
{
  double raw = drive_m.El->servo_coefficients[0];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_0, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff0();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff1() const
{
  double raw = drive_m.El->servo_coefficients[1];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_1, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff1();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff2() const
{
  double raw = drive_m.El->servo_coefficients[2];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_2, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff2();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff3() const
{
  double raw = drive_m.El->servo_coefficients[3];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_3, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff3();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff4() const
{
  double raw = drive_m.El->servo_coefficients[4];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_4, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff4();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff5() const
{
  double raw = drive_m.El->servo_coefficients[5];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_5, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff5();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff6() const
{
  double raw = drive_m.El->servo_coefficients[6];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_6, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff6();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff7() const
{
  double raw = drive_m.El->servo_coefficients[7];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_7, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff7();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff8() const
{
  double raw = drive_m.El->servo_coefficients[8];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_8, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff8();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeff9() const
{
  double raw = drive_m.El->servo_coefficients[9];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_9, data);
  return MountVertexHWSimBase::getMonitorElServoCoeff9();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeffA() const
{
  double raw = drive_m.El->servo_coefficients[10];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_A, data);
  return MountVertexHWSimBase::getMonitorElServoCoeffA();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeffB() const
{
  double raw = drive_m.El->servo_coefficients[11];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_B, data);
  return MountVertexHWSimBase::getMonitorElServoCoeffB();
}

std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorElServoCoeffC() const
{
  double raw = drive_m.El->servo_coefficients[12];
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_SERVO_COEFF_C, data);
  return MountVertexHWSimBase::getMonitorElServoCoeffC();
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff0(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff0(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[0] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff1(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff1(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[1] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff2(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff2(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[2] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff3(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff3(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[3] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff4(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff4(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[4] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff5(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff5(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[5] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff6(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff6(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[6] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff7(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff7(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[7] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff8(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff8(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[8] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeff9(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeff9(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[9] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeffA(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeffA(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[10] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeffB(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeffB(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[11] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoCoeffC(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoCoeffC(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.Az->servo_coefficients[12] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff0(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff0(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[0] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff1(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff1(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[1] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff2(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff2(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[2] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff3(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff3(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[3] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff4(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff4(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[4] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff5(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff5(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[5] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff6(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff6(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[6] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff7(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff7(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[7] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff8(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff8(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[8] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeff9(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeff9(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[9] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeffA(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeffA(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[10] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeffB(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeffB(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[11] = value;
}

void MountVertexHWSimImpl::setControlSetElServoCoeffC(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoCoeffC(data);
  double value;
  AMB::TypeConversion::dataToValue(data,value);
  drive_m.El->servo_coefficients[12] = value;
}

void MountVertexHWSimImpl::setControlSetAzServoDefault(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetAzServoDefault(data);
  drive_m.Az->set_servo_defaults();
}

void MountVertexHWSimImpl::setControlSetElServoDefault(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetElServoDefault(data);
  drive_m.El->set_servo_defaults();
}

void MountVertexHWSimImpl::setControlSetStowPin(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetStowPin(data);  
  CAN::byte_t azStowPin = data[0];
  CAN::byte_t elStowPin = data[1];

  drive_m.Az->set_stow_pin(static_cast<Stow_Pin_t>(azStowPin));
  drive_m.El->set_stow_pin(static_cast<Stow_Pin_t>(elStowPin));
}

void MountVertexHWSimImpl::setControlSetMetrMode(const std::vector<CAN::byte_t>& data)
{
  MountVertexHWSimBase::setControlSetMetrMode(data);
  associate(monitorPoint_METR_MODE, data);
}
std::vector<CAN::byte_t> MountVertexHWSimImpl::getMonitorStowPin() const 
{
  std::vector<CAN::byte_t> data;
  CAN::byte_t azStowPin = static_cast<CAN::byte_t>(drive_m.Az->get_stow_pin());
  CAN::byte_t elStowPin = static_cast<CAN::byte_t>(drive_m.El->get_stow_pin());
  data.push_back(azStowPin);
  data.push_back(elStowPin);
  associate(monitorPoint_STOW_PIN, data);
  return MountVertexHWSimBase::getMonitorStowPin();
}

void AMB::MountVertexHWSimImpl::setControlSetMetrCoeff0(
    const std::vector< CAN::byte_t >& data) {
    AMB::MountVertexHWSimBase::setControlSetMetrCoeff0(data);
    associate(monitorPoint_METR_COEFF_0, data);
}

void AMB::MountVertexHWSimImpl::setControlSetMetrCoeff1(
    const std::vector< CAN::byte_t >& data) {
    AMB::MountVertexHWSimBase::setControlSetMetrCoeff0(data);
    associate(monitorPoint_METR_COEFF_1, data);
}
