// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#include <MountVertexImpl.h>
#include <maciACSComponentDefines.h>
#include <MountError.h>
#include <TMCDBAccessIFC.h> // for TMCDB::Access
#include <iostream> 
#include <iomanip> 

using log_audience::OPERATOR;
using ControlExceptions::CAMBErrorExImpl;
using ControlExceptions::CAMBErrorEx;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::INACTErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::HardwareErrorEx;
using MountError::TMCDBUnavailableExImpl;
using MountError::TMCDBUnavailableEx;
using std::string;
using std::ostringstream;
using Control::LongSeq;
using std::abs;

MountVertexImpl::MountVertexImpl(const ACE_CString& name, maci::ContainerServices* pCS)
    : MountVertexBase(name, pCS)
{
    antennaModel_m = "MountVertex";
}

MountVertexImpl::~MountVertexImpl()
{
}

void MountVertexImpl::initialize() {
    MountVertexBase::initialize();
}

void MountVertexImpl::cleanUp() {
    MountVertexBase::cleanUp();
}

void MountVertexImpl::hwInitializeAction(){
    MountVertexBase::hwInitializeAction();

    // Set metrology mode to a default state
    try {
        LongSeq metrMode;
        metrMode.length(4);
        metrMode[0] = 0x42; // (bit 5 and bit 1)
        metrMode[1] = 0;
        metrMode[2] = 0;
        metrMode[3] = 0;

        SET_METR_MODE(metrMode);
    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as there is a communications problem with the ACU.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    }

    // Set receiver cabin temperature
    try {
      
      SET_REC_CAB_TEMP(receiverCabinTemp_m);
      ACS_LOG(LM_SOURCE_INFO,__func__,(LM_INFO,"Receiver Cabin Temperature set to %d[C]", receiverCabinTemp_m));      
    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot set the receiver cabin temperature to the default value";
        msg += " as there is a communications problem with the ACU.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot set the receiver cabin temperature to the default value";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } 
}

void MountVertexImpl::hwOperational() {
    MountVertexBase::hwOperational();
    try {
        reloadMetrologyCoefficients();
    } catch (CAMBErrorEx& ex) {
        ostringstream msg;
        msg << "Cannot set the metrology coefficients "
            << " as there is a communications problem with the ACU.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        ostringstream msg;
        msg << "Cannot set the metrology coefficients "
            << " as the control software is not operational.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        // TODO Set an alarm
    } catch (TMCDBUnavailableEx& ex) {
        ostringstream msg;
        msg << "Cannot set the metrology coefficients "
            << " as they cannot be obtained from teh TMCDB.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        TMCDBUnavailableExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        // TODO Set an alarm
    }
}



/**
 * Reload metrology coefficients to ACU
 *
 * Important update:
 * As for May 10, 2011 there was an agreement that this task will be executed
 * manually in ALMA production environments using scripts outside CONTROL scope
 *
 * -- jgil@alma.cl 20110510
 */
void MountVertexImpl::reloadMetrologyCoefficients() {
    ostringstream msg;
    msg << "Metrology must be set manually for Vertex Mount.";
    LOG_TO_OPERATOR(LM_INFO, msg.str());
    ACS_LOG(LM_SOURCE_INFO,__func__,(LM_NOTICE, "Deprecated function. Metrology must be set manually for Vertex Mount"));

//     try {
//         maci::SmartPtr<TMCDB::Access> tmcdb =
//             getContainerServices()->getDefaultComponentSmartPtr<TMCDB::Access>
//             ("IDL:alma/TMCDB/Access:1.0");
//
//         const string antName = componentToAntennaName(CORBA::String_var(name()).in());
//         TMCDB::MetrologyCoeffSeq_var coeffs =
//             tmcdb->getMetrologyCoefficients(antName.c_str());
//         if (coeffs->length() != 2) {
//             TMCDBUnavailableExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
//             ostringstream msg;
//             msg << "Expected two metrology coefficients from the TMCDB but got "
//                 << coeffs->length();
//             ex.addData("Reason", msg.str());
//             ex.log();
//             throw ex.getTMCDBUnavailableEx();
//         }
//
// 	/* Start by setting both coeffs */
// 	SET_METR_COEFF_0(coeffs[0]);
// 	SET_METR_COEFF_1(coeffs[1]);
//
//         // To investigate AIV-3319 check if the coefficient really made it and
//         // check to see if they were set correctly.
//         // Note: Jeff Kern Dev 21, 2010 With the reworked version of this we
//         // are correctly setting the coeffiecients as far as I can see.  I
//         // suspect that the returned values are not valid until a 48 ms tick
//         // has gone by, and thus we continue to see the errors reported below.
//         // The correct thing to do is get the timestamp from the above set
//         // methods, and then insist that the readback happens at least 48 ms
//         // later.
//         ACS::Time ts;
//         double readBackValue0 = GET_METR_COEFF_0(ts);
//         double readBackValue1 = GET_METR_COEFF_1(ts);
//
//         ostringstream msg;
//         bool itsBad = false;
//         if (abs(coeffs[0] - readBackValue0) > .1) {
//             msg << std::setprecision(3) << std::fixed
//                 << "Metrology coefficient 0 (an0) incorrectly set to "
//                 << readBackValue0 << ". It should have been "<< coeffs[0];
//             itsBad = true;
//         }
//
//         if (abs(coeffs[1] - readBackValue1) > .1) {
//             msg << std::setprecision(3) << std::fixed
//                 << " Metrology coefficient 1 (aw0) incorrectly set to "
//                 << readBackValue1 << ". It should have been "<< coeffs[1];
//             itsBad = true;
//         }
//
//         if (itsBad) {
//             LOG_TO_OPERATOR(LM_ERROR, msg.str());
//             CAMBErrorExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
//             ex.addData("Reason", msg.str());
//             ex.log();
//             throw ex.getCAMBErrorEx();
//         }
//         {
//             ostringstream msg;
//             msg  << std::setprecision(3) << std::fixed
//                  << "Using metrology coefficients of an0=" << coeffs[0]
//                  << " aw0=" << coeffs[1] << " arc-seconds";
//             LOG_TO_OPERATOR(LM_INFO, msg.str());
//         }
//     } catch (maciErrType::maciErrTypeExImpl& ex) {
//         TMCDBUnavailableExImpl newEx(ex,__FILE__,__LINE__,__PRETTY_FUNCTION__);
//         ex.addData("Reason", "Cannot get a reference to the TMCDB component.");
//         ex.log();
//         throw newEx.getTMCDBUnavailableEx();
//     }
}



void MountVertexImpl::
SET_METR_MODE(const LongSeq& world) {

    MountBase::SET_METR_MODE(world);
    ostringstream msg;
    msg << "SET_METR_MODE command executed with parameters: ["
        << world[0] << ", "
        << world[1] << ", "
        << world[2] << ", "
        << world[3] << "]";
    LOG_TO_AUDIENCE(LM_NOTICE, __func__, msg.str(), OPERATOR);
}

double MountVertexImpl::subrefXLimit() {
    return 0.007; // 7mm
}

double MountVertexImpl::subrefZLimit() {
    return 0.011; // 11mm
}


/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(MountVertexImpl)
