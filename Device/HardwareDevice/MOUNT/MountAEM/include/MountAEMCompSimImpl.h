// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Copyright by ESO, 2008
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef MOUNTAEMCOMPSIMIMPL_H
#define MOUNTAEMCOMPSIMIMPL_H

#include "MountAEMCompSimBase.h"
#include "MountAEMHWSimImpl.h"

class MountAEMCompSimImpl: public MountAEMCompSimBase {
  public:
    MountAEMCompSimImpl(const ACE_CString& name,
                        maci::ContainerServices* pCS);
    virtual ~MountAEMCompSimImpl();
    
    // \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();
    
    // \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

  private:
    AMB::MountAEMHWSimImpl* device_m;
    
    MountAEMCompSimImpl(const MountAEMCompSimImpl&);
    MountAEMCompSimImpl& operator=(const MountAEMCompSimImpl&);

}; // class MountAEMCompSimImpl

#endif // MOUNTAEMCOMPSIMIMPL_H
