// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MOUNTAEMIMPL_H
#define MOUNTAEMIMPL_H

#include <MountAEMBase.h>
#include <MountAEMS.h>

class MountAEMImpl: public virtual POA_Control::MountAEM,
                    public MountAEMBase
{
  public:
    
    MountAEMImpl(const ACE_CString& name, maci::ContainerServices* pCS);
    ~MountAEMImpl();
    
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    void cleanUp();
 
    /// Set the metrology mode in the ACU. This just logs the metrology mode
    /// and calls the base class version.
    /// \exception ControlExceptions::CAMBErrorEx,
    /// \exception ControlExceptions::INACTErrorEx
    virtual void SET_METR_MODE(const Control::LongSeq& world);

    /// Initialize azimuth and elevation encoders.
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception MountError::TimeOutExImpl
    virtual void initializeEncoders();

    /// \exception ControlExceptions::CAMBErrorEx
    virtual void RESET_ACU_CMD_1(Control::MountAEM::ResetACUCmdOptions1 option);

  protected:

    /// Initialize encoders and set metrology mode to default.
    virtual void hwInitializeAction();

    /// \exception MountError::TimeOutExImpl
    void waitForAzimuthEncoderInitialization();
    bool isAzimuthEncoderInitialized();

    /// \exception MountError::TimeOutExImpl
    void waitForElevationEncoderInitialization();
    bool isElevationEncoderInitialized();

  private:
    // copy and assignment are not allowed
    MountAEMImpl(const MountAEMImpl&);
    MountAEMImpl& operator = (const MountAEMImpl&);
    
};
#endif // MOUNTAEMIMPL_H 
