// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Copyright by ESO, 2008
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef MOUNTAEMHWSIMIMPL_H
#define MOUNTAEMHWSIMIMPL_H

#include "MountAEMHWSimBase.h"
#include "MountAEMHWSimImplDrive.h"

namespace AMB {
    class MountAEMHWSimImpl : public MountAEMHWSimBase
    {
    public:
      MountAEMHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber);
      static const double bitsPerDegree;
      static const double degreesPerBit;
      static const double encoderOffset;
      void start();
      void stop();

    protected:
      virtual std::vector<CAN::byte_t> getMonitorAcuModeRsp() const;
      virtual std::vector<CAN::byte_t> getMonitorAzPosnRsp() const;
      virtual std::vector<CAN::byte_t> getMonitorElPosnRsp() const;
      virtual std::vector<CAN::byte_t> getMonitorAzEnc() const;
      virtual std::vector<CAN::byte_t> getMonitorElEnc() const;
      virtual std::vector<CAN::byte_t> getMonitorAzBrake() const;
      virtual std::vector<CAN::byte_t> getMonitorElBrake() const;
      virtual std::vector<CAN::byte_t> getMonitorStowPin() const;

      virtual void setControlAcuModeCmd(const std::vector<CAN::byte_t>& data);
      virtual void setControlAzTrajCmd(const std::vector<CAN::byte_t>& data);
      virtual void setControlElTrajCmd(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetAzBrake(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetElBrake(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetStowPin(const std::vector<CAN::byte_t>& data);

      AEM::MountAEMHWSimImplDrive drive_m;

    }; // class MountAEMHWSimImpl
} // namespace AMB

#endif // MOUNTAEMHWSIMIMPL_H
