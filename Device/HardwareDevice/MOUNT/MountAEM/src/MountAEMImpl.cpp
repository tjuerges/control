// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#include <MountAEMImpl.h>
#include <MountError.h>
#include <maciACSComponentDefines.h>

using log_audience::OPERATOR;
using ControlExceptions::CAMBErrorExImpl;
using ControlExceptions::CAMBErrorEx;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::INACTErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::HardwareErrorEx;
using std::string;
using std::ostringstream;

MountAEMImpl::MountAEMImpl(const ACE_CString& name,
                           maci::ContainerServices* pCS)
    :MountAEMBase(name, pCS)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    antennaModel_m = "MountAEM";
}

MountAEMImpl::~MountAEMImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void MountAEMImpl::initialize() {
  MountAEMBase::initialize();
}

void MountAEMImpl::cleanUp() {
  MountAEMBase::cleanUp();
}


void MountAEMImpl::hwInitializeAction() {
    AUTO_TRACE(__PRETTY_FUNCTION__);
    MountAEMBase::hwInitializeAction();

    // Set metrology mode to a default state
    try {
        Control::LongSeq metrMode;
        metrMode.length(4);
        metrMode[0] = 0;
        metrMode[1] = 0;
        metrMode[2] = 0;
        metrMode[3] = 0;

        SET_METR_MODE(metrMode);
    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as there is a communications problem with the ACU.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
    }

    // Initialize encoders, if needed
    try {
        initializeEncoders();
    } catch (ControlExceptions::CAMBErrorEx& _ex) {
        ACS_LOG(LM_SOURCE_INFO,__func__,
                (LM_WARNING,"Communication error while initializing encoders. Please run initializeEncoders() manually before moving the antenna."));
    } catch (MountError::TimeOutExImpl& _ex) {
        ACS_LOG(LM_SOURCE_INFO,__func__,
                (LM_WARNING,"Timeout error while initializing encoders. Please run initializeEncoders() manually before moving the antenna."));
    }
}


void MountAEMImpl::initializeEncoders()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // TODO: Add here a reading of GET_ACU_ERROR monitor point
    // to determine if we really want to clear this fault
    setClearFaultCmd();

    // Azimuth encoder
    if (!isAzimuthEncoderInitialized()) {
        setInitAzEncAbsPos();
        try {
            waitForAzimuthEncoderInitialization();
        } catch(MountError::TimeOutExImpl &_ex) {
            // TODO: Consider also setting an alarm
            setError("Timeout error while initializing encoders. Is the antenna unlocked?");
            throw _ex;
        }
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_INFO,"Azimuth encoder successfully initialized."));
    } else {
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_DEBUG,"Azimuth encoder was already initialized."));
    }

    // Elevation encoder
    if (!isElevationEncoderInitialized()) {
        setInitElEncAbsPos();
        try {
            waitForElevationEncoderInitialization();
        } catch(MountError::TimeOutExImpl &_ex) {
            // TODO: Consider also setting an alarm
            setError("Timeout error while initializing encoders. Is the antenna unlocked?");
            throw _ex;
        }
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_INFO,"Elevation encoder successfully initialized."));
    } else {
        ACS_LOG(LM_SOURCE_INFO,__func__,(LM_DEBUG,"Elevation encoder was already initialized."));
    }
    clearError();
}


bool MountAEMImpl::isAzimuthEncoderInitialized()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp;
    if (getAzAbsEncUnavailable(timestamp)) {
        return false;
    } else {
        return true;
    }
}

bool MountAEMImpl::isElevationEncoderInitialized()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp;
    if (getElAbsEncUnavailable(timestamp)) {
        return false;
    } else {
        return true;
    }
}

void MountAEMImpl::waitForAzimuthEncoderInitialization()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // TODO: Adjust sleep time to what is really required
    // to finish initialization
    bool ready = false;
    int numRetries = 14;
    unsigned long pollingTime = 5000000; // 5 secs

    // Wait a maximum of 5*14 secs to initialize Azimuth
    do {
        ACS_LOG(LM_SOURCE_INFO,__func__,
                (LM_DEBUG,"Retries pending %ld",numRetries));
        usleep(pollingTime);
        ready = isAzimuthEncoderInitialized();
        numRetries--;
    } while (!ready && numRetries > 0);

    if (!ready) {
        MountError::TimeOutExImpl ex(__FILE__,__LINE__,__func__);
        throw ex;
    }
}

void MountAEMImpl::waitForElevationEncoderInitialization()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // TODO: Adjust sleep time to what is really required 
    // to finish initialization
    bool ready = false;
    int numRetries = 28;
    unsigned long pollingTime = 5000000; // 5 secs

    // Wait a maximum of 5*28 secs to initialize Elevation
    do {
        ACS_LOG(LM_SOURCE_INFO,__func__,
                (LM_DEBUG,"Retries pending %ld",numRetries));
        usleep(pollingTime);
        ready = isElevationEncoderInitialized();
        numRetries--;
    } while (!ready && numRetries > 0);

    if(!ready) {
        MountError::TimeOutExImpl ex(__FILE__,__LINE__,__func__);
        throw ex;
    }
}

void MountAEMImpl::SET_METR_MODE(const Control::LongSeq& world) {
    MountAEMBase::SET_METR_MODE(world);
    ostringstream msg;
    msg << "SET_METR_MODE command executed with parameters: ["
        << world[0] << ", "
        << world[1] << ", "
        << world[2] << ", "
        << world[3] << "]";
    LOG_TO_AUDIENCE(LM_NOTICE, __func__, msg.str(), OPERATOR);
}

void MountAEMImpl::RESET_ACU_CMD_1(Control::MountAEM::ResetACUCmdOptions1 option) {
    switch (option) {
        case Control::MountAEM::COMPLETE_REBOOT_1:
            sendOneByteCommand(getControlRCAResetAcuCmd1());
            break;
        case Control::MountAEM::METROLOGY_REBOOT_1:
            sendOneByteCommand(getControlRCAResetAcuCmd1(),0x02);
            break;
        case Control::MountAEM::SUBREFLECTOR_REBOOT_1:
            sendOneByteCommand(getControlRCAResetAcuCmd1(),0x04);
            break;
        case Control::MountAEM::AZ_DRIVES:
            sendOneByteCommand(getControlRCAResetAcuCmd1(),0x08);
            break;
        case Control::MountAEM::EL_DRIVES:
            sendOneByteCommand(getControlRCAResetAcuCmd1(),0x10);
            break;
        case Control::MountAEM::SUBREFLECTOR_REBOOT_TAPE_SWITCH_OVERRIDE:
            sendOneByteCommand(getControlRCAResetAcuCmd1(),0x20);
            break;
        case Control::MountAEM::TE_RESYNC:
            sendOneByteCommand(getControlRCAResetAcuCmd1(),0x40);
            break;
        case Control::MountAEM::ENC_HALL_INCONSISTENCY_CLEAR:
            sendOneByteCommand(getControlRCAResetAcuCmd1(),0x80);
            break;
        default:
            /*
             * This should never be executed
             */
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                    (LM_ERROR,"Unknown action. Ignored"));
    }
    return;
}


/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(MountAEMImpl)
