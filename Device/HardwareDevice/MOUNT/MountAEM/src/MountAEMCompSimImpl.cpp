// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Copyright by ESO, 2008
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "MountAEMCompSimImpl.h"
#include <maciACSComponentDefines.h>
#include <TypeConversion.h>
#include <SimulatedSerialNumber.h>

MountAEMCompSimImpl::MountAEMCompSimImpl(const ACE_CString& name, 
                                         maci::ContainerServices* pCS)
	: MountAEMCompSimBase(name,pCS)
{
    const std::string fnName("MountAEMCompSimImpl::MountAEMCompSimImpl");
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string componentName(name.c_str());
    unsigned long long hashed_sn=AMB::Utils::getSimSerialNumber(componentName,"MountAEM");
    std::ostringstream msg;
    msg<< "simSerialNumber for  " << name.c_str() << "  with assembly name " << "MountAEM" << " is " << std::hex << "0x" << hashed_sn << std::endl;
    ACS_LOG(LM_SOURCE_INFO, fnName, (LM_DEBUG, msg.str().c_str()));

    std::vector< CAN::byte_t > sn;
    AMB::node_t node = 0x00;
    AMB::TypeConversion::valueToData(sn,hashed_sn, 8U);

    device_m = new AMB::MountAEMHWSimImpl(node,sn);
    simulationIf_m.setSimObj(device_m);
}

MountAEMCompSimImpl::~MountAEMCompSimImpl()
{
    if(device_m) delete device_m;
}

void MountAEMCompSimImpl::initialize() {
    MountAEMCompSimBase::initialize();
    device_m->start();
}

void MountAEMCompSimImpl::cleanUp() {
    device_m->stop();
    MountAEMCompSimBase::cleanUp();
}


/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(MountAEMCompSimImpl)
