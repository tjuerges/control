// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Copyright by ESO, 2008
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "MountAEMHWSimImpl.h"
#include <vector>
#include <Axis.h>

using namespace AMB;
using std::vector;

const double MountAEMHWSimImpl::bitsPerDegree = (static_cast<double>(0x40000000) / 180.0);
const double MountAEMHWSimImpl::degreesPerBit = (180.0 / static_cast<double>(0x40000000));
const double MountAEMHWSimImpl::encoderOffset = 0.0;

MountAEMHWSimImpl::MountAEMHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber)
    : MountAEMHWSimBase::MountAEMHWSimBase(node, serialNumber)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    azTransf_m.initialize(bitsPerDegree, degreesPerBit, encoderOffset);
    elTransf_m.initialize(bitsPerDegree, degreesPerBit, encoderOffset);
}

void MountAEMHWSimImpl::start() {
    drive_m.start();
}

void MountAEMHWSimImpl::stop() {
    drive_m.stop();
}

std::vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorAcuModeRsp() const {
  std::vector<char> mode = drive_m.getACUMode();
  (state_m.find(monitorPoint_ACU_MODE_RSP)->second)->clear();
  for(unsigned short index=0; index<static_cast<unsigned short>(mode.size()); index++)
    (state_m.find(monitorPoint_ACU_MODE_RSP)->second)->push_back(static_cast<CAN::byte_t>(mode[index]));
  return MountAEMHWSimBase::getMonitorAcuModeRsp();
}

void MountAEMHWSimImpl::setControlAcuModeCmd(const std::vector<CAN::byte_t>& data) {
  MountAEMHWSimBase::setControlAcuModeCmd(data);
  drive_m.setACUMode(static_cast<unsigned char>(data[0]));
}

vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorAzPosnRsp() const {
    double pos, vel;
    drive_m.getAzPsn(pos, vel);
    long raw;
    azTransf_m.degreesToBits(pos, raw);
    unsigned long long position = static_cast<unsigned long long>(raw);
    position += (position << 32);
    vector<CAN::byte_t> data;
    AMB::TypeConversion::valueToData(data, position);
    associate(monitorPoint_AZ_POSN_RSP, data);
    return MountAEMHWSimBase::getMonitorAzPosnRsp();
} 

vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorElPosnRsp() const {
    double pos, vel;
    drive_m.getElPsn(pos, vel);
    long raw;
    elTransf_m.degreesToBits(pos, raw);
    unsigned long long position = static_cast<unsigned long long>(raw);
    position += (position << 32);
    std::vector<CAN::byte_t> data;
    AMB::TypeConversion::valueToData(data, position);
    associate(monitorPoint_EL_POSN_RSP, data);
    return MountAEMHWSimBase::getMonitorElPosnRsp();
}

vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorAzEnc() const {
    double pos, vel;
    drive_m.getAzPsn(pos,vel);
    long raw;
    azTransf_m.degreesToEncoder(pos,raw);
    vector<CAN::byte_t> data;
    AMB::TypeConversion::valueToData(data, raw);
    associate(monitorPoint_AZ_ENC, data);
    return MountAEMHWSimBase::getMonitorAzEnc();
}

vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorElEnc() const {
    double pos, vel;
    drive_m.getElPsn(pos,vel);
    long raw;
    elTransf_m.degreesToEncoder(pos,raw);
    vector<CAN::byte_t> data;
    AMB::TypeConversion::valueToData(data, raw);
    associate(monitorPoint_EL_ENC, data);
    return MountAEMHWSimBase::getMonitorElEnc();
}

void MountAEMHWSimImpl::setControlAzTrajCmd(const vector<CAN::byte_t>& data) {
    MountAEMHWSimBase::setControlAzTrajCmd(data);
    std::vector<CAN::byte_t> pos;
    std::vector<CAN::byte_t> vel;
  
    const unsigned short longSize = sizeof(long);
    for(unsigned short index=0; index <longSize; index++)
      {
        pos.push_back(data[index]);
        vel.push_back(data[index+longSize]);
      }
    long rawPos, rawVel;
    double position, velocity;
    AMB::TypeConversion::dataToValue(pos, rawPos);
    AMB::TypeConversion::dataToValue(vel, rawVel);
    azTransf_m.bitsToDegrees(position,rawPos);
    azTransf_m.bitsToDegrees(velocity,rawVel);
    drive_m.setAzPsn(position,velocity);
}

void MountAEMHWSimImpl::setControlElTrajCmd(const vector<CAN::byte_t>& data) {
  MountAEMHWSimBase::setControlElTrajCmd(data);
  std::vector<CAN::byte_t> pos;
  std::vector<CAN::byte_t> vel;

  const unsigned short longSize = sizeof(long);
  for(unsigned short index=0; index <longSize; index++)
    {
      pos.push_back(data[index]);
      vel.push_back(data[index+longSize]);
    }
  long rawPos, rawVel;
  double position, velocity;
  AMB::TypeConversion::dataToValue(pos, rawPos);
  AMB::TypeConversion::dataToValue(vel, rawVel);
  elTransf_m.bitsToDegrees(position,rawPos);
  elTransf_m.bitsToDegrees(velocity,rawVel);
  drive_m.setElPsn(position,velocity);
}

void MountAEMHWSimImpl::setControlSetAzBrake(const std::vector<CAN::byte_t>& data)
{
  MountAEMHWSimBase::setControlSetAzBrake(data);
  unsigned char raw;
  AMB::TypeConversion::dataToValue(data, raw);
  drive_m.Az->set_brake(static_cast<AZEL_Brake_t>(raw));
} 
  
void MountAEMHWSimImpl::setControlSetElBrake(const std::vector<CAN::byte_t>& data)
{
  MountAEMHWSimBase::setControlSetElBrake(data);
  unsigned char raw;
  AMB::TypeConversion::dataToValue(data, raw);
  drive_m.El->set_brake(static_cast<AZEL_Brake_t>(raw));
} 

std::vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorAzBrake() const
{
  CAN::byte_t raw = static_cast<CAN::byte_t>(drive_m.Az->get_brake());
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_BRAKE, data);
  return MountAEMHWSimBase::getMonitorAzBrake();
}

std::vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorElBrake() const
{
  CAN::byte_t raw = static_cast<CAN::byte_t>(drive_m.El->get_brake());
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_BRAKE, data);
  return MountAEMHWSimBase::getMonitorElBrake();
} 

void MountAEMHWSimImpl::setControlSetStowPin(const std::vector<CAN::byte_t>& data)
{
  MountAEMHWSimBase::setControlSetStowPin(data);
  CAN::byte_t azStowPin = data[0];
  CAN::byte_t elStowPin = data[1];

  drive_m.Az->set_stow_pin(static_cast<Stow_Pin_t>(azStowPin));
  drive_m.El->set_stow_pin(static_cast<Stow_Pin_t>(elStowPin));
}

std::vector<CAN::byte_t> MountAEMHWSimImpl::getMonitorStowPin() const
{
  std::vector<CAN::byte_t> data;
  CAN::byte_t azStowPin = static_cast<CAN::byte_t>(drive_m.Az->get_stow_pin());
  CAN::byte_t elStowPin = static_cast<CAN::byte_t>(drive_m.El->get_stow_pin());
  data.push_back(azStowPin);
  data.push_back(elStowPin);
  associate(monitorPoint_STOW_PIN, data);
  return MountAEMHWSimBase::getMonitorStowPin();
}

