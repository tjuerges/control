#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 - 2009
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
"""
This module defines the MountAEM Control Command Language (CCL)
"""

import MountAEMBase
import Control

class MountAEM(MountAEMBase.MountAEMBase):
    '''
    The purpose of this document is to define the interface between
    the antenna, specifically it's control unit and ALMA's monitor and
    control (M&C) system. The ICD provides the interface definitions
    for the minimum control functionality which is identified at
    present for the control of the antenna.
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        '''
        # Initialize the base class. The base class contains the
        # reference to the underlying Mount component.
        MountAEMBase.MountAEMBase.__init__(self, \
                                     antennaName, \
                                     componentName, \
                                     stickyFlag);

    def initializeEncoders(self):
        '''
        Initialize both azimuth and elevation encoders. Returns when ready.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].initializeEncoders()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

