/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MountACACommonHWSimImpl.cpp
 */

//-------------------------------------------------------------------------
// Simulation code for MELCO antenna specific commands.
//                                                     2008-06-30 M.Tasaki
// Most of the entry points are defined in MountACACommon_spreadsheet.xml.
// However, this class overrides some of the functions of the upper classes,
// because the drive status machine must set some of them and some others have
// MELCO specific bit array.
//-------------------------------------------------------------------------
#include <stdio.h>
#include <string>
#include <iostream>
#include "MountACACommonHWSimImpl.h"
#include <maciACSComponentDefines.h>

using namespace AMB;

const double MountACACommonHWSimImpl::bitsPerDegree = (static_cast<double>(0x40000000) / 180.0);
const double MountACACommonHWSimImpl::degreesPerBit = (180.0 / static_cast<double>(0x40000000));
const double MountACACommonHWSimImpl::encoderOffset = 0.0;
 

/* Empty example class -- please feel free to modify here inherited methods */
MountACACommonHWSimImpl::MountACACommonHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber)
    : MountACACommonHWSimBase::MountACACommonHWSimBase(node, serialNumber)
{
  const char* __METHOD__ = "MountACACommonHWSimImpl::MountACACommonHWSimImpl";
  ACS_TRACE(__METHOD__);

  azTransf_m.initialize(bitsPerDegree, degreesPerBit, encoderOffset);
  elTransf_m.initialize(bitsPerDegree, degreesPerBit, encoderOffset);
}

MountACACommonHWSimImpl::~MountACACommonHWSimImpl() {
  stop();
}

void MountACACommonHWSimImpl::start() {
  drive_m.start();

  // set the shutter closed for initialization
  unsigned char getShutterValue = 0x0; // 0x0=close status value
  std::vector<CAN::byte_t> getShutterData;
  TypeConversion::valueToData(getShutterData, getShutterValue);
  setControlSetShutter( getShutterData );

}
void MountACACommonHWSimImpl::stop()
{
  drive_m.stop();
}

//---------------------------------------------------------------------
// Antenna Commands copied from MountVertexHWSimImpl.cpp  
//
//
//---------------------------------------------------------------------

//-----get/set ACUMODE------------------
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAcuModeRsp() const
{
  std::vector<char> mode = drive_m.getACUMode();
  (state_m.find(monitorPoint_ACU_MODE_RSP)->second)->clear();
  for(unsigned short index=0; index<static_cast<unsigned short>(mode.size()); index++)
    (state_m.find(monitorPoint_ACU_MODE_RSP)->second)->push_back(static_cast<CAN::byte_t>(mode[index]));
  return MountACACommonHWSimBase::getMonitorAcuModeRsp();
}
void MountACACommonHWSimImpl::setControlAcuModeCmd(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setControlAcuModeCmd";
  ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlAcuModeCmd(data);
  drive_m.setACUMode(static_cast<unsigned char>(data[0]));
}
void MountACACommonHWSimImpl::setMonitorAcuModeRsp(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setMonitorAcuModeRsp";
  ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setMonitorAcuModeRsp(data);
  std::vector<unsigned char> mode;
  for(unsigned short index=0; index<static_cast<unsigned short>(data.size()); index++)
    mode.push_back(static_cast<unsigned char>(data[index]));
  drive_m.setACUMode(mode[0]);
}
//-----End of get/set ACUMODE-------------

//-----get AZ(EL) position and encoder -------------
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAzPosnRsp() const
{
  double pos, vel;
  long raw;
  drive_m.getAzPsn(pos,vel);
  azTransf_m.degreesToBits(pos,raw);
  unsigned long long position = static_cast<unsigned long long>(raw);
  position += (position << 32);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, position);
  associate(monitorPoint_AZ_POSN_RSP, data);
  return MountACACommonHWSimBase::getMonitorAzPosnRsp();
} 
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorElPosnRsp() const
{
  double pos, vel;
  long raw;
  drive_m.getElPsn(pos,vel);
  elTransf_m.degreesToBits(pos,raw);
  unsigned long long position = static_cast<unsigned long long>(raw);
  position += (position << 32);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, position);
  associate(monitorPoint_EL_POSN_RSP, data);
  return MountACACommonHWSimBase::getMonitorElPosnRsp();
}

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAzEnc() const
{
  double pos, vel;
  long raw;
  drive_m.getAzPsn(pos,vel);
  azTransf_m.degreesToEncoder(pos,raw);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_ENC, data);
  return MountACACommonHWSimBase::getMonitorAzEnc();
}

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorElEnc() const
{
  double pos, vel;
  long raw;
  drive_m.getElPsn(pos,vel);
  elTransf_m.degreesToEncoder(pos,raw);
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_ENC, data);
  return MountACACommonHWSimBase::getMonitorElEnc();
}
//-----End of get AZ(EL) position and encoder -------------

//-----set AZ(EL) trajectory cmd -------------
void MountACACommonHWSimImpl::setControlSetAzTrajCmd(const std::vector<CAN::byte_t>& data)
{
  setControlAzTrajCmd(data);
  // I do not know if it will work.
}
void MountACACommonHWSimImpl::setControlAzTrajCmd(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setControlAzTrajCmd";
  ACS_TRACE(__METHOD__);
    
  MountACACommonHWSimBase::setControlAzTrajCmd(data);
  std::vector<CAN::byte_t> pos;
  std::vector<CAN::byte_t> vel;

  const unsigned short longSize = sizeof(long);
  for(unsigned short index=0; index <longSize; index++)
    {
      pos.push_back(data[index]);
      vel.push_back(data[index+longSize]);
    }
  long rawPos, rawVel;
  double position, velocity;
  AMB::TypeConversion::dataToValue(pos, rawPos);
  AMB::TypeConversion::dataToValue(vel, rawVel);
  azTransf_m.bitsToDegrees(position,rawPos);
  azTransf_m.bitsToDegrees(velocity,rawVel);

  drive_m.setAzPsn(position,velocity);
}

void MountACACommonHWSimImpl::setControlElTrajCmd(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setControlElTrajCmd";
  ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlElTrajCmd(data);
  std::vector<CAN::byte_t> pos;
  std::vector<CAN::byte_t> vel;

  const unsigned short longSize = sizeof(long);
  for(unsigned short index=0; index <longSize; index++)
    {
      pos.push_back(data[index]);
      vel.push_back(data[index+longSize]);
    }
  long rawPos, rawVel;
  double position, velocity;
  AMB::TypeConversion::dataToValue(pos, rawPos);
  AMB::TypeConversion::dataToValue(vel, rawVel);
  elTransf_m.bitsToDegrees(position,rawPos);
  elTransf_m.bitsToDegrees(velocity,rawVel);
  drive_m.setElPsn(position,velocity);
}
//-----End of set AZ(EL) trajectory cmd -------------


//-----get/set AZ(EL)  brake -----------------------
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAzBrake() const
{
  CAN::byte_t raw = static_cast<CAN::byte_t>(drive_m.Az->get_brake());
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_AZ_BRAKE, data);
  return MountACACommonHWSimBase::getMonitorAzBrake();
}

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorElBrake() const
{
  CAN::byte_t raw = static_cast<CAN::byte_t>(drive_m.El->get_brake());
  std::vector<CAN::byte_t> data;
  AMB::TypeConversion::valueToData(data, raw);
  associate(monitorPoint_EL_BRAKE, data);
  return MountACACommonHWSimBase::getMonitorElBrake();
}
void MountACACommonHWSimImpl::setControlSetAzBrake(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetAzBrake";
  ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetAzBrake(data);
  unsigned char raw;
  AMB::TypeConversion::dataToValue(data, raw);
  drive_m.Az->set_brake(static_cast<AZEL_Brake_t>(raw));
}

void MountACACommonHWSimImpl::setControlSetElBrake(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetElBrake";
  ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetElBrake(data);
  unsigned char raw;
  AMB::TypeConversion::dataToValue(data, raw);
  drive_m.El->set_brake(static_cast<AZEL_Brake_t>(raw));
}
//-----End of get/set AZ(EL) brake -------------

//-----get/set AZ(EL) Servo Coeffcient 0-F -------------
// get(set)ServoCoefficient0-F must be defined in MountHWSimImpl,
// because they are common to all antennas. I put code here until it happens.
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAzServoCoeff0() const
{
//  double raw = drive_m.Az->servo_coefficients[0];
//  std::vector<CAN::byte_t> data;
//  AMB::TypeConversion::valueToData(data, raw);
//  associate(monitorPoint_AZ_SERVO_COEFF_0, data); 
  return MountACACommonHWSimBase::getMonitorAzServoCoeff0();
}

#define MacroGetAzServoCoeff( Index, Suffix) \
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAzServoCoeff##Suffix() const \
{ \
  return MountACACommonHWSimBase::getMonitorAzServoCoeff##Suffix(); \
}

MacroGetAzServoCoeff( 1, 1) 
MacroGetAzServoCoeff( 2, 2) 
MacroGetAzServoCoeff( 3, 3) 
MacroGetAzServoCoeff( 4, 4) 
MacroGetAzServoCoeff( 5, 5) 
MacroGetAzServoCoeff( 6, 6) 
MacroGetAzServoCoeff( 7, 7) 
MacroGetAzServoCoeff( 8, 8) 
MacroGetAzServoCoeff( 9, 9) 
MacroGetAzServoCoeff(10, A) 
MacroGetAzServoCoeff(11, B) 
MacroGetAzServoCoeff(12, C) 
MacroGetAzServoCoeff(13, D)
MacroGetAzServoCoeff(14, E) 
MacroGetAzServoCoeff(15, F)  

#undef MacroGetAzServoCoeff

#define MacroGetElServoCoeff( Index, Suffix) \
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorElServoCoeff##Suffix() const \
{ \
  return MountACACommonHWSimBase::getMonitorElServoCoeff##Suffix(); \
}

MacroGetElServoCoeff( 0, 0) 
MacroGetElServoCoeff( 1, 1) 
MacroGetElServoCoeff( 2, 2) 
MacroGetElServoCoeff( 3, 3) 
MacroGetElServoCoeff( 4, 4)
MacroGetElServoCoeff( 5, 5) 
MacroGetElServoCoeff( 6, 6) 
MacroGetElServoCoeff( 7, 7) 
MacroGetElServoCoeff( 8, 8) 
MacroGetElServoCoeff( 9, 9) 
MacroGetElServoCoeff(10, A) 
MacroGetElServoCoeff(11, B) 
MacroGetElServoCoeff(12, C)  
MacroGetElServoCoeff(13, D) 
MacroGetElServoCoeff(14, E) 
MacroGetElServoCoeff(15, F) 

#undef MacroGetElServoCoeff

void MountACACommonHWSimImpl::setControlSetAzServoCoeff0(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetAzServoCoeff0";
   ACS_TRACE(__METHOD__);

    MountACACommonHWSimBase::setControlSetAzServoCoeff0(data);
//  double value;
//  AMB::TypeConversion::dataToValue(data,value);
//  drive_m.Az->servo_coefficients[0] = value;

  associate(monitorPoint_AZ_SERVO_COEFF_0, data);
}

#define MacroSetAzServoCoeff( Index, Suffix) \
void MountACACommonHWSimImpl::setControlSetAzServoCoeff##Suffix(const std::vector<CAN::byte_t>& data) \
{ \
  MountACACommonHWSimBase::setControlSetAzServoCoeff##Suffix(data); \
  associate(monitorPoint_AZ_SERVO_COEFF_##Suffix, data); \
}

MacroSetAzServoCoeff( 1, 1) 
MacroSetAzServoCoeff( 2, 2) 
MacroSetAzServoCoeff( 3, 3) 
MacroSetAzServoCoeff( 4, 4) 
MacroSetAzServoCoeff( 5, 5) 
MacroSetAzServoCoeff( 6, 6) 
MacroSetAzServoCoeff( 7, 7) 
MacroSetAzServoCoeff( 8, 8) 
MacroSetAzServoCoeff( 9, 9) 
MacroSetAzServoCoeff(10, A) 
MacroSetAzServoCoeff(11, B) 
MacroSetAzServoCoeff(12, C)
MacroSetAzServoCoeff(13, D) 
MacroSetAzServoCoeff(14, E)  
MacroSetAzServoCoeff(15, F)

#undef MacroSetAzServoCoeff
 
#define MacroSetElServoCoeff( Index, Suffix) \
void MountACACommonHWSimImpl::setControlSetElServoCoeff##Suffix(const std::vector<CAN::byte_t>& data) \
{ \
  MountACACommonHWSimBase::setControlSetElServoCoeff##Suffix(data); \
  associate(monitorPoint_EL_SERVO_COEFF_##Suffix, data); \
}

MacroSetElServoCoeff( 0, 0) 
MacroSetElServoCoeff( 1, 1) 
MacroSetElServoCoeff( 2, 2) 
MacroSetElServoCoeff( 3, 3) 
MacroSetElServoCoeff( 4, 4) 
MacroSetElServoCoeff( 5, 5) 
MacroSetElServoCoeff( 6, 6) 
MacroSetElServoCoeff( 7, 7) 
MacroSetElServoCoeff( 8, 8) 
MacroSetElServoCoeff( 9, 9) 
MacroSetElServoCoeff(10, A) 
MacroSetElServoCoeff(11, B) 
MacroSetElServoCoeff(12, C)
MacroSetElServoCoeff(13, D) 
MacroSetElServoCoeff(14, E)  
MacroSetElServoCoeff(15, F)

#undef MacroSetElServoCoeff

void MountACACommonHWSimImpl::setControlSetAzServoDefault(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetAzServoDefault";
  ACS_TRACE(__METHOD__);

  // argument data always must be 1. It does not affect the value to set. 
  double defaultServoCoeff = 1.1; 
  std::vector<CAN::byte_t> dat2;
  TypeConversion::valueToData(dat2, defaultServoCoeff);

  setControlSetAzServoCoeff0(dat2);   setControlSetAzServoCoeff1(dat2); 
  setControlSetAzServoCoeff2(dat2);   setControlSetAzServoCoeff3(dat2); 
  setControlSetAzServoCoeff4(dat2);   setControlSetAzServoCoeff5(dat2); 
  setControlSetAzServoCoeff6(dat2);   setControlSetAzServoCoeff7(dat2); 
  setControlSetAzServoCoeff8(dat2);   setControlSetAzServoCoeff9(dat2); 
  setControlSetAzServoCoeffA(dat2);   setControlSetAzServoCoeffB(dat2); 
  setControlSetAzServoCoeffC(dat2);   setControlSetAzServoCoeffD(dat2); 
  setControlSetAzServoCoeffE(dat2);   setControlSetAzServoCoeffF(dat2); 
}

void MountACACommonHWSimImpl::setControlSetElServoDefault(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetElServoCoeff0";
   ACS_TRACE(__METHOD__);

  // argument data always must be 1. It does not affect the value to set. 
  double defaultServoCoeff = 2.1; 
  std::vector<CAN::byte_t> dat2;
  TypeConversion::valueToData(dat2, defaultServoCoeff);

  setControlSetElServoCoeff0(dat2);   setControlSetElServoCoeff1(dat2); 
  setControlSetElServoCoeff2(dat2);   setControlSetElServoCoeff3(dat2); 
  setControlSetElServoCoeff4(dat2);   setControlSetElServoCoeff5(dat2); 
  setControlSetElServoCoeff6(dat2);   setControlSetElServoCoeff7(dat2); 
  setControlSetElServoCoeff8(dat2);   setControlSetElServoCoeff9(dat2); 
  setControlSetElServoCoeffA(dat2);   setControlSetElServoCoeffB(dat2); 
  setControlSetElServoCoeffC(dat2);   setControlSetElServoCoeffD(dat2); 
  setControlSetElServoCoeffE(dat2);   setControlSetElServoCoeffF(dat2); 
}
//-----End of get/set AZ(EL) Servo Coeffcient 0-F -------------


//-----Stow pin--------------------------------------------
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorStowPin() const 
{
  std::vector<CAN::byte_t> data;
  CAN::byte_t azStowPin = static_cast<CAN::byte_t>(drive_m.Az->get_stow_pin());
  CAN::byte_t elStowPin = static_cast<CAN::byte_t>(drive_m.El->get_stow_pin());
  data.push_back(azStowPin);
  data.push_back(elStowPin);
  associate(monitorPoint_STOW_PIN, data);
  return MountACACommonHWSimBase::getMonitorStowPin();
}

void MountACACommonHWSimImpl::setControlSetStowPin(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setStowPin";
  ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetStowPin(data);  
  CAN::byte_t azStowPin = data[0];
  CAN::byte_t elStowPin = data[1];

  drive_m.Az->set_stow_pin(static_cast<Stow_Pin_t>(azStowPin));
  drive_m.El->set_stow_pin(static_cast<Stow_Pin_t>(elStowPin));
}
//-----End of Stow pin--------------------------------------------


//---------------------------------------------------------------------------
// Version. 1.0 2008-06-02
//
// ACA antenna specific hardware simulation code
// modified by Masahiko Tasaki
//
//---------------------------------------------------------------------------
//---------------------------------------------------------------------
// By the way, associate() is declared in /Mount/src/MountHWSimImpl.cpp
//void MountHWSimImpl::associate(const rca_t RCA, const std::vector<CAN::byte_t>& data) const
//{
//  (state_m.find(RCA)->second)->clear();
//  *(state_m.find(RCA)->second)=data;
//}
//---------------------------------------------------------------------

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAzStatus() const
{
// 8bytes  ubyte[8]
// byte 0: limited status
// bit0:2nd limit CW, 1:2nd limit CCW, 2:1st limit CW, 3:1st limit CCW
//    4:pre-limit CW, 5:pre-limit CCW, 6:limint warnng CW,7:lim w. CCW
// byte 1: 1st limit. override switch. 
// bit0:invalid direction. 1:override switch enabled, 2-7:rsvd
// byte 2: drive system failure
// bit0:AZ-L1 DPA fault, 1:AZ-L2 DPA fault, 2:AZ-R1 DPA f 3:AZ-R2 DPA f
//    4:AZ-L1 motor overheat, 5:AZ-L2 same, 6:AZ-R1 same, 7:AZ-R2 same
// byte 3: capacitor bank status
// bit0:AZ-L1 capacitor bank fault, 1:AZ-L2 cbf, 2:AZ-R1 cbf, 3:AZ-R2cbf
//    4:AZ-L1 capacitor bank full, 5:AZ-L2 same,6;AZ-R1 same,7:AZ-R2same
// byte 4: servo condition
// bit0:AZ-L1 excessive current, 1:AZ-L2 same, 2:AZ-R1 same, 3:AZ-R2same
//    4:servo oscillation, 5:runaway, 6:over speed, 7:angle input fault
// byte5: drive system failure
// bit0:AZ-L DCPA rack circuit braker off, 1:AZ-R same, 2:rsvd 
//    3:AZ-All encoder alarm, 4:AZ-LF same, 5:AZ-LR same, 6:AZ-RF same
//    7:AZ-RR same
// byte6: brake condition
// bit0-3,6-7:rsvd, 4:Brake power failure, 5:brake power fuse blow,
// byte7: misc status
// bit0:survival stow position, 1:maintenance stow position
//    2-3,6:rsvd, 4:cable overlap CW, 5:cable overlap CCW, 
//    7:other AZ fault (see monitorPoint_AZ_STATUS_2)
// default 8 bytes x 0
// survival stow pos(byte7bit0) and maintenance stow pos(byte7bit1).
// Survival and maintenance are set by ACU_MODE_CMD(0x1022)
// bit0-3:AZ, bit4-7:EL  value=0x4 survival, 0x5=maintenance stow
// Say this status will be set in internal vars azStatus, elStatus.

  std::vector<CAN::byte_t> data; // = MountACACommonHWSimBase::getAzStatus();
  // The value of data will not be used. This is just for debug. 

//---------------------
//  std::vector<uint8_t> azStatus8 = drive_m.getAzStatus();
/*
  unsigned long long azStatus8;
  drive_m.getAzStatus( azStatus8 );  
  TypeConversion::valueToData(data, azStatus8);	

  associate(monitorPoint_AZ_STATUS, data);
*/
  return MountACACommonHWSimBase::getMonitorAzStatus();
}


std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorAzStatus2() const
{
//  NOT COMPLETED YET. IT TEMPORALILY RETURNS 0.
// 8bytes default = 8bytes x 0
// byte 0: drive system failure
// bit0:AZ-LF encoder disconnected, 1:AZ-LR enc. discon, 2:AZ-RF enc. disconn,
// 3:AZ-RR enc. disconn. 4:AZ-LF encoder not initialized, 5:AZ-LR enc. not init,
// 6:AZ-RF enc. not init. 7:AZ-RR enc. not init.
// byte 1: drive system failure.
// bit0:AZ-L1 DPA discon. 1:AZ-L2 DPA discon. 2:AZ-R1 DPA discon., 3:AZ-R2 DPA. 
// 4-7:rscv
// byte 2: drive system failure
// bit0:AZ-L1 DPA contactor off, 1:AZ-L2 c. off. 2:AZ-R1 c. off, 3:AZ-R2 c. off.
// 4:AZ-L1 DPA discharge, 5:AZ-L2 discharge. 6:AZ-R1 dischage, 7:AZ-R2 discharge.
// byte 3: servo condition
// bit0:position filter fault, 1:major filter fault, 2:minor filter fault,
// 3:feedback filter fault. 4-7: rsvd
// byte 4: drive on timeout
// bit0:AZ-L1 drive on timeout, 1:AZ-L2 d.o TO, 2:AZ-R1 d.o.TO, 3: AZ-R2 d.o.TO
// 4:AZ-L1 power on timeout, 5:AZ-L2 p.o TO, 6:AZ-R1 p.o.TO, 7: AZ-R2 p.o.TO
// byte 5: brake axis failure
// bit0:brake axis DPA fault, 1:brake axis DPA circuit braker off, 
// 2:brake axis DPA contactor off, 3:brake axis resolver disconnected
// 4:brake axis async, 5-6:rsvd, 7:brake axis drive on timeout
// byte 6-7: rsvd
/*
  std::vector<CAN::byte_t> data; 
  unsigned long long azStatus8;
  drive_m.getAzStatus2( azStatus8 );  
  TypeConversion::valueToData(data, azStatus8);	

  associate(monitorPoint_AZ_STATUS_2, data);
*/
  return MountACACommonHWSimBase::getMonitorAzStatus2();
}

// 2008-07-01 M.Tasaki We must use the function in MountACACommonHWSimBase.cpp
// 2008-07-02          However, ones in MountACACommonHWSimBase do not work. 
//std::vector<CAN::byte_t> MountACACommonHWSimImpl::getAzAuxMode() const 
//{
// ubyte  default 1byte x 0  AZ motor selection 
// 0x0 Normal, 0x01 Half-L1/L2, 0x2 Half-R1/R2, 0x3 Half L1/R1,
// 0x4 Half L2/R2, 0x5 Quarter-L1, 0x6 Quarter-L2, 0x7 Q-R1, 0x8 Q-R2

/** 2008-07-02 Do not use drive.
   std::vector<CAN::byte_t> data ;
   unsigned char azAuxmode1;
   drive_m.getAzAuxMode( azAuxmode1 );  
   TypeConversion::valueToData(data, azAuxmode1);	

   associate(monitorPoint_AZ_AUX_MODE, data);
*/
//  return MountACACommonHWSimBase::getAzAuxMode();
//}

//std::vector<CAN::byte_t> MountACACommonHWSimImpl::getAzRatefdbkMode() const 
//{
// ubyte  default 1byte x 0
// bit0:AZ-LF encoder disabled, 1:AZ-LR enc. disabl. 2:AZ-RF e.d. 3:AZ-RR e.d.
// 4:AZ-LF encoder is in gap angle, 4:AZ-LR enc. gap. 2:AZ-RF e.g. 3:AZ-RR e.g.
// bit0-3 is set by setAzRatefdbkMode(), bit 4-7 is alwasy set to 0 for simulation.

/*2008-07-02 Do not use drive. 
  std::vector<CAN::byte_t> data ;
  unsigned char azRatefdbkMode1;
  drive_m.getAzRatefdbkMode( azRatefdbkMode1 );  
  TypeConversion::valueToData(data, azRatefdbkMode1);	

  associate(monitorPoint_AZ_RATEFDBK_MODE, data);
*/
//  return MountACACommonHWSimBase::getAzRatefdbkMode();
//}


std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorElStatus() const
{
// byte0:limit status
// bit0:2nd limit UP, 1:2nd limit DN, 2:1st limit UP, 3:1st limit DN,
//    4:prelimit UP, 5:prelimit DN, 6:limit warning UP, 7:limit warn DN
// byte1:1st limit override
// byte2:drive system failure
// byte3:capacitor bank status
// byte4:servo condition
// byte5:drive system failure
// byte6:brake condition
// byte7: misc status
// bit0:survival stow position, 1:maintenance stow position
//    2:Zenith position, 
//    7:other EL fault (see monitorPoint_EL_STATUS_2)
// default 8 bytes x 0

/*2008-07-02 Do not use drive. 
  std::vector<CAN::byte_t> data; // = MountACACommonHWSimBase::getMonitorElStatus();
  // The value of data will not be used. This is just for debug. 
  unsigned long long elStatus8;
  drive_m.getMonitorElStatus( elStatus8 );  
  TypeConversion::valueToData(data, elStatus8);	

  associate(monitorPoint_EL_STATUS, data);
*/
  return MountACACommonHWSimBase::getMonitorElStatus();
}

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorElStatus2() const 
{
// 8bytes default = 8bytes x 0
// byte0:drive system failure
// byte1:drive system failure
// byte2:drive system failure
// byte3:servo condition
// byte4:drive on timeout
// byte5:brake axis failure
// byte6-7:rsvd
/*2008-07-02 Do not use drive. 
  std::vector<CAN::byte_t> data; 
  unsigned long long elStatus8;
  drive_m.getMonitorElStatus2( elStatus8 );  
  TypeConversion::valueToData(data, elStatus8);	

  associate(monitorPoint_EL_STATUS_2, data);
*/
  return MountACACommonHWSimBase::getMonitorElStatus2();
}


//std::vector<CAN::byte_t> MountACACommonHWSimImpl::getElAuxMode() const 
//{
// ubyte  default 1byte x 0  EL motor selection 
// 0x0 Normal, 0x01 Half-L, 0x2 Half-R
/*2008-07-02 Do not use drive. 
  std::vector<CAN::byte_t> data ;
  unsigned char elAuxmode1;
  drive_m.getElAuxMode( elAuxmode1 );  
  TypeConversion::valueToData(data, elAuxmode1);	

  associate(monitorPoint_EL_AUX_MODE, data);
*/
//  return MountACACommonHWSimBase::getElAuxMode();
//}
//std::vector<CAN::byte_t> MountACACommonHWSimImpl::getElRatefdbkMode() const 
//{
// ubyte  default 1byte x 0  
// bit0:EL-F encoder disabled
// bit1:EL-R encoder disabled
// bit2-7: rsvd
/*2008-07-02 Do not use drive. 
  std::vector<CAN::byte_t> data ;
  unsigned char elRatefdbkMode1;
  drive_m.getElRatefdbkMode( elRatefdbkMode1 );  
  TypeConversion::valueToData(data, elRatefdbkMode1);	

  associate(monitorPoint_EL_RATEFDBK_MODE, data);
*/
//  return MountACACommonHWSimBase::getElRatefdbkMode();
//}


std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorSystemStatus() const 
{
// ubyte  default 8byte x 0
// byte0: alarms
// byte1: safety interlock
// byte2: door interlock
// byte3: handle interlock
// byte4: stow status
// byte5: shutter status
// byte6: misc status
// byte7: misc status

/* 
  std::vector<CAN::byte_t> data; 
  unsigned long long systemStatus8 = 0;
 //Not a drive function. drive_m.getMonitorSystemStatus( systemStatus8 );  
  TypeConversion::valueToData(data, systemStatus8);	

  associate(monitorPoint_SYSTEM_STATUS, data);
*/
  return MountACACommonHWSimBase::getMonitorSystemStatus();
}

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorSystemStatus2() const 
{
// ubyte  default 8byte x 0
// byte0: ACU fault
// byte1: ACU fault
// byte2: ACU fault
// byte3: PCU
// byte4: drive system failure(DIFC:DPA interface card)
// byte5: dirve system failure

/*
  std::vector<CAN::byte_t> data; 
  unsigned long long systemStatus8 = 0;
  TypeConversion::valueToData(data, systemStatus8);	

  associate(monitorPoint_SYSTEM_STATUS_2, data);
*/
  return MountACACommonHWSimBase::getMonitorSystemStatus2();
}

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorSubrefModeRsp() const 
{
// ubyte  default 1byte x 0   subref operation mode
// 0x0: shutdown, 0x2: encoder, 0x3: autonomous, 0x7: link reset
  std::vector<CAN::byte_t> data; 
  TypeConversion::valueToData(data, drive_m.subrefMode1);	

  associate(monitorPoint_SUBREF_MODE_RSP, data);
  return MountACACommonHWSimBase::getMonitorSubrefModeRsp();
}


//
//std::vector<CAN::byte_t> MountACACommonHWSimImpl::getSubrefLimits() const 
//{
// 8bytes default = 8bytes x 0
// byte 0:   bit0:X axis positive sw limit, 1:X axis negative sw limit, 
// 2-3:rsvd, 4:Link-1 positive limit. 5:Link-1 negative limit,
// 6:Link-1 out of range, 7:rsvd
// byte 1:   0:Y axis positive sw limit, 1:Y axis negative sw limit, 2-3:rsvd,
// 4:Link-2 posi. limit. 5:Link-2 nega. limit, 6:Link-2 out of range, 7:rsvd
// byte 2:   0:Z axis positive sw limit, 1:Z axis negative sw limit, 2-3:rsvd,
// 4:Link-3 posi. limit. 5:Link-3 nega. limit, 6:Link-3 out of range, 7:rsvd
// byte 3:   0:0x positive sw limit, 1:0x negative sw limit, 2-3:rsvd,
// 4:Link-4 posi. limit. 5:Link-4 nega. limit, 6:Link-4 out of range, 7:rsvd
// byte 4:   0:0y positive sw limit, 1:0y negative sw limit, 2-3:rsvd,
// 4:Link-5 posi. limit. 5:Link-5 nega. limit, 6:Link-5 out of range, 7:rsvd
// byte 5:   0:0z positive sw limit, 1:0z negative sw limit, 2-3:rsvd,
// 4:Link-6 posi. limit. 5:Link-6 nega. limit, 6:Link-6 out of range, 7:rsvd
// byte 6:   0:Collision limit inside, 1:Collision limit outside,
// 2:Collision limit disabled, 3-7:rsvd
// byte 7: rsvd 

//}

//std::vector<CAN::byte_t> MountACACommonHWSimImpl::getSubrefStatus() const 
//{
// 8bytes default = 8bytes x 0
// byte 0: bit0:Link-1 brake,1:Link-2 brake,2:Link-3 brake,3:Link-4 brake,
// 4:Link-5 brake, 5:Link-6 brake, 6-7:rsvd
// byte 1: bit0:Link-1 DPA alarm(F),1:Link-2 DPA alarm(F),2:Link-3 DPA a(F),
// 3:Link-4 DPA alarm(F),4:Link-5 DPA alarm(F),5:Link-6 DPA a(F),6-7:rsvd
// byte 2: 
// byte 3: 
// byte 4: 
// byte 5: 
// byte 6-7: rsvd
 
//}

//--------------Use MountACACommonHWSimBase. No need to impl.------
//std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorSubrefPtCoeff00--1F() const {}
// double   A1(unit in mm)


std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrEquipStatus() const 
{
// ubyte  default 8byte x 0
// byte0: data logger and metrology status
// byte1: gap sensor and linear scale
// byte2: linear sensor 9-16
// byte3: linear sensor 17
// byte4-7:rsvd
  std::vector<CAN::byte_t> data; 
  unsigned long long metrEquipStatus8 = 0;
  TypeConversion::valueToData(data, metrEquipStatus8);	

  associate(monitorPoint_METR_EQUIP_STATUS, data);
  return MountACACommonHWSimBase::getMonitorMetrEquipStatus();
}

//---------------Use default value 0. No need to impl.---
// std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMetrDispl06--17() const {}
// double   Displacement sensor 6

//-----Always return predetermined value.-----------------
//-----Return 299.92 for temp between the sensor 30-4F.---
// std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps1A--4F() const {}
// 8bytes  int16 x 4  default 0
//   value is in multiples of 0.01 degC.
// -299.90: -overflow 299.90:+overflow. 299.91: disconnected. 
// 299.92: No censor or censor disabled.
// byte0-1:Temperature sensor Number 0x1A*4
//     2-3:Temperature sensor Number 0x1A*4+1
//     4-5:Temperature sensor Number 0x1A*4+2
//     6-7:Temperature sensor Number 0x1A*4+3
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps1a() const
{
  std::vector<CAN::byte_t> data;
  std::vector<uint16_t> testData(4);
  testData[0] = 1101;  testData[1] = 1102;  testData[2] = 1103; testData[3] = 1104;

  TypeConversion::valueToData(data, testData);
  associate(monitorPoint_METR_TEMPS_1A, data);
  return MountACACommonHWSimBase::getMonitorMetrTemps1a();
}
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps1b() const
{
  std::vector<CAN::byte_t> data;
  std::vector<uint16_t> testData(4);
  testData[0] = 1111;  testData[1] = 1112;  testData[2] = 1113; testData[3] = 1114;

  TypeConversion::valueToData(data, testData);
  associate(monitorPoint_METR_TEMPS_1B, data);
  return MountACACommonHWSimBase::getMonitorMetrTemps1b();
}
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps1c() const
{
  std::vector<CAN::byte_t> data;
  std::vector<uint16_t> testData(4);
  testData[0] = 1121;  testData[1] = 1122;  testData[2] = 1123; testData[3] = 1124;

  TypeConversion::valueToData(data, testData);
  associate(monitorPoint_METR_TEMPS_1C, data);
  return MountACACommonHWSimBase::getMonitorMetrTemps1c();
}
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps1d() const
{
  std::vector<CAN::byte_t> data;
  std::vector<uint16_t> testData(4);
  testData[0] = 1131;  testData[1] = 1132;  testData[2] = 1133; testData[3] = 1134;

  TypeConversion::valueToData(data, testData);
  associate(monitorPoint_METR_TEMPS_1D, data);
  return MountACACommonHWSimBase::getMonitorMetrTemps1d();
}
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps1e() const
{
  std::vector<CAN::byte_t> data;
  std::vector<uint16_t> testData(4);
  testData[0] = 1141;  testData[1] = 1142;  testData[2] = 1143; testData[3] = 1144;

  TypeConversion::valueToData(data, testData);
  associate(monitorPoint_METR_TEMPS_1E, data);
  return MountACACommonHWSimBase::getMonitorMetrTemps1e();
}
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps1f() const
{
  std::vector<CAN::byte_t> data;
  std::vector<uint16_t> testData(4);
  testData[0] = 1151;  testData[1] = 1152;  testData[2] = 1153; testData[3] = 1154;

  TypeConversion::valueToData(data, testData);	
  associate(monitorPoint_METR_TEMPS_1F, data);
  return MountACACommonHWSimBase::getMonitorMetrTemps1f();
}

#define MacroGetMetrTemps(sLower, sUpper, num) \
std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorMetrTemps##sLower() const \
{  \
  std::vector<CAN::byte_t> data; \
  std::vector<uint16_t> testData(4); \
  testData[0] = num;  testData[1] = num;  testData[2] = num; testData[3] = num; \
  TypeConversion::valueToData(data, testData);	\
  associate(monitorPoint_METR_TEMPS_##sUpper, data); \
  return MountACACommonHWSimBase::getMonitorMetrTemps##sLower(); \
} \
 
MacroGetMetrTemps(20, 20, 1200)
MacroGetMetrTemps(21, 21, 1210)
MacroGetMetrTemps(22, 22, 1220)
MacroGetMetrTemps(23, 23, 1230)
MacroGetMetrTemps(24, 24, 1240)
MacroGetMetrTemps(25, 25, 1250)
MacroGetMetrTemps(26, 26, 1260)
MacroGetMetrTemps(27, 27, 1270)
MacroGetMetrTemps(28, 28, 1280)
MacroGetMetrTemps(29, 29, 1290)
MacroGetMetrTemps(2a, 2A, 1300)
MacroGetMetrTemps(2b, 2B, 1310)
MacroGetMetrTemps(2c, 2C, 1320)
MacroGetMetrTemps(2d, 2D, 1330)
MacroGetMetrTemps(2e, 2E, 1340)
MacroGetMetrTemps(2f, 2F, 1350)

//--------------------------------------------------------------
// From 30 to 4F, Return value 29992, which means no sensor installed.
//--------------------------------------------------------------
MacroGetMetrTemps(30, 30, 29992)
MacroGetMetrTemps(31, 31, 29992)
MacroGetMetrTemps(32, 32, 29992)
MacroGetMetrTemps(33, 33, 29992)
MacroGetMetrTemps(34, 34, 29992)
MacroGetMetrTemps(35, 35, 29992)
MacroGetMetrTemps(36, 36, 29992)
MacroGetMetrTemps(37, 37, 29992)
MacroGetMetrTemps(38, 38, 29992)
MacroGetMetrTemps(39, 39, 29992)
MacroGetMetrTemps(3a, 3A, 29992)
MacroGetMetrTemps(3b, 3B, 29992)
MacroGetMetrTemps(3c, 3C, 29992)
MacroGetMetrTemps(3d, 3D, 29992)
MacroGetMetrTemps(3e, 3E, 29992)
MacroGetMetrTemps(3f, 3F, 29992)
MacroGetMetrTemps(40, 40, 29992)
MacroGetMetrTemps(41, 41, 29992)
MacroGetMetrTemps(42, 42, 29992)
MacroGetMetrTemps(43, 43, 29992)
MacroGetMetrTemps(44, 44, 29992)
MacroGetMetrTemps(45, 45, 29992)
MacroGetMetrTemps(46, 46, 29992)
MacroGetMetrTemps(47, 47, 29992)
MacroGetMetrTemps(48, 48, 29992)
MacroGetMetrTemps(49, 49, 29992)
MacroGetMetrTemps(4a, 4A, 29992)
MacroGetMetrTemps(4b, 4B, 29992)
MacroGetMetrTemps(4c, 4C, 29992)
MacroGetMetrTemps(4d, 4D, 29992)
MacroGetMetrTemps(4e, 4E, 29992)
MacroGetMetrTemps(4f, 4F, 29992)

#undef MacroGetMetrTemps

//--------------Use MountACACommonHWSimBase. No need to impl.----
// std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMetrCoeff00--1F() const {}
// double 
//---------------------------------------------------


//---------------Use default value 0. No need to impl.---
// std::vector<CAN::byte_t> MountACACommonHWSimImpl::getPowerStatus() const {}
// 3bytes  ubyte x 3  default 0
// byte0: power
// bit0:base power,1:transporter power,2:AC power failure,3:AC abnormal phase
// byte1: UPS-1
// bit0:UPS-1 disconnected,1:UPS-1 alarm,2:UPS-1 input failure,3:UPS-1 battery low
// byte2: UPS-2
// bit0:UPS-2 disconnected,1:UPS-2 alarm,2:UPS-2 input failure,3:UPS-2 battery low
//--------------------------------------------------

//---------------Use default value 0. No need to impl.---
// std::vector<CAN::byte_t> MountACACommonHWSimImpl::getAcStatus() const {}
// 1byte  ubyte x 1  default 0
// bit0:HVAC alarm,1:HVAC temperature down,2:HVAC disconnected,3:rsvd
//    4:control cabin A/C alarm,5:control cabin A/C temperature alarm,
//    6:base A/C alarm, 7:base A/C temperature alarm
//-------------------------------------

//---------------Use default value 0. No need to impl.---
// std::vector<CAN::byte_t> MountACACommonHWSimImpl::getFanStatus() const {}
// 6byte  ubyte x 6  default 0x0  x 6
// byte0: alarm
// bit0:ACU fan alarm,1:R/D cpnverter fan alarm,2:sebreflector controller fan 
//   alarm, 3:ACR fan alarm,4:subreflector rack fan alarm,5:nutator rack fan
//   alarm, 6:AZ DCPA rack fan alarm, 7:EL DCPA rack fan alarm
// byte1: DPA alarm
// bit0:AZ-L1 DPA fan alarm, 1:AZ-L2 DPA, 2:AZ-R1 DPA, 3:AZ-R2 DPA fan, 
//    3:EL-L DPA, 4:EL-L DPA, 5:El-R DPA, 6:subreflector DPA alarm, 7:rsvd
// byte2: rsvd
// byte3: main reflector fan alarm
// bit0:fan alarm 1, 1: ..2, 2: ..3, 3: ..4,
//    4:fan alarm 5, 5: ..6, 6: ..7, 7: ..8
// byte4: main reflector fan alarm
// bit0:fan alarm 9,  1: ..10, 2: ..11, 3: ..12,
//    4:fan alarm 13, 5: ..14, 6: ..15, 7: ..16
// byte5: york fan alarm
// bit0:yoke-L fan alarm,  1:yoke-R fan alarm, 2-7:rsvd
//---------------------------------------

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorUpsOutputVolts2() const
{
// 6byte  int16 x 3  LSB=0.01V   default 0  x 3
// bit0-1:Output voltage phase 1(V)
// bit2-3:Output voltage phase 2(V)
// bit4-5:Output voltage phase 3(V)

  std::vector<uint16_t> voltsData(3);
  voltsData[0] = 2100;  //  21.00V
  voltsData[1] = 2200;  //  22.00V
  voltsData[2] = 2300;  //  23.00V

  std::vector<CAN::byte_t> data;
  TypeConversion::valueToData(data, voltsData);
  associate(monitorPoint_UPS_OUTPUT_VOLTS_2, data);
  return MountACACommonHWSimBase::getMonitorUpsOutputVolts2();
}

std::vector<CAN::byte_t> MountACACommonHWSimImpl::getMonitorUpsOutputCurrent2() const
{
// 6byte  int16 x 3  LSB=0.01A    default 0  x 3
// bit0-1:Output current phase 1(V)
// bit2-3:Output current phase 2(V)
// bit4-5:Output current phase 3(V)

  std::vector<uint16_t> currData(3);
  currData[0] = 1100;  //  11.00A
  currData[1] = 1200;  //  12.00A
  currData[2] = 1300;  //  13.00A

  std::vector<CAN::byte_t> data;
  TypeConversion::valueToData(data, currData);
  associate(monitorPoint_UPS_OUTPUT_CURRENT_2, data);
  return MountACACommonHWSimBase::getMonitorUpsOutputCurrent2();
}

//------------------------------------------------------------------
//
//
//
//
//  Set
//
//
//
//
//------------------------------------------------------------------

//------- End of common commnads whose code my code will override-------------
// 2008-07-01 M.Tasaki We must use the function in MountACACommonHWSimBase.cpp
// 2008-07-02          However, ones in MountACACommonHWSimBase do not work. 
void MountACACommonHWSimImpl::setControlSetAzAuxMode(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetAzAuxMode";
   ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetAzAuxMode(data);
  associate(monitorPoint_AZ_AUX_MODE, data);
}

void MountACACommonHWSimImpl::setControlSetAzRatefdbkMode(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetAzRatefdbkMode";
   ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetAzRatefdbkMode(data);
  associate(monitorPoint_AZ_RATEFDBK_MODE, data);
}

void MountACACommonHWSimImpl::setControlSetElAuxMode(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetElAuxMode";
   ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetElAuxMode(data);
  associate(monitorPoint_EL_AUX_MODE, data);
}

void MountACACommonHWSimImpl::setControlSetElRatefdbkMode(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetElRatefdbkMode";
   ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetElRatefdbkMode(data);
  associate(monitorPoint_EL_RATEFDBK_MODE, data);
}


void MountACACommonHWSimImpl::setControlSetShutter(const std::vector<CAN::byte_t>& data)
{
  const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetShutter";
  ACS_TRACE(__METHOD__);

  // parameter 1Byte: 0x0: close shutter, 0x1: open shutter 
  unsigned char setShutterValue, cnvdSetShutterValue;
  AMB::TypeConversion::dataToValue(data, setShutterValue);  
  std::vector<CAN::byte_t> data2;
  
  if ( setShutterValue == 0x00 ) { // close command.
     cnvdSetShutterValue = 0x02 ; // close status
  } else if ( setShutterValue == 0x01 ) { // open command
     cnvdSetShutterValue = 0x01;
  } else {             // bad command
     cnvdSetShutterValue = 0x08;  // set error to shutter status;
  } 
        /*
        int iSet = (int)setShutterValue ; int iGet = (int) cnvdSetShutterValue;
        char A1[100], A2[100]; 
        sprintf( A1," input setShuetter Val.=%d", iSet );  
        sprintf( A2," out setShutter Val.= %d",   iGet );
        std::cout << "ACA-HWSimImpl:setShutter:" << A1 << A2 << std::endl;
        */
  AMB::TypeConversion::valueToData( data2, cnvdSetShutterValue);
  MountHWSimBase::setMonitorShutter( data2 );
}

//------- End of common commnads whose code my code will override-------------


void MountACACommonHWSimImpl::setControlSetPtDefault(const std::vector<CAN::byte_t>& data) {
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetPtDefault";
   ACS_TRACE(__METHOD__);

// Use setControlSetPtModelCoeffnn in MountProductionHWSimImpl.cpp.
// controlPoint_PT_MODEL_COEFFnn(00-1F) will take a 8 byte parameter of type of double.
// argument data must be always 0x01.

  double defaultPtCoeff = 0; //
  std::vector<CAN::byte_t> defv;
  TypeConversion::valueToData(defv, defaultPtCoeff);

  setControlSetPtModelCoeff00(defv);   setControlSetPtModelCoeff01(defv);
  setControlSetPtModelCoeff02(defv);   setControlSetPtModelCoeff03(defv);
  setControlSetPtModelCoeff04(defv);   setControlSetPtModelCoeff05(defv);
  setControlSetPtModelCoeff06(defv);   setControlSetPtModelCoeff07(defv);
  setControlSetPtModelCoeff08(defv);   setControlSetPtModelCoeff09(defv);
  setControlSetPtModelCoeff0a(defv);   setControlSetPtModelCoeff0b(defv);
  setControlSetPtModelCoeff0c(defv);   setControlSetPtModelCoeff0d(defv);
  setControlSetPtModelCoeff0e(defv);   setControlSetPtModelCoeff0f(defv);

  setControlSetPtModelCoeff10(defv);   setControlSetPtModelCoeff11(defv);
  setControlSetPtModelCoeff12(defv);   setControlSetPtModelCoeff13(defv);
  setControlSetPtModelCoeff14(defv);   setControlSetPtModelCoeff15(defv);
  setControlSetPtModelCoeff16(defv);   setControlSetPtModelCoeff17(defv);
  setControlSetPtModelCoeff18(defv);   setControlSetPtModelCoeff19(defv);
  setControlSetPtModelCoeff1a(defv);   setControlSetPtModelCoeff1b(defv);
  setControlSetPtModelCoeff1c(defv);   setControlSetPtModelCoeff1d(defv);
  setControlSetPtModelCoeff1e(defv);   setControlSetPtModelCoeff1f(defv);
}

void MountACACommonHWSimImpl::setSubrefModeCmd(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setSubrefModeCmd";
   ACS_TRACE(__METHOD__);

// 0x0: shutdown, 0x2: encoder, 0x3: autonomous, 0x7: link reset
  unsigned char subrefmode;

  TypeConversion::dataToValue(data, subrefmode);	

  drive_m.subrefMode1 = subrefmode ;  
//  MountACACommonHWSimBase::setControlSetSubrefModeCmd(data);
  associate(monitorPoint_SUBREF_MODE_RSP, data);
}

// KA
void MountACACommonHWSimImpl::setSubrefDeltaZeroCmd(
    const std::vector< CAN::byte_t >& data)
{
    checkSize(data, 1U, "controlPoint_SUBREF_DELTA_ZERO_CMD");
    if(state_m.find(controlPoint_SUBREF_DELTA_ZERO_CMD) != state_m.end())
    {
        *(state_m.find(controlPoint_SUBREF_DELTA_ZERO_CMD)->second) = data;
    }
    else
    {
        throw CAN::Error("Trying to set controlPoint_SUBREF_DELTA_ZERO_CMD. Member not found.");
    }
    std::vector<uint16_t> position(3);
    position[0] = position[1] = position[2] = 0;
    std::vector<CAN::byte_t> data2;
    TypeConversion::valueToData(data2, position);
    setControlSetSubrefDeltaPosn(data2);
}

// Specific Monitor set helpers
// ----------------------------------------------------------------------------


//void MountACACommonHWSimImpl::setControlSetSubrefPtCoeff00--1f(const std::vector<CAN::byte_t>& data)
void MountACACommonHWSimImpl::setControlSetSubrefPtCoeff00(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetSubrefPtCoeff00";
   ACS_TRACE(__METHOD__);

  //double x = 123459;
  //std::vector<CAN::byte_t> data2;
  //TypeConversion::valueToData(data2, x);
  MountACACommonHWSimBase::setControlSetSubrefPtCoeff00(data);
  associate(monitorPoint_SUBREF_PT_COEFF_00, data);
}

#define MacroSetSubrefPtCoeff(sLower, sUpper) \
void MountACACommonHWSimImpl::setControlSetSubrefPtCoeff##sLower(const std::vector<CAN::byte_t>& data) \
 {  \
    MountACACommonHWSimBase::setControlSetSubrefPtCoeff##sLower(data); \
    associate(monitorPoint_SUBREF_PT_COEFF_##sUpper, data);  \
 } \
 
MacroSetSubrefPtCoeff(01,01)
MacroSetSubrefPtCoeff(02,02)
MacroSetSubrefPtCoeff(03,03)
MacroSetSubrefPtCoeff(04,04)
MacroSetSubrefPtCoeff(05,05)
MacroSetSubrefPtCoeff(06,06)
MacroSetSubrefPtCoeff(07,07)
MacroSetSubrefPtCoeff(08,08)
MacroSetSubrefPtCoeff(09,09)
MacroSetSubrefPtCoeff(0a,0A)
MacroSetSubrefPtCoeff(0b,0B)
MacroSetSubrefPtCoeff(0c,0C)
MacroSetSubrefPtCoeff(0d,0D)
MacroSetSubrefPtCoeff(0e,0E)
MacroSetSubrefPtCoeff(0f,0F)
MacroSetSubrefPtCoeff(10,10)
MacroSetSubrefPtCoeff(11,11)
MacroSetSubrefPtCoeff(12,12)
MacroSetSubrefPtCoeff(13,13)
MacroSetSubrefPtCoeff(14,14)
MacroSetSubrefPtCoeff(15,15)
MacroSetSubrefPtCoeff(16,16)
MacroSetSubrefPtCoeff(17,17)
MacroSetSubrefPtCoeff(18,18)
MacroSetSubrefPtCoeff(19,19)
MacroSetSubrefPtCoeff(1a,1A)
MacroSetSubrefPtCoeff(1b,1B)
MacroSetSubrefPtCoeff(1c,1C)
MacroSetSubrefPtCoeff(1d,1D)
MacroSetSubrefPtCoeff(1e,1E)
MacroSetSubrefPtCoeff(1f,1F)

#undef MacroSetSubrefPtCoeff
//------------------------------------------------------

void MountACACommonHWSimImpl::setControlSetSubrefPtDefault(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetSubrefPtDefault";
   ACS_TRACE(__METHOD__);

//  8byte : double
  double raw = 3; //to see if it is working.
  std::vector<CAN::byte_t> data2;
  //it is strage, but this statement fails. commandTE returns code=2.
  // Fails at MountACACommonBase.cpp::setCntlSubrefPtDefault.  
  // TypeConversion::dataToValue(data2, raw);
  data2 = MountACACommonHWSimBase::getMonitorSubrefPtCoeff00();
  //  code above is just for debug.
  //      data2[0] = 11;  sign+exp(11bit)+d(52bit) the 1st byte was sign+exp(7bits)

  TypeConversion::valueToData(data2, raw);  
  setMonitorSubrefPtCoeff00(data2);     setMonitorSubrefPtCoeff01(data2);
  setMonitorSubrefPtCoeff02(data2);     setMonitorSubrefPtCoeff03(data2);
  setMonitorSubrefPtCoeff04(data2);     setMonitorSubrefPtCoeff05(data2);
  setMonitorSubrefPtCoeff06(data2);     setMonitorSubrefPtCoeff07(data2);
  setMonitorSubrefPtCoeff08(data2);     setMonitorSubrefPtCoeff09(data2);
  setMonitorSubrefPtCoeff0a(data2);     setMonitorSubrefPtCoeff0b(data2);
  setMonitorSubrefPtCoeff0c(data2);     setMonitorSubrefPtCoeff0d(data2);
  setMonitorSubrefPtCoeff0e(data2);     setMonitorSubrefPtCoeff0f(data2);
  setMonitorSubrefPtCoeff10(data2);     setMonitorSubrefPtCoeff11(data2);
  setMonitorSubrefPtCoeff12(data2);     setMonitorSubrefPtCoeff13(data2);
  setMonitorSubrefPtCoeff14(data2);     setMonitorSubrefPtCoeff15(data2);
  setMonitorSubrefPtCoeff16(data2);     setMonitorSubrefPtCoeff17(data2);
  setMonitorSubrefPtCoeff18(data2);     setMonitorSubrefPtCoeff19(data2);
  setMonitorSubrefPtCoeff1a(data2);     setMonitorSubrefPtCoeff1b(data2);
  setMonitorSubrefPtCoeff1c(data2);     setMonitorSubrefPtCoeff1d(data2);
  setMonitorSubrefPtCoeff1e(data2);     setMonitorSubrefPtCoeff1f(data2);
}


//void MountACACommonHWSimImpl::setControlSetMetrCoeff00--1f(const std::vector<CAN::byte_t>& data)
void MountACACommonHWSimImpl::setControlSetMetrCoeff00(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetMetrCoeff00";
   ACS_TRACE(__METHOD__);

  MountACACommonHWSimBase::setControlSetMetrCoeff00(data);
  associate(monitorPoint_METR_COEFF_00, data); 
}

#define MacroSetMetrCoeff(sLower, sUpper) \
void MountACACommonHWSimImpl::setControlSetMetrCoeff##sLower(const std::vector<CAN::byte_t>& data) \
 {  \
    MountACACommonHWSimBase::setControlSetMetrCoeff##sLower(data); \
    associate(monitorPoint_METR_COEFF_##sUpper, data);  \
 } \
 
MacroSetMetrCoeff(01,01)
MacroSetMetrCoeff(02,02)
MacroSetMetrCoeff(03,03)
MacroSetMetrCoeff(04,04)
MacroSetMetrCoeff(05,05)
MacroSetMetrCoeff(06,06)
MacroSetMetrCoeff(07,07)
MacroSetMetrCoeff(08,08)
MacroSetMetrCoeff(09,09)
MacroSetMetrCoeff(0a,0A)
MacroSetMetrCoeff(0b,0B)
MacroSetMetrCoeff(0c,0C)
MacroSetMetrCoeff(0d,0D)
MacroSetMetrCoeff(0e,0E)
MacroSetMetrCoeff(0f,0F)
MacroSetMetrCoeff(10,10)
MacroSetMetrCoeff(11,11)
MacroSetMetrCoeff(12,12)
MacroSetMetrCoeff(13,13)
MacroSetMetrCoeff(14,14)
MacroSetMetrCoeff(15,15)
MacroSetMetrCoeff(16,16)
MacroSetMetrCoeff(17,17)
MacroSetMetrCoeff(18,18)
MacroSetMetrCoeff(19,19)
MacroSetMetrCoeff(1a,1A)
MacroSetMetrCoeff(1b,1B)
MacroSetMetrCoeff(1c,1C)
MacroSetMetrCoeff(1d,1D)
MacroSetMetrCoeff(1e,1E)
MacroSetMetrCoeff(1f,1F)

#undef MacroSetMetrCoeff
//----------------------------------------------------------------------------

void MountACACommonHWSimImpl::setControlSetMetrDefault(const std::vector<CAN::byte_t>& data)
{
   const char *__METHOD__ = "MountACACommonHWSimImpl::setControlSetMetrDefault";
   ACS_TRACE(__METHOD__);

// Reset all Metr_Coeffs(8byte each, double) x 32(00-1F)
// input value must be 0x01 
// data must be always 0x01.

  double raw = 1; //try to set 1.0 to see if it is working.
  std::vector<CAN::byte_t> data2;
  AMB::TypeConversion::valueToData(data2, raw);

  setControlSetMetrCoeff00(data2); setControlSetMetrCoeff01(data2); setControlSetMetrCoeff02(data2);
  setControlSetMetrCoeff03(data2); setControlSetMetrCoeff04(data2); setControlSetMetrCoeff05(data2);
  setControlSetMetrCoeff06(data2); setControlSetMetrCoeff07(data2); setControlSetMetrCoeff08(data2);
  setControlSetMetrCoeff09(data2); setControlSetMetrCoeff0a(data2); setControlSetMetrCoeff0b(data2);
  setControlSetMetrCoeff0c(data2); setControlSetMetrCoeff0d(data2); setControlSetMetrCoeff0e(data2);
  setControlSetMetrCoeff0f(data2);
  setControlSetMetrCoeff10(data2); setControlSetMetrCoeff11(data2); setControlSetMetrCoeff12(data2);
  setControlSetMetrCoeff13(data2); setControlSetMetrCoeff14(data2); setControlSetMetrCoeff15(data2);
  setControlSetMetrCoeff16(data2); setControlSetMetrCoeff17(data2); setControlSetMetrCoeff18(data2);
  setControlSetMetrCoeff19(data2); setControlSetMetrCoeff1a(data2); setControlSetMetrCoeff1b(data2);
  setControlSetMetrCoeff1c(data2); setControlSetMetrCoeff1d(data2); setControlSetMetrCoeff1e(data2);
  setControlSetMetrCoeff1f(data2);
}

//----------------------------------------------------------------------------

void MountACACommonHWSimImpl::setControlSetPtModelCoeff10(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff10(data);
  associate(monitorPoint_PT_MODEL_COEFF_10, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff11(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff11(data);
  associate(monitorPoint_PT_MODEL_COEFF_11, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff12(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff12(data);
  associate(monitorPoint_PT_MODEL_COEFF_12, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff13(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff13(data);
  associate(monitorPoint_PT_MODEL_COEFF_13, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff14(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff14(data);
  associate(monitorPoint_PT_MODEL_COEFF_14, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff15(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff15(data);
  associate(monitorPoint_PT_MODEL_COEFF_15, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff16(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff16(data);
  associate(monitorPoint_PT_MODEL_COEFF_16, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff17(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff17(data);
  associate(monitorPoint_PT_MODEL_COEFF_17, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff18(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff18(data);
  associate(monitorPoint_PT_MODEL_COEFF_18, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff19(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff19(data);
  associate(monitorPoint_PT_MODEL_COEFF_19, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff1a(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff1a(data);
  associate(monitorPoint_PT_MODEL_COEFF_1A, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff1b(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff1b(data);
  associate(monitorPoint_PT_MODEL_COEFF_1B, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff1c(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff1c(data);
  associate(monitorPoint_PT_MODEL_COEFF_1C, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff1d(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff1d(data);
  associate(monitorPoint_PT_MODEL_COEFF_1D, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff1e(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff1e(data);
  associate(monitorPoint_PT_MODEL_COEFF_1E, data);
}

void MountACACommonHWSimImpl::setControlSetPtModelCoeff1f(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetPtModelCoeff1f(data);
  associate(monitorPoint_PT_MODEL_COEFF_1F, data);
}

void MountACACommonHWSimImpl::setControlSetMetrMode(const std::vector<CAN::byte_t>& data)
{
  MountACACommonHWSimBase::setControlSetMetrMode(data);
  associate(monitorPoint_METR_MODE, data);
}

//------controlPoint_METR_CALIBRATION does not affect other. No need to imple.---
//void MountACACommonHWSimImpl::setControlSetMetrCalibration(const std::vector<CAN::byte_t>& data)
//------------------------------------------------------------------

//------controlPoint_AC_TEMP does not affect other.  No need to imple.----
//void MountACACommonHWSimImpl::setControlSetAcTemp(const std::vector<CAN::byte_t>& data)
//1 byte  unit8
// HVAC temperature (range 16 - 22 degC)
//----------------------------------------


