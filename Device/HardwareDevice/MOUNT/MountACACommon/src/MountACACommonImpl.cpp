// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Nobeyama Radio Observatory - NAOJ, 2008
// (c) Associated Universities Inc., 2009, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#include "MountACACommonImpl.h"
#include <maciACSComponentDefines.h>
#include <string>
#include <vector>

//using Control::MountACACommonImpl;
using log_audience::OPERATOR;
using ControlExceptions::CAMBErrorExImpl;
using ControlExceptions::CAMBErrorEx;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::INACTErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::HardwareErrorEx;
using std::string;
using std::ostringstream;
using Control::LongSeq;

//// GET_SYSTEM_STATUS
/// Byte 0
const int MountACACommonImpl::SYSTEM_STATUS_EMERGENCY_STOP          = 3000;
const int MountACACommonImpl::SYSTEM_STATUS_STAIRWAY_INTERLOCK      = 3001;
const int MountACACommonImpl::SYSTEM_STATUS_HANDLING_INTERLOCK      = 3002;
const int MountACACommonImpl::SYSTEM_STATUS_SMOKE_ALARM             = 3003;
const int MountACACommonImpl::SYSTEM_STATUS_ACU_FAULT               = 3004;
const int MountACACommonImpl::SYSTEM_STATUS_SURVIVAL_FOR_MISSING_COMMANDS= 3005;
const int MountACACommonImpl::SYSTEM_STATUS_SURVIVAL_FOR_MISSING_TIMING= 3006;
const int MountACACommonImpl::SYSTEM_STATUS_TIMING_PULSE            = 3007;
/// Byte 1: sfety interlock
const int MountACACommonImpl::SYSTEM_STATUS_RCV_CABIN_SAFETY        = 3010;
const int MountACACommonImpl::SYSTEM_STATUS_ANT_BASE_SAFETY         = 3011;
const int MountACACommonImpl::SYSTEM_STATUS_VERANDA_EQUIP_FREE      = 3012;
const int MountACACommonImpl::SYSTEM_STATUS_LADDER_SWITCH           = 3013;
const int MountACACommonImpl::SYSTEM_STATUS_TRANSP_INTLK            = 3014;
const int MountACACommonImpl::SYSTEM_STATUS_TRANSP_INTLK_EXCPT_AZ   = 3015;
// bit6-7: reserved
/// Byte 2: door interlock
const int MountACACommonImpl::SYSTEM_STATUS_RCV_CABIN_DOOR_OPEN     = 3020;
const int MountACACommonImpl::SYSTEM_STATUS_CNTR_CABIN_DOOR_OPEN    = 3021;
const int MountACACommonImpl::SYSTEM_STATUS_ANT_BASE_DOOR_OPEN      = 3022;
const int MountACACommonImpl::SYSTEM_STATUS_UPS_ROOM_DOOR_OPEN      = 3023;
const int MountACACommonImpl::SYSTEM_STATUS_PDB_ROOM_DOOR_OPEN      = 3024;
const int MountACACommonImpl::SYSTEM_STATUS_MAIN_REF_HATCH_OPEN     = 3025;
const int MountACACommonImpl::SYSTEM_STATUS_YOKE_L_HATCH_OPEN       = 3026;
const int MountACACommonImpl::SYSTEM_STATUS_YOKE_R_HATCH_OPEN       = 3027;
/// Byte 3: handle interlock
const int MountACACommonImpl::SYSTEM_STATUS_AZ_HNDL_INTLK           = 3030;
const int MountACACommonImpl::SYSTEM_STATUS_EL_HNDL_INTLK           = 3031;
const int MountACACommonImpl::SYSTEM_STATUS_AZ_STOW_HNDL_INTLK      = 3032;
const int MountACACommonImpl::SYSTEM_STATUS_EL_STOW_HNDL_INTLK      = 3033;
const int MountACACommonImpl::SYSTEM_STATUS_SHUTTER_HNDL_INTLK      = 3034;
// bit5-7: reserved
/// Byte 4: stow status
const int MountACACommonImpl::SYSTEM_STATUS_AZ_STOW_TO              = 3040;
const int MountACACommonImpl::SYSTEM_STATUS_AZ_STOW_THERMAL_TRIP    = 3041;
const int MountACACommonImpl::SYSTEM_STATUS_STOW_DPA_CB_OFF         = 3042;
const int MountACACommonImpl::SYSTEM_STATUS_STOW_DPA_COND_OFF       = 3043;
const int MountACACommonImpl::SYSTEM_STATUS_EL_STOW_TO              = 3044;
const int MountACACommonImpl::SYSTEM_STATUS_EL_STOW_THERMAL_TRIP    = 3045;
// bit6-7: reserved
/// Byte 5: shutter status
const int MountACACommonImpl::SYSTEM_STATUS_SHUTTER_TO              = 3050;
const int MountACACommonImpl::SYSTEM_STATUS_SHUTTER_THERMAL_TRIP    = 3051;
const int MountACACommonImpl::SYSTEM_STATUS_SHUTTER_DPA_CB_OFF      = 3052;
const int MountACACommonImpl::SYSTEM_STATUS_SHUTTER_DPA_COND_OFF    = 3053;
// bit4-6: reserved
const int MountACACommonImpl::SYSTEM_STATUS_ZENITH_SHUTTER_OPEN     = 3057;
/// Byte 6: misc status
const int MountACACommonImpl::SYSTEM_STATUS_RD_CONV_FAIL            = 3060;
const int MountACACommonImpl::SYSTEM_STATUS_IF_PANEL_FAIL           = 3061;
const int MountACACommonImpl::SYSTEM_STATUS_ACR2_PS_ALARM           = 3062;
const int MountACACommonImpl::SYSTEM_STATUS_NUTATOR_RACK_PS_ALARM   = 3063;
const int MountACACommonImpl::SYSTEM_STATUS_BRAKE_AXIS_HEATER_ALARM = 3064;
const int MountACACommonImpl::SYSTEM_STATUS_MOTOR_PUMP_FAIL         = 3065;
const int MountACACommonImpl::SYSTEM_STATUS_ARRESTER_BROKEN_ALARM   = 3066;
const int MountACACommonImpl::SYSTEM_STATUS_DC_POWER_CIRCUIT_ALARM  = 3067;
/// Byte 7: misc status
const int MountACACommonImpl::SYSTEM_STATUS_ACU_MAINT               = 3070;
const int MountACACommonImpl::SYSTEM_STATUS_ANT_STATION_POWER_DISCON= 3071;
const int MountACACommonImpl::SYSTEM_STATUS_ANT_INTLK               = 3072;
const int MountACACommonImpl::SYSTEM_STATUS_NUTATOR_RACK_CB_OFF     = 3073;
const int MountACACommonImpl::SYSTEM_STATUS_DRIVE_SYSTEM_FAIL       = 3076;
const int MountACACommonImpl::SYSTEM_STATUS_MORE_PCU_CONNECT        = 3077;

//// GET_SYSTEM_STATUS_2
/// Byte 0 (ubyte): ACU fault
const int MountACACommonImpl::SYSTEM_STATUS_DC12V_FAIL              = 3100;
const int MountACACommonImpl::SYSTEM_STATUS_SYS_FAIL                = 3101;
const int MountACACommonImpl::SYSTEM_STATUS_AC_FAIL                 = 3102;
const int MountACACommonImpl::SYSTEM_STATUS_DATA_FAULT              = 3103;
const int MountACACommonImpl::SYSTEM_STATUS_AD_CONV_FAIL            = 3104;
// bit5: reserved
const int MountACACommonImpl::SYSTEM_STATUS_CAN_BOARD_FAIL          = 3106;
const int MountACACommonImpl::SYSTEM_STATUS_CLOCK_BOARD_FAIL        = 3107;
/// Byte 1 (ubyte): ACU fault
const int MountACACommonImpl::SYSTEM_STATUS_DSP_FAIL                = 3110;
const int MountACACommonImpl::SYSTEM_STATUS_DSP_AD_CONV_FAIL        = 3111;
// bit2-7: reserved
/// Byte 2 (ubyte): ACU fault
const int MountACACommonImpl::SYSTEM_STATUS_IMP1_FAIL               = 3120;
const int MountACACommonImpl::SYSTEM_STATUS_IMP2_FAIL               = 3121;
const int MountACACommonImpl::SYSTEM_STATUS_IMP3_FAIL               = 3122;
const int MountACACommonImpl::SYSTEM_STATUS_IMP4_FAIL               = 3123;
const int MountACACommonImpl::SYSTEM_STATUS_IMP5_FAIL               = 3124;
const int MountACACommonImpl::SYSTEM_STATUS_IMP6_FAIL               = 3125;
const int MountACACommonImpl::SYSTEM_STATUS_IMP7_FAIL               = 3126;
// bit7: reserved
/// Byte 3 (ubyte): PCU
const int MountACACommonImpl::SYSTEM_STATUS_PCU1_CONNECTED          = 3130;
const int MountACACommonImpl::SYSTEM_STATUS_PCU2_CONNECTED          = 3131;
const int MountACACommonImpl::SYSTEM_STATUS_PCU3_CONNECTED          = 3132;
// bit3-7: reserved
/// Byte 4 (ubyte): drive system failure (DIFC : DPA Interface Card)
// bit0-5: reserved
const int MountACACommonImpl::SYSTEM_STATUS_DIFC_FAULT              = 3146;
const int MountACACommonImpl::SYSTEM_STATUS_DIFC_POWER_FAIL         = 3147;
/// Byte 5 (ubyte): drive system failure
const int MountACACommonImpl::SYSTEM_STATUS_ACU2DIFC_LINE1_DISCON   = 3150;
const int MountACACommonImpl::SYSTEM_STATUS_ACU2DIFC_LINE2_DISCON   = 3151;
const int MountACACommonImpl::SYSTEM_STATUS_DIFC2ACU_LINE1_DISCON   = 3152;
const int MountACACommonImpl::SYSTEM_STATUS_DIFC2ACU_LINE2_DISCON   = 3153;
const int MountACACommonImpl::SYSTEM_STATUS_ACU2DIFC_LINE3_DISCON   = 3154;
// bit5-7: reserved
/// Bytes 6-7: reserved

//// GET_AZ_STATUS
/// Byte 0 (ubyte): limit status
const int MountACACommonImpl::AZ_STATUS_LIMIT2_CW                   = 3200;
const int MountACACommonImpl::AZ_STATUS_LIMIT2_CCW                  = 3201;
const int MountACACommonImpl::AZ_STATUS_LIMIT1_CW                   = 3202;
const int MountACACommonImpl::AZ_STATUS_LIMIT1_CCW                  = 3203;
const int MountACACommonImpl::AZ_STATUS_PRE_LIMIT_CW                = 3204;
const int MountACACommonImpl::AZ_STATUS_PRE_LIMIT_CCW               = 3205;
const int MountACACommonImpl::AZ_STATUS_LIMIT_WARN_CW               = 3206;
const int MountACACommonImpl::AZ_STATUS_LIMIT_WARN_CCW              = 3207;
/// Byte 1 (ubyte): 1st limit, override switch
const int MountACACommonImpl::AZ_STATUS_INVALID_DIR                 = 3210;
const int MountACACommonImpl::AZ_STATUS_OVERRIDE_SW_ENABLED         = 3211;
// bit2-7: reserved
/// Byte 2 (ubyte): drive system failure
const int MountACACommonImpl::AZ_STATUS_AZ_L1_DPA_FAULT             = 3220;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_DPA_FAULT             = 3221;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_DPA_FAULT             = 3222;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_DPA_FAULT             = 3223;
const int MountACACommonImpl::AZ_STATUS_AZ_L1_MOTOR_OH              = 3224;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_MOTOR_OH              = 3225;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_MOTOR_OH              = 3226;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_MOTOR_OH              = 3227;
/// Byte 3 (ubyte): capacitor bank status
const int MountACACommonImpl::AZ_STATUS_AZ_L1_CBANK_FAULT           = 3230;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_CBANK_FAULT           = 3231;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_CBANK_FAULT           = 3232;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_CBANK_FAULT           = 3233;
const int MountACACommonImpl::AZ_STATUS_AZ_L1_CBANK_FULL            = 3234;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_CBANK_FULL            = 3235;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_CBANK_FULL            = 3236;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_CBANK_FULL            = 3237;
/// Byte 4 (ubyte): servo condition
const int MountACACommonImpl::AZ_STATUS_AZ_L1_EXCSV_CURR            = 3240;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_EXCSV_CURR            = 3241;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_EXCSV_CURR            = 3242;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_EXCSV_CURR            = 3243;
const int MountACACommonImpl::AZ_STATUS_SERVO_OSCILLATION           = 3244;
const int MountACACommonImpl::AZ_STATUS_RUNAWAY                     = 3245;
const int MountACACommonImpl::AZ_STATUS_OVERSPEED                   = 3246;
const int MountACACommonImpl::AZ_STATUS_ANGLE_INPUT_FAULT           = 3247;
/// Byte 5 (ubyte): drive system failure
const int MountACACommonImpl::AZ_STATUS_AZ_L_DCPA_CB_OFF            = 3250;
const int MountACACommonImpl::AZ_STATUS_AZ_R_DCPA_CB_OFF            = 3251;
// bit2: reserved
const int MountACACommonImpl::AZ_STATUS_AZ_ALL_ENCODER_ALARM        = 3253;
const int MountACACommonImpl::AZ_STATUS_AZ_LF_ENCODER_ALARM         = 3254;
const int MountACACommonImpl::AZ_STATUS_AZ_LR_ENCODER_ALARM         = 3255;
const int MountACACommonImpl::AZ_STATUS_AZ_RF_ENCODER_ALARM         = 3256;
const int MountACACommonImpl::AZ_STATUS_AZ_RR_ENCODER_ALARM         = 3257;
/// Byte 6 (ubyte): brake condition
// bit0-3: reserved
const int MountACACommonImpl::AZ_STATUS_BRAKE_POWER_FAIL            = 3264;
const int MountACACommonImpl::AZ_STATUS_BRAKE_PS_FUSE_BLOW          = 3265;
// bit6-7: reserved
/// Byte 7 (ubyte): misc status
const int MountACACommonImpl::AZ_STATUS_SURVIVAL_STOW_POS           = 3270;
const int MountACACommonImpl::AZ_STATUS_MAINT_STOW_POS              = 3271;
// bit2-3: reserved
const int MountACACommonImpl::AZ_STATUS_CABLE_OVERLAP_CW            = 3274;
const int MountACACommonImpl::AZ_STATUS_CABLE_OVERLAP_CCW           = 3275;
// bit6: reserved
const int MountACACommonImpl::AZ_STATUS_OTHER_FAULT                 = 3277;

//// GET_AZ_STATUS_2
/// Byte 0 (ubyte): drive system failure
const int MountACACommonImpl::AZ_STATUS_AZ_LF_ENCDR_DISCON          = 3300;
const int MountACACommonImpl::AZ_STATUS_AZ_LR_ENCDR_DISCON          = 3301;
const int MountACACommonImpl::AZ_STATUS_AZ_RF_ENCDR_DISCON          = 3302;
const int MountACACommonImpl::AZ_STATUS_AZ_RR_ENCDR_DISCON          = 3303;
const int MountACACommonImpl::AZ_STATUS_AZ_LF_ENCDR_NOT_INIT        = 3304;
const int MountACACommonImpl::AZ_STATUS_AZ_LR_ENCDR_NOT_INIT        = 3305;
const int MountACACommonImpl::AZ_STATUS_AZ_RF_ENCDR_NOT_INIT        = 3306;
const int MountACACommonImpl::AZ_STATUS_AZ_RR_ENCDR_NOT_INIT        = 3307;
/// Byte 1 (ubyte): drive system failure
const int MountACACommonImpl::AZ_STATUS_AZ_L1_DPA_DISCON            = 3310;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_DPA_DISCON            = 3311;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_DPA_DISCON            = 3312;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_DPA_DISCON            = 3313;
// bit4-7: reserved
/// Byte 2 (ubyte): drive system failure
const int MountACACommonImpl::AZ_STATUS_AZ_L1_DPA_COND_OFF          = 3320;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_DPA_COND_OFF          = 3321;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_DPA_COND_OFF          = 3322;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_DPA_COND_OFF          = 3323;
const int MountACACommonImpl::AZ_STATUS_AZ_L1_DPA_DISCHARGE         = 3324;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_DPA_DISCHARGE         = 3325;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_DPA_DISCHARGE         = 3326;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_DPA_DISCHARGE         = 3327;
/// Byte 3 (ubyte): servo condition
const int MountACACommonImpl::AZ_STATUS_POSN_FILTER_FAULT           = 3330;
const int MountACACommonImpl::AZ_STATUS_MAJOR_FILTER_FAULT          = 3331;
const int MountACACommonImpl::AZ_STATUS_MINOR_FILTER_FAULT          = 3332;
const int MountACACommonImpl::AZ_STATUS_FDBK_FILTER_FAULT           = 3333;
// bit4-7: reserved
/// Byte 4 (ubyte): drive on timeout
const int MountACACommonImpl::AZ_STATUS_AZ_L1_DRIVE_ON_TO           = 3340;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_DRIVE_ON_TO           = 3341;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_DRIVE_ON_TO           = 3342;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_DRIVE_ON_TO           = 3343;
const int MountACACommonImpl::AZ_STATUS_AZ_L1_POWER_ON_TO           = 3344;
const int MountACACommonImpl::AZ_STATUS_AZ_L2_POWER_ON_TO           = 3345;
const int MountACACommonImpl::AZ_STATUS_AZ_R1_POWER_ON_TO           = 3346;
const int MountACACommonImpl::AZ_STATUS_AZ_R2_POWER_ON_TO           = 3347;
/// Byte 5 (ubyte): brake axis failure
const int MountACACommonImpl::AZ_STATUS_BRAKE_AXIS_DPA_FAULT        = 3350;
const int MountACACommonImpl::AZ_STATUS_BRAKE_AXIS_DPA_CB_OFF       = 3351;
const int MountACACommonImpl::AZ_STATUS_BRAKE_AXIS_DPA_COND_OFF     = 3352;
const int MountACACommonImpl::AZ_STATUS_BRAKE_AXIS_RSLV_DISCON      = 3353;
const int MountACACommonImpl::AZ_STATUS_BRAKE_AXIS_ASYNC            = 3354;
// bit5-6: reserved
const int MountACACommonImpl::AZ_STATUS_BRAKE_AXIS_DRIVE_ON_TO      = 3357;
/// Bytes 6-7: reserved

//// GET_EL_STATUS
/// Byte 0 (ubyte): limit status
const int MountACACommonImpl::EL_STATUS_LIMIT2_UP                   = 3400;
const int MountACACommonImpl::EL_STATUS_LIMIT2_DOWN                 = 3401;
const int MountACACommonImpl::EL_STATUS_LIMIT1_UP                   = 3402;
const int MountACACommonImpl::EL_STATUS_LIMIT1_DOWN                 = 3403;
const int MountACACommonImpl::EL_STATUS_PRE_LIMIT_UP                = 3404;
const int MountACACommonImpl::EL_STATUS_PRE_LIMIT_DOWN              = 3405;
const int MountACACommonImpl::EL_STATUS_LIMIT_WARN_UP               = 3406;
const int MountACACommonImpl::EL_STATUS_LIMIT_WARN_DOWN             = 3407;
/// Byte 1 (ubyte): 1st limit, override
const int MountACACommonImpl::EL_STATUS_INVALID_DIR                 = 3410;
const int MountACACommonImpl::EL_STATUS_OVERRIDE_SW_ENABLED         = 3411;
// bit2-7: reserved
/// Byte 2 (ubyte): drive system failure
const int MountACACommonImpl::EL_STATUS_EL_L_DPA_FAULT              = 3420;
const int MountACACommonImpl::EL_STATUS_EL_R_DPA_FAULT              = 3421;
// bit2-3: reserved
const int MountACACommonImpl::EL_STATUS_EL_L_MOTOR_OH               = 3424;
const int MountACACommonImpl::EL_STATUS_EL_R_MOTOR_OH               = 3425;
// bit6-7: reserved
/// Byte 3 (ubyte): capacitor bank status
const int MountACACommonImpl::EL_STATUS_EL_L_CBANK_FAULT            = 3430;
const int MountACACommonImpl::EL_STATUS_EL_R_CBANK_FAULT            = 3431;
// bit2-3: reserved
const int MountACACommonImpl::EL_STATUS_EL_L_CBANK_FULL             = 3434;
const int MountACACommonImpl::EL_STATUS_EL_R_CBANK_FULL             = 3435;
// bit6-7: reserved
/// Byte 4 (ubyte): servo condition
const int MountACACommonImpl::EL_STATUS_EL_L_EXCSV_CURR             = 3440;
const int MountACACommonImpl::EL_STATUS_EL_R_EXCSV_CURR             = 3441;
// bit2-3: reserved
const int MountACACommonImpl::EL_STATUS_SERVO_OSCILLATION           = 3444;
const int MountACACommonImpl::EL_STATUS_RUNAWAY                     = 3445;
const int MountACACommonImpl::EL_STATUS_OVERSPEED                   = 3446;
const int MountACACommonImpl::EL_STATUS_ANGLE_INPUT_FAULT           = 3447;
/// Byte 5 (ubyte): drive system failure
const int MountACACommonImpl::EL_STATUS_DCPA_CB_OFF                 = 3450;
//const int MountACACommonImpl::EL_STATUS_DCPA_CB_OFF                 = 3451;
const int MountACACommonImpl::EL_STATUS_EL_ALL_ENCDR_ALARM          = 3453;
const int MountACACommonImpl::EL_STATUS_EL_F_ENCDR_ALARM            = 3454;
const int MountACACommonImpl::EL_STATUS_EL_R_ENCDR_ALARM            = 3455;
// bit6-7: reserved
/// Byte 6 (ubyte): brake condition
// bit0-3: reserved
const int MountACACommonImpl::EL_STATUS_BRAKE_POWER_FAIL            = 3464;
const int MountACACommonImpl::EL_STATUS_BRAKE_PS_FUSE_BLOW          = 3465;
// bit6-7: reserved
/// Byte 7 (ubyte): misc status
const int MountACACommonImpl::EL_STATUS_SURVIVAL_STOW_POS           = 3470;
const int MountACACommonImpl::EL_STATUS_MAINT_STOW_POS              = 3471;
const int MountACACommonImpl::EL_STATUS_ZENITH_POS                  = 3472;
// bit3-6: reserved
const int MountACACommonImpl::EL_STATUS_OTHER_FAULT                 = 3477;

/// GET_EL_STATUS_2
// Byte 0 (ubyte): drive system failure
const int MountACACommonImpl::EL_STATUS_EL_F_ENCDR_DISCON           = 3500;
const int MountACACommonImpl::EL_STATUS_EL_R_ENCDR_DISCON           = 3501;
// bit2-3: reserved
const int MountACACommonImpl::EL_STATUS_EL_F_ENCDR_NOT_INIT         = 3504;
const int MountACACommonImpl::EL_STATUS_EL_R_ENCDR_NOT_INIT         = 3505;
// bit6-7: reserved
/// Byte 1 (ubyte): drive system failure
const int MountACACommonImpl::EL_STATUS_EL_L_DPA_DISCON             = 3510;
const int MountACACommonImpl::EL_STATUS_EL_R_DPA_DISCON             = 3511;
// bit2-7: reserved
/// Byte 2 (ubyte): drive system failure
const int MountACACommonImpl::EL_STATUS_EL_L_DPA_COND_OFF           = 3520;
const int MountACACommonImpl::EL_STATUS_EL_R_DPA_COND_OFF           = 3521;
// bit2-3: reserved
const int MountACACommonImpl::EL_STATUS_EL_L_DPA_DISCHARGE          = 3524;
const int MountACACommonImpl::EL_STATUS_EL_R_DPA_DISCHARGE          = 3525;
// bit6-7: reserved
/// Byte 3 (ubyte): servo condition
const int MountACACommonImpl::EL_STATUS_POSN_FILTER_FAULT           = 3530;
const int MountACACommonImpl::EL_STATUS_MAJOR_FILTER_FAULT          = 3531;
const int MountACACommonImpl::EL_STATUS_MINOR_FILTER_FAULT          = 3532;
const int MountACACommonImpl::EL_STATUS_FDBK_FILTER_FAULT           = 3533;
// bit4-7: reserved
/// Byte 4 (ubyte): drive on timeout
const int MountACACommonImpl::EL_STATUS_EL_L_DRIVE_ON_TO            = 3540;
const int MountACACommonImpl::EL_STATUS_EL_R_DRIVE_ON_TO            = 3541;
// bit2-3: reserved
const int MountACACommonImpl::EL_STATUS_EL_L_POWER_ON_TO            = 3544;
const int MountACACommonImpl::EL_STATUS_EL_R_POWER_ON_TO            = 3545;
// bit6-7: reserved
/// Byte 5 (ubyte): brake axis failure
const int MountACACommonImpl::EL_STATUS_BRAKE_AXIS_DPA_FAULT        = 3550;
const int MountACACommonImpl::EL_STATUS_BRAKE_AXIS_DPA_CB_OFF       = 3551;
const int MountACACommonImpl::EL_STATUS_BRAKE_AXIS_DPA_COND_OFF     = 3552;
const int MountACACommonImpl::EL_STATUS_BRAKE_AXIS_RSLV_DISCON      = 3553;
const int MountACACommonImpl::EL_STATUS_BRAKE_AXIS_ASYNC            = 3554;
// bit5-6: reserved
const int MountACACommonImpl::EL_STATUS_BRAKE_AXIS_DRIVE_ON_TO      = 3557;
/// Bytes 6-7: reserved

MountACACommonImpl::MountACACommonImpl(const ACE_CString& name,
                           maci::ContainerServices* pCS)
    : MountACACommonBase(name,pCS)
{
    ACS_TRACE(__func__);

    // 20080706 M.Tasaki focusModel_m must initialize, otherwise the code
    //   will trap in MountImpl::writeLoop due to null ptr of focusModel_m.
    // focusModel_m = new MountACACommonFocusModel(this,getLogger());
    // This is bad
}

MountACACommonImpl::~MountACACommonImpl()
{
    ACS_TRACE(__func__);

    // 20080706 M.Tasaki focusModel_m was added due to jorge's change in June.
    //    if (focusModel_m != NULL) delete focusModel_m;
    //   This causes trap, because ~MountImpl() will delete focusModel()

}

double MountACACommonImpl::subrefXLimit() {
    return 0.007;
}

double MountACACommonImpl::subrefYLimit() {
    return subrefXLimit();
}

double MountACACommonImpl::subrefZLimit() {
    return 0.011;
}

void MountACACommonImpl::hwInitializeAction(){
    ACS_TRACE(__func__);
    MountACACommonBase::hwInitializeAction();

    // Set metrology mode to a default state
    try {
        SET_METR_CALIBRATION(1);

        LongSeq metrMode;
        metrMode.length(4);
        metrMode[0] = 0x16; // (bit 4 and bit 2 and bit 1)
        metrMode[1] = 0x3;  // (bit 1 and bit 0)
        metrMode[2] = 0;
        metrMode[3] = 0;
        SET_METR_MODE(metrMode);    // see AIV-1268
        setSubrefModeCmd(0x2); // Set the Subreflector to encoder.
    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as there is a communications problem with the ACU.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    }

    // Set receiver cabin temperature
    try {
      SET_AC_TEMP(receiverCabinTemp_m);
      ACS_LOG(LM_SOURCE_INFO,__func__,(LM_INFO,"Receiver Cabin Temperature set to %d[C]", receiverCabinTemp_m));
    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot set the receiver cabin temperature to the default value";
        msg += " as there is a communications problem with the ACU.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot set the receiver cabin temperature to the default value";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    }
}

void MountACACommonImpl::hwOperationalAction(){
    ACS_TRACE(__func__);
    MountACACommonBase::hwOperationalAction();

    // Send CLEAR_FAULT_CMD iff "timing pulse missing" is set
    try {
        std::vector< unsigned char > systemStatusRef;
        systemStatusRef.reserve(8);
        systemStatusRef[0] = 0x80; // only bit 7 should be set (10000000)
        // byte 1-7 should be all 0, including the unassigned bits
        systemStatusRef[1] = 0;
        systemStatusRef[2] = 0;
        systemStatusRef[3] = 0;
        systemStatusRef[4] = 0;
        systemStatusRef[5] = 0;
        systemStatusRef[6] = 0;
        systemStatusRef[7] = 0;

        ACS::Time ts;
        std::vector< unsigned char > systemStatus = getSystemStatus(ts);
        bool clearFault = true;
        for (int i=0; i<systemStatus.size(); i++) {
            if (systemStatus[i] != systemStatusRef[i]) {
                clearFault = false;
                ACS_LOG(LM_SOURCE_INFO, __func__, (LM_INFO, "not sending CLEAR_FAULT because systemStatus[%d]=%x", i, systemStatus[i]));
                break;
            }
        }
        if (clearFault) {
            LOG_TO_AUDIENCE(LM_INFO, __func__, "timing pulse missing, will send CLEAR_FAULT", OPERATOR);
            SET_CLEAR_FAULT_CMD();
        }
    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot check the system status or send CLEAR_FAULT_CMD";
        msg += " as there is a communications problem with the ACU.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot check the system status or send CLEAR_FAULT_CMD";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    }
}

void  MountACACommonImpl::configureAlarms() {

    // Configure all the alarms in the base (MountImpl) class.
    MountACACommonBase::configureAlarms();

    std::vector<Control::AlarmInformation> alarms(220);
    int i = 0;
    alarms[i++].alarmCode = SYSTEM_STATUS_EMERGENCY_STOP;
    alarms[i++].alarmCode = SYSTEM_STATUS_STAIRWAY_INTERLOCK;
    alarms[i++].alarmCode = SYSTEM_STATUS_HANDLING_INTERLOCK;
    alarms[i++].alarmCode = SYSTEM_STATUS_SMOKE_ALARM;
    alarms[i++].alarmCode = SYSTEM_STATUS_ACU_FAULT;
    alarms[i++].alarmCode = SYSTEM_STATUS_SURVIVAL_FOR_MISSING_COMMANDS;
    alarms[i++].alarmCode = SYSTEM_STATUS_SURVIVAL_FOR_MISSING_TIMING;
    alarms[i++].alarmCode = SYSTEM_STATUS_TIMING_PULSE;
    alarms[i++].alarmCode = SYSTEM_STATUS_RCV_CABIN_SAFETY;
    alarms[i++].alarmCode = SYSTEM_STATUS_ANT_BASE_SAFETY;
    alarms[i++].alarmCode = SYSTEM_STATUS_VERANDA_EQUIP_FREE;
    alarms[i++].alarmCode = SYSTEM_STATUS_LADDER_SWITCH;
    alarms[i++].alarmCode = SYSTEM_STATUS_TRANSP_INTLK;
    alarms[i++].alarmCode = SYSTEM_STATUS_TRANSP_INTLK_EXCPT_AZ;
    alarms[i++].alarmCode = SYSTEM_STATUS_RCV_CABIN_DOOR_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_CNTR_CABIN_DOOR_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_ANT_BASE_DOOR_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_UPS_ROOM_DOOR_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_PDB_ROOM_DOOR_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_MAIN_REF_HATCH_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_YOKE_L_HATCH_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_YOKE_R_HATCH_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_AZ_HNDL_INTLK;
    alarms[i++].alarmCode = SYSTEM_STATUS_EL_HNDL_INTLK;
    alarms[i++].alarmCode = SYSTEM_STATUS_AZ_STOW_HNDL_INTLK;
    alarms[i++].alarmCode = SYSTEM_STATUS_EL_STOW_HNDL_INTLK;
    alarms[i++].alarmCode = SYSTEM_STATUS_SHUTTER_HNDL_INTLK;
    alarms[i++].alarmCode = SYSTEM_STATUS_AZ_STOW_TO;
    alarms[i++].alarmCode = SYSTEM_STATUS_AZ_STOW_THERMAL_TRIP;
    alarms[i++].alarmCode = SYSTEM_STATUS_STOW_DPA_CB_OFF;
    alarms[i++].alarmCode = SYSTEM_STATUS_STOW_DPA_COND_OFF;
    alarms[i++].alarmCode = SYSTEM_STATUS_EL_STOW_TO;
    alarms[i++].alarmCode = SYSTEM_STATUS_EL_STOW_THERMAL_TRIP;
    alarms[i++].alarmCode = SYSTEM_STATUS_SHUTTER_TO;
    alarms[i++].alarmCode = SYSTEM_STATUS_SHUTTER_THERMAL_TRIP;
    alarms[i++].alarmCode = SYSTEM_STATUS_SHUTTER_DPA_CB_OFF;
    alarms[i++].alarmCode = SYSTEM_STATUS_SHUTTER_DPA_COND_OFF;
    alarms[i++].alarmCode = SYSTEM_STATUS_ZENITH_SHUTTER_OPEN;
    alarms[i++].alarmCode = SYSTEM_STATUS_RD_CONV_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IF_PANEL_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_ACR2_PS_ALARM;
    alarms[i++].alarmCode = SYSTEM_STATUS_NUTATOR_RACK_PS_ALARM;
    alarms[i++].alarmCode = SYSTEM_STATUS_BRAKE_AXIS_HEATER_ALARM;
    alarms[i++].alarmCode = SYSTEM_STATUS_MOTOR_PUMP_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_ARRESTER_BROKEN_ALARM;
    alarms[i++].alarmCode = SYSTEM_STATUS_DC_POWER_CIRCUIT_ALARM;
    alarms[i++].alarmCode = SYSTEM_STATUS_ACU_MAINT;
    alarms[i++].alarmCode = SYSTEM_STATUS_ANT_STATION_POWER_DISCON;
    alarms[i++].alarmCode = SYSTEM_STATUS_ANT_INTLK;
    alarms[i++].alarmCode = SYSTEM_STATUS_NUTATOR_RACK_CB_OFF;
    alarms[i++].alarmCode = SYSTEM_STATUS_DRIVE_SYSTEM_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_MORE_PCU_CONNECT;
    alarms[i++].alarmCode = SYSTEM_STATUS_DC12V_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_SYS_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_AC_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_DATA_FAULT;
    alarms[i++].alarmCode = SYSTEM_STATUS_AD_CONV_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_CAN_BOARD_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_CLOCK_BOARD_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_DSP_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_DSP_AD_CONV_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IMP1_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IMP2_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IMP3_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IMP4_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IMP5_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IMP6_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_IMP7_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_PCU1_CONNECTED;
    alarms[i++].alarmCode = SYSTEM_STATUS_PCU2_CONNECTED;
    alarms[i++].alarmCode = SYSTEM_STATUS_PCU3_CONNECTED;
    alarms[i++].alarmCode = SYSTEM_STATUS_DIFC_FAULT;
    alarms[i++].alarmCode = SYSTEM_STATUS_DIFC_POWER_FAIL;
    alarms[i++].alarmCode = SYSTEM_STATUS_ACU2DIFC_LINE1_DISCON;
    alarms[i++].alarmCode = SYSTEM_STATUS_ACU2DIFC_LINE2_DISCON;
    alarms[i++].alarmCode = SYSTEM_STATUS_DIFC2ACU_LINE1_DISCON;
    alarms[i++].alarmCode = SYSTEM_STATUS_DIFC2ACU_LINE2_DISCON;
    alarms[i++].alarmCode = SYSTEM_STATUS_ACU2DIFC_LINE3_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_LIMIT2_CW;
    alarms[i++].alarmCode = AZ_STATUS_LIMIT2_CCW;
    alarms[i++].alarmCode = AZ_STATUS_LIMIT1_CW;
    alarms[i++].alarmCode = AZ_STATUS_LIMIT1_CCW;
    alarms[i++].alarmCode = AZ_STATUS_PRE_LIMIT_CW;
    alarms[i++].alarmCode = AZ_STATUS_PRE_LIMIT_CCW;
    alarms[i++].alarmCode = AZ_STATUS_LIMIT_WARN_CW;
    alarms[i++].alarmCode = AZ_STATUS_LIMIT_WARN_CCW;
    alarms[i++].alarmCode = AZ_STATUS_INVALID_DIR;
    alarms[i++].alarmCode = AZ_STATUS_OVERRIDE_SW_ENABLED;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_DPA_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_DPA_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_DPA_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_DPA_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_MOTOR_OH;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_MOTOR_OH;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_MOTOR_OH;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_MOTOR_OH;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_CBANK_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_CBANK_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_CBANK_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_CBANK_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_CBANK_FULL;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_CBANK_FULL;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_CBANK_FULL;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_CBANK_FULL;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_EXCSV_CURR;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_EXCSV_CURR;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_EXCSV_CURR;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_EXCSV_CURR;
    alarms[i++].alarmCode = AZ_STATUS_SERVO_OSCILLATION;
    alarms[i++].alarmCode = AZ_STATUS_RUNAWAY;
    alarms[i++].alarmCode = AZ_STATUS_OVERSPEED;
    alarms[i++].alarmCode = AZ_STATUS_ANGLE_INPUT_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L_DCPA_CB_OFF;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R_DCPA_CB_OFF;
    alarms[i++].alarmCode = AZ_STATUS_AZ_ALL_ENCODER_ALARM;
    alarms[i++].alarmCode = AZ_STATUS_AZ_LF_ENCODER_ALARM;
    alarms[i++].alarmCode = AZ_STATUS_AZ_LR_ENCODER_ALARM;
    alarms[i++].alarmCode = AZ_STATUS_AZ_RF_ENCODER_ALARM;
    alarms[i++].alarmCode = AZ_STATUS_AZ_RR_ENCODER_ALARM;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_POWER_FAIL;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_PS_FUSE_BLOW;
    alarms[i++].alarmCode = AZ_STATUS_SURVIVAL_STOW_POS;
    alarms[i++].alarmCode = AZ_STATUS_MAINT_STOW_POS;
    alarms[i++].alarmCode = AZ_STATUS_CABLE_OVERLAP_CW;
    alarms[i++].alarmCode = AZ_STATUS_CABLE_OVERLAP_CCW;
    alarms[i++].alarmCode = AZ_STATUS_OTHER_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_LF_ENCDR_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_LR_ENCDR_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_RF_ENCDR_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_RR_ENCDR_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_LF_ENCDR_NOT_INIT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_LR_ENCDR_NOT_INIT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_RF_ENCDR_NOT_INIT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_RR_ENCDR_NOT_INIT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_DPA_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_DPA_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_DPA_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_DPA_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_DPA_COND_OFF;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_DPA_COND_OFF;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_DPA_COND_OFF;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_DPA_COND_OFF;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_DPA_DISCHARGE;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_DPA_DISCHARGE;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_DPA_DISCHARGE;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_DPA_DISCHARGE;
    alarms[i++].alarmCode = AZ_STATUS_POSN_FILTER_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_MAJOR_FILTER_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_MINOR_FILTER_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_FDBK_FILTER_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_DRIVE_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_DRIVE_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_DRIVE_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_DRIVE_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L1_POWER_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_AZ_L2_POWER_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R1_POWER_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_AZ_R2_POWER_ON_TO;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_AXIS_DPA_FAULT;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_AXIS_DPA_CB_OFF;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_AXIS_DPA_COND_OFF;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_AXIS_RSLV_DISCON;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_AXIS_ASYNC;
    alarms[i++].alarmCode = AZ_STATUS_BRAKE_AXIS_DRIVE_ON_TO;
    alarms[i++].alarmCode = EL_STATUS_LIMIT2_UP;
    alarms[i++].alarmCode = EL_STATUS_LIMIT2_DOWN;
    alarms[i++].alarmCode = EL_STATUS_LIMIT1_UP;
    alarms[i++].alarmCode = EL_STATUS_LIMIT1_DOWN;
    alarms[i++].alarmCode = EL_STATUS_PRE_LIMIT_UP;
    alarms[i++].alarmCode = EL_STATUS_PRE_LIMIT_DOWN;
    alarms[i++].alarmCode = EL_STATUS_LIMIT_WARN_UP;
    alarms[i++].alarmCode = EL_STATUS_LIMIT_WARN_DOWN;
    alarms[i++].alarmCode = EL_STATUS_INVALID_DIR;
    alarms[i++].alarmCode = EL_STATUS_OVERRIDE_SW_ENABLED;
    alarms[i++].alarmCode = EL_STATUS_EL_L_DPA_FAULT;
    alarms[i++].alarmCode = EL_STATUS_EL_R_DPA_FAULT;
    alarms[i++].alarmCode = EL_STATUS_EL_L_MOTOR_OH;
    alarms[i++].alarmCode = EL_STATUS_EL_R_MOTOR_OH;
    alarms[i++].alarmCode = EL_STATUS_EL_L_CBANK_FAULT;
    alarms[i++].alarmCode = EL_STATUS_EL_R_CBANK_FAULT;
    alarms[i++].alarmCode = EL_STATUS_EL_L_CBANK_FULL;
    alarms[i++].alarmCode = EL_STATUS_EL_R_CBANK_FULL;
    alarms[i++].alarmCode = EL_STATUS_EL_L_EXCSV_CURR;
    alarms[i++].alarmCode = EL_STATUS_EL_R_EXCSV_CURR;
    alarms[i++].alarmCode = EL_STATUS_SERVO_OSCILLATION;
    alarms[i++].alarmCode = EL_STATUS_RUNAWAY;
    alarms[i++].alarmCode = EL_STATUS_OVERSPEED;
    alarms[i++].alarmCode = EL_STATUS_ANGLE_INPUT_FAULT;
//    alarms[i++].alarmCode = EL_STATUS_EL_CB_OFF;
    alarms[i++].alarmCode = EL_STATUS_DCPA_CB_OFF;
    alarms[i++].alarmCode = EL_STATUS_EL_ALL_ENCDR_ALARM;
    alarms[i++].alarmCode = EL_STATUS_EL_F_ENCDR_ALARM;
    alarms[i++].alarmCode = EL_STATUS_EL_R_ENCDR_ALARM;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_POWER_FAIL;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_PS_FUSE_BLOW;
    alarms[i++].alarmCode = EL_STATUS_SURVIVAL_STOW_POS;
    alarms[i++].alarmCode = EL_STATUS_MAINT_STOW_POS;
    alarms[i++].alarmCode = EL_STATUS_ZENITH_POS;
    alarms[i++].alarmCode = EL_STATUS_OTHER_FAULT;
    alarms[i++].alarmCode = EL_STATUS_EL_F_ENCDR_DISCON;
    alarms[i++].alarmCode = EL_STATUS_EL_R_ENCDR_DISCON;
    alarms[i++].alarmCode = EL_STATUS_EL_F_ENCDR_NOT_INIT;
    alarms[i++].alarmCode = EL_STATUS_EL_R_ENCDR_NOT_INIT;
    alarms[i++].alarmCode = EL_STATUS_EL_L_DPA_DISCON;
    alarms[i++].alarmCode = EL_STATUS_EL_R_DPA_DISCON;
    alarms[i++].alarmCode = EL_STATUS_EL_L_DPA_COND_OFF;
    alarms[i++].alarmCode = EL_STATUS_EL_R_DPA_COND_OFF;
    alarms[i++].alarmCode = EL_STATUS_EL_L_DPA_DISCHARGE;
    alarms[i++].alarmCode = EL_STATUS_EL_R_DPA_DISCHARGE;
    alarms[i++].alarmCode = EL_STATUS_POSN_FILTER_FAULT;
    alarms[i++].alarmCode = EL_STATUS_MAJOR_FILTER_FAULT;
    alarms[i++].alarmCode = EL_STATUS_MINOR_FILTER_FAULT;
    alarms[i++].alarmCode = EL_STATUS_FDBK_FILTER_FAULT;
    alarms[i++].alarmCode = EL_STATUS_EL_L_DRIVE_ON_TO;
    alarms[i++].alarmCode = EL_STATUS_EL_R_DRIVE_ON_TO;
    alarms[i++].alarmCode = EL_STATUS_EL_L_POWER_ON_TO;
    alarms[i++].alarmCode = EL_STATUS_EL_R_POWER_ON_TO;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_AXIS_DPA_FAULT;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_AXIS_DPA_CB_OFF;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_AXIS_DPA_COND_OFF;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_AXIS_RSLV_DISCON;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_AXIS_ASYNC;
    alarms[i++].alarmCode = EL_STATUS_BRAKE_AXIS_DRIVE_ON_TO;

    const string antName = HardwareDeviceImpl::componentToAntennaName(
        CORBA::String_var(acscomponent::ACSComponentImpl::name()).in());
    alarms_m.initializeAlarms(antennaModel_m.c_str(), antName, alarms);
    alarms_m.forceTerminateAllAlarms();
}

std::vector< unsigned char > MountACACommonImpl::getAzStatus(ACS::Time& timestamp) {
    ACS_TRACE(__func__);

    std::vector<unsigned char > value =  MountACACommonBase::getAzStatus(timestamp);

/*  http://jira.alma.cl/browse/COMP-4430
    ACS::Time ts;
    alarms_m.updateAlarm(AZ_STATUS_LIMIT2_CW, getAzStatus2ndLimitCw(ts));
    alarms_m.updateAlarm(AZ_STATUS_LIMIT2_CCW, getAzStatus2ndLimitCcw(ts));
    alarms_m.updateAlarm(AZ_STATUS_LIMIT1_CW, getAzStatus1stLimitCw(ts));
    alarms_m.updateAlarm(AZ_STATUS_LIMIT1_CCW, getAzStatus1stLimitCcw(ts));
    alarms_m.updateAlarm(AZ_STATUS_PRE_LIMIT_CW, getAzStatusPreLimitCw(ts));
    alarms_m.updateAlarm(AZ_STATUS_PRE_LIMIT_CCW, getAzStatusPreLimitCcw(ts));
    alarms_m.updateAlarm(AZ_STATUS_LIMIT_WARN_CW, getAzStatusLimitWarningCw(ts));
    alarms_m.updateAlarm(AZ_STATUS_LIMIT_WARN_CCW, getAzStatusLimitWarningCcw(ts));
    alarms_m.updateAlarm(AZ_STATUS_INVALID_DIR, getAzStatusInvalidDir(ts));
    alarms_m.updateAlarm(AZ_STATUS_OVERRIDE_SW_ENABLED, getAzStatusOverrideSwitch(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_DPA_FAULT, getAzStatusAzL1DpaFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_DPA_FAULT, getAzStatusAzL2DpaFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_DPA_FAULT, getAzStatusAzR1DpaFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_DPA_FAULT, getAzStatusAzR2DpaFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_MOTOR_OH, getAzStatusAzL1MotorOh(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_MOTOR_OH, getAzStatusAzL2MotorOh(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_MOTOR_OH, getAzStatusAzR1MotorOh(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_MOTOR_OH, getAzStatusAzR2MotorOh(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_CBANK_FAULT, getAzStatusAzL1CbankFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_CBANK_FAULT, getAzStatusAzL2CbankFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_CBANK_FAULT, getAzStatusAzR1CbankFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_CBANK_FAULT, getAzStatusAzR2CbankFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_CBANK_FULL, getAzStatusAzL1CbankFull(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_CBANK_FULL, getAzStatusAzL2CbankFull(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_CBANK_FULL, getAzStatusAzR1CbankFull(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_CBANK_FULL, getAzStatusAzR2CbankFull(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_EXCSV_CURR, getAzStatusAzL1ExcsvCur(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_EXCSV_CURR, getAzStatusAzL2ExcsvCur(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_EXCSV_CURR, getAzStatusAzR1ExcsvCur(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_EXCSV_CURR, getAzStatusAzR2ExcsvCur(ts));
    alarms_m.updateAlarm(AZ_STATUS_SERVO_OSCILLATION, getAzStatusServoOscillation(ts));
    alarms_m.updateAlarm(AZ_STATUS_RUNAWAY, getAzStatusRunaway(ts));
    alarms_m.updateAlarm(AZ_STATUS_OVERSPEED, getAzStatusOverspeed(ts));
    alarms_m.updateAlarm(AZ_STATUS_ANGLE_INPUT_FAULT, getAzStatusAngleInputFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L_DCPA_CB_OFF, getAzStatusAzLDcpaCbOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R_DCPA_CB_OFF, getAzStatusAzRDcpaCbOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_ALL_ENCODER_ALARM, getAzStatusAzAllEncoderAlarm(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_LF_ENCODER_ALARM, getAzStatusAzLfEncoderAlarm(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_LR_ENCODER_ALARM, getAzStatusAzLrEncoderAlarm(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_RF_ENCODER_ALARM, getAzStatusAzRfEncoderAlarm(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_RR_ENCODER_ALARM, getAzStatusAzRrEncoderAlarm(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_POWER_FAIL, getAzStatusBrakePowerFail(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_PS_FUSE_BLOW, getAzStatusBrakePsFuseBlow(ts));
    alarms_m.updateAlarm(AZ_STATUS_SURVIVAL_STOW_POS, getAzStatusSurvivalStowPos(ts));
    alarms_m.updateAlarm(AZ_STATUS_MAINT_STOW_POS, getAzStatusMaintStowPos(ts));
    alarms_m.updateAlarm(AZ_STATUS_CABLE_OVERLAP_CW, getAzStatusCableOverlapCw(ts));
    alarms_m.updateAlarm(AZ_STATUS_CABLE_OVERLAP_CCW, getAzStatusCableOverlapCcw(ts));
    alarms_m.updateAlarm(AZ_STATUS_OTHER_FAULT,       getAzStatusOtherFault(ts));
    */
    return value;
}

std::vector< unsigned char > MountACACommonImpl::getAzStatus2(ACS::Time& timestamp) {
    ACS_TRACE(__func__);

    std::vector<unsigned char > value =  MountACACommonBase::getAzStatus2(timestamp);

/*  http://jira.alma.cl/browse/COMP-4430
    ACS::Time ts;
    alarms_m.updateAlarm(AZ_STATUS_AZ_LF_ENCDR_DISCON, getAzStatus2AzLfEncdrDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_LR_ENCDR_DISCON, getAzStatus2AzLrEncdrDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_RF_ENCDR_DISCON, getAzStatus2AzRfEncdrDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_RR_ENCDR_DISCON, getAzStatus2AzRrEncdrDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_LF_ENCDR_NOT_INIT, getAzStatus2AzLfEncdrNotInit(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_LR_ENCDR_NOT_INIT, getAzStatus2AzLrEncdrNotInit(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_RF_ENCDR_NOT_INIT, getAzStatus2AzRfEncdrNotInit(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_RR_ENCDR_NOT_INIT, getAzStatus2AzRrEncdrNotInit(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_DPA_DISCON, getAzStatus2AzL1DpaDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_DPA_DISCON, getAzStatus2AzL2DpaDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_DPA_DISCON, getAzStatus2AzR1DpaDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_DPA_DISCON, getAzStatus2AzR2DpaDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_DPA_COND_OFF, getAzStatus2AzL1DpaCondOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_DPA_COND_OFF, getAzStatus2AzL2DpaCondOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_DPA_COND_OFF, getAzStatus2AzR1DpaCondOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_DPA_COND_OFF, getAzStatus2AzR2DpaCondOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_DPA_DISCHARGE, getAzStatus2AzL1DpaDischarge(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_DPA_DISCHARGE, getAzStatus2AzL2DpaDischarge(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_DPA_DISCHARGE, getAzStatus2AzR1DpaDischarge(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_DPA_DISCHARGE, getAzStatus2AzR2DpaDischarge(ts));
    alarms_m.updateAlarm(AZ_STATUS_POSN_FILTER_FAULT, getAzStatus2PositionFilterFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_MAJOR_FILTER_FAULT, getAzStatus2MajorFilterFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_MINOR_FILTER_FAULT, getAzStatus2MinorFilterFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_FDBK_FILTER_FAULT, getAzStatus2FdbkFilterFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_DRIVE_ON_TO, getAzStatus2AzL1DriveOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_DRIVE_ON_TO, getAzStatus2AzL2DriveOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_DRIVE_ON_TO, getAzStatus2AzR1DriveOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_DRIVE_ON_TO, getAzStatus2AzR2DriveOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L1_POWER_ON_TO, getAzStatus2AzL1PowerOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_L2_POWER_ON_TO, getAzStatus2AzL2PowerOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R1_POWER_ON_TO, getAzStatus2AzR1PowerOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_AZ_R2_POWER_ON_TO, getAzStatus2AzR2PowerOnTo(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_AXIS_DPA_FAULT, getAzStatus2BrakeAxisDpaFault(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_AXIS_DPA_CB_OFF, getAzStatus2BrakeAxisDpaCbOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_AXIS_DPA_COND_OFF, getAzStatus2BrakeAxisDpaCondOff(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_AXIS_RSLV_DISCON, getAzStatus2BrakeAxisRslvDiscon(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_AXIS_ASYNC, getAzStatus2BrakeAxisAsync(ts));
    alarms_m.updateAlarm(AZ_STATUS_BRAKE_AXIS_DRIVE_ON_TO, getAzStatus2BrakeAxisDriveOnTo(ts));
    */
    return value;
}

std::vector< unsigned char > MountACACommonImpl::getElStatus(ACS::Time& timestamp) {
    ACS_TRACE(__func__);

    std::vector<unsigned char > value =  MountACACommonBase::getElStatus(timestamp);

/*  http://jira.alma.cl/browse/COMP-4430
    ACS::Time ts;
    alarms_m.updateAlarm(EL_STATUS_LIMIT2_UP, getElStatus2ndLimitUp(ts));
    alarms_m.updateAlarm(EL_STATUS_LIMIT2_DOWN, getElStatus2ndLimitDown(ts));
    alarms_m.updateAlarm(EL_STATUS_LIMIT1_UP, getElStatus1stLimitUp(ts));
    alarms_m.updateAlarm(EL_STATUS_LIMIT1_DOWN, getElStatus1stLimitDown(ts));
    alarms_m.updateAlarm(EL_STATUS_PRE_LIMIT_UP, getElStatusPreLimitUp(ts));
    alarms_m.updateAlarm(EL_STATUS_PRE_LIMIT_DOWN, getElStatusPreLimitDown(ts));
    alarms_m.updateAlarm(EL_STATUS_LIMIT_WARN_UP, getElStatusLimitWarnUp(ts));
    alarms_m.updateAlarm(EL_STATUS_LIMIT_WARN_DOWN, getElStatusLimitWarnDown(ts));
    alarms_m.updateAlarm(EL_STATUS_INVALID_DIR, getElStatusInvalidDir(ts));
    alarms_m.updateAlarm(EL_STATUS_OVERRIDE_SW_ENABLED, getElStatusOverrideSwEnabled(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_DPA_FAULT, getElStatusElLDpaFault(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_DPA_FAULT, getElStatusElRDpaFault(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_MOTOR_OH, getElStatusElLMotorOh(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_MOTOR_OH, getElStatusElRMotorOh(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_CBANK_FAULT, getElStatusElLCbankFault(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_CBANK_FAULT, getElStatusElRCbankFault(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_CBANK_FULL, getElStatusElLCbankFull(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_CBANK_FULL, getElStatusElRCbankFull(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_EXCSV_CURR, getElStatusElLExcsvCurr(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_EXCSV_CURR, getElStatusElRExcsvCurr(ts));
    alarms_m.updateAlarm(EL_STATUS_SERVO_OSCILLATION, getElStatusServoOscillation(ts));
    alarms_m.updateAlarm(EL_STATUS_RUNAWAY, getElStatusServoRunaway(ts));
    alarms_m.updateAlarm(EL_STATUS_OVERSPEED, getElStatusServoOverspeed(ts));
    alarms_m.updateAlarm(EL_STATUS_ANGLE_INPUT_FAULT, getElStatusAngleInputFault(ts));
//    alarms_m.updateAlarm(EL_STATUS_EL_CB_OFF, getElStatusElCbOff(ts));
//    alarms_m.updateAlarm(EL_STATUS_DCPA_CB_OFF, getElStatusDcpaCbOff(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_ALL_ENCDR_ALARM, getElStatusElAllEncdrAlarm(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_F_ENCDR_ALARM, getElStatusElFEncdrAlarm(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_ENCDR_ALARM, getElStatusElREncdrAlarm(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_POWER_FAIL, getElStatusBrakePowerFail(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_PS_FUSE_BLOW, getElStatusBrakePsFuseBlow(ts));
    alarms_m.updateAlarm(EL_STATUS_SURVIVAL_STOW_POS, getElStatusSurvivalStowPos(ts));
    alarms_m.updateAlarm(EL_STATUS_MAINT_STOW_POS, getElStatusMaintStowPos(ts));
    alarms_m.updateAlarm(EL_STATUS_ZENITH_POS, getElStatusZenithPos(ts));
    alarms_m.updateAlarm(EL_STATUS_OTHER_FAULT, getElStatusOtherFault(ts));
    */
    return value;
}

std::vector< unsigned char > MountACACommonImpl::getElStatus2(ACS::Time& timestamp) {
    ACS_TRACE(__func__);

    std::vector<unsigned char > value =  MountACACommonBase::getElStatus2(timestamp);

/*  http://jira.alma.cl/browse/COMP-4430
    ACS::Time ts;
    alarms_m.updateAlarm(EL_STATUS_EL_F_ENCDR_DISCON, getElStatus2ElFEncdrDiscon(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_ENCDR_DISCON, getElStatus2ElREncdrDiscon(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_F_ENCDR_NOT_INIT, getElStatus2ElFEncdrNotInit(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_ENCDR_NOT_INIT, getElStatus2ElREncdrNotInit(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_DPA_DISCON, getElStatus2ElLDpaDiscon(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_DPA_DISCON, getElStatus2ElRDpaDiscon(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_DPA_COND_OFF, getElStatus2ElLDpaCondOff(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_DPA_COND_OFF, getElStatus2ElRDpaCondOff(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_DPA_DISCHARGE, getElStatus2ElLDpaDischarge(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_DPA_DISCHARGE, getElStatus2ElRDpaDischarge(ts));
    alarms_m.updateAlarm(EL_STATUS_POSN_FILTER_FAULT, getElStatus2PosnFilterFault(ts));
    alarms_m.updateAlarm(EL_STATUS_MAJOR_FILTER_FAULT, getElStatus2MajorFilterFault(ts));
    alarms_m.updateAlarm(EL_STATUS_MINOR_FILTER_FAULT, getElStatus2MinorFilterFault(ts));
    alarms_m.updateAlarm(EL_STATUS_FDBK_FILTER_FAULT, getElStatus2FdbkFilterFault(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_DRIVE_ON_TO, getElStatus2ElLDriveOnTo(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_DRIVE_ON_TO, getElStatus2ElRDriveOnTo(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_L_POWER_ON_TO, getElStatus2ElLPowerOnTo(ts));
    alarms_m.updateAlarm(EL_STATUS_EL_R_POWER_ON_TO, getElStatus2ElRPowerOnTo(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_AXIS_DPA_FAULT, getElStatus2BrakeAxisDpaFault(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_AXIS_DPA_CB_OFF, getElStatus2BrakeAxisDpaCbOff(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_AXIS_DPA_COND_OFF, getElStatus2BrakeAxisDpaCondOff(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_AXIS_RSLV_DISCON, getElStatus2BrakeAxisRslvDiscon(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_AXIS_ASYNC, getElStatus2BrakeAxisAsync(ts));
    alarms_m.updateAlarm(EL_STATUS_BRAKE_AXIS_DRIVE_ON_TO, getElStatus2BrakeAxisDriveOnTo(ts));
    */
    return value;
}

std::vector< unsigned char > MountACACommonImpl::getSystemStatus(ACS::Time& timestamp) {
    ACS_TRACE(__func__);

    std::vector<unsigned char > value =  MountACACommonBase::getSystemStatus(timestamp);


/*  http://jira.alma.cl/browse/COMP-4430
    ACS::Time ts;
    alarms_m.updateAlarm(SYSTEM_STATUS_EMERGENCY_STOP, getSystemStatusEmergencyStop(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_STAIRWAY_INTERLOCK, getSystemStatusStairwayInterlock(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_HANDLING_INTERLOCK, getSystemStatusHandleInterlock(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SMOKE_ALARM, getSystemStatusSmokeAlarm(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ACU_FAULT, getSystemStatusAcuFault(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SURVIVAL_FOR_MISSING_COMMANDS, getSystemStatusSurvivalStowCmdTo(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SURVIVAL_FOR_MISSING_TIMING, getSystemStatusSurvivalStowClkTo(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_TIMING_PULSE, getSystemStatusExtClockFault(ts));
//    alarms_m.updateAlarm(SYSTEM_STATUS_RCV_CABIN_SAFETY, getSystemStatusRcvCabinSafety(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ANT_BASE_SAFETY, getSystemStatusAntBaseSafety(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_VERANDA_EQUIP_FREE, getSystemStatusVerandaEquipFree(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_LADDER_SWITCH, getSystemStatusLadderSwitch(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_TRANSP_INTLK, getSystemStatusTranspIntlk(ts));
//    alarms_m.updateAlarm(SYSTEM_STATUS_TRANSP_INTLK_EXCPT_AZ, getSystemStatusTranspIntlkExcptAz(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_RCV_CABIN_DOOR_OPEN, getSystemStatusRcvCabinDoorOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_CNTR_CABIN_DOOR_OPEN, getSystemStatusCntrCabinDoorOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ANT_BASE_DOOR_OPEN, getSystemStatusAntCbaseDoorOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_UPS_ROOM_DOOR_OPEN, getSystemStatusUpsRoomDoorOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_PDB_ROOM_DOOR_OPEN, getSystemStatusPdbRoomDoorOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_MAIN_REF_HATCH_OPEN, getSystemStatusMainRefHatchOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_YOKE_L_HATCH_OPEN, getSystemStatusYokeLHatchOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_YOKE_R_HATCH_OPEN, getSystemStatusYokeRHatchOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_AZ_HNDL_INTLK, getSystemStatusAzHndlIntlk(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_EL_HNDL_INTLK, getSystemStatusElHndlIntlk(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_AZ_STOW_HNDL_INTLK, getSystemStatusAzStowHndlIntlk(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_EL_STOW_HNDL_INTLK, getSystemStatusElStowHndlIntlk(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SHUTTER_HNDL_INTLK, getSystemStatusShutterHndlIntlk(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_AZ_STOW_TO, getSystemStatusAzStowTo(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_AZ_STOW_THERMAL_TRIP, getSystemStatusAzStowThermalTrip(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_STOW_DPA_CB_OFF, getSystemStatusStowDpaCbPff(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_STOW_DPA_COND_OFF, getSystemStatusStowDpaCondOff(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_EL_STOW_TO, getSystemStatusElStowTo(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_EL_STOW_THERMAL_TRIP, getSystemStatusElStowThermalTrip(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SHUTTER_TO, getSystemStatusShutterTo(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SHUTTER_THERMAL_TRIP, getSystemStatusShutterThermalTrip(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SHUTTER_DPA_CB_OFF, getSystemStatusShutterDpaDbOff(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SHUTTER_DPA_COND_OFF, getSystemStatusShutterDpaCondOff(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ZENITH_SHUTTER_OPEN, getSystemStatusZenithShutterOpen(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_RD_CONV_FAIL, getSystemStatusRdConvFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IF_PANEL_FAIL, getSystemStatusIfPanelFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_NUTATOR_RACK_PS_ALARM, getSystemStatusNutatorRackPsAlarm(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_BRAKE_AXIS_HEATER_ALARM, getSystemStatusBrakeAxisHeaterAlarm(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_MOTOR_PUMP_FAIL, getSystemStatusMotorPumpFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ARRESTER_BROKEN_ALARM, getSystemStatusArresterBrokenAlarm(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DC_POWER_CIRCUIT_ALARM, getSystemStatusDcPowerCircuitAlarm(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ACU_MAINT, getSystemStatusAcuMaint(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ANT_STATION_POWER_DISCON, getSystemStatusAntStationPowerDiscon(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ANT_INTLK, getSystemStatusAntIntlk(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DRIVE_SYSTEM_FAIL, getSystemStatusDriveSysytemFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_MORE_PCU_CONNECT, getSystemStatus2orMorePcuConnect(ts));
    */
    return value;
}

std::vector< unsigned char > MountACACommonImpl::getSystemStatus2(ACS::Time& timestamp) {
    ACS_TRACE(__func__);

    std::vector<unsigned char > value =  MountACACommonBase::getSystemStatus2(timestamp);

/*  http://jira.alma.cl/browse/COMP-4430
    ACS::Time ts;
    alarms_m.updateAlarm(SYSTEM_STATUS_DC12V_FAIL, getSystemStatus2Dc12vFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_SYS_FAIL, getSystemStatus2SysFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_AC_FAIL, getSystemStatus2AcFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DATA_FAULT, getSystemStatus2DataFault(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_AD_CONV_FAIL, getSystemStatus2AdConvFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_CAN_BOARD_FAIL, getSystemStatus2CanBoardFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_CLOCK_BOARD_FAIL, getSystemStatus2ClockBoardFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DSP_FAIL, getSystemStatus2DspFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DSP_AD_CONV_FAIL, getSystemStatus2DspAdConvFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IMP1_FAIL, getSystemStatus2Imp1Fail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IMP2_FAIL, getSystemStatus2Imp2Fail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IMP3_FAIL, getSystemStatus2Imp3Fail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IMP4_FAIL, getSystemStatus2Imp4Fail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IMP5_FAIL, getSystemStatus2Imp5Fail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IMP6_FAIL, getSystemStatus2Imp6Fail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_IMP7_FAIL, getSystemStatus2Imp7Fail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_PCU1_CONNECTED, getSystemStatus2Pcu1Connected(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_PCU2_CONNECTED, getSystemStatus2Pcu2Connected(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_PCU3_CONNECTED, getSystemStatus2Pcu3Connected(ts));
//    alarms_m.updateAlarm(SYSTEM_STATUS_DIFC_FAULT, getSystemStatus2DifcFault(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DIFC_POWER_FAIL, getSystemStatus2DifcPowerFail(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ACU2DIFC_LINE1_DISCON, getSystemStatus2Acu2difcLine1Discon(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ACU2DIFC_LINE2_DISCON, getSystemStatus2Acu2difcLine2Discon(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DIFC2ACU_LINE1_DISCON, getSystemStatus2Difc2acuLine1Discon(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_DIFC2ACU_LINE2_DISCON, getSystemStatus2Difc2acuLine2Discon(ts));
    alarms_m.updateAlarm(SYSTEM_STATUS_ACU2DIFC_LINE3_DISCON, getSystemStatus2Acu2difcLine3Discon(ts));
    */
    return value;
}

//------------------------------------------------------------------------------
std::vector< unsigned char > MountACACommonImpl::getGetAcuError(ACS::Time& timestamp) {
    std::vector< unsigned char > rv;
    return rv;
}

void MountACACommonImpl::SET_METR_MODE(const LongSeq& world) {

    MountBase::SET_METR_MODE(world);
    ostringstream msg;
    msg << "SET_METR_MODE command executed with parameters: ["
        << world[0] << ", "
        << world[1] << ", "
        << world[2] << ", "
        << world[3] << "]";
    LOG_TO_AUDIENCE(LM_NOTICE, __func__, msg.str(), OPERATOR);
}

void MountACACommonImpl::SET_CLEAR_FAULT_CMD() {
// see resetMelcoTimingPulse.py
    if (!inShutdownMode()) {
        LOG_TO_AUDIENCE(LM_INFO, __func__, "bringing axis to shutdown...", OPERATOR);
        shutdown();
        sleep(20);
    }
    ACS::Time ts;
    std::vector< unsigned char > srMode = getSubrefModeRsp(ts);
    if (srMode[0] != 0) {
        LOG_TO_AUDIENCE(LM_INFO, __func__, "bringing subref to shutdown...", OPERATOR);
        setSubrefModeCmd(0);
    }
    MountBase::SET_CLEAR_FAULT_CMD();
    sleep(2);
    MountBase::SET_CLEAR_FAULT_CMD();
    LOG_TO_AUDIENCE(LM_INFO, __func__, "CLEAR_FAULT_CMD issued", OPERATOR);
    setSubrefModeCmd(0x2); // Set the Subreflector to encoder.
}

