//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

//-------------------------------------------------------------------------
// 20080702 M.Tasaki based on MountVertexHWSimImplAxis.cpp
//                   Added state flags of Az(El)Status.
//-------------------------------------------------------------------------

#include <math.h>
#include <stdio.h>
#include <iostream>
#include "Axis.h"
#include "MountACACommonHWSimImplAxis.h"

using namespace std;
using namespace ACA;

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define abs(val) ((val) < 0 ? -(val) : (val))

#define MAX_VEL     6.0
#define MAX_ACC     10.0

#define STATE_CHANGES 2
#define OTHER_DEBUG   4
#define DEBUG_DRIVE   8
#define DEBUG_TICK    0x10
//------------------------------------------------------------------------------

MountACACommonHWSimImplAxis::MountACACommonHWSimImplAxis() :
  ACUSim::Axis::Axis()
{ 
 //  drive_state = STOP; 
   current_pos = 10.0; //deg. Do not use zero as its outside the elevation limit
   current_vel = current_acc = 0;
  // decel_offset = decel_vel = 0;
  // track_pos = track_vel = delta_track = 0;
 //  lower_limit = upper_limit = 0;
 //  limit = 0;
  // count = 0;
 //  newpos = false;
//   newmode = mode = SHUTDOWN;
//   brake = BRAKE_ON;
//   pin = PIN_OUT;
   isPinIn = false;

//  2008-07-01 M.Tasaki We must use the functions in MountACACommonHWSimBase.cpp
//   auxMode1 = ratefdbkMode1 = 0;

   clockCount =0;
//   set_servo_defaults();
//   auxmode=AUX_DISABLED;
}



//----------AZ(EL)Status byte7
/*
bool MountACACommonHWSimImplAxis::isSurvivalStowPos()
{  
   int a = Axis::get_stow_pin();
   if(a == PIN_IN) isPinIn = true; else isPinIn = false;
   bool isInSstowPos = inrange( current_pos, 0.010, survivalStowPosition ); 
   if ( isPinIn && isInSstowPos ) {
      return true;
   }
  return false; 
}
bool MountACACommonHWSimImplAxis::isMaintStowPos()
{ 
   int a = Axis::get_stow_pin();
   if(a == PIN_IN) isPinIn = true; else isPinIn = false;
   bool isInMstowPos = inrange( current_pos, 0.010, maintStowPosition );
   if ( isPinIn && isInMstowPos )  {
      return true;
   }
  return false;
}
*/
//-----------------------------------------------------
struct STATUS_VAR_STRUCT {
  bool * statusFlag;
  unsigned int  bytePos;
  char  flagPattern;
};

unsigned long long MountACACommonHWSimImplAxis::getStatus()
{
// Byte0:b0:2nd limit CW, b1:2nd limit CCW, b2:1st limit CW, b3:1st limit CCW,
// b4:Pre limit CW, b5:Pre limit CCW, b6:Limit warning CW, b7:Limint warning CCW
// Byte1:b0:Invalid direction, b1:Override switch
// Byte 2: Drive system failure is not implemented.
// b0:AZ-L1 DPA fault, b1:AZ-L2 DPA fault, b2:AZ-R1 DPA fault,b3:AZ-R2 DPA fault
// b4:AZ-L1 Motor overht,b5:AZ-L2 Motor oh,b6:AZ-R1 Motor oh, b7:AZ-R2 Motor oh
// Byte 3: C-bank status is not implemented.
// b0:AZ-L1 C-bank fault, b1:AZ-L2 C-bank fault, b2:AZ-R1, b3:AZ-R2,
// b4:AZ-L1, b5:AZ-L2, b6:AZ-R1, b7:AZ-R2
// Byte 4: Servo condition is not implemented.
// b0:AZ-L1 excs cur, b1:AZ-L2 excsv cur, b2:AZ-R1, b3:AZ-R2,
// b4:Servo oscill, b5:Run away, b6:Overspeed, b7:Angle input fault
// Byte 5: Drive system failure is not implemented
// Byte 6: Brake condition is not implemented.
// Byte 7: Misc, status.
// b0:Survival stow pos, b1:maint. stow pos, b2,b3,b6:always 0,
// b4:Cable overwrap CW, b5:Cable overwrap CCW, b7:others(See AZ_STATUS2)
  unsigned long long lstat = 0;
  char* p(reinterpret_cast< char* >(&lstat));

  static struct STATUS_VAR_STRUCT statStruct[] =
  {
  // At this moment, I have not implemented all flag but ones below.  
    { &this->is2ndLimitCwUp,     0, 0x01 },
    { &this->is2ndLimitCcwDown,  0, 0x02 },
    { &this->is1stLimitCwUp,     0, 0x04 },
    { &this->is1stLimitCcwDown,  0, 0x08 },
    { &this->isPreLimitCwUp,     0, 0x10 },
    { &this->isPreLimitCcwDown,  0, 0x20 },
    { &this->isLimWarningCwUp,   0, 0x40 },
    { &this->isLimWarningCcwDown,0, 0x80 },

    { &this->isInvalidDirection, 1, 0x01 },
    { &this->isOverrideSwitchEnable, 1, 0x02 },

    { &this->isAngleInputFault,  4, 0x80 },

    { &this->isSurvivalStowPos,  7, 0x01 },
    { &this->isMaintStowPos,     7, 0x02 }  
  } ;
  unsigned int nStatFlags = sizeof(statStruct)/sizeof(statStruct[0]);

  if( mode == SURVIVAL_STOW ) { 
     isSurvivalStowPos = true;  isMaintStowPos = false;
  } else if( mode == MAINTENANCE_STOW ) {
     isSurvivalStowPos = false; isMaintStowPos = true;
  } else { 
     isSurvivalStowPos = false; isMaintStowPos = false;
  }



   is2ndLimitCwUp= is2ndLimitCcwDown = is1stLimitCwUp = is1stLimitCcwDown
  =isPreLimitCwUp=isPreLimitCcwDown= isLimWarningCwUp = isLimWarningCcwDown
  =isInvalidDirection=isOverrideSwitchEnable=isAngleInputFault
  =isSurvivalStowPos=isMaintStowPos =false;

  // just for test byte=9,byte1=2, byte4=128, byte7=3   
/*  Delete test code.
  is2ndLimitCwUp = is1stLimitCcwDown = true; //9
  isOverrideSwitchEnable = true;  //2
  isAngleInputFault= true;   //128
  isSurvivalStowPos=isMaintStowPos=true;  //3
*/


  for(unsigned int idx=0; idx<nStatFlags; idx++ ) {
     unsigned int  lbytePos = statStruct[idx].bytePos;
     bool lflag = *(statStruct[idx].statusFlag);
     char lpattern = lflag ? statStruct[idx].flagPattern : 0 ;
     p[lbytePos] |= lpattern;  

/**
            int aa=(int) lpattern;
            char A2[100];
            sprintf(A2,"%d", aa);
	          std::cout << "ACA-Axis:getStatus:pos=" << lbytePos << ",lpattern=" << A2 << std::endl;
**/  
  }
    /*
            char AAA[100];  sprintf(AAA,"%d", lstat);
	          std::cout << "ACA-Axis:getStatus:lstat=" << AAA << std::endl;
    */
  return lstat;
}

unsigned long long MountACACommonHWSimImplAxis::getStatus2()
{
// 8bytes default = 8bytes x 0
// byte 0: drive system failure
// bit0:AZ-LF encoder disconnected, 1:AZ-LR enc. discon, 2:AZ-RF enc. disconn,
// 3:AZ-RR enc. disconn. 4:AZ-LF encoder not initialized, 5:AZ-LR enc. not init,
// 6:AZ-RF enc. not init. 7:AZ-RR enc. not init.
// byte 1: drive system failure.
// bit0:AZ-L1 DPA discon. 1:AZ-L2 DPA discon. 2:AZ-R1 DPA discon., 3:AZ-R2 DPA. 
// 4-7:rscv
// byte 2: drive system failure
// bit0:AZ-L1 DPA contactor off, 1:AZ-L2 c. off. 2:AZ-R1 c. off, 3:AZ-R2 c. off.
// 4:AZ-L1 DPA discharge, 5:AZ-L2 discharge. 6:AZ-R1 dischage, 7:AZ-R2 discharge.
// byte 3: servo condition
// bit0:position filter fault, 1:major filter fault, 2:minor filter fault,
// 3:feedback filter fault. 4-7: rsvd
// byte 4: drive on timeout
// bit0:AZ-L1 drive on timeout, 1:AZ-L2 d.o TO, 2:AZ-R1 d.o.TO, 3: AZ-R2 d.o.TO
// 4:AZ-L1 power on timeout, 5:AZ-L2 p.o TO, 6:AZ-R1 p.o.TO, 7: AZ-R2 p.o.TO
// byte 5: brake axis failure
// bit0:brake axis DPA fault, 1:brake axis DPA circuit braker off, 
// 2:brake axis DPA contactor off, 3:brake axis resolver disconnected
// 4:brake axis async, 5-6:rsvd, 7:brake axis drive on timeout
// byte 6-7: rsvd
  unsigned long long lstat = 0;
  char* p(reinterpret_cast< char* >(&lstat));

  static struct STATUS_VAR_STRUCT statStruct[] =
  {
     // At this moment, I have not implemented any flag.
  } ;
  unsigned int nStatFlags = 0; // =sizeof(statusStruct2)/sizeof(statusStruct2[0]);

  // keep the following code for future use.
  for(unsigned int idx=0; idx<nStatFlags; idx++ ) {
     unsigned int  lbytePos = statStruct[idx].bytePos;
     bool lflag = *(statStruct[idx].statusFlag);
     char lpattern = lflag ? statStruct[idx].flagPattern : 0 ;
     p[lbytePos] |= lpattern;  
  }
  return lstat;
}
//-----------------------------------------------------
// Internal state
//-----------------------------------------------------
//del.20080702 void MountACACommonHWSimImplAxis::setStowPos(double maint,double survive)
//{
//    maintStowPosition = maint;
//    survivalStowPosition = survive;
//}


//----------------------------------------------------------
// Copied from Axis.cpp
//
//----------------------------------------------------------
//------------------------------------------------------------------------------
// Use one in Axis.cpp
/**
void Axis::transition()
{
#ifdef DEBUG
    cout << "ACA-Axis::transition" << endl;
#endif

    mode = evaluate(newmode);    // can we make the transition?
#ifdef DEBUG
    cout << "ACA-Axis::transition: evaluate called" << endl;
    cout << "ACA-Axis::transition: new mode: " << (int) newmode <<endl;
    cout << "ACA-Axis::transition:     mode: " << (int) mode <<endl;
#endif
    newmode = mode;
    switch(mode)
	{
	case SHUTDOWN:
#ifdef DEBUG
	    cout << "ACA-Axis::transition. SHUTDOWN received " <<endl;
#endif
	    brake = BRAKE_ON;
	    drive_state = STOP;
	    cur_vel = 0;
	    cur_acc = 0;
	    // turn off power, pull pin
	    break;
	    
	case STANDBY:
#ifdef DEBUG
	    cout << "ACA-Axis::transition: STANDBY received " <<endl;
#endif
	    // turn on power, pull pin
	    brake = BRAKE_ON;
	    drive_state = STOP;
	    cur_vel = 0;
	    cur_acc = 0;
	    pin = PIN_OUT;
	    break;
	    
	case SURVIVAL_STOW:
#ifdef DEBUG
	    cout << "ACA-Axis::transition: SURVIVAL_STOW received " <<endl;
#endif
	    brake = BRAKE_OFF;      // always release brakes
	    set(sstow,0.);
	    break;
	    
	case MAINTENANCE_STOW:
#ifdef DEBUG
	    cout << "ACA-Axis::transition: MAINTENANCE_STOW received " <<endl;
#endif
	    brake = BRAKE_OFF;      // always release brakes
	    set(mstow,0.);
	    break;
	    
	case ENCODER:
	case AUTONOMOUS:
#ifdef DEBUG
	    cout << "ACA-Axis::transition: ENCODER or AUTONOMOUS received " <<endl;
#endif
	    brake = BRAKE_OFF;
	    break;
	    
	default:
#ifdef DEBUG
	    cout << "ACA-Axis::transition: unknown mode received " <<endl;
#endif
	    break;
	}
#ifdef DEBUG
    cout << "ACA-Axis::transition. completed " <<endl;
#endif
}
***/
//------------------------------------------------------------------------------
// Use one in Axis.cpp
//AZEL_Mode_t Axis::evaluate(AZEL_Mode_t desiredMode)
//{
//   static int trans_table[6][6] = {
//	{ 0, 1, 0, 0, 0, 0 },         // Shutdown to ...
//	{ 0, 1, 2, 3, 4, 5 },         // Standby to ...
//	{ 0, 1, 2, 3, 4, 5 },         // Encoder to ...
//	{ 0, 1, 2, 3, 4, 5 },         // Autonomous to ...
//	{ 0, 4, 4, 4, 4, 5 },         // Survival stow to ...
//	{ 0, 5, 5, 5, 5, 5 }};        // Maintenance stow to ...

//   return (AZEL_Mode_t)trans_table[(int)mode][(int)desiredMode];
//}

//------------------------------------------------------------------------------
// Use one in Axis.cpp
//void Axis::set_servo_defaults()
//{    
//    for (int i = 0; i < N_COEFFS; i++)
//	servo_coefficients[i] = (long long)i << 32;
//}

//------------------------------------------------------------------------------
// Use one in Axis.cpp
//void Axis::set_aux_mode(AUX_Mode_t m)
//{
//    if (m < AUX_DISABLED || m > AUX12_MODE)
//	return;
    
//    auxmode = m;
//}

//------------------------------------------------------------------------------
// Use one in Axis.cpp
//AUX_Mode_t Axis::get_aux_mode() const
//{
//    return auxmode;
//}

//------------------------------------------------------------------------------
// Use one in Axis.cpp
//bool Axis::driving() const
//{
//    if (drive_state == STOP
//	|| (drive_state == SERVO && delta_track == 0))
//	return true;
//    return false;
//}

//------------------------------------------------------------------------------
// At this moment, it is same as in Axis.cpp
/**
int Axis::set_mode(AZEL_Mode_t m)
{
#ifdef DEBUG
    cout << "ACA-Axis::set_mode" << endl;
    cout << "ACA-Axis::set_mode: new mode received " << ((int)newmode) << endl;
#endif
    newmode = m;
    if (newmode == mode) {
#ifdef DEBUG
	cout << "ACA-Axis::the desired mode is the current mode. No action is taken." << endl;
#endif
       return 0;      // OK
    } else {
       transition();
	     if (mode == m) {
#ifdef DEBUG
    cout << "ACA-Axis::set_mode: completed" << endl;    
#endif
          return 0;      // OK
       } else {
#ifdef DEBUG
	cout << "ACA-Axis::set_mode: ERROR: current mode <> desired mode." << endl;
#endif
	        return 1;  // ERROR
	     }
    }
    return 0;      // OK. The program never reachs here. 
}
**/
//--- Use one in Axis.cpp-------------------------------------------------------
//AZEL_Mode_t Axis::get_mode() const
//{
//    return mode;
//}

//---- Use one in Axis.cpp------------------------------------------------------

//AZEL_Brake_t Axis::get_brake() const
//{
//    return brake;
//}    

//------------------------------------------------------------------------------
/*
int Axis::set_brake(AZEL_Brake_t b)
{
    if (b < BRAKE_OFF || b > BRAKE_ON)
	return 2;
    // if moving, then don't put on brakes
    if (drive_state == STOP
	|| (drive_state == SERVO && delta_track == 0))
	{
	brake = b;
	return 0;
	}
    return 1;
}
*/

//------------------------------------------------------------------------------


//As of 20080701. I delete my inc_cur() so that we can test ACS with Vertex.
//With this function, drive does not work correctly.
/*
void MountACACommonHWSimImplAxis::inc_cur()
{

         std::cout << "ACA-Axis inc_cur: " << current_pos-track_pos <<
	       ", decel off is " << decel_offset << std::endl;

    // accelerate up to max velocity or within delta_x
  count++;
  if (brake == BRAKE_ON)
	return;

     is2ndLimitCcwDown=is1stLimitCcwDown=isPreLimitCcwDown=isLimWarningCcwDown=false;
     is2ndLimitCwUp=is1stLimitCwUp=isPreLimitCwUp=isLimWarningCwUp=false;  
	   
     limit = 0; //20080620MTasaki I think the original code Axis.cpp has a bug.
     // limit must be cleared when current_pos is close to the 
     // lower(or higher)_limit and the current_vel is zero.
    
    // limits are tripped a little ahead of where they really are
    // so simulated tests can set a limit
  if (current_pos <= (lower_limit + 0.1)) {
     if (current_vel < 0 || delta_track < 0) {
	      limit = (PRELIM_DOWN | LIMIT_DOWN);
          is2ndLimitCcwDown=is1stLimitCcwDown=isPreLimitCcwDown=isLimWarningCcwDown=true;
	      drive_state = STOP;
	      current_vel = 0;
	      current_acc = 0;
	      return;
	    }
	} else if (current_pos >= (upper_limit - 0.1))  {
     if (current_vel > 0 || delta_track < 0)  {
        limit = (PRELIM_UP | LIMIT_UP);
          is2ndLimitCwUp=is1stLimitCwUp=isPreLimitCwUp=isLimWarningCwUp=true;  
	      drive_state = STOP;
	      current_vel = 0;
	      current_acc = 0;
	      return;
     }
	} else {
	// execute other checks for pre limits, etc.
	   limit = 0;
	}
  
           isOverSpeed=false;
  
  switch(drive_state)
	{
	case ACCELERATE:
     current_pos += current_vel * TIME_UNIT 
                 + current_acc * TIME_UNIT * TIME_UNIT / 2;
     current_vel += current_acc * TIME_UNIT;

     if (current_vel > MAX_VEL)
		 {
        drive_state = SLEW;
	      current_vel = MAX_VEL;
          isOverSpeed=true;
		 } else if (current_vel < -MAX_VEL) {
		    drive_state = SLEW;
        current_vel = -MAX_VEL;
          isOverSpeed=true;
		 }
	    
     if (inrange(decel_offset,
            abs(current_vel * TIME_UNIT + current_acc * TIME_UNIT * TIME_UNIT / 2),
		        (current_pos - track_pos))) {
        if (debug_level & STATE_CHANGES) 
		          std::cout << "acc: cur-track is " << current_pos-track_pos <<
               ", decel off is " << decel_offset << std::endl;
		    drive_state = DECELERATE;
	   }
	break;
	    
	case SLEW:
	   current_pos += current_vel * TIME_UNIT;
	   {
	      double diff = track_pos - current_pos;
	          if (debug_level & STATE_CHANGES) 
	      	     std::cout << "diff " << diff << ", offset " << 
		           decel_offset << std::endl;
	      if ((current_vel > 0 && track_vel >= 0 && diff <= decel_offset)
	      || (current_vel < 0 && track_vel <= 0 && diff >= decel_offset)
	      || (current_vel > 0 && track_vel <= 0 && diff <= decel_offset)
	      || (current_vel < 0 && track_vel >= 0 && diff >= decel_offset)) {
	         drive_state = DECELERATE;
		    }
	   }
  break;

	case DECELERATE:
     current_pos += current_vel * TIME_UNIT 
                 - current_acc * TIME_UNIT * TIME_UNIT / 2;
     current_vel -= current_acc * TIME_UNIT;

            if (debug_level & STATE_CHANGES) 
		            std::cout << "acc " << current_acc << ", v " << 
		            current_vel << ", dv " << decel_vel << std::endl;
  
     if ((current_acc < 0 && current_vel > decel_vel)
     || (current_acc > 0 && current_vel < decel_vel)) {
        current_vel = track_vel;
        drive_state = SERVO;
     }
	    
     if (current_vel > MAX_VEL || current_vel < -MAX_VEL) {
        drive_state = STOP;
        current_vel = current_acc = 0.;
     }
  break;
	    
	case SERVO:
     servo();
  break;
	    
	case STOP:
  break;
	    
	default:
  break;
	}
}
*/

void MountACACommonHWSimImplAxis::inc_cur()
{
    // accelerate up to max velocity or within delta_x
   count++;
   if (brake == BRAKE_ON)
      return;

      //    !! These flags are not volatile states. They should not be reset at each call.
      // is2ndLimitCcwDown=is1stLimitCcwDown=isPreLimitCcwDown=isLimWarningCcwDown=false;
      // is2ndLimitCwUp=is1stLimitCwUp=isPreLimitCwUp=isLimWarningCwUp=false;  
	   
      //20080620MTasaki I think the original code Axis.cpp has a bug.
      //    !!limit is not volatile state.It should not be reset at each call.
      //    limit = 0; 
      // limit must be cleared when current_pos is close to the 
      // lower(or higher)_limit and the current_vel is zero.

    
   // limits are tripped a little ahead of where they really are
   // so simulated tests can set a limit
   if (cur_pos <= (lower_limit + 0.1))  {
      if (cur_vel < 0 || delta_track < 0) {
         limit = (PRELIM_DOWN | LIMIT_DOWN);
               is2ndLimitCcwDown=is1stLimitCcwDown=isPreLimitCcwDown=isLimWarningCcwDown=true;
         drive_state = STOP;
         cur_vel = 0;
         cur_acc = 0;
         return;
      }
   } else if (cur_pos >= (upper_limit - 0.1))  {
      if (cur_vel > 0 || delta_track < 0) {
         limit = (PRELIM_UP | LIMIT_UP);
               is2ndLimitCwUp=is1stLimitCwUp=isPreLimitCwUp=isLimWarningCwUp=true;  
         drive_state = STOP;
         cur_vel = 0;
         cur_acc = 0;
         return;
      }
   } else {
       // execute other checks for pre limits, etc.
	     limit = 0;
	 }
    
    // Not volatile. do not reaset each time.   isOverSpeed=false;

   switch(drive_state)
   {
   case ACCELERATE:
       cur_pos += cur_vel * TIME_UNIT 
               + cur_acc * TIME_UNIT * TIME_UNIT / 2;
       cur_vel += cur_acc * TIME_UNIT;
    
       if (cur_vel > MAX_VEL) {
           drive_state = SLEW;
           cur_vel = MAX_VEL;
                isOverSpeed=true;
       } else if (cur_vel < -MAX_VEL) {
           drive_state = SLEW;
           cur_vel = -MAX_VEL;
                isOverSpeed=true;
       }

       if (inrange(decel_offset,
               abs(cur_vel * TIME_UNIT + cur_acc * TIME_UNIT * TIME_UNIT / 2),
	             (cur_pos - track_pos)))  {
                   if (debug_level & STATE_CHANGES) 
                        std::cout << "acc: cur-track is " << cur_pos-track_pos <<
	                      ", decel off is " << decel_offset << std::endl;
           drive_state = DECELERATE;
       }
    break;
   
    case SLEW:
       cur_pos += cur_vel * TIME_UNIT;
       {
       double diff = track_pos - cur_pos;
              if (debug_level & STATE_CHANGES) 
	               std::cout << "diff " << diff << ", offset " << 
	               decel_offset << std::endl;
       if ((cur_vel > 0 && track_vel >= 0 && diff <= decel_offset)
       || (cur_vel < 0 && track_vel <= 0 && diff >= decel_offset)
       || (cur_vel > 0 && track_vel <= 0 && diff <= decel_offset)
       || (cur_vel < 0 && track_vel >= 0 && diff >= decel_offset))  {
          drive_state = DECELERATE;
       }
       }
    break;

    case DECELERATE:
       cur_pos += cur_vel * TIME_UNIT 
               - cur_acc * TIME_UNIT * TIME_UNIT / 2;
       cur_vel -= cur_acc * TIME_UNIT;
	              if (debug_level & STATE_CHANGES) 
                   std::cout << "acc " << cur_acc << ", v " << 
                  cur_vel << ", dv " << decel_vel << std::endl;
    
       if ((cur_acc < 0 && cur_vel > decel_vel)
       || (cur_acc > 0 && cur_vel < decel_vel)) {
          cur_vel = track_vel;
          drive_state = SERVO;
       }
    
       if (cur_vel > MAX_VEL || cur_vel < -MAX_VEL) {
          drive_state = STOP;
          cur_vel = cur_acc = 0.;
       }
    break;
    
    case SERVO:
       servo();
    break;
    
    case STOP:
    break;
    
    default:
    break;
}
}



//------------------------------------------------------------------------------
/*
void Axis::drive()
{
    // get current position and figure out the velocity and
    // acceleration
    double diff = track_pos - cur_pos;
    double max_vel;

    if (diff < 0)
	{
	max_vel = -MAX_VEL;
	cur_acc = -MAX_ACC;
	}
    else
	{
  max_vel = MAX_VEL;
	cur_acc = MAX_ACC;
	}
    
    // Does this move need an acceleration up to a velocity, slew, and then 
    // deceleration?  Distances smaller than a * dt * dt / 2 away can be 
    // handled in one time step.

    // The move might be a bigger step than can be handled in one timing 
    // interval.
    // Need to calculate time to go from max velocity to decel_vel.

    // time to decelerate from max_vel to track_vel
    double t = (max_vel - track_vel) / cur_acc;
 */   
    /*
     * x1 - x0 = v0 * t + a * t * t / 2 = dx
     * t^2 * a / 2 + t * v0 - dx = 0
     * t = - (-v0 +- sqrt(v0*v0 + 2*a*dx)) / a, decelerating, so
     * - * - = +, t = (-v0 +- sqrt(v0*v0 + 2*a*dx)) / a
     */
/*    double t0 = (-cur_vel +
		 sqrt(cur_vel * cur_vel + 2 * cur_acc * diff)) / cur_acc;
    if (t0 < 0)
	t0 = (-cur_vel -
	      sqrt(cur_vel * cur_vel + 2 * cur_acc * diff)) / cur_acc;

    if (debug_level & DEBUG_DRIVE) 
	std::cout << "t0 is " << t0 << ", t is " << t << std::endl;

    if (t0 < t)
	{
	t = t0;
	max_vel = cur_acc * t;
	}
    
    double dv = track_vel - max_vel;
    double dt = -dv / cur_acc;

    if (debug_level & DEBUG_DRIVE)
	std::cout << "diff is " << diff;

    if (debug_level & DEBUG_DRIVE)
	std::cout << ", dt is " << dt;

    if (dt < TIME_UNIT * 3)
	{
	drive_state = SERVO;
	if (debug_level & DEBUG_DRIVE)
	    std::cout << std::endl;
	}
    else
	{
	if ((cur_acc > 0 && track_vel > 0)
	 || (cur_acc < 0 && track_vel < 0)
         || track_vel == 0)
	    {
	    decel_offset = -max_vel * dv / cur_acc - dv * dv
		/ (2 * cur_acc);
	    decel_vel = track_vel;
	    if (debug_level & DEBUG_DRIVE) 
		std::cout << ", decel_offset " << decel_offset 
		     << ", max_vel is " << max_vel << std::endl;
	    }
	else
	    {
	    // need to drive past desired position and then back up
	    // overdrive by distance needed to accelerate to new 
	    // velocity.
	    // dv = a * dt, dx = a*dt*dt/2, dx = dv * dv / (2*a)
	    decel_vel = 0;
	    decel_offset = - track_vel * track_vel / (2 * MAX_ACC);
	    if (debug_level & DEBUG_DRIVE) 
		std::cout << "oddball case, decel offset "
		     << decel_offset << std::endl;
	    }
	
	drive_state = ACCELERATE;
	}
}
*/
//------------------------------------------------------------------------------
/*
int Axis::set(double x,double vel)
{
    if (brake == BRAKE_ON)
	return 1;         // this should be an error
    if (x < lower_limit || x > upper_limit) 
	return 2;
    if (vel < -MAX_VEL || vel > MAX_VEL)
	return 3;
    if (mode == SHUTDOWN || mode == STANDBY)
	return 4;
    if (debug_level & OTHER_DEBUG) 
	std::cout << "set to x " << x << ", v " << vel << std::endl;
    track_pos = x;
    track_vel = vel;
    delta_track = vel * TIME_UNIT;
    newpos = true;
    return 0;
}
*/
//------------------------------------------------------------------------------
/*
void Axis::limits(double lower,double upper)
{
    lower_limit = lower;
    upper_limit = upper;
}
*/
//------------------------------------------------------------------------------
/*
void Axis::stows(double maint,double survive)
{
    mstow = maint;
    sstow = survive;
}
*/


