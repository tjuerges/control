
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* M.Tasaki 2008/07/06 Created ACA focus model bases on VertexFocusModel.
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*
*   
*   PARENT CLASS
*
* 
*   DESCRIPTION
*
*
*   PUBLIC METHODS
*
*
*   PUBLIC DATA MEMBERS
*
*
*   PROTECTED METHODS
*
*
*   PROTECTED DATA MEMBERS
*
*
*   PRIVATE METHODS
*
*
*   PRIVATE DATA MEMBERS
*
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <MountACACommonFocusModel.h>
#include <MountACACommonImpl.h>

MountACACommonFocusModel::MountACACommonFocusModel(MountACACommonImpl* mvp) :    
  FocusModelBase(mvp),
  mvp_m(mvp)
{

}

MountACACommonFocusModel::MountACACommonFocusModel(MountACACommonImpl* mvp,
			       Logging::Logger::LoggerSmartPtr logger) :
  FocusModelBase(mvp, logger),
  mvp_m(mvp)
{
 
}

MountACACommonFocusModel::~MountACACommonFocusModel() {
}

void MountACACommonFocusModel::getFocusZeroPoint(double& xZero, double& yZero, double& zZero)
{
/*
  ACS::Time timestamp;
  Control::MountACACommonBase::LongSeq_var zeroPoint;
  zeroPoint = mvp_m->GET_SUBREF_ABS_POSN(timestamp);
  xZero = static_cast<double>(zeroPoint[0]);
  yZero = static_cast<double>(zeroPoint[1]);
  zZero = static_cast<double>(zeroPoint[2]);
*/
  xZero = 0.0;
  yZero = 0.0;
  zZero = 0.0;
}

void MountACACommonFocusModel::getFocusOffset(double& xOffset, double& yOffset, double& zOffset)
{
 /*
  ACS::Time timestamp;
  Control::MountACACommonBase::LongSeq_var offset;
  offset  = mvp_m->GET_SUBREF_DELTA_POSN(timestamp);
  xOffset = static_cast<double>(offset[0]);
  yOffset = static_cast<double>(offset[1]);
  zOffset = static_cast<double>(offset[2]);
*/
  xOffset = 0.0;
  yOffset = 0.0;
  zOffset = 0.0;
}

void MountACACommonFocusModel::setFocusZeroPoint(double xZero, double yZero, double zZero) 
{
  FocusModelBase::setFocusZeroPoint(xZero, yZero, zZero);
  Control::MountACACommonBase::LongSeq  zeroPoint;
  zeroPoint.length(3);
  zeroPoint[0]=static_cast<CORBA::Long>(xZero);
  zeroPoint[1]=static_cast<CORBA::Long>(yZero);
  zeroPoint[2]=static_cast<CORBA::Long>(zZero);
//20080706 mtasaki just for demoving the compile error.
//  mvp_m->SET_SUBREF_ABS_POSN(zeroPoint);
}

/*___oOo___*/
