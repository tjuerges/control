//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

//-------------------------------------------------------------------------
//  Created by Masa Tasaki based on MountVertexHWSimImplDrive.cpp  20080629
//-------------------------------------------------------------------------
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include "MountACACommonHWSimImplAxis.h"
#include "MountACACommonHWSimImplDrive.h"

#undef DEBUG

using namespace std;
using namespace ACA;
using namespace ACUSim;
//------------------------------------------------------------------------------

MountACACommonHWSimImplDrive::MountACACommonHWSimImplDrive()  
  : Drive::Drive()
{
  initialize();
}

MountACACommonHWSimImplDrive::~MountACACommonHWSimImplDrive()  
{
  cleanUp();
}

void MountACACommonHWSimImplDrive::initialize(){
  Az = new MountACACommonHWSimImplAxis();
  El = new MountACACommonHWSimImplAxis();

 //-------------------------------------------------- 
 // Copy the poiners to axis. 
 // Drive::Az = Az;
 // Drive::El = El;
  Drive::initialize(); 

  // The following 2 statements are redundant, since Axis.cpp have the same functions.
  // However, I need sstow,mstow to be public or protected.
  //Az->setStowPos(AZ_MAINT_STOW,AZ_SURVL_STOW);
  //El->setStowPos(EL_MAINT_STOW,EL_SURVL_STOW);
  //-------------------------------------------------

  subrefMode1 = 0; //SUBREF_SHUTDOWN; 

}

void MountACACommonHWSimImplDrive::cleanUp(){
  Drive::cleanUp();
  if (Az != NULL) delete Az;
  if (El != NULL) delete El;
}


//------------------------------------------------------------------------------
void MountACACommonHWSimImplDrive::getAzStatus(unsigned long long& status) const
{
  unsigned char* ptr = (unsigned char*)&status;
  unsigned long long lstat = Az->getStatus();
  char* p(reinterpret_cast< char* >(&lstat));
  for (int idx=0; idx<8; idx++ ) {
  // I have to turn the bytes in reverse, because the user of this
  // function will reverse them.
      ptr[idx] = p[7-idx];
  }
   /*
            char AAA[100], AA2[100];
            sprintf(AAA,"%d", lstat); sprintf(AA2,"%d", status);
	          std::cout << "ACA-Drive:getStatus:lstat=" << AAA << ",status=" << AA2 << std::endl;
   */
}

void MountACACommonHWSimImplDrive::getAzStatus2(unsigned long long& status) const
{
  unsigned char* ptr = (unsigned char*)&status;
  unsigned long long lstat = Az->getStatus2();
  char* p(reinterpret_cast< char* >(&lstat));
  for (int idx=0; idx<8; idx++ ) {
  // I have to turn the bytes in reverse, because the user of this
  // function will reverse them.
      ptr[idx] = p[7-idx];
  }
}

void MountACACommonHWSimImplDrive::getElStatus(unsigned long long& status) const
{
  unsigned char* ptr = (unsigned char*)&status;
  unsigned long long lstat = El->getStatus();
  char* p(reinterpret_cast< char* >(&lstat));
  for (int idx=0; idx<8; idx++ ) {
  // I have to turn the bytes in reverse, because the user of this
  // function will reverse them.
      ptr[idx] = p[7-idx];
  }
}

void MountACACommonHWSimImplDrive::getElStatus2(unsigned long long& status) const
{
  unsigned char* ptr = (unsigned char*)&status;
  unsigned long long lstat = El->getStatus2();
  char* p(reinterpret_cast< char* >(&lstat));
  for (int idx=0; idx<8; idx++ ) {
  // I have to turn the bytes in reverse, because the user of this
  // function will reverse them.
      ptr[idx] = p[7-idx];
  }
}
/* 2008-07-01 M.Tasaki. We have to use the functions in MountACACommonHWSimBase.cpp
void MountACACommonHWSimImplDrive::getAzAuxMode(unsigned char& status) const
{
    unsigned char auxmode = Az->auxMode1;
    unsigned char* ptr = (unsigned char*)&status;
    ptr[0] = auxmode;
}
void MountACACommonHWSimImplDrive::getElAuxMode(unsigned char& status) const
{
//    unsigned char auxmode = El->auxMode1;
//    unsigned char* ptr = (unsigned char*)&status;
//    ptr[0] = auxmode;
}
void MountACACommonHWSimImplDrive::getAzRatefdbkMode(unsigned char& status) const
{
//    unsigned char ratefdbkmode = Az->ratefdbkMode1;
//    unsigned char* ptr = (unsigned char*)&status;
//    ptr[0] = ratefdbkmode;
}
void MountACACommonHWSimImplDrive::getElRatefdbkMode(unsigned char& status) const
{
//    unsigned char ratefdbkmode = El->ratefdbkMode1;
//    unsigned char* ptr = (unsigned char*)&status;
//    ptr[0] = ratefdbkmode;
}
*/
//------------------------------------
// Set drive status
//------------------------------------
/*
void MountACACommonHWSimImplDrive::setAzAuxMode(unsigned char status) const
{
 //   unsigned char* ptr = (unsigned char*)&status;
 //   ptr[0] = auxmode;
    Az->auxMode1 = status;
}
void MountACACommonHWSimImplDrive::setElAuxMode(unsigned char status) const
{
 //   El->auxMode1 = status;
}
void MountACACommonHWSimImplDrive::setAzRatefdbkMode(unsigned char status) const
{
 //   Az->ratefdbkMode1 = status;
}
void MountACACommonHWSimImplDrive::setElRatefdbkMode(unsigned char status) const
{
  //  El->ratefdbkMode1 = status;
}
void MountACACommonHWSimImplDrive::setSubrefMode1(unsigned char status) const
{
 //   subrefMode1 = status;
}
*/

