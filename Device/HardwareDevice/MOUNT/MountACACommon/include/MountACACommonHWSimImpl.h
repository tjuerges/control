/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MountACACommonHWSimImpl.h
 */
#ifndef MountACACommonHWSimImpl_H
#define MountACACommonHWSimImpl_H

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include "MountACACommonHWSimBase.h"
#include "MountACACommonHWSimImplDrive.h"

namespace AMB
{
    /* Empty example class -- please feel free to modify here inherited methods */
    class MountACACommonHWSimImpl : public MountACACommonHWSimBase
    {
      public :
	MountACACommonHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber);
	~MountACACommonHWSimImpl();

        static const double bitsPerDegree;
        static const double degreesPerBit;
        static const double encoderOffset;
        void start();
        void stop();

      protected :
//  Copied from MountVertexHWSimImpl.h -----------
      virtual std::vector<CAN::byte_t> getMonitorAcuModeRsp() const;
      virtual void setControlAcuModeCmd(const std::vector<CAN::byte_t>& data);
      virtual void setMonitorAcuModeRsp(const std::vector<CAN::byte_t>& data);
      virtual std::vector<CAN::byte_t> getMonitorAzPosnRsp() const;
      virtual std::vector<CAN::byte_t> getMonitorElPosnRsp() const;
      virtual std::vector<CAN::byte_t> getMonitorAzEnc() const;
      virtual std::vector<CAN::byte_t> getMonitorElEnc() const;
      virtual void setControlAzTrajCmd(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetAzTrajCmd(const std::vector<CAN::byte_t>& data);
      virtual void setControlElTrajCmd(const std::vector<CAN::byte_t>& data);
      virtual std::vector<CAN::byte_t> getMonitorAzBrake() const;
      virtual std::vector<CAN::byte_t> getMonitorElBrake() const;
      virtual void setControlSetAzBrake(const std::vector<CAN::byte_t>& data);
      virtual void setControlSetElBrake(const std::vector<CAN::byte_t>& data);

      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff0() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff1() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff2() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff3() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff4() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff5() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff6() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff7() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff8() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeff9() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeffA() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeffB() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeffC() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeffD() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeffE() const ;
      virtual std::vector<CAN::byte_t> getMonitorAzServoCoeffF() const ;

      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff0() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff1() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff2() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff3() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff4() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff5() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff6() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff7() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff8() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeff9() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeffA() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeffB() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeffC() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeffD() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeffE() const ;
      virtual std::vector<CAN::byte_t> getMonitorElServoCoeffF() const ;

      virtual void setControlSetAzServoDefault(const std::vector<CAN::byte_t> &data);
      virtual void setControlSetElServoDefault(const std::vector<CAN::byte_t> &data);
      virtual void setControlSetStowPin(const std::vector<CAN::byte_t>& data);
      virtual std::vector<CAN::byte_t> getMonitorStowPin() const;


//  ACA specific simulation codes ----------------
        virtual std::vector<CAN::byte_t> getMonitorAzStatus() const;
        virtual std::vector<CAN::byte_t> getMonitorAzStatus2() const;
        virtual std::vector<CAN::byte_t> getMonitorElStatus() const;
        virtual std::vector<CAN::byte_t> getMonitorElStatus2() const;

   //2008-07-01 Use ones in MountACACommonHWSimBase for the followin four functions.
   //     virtual std::vector<CAN::byte_t> getAzAuxMode() const;
   //     virtual std::vector<CAN::byte_t> getElAuxMode() const;
   //     virtual std::vector<CAN::byte_t> getAzRatefdbkMode() const;
   //     virtual std::vector<CAN::byte_t> getElRatefdbkMode() const;
        virtual std::vector<CAN::byte_t> getMonitorSystemStatus() const;
        virtual std::vector<CAN::byte_t> getMonitorSystemStatus2() const;

        virtual std::vector<CAN::byte_t> getMonitorSubrefModeRsp() const;

        virtual std::vector<CAN::byte_t> getMonitorMetrEquipStatus() const;

        virtual std::vector<CAN::byte_t> getMonitorMetrTemps1a() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps1b() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps1c() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps1d() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps1e() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps1f() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps20() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps21() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps22() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps23() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps24() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps25() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps26() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps27() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps28() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps29() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps2a() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps2b() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps2c() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps2d() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps2e() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps2f() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps30() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps31() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps32() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps33() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps34() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps35() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps36() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps37() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps38() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps39() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps3a() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps3b() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps3c() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps3d() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps3e() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps3f() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps40() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps41() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps42() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps43() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps44() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps45() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps46() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps47() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps48() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps49() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps4a() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps4b() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps4c() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps4d() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps4e() const;
        virtual std::vector<CAN::byte_t> getMonitorMetrTemps4f() const;


        virtual std::vector<CAN::byte_t> getMonitorUpsOutputVolts2() const;
        virtual std::vector<CAN::byte_t> getMonitorUpsOutputCurrent2() const;
// ---------------------------------------------------------------
//  Set
//----------------------------------------------------------------
        virtual void setControlSetAzAuxMode(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetElAuxMode(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetAzRatefdbkMode(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetElRatefdbkMode(const std::vector<CAN::byte_t> &data);
   //     virtual void setShutter(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetShutter(const std::vector<CAN::byte_t>& data);

        virtual void setControlSetPtDefault(const std::vector<CAN::byte_t>& data);

        virtual void setControlSetAzServoCoeff0(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff1(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff2(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff3(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff4(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff5(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff6(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff7(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff8(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeff9(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffA(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffB(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffC(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffD(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffE(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffF(const std::vector<CAN::byte_t>& data);

        virtual void setControlSetElServoCoeff0(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff1(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff2(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff3(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff4(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff5(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff6(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff7(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff8(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeff9(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffA(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffB(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffC(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffD(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffE(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffF(const std::vector<CAN::byte_t>& data);

        virtual void setSubrefModeCmd(const std::vector<CAN::byte_t>& data);

        virtual void setControlSetSubrefPtCoeff00(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff01(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff02(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff03(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff04(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff05(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff06(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff07(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff08(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff09(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff0a(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff0b(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff0c(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff0d(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff0e(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff0f(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff10(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff11(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff12(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff13(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff14(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff15(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff16(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff17(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff18(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff19(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff1a(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff1b(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff1c(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff1d(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff1e(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtCoeff1f(const std::vector<CAN::byte_t> &data);
        virtual void setControlSetSubrefPtDefault(const std::vector<CAN::byte_t> &data);

        virtual void setControlSetMetrCoeff00(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff01(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff02(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff03(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff04(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff05(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff06(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff07(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff08(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff09(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff0a(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff0b(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff0c(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff0d(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff0e(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff0f(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff10(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff11(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff12(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff13(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff14(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff15(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff16(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff17(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff18(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff19(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff1a(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff1b(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff1c(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff1d(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff1e(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrCoeff1f(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrDefault(const std::vector<CAN::byte_t>& data);

        // Moved from MountHWSimImpl
	virtual void setControlSetPtModelCoeff10(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff11(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff12(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff13(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff14(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff15(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff16(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff17(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff18(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff19(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff1a(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff1b(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff1c(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff1d(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff1e(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff1f(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetMetrMode(const std::vector<CAN::byte_t>& data);

        ACA::MountACACommonHWSimImplDrive drive_m;

	virtual void setSubrefDeltaZeroCmd(const std::vector< CAN::byte_t >& data);

    }; // class MountACACommonHWSimImpl
} // namespace AMB

#endif // MountACACommonHWSimImpl_H
