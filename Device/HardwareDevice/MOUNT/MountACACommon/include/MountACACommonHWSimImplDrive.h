//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

//----------------------------------------------------------------------------
//  Created by Masa Tasaki based on MountVertexHWSimImplDrive.h
//----------------------------------------------------------------------------

#ifndef MountACACommonHWSimImplDrive_H
#define MountACACommonHWSimImplDrive_H

#include "Drive.h"
#include "MountACACommonHWSimImplAxis.h"

/* 2008-07-01 M.Tasaki EL_MAINT_STOW should not be 90.0.
   We should use the definition in Drive.h
// definitions that are configurable. Already defined in Drive.h
#define AZ_LOWER_LIMIT  -270.
#define AZ_UPPER_LIMIT   270.
//#define EL_LOWER_LIMIT   -20.
//#define EL_UPPER_LIMIT   120.
#define AZ_MAINT_STOW     90.
#define AZ_SURVL_STOW     90.
#define EL_MAINT_STOW     90.
#define EL_SURVL_STOW     15.

//#undef  EL_LOWER_LIMIT
//#undef  EL_UPPER_LIMIT
// EL_LOWER_LIMIT causes warinig of redifinition.  
#define EL_LOWER_LIMIT2   2.
#define EL_UPPER_LIMIT2   88.9
#define AZ_POSITIVE_MAX_VELOCITY  6.
#define AZ_NEGATIVE_MAX_VELOCITY -6.
#define EL_POSITIVE_MAX_VELOCITY  3.
#define EL_NEGATIVE_MAX_VELOCITY -3.
*/

/*
 * Drive Class
 */
namespace ACA {
class MountACACommonHWSimImplDrive : public ACUSim::Drive

{
  public:
    MountACACommonHWSimImplDrive();
    virtual ~MountACACommonHWSimImplDrive();
    virtual void initialize();
    virtual void cleanUp();

    virtual void getAzStatus(unsigned long long& status) const;
    virtual void getAzStatus2(unsigned long long& status) const;
    virtual void getElStatus(unsigned long long& status) const;
    virtual void getElStatus2(unsigned long long& status) const;

//  2008-07-01 M.Tasaki We must use the functions in MountACACommonHWSimBase.cpp
//    virtual void getAzAuxMode(unsigned char& status) const;
//    virtual void getElAuxMode(unsigned char& status) const;
//    virtual void getAzRatefdbkMode(unsigned char& status) const;
//    virtual void getElRatefdbkMode(unsigned char& status) const;

//----------------------------------------------
// Set drive status
//----------------------------------------------
//  2008-07-01 M.Tasaki We must use the functions in MountACACommonHWSimBase.cpp
//    virtual void setAzAuxMode(unsigned char status) const;
//    virtual void setElAuxMode(unsigned char status) const;
//    virtual void setAzRatefdbkMode(unsigned char status) const;
//    virtual void setElRatefdbkMode(unsigned char status) const;
//    virtual void setSubrefMode1(unsigned char status) const;

//-----------------------------------------------
//  Copied from Drive.cpp to override
//-----------------------------------------------
 
    // Override members
   // MountACACommonHWSimImplAxis* Az;
   // MountACACommonHWSimImplAxis* Els;

    unsigned char subrefMode1;

  protected:

    static void* acaupdateLoop(void* data);

    bool acaactive_m;

  private:
      pthread_t acatid;

};
}; /* namespace */

#endif /* MountACACommonHWSimImplDrive_H */
