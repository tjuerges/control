//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

//------------------------------------------------------------
//  Created by Masa Tasaki based on MountVertexHWSimImplAxis.h  20080613
//------------------------------------------------------------

#ifndef MountACACommonHWSimImplAxis_H
#define MountACACommonHWSimImplAxis_H 

#include "AMBSimACUbase.h"
#include "Axis.h"

/*
 * MountACACommonHWSimImplAxis Class - track an axis motion
 */
namespace ACA 
{
//20080613M.T  class MountACACommonHWSimImplAxis : public ACUSim::Axis {
//             class MountACACommonHWSimImplAxis {

  class MountACACommonHWSimImplAxis : public ACUSim::Axis {


  public:

   double current_pos, current_vel, current_acc;
  

    bool isPinIn;

//  2008-07-01 M.Tasaki We must use the functions in MountACACommonHWSimBase.cpp
//    unsigned char auxMode1;
//    unsigned char ratefdbkMode1;


    MountACACommonHWSimImplAxis();


//------------------
//d20080702    void setStowPos(double maint,double survive);

    virtual unsigned long long getStatus();
    virtual unsigned long long getStatus2();

//------------------------------------------------------
//  Functions override Axis
//------------------------------------------------------
    virtual void inc_cur();
    // virtual void MountACACommonHWSimImplAxis::tick();

  private:
    // stow positions
    //d20080702 double maintStowPosition, survivalStowPosition;


   // clock tick
   int clockCount;


//----------AZ(EL)Status byte0
    bool is2ndLimitCwUp; 
    bool is2ndLimitCcwDown ;
    bool is1stLimitCwUp ;
    bool is1stLimitCcwDown ;
    bool isPreLimitCwUp ;
    bool isPreLimitCcwDown;
    bool isLimWarningCwUp ;
    bool isLimWarningCcwDown;
//----------AZ(EL)Status byte1
    bool isInvalidDirection;
    bool isOverrideSwitchEnable;
//----------AZ(EL)Status byte2
    bool isL1DpaFault; 
    bool isL2DpaFault; 
    bool isR1DpaFault;
    bool isR2DpaFault;
    bool isL1MotorOverHeat;
    bool isL2MotorOverHeat;
    bool isR1MotorOverHeat;
    bool isR2MotorOverHeat;
//----------AZ(EL)Status byte3
    bool isL1CapacitorBankFault;
    bool isL2CapacitorBankFault;
    bool isR1CapacitorBankFault;
    bool isR2CapacitorBankFault;
    bool isL1CapacitorBankFull;
    bool isL2CapacitorBankFull;
    bool isR1CapacitorBankFull;
    bool isR2CapacitorBankFull;
//----------AZ(EL)Status byte4
    bool isL1ExcessiveCurrent;
    bool isL2ExcessiveCurrent;
    bool isR1ExcessiveCurrent;
    bool isR2ExcessiveCurrent;
    bool isServoOscillation;
    bool isRunaway;
    bool isOverSpeed;
    bool isAngleInputFault;
//----------AZ(EL)Status byte5
    bool isLDpcaRackCircuitBrakerOff;
    bool isRDpcaRackCircuitBrakerOff;
    bool isAllEncoderAlarm;
    bool isLFEncoderAlarm;
    bool isLREncoderAlarm;
    bool isRFEncoderAlarm;
    bool isRREncoderAlarm;
//----------AZ(EL)Status byte6
    bool isBrakerPowerFailure;
    bool isBrakerPowerFuseDown;
//----------AZ(EL)Status byte7
    bool isSurvivalStowPos;
    bool isMaintStowPos;
    bool isCableOverwrapCw;
    bool isCableOverwrapCcw;
    bool isOtherFault ;

  };
}; /* namespace */
#endif /* MountACACommonHWSimImplAxis_H */
