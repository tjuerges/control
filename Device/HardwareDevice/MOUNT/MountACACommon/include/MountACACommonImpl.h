// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Nobeyama Radio Observatory - NAOJ, 2008
// (c) Associated Universities Inc., 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MountACACommonImpl_H
#define MountACACommonImpl_H

#include <MountACACommonBase.h>
#include <MountACACommonS.h>

namespace Control {
  class MountACACommonImpl:
      public virtual POA_Control::MountACACommon,
      public ::MountACACommonBase
  {
    public:
      
      MountACACommonImpl(const ACE_CString& name, maci::ContainerServices* pCS);
      ~MountACACommonImpl();
  
      /// \exception ControlExceptions::CAMBErrorExImpl
      /// \exception ControlExceptions::INACTErrorExImpl
      virtual std::vector< unsigned char > getGetAcuError(ACS::Time& timestamp);

    protected:
  
      virtual double subrefXLimit();
      virtual double subrefYLimit();
      virtual double subrefZLimit();
  
      /// \exception ControlExceptions::CAMBErrorExImpl
      /// \exception ControlExceptions::INACTErrorExImpl
      virtual std::vector< unsigned char > getAzStatus(ACS::Time& timestamp);
  
      /// \exception ControlExceptions::CAMBErrorExImpl
      /// \exception ControlExceptions::INACTErrorExImpl
      virtual std::vector< unsigned char > getAzStatus2(ACS::Time& timestamp);
  
      /// \exception ControlExceptions::CAMBErrorExImpl
      /// \exception ControlExceptions::INACTErrorExImpl
      virtual std::vector< unsigned char > getElStatus(ACS::Time& timestamp);
  
      /// \exception ControlExceptions::CAMBErrorExImpl
      /// \exception ControlExceptions::INACTErrorExImpl
      virtual std::vector< unsigned char > getElStatus2(ACS::Time& timestamp);
  
      /// \exception ControlExceptions::CAMBErrorExImpl
      /// \exception ControlExceptions::INACTErrorExImpl
      virtual std::vector< unsigned char > getSystemStatus(ACS::Time& timestamp);
  
      /// \exception ControlExceptions::CAMBErrorExImpl
      /// \exception ControlExceptions::INACTErrorExImpl
      virtual std::vector<unsigned char> getSystemStatus2(ACS::Time& timestamp);
      
      /// Set the cabin temperature to a default value.
      virtual void hwInitializeAction();
      virtual void hwOperationalAction();
      
      /// Set the metrology mode in the ACU. This just logs the metrology mode
      /// and calls the base class version.
      /// \exception ControlExceptions::CAMBErrorEx, 
      /// \exception ControlExceptions::INACTErrorEx
      void SET_METR_MODE(const Control::LongSeq& world);
  
      /// Sends the CLEAR_FAULT_CMD to the ACU. Before doing it checks the axis
      /// (az, el, subref) and make sure they are in shutdown.
      /// \exception ControlExceptions::CAMBErrorEx, 
      /// \exception ControlExceptions::INACTErrorEx
      void SET_CLEAR_FAULT_CMD();

      // A function that sets up all the alarms that might be sent by this class.
      virtual void configureAlarms();
  
      // Alarms that this class might send
      //// GET_SYSTEM_STATUS
      /// Byte 0
      static const int SYSTEM_STATUS_EMERGENCY_STOP;
      static const int SYSTEM_STATUS_STAIRWAY_INTERLOCK;
      static const int SYSTEM_STATUS_HANDLING_INTERLOCK;
      static const int SYSTEM_STATUS_SMOKE_ALARM;
      static const int SYSTEM_STATUS_ACU_FAULT;
      static const int SYSTEM_STATUS_SURVIVAL_FOR_MISSING_COMMANDS;
      static const int SYSTEM_STATUS_SURVIVAL_FOR_MISSING_TIMING;
      static const int SYSTEM_STATUS_TIMING_PULSE;
      /// Byte 1: sfety interlock
      static const int SYSTEM_STATUS_RCV_CABIN_SAFETY;
      static const int SYSTEM_STATUS_ANT_BASE_SAFETY;
      static const int SYSTEM_STATUS_VERANDA_EQUIP_FREE;
      static const int SYSTEM_STATUS_LADDER_SWITCH;
      static const int SYSTEM_STATUS_TRANSP_INTLK;
      static const int SYSTEM_STATUS_TRANSP_INTLK_EXCPT_AZ;
      // bit6-7: reserved 
      /// Byte 2: door interlock 
      static const int SYSTEM_STATUS_RCV_CABIN_DOOR_OPEN;
      static const int SYSTEM_STATUS_CNTR_CABIN_DOOR_OPEN;
      static const int SYSTEM_STATUS_ANT_BASE_DOOR_OPEN;
      static const int SYSTEM_STATUS_UPS_ROOM_DOOR_OPEN;  //Only meaningful for MountACA
      static const int SYSTEM_STATUS_PDB_ROOM_DOOR_OPEN;  //Only meaningful for MountACA
      static const int SYSTEM_STATUS_MAIN_REF_HATCH_OPEN;  //Only meaningful for MountACA
      static const int SYSTEM_STATUS_YOKE_L_HATCH_OPEN;
      static const int SYSTEM_STATUS_YOKE_R_HATCH_OPEN;
      /// Byte 3: handle interlock  
      static const int SYSTEM_STATUS_AZ_HNDL_INTLK;
      static const int SYSTEM_STATUS_EL_HNDL_INTLK;
      static const int SYSTEM_STATUS_AZ_STOW_HNDL_INTLK;
      static const int SYSTEM_STATUS_EL_STOW_HNDL_INTLK;
      static const int SYSTEM_STATUS_SHUTTER_HNDL_INTLK;
      // bit5-7: reserved 
      /// Byte 4: stow status  
      static const int SYSTEM_STATUS_AZ_STOW_TO;
      static const int SYSTEM_STATUS_AZ_STOW_THERMAL_TRIP;
      static const int SYSTEM_STATUS_STOW_DPA_CB_OFF;
      static const int SYSTEM_STATUS_STOW_DPA_COND_OFF;
      static const int SYSTEM_STATUS_EL_STOW_TO;
      static const int SYSTEM_STATUS_EL_STOW_THERMAL_TRIP;
      // bit6-7: reserved 
      /// Byte 5: shutter status 
      static const int SYSTEM_STATUS_SHUTTER_TO;
      static const int SYSTEM_STATUS_SHUTTER_THERMAL_TRIP;
      static const int SYSTEM_STATUS_SHUTTER_DPA_CB_OFF;
      static const int SYSTEM_STATUS_SHUTTER_DPA_COND_OFF;
      // bit4-6: reserved 
      static const int SYSTEM_STATUS_ZENITH_SHUTTER_OPEN;
      /// Byte 6: misc status 
      static const int SYSTEM_STATUS_RD_CONV_FAIL;
      static const int SYSTEM_STATUS_IF_PANEL_FAIL;
      static const int SYSTEM_STATUS_ACR2_PS_ALARM;  //Only meaningful for MountA7M
      static const int SYSTEM_STATUS_NUTATOR_RACK_PS_ALARM;
      static const int SYSTEM_STATUS_BRAKE_AXIS_HEATER_ALARM;
      static const int SYSTEM_STATUS_MOTOR_PUMP_FAIL;
      static const int SYSTEM_STATUS_ARRESTER_BROKEN_ALARM;
      static const int SYSTEM_STATUS_DC_POWER_CIRCUIT_ALARM;
      /// Byte 7: misc status 
      static const int SYSTEM_STATUS_ACU_MAINT;
      static const int SYSTEM_STATUS_ANT_STATION_POWER_DISCON;
      static const int SYSTEM_STATUS_ANT_INTLK;
      static const int SYSTEM_STATUS_NUTATOR_RACK_CB_OFF;
      static const int SYSTEM_STATUS_DRIVE_SYSTEM_FAIL;
      static const int SYSTEM_STATUS_MORE_PCU_CONNECT;
      
      //// GET_SYSTEM_STATUS_2
      /// Byte 0 (ubyte): ACU fault 
      static const int SYSTEM_STATUS_DC12V_FAIL;
      static const int SYSTEM_STATUS_SYS_FAIL;
      static const int SYSTEM_STATUS_AC_FAIL;
      static const int SYSTEM_STATUS_DATA_FAULT;
      static const int SYSTEM_STATUS_AD_CONV_FAIL;
      // bit5: reserved 
      static const int SYSTEM_STATUS_CAN_BOARD_FAIL;
      static const int SYSTEM_STATUS_CLOCK_BOARD_FAIL;
      /// Byte 1 (ubyte): ACU fault 
      static const int SYSTEM_STATUS_DSP_FAIL;
      static const int SYSTEM_STATUS_DSP_AD_CONV_FAIL;
      // bit2-7: reserved 
      /// Byte 2 (ubyte): ACU fault 
      static const int SYSTEM_STATUS_IMP1_FAIL;
      static const int SYSTEM_STATUS_IMP2_FAIL;
      static const int SYSTEM_STATUS_IMP3_FAIL;
      static const int SYSTEM_STATUS_IMP4_FAIL;
      static const int SYSTEM_STATUS_IMP5_FAIL;
      static const int SYSTEM_STATUS_IMP6_FAIL;
      static const int SYSTEM_STATUS_IMP7_FAIL;
      // bit7: reserved 
      /// Byte 3 (ubyte): PCU 
      static const int SYSTEM_STATUS_PCU1_CONNECTED;
      static const int SYSTEM_STATUS_PCU2_CONNECTED;
      static const int SYSTEM_STATUS_PCU3_CONNECTED;
      // bit3-7: reserved 
      /// Byte 4 (ubyte): drive system failure (DIFC : DPA Interface Card) 
      // bit0-5: reserved 
      static const int SYSTEM_STATUS_DIFC_FAULT;
      static const int SYSTEM_STATUS_DIFC_POWER_FAIL;
      /// Byte 5 (ubyte): drive system failure 
      static const int SYSTEM_STATUS_ACU2DIFC_LINE1_DISCON;
      static const int SYSTEM_STATUS_ACU2DIFC_LINE2_DISCON;
      static const int SYSTEM_STATUS_DIFC2ACU_LINE1_DISCON;
      static const int SYSTEM_STATUS_DIFC2ACU_LINE2_DISCON;
      static const int SYSTEM_STATUS_ACU2DIFC_LINE3_DISCON;
      // bit5-7: reserved 
      /// Bytes 6-7: reserved 
  
      //// GET_AZ_STATUS
      /// Byte 0 (ubyte): limit status 
      static const int AZ_STATUS_LIMIT2_CW;
      static const int AZ_STATUS_LIMIT2_CCW;
      static const int AZ_STATUS_LIMIT1_CW;
      static const int AZ_STATUS_LIMIT1_CCW;
      static const int AZ_STATUS_PRE_LIMIT_CW;
      static const int AZ_STATUS_PRE_LIMIT_CCW;
      static const int AZ_STATUS_LIMIT_WARN_CW;
      static const int AZ_STATUS_LIMIT_WARN_CCW;
      /// Byte 1 (ubyte): 1st limit, override switch 
      static const int AZ_STATUS_INVALID_DIR;
      static const int AZ_STATUS_OVERRIDE_SW_ENABLED;
      // bit2-7: reserved 
      /// Byte 2 (ubyte): drive system failure 
      static const int AZ_STATUS_AZ_L1_DPA_FAULT;
      static const int AZ_STATUS_AZ_L2_DPA_FAULT;
      static const int AZ_STATUS_AZ_R1_DPA_FAULT;
      static const int AZ_STATUS_AZ_R2_DPA_FAULT;
      static const int AZ_STATUS_AZ_L1_MOTOR_OH;
      static const int AZ_STATUS_AZ_L2_MOTOR_OH;
      static const int AZ_STATUS_AZ_R1_MOTOR_OH;
      static const int AZ_STATUS_AZ_R2_MOTOR_OH;
      /// Byte 3 (ubyte): capacitor bank status 
      static const int AZ_STATUS_AZ_L1_CBANK_FAULT;
      static const int AZ_STATUS_AZ_L2_CBANK_FAULT;
      static const int AZ_STATUS_AZ_R1_CBANK_FAULT;
      static const int AZ_STATUS_AZ_R2_CBANK_FAULT;
      static const int AZ_STATUS_AZ_L1_CBANK_FULL;
      static const int AZ_STATUS_AZ_L2_CBANK_FULL;
      static const int AZ_STATUS_AZ_R1_CBANK_FULL;
      static const int AZ_STATUS_AZ_R2_CBANK_FULL;
      /// Byte 4 (ubyte): servo condition 
      static const int AZ_STATUS_AZ_L1_EXCSV_CURR;
      static const int AZ_STATUS_AZ_L2_EXCSV_CURR;
      static const int AZ_STATUS_AZ_R1_EXCSV_CURR;
      static const int AZ_STATUS_AZ_R2_EXCSV_CURR;
      static const int AZ_STATUS_SERVO_OSCILLATION;
      static const int AZ_STATUS_RUNAWAY;
      static const int AZ_STATUS_OVERSPEED;
      static const int AZ_STATUS_ANGLE_INPUT_FAULT;
      /// Byte 5 (ubyte): drive system failure 
      static const int AZ_STATUS_AZ_L_DCPA_CB_OFF;
      static const int AZ_STATUS_AZ_R_DCPA_CB_OFF;
      // bit2: reserved 
      static const int AZ_STATUS_AZ_ALL_ENCODER_ALARM;
      static const int AZ_STATUS_AZ_LF_ENCODER_ALARM;
      static const int AZ_STATUS_AZ_LR_ENCODER_ALARM;
      static const int AZ_STATUS_AZ_RF_ENCODER_ALARM;
      static const int AZ_STATUS_AZ_RR_ENCODER_ALARM;
      /// Byte 6 (ubyte): brake condition 
      // bit0-3: reserved 
      static const int AZ_STATUS_BRAKE_POWER_FAIL;
      static const int AZ_STATUS_BRAKE_PS_FUSE_BLOW;
      // bit6-7: reserved 
      /// Byte 7 (ubyte): misc status 
      static const int AZ_STATUS_SURVIVAL_STOW_POS;
      static const int AZ_STATUS_MAINT_STOW_POS;
      // bit2-3: reserved 
      static const int AZ_STATUS_CABLE_OVERLAP_CW;
      static const int AZ_STATUS_CABLE_OVERLAP_CCW;
      // bit6: reserved 
      static const int AZ_STATUS_OTHER_FAULT;
                  
      //// GET_AZ_STATUS_2
      /// Byte 0 (ubyte): drive system failure 
      static const int AZ_STATUS_AZ_LF_ENCDR_DISCON;
      static const int AZ_STATUS_AZ_LR_ENCDR_DISCON;
      static const int AZ_STATUS_AZ_RF_ENCDR_DISCON;
      static const int AZ_STATUS_AZ_RR_ENCDR_DISCON;
      static const int AZ_STATUS_AZ_LF_ENCDR_NOT_INIT;
      static const int AZ_STATUS_AZ_LR_ENCDR_NOT_INIT;
      static const int AZ_STATUS_AZ_RF_ENCDR_NOT_INIT;
      static const int AZ_STATUS_AZ_RR_ENCDR_NOT_INIT;
      /// Byte 1 (ubyte): drive system failure 
      static const int AZ_STATUS_AZ_L1_DPA_DISCON;
      static const int AZ_STATUS_AZ_L2_DPA_DISCON;
      static const int AZ_STATUS_AZ_R1_DPA_DISCON;
      static const int AZ_STATUS_AZ_R2_DPA_DISCON;
      // bit4-7: reserved 
      /// Byte 2 (ubyte): drive system failure 
      static const int AZ_STATUS_AZ_L1_DPA_COND_OFF;
      static const int AZ_STATUS_AZ_L2_DPA_COND_OFF;
      static const int AZ_STATUS_AZ_R1_DPA_COND_OFF;
      static const int AZ_STATUS_AZ_R2_DPA_COND_OFF;
      static const int AZ_STATUS_AZ_L1_DPA_DISCHARGE;
      static const int AZ_STATUS_AZ_L2_DPA_DISCHARGE;
      static const int AZ_STATUS_AZ_R1_DPA_DISCHARGE;
      static const int AZ_STATUS_AZ_R2_DPA_DISCHARGE;
      /// Byte 3 (ubyte): servo condition 
      static const int AZ_STATUS_POSN_FILTER_FAULT;
      static const int AZ_STATUS_MAJOR_FILTER_FAULT;
      static const int AZ_STATUS_MINOR_FILTER_FAULT;
      static const int AZ_STATUS_FDBK_FILTER_FAULT;
      // bit4-7: reserved 
      /// Byte 4 (ubyte): drive on timeout 
      static const int AZ_STATUS_AZ_L1_DRIVE_ON_TO;
      static const int AZ_STATUS_AZ_L2_DRIVE_ON_TO;
      static const int AZ_STATUS_AZ_R1_DRIVE_ON_TO;
      static const int AZ_STATUS_AZ_R2_DRIVE_ON_TO;
      static const int AZ_STATUS_AZ_L1_POWER_ON_TO;
      static const int AZ_STATUS_AZ_L2_POWER_ON_TO;
      static const int AZ_STATUS_AZ_R1_POWER_ON_TO;
      static const int AZ_STATUS_AZ_R2_POWER_ON_TO;
      /// Byte 5 (ubyte): brake axis failure 
      static const int AZ_STATUS_BRAKE_AXIS_DPA_FAULT;
      static const int AZ_STATUS_BRAKE_AXIS_DPA_CB_OFF;
      static const int AZ_STATUS_BRAKE_AXIS_DPA_COND_OFF;
      static const int AZ_STATUS_BRAKE_AXIS_RSLV_DISCON;
      static const int AZ_STATUS_BRAKE_AXIS_ASYNC;
      // bit5-6: reserved
      static const int AZ_STATUS_BRAKE_AXIS_DRIVE_ON_TO;
      /// Bytes 6-7: reserved 
   
      //// GET_EL_STATUS
      /// Byte 0 (ubyte): limit status 
      static const int EL_STATUS_LIMIT2_UP;
      static const int EL_STATUS_LIMIT2_DOWN;
      static const int EL_STATUS_LIMIT1_UP;
      static const int EL_STATUS_LIMIT1_DOWN;
      static const int EL_STATUS_PRE_LIMIT_UP;
      static const int EL_STATUS_PRE_LIMIT_DOWN;
      static const int EL_STATUS_LIMIT_WARN_UP;
      static const int EL_STATUS_LIMIT_WARN_DOWN;
      /// Byte 1 (ubyte): 1st limit, override 
      static const int EL_STATUS_INVALID_DIR;
      static const int EL_STATUS_OVERRIDE_SW_ENABLED;
      // bit2-7: reserved 
      /// Byte 2 (ubyte): drive system failure 
      static const int EL_STATUS_EL_L_DPA_FAULT;
      static const int EL_STATUS_EL_R_DPA_FAULT;
      // bit2-3: reserved 
      static const int EL_STATUS_EL_L_MOTOR_OH;
      static const int EL_STATUS_EL_R_MOTOR_OH;
      // bit6-7: reserved 
      /// Byte 3 (ubyte): capacitor bank status 
      static const int EL_STATUS_EL_L_CBANK_FAULT;
      static const int EL_STATUS_EL_R_CBANK_FAULT;
      // bit2-3: reserved 
      static const int EL_STATUS_EL_L_CBANK_FULL;
      static const int EL_STATUS_EL_R_CBANK_FULL;
      // bit6-7: reserved 
      /// Byte 4 (ubyte): servo condition 
      static const int EL_STATUS_EL_L_EXCSV_CURR;
      static const int EL_STATUS_EL_R_EXCSV_CURR;
      // bit2-3: reserved 
      static const int EL_STATUS_SERVO_OSCILLATION;
      static const int EL_STATUS_RUNAWAY;
      static const int EL_STATUS_OVERSPEED;
      static const int EL_STATUS_ANGLE_INPUT_FAULT;
      /// Byte 5 (ubyte): drive system failure 
      static const int EL_STATUS_CB_OFF;
      static const int EL_STATUS_DCPA_CB_OFF;  //Only meaningful for MountA7M
      // bit2: reserved 
      static const int EL_STATUS_EL_ALL_ENCDR_ALARM;
      static const int EL_STATUS_EL_F_ENCDR_ALARM;
      static const int EL_STATUS_EL_R_ENCDR_ALARM;
      // bit6-7: reserved 
      /// Byte 6 (ubyte): brake condition 
      // bit0-3: reserved 
      static const int EL_STATUS_BRAKE_POWER_FAIL;
      static const int EL_STATUS_BRAKE_PS_FUSE_BLOW;
      // bit6-7: reserved 
      /// Byte 7 (ubyte): misc status 
      static const int EL_STATUS_SURVIVAL_STOW_POS;
      static const int EL_STATUS_MAINT_STOW_POS;
      static const int EL_STATUS_ZENITH_POS;
      // bit3-6: reserved 
      static const int EL_STATUS_OTHER_FAULT;
  
      /// GET_EL_STATUS_2
      // Byte 0 (ubyte): drive system failure 
      static const int EL_STATUS_EL_F_ENCDR_DISCON;
      static const int EL_STATUS_EL_R_ENCDR_DISCON;
      // bit2-3: reserved 
      static const int EL_STATUS_EL_F_ENCDR_NOT_INIT;
      static const int EL_STATUS_EL_R_ENCDR_NOT_INIT;
      // bit6-7: reserved 
      /// Byte 1 (ubyte): drive system failure 
      static const int EL_STATUS_EL_L_DPA_DISCON;
      static const int EL_STATUS_EL_R_DPA_DISCON;
      // bit2-7: reserved 
      /// Byte 2 (ubyte): drive system failure 
      static const int EL_STATUS_EL_L_DPA_COND_OFF;
      static const int EL_STATUS_EL_R_DPA_COND_OFF;
      // bit2-3: reserved 
      static const int EL_STATUS_EL_L_DPA_DISCHARGE;
      static const int EL_STATUS_EL_R_DPA_DISCHARGE;
      // bit6-7: reserved 
      /// Byte 3 (ubyte): servo condition 
      static const int EL_STATUS_POSN_FILTER_FAULT;
      static const int EL_STATUS_MAJOR_FILTER_FAULT;
      static const int EL_STATUS_MINOR_FILTER_FAULT;
      static const int EL_STATUS_FDBK_FILTER_FAULT;
      // bit4-7: reserved 
      /// Byte 4 (ubyte): drive on timeout 
      static const int EL_STATUS_EL_L_DRIVE_ON_TO;
      static const int EL_STATUS_EL_R_DRIVE_ON_TO;
      // bit2-3: reserved 
      static const int EL_STATUS_EL_L_POWER_ON_TO;
      static const int EL_STATUS_EL_R_POWER_ON_TO;
      // bit6-7: reserved 
      /// Byte 5 (ubyte): brake axis failure 
      static const int EL_STATUS_BRAKE_AXIS_DPA_FAULT;
      static const int EL_STATUS_BRAKE_AXIS_DPA_CB_OFF;
      static const int EL_STATUS_BRAKE_AXIS_DPA_COND_OFF;
      static const int EL_STATUS_BRAKE_AXIS_RSLV_DISCON;
      static const int EL_STATUS_BRAKE_AXIS_ASYNC;
      // bit5-6: reserved 
      static const int EL_STATUS_BRAKE_AXIS_DRIVE_ON_TO;
      /// Bytes 6-7: reserved 
  
    private:
      // copy and assignment are not allowed
      MountACACommonImpl(const MountACACommonImpl&);
      MountACACommonImpl& operator= (const MountACACommonImpl&);
  }; // end MountACACommonImpl class
}; // end Control namespace

// Remove the following line when the code generated base classes are moved
// into the Control namespace. Its needed by the Mount*Base classes.
using Control::MountACACommonImpl;

#endif // MOUNTACAIMPL_H
