#ifndef MountACACommonFocusModel_H
#define MountACACommonFocusModel_H
/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-03-26  created
* mtasaki   2008-07-06  created from MountVertexPrototypeFocusModel 
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C
#endif

#include <FocusModelBase.h>

//Forward reference
class MountACACommonImpl;

class MountACACommonFocusModel  : public Control::FocusModelBase
{
 public:
  MountACACommonFocusModel(MountACACommonImpl* mvp);
  MountACACommonFocusModel(MountACACommonImpl* mvp,
		 Logging::Logger::LoggerSmartPtr logger);
  virtual ~MountACACommonFocusModel();

  virtual void getFocusZeroPoint(double& xZero, double& yZero, double& zZero);
  virtual void getFocusOffset(double& xOffset, double& yOffset, double& zOffset);
  virtual void setFocusZeroPoint(double xZero, double yZero, double zZero);

 private:

  // Reference to the mount component (yuk!)
  MountACACommonImpl* mvp_m;

};



#endif /*!MOUNTACAFOCUSMODEL_H*/
