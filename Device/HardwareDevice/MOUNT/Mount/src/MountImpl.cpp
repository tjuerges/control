// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "MountImpl.h"
#include <FocusModel.h>
#include <TETimeUtil.h>
#include <MountError.h>
#include <ControlExceptions.h>
#include <ControlBasicInterfacesC.h> // for CHANNELNAME_CONTROLREALTIME
#include <AntennaStatusReportingC.h> // for Control::AntennaStatusReporting
#include <TmcdbErrType.h> // TMCDB error declarations
#include <TMCDBComponentC.h> // for TMCDB::PointingModel & TMCDB::FocusModel
#include <TMCDBAccessIFC.h> // for TMCDB::Access
#include <HardwareControllerC.h> // for TMCDB::Access
#include <RepeatGuard.h> // For the repeat guard in enforceSoftwareLimits
#include <acstimeTimeUtil.h> // for TimeUtil
#include <acsncSimpleSupplier.h>
#include <slamac.h> // for DPI
#include <slalib.h> // for slaDsep
#include <time.h>
#include <cmath>
#include <unistd.h>
#include <LogToAudience.h> // for LOG_TO_AUDIENCE

using Control::MountImpl;
using std::string;
using std::vector;
using std::ostringstream;
using Control::LongSeq;
using Control::MountStatusData;
using Control::MountStatusData_var;
using std::abs;
using std::max;
using std::min;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::HardwareErrorEx;
using ControlExceptions::IllegalParameterErrorEx;
using CORBA::String_var;

const char* MountImpl::DEFAULT_ANTENNA_MODEL_DESCRIPTOR="Mount";

const int MountImpl::CLOUDSAT_SHUTTER_NOT_CLOSED     = 0;
const int MountImpl::CLOUDSAT_SHUTTER_NOT_REOPEN     = 1;
const int MountImpl::NC_FAILURE                      = 2;
const int MountImpl::ABM_HW_COMM_ERROR               = 3;
const int MountImpl::AMBERR_RECEIVED_IN_MOUNT_STATUS = 4;
const int MountImpl::POINTING_MODEL_UNAVAILABLE      = 5;
const int MountImpl::POINTING_MODEL_ILLEGAL_TERMS    = 6;
const int MountImpl::INCORRECT_EXECUTION_TIME_FOUND  = 7;

//-----------------------------------------------------------------------------
MountImpl::MountImpl(const ACE_CString& name,
                     maci::ContainerServices* pCS)
    :MountBase(name, pCS),
     alarms_m(),
     antennaModel_m(DEFAULT_ANTENNA_MODEL_DESCRIPTOR),
     receiverCabinTemp_m(DEFAULT_REC_CAB_TEMP),
     encoderTransforms_m(new EncoderTransformations()),
     mountStatusBuffer_m(this,getLogger()),
     trajectoryQueue_m(getLogger()),
     mountAxisMode_m(this,getLogger()),
     focusModel_m(0),
     pointingModel_m(getLogger()),
     auxPointingModel_m(getLogger()),
     writerThread_p(NULL),
     readerThread_p(NULL),
     MountHwAzMax(270*M_PI/180.0),
     MountHwAzMin(-MountHwAzMax),
     MountHwElMax(88.9*M_PI/180.0),
     MountHwElMin(-2.0*M_PI/180.0),
     MountHwAzRateMax(6.0*M_PI/180.0),
     MountHwAzRateMin(-MountHwAzRateMax),
     MountHwElRateMax(3.0*M_PI/180.0),
     MountHwElRateMin(-MountHwElRateMax),
     azMax_m(MountHwAzMax),
     azMin_m(MountHwAzMin),
     elMax_m(MountHwElMax),
     elMin_m(MountHwElMin),
     isCLOUDSATEnabled_m(true),
     insideCLOUDSATRegion_m(false),
     insideCLOUDSATRegionPrevious_m(false),
     transitionIn_m(false),
     reopenShutter_m(false),
     avoidCLOUDSATRegion_m(false),
     mountStatusDataSupplier_p(NULL),
     mountStatusDataPublicationEnabled_m(0),
     tolerance_m(DEFAULT_TOLERANCE),
     engineeringTolerance_m(DEFAULT_TOLERANCE),
     engineeringTimeout_m(DEFAULT_ENGINEERING_TIMEOUT),
     engineeringPollingTime_m(DEFAULT_ENGINEERING_POLLING_TIME),
     engineeringRetries_m(1),
     cloudsatZenithDistanceLimit_m(DEFAULT_CLOUDSAT_ZENITH_DISTANCE_LIMIT),
     cloudsatElevationLowerLimit_m(DPIBY2 - DEFAULT_CLOUDSAT_ZENITH_DISTANCE_LIMIT),
     cloudsatElevationUpperLimit_m(DPIBY2 + DEFAULT_CLOUDSAT_ZENITH_DISTANCE_LIMIT),
     onSourceCounter_m(0),
     onSourceCriteria_m(DEFAULT_ON_SOURCE_CRITERIA),
     lastSubrefCmdX_m(0),
     lastSubrefCmdY_m(0),
     lastSubrefCmdZ_m(0),
     lastSubrefCmdTip_m(0),
     lastSubrefCmdTilt_m(0)
{
    mountStatus_m.antennaName = CORBA::string_dup
        (HardwareDeviceImpl::componentToAntennaName(name.c_str()).c_str());
    mountStatus_m.azCommandedValid = false;
    mountStatus_m.elCommandedValid = false;
    mountStatus_m.azPositionsValid = false;
    mountStatus_m.elPositionsValid = false;
    mountStatus_m.azEncoderValid   = false;
    mountStatus_m.elEncoderValid   = false;
    mountStatus_m.subrefPositionValid = false;
    mountStatus_m.subrefRotationValid = false;

    stringAmbErrorCode[AMBERR_NOERR]      = "OK";
    stringAmbErrorCode[AMBERR_CONTINUE]   = "CONTINUE";
    stringAmbErrorCode[AMBERR_BADCMD]     = "BADCMD";
    stringAmbErrorCode[AMBERR_BADCHAN]    = "BADCHAN";
    stringAmbErrorCode[AMBERR_UNKNOWNDEV] = "UNKNOWNDEV";
    stringAmbErrorCode[AMBERR_INITFAILED] = "INITFAILED";
    stringAmbErrorCode[AMBERR_WRITEERR]   = "WRITEERR";
    stringAmbErrorCode[AMBERR_READERR]    = "READERR";
    stringAmbErrorCode[AMBERR_FLUSHED]    = "FLUSHED";
    stringAmbErrorCode[AMBERR_TIMEOUT]    = "TIMEOUT";
    stringAmbErrorCode[AMBERR_RESPFIFO]   = "RESPFIFO";
    stringAmbErrorCode[AMBERR_NOMEM]      = "NOMEM";
    stringAmbErrorCode[AMBERR_PENDING]    = "PENDING";
    stringAmbErrorCode[AMBERR_ADDRERR]    = "ADDRERR";

    // I get container crashes, when the control subsystem shuts down, if I
    // move the following line to the initialize function. I've no idea why.
    mountStatusDataSupplier_p =
        new nc::SimpleSupplier(Control::CHANNELNAME_CONTROLREALTIME, this);
}

//-----------------------------------------------------------------------------
MountImpl::~MountImpl()
{
    // Normally the threads have already been shutdown
    destroyThreads();
    disconnectPositionSupplier();
    delete focusModel_m; focusModel_m = 0;
    delete encoderTransforms_m; encoderTransforms_m = 0;
}

/* ----------------- ACS Lifecycle ---------------------------- */
void MountImpl::initialize() {
    MountBase::initialize();


    // These need to be read from the TMCDB.. until then from the CDB

    std::auto_ptr<const ControlXmlParser> cdbConfig;

    EncoderTransformations::TransformationParameters azParams;
    EncoderTransformations::TransformationParameters elParams;
    readAxisParameters("azimuthParameters",
                       azParams,
                       MountHwAzMax,
                       MountHwAzMin,
                       MountHwAzRateMax,
                       MountHwAzRateMin);
    readAxisParameters("elevationParameters",
                       elParams,
                       MountHwElMax,
                       MountHwElMin,
                       MountHwElRateMax,
                       MountHwElRateMin);

    readMountParameters("mountParameters");

    if (encoderTransforms_m == NULL) {
        const char* __REASON__ = "Encoder transformation object has not been created";
        acsErrTypeLifeCycle::LifeCycleExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_ERROR,__REASON__));
        ex.addData("Reason",__REASON__);
        ex.log();
        throw ex;
    } else {
        encoderTransforms_m->setConversionFactors(azParams, elParams);
    }
    resetLimits();
    focusModel_m = new FocusModel(focalRatio(), beamDeviationFactor(),
                                  subrefXLimit(), subrefYLimit(), subrefZLimit(),
                                  subrefTipLimit(), subrefTiltLimit(), getLogger());
    // Setup the alarms
    configureAlarms();
}

void  MountImpl::configureAlarms() {

    const string antName =
        componentToAntennaName(String_var(name()).in());

    vector<Control::AlarmInformation> alarms(8);

    alarms[0].alarmCode = CLOUDSAT_SHUTTER_NOT_CLOSED;
    alarms[0].alarmDescription =
        string("The shutter is open and the mount is pointing at the zenith.")
        + " This makes the front-end vulnerable to damage from CloudSat.";
    alarms[1].alarmCode = CLOUDSAT_SHUTTER_NOT_REOPEN;
    alarms[1].alarmDescription = "The shutter will not open.";
    alarms[2].alarmCode = NC_FAILURE;
    alarms[2].alarmDescription = string("No position data can be sent")
        + " as the  notification channel is not working.";
    alarms[3].alarmCode = ABM_HW_COMM_ERROR;
    alarms[3].alarmDescription = string("Communication errors")
        +" when sending commands and/or monitor requests to the ACU.";
    alarms[4].alarmCode = AMBERR_RECEIVED_IN_MOUNT_STATUS;
    alarms[4].alarmDescription = string("Communication errors")
        + " in the monitor data received from the ACU.";

    alarms[5].alarmCode = POINTING_MODEL_UNAVAILABLE;
    alarms[5].alarmDescription =
        "No pointing model can be found in the data base.";

    alarms[6].alarmCode = POINTING_MODEL_ILLEGAL_TERMS;
    alarms[6].alarmDescription =
        "The pointing model is not valid. Some terms or values are incorrect.";

    alarms[7].alarmCode = INCORRECT_EXECUTION_TIME_FOUND;
    alarms[7].alarmDescription =
        "A command or monitor request was not executed at the correct time";

    alarms_m.initializeAlarms(antennaModel_m.c_str(), antName, alarms);
    alarms_m.forceTerminateAllAlarms();
}

void MountImpl::readAxisParameters(string elementName,
                                   EncoderTransformations::TransformationParameters &transformationParameters,
                                   double &maximumValue,
                                   double &minimumValue,
                                   double &maximumRate,
                                   double &minimumRate) {
    try {
        std::auto_ptr<const ControlXmlParser> cdbConfig = getElement(elementName.c_str());
        transformationParameters.scale   = (double) (cdbConfig->getDoubleAttribute("scale"));
        transformationParameters.offset  = (double) (cdbConfig->getDoubleAttribute("offset"));
        maximumValue                     = (double) (cdbConfig->getDoubleAttribute("maximum_value"));
        minimumValue                     = (double) (cdbConfig->getDoubleAttribute("minimum_value"));
        maximumRate                      = (double) (cdbConfig->getDoubleAttribute("maximum_rate"));
        minimumRate                      = (double) (cdbConfig->getDoubleAttribute("minimum_rate"));
    } catch(ControlExceptions::XmlParserErrorExImpl &_ex) {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_ERROR,"Could not read %s CDB element",elementName.c_str()));
        acsErrTypeLifeCycle::LifeCycleExImpl nex(_ex,__FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(...) {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_ERROR,"Unknown exception caught"));
        acsErrTypeLifeCycle::LifeCycleExImpl nex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void MountImpl::readMountParameters(string elementName) {
    try {
        std::auto_ptr<const ControlXmlParser> cdbConfig = getElement(elementName.c_str());
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"Reading on_source_criteria ..."));
        onSourceCriteria_m = static_cast<unsigned int>((cdbConfig->getLongAttribute("on_source_criteria")));

        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"Reading on_source_tolerance ..."));
        tolerance_m        = static_cast<double>((cdbConfig->getDoubleAttribute("on_source_tolerance")));

        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"engineering_tolerance ..."));
        engineeringTolerance_m = static_cast<double>((cdbConfig->getDoubleAttribute("engineering_tolerance")));

        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"engineering_timeout ..."));
        engineeringTimeout_m = static_cast<double>((cdbConfig->getDoubleAttribute("engineering_timeout")));

        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"engineering_polling_time ..."));
        engineeringPollingTime_m = static_cast<double>((cdbConfig->getDoubleAttribute("engineering_polling_time")));
        engineeringPollingTimeInUs_m = static_cast<int>(engineeringPollingTime_m * 1E6);
        engineeringRetries_m = static_cast<int>(engineeringTimeout_m/engineeringPollingTime_m);

        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"cloudsat_zenith_distance_limit ..."));
        cloudsatZenithDistanceLimit_m = static_cast<double>((cdbConfig->getDoubleAttribute("cloudsat_zenith_distance_limit")));
        cloudsatElevationLowerLimit_m = DPIBY2 - cloudsatZenithDistanceLimit_m;
        cloudsatElevationUpperLimit_m = DPIBY2 + cloudsatZenithDistanceLimit_m;

        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_DEBUG,"receiver_cabin_temperature ..."));
        receiverCabinTemp_m = static_cast<unsigned int>((cdbConfig->getDoubleAttribute("receiver_cabin_temp")));
    } catch(ControlExceptions::XmlParserErrorExImpl &_ex) {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_ERROR,"Could not read %s CDB element",elementName.c_str()));
        acsErrTypeLifeCycle::LifeCycleExImpl nex(_ex,__FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    } catch(...) {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,(LM_ERROR,"Unknown exception caught"));
        acsErrTypeLifeCycle::LifeCycleExImpl nex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex;
    }
}

void MountImpl::disconnectPositionSupplier() {
    if (mountStatusDataSupplier_p != NULL) {
        mountStatusDataSupplier_p->disconnect();
        mountStatusDataSupplier_p = NULL;
    }
}

void MountImpl::cleanUp() {
    MountBase::cleanUp();
}


void MountImpl::hwConfigureAction() {
    /* Here we want to build some basic utility classes:
       Cloudsat Monitor
       Commanded Position Handler
    */
    MountBase::hwConfigureAction();
}

void MountImpl::getDeviceUniqueId(string& deviceID) {
    broadcastBasedDeviceUniqueID(deviceID);
}

void MountImpl::hwInitializeAction() {
    MountBase::hwInitializeAction();
    createThreads();  // Creates the writer thread, but not running
}

void MountImpl::hwOperationalAction(){
    MountBase::hwOperationalAction();

    /* Set the last written time to the (very) near future */
    lastWrittenTime_m =
        TETimeUtil::plusTE(TimeUtil::ace2epoch(ACE_OS::gettimeofday()),20).value;

    // Start the writer and reader threads to begin collecting monitor
    // data. This needs to be done before we initailze the trajectory queue and
    // focus model as these functions use the monitor data.
    readerThread_p->resume();
    writerThread_p->resume();

    // Cache the initial antenna and subreflector positions
    initializeTrajectoryQueueAndSubrefPosition();

    try {
        TMCDB::Access_var tmcdb =
            getContainerServices()->getDefaultComponent<TMCDB::Access>
            ("IDL:alma/TMCDB/Access:1.0");
         reloadPointingModel(tmcdb.in());
         reloadFocusModel(tmcdb.in());
         CORBA::String_var tmcdbComponentName = tmcdb->name();
         getContainerServices()->releaseComponent(tmcdbComponentName.in());
    } catch (maciErrType::maciErrTypeExImpl& _ex) {
        ControlDeviceExceptions::HwLifecycleExImpl ex(_ex,__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Reason", "Failed to get TMCDB reference");
        ex.log();
        throw ex.getHwLifecycleEx();
    }
}

void MountImpl::reloadPointingModel(TMCDB::Access* tmcdb) {
    string antName;
    try {
        antName = componentToAntennaName(String_var(name()).in());
        TMCDB::BandPointingModel_var model =
            tmcdb->getCurrentAntennaPointingModel(antName.c_str());
        pointingModel_m.setPointingModel(model.in());
        alarms_m.deactivateAlarm(POINTING_MODEL_UNAVAILABLE);
        const int currentBand = pointingModel_m.getCurrentBand();
        pointingModel_m.useBand(abs(currentBand));
        pointingModel_m.enable();
        auxPointingModel_m.zeroCoefficients();
        auxPointingModel_m.disable();
        reportPointingModel();
        alarms_m.deactivateAlarm(POINTING_MODEL_ILLEGAL_TERMS);
    } catch (TmcdbErrType::TmcdbErrorEx& ex) {
        string msg = "Cannot load the pointing model";
        msg += " as it cannot be retreived from the TMCDB.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
        TmcdbErrType::TmcdbErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        alarms_m.activateAlarm(POINTING_MODEL_UNAVAILABLE);
    } catch (TmcdbErrType::TmcdbNoSuchRowEx& ex) {
        string msg = "Cannot load the pointing model";
        msg += " as the antenna name of " + antName + " is not recognized.";
        msg += antName;
        LOG_TO_OPERATOR(LM_ERROR, msg);
        TmcdbErrType::TmcdbNoSuchRowExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        alarms_m.activateAlarm(POINTING_MODEL_UNAVAILABLE);
    } catch (IllegalParameterErrorEx& ex) {
        string msg = "Cannot load the pointing model";
        msg += " as it contains unknown coefficient names or";
        msg += " values that are too big.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
        IllegalParameterErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        alarms_m.activateAlarm(POINTING_MODEL_ILLEGAL_TERMS);
    }
}

void MountImpl::reloadFocusModel(TMCDB::Access* tmcdb) {
    string antName;
    try {
        antName = componentToAntennaName(String_var(name()).in());
        TMCDB::BandFocusModel_var model =
            tmcdb->getCurrentAntennaFocusModel(antName.c_str());
        // alarms_m.deactivateAlarm(FOCUS_MODEL_UNAVAILABLE);
        focusModel_m->setFocusModel(model);

        const int currentBand = focusModel_m->getCurrentBand();
        focusModel_m->useBand(abs(currentBand));
        focusModel_m->enable();
        reportFocusModel();
        // alarms_m.deactivateAlarm(FOCUS_MODEL_ILLEGAL_TERMS);
    } catch (TmcdbErrType::TmcdbErrorEx& ex) {
        string msg = "Cannot load the focus model";
        msg += " as it cannot be retreived from the TMCDB.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
        TmcdbErrType::TmcdbErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        // alarms_m.activateAlarm(FOCUS_MODEL_UNAVAILABLE);
    } catch (TmcdbErrType::TmcdbNoSuchRowEx& ex) {
        string msg = "Cannot load the focus model";
        msg += " as the antenna name of " + antName + " is not recognized.";
        msg += antName;
        LOG_TO_OPERATOR(LM_ERROR, msg);
        TmcdbErrType::TmcdbNoSuchRowExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        // alarms_m.activateAlarm(FOCUS_MODEL_UNAVAILABLE);
    } catch (IllegalParameterErrorEx& ex) {
        string msg = "Cannot load the focusmodel";
        msg += " as it contains unknown coefficient names or";
        msg += " values that are too big.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
        IllegalParameterErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        // alarms_m.activateAlarm(FOCUS_MODEL_ILLEGAL_TERMS);
    }
}

void MountImpl::initializeTrajectoryQueueAndSubrefPosition() {
    // 1. Initialize the trajectory queue with the current position of the
    // antenna.
    // 2. Initialize the last commanded subreflector position with the current
    // position of the subreflector.
    // Both these tasks must occur after the writer and reader threads are
    // running

    int numRetries = 20;
    bool gotPosition = false;
    bool gotSubrefPosition = false;
    bool gotSubrefRotation = false;
    do {
        usleep(static_cast<unsigned long>(engineeringPollingTimeInUs_m));
        {
            ACS::ThreadSyncGuard guard(&mountStatusMutex_m);
            if (!gotPosition &&
                mountStatus_m.azPositionsValid &&
                mountStatus_m.elPositionsValid) {
                const double az = mountStatus_m.azPosition;
                const double el = mountStatus_m.elPosition;
                trajectoryQueue_m.setCurrentPosition(az, el);
                gotPosition = true;
            }
            if (!gotSubrefPosition &&
                mountStatus_m.subrefPositionValid) {
                lastSubrefCmdX_m = mountStatus_m.subrefX;
                lastSubrefCmdY_m = mountStatus_m.subrefY;
                lastSubrefCmdZ_m = mountStatus_m.subrefZ;
                gotSubrefPosition = true;
            }
            if (!gotSubrefRotation &&
                mountStatus_m.subrefRotationValid) {
                lastSubrefCmdTip_m = mountStatus_m.subrefTip;
                lastSubrefCmdTilt_m = mountStatus_m.subrefTilt;
                gotSubrefRotation = true;
            }
            numRetries--;
        }
    } while (!(gotPosition && gotSubrefPosition && gotSubrefRotation) && numRetries > 0);

    // TBD: Throw an exception if numRetries == 0
}

void MountImpl::hwStopAction() {
    destroyThreads();
    MountBase::hwStopAction();
}

char* MountImpl::antennaModel() {
    return CORBA::string_dup(antennaModel_m.c_str());
}

/* ===================== MISSING ICD POINTS ===================== */
///////////////////////////////////
// MonitorPoint: ACU_ERROR
//
// Vendor defined ACU error conditions. This monitor point
// returns an error stack which includes an error code and
// an identification of the command causing the error. Conditions
// may be fault conditions or status information. Fault conditions
// require the use of the CLEAR_FAULT_CMD to clear, while status
// information will clear then the hardware condition is cleared.
///////////////////////////////////

/**
 * Get the current value of ACU_ERROR from the device.
 */
vector< unsigned char > MountImpl::getAcuError(ACS::Time& timestamp) {
    const unsigned int SIZE_OF_ACU_ERROR_DATA = 5;
    AmbRelativeAddr  rca(getMonitorRCAAcuError());
    AmbDataLength_t length;
    vector< unsigned char > raw(SIZE_OF_ACU_ERROR_DATA);
    sem_t synchLock;
    AmbErrorCode_t status;

    sem_init(&synchLock, 0,0);
    monitor(rca, length, reinterpret_cast< AmbDataMem_t* >(&(raw[0])),
        &synchLock, &timestamp, &status);
    sem_wait(&synchLock);
    sem_destroy(&synchLock);

    if (length == 0)
        {
        // Return a sequence of 0s if no error is reported
        for (unsigned int i = 0; i < raw.size(); i++)
            raw[i] = static_cast<unsigned char>(0U);
        }
    else
        {
        if (length != SIZE_OF_ACU_ERROR_DATA || status != AMBERR_NOERR)
            {
              ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
              ex.addData("ErrorCode", static_cast< int >(status));
              ex.log();
              throw ex;
            }
        }

    vector< unsigned char > ret(raw.size());
    ret = raw;
    return ret;
}

///////////////////////////////////
// MonitorPoint: GET_ACU_ERROR
///////////////////////////////////

LongSeq* MountImpl::GET_ACU_ERROR(ACS::Time& timestamp) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    vector< unsigned char > ret;
    try {
        ret = getAcuError(timestamp);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }

    const unsigned int SIZE = 2;
    Control::LongSeq_var returnValue(new LongSeq(SIZE));
    returnValue->length(SIZE);

    union holder
    {
        unsigned char UChar[4];
        long          Long;
    } data;

    returnValue[0] = static_cast< CORBA::Long >(ret[0]);

    for(unsigned int index = 1; index < ret.size(); index++) {
        data.UChar[index-1] = ret[index];
    }

    returnValue[1] = static_cast< CORBA::Long >(data.Long);
    return returnValue._retn();
}

Control::Mount::BrakesStatus MountImpl::GET_AZ_BRAKE(ACS::Time& timestamp) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::Mount::BrakesStatus ret;
    CORBA::LongLong brakeStatus(0);

    try {
        brakeStatus = getAzBrake(timestamp);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }

    switch(brakeStatus) {
    case 0:
        ret = Control::Mount::BRAKE_DISENGAGED;
        break;
    case 1:
        ret = Control::Mount::BRAKE_ENGAGED;
        break;
    default:
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Reason","Invalid data received");
        ex.addData("Value",brakeStatus);
        ex.log();
        throw ex.getCAMBErrorEx();
    }
    return ret;
}

Control::Mount::BrakesStatus MountImpl::GET_EL_BRAKE(ACS::Time& timestamp) {
    if (isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    Control::Mount::BrakesStatus ret;
    CORBA::LongLong brakeStatus(0);
    try {
        brakeStatus = getElBrake(timestamp);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }

    switch(brakeStatus) {
    case 0:
        ret = Control::Mount::BRAKE_DISENGAGED;
        break;
    case 1:
        ret = Control::Mount::BRAKE_ENGAGED;
        break;
    default:
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Reason","Invalid data received");
        ex.addData("Value",brakeStatus);
        ex.log();
        throw ex.getCAMBErrorEx();
    }
    return ret;
}

void MountImpl::SET_AZ_BRAKE(Control::Mount::BrakesStatus mode) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    unsigned char world=0x0;
    switch(mode) {
    case Control::Mount::BRAKE_DISENGAGED:
        world = 0x00;
        break;
    case Control::Mount::BRAKE_ENGAGED:
        world = 0x01;
        break;
    }
    try {
        setCntlAzBrake(world);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
}

void MountImpl::SET_EL_BRAKE(Control::Mount::BrakesStatus mode) {
    if (isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    unsigned char world=0x00;
    switch(mode) {
    case Control::Mount::BRAKE_DISENGAGED:
        world = 0x00;
        break;
    case Control::Mount::BRAKE_ENGAGED:
        world = 0x01;
        break;
    }
    try {
        setCntlElBrake(world);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex){
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex){
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
}

void MountImpl::SET_SHUTTER(Control::Mount::ShutterMode mode) {
    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        ex.log();
        throw ex.getINACTErrorEx();
    }

    CORBA::Long world=0x00;
    switch(mode) {
    case Control::Mount::SHUTTER_CLOSED:
        world = 0x00;
        break;
    case Control::Mount::SHUTTER_OPEN:
        world = 0x01;
        break;
    }
    try {
        setCntlShutter(world);
    } catch(const ControlExceptions::CAMBErrorExImpl& ex) {
        ControlExceptions::CAMBErrorExImpl nex(ex,__FILE__,__LINE__,__PRETTY_FUNCTION__);
        nex.log();
        throw nex.getCAMBErrorEx();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::INACTErrorExImpl nex(ex,__FILE__,__LINE__,__PRETTY_FUNCTION__);
        nex.log();
        throw nex.getINACTErrorEx();
    }
}

void MountImpl::openShutter() {
    if(!isInsideCLOUDSATRegion(getMountStatusData()->elPosition)) {
        SET_SHUTTER(Control::Mount::SHUTTER_OPEN);
    } else {
        MountError::CLOUDSATExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Reason","Inside CLOUDSAT avoidance region");
        ex.log();
        throw ex.getCLOUDSATEx();
    }
}

void MountImpl::closeShutter() {
    SET_SHUTTER(Control::Mount::SHUTTER_CLOSED);
}

CORBA::Boolean MountImpl::isShutterOpen() {
    return checkShutterStatus(Control::Mount::SHUTTER_OPEN);
}

CORBA::Boolean MountImpl::isShutterClosed() {
    return checkShutterStatus(Control::Mount::SHUTTER_CLOSED);
}

CORBA::Boolean MountImpl::checkShutterStatus(Control::Mount::ShutterMode status) {
    CORBA::Boolean returnValue=false;

    if(status == Control::Mount::SHUTTER_OPEN)
        returnValue = checkShutterOpen();

    if(status == Control::Mount::SHUTTER_CLOSED)
        returnValue =  checkShutterClosed();

    //TBD Throw an exception if status is <> SHUTTER_OPEN, SHUTTER_CLOSED
    return returnValue;
}

CORBA::Boolean MountImpl::checkShutterOpen() {
    ACS::Time timestamp;
    CORBA::Boolean returnValue;
    CORBA::Long status = GET_SHUTTER(timestamp);

    if ((status & Control::Mount::SHUTTER_STATUS_OPEN_POSITION) != 0) {
        returnValue = true;
    } else {
        returnValue = false;
    }
    return returnValue;
}

CORBA::Boolean MountImpl::checkShutterClosed() {
    ACS::Time timestamp;
    CORBA::Boolean returnValue;
    CORBA::Long status = GET_SHUTTER(timestamp);

    if ((status & Control::Mount::SHUTTER_STATUS_CLOSE_POSITION) != 0) {
        returnValue = true;
    } else {
        returnValue = false;
    }
    return returnValue;
}

/* ===================== Writer Loop Method ===================== */
void MountImpl::writeMessages() {
    // The Mutex stops this function running if we are flushing.
    ACE_Guard<ACE_Thread_Mutex> guard(flushingMutex_m);

    const ACS::Time timeNow = ::getTimeStamp();
    if (timeNow > lastWrittenTime_m) {
        ostringstream msg;
        msg << "Missed sending some monitor requests!"
            << " Time now " << TETimeUtil::toTimeString(timeNow)
            << " Last request was sent at "
            << TETimeUtil::toTimeString(lastWrittenTime_m);
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        lastWrittenTime_m = TETimeUtil::floorTE(timeNow);
    }

    // The last written time is the time at which we expect the command to take
    // effect in the Antenna.  So the timestamps of commands are less that the
    // lastWrittenTime and the timestamps of monitors are greater than the
    // lastWrittenTime
    while (timeNow + LOOK_AHEAD_TIME > lastWrittenTime_m) {
        lastWrittenTime_m += TETimeUtil::TE_PERIOD_ACS;

        MountStatusStructure* mountStruct =
            mountStatusBuffer_m.getNextAvailable();

        mountStruct->initialize(); // This sets all times to zero
        mountStruct->applicationTimestamp = lastWrittenTime_m;

        // Send the monitor requests to the real-time layers
        queueMonitors(mountStruct);

        if (mountAxisMode_m.enableMountControls &&
            !trajectoryQueue_m.isEmpty()) {
            // Get the next trajectory from queue
            Trajectory newTrajectory =
                trajectoryQueue_m.getTrajectory(lastWrittenTime_m);

            // The local (user) pointing model
            mountStruct->pointingControlsModified =
                applyAuxPointingModel(newTrajectory, *mountStruct);

            // The global pointing model. This includes the band offsets
            mountStruct->pointingControlsModified |=
                applyPointingModel(newTrajectory, *mountStruct);

            // 1. If necessary send a command to move the subreflector.
            // 2. Adjust the pointing based on the commanded subreflector
            //    position
            applyFocusModel(newTrajectory, *mountStruct);

            // Make sure we are not too close to the zenith. As the upper
            // elevation limit is now 88.9 degrees this is largely
            // unnecessary. However this check is kept in case to the upper
            // elevation limit is increased.
            mountStruct->pointingControlsModified |= avoidCLOUDSAT(newTrajectory);
            // 1. Clip the trajectory to ensure we do not exceed any limits
            // 2. Put the trajectory into the  mountStruct
            mountStruct->pointingControlsModified |=
                enforceSoftwareLimits(newTrajectory, *mountStruct);
            // send position commands to the real-time layers
            queueCommands(mountStruct->positionControlTimestamp,
                          mountStatusBuffer_m.getTEPositionControlVector(),
                          mountStruct);
            // send focus commands to the real-time layers
            queueCommands(mountStruct->focusControlTimestamp,
                          mountStatusBuffer_m.getTEFocusControlVector(),
                          mountStruct);
        }

        // Add this structure to the pending queue so it can be picked up by
        // the readMessages function after the AMB messages have been executed.
        mountStatusBuffer_m.addToPendingQueue(mountStruct);
    }
}

/* ===================== Reader Loop Method ===================== */
void MountImpl::readMessages() {
    MountStatusStructure* returnedStatus;

    while ((returnedStatus=mountStatusBuffer_m.getCompletedStructure()) != 0) {

        const bool commanded = (returnedStatus->positionControlTimestamp!=0);
        const bool focused = (returnedStatus->focusControlTimestamp != 0);

        // If this was just a flush, there is nothing to do and we can simply
        // return the position & focus Data to the availableQueue and
        // return. Loosing all data should not be a problem as only a Te or two
        // of data is lost and flushes only occur when switching sources.
        if (returnedStatus->GET_AZ_ENC.ambStatus  == AMBERR_FLUSHED ||
            returnedStatus->GET_EL_ENC.ambStatus  == AMBERR_FLUSHED ||
            returnedStatus->AZ_POSN_RSP.ambStatus == AMBERR_FLUSHED ||
            returnedStatus->EL_POSN_RSP.ambStatus == AMBERR_FLUSHED ||
            returnedStatus->GET_SUBREF_ABS_POSN.ambStatus == AMBERR_FLUSHED ||
            returnedStatus->GET_SUBREF_ROTATION.ambStatus == AMBERR_FLUSHED ||
            (commanded &&
             (returnedStatus->AZ_TRAJ_CMD.ambStatus == AMBERR_FLUSHED ||
              returnedStatus->EL_TRAJ_CMD.ambStatus == AMBERR_FLUSHED)) ||
            (focused &&
             (returnedStatus->SET_SUBREF_ABS_POSN.ambStatus==AMBERR_FLUSHED ||
              returnedStatus->SET_SUBREF_ROTATION.ambStatus==AMBERR_FLUSHED))){
            ostringstream msg;
            msg << "At " <<  TETimeUtil::toTimeString(::getTimeStamp())
                << " processed a flushed message with time "
                << TETimeUtil::toTimeString(returnedStatus->applicationTimestamp);
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            // Now put this message back in the availableQueue
            mountStatusBuffer_m.addToAvailableQueue(returnedStatus);
            continue;
        }
        {
            ACS::ThreadSyncGuard guard(&mountStatusMutex_m);
            mountStatus_m.timestamp = returnedStatus->applicationTimestamp;

            flagAMBErrors(returnedStatus);
            flagIncorrectExecutionTimes(returnedStatus);
            setCorrectValues(returnedStatus);
            setOnSource(returnedStatus->pointingControlsModified);
            publishMountStatusData();
        }
        // Now put this strucuture back in the availableQueue
        mountStatusBuffer_m.addToAvailableQueue(returnedStatus);

        // Check if we have somehow gotten into the region where the
        // shutter is to be closed due to cloudsat.  If so then close it
        checkCLOUDSAT();
    }
}

void MountImpl::flagAMBErrors(MountStatusStructure* returnedStatus) {

    if (returnedStatus->AZ_POSN_RSP.ambStatus == AMBERR_NOERR) {
        mountStatus_m.azPositionsValid = true;
    } else {
        mountStatus_m.azPositionsValid = false;
    }
    if (returnedStatus->EL_POSN_RSP.ambStatus == AMBERR_NOERR) {
        mountStatus_m.elPositionsValid = true;
    } else {
        mountStatus_m.elPositionsValid = false;
    }

    if (returnedStatus->GET_AZ_ENC.ambStatus == AMBERR_NOERR) {
        mountStatus_m.azEncoderValid = true;
    } else {
        mountStatus_m.azEncoderValid = false;
    }
    if (returnedStatus->GET_EL_ENC.ambStatus == AMBERR_NOERR) {
        mountStatus_m.elEncoderValid = true;
    } else {
        mountStatus_m.elEncoderValid = false;
    }

    if (returnedStatus->GET_SUBREF_ABS_POSN.ambStatus == AMBERR_NOERR) {
        mountStatus_m.subrefPositionValid = true;
    } else {
        mountStatus_m.subrefPositionValid = false;
    }

    if (returnedStatus->GET_SUBREF_ROTATION.ambStatus == AMBERR_NOERR) {
        mountStatus_m.subrefRotationValid = true;
    } else {
        mountStatus_m.subrefRotationValid = false;
    }
    const bool commanded = (returnedStatus->positionControlTimestamp != 0);

    if (commanded && returnedStatus->AZ_TRAJ_CMD.ambStatus == AMBERR_NOERR) {
        mountStatus_m.azCommandedValid = true;
    } else {
        mountStatus_m.azCommandedValid = false;
    }
    if (commanded && returnedStatus->EL_TRAJ_CMD.ambStatus == AMBERR_NOERR) {
        mountStatus_m.elCommandedValid = true;
    } else {
        mountStatus_m.elCommandedValid = false;
    }

    const bool focused = (returnedStatus->focusControlTimestamp != 0);

    if (focused &&
        returnedStatus->SET_SUBREF_ABS_POSN.ambStatus == AMBERR_NOERR) {
        mountStatus_m.subrefPositionCmdValid = true;
    } else {
        mountStatus_m.subrefPositionCmdValid = false;
    }

    if (focused &&
        returnedStatus->SET_SUBREF_ROTATION.ambStatus == AMBERR_NOERR) {
        mountStatus_m.subrefRotationCmdValid = true;
    } else {
        mountStatus_m.subrefRotationCmdValid = false;
    }

    if (commanded && !mountStatus_m.azCommandedValid ||
        commanded && !mountStatus_m.elCommandedValid ||
        focused && !mountStatus_m.subrefPositionCmdValid ||
        focused && !mountStatus_m.subrefRotationCmdValid ||
        !mountStatus_m.azPositionsValid ||
        !mountStatus_m.elPositionsValid ||
        !mountStatus_m.azEncoderValid ||
        !mountStatus_m.elEncoderValid ||
        !mountStatus_m.subrefPositionValid ||
        !mountStatus_m.subrefRotationValid) {
        // TODO. Fix up this log!
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                (LM_DEBUG,
                 "time: %llu AMBERROR (AZ_TRAJ_CMD) %d (EL_TRAJ_CMD) %d (AZ_POSN_RSP) %d (EL_POSN_RSP) %d (GET_AZ_ENC) %d (GET_EL_ENC) %d",
                 returnedStatus->applicationTimestamp,
                 returnedStatus->AZ_TRAJ_CMD.ambStatus,
                 returnedStatus->EL_TRAJ_CMD.ambStatus,
                 returnedStatus->AZ_POSN_RSP.ambStatus,
                 returnedStatus->EL_POSN_RSP.ambStatus,
                 returnedStatus->GET_AZ_ENC.ambStatus,
                 returnedStatus->GET_EL_ENC.ambStatus));
        alarms_m.activateAlarm(AMBERR_RECEIVED_IN_MOUNT_STATUS);
    } else {
        alarms_m.deactivateAlarm(AMBERR_RECEIVED_IN_MOUNT_STATUS);
    }
}

void MountImpl::flagIncorrectExecutionTimes(MountStatusStructure* status) {
    bool incorrectExecutionTimeFound = false;

    if (mountStatus_m.azCommandedValid &&
        !isExecutionTimeCorrect("AZ_TRAJ_CMD",
                                status->positionControlTimestamp,
                                status->AZ_TRAJ_CMD.executionTimestamp,
                                false) ) {
        mountStatus_m.azCommandedValid = false;
        incorrectExecutionTimeFound = true;
    }

    if (mountStatus_m.elCommandedValid &&
        !isExecutionTimeCorrect("EL_TRAJ_CMD",
                                status->positionControlTimestamp,
                                status->EL_TRAJ_CMD.executionTimestamp,
                                false) ) {
        mountStatus_m.elCommandedValid = false;
        incorrectExecutionTimeFound = true;
    }

    if (mountStatus_m.azPositionsValid &&
        !isExecutionTimeCorrect("AZ_POSN_RSP",
                                status->monitorTimestamp,
                                status->AZ_POSN_RSP.executionTimestamp,
                                true) ) {
        mountStatus_m.azPositionsValid = false;
        incorrectExecutionTimeFound = true;
    }

    if (mountStatus_m.elPositionsValid &&
        !isExecutionTimeCorrect("EL_POSN_RSP",
                                status->monitorTimestamp,
                                status->EL_POSN_RSP.executionTimestamp,
                                true) ) {
        mountStatus_m.elPositionsValid = false;
        incorrectExecutionTimeFound = true;
    }

    if (mountStatus_m.azEncoderValid &&
        !isExecutionTimeCorrect("GET_AZ_ENC",
                                status->monitorTimestamp,
                                status->GET_AZ_ENC.executionTimestamp,
                                true) ) {
        mountStatus_m.azEncoderValid = false;
        incorrectExecutionTimeFound = true;
    }

    if (mountStatus_m.elEncoderValid &&
        !isExecutionTimeCorrect("GET_EL_ENC",
                                status->monitorTimestamp,
                                status->GET_EL_ENC.executionTimestamp,
                                true) ) {
        mountStatus_m.elEncoderValid = false;
        incorrectExecutionTimeFound = true;
    }

    if (mountStatus_m.subrefPositionValid &&
        !isExecutionTimeCorrect("GET_SUBREF_ABS_POSN",
                                status->monitorTimestamp,
                                status->GET_SUBREF_ABS_POSN.executionTimestamp,
                                true) ) {
        mountStatus_m.subrefPositionValid = false;
        incorrectExecutionTimeFound = true;
    }
    if (mountStatus_m.subrefRotationValid &&
        !isExecutionTimeCorrect("GET_SUBREF_ROTATION",
                                status->monitorTimestamp,
                                status->GET_SUBREF_ROTATION.executionTimestamp,
                                true) ) {
        mountStatus_m.subrefRotationValid = false;
        incorrectExecutionTimeFound = true;
    }

    if (mountStatus_m.subrefPositionCmdValid &&
        !isExecutionTimeCorrect("SET_SUBREF_ABS_POSN",
                                status->focusControlTimestamp,
                                status->SET_SUBREF_ABS_POSN.executionTimestamp,
                                false) ) {
        mountStatus_m.subrefPositionCmdValid = false;
        incorrectExecutionTimeFound = true;
    }
    if (mountStatus_m.subrefRotationCmdValid &&
        !isExecutionTimeCorrect("SET_SUBREF_ROTATION",
                                status->focusControlTimestamp,
                                status->SET_SUBREF_ROTATION.executionTimestamp,
                                false) ) {
        mountStatus_m.subrefRotationCmdValid = false;
        incorrectExecutionTimeFound = true;
    }
    alarms_m.updateAlarm(INCORRECT_EXECUTION_TIME_FOUND,
                         incorrectExecutionTimeFound);
}

void MountImpl::setCorrectValues(MountStatusStructure* returnedStatus) {

    if (mountStatus_m.azPositionsValid) {
        mountStatus_m.azPrePosition =
            encoderTransforms_m->posToRad(&returnedStatus->AZ_POSN_RSP.value[4]);
        mountStatus_m.azPosition =
            encoderTransforms_m->posToRad(returnedStatus->AZ_POSN_RSP.value);
    }

    if (mountStatus_m.elPositionsValid) {
        mountStatus_m.elPrePosition =
            encoderTransforms_m->posToRad(&returnedStatus->EL_POSN_RSP.value[4]);
        mountStatus_m.elPosition =
            encoderTransforms_m->posToRad(returnedStatus->EL_POSN_RSP.value);
    }

    if (mountStatus_m.azEncoderValid) {
        mountStatus_m.azEncoder =
            encoderTransforms_m->azEncToRad(returnedStatus->GET_AZ_ENC.value);
    }

    if (mountStatus_m.elEncoderValid) {
        mountStatus_m.elEncoder =
            encoderTransforms_m->elEncToRad(returnedStatus->GET_EL_ENC.value);
    }

    if (mountStatus_m.azCommandedValid){
        mountStatus_m.azCommanded   =
            encoderTransforms_m->posToRad(returnedStatus->AZ_TRAJ_CMD.value);
    }

    if (mountStatus_m.elCommandedValid){
        mountStatus_m.elCommanded   =
            encoderTransforms_m->posToRad(returnedStatus->EL_TRAJ_CMD.value);
    }

    if (mountStatus_m.azCommandedValid) {
        mountStatus_m.azCommanded +=
            returnedStatus->focusPointingModelCorrectionAz;
    }
    if (mountStatus_m.elCommandedValid) {
        mountStatus_m.elCommanded +=
            returnedStatus->focusPointingModelCorrectionEl;
    }
    if (mountStatus_m.azPositionsValid) {
        mountStatus_m.azPrePosition +=
            returnedStatus->focusPointingModelCorrectionAz;
        mountStatus_m.azPosition +=
            returnedStatus->focusPointingModelCorrectionAz;
    }
    if (mountStatus_m.elPositionsValid) {
        mountStatus_m.elPrePosition +=
            returnedStatus->focusPointingModelCorrectionEl;
        mountStatus_m.elPosition +=
            returnedStatus->focusPointingModelCorrectionEl;
    }

    mountStatus_m.pointingModel = returnedStatus->pointingModel;
    if (mountStatus_m.pointingModel) {
        mountStatus_m.azPointingModelCorrection =
            returnedStatus->pointingModelCorrectionAz;
        mountStatus_m.elPointingModelCorrection =
            returnedStatus->pointingModelCorrectionEl;

        if (mountStatus_m.azCommandedValid) {
            mountStatus_m.azCommanded +=
                returnedStatus->pointingModelCorrectionAz;
        }
        if (mountStatus_m.elCommandedValid) {
            mountStatus_m.elCommanded +=
                returnedStatus->pointingModelCorrectionEl;
        }
        if (mountStatus_m.azPositionsValid) {
            mountStatus_m.azPrePosition +=
                returnedStatus->pointingModelCorrectionAz;
            mountStatus_m.azPosition +=
                returnedStatus->pointingModelCorrectionAz;
        }
        if (mountStatus_m.elPositionsValid) {
            mountStatus_m.elPrePosition +=
                returnedStatus->pointingModelCorrectionEl;
            mountStatus_m.elPosition    +=
                returnedStatus->pointingModelCorrectionEl;
        }
    } else {
        mountStatus_m.azPointingModelCorrection = 0.0;
        mountStatus_m.elPointingModelCorrection = 0.0;
    }

    mountStatus_m.auxPointingModel = returnedStatus->auxPointingModel;
    if (mountStatus_m.auxPointingModel) {
        mountStatus_m.azAuxPointingModelCorrection =
            returnedStatus->auxPointingModelCorrectionAz;
        mountStatus_m.elAuxPointingModelCorrection =
            returnedStatus->auxPointingModelCorrectionEl;

        if (mountStatus_m.azCommandedValid) {
            mountStatus_m.azCommanded +=
                returnedStatus->auxPointingModelCorrectionAz;
        }
        if (mountStatus_m.elCommandedValid) {
            mountStatus_m.elCommanded +=
                returnedStatus->auxPointingModelCorrectionEl;
        }
        if (mountStatus_m.azPositionsValid) {
            mountStatus_m.azPrePosition +=
                returnedStatus->auxPointingModelCorrectionAz;
            mountStatus_m.azPosition +=
                returnedStatus->auxPointingModelCorrectionAz;
        }
        if (mountStatus_m.elPositionsValid) {
            mountStatus_m.elPrePosition +=
                returnedStatus->auxPointingModelCorrectionEl;
            mountStatus_m.elPosition    +=
                returnedStatus->auxPointingModelCorrectionEl;
        }
    } else {
        mountStatus_m.azAuxPointingModelCorrection = 0.0;
        mountStatus_m.elAuxPointingModelCorrection = 0.0;
    }

    if (mountStatus_m.subrefPositionValid) {
            encoderTransforms_m->
                focusToPos(returnedStatus->GET_SUBREF_ABS_POSN.value,
                           mountStatus_m.subrefX,
                           mountStatus_m.subrefY,
                           mountStatus_m.subrefZ);
    }
    if (mountStatus_m.subrefRotationValid) {
            encoderTransforms_m->
                focusToRot(returnedStatus->GET_SUBREF_ROTATION.value,
                           mountStatus_m.subrefTip,
                           mountStatus_m.subrefTilt);
    }
    if (mountStatus_m.subrefPositionCmdValid) {
            encoderTransforms_m->
                focusToPos(returnedStatus->SET_SUBREF_ABS_POSN.value,
                           mountStatus_m.subrefCmdX,
                           mountStatus_m.subrefCmdY,
                           mountStatus_m.subrefCmdZ);
    }
    if (mountStatus_m.subrefRotationCmdValid) {
            encoderTransforms_m->
                focusToRot(returnedStatus->SET_SUBREF_ROTATION.value,
                           mountStatus_m.subrefCmdTip,
                           mountStatus_m.subrefCmdTilt);
    }
}

void MountImpl::checkCLOUDSAT() {
    MountStatusData_var mountStatus;
    {
        ACS::ThreadSyncGuard guard(&mountStatusMutex_m);
        mountStatus = new MountStatusData(mountStatus_m);
    }
    if (isCLOUDSATEnabled_m && mountStatus->elPositionsValid) {

        insideCLOUDSATRegionPrevious_m = insideCLOUDSATRegion_m;
        insideCLOUDSATRegion_m = isInsideCLOUDSATRegion(mountStatus->elPosition);

        if (insideCLOUDSATRegion_m !=insideCLOUDSATRegionPrevious_m) {
            ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                    (LM_DEBUG," Transition detected"));
            if (insideCLOUDSATRegion_m) {
                ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                        (LM_DEBUG," Moving inside CLOUDSAT region"));
                transitionIn_m = true;
            } else {
                ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                        (LM_DEBUG,"Moving outside CLOUDSAT region"));
                if(reopenShutter_m) {
                    ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                            (LM_DEBUG,"Re-opening shutter when elevation = %lf", mountStatus->elPosition));
                    try {
                        SET_SHUTTER(Control::Mount::SHUTTER_OPEN);
                        reopenShutter_m = false;
                    } catch(ControlExceptions::CAMBErrorEx &ex){
                        alarms_m.activateAlarm(CLOUDSAT_SHUTTER_NOT_REOPEN);
                    } catch(ControlExceptions::INACTErrorEx &ex){
                        // TBD: LOG?
                    }
                }
            }
        } else {
            transitionIn_m = false;
        }

        try {
            if(insideCLOUDSATRegion_m) {
                if (isShutterOpen()) {
                    ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                            (LM_DEBUG,"Closing shutter when elevation = %lf", mountStatus->elPosition));
                    if (transitionIn_m) {
                        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                                (LM_DEBUG,"Shutter will be re-opened later"));
                        reopenShutter_m = true;
                    }
                    closeShutter();
                    alarms_m.deactivateAlarm(CLOUDSAT_SHUTTER_NOT_CLOSED);
                }
            }
        } catch(ControlExceptions::CAMBErrorEx &ex){

            alarms_m.activateAlarm(CLOUDSAT_SHUTTER_NOT_CLOSED);
        } catch(ControlExceptions::INACTErrorEx &ex){
            // TBD: LOG?
        }

        try {
            if(!insideCLOUDSATRegion_m && reopenShutter_m) {
                ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                        (LM_DEBUG,"Re-opening shutter (retry)"));
                SET_SHUTTER(Control::Mount::SHUTTER_OPEN);
                reopenShutter_m = false;
                alarms_m.deactivateAlarm(CLOUDSAT_SHUTTER_NOT_REOPEN);
            }
        } catch(ControlExceptions::CAMBErrorEx &ex){
            alarms_m.activateAlarm(CLOUDSAT_SHUTTER_NOT_REOPEN);
        } catch(ControlExceptions::INACTErrorEx &ex){
            //TBD: LOG?
        }
    }
}

bool MountImpl::isInsideCLOUDSATRegion(double elevation) {
    if ((elevation > cloudsatElevationLowerLimit_m) &&
        (elevation < cloudsatElevationUpperLimit_m)) {
        return true;
    } else {
        return false;
    }
}

void MountImpl::setOnSource(bool pointingControlsModified) {
    // keep track of the last commands sent to the subreflector as they are
    // needed to calculate when we are on-source.
    if (mountStatus_m.subrefRotationCmdValid) {
        lastSubrefCmdTip_m = mountStatus_m.subrefCmdTip;
        lastSubrefCmdTilt_m = mountStatus_m.subrefCmdTilt;
        ostringstream msg;
        msg << "At " << TETimeUtil::toTimeString(mountStatus_m.timestamp)
            << " the commanded subreflector rotation changed to ("
            << mountStatus_m.subrefCmdTip*180/M_PI << ", "
            << mountStatus_m.subrefCmdTilt*180/M_PI << ")";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    if (mountStatus_m.subrefPositionCmdValid) {
        lastSubrefCmdX_m = mountStatus_m.subrefCmdX;
        lastSubrefCmdY_m = mountStatus_m.subrefCmdY;
        lastSubrefCmdZ_m = mountStatus_m.subrefCmdZ;
        ostringstream msg;
        msg << "At " << TETimeUtil::toTimeString(mountStatus_m.timestamp)
            << " the commanded subreflector position changed to ("
            << mountStatus_m.subrefCmdX*1E3 << ", "
            << mountStatus_m.subrefCmdY*1E3 << ", "
            << mountStatus_m.subrefCmdZ*1E3 << ")";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    const bool wasOnSource = onSourceCounter_m >= onSourceCriteria_m;
    // If there is any invalid data then we cannot determine if we are
    // "on-source" and its assumed we are "off-source"
    if (pointingControlsModified ||
        !mountStatus_m.azCommandedValid || !mountStatus_m.elCommandedValid ||
        !mountStatus_m.azPositionsValid || !mountStatus_m.elPositionsValid ||
        !mountStatus_m.azPositionsValid || !mountStatus_m.elPositionsValid ||
        !mountStatus_m.subrefPositionValid || !mountStatus_m.subrefRotationValid) {
        onSourceCounter_m = 0;
        mountStatus_m.onSource = false;
        flagIfOffSource(mountStatus_m.onSource, wasOnSource, mountStatus_m.timestamp);
        return;
    }
    {
        const double positionError =
            slaDsep(mountStatus_m.azCommanded, mountStatus_m.elCommanded,
                    mountStatus_m.azPosition, mountStatus_m.elPosition);

//         {
//             ostringstream msg;
//             msg << "At " << TETimeUtil::toTimeString(mountStatus_m.timestamp)
//                 << " the position error is " << positionError*180/M_PI*60*60
//                 << " arc-secs and the tolerance is "
//                 << tolerance_m*180/M_PI*60*60
//                 << " arc-secs. This antenna is "
//                 << (mountStatus_m.onSource ? "on-source": "off-source")
//                 << ". Measured ";
//             printAzEl(msg, mountStatus_m.azPosition, mountStatus_m.elPosition);
//             msg << " Commanded ";
//             printAzEl(msg,mountStatus_m.azCommanded,mountStatus_m.elCommanded);
//             LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
//         }
        if (positionError > tolerance_m) {
            onSourceCounter_m = 0;
            mountStatus_m.onSource = false;
            flagIfOffSource(mountStatus_m.onSource, wasOnSource, mountStatus_m.timestamp);
            return;
        }
    }
    {
        const double focusPositionTolXY = focusModel_m->getPositionToleranceXY();
        const double focusPositionTolZ = focusModel_m->getPositionToleranceZ();
        const double dx = (lastSubrefCmdX_m - mountStatus_m.subrefX)/focusPositionTolXY;
        const double dy = (lastSubrefCmdY_m - mountStatus_m.subrefY)/focusPositionTolXY;
        const double dz = (lastSubrefCmdZ_m - mountStatus_m.subrefZ)/focusPositionTolZ;
//         {
//             ostringstream msg;
//             msg << "At " << TETimeUtil::toTimeString(mountStatus_m.timestamp)
//                 << " the subreflector position error is "
//                 << std::sqrt(dx*dx + dy*dy + dz*dz)
//                 << " and the tolerance is " << focusPositionTolXY*1E3
//                 << " mm in the X and Y axes and " << focusPositionTolZ*1E3
//                 << " mm on the Z axis. This antenna is "
//                 << (mountStatus_m.onSource ? "on-source": "off-source")
//                 << ". Measured ("
//                 << mountStatus_m.subrefX*1E3 << ", "
//                 << mountStatus_m.subrefY*1E3 << ", "
//                 << mountStatus_m.subrefZ*1E3
//                 << ") Commanded ("
//                 << lastSubrefCmdX_m*1E3 << ", "
//                 << lastSubrefCmdY_m*1E3 << ", "
//                 << lastSubrefCmdZ_m*1E3 << ")";
//             LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
//         }
        if (dx*dx + dy*dy + dz*dz > 1) {
            onSourceCounter_m = 0;
            mountStatus_m.onSource = false;
            flagIfOffSource(mountStatus_m.onSource, wasOnSource, mountStatus_m.timestamp);
            return;
        }
    }

    {
        const double focusAngleTol = focusModel_m->getRotationTolerance();
        const double dTip =  lastSubrefCmdTip_m - mountStatus_m.subrefTip;
        const double dTilt = lastSubrefCmdTilt_m - mountStatus_m.subrefTilt;
//         {
//             ostringstream msg;
//             msg << "At " << TETimeUtil::toTimeString(mountStatus_m.timestamp)
//                 << " the maximum subreflector rotation error is " << std::max(abs(dTip), abs(dTilt))
//                 << " arc-secs and the tolerance is " << focusAngleTol*180/M_PI
//                 << " deg. This antenna is "
//                 << (mountStatus_m.onSource ? "on-source": "off-source")
//                 << ". Measured ("
//                 << mountStatus_m.subrefTip*180/M_PI << ", "
//                 << mountStatus_m.subrefTilt*180/M_PI
//                 << ") Commanded ("
//                 << lastSubrefCmdTip_m*180/M_PI << ", "
//                 << lastSubrefCmdTilt_m*180/M_PI << ")";
//             LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
//         }
        if (abs(dTip) > focusAngleTol ||
            abs(dTilt) > focusAngleTol) {
            onSourceCounter_m = 0;
            mountStatus_m.onSource = false;
            flagIfOffSource(mountStatus_m.onSource, wasOnSource, mountStatus_m.timestamp);
            return;
        }
    }
    onSourceCounter_m++;
    mountStatus_m.onSource = (onSourceCounter_m >= onSourceCriteria_m);
    flagIfOffSource(mountStatus_m.onSource, wasOnSource, mountStatus_m.timestamp);
}

void  MountImpl::flagIfOffSource(bool isOnSource, bool wasOnSource, ACS::Time when) {
    if (isOnSource == wasOnSource) return;

    maci::SmartPtr<Control::ControlDevice> parent = getParentReference();
    if (parent.isNil()) return;

    Control::HardwareController_var antenna = 
        Control::HardwareController::_narrow(&*parent);
    if (CORBA::is_nil(antenna)) return;

    String_var componentName = name();
    try {
        antenna->flagData(!isOnSource, when, componentName.in(), 
                          "Mount is off source");
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named " 
            << ex._name() << " while trying to "
            << ((isOnSource) ? "unflag": "flag") << " data."
            << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
    }
    {
        ostringstream msg;
        msg << "Antenna has gone "
            << (isOnSource ? "on" : "off") << " source at "
            << TETimeUtil::toTimeString(when);
        LOG_TO_OPERATOR(LM_DEBUG, msg.str());
    }
}

void MountImpl::publishMountStatusData() {

    if (mountStatusDataPublicationEnabled_m > 0) {

        // Do not bother publishing if its all invalid. This helps ensure we do
        // not publish events with the same timestamp (which would happen if
        // data if we published flushed data).
        bool anythingValid =
            mountStatus_m.azCommandedValid || mountStatus_m.elCommandedValid ||
            mountStatus_m.azPositionsValid || mountStatus_m.elPositionsValid ||
            mountStatus_m.azEncoderValid || mountStatus_m.elEncoderValid;

        try {
            if (anythingValid) {
                mountStatusDataSupplier_p->
                    publishData<MountStatusData>(mountStatus_m);
            }
        } catch (acsncErrType::PublishEventFailureExImpl& ex) {
            // TBD: encapsulate this into his own class
            // Use a repeat guard here so that the logs won't flood the system.
            // The guard will allow logging every 10s and there is no limit to
            // the number of logs. The RepeatGuard object must be static to
            // ensure it maintains its state through each invocation of this
            // function.
            static RepeatGuard repeatGuard(ACS::TimeInterval(10.0/100e-9), 0);
            if (repeatGuard.check() == true) {
                string msg;
                msg = "Cannot publish the mount position. Data collection cannot proceed.";
                LOG_TO_OPERATOR(LM_ERROR, msg);
                ex.log();
            }
            alarms_m.activateAlarm(NC_FAILURE);
        }
        alarms_m.deactivateAlarm(NC_FAILURE);
    }
}

bool MountImpl::isExecutionTimeCorrect(string  commandName,
                                       ACS::Time timestampRequest,
                                       ACS::Time executionTime,
                                       bool monitor) {
    if (executionTime < timestampRequest) {
        ostringstream msg;
        msg << "The " << commandName
            <<  (monitor ? " monitor point" : " control point")
            << " was executed before the requested time."
            << " Requested execution time " << TETimeUtil::toTimeString(timestampRequest)
            << " Actual execution time " << TETimeUtil::toTimeString(executionTime);
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
        return false;
    }
    const ACS::TimeInterval delay = executionTime - timestampRequest;

    if (delay > static_cast<ACS::TimeInterval>(TETimeUtil::TE_PERIOD_ACS)) {
        ostringstream msg;
        msg << "The " << commandName
            <<  (monitor ? " monitor point" : " control point")
            << " was executed in the wrong timing event."
            << " Requested execution time " << TETimeUtil::toTimeString(timestampRequest)
            << ". Actual execution time " << TETimeUtil::toTimeString(executionTime)
            << ". Delay is " << delay*100e-6 << " ms";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
        return false;
    }
    const unsigned int timeAfterTE = executionTime%TETimeUtil::TE_PERIOD_ACS;
    if (monitor) {
        if (timeAfterTE < TETimeUtil::TE_PERIOD_ACS/2 ||
            timeAfterTE > TETimeUtil::TE_PERIOD_ACS*44/48) {
            ostringstream msg;
            msg << "The " << commandName << " monitor point"
                << " was not executed between 24 and 44ms after the timing event."
                << " Requested execution time " << TETimeUtil::toTimeString(timestampRequest)
                << ". Actual execution time " << TETimeUtil::toTimeString(executionTime)
                << ". Time after the TE " << timeAfterTE*100E-6 << " ms";
            LOG_TO_DEVELOPER(LM_ERROR, msg.str());
            return false;
        }
    } else {
        if (timeAfterTE > TETimeUtil::TE_PERIOD_ACS/2) {
            ostringstream msg;
            msg << "The " << commandName << " control point"
                << " was not executed between 0 and 24ms after the timing event."
                << " Requested execution time " << TETimeUtil::toTimeString(timestampRequest)
                << ". Actual execution time " << TETimeUtil::toTimeString(executionTime)
                << ". Time after the TE " << timeAfterTE*100E-6 << " ms";
            LOG_TO_DEVELOPER(LM_ERROR, msg.str());
            return false;
        }
    }
    return true;
}

bool MountImpl::enforceSoftwareLimits(Trajectory& trajectory,
                                      MountStatusStructure& mountStatus) {

    // Use a repeat guard here so that the logs won't flood the system.  The
    // guard will allow logging every 10s and there is no limit to the number
    // of logs. The RepeatGuard object must be static to ensure it maintains
    // its state through each invocation of this function.
    static RepeatGuard repeatGuard(ACS::TimeInterval(10.0/100e-9), 0);

    bool modified = false;
    ostringstream reason;
    if (trajectory.az > azMax_m) {
        reason << " Commanded azimuth of " << trajectory.az*180/M_PI
               << " degrees is larger than limit of "
               <<  azMax_m*180/M_PI  << ".";
        trajectory.az = azMax_m;
        trajectory.azRate = 0;
        modified = true;
    }

    if (trajectory.az < azMin_m) {
        reason << " Commanded azimuth of " << trajectory.az*180/M_PI
               << " degrees is smaller than limit of "
               << azMin_m*180/M_PI << ".";
        trajectory.az = azMin_m;
        trajectory.azRate = 0;
        modified = true;
    }

    if (trajectory.el > elMax_m) {
        reason << " Commanded elevation of " << trajectory.el*180/M_PI
               << " degrees is larger than limit of "
               << elMax_m*180/M_PI  << ".";
        trajectory.el = elMax_m;
        trajectory.elRate = 0;
        modified = true;
    }

    if (trajectory.el < elMin_m) {
        reason << " Commanded elevation of " << trajectory.el*180/M_PI
               << " degrees is smaller than limit of "
               << elMin_m*180/M_PI  << ".";
        trajectory.el = elMin_m;
        trajectory.elRate = 0;
        modified = true;
    }

    if (trajectory.azRate > MountHwAzRateMax) {
        reason << " Commanded azimuth rate of " << trajectory.azRate*180/M_PI
               << " deg/sec is larger than limit of "
               <<  MountHwAzRateMax*180/M_PI << ".";
        trajectory.azRate = MountHwAzRateMax;
        modified = true;
    }

    if (trajectory.azRate < MountHwAzRateMin) {
        reason << " Commanded azimuth rate of " << trajectory.azRate*180/M_PI
               << " deg/sec is less than limit of "
               <<  MountHwAzRateMin*180/M_PI << ".";
        trajectory.azRate = MountHwAzRateMin;
        modified = true;
    }

    if (trajectory.elRate > MountHwElRateMax) {
        reason << " Commanded elevation rate of "<< trajectory.elRate*180/M_PI
               << " deg/sec is larger than limit of "
               <<  MountHwElRateMax*180/M_PI << ".";
        trajectory.elRate = MountHwElRateMax;
        modified = true;
    }

    if (trajectory.elRate < MountHwElRateMin) {
        reason << " Commanded elevation rate of "<< trajectory.elRate*180/M_PI
               << " deg/sec is less than limit of "
               <<  MountHwElRateMin*180/M_PI << ".";
        trajectory.elRate = MountHwElRateMin;
        modified = true;
    }

    if ((modified == true) && (repeatGuard.check() == true)) {
        string msg("Hit a software limit on the mount.");
        msg += reason.str();
        msg += " Clipping to the limit value.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
    }

    // enforce software limits is the last function called before the
    // trajectory is converted to a value that can be sent to the ACU.
    mountStatus.AZ_TRAJ_CMD.valueLength = 8;
    encoderTransforms_m->toPos(trajectory.az, trajectory.azRate,
                               mountStatus.AZ_TRAJ_CMD.value);
    mountStatus.EL_TRAJ_CMD.valueLength = 8;
    encoderTransforms_m->toPos(trajectory.el, trajectory.elRate,
                               mountStatus.EL_TRAJ_CMD.value);
    // setting the time to a non-zero value sends the position commands
    mountStatus.positionControlTimestamp =
        lastWrittenTime_m - 2*TETimeUtil::TE_PERIOD_ACS;
    return modified;
}

bool MountImpl::avoidCLOUDSAT(Trajectory& trajectory) {
    bool modified = false;

    if (avoidCLOUDSATRegion_m) {
        if (isInsideCLOUDSATRegion(trajectory.el)) {
            double candidate1 = fabs(trajectory.el - cloudsatElevationLowerLimit_m);
            double candidate2 = fabs(trajectory.el - cloudsatElevationUpperLimit_m);

            if (candidate1 <= candidate2) {
                trajectory.el = cloudsatElevationLowerLimit_m - DEFAULT_CLOUDSAT_AVOID_TOLERANCE;
            } else {
                trajectory.el = cloudsatElevationUpperLimit_m + DEFAULT_CLOUDSAT_AVOID_TOLERANCE;
            }

            trajectory.elRate = 0.0;
            modified = true;
            if (modified) {
                ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                        (LM_INFO,"Trajectory modified (az) %lf (el) %lf (azRate) %lf (elRate) %lf",
                         trajectory.az,
                         trajectory.el,
                         trajectory.azRate,
                         trajectory.elRate));
            }
        }
    }
    return modified;
}

double MountImpl::subrefXLimit() {
    // The antenna specification requires all antennas to have at least +/- 5
    // mm of subreflector movement in the X and Y directions.  Use this limit
    // unless we have better numbers.
  return 0.005;
}

double MountImpl::subrefYLimit() {
    return subrefXLimit();
}

double MountImpl::subrefZLimit() {
    // The antenna specification requires all antennas to have at least +/- 10
    // mm of subreflector movement in the Z direction.  Use this limit unless
    // we have better numbers.
    return 0.010;
}

double MountImpl::subrefTipLimit() {
    return 3.2767*M_PI/180.0;
}

double MountImpl::subrefTiltLimit() {
    return subrefTipLimit();
}

double MountImpl::beamDeviationFactor() {
    // For a 12m antenna
    return 34.1;// arc-sec/mm
}

double MountImpl::focalRatio() {
    // For a 12m antenna
    return 6.177/96.0;
}

// ================= Real-time layer interaction methods =================
void MountImpl::queueMonitors(MountStatusStructure* mountStatus)
{
    // The monitor points need to be read half a TE after they are measured.
    mountStatus->monitorTimestamp =
        lastWrittenTime_m + TETimeUtil::TE_PERIOD_ACS / 2;
    vector<AMBMsgDef>::const_iterator iter;
    vector<AMBMsgDef> monitorList = mountStatusBuffer_m.getTEMonitorVector();
    try {
        for (iter = monitorList.begin(); iter != monitorList.end(); iter++) {
            AMBMsgData& amd = iter->locatorMethod(mountStatus);
            amd.ambStatus = AMBERR_PENDING;
            monitorTE(mountStatus->monitorTimestamp, iter->messageRCA,
                      amd.valueLength, amd.value, NULL,
                      &amd.executionTimestamp, &amd.ambStatus);
        }
    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        // TODO. Produce a error level log protected by a repeat guard.
        alarms_m.activateAlarm(ABM_HW_COMM_ERROR);
    }
    alarms_m.deactivateAlarm(ABM_HW_COMM_ERROR);
}

void MountImpl::queueCommands(const ACS::Time& ts,
                              const vector<AMBMsgDef>& msgList,
                              MountStatusStructure* mountStatus) {
    if (ts == 0) return;

    try {
        vector<AMBMsgDef>::const_iterator iter;
        for (iter = msgList.begin(); iter != msgList.end(); ++iter) {
            AMBMsgData& amd = iter->locatorMethod(mountStatus);
            amd.ambStatus = AMBERR_PENDING;
            commandTE(ts, iter->messageRCA,
                      amd.valueLength, amd.value, NULL,
                      &amd.executionTimestamp, &amd.ambStatus);
        }
    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        alarms_m.activateAlarm(ABM_HW_COMM_ERROR);
    }
    alarms_m.deactivateAlarm(ABM_HW_COMM_ERROR);
}

/* ============ Thread creator / destructor Methods ============== */
void MountImpl::createThreads() {
    // Shut down any threads already running. Normally this should do nothing.
    destroyThreads();

    const string compName = String_var(name()).in();
    string threadName = compName + "WriterThread";
    writerThread_p = getContainerServices()->getThreadManager()->
        create<WriterThread, MountImpl* const>
        (threadName.c_str(), this);
    writerThread_p->setSleepTime(1000000);//LOOK_AHEAD_TIME/2);

    threadName = compName + "ReaderThread";
    readerThread_p = getContainerServices()->getThreadManager()->
        create<ReaderThread, MountImpl* const>
        (threadName.c_str(), this);
    readerThread_p->setSleepTime(1000000);
}

void MountImpl::destroyThreads() {
    if (writerThread_p != NULL) {
        if (writerThread_p->isSuspended()) {
            writerThread_p->resume();
        }
        writerThread_p->terminate();
        delete writerThread_p;
        writerThread_p = NULL;
    }

    if (readerThread_p != NULL) {
        if (readerThread_p->isSuspended()) {
            readerThread_p->resume();
        }
        readerThread_p->terminate();
        delete readerThread_p;
        readerThread_p = NULL;
    }
}

/* =============  Writer Thread Implementation  ================= */
MountImpl::WriterThread::WriterThread(const ACE_CString&     name,
                                            const MountImpl* mci)
    :ACS::Thread(name),
     mountImpl_p(const_cast<MountImpl*>(mci))
{
}

void MountImpl::WriterThread::runLoop() {
    try {
        mountImpl_p->writeMessages();
    } catch (ACSErr::ACSbaseExImpl& ex) {
        ex.log();
        ostringstream msg;
        msg << "Caught an unexpected ACS exception in the Mount::writeMessages function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named " << ex._name();
        msg << " in the Mount::writeMessages function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    } catch (...) {
        ostringstream msg;
        msg << "Caught an unexpected exception in the Mount::writeMessages function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    }
}

void MountImpl::WriterThread::onStop() {
    mountImpl_p->flushTrajectory(0, false);
}

/* =============  Reader Thread Implementation  ================= */
MountImpl::ReaderThread::ReaderThread(const ACE_CString&     name,
                                            const MountImpl* mci)
    :ACS::Thread(name),
     mountImpl_p(const_cast<MountImpl*>(mci))
{
}

void MountImpl::ReaderThread::runLoop() {
    try {
        mountImpl_p->readMessages();
    } catch (ACSErr::ACSbaseExImpl& ex) {
        ex.log();
        ostringstream msg;
        msg << "Caught an unexpected ACS exception in the Mount::readMessages function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named " << ex._name();
        msg << " in the Mount::readMessages function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    } catch (...) {
        ostringstream msg;
        msg << "Caught an unexpected exception in the Mount::readMessages function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    }
}

void MountImpl::ReaderThread::onStop() {
   mountImpl_p->readMessages();
}

/* ------------- Local External Methods -----------------------*/

void MountImpl::flushTrajectory(ACS::Time  applicationTime, bool suspendWriter) {
    applicationTime = TETimeUtil::floorTE(applicationTime);

    {
        ostringstream msg;
        msg << "Flushing all queued commands and monitor requests with"
            << " timestamps from "
            << TETimeUtil::toTimeString(applicationTime);
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    // Ensure the writer thread is not running to avoid a race condition were
    // new commands the sent to the AMB server while we are flushing out old
    // ones. The thread is resumed at the end of this function or when we next
    // call setTrajectory.
    flushingMutex_m.acquire();

    try {

        trajectoryQueue_m.flush(applicationTime);

        if (applicationTime <= lastWrittenTime_m) {
            {
                string msg = "Flushing queued commands and monitor requests"
                    " from the real-time layers.";
                LOG_TO_DEVELOPER(LM_DEBUG, msg);
            }
            vector<AMBMsgDef>::iterator iter;
            ACS::Time timestamp;
            AmbErrorCode_t status;
            { // Flush the position control requests. As the trajectory
              // commands are applicable two TE's after they are sent we flush
              // them two TE's earlier.
                vector<AMBMsgDef> positionControlList =
                    mountStatusBuffer_m.getTEPositionControlVector();
                ostringstream msg;
                msg << "Flushing commands with RCA";
                for (iter = positionControlList.begin();
                     iter != positionControlList.end();
                     iter++) {
                    msg << " 0x" << std::hex << iter->messageRCA;
                    ACS::Time flushTime = applicationTime;
                    if (iter->messageRCA == getControlRCAAzTrajCmd() ||
                        iter->messageRCA == getControlRCAElTrajCmd()) {
                        flushTime -= 2*TETimeUtil::TE_PERIOD_ACS;
                    }
                    flushRCA(flushTime, iter->messageRCA, &timestamp, &status);
                }
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            }
            { // Flush the focus control requests.
                vector<AMBMsgDef> focusControlList =
                    mountStatusBuffer_m.getTEFocusControlVector();
                ostringstream msg;
                msg << "Flushing commands with RCA";
                for (iter = focusControlList.begin();
                     iter != focusControlList.end();
                     iter++) {
                    msg << " 0x" << std::hex << iter->messageRCA;
                    ACS::Time flushTime = applicationTime;
                    flushRCA(flushTime, iter->messageRCA, &timestamp, &status);
                }
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            }
            { // No need to flush the monitors as they are harmless. But
              // this may prevent duplicate monitor requests.
                vector<AMBMsgDef> monitorList =
                    mountStatusBuffer_m.getTEMonitorVector();
                ostringstream msg;
                msg << "Flushing monitor requests with RCA";
                for (iter = monitorList.begin();
                     iter != monitorList.end();
                     iter++) {
                    msg << " 0x" << std::hex << iter->messageRCA;
                    flushRCA(applicationTime, iter->messageRCA,
                             &timestamp, &status);
                }
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            }

            ACS::Time nextTime = ::getTimeStamp();
            if (applicationTime > nextTime) nextTime = applicationTime;
            lastWrittenTime_m = TETimeUtil::floorTE(nextTime);
            {
                ostringstream msg;
                msg << "Queuing of commands and monitor requests will resume"
                    << " after " << TETimeUtil::toTimeString(lastWrittenTime_m)
                    << " (" << lastWrittenTime_m << ")";
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            }
        } else {
            string msg =
                "Nothing needs to be flushed from the real-time queues.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg);
        }
    } catch (...) {
        flushingMutex_m.release();
        throw;
    }

    // Force the focus model to send a new subrefelctor positioning command to
    // the ACU (as the previous one may have been flushed).
    focusModel_m->forceUpdate();

    if (!suspendWriter) flushingMutex_m.release();
}

MountStatusData* MountImpl::getMountStatusData() {
    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                                               __PRETTY_FUNCTION__);
        string msg("Cannot get the current position");
        msg += " as the mount component is not operational.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getINACTErrorEx();
    }

    ACS::ThreadSyncGuard guard(&mountStatusMutex_m);
    MountStatusData_var returnValue =
        new MountStatusData(mountStatus_m);
    return returnValue._retn();
}

void MountImpl::enableMountStatusDataPublication(CORBA::Boolean enable) {
    if (enable) {
        mountStatusDataPublicationEnabled_m++;
    } else {
        if (mountStatusDataPublicationEnabled_m > 0) {
            mountStatusDataPublicationEnabled_m--;
        }
    }
}

CORBA::Boolean MountImpl::isMountStatusDataPublicationEnabled() {
    return (mountStatusDataPublicationEnabled_m > 0);
}

CORBA::Boolean MountImpl::onSource() {
    return getMountStatusData()->onSource;
}

/* ------- Methods Delegated to TrajectoryQueue Class --------- */
void MountImpl::setTrajectory(const TrajectorySeq& trajectory) {
    trajectoryQueue_m.insert(trajectory);
    flushingMutex_m.release();
}

void MountImpl::setMaintenanceAzEl(CORBA::Double azimuth, CORBA::Double elevation) {
    if(!isMoveable()) {
        Control::Mount::AxisMode azAxisMode, elAxisMode;
        getAxisMode(azAxisMode,elAxisMode);
        MountError::IllegalAxisModeExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Reason","Antenna cannot be moved in the current axis state");
        ex.addData("AzAxisMode",azAxisMode);
        ex.addData("ElAxisMode",elAxisMode);
        ex.log();
        throw ex.getIllegalAxisModeEx();
    }

    // TBD as:
    // checkAzElLimits(azimuth, elevation);
    if ((azimuth > azMax_m)  ||
        (azimuth < azMin_m)  ||
        (elevation < elMin_m) ||
        (elevation > elMax_m)) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Reason","Requested position out of limits");
        ex.addData("Azimuth", azimuth);
        ex.addData("Elevation", elevation);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    flushTrajectory(0, false);

    //TBD as:
    //setMaintenanceAzElTrajectory(azimuth,elevation);
    TrajectorySeq_var commands = new TrajectorySeq(1);
    commands->length(1);
    commands[0].time = lastWrittenTime_m + 5 * TETimeUtil::TE_PERIOD_ACS;;
    commands[0].az = azimuth;
    commands[0].el = elevation;
    commands[0].azRate = 0.0;
    commands[0].elRate = 0.0;

    ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
            (LM_DEBUG,"time: %llu Requested", commands[0].time));
    setTrajectory(commands);

    //TBD as:
    //waitMaintenanceAzEl(azimuth,elevation);
    MountStatusData_var current;
    bool ready;
    double azError;
    double elError;
    double positionError;
    int numRetries = engineeringRetries_m;

    ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
            (LM_DEBUG,"Waiting to arrive requested position"));
    ready = false;
    do {
        ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                (LM_DEBUG,"Retries pending %ld",numRetries));
        usleep(engineeringPollingTimeInUs_m);
        current = getMountStatusData();
        azError =
            (azimuth - current->azPosition);
        elError =
            (elevation - current->elPosition);
        positionError =
            sqrt((azError*azError) + (elError*elError));
        if (positionError <= engineeringTolerance_m)
            ready = true;
        numRetries --;
    } while (ready == false && numRetries > 0);

    if(!ready) {
        MountError::TimeOutExImpl ex(__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.log();
        throw ex.getTimeOutEx();
    }
}

double MountImpl::timeToSource(ACS::Time startOfSource) {
    if (!isMoveable()) {
        IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Cannot compute how long it will take to go on-source";
        msg += " as the brakes are set.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    ACS::Time now = ::getTimeStamp();

    // The MountController flushes sources 5TE's in the future. So allow
    // 10TE's of "grace" in this time check.
    if (startOfSource > now + TETimeUtil::toTimeInterval(0.5)) {
        IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Cannot compute how long it will take to go on-source";
        msg += " as the start of the current source is too far in the future!";
        msg += " Time now is " + TETimeUtil::toTimeString(now);
        msg += " Specified start of source is " + TETimeUtil::toTimeString(startOfSource);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    // This may throw an ControlExceptions::INACTErrorEx exception
    MountStatusData_var cur(getMountStatusData());
    bool currentSource = false;
    if (cur->timestamp > startOfSource) currentSource = true;

    for (int i = 0; i < 20 && !currentSource; i++) {
        usleep(static_cast<unsigned long>(engineeringPollingTimeInUs_m));
        cur = getMountStatusData();
        if (cur->timestamp > startOfSource) currentSource = true;
    }

    if (!currentSource || !cur->azPositionsValid || !cur->elPositionsValid) {
        ControlExceptions::CAMBErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Cannot compute how long it will take to go on-source";
        msg += " as I cannot get the current antenna positions.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getCAMBErrorEx();
    }

    if (cur->onSource) return 0.0;

    if (!cur->azCommandedValid || !cur->elCommandedValid) {
        IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Cannot compute how long it will take to go on-source";
        msg += " as the antenna is not being sent commands.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    // The ACU does not return the actual speed. It could be estimated by
    // differencing two positions at different times.
    // The commanded velocity is not in the MountStatusBuffer. If its
    // important in the timeToSlew computation it could be added.
    return timeToSlew(cur->azPosition, cur->elPosition,
                      cur->azCommanded, cur->elCommanded);
}

double MountImpl::timeToSlew(double fromAz, double fromEl,
                             double toAz, double toEl) {
    // The timeToSource is computed by calculating how far the antenna has to
    // slew and knowing the slew speed. It then adds a small fixed offset to
    // the time to account for acceleration and settling. More accurate
    // modeling is required and its expected this will depend on details on the
    // antenna (which is why this function is in the Mount component instead of
    // the MountController)
    if (fromAz < azMin_m || fromAz > azMax_m ||
        fromEl < elMin_m || fromEl > elMax_m ||
        toAz < azMin_m || toAz > azMax_m ||
        toEl < elMin_m || toEl > elMax_m) {
        ostringstream msg;
        msg << "Cannot estimate the slew time. One of the supplied positions of ";
        printAzEl(msg, fromAz, fromEl);
        msg << " or ";
        printAzEl(msg, toAz, toEl);
        msg << " is outside the limits of ";
        printAzEl(msg, azMin_m, elMin_m);
        msg << " to ";
        printAzEl(msg, azMax_m, elMax_m);
        IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    const double azSlewSpeed = min(abs(MountHwAzRateMin), abs(MountHwAzRateMax));
    const double elSlewSpeed = min(abs(MountHwElRateMin), abs(MountHwElRateMax));

    const double azSlewTime = abs(fromAz - toAz)/azSlewSpeed;
    const double elSlewTime = abs(fromEl - toEl)/elSlewSpeed;
    return max(azSlewTime, elSlewTime) + 0.5;
}

void MountImpl::setTolerance(CORBA::Double tolerance) {

    if((tolerance <0) ||
       (tolerance > DPI)) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Tolerance",tolerance);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    tolerance_m=tolerance;
}

CORBA::Double MountImpl::getTolerance() {
    return tolerance_m;
}

CORBA::Boolean MountImpl::isInsideCLOUDSATRegion() {

    if (!isReady()) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ErrorCode",ControlExceptions::INACTError);
        ex.log();
        throw ex.getINACTErrorEx();
    }

    return insideCLOUDSATRegion_m;
}

void MountImpl::avoidCLOUDSATRegion(CORBA::Boolean enable) {
    avoidCLOUDSATRegion_m = enable;
}

CORBA::Boolean MountImpl::isCLOUDSATRegionAvoided() {
    return avoidCLOUDSATRegion_m;
}

/* ----------- Methods Delegated to MountAxisMode Class --------- */
void MountImpl::setAxisMode(Control::Mount::AxisMode azAxisMode,
                            Control::Mount::AxisMode elAxisMode) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try {
        mountAxisMode_m.setAxisMode(azAxisMode, elAxisMode);
    } catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (IllegalParameterErrorExImpl& ex){
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::setAzAxisMode(Control::Mount::AxisMode azAxisMode) {
    try {
        mountAxisMode_m.setAzAxisMode(azAxisMode);
    } catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (IllegalParameterErrorExImpl& ex){
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::setElAxisMode(Control::Mount::AxisMode elAxisMode) {
    try{
        mountAxisMode_m.setElAxisMode(elAxisMode);
    } catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (ControlExceptions::IllegalParameterErrorExImpl& ex){
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::getAxisMode(Control::Mount::AxisMode &azAxisMode,
                                  Control::Mount::AxisMode &elAxisMode) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    try {
        mountAxisMode_m.getAxisMode(azAxisMode, elAxisMode);
    } catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

CORBA::Boolean MountImpl::inLocalMode() {
    try {
        return mountAxisMode_m.inLocalMode();
    }
    catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::shutdown() {
    try {
        mountAxisMode_m.shutdown();
    }
    catch(MountError::TimeOutExImpl &ex){
        ex.log();
        throw ex.getTimeOutEx();
    }
    catch(MountError::LocalModeExImpl &ex){
        ex.log();
        throw ex.getLocalModeEx();
    }
    catch(ControlExceptions::CAMBErrorExImpl &ex){
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

CORBA::Boolean MountImpl::inShutdownMode() {
    try {
        return mountAxisMode_m.inShutdownMode();
    }
    catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
  }
}

void MountImpl::standby() {
    try {
        mountAxisMode_m.standby();
    } catch(MountError::TimeOutExImpl &ex){
        ex.log();
        throw ex.getTimeOutEx();
    } catch(MountError::LocalModeExImpl &ex){
        ex.log();
        throw ex.getLocalModeEx();
    } catch(ControlExceptions::CAMBErrorExImpl &ex){
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

CORBA::Boolean MountImpl::inStandbyMode() {
    try{
        return mountAxisMode_m.inStandbyMode();
    } catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::encoder() {
    try{
        mountAxisMode_m.encoder();
    } catch(MountError::TimeOutExImpl &ex){
        ex.log();
        throw ex.getTimeOutEx();
    } catch(MountError::LocalModeExImpl &ex){
        ex.log();
        throw ex.getLocalModeEx();
    } catch(ControlExceptions::CAMBErrorExImpl &ex){
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::autonomous() {
    try{
        mountAxisMode_m.autonomous();
    } catch(MountError::TimeOutExImpl& ex){
        ex.log();
        throw ex.getTimeOutEx();
    } catch(MountError::LocalModeExImpl& ex){
        ex.log();
        throw ex.getLocalModeEx();
    } catch(ControlExceptions::CAMBErrorExImpl& ex){
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::autonomousAsync() {
    try {
        mountAxisMode_m.autonomous();
    } catch (MountError::TimeOutExImpl& ex){
        ex.log();
    } catch (MountError::LocalModeExImpl& ex){
        ex.log();
    } catch (ControlExceptions::CAMBErrorExImpl& ex){
        ex.log();
    }
}

CORBA::Boolean MountImpl::isMoveable() {
    try{
        return mountAxisMode_m.isMoveable();
    } catch (ControlExceptions::INACTErrorExImpl& ex){
        ex.log();
        throw ex.getINACTErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        ex.log();
        throw ex.getCAMBErrorEx();
    }
}

void MountImpl::survivalStow() {
    flushTrajectory(0, false);
    try {
        mountAxisMode_m.survivalStow();
    } catch(MountError::TimeOutExImpl &ex){
        ex.log();
        throw ex.getTimeOutEx();
    } catch(MountError::LocalModeExImpl &ex){
        ex.log();
        throw ex.getLocalModeEx();
    } catch(ControlExceptions::CAMBErrorExImpl &ex){
        ex.log();
        throw ex.getCAMBErrorEx();
    }
    initializeTrajectoryQueueAndSubrefPosition();
}

void MountImpl::maintenanceStow() {
    flushTrajectory(0, false);
    try{
        mountAxisMode_m.maintenanceStow();
    } catch(MountError::TimeOutExImpl &ex){
        ex.log();
        throw ex.getTimeOutEx();
    } catch(MountError::LocalModeExImpl &ex){
        ex.log();
        throw ex.getLocalModeEx();
    } catch(ControlExceptions::CAMBErrorExImpl &ex){
        ex.log();
        throw ex.getCAMBErrorEx();
    }
    initializeTrajectoryQueueAndSubrefPosition();
}

void MountImpl::survivalStowPrototype() {
    // Current default values for prototypes
    // TBD: include them in the CDB if needed
    const double survivalStowAz_m = 66.0*M_PI/180.0;
    const double survivalStowEl_m = 15.0*M_PI/180.0;
    stowPrototype(survivalStowAz_m,survivalStowEl_m);
}

void MountImpl::maintenanceStowPrototype() {
    // Current default values for prototypes
    // TBD: include them in the CDB if needed
    const double maintenanceStowAz_m = 180.0*M_PI/180.0;
    const double maintenanceStowEl_m = 88.9*M_PI/180.0;
    stowPrototype(maintenanceStowAz_m,maintenanceStowEl_m);
}

void MountImpl::checkStowPinValue(unsigned char value) {
    if (static_cast<unsigned short>(value) != Control::Mount::STOW_PIN_INSERT &&
        static_cast<unsigned short>(value) != Control::Mount::STOW_PIN_RELEASE) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Reason","Incorrect value");
        ex.log();
        throw ex;
    }
}

void MountImpl::checkAzStowPinValue(unsigned char value) {
    try{
        checkStowPinValue(value);
    } catch(IllegalParameterErrorExImpl& _ex){
        _ex.addData("StowPin","Azimuth");
        _ex.addData("Value",static_cast<unsigned short>(value));
        _ex.log();
        throw _ex;
    }
}
void MountImpl::checkElStowPinValue(unsigned char value) {
    try{
        checkStowPinValue(value);
    } catch(IllegalParameterErrorExImpl &_ex){
        _ex.addData("StowPin","Elevation");
        _ex.addData("Value",static_cast<unsigned short>(value));
        _ex.log();
        throw _ex;
    }
}

void MountImpl::checkSetStowPinArguments(const vector< unsigned char >& world) {
    const unsigned short AZIMUTH_STOW_PIN = 0;
    const unsigned short ELEVATION_STOW_PIN = 1;

    checkAzStowPinValue(world[AZIMUTH_STOW_PIN]);
    checkElStowPinValue(world[ELEVATION_STOW_PIN]);
}

void MountImpl::SET_STOW_PIN(const LongSeq& world) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ErrorCode", static_cast< int >(ControlExceptions::INACTError));
        ex.log();
        throw ex.getINACTErrorEx();
    }

    vector< unsigned char > x(world.length());
    std::size_t i(0);
    for(vector< unsigned char >::iterator iter(x.begin());
        iter != x.end(); ++iter, ++i) {
        (*iter) = static_cast< unsigned char >(world[i]);
    }

    try {
        checkSetStowPinArguments(x);
    } catch (IllegalParameterErrorExImpl& ex){
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    try {
        setCntlStowPin(x);
    } catch(ControlExceptions::CAMBErrorExImpl &ex){
        ex.log();
        throw ex.getCAMBErrorEx();
    } catch(ControlExceptions::INACTErrorExImpl &ex){
        // TBD: This catch is no longer needed
        ex.log();
        throw ex.getINACTErrorEx();
    }
}
void MountImpl::stowPrototype(double azimuth, double elevation) {
    if(!isMoveable()){
        autonomous();
    }
    flushTrajectory(0, false);
    try {
        setMaintenanceAzEl(azimuth, elevation);
    } catch (IllegalParameterErrorEx& _ex){
    // TBD: Alarm maintenanceStow values are incorrectly set in the CDB
    }
    shutdown();
}

// ==== Functions related to the focus model ====

void MountImpl::reloadFocusModel() {
    try {
        TMCDB::Access_var tmcdb =
            getContainerServices()->getDefaultComponent<TMCDB::Access>
            ("IDL:alma/TMCDB/Access:1.0");
        reloadFocusModel(tmcdb.in());
        CORBA::String_var tmcdbComponentName = tmcdb->name();
        getContainerServices()->releaseComponent(tmcdbComponentName.in());
    } catch (maciErrType::maciErrTypeExImpl& _ex) {
        MountError::TMCDBUnavailableExImpl ex(_ex,__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Reason", "TMCDB component Unavailable");
        ex.log();
        throw ex.getTMCDBUnavailableEx();
    }
}

void MountImpl::setBand(short band) {
    if (band < 1 || band > 10) {
        ostringstream msg;
        msg << "The band specified is incorrect."
            << " It must be between 1 and 10 an you specified " << band
            << ". The focus and pointing models have not been changed.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (band != abs(pointingModel_m.getCurrentBand())) {
        pointingModel_m.useBand(band);
        pointingModel_m.enable();
        reportPointingModel();
    }
    if (band != abs(focusModel_m->getCurrentBand())) {
        focusModel_m->useBand(band);
        focusModel_m->enable();
        reportFocusModel();
    }
}

short MountImpl::getFocusModelBand() {
    return focusModel_m->getCurrentBand();
}

void MountImpl::setAmbientTemperature(double tempInC) {
    focusModel_m->setAmbientTemperature(tempInC);
}

TMCDB::FocusModel* MountImpl::getFocusModel() {
    return focusModel_m->getFocusModel();
}

double MountImpl::getFocusModelCoefficient(const char* name) {
    return focusModel_m->getCoefficient(name);
}

void MountImpl::setFocusModel(const TMCDB::FocusModel& model) {
    focusModel_m->setFocusModel(model);
    reportFocusModel();
}

void MountImpl::setFocusModelCoefficient(const char* name, double value) {
    focusModel_m->setCoefficient(name, value);
    reportFocusModel();
}

void MountImpl::enableFocusModel(bool enabled) {
    if (enabled && !focusModel_m->isEnabled()) {
        focusModel_m->enable();
        reportFocusModel();
    } else if (!enabled && focusModel_m->isEnabled()) {
        focusModel_m->disable();
        reportFocusModel();
    }
}

bool MountImpl::isFocusModelEnabled() {
    return focusModel_m->isEnabled();
}

void MountImpl::enableFocusPointingModel(bool enabled) {
    if (enabled) {
      focusModel_m->enablePointingCorrection();
    } else if (!enabled && focusModel_m->adjustPointing()) {
      focusModel_m->disablePointingCorrection();
    }
}

bool MountImpl::isFocusPointingModelEnabled() {
  return focusModel_m->adjustPointing();
}

void MountImpl::setSubreflectorPositionToleranceXY(double tolerance) {
    return focusModel_m->setPositionToleranceXY(tolerance);
}

void MountImpl::setSubreflectorPositionToleranceZ(double tolerance) {
    return focusModel_m->setPositionToleranceZ(tolerance);
}

void MountImpl::setSubreflectorRotationTolerance(double tolerance) {
    return focusModel_m->setRotationTolerance(tolerance);
}

double MountImpl::getSubreflectorPositionToleranceXY() {
    return focusModel_m->getPositionToleranceXY();
}

double MountImpl::getSubreflectorPositionToleranceZ() {
    return focusModel_m->getPositionToleranceZ();
}

double MountImpl::getSubreflectorRotationTolerance() {
    return focusModel_m->getRotationTolerance();
}

void MountImpl::setSubreflectorPositionOffset(double x, double y, double z) {
    focusModel_m->setPositionOffset(x, y, z);
}

void MountImpl::getSubreflectorPositionOffset(double& x, double& y, double& z) {
    focusModel_m->getPositionOffset(x, y, z);
}

void MountImpl::setSubreflectorRotationOffset(double tip, double tilt) {
    focusModel_m->setRotationOffset(tip, tilt);
}

void MountImpl::getSubreflectorRotationOffset(double& tip, double& tilt) {
    focusModel_m->getRotationOffset(tip, tilt);
}

void MountImpl::getSubreflectorPosition(double& x, double& y, double& z) {
    try {
        ACS::Time ts;
        x = getSubrefAbsPosnXAxis(ts);
        y = getSubrefAbsPosnYAxis(ts);
        z = getSubrefAbsPosnZAxis(ts);
    } catch (ControlExceptions::INACTErrorExImpl& ex) {
        ControlExceptions::HardwareErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getHardwareErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        HardwareErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getHardwareErrorEx();
    }
}

void MountImpl::setSubreflectorPosition(double x, double y, double z) {
    EncoderTransformations::roundFocusPos(x, y, z);
    if (abs(x) > subrefXLimit() ||
        abs(y) > subrefYLimit() ||
        abs(z) > subrefZLimit()) {
        ostringstream msg;
        msg << "Cannot move the subreflector as the requested position of"
            << " [" << x*1000 << ", " << y*1000 << ", " << z*1000 << "] mm"
            << " is greater than the limits of +/- ["
            <<  subrefXLimit()*1000 << ", " << subrefYLimit()*1000
            << ", " << subrefZLimit()*1000 << "] mm";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        IllegalParameterErrorExImpl
            ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    focusModel_m->setSubrefAbsPosn(x, y, z);

    // The command to move the subreflector will not be sent if we are not
    // sending commands to the antenna i.e, the applyFocus function is not
    // being called. When this happens this command is sent directly using the
    // following code. Its still necessary to call the
    // FocusModel::setSubrefAbsPosn function to that the focus model knows the
    // last commanded position of the subreflector.
    if (!mountAxisMode_m.enableMountControls || trajectoryQueue_m.isEmpty()) {
        vector<short> newPosn(3);
        newPosn[0] = EncoderTransformations::metersToFocus(x);
        newPosn[1] = EncoderTransformations::metersToFocus(y);
        newPosn[2] = EncoderTransformations::metersToFocus(z);
        setCntlSubrefAbsPosn(newPosn);
    }
}

void MountImpl::setSubreflectorRotation(double tip, double tilt) {
    if (abs(tip) > subrefTipLimit() ||
        abs(tilt) > subrefTiltLimit()) {
        ostringstream msg;
        msg << "Cannot rotate the subreflector as the requested tip & tilt of"
            << " [" << tip*180/M_PI << ", " << tilt*180.0/M_PI << "] deg. "
            << " is greater than the limits of +/- ["
            <<  subrefTipLimit()*180/M_PI << ", " << subrefTiltLimit()*180.0/M_PI
            << "] deg.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        IllegalParameterErrorExImpl
            ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    focusModel_m->setSubrefRotation(tip, tilt);

    // The command to rotate the subreflector will not be sent if we are not
    // sending commands to the antenna i.e, the applyFocus function is not
    // being called. When this happens this command is sent directly using the
    // following code. Its still necessary to call the
    // FocusModel::setSubrefRotation function so that the focus model knows the
    // last commanded rotation of the subreflector.
    if (!mountAxisMode_m.enableMountControls || trajectoryQueue_m.isEmpty()) {
        vector<short> newAngles(3);
        newAngles[0] = EncoderTransformations::radToFocus(tip);
        newAngles[1] = EncoderTransformations::radToFocus(tilt);
        newAngles[2] = 0;
        setCntlSubrefRotation(newAngles);
    }
}

void MountImpl::getSubreflectorCommandedPosition(double& x, double& y, double& z) {
    // The lastSubrefCmd* variables are the last commanded values as read by
    // the reader thread ie., after the commands have been sent and the
    // response, for the relevant TE, processed. The lastSubrefCmd* variables
    // are initialized to the measured value of the subreflector.
    x = lastSubrefCmdX_m;
    y = lastSubrefCmdY_m;
    z = lastSubrefCmdZ_m;
}

void MountImpl::getSubreflectorCommandedRotation(double& tip, double& tilt) {
    tip =  lastSubrefCmdTip_m;
    tilt =  lastSubrefCmdTilt_m;
}

void MountImpl::getSubreflectorLimits(double& x, double& y, double& z, 
                                      double& tip, double& tilt) {
    if(isReady() == false) {
        INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Cannot get the motion limits ";
        msg += " as the mount component for antenna ";
        msg += componentToAntennaName(String_var(name()).in());
        msg += " is not operational.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getINACTErrorEx();
    }
    x = subrefXLimit();
    y = subrefYLimit();
    z = subrefZLimit();
    tip = subrefTipLimit();
    tilt = subrefTiltLimit();
}

void MountImpl::getSubreflectorRotation(double& tip, double& tilt) {
    try {
        ACS::Time ts;
        //Control::LongSeq* rotation = GET_SUBREF_ROTATION(ts);
        std::vector<short> rotation = getSubrefRotation(ts);
        tip = static_cast<double>(rotation[0]) * 1.7453292519943294E-6;
        tilt = static_cast<double>(rotation[1]) * 1.7453292519943294E-6;
    } catch (ControlExceptions::INACTErrorExImpl& ex) {
        HardwareErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getHardwareErrorEx();
    } catch (ControlExceptions::CAMBErrorExImpl& ex) {
        HardwareErrorExImpl
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.log();
        throw newEx.getHardwareErrorEx();
    }
}

void MountImpl::reportFocusModel() {
    Control::AntennaStatusReporting_var parent =
        Control::AntennaStatusReporting::_narrow(&*getParentReference());
    if (!CORBA::is_nil(parent)) {
        try {
            parent->reportFocusModel();
        } catch (const ControlExceptions::INACTErrorEx& ex) {
            /*  This just means that the Antenna is not available yet
                This is ok, just proceed */
        }
    }
}

void MountImpl::reportFocusPosition() {
    Control::AntennaStatusReporting_ptr parent;
    parent = Control::AntennaStatusReporting::_narrow(&*getParentReference());
    if (!CORBA::is_nil(parent)) {
        try {
            parent->reportSubreflectorPosition();
        } catch (const ControlExceptions::INACTErrorEx& ex) {
            /*  This just means that the Antenna is not available yet
                This is ok, just proceed */
        }
    }
}

void MountImpl::applyFocusModel(Trajectory& trajectory,
                                MountStatusStructure& mountStatus) {
    double cmdAz = trajectory.az;
    double cmdEl = trajectory.el;
    double x, y, z, tip, tilt;

    if (focusModel_m->getFocus(trajectory.az, trajectory.el, x, y, z,
                               tip, tilt)) {
        // Not clear if this is the right lead time as I have no idea how long
        // it takes for the subreflector to complete its move, or even start
        // moving, in response to this command.
        mountStatus.focusControlTimestamp = lastWrittenTime_m;
        mountStatus.SET_SUBREF_ABS_POSN.valueLength = 6;
        encoderTransforms_m->
            toFocusPos(x, y, z, mountStatus.SET_SUBREF_ABS_POSN.value);
        mountStatus.SET_SUBREF_ROTATION.valueLength = 6;
        encoderTransforms_m->
            toFocusRotation(tip, tilt, mountStatus.SET_SUBREF_ROTATION.value);
    } else {
        mountStatus.focusControlTimestamp = 0; // means do not send these commands
    }

    mountStatus.focusPointingModel = focusModel_m->isEnabled();
    mountStatus.focusPointingModelCorrectionAz = cmdAz - trajectory.az;
    mountStatus.focusPointingModelCorrectionEl = cmdEl - trajectory.el;
}

// ==== Functions related to the pointing model ====
void MountImpl::reloadPointingModel() {
    try {
        TMCDB::Access_var tmcdb =
            getContainerServices()->getDefaultComponent<TMCDB::Access>
            ("IDL:alma/TMCDB/Access:1.0");
        reloadPointingModel(tmcdb.in());
        CORBA::String_var tmcdbComponentName = tmcdb->name();
        getContainerServices()->releaseComponent(tmcdbComponentName.in());
    } catch (maciErrType::maciErrTypeExImpl& _ex) {
        MountError::TMCDBUnavailableExImpl ex(_ex,__FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("Reason", "TMCDB component Unavailable");
        ex.log();
        throw ex.getTMCDBUnavailableEx();
    }
}

short MountImpl::getPointingModelBand() {
    return pointingModel_m.getCurrentBand();
}

TMCDB::PointingModel* MountImpl::getPointingModel() {
    return pointingModel_m.getPointingModel();
}

CORBA::Double MountImpl::getPointingModelCoefficient(const char* name) {
    return pointingModel_m.getCoefficient(string(name));
}

void MountImpl::setPointingModel(const TMCDB::PointingModel& model) {
    pointingModel_m.setPointingModel(model);
    pointingModel_m.enable();
    alarms_m.deactivateAlarm(POINTING_MODEL_ILLEGAL_TERMS);
    reportPointingModel();
}

void MountImpl::setPointingModelCoefficient(const char* name, double value) {
    pointingModel_m.setCoefficient(name, value);
    pointingModel_m.enable();
    reportPointingModel();
}

void MountImpl::zeroPointingModel() {
    pointingModel_m.zeroCoefficients();
    pointingModel_m.disable();
    reportPointingModel();
}

void MountImpl::enablePointingModel(bool enabled) {
    if (enabled && !pointingModel_m.isEnabled()) {
        pointingModel_m.enable();
        reportPointingModel();
    } else if (!enabled && pointingModel_m.isEnabled()) {
        pointingModel_m.disable();
        reportPointingModel();
    }
}

CORBA::Boolean MountImpl::isPointingModelEnabled() {
    return pointingModel_m.isEnabled();
}

void MountImpl::reportPointingModel(){
    Control::AntennaStatusReporting_var parent =
        Control::AntennaStatusReporting::_narrow(&*getParentReference());
    if (!CORBA::is_nil(parent)) {
        try {
            parent->reportPointingModel();
        } catch (const ControlExceptions::INACTErrorEx& ex) {
            /*  This just means that the Antenna is not available yet
                This is ok, just proceed */
            // TODO. Log a debug message
        }
    }
}

bool MountImpl::applyPointingModel(Trajectory& trajectory,
                                   MountStatusStructure& mountStatus) {
    double cmdAz = trajectory.az;
    double cmdEl = trajectory.el;

    mountStatus.pointingModel = pointingModel_m.isEnabled();
    bool badCorrection = pointingModel_m.applyModel(cmdAz, cmdEl,
                                                    trajectory.az, trajectory.el);
    mountStatus.pointingModelCorrectionAz = cmdAz-trajectory.az;
    mountStatus.pointingModelCorrectionEl = cmdEl-trajectory.el;
    return badCorrection;
}

// ==== Public functions related to the aux pointing model ====

void MountImpl::setAuxPointingModel(const TMCDB::PointingModel& model) {
    auxPointingModel_m.setPointingModel(model);
    auxPointingModel_m.enable();
    reportPointingModel();
}

void MountImpl::setAuxPointingModelCoefficient(const char* name, double value) {
    auxPointingModel_m.setCoefficient(name, value);
    auxPointingModel_m.enable();
    reportPointingModel();
}

void MountImpl::zeroAuxPointingModel() {
    auxPointingModel_m.zeroCoefficients();
    auxPointingModel_m.disable();
    reportPointingModel();
}

TMCDB::PointingModel* MountImpl::getAuxPointingModel() {
    return auxPointingModel_m.getPointingModel();
}

CORBA::Double MountImpl::getAuxPointingModelCoefficient(const char* name) {
    return auxPointingModel_m.getCoefficient(name);
}

void MountImpl::enableAuxPointingModel(bool enabled) {
    if (enabled && !auxPointingModel_m.isEnabled()) {
        auxPointingModel_m.enable();
        reportPointingModel();
    } else if (!enabled && auxPointingModel_m.isEnabled()) {
        auxPointingModel_m.disable();
        reportPointingModel();
    }
}

CORBA::Boolean MountImpl::isAuxPointingModelEnabled() {
    return auxPointingModel_m.isEnabled();
}

bool MountImpl::applyAuxPointingModel(Trajectory& trajectory,
                                      MountStatusStructure& mountStatus) {
    double cmdAz = trajectory.az;
    double cmdEl = trajectory.el;

    mountStatus.auxPointingModel = auxPointingModel_m.isEnabled();
    bool badCorrection = auxPointingModel_m.applyModel(cmdAz, cmdEl,
                                  trajectory.az, trajectory.el);
    mountStatus.auxPointingModelCorrectionAz = cmdAz-trajectory.az;
    mountStatus.auxPointingModelCorrectionEl = cmdEl-trajectory.el;
    return badCorrection;
}

void MountImpl::setMinimumElevation(double minEl) {
    if(isReady() == false) {
        INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Cannot set the minimum elevation.";
        msg += " as the mount component for antenna ";
        msg += componentToAntennaName(String_var(name()).in());
        msg += " is not operational.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getINACTErrorEx();
    }
    if (minEl < MountHwElMin || minEl > MountHwElMax) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ostringstream msg;
        msg << "Cannot set the minimum elevation to "
            << minEl*180/M_PI << " degrees as its "
            << ((minEl < MountHwElMin) ? "less": "greater")
            << " than the hardware limit of "
            << ((minEl < MountHwElMin) ? MountHwElMin: MountHwElMax)*180/M_PI
            << " degrees.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    elMin_m = minEl;
}

void MountImpl::resetLimits() {
    azMin_m = MountHwAzMin;
    azMax_m = MountHwAzMax;
    elMin_m = MountHwElMin;
    elMax_m = MountHwElMax;
}

void MountImpl::getMotionLimits(double& minAz, double& maxAz,
                                double& minEl, double& maxEl) {
    if(isReady() == false) {
        INACTErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Cannot get the motion limits ";
        msg += " as the mount component for antenna ";
        msg += componentToAntennaName(String_var(name()).in());
        msg += " is not operational.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getINACTErrorEx();
    }
    minAz = azMin_m;
    maxAz = azMax_m;
    minEl = elMin_m;
    maxEl = elMax_m;
}

vector<int>  MountImpl::getAzPosnRsp(Time & timestamp) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    std::vector< int > raw(2U);
    raw[0] = static_cast<int>(mountStatus_m.azPosition);
    raw[1] = static_cast<int>(mountStatus_m.azPrePosition);
    timestamp = mountStatus_m.timestamp;

    std::vector< int > ret(raw.size());
    ret = raw;

//    valueAzPosnRspLtp = static_cast< int >(raw[0]);
//    timeAzPosnRspLtp = timestamp;
//
//    valueAzPosnRsp24msBeforeLtp = static_cast< int >(raw[1]);
//    timeAzPosnRsp24msBeforeLtp = timestamp;

    return ret;
}

vector<int>  MountImpl::getElPosnRsp(Time & timestamp) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    std::vector< int > raw(2U);
    raw[0] = static_cast<int>(mountStatus_m.elPosition);
    raw[1] = static_cast<int>(mountStatus_m.elPrePosition);
    timestamp = mountStatus_m.timestamp;

    std::vector< int > ret(raw.size());
    ret = raw;

//    valueElPosnRspLtp = static_cast< int >(raw[0]);
//    timeElPosnRspLtp = timestamp;
//
//    valueElPosnRsp24msBeforeLtp = static_cast< int >(raw[1]);
//    timeElPosnRsp24msBeforeLtp = timestamp;

    return ret;
}

unsigned int  MountImpl::getAzEnc(Time & timestamp) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    unsigned int raw(0U);
    raw = static_cast<int>(mountStatus_m.azEncoder);
    timestamp = mountStatus_m.timestamp;

    unsigned int ret(raw);

    return ret;
}

unsigned int  MountImpl::getElEnc(Time & timestamp) {
    if(isReady() == false) {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg("Cannot execute monitor request.  Device is "
            "inactive.");
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    unsigned int raw(0U);
    raw = static_cast<int>(mountStatus_m.elEncoder);
    timestamp = mountStatus_m.timestamp;

    unsigned int ret(raw);

    return ret;
}

void MountImpl::RESET_ACU_CMD(Control::Mount::ResetACUCmdOptions option) {
    switch (option){
        case Control::Mount::COMPLETE_REBOOT:
            sendOneByteCommand(getControlRCAResetAcuCmd());
            break;
        case Control::Mount::METROLOGY_REBOOT:
            sendOneByteCommand(getControlRCAResetAcuCmd(),0x02);
            break;
        case Control::Mount::SUBREFLECTOR_REBOOT:
            sendOneByteCommand(getControlRCAResetAcuCmd(),0x04);
            break;
        default:
            /*
             * This should never be executed
             */
            ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__,
                    (LM_ERROR,"Unknown action. Ignored"));
    }
    return;
}

void MountImpl::
printAzEl(ostream& msg,  const double& az, const double& el,
          const double& azRate, const double& elRate) {
    printAzEl(msg, az, el);
    msg << " & rates (" << std::setprecision(4)
        << azRate*180/M_PI << ", " << elRate*180/M_PI << ") deg/sec.";
}

void MountImpl::printAzEl(ostream& msg,  const double& az, const double& el) {
    msg << "Az/El: (" << std::setprecision(12)
        << az*180/M_PI << ", " << el*180/M_PI << ") deg.";
}
