// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <math.h>
#include <stdio.h>
#include <iostream>
#include "Axis.h"

using namespace std;
using namespace ACUSim;

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define abs(val) ((val) < 0 ? -(val) : (val))

#define MAX_VEL     6.0
#define MAX_ACC     12.0

#define STATE_CHANGES 2
#define OTHER_DEBUG   4
#define DEBUG_DRIVE   8
#define DEBUG_TICK    0x10

#undef DEBUG

//------------------------------------------------------------------------------

Axis::Axis() 
{ 
    drive_state = STOP; 
    cur_pos = 10; //deg. Do not use zero as its outside the elevation limit
    cur_vel = cur_acc = 0;
    decel_offset = decel_vel = 0;
    track_pos = track_vel = delta_track = 0;
    lower_limit = upper_limit = 0;
    limit = 0;
    count = 0;
    newpos = false;
    newmode = mode = SHUTDOWN;
    brake = BRAKE_ON;
    pin = PIN_OUT;
    set_servo_defaults();
    auxmode=AUX_DISABLED;
}

//------------------------------------------------------------------------------

void Axis::tick()
{
    if ((debug_level & DEBUG_TICK) && ((count % 20) == 0))
	{
	printf("%+7.2f s=%+2d/%+2d %+7.1f/%+4.1f : %+7.1f/%+4.1f | %+7.3f\n",
	       count * TIME_UNIT,mode,drive_state,cur_pos,cur_vel,
	       track_pos, track_vel, track_pos-cur_pos);
	}

    /*
     * how to handle stow mode if brakes are set?
     */
    switch(mode)
	{
	case SHUTDOWN:
	case STANDBY:
	    break;

	case SURVIVAL_STOW:
	case MAINTENANCE_STOW:
	case ENCODER:
	case AUTONOMOUS:
	    if (newpos)
		{
		newpos = false;
		drive();
		}
	    inc_track();
	    inc_cur();
	    if (mode == MAINTENANCE_STOW)
		{
		if (inrange(mstow,0.010,cur_pos))
		    {
		    pin = PIN_IN;
		    set_mode(SHUTDOWN);
		    }
		}
	    if (mode == SURVIVAL_STOW)
		{
		if (inrange(sstow,0.010,cur_pos))
                    {
		    pin = PIN_IN;
		    set_mode(SHUTDOWN);
                    }
		}
	    break;

	default:
	    break;
	}
}
        
//------------------------------------------------------------------------------

void Axis::transition()
{
#ifdef DEBUG
    cout << "Axis::transition" << endl;
#endif

    mode = evaluate(newmode);    // can we make the transition?
#ifdef DEBUG
    cout << "Axis::transition: evaluate called" << endl;
    cout << "Axis::transition: new mode: " << (int) newmode <<endl;
    cout << "Axis::transition:     mode: " << (int) mode <<endl;
#endif
    newmode = mode;
#ifdef DEBUG
    cout << "Axis::transition: new mode: " << (int) newmode <<endl;
#endif
    switch(mode)
	{
	case SHUTDOWN:
#ifdef DEBUG
	    cout << "Axis::transition. SHUTDOWN received " <<endl;
#endif
	    brake = BRAKE_ON;
	    drive_state = STOP;
	    cur_vel = 0;
	    cur_acc = 0;
	    // turn off power, pull pin
	    break;
	    
	case STANDBY:
#ifdef DEBUG
	    cout << "Axis::transition: STANDBY received " <<endl;
#endif
	    // turn on power, pull pin
	    brake = BRAKE_ON;
	    drive_state = STOP;
	    cur_vel = 0;
	    cur_acc = 0;
	    pin = PIN_OUT;
	    break;
	    
	case SURVIVAL_STOW:
#ifdef DEBUG
	    cout << "Axis::transition: SURVIVAL_STOW received " <<endl;
#endif
	    brake = BRAKE_OFF;      // always release brakes
	    set(sstow,0.);
	    break;
	    
	case MAINTENANCE_STOW:
#ifdef DEBUG
	    cout << "Axis::transition: MAINTENANCE_STOW received " <<endl;
#endif
	    brake = BRAKE_OFF;      // always release brakes
	    set(mstow,0.);
	    break;
	    
	case ENCODER:
	case AUTONOMOUS:
#ifdef DEBUG
	    cout << "Axis::transition: ENCODER or AUTONOMOUS received " <<endl;
#endif
	    brake = BRAKE_OFF;
	    break;
	    
	default:
#ifdef DEBUG
	    cout << "Axis::transition: unknown mode received " <<endl;
#endif
	    break;
	}
#ifdef DEBUG
    cout << "Axis::transition. completed " <<endl;
#endif
}

//------------------------------------------------------------------------------

AZEL_Mode_t Axis::evaluate(AZEL_Mode_t desiredMode)
{
#ifdef DEBUG
    cout << "Axis::evaluate" << endl;
#endif
    static int trans_table[6][6] = {
	{ 0, 1, 0, 0, 0, 0 },         // Shutdown to ...
	{ 0, 1, 2, 3, 4, 5 },         // Standby to ...
	{ 0, 1, 2, 3, 4, 5 },         // Encoder to ...
	{ 0, 1, 2, 3, 4, 5 },         // Autonomous to ...
	{ 0, 4, 4, 4, 4, 5 },         // Survival stow to ...
	{ 0, 5, 5, 5, 5, 5 }};        // Maintenance stow to ...

    return (AZEL_Mode_t)trans_table[(int)mode][(int)desiredMode];
}

//------------------------------------------------------------------------------

void Axis::set_servo_defaults()
{    
    for (int i = 0; i < N_COEFFS; i++)
	servo_coefficients[i] = (long long)i << 32;
}

//------------------------------------------------------------------------------

void Axis::set_aux_mode(AUX_Mode_t m)
{
    if (m < AUX_DISABLED || m > AUX12_MODE)
	return;
    
    auxmode = m;
}

//------------------------------------------------------------------------------

AUX_Mode_t Axis::get_aux_mode() const
{
    return auxmode;
}

//------------------------------------------------------------------------------

bool Axis::driving() const
{
    if (drive_state == STOP
	|| (drive_state == SERVO && delta_track == 0))
	return true;
    return false;
}

//------------------------------------------------------------------------------

int Axis::set_mode(AZEL_Mode_t m)
{
#ifdef DEBUG
    cout << "Axis::set_mode" << endl;
#endif
    newmode = m;
#ifdef DEBUG
    cout << "Axis::set_mode: new mode received " << ((int)newmode) << endl;
#endif
    if (newmode != mode)
	{
#ifdef DEBUG
	cout << "Axis::set_mode: about to transition" << endl;
#endif
	transition();
#ifdef DEBUG
	cout << "Axis::set_mode: transition done" << endl;
#endif
	}
    if (mode != m)
	{
#ifdef DEBUG
	cout << "Axis::set_mode: ERROR: current mode <> desired mode." << endl;
#endif
	return 1;  // ERROR
	}
#ifdef DEBUG
    cout << "Axis::set_mode: completed" << endl;    
#endif
    return 0;      // OK
}

//------------------------------------------------------------------------------

AZEL_Mode_t Axis::get_mode() const
{
#ifdef DEBUG
    cout << "Axis::get_mode: mode " << ((int)mode) << endl;
#endif
    return mode;
}

//------------------------------------------------------------------------------

AZEL_Brake_t Axis::get_brake() const
{
    return brake;
}    

//------------------------------------------------------------------------------

int Axis::set_brake(AZEL_Brake_t b)
{
    if (b < BRAKE_OFF || b > BRAKE_ON)
	return 2;
    // if moving, then don't put on brakes
    if (drive_state == STOP
	|| (drive_state == SERVO && delta_track == 0))
	{
	brake = b;
	return 0;
	}
    return 1;
}

//------------------------------------------------------------------------------

Stow_Pin_t Axis::get_stow_pin() const
{
    return pin;
}

//------------------------------------------------------------------------------

void Axis::set_stow_pin(Stow_Pin_t arg)
{
    // Can't insert stow pins if NOT in correct position
    if (arg == PIN_IN)
	{
	if (drive_state == STOP
	    || (drive_state == SERVO && delta_track == 0))
	    {
	    if (! inrange(cur_pos,0.010,mstow)
		&& ! inrange(cur_pos,0.010,sstow))
		{
		return;
		}
	    }
	}
    pin = (arg == PIN_IN) ? PIN_IN : (arg == PIN_OUT) ? PIN_OUT : pin;
}

//------------------------------------------------------------------------------

void Axis::get_cur(double& tp,double& tv,double& ta) const
{
    tp = cur_pos;
    tv = cur_vel;
    ta = cur_acc;
}

//------------------------------------------------------------------------------

void Axis::get_track(double &tp,double& tv,double& ta) const
{
    tp = track_pos;
    tv = track_vel;
    ta = 0;
}

//------------------------------------------------------------------------------

void Axis::servo()
{
    cur_pos += delta_track;
    double error = track_pos - cur_pos;
    //double dx = MAX_ACC * TIME_UNIT * TIME_UNIT / 2 + cur_vel * TIME_UNIT;
    
    if (error < 0)
	{
	error = min(-MAX_ACC * TIME_UNIT * TIME_UNIT / 2,error);
	}
    else
	{
	error = min(MAX_ACC * TIME_UNIT * TIME_UNIT / 2,error);
	}
    if (debug_level & OTHER_DEBUG)
	std::cout << "error is " << error << std::endl;
    cur_pos += error;

    // this needs work.
    cur_vel = track_vel;
}

//------------------------------------------------------------------------------

bool Axis::inrange(double actual,double delta,double compare)
{
    double minv = actual - delta;
    double maxv = actual + delta;
    return (compare >= minv) ? (maxv >= compare) ? 1 : 0 : 0;
}

//------------------------------------------------------------------------------

void Axis::inc_track()
{
    if ((track_pos <= (lower_limit+0.1) && delta_track < 0)
	|| (track_pos >= (upper_limit-0.1) && delta_track > 0))
	{
	// this is in a limit
	}
    else
	{
	track_pos += delta_track;
	}
}

//------------------------------------------------------------------------------

void Axis::inc_cur()
{
    // accelerate up to max velocity or within delta_x
    count++;
    if (brake == BRAKE_ON)
	return;
    
    // limits are tripped a little ahead of where they really are
    // so simulated tests can set a limit
    if (cur_pos <= (lower_limit + 0.1)) 
	{
	if (cur_vel < 0 || delta_track < 0) 
	    {
	    limit = (PRELIM_DOWN | LIMIT_DOWN);
	    drive_state = STOP;
	    cur_vel = 0;
	    cur_acc = 0;
	    return;
	    }
	}
    else if (cur_pos >= (upper_limit - 0.1)) 
	{
	if (cur_vel > 0 || delta_track < 0)
	    {
	    limit = (PRELIM_UP | LIMIT_UP);
	    drive_state = STOP;
	    cur_vel = 0;
	    cur_acc = 0;
	    return;
	    }
	}
    else
	{
	// execute other checks for pre limits, etc.
	limit = 0;
	}
    
    switch(drive_state)
	{
	case ACCELERATE:
	    cur_pos += cur_vel * TIME_UNIT 
		+ cur_acc * TIME_UNIT * TIME_UNIT / 2;
	    cur_vel += cur_acc * TIME_UNIT;
	    
	    if (cur_vel > MAX_VEL)
		{
		drive_state = SLEW;
		cur_vel = MAX_VEL;
		}
	    else if (cur_vel < -MAX_VEL)
		{
		drive_state = SLEW;
		cur_vel = -MAX_VEL;
		}
	    
	    if (inrange(decel_offset,
                abs(cur_vel * TIME_UNIT + cur_acc * TIME_UNIT * TIME_UNIT / 2),
		(cur_pos - track_pos)))
		{
		if (debug_level & STATE_CHANGES) 
		    std::cout << "acc: cur-track is " << cur_pos-track_pos <<
			", decel off is " << decel_offset << std::endl;
		drive_state = DECELERATE;
		}
	    break;
	    
	case SLEW:
	    cur_pos += cur_vel * TIME_UNIT;
	    {
	    double diff = track_pos - cur_pos;
	    if (debug_level & STATE_CHANGES) 
		std::cout << "diff " << diff << ", offset " << 
		    decel_offset << std::endl;
	    if ((cur_vel > 0 && track_vel >= 0 && diff <= decel_offset)
	     || (cur_vel < 0 && track_vel <= 0 && diff >= decel_offset)
	     || (cur_vel > 0 && track_vel <= 0 && diff <= decel_offset)
	     || (cur_vel < 0 && track_vel >= 0 && diff >= decel_offset))
		{
		drive_state = DECELERATE;
		}
	    }
	    break;

	case DECELERATE:
	    cur_pos += cur_vel * TIME_UNIT 
		- cur_acc * TIME_UNIT * TIME_UNIT / 2;
	    cur_vel -= cur_acc * TIME_UNIT;
	    
	    if (debug_level & STATE_CHANGES) 
		std::cout << "acc " << cur_acc << ", v " << 
		    cur_vel << ", dv " << decel_vel << std::endl;
	    
	    if ((cur_acc < 0 && cur_vel > decel_vel)
		|| (cur_acc > 0 && cur_vel < decel_vel))
		{
		cur_vel = track_vel;
		drive_state = SERVO;
		}
	    
	    if (cur_vel > MAX_VEL || cur_vel < -MAX_VEL)
		{
		drive_state = STOP;
		cur_vel = cur_acc = 0.;
		}
	    break;
	    
	case SERVO:
	    servo();
	    break;
	    
	case STOP:
	    break;
	    
	default:
	    break;
	}
}

//------------------------------------------------------------------------------

void Axis::drive()
{
    // get current position and figure out the velocity and
    // acceleration
    double diff = track_pos - cur_pos;
    double max_vel;

    if (diff < 0)
	{
	max_vel = -MAX_VEL;
	cur_acc = -MAX_ACC;
	}
    else
	{
	max_vel = MAX_VEL;
	cur_acc = MAX_ACC;
	}
    
    // Does this move need an acceleration up to a velocity, slew, and then 
    // deceleration?  Distances smaller than a * dt * dt / 2 away can be 
    // handled in one time step.

    // The move might be a bigger step than can be handled in one timing 
    // interval.
    // Need to calculate time to go from max velocity to decel_vel.

    // time to decelerate from max_vel to track_vel
    double t = (max_vel - track_vel) / cur_acc;
    
    /*
     * x1 - x0 = v0 * t + a * t * t / 2 = dx
     * t^2 * a / 2 + t * v0 - dx = 0
     * t = - (-v0 +- sqrt(v0*v0 + 2*a*dx)) / a, decelerating, so
     * - * - = +, t = (-v0 +- sqrt(v0*v0 + 2*a*dx)) / a
     */
    double t0 = (-cur_vel +
		 sqrt(cur_vel * cur_vel + 2 * cur_acc * diff)) / cur_acc;
    if (t0 < 0)
	t0 = (-cur_vel -
	      sqrt(cur_vel * cur_vel + 2 * cur_acc * diff)) / cur_acc;

    if (debug_level & DEBUG_DRIVE) 
	std::cout << "t0 is " << t0 << ", t is " << t << std::endl;

    if (t0 < t)
	{
	t = t0;
	max_vel = cur_acc * t;
	}
    
    double dv = track_vel - max_vel;
    double dt = -dv / cur_acc;

    if (debug_level & DEBUG_DRIVE)
	std::cout << "diff is " << diff;

    if (debug_level & DEBUG_DRIVE)
	std::cout << ", dt is " << dt;

    if (dt < TIME_UNIT * 3)
	{
	drive_state = SERVO;
	if (debug_level & DEBUG_DRIVE)
	    std::cout << std::endl;
	}
    else
	{
	if ((cur_acc > 0 && track_vel > 0)
	 || (cur_acc < 0 && track_vel < 0)
         || track_vel == 0)
	    {
	    decel_offset = -max_vel * dv / cur_acc - dv * dv
		/ (2 * cur_acc);
	    decel_vel = track_vel;
	    if (debug_level & DEBUG_DRIVE) 
		std::cout << ", decel_offset " << decel_offset 
		     << ", max_vel is " << max_vel << std::endl;
	    }
	else
	    {
	    // need to drive past desired position and then back up
	    // overdrive by distance needed to accelerate to new 
	    // velocity.
	    // dv = a * dt, dx = a*dt*dt/2, dx = dv * dv / (2*a)
	    decel_vel = 0;
	    decel_offset = - track_vel * track_vel / (2 * MAX_ACC);
	    if (debug_level & DEBUG_DRIVE) 
		std::cout << "oddball case, decel offset "
		     << decel_offset << std::endl;
	    }
	
	drive_state = ACCELERATE;
	}
}

//------------------------------------------------------------------------------

int Axis::set(double x,double vel)
{
    if (brake == BRAKE_ON)
	return 1;         // this should be an error
    if (x < lower_limit || x > upper_limit) 
	return 2;
    if (vel < -MAX_VEL || vel > MAX_VEL)
	return 3;
    if (mode == SHUTDOWN || mode == STANDBY)
	return 4;
    if (debug_level & OTHER_DEBUG) 
	std::cout << "set to x " << x << ", v " << vel << std::endl;
    track_pos = x;
    track_vel = vel;
    delta_track = vel * TIME_UNIT;
    newpos = true;
    return 0;
}

//------------------------------------------------------------------------------

void Axis::limits(double lower,double upper)
{
    lower_limit = lower;
    upper_limit = upper;
}

//------------------------------------------------------------------------------

void Axis::stows(double maint,double survive)
{
    mstow = maint;
    sstow = survive;
}


unsigned long long Axis::getStatus()
{
    unsigned long long lstat=0;
    return lstat;
}
unsigned long long Axis::getStatus2()
{
    unsigned long long lstat=0;
    return lstat;
}

