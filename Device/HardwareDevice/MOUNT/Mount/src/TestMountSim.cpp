// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <CANTypes.h>
#include <ACU.h>
#include "TestMountSim.h"
#include <maciACSComponentDefines.h>

using namespace maci;
using Control::MountImpl;
using Control::TestMountSim;

//------------------------------------------------------------------------------

TestMountSim::TestMountSim(const ACE_CString& name, maci::ContainerServices* pCS)
    : MountImpl(name,pCS),
      device_m(NULL)
{
    const char* __METHOD__ = "TestMountSim::TestMountSim";
    ACS_TRACE(__METHOD__); 
    
    encoderTransforms_p = new VAProtoEncTrans();
    
    std::vector<CAN::byte_t> sn(8, '0');
    device_m = new AMB::ACU(0x0,sn);
    simulationIf_m.setSimObj(device_m);
}

//------------------------------------------------------------------------------

TestMountSim::~TestMountSim()
{
    const char* __METHOD__ = "TestMountSim::~TestMountSim";
    ACS_TRACE(__METHOD__);
    
    if (encoderTransforms_p != NULL) delete encoderTransforms_p;
    if (device_m != NULL) delete device_m;
}

void TestMountSim::monitor(AmbRelativeAddr  RCA,
			   AmbDataLength_t& dataLength, 
			   AmbDataMem_t*    data,
			   sem_t*           synchLock,
			   Time*            timestamp,
			   AmbErrorCode_t*  status)
{
    simulationIf_m.monitor(RCA, dataLength, data, synchLock, timestamp, status);
}

void TestMountSim::command(AmbRelativeAddr     RCA,
			   AmbDataLength_t     dataLength,
			   const AmbDataMem_t* data,
			   sem_t*              synchLock,
			   Time*               timestamp,
			   AmbErrorCode_t*     status)
{
    simulationIf_m.command(RCA, dataLength, data, synchLock, timestamp, status);
}

void TestMountSim::monitorTE(ACS::Time        TimeEvent,
			     AmbRelativeAddr  RCA,
			     AmbDataLength_t& dataLength, 
			     AmbDataMem_t*    data,
			     sem_t*           synchLock,
			     Time*            timestamp,
			     AmbErrorCode_t*  status)
{
    simulationIf_m.monitorTE(TimeEvent, 
			     RCA, dataLength, data, synchLock, timestamp, status);
}

void TestMountSim::commandTE(ACS::Time           TimeEvent,
			     AmbRelativeAddr     RCA,
			     AmbDataLength_t     dataLength, 
			     const AmbDataMem_t* data,
			     sem_t*              synchLock,
			     Time*               timestamp,
			     AmbErrorCode_t*     status)
{
    simulationIf_m.commandTE(TimeEvent, RCA, dataLength, data, 
			     synchLock, timestamp, status);
}

void TestMountSim::monitorNextTE(AmbRelativeAddr     RCA,
				 AmbDataLength_t&    dataLength, 
				 AmbDataMem_t*       data,
				 sem_t*              synchLock,
				 Time*               timestamp,
				 AmbErrorCode_t*     status)
{
    simulationIf_m.monitorNextTE(RCA, dataLength, data, synchLock, timestamp, status);
}

void TestMountSim::commandNextTE(AMBSystem::AmbRelativeAddr RCA,
				 AmbDataLength_t            dataLength, 
				 const AmbDataMem_t*        data,
				 sem_t*                     synchLock,
				 ACS::Time*                 timestamp,
				 AmbErrorCode_t*            status)
{
    simulationIf_m.commandNextTE(RCA, dataLength, data, synchLock, timestamp, status);
}

void TestMountSim::flushNode(ACS::Time                      TimeEvent,
			     ACS::Time*                     timestamp,
			     AmbErrorCode_t*                status)
{
    simulationIf_m.flushNode(TimeEvent, timestamp, status);
}

void TestMountSim::flushRCA(ACS::Time                      TimeEvent,
			    AmbRelativeAddr                RCA,
			    ACS::Time*                     timestamp,
			    AmbErrorCode_t*                status)
{
    simulationIf_m.flushRCA(TimeEvent, RCA,  timestamp, status);
}



/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(TestMountSim)
    
