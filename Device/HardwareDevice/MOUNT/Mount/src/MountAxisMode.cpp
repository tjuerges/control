// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountAxisMode.h"
#include <MountImpl.h>
#include <RepeatGuard.h> // For the repeat guard in getAxisMode
#include <ControlExceptions.h>
#include <MountError.h>

const unsigned int POLLING_SLEEP_TIME = 1;

using std::vector;
using std::ostringstream;
using Control::MountImpl;
using Control::MountAxisMode;

MountAxisMode::MountAxisMode(MountImpl* mountReference,
                             unsigned int pollingTimeOut):
    Logging::Loggable(),
    enableMountControls(false),
    pollingTimeOut_m(pollingTimeOut),
    mountReference_m(mountReference)
{
    //pollingTimeOut_m=pollingTimeOut;
}

MountAxisMode::MountAxisMode(MountImpl* mountReference,
                             Logging::Logger::LoggerSmartPtr logger,
                             unsigned int pollingTimeOut):
    Logging::Loggable(logger),
    enableMountControls(false),
    pollingTimeOut_m(pollingTimeOut),
    mountReference_m(mountReference)
{
    //pollingTimeOut_m=pollingTimeOut;
}

MountAxisMode::~MountAxisMode(){}

void MountAxisMode::setAxisMode(Control::Mount::AxisMode azAxisMode, 
                                Control::Mount::AxisMode elAxisMode) {
    ACS_TRACE(__func__);
    
    if ((azAxisMode == Control::Mount::VELOCITY_MODE) || 
        (elAxisMode == Control::Mount::VELOCITY_MODE)) {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,__LINE__,__func__);
        ex.addData("AzMode",azAxisMode);
        ex.addData("ElMode",elAxisMode);
        throw ex;
    }
    
    Control::Mount::AxisMode currentAzAxisMode;
    Control::Mount::AxisMode currentElAxisMode;
    getAxisMode(currentAzAxisMode, currentElAxisMode);
    
    if((azAxisMode ==  currentAzAxisMode && 
        elAxisMode ==  currentElAxisMode)) {
        return;
    }

    if((azAxisMode == Control::Mount::ENCODER_MODE ||
        azAxisMode == Control::Mount::AUTONOMOUS_MODE ||
        azAxisMode == Control::Mount::SURVIVAL_STOW_MODE ||
        azAxisMode == Control::Mount::MAINTENANCE_STOW_MODE) &&
       (currentAzAxisMode != Control::Mount::STANDBY_MODE && currentAzAxisMode != azAxisMode)) {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,__LINE__,__func__);
        ex.addData("CurrentAzMode",currentAzAxisMode);
        ex.addData("CurrentElMode",currentElAxisMode);
        throw ex;
    }

    if((elAxisMode == Control::Mount::ENCODER_MODE ||
        elAxisMode == Control::Mount::AUTONOMOUS_MODE ||
        elAxisMode == Control::Mount::SURVIVAL_STOW_MODE ||
        elAxisMode == Control::Mount::MAINTENANCE_STOW_MODE) &&
       (currentElAxisMode != Control::Mount::STANDBY_MODE && currentElAxisMode != elAxisMode)) {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,__LINE__,__func__);
        ex.addData("CurrentAzMode",currentAzAxisMode);
        ex.addData("CurrentElMode",currentElAxisMode);
        throw ex;
    }
    
    unsigned char acuModeCmd = 0xFF;
    
    switch (elAxisMode) {
    case Control::Mount::SHUTDOWN_MODE:
        acuModeCmd &= 0x0F;
        break;
    case Control::Mount::STANDBY_MODE:
        acuModeCmd &= 0x1F;
        break;
    case Control::Mount::ENCODER_MODE:
        acuModeCmd &= 0x2F;
        break;
    case Control::Mount::AUTONOMOUS_MODE:
        acuModeCmd &= 0x3F;
        break;
    case Control::Mount::SURVIVAL_STOW_MODE:
        acuModeCmd &= 0x4F;
        break;
    case Control::Mount::MAINTENANCE_STOW_MODE:
        acuModeCmd &= 0x5F;
        break;
    case Control::Mount::VELOCITY_MODE:
        acuModeCmd &= 0x6F;
        break;
    }
    
    switch (azAxisMode) {
    case Control::Mount::SHUTDOWN_MODE:
        acuModeCmd &= 0xF0;
        break;
    case Control::Mount::STANDBY_MODE:
        acuModeCmd &= 0xF1;
        break;
    case Control::Mount::ENCODER_MODE:
        acuModeCmd &= 0xF2;
        break;
    case Control::Mount::AUTONOMOUS_MODE:
        acuModeCmd &= 0xF3;
        break;
    case Control::Mount::SURVIVAL_STOW_MODE:
        acuModeCmd &= 0xF4;
        break;
    case Control::Mount::MAINTENANCE_STOW_MODE:
        acuModeCmd &= 0xF5;
        break;
    case Control::Mount::VELOCITY_MODE:
        acuModeCmd &= 0xF6;
        break;
    }
    
    try {
        mountReference_m->setAcuModeCmd(acuModeCmd);
    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,__func__);
    } catch (ControlExceptions::INACTErrorExImpl& ex) {
        throw ControlExceptions::INACTErrorExImpl(ex,__FILE__,__LINE__,__func__);
    }
    
    if (acuModeCmd == 0x22 || acuModeCmd == 0x33) {
        enableMountControls = true;
    } else {
        enableMountControls = false;
    }
}

void MountAxisMode::setAzAxisMode(Control::Mount::AxisMode azAxisMode) {
    ACS_TRACE(__func__);
  
    Control::Mount::AxisMode oldAzAxisMode;
    Control::Mount::AxisMode oldElAxisMode;
    
    getAxisMode(oldAzAxisMode, oldElAxisMode);
    setAxisMode(azAxisMode, oldElAxisMode);
}

void MountAxisMode::setElAxisMode(Control::Mount::AxisMode elAxisMode) {
    ACS_TRACE(__func__);
    
    Control::Mount::AxisMode oldAzAxisMode;
    Control::Mount::AxisMode oldElAxisMode;
    
    getAxisMode(oldAzAxisMode, oldElAxisMode);
    setAxisMode(oldAzAxisMode, elAxisMode);
}

void MountAxisMode::getAxisMode(Control::Mount::AxisMode &azAxisMode, 
                                Control::Mount::AxisMode &elAxisMode) {
    ACS_TRACE(__func__);

    // vgonzale, 2008-12-04:
    // Use a repeat guard here so that the logs won't flood the system.
    // The guard will allow logging every 5s and does not limit the number of
    // logs. I've used same approach as in MountImpl::enforceSoftwareLimits
    static RepeatGuard repeatGuard(ACS::TimeInterval(5 * 1e7), 0);
    
    vector<unsigned char> acuModeRsp;
    ACS::Time             timestamp;
    
    try {
        acuModeRsp = mountReference_m->getAcuModeRsp(timestamp);
    } catch(ControlExceptions::CAMBErrorExImpl& ex) {
        throw ControlExceptions::CAMBErrorExImpl(ex,__FILE__,__LINE__,__func__);
    } catch (ControlExceptions::INACTErrorExImpl& ex) {
        throw ControlExceptions::INACTErrorExImpl(ex,__FILE__,__LINE__,__func__);
    }
    
    /* Elevation Axis */
    switch (acuModeRsp[0]& 0xF0) {
    case 0x00:
        elAxisMode = Control::Mount::SHUTDOWN_MODE;
        break;
    case 0x10:
        elAxisMode = Control::Mount::STANDBY_MODE;
        break;
    case 0x20:
        elAxisMode = Control::Mount::ENCODER_MODE;
        break;
    case 0x30:
        elAxisMode = Control::Mount::AUTONOMOUS_MODE;
        break;
    case 0x40:
        elAxisMode = Control::Mount::SURVIVAL_STOW_MODE;
        break;
    case 0x50:
        elAxisMode = Control::Mount::MAINTENANCE_STOW_MODE;
        break;
    case 0x60:
        elAxisMode = Control::Mount::VELOCITY_MODE;
        break;
    }
    
    /* Find the correct state for the azimuth axis */
    switch (acuModeRsp[0]& 0x0F) {
    case 0x00:
        azAxisMode = Control::Mount::SHUTDOWN_MODE;
        break;
    case 0x01:
        azAxisMode = Control::Mount::STANDBY_MODE;
        break;
    case 0x02:
        azAxisMode = Control::Mount::ENCODER_MODE;
        break;
    case 0x03:
        azAxisMode = Control::Mount::AUTONOMOUS_MODE;
        break;
    case 0x04:
        azAxisMode = Control::Mount::SURVIVAL_STOW_MODE;
        break;
    case 0x05:
        azAxisMode = Control::Mount::MAINTENANCE_STOW_MODE;
        break;
    case 0x06:
        azAxisMode = Control::Mount::VELOCITY_MODE;
        break;
    }

    if(repeatGuard.check() == true){
        ACS_LOG(LM_SOURCE_INFO,__func__,
                (LM_DEBUG, 
                 "azAxisMode: %d, elAxisMode: %d",
                 azAxisMode,
                 elAxisMode));
    }
}

bool MountAxisMode::inLocalMode() {
    AUTO_TRACE(__func__);

    ACS::Time timestamp;
    const vector<unsigned char> acuModeRsp = 
        mountReference_m->getAcuModeRsp(timestamp);
    const int mode = static_cast<int>(acuModeRsp[1]);

    // Check the value is in range. Only the Vertex antenna allows modes 3 & 4.
    // TODO. Use a virtual functions to get the mode range from the sub-class.
    if (mode == 0 || mode > 4) {
        ostringstream msg;
        msg << "Unrecognized response from ACU: 0x" << hex << mode;
        ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__, __func__);
        ex.addData("Reason", msg.str());
        throw ex;
    }

    if (mode == 2) return false;
    return true;
}

void MountAxisMode::pollingForAxisMode(Control::Mount::AxisMode axisMode) {
    ACS_TRACE(__func__);
    
    unsigned int index = 0;
    Control::Mount::AxisMode azAxisMode;
    Control::Mount::AxisMode elAxisMode;
    
    while(index < pollingTimeOut_m) {
        try{
            getAxisMode(azAxisMode,elAxisMode);
        }
        catch(ControlExceptions::INACTErrorExImpl& _ex){
            throw MountError::TimeOutExImpl(_ex,__FILE__,__LINE__,__func__);
        }
        if (azAxisMode == axisMode && elAxisMode == axisMode) return;
        ACE_OS::sleep(POLLING_SLEEP_TIME);
        index++;
    }
    throw MountError::TimeOutExImpl(__FILE__,__LINE__,__func__);
}

void MountAxisMode::waitForAxisMode(Control::Mount::AxisMode axisMode) {
    ACS_TRACE(__func__);
    
    ACS_LOG(LM_SOURCE_INFO,__func__,
            (LM_DEBUG,"Requesting new axis mode: %d", axisMode)); 
    try {
        if(inLocalMode())
            throw MountError::LocalModeExImpl(__FILE__,__LINE__,__func__);
    }
    catch(ControlExceptions::INACTErrorExImpl &_ex){
        throw MountError::TimeOutExImpl(_ex,__FILE__,__LINE__,__func__);
    }
    
    ACS_LOG(LM_SOURCE_INFO,__func__,
            (LM_DEBUG,"Get current axis mode ...")); 
    Control::Mount::AxisMode azAxisMode;
    Control::Mount::AxisMode elAxisMode;
    
    try {
        getAxisMode(azAxisMode,elAxisMode);
    }
    catch(ControlExceptions::INACTErrorExImpl& _ex){
        throw MountError::TimeOutExImpl(_ex,__FILE__,__LINE__,__func__);
    }
    
    if (azAxisMode != axisMode || elAxisMode != axisMode) {
        if((axisMode == Control::Mount::ENCODER_MODE ||
            axisMode == Control::Mount::AUTONOMOUS_MODE ||
            axisMode == Control::Mount::SURVIVAL_STOW_MODE ||
            axisMode == Control::Mount::MAINTENANCE_STOW_MODE)) {
            try {
                ACS_LOG(LM_SOURCE_INFO,__func__,
                        (LM_DEBUG,"Going to STANDBY_MODE ...")); 
                setAxisMode(Control::Mount::STANDBY_MODE, Control::Mount::STANDBY_MODE);
            }
            catch(ControlExceptions::INACTErrorExImpl &_ex){
                throw MountError::TimeOutExImpl(_ex,__FILE__,__LINE__,__func__);
            }
            pollingForAxisMode(Control::Mount::STANDBY_MODE);
        }
        
        try {
            ACS_LOG(LM_SOURCE_INFO,__func__,
                    (LM_DEBUG,"Setting axis mode to %d", axisMode)); 
            setAxisMode(axisMode, axisMode);
        }
        catch(ControlExceptions::INACTErrorExImpl &_ex){
            throw MountError::TimeOutExImpl(_ex,__FILE__,__LINE__,__func__);
        }
        if((axisMode == Control::Mount::SURVIVAL_STOW_MODE) ||
           (axisMode == Control::Mount::MAINTENANCE_STOW_MODE)) {
            ACS_LOG(LM_SOURCE_INFO,__func__,
                    (LM_DEBUG,"Waiting to reach SHUTDOWN_MODE")); 
            pollingForAxisMode(Control::Mount::SHUTDOWN_MODE);
        } else {
            ACS_LOG(LM_SOURCE_INFO,__func__,
                    (LM_DEBUG,"Waiting to reach requested mode: %d",axisMode)); 
            pollingForAxisMode(axisMode);
        }
    } else {
        if (axisMode == Control::Mount::ENCODER_MODE ||
            axisMode == Control::Mount::AUTONOMOUS_MODE) {
            enableMountControls = true;
        } else {
            enableMountControls = false;
        }
    }
}

void MountAxisMode::shutdown() {
    ACS_TRACE(__func__);
    
    waitForAxisMode(Control::Mount::SHUTDOWN_MODE);
    
}

bool MountAxisMode::inShutdownMode() {
    ACS_TRACE(__func__);
    
    Control::Mount::AxisMode azAxisMode;
    Control::Mount::AxisMode elAxisMode;
    
    getAxisMode(azAxisMode, elAxisMode);
    
    return (azAxisMode == Control::Mount::SHUTDOWN_MODE &&
            elAxisMode == Control::Mount::SHUTDOWN_MODE);
}

void MountAxisMode::standby() {
    ACS_TRACE(__func__);
    
    waitForAxisMode(Control::Mount::STANDBY_MODE);
    
}

bool MountAxisMode::inStandbyMode() {
    ACS_TRACE(__func__);
    
    Control::Mount::AxisMode azAxisMode;
    Control::Mount::AxisMode elAxisMode;
    
    getAxisMode(azAxisMode, elAxisMode);
    
    return (azAxisMode == Control::Mount::STANDBY_MODE &&
            elAxisMode == Control::Mount::STANDBY_MODE);
}

void MountAxisMode::encoder() {
    ACS_TRACE(__func__);
    
    waitForAxisMode(Control::Mount::ENCODER_MODE);
    
}

void MountAxisMode::autonomous() {
    ACS_TRACE(__func__);
    
    waitForAxisMode(Control::Mount::AUTONOMOUS_MODE);
}

bool MountAxisMode::isMoveable() {
    ACS_TRACE(__func__);
    
    Control::Mount::AxisMode azAxisMode;
    Control::Mount::AxisMode elAxisMode;
    
    getAxisMode(azAxisMode, elAxisMode);
    
    /* Note, in principle you can track having one axis in ENCODER and one
       in AUTONOMOUS.  To prevent accedently putting the antenna in this state
       and running in it, we exclude that possibilty below 
       Jorge and Jeff March 17 2007
    */
    
    return ((azAxisMode == Control::Mount::ENCODER_MODE  &&
             elAxisMode == Control::Mount::ENCODER_MODE) ||
            (azAxisMode == Control::Mount::AUTONOMOUS_MODE &&
             elAxisMode == Control::Mount::AUTONOMOUS_MODE));
}

void MountAxisMode::survivalStow() {
    ACS_TRACE(__func__);
    
    waitForAxisMode(Control::Mount::SURVIVAL_STOW_MODE);
}

void MountAxisMode::maintenanceStow() {
    ACS_TRACE(__func__);
    
    waitForAxisMode(Control::Mount::MAINTENANCE_STOW_MODE);
}
