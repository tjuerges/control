/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MountHWSimImpl.cpp
 */

#include "MountHWSimImpl.h"
#include "TypeConversion.h"

using namespace AMB;

const double MountHWSimImpl::PositionTransformations::defaultBitsPerDegree = (static_cast<double>(0x40000000) / 180.0);
const double MountHWSimImpl::PositionTransformations::defaultDegreesPerBit = (180.0 / static_cast<double>(0x40000000));
const double MountHWSimImpl::PositionTransformations::defaultEncoderOffset = 0.0;

MountHWSimImpl::PositionTransformations::PositionTransformations()
{
  bitsPerDegree_m = defaultBitsPerDegree;
  degreesPerBit_m = defaultDegreesPerBit;
  encoderOffset_m = defaultEncoderOffset;
}

void MountHWSimImpl::PositionTransformations::initialize(double bitsPerDegree, 
							 double degreesPerBit, 
							 double encoderOffset)
{
  bitsPerDegree_m = bitsPerDegree;
  degreesPerBit_m = degreesPerBit;
  encoderOffset_m = encoderOffset;
}

void MountHWSimImpl::PositionTransformations::degreesToBits(double degrees, long &bits) const
{
    bits = static_cast<long>(degrees * bitsPerDegree_m);
}

void MountHWSimImpl::PositionTransformations::bitsToDegrees(double &degrees, long bits)
{
  degrees = static_cast<double>(bits) * degreesPerBit_m;
}

void MountHWSimImpl::PositionTransformations::degreesToEncoder(double degrees, unsigned long &bits)
{
  long signedBits;
  degreesToBits(degrees-encoderOffset_m, signedBits);
  bits = static_cast<unsigned long>(signedBits);
}

void MountHWSimImpl::PositionTransformations::encoderToDegrees(double &degrees, unsigned long bits)
{
  long signedBits = static_cast<long>(bits);
  bitsToDegrees(degrees, signedBits);
  degrees += encoderOffset_m;
}

void MountHWSimImpl::PositionTransformations::degreesToEncoder(double degrees, long &bits) const
{
  degreesToBits(degrees-encoderOffset_m, bits);
}

void MountHWSimImpl::PositionTransformations::encoderToDegrees(double &degrees, long bits)
{
  bitsToDegrees(degrees, bits);
  degrees +=encoderOffset_m;
}

MountHWSimImpl::MountHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber)
  : MountHWSimBase::MountHWSimBase(node, serialNumber)
{
    
}

void MountHWSimImpl::associate(const rca_t RCA, const std::vector<CAN::byte_t>& data) const
{
  (state_m.find(RCA)->second)->clear();
  *(state_m.find(RCA)->second)=data; 
}

void MountHWSimImpl::setControlSetAzServoCoeffD(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetAzServoCoeffD(data);
  associate(monitorPoint_AZ_SERVO_COEFF_D, data);
}

void MountHWSimImpl::setControlSetAzServoCoeffE(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetAzServoCoeffE(data);
  associate(monitorPoint_AZ_SERVO_COEFF_E,data);
}

void MountHWSimImpl::setControlSetAzServoCoeffF(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetAzServoCoeffF(data);
  associate(monitorPoint_AZ_SERVO_COEFF_F, data);
}

void MountHWSimImpl::setControlSetElServoCoeffD(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetElServoCoeffD(data);
  associate(monitorPoint_EL_SERVO_COEFF_D, data);
}

void MountHWSimImpl::setControlSetElServoCoeffE(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetElServoCoeffE(data);
  associate(monitorPoint_EL_SERVO_COEFF_E, data);
}

void MountHWSimImpl::setControlSetElServoCoeffF(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetElServoCoeffF(data);
  associate(monitorPoint_EL_SERVO_COEFF_F, data);
}

void MountHWSimImpl::setControlSetSubrefAbsPosn(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetSubrefAbsPosn(data);
  associate(monitorPoint_SUBREF_ABS_POSN, data);
}

void MountHWSimImpl::setControlSetSubrefDeltaPosn(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetSubrefDeltaPosn(data);
  associate(monitorPoint_SUBREF_DELTA_POSN, data);
}

void MountHWSimImpl::setControlAcuTrkMode(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlAcuTrkMode(data);
  associate(monitorPoint_ACU_TRK_MODE_RSP, data);
}

void MountHWSimImpl::setControlSetIpAddress(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetIpAddress(data);
  associate(monitorPoint_IP_ADDRESS, data);  
}

void MountHWSimImpl::setControlSetIpGateway(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetIpGateway(data);
  associate(monitorPoint_IP_GATEWAY, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff00(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff00(data);
  associate(monitorPoint_PT_MODEL_COEFF_00, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff01(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff01(data);
  associate(monitorPoint_PT_MODEL_COEFF_01, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff02(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff02(data);
  associate(monitorPoint_PT_MODEL_COEFF_02, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff03(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff03(data);
  associate(monitorPoint_PT_MODEL_COEFF_03, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff04(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff04(data);
  associate(monitorPoint_PT_MODEL_COEFF_04, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff05(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff05(data);
  associate(monitorPoint_PT_MODEL_COEFF_05, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff06(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff06(data);
  associate(monitorPoint_PT_MODEL_COEFF_06, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff07(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff07(data);
  associate(monitorPoint_PT_MODEL_COEFF_07, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff08(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff08(data);
  associate(monitorPoint_PT_MODEL_COEFF_08, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff09(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff09(data);
  associate(monitorPoint_PT_MODEL_COEFF_09, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff0a(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff0a(data);
  associate(monitorPoint_PT_MODEL_COEFF_0A, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff0b(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff0b(data);
  associate(monitorPoint_PT_MODEL_COEFF_0B, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff0c(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff0c(data);
  associate(monitorPoint_PT_MODEL_COEFF_0C, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff0d(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff0d(data);
  associate(monitorPoint_PT_MODEL_COEFF_0D, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff0e(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff0e(data);
  associate(monitorPoint_PT_MODEL_COEFF_0E, data);
}

void MountHWSimImpl::setControlSetPtModelCoeff0f(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetPtModelCoeff0f(data);
  associate(monitorPoint_PT_MODEL_COEFF_0F, data);
}

void MountHWSimImpl::setControlSetSubrefRotation(const std::vector<CAN::byte_t>& data)
{
  MountHWSimBase::setControlSetSubrefRotation(data);
  associate(monitorPoint_SUBREF_ROTATION, data);
}

