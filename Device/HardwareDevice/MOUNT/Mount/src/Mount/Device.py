#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

import sys
import string
import time
from CCL.AmbManager import *
import struct
import ControlExceptions

# TBD: Replace this by an ACS exception
class DeviceException(Exception):
    '''
    General device exception
    '''
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

class Device:
    '''
    '''
    ICD          = {}
    command_type = {}
    def __init__(self,ambmgr,node,channel):
        '''
        '''
        self.command_type['monitor'] = self.monitor
        self.command_type['command'] = self.command
        self.ambmgr  = ambmgr
        self.node    = node
        self.channel = channel
        
    def command(self,RCA,format,*data):
        amb_data = struct.pack(format,*data)
        self.ambmgr.command(self.channel, self.node, RCA, amb_data)
        return None
        
    def monitor(self,RCA,format):
        ans,ts =  self.ambmgr.monitor(self.channel, self.node, RCA)
        # format'' is only used for get_acu_error monitor
        # the struct is unpacked only if there are errors
        if format == '':
            try:
                return struct.unpack('!5B',ans)
	    except struct.error:
                return ''
        else:
            return struct.unpack(format,ans)
        
    def execute(self,key,*data):
        if not self.ICD:
            raise DeviceException("Empty ICD specification")
        try:
            return self.command_type[self.ICD[key]['type']](self.ICD[key]['RCA'],self.ICD[key]['format'],*data)
        except KeyError:
            return "ICD command not defined"
	#TBD: Improve error message
	except ControlExceptions.CAMBErrorEx:
	    return "ABM/ACU comm error"
        #TBD: Handle incorrect struct unpack exception
	except struct.error:
	    return "Bytes received does not match specs"
