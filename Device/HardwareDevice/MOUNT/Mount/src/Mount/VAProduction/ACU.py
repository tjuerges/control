#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

from Mount.Common.ACU import ACU as ACUCommon
import struct

class ACU(ACUCommon):

    def __init__(self, ambmgr, node, channel):
        # Listed in ICD order
        # Commented entries are defined in Mount.Common.ACU
        
        # Monitor points
        #self.ICD['acu_mode_rsp'] = {'RCA': 0x22, 'format': '!2B', 'type': 'monitor'}
        #self.ICD['az_posn_rsp']  = {'RCA': 0x12, 'format': '!2l', 'type': 'monitor'}
        #self.ICD['el_posn_rsp']  = {'RCA': 0x02, 'format': '!2l', 'type': 'monitor'}
        self.ICD['get_acu_error'] = {'RCA': 0x2F, 'format': '!5B', 'type': 'monitor'} 
        self.ICD['acu_trk_mode_rsp'] = {'RCA': 0x20, 'format': '!B', 'type': 'monitor'} 
        self.ICD['get_az_traj_cmd'] = {'RCA': 0x13, 'format': '!2l', 'type': 'monitor'}
        self.ICD['get_az_aux_mode'] = {'RCA': 0x16, 'format': '!B',  'type': 'monitor'}
        #self.ICD['get_az_brake'] = {'RCA': 0x14, 'format': '!B',  'type': 'monitor'}
        #self.ICD['get_az_enc']   = {'RCA': 0x17, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_az_enc_status'] = {'RCA': 0x18, 'format': '!B',  'type': 'monitor'}
        self.ICD['get_az_encoder_offset'] = {'RCA': 0x1C, 'format': '!L',  'type': 'monitor'}        
        self.ICD['get_az_motor_currents'] = {'RCA': 0x19, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_az_motor_temps'] = {'RCA': 0x1A, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_az_motor_torque'] = {'RCA': 0x15, 'format': '!4B',  'type': 'monitor'}
        #for index in range(13,16):
        #    self.ICD['get_az_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x3020+index, 'format': '!d',  'type': 'monitor'}
        #self.ICD['get_az_status'] = {'RCA': 0x1B, 'format': '!8B',  'type': 'monitor'}
        #self.ICD['get_can_error'] = {'RCA': 0x30001, 'format': '!L',  'type': 'monitor'} #TBC
        self.ICD['get_el_aux_mode'] = {'RCA': 0x06, 'format': 'B',  'type': 'monitor'}
        self.ICD['get_el_traj_cmd'] = {'RCA': 0x03, 'format': '!2l', 'type': 'monitor'}
        #self.ICD['get_el_brake'] = {'RCA': 0x04, 'format': 'B',  'type': 'monitor'}
        self.ICD['get_el_enc'] = {'RCA': 0x07, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_el_enc_status'] = {'RCA': 0x08, 'format': '!B',  'type': 'monitor'}
        self.ICD['get_el_encoder_offset'] = {'RCA': 0x0C, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_el_motor_currents'] = {'RCA': 0x09, 'format': '!4B',  'type': 'monitor'} #TBC
        self.ICD['get_el_motor_temps'] = {'RCA': 0x0A, 'format': '!4B',  'type': 'monitor'} #TBC
        self.ICD['get_el_motor_torque'] = {'RCA': 0x05, 'format': '!4B',  'type': 'monitor'}
        #for index in range(13,16):
        #    self.ICD['get_el_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x3010+index, 'format': '!d',  'type': 'monitor'}
        #self.ICD['get_el_status'] = {'RCA': 0x0B, 'format': '!8B',  'type': 'monitor'}
        self.ICD['get_system_id'] = {'RCA': 0x30004, 'format': '!3B',  'type': 'monitor'}
        #self.ICD['get_idle_stow_time'] = {'RCA': 0x25, 'format': '!H',  'type': 'monitor'}
        self.ICD['get_ip_address'] = {'RCA': 0x2D, 'format': '!8B',  'type': 'monitor'}
        self.ICD['get_ip_gateway'] = {'RCA': 0x38, 'format': '!4B',  'type': 'monitor'}
        #self.ICD['get_num_trans'] = {'RCA': 0x30002, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_system_status'] = {'RCA': 0x23, 'format': '!6B',  'type': 'monitor'}
        for index in range(0,16):
            self.ICD['get_pt_model_coeff_'+str(index)] = {'RCA': 0x3040+index, 'format': '!d',  'type': 'monitor'}        
        #self.ICD['get_shutter'] = {'RCA': 0x2E, 'format': 'B',  'type': 'monitor'}
        #self.ICD['get_stow_pin'] = {'RCA': 0x24, 'format': '!2B',  'type': 'monitor'}
        self.ICD['get_subref_abs_posn'] = {'RCA': 0x26, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_subref_delta_posn'] = {'RCA': 0x27, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_subref_limits'] = {'RCA': 0x28, 'format': '!8B',    'type': 'monitor'}
        self.ICD['get_subref_rotation'] = {'RCA': 0x2A, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_subref_status'] = {'RCA': 0x29, 'format': '!5B',    'type': 'monitor'}
        self.ICD['get_metr_mode']  = {'RCA': 0x31, 'format': '!4B', 'type': 'monitor'}
        self.ICD['get_metr_equip_status']  = {'RCA': 0x32, 'format': '!4B', 'type': 'monitor'}
        for index in range(0,4):
            self.ICD['get_metr_displ_'+str(index)] = {'RCA': 0x6000+index, 'format': '!i',  'type': 'monitor'} #TBC
        for index in range(0,3):
            self.ICD['get_metr_tilt_'+str(index)] = {'RCA': 0x5000+index, 'format': '!3h',  'type': 'monitor'}
        self.ICD['get_metr_coeff_0']  = {'RCA': 0x3050, 'format': '!d', 'type': 'monitor'}
        self.ICD['get_metr_coeff_1']  = {'RCA': 0x3051, 'format': '!d', 'type': 'monitor'}
        self.ICD['get_metr_deltapath']  = {'RCA': 0x53, 'format': '!l', 'type': 'monitor'}
        self.ICD['get_metr_deltas']  = {'RCA': 0x34, 'format': '!2l', 'type': 'monitor'}
        self.ICD['get_metr_deltas_temp']  = {'RCA': 0x33, 'format': '!8B', 'type': 'monitor'}
        for index in range(0,25): 
          self.ICD['get_metr_temps_'+str(index)]  = {'RCA': 0x4000+index, 'format': '!4h', 'type': 'monitor'}
        self.ICD['get_power_status'] = {'RCA': 0x30, 'format': '!2B',  'type': 'monitor'}
        self.ICD['get_ac_status'] = {'RCA': 0x2C, 'format': 'B', 'type': 'monitor'}
        self.ICD['get_ups_output_volts'] = {'RCA': 0x35, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_output_current'] = {'RCA': 0x36, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_alarms'] = {'RCA': 0x3C, 'format': '!4B',    'type': 'monitor'}
        self.ICD['get_ups_battery_output'] = {'RCA': 0x38, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_battery_status'] = {'RCA': 0x39, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_bypass_volts'] = {'RCA': 0x3A, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_freqs'] = {'RCA': 0x3B, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_inverter_volts'] = {'RCA': 0x3D, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_antenna_temps'] = {'RCA': 0x37, 'format': '!2h',  'type': 'monitor'}
        #self.ICD['get_sw_rev_level'] = {'RCA': 0x30000, 'format': '!3B',  'type': 'monitor'}
        self.ICD['selftest_rsp'] = {'RCA': 0x40, 'format': '!5B',  'type': 'monitor'} #TBC       
        self.ICD['selftest_err'] = {'RCA': 0x41, 'format': '!6B',  'type': 'monitor'} #TBC      
        self.ICD['get_als_status'] = {'RCA': 0x44, 'format': '!2B',  'type': 'monitor'}        

        # Control points
        self.ICD['acu_mode_cmd'] = {'RCA': 0x1022, 'format': '!B',  'type': 'command'}
        self.ICD['az_traj_cmd'] = {'RCA': 0x1012, 'format': '!2l',  'type': 'command'}
        self.ICD['el_traj_cmd'] = {'RCA': 0x1002, 'format': '!2l',  'type': 'command'}
        #self.ICD['clear_fault_cmd'] = {'RCA': 0x1021, 'format': '!B',  'type': 'command'}
        self.ICD['init_subref_cmd'] = {'RCA': 0x1023, 'format': '!B',  'type': 'command'}
        #self.ICD['reset_acu_cmd'] = {'RCA': 0x102F, 'format': '!B',  'type': 'command'}
        self.ICD['set_az_aux_mode'] = {'RCA': 0x1016, 'format': 'B',  'type': 'command'}
        #self.ICD['set_az_brake'] = {'RCA': 0x1014, 'format': 'B',  'type': 'command'}
        for index in range(13,16):
            self.ICD['set_az_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x2020+index, 'format': '!d',  'type': 'command'}        
        #self.ICD['set_az_servo_default'] = {'RCA': 0x1017, 'format': 'B',  'type': 'command'}
        self.ICD['set_el_aux_mode'] = {'RCA': 0x1006, 'format': 'B',  'type': 'command'}
        #self.ICD['set_el_brake'] = {'RCA': 0x1004, 'format': 'B',  'type': 'command'}
        for index in range(13,16):
            self.ICD['set_el_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x2010+index, 'format': '!d',  'type': 'command'}        
        #self.ICD['set_el_servo_default'] = {'RCA': 0x1007, 'format': 'B',  'type': 'command'}
        #self.ICD['set_idle_stow_time'] = {'RCA': 0x1025, 'format': '!H',  'type': 'command'}
        self.ICD['set_ip_address'] = {'RCA': 0x1024, 'format': '!8B',  'type': 'command'}
        self.ICD['set_ip_gateway'] = {'RCA': 0x1038, 'format': '!4B',  'type': 'command'}
        for index in range(0,16):
            self.ICD['set_pt_model_coeff_'+str(index)] = {'RCA': 0x2040+index, 'format': '!d',  'type': 'command'}
        #self.ICD['set_stow_pin'] = {'RCA': 0x102D, 'format': '!2B',  'type': 'command'}
        self.ICD['set_subref_abs_posn'] = {'RCA': 0x1029, 'format': '!3h',  'type': 'command'}        
        self.ICD['set_subref_delta_posn'] = {'RCA': 0x102A, 'format': '!3h',  'type': 'command'}        
        self.ICD['subref_delta_zero_cmd'] = {'RCA': 0x102B, 'format': '!B',  'type': 'command'}        
        self.ICD['set_subref_rotation'] = {'RCA': 0x1028, 'format': '!3h',  'type': 'command'}        
        self.ICD['set_metr_mode'] = {'RCA': 0x1026, 'format': '!4B',  'type': 'command'}
        self.ICD['set_metr_coeff_0'] = {'RCA': 0x2050, 'format': '!d',  'type': 'command'}
        self.ICD['set_metr_coeff_1'] = {'RCA': 0x2051, 'format': '!d',  'type': 'command'}
        #self.ICD['set_shutter'] = {'RCA': 0x102E, 'format': '!B',  'type': 'command'}
        self.ICD['selftest_cmd'] = {'RCA': 0x1030, 'format': '!B',  'type': 'command'}
        self.ICD['set_als'] = {'RCA': 0x1044, 'format': 'B',  'type': 'command'}        
        self.ICD['set_rec_cab_temp'] = {'RCA': 0x1045, 'format': 'B',  'type': 'command'}
        self.ICD['acu_trk_mode'] = {'RCA': 0x1020, 'format': '!B',  'type': 'command'}
    
        ACUCommon(ambmgr,node,channel)

    def acu_mode_rsp(self):
        m1,m2 =  self.execute('acu_mode_rsp')
        maz = m1 & 0xf
        mel = m1 & 0xf0
        mel = mel >> 4
        return (maz,mel,m2)

    def az_posn_rsp(self):
        return self.execute('az_posn_rsp')

    def el_posn_rsp(self):
        return self.execute('el_posn_rsp')

    def get_acu_error(self):
        return self.execute('get_acu_error')

    def get_az_traj_cmd(self):
        return self.execute('get_az_traj_cmd')
    
    def get_az_aux_mode(self):
        return self.execute('get_az_aux_mode')

    def get_az_brake(self):
        return self.execute('get_az_brake')

    def get_az_enc(self):
        return self.execute('get_az_enc')

    def get_az_enc_status(self):
        return self.execute('get_az_enc_status')

    def get_az_encoder_offset(self):
        return self.execute('get_az_encoder_offset')

    def get_az_motor_currents(self):
        return self.execute('get_az_motor_currents')

    def get_az_motor_temps(self):
        return self.execute('get_az_motor_temps')

    def get_az_motor_torque(self):
        return self.execute('get_az_motor_torque')

    def get_az_servo_coeff_0(self):
        return self.execute('get_az_servo_coeff_0')

    def get_az_servo_coeff_1(self):
        return self.execute('get_az_servo_coeff_1')
    
    def get_az_servo_coeff_2(self):
        return self.execute('get_az_servo_coeff_2')
    
    def get_az_servo_coeff_3(self):
        return self.execute('get_az_servo_coeff_3')
    
    def get_az_servo_coeff_4(self):
        return self.execute('get_az_servo_coeff_4')
    
    def get_az_servo_coeff_5(self):
        return self.execute('get_az_servo_coeff_5')
    
    def get_az_servo_coeff_6(self):
        return self.execute('get_az_servo_coeff_6')
    
    def get_az_servo_coeff_7(self):
        return self.execute('get_az_servo_coeff_7')
    
    def get_az_servo_coeff_8(self):
        return self.execute('get_az_servo_coeff_8')
    
    def get_az_servo_coeff_9(self):
        return self.execute('get_az_servo_coeff_9')
    
    def get_az_servo_coeff_a(self):
        return self.execute('get_az_servo_coeff_a')
    
    def get_az_servo_coeff_b(self):
        return self.execute('get_az_servo_coeff_b')
    
    def get_az_servo_coeff_c(self):
        return self.execute('get_az_servo_coeff_c')

    def get_az_servo_coeff_d(self):
        return self.execute('get_az_servo_coeff_d')

    def get_az_servo_coeff_e(self):
        return self.execute('get_az_servo_coeff_e')

    def get_az_servo_coeff_f(self):
        return self.execute('get_az_servo_coeff_f')

    def get_az_status(self):
        return self.execute('get_az_status')
    
    def get_can_error(self):
        return self.execute('get_can_error')

    def get_el_aux_mode(self):
        return self.execute('get_el_aux_mode')

    def get_el_traj_cmd(self):
        return self.execute('get_el_traj_cmd')
    
    def get_el_brake(self):
        return self.execute('get_el_brake')

    def get_el_enc(self):
        return self.execute('get_el_enc')

    def get_el_enc_status(self):
        return self.execute('get_el_enc_status')

    def get_el_encoder_offset(self):
        return self.execute('get_el_encoder_offset')

    def get_el_motor_currents(self):
        return self.execute('get_el_motor_currents')

    def get_el_motor_temps(self):
        return self.execute('get_el_motor_temps')

    def get_el_motor_torque(self):
        return self.execute('get_el_motor_torque')

    def get_el_servo_coeff_0(self):
        return self.execute('get_el_servo_coeff_0')

    def get_el_servo_coeff_1(self):
        return self.execute('get_el_servo_coeff_1')
    
    def get_el_servo_coeff_2(self):
        return self.execute('get_el_servo_coeff_2')
    
    def get_el_servo_coeff_3(self):
        return self.execute('get_el_servo_coeff_3')
    
    def get_el_servo_coeff_4(self):
        return self.execute('get_el_servo_coeff_4')
    
    def get_el_servo_coeff_5(self):
        return self.execute('get_el_servo_coeff_5')
    
    def get_el_servo_coeff_6(self):
        return self.execute('get_el_servo_coeff_6')
    
    def get_el_servo_coeff_7(self):
        return self.execute('get_el_servo_coeff_7')
    
    def get_el_servo_coeff_8(self):
        return self.execute('get_el_servo_coeff_8')
    
    def get_el_servo_coeff_9(self):
        return self.execute('get_el_servo_coeff_9')
    
    def get_el_servo_coeff_a(self):
        return self.execute('get_el_servo_coeff_a')
    
    def get_el_servo_coeff_b(self):
        return self.execute('get_el_servo_coeff_b')
    
    def get_el_servo_coeff_c(self):
        return self.execute('get_el_servo_coeff_c')

    def get_el_servo_coeff_d(self):
        return self.execute('get_el_servo_coeff_d')

    def get_el_servo_coeff_e(self):
        return self.execute('get_el_servo_coeff_e')

    def get_el_servo_coeff_f(self):
        return self.execute('get_el_servo_coeff_f')

    def get_el_status(self):
        return self.execute('get_el_status')

    def get_system_id(self):
        return self.execute('get_system_id')

    def get_idle_stow_time(self):
        return self.execute('get_idle_stow_time')

    def get_ip_address(self):
        return self.execute('get_ip_address')

    def get_ip_gateway(self):
        return self.execute('get_ip_gateway')

    def get_num_trans(self):
        return self.execute('get_num_trans')

    def get_system_status(self):
        return self.execute('get_system_status')

    def get_pt_model_coeff_0(self):
        return self.execute('get_pt_model_coeff_0')

    def get_pt_model_coeff_1(self):
        return self.execute('get_pt_model_coeff_1')

    def get_pt_model_coeff_2(self):
        return self.execute('get_pt_model_coeff_2')

    def get_pt_model_coeff_3(self):
        return self.execute('get_pt_model_coeff_3')

    def get_pt_model_coeff_4(self):
        return self.execute('get_pt_model_coeff_4')

    def get_pt_model_coeff_5(self):
        return self.execute('get_pt_model_coeff_5')

    def get_pt_model_coeff_6(self):
        return self.execute('get_pt_model_coeff_6')

    def get_pt_model_coeff_7(self):
        return self.execute('get_pt_model_coeff_7')

    def get_pt_model_coeff_8(self):
        return self.execute('get_pt_model_coeff_8')

    def get_pt_model_coeff_9(self):
        return self.execute('get_pt_model_coeff_9')

    def get_pt_model_coeff_10(self):
        return self.execute('get_pt_model_coeff_10')

    def get_pt_model_coeff_11(self):
        return self.execute('get_pt_model_coeff_11')

    def get_pt_model_coeff_12(self):
        return self.execute('get_pt_model_coeff_12')

    def get_pt_model_coeff_13(self):
        return self.execute('get_pt_model_coeff_13')

    def get_pt_model_coeff_14(self):
        return self.execute('get_pt_model_coeff_14')

    def get_pt_model_coeff_15(self):
        return self.execute('get_pt_model_coeff_15')

    def get_shutter(self):
        return self.execute('get_shutter')

    def get_stow_pin(self):
        return self.execute('get_stow_pin')

    def get_subref_abs_posn(self):
        return self.execute('get_subref_abs_posn')
    
    def get_subref_delta_posn(self):
        return self.execute('get_subref_delta_posn')
    
    def get_subref_limits(self):
        return self.execute('get_subref_limits')
    
    def get_subref_rotation(self):
        return self.execute('get_subref_rotation')
    
    def get_subref_status(self):
        return self.execute('get_subref_status')

    def get_metr_mode(self):
        return self.execute('get_metr_mode')
    
    def get_metr_equip_status(self):
        return self.execute('get_metr_equip_status')
    
    def get_metr_displ_0(self):
        return self.execute('get_metr_displ_0')
    
    def get_metr_displ_1(self):
        return self.execute('get_metr_displ_1')
    
    def get_metr_displ_2(self):
        return self.execute('get_metr_displ_2')
    
    def get_metr_displ_3(self):
        return self.execute('get_metr_displ_3')
    
    def get_metr_tilt_0(self):
        return self.execute('get_metr_tilt_0')
    
    def get_metr_tilt_1(self):
        return self.execute('get_metr_tilt_1')
    
    def get_metr_tilt_2(self):
        return self.execute('get_metr_tilt_2')
    
    def get_metr_coeff_0(self):
        return self.execute('get_metr_coeff_0')

    def get_metr_coeff_1(self):
        return self.execute('get_metr_coeff_1')

    def get_metr_deltapath(self):
        return self.execute('get_metr_deltapath')
        
    def get_metr_deltas(self):
        return self.execute('get_metr_deltas')
    
    def get_metr_temps_0(self):
        return self.execute('get_metr_temps_0')
    
    def get_metr_temps_1(self):
        return self.execute('get_metr_temps_1')
    
    def get_metr_temps_2(self):
        return self.execute('get_metr_temps_2')
    
    def get_metr_temps_3(self):
        return self.execute('get_metr_temps_3')
    
    def get_metr_temps_4(self):
        return self.execute('get_metr_temps_4')
    
    def get_metr_temps_5(self):
        return self.execute('get_metr_temps_5')
    
    def get_metr_temps_6(self):
        return self.execute('get_metr_temps_6')
    
    def get_metr_temps_7(self):
        return self.execute('get_metr_temps_7')
    
    def get_metr_temps_8(self):
        return self.execute('get_metr_temps_8')
    
    def get_metr_temps_9(self):
        return self.execute('get_metr_temps_9')
    
    def get_metr_temps_10(self):
        return self.execute('get_metr_temps_10')
    
    def get_metr_temps_11(self):
        return self.execute('get_metr_temps_11')
    
    def get_metr_temps_12(self):
        return self.execute('get_metr_temps_12')
    
    def get_metr_temps_13(self):
        return self.execute('get_metr_temps_13')
    
    def get_metr_temps_14(self):
        return self.execute('get_metr_temps_14')
    
    def get_metr_temps_15(self):
        return self.execute('get_metr_temps_15')
    
    def get_metr_temps_16(self):
        return self.execute('get_metr_temps_16')
    
    def get_metr_temps_17(self):
        return self.execute('get_metr_temps_17')
    
    def get_metr_temps_18(self):
        return self.execute('get_metr_temps_18')
    
    def get_metr_temps_19(self):
        return self.execute('get_metr_temps_19')
    
    def get_metr_temps_20(self):
        return self.execute('get_metr_temps_20')
    
    def get_metr_temps_21(self):
        return self.execute('get_metr_temps_21')
    
    def get_metr_temps_22(self):
        return self.execute('get_metr_temps_22')
    
    def get_metr_temps_23(self):
        return self.execute('get_metr_temps_23')
    
    def get_metr_temps_24(self):
        return self.execute('get_metr_temps_24')
    
    def get_power_status(self):
        return self.execute('get_power_status')
    
    def get_ac_status(self):
        return self.execute('get_ac_status')
    
    def get_ups_output_volts(self):
        return self.execute('get_ups_output_volts')
    
    def get_ups_output_current(self):
        return self.execute('get_ups_output_current')

    def get_ups_alarms(self):
        return self.execute('get_ups_alarms')
    
    def get_ups_battery_output(self):
        return self.execute('get_ups_battery_output')
    
    def get_ups_battery_status(self):
        return self.execute('get_ups_battery_status')
    
    def get_ups_bypass_volts(self):
        return self.execute('get_ups_bypass_volts')
    
    def get_ups_freqs(self):
        return self.execute('get_ups_freqs')
    
    def get_ups_inverter_volts(self):
        return self.execute('get_ups_inverter_volts')

    def get_antenna_temps(self):
        return self.execute('get_antenna_temps')
    
    def get_sw_rev_level(self):
        return self.execute('get_sw_rev_level')

    def selftest_rsp(self):
        return self.execute('selftest_rsp')
 
    def selftest_err(self):
        r = self.execute('selftest_err')
        i = (r[0] << 8) | r[1]
        f = '' + chr(r[2]) + chr(r[3]) + chr(r[4]) + chr(r[5])
        result = [int(i),struct.unpack(">1f", f)]
        return result

    def get_als_status(self):
        return self.execute('get_als_status')

    def acu_trk_mode_rsp(self):
        return self.execute('acu_trk_mode_rsp')

    def acu_mode_cmd(self,data):
        self.execute('acu_mode_cmd',data)
        return

    def az_traj_cmd(self,data1, data2):
        self.execute('az_traj_cmd',data1,data2)
        return
    
    def el_traj_cmd(self,data1,data2):
        self.execute('el_traj_cmd',data1,data2)
        return

    def clear_fault_cmd(self,data):
        self.execute('clear_fault_cmd',data)
        return
    
    def init_subref_cmd(self,data):
        self.execute('init_subref_cmd',data)
        return

    def reset_acu_cmd(self,data):
        self.execute('reset_acu_cmd',data)
        return

    def set_az_aux_mode(self,data):
        self.execute('set_az_aux_mode',data)
        return

    def set_az_brake(self,data):
        self.execute('set_az_brake',data)
        return

    def set_servo_coeff(self,axis,index,data):
        self.execute('set_'+axis+'_servo_coeff_'+index,data)

    def set_az_servo_coeff(self,index,data):
        self.set_servo_coeff('az',index,data)
        return
    
    def set_az_servo_coeff_0(self,data):
        self.set_az_servo_coeff('0',data)
        return

    def set_az_servo_coeff_1(self,data):
        self.set_az_servo_coeff('1',data)
        return

    def set_az_servo_coeff_2(self,data):
        self.set_az_servo_coeff('2',data)
        return

    def set_az_servo_coeff_3(self,data):
        self.set_az_servo_coeff('3',data)
        return

    def set_az_servo_coeff_4(self,data):
        self.set_az_servo_coeff('4',data)
        return

    def set_az_servo_coeff_5(self,data):
        self.set_az_servo_coeff('5',data)
        return

    def set_az_servo_coeff_6(self,data):
        self.set_az_servo_coeff('6',data)
        return

    def set_az_servo_coeff_7(self,data):
        self.set_az_servo_coeff('7',data)
        return

    def set_az_servo_coeff_8(self,data):
        self.set_az_servo_coeff('8',data)
        return

    def set_az_servo_coeff_9(self,data):
        self.set_az_servo_coeff('9',data)
        return

    def set_az_servo_coeff_a(self,data):
        self.set_az_servo_coeff('a',data)
        return

    def set_az_servo_coeff_b(self,data):
        self.set_az_servo_coeff('b',data)
        return

    def set_az_servo_coeff_c(self,data):
        self.set_az_servo_coeff('c',data)
        return

    def set_az_servo_coeff_d(self,data):
        self.set_az_servo_coeff('d',data)
        return

    def set_az_servo_coeff_e(self,data):
        self.set_az_servo_coeff('e',data)
        return

    def set_az_servo_coeff_f(self,data):
        self.set_az_servo_coeff('f',data)
        return

    def set_az_servo_default(self,data):
        self.execute('set_az_servo_default',data)
        return

    def set_el_aux_mode(self,data):
        self.execute('set_el_aux_mode',data)
        return
    
    def set_el_brake(self,data):
        self.execute('set_el_brake',data)
        return

    def set_el_servo_coeff(self,index,data):
        self.set_servo_coeff('el',index,data)
        
    def set_el_servo_coeff_0(self,data):
        self.set_el_servo_coeff('0',data)
        return
    
    def set_el_servo_coeff_1(self,data):
        self.set_el_servo_coeff('1',data)
        return
    
    def set_el_servo_coeff_2(self,data):
        self.set_el_servo_coeff('2',data)
        return
    
    def set_el_servo_coeff_3(self,data):
        self.set_el_servo_coeff('3',data)
        return
    
    def set_el_servo_coeff_4(self,data):
        self.set_el_servo_coeff('4',data)
        return
    
    def set_el_servo_coeff_5(self,data):
        self.set_el_servo_coeff('5',data)
        return
    
    def set_el_servo_coeff_6(self,data):
        self.set_el_servo_coeff('6',data)
        return
    
    def set_el_servo_coeff_7(self,data):
        self.set_el_servo_coeff('7',data)
        return
    
    def set_el_servo_coeff_8(self,data):
        self.set_el_servo_coeff('8',data)
        return
    
    def set_el_servo_coeff_9(self,data):
        self.set_el_servo_coeff('9',data)
        return
    
    def set_el_servo_coeff_a(self,data):
        self.set_el_servo_coeff('a',data)
        return
    
    def set_el_servo_coeff_b(self,data):
        self.set_el_servo_coeff('b',data)
        return
    
    def set_el_servo_coeff_c(self,data):
        self.set_el_servo_coeff('c',data)
        return
    
    def set_el_servo_coeff_d(self,data):
        self.set_el_servo_coeff('d',data)
        return
    
    def set_el_servo_coeff_e(self,data):
        self.set_el_servo_coeff('e',data)
        return
    
    def set_el_servo_coeff_f(self,data):
        self.set_el_servo_coeff('f',data)
        return
    
    def set_el_servo_default(self,data):
        self.execute('set_el_servo_default',data)
        return
    
    def set_idle_stow_time(self,data):
        self.execute('set_idle_stow_time',data)
        return
    
    def set_ip_address(self,data1,data2,data3,data4,data5,data6,data7,data8):
        self.execute('set_ip_address',data1,data2,data3,data4,data5,data6,data7,data8)
        return

    def set_ip_gateway(self,data1,data2,data3,data4):
        self.execute('set_ip_gateway',data1,data2,data3,data4)
	return

    def set_pt_model_coeff_0(self,data):
        self.execute('set_pt_model_coeff_0',data)
        return
    
    def set_pt_model_coeff_1(self,data):
        self.execute('set_pt_model_coeff_1',data)
        return
    
    def set_pt_model_coeff_2(self,data):
        self.execute('set_pt_model_coeff_2',data)
        return
    
    def set_pt_model_coeff_3(self,data):
        self.execute('set_pt_model_coeff_3',data)
        return
    
    def set_pt_model_coeff_4(self,data):
        self.execute('set_pt_model_coeff_4',data)
        return
    
    def set_pt_model_coeff_5(self,data):
        self.execute('set_pt_model_coeff_5',data)
        return
    
    def set_pt_model_coeff_6(self,data):
        self.execute('set_pt_model_coeff_6',data)
        return
    
    def set_pt_model_coeff_7(self,data):
        self.execute('set_pt_model_coeff_7',data)
        return
    
    def set_pt_model_coeff_8(self,data):
        self.execute('set_pt_model_coeff_8',data)
        return
    
    def set_pt_model_coeff_9(self,data):
        self.execute('set_pt_model_coeff_9',data)
        return
    
    def set_pt_model_coeff_10(self,data):
        self.execute('set_pt_model_coeff_10',data)
        return
    
    def set_pt_model_coeff_11(self,data):
        self.execute('set_pt_model_coeff_11',data)
        return
    
    def set_pt_model_coeff_12(self,data):
        self.execute('set_pt_model_coeff_12',data)
        return
    
    def set_pt_model_coeff_13(self,data):
        self.execute('set_pt_model_coeff_13',data)
        return
    
    def set_pt_model_coeff_14(self,data):
        self.execute('set_pt_model_coeff_14',data)
        return
    
    def set_pt_model_coeff_15(self,data):
        self.execute('set_pt_model_coeff_15',data)
        return
    
    def set_stow_pin(self,data1,data2):
        self.execute('set_stow_pin',data1,data2)
        return 

    def set_subref_abs_posn(self,data1,data2,data3):
        self.execute('set_subref_abs_posn',data1,data2,data3)
        return
    
    def set_subref_delta_posn(self,data1,data2,data3):
        self.execute('set_subref_delta_posn',data1,data2,data3)
        return
    
    def subref_delta_zero_cmd(self,data1):
        self.execute('subref_delta_zero_cmd',data1)
        return
    
    def set_subref_rotation(self,data1,data2,data3):
        self.execute('set_subref_rotation',data1,data2,data3)
        return

    def set_metr_mode(self,data):
        self.execute('set_metr_mode',data)
        return
    
    def set_metr_coeff_0(self,data):
        self.execute('set_metr_coeff_0',data)
	return 

    def set_metr_coeff_1(self,data):
        self.execute('set_metr_coeff_1',data)
	return 

    def set_shutter(self,data):
        self.execute('set_shutter',data)
        return
    
    def selftest_cmd(self,data):
        self.execute('selftest_cmd',data)
        return

    def set_als(self,data):
        self.execute('set_als',data)
        return

    def set_rec_cab_temp(self,data):
        self.execute('set_rec_cab_temp',data)
        return

    def acu_trk_mode(self,data):
        self.execute('acu_trk_mode', data)
        return 
