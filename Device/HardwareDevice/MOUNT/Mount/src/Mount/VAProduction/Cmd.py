#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

import math
from Mount.Common.Cmd  import CommonCmd, ACUCommonCmd
from Mount.VAProduction.ACU import ACU

class ACUCmd(ACUCommonCmd):
    prompt = "VERTEX ACU> "

    def preloop(self):
        self.acu = ACU(self.acuClient.ambmgr,self.node,self.channel)

    # Command definitions
    # MONITOR POINTS
    def do_get_acu_error(self, rest):
	print self.acu.get_acu_error()

    def do_acu_trk_mode_rsp(self, rest):
	print self.acu.acu_trk_mode_rsp()

    def do_get_ip_gateway(self, rest):
	print self.acu.get_ip_gateway()

    def do_get_az_traj_cmd(self,rest):
        print self.acu.get_az_traj_cmd()

    def do_get_az_aux_mode(self,rest):
        print self.acu.get_az_aux_mode()

    def do_get_az_enc_status(self,rest):
        status = self.acu.get_az_enc_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)

    def do_get_az_encoder_offset(self,rest):
        print self.acu.get_az_encoder_offset()

    def do_get_az_servo_coeff_d(self,rest):
        print self.acu.get_az_servo_coeff_d()
        
    def do_get_az_servo_coeff_e(self,rest):
        print self.acu.get_az_servo_coeff_e()
        
    def do_get_az_servo_coeff_f(self,rest):
        print self.acu.get_az_servo_coeff_f()
        
    def do_get_el_aux_mode(self,rest):
        print self.acu.get_el_aux_mode()
        
    def do_get_el_traj_cmd(self,rest):
        print self.acu.get_el_traj_cmd()

    def do_get_el_enc_status(self,rest):
        status =  self.acu.get_el_enc_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)

    def do_get_el_encoder_offset(self,rest):
        print self.acu.get_el_encoder_offset()

    def do_get_el_servo_coeff_d(self,rest):
        print self.acu.get_el_servo_coeff_d()

    def do_get_el_servo_coeff_e(self,rest):
        print self.acu.get_el_servo_coeff_e()

    def do_get_el_servo_coeff_f(self,rest):
        print self.acu.get_el_servo_coeff_f()

    def do_get_pt_model_coeff_0(self,rest):
        print self.acu.get_pt_model_coeff_0()

    def do_get_pt_model_coeff_1(self,rest):
        print self.acu.get_pt_model_coeff_1()

    def do_get_pt_model_coeff_2(self,rest):
        print self.acu.get_pt_model_coeff_2()

    def do_get_pt_model_coeff_3(self,rest):
        print self.acu.get_pt_model_coeff_3()

    def do_get_pt_model_coeff_4(self,rest):
        print self.acu.get_pt_model_coeff_4()

    def do_get_pt_model_coeff_5(self,rest):
        print self.acu.get_pt_model_coeff_5()

    def do_get_pt_model_coeff_6(self,rest):
        print self.acu.get_pt_model_coeff_6()

    def do_get_pt_model_coeff_7(self,rest):
        print self.acu.get_pt_model_coeff_7()

    def do_get_pt_model_coeff_8(self,rest):
        print self.acu.get_pt_model_coeff_8()

    def do_get_pt_model_coeff_9(self,rest):
        print self.acu.get_pt_model_coeff_9()

    def do_get_pt_model_coeff_10(self,rest):
        print self.acu.get_pt_model_coeff_10()

    def do_get_pt_model_coeff_11(self,rest):
        print self.acu.get_pt_model_coeff_11()

    def do_get_pt_model_coeff_12(self,rest):
        print self.acu.get_pt_model_coeff_12()

    def do_get_pt_model_coeff_13(self,rest):
        print self.acu.get_pt_model_coeff_13()

    def do_get_pt_model_coeff_14(self,rest):
        print self.acu.get_pt_model_coeff_14()

    def do_get_pt_model_coeff_15(self,rest):
        print self.acu.get_pt_model_coeff_15()

    def do_get_subref_abs_posn(self,rest):
        print self.acu.get_subref_abs_posn()

    def do_get_subref_delta_posn(self,rest):
        print self.acu.get_subref_delta_posn()

    def do_get_subref_limits(self,rest):
        print self.acu.get_subref_limits()

    def do_get_subref_rotation(self,rest):
        print self.acu.get_subref_rotation()

    def do_get_subref_status(self,rest):
        print self.acu.get_subref_status()

    def do_get_metr_mode(self,rest):
        status = self.acu.get_metr_mode()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)

    def do_get_metr_equip_status(self,rest):
        print self.acu.get_metr_equip_status()

    def do_get_metr_displ_0(self,rest):
        print self.acu.get_metr_displ_0()

    def do_get_metr_displ_1(self,rest):
        print self.acu.get_metr_displ_1()

    def do_get_metr_displ_2(self,rest):
        print self.acu.get_metr_displ_2()

    def do_get_metr_displ_3(self,rest):
        print self.acu.get_metr_displ_3()

    def do_get_metr_tilt_0(self,rest):
        print self.acu.get_metr_tilt_0()

    def do_get_metr_tilt_1(self,rest):
        print self.acu.get_metr_tilt_1()

    def do_get_metr_tilt_2(self,rest):
        print self.acu.get_metr_tilt_2()

    def do_get_metr_coeff_0(self,rest):
        print self.acu.get_metr_coeff_0()

    def do_get_metr_coeff_1(self,rest):
        print self.acu.get_metr_coeff_1()

    def do_get_metr_temps_0(self,rest):
        print self.acu.get_metr_temps_0()

    def do_get_metr_temps_1(self,rest):
        print self.acu.get_metr_temps_1()

    def do_get_metr_temps_2(self,rest):
        print self.acu.get_metr_temps_2()

    def do_get_metr_temps_3(self,rest):
        print self.acu.get_metr_temps_3()

    def do_get_metr_temps_4(self,rest):
        print self.acu.get_metr_temps_4()

    def do_get_metr_temps_5(self,rest):
        print self.acu.get_metr_temps_5()

    def do_get_metr_temps_6(self,rest):
        print self.acu.get_metr_temps_6()

    def do_get_metr_temps_7(self,rest):
        print self.acu.get_metr_temps_7()

    def do_get_metr_temps_8(self,rest):
        print self.acu.get_metr_temps_8()

    def do_get_metr_temps_9(self,rest):
        print self.acu.get_metr_temps_9()

    def do_get_metr_temps_10(self,rest):
        print self.acu.get_metr_temps_10()

    def do_get_metr_temps_11(self,rest):
        print self.acu.get_metr_temps_11()

    def do_get_metr_temps_12(self,rest):
        print self.acu.get_metr_temps_12()

    def do_get_metr_temps_13(self,rest):
        print self.acu.get_metr_temps_13()

    def do_get_metr_temps_14(self,rest):
        print self.acu.get_metr_temps_14()

    def do_get_metr_temps_15(self,rest):
        print self.acu.get_metr_temps_15()

    def do_get_metr_temps_16(self,rest):
        print self.acu.get_metr_temps_16()

    def do_get_metr_temps_17(self,rest):
        print self.acu.get_metr_temps_17()

    def do_get_metr_temps_18(self,rest):
        print self.acu.get_metr_temps_18()

    def do_get_metr_temps_19(self,rest):
        print self.acu.get_metr_temps_19()

    def do_get_metr_temps_20(self,rest):
        print self.acu.get_metr_temps_20()

    def do_get_metr_temps_21(self,rest):
        print self.acu.get_metr_temps_21()

    def do_get_metr_temps_22(self,rest):
        print self.acu.get_metr_temps_22()

    def do_get_metr_temps_23(self,rest):
        print self.acu.get_metr_temps_23()

    def do_get_metr_temps_24(self,rest):
        print self.acu.get_metr_temps_24()

    def do_get_metr_deltapath(self,rest):
        print self.acu.get_metr_deltapath()
        
    def do_get_metr_deltas(self,rest):
        print self.acu.get_metr_deltas()

    def do_get_power_status(self,rest):
        print self.acu.get_power_status()

    def do_get_ac_status(self,rest):
        print self.acu.get_ac_status()

    def do_get_ups_output_volts(self,rest):
        print self.acu.get_ups_output_volts()

    def do_get_ups_output_current(self,rest):
        print self.acu.get_ups_output_current()
        
    def do_get_ups_alarms(self,rest):
        print self.acu.get_ups_alarms()

    def do_get_ups_battery_output(self,rest):
        print self.acu.get_ups_battery_output()

    def do_get_ups_battery_status(self,rest):
        print self.acu.get_ups_battery_status()

    def do_get_ups_bypass_volts(self,rest):
        print self.acu.get_ups_bypass_volts()

    def do_get_ups_freqs(self,rest):
        print self.acu.get_ups_freqs()      

    def do_get_ups_inverter_volts(self,rest):
        print self.acu.get_ups_inverter_volts()

    def do_get_antenna_temps(self,rest):
        print self.acu.get_antenna_temps()

    def do_selftest_rsp(self,rest):
        print self.acu.selftest_rsp()

    def do_selftest_err(self,rest):
        print self.acu.selftest_err()

    def do_get_als_status(self,rest):
        print self.acu.get_als_status()

    # CONTROL POINTS
    def help_clear_fault_cmd(self):
        print "clear_fault_cmd 1"
    def do_clear_fault_cmd(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.clear_fault_cmd(arguments[0])
        else:
            self.help_clear_fault_cmd()

    def help_init_subref_cmd(self):
        print "init_subref_cmd 1"
    def do_init_subref_cmd(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.init_subref_cmd(arguments[0])
        else:
            self.help_init_subref_cmd()

    def help_set_az_aux_mode(self):
        self.short_help("set_az_aux_mode <mode>")
    def do_set_az_aux_mode(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_aux_mode(arguments[0])
        else:
            self.help_set_az_aux_mode()

    def help_set_az_servo_coeff_d(self):
        self.build_help_set_az_servo_coeff('d')
    def do_set_az_servo_coeff_d(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_d(arguments[0])
        else:
            self.help_set_az_servo_coeff_d()

    def help_set_az_servo_coeff_e(self):
        self.build_help_set_az_servo_coeff('e')
    def do_set_az_servo_coeff_e(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_e(arguments[0])
        else:
            self.help_set_az_servo_coeff_e()

    def help_set_az_servo_coeff_f(self):
        self.build_help_set_az_servo_coeff('f')
    def do_set_az_servo_coeff_f(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_f(arguments[0])
        else:
            self.help_set_az_servo_coeff_f()

    def help_set_el_aux_mode(self):
        self.short_help("set_el_aux_mode <mode>")
    def do_set_el_aux_mode(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_aux_mode(arguments[0])
        else:
            self.help_set_el_aux_mode()

    def help_set_el_servo_coeff_d(self):
        self.build_help_set_el_servo_coeff('d')
    def do_set_el_servo_coeff_d(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_d(arguments[0])
        else:
            self.help_set_el_servo_coeff_d()

    def help_set_el_servo_coeff_e(self):
        self.build_help_set_el_servo_coeff('e')
    def do_set_el_servo_coeff_e(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_e(arguments[0])
        else:
            self.help_set_el_servo_coeff_e()

    def help_set_el_servo_coeff_f(self):
        self.build_help_set_el_servo_coeff('f')
    def do_set_el_servo_coeff_f(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_f(arguments[0])
        else:
            self.help_set_el_servo_coeff_f()

    def help_set_ip_address(self):
        self.short_help("set_ip_address <ip>,<ip>,<ip>,<ip>,<mask>,<mask>,<mask>,<mask>")

    def help_set_ip_gateway(self):
        self.short_help("set_ip_gateway <nnn>,<nnn>,<nnn>,<nnn>")

    def do_set_ip_address(self,rest):
        arguments = self.parse_rest(rest,4)
        if arguments:
            self.acu.set_ip_address(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5],arguments[6],arguments[7])
        else:
            self.help_set_ip_address()

    def do_set_ip_gateway(self,rest):
        arguments = self.parse_rest(rest,4)
        if arguments:
            self.acu.set_ip_gateway(arguments[0],arguments[1],arguments[2],arguments[3])
        else:
            self.help_set_ip_gateway()
        
    def build_set_pt_model_coeff(self,index):
        print "set_pt_model_coeff_"+index+" <arg1>"
        print "where:"
        print "    <arg1>: coefficient"
        
    def help_set_pt_model_coeff_0(self):
        self.build_help_set_pt_model_coeff('0')
    def do_set_pt_model_coeff_0(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_0(arguments[0])
        else:
            self.help_set_pt_model_coeff_0()

    def help_set_pt_model_coeff_1(self):
        self.build_help_set_pt_model_coeff('1')
    def do_set_pt_model_coeff_1(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_1(arguments[0])
        else:
            self.help_set_pt_model_coeff_1()

    def help_set_pt_model_coeff_2(self):
        self.build_help_set_pt_model_coeff('2')
    def do_set_pt_model_coeff_2(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_2(arguments[0])
        else:
            self.help_set_pt_model_coeff_2()

    def help_set_pt_model_coeff_3(self):
        self.build_help_set_pt_model_coeff('3')
    def do_set_pt_model_coeff_3(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_3(arguments[0])
        else:
            self.help_set_pt_model_coeff_3()

    def help_set_pt_model_coeff_4(self):
        self.build_help_set_pt_model_coeff('4')
    def do_set_pt_model_coeff_4(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_4(arguments[0])
        else:
            self.help_set_pt_model_coeff_4()

    def help_set_pt_model_coeff_5(self):
        self.build_help_set_pt_model_coeff('5')
    def do_set_pt_model_coeff_5(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_5(arguments[0])
        else:
            self.help_set_pt_model_coeff_5()

    def help_set_pt_model_coeff_6(self):
        self.build_help_set_pt_model_coeff('6')
    def do_set_pt_model_coeff_6(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_6(arguments[0])
        else:
            self.help_set_pt_model_coeff_6()

    def help_set_pt_model_coeff_7(self):
        self.build_help_set_pt_model_coeff('7')
    def do_set_pt_model_coeff_7(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_7(arguments[0])
        else:
            self.help_set_pt_model_coeff_7()

    def help_set_pt_model_coeff_8(self):
        self.build_help_set_pt_model_coeff('8')
    def do_set_pt_model_coeff_8(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_8(arguments[0])
        else:
            self.help_set_pt_model_coeff_8()

    def help_set_pt_model_coeff_9(self):
        self.build_help_set_pt_model_coeff('9')
    def do_set_pt_model_coeff_9(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_9(arguments[0])
        else:
            self.help_set_pt_model_coeff_9()

    def help_set_pt_model_coeff_10(self):
        self.build_help_set_pt_model_coeff('a')
    def do_set_pt_model_coeff_10(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_10(arguments[0])
        else:
            self.help_set_pt_model_coeff_10()

    def help_set_pt_model_coeff_11(self):
        self.build_help_set_pt_model_coeff('b')
    def do_set_pt_model_coeff_11(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_11(arguments[0])
        else:
            self.help_set_pt_model_coeff_11()

    def help_set_pt_model_coeff_12(self):
        self.build_help_set_pt_model_coeff('c')
    def do_set_pt_model_coeff_12(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_12(arguments[0])
        else:
            self.help_set_pt_model_coeff_12()

    def help_set_pt_model_coeff_13(self):
        self.build_help_set_pt_model_coeff('d')
    def do_set_pt_model_coeff_13(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_13(arguments[0])
        else:
            self.help_set_pt_model_coeff_13()

    def help_set_pt_model_coeff_14(self):
        self.build_help_set_pt_model_coeff('e')
    def do_set_pt_model_coeff_14(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_14(arguments[0])
        else:
            self.help_set_pt_model_coeff_14()

    def help_set_pt_model_coeff_15(self):
        self.build_help_set_pt_model_coeff('f')
    def do_set_pt_model_coeff_15(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_15(arguments[0])
        else:
            self.help_set_pt_model_coeff_15()

    def help_set_subref_abs_posn(self):
        self.short_help("set_subref_abs_posn <data1>,<data2>,<data3>")
    def do_set_subref_abs_posn(self,rest):
        arguments = self.parse_rest(rest,3)
        if arguments:
            self.acu.set_subref_abs_posn(arguments[0],arguments[1],arguments[2])
        else:
            self.help_set_subref_abs_posn()

    def help_set_subref_delta_posn(self):
        self.short_help("set_subref_delta_posn <data1>,<data2>,<data3>")
    def do_set_subref_delta_posn(self,rest):
        arguments = self.parse_rest(rest,3)
        if arguments:
            self.acu.set_subref_delta_posn(arguments[0],arguments[1],arguments[2])
        else:
            self.help_set_subref_delta_posn()

    def help_subref_delta_zero_cmd(self):
        print "subref_delta_zero_cmd 1"
    def do_subref_delta_zero_cmd(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.subref_delta_zero_cmd(arguments[0])
        else:
            self.help_subref_delta_zero_cmd()

    def help_set_subref_rotation(self):
        self.short_help("set_subref_rotation <data1>,<data2>,<data3>")
    def do_set_subref_rotation(self,rest):
        arguments = self.parse_rest(rest,3)
        if arguments:
            self.acu.set_subref_rotation(arguments[0],arguments[1],arguments[2])
        else:
            self.help_set_subref_rotation()

    def help_set_metr_mode(self):
        self.short_help("set_metr_mode <data>")
    def do_set_metr_mode(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_metr_mode(arguments[0])
        else:
            self.help_set_metr_mode()

    def help_set_metr_coeff_0(self):
        self.short_help("set_metr_coeff_0 <data>")
    def do_set_metr_coeff_0(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_metr_coeff_0(arguments[0])
        else:
            self.help_set_metr_coeff_0()

    def help_set_metr_coeff_1(self):
        self.short_help("set_metr_coeff_1 <data>")
    def do_set_metr_coeff_1(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_metr_coeff_1(arguments[0])
        else:
            self.help_set_metr_coeff_1()

    def help_selftest_cmd(self):
        self.short_help("selftest_cmd <data>")
    def do_selftest_cmd(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.selftest_cmd(arguments[0])
        else:
            self.help_selftest_cmd()

    def help_set_als(self):
        self.short_help("set_als <data>")
    def do_set_als(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_als(arguments[0])
        else:
            self.help_set_als()

    def help_set_rec_cab_temp(self):
        self.short_help("set_rec_cab_temp <data>")
    def do_set_rec_cab_temp(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_rec_cab_temp(arguments[0])
        else:
            self.help_set_rec_cab_temp()

    def help_acu_trk_mode(self):
        self.short_help("acu_trk_mode <data>")
    def do_acu_trk_mode(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.acu_trk_mode(arguments[0])
        else:
            self.help_acu_trk_mode()


       
      
