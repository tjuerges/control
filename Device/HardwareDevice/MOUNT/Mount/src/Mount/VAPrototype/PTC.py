#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

import sys
import string
import time
from CCL.AmbManager import *
import struct
from Mount.Device import Device
from Mount.Common.ACU import ClientException as PTCClientException

from Acspy.Clients.SimpleClient import PySimpleClient
import Control__POA
import Control

class PTC(Device):

    def __init__(self, ambmgr, node, channel):
        # Listed in ICD order
        self.ICD['get_ac_status'] = {'RCA': 0x02, 'format': 'B', 'type': 'monitor'}
        self.ICD['get_can_error'] = {'RCA': 0x30001, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_ip_address'] = {'RCA': 0x14, 'format': '!4B',  'type': 'monitor'}
        for index in range(0,2):
            self.ICD['get_metr_displ_'+str(index)] = {'RCA': 0x6000+index, 'format': '!h',  'type': 'monitor'}
        self.ICD['get_metr_equip_status']  = {'RCA': 0x10, 'format': '!2B', 'type': 'monitor'}
        self.ICD['get_metr_mode']  = {'RCA': 0x11, 'format': '!2B', 'type': 'monitor'}
        for index in range(0,25):
            self.ICD['get_metr_temps_'+str(index)] = {'RCA': 0x4000+index, 'format': '!4h',  'type': 'monitor'}
        for index in range(0,5):
            self.ICD['get_metr_tilt_'+str(index)] = {'RCA': 0x5000+index, 'format': '!3h',  'type': 'monitor'}
        self.ICD['get_num_trans'] = {'RCA': 0x30002, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_other_status'] = {'RCA': 0x3A, 'format': '!B',  'type': 'monitor'}
        self.ICD['get_power_status'] = {'RCA': 0x03, 'format': '!2B',  'type': 'monitor'}
        for index in range(0,10):
            self.ICD['get_pt_model_coeff_'+str(index)] = {'RCA': 0x3000+index, 'format': '!d',  'type': 'monitor'}        
        self.ICD['get_ptc_error'] = {'RCA': 0x15, 'format': '',    'type': 'monitor'}
        self.ICD['get_subref_abs_posn'] = {'RCA': 0x20, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_subref_delta_posn'] = {'RCA': 0x21, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_subref_limits'] = {'RCA': 0x22, 'format': '!8B',    'type': 'monitor'}
        self.ICD['get_subref_rotation'] = {'RCA': 0x24, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_subref_status'] = {'RCA': 0x23, 'format': '!5B',    'type': 'monitor'}
        self.ICD['get_sw_rev_level'] = {'RCA': 0x30000, 'format': '!3B',    'type': 'monitor'}
        self.ICD['get_system_id'] = {'RCA': 0x30004, 'format': '!3B',    'type': 'monitor'}
        self.ICD['get_ups_alarms_1'] = {'RCA': 0x30, 'format': '!4B',    'type': 'monitor'}
        self.ICD['get_ups_battery_output_1'] = {'RCA': 0x31, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_battery_status_1'] = {'RCA': 0x32, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_bypass_volts_1'] = {'RCA': 0x33, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_freqs_1'] = {'RCA': 0x34, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_inverter_sw_1'] = {'RCA': 0x35, 'format': 'B',    'type': 'monitor'}
        self.ICD['get_ups_inverter_volts_1'] = {'RCA': 0x36, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_output_current_1'] = {'RCA': 0x37, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_output_volts_1'] = {'RCA': 0x38, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_status_1'] = {'RCA': 0x39, 'format': 'B',    'type': 'monitor'}
        self.ICD['get_ups_alarms_2'] = {'RCA': 0x40, 'format': '!4B',    'type': 'monitor'}
        self.ICD['get_ups_battery_output_2'] = {'RCA': 0x41, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_battery_status_2'] = {'RCA': 0x42, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_bypass_volts_2'] = {'RCA': 0x43, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_freqs_2'] = {'RCA': 0x44, 'format': '!2h',    'type': 'monitor'}
        self.ICD['get_ups_inverter_sw_2'] = {'RCA': 0x45, 'format': 'B',    'type': 'monitor'}
        self.ICD['get_ups_inverter_volts_2'] = {'RCA': 0x46, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_output_current_2'] = {'RCA': 0x47, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_output_volts_2'] = {'RCA': 0x48, 'format': '!3h',    'type': 'monitor'}
        self.ICD['get_ups_status_2'] = {'RCA': 0x49, 'format': 'B',    'type': 'monitor'}

        self.ICD['clear_fault_cmd'] = {'RCA': 0x1003, 'format': '!B',  'type': 'command'}
        self.ICD['init_subref_cmd'] = {'RCA': 0x1023, 'format': '!B',  'type': 'command'}
        self.ICD['reset_ptc_cmd'] = {'RCA': 0x1001, 'format': '!B',  'type': 'command'}
        self.ICD['reset_ups_cmd_1'] = {'RCA': 0x1002, 'format': '!B',  'type': 'command'}
        self.ICD['reset_ups_cmd_2'] = {'RCA': 0x1012, 'format': '!B',  'type': 'command'}
        self.ICD['set_metr_mode'] = {'RCA': 0x1011, 'format': '!BB',  'type': 'command'}
        for index in range(0,10):
            self.ICD['set_pt_model_coeff_'+str(index)] = {'RCA': 0x2000+index, 'format': '!d',  'type': 'command'}
        self.ICD['set_subref_abs_posn'] = {'RCA': 0x1020, 'format': '!3h',  'type': 'command'}        
        self.ICD['set_subref_delta_posn'] = {'RCA': 0x1021, 'format': '!3h',  'type': 'command'}        
        self.ICD['set_subref_rotation'] = {'RCA': 0x1024, 'format': '!3h',  'type': 'command'}        
        self.ICD['subref_delta_zero_cmd'] = {'RCA': 0x1022, 'format': 'B',  'type': 'command'}        
        self.ICD['ups_battery_test_cmd_1'] = {'RCA': 0x1031, 'format': 'B',  'type': 'command'}        
        self.ICD['ups_battery_test_cmd_2'] = {'RCA': 0x1041, 'format': 'B',  'type': 'command'}        
        Device(ambmgr,node,channel)

class PTCClient:
    def __init__(self,name='DV01', channel=0, node=1):
        self.client = PySimpleClient.getInstance()
        self.mgrName = "CONTROL/"+name+"/AmbManager"
        try:
            self.ambmgr = self.client.getComponent(self.mgrName)
        except:
            msg = 'Manager reference failed for %s.' % (self.mgrName)
            raise PTCClientException(msg)
        if self.ambmgr == None:
            msg = 'Component %s not found.' % (name)
            raise PTCClientException(msg)
        self.ptc = PTC(self.ambmgr,  node, channel)
        
    def __del__(self):
        self.client.releaseComponent(self.mgrName)
        self.client.disconnect()
        
    def get_ac_status(self):
        return self.ptc.execute('get_ac_status')

    def get_can_error(self):
        return self.ptc.execute('get_can_error')
    
    def get_ip_address(self):
        return self.ptc.execute('get_ip_address')

    def get_metr_displ(self,index):
        return self.ptc.execute('get_metr_displ_'+index)

    def get_metr_displ_0(self):
        return self.get_metr_displ('0')

    def get_metr_displ_1(self):
        return self.get_metr_displ('1')

    def get_metr_equip_status(self):
        return self.ptc.execute('get_metr_equip_status')
    
    def get_metr_mode(self):
        return self.ptc.execute('get_metr_mode')
    
    def get_metr_temps(self,index):
        return self.ptc.execute('get_metr_temps_'+index)
    
    def get_metr_temps_0(self):
        return self.get_metr_temps('0')
    
    def get_metr_temps_1(self):
        return self.get_metr_temps('1')
    
    def get_metr_temps_2(self):
        return self.get_metr_temps('2')
    
    def get_metr_temps_3(self):
        return self.get_metr_temps('3')
    
    def get_metr_temps_4(self):
        return self.get_metr_temps('4')
    
    def get_metr_temps_5(self):
        return self.get_metr_temps('5')
    
    def get_metr_temps_6(self):
        return self.get_metr_temps('6')
    
    def get_metr_temps_7(self):
        return self.get_metr_temps('7')

    def get_metr_temps_8(self):
        return self.get_metr_temps('8')
    
    def get_metr_temps_9(self):
        return self.get_metr_temps('9')
    
    def get_metr_temps_10(self):
        return self.get_metr_temps('10')
    
    def get_metr_temps_11(self):
        return self.get_metr_temps('11')
    
    def get_metr_temps_12(self):
        return self.get_metr_temps('12')
    
    def get_metr_temps_13(self):
        return self.get_metr_temps('13')
    
    def get_metr_temps_14(self):
        return self.get_metr_temps('14')
    
    def get_metr_temps_15(self):
        return self.get_metr_temps('15')
    
    def get_metr_temps_16(self):
        return self.get_metr_temps('16')
    
    def get_metr_temps_17(self):
        return self.get_metr_temps('17')
    
    def get_metr_temps_18(self):
        return self.get_metr_temps('18')
    
    def get_metr_temps_19(self):
        return self.get_metr_temps('19')
    
    def get_metr_temps_20(self):
        return self.get_metr_temps('20')
    
    def get_metr_temps_21(self):
        return self.get_metr_temps('21')
    
    def get_metr_temps_22(self):
        return self.get_metr_temps('22')
    
    def get_metr_temps_23(self):
        return self.get_metr_temps('23')
    
    def get_metr_temps_24(self):
        return self.get_metr_temps('24')

    def get_metr_tilt(self,index):
        return self.ptc.execute('get_metr_tilt_'+index)
    
    def get_metr_tilt_0(self):
        return self.get_metr_tilt('0')
    
    def get_metr_tilt_1(self):
        return self.get_metr_tilt('1')
    
    def get_metr_tilt_2(self):
        return self.get_metr_tilt('2')
    
    def get_metr_tilt_3(self):
        return self.get_metr_tilt('3')
    
    def get_metr_tilt_4(self):
        return self.get_metr_tilt('4')
    
    def get_num_trans(self):
        return self.ptc.execute('get_num_trans')

    def get_other_status(self):
        return self.ptc.execute('get_other_status')

    def get_power_status(self):
        return self.ptc.execute('get_power_status')

    def get_pt_model_coeff(self,index):
        return self.ptc.execute('get_pt_model_coeff_'+index)
    
    def get_pt_model_coeff_0(self):
        return self.get_pt_model_coeff('0')
    
    def get_pt_model_coeff_1(self):
        return self.get_pt_model_coeff('1')

    def get_pt_model_coeff_2(self):
        return self.get_pt_model_coeff('2')
    
    def get_pt_model_coeff_3(self):
        return self.get_pt_model_coeff('3')
    
    def get_pt_model_coeff_4(self):
        return self.get_pt_model_coeff('4')
    
    def get_pt_model_coeff_5(self):
        return self.get_pt_model_coeff('5')
    
    def get_pt_model_coeff_6(self):
        return self.get_pt_model_coeff('6')
    
    def get_pt_model_coeff_7(self):
        return self.get_pt_model_coeff('7')
    
    def get_pt_model_coeff_8(self):
        return self.get_pt_model_coeff('8')
    
    def get_pt_model_coeff_9(self):
        return self.get_pt_model_coeff('9')
    
    def get_ptc_error(self):
        return self.ptc.execute('get_ptc_error')

    def get_subref_abs_posn(self):
        return self.ptc.execute('get_subref_abs_posn')

    def get_subref_delta_posn(self):
        return self.ptc.execute('get_subref_delta_posn')

    def get_subref_limits(self):
        return self.ptc.execute('get_subref_limits')

    def get_subref_rotation(self):
        return self.ptc.execute('get_subref_rotation')

    def get_subref_status(self):
        return self.ptc.execute('get_subref_status')

    def get_sw_rev_level(self):
        return self.ptc.execute('get_sw_rev_level')

    def get_system_id(self): 
        return self.ptc.execute('get_system_id')

    def get_ups_alarms_1(self):
        return self.ptc.execute('get_ups_alarms_1')

    def get_ups_battery_output_1(self):
        return self.ptc.execute('get_ups_battery_output_1')

    def get_ups_battery_status_1(self):
        return self.ptc.execute('get_ups_battery_status_1')

    def get_ups_bypass_volts_1(self):
        return self.ptc.execute('get_ups_bypass_volts_1')

    def get_ups_freqs_1(self):
        return self.ptc.execute('get_ups_freqs_1')
    
    def get_ups_inverter_sw_1(self):
        return self.ptc.execute('get_ups_inverter_sw_1')

    def get_ups_inverter_volts_1(self):
        return self.ptc.execute('get_ups_inverter_volts_1')

    def get_ups_output_current_1(self):
        return self.ptc.execute('get_ups_output_current_1')

    def get_ups_output_volts_1(self):
        return self.ptc.execute('get_ups_output_volts_1')

    def get_ups_status_1(self):
        return self.ptc.execute('get_ups_status_1')

    def get_ups_alarms_2(self):
        return self.ptc.execute('get_ups_alarms_2')

    def get_ups_battery_output_2(self):
        return self.ptc.execute('get_ups_battery_output_2')

    def get_ups_battery_status_2(self):
        return self.ptc.execute('get_ups_battery_status_2')

    def get_ups_bypass_volts_2(self):
        return self.ptc.execute('get_ups_bypass_volts_2')

    def get_ups_freqs_2(self):
        return self.ptc.execute('get_ups_freqs_2')

    def get_ups_inverter_sw_2(self):
        return self.ptc.execute('get_ups_inverter_sw_2')

    def get_ups_inverter_volts_2(self):
        return self.ptc.execute('get_ups_inverter_volts_2')

    def get_ups_output_current_2(self):
        return self.ptc.execute('get_ups_output_current_2')

    def get_ups_output_volts_2(self):
        return self.ptc.execute('get_ups_output_volts_2')

    def get_ups_status_2(self):
        return self.ptc.execute('get_ups_status_2')

    def clear_fault_cmd(self,data):
        self.ptc.execute('clear_fault_cmd',data)
        return

    def init_subref_cmd(self,data):
        self.ptc.execute('init_subref_cmd',data)
        return

    def reset_ptc_cmd(self,data):
        self.ptc.execute('reset_ptc_cmd',data)
        return

    def reset_ups_cmd_1(self,data):
        self.ptc.execute('reset_ups_cmd_1',data)
        return

    def reset_ups_cmd_2(self,data):
        self.ptc.execute('reset_ups_cmd_2',data)
        return
  
    def set_metr_mode(self,data1,data2):
        self.ptc.execute('set_metr_mode',data1,data2)
        return

    def set_pt_model_coeff(self,index,data):
        self.ptc.execute('set_pt_model_coeff_'+index,data)
        return

    def set_pt_model_coeff_0(self,data):
        self.set_pt_model_coeff('0',data)
        return

    def set_pt_model_coeff_1(self,data):
        self.set_pt_model_coeff('1',data)
        return

    def set_pt_model_coeff_2(self,data):
        self.set_pt_model_coeff('2',data)
        return

    def set_pt_model_coeff_3(self,data):
        self.set_pt_model_coeff('3',data)
        return

    def set_pt_model_coeff_4(self,data):
        self.set_pt_model_coeff('4',data)
        return

    def set_pt_model_coeff_5(self,data):
        self.set_pt_model_coeff('5',data)
        return

    def set_pt_model_coeff_6(self,data):
        self.set_pt_model_coeff('6',data)
        return

    def set_pt_model_coeff_7(self,data):
        self.set_pt_model_coeff('7',data)
        return

    def set_pt_model_coeff_8(self,data):
        self.set_pt_model_coeff('8',data)
        return

    def set_pt_model_coeff_9(self,data):
        self.set_pt_model_coeff('9',data)
        return
    
    def set_subref_abs_posn(self,data1,data2,data3):
        self.ptc.execute('set_subref_abs_posn',data1,data2,data3)
        return
    
    def set_subref_delta_posn(self,data1,data2,data3):
        self.ptc.execute('set_subref_delta_posn',data1,data2,data3)
        return
    
    def set_subref_rotation(self,data1,data2,data3):
        self.ptc.execute('set_subref_rotation',data1,data2,data3)
        return
    
    def subref_delta_zero_cmd(self,data):
        self.ptc.execute('subref_delta_zero_cmd',data)
        return

    def ups_battery_test_cmd_1(self,data):
        self.ptc.execute('ups_battery_test_cmd_1',data)
        return

    def ups_battery_test_cmd_2(self,data):
        self.ptc.execute('ups_battery_test_cmd_2',data)
        return

    
