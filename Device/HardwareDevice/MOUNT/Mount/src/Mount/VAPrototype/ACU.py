#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

from Mount.Common.ACU import ACU as ACUCommon

class ACU(ACUCommon):

    def __init__(self, ambmgr, node, channel):
        # Listed in ICD order
        
        # Monitor points
        #self.ICD['acu_mode_rsp'] = {'RCA': 0x22, 'format': '!2B', 'type': 'monitor'}
        #self.ICD['az_posn_rsp']  = {'RCA': 0x12, 'format': '!2l', 'type': 'monitor'}
        #self.ICD['el_posn_rsp']  = {'RCA': 0x02, 'format': '!2l', 'type': 'monitor'}
        #self.ICD['get_acu_error'] = {'RCA': 0x2F, 'format': '',    'type': 'monitor'}
        self.ICD['get_az_aux_mode'] = {'RCA': 0x16, 'format': '!B',  'type': 'monitor'}
        #self.ICD['get_az_brake'] = {'RCA': 0x14, 'format': '!B',  'type': 'monitor'}
        #self.ICD['get_az_enc']   = {'RCA': 0x17, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_az_enc_status'] = {'RCA': 0x18, 'format': '!B',  'type': 'monitor'}
        self.ICD['get_az_motor_currents'] = {'RCA': 0x19, 'format': '!2B',  'type': 'monitor'}
        self.ICD['get_az_motor_temps'] = {'RCA': 0x1A, 'format': '!2B',  'type': 'monitor'}
        self.ICD['get_az_motor_torque'] = {'RCA': 0x15, 'format': '!2B',  'type': 'monitor'}
        #for index in range(0,13):
        #    self.ICD['get_az_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x3020+index, 'format': '!d',  'type': 'monitor'}
        #self.ICD['get_az_status'] = {'RCA': 0x1B, 'format': '!8B',  'type': 'monitor'}
        #self.ICD['get_can_error'] = {'RCA': 0x30001, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_el_aux_mode'] = {'RCA': 0x06, 'format': 'B',  'type': 'monitor'}
        #self.ICD['get_el_brake'] = {'RCA': 0x04, 'format': 'B',  'type': 'monitor'}
        self.ICD['get_el_enc'] = {'RCA': 0x07, 'format': '!2L',  'type': 'monitor'}
        self.ICD['get_el_enc_status'] = {'RCA': 0x08, 'format': '!3B',  'type': 'monitor'}
        self.ICD['get_el_motor_currents'] = {'RCA': 0x09, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_el_motor_temps'] = {'RCA': 0x0A, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_el_motor_torque'] = {'RCA': 0x05, 'format': '!4B',  'type': 'monitor'}
        #for index in range(0,13):
        #    self.ICD['get_el_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x3010+index, 'format': '!d',  'type': 'monitor'}
        #self.ICD['get_el_status'] = {'RCA': 0x0B, 'format': '!8B',  'type': 'monitor'}
        #self.ICD['get_idle_stow_time'] = {'RCA': 0x25, 'format': '!H',  'type': 'monitor'}
        #self.ICD['get_ip_address'] = {'RCA': 0x2D, 'format': '!4B',  'type': 'monitor'}
        #self.ICD['get_num_trans'] = {'RCA': 0x30002, 'format': '!L',  'type': 'monitor'}
        #self.ICD['get_shutter'] = {'RCA': 0x2E, 'format': 'B',  'type': 'monitor'}
        #self.ICD['get_stow_pin'] = {'RCA': 0x24, 'format': '!2B',  'type': 'monitor'}
        #self.ICD['get_sw_rev_level'] = {'RCA': 0x30000, 'format': '!3B',  'type': 'monitor'}
        self.ICD['get_system_id'] = {'RCA': 0x30004, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_system_status'] = {'RCA': 0x23, 'format': '!3B',  'type': 'monitor'}

        # Control points
        #self.ICD['acu_mode_cmd'] = {'RCA': 0x1022, 'format': 'B',  'type': 'command'}
        #self.ICD['az_traj_cmd'] = {'RCA': 0x1012, 'format': '!ll',  'type': 'command'}
        #self.ICD['clear_fault_cmd'] = {'RCA': 0x1021, 'format': '!B',  'type': 'command'}
        #self.ICD['el_traj_cmd'] = {'RCA': 0x1002, 'format': '!ll',  'type': 'command'}
        #self.ICD['reset_acu_cmd'] = {'RCA': 0x102F, 'format': '!B',  'type': 'command'}
        self.ICD['set_az_aux_mode'] = {'RCA': 0x1016, 'format': 'B',  'type': 'command'}
        #self.ICD['set_az_brake'] = {'RCA': 0x1014, 'format': 'B',  'type': 'command'}
        #for index in range(0,13):
        #    self.ICD['set_az_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x2020+index, 'format': '!d',  'type': 'command'}        
        #self.ICD['set_az_servo_default'] = {'RCA': 0x1017, 'format': 'B',  'type': 'command'}
        self.ICD['set_el_aux_mode'] = {'RCA': 0x1006, 'format': 'B',  'type': 'command'}
        #self.ICD['set_el_brake'] = {'RCA': 0x1004, 'format': 'B',  'type': 'command'}
        #for index in range(0,13):
        #    self.ICD['set_el_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x2010+index, 'format': '!d',  'type': 'command'}        
        #self.ICD['set_el_servo_default'] = {'RCA': 0x1007, 'format': 'B',  'type': 'command'}
        #self.ICD['set_idle_stow_time'] = {'RCA': 0x1025, 'format': '!H',  'type': 'command'}
        #self.ICD['set_shutter'] = {'RCA': 0x102E, 'format': '!B',  'type': 'command'}
        #self.ICD['set_stow_pin'] = {'RCA': 0x102D, 'format': '!2B',  'type': 'command'}        

        ACUCommon(ambmgr,node,channel)

    def acu_mode_rsp(self):
        m1,m2 =  self.execute('acu_mode_rsp')
        maz = m1 & 0xf
        mel = m1 & 0xf0
        mel = mel >> 4
        return (maz,mel,m2)

    def az_posn_rsp(self):
        return self.execute('az_posn_rsp')

    def el_posn_rsp(self):
        return self.execute('el_posn_rsp')

    def get_acu_error(self):
        return self.execute('get_acu_error')

    def get_az_aux_mode(self):
        return self.execute('get_az_aux_mode')

    def get_az_brake(self):
        return self.execute('get_az_brake')

    def get_az_enc(self):
        return self.execute('get_az_enc')

    def get_az_enc_status(self):
        return self.execute('get_az_enc_status')

    def get_az_motor_currents(self):
        return self.execute('get_az_motor_currents')

    def get_az_motor_temps(self):
        return self.execute('get_az_motor_temps')

    def get_az_motor_torque(self):
        return self.execute('get_az_motor_torque')

    def get_az_servo_coeff_0(self):
        return self.execute('get_az_servo_coeff_0')

    def get_az_servo_coeff_1(self):
        return self.execute('get_az_servo_coeff_1')
    
    def get_az_servo_coeff_2(self):
        return self.execute('get_az_servo_coeff_2')
    
    def get_az_servo_coeff_3(self):
        return self.execute('get_az_servo_coeff_3')
    
    def get_az_servo_coeff_4(self):
        return self.execute('get_az_servo_coeff_4')
    
    def get_az_servo_coeff_5(self):
        return self.execute('get_az_servo_coeff_5')
    
    def get_az_servo_coeff_6(self):
        return self.execute('get_az_servo_coeff_6')
    
    def get_az_servo_coeff_7(self):
        return self.execute('get_az_servo_coeff_7')
    
    def get_az_servo_coeff_8(self):
        return self.execute('get_az_servo_coeff_8')
    
    def get_az_servo_coeff_9(self):
        return self.execute('get_az_servo_coeff_9')
    
    def get_az_servo_coeff_a(self):
        return self.execute('get_az_servo_coeff_a')
    
    def get_az_servo_coeff_b(self):
        return self.execute('get_az_servo_coeff_b')
    
    def get_az_servo_coeff_c(self):
        return self.execute('get_az_servo_coeff_c')

    def get_az_status(self):
        return self.execute('get_az_status')
    
    def get_can_error(self):
        return self.execute('get_can_error')

    def get_el_aux_mode(self):
        return self.execute('get_el_aux_mode')

    def get_el_brake(self):
        return self.execute('get_el_brake')

    def get_el_enc(self):
        return self.execute('get_el_enc')

    def get_el_enc_status(self):
        return self.execute('get_el_enc_status')

    def get_el_motor_currents(self):
        return self.execute('get_el_motor_currents')

    def get_el_motor_temps(self):
        return self.execute('get_el_motor_temps')

    def get_el_motor_torque(self):
        return self.execute('get_el_motor_torque')

    def get_el_servo_coeff_0(self):
        return self.execute('get_el_servo_coeff_0')

    def get_el_servo_coeff_1(self):
        return self.execute('get_el_servo_coeff_1')
    
    def get_el_servo_coeff_2(self):
        return self.execute('get_el_servo_coeff_2')
    
    def get_el_servo_coeff_3(self):
        return self.execute('get_el_servo_coeff_3')
    
    def get_el_servo_coeff_4(self):
        return self.execute('get_el_servo_coeff_4')
    
    def get_el_servo_coeff_5(self):
        return self.execute('get_el_servo_coeff_5')
    
    def get_el_servo_coeff_6(self):
        return self.execute('get_el_servo_coeff_6')
    
    def get_el_servo_coeff_7(self):
        return self.execute('get_el_servo_coeff_7')
    
    def get_el_servo_coeff_8(self):
        return self.execute('get_el_servo_coeff_8')
    
    def get_el_servo_coeff_9(self):
        return self.execute('get_el_servo_coeff_9')
    
    def get_el_servo_coeff_a(self):
        return self.execute('get_el_servo_coeff_a')
    
    def get_el_servo_coeff_b(self):
        return self.execute('get_el_servo_coeff_b')
    
    def get_el_servo_coeff_c(self):
        return self.execute('get_el_servo_coeff_c')

    def get_el_status(self):
        return self.execute('get_el_status')

    def get_idle_stow_time(self):
        return self.execute('get_idle_stow_time')

    def get_ip_address(self):
        return self.execute('get_ip_address')

    def get_num_trans(self):
        return self.execute('get_num_trans')

    def get_shutter(self):
        return self.execute('get_shutter')

    def get_stow_pin(self):
        return self.execute('get_stow_pin')

    def get_sw_rev_level(self):
        return self.execute('get_sw_rev_level')

    def get_system_id(self):
        return self.execute('get_system_id')

    def get_system_status(self):
        return self.execute('get_system_status')

    def acu_mode_cmd(self,data):
        self.execute('acu_mode_cmd',data)
        return

    def az_traj_cmd(self,data1, data2):
        self.execute('az_traj_cmd',data1,data2)
        return
    
    def clear_fault_cmd(self,data):
        self.execute('clear_fault_cmd',data)
        return
    
    def el_traj_cmd(self,data1,data2):
        self.execute('el_traj_cmd',data1,data2)
        return

    def reset_acu_cmd(self,data):
        self.execute('reset_acu_cmd',data)
        return

    def set_az_aux_mode(self,data):
        self.execute('set_az_aux_mode',data)
        return

    def set_az_brake(self,data):
        self.execute('set_az_brake',data)
        return

    def set_servo_coeff(self,axis,index,data):
        self.execute('set_'+axis+'_servo_coeff_'+index,data)

    def set_az_servo_coeff(self,index,data):
        self.set_servo_coeff('az',index,data)
        return
    
    def set_az_servo_coeff_0(self,data):
        self.set_az_servo_coeff('0',data)
        return

    def set_az_servo_coeff_1(self,data):
        self.set_az_servo_coeff('1',data)
        return

    def set_az_servo_coeff_2(self,data):
        self.set_az_servo_coeff('2',data)
        return

    def set_az_servo_coeff_3(self,data):
        self.set_az_servo_coeff('3',data)
        return

    def set_az_servo_coeff_4(self,data):
        self.set_az_servo_coeff('4',data)
        return

    def set_az_servo_coeff_5(self,data):
        self.set_az_servo_coeff('5',data)
        return

    def set_az_servo_coeff_6(self,data):
        self.set_az_servo_coeff('6',data)
        return

    def set_az_servo_coeff_7(self,data):
        self.set_az_servo_coeff('7',data)
        return

    def set_az_servo_coeff_8(self,data):
        self.set_az_servo_coeff('8',data)
        return

    def set_az_servo_coeff_9(self,data):
        self.set_az_servo_coeff('9',data)
        return

    def set_az_servo_coeff_a(self,data):
        self.set_az_servo_coeff('a',data)
        return

    def set_az_servo_coeff_b(self,data):
        self.set_az_servo_coeff('b',data)
        return

    def set_az_servo_coeff_c(self,data):
        self.set_az_servo_coeff('c',data)
        return

    def set_az_servo_default(self,data):
        self.execute('set_az_servo_default',data)
        return

    def set_el_aux_mode(self,data):
        self.execute('set_el_aux_mode',data)
        return
    
    def set_el_brake(self,data):
        self.execute('set_el_brake',data)
        return

    def set_el_servo_coeff(self,index,data):
        self.set_servo_coeff('el',index,data)
        
    def set_el_servo_coeff_0(self,data):
        self.set_el_servo_coeff('0',data)
        return
    
    def set_el_servo_coeff_1(self,data):
        self.set_el_servo_coeff('1',data)
        return
    
    def set_el_servo_coeff_2(self,data):
        self.set_el_servo_coeff('2',data)
        return
    
    def set_el_servo_coeff_3(self,data):
        self.set_el_servo_coeff('3',data)
        return
    
    def set_el_servo_coeff_4(self,data):
        self.set_el_servo_coeff('4',data)
        return
    
    def set_el_servo_coeff_5(self,data):
        self.set_el_servo_coeff('5',data)
        return
    
    def set_el_servo_coeff_6(self,data):
        self.set_el_servo_coeff('6',data)
        return
    
    def set_el_servo_coeff_7(self,data):
        self.set_el_servo_coeff('7',data)
        return
    
    def set_el_servo_coeff_8(self,data):
        self.set_el_servo_coeff('8',data)
        return
    
    def set_el_servo_coeff_9(self,data):
        self.set_el_servo_coeff('9',data)
        return
    
    def set_el_servo_coeff_a(self,data):
        self.set_el_servo_coeff('a',data)
        return
    
    def set_el_servo_coeff_b(self,data):
        self.set_el_servo_coeff('b',data)
        return
    
    def set_el_servo_coeff_c(self,data):
        self.set_el_servo_coeff('c',data)
        return
    
    def set_el_servo_default(self,data):
        self.execute('set_el_servo_default',data)
        return
    
    def set_idle_stow_time(self,data):
        self.execute('set_idle_stow_time',data)
        return
    
    def set_shutter(self,data):
        self.execute('set_shutter',data)
        return
    
    def set_stow_pin(self,data1,data2):
        self.execute('set_stow_pin',data1,data2)
        return 
