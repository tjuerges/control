#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

from Mount.Common.Cmd  import CommonCmd, ACUCommonCmd
from Mount.AECPrototype.ACU import ACU

class ACUCmd(ACUCommonCmd):
    prompt = "ALCATEL ACU> "

    def preloop(self):
        self.acu = ACU(self.acuClient.ambmgr,self.node,self.channel)

    # Command definitions
    def do_get_az_motor(self,rest):
        print self.acu.get_az_motor()
    def do_get_az_servo_coeff_d(self,rest):
        print self.acu.get_az_servo_coeff_d()
    def do_get_az_servo_coeff_e(self,rest):
        print self.acu.get_az_servo_coeff_e()
    def do_get_az_servo_coeff_f(self,rest):
        print self.acu.get_az_servo_coeff_f()
    def do_get_az_motion_lim(self,rest):
        print self.acu.get_az_motion_lim()
    def do_get_el_motor(self,rest):
        print self.acu.get_el_motor()
    def do_get_el_servo_coeff_d(self,rest):
        print self.acu.get_el_servo_coeff_d()
    def do_get_el_servo_coeff_e(self,rest):
        print self.acu.get_el_servo_coeff_e()
    def do_get_el_servo_coeff_f(self,rest):
        print self.acu.get_el_servo_coeff_f()
    def do_get_el_motion_lim(self,rest):
        print self.acu.get_el_motion_lim()
    def do_get_pt_model_coeff_0(self,rest):
        print self.acu.get_pt_model_coeff_0()
    def do_get_pt_model_coeff_1(self,rest):
        print self.acu.get_pt_model_coeff_1()
    def do_get_pt_model_coeff_2(self,rest):
        print self.acu.get_pt_model_coeff_2()
    def do_get_pt_model_coeff_3(self,rest):
        print self.acu.get_pt_model_coeff_3()
    def do_get_pt_model_coeff_4(self,rest):
        print self.acu.get_pt_model_coeff_4()
    def do_get_pt_model_coeff_5(self,rest):
        print self.acu.get_pt_model_coeff_5()
    def do_get_pt_model_coeff_6(self,rest):
        print self.acu.get_pt_model_coeff_6()
    def do_get_pt_model_coeff_7(self,rest):
        print self.acu.get_pt_model_coeff_7()
    def do_get_pt_model_coeff_8(self,rest):
        print self.acu.get_pt_model_coeff_8()
    def do_get_pt_model_coeff_9(self,rest):
        print self.acu.get_pt_model_coeff_9()
    def do_get_subref_abs_posn(self,rest):
        print self.acu.get_subref_abs_posn()
    def do_get_subref_delta_posn(self,rest):
        print self.acu.get_subref_delta_posn()
    def do_get_subref_limits(self,rest):
        status = self.acu.get_subref_limits()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)
    def do_get_subref_status(self,rest):
        status = self.acu.get_subref_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)
    def do_get_metr_mode(self,rest):
        status = self.acu.get_metr_mode()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)
    def do_get_metr_tilt_0(self,rest):
        print self.acu.get_metr_tilt_0()
    def do_get_metr_tilt_1(self,rest):
        print self.acu.get_metr_tilt_1()
    def do_get_metr_tilt_2(self,rest):
        print self.acu.get_metr_tilt_2()
    def do_get_metr_temps_0(self,rest):
        print self.acu.get_metr_temps_0()
    def do_get_metr_temps_1(self,rest):
        print self.acu.get_metr_temps_1()
    def do_get_metr_temps_2(self,rest):
        print self.acu.get_metr_temps_2()
    def do_get_metr_temps_3(self,rest):
        print self.acu.get_metr_temps_3()
    def do_get_metr_temps_4(self,rest):
        print self.acu.get_metr_temps_4()
    def do_get_metr_temps_5(self,rest):
        print self.acu.get_metr_temps_5()
    def do_get_metr_temps_6(self,rest):
        print self.acu.get_metr_temps_6()
    def do_get_metr_temps_7(self,rest):
        print self.acu.get_metr_temps_7()
    def do_get_metr_temps_8(self,rest):
        print self.acu.get_metr_temps_8()
    def do_get_metr_temps_9(self,rest):
        print self.acu.get_metr_temps_9()
    def do_get_metr_temps_10(self,rest):
        print self.acu.get_metr_temps_10()
    def do_get_metr_temps_11(self,rest):
        print self.acu.get_metr_temps_11()
    def do_get_metr_temps_12(self,rest):
        print self.acu.get_metr_temps_12()
    def do_get_metr_temps_13(self,rest):
        print self.acu.get_metr_temps_13()
    def do_get_metr_temps_14(self,rest):
        print self.acu.get_metr_temps_14()
    def do_get_metr_temps_15(self,rest):
        print self.acu.get_metr_temps_15()
    def do_get_metr_temps_16(self,rest):
        print self.acu.get_metr_temps_16()
    def do_get_metr_temps_17(self,rest):
        print self.acu.get_metr_temps_17()
    def do_get_metr_temps_18(self,rest):
        print self.acu.get_metr_temps_18()
    def do_get_metr_temps_19(self,rest):
        print self.acu.get_metr_temps_19()
    def do_get_metr_temps_20(self,rest):
        print self.acu.get_metr_temps_20()
    def do_get_metr_temps_21(self,rest):
        print self.acu.get_metr_temps_21()
    def do_get_metr_temps_22(self,rest):
        print self.acu.get_metr_temps_22()
    def do_get_metr_temps_23(self,rest):
        print self.acu.get_metr_temps_23()
    def do_get_metr_temps_24(self,rest):
        print self.acu.get_metr_temps_24()
    def do_get_metr_temps_25(self,rest):
        print self.acu.get_metr_temps_25()
    def do_get_metr_deltas(self,rest):
        print self.acu.get_metr_deltas()
    def do_get_power_status(self,rest):
        status = self.acu.get_power_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)
    def do_get_ac_status(self,rest):
        status = self.acu.get_ac_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)

    def help_set_az_motor_power(self):
        self.short_help("set_az_motor_power 0|1")
    def do_set_az_motor_power(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_motor_power(arguments[0])
        else:
            self.help_set_az_motor_power()

    def help_set_az_motor_driver(self):
        self.short_help("set_az_motor_driver 0|1")
    def do_set_az_motor_driver(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_motor_driver(arguments[0])
        else:
            self.help_set_az_motor_driver()
        
    def help_set_az_servo_coeff_d(self):
        self.build_help_set_az_servo_coeff('d')
    def do_set_az_servo_coeff_d(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_d(arguments[0])
        else:
            self.help_set_az_servo_coeff_d()

    def help_set_az_servo_coeff_e(self):
        self.build_help_set_az_servo_coeff('e')
    def do_set_az_servo_coeff_e(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_e(arguments[0])
        else:
            self.help_set_az_servo_coeff_e()

    def help_set_az_servo_coeff_f(self):
        self.build_help_set_az_servo_coeff('f')
    def do_set_az_servo_coeff_f(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_f(arguments[0])
        else:
            self.help_set_az_servo_coeff_f()

    def help_set_az_motion_lim(self):
        self.short_help("set_az_motion_lim [0:600],[0:2400]")
    def do_set_az_motion_lim(self,rest):
        arguments = self.parse_rest(rest,2)
        if arguments:
            self.acu.set_az_motion_lim(arguments[0],arguments[1])
        else:
            self.help_set_az_motion_lim()
        
    def help_init_az_enc_abs_pos(self):
        self.short_help("init_az_enc_abs_pos 1")
    def do_init_az_enc_abs_pos(self):
        arguments = self.parse_rest(rest)
        if arguments:
            if arguments[0] == 1:
                self.acu.init_az_enc_abs_pos(arguments[0])
            else:
                self.message("init_az_enc_abs_pos ignored. Invalid argument: "+rest)
                self.help_init_az_enc_abs_pos()
        else:
            self.help_init_az_enc_abs_pos()
        
    def help_set_el_motor_power(self):
        self.short_help("set_el_motor_power 0|1")
    def do_set_el_motor_power(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_motor_power(arguments[0])
        else:
            self.help_set_el_motor_power()

    def help_set_el_motor_driver(self):
        self.short_help("set_el_motor_driver 0|1")
    def do_set_el_motor_driver(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_motor_driver(arguments[0])
        else:
            self.help_set_el_motor_driver()
        

    def help_set_el_servo_coeff_d(self):
        self.build_help_set_el_servo_coeff('d')
    def do_set_el_servo_coeff_d(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_d(arguments[0])
        else:
            self.help_set_el_servo_coeff_d()

    def help_set_el_servo_coeff_e(self):
        self.build_help_set_el_servo_coeff('e')
    def do_set_el_servo_coeff_e(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_e(arguments[0])
        else:
            self.help_set_el_servo_coeff_e()

    def help_set_el_servo_coeff_f(self):
        self.build_help_set_el_servo_coeff('f')
    def do_set_el_servo_coeff_f(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_f(arguments[0])
        else:
            self.help_set_el_servo_coeff_f()

    def help_set_el_motion_lim(self):
        self.short_help("set_el_motion_lim [0:300],[0:1200]")
    def do_set_el_motion_lim(self,rest):
        arguments = self.parse_rest(rest,2)
        if arguments:
            self.acu.set_el_motion_lim(arguments[0],arguments[1])
        else:
            self.help_set_el_motion_lim()
        
    def help_init_el_enc_abs_pos(self):
        self.short_help("init_el_enc_abs_pos 1")
    def do_init_el_enc_abs_pos(self):
        arguments = self.parse_rest(rest)
        if arguments:
            if arguments[0] == 1:
                self.acu.init_el_enc_abs_pos(arguments[0])
            else:
                self.message("init_el_enc_abs_pos ignored. Invalid argument: "+rest)
                self.help_init_el_enc_abs_pos()
        else:
            self.help_init_el_enc_abs_pos()

    def help_set_ip_address(self):
        self.short_help("set_ip_address <nnn>,<nnn>,<nnn>,<nnn>")
    def do_set_ip_address(self,rest):
        arguments = self.parse_rest(rest,4)
        if arguments:
            self.acu.set_ip_address(arguments[0],arguments[1],arguments[2],arguments[3])
        else:
            self.help_set_ip_address()
        
    def build_set_pt_model_coeff(self,index):
        print "set_pt_model_coeff_"+index+" <arg1>"
        print "where:"
        print "    <arg1>: coefficient"
        
    def help_set_pt_model_coeff_0(self):
        self.build_help_set_pt_model_coeff('0')
    def do_set_pt_model_coeff_0(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_0(arguments[0])
        else:
            self.help_set_pt_model_coeff_0()

    def help_set_pt_model_coeff_1(self):
        self.build_help_set_pt_model_coeff('1')
    def do_set_pt_model_coeff_1(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_1(arguments[0])
        else:
            self.help_set_pt_model_coeff_1()

    def help_set_pt_model_coeff_2(self):
        self.build_help_set_pt_model_coeff('2')
    def do_set_pt_model_coeff_2(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_2(arguments[0])
        else:
            self.help_set_pt_model_coeff_2()

    def help_set_pt_model_coeff_3(self):
        self.build_help_set_pt_model_coeff('3')
    def do_set_pt_model_coeff_3(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_3(arguments[0])
        else:
            self.help_set_pt_model_coeff_3()

    def help_set_pt_model_coeff_4(self):
        self.build_help_set_pt_model_coeff('4')
    def do_set_pt_model_coeff_4(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_4(arguments[0])
        else:
            self.help_set_pt_model_coeff_4()

    def help_set_pt_model_coeff_5(self):
        self.build_help_set_pt_model_coeff('5')
    def do_set_pt_model_coeff_5(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_5(arguments[0])
        else:
            self.help_set_pt_model_coeff_5()

    def help_set_pt_model_coeff_6(self):
        self.build_help_set_pt_model_coeff('6')
    def do_set_pt_model_coeff_6(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_6(arguments[0])
        else:
            self.help_set_pt_model_coeff_6()

    def help_set_pt_model_coeff_7(self):
        self.build_help_set_pt_model_coeff('7')
    def do_set_pt_model_coeff_7(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_7(arguments[0])
        else:
            self.help_set_pt_model_coeff_7()

    def help_set_pt_model_coeff_8(self):
        self.build_help_set_pt_model_coeff('8')
    def do_set_pt_model_coeff_8(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_8(arguments[0])
        else:
            self.help_set_pt_model_coeff_8()

    def help_set_pt_model_coeff_9(self):
        self.build_help_set_pt_model_coeff('9')
    def do_set_pt_model_coeff_9(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_pt_model_coeff_9(arguments[0])
        else:
            self.help_set_pt_model_coeff_9()

    def help_set_subref_abs_posn(self):
        self.short_help("set_subref_abs_posn <x>,<y>,<z>")
    def do_set_subref_abs_posn(self,rest):
        arguments = self.parse_rest(rest,3)
        if arguments:
            self.acu.set_subref_abs_posn(arguments[0],arguments[1],arguments[2])
        else:
            self.help_set_subref_abs_posn()

    def help_set_subref_delta_posn(self):
        self.short_help("set_subref_delta_posn <x>,<y>,<z>")
    def do_set_subref_delta_posn(self,rest):
        arguments = self.parse_rest(rest,3)
        if arguments:
            self.acu.set_subref_delta_posn(arguments[0],arguments[1],arguments[2])
        else:
            self.help_set_subref_delta_posn()

    def help_subref_delta_zero_cmd(self):
        self.short_help("subref_delta_zero_cmd 1")
    def do_subref_delta_zero_cmd(self):
        arguments = self.parse_rest(rest)
        if arguments:
            if arguments[0] == 1:
                self.acu.subref_delta_zero_cmd(arguments[0])
            else:
                self.message("subref_delta_zero_cmd ignored. Invalid argument: "+rest)
                self.help_subref_delta_zero_cmd()
        else:
            self.help_subref_delta_zero_cmd()

    def help_set_metr_mode(self):
        self.short_help("set_metr_mode <0|1>,<0|1>,<0|1>")
    def do_set_metr_mode(self,rest):
        arguments = self.parse_rest(rest,3)
        if arguments:
            byte = arguments[0]|arguments[1]|arguments[2]
            self.acu.set_metr_mode(byte)
        else:
            self.help_set_metr_mode()

    def help_set_air_conditioning(self):
        self.short_help("set_air_conditioning <0|1>")
    def do_set_air_conditioning(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_air_conditioning(arguments[0])
        else:
            self.help_set_air_conditioning()
