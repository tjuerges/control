#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
Mount component common test cases.
'''
import unittest
import time
from PyUCommonTestCases import *
import Control
import ControlExceptions
import MountError

class CommonTestCase(PyUCommonTestCase):
    def __init__(self,
                 methodName='runTest',
                 componentName=None):
        '''
        '''
        self.MODES = {0: Control.Mount.SHUTDOWN_MODE,
                      1: Control.Mount.STANDBY_MODE,
                      2: Control.Mount.ENCODER_MODE,
                      3: Control.Mount.AUTONOMOUS_MODE,
                      4: Control.Mount.SURVIVAL_STOW_MODE,
                      5: Control.Mount.MAINTENANCE_STOW_MODE}
               
        self.NUMERIC_MODES = {Control.Mount.SHUTDOWN_MODE:        0,
                              Control.Mount.STANDBY_MODE:         1,
                              Control.Mount.ENCODER_MODE:         2,
                              Control.Mount.AUTONOMOUS_MODE:      3,
                              Control.Mount.SURVIVAL_STOW_MODE:   4,
                              Control.Mount.MAINTENANCE_STOW_MODE:5}
               
        self.TRANSITIONS = [[ 0, 1, 0, 0, 0, 0 ],       
                            [ 0, 1, 2, 3, 0, 0 ],         
                            [ 0, 1, 2, 2, 0, 0 ],        
                            [ 0, 1, 3, 3, 0, 0 ]]
        
        PyUCommonTestCase.__init__(self,methodName,componentName)

    def HwLifeCycleUp(self):
        self.hwState = self.reference.getHwState()
        self.assertEqual(self.hwState,Control.HardwareDevice.Start,
                         "Incorrect HW State: "+str(self.hwState))
        self.reference.hwConfigure()
        self.hwState = self.reference.getHwState()
        self.assertEqual(self.hwState,Control.HardwareDevice.Configure,
                         "Incorrect HW State: "+str(self.hwState))
        self.reference.hwInitialize()
        self.hwState = self.reference.getHwState()
        self.assertEqual(self.hwState,Control.HardwareDevice.Initialize,
                         "Incorrect HW State: "+str(self.hwState))
        self.reference.hwOperational()
        self.hwState = self.reference.getHwState()
        self.assertEqual(self.hwState,Control.HardwareDevice.Operational,
                         "Incorrect HW State: "+str(self.hwState))

        #Not required once the state is fixed in the base class
        time.sleep(1.0)

    def HwLifeCycleDown(self):
        self.hwState = self.reference.getHwState()
        self.assertEqual(self.hwState,Control.HardwareDevice.Operational,
                         "Incorrect HW State: "+str(self.hwState))
        self.reference.hwStop()
        self.hwState = self.reference.getHwState()
        self.assertEqual(self.hwState,Control.HardwareDevice.Stop,
                         "Incorrect HW State: "+str(self.hwState))

        #Not required once the state is fixed in the base class
        time.sleep(1.0)

    def pollingForValue(self,monitor,value,timeout=60):
        for index in range(0,60):
            actualValue = monitor()
            if(actualValue == value):
                break
            else:
                time.sleep(1.0)
        return actualValue

    def check_InactiveExceptions(self):
        try:
            self.reference.getAxisMode()
        except ControlExceptions.INACTErrorEx:
            pass
        except:
            self.fail("ControlExceptions.INACTErrorEx not thrown")
        else:
            self.fail("ControlExceptions.INACTErrorEx not thrown")
            
        try:
            self.reference.setAxisMode(Control.Mount.SHUTDOWN_MODE,
                                       Control.Mount.SHUTDOWN_MODE)
        except ControlExceptions.INACTErrorEx:
            pass
        except:
            self.fail("ControlExceptions.INACTErrorEx not thrown")
        else:
            self.fail("ControlExceptions.INACTErrorEx not thrown")
       
    def check_IllegalAxisModesExceptions(self):
        self.reference.setAxisMode(Control.Mount.SHUTDOWN_MODE,
                                   Control.Mount.SHUTDOWN_MODE)
        (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                       (Control.Mount.SHUTDOWN_MODE,
                                                        Control.Mount.SHUTDOWN_MODE))
        self.assertEqual(azAxisMode,Control.Mount.SHUTDOWN_MODE,
                         "azAxisMode: "+str(azAxisMode))
        self.assertEqual(elAxisMode,Control.Mount.SHUTDOWN_MODE,
                         "elAxisMode: "+str(elAxisMode))

        IllegalAxisModes = [ [Control.Mount.SHUTDOWN_MODE,         Control.Mount.VELOCITY_MODE],
                             [Control.Mount.VELOCITY_MODE,         Control.Mount.SHUTDOWN_MODE],
                             [Control.Mount.VELOCITY_MODE,         Control.Mount.VELOCITY_MODE],
                             [Control.Mount.SHUTDOWN_MODE,         Control.Mount.ENCODER_MODE],
                             [Control.Mount.SHUTDOWN_MODE,         Control.Mount.AUTONOMOUS_MODE],
                             [Control.Mount.SHUTDOWN_MODE,         Control.Mount.MAINTENANCE_STOW_MODE],
                             [Control.Mount.SHUTDOWN_MODE,         Control.Mount.SURVIVAL_STOW_MODE],
                             [Control.Mount.ENCODER_MODE,          Control.Mount.SHUTDOWN_MODE],
                             [Control.Mount.AUTONOMOUS_MODE,       Control.Mount.SHUTDOWN_MODE],
                             [Control.Mount.MAINTENANCE_STOW_MODE, Control.Mount.SHUTDOWN_MODE],
                             [Control.Mount.SURVIVAL_STOW_MODE,    Control.Mount.SHUTDOWN_MODE]]

        for modes in IllegalAxisModes:
            try:
                self.reference.setAxisMode(modes[0],modes[1])
            except ControlExceptions.IllegalParameterErrorEx:
                pass
            else:
                self.fail("ControlExceptions.IllegalParameterErrorEx not thrown")

            (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                       (Control.Mount.SHUTDOWN_MODE,
                                                        Control.Mount.SHUTDOWN_MODE))
            self.assertEqual(azAxisMode,Control.Mount.SHUTDOWN_MODE,
                             "azAxisMode: "+str(azAxisMode))
            self.assertEqual(elAxisMode,Control.Mount.SHUTDOWN_MODE,
                             "elAxisMode: "+str(elAxisMode))

        try:
            self.reference.encoder()
        except MountError.TimeOutEx:
            self.fail("MountError.TimeOutEx thrown")
        (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                       (Control.Mount.ENCODER_MODE,
                                                        Control.Mount.ENCODER_MODE))
        self.assertEqual(azAxisMode,Control.Mount.ENCODER_MODE,
                         "azAxisMode: "+str(azAxisMode))
        self.assertEqual(elAxisMode,Control.Mount.ENCODER_MODE,
                         "elAxisMode: "+str(elAxisMode))

        IllegalAxisModes = [ [Control.Mount.AUTONOMOUS_MODE,       Control.Mount.ENCODER_MODE],
                             [Control.Mount.MAINTENANCE_STOW_MODE, Control.Mount.ENCODER_MODE],
                             [Control.Mount.SURVIVAL_STOW_MODE,    Control.Mount.ENCODER_MODE],
                             [Control.Mount.ENCODER_MODE,          Control.Mount.AUTONOMOUS_MODE],
                             [Control.Mount.ENCODER_MODE,          Control.Mount.MAINTENANCE_STOW_MODE],
                             [Control.Mount.ENCODER_MODE,          Control.Mount.SURVIVAL_STOW_MODE] ]

        for modes in IllegalAxisModes:
            try:
                self.reference.setAxisMode(modes[0],modes[1])
            except ControlExceptions.IllegalParameterErrorEx:
                pass
            else:
                self.fail("ControlExceptions.IllegalParameterErrorEx not thrown")
            (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                           (Control.Mount.ENCODER_MODE,
                                                            Control.Mount.ENCODER_MODE))
            self.assertEqual(azAxisMode,Control.Mount.ENCODER_MODE,
                             "azAxisMode: "+str(azAxisMode))
            self.assertEqual(elAxisMode,Control.Mount.ENCODER_MODE,
                             "elAxisMode: "+str(elAxisMode))

        try:
            self.reference.autonomous()
        except MountError.TimeOutEx:
            self.fail("MountError.TimeOutEx thrown")
        (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                       (Control.Mount.AUTONOMOUS_MODE,
                                                        Control.Mount.AUTONOMOUS_MODE))
        self.assertEqual(azAxisMode,Control.Mount.AUTONOMOUS_MODE,
                         "azAxisMode: "+str(azAxisMode))
        self.assertEqual(elAxisMode,Control.Mount.AUTONOMOUS_MODE,
                         "elAxisMode: "+str(elAxisMode))

        IllegalAxisModes = [ [Control.Mount.ENCODER_MODE,          Control.Mount.AUTONOMOUS_MODE],
                             [Control.Mount.MAINTENANCE_STOW_MODE, Control.Mount.AUTONOMOUS_MODE],
                             [Control.Mount.SURVIVAL_STOW_MODE,    Control.Mount.AUTONOMOUS_MODE],
                             [Control.Mount.AUTONOMOUS_MODE,       Control.Mount.ENCODER_MODE],
                             [Control.Mount.AUTONOMOUS_MODE,       Control.Mount.MAINTENANCE_STOW_MODE],
                             [Control.Mount.AUTONOMOUS_MODE,       Control.Mount.SURVIVAL_STOW_MODE] ]

        for modes in IllegalAxisModes:
            try:
                self.reference.setAxisMode(modes[0],modes[1])
            except ControlExceptions.IllegalParameterErrorEx:
                pass
            else:
                self.fail("ControlExceptions.IllegalParameterErrorEx not thrown")
            (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                           (Control.Mount.AUTONOMOUS_MODE,
                                                            Control.Mount.AUTONOMOUS_MODE))
            self.assertEqual(azAxisMode,Control.Mount.AUTONOMOUS_MODE,
                             "azAxisMode: "+str(azAxisMode))
            self.assertEqual(elAxisMode,Control.Mount.AUTONOMOUS_MODE,
                             "elAxisMode: "+str(elAxisMode))

                           
    def check_AnyToAny(self,initial,transitions):
        initial()
        (azInitialMode, elInitialMode) = self.reference.getAxisMode()
        
        #AZIMUTH
        for cmdMode in transitions:
            self.reference.setAxisMode(self.MODES[cmdMode],elInitialMode)
            (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                           (self.MODES[self.TRANSITIONS[self.NUMERIC_MODES[azInitialMode]][cmdMode]],
                                                            elInitialMode), 120)
            self.assertEqual(azAxisMode,
                             self.MODES[self.TRANSITIONS[self.NUMERIC_MODES[azInitialMode]][cmdMode]],
                             "Initial Az Mode: "+ str(azInitialMode) +
                             " Commanded Mode: "+ str(self.MODES[cmdMode]) +
                             " Expected Mode: "+ str(self.MODES[self.TRANSITIONS[self.NUMERIC_MODES[azInitialMode]][cmdMode]]) +
                             " Incorrect Final Mode: " + str(azAxisMode))
            initial()
            (azInitialMode, elInitialMode) = self.reference.getAxisMode()
            
        #ELEVATION
        for cmdMode in transitions:
            self.reference.setAxisMode(azInitialMode,self.MODES[cmdMode])
            (azAxisMode,elAxisMode) = self.pollingForValue(self.reference.getAxisMode,
                                                           (azInitialMode,
                                                            self.MODES[self.TRANSITIONS[self.NUMERIC_MODES[elInitialMode]][cmdMode]]), 120)
            self.assertEqual(elAxisMode,
                             self.MODES[self.TRANSITIONS[self.NUMERIC_MODES[elInitialMode]][cmdMode]],
                             "Initial El Mode: "+ str(elInitialMode) +
                             " Commanded Mode: "+ str(self.MODES[cmdMode]) +
                             " Expected Mode: "+ str(self.MODES[self.TRANSITIONS[self.NUMERIC_MODES[elInitialMode]][cmdMode]]) +
                             " Incorrect Final Mode: " + str(elAxisMode))
            initial()
            (azInitialMode, elInitialMode) = self.reference.getAxisMode()

    def check_AxisModes(self):
        self.check_InactiveExceptions()
        self.HwLifeCycleUp()
        self.check_IllegalAxisModesExceptions()

        self.check_AnyToAny(self.reference.shutdown,[1])
        self.check_AnyToAny(self.reference.standby,[0,1,2,3,4,5])
        self.check_AnyToAny(self.reference.encoder,[0,1,2])
        self.check_AnyToAny(self.reference.autonomous,[0,1,3])

        self.HwLifeCycleDown()
