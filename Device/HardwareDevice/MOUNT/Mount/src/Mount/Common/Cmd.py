#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

import math
import cmd
from Mount.Common.Client import ACUClient

class CommonCmd(cmd.Cmd):

    def set_communication(self,name, channel, node):
        self.ambgmrName = name
        self.channel = channel
        self.node = node
        self.acuClient = ACUClient(name,channel,node)
        
    def emptyline(self):
        pass

    def message(self,msg):
        print msg
        
    def parse_rest(self,rest,number=1):
        arguments = []
        no_exception = True
        if rest:
            for element in rest.split(","):
                try:
                    arguments.append(eval(element))
                except:
                    self.message("Cannot parse argument: "+element)
                    arguments = []
                    no_exception = False
                    break
            if no_exception and not len(arguments)>=number:
                self.message("Insufficient number of arguments")
                arguments = []
        return arguments

    def help_quit(self):
        print "quit"
        
    def do_quit(self,rest):
        return 1

    def short_help(self,msg):
        print msg

class ACUCommonCmd(CommonCmd):

    def set_units(self, units='encoder'):
        self.units = units

    def set_angle_conversion_defaults(self,offset=0.,turnResolution=0x40000000,bitResolution=0x40000000):
        self.offset = offset
        self.turnResolution = turnResolution
        self.bitResolution  = bitResolution
        self.units = 'encoder'

    def encoder_to_radians1(self,angle_encoder):
        return float(angle_encoder)*math.pi/float(self.turnResolution)+self.offset
       
    def radians_to_encoder1(self,angle_radians):
        return long((angle_radians-self.offset)*float(self.turnResolution)/math.pi)     
       
    def encoder_to_radians2(self,angle_encoder):
        return float(angle_encoder)*math.pi/float(self.bitResolution)+self.offset
       
    def radians_to_encoder2(self,angle_radians):
        return long((angle_radians-self.offset)*float(self.bitResolution)/math.pi)     

    def postloop(self):
        del(self.acuClient)

    def report_byte(self,byte,index,bit_description=[]):
        print "byte"+str(index)+":"
        bits = []
        for bit_index in range(0,8):
            if (byte >> bit_index) & 0x01:
                bits.append("true")
            else:
                bits.append("false")
        if not (len(bit_description) == 8):
            for bit_index in range(0,8):
                print "    bit"+str(bit_index)+": "+bits[bit_index]
        else: 
            for bit_index in range(0,8):
                print "    bit"+str(bit_index)+": "+bits[bit_index]+"("+str(bit_description[bit_index])+")"

    #def help_interface_angle_units(self):
    #    print "interface_angle_units <unit> where:"
    #    print "    <unit> = encoder"
    #    print "    <unit> = degrees"
    #    print "    <unit> = radians"
    #def do_interface_angle_units(self,rest):
    #    if rest not in ['encoder','radians','degrees']:
    #        print "Incorrect angle units: "+rest
    #        self.help_interface_angle_units()
    #    else:
    #        self.units = rest

    # Common command definitions
    def do_acu_mode_rsp(self,rest):
        print self.acu.acu_mode_rsp()
    def do_az_posn_rsp(self,rest):
        (posn0,posn1) = self.acu.az_posn_rsp()
        pos0=posn0*360.00/2**31
        pos1=posn1*360.00/2**31
        print "%f, %f"%(pos0,pos1)
        #if self.units == 'encoder':
        #    print (posn0,posn1)
        #elif self.units == 'radians':
        #    print (self.encoder_to_radians1(posn0),self.encoder_to_radians1(posn1))
        #elif self.units == 'degrees':
        #    print (math.degrees(self.encoder_to_radians1(posn0)),math.degrees(self.encoder_to_radians1(posn1)))
    def do_el_posn_rsp(self,rest):
        (posn0,posn1) = self.acu.el_posn_rsp()
	pos0=posn0*360.00/2**31
	pos1=posn1*360.00/2**31
	print "%f, %f"%(pos0,pos1)
        #if self.units == 'encoder':
        #    print (posn0,posn1)
        #elif self.units == 'radians':
        #    print (self.encoder_to_radians1(posn0),self.encoder_to_radians1(posn1))
        #elif self.units == 'degrees':
        #    print (math.degrees(self.encoder_to_radians1(posn0)),math.degrees(self.encoder_to_radians1(posn1)))
    def do_get_acu_error(self,rest):
        print self.acu.get_acu_error()
    def do_get_az_brake(self,rest):
        print self.acu.get_az_brake()
    def do_get_az_enc(self,rest):
        posn, = self.acu.get_az_enc()
        print (posn,)
        #if self.units == 'encoder':
        #    print (posn,)
        #elif self.units == 'radians':
        #    print (self.encoder_to_radians1(posn),)
        #elif self.units == 'degrees':
        #    print (math.degrees(self.encoder_to_radians1(posn)),)
    def do_get_az_motor_currents(self,rest):
        print self.acu.get_az_motor_currents()
    def do_get_az_motor_temps(self,rest):
        print self.acu.get_az_motor_temps()
    def do_get_az_motor_torque(self,rest):
        print self.acu.get_az_motor_torque()
    def do_get_az_servo_coeff_0(self,rest):
        print self.acu.get_az_servo_coeff_0()
    def do_get_az_servo_coeff_1(self,rest):
        print self.acu.get_az_servo_coeff_1()
    def do_get_az_servo_coeff_2(self,rest):
        print self.acu.get_az_servo_coeff_2()
    def do_get_az_servo_coeff_3(self,rest):
        print self.acu.get_az_servo_coeff_3()
    def do_get_az_servo_coeff_4(self,rest):
        print self.acu.get_az_servo_coeff_4()
    def do_get_az_servo_coeff_5(self,rest):
        print self.acu.get_az_servo_coeff_5()
    def do_get_az_servo_coeff_6(self,rest):
        print self.acu.get_az_servo_coeff_6()
    def do_get_az_servo_coeff_7(self,rest):
        print self.acu.get_az_servo_coeff_7()
    def do_get_az_servo_coeff_8(self,rest):
        print self.acu.get_az_servo_coeff_8()
    def do_get_az_servo_coeff_9(self,rest):
        print self.acu.get_az_servo_coeff_9()
    def do_get_az_servo_coeff_a(self,rest):
        print self.acu.get_az_servo_coeff_a()
    def do_get_az_servo_coeff_b(self,rest):
        print self.acu.get_az_servo_coeff_b()
    def do_get_az_servo_coeff_c(self,rest):
        print self.acu.get_az_servo_coeff_c()
    def do_get_az_status(self,rest):
        status = self.acu.get_az_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)
    def do_get_can_error(self,rest):
        print self.acu.get_can_error()
    def do_get_el_brake(self,rest):
        print self.acu.get_el_brake()
    def do_get_el_enc(self,rest):
        positions = self.acu.get_el_enc()
        reported_positions = []
        reported_positions.append(positions)
        #for position in positions:
        #    if self.units == 'encoder':
        #        reported_positions.append(positions)
        #    elif self.units == 'radians':
        #        reported_positions.append(self.encoder_to_radians1(positions))
        #    elif self.units == 'degrees':
        #        reported_positions.append(math.degrees(self.encoder_to_radians1(positions)))
        print tuple(reported_positions)
    def do_get_el_motor_currents(self,rest):
        print self.acu.get_el_motor_currents()
    def do_get_el_motor_temps(self,rest):
        print self.acu.get_el_motor_temps()
    def do_get_el_motor_torque(self,rest):
        print self.acu.get_el_motor_torque()
    def do_get_el_servo_coeff_0(self,rest):
        print self.acu.get_el_servo_coeff_0()
    def do_get_el_servo_coeff_1(self,rest):
        print self.acu.get_el_servo_coeff_1()
    def do_get_el_servo_coeff_2(self,rest):
        print self.acu.get_el_servo_coeff_2()
    def do_get_el_servo_coeff_3(self,rest):
        print self.acu.get_el_servo_coeff_3()
    def do_get_el_servo_coeff_4(self,rest):
        print self.acu.get_el_servo_coeff_4()
    def do_get_el_servo_coeff_5(self,rest):
        print self.acu.get_el_servo_coeff_5()
    def do_get_el_servo_coeff_6(self,rest):
        print self.acu.get_el_servo_coeff_6()
    def do_get_el_servo_coeff_7(self,rest):
        print self.acu.get_el_servo_coeff_7()
    def do_get_el_servo_coeff_8(self,rest):
        print self.acu.get_el_servo_coeff_8()
    def do_get_el_servo_coeff_9(self,rest):
        print self.acu.get_el_servo_coeff_9()
    def do_get_el_servo_coeff_a(self,rest):
        print self.acu.get_el_servo_coeff_a()
    def do_get_el_servo_coeff_b(self,rest):
        print self.acu.get_el_servo_coeff_b()
    def do_get_el_servo_coeff_c(self,rest):
        print self.acu.get_el_servo_coeff_c()
    def do_get_el_status(self,rest):
        status = self.acu.get_el_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)
    def do_get_idle_stow_time(self,rest):
        print self.acu.get_idle_stow_time()
    def do_get_ip_address(self,rest):
        print self.acu.get_ip_address()
    def do_get_num_trans(self,rest):
        print self.acu.get_num_trans()
    def do_get_shutter(self,rest):
        print self.acu.get_shutter()
    def do_get_stow_pin(self,rest):
        print self.acu.get_stow_pin()
    def do_get_sw_rev_level(self,rest):
        print self.acu.get_sw_rev_level()
    def do_get_system_id(self,rest):
        print self.acu.get_system_id()
    def do_get_system_status(self,rest):
        status = self.acu.get_system_status()
        for index in range(0,len(status)):
            self.report_byte(status[index],index)
  

    def help_acu_mode_cmd(self):
        print "acu_mode_cmd <arg1>,<arg2>"
        print "where:"
        print "    <arg1>: azimuth mode"
        print "    <arg2>: elevation mode"
    def do_acu_mode_cmd(self,rest):
        arguments = self.parse_rest(rest,2)
        if arguments:
            data = arguments[0]+(arguments[1] << 4)
            self.acu.acu_mode_cmd(data)
        else:
            self.help_acu_mode_cmd()

    def help_az_traj_cmd(self):
        pass
    def do_az_traj_cmd(self,rest):
        arguments = self.parse_rest(rest,2)
        if arguments:
            if self.units == 'encoder':
                position = arguments[0]
                velocity = arguments[1]
            elif self.units == 'radians':
                position = self.radians_to_encoder1(arguments[0])
                velocity = self.radians_to_encoder1(arguments[1])
            elif self.units == 'degrees':
                position = self.radians_to_encoder1(math.radians(arguments[0]))
                velocity = self.radians_to_encoder1(math.radians(arguments[1]))
            self.acu.az_traj_cmd(position,velocity)
        else:
            self.help_az_traj_cmd()
        
    def help_clear_fault_cmd(self):
        print "clear_fault_cmd 1"
    def do_clear_fault_cmd(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            if arguments[0] == 1:
                self.acu.clear_fault_cmd(arguments[0])
            else:
                self.message("clear_fault_cmd ignored. Invalid argument: "+rest)
                self.help_clear_fault_cmd()
        else:
            self.help_clear_fault_cmd()

    def help_el_traj_cmd(self):
        pass
    def do_el_traj_cmd(self,rest):
        arguments = self.parse_rest(rest,2)
        if arguments:
            if self.units == 'encoder':
                position = arguments[0]
                velocity = arguments[1]
            elif self.units == 'radians':
                position = self.radians_to_encoder1(arguments[0])
                velocity = self.radians_to_encoder1(arguments[1])
            elif self.units == 'degrees':
                position = self.radians_to_encoder1(math.radians(arguments[0]))
                velocity = self.radians_to_encoder1(math.radians(arguments[1]))
            self.acu.el_traj_cmd(position,velocity)
        else:
            self.help_el_traj_cmd()
        
    def help_reset_acu_cmd(self):
        print "reset_acu_cmd 1"
    def do_reset_acu_cmd(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            if arguments[0] == 1:
                self.acu.reset_acu_cmd(arguments[0])
            else:
                self.message("reset_acu_cmd ignored. Invalid argument: "+rest)
                self.help_reset_acu_cmd()
        else:
            self.help_reset_acu_cmd()

    def help_set_az_brake(self):
        pass
    def do_set_az_brake(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_brake(arguments[0])
        else:
            self.help_set_az_brake()

    def build_help_set_servo_coeff(self,axis,index):
        print "set_"+axis+"_servo_coeff_"+index+" <arg1>"
        print "where:"
        print "    <arg1>: coefficient"
        
    def build_help_set_az_servo_coeff(self,index):
        self.build_help_set_servo_coeff('az',index)
        
    def help_set_az_servo_coeff_0(self):
        self.build_help_set_az_servo_coeff('0')
    def do_set_az_servo_coeff_0(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_0(arguments[0])
        else:
            self.help_set_az_servo_coeff_0()

    def help_set_az_servo_coeff_1(self):
        self.build_help_set_az_servo_coeff('1')
    def do_set_az_servo_coeff_1(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_1(arguments[0])
        else:
            self.help_set_az_servo_coeff_1()

    def help_set_az_servo_coeff_2(self):
        self.build_help_set_az_servo_coeff('2')
    def do_set_az_servo_coeff_2(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_2(arguments[0])
        else:
            self.help_set_az_servo_coeff_2()

    def help_set_az_servo_coeff_3(self):
        self.build_help_set_az_servo_coeff('3')
    def do_set_az_servo_coeff_3(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_3(arguments[0])
        else:
            self.help_set_az_servo_coeff_0()
            
    def help_set_az_servo_coeff_4(self):
        self.build_help_set_az_servo_coeff('4')
    def do_set_az_servo_coeff_4(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_4(arguments[0])
        else:
            self.help_set_az_servo_coeff_4()

    def help_set_az_servo_coeff_5(self):
        self.build_help_set_az_servo_coeff('5')
    def do_set_az_servo_coeff_5(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_5(arguments[0])
        else:
            self.help_set_az_servo_coeff_5()

    def help_set_az_servo_coeff_6(self):
        self.build_help_set_az_servo_coeff('6')
    def do_set_az_servo_coeff_6(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_6(arguments[0])
        else:
            self.help_set_az_servo_coeff_6()

    def help_set_az_servo_coeff_7(self):
        self.build_help_set_az_servo_coeff('7')
    def do_set_az_servo_coeff_7(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_7(arguments[0])
        else:
            self.help_set_az_servo_coeff_7()

    def help_set_az_servo_coeff_8(self):
        self.build_help_set_az_servo_coeff('8')
    def do_set_az_servo_coeff_8(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_8(arguments[0])
        else:
            self.help_set_az_servo_coeff_8()

    def help_set_az_servo_coeff_9(self):
        self.build_help_set_az_servo_coeff('9')
    def do_set_az_servo_coeff_9(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_9(arguments[0])
        else:
            self.help_set_az_servo_coeff_9()

    def help_set_az_servo_coeff_a(self):
        self.build_help_set_az_servo_coeff('a')
    def do_set_az_servo_coeff_a(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_a(arguments[0])
        else:
            self.help_set_az_servo_coeff_a()

    def help_set_az_servo_coeff_b(self):
        self.build_help_set_az_servo_coeff('b')
    def do_set_az_servo_coeff_b(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_b(arguments[0])
        else:
            self.help_set_az_servo_coeff_b()

    def help_set_az_servo_coeff_c(self):
        self.build_help_set_az_servo_coeff('c')
    def do_set_az_servo_coeff_c(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_az_servo_coeff_c(arguments[0])
        else:
            self.help_set_az_servo_coeff_c()

    def help_set_az_servo_default(self):
        self.short_help("set_az_servo_default 1")
    def do_set_az_servo_default(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            if arguments[0] == 1:
                self.acu.set_az_servo_default(arguments[0])
            else:
                self.message("set_az_servo_default ignored. Invalid argument: "+rest)
        else:
            self.help_set_az_servo_default()

    def help_set_el_brake(self):
        pass
    def do_set_el_brake(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_brake(arguments[0])
        else:
            self.help_set_el_brake()

    def build_help_set_el_servo_coeff(self,index):
        self.build_help_set_servo_coeff('el',index)
        
    def help_set_el_servo_coeff_0(self):
        self.build_help_set_el_servo_coeff('0')
    def do_set_el_servo_coeff_0(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_0(arguments[0])
        else:
            self.help_set_el_servo_coeff_0()

    def help_set_el_servo_coeff_1(self):
        self.build_help_set_el_servo_coeff('1')
    def do_set_el_servo_coeff_1(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_1(arguments[0])
        else:
            self.help_set_el_servo_coeff_1()

    def help_set_el_servo_coeff_2(self):
        self.build_help_set_el_servo_coeff('2')
    def do_set_el_servo_coeff_2(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_2(arguments[0])
        else:
            self.help_set_el_servo_coeff_2()

    def help_set_el_servo_coeff_3(self):
        self.build_help_set_el_servo_coeff('3')
    def do_set_el_servo_coeff_3(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_3(arguments[0])
        else:
            self.help_set_el_servo_coeff_0()
            
    def help_set_el_servo_coeff_4(self):
        self.build_help_set_el_servo_coeff('4')
    def do_set_el_servo_coeff_4(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_4(arguments[0])
        else:
            self.help_set_el_servo_coeff_4()

    def help_set_el_servo_coeff_5(self):
        self.build_help_set_el_servo_coeff('5')
    def do_set_el_servo_coeff_5(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_5(arguments[0])
        else:
            self.help_set_el_servo_coeff_5()

    def help_set_el_servo_coeff_6(self):
        self.build_help_set_el_servo_coeff('6')
    def do_set_el_servo_coeff_6(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_6(arguments[0])
        else:
            self.help_set_el_servo_coeff_6()

    def help_set_el_servo_coeff_7(self):
        self.build_help_set_el_servo_coeff('7')
    def do_set_el_servo_coeff_7(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_7(arguments[0])
        else:
            self.help_set_el_servo_coeff_7()

    def help_set_el_servo_coeff_8(self):
        self.build_help_set_el_servo_coeff('8')
    def do_set_el_servo_coeff_8(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_8(arguments[0])
        else:
            self.help_set_el_servo_coeff_8()

    def help_set_el_servo_coeff_9(self):
        self.build_help_set_el_servo_coeff('9')
    def do_set_el_servo_coeff_9(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_9(arguments[0])
        else:
            self.help_set_el_servo_coeff_9()

    def help_set_el_servo_coeff_a(self):
        self.build_help_set_el_servo_coeff('a')
    def do_set_el_servo_coeff_a(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_a(arguments[0])
        else:
            self.help_set_el_servo_coeff_a()

    def help_set_el_servo_coeff_b(self):
        self.build_help_set_el_servo_coeff('b')
    def do_set_el_servo_coeff_b(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_b(arguments[0])
        else:
            self.help_set_el_servo_coeff_b()

    def help_set_el_servo_coeff_c(self):
        self.build_help_set_el_servo_coeff('c')
    def do_set_el_servo_coeff_c(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_el_servo_coeff_c(arguments[0])
        else:
            self.help_set_el_servo_coeff_c()

    def help_set_el_servo_default(self):
        self.short_help("set_el_servo_default 1")
    def do_set_el_servo_default(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            if arguments[0] == 1:
                self.acu.set_el_servo_default(arguments[0])
            else:
                self.message("set_el_servo_default ignored. Invalid argument: "+rest)
        else:
            self.help_set_el_servo_default()

    def help_set_idle_stow_time(self):
        self.short_help("set_idle_stow_time <secs>")
    def do_set_idle_stow_time(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_idle_stow_time(arguments[0])
        else:
            self.help_set_idle_stow_time()
    
    def help_set_shutter(self):
        pass
    def do_set_shutter(self,rest):
        arguments = self.parse_rest(rest)
        if arguments:
            self.acu.set_shutter(arguments[0])
        else:
            self.help_set_shutter()
    
    def help_set_stow_pin(self):
        print "set_stow_pin <arg1> <arg2>"
        print "where:"
        print "    <arg1>: az stow pin"
        print "    <arg1>: el stow pin"
    def do_set_stow_pin(self,rest):
        arguments = self.parse_rest(rest,2)
        if arguments:
            self.acu.set_stow_pin(arguments[0],arguments[1])
        else:
            self.help_set_stow_pin()

