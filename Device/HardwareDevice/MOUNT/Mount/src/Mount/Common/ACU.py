#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

import sys
import string
import time
import struct
from Mount.Device import Device

class ACU(Device):

    def __init__(self, ambmgr, node, channel):
        # Listed in ICD order

        # Common monitor points
        self.ICD['acu_mode_rsp'] = {'RCA': 0x22, 'format': '!2B', 'type': 'monitor'}
        self.ICD['acu_trk_mode_rsp'] = {'RCA': 0x20, 'format': '!1B', 'type': 'monitor'}
        self.ICD['az_posn_rsp']  = {'RCA': 0x12, 'format': '!2l', 'type': 'monitor'}
        self.ICD['el_posn_rsp']  = {'RCA': 0x02, 'format': '!2l', 'type': 'monitor'}
        self.ICD['get_acu_error'] = {'RCA': 0x2F, 'format': '',    'type': 'monitor'}
        self.ICD['get_az_brake'] = {'RCA': 0x14, 'format': '!B',  'type': 'monitor'}
        self.ICD['get_az_enc']   = {'RCA': 0x17, 'format': '!L',  'type': 'monitor'}
	# TBD: Move this down in the hierarchy as they conflict with ICD C5 definitios
        self.ICD['get_az_motor_currents'] = {'RCA': 0x19, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_az_motor_temps'] = {'RCA': 0x1A, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_az_motor_torque'] = {'RCA': 0x15, 'format': '!4B',  'type': 'monitor'}
        for index in range(0,13):
            self.ICD['get_az_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x3020+index, 'format': '!d',  'type': 'monitor'}
        self.ICD['get_az_status'] = {'RCA': 0x1B, 'format': '!8B',  'type': 'monitor'}
        self.ICD['get_can_error'] = {'RCA': 0x70001, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_el_brake'] = {'RCA': 0x04, 'format': 'B',  'type': 'monitor'}
        for index in range(0,13):
            self.ICD['get_el_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x3010+index, 'format': '!d',  'type': 'monitor'}
        self.ICD['get_el_status'] = {'RCA': 0x0B, 'format': '!8B',  'type': 'monitor'}
        self.ICD['get_idle_stow_time'] = {'RCA': 0x25, 'format': '!H',  'type': 'monitor'}
        self.ICD['get_ip_address'] = {'RCA': 0x2D, 'format': '!8B',  'type': 'monitor'}
        self.ICD['get_ip_gateway'] = {'RCA': 0x38, 'format': '!4B',  'type': 'monitor'}
        self.ICD['get_num_trans'] = {'RCA': 0x30002, 'format': '!L',  'type': 'monitor'}
        self.ICD['get_shutter'] = {'RCA': 0x2E, 'format': 'B',  'type': 'monitor'}
        self.ICD['get_stow_pin'] = {'RCA': 0x24, 'format': '!2B',  'type': 'monitor'}
        self.ICD['get_sw_rev_level'] = {'RCA': 0x30000, 'format': '!3B',  'type': 'monitor'}

        # Common control points
        self.ICD['acu_track_mode'] = {'RCA': 0x1022, 'format': 'B',  'type': 'command'}
        self.ICD['az_traj_cmd'] = {'RCA': 0x1012, 'format': '!ll',  'type': 'command'}
        self.ICD['clear_fault_cmd'] = {'RCA': 0x1021, 'format': '!B',  'type': 'command'}
        self.ICD['el_traj_cmd'] = {'RCA': 0x1002, 'format': '!ll',  'type': 'command'}
        self.ICD['reset_acu_cmd'] = {'RCA': 0x102F, 'format': '!B',  'type': 'command'}
        self.ICD['set_az_brake'] = {'RCA': 0x1014, 'format': 'B',  'type': 'command'}
        for index in range(0,13):
            self.ICD['set_az_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x2020+index, 'format': '!d',  'type': 'command'}        
        self.ICD['set_az_servo_default'] = {'RCA': 0x1017, 'format': 'B',  'type': 'command'}
        self.ICD['set_el_brake'] = {'RCA': 0x1004, 'format': 'B',  'type': 'command'}
        for index in range(0,13):
            self.ICD['set_el_servo_coeff_'+hex(index)[2:]] = {'RCA': 0x2010+index, 'format': '!d',  'type': 'command'}        
        self.ICD['set_el_servo_default'] = {'RCA': 0x1007, 'format': 'B',  'type': 'command'}
        self.ICD['set_idle_stow_time'] = {'RCA': 0x1025, 'format': '!H',  'type': 'command'}
        self.ICD['set_shutter'] = {'RCA': 0x102E, 'format': '!B',  'type': 'command'}
        self.ICD['set_stow_pin'] = {'RCA': 0x102D, 'format': '!2B',  'type': 'command'}        

        Device(ambmgr,node,channel)

class ClientException(Exception):
    '''
    '''
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

