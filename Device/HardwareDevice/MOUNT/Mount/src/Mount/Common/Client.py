#!/usr/bin/env python
# @(#) $Id$
#
# Copyright (C) 2001
# Associated Universities, Inc. Washington DC, USA.
#
# Produced for the ALMA project
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Library General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
# more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
# Correspondence concerning ALMA should be addressed as follows:
# Internet email: alma-sw-admin@nrao.edu

from CCL.AmbManager import *
from Mount.Device import Device
from Mount.Common.ACU import ClientException
from Mount.Common.ACU import ACU as ACUCommon

from Acspy.Clients.SimpleClient import PySimpleClient
import Control__POA
import Control

class ACUClient:
    def __init__(self,name='DV01', channel=0, node=0):
        try:
            self.client = PySimpleClient.getInstance()
        except:
            msg = "Cannot get simple client instance"
            raise ClientException(msg)
        self.mgrName = "CONTROL/" + name + "/AmbManager"
        try:
            self.ambmgr = self.client.getComponent(self.mgrName)
        except:
            msg = 'Cannot get reference for AmbManager component %s.' % (self.mgrName)
            raise ClientException(msg)
        
    def __del__(self):
        self.client.releaseComponent(self.mgrName)
        self.client.disconnect()
        
