#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 - 2010
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
"""
This module defines the Mount Control Command Language (CCL)
base class. It is automatically generated and provides access to
all set monitor, control points and device methods.
"""

import CCL.MountBase

class Mount(CCL.MountBase.MountBase):
    '''
    The purpose of this document is to define the interface between
    the antenna, specifically it's control unit and ALMA's monitor and
    control (M&C) system. The ICD provides the interface definitions
    for the minimum control functionality which is identified at
    present for the control of the antenna.
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        '''
        # Initialize the base class. The base class contains the
        # reference to the underlying Mount component.
        CCL.MountBase.MountBase.__init__(self, \
                                     antennaName, \
                                     componentName, \
                                     stickyFlag);

    def __del__(self):
        CCL.MountBase.MountBase.__del__(self)

    #
    # --------------------- General Methods ---------------------
    #
    def GET_AZ_BRAKE(self):
        '''
        Get azimuth brake status
        '''
        return self._HardwareDevice__hw.GET_AZ_BRAKE()

    def GET_EL_BRAKE(self):
        '''
        Get elevation brake status
        '''
        return self._HardwareDevice__hw.GET_EL_BRAKE()

    def SET_AZ_BRAKE(self, mode):
        '''
        Set azimuth brake state
        '''
        self._HardwareDevice__hw.SET_AZ_BRAKE(mode)
        return

    def SET_EL_BRAKE(self, mode):
        '''
        Set elevation brake state
        '''
        self._HardwareDevice__hw.SET_EL_BRAKE(mode)
        return

    def GET_ACU_ERROR(self):
        '''
        GET_ACU_ERROR returns an list with a antenna specific error
        code and the RCA of the offending CAN command. If the error
        code is characterized as a fault (see ICD), the
        CLEAR_FAULT_CMD should be sent to clear that state. Error
        codes are defined in each antenna specific ICD.
        '''
        return self._HardwareDevice__hw.GET_ACU_ERROR()


    def SET_SHUTTER(self, mode):
        '''
        OPEN/CLOSE the shutter. The enum types needed to use this
        method are defined in Control library:
        Control.Mount.SHUTTER_CLOSED,
        Control.Mount.SHUTTER_OPEN. Example:
    
        import Control
        import CCL.Mount
        mount = CCL.Mount.Mount(componentName="CONTROL/DV01/MOUNT")
        mount.SET_SHUTTER(Control.Mount.SHUTTER_CLOSED)
        '''
        self._HardwareDevice__hw.SET_SHUTTER(mode)
        return
    
    def openShutter(self):
        '''
        Open the shutter
        '''
        self._HardwareDevice__hw.openShutter()
        return
    
    def closeShutter(self):
        '''
        Close the shutter
        '''
        self._HardwareDevice__hw.closeShutter()
        return

    def isShutterOpen(self):
        '''
        Returns true if the shutter is open
        '''
        return self._HardwareDevice__hw.isShutterOpen()

    def isShutterClosed(self):
        '''
        Returns true if the shutter is closed
        '''
        return self._HardwareDevice__hw.isShutterClosed()

    def SET_STOW_PIN(self, position):
        '''
        '''
        self._HardwareDevice__hw.SET_STOW_PIN(position)
        return
        
    def setAxisMode(self, azMode, elMode):
        '''
        Set the axis mode for both the azimuth and elevation axes.
        This function is used to change the mode of the both axes.
        The axes can be commanded to any one of the states listed in
        the AxisMode enumerator except VELOCITY_MODE. To switch into
        some modes, like ENCODER_MODE, you need to have already
        switched to an an appropriate preliminary mode, like
        STANDBY_MODE.  The exact relationship between modes is
        described in the ICD.  This function does not check if the
        specifed mode is accessible from the current mode.  This
        function returns immediatly and does not wait for the axes to
        transition to the specified mode.

        This function will throw a:
        * IllegalParameterErrorEx exception if VELOCITY_MODE is
          specified.
        * CAMBErrorEx exception if this function cannot communicate
          with the ACU
        '''
        self._HardwareDevice__hw.setAxisMode(azMode, elMode)
        return
    
    def setAzAxisMode(self, mode): 
        '''
        Set the azimuth axis mode.  This function is identical to the
        setAxisMode function except that it applies to only the
        azimuth axis. The AxisMode on the elevation axis is unchanged.
        '''
        self._HardwareDevice__hw.setAzAxisMode(mode)
        return
    
    def setElAxisMode(self,mode):
        '''
        Set the elevation axis mode.  This function is identical to
        the setAxisMode function except that it applies to only the
        elevation axis. The AxisMode on the azimuth axis is unchanged.
        '''
        self._HardwareDevice__hw.setElAxisMode(mode)
        return
    
    def getAxisMode(self):
        '''
        Returns the axis mode for both the azimuth and elevation axes.
        See the AxisMode enumerator for a description of each mode.

        This function will throw a:
        * CAMBErrorEx exception if this function cannot communicate
          with the ACU.
        '''
        return self._HardwareDevice__hw.getAxisMode()

    def inLocalMode(self):
        '''
        This function returns true if the ACU is in local access
        mode. In local access mode the ACU will ignore all commands
        and only respond to monitor requests.

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        '''
        return self._HardwareDevice__hw.inLocalMode()

    def shutdown(self):
        '''
        This function will put both axes in shutdown mode. It will
        wait until both axes are in this mode before returning and
        normally this should not take very long (less than a second).

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        * TimeOut exception if the antenna does not move to shutdown
          within a specified timeout period.
        * LocalMode exception if the antenna is in local mode (and
          hence will not respond to commands)
        '''
        self._HardwareDevice__hw.shutdown()
        return
    
    def inShutdownMode(self):
        '''
        This function will return true if both axes are in shutdown
        mode

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        '''
        return self._HardwareDevice__hw.inShutdownMode()

    def standby(self):
        '''
        This function will put both axes in standby mode. It will wait
        until both axes are in this mode and, if necessary, clear
        faults and initialise encoders. This function may take some
        time (minutes) to complete if encoders need to be initialized.

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        * TimeOut exception if the antenna does not move to standby
          within a specified timeout period.
        * LocalMode exception if the antenna is in local mode (and
          hence will not respond to commands)
        '''
        self._HardwareDevice__hw.standby()
        return
    
    def inStandbyMode(self):
        '''
        This function will return true if both axes are in standby
        mode

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        '''
        return self._HardwareDevice__hw.inStandbyMode()

    def encoder(self):
        '''
        This function will put both axes in encoder mode. This
        function will, if necessary, sequence through the relevant
        intermediate modes (like standby mode) and wait for both axes
        to get to encoder mode before returning. This may take minutes
        if we are using the Alcatel antenna.

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        * TimeOut exception if the antenna does not move to track mode
          within a specified timeout period.
        * LocalMode exception if the antenna is in local mode (and
          hence will not respond to commands)
        '''
        self._HardwareDevice__hw.encoder()
        return
              
    def autonomous(self):
        '''
        This function will put both axes in autonomous mode. This
        function will, if necessary, sequence through the relevant
        intermediate modes (like standby mode) and wait for both axes
        to get to autonomous mode before returning. This may take
        minutes if we are using the Alcatel antenna.

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        * TimeOut exception if the antenna does not move to track mode
          within a specified timeout period.q
        * LocalMode exception if the antenna is in local mode (and
          hence will not respond to commands)
        '''
        self._HardwareDevice__hw.autonomous()
        return
    
    def isMoveable(self):
        '''
        This function will return true if both axes are in encoder
        mode (if the ACU pointing model is disabled) or autonomous
        mode (if the ACU pointing model is enabled).

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        '''
        return self._HardwareDevice__hw.isMoveable()
    
    def survivalStow(self):
        '''
        This function will move the telescope to survival stow
        position.  This function will, if necessary, sequence through
        the relevant intermediate modes (like standby mode) and wait
        for both axes to get to SHUTDOWN_MODE mode before returning.

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        * TimeOut exception if the antenna does not move to track mode
          within a specified timeout period.
        * LocalMode exception if the antenna is in local mode (and
          hence will not respond to commands)
        '''
        self._HardwareDevice__hw.survivalStow()
        return
    
    def maintenanceStow(self):
        '''
        This function will move the telescope to maintenace stow
        position.  This function will, if necessary, sequence through
        the relevant intermediate modes (like standby mode) and wait
        for both axes to get to SHUTDOWN_MODE mode before
        returning. This may take minutes if we are using the Alcatel
        antenna.

        This function will throw a:
        * CAMBError exception if this function cannot communicate with
          the ACU
        * TimeOut exception if the antenna does not move to track mode
          within a specified timeout period.
        * LocalMode exception if the antenna is in local mode (and
          hence will not respond to commands)
        '''
        self._HardwareDevice__hw.maintenanceStow()
        return

    def setMaintenanceAzEl(self, azimuth, elevation):
        '''
        This function will move the antenna to the specified azimuth
        and elevation position for engineering maintenace
        purposes. The precision used for positioning is lower than the
        default tolerance for used in scientific observations
        '''
        self._HardwareDevice__hw.setMaintenanceAzEl(azimuth,elevation)
        return
    
    def flushTrajectory(self,applicationTime):
        '''
        This method will remove all trajectory monitor and commands
        after the application time. It should be calles by the mount
        controller component when a new position is requested.
        '''
        self._HardwareDevice__hw.flushTrajectory(self, applicationTime)
        return
    
    def getMountStatusData(self):
        '''
        Returns the current value of the MountStatusData structure for
        the latest time available.
        '''
        return self._HardwareDevice__hw.getMountStatusData()

    def enableMountStatusDataPublication(self, enable):
        '''
        Enables (true)/disables(false) position MountStatusData event
        publication
        '''
        self._HardwareDevice__hw.enableMountStatusDataPublication(enable)
        return
    
    def isMountStatusDataPublicationEnabled(self):
        '''
        Returns true if MountStatusData event publication is enabled
        '''
        return self._HardwareDevice__hw.isMountStatusDataPublicationEnabled()

    def onSource(self):
        '''
        Returns true if the requested position has been reached
        according to the tolerance and criteria
        '''
        return self._HardwareDevice__hw.onSource()

    def setTolerance(self, tolerance):
        '''
        Set the tolerance to be used in the onSource calculation
        '''
        self._HardwareDevice__hw.setTolerance(tolerance)
        return
    
    def getTolerance(self):
        '''
        Get the tolerance currently used by the Mount component
        '''
        return self._HardwareDevice__hw.getTolerance()

    def isInsideCLOUDSATRegion(self):
        '''
        Returns true if the current antenna position is inside the
        CLOUDSAT avoidance region
        '''
        return self._HardwareDevice__hw.isInsideCLOUDSATRegion()

    def avoidCLOUDSATRegion(self,enable):
        '''
        Enable/disable to avoid CLOUDSAT region in the received
        trajectory. If it is enabled, all elevations inside the
        avoidance region will be replaced by the proper elevation
        limit and the elevation velocity will be set to zero
        '''
        self._HardwareDevice__hw.avoidCLOUDSATRegion(enable)
        return

    def isCLOUDSATRegionAvoided(self):
        '''
        Returns true if the CLOUDSAT region is being avoided (as set
        by a call to avoidCLOUDSATRegion), otherwise returns false
        '''
        return self._HardwareDevice__hw.isCLOUDSATRegionAvoided()

    def reloadFocusModel(self):
        '''
        Reloads the focus model from the data base (TMCDB). This should
        be done to pick up changes after the model has been
        changed. Currently the focus model is in
        .../FocusModel.xml

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If any value in the data base is incorrect or there is any
          other other problem getting the data from the data base.
          
        '''
        return self._HardwareDevice__hw.reloadFocusModel()
    
    def setBand(self, bandNumber):
        '''        
        Set the band used by both the focus *and* pointing models. If
        this involves a band change it will replace any user specified
        focus and pointing models, set using the setFocusModel and
        setPointingModel functions, with models obtained from the
        data-base (TMCDB). The band must be in the range 1 to 10.

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If the band is outside the range 1 to 10
        '''
        return self._HardwareDevice__hw.setBand(bandNumber)
    
    def getFocusModelBand(self):
        '''
        Returns the current band used by the focus model. If the
        current model does not correspond to the sum of the antenna
        and band focus models, because it has been set using the
        setFocusModel function, then it returns a negative number with
        a magnitude that corresponds to the current band e.g., -3.
        '''
        return self._HardwareDevice__hw.getFocusModelBand()
    
    def setAmbientTemperature(self, tempInC):
        '''
        Set the ambient temperature in degrees Celsius. This is used
        only by the focus model and should be obtained from a nearby
        weather station. In normal operation the MountController
        component will call this function when necessary.
        
        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If the temperature is outside the range -50 to 100
        '''
        return self._HardwareDevice__hw.setAmbientTemperature(tempInC)
    
    def getFocusModel(self):
        '''
        This returns the current coefficients used by the focus
        model. Normally these coefficients can be decomposed into an
        antenna and band specific parts.
        '''
        return self._HardwareDevice__hw.getFocusModel()

    def getFocusModelCoefficient(self, termName):
        '''
        This returns the current value of the specified term in the
        focus model.  

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If the specified term is not recognized.
        '''
        return self._HardwareDevice__hw.getFocusModelCoefficient(termName)

    def setFocusModel(self, newPointingModel):
        '''
        This sets the focus model for the mount, any terms not listed are
        set to zero. To individually change selected coefficients use the
        setFocusModelCoefficient function.

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If any coefficient is not recognized or its value is too big.
        '''
        return self._HardwareDevice__hw.setFocusModel(newPointingModel)
    
    def setFocusModelCoefficient(self, name, value):
        '''
        Replace the specified coefficient in the focus model. 

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If the coefficient is not recognized or its value is too big.
        '''
        return self._HardwareDevice__hw.setFocusModelCoefficient(name, value)
    
    def enableFocusModel(self, enabled=True):
        '''
        Enable/Disable focus model. A disabled model will not
        automatically send commands to move the subreflector. Use the
        enableFocusPointingModel function to disable the pointing
        correction that compensates for the subreflector position.
        '''
        return self._HardwareDevice__hw.enableFocusModel(enabled)
    
    def isFocusModelEnabled(self):
        '''
        Returns true if the focus model is enabled, false otherwise
        '''
        return self._HardwareDevice__hw.isFocusModelEnabled()

    def enableFocusPointingModel(self, enabled=True):
        '''
        Enable/Disable the focus pointing model. A disabled focus
        pointing model will not adjust the pointing to compensate for
        the position of the subreflector.
        '''
        return self._HardwareDevice__hw.enableFocusPointingModel(enabled)

    def isFocusPointingModelEnabled(self):
        '''
        Returns true if the focus pointing model is enabled, false otherwise
        '''
        return self._HardwareDevice__hw.isFocusPointingModelEnabled()

    def setSubreflectorPositionToleranceXY(self, tolerance):
        '''
        Set the maximum error in the subreflector position. The code that
        automatically adjusts the subreflector position will not send a
        command to move the subreflector unless the ideal position is more
        than this value away for its current position. And the mount is not
        condidered to be "on-source" unless the difference between the last
        commanded position and the actual position is less than the tolerance
        There are three tolerances:
        1. For the z position (with a default of 10um) 
        2. For the x and y positions (with a default of 100um)
        3. For the tip and tilt (with a default of 1 milli-degree)
        All values need to be specified in meters or radians.

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If the tolerance is far too big or less then the precision
          of the subreflector positioning mechanism
        '''
        return self._HardwareDevice__hw.setSubreflectorPositionToleranceXY(tolerance)
    
    def setSubreflectorPositionToleranceZ(self, tolerance):
        '''
        Set the subreflector position tolerance on the Z axis. See the
        documentation for the setSubreflectorPositionToleranceXY
        function for more details.
        '''
        return self._HardwareDevice__hw.setSubreflectorPositionToleranceZ(tolerance)
    
    def setSubreflectorRotationTolerance(self, tolerance):
        '''
        Set the subreflector rotation tolerance. See the
        documentation for the setSubreflectorPositionToleranceXY
        function for more details.
        '''
        return self._HardwareDevice__hw.setSubreflectorRotationTolerance(tolerance)
    
    def getSubreflectorPositionToleranceXY(self):
        '''
        Get the subreflector position tolerance on the x and y axes. See the
        documentation for the setSubreflectorPositionToleranceXY
        function for more details.
        '''
        return self._HardwareDevice__hw.getSubreflectorPositionToleranceXY()
    
    def getSubreflectorPositionToleranceZ(self):
        '''
        Get the subreflector position tolerance on the z axis. See the
        documentation for the setSubreflectorPositionToleranceXY
        function for more details.
        '''
        return self._HardwareDevice__hw.getSubreflectorPositionToleranceZ()
    
    def getSubreflectorRotationTolerance(self):
        '''
        Get the subreflector rotation tolerance. See the
        documentation for the setSubreflectorPositionToleranceXY
        function for more details.
        '''
        return self._HardwareDevice__hw.getSubreflectorRotationTolerance()

    def getSubreflectorPosition(self):
        '''
        Get the current subreflector position. All values are in
        meters.

        This function will throw a:
        * ControlExceptions.HardwareErrorEx
          If the value cannot be obtained from the ACU
        '''
        return self._HardwareDevice__hw.getSubreflectorPosition()
    
    def getSubreflectorRotation(self):
        '''
        Get the current rotation of the subreflector. All values are
        in radians.

        This function will throw a:
        * ControlExceptions.HardwareErrorEx
          If the value cannot be obtained from the ACU
        '''
        return self._HardwareDevice__hw.getSubreflectorRotation()
    
    def setSubreflectorPosition(self, x, y, z):
        '''
        Set the subreflector position to the specified value. Calling this
        function will disable the focus model. All values are in meters.

        This function will throw a
        * ControlExceptions.IllegalParameterErrorEx
          If any value is outside the allowed range
        '''
        return self._HardwareDevice__hw.setSubreflectorPosition(x, y, z)
    
    def SET_SUBREF_ABS_POSN(self, x, y, z):
        '''
        This function is added to override a similar function, in the
        base class, which should *never* be used. Its deprecated and
        users should call the setSubreflectorPosition function
        instead.
        '''
        return self.setSubreflectorPosition(x, y, z)

    def setSubreflectorRotation(self, tip, tilt):
        '''
        Set the subreflector rotation to the specified value. Calling this
        function will disable the focus model. All values are in radians.

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If any value is outside the allowed range
        '''
        return self._HardwareDevice__hw.setSubreflectorRotation(tip, tilt)
    
    def SET_SUBREF_ROTATION(self, tip, tilt):
        '''
        This function is added to override a similar function, in the
        base class, which should *never* be used. Its deprecated and
        users should call the setSubreflectorRotation function
        instead.
        '''
        return self.setSubreflectorRotation(tip, tilt)

    def getSubreflectorPositionOffset(self):
        '''
        Get the current subreflector position offset. All values are
        in meters.
        '''
        return self._HardwareDevice__hw.getSubreflectorPositionOffset()
    
    def setSubreflectorPositionOffset(self, x, y, z):
        '''
        Specify an offset in the subreflector position. This offset is
        added to the value computed by the focus model. Its used even
        if the focus model is disabled. Its primary use if for doing a
        focus calibration and the subsequent tweaking of the
        focus. All values are in meters.

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If any value is outside the allowed range
        '''
        return self._HardwareDevice__hw.setSubreflectorPositionOffset(x, y, z)
    
    def getSubreflectorRotationOffset(self):
        '''
        Get the current subreflector rotation offsets. All values are
        in radians.
        '''
        return self._HardwareDevice__hw.getSubreflectorRotationOffset()
    
    def setSubreflectorRotationOffset(self, tip, tilt):
        '''
        Specify an offset in the subreflector rotation. This offset is
        added to the value computed by the focus model. It is not used
        if the focus model is disabled. Its primary use if for
        commisioning tasks that establish the focus model. All values
        are in radians.

        This function will throw a:
        * ControlExceptions.IllegalParameterErrorEx
          If any value is outside the allowed range
        '''
        return self._HardwareDevice__hw.setSubreflectorRotationOffset(tip, tilt)
    
    def getSubreflectorCommandedPosition(self):
        '''
        Get the last position command sent to the subreflector. If no
        commands have been sent to the subreflector then this will
        return the measured subreflector position. All values are in
        meters.
        '''
        return self._HardwareDevice__hw.getSubreflectorCommandedPosition()
    
    def getSubreflectorCommandedRotation(self):
        '''
        Get the last rotation command sent to the subreflector. If no
        commands have been sent to the subreflector then this will
        return the measured subreflector rotation. All values are in
        radians.
        '''
        return self._HardwareDevice__hw.getSubreflectorCommandedRotation()
    
    def getSubreflectorLimits(self):
        '''
        Returns the movement limits for the subreflector. Five values
        are returned and these are the limits on the x, y, & z axis as
        well as the tip and tilt limits. The subreflector can only be
        commanded to positions within plus or minus this limit. All
        positions are returned in meters and all angles are returned
        in radians.
        '''
        return self._HardwareDevice__hw.getSubreflectorLimits()


    def reloadPointingModel(self):
        '''
        Reloads pointing model from TMCDB
        '''
        self._HardwareDevice__hw.reloadPointingModel()
        return

    def setPointingModel(self, coefficients):
        '''
        Insert a new set of pointing model coefficients
        '''
        self._HardwareDevice__hw.setPointingModel(coefficients)
        return
    
    def setPointingModelCoefficient(self, name, value):
        '''
        Replace the specified coefficient in the pointing model.
        
        This function will throw a:
        * IllegalParameterErrorEx exception if the coefficient is not
          recognized or its value is too big.
        '''
        self._HardwareDevice__hw.setPointingModelCoefficient(name, value)
        return
    
    def getPointingModel(self):
        '''
        Returns a sequence with the complete set of pointing model
        coefficients
        '''
        return self._HardwareDevice__hw.getPointingModel()

    def getPointingModelCoefficient(self, name):
        '''
        Returns the value of the specified pointing model coefficient
        '''
        return self._HardwareDevice__hw.getPointingModelCoefficient(name)

    def enablePointingModel(self, enabled):
        '''
        Enable/Disable ABM pointing model
        '''
        self._HardwareDevice__hw.enablePointingModel(enabled)
        return
    
    def isPointingModelEnabled(self):
        '''
        Returns true is the ABM pointing model is enabled,
        false otherwise
        '''
        return self._HardwareDevice__hw.isPointingModelEnabled()

    def setAuxPointingModel(self, coefficients):
        '''
        Insert a new set of aux pointing model coefficients
        '''
        self._HardwareDevice__hw.setAuxPointingModel(coefficients)
        return
    
    def setAuxPointingModelCoefficient(self, name, value):
        '''
        Replace the specified coefficient in the auxiliary pointing
        model.
        
        This function will throw a:
        * IllegalParameterErrorEx exception if the coefficient is not
          recognized or its value is too big.
        '''
        self._HardwareDevice__hw.setAuxPointingModelCoefficient(name, value)
        return
    
    def getAuxPointingModel(self):
        '''
        Returns a sequence with the complete set of aux pointing model
        coefficients
        '''
        return self._HardwareDevice__hw.getAuxPointingModel()

    def getAuxPointingModelCoefficient(self, name):
        '''
        Returns the value of the specified aux pointing model
        coefficient
        '''
        return self._HardwareDevice__hw.getAuxPointingModelCoefficient(name)

    def enableAuxPointingModel(self, enabled):
        '''
        Enable/Disable ABM pointing model
        '''
        self._HardwareDevice__hw.enableAuxPointingModel(enabled)
        return
    
    def isAuxPointingModelEnabled(self, ):
        '''
        Returns true is the ABM pointing model is enabled, false
        otherwise
        '''
        return self._HardwareDevice__hw.isAuxPointingModelEnabled()

    def isShutterOpen(self):
        '''
        Returns true is shutter is open, false otherwise. Please note
        that false does not imply that the shutter is closed as the
        shutter could be in an error state.
        '''
        return self._HardwareDevice__hw.isShutterOpen()

    def isShutterClosed(self):
        '''
        Returns true is shutter is closed, false otherwise. Please
        note that false does not imply that the shutter is open as the
        shutter could be in an error state.
        '''
        return self._HardwareDevice__hw.isShutterClosed()

    def RESET_ACU_CMD(self, option):
        '''
        Performs a reboot of the ACU or a partial reboot of the
        systems controlled by it.  For this antenna model the options
        are: COMPLETE_REBOOT, METROLOGY_REBOOT, and
        SUBREFLECTOR_REBOOT. The enum types are found in
        MountProduction namespace.
        '''
        self._HardwareDevice__hw.RESET_ACU_CMD(option)
        return
