/******************************************************************************
* "@(#) $Id$"
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005, 2007
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
*/

static char *rcsId="@(#) $Id$";
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <signal.h>
#include <unistd.h> // for sleep
#include <cmath> // for M_PI
#include <maciSimpleClient.h>
#include <PositionStreamConsumer.h>
#include <pthread.h>
#include <ace/Time_Value.h>
#include <MountC.h> // for Control::Mount
#include <ControlDeviceC.h> // for Control::ControlDevice
#include <iostream> // for cerr, cout & endl;
#include <iomanip> // for fixed & setprecision
#include <TETimeUtil.h>
#include <fstream>
#include <string>
#include <vector>
#include <loggingMACROS.h> // for AUTO_TRACE
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::string;

/******************************************************************************
 * Since I'm not very sure where else to put this I will put it here
 * The format of the output is on each line (in order):
 * Time of this set of data
 * The Commanded Az and El (in degrees)
 * The Position of the Telescope (Az and El) 24 ms before the current Timestamp
 * The Position of the Telescope (Az and El) at the current Timestamp.
 * The Encoder value of Az and El (in degrees)
 *****************************************************************************/

const double radToDeg = 180./M_PI;
bool runFlag = true;

void* runLoop(void* simpleClient) {
  ACE_Time_Value t;

  while(runFlag) {
    t.set(1L,0L);
    static_cast<maci::SimpleClient*>(simpleClient)->run(t);
  }
  return NULL;
}

void SigCatcher(int num) {
  runFlag = false;
}

void print(double data, bool isValid, int fieldWidth, ostream& file) {
  file << " " << std::setw(fieldWidth);
  if (isValid) {
    file << data*radToDeg;
  } else {
    file << "invalid";
  }
}


class PositionWriter : public PositionStreamConsumer {
public:
  PositionWriter(const string& antennaName) :
    PositionStreamConsumer(antennaName.c_str()){};

  void write(ostream& file) {
    AUTO_TRACE("PositionStreamWrite::write");
    pthread_mutex_lock(&dataMtx_m);
    if (!sorted_m) sortData();


    for (vector<Control::MountStatusData>::iterator
           iter = dataVector_m.begin(); iter < dataVector_m.end(); iter++) {
      //file << TETimeUtil::toTimeString(iter->timestamp)
      EpochHelper eph(iter->timestamp);

      file << eph.toString(acstime::TSArray, "%Y-%m-%dT%H:%M:%S.%3q", 0, 0).c_str()
           << std::fixed << std::setprecision(6);
      print(iter->azCommanded, iter->azCommandedValid, 11, file);
      print(iter->elCommanded, iter->elCommandedValid, 9, file);
      print(iter->azPrePosition, iter->azPositionsValid, 11, file);
      print(iter->elPrePosition, iter->elPositionsValid, 9, file);
      print(iter->azPosition, iter->azPositionsValid, 11, file);
      print(iter->elPosition, iter->elPositionsValid, 9, file);
      print(iter->azEncoder, iter->azEncoderValid, 11, file);
      print(iter->elEncoder, iter->azEncoderValid, 9, file);
      file << ((iter->onSource) ? " On source": " Off source") << endl;
    }

    dataVector_m.clear();
    pthread_mutex_unlock(&dataMtx_m);
    file.flush();
  }
};


int main(int argc, char *argv[]) {

  // Parse the input arguments and ensure they make sense
  if (argc < 2 || argc > 3) {
    cerr << "PositionStreamClient <AntennaName> <output-file> " << endl;
    cerr << "e.g., PositionStreamClient DV01 positions.dat" << endl;
    cerr << "If no output file is specified data will be sent to the terminal."
         <<endl;
    cerr << "Use Ctrl-C to exit program" << endl;
    return -1;
  }

  //
  // The user has provided required arguments.  We may proceed.
  maci::SimpleClient sc;
  ostream* out = &cout;

  sc.init(argc, argv); // Just pass in the name of the program
  if (!sc.login()) {
    cerr << "Problem connecting to the central services."
         << " Is the system operational?" << endl;;
    return -1;
  }

  if (argc == 3) {
    ostream* fout = new std::ofstream(argv[2], ios_base::out);
    if (!(*fout)) {
      cerr << "Error opening output file: \"" << argv[2] << "\" exiting!"
           << endl;
      sc.logout();
      return -1;
    }
    out = fout;
    cerr << "Position data sent to file: \"" << argv[2] << "\"" << endl;
  } else {
    cerr << "Position data sent to the terminal." << endl;
  }

  // Enable the signal handler
  if (signal(SIGINT, SigCatcher) == SIG_ERR) {
    perror("Unable to catch interupt signals");
    sc.logout();
    return -1;
  }

  // argv[1] is the name of the antenna e.g., DV01. We need to use
  // this to find the name of the antenna component.
  string deviceName;
  Control::Mount_var mount_p = Control::Mount::_nil();

  try {
    string antennaName = "CONTROL/";
    antennaName += argv[1];
    Control::ControlDevice_var antenna_p =
      Control::ControlDevice::_nil();
    try {
      antenna_p = sc.getComponentNonSticky<Control::ControlDevice>
        (antennaName.c_str());
    } catch (maciErrType::CannotGetComponentExImpl&) {
      cerr << "Unable to contact the antenna called " << argv[1] << endl;
      cerr << "Have you specified a valid name antenna name?" << endl;
      sc.logout();
      return -1;
    }

    try {
      deviceName = antenna_p->getSubdeviceName("Mount");
      mount_p = sc.getComponentNonSticky<Control::Mount>(deviceName.c_str());
    } catch(ControlExceptions::IllegalParameterErrorEx) {
      cerr << "Unable to find the mount component." << endl;
      cerr << "Does the specified antenna contain a mount?" << endl;
      sc.releaseComponent(CORBA::String_var(antenna_p->name()).in());
      antenna_p = Control::ControlDevice::_nil();
      sc.logout();
      return -1;
    } catch (maciErrType::CannotGetComponentExImpl) {
      cerr << "Unable to contact the mount ccomponent." << endl;
      cerr << "Has the relevant container crashed?" << endl;
      sc.releaseComponent(CORBA::String_var(antenna_p->name()).in());
      antenna_p = Control::ControlDevice::_nil();
      sc.logout();
      return -1;
    }

  } catch (...) {
    cerr <<"Caught an exception while trying to get the components."
         << " Returning prematurely." << endl;
    sc.logout();
    return -1;
  }

  mount_p->enableMountStatusDataPublication(true);

  PositionWriter         psWriter(argv[1]);
  pthread_t              tID;
  pthread_create(&tID, NULL, runLoop, dynamic_cast<void*>(&sc));

  ACS::Time ts;
  cerr << "Use Ctrl-C to exit program" << endl;
  *out << "Position log for antenna " << argv[1];
  *out << " | AZ_ENCODER_OFFSET: " << std::fixed << std::setprecision(6) << (mount_p->GET_AZ_ENCODER_OFFSET(ts) / 134217728.0 * 360.0);//Offset / 2^27 *360
  *out << " EL_ENCODER_OFFSET: " <<  std::fixed << std::setprecision(6) << (mount_p->GET_EL_ENCODER_OFFSET(ts) / 134217728.0 * 360.0) <<endl;
  *out << "     Time      commanded (Az, El)      mid-TE (Az, El)"
       << "       on-TE (Az, El)        encoder (Az, El) " << endl;

  while (runFlag) {
    psWriter.write(*out);
    sleep(1);
  }
  pthread_join(tID,NULL);

  psWriter.write(*out);
  psWriter.disconnect();
  // There is no need to close the file as its done in the destructor!
  mount_p->enableMountStatusDataPublication(false);
  // There is no need to release the reference to the mount as its non-sticky
  mount_p = Control::Mount::_nil();
  sc.logout();
}
