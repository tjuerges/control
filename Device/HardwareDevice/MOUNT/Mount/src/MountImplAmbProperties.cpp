// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#include <MountImpl.h>
#include <ControlExceptions.h>

using Control::MountImpl;

//-----------------------------------------------------------------------------
void MountImpl::sendOneByteCommand(AmbRelativeAddr address, AmbDataMem_t dataToSend) {
    const char *__METHOD__ = "MountImpl::sendOneByteCommand";
    ACS_TRACE(__METHOD__);

    AmbDataMem_t    data       = dataToSend;
    AmbDataLength_t dataLength = 1;
    sem_t           synchLock;
    ACS::Time       timestamp;
    AmbErrorCode_t  status;
    
    sem_init(&synchLock,0,0);   
    command(address, dataLength, &data, &synchLock,
	    &timestamp, &status);
    sem_wait(&synchLock); // @todo GCH: This is potentially blocking!!!! Timeout
    sem_destroy(&synchLock);
    
    switch (status){
    case AMBERR_NOERR:
	ACS_LOG(LM_SOURCE_INFO,__METHOD__,
		(LM_DEBUG,"RCA: 0x%x succesfully executed", address));
	break;
    default:
	ControlExceptions::CAMBErrorExImpl ex(__FILE__, __LINE__,__METHOD__);
	ex.addData("AMBError",status);
	throw ex.getCAMBErrorEx();
    }
}

