#!/usr/bin/env python
import getopt, sys

def usage():
    print "Usage:"
    print sys.argv[0]+" [-h|--help] [-a|--antenna=<name>] [-n|--node=<node>] [-c|--channel=<channel>] [-i|--icd=<icd version>]"
    print "Where:"
    print "    <name>   : antenna name (DV01, DA41, PM01)"
    print "    <node>   : ACU node number"
    print "    <channel>: ACU channel number"
    print "    <icd>    : icd version"
def error(msg):
    print "Error: "+msg
    usage()
    sys.exit(1)
    
if __name__ == '__main__':

    try:
        opts, args = getopt.getopt(sys.argv[1:], "a:n:c:i:h", ["antenna=", "node=", "channel=", "icd=", "help"])
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    antenna = "Test"
    node = "0"
    channel = "0"
    icd = "none"

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)
        if o in ("-a", "--antenna"):
            antenna = a
        if o in ("-n", "--node"):
            node = a
        if o in ("-c", "--channel"):
            channel = a
        if o in ("-i", "--icd"):
            icd = a

    try:
        int(node)
    except ValueError:
        error("node must be an integer")
    try:
        int(channel)
    except ValueError:
        error("channel must be an integer")
        
    if antenna == "Test" and node == "0" and channel == "0":
        print "Connecting to vertex ACU (simulation)"
        from Mount.VAPrototype.Cmd import ACUCmd as UnitCmd
    elif antenna == "Test" and node == "1" and channel == "0":
        print "Connecting to vertex PTC (simulation)"
        from Mount.VAPrototype.Cmd import PTCCmd as UnitCmd
    elif antenna == "Test" and node == "3" and channel == "0":
        print "Connecting to alcatel ACU (simulation)"
        from Mount.AECPrototype.Cmd import ACUCmd as UnitCmd
    elif icd == "protoVAACU":
        print "Connecting to vertex ACU"
        from Mount.VAPrototype.Cmd import ACUCmd as UnitCmd
    elif icd == "protoVAPTC":
        print "Connecting to vertex PTC"
        from Mount.VAPrototype.Cmd import PTCCmd as UnitCmd
    elif icd == "protoAEC":
        print "Connecting to alcatel ACU"
        from Mount.AECPrototype.Cmd import ACUCmd as UnitCmd
    elif icd == "C5":
        print "Connecting to vertex ACU"
        from Mount.VAProduction.Cmd import ACUCmd as UnitCmd
    else:
        print "Options combination not supported"
        print "antenna ", antenna
        print "node   ", node
        print "channel", channel
        print "icd", icd
        sys.exit(1)

    cmd = UnitCmd()

    if antenna == "Test" and node == "0" and channel == "0":
        cmd.set_angle_conversion_defaults()
    elif antenna == "Test" and node == "3" and channel == "0":
        cmd.set_angle_conversion_defaults(0., 0x40000000, 0x2000000)
    cmd.set_units()
    print "Units are set to the default: ", cmd.units
    cmd.set_communication(antenna, int(channel), int(node))    
    cmd.cmdloop()
