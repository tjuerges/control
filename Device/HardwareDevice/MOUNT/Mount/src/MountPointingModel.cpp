// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) European Southern Observatory, 2007 - 2008 
// (c) Associated Universities Inc., 2008 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountPointingModel.h"
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <TMCDBComponentC.h> // for TMCDB::*PointingModel
#include <ControlExceptions.h>
#include <slamac.h> // for DAS2R & DPI
#include <cmath> // for std::abs etc.
#include <string> 
#include <sstream>

using Control::PointingModel;
using std::map;
using std::string;
using std::abs;
using std::sin;
using std::cos;
using std::atan2;
using std::asin;
using std::ostringstream;
using ControlExceptions::IllegalParameterErrorExImpl;

PointingModel::PointingModel(Logging::Logger::LoggerSmartPtr logger):
    Logging::Loggable(logger),
    enabled_m(false),
    model_m() {
    initializeModel(model_m);
}

void PointingModel::setPointingModel(const TMCDB::PointingModel& model) {
    // C++ treats typedefs as different types. So I have to do a copy here.
    TMCDB::ModelTermSeq_var s = new TMCDB::ModelTermSeq();
    s->length( model.length());
    for (unsigned int i = 0; i < model.length(); i++) {
        s[i] = model[i];
    }
    setPointingModel(s, model_m, getLogger());
}

void PointingModel::zeroCoefficients() {
    zeroModel(model_m);
}

void PointingModel::zeroModel(map<string, double>& model) {
    map<string, double>::iterator iter;   
    for(iter = model.begin(); iter != model.end(); ++iter) {
        iter->second = 0.0;
    }
}

void PointingModel::setCoefficient(string name, double value) {
    setCoefficient(name, value, model_m, getLogger());
}

void PointingModel::setCoefficient(string name, double value, map<string, double>& model,
                                   Logging::Logger::LoggerSmartPtr logger) {
    // Make the comparison case insensitive by converting string to upper case.
    for (string::iterator p = name.begin(); p !=  name.end(); ++p) {
        *p = std::toupper(*p);
    }
 
    if (model.count(name) == 0) {
        string msg = "Cannot change the pointing model coefficient called '";
        msg += name + "' as it is an unknown coefficient.";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR_WITH_LOGGER(LM_ERROR, msg, logger);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    // A large value of CA will cause problems in the applyModel function.
    const double maxCoeff = ((name == "CA") ? 1.1 : 5.0 ) * 60 * 60;
    if (abs(value) > maxCoeff) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ostringstream msg;
        msg << "Cannot change the pointing model as the magnitude of the " 
            << name << " pointing coefficient is too big. Its " << value 
            << ", arc-seconds and the maximum is " << maxCoeff 
            << " arc-seconds.";
        LOG_TO_OPERATOR_WITH_LOGGER(LM_ERROR, msg.str(), logger);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    model[name] = value*DAS2R;
}

bool PointingModel::applyModel(
    double observedAz,   double observedEl,
    double& correctedAz, double& correctedEl) {
 
    if (!enabled_m) {
        correctedAz = observedAz;    
        correctedEl = observedEl;
        return false;
    }
 
    bool badCorrection = false;
    double a = observedAz;
    double e = observedEl;
    const double pmCA = model_m["CA"];
    {
        const double se = sin(e);
        const double cpmCA = cos(pmCA);
        if (se <= cpmCA) {
            e = asin(se/cpmCA);
        } else { // This branch should never be taken..
            e = M_PI/2;
            badCorrection = true;
        }
    }
 
    double deltaCA;
    {
        const double ce = cos(e);
        const double spmCA = sin(pmCA);
        if (spmCA <= ce) {
            deltaCA = asin(spmCA/ce) * ce;
        } else { // This branch should never be taken... 
            deltaCA = M_PI/2*ce;
            badCorrection = true;
        }
    }
    double sa = sin(a);
    double ca = cos(a);
    const double ds = (
        - model_m["AW"]*ca
        - model_m["AN"]*sa
        - model_m["NPAE"]) * sin(e)
        - deltaCA;
 
    e -= model_m["AW"]*sa
       - model_m["AN"]*ca;
 
    // keyhole -> +- 2.5 arc-minutes
    if (e < (DPI/2-2.5*60*DAS2R) || 
        e > (DPI/2+2.5*60*DAS2R)) {
        a -= atan2(ds,cos(e));
    } else if (e < DPI/2) {
        a -= atan2(ds,cos(DPI/2-2.5*60*DAS2R));
        badCorrection = true;
    } else if (e > DPI/2) {
        a -= atan2(ds,cos(DPI/2+2.5*60*DAS2R));
        badCorrection = true;
    }
 
    const double a2 = a+a;
    const double a3 = a+a+a;
    sa = sin(a); // recompute this as 'a' may have changed
    const double s2a = sin(a2);
    const double s3a = sin(a3);
    ca = cos(a); // recompute this as 'a' may have changed
    const double c2a = cos(a2);
    const double c3a = cos(a3);
 
    e -= 
        - model_m["HECA3"]*c3a
        + model_m["HESA3"]*s3a
        + model_m["HECA2"]*c2a
        - model_m["HESA2"]*s2a
        + model_m["HESA"]*sa
        + model_m["HECE"]*cos(e) // As e has changed cos(e) is not the same as ce
        + model_m["HESE"]*sin(e) 
        + model_m["IE"];
 
    a -= model_m["HACA3"]*c3a
        + model_m["HASA2"]*s2a
        - model_m["HACA2"]*c2a
        + model_m["HACA"]*ca
        - model_m["HASA"]*sa
        - model_m["IA"]
        - model_m["HASA3"]*s3a;
 
    correctedAz = a;
    correctedEl = e;
    return badCorrection;
}

TMCDB::PointingModel* PointingModel::getPointingModel() {
    TMCDB::PointingModel_var model = new TMCDB::PointingModel();
    model->length(model_m.size());
    map<string, double>::const_iterator iter;
    unsigned index = 0;
    for (iter = model_m.begin(); iter != model_m.end(); 
         ++iter, ++index) {
        TMCDB::ModelTerm& coeff = model[index];
        coeff.name = CORBA::string_dup((iter->first).c_str());
        coeff.value = (iter->second)/DAS2R;
    }
    return model._retn();
}

double PointingModel::getCoefficient(const string& coeffName) {
    // Make the comparison case insensitive by converting string to upper case.
    string name = coeffName;
    for (string::iterator p = name.begin(); p !=  name.end(); ++p) {
        *p = std::toupper(*p);
    }
    if (model_m.count(name) == 0) {
        string msg = "Cannot get the pointing model coefficient called '";
        msg += name + "' as it is not known. Please check the name.";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    return model_m[name]/DAS2R;
}

void PointingModel::initializeModel(map<string, double>& model) {
    model["IA"] = 0.0;
    model["IE"] = 0.0;
    model["CA"] = 0.0;
    model["AN"] = 0.0;
    model["AW"] = 0.0;
    model["NPAE"] = 0.0;

    model["HASA"] = 0.0;
    model["HACA"] = 0.0;
    model["HASA2"] = 0.0;
    model["HACA2"] = 0.0;
    model["HASA3"] = 0.0;
    model["HACA3"] = 0.0;

    model["HESE"] = 0.0;
    model["HECE"] = 0.0;

    model["HESA"] = 0.0;
    model["HESA2"] = 0.0;
    model["HECA2"] = 0.0;
    model["HESA3"] = 0.0;
    model["HECA3"] = 0.0;
}

void PointingModel::setPointingModel(const TMCDB::ModelTermSeq& inModel, 
                                     map<string, double>& outModel,
                                     Logging::Logger::LoggerSmartPtr logger) {
    zeroModel(outModel);
    // Set the non zero values to whatever is specified.
    for (unsigned int index = 0; index < inModel.length(); index++) {
        const TMCDB::ModelTerm& coeff = inModel[index];
        setCoefficient(string(coeff.name), coeff.value, outModel, logger);
    }
}
