// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#include <PositionStreamConsumer.h>
#include <TETimeUtil.h>
#include <sstream> // for ostringstream
#include <ControlBasicInterfacesC.h> // for CHANNELNAME_CONTROLREALTIME
#include <ControlExceptions.h>
#include <LogToAudience.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using std::string;
using std::ostringstream;
using std::endl;
using std::vector;
using ControlExceptions::IllegalParameterErrorEx;
using ControlExceptions::IllegalParameterErrorExImpl;
using Control::MountStatusData;

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

PositionStreamConsumer::PositionStreamConsumer(const string& antennaName)
    :nc::SimpleConsumer<MountStatusData>
(Control::CHANNELNAME_CONTROLREALTIME),
     dataVector_m(0),
     sorted_m(true),
     antennaName_m(antennaName),
     startTime_m(0),
     stopTime_m(0),
     pastStopTime_m(false)
{
    pthread_mutex_init(&dataMtx_m, NULL);
    addSubscription<MountStatusData>
        (&PositionStreamConsumer_eventHandler, dynamic_cast<void*>(this));
    consumerReady();
}

void PositionStreamConsumer::disconnect() {
    pthread_mutex_destroy(&dataMtx_m);
    nc::SimpleConsumer<MountStatusData>::disconnect();
}

void PositionStreamConsumer_eventHandler(MountStatusData data,
                                         void *reference){
    static_cast<PositionStreamConsumer*>(reference)->processData(data);
}

void 
PositionStreamConsumer::processData(MountStatusData data){

    pthread_mutex_lock(&dataMtx_m);
    if (!strcmp(data.antennaName, antennaName_m.c_str())) {
        const ACS::Time thisTime = data.timestamp;
        if ((thisTime >= startTime_m) && 
            ((stopTime_m == 0) || (thisTime < stopTime_m))) {
            if (sorted_m &&
                !dataVector_m.empty() &&
                dataVector_m.back().timestamp > thisTime) {
                sorted_m = false;
            }
            dataVector_m.push_back(data);
        } else if (!pastStopTime_m && thisTime >= stopTime_m) {
            pastStopTime_m = true;
        }
    }
    pthread_mutex_unlock(&dataMtx_m);
}

void PositionStreamConsumer::clear(){
    pthread_mutex_lock(&dataMtx_m);
    dataVector_m.clear();
    sorted_m = true;
    pastStopTime_m = false;
    pthread_mutex_unlock(&dataMtx_m);
}

unsigned long PositionStreamConsumer::size(){
    return dataVector_m.size();
}

void PositionStreamConsumer::setTimeRange(ACS::Time startTime, 
                                          ACS::Time stopTime) {
    pthread_mutex_lock(&dataMtx_m);
    startTime_m = startTime;
    stopTime_m = stopTime;
    pthread_mutex_unlock(&dataMtx_m);
    clear();
}

bool PositionStreamConsumer::pastStopTime() {
    bool retVal;
    pthread_mutex_lock(&dataMtx_m);
    retVal = pastStopTime_m;
    pthread_mutex_unlock(&dataMtx_m);
    return retVal;
}

bool PositionStreamConsumer::pastStopTime(ACS::Time stopTime) {
    bool gotDataAtEnd = false;
    // I need to subtract a TE from the stop time as we may not be collecting
    // any more data beyond the specified stop time. Subtracting a TE ensures
    // we always see the last value we do collect.
    stopTime -= TETimeUtil::TE_PERIOD_DURATION.value;
    pthread_mutex_lock(&dataMtx_m);
    if (!dataVector_m.empty()) {
        sortData();
        const ACS::Time lastTime = dataVector_m.back().timestamp;
        if (lastTime >= stopTime) {
            gotDataAtEnd = true;
        }
    }
    pthread_mutex_unlock(&dataMtx_m);
    return gotDataAtEnd;
}

//TODO. Put the common code in the get{Act,Cmd,Enc}Position functions into
//separate functions.
void PositionStreamConsumer::getEncPosition(const ACS::Time& timestamp, 
                                            double& az, 
                                            double& el) {
    pthread_mutex_lock(&dataMtx_m);

    // Check that we have some data in the buffer.
    if (dataVector_m.size() == 0) {
        pthread_mutex_unlock(&dataMtx_m);

        string message;
        message += "There is no data in the positions buffer!";
        message += " Have you turned on rapid position reporting? ";
        message += " No data returned.";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message);
        ex.log();
        throw ex;
    }

    sortData();

    // The lwrBnd variable points to the element of the dataVector that has the
    // same timestamp as the requested timestamp. If there is no element that
    // has the requested timestamp it points to the element with the next
    // larger timestamp.
    vector<MountStatusData>::const_iterator lwrBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), timestamp,
                    PositionStreamConsumer::Compare_MountStatusData_AcsTime());

    // Check if the requested timestamp is out of bounds.
    if (lwrBnd == dataVector_m.end()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is beyond than end of the available data (which ends at ";
        const ACS::Time lastTime = 
            dataVector_m[dataVector_m.size() - 1].timestamp;
        message << TETimeUtil::toTimeString(lastTime) << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    if (lwrBnd == dataVector_m.begin()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is before than start of the available data "
                << "(which starts at ";
        const ACS::Time sTime = 
            dataVector_m[0].timestamp - TETimeUtil::TE_PERIOD_ACS/2;
        message << TETimeUtil::toTimeString(sTime) << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    // Now handle a few boundry cases that would otherwise cause problems for
    // the interpolation at the end of this function.
    if (lwrBnd->timestamp == timestamp) {
        // In this case the requested timestamp is a TE
        az = lwrBnd->azEncoder;
        el = lwrBnd->elEncoder;
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is coincident with a time event. No interpolation required.";
        LOG_TO_DEVELOPER(LM_DEBUG, message.str());
        return;
    }

    // Linear interpolate between the two nearest positions

    const double az1 = lwrBnd->azEncoder;
    const double el1 = lwrBnd->elEncoder;

    // We know the iterator is not at the beginning (because we tested for this
    // condition earlier) so this always works.
    lwrBnd--;

    const double az0 = lwrBnd->azEncoder;
    const double el0 = lwrBnd->elEncoder;

    // I use long doubles to avoid precision loss when subtracting two nearly
    // equal big numbers (like the timestamps).
    const long double dt = (static_cast<long double>(timestamp) - 
                            static_cast<long double>(lwrBnd->timestamp))/
        static_cast<long double>(TETimeUtil::TE_PERIOD_ACS);

    pthread_mutex_unlock(&dataMtx_m);

    az = (az1 - az0) * dt + az0;
    el = (el1 - el0) * dt + el0;
}

void PositionStreamConsumer::getActPosition(const ACS::Time& timestamp, 
                                            double& az, double& el) {
    pthread_mutex_lock(&dataMtx_m);

    // Check that we have some data in the buffer.
    if (dataVector_m.size() == 0) {
        pthread_mutex_unlock(&dataMtx_m);

        string message;
        message += "There is no data in the positions buffer!";
        message += " Have you turned on rapid position reporting?";
        message += " No data returned.";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message);
        ex.log();
        throw ex;
    }

    sortData();

    // The lwrBnd variable points to the element of the dataVector that has the
    // same timestamp as the requested timestamp. If there is no element that
    // has the requested timestamp it points to the element with the next
    // larger timestamp.
    vector<MountStatusData>::const_iterator lwrBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), timestamp,
                    PositionStreamConsumer::Compare_MountStatusData_AcsTime());

    // Check if the requested timestamp is out of bounds.
    if (lwrBnd == dataVector_m.end()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is beyond than end of the available data (which ends at ";
        const ACS::Time lastTime = 
            dataVector_m[dataVector_m.size() - 1].timestamp;
        message << TETimeUtil::toTimeString(lastTime) << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    if (lwrBnd == dataVector_m.begin() && 
        timestamp < lwrBnd->timestamp - (TETimeUtil::TE_PERIOD_ACS/2)) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is before the start of the available data "
                << "(which starts at "
                << TETimeUtil::toTimeString
            (dataVector_m[0].timestamp - TETimeUtil::TE_PERIOD_ACS/2)
                << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    // Now handle a few boundry cases that would otherwise cause problems for
    // the interpolation at the end of this function.
    if (lwrBnd->timestamp == timestamp) {
        // In this case the requested timestamp is a TE
        az = lwrBnd->azPosition;
        el = lwrBnd->elPosition;
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is coincident with a time event. No interpolation required.";
        LOG_TO_DEVELOPER(LM_DEBUG, message.str());
        return;
    }

    if (lwrBnd->timestamp - (TETimeUtil::TE_PERIOD_ACS/2) == timestamp) {
        // This is the case if the requested timestamp is pre-TE
        az = lwrBnd->azPrePosition;
        el = lwrBnd->elPrePosition;
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is coincident with a mid-TE. No interpolation required.";
        LOG_TO_DEVELOPER(LM_DEBUG, message.str());
        return;
    }

    // Linear interpolate between the two nearest positions

    // I use long doubles to avoid precision loss when subtracting two nearly
    // equal big numbers (like the timestamps).
    const long double te2 =
        static_cast<long double>(TETimeUtil::TE_PERIOD_ACS/2);
    const long double t = static_cast<long double>(timestamp);
    long double t0;
    double az0, el0, az1, el1;
    if ((lwrBnd->timestamp - timestamp) < (TETimeUtil::TE_PERIOD_ACS/2)) {
        // requested time is in the second half of the TE
        az0 = lwrBnd->azPrePosition;
        el0 = lwrBnd->elPrePosition;
        az1 = lwrBnd->azPosition;
        el1 = lwrBnd->elPosition;
        t0 = static_cast<long double>(lwrBnd->timestamp) - te2;
    } else {
        // requested time is in the first half of the TE.
        // need to use preTE position from selected element and TE position
        // from previous element.
        az1 = lwrBnd->azPrePosition;
        el1 = lwrBnd->elPrePosition;
        lwrBnd--; 
        // This should never fail as we cannot be in the first half of a TE
        // when lwrBnd is pointing to the first element.
        az0 = lwrBnd->azPosition;
        el0 = lwrBnd->elPosition;
        t0 = static_cast<long double>(lwrBnd->timestamp);
    }
    pthread_mutex_unlock(&dataMtx_m);

    az = (az1 - az0)/te2 * (t - t0) + az0;
    el = (el1 - el0)/te2 * (t - t0) + el0;
}

void PositionStreamConsumer::getCmdPosition(const ACS::Time& timestamp, 
                                            double& az, 
                                            double& el) {
    pthread_mutex_lock(&dataMtx_m);

    // Check that we have some data in the buffer.
    if (dataVector_m.size() == 0) {
        pthread_mutex_unlock(&dataMtx_m);

        string message;
        message += "There is no data in the positions buffer!";
        message += " Have you turned on rapid position reporting? ";
        message += " No data returned.";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message);
        ex.log();
        throw ex;
    }

    sortData();

    // The lwrBnd variable points to the element of the dataVector that has the
    // same timestamp as the requested timestamp. If there is no element that
    // has the requested timestamp it points to the element with the next
    // larger timestamp.
    vector<MountStatusData>::const_iterator lwrBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), timestamp,
                    PositionStreamConsumer::Compare_MountStatusData_AcsTime());

    // Check if the requested timestamp is out of bounds.
    if (lwrBnd == dataVector_m.end()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is beyond than end of the available data (which ends at ";
        const ACS::Time lastTime = 
            dataVector_m[dataVector_m.size() - 1].timestamp;
        message << TETimeUtil::toTimeString(lastTime) << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    if (lwrBnd == dataVector_m.begin()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is before than start of the available data "
                << "(which starts at ";
        const ACS::Time sTime = 
            dataVector_m[0].timestamp - TETimeUtil::TE_PERIOD_ACS/2;
        message << TETimeUtil::toTimeString(sTime) << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    // Now handle a few boundry cases that would otherwise cause problems for
    // the interpolation at the end of this function.
    if (lwrBnd->timestamp == timestamp) {
        // In this case the requested timestamp is a TE
        az = lwrBnd->azCommanded;
        el = lwrBnd->elCommanded;
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is coincident with a time event. No interpolation required.";
        LOG_TO_DEVELOPER(LM_DEBUG, message.str());
        return;
    }

    // Linear interpolate between the two nearest positions

    const double az1 = lwrBnd->azCommanded;
    const double el1 = lwrBnd->elCommanded;

    // We know the iterator is not at the beginning (because we tested for this
    // condition earlier) so this always works.
    lwrBnd--;

    const double az0 = lwrBnd->azCommanded;
    const double el0 = lwrBnd->elCommanded;

    // I use long doubles to avoid precision loss when subtracting two nearly
    // equal big numbers (like the timestamps).
    const long double dt = (static_cast<long double>(timestamp) - 
                            static_cast<long double>(lwrBnd->timestamp))/
        static_cast<long double>(TETimeUtil::TE_PERIOD_ACS);

    pthread_mutex_unlock(&dataMtx_m);

    az = (az1 - az0) * dt + az0;
    el = (el1 - el0) * dt + el0;
}

void PositionStreamConsumer::getPMCorrection(const ACS::Time& timestamp, 
                                             double& az, double& el) {
    pthread_mutex_lock(&dataMtx_m);

    // Check that we have some data in the buffer.
    if (dataVector_m.size() == 0) {
        pthread_mutex_unlock(&dataMtx_m);

        string message;
        message += "There is no data in the positions buffer!";
        message += " Have you turned on rapid position reporting? ";
        message += " No data returned.";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message);
        ex.log();
        throw ex;
    }

    sortData();

    // The lwrBnd variable points to the element of the dataVector that has the
    // same timestamp as the requested timestamp. If there is no element that
    // has the requested timestamp it points to the element with the next
    // larger timestamp.
    vector<MountStatusData>::const_iterator lwrBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), timestamp,
                    PositionStreamConsumer::Compare_MountStatusData_AcsTime());

    // Check if the requested timestamp is out of bounds.
    if (lwrBnd == dataVector_m.end()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is beyond than end of the available data (which ends at ";
        const ACS::Time lastTime = 
            dataVector_m[dataVector_m.size() - 1].timestamp;
        message << TETimeUtil::toTimeString(lastTime) << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    if (lwrBnd == dataVector_m.begin()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is before than start of the available data "
                << "(which starts at ";
        const ACS::Time sTime = 
            dataVector_m[0].timestamp - TETimeUtil::TE_PERIOD_ACS/2;
        message << TETimeUtil::toTimeString(sTime) << ").";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    // Now handle a few boundry cases that would otherwise cause problems for
    // the interpolation at the end of this function.
    if (lwrBnd->timestamp == timestamp) {
        // In this case the requested timestamp is a TE
        az = 0.0;
        el = 0.0;
        if (lwrBnd->pointingModel) {
            az += lwrBnd->azPointingModelCorrection;
            el += lwrBnd->elPointingModelCorrection;
        }
        if (lwrBnd->auxPointingModel) {
            az += lwrBnd->azAuxPointingModelCorrection;
            el += lwrBnd->elAuxPointingModelCorrection;
        }
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested position (at time "
                << TETimeUtil::toTimeString(timestamp)
                << ") is coincident with a time event. No interpolation required.";
        LOG_TO_DEVELOPER(LM_DEBUG, message.str());
        return;
    }

    // Linear interpolate between the two nearest positions

    bool usingPM = false;
    bool usingAuxPM = false;
    double az1 = 0.0;
    double el1 = 0.0;

    if (lwrBnd->pointingModel) {
        usingPM = true;
        az1 += lwrBnd->azPointingModelCorrection;
        el1 += lwrBnd->elPointingModelCorrection;
    }
    if (lwrBnd->auxPointingModel) { 
        usingAuxPM = true;
        az1 += lwrBnd->azAuxPointingModelCorrection;
        el1 += lwrBnd->elAuxPointingModelCorrection;
    }

    // We know the iterator is not at the beginning (because we tested for this
    // condition earlier) so this always works.
    lwrBnd--;

    double az0 = 0.0;
    double el0 = 0.0;
    if ((usingPM != lwrBnd->pointingModel) ||
        (usingAuxPM != lwrBnd->auxPointingModel)) {
        string msg("Pointing model use changes in the middle of a sub-scan.");
        msg += " Pointing model correction is likely to be incorrect";
        LOG_TO_OPERATOR(LM_INFO, msg);
    }

    if (lwrBnd->pointingModel) {
        az0 += lwrBnd->azPointingModelCorrection;
        el0 += lwrBnd->elPointingModelCorrection;
    }
    if (lwrBnd->auxPointingModel) {
        az0 += lwrBnd->azAuxPointingModelCorrection;
        el0 += lwrBnd->elAuxPointingModelCorrection;
    }

    // I use long doubles to avoid precision loss when subtracting two nearly
    // equal big numbers (like the timestamps).
    const long double dt = (static_cast<long double>(timestamp) - 
                            static_cast<long double>(lwrBnd->timestamp))/
        static_cast<long double>(TETimeUtil::TE_PERIOD_ACS);

    pthread_mutex_unlock(&dataMtx_m);

    az = (az1 - az0) * dt + az0;
    el = (el1 - el0) * dt + el0;
}

void PositionStreamConsumer::
getAvgCmdPosition(const ACS::Time& startTime, const ACS::Time& stopTime,
                  double& az, double& el) {
    vector<double> azvec; 
    vector<double> elvec;
    getCmdPosition(startTime, stopTime, azvec, elvec);
  
    //getCmdPosition(const ACS::Time&, const ACS::Time&, 
    //                 vector<double>&, vector<double>&)
    //Return when dataVector.size() == 0, hence we have to check here.
    if (azvec.size() == 0 || elvec.size() == 0 ) {
        string message;
        message += "there is no data in the positions buffer!";
        message += " have you turned on rapid position reporting? ";
        message += " no data returned.";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message);
        ex.log();
        throw ex;
    }

    avgVector(azvec, az);
    avgVector(elvec, el);
    return;
}

void PositionStreamConsumer::
getCmdPosition(const ACS::Time& startTime, const ACS::Time& stopTime, 
               vector<double>& az, vector<double>& el) {
    az.clear(); el.clear();

    // Check that startTime is less than stopTime 
    if (startTime > stopTime) {
        ControlExceptions::IllegalParameterErrorExImpl 
            ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ParameterName", "startTime");
        ex.addData("Value", TETimeUtil::toTimeString(startTime));

        ostringstream message;
        message << "The requested start time ("
                << TETimeUtil::toTimeString(startTime)
                << ") is greater than the stop time ("
                << TETimeUtil::toTimeString(stopTime)
                << "). No data returned.";
        ex.addData("ParameterName1", "stopTime");
        ex.addData("Value1", TETimeUtil::toTimeString(stopTime));
        ex.addData("ValidRange", "startTime < stopTime");
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    pthread_mutex_lock(&dataMtx_m);
    sortData();

    // Check that we have some data in the buffer.
    if (dataVector_m.size() == 0) {
        pthread_mutex_unlock(&dataMtx_m);
        string message;
        message += "There is no data in the positions buffer!";
        message += " Have you turned on rapid position reporting?";
        message += " No data returned.";
        LOG_TO_DEVELOPER(LM_ERROR, message);
        return;
    }

    vector<MountStatusData>::const_iterator startBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), startTime,
                    Compare_MountStatusData_AcsTime());
    if (startBnd == dataVector_m.end()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested start time ("
                << TETimeUtil::toTimeString(startTime)
                << ") is beyond than end the available data (which ends at "
                << TETimeUtil::toTimeString(startBnd->timestamp)
                << "). No data returned.";
        LOG_TO_DEVELOPER(LM_WARNING, message.str());
        return;
    }

    vector<MountStatusData>::const_iterator stopBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), stopTime,
                    Compare_MountStatusData_AcsTime());
    if (stopBnd == dataVector_m.begin()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested stop time ("
                << TETimeUtil::toTimeString(stopTime)
                << ") is before the start of any available data (which starts at "
                << TETimeUtil::toTimeString(startBnd->timestamp)
                << "). No data returned.";
        LOG_TO_DEVELOPER(LM_WARNING, message.str());
        return;
    }

    while (startBnd != stopBnd) {
        az.push_back(startBnd->azCommanded);
        el.push_back(startBnd->elCommanded);
        ++startBnd;
    }
    pthread_mutex_unlock(&dataMtx_m);
}

void PositionStreamConsumer::
getTimes(const ACS::Time& startTime, const ACS::Time& stopTime, 
         vector<ACS::Time>& times) {
    times.clear(); 

    // Check that startTime is less than stopTime 
    if (startTime > stopTime) {
        ControlExceptions::IllegalParameterErrorExImpl 
            ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ParameterName", "startTime");
        ex.addData("Value", TETimeUtil::toTimeString(startTime));

        ostringstream message;
        message << "The requested start time ("
                << TETimeUtil::toTimeString(startTime)
                << ") is greater than the stop time ("
                << TETimeUtil::toTimeString(stopTime)
                << "). No data returned.";
        ex.addData("ParameterName1", "stopTime");
        ex.addData("Value1", TETimeUtil::toTimeString(stopTime));
        ex.addData("ValidRange", "startTime < stopTime");
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    pthread_mutex_lock(&dataMtx_m);
    sortData();

    // Check that we have some data in the buffer.
    if (dataVector_m.size() == 0) {
        pthread_mutex_unlock(&dataMtx_m);
        string message;
        message += "There is no data in the positions buffer!";
        message += " Have you turned on rapid position reporting?";
        message += " No data returned.";
        LOG_TO_DEVELOPER(LM_ERROR, message);
        return;
    }

    vector<MountStatusData>::const_iterator startBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), startTime,
                    Compare_MountStatusData_AcsTime());
    if (startBnd == dataVector_m.end()) {
        ostringstream message;
        message << "The requested start time ("
                << TETimeUtil::toTimeString(startTime)
                << ") is beyond than end the available data (which ends at "
                << TETimeUtil::toTimeString(startBnd->timestamp)
                << "). No data returned.";
        LOG_TO_DEVELOPER(LM_WARNING, message.str());
        pthread_mutex_unlock(&dataMtx_m);
        return;
    }

    vector<MountStatusData>::const_iterator stopBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), stopTime,
                    Compare_MountStatusData_AcsTime());
    if (stopBnd == dataVector_m.begin()) {
        ostringstream message;
        message << "The requested stop time ("
                << TETimeUtil::toTimeString(stopTime)
                << ") is before the start of any available data (which starts at "
                << TETimeUtil::toTimeString(startBnd->timestamp)
                << "). No data returned.";
        LOG_TO_DEVELOPER(LM_WARNING, message.str());
        pthread_mutex_unlock(&dataMtx_m);
        return;
    }

    while (startBnd != stopBnd) {
        times.push_back(startBnd->timestamp);
        ++startBnd;
    }
    pthread_mutex_unlock(&dataMtx_m);
}

void PositionStreamConsumer::
getAvgActPosition(const ACS::Time& startTime, const ACS::Time& stopTime,
                  double& az, double& el) {
    vector<double> azvec; 
    vector<double> elvec;
    getActPosition(startTime, stopTime, azvec, elvec);

    //getActPosition(const ACS::Time&, const ACS::Time&, 
    //                 vector<double>&, vector<double>&)
    //Return when dataVector.size() == 0, hence we have to check here.
    if (azvec.size() == 0 || elvec.size() == 0 ) {
        string message;
        message += "there is no data in the positions buffer!";
        message += " have you turned on rapid position reporting? ";
        message += " no data returned.";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message);
        ex.log();
        throw ex;
    }

    avgVector(azvec, az);
    avgVector(elvec, el);
    return;
}

void PositionStreamConsumer::
getActPosition(const ACS::Time& startTime, const ACS::Time& stopTime, 
               vector<double>& az, vector<double>& el) {
    az.clear(); el.clear();

    // Check that startTime is less than stopTime 
    if (startTime > stopTime) {
        ControlExceptions::IllegalParameterErrorExImpl 
            ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ParameterName", "startTime");
        ex.addData("Value", TETimeUtil::toTimeString(startTime));

        ostringstream message;
        message << "The requested start time ("
                << TETimeUtil::toTimeString(startTime)
                << ") is greater than the stop time ("
                << TETimeUtil::toTimeString(stopTime)
                << "). No data returned.";
        ex.addData("ParameterName1", "stopTime");
        ex.addData("Value1", TETimeUtil::toTimeString(stopTime));
        ex.addData("ValidRange", "startTime < stopTime");
        ex.addData("Detail", message.str());
        ex.log();
        throw ex;
    }

    pthread_mutex_lock(&dataMtx_m);
    sortData();

    // Check that we have some data in the buffer.
    if (dataVector_m.size() == 0) {
        pthread_mutex_unlock(&dataMtx_m);
        string message;
        message += "There is no data in the positions buffer!";
        message += " Have you turned on rapid position reporting?";
        message += " No data returned.";
        LOG_TO_DEVELOPER(LM_ERROR, message);
        return;
    }

    vector<MountStatusData>::const_iterator startBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), startTime,
                    Compare_MountStatusData_AcsTime());
    if (startBnd == dataVector_m.end()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested start time ("
                << TETimeUtil::toTimeString(startTime)
                << ") is beyond than end the available data (which ends at "
                << TETimeUtil::toTimeString(startBnd->timestamp)
                << "). No data returned.";
        LOG_TO_DEVELOPER(LM_WARNING, message.str());
        return;
    }

    vector<MountStatusData>::const_iterator stopBnd =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), stopTime,
                    Compare_MountStatusData_AcsTime());
    if (stopBnd == dataVector_m.begin()) {
        pthread_mutex_unlock(&dataMtx_m);
        ostringstream message;
        message << "The requested stop time ("
                << TETimeUtil::toTimeString(stopTime)
                << ") is before the start of any available data (which starts at "
                << TETimeUtil::toTimeString(startBnd->timestamp)
                << "). No data returned.";
        LOG_TO_DEVELOPER(LM_WARNING, message.str());
        return;
    }

    const bool atEnd = (stopBnd == dataVector_m.end() ? true: false);
    while (startBnd != stopBnd) {
        az.push_back(startBnd->azPosition);
        el.push_back(startBnd->elPosition);
        ++startBnd;
        if (!(atEnd && (startBnd == stopBnd))) {
            az.push_back(startBnd->azPrePosition);
            el.push_back(startBnd->elPrePosition);
        }
    }
    pthread_mutex_unlock(&dataMtx_m);
}

vector<MountStatusData> PositionStreamConsumer::
getDataRange(const ACS::Time& startTime, const ACS::Time& stopTime) {
    // Check that startTime < stopTime;
    if (startTime >= stopTime) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ostringstream msg;
        msg << "Cannot collect pointing data as the start time of " 
            << TETimeUtil::toTimeString(startTime) 
            << " is not less than the stop time of "
            << TETimeUtil::toTimeString(stopTime) ;
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex;
    }

    pthread_mutex_lock(&dataMtx_m);
    sortData();

    // The firstElem variable points to the element of the dataVector that has
    // the same timestamp as the requested timestamp. If there is no element
    // that has the requested timestamp it points to the element with the next
    // larger timestamp.
    vector<MountStatusData>::const_iterator firstElem =
        lower_bound(dataVector_m.begin(), dataVector_m.end(), startTime,
                    PositionStreamConsumer::Compare_MountStatusData_AcsTime());

    // The lastElem variable points to the next element of the dataVector that
    // has a timestamp greater than the requested timestamp. It will point one
    // beyond the end of dataVector if all elements in dataVector have a
    // timestamp less than than stoptime
    vector<MountStatusData>::const_iterator lastElem =
        upper_bound(dataVector_m.begin(), dataVector_m.end(), stopTime,
                    PositionStreamConsumer::Compare_MountStatusData_AcsTime());
  
    const vector<MountStatusData> retVal(firstElem, lastElem);
    pthread_mutex_unlock(&dataMtx_m);

    return retVal;
}

MountStatusData PositionStreamConsumer::
average(const vector<MountStatusData>& positions) {
    if (positions.empty()) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Supplied vector of positions cannot be zero length.";
        ex.addData("Detail", msg);
        ex.log();
        throw ex;
    }

    MountStatusData avg;
    avg.antennaName = positions.front().antennaName;
    avg.timestamp = 0;
    avg.onSource = true;
    avg.azCommanded = avg.elCommanded = 0;
    avg.azCommandedValid = avg.elCommandedValid = false;
    int azCommandedCtr = 0;
    int elCommandedCtr = 0;
    avg.azPrePosition = avg.elPrePosition = 0;
    avg.azPosition = avg.elPosition = 0;
    avg.azPositionsValid = avg.elPositionsValid = false;
    int azPositionsCtr = 0;
    int elPositionsCtr = 0;
    avg.azPointingModelCorrection = avg.elPointingModelCorrection = 0;
    avg.pointingModel = true;
    int pointingModelCorrectionCtr = 0;
    avg.azAuxPointingModelCorrection = avg.elAuxPointingModelCorrection = 0;
    avg.auxPointingModel = true; 
    int auxPointingModelCorrectionCtr = 0;
    avg.azEncoder = avg.elEncoder = 0;
    avg.azEncoderValid = avg.elEncoderValid = false;
    int azEncoderCtr = 0;
    int elEncoderCtr = 0;

    avg.subrefPositionValid = false;
    avg.subrefX = avg.subrefY = avg.subrefZ = 0.0;
    int subrefPositionCtr = 0;
    avg.subrefRotationValid = false;
    avg.subrefTip = avg.subrefTilt = 0.0;
    int subrefRotationCtr = 0;

    avg.subrefPositionCmdValid = false;
    avg.subrefCmdX = avg.subrefCmdY = avg.subrefCmdZ = 0.0;
    int subrefPositionCmdCtr = 0;
    avg.subrefRotationCmdValid = false;
    avg.subrefCmdTip = avg.subrefCmdTilt = 0.0;
    int subrefRotationCmdCtr = 0;

    for (vector<MountStatusData>::const_iterator p = positions.begin(); 
         p != positions.end(); ++p) {
        avg.timestamp += p->timestamp/positions.size();
        avg.onSource = avg.onSource && p->onSource;
        if (p->azCommandedValid) {
            avg.azCommanded += p->azCommanded;
            azCommandedCtr++;
            avg.azCommandedValid = true;
        }
        if (p->elCommandedValid) {
            avg.elCommanded += p->elCommanded;
            elCommandedCtr++;
            avg.elCommandedValid = true;
        }
        if (p->azPositionsValid) {
            avg.azPosition += p->azPosition;
            avg.azPrePosition += p->azPrePosition;
            azPositionsCtr++;
            avg.azPositionsValid = true;
        }
        if (p->elPositionsValid) {
            avg.elPosition += p->elPosition;
            avg.elPrePosition += p->elPrePosition;
            elPositionsCtr++;
            avg.elPositionsValid = true;
        }
        if (p->pointingModel) {
            avg.azPointingModelCorrection += p->azPointingModelCorrection;
            avg.elPointingModelCorrection += p->elPointingModelCorrection;
            pointingModelCorrectionCtr++;
        } else {
            avg.pointingModel = false;
        }
        if (p->auxPointingModel) {
            avg.azAuxPointingModelCorrection += p->azAuxPointingModelCorrection;
            avg.elAuxPointingModelCorrection += p->elAuxPointingModelCorrection;
            auxPointingModelCorrectionCtr++;
        } else {
            avg.auxPointingModel = false;
        }
        if (p->azEncoderValid) {
            avg.azEncoder += p->azEncoder;
            azEncoderCtr++;
            avg.azEncoderValid = true;
        }
        if (p->elEncoderValid) {
            avg.elEncoder += p->elEncoder;
            elEncoderCtr++;
            avg.elEncoderValid = true;
        }
        if (p->subrefPositionValid) {
            avg.subrefX += p->subrefX;
            avg.subrefY += p->subrefY;
            avg.subrefZ += p->subrefZ;
            subrefPositionCtr++;
            avg.subrefPositionValid = true;
        }
        if (p->subrefRotationValid) {
            avg.subrefTip += p->subrefTip;
            avg.subrefTilt += p->subrefTilt;
            subrefRotationCtr++;
            avg.subrefRotationValid = true;
        }
        if (p->subrefPositionCmdValid) {
            avg.subrefCmdX += p->subrefCmdX;
            avg.subrefCmdY += p->subrefCmdY;
            avg.subrefCmdZ += p->subrefCmdZ;
            subrefPositionCmdCtr++;
            avg.subrefPositionCmdValid = true;
        }
        if (p->subrefRotationCmdValid) {
            avg.subrefCmdTip += p->subrefCmdTip;
            avg.subrefCmdTilt += p->subrefCmdTilt;
            subrefRotationCmdCtr++;
            avg.subrefRotationCmdValid = true;
        }
    }
    if (azCommandedCtr > 0) avg.azCommanded /= azCommandedCtr;
    if (elCommandedCtr > 0) avg.elCommanded /= elCommandedCtr;
    if (azPositionsCtr > 0) {
        avg.azPrePosition /= azPositionsCtr;
        avg.azPosition /= azPositionsCtr;
    }
    if (elPositionsCtr > 0) {
        avg.elPrePosition /= elPositionsCtr;
        avg.elPosition /= elPositionsCtr;
    }
    if (pointingModelCorrectionCtr > 0) {
        avg.azPointingModelCorrection /= pointingModelCorrectionCtr;
        avg.elPointingModelCorrection /= pointingModelCorrectionCtr;
    }
    if (auxPointingModelCorrectionCtr > 0) {
        avg.azAuxPointingModelCorrection /= auxPointingModelCorrectionCtr;
        avg.elAuxPointingModelCorrection /= auxPointingModelCorrectionCtr;
    }
    if (azEncoderCtr > 0) avg.azEncoder /= azEncoderCtr;
    if (elEncoderCtr > 0) avg.elEncoder /= elEncoderCtr;

    if (subrefPositionCtr > 0) {
        avg.subrefX /= subrefPositionCtr;
        avg.subrefY /= subrefPositionCtr;
        avg.subrefZ /= subrefPositionCtr;
    }
    if (subrefRotationCtr > 0) {
        avg.subrefTip  /= subrefRotationCtr;
        avg.subrefTilt /= subrefRotationCtr;
    }
    if (subrefPositionCmdCtr > 0) {
        avg.subrefCmdX /= subrefPositionCmdCtr;
        avg.subrefCmdY /= subrefPositionCmdCtr;
        avg.subrefCmdZ /= subrefPositionCmdCtr;
    }
    if (subrefRotationCmdCtr > 0) {
        avg.subrefCmdTip  /= subrefRotationCmdCtr;
        avg.subrefCmdTilt /= subrefRotationCmdCtr;
    }
    return avg;
}

void PositionStreamConsumer::
checkPositions(const vector<MountStatusData>& positions) {
    if (positions.empty()) {
        const string msg = "There is no position data.";
        throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__, __PRETTY_FUNCTION__);
    }

    vector<MountStatusData>::const_iterator p = positions.begin(); 
    ACS::Time lastTime = p->timestamp;
    const bool onSource = p->onSource;
    const bool pointingModel = p->pointingModel;
    const bool auxPointingModel = p->auxPointingModel;
    const string antennaName = p->antennaName.in();

    while (p != positions.end()) {
        // Do these checks for all elements
        if (!p->azCommandedValid || !p->elCommandedValid) {
            const string msg = "The commanded positions are invalid.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        }
        if (!p->azPositionsValid || !p->elPositionsValid) {
            const string msg = "The measured positions are invalid.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        }
        if (!p->azEncoderValid || !p->elEncoderValid) {
            const string msg = "The encoder positions are invalid.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        }
        
        ++p;
        if  (p == positions.end()) break;

        // Ignore the  following checks for the first element
        if ((p->timestamp - lastTime) != TETimeUtil::TE_PERIOD_ACS) {
            const string msg = "Timestamps are not incrementing by 48ms.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        } 
        lastTime = p->timestamp;

        if (p->onSource != onSource) {
            const string msg = "The on-source flag is changing.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        }
        if (p->pointingModel != pointingModel) {
            const string msg = "The pointing model is changing.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        }
        if (p->auxPointingModel != auxPointingModel) {
            string msg = "The auxiliary pointing model is changing.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        }
        if (p->antennaName.in() != antennaName) {
            const string msg = "The antenna name is changing.";
            throwHardwareErrorExImpl(msg.c_str(), __LINE__, __FILE__,__PRETTY_FUNCTION__);
        }
    }
}

MountStatusData PositionStreamConsumer::operator[](int idx){
    MountStatusData returnValue;
    pthread_mutex_lock(&dataMtx_m);
    sortData();

    returnValue = dataVector_m[idx];
    pthread_mutex_unlock(&dataMtx_m);

    return returnValue;
}

void PositionStreamConsumer::sortData() {
    if (!sorted_m) {
        sort(dataVector_m.begin(),dataVector_m.end(),
             PositionStreamConsumer::Compare_MountStatusData());
        sorted_m = true;
    }
}

void PositionStreamConsumer::
avgVector(vector<double>& vec, double& avg) {
    avg=0.0;
  
    if (vec.size()==0) {
        string message;
        message ="Trying to average a zero length vector!!";
        ControlExceptions::IllegalParameterErrorExImpl
            ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("Detail", message);
        ex.log();
        throw ex;
    }

    vector<double>::iterator it; 
    for(it=vec.begin(); it < vec.end(); it++) {
        avg+=*it;
    }
    avg=avg/vec.size();
    return;
}

void PositionStreamConsumer::
throwHardwareErrorExImpl(const char* msg, int line, 
                         const char* file, const char* func) {
    ControlExceptions::HardwareErrorExImpl ex(file, line, func);
    ex.addData("Detail", msg);
    ex.log();
    throw ex;
}

