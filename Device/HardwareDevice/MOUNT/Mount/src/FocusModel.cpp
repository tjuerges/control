// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010, 2011
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#include "FocusModel.h"
#include <TMCDBComponentC.h> // for TMCDB::*FocusModel
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <RepeatGuard.h> // For the repeat guard 
#include <EncoderTransformations.h>
#include <ControlExceptions.h>
#include <cmath> // for std::abs
#include <string> 
#include <sstream>

using Control::FocusModel;
using std::map;
using std::string;
using std::ostringstream;
using ControlExceptions::IllegalParameterErrorExImpl;
using std::abs;
using std::cos;
using std::sin;
using Logging::Logger;

FocusModel::FocusModel(double beamDeviationFactor, double focalRatio, 
                       double xLimit, double yLimit, double zLimit, 
                       double tipLimit, double tiltLimit,
                       Logger::LoggerSmartPtr logger) :
    Logging::Loggable(logger),
    enabled_m(false),
    doPointingCorrection_m(true),
    model_m(),
    antenna_m(),
    currentBand_m(3),
    band_m(10),
    temperature_m(0.0),
    offsetX_m(0.0),
    offsetY_m(0.0),
    offsetZ_m(0.0),
    offsetTip_m(0.0),
    offsetTilt_m(0.0),
    pointingModel_m(logger),
    toleranceXY_m(100E-6),
    toleranceZ_m(10E-6),
    toleranceRotation_m(0.001*M_PI/180.0),
    lastCommandedX_m(0.0),
    lastCommandedY_m(0.0),
    lastCommandedZ_m(0.0),
    lastCommandedTip_m(0.0),
    lastCommandedTilt_m(0.0),
    beamDeviationFactor_m(beamDeviationFactor),
    focalRatio_m(focalRatio),
    xLimit_m(xLimit),
    yLimit_m(yLimit),
    zLimit_m(zLimit),
    tipLimit_m(tipLimit),
    tiltLimit_m(tiltLimit),
    manualPosition_m(false),
    x_m(0.0),
    y_m(0.0),
    z_m(0.0),
    manualRotation_m(false),
    tip_m(0.0),
    tilt_m(0.0),
    forceUpdate_m(false)
{ 
    model_m["XR"] = 0.0;
    model_m["XC"] = 0.0;
    model_m["XS"] = 0.0;
    model_m["XTA"] = 0.0;

    model_m["YR"] = 0.0;
    model_m["YC"] = 0.0;
    model_m["YS"] = 0.0;
    model_m["YTA"] = 0.0;

    model_m["ZR"] = 0.0;
    model_m["ZC"] = 0.0;
    model_m["ZS"] = 0.0;
    model_m["ZTA"] = 0.0;

    model_m["ALPHA"] = 0.0;
    model_m["BETA"] = 0.0;

    antenna_m = model_m;
    for (unsigned int i = 0; i < band_m.size(); i++) {
        band_m[i] = model_m;
    }
    pointingModel_m.enable();
}

bool FocusModel::getFocus(double& az, double& el, 
                          double& focusX, double& focusY, double& focusZ,
                          double& tip, double& tilt) {

    if (isEnabled()) {
        const double referenceTemp = 0.0;
        const double referenceEl = 50.0*M_PI/180.0;
        // TODO. Check that the az/el are in range. Perhaps this should be done
        // in the mount.
        const double deltaCosEl = cos(el) - cos(referenceEl);
        const double deltaSinEl = sin(el) - sin(referenceEl);
        const double deltaTemp  = temperature_m - referenceTemp;
        focusX = model_m["XR"] 
            + model_m["XC"]  * deltaCosEl
            + model_m["XS"]  * deltaSinEl
            + model_m["XTA"] * deltaTemp;
        focusY = model_m["YR"] 
            + model_m["YC"]  * deltaCosEl
            + model_m["YS"]  * deltaSinEl
            + model_m["YTA"] * deltaTemp;
        focusZ = model_m["ZR"] 
            + model_m["ZC"]  * deltaCosEl
            + model_m["ZS"]  * deltaSinEl
            + model_m["ZTA"] * deltaTemp;

        tip =  model_m["ALPHA"];
        tilt = model_m["BETA"];
    } else {
        if (manualPosition_m) {
            focusX = x_m;
            focusY = y_m;
            focusZ = z_m;
        } else {
            focusX = lastCommandedX_m;
            focusY = lastCommandedY_m;
            focusZ = lastCommandedZ_m;
        }
        if (manualRotation_m) {
            tip = tip_m;
            tilt = tilt_m;
        } else {
            tip = lastCommandedTip_m;
            tilt = lastCommandedTilt_m;
        }
    }
    // Add the offsets
    focusX += offsetX_m;
    focusY += offsetY_m;
    focusZ += offsetZ_m;
    tip += offsetTip_m;
    tilt += offsetTilt_m;
    
    // Clip all values to be within the limits
    static RepeatGuard positionLimitsGuard(ACS::TimeInterval(10.0/100e-9), 0);
    if (enforcePositionLimits(focusX, focusY, focusZ) && positionLimitsGuard.check()) {
        ostringstream msg;
        msg << "Limiting the subreflector position to"
            << " [" << focusX*1000 << ", " 
            << focusY*1000 << ", " 
            << focusZ*1000 << "] mm" 
            << " to ensure it does not exceed the limits of +/- ["
            << xLimit_m*1000 << ", " 
            << yLimit_m*1000 << ", " 
            << zLimit_m*1000 << "] mm";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
    }
    static RepeatGuard rotationLimitsGuard(ACS::TimeInterval(10.0/100e-9), 0);
    if (enforceRotationLimits(tip, tilt) && rotationLimitsGuard.check()) {
        ostringstream msg;
        msg << "Limiting the subreflector rotation to"
            << " [" << tip*180/M_PI << ", " << tilt*180.0/M_PI << "] deg. " 
            << " to ensure it does not exceed the limits of +/- ["
            <<  tipLimit_m*180/M_PI << ", " << tiltLimit_m*180.0/M_PI
            << "] deg.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
    }

    // Now work out if we need to send a command
    bool sendCommand = false;
    const double dx = (lastCommandedX_m - focusX)/toleranceXY_m;
    const double dy = (lastCommandedY_m - focusY)/toleranceXY_m;
    const double dz = (lastCommandedZ_m - focusZ)/toleranceZ_m;
    const double dTip =  abs(lastCommandedTip_m - tip);
    const double dTilt =  abs(lastCommandedTilt_m - tilt);
    if (forceUpdate_m ||
        dx*dx + dy*dy + dz*dz > 1 ||
        dTip > toleranceRotation_m ||
        dTilt > toleranceRotation_m) {
        sendCommand = true;
        forceUpdate_m = false;
        // Round the ideal values to the actual ones so that we know what was
        // really sent to the hardware.
        EncoderTransformations::roundFocusPos(focusX, focusY, focusZ);
        lastCommandedX_m = focusX;
        lastCommandedY_m = focusY;
        lastCommandedZ_m = focusZ;
        EncoderTransformations::roundFocusRotation(tip, tilt);
        lastCommandedTip_m = tip;
        lastCommandedTilt_m = tilt;
    }

    if (sendCommand) {
        ostringstream msg;
        msg << "Moving the subreflector to (" 
            << lastCommandedX_m*1000 << ", "
            << lastCommandedY_m*1000 << ", "
            << lastCommandedZ_m*1000 << ") mm"
            << " with a tip/tilt of ("
            << lastCommandedTip_m*180/M_PI << ", "
            << lastCommandedTilt_m*180/M_PI << ") degrees."
            << " This includes position offsets of (" 
            << offsetX_m*1000 << ", "
            << offsetY_m*1000 << ", "
            << offsetZ_m*1000 << ") mm"
            << " and tip/tilt offsets of ("
            << offsetTip_m*180/M_PI << ", "
            << offsetTilt_m*180/M_PI << ") degrees.";        
        LOG_TO_OPERATOR(LM_DEBUG, msg.str());    
    }
    if (doPointingCorrection_m) {
        const double CXInArcSecPerMeter = beamDeviationFactor_m*1E3;
        const double EYInArcSecPerMeter = CXInArcSecPerMeter;
        const double CVInArcsec = focalRatio_m*180/M_PI*60*60;
        const double EUInArcsec = CVInArcsec;
        const double ca = 
            CXInArcSecPerMeter * lastCommandedX_m + 
            CVInArcsec * lastCommandedTilt_m;
        const double ie = 
            EYInArcSecPerMeter * lastCommandedY_m - 
            EUInArcsec * lastCommandedTip_m;
        pointingModel_m.setCoefficient("CA", ca);
        pointingModel_m.setCoefficient("IE", ie);
        double origAz = az;
        double origEl = el;
        pointingModel_m.applyModel(origAz, origEl, az, el);
        if (sendCommand) {
            ostringstream msg;
            msg << "Az/El adjusted by (" 
                << (origAz - az)*180/M_PI*3600 << ", "
                << (origEl - el)*180/M_PI*3600 << ") arcsecs"
                << " (CA=" << ca << " IE=" << ie << " arcsecs)"
                << " to compensate for the subreflector position and rotation.";
            LOG_TO_OPERATOR(LM_DEBUG, msg.str());
        }
    }
    return sendCommand;
}

void FocusModel::setFocusModel(const TMCDB::FocusModel& model) {
    currentBand_m = - abs(currentBand_m);
    // C++ treats typedefs as different types. So I have to do a copy here.
    TMCDB::ModelTermSeq_var s = new TMCDB::ModelTermSeq();
    s->length( model.length());
    for (unsigned int i = 0; i < model.length(); i++) {
        s[i] = model[i];
    }
    setFocusModel(s, model_m, getLogger());
}

void FocusModel::zeroCoefficients() {
    zeroModel(model_m);
}

void FocusModel::setCoefficient(string name, double value) {
    setCoefficient(name, value, model_m, getLogger());
}

TMCDB::FocusModel* FocusModel::getFocusModel() {
    TMCDB::FocusModel_var model = new TMCDB::FocusModel();
    model->length(model_m.size());
    map<string, double>::const_iterator iter;
    unsigned index = 0;
    for (iter = model_m.begin(); iter != model_m.end(); 
         ++iter, ++index) {
        TMCDB::ModelTerm& coeff = model[index];
        coeff.name = CORBA::string_dup((iter->first).c_str());
        coeff.value = iter->second;
    }
    return model._retn();
}

double FocusModel::getCoefficient(const string& coeffName) {
    // Make the comparison case insensitive by converting string to upper case.
    string name = coeffName;
    for (string::iterator p = name.begin(); p !=  name.end(); ++p) {
        *p = std::toupper(*p);
    }
    if (model_m.count(name) == 0) {
        string msg = "Cannot get the focus model coefficient called '";
        msg += name + "' as it is not known. Please check the name.";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    return model_m[name];
}

void FocusModel::setFocusModel(const TMCDB::BandFocusModel& model) {
    setFocusModel(model.base, antenna_m, getLogger());

    for (unsigned int i = 0; i < band_m.size(); i++) {
        zeroModel(band_m[i]);
    }
    for (unsigned int i = 0; i < model.offsets.length(); i++) {
        const int band = model.offsets[i].bandNumber;
        throwIfBadBand(band, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        setFocusModel(model.offsets[i].terms, band_m[band-1], getLogger());
    }
}

void FocusModel::useBand(int band) {
    throwIfBadBand(band, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    map<string, double>& offset = band_m[band-1];
    map<string, double>::iterator iter;   
    for (iter = antenna_m.begin(); iter != antenna_m.end(); ++iter) {
        string term = iter->first;
        double value = iter->second + offset[term];
        setCoefficient(term, value);
    }
    currentBand_m = band;
}

int FocusModel::getCurrentBand() {
    return currentBand_m;
}

void FocusModel::setAmbientTemperature(double temperature) {
    if (temperature < -50 || temperature > 100) {
        ostringstream msg;
        msg << "Cannot set the temperature used by the focus model."
            << " The supplied value of " << temperature << " deg. C."
            <<  " is outside the range of -50 to 100 deg C.";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    temperature_m = temperature;
}

void FocusModel::setPositionOffset(double x, double y, double z) {
    ostringstream msg;
    if ((abs(x) > 2 * xLimit_m) ||
        (abs(y) > 2 * yLimit_m) ||
        (abs(z) > 2 * zLimit_m)) {
        msg << "The subreflector position offset of"
            << " [" << x*1000 << ", " 
            << y*1000 << ", " 
            << z*1000 << "] mm" 
            << " is more than twice the limits of +/- ["
            << xLimit_m*1000 << ", " 
            << yLimit_m*1000 << ", " 
            << zLimit_m*1000 << "] mm";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    if (!isEnabled()) forceUpdate_m = true;
    offsetX_m = x;
    offsetY_m = y;
    offsetZ_m = z;
    msg << "Offsetting the subreflector position by " 
        << " [" << offsetX_m*1000 << ", " 
        << offsetY_m*1000 << ", " 
        << offsetZ_m*1000 << "] mm";
    LOG_TO_OPERATOR(LM_DEBUG, msg.str());
}

void FocusModel::getPositionOffset(double& x, double& y, double& z) {
    x = offsetX_m;
    y = offsetY_m;
    z = offsetZ_m;
}

void FocusModel::setRotationOffset(double tip, double tilt) {
    ostringstream msg;
    if ((abs(tip) > 2 * tipLimit_m) ||
        (abs(tilt) > 2 * tiltLimit_m)) {
        msg << "The subreflector rotation offset of"
            << " [" << tip*180/M_PI << ", " << tilt*180/M_PI << "] deg." 
            << " is more than twice the limits of +/- ["
            << tipLimit_m*180/M_PI << ", " << tiltLimit_m*180/M_PI << "] deg.";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    if (!isEnabled()) forceUpdate_m = true;
    offsetTip_m = tip;
    offsetTilt_m = tilt;
    msg << "Offsetting the subreflector rotation by " 
        << " [" << offsetTip_m*180/M_PI << ", " 
        << offsetTilt_m*180/M_PI << "] deg "; 
    LOG_TO_OPERATOR(LM_DEBUG, msg.str());
}

void FocusModel::getRotationOffset(double& tip, double& tilt) {
    tip = offsetTip_m;
    tilt = offsetTilt_m;
}

void FocusModel::setPositionToleranceXY(double tolerance) {
    if (tolerance < 1E-6 || tolerance > 30E-3) {
        ostringstream msg;
        msg << "Cannot set the x and y axis tolerance used by the focus model."
            << " The supplied value of " << tolerance << " meters"
            <<  " is outside the range of 1um to 30mm";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    toleranceXY_m = tolerance;
}

void FocusModel::setPositionToleranceZ(double tolerance) {
    if (tolerance < 1E-6 || tolerance > 30E-3) {
        ostringstream msg;
        msg << "Cannot set the z-axis tolerance used by the focus model."
            << " The supplied value of " << tolerance << " meters"
            <<  " is outside the range of 1um to 30mm";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    toleranceZ_m = tolerance;
}

void FocusModel::setRotationTolerance(double tolerance) {
    if (tolerance < 0.0001*M_PI/180.0 || tolerance > 5*M_PI/180) {
        ostringstream msg;
        msg << "Cannot set the rotation tolerance used by the focus model."
            << " The supplied value of " << tolerance*180/M_PI << " degrees"
            <<  " is outside the range of 5deg to 0.0001 deg";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    toleranceRotation_m = tolerance;
}

double FocusModel::getPositionToleranceXY() {
    return toleranceXY_m;
}

double FocusModel::getPositionToleranceZ() {
    return toleranceZ_m;
}

double FocusModel::getRotationTolerance() {
    return toleranceRotation_m;
}

bool FocusModel::isEnabled() {
    return enabled_m;
}

void FocusModel::enable() {
    enabled_m = true;
    manualPosition_m = manualRotation_m = false;
    forceUpdate_m = true;
}

void FocusModel::disable() {
    enabled_m = false;
}

bool FocusModel::adjustPointing() {
    return doPointingCorrection_m;
}

void FocusModel::enablePointingCorrection() {
     doPointingCorrection_m = true;
}

void FocusModel::disablePointingCorrection() {
     doPointingCorrection_m = false;
}

void FocusModel::setSubrefAbsPosn(double x, double y, double z) {
    disable();
    manualPosition_m = true;
    forceUpdate_m = true;
    x_m = x;
    y_m = y;
    z_m = z;
}

void FocusModel::setSubrefRotation(double tip, double tilt) {
    disable();
    manualRotation_m = true;
    forceUpdate_m = true;
    tip_m = tip;
    tilt_m = tilt;
}

void FocusModel::forceUpdate() {
    forceUpdate_m = true;
}

void FocusModel::setCoefficient(string name, double value, map<string, double>& model,
                                Logger::LoggerSmartPtr logger) {
    // Make the comparison case insensitive by converting string to upper case.
    for (string::iterator p = name.begin(); p !=  name.end(); ++p) {
        *p = std::toupper(*p);
    }
 
    if (model.count(name) == 0) {
        string msg = "Cannot change the focus model coefficient called '";
        msg += name + "' as it is an unknown coefficient.";
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        LOG_TO_OPERATOR_WITH_LOGGER(LM_ERROR, msg, logger);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    // These limits are determined assuming the subreflector motion limits are
    // +/-32.768mm and the rotation limits are +/- 3.2768 deg.
    double maxCoeff = 0.0;
    string unit("");
    if (name == "XR" || name == "XC" || name == "XS" ||
        name == "YR" || name == "YC" || name == "YS" ||
        name == "ZR" || name == "ZC" || name == "ZS") {
        // These terms are in meters
        maxCoeff = 50E-3; // 50mm
        unit = " meters";
    } else if (name == "ALPHA" || name == "BETA") {
        // These terms are in radians
        maxCoeff = 3.5*M_PI/180.0; // 3.5 degrees
        unit = " radians";
    } else if (name == "XTA" || name == "YTA" || name == "ZTA") {
        // These terms are in meters/deg.
        maxCoeff = 5E-3; // 5mm/deg C
        unit = " meters/deg. C.";
    }
    if (abs(value) > maxCoeff) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ostringstream msg;
        msg << "Cannot change the focus model as the magnitude of the " 
            << name << " coefficient is too big. Its " << value 
            << unit << " and the maximum is " << maxCoeff << unit;
        LOG_TO_OPERATOR_WITH_LOGGER(LM_ERROR, msg.str(), logger);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    model[name] = value;
}

void FocusModel::zeroModel(map<string, double>& model) {
    map<string, double>::iterator iter;   
    for(iter = model.begin(); iter != model.end(); ++iter) {
        iter->second = 0.0;
    }
}

void FocusModel::setFocusModel(const TMCDB::ModelTermSeq& inModel, 
                               map<string, double>& outModel,
                               Logger::LoggerSmartPtr logger) {
    zeroModel(outModel);
    // Set the non zero values to whatever is specified.
    for (unsigned int index = 0; index < inModel.length(); index++) {
        const TMCDB::ModelTerm& coeff = inModel[index];
        setCoefficient(string(coeff.name), coeff.value, outModel, logger);
    }
}

bool FocusModel::enforcePositionLimits(double& x, double& y, double& z) {
    bool hitLimit = false;
    if (x < -xLimit_m) {
        x = -xLimit_m;
        hitLimit = true;
    } else if (x > xLimit_m) {
        x = xLimit_m;
        hitLimit = true;
    }
    if (y < -yLimit_m) {
        y = -yLimit_m;
        hitLimit = true;
    } else if (y > yLimit_m) {
        y = yLimit_m;
        hitLimit = true;
    }
    if (z < -zLimit_m) {
        z = -zLimit_m;
        hitLimit = true;
    } else if (z > zLimit_m) {
        z = zLimit_m;
        hitLimit = true;
    }
    return hitLimit;
}

bool FocusModel::enforceRotationLimits(double& tip, double& tilt) {
    bool hitLimit = false;
    if (tip < -tipLimit_m) {
        tip = -tipLimit_m;
        hitLimit = true;
    } else if (tip > tipLimit_m){
        tip = tipLimit_m;
        hitLimit = true;
    }

    if (tilt < -tiltLimit_m) {
        tilt = -tiltLimit_m;
        hitLimit = true;
    } else if (tilt > tiltLimit_m){
        tilt = tiltLimit_m;
        hitLimit = true;
    }
    return hitLimit;
}

void FocusModel::
throwIfBadBand(int band, const char* file, int line, const char* routine) {
    if (band < 1 || band > 10) {
        ostringstream msg;
        msg << "Cannot change the band offsets as band " << band 
            << " is outside the range 1 to 10";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        IllegalParameterErrorExImpl ex(file, line, routine);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}
