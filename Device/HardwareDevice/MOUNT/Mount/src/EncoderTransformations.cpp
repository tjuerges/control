// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <EncoderTransformations.h>
#include <arpa/inet.h> // for htonl
#include <cmath> // for M_PI

const double fToMeters = 1E-6;
const double fToRad = 1E-4*M_PI/180.0;

using Control::EncoderTransformations;

EncoderTransformations::EncoderTransformations(){
    azParameters_m.offset = 0.0;
    azParameters_m.scale = M_PI/std::pow(2.0, 26);
    elParameters_m.offset = azParameters_m.offset;
    elParameters_m.scale = azParameters_m.scale;
}

double EncoderTransformations::
azEncToRad(const AmbDataMem_t* const enc) const {
    return encToRad(enc, azParameters_m);
}

double EncoderTransformations::
elEncToRad(const AmbDataMem_t* const enc) const {
    return encToRad(enc, elParameters_m);
}

void EncoderTransformations::
setConversionFactors(const TransformationParameters& azParameters,
		     const TransformationParameters& elParameters) {
    azParameters_m = azParameters;
    elParameters_m = elParameters;
}

double EncoderTransformations::posToRad(const AmbDataMem_t* const enc) {
    // There are 2 values here but we only use the first one, the on-TE value.
    const int32_t encValue = ntohl(*reinterpret_cast<const int32_t*>(enc));
    return static_cast<double>(encValue)/0x40000000 * M_PI;
}

uint32_t EncoderTransformations::radToPos(const double& rad) {
    // Convert the double value to turns 
    return htonl(static_cast<long>((rad/M_PI)*0x40000000));
}

double EncoderTransformations::
encToRad(const AmbDataMem_t* const encoder, 
         const TransformationParameters& params) {
    const uint32_t encValue = 
        ntohl(*reinterpret_cast<const uint32_t*>(encoder));
    return static_cast<double>(encValue)*params.scale+params.offset;
}

double EncoderTransformations::
focusToMeter(const uint16_t * const pos) {
    uint16_t swappedPos = ntohs(*pos);
    short signedPos = *reinterpret_cast<int16_t *>(&swappedPos);
    return signedPos * fToMeters;
}

void EncoderTransformations::
focusToPos(const AmbDataMem_t * const subrefAbsPosn, 
              double& x, double& y, double& z) {
    x = focusToMeter(reinterpret_cast<const uint16_t* const>(subrefAbsPosn));
    y = focusToMeter(reinterpret_cast<const uint16_t* const>(subrefAbsPosn+2));
    z = focusToMeter(reinterpret_cast<const uint16_t* const>(subrefAbsPosn+4));
}

void EncoderTransformations::
focusToRot(const AmbDataMem_t* const raw, double& tip, double& tilt) {
    tip = focusToRadians(reinterpret_cast<const uint16_t* const>(raw));
    tilt = focusToRadians(reinterpret_cast<const uint16_t* const>(raw+2));
}

void EncoderTransformations::toPos(const double& posn, const double& rate,
                                   AmbDataMem_t* const trajCmd) {
    *reinterpret_cast<uint32_t* const>(trajCmd) = radToPos(posn);
    *reinterpret_cast<uint32_t* const>(trajCmd+4) = radToPos(rate);
} 

short EncoderTransformations::radToFocus(const double& rad) {
    return static_cast<short>(rad/fToRad);
}

void EncoderTransformations::
toFocusRotation(const double& tip, const double& tilt,
                AmbDataMem_t* const subrefRotation) {
    *reinterpret_cast<uint16_t* const>(subrefRotation) = htons(radToFocus(tip));
    *reinterpret_cast<uint16_t* const>(subrefRotation+2) = htons(radToFocus(tilt));
    *reinterpret_cast<uint16_t* const>(subrefRotation+4) = 0;
} 

short EncoderTransformations::metersToFocus(const double& meters) {
    return static_cast<short>(meters/fToMeters);
}

void EncoderTransformations::
toFocusPos(const double& x, const double& y, const double& z,
           AmbDataMem_t* const subrefAbsPosn) {
    *reinterpret_cast<uint16_t* const>(subrefAbsPosn) = htons(metersToFocus(x));
    *reinterpret_cast<uint16_t* const>(subrefAbsPosn+2) = htons(metersToFocus(y));
    *reinterpret_cast<uint16_t* const>(subrefAbsPosn+4) = htons(metersToFocus(z));
} 

void EncoderTransformations::roundFocusPos(double& x, double& y, double& z) {
    AmbDataMem_t data[6];
    toFocusPos(x, y, z, data);
    focusToPos(data, x, y, z);
}

void EncoderTransformations::roundFocusRotation(double& tip, double& tilt) {
    AmbDataMem_t data[6];
    toFocusRotation(tip, tilt, data);
    focusToRot(data, tip, tilt);
}

double EncoderTransformations::focusToRadians(const uint16_t * const raw) {
    uint16_t swappedPos = ntohs(*raw);
    short signedPos = *reinterpret_cast<int16_t *>(&swappedPos);
    return signedPos * fToRad;
}
