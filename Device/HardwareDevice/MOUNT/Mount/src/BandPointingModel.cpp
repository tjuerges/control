// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "BandPointingModel.h"
#include <TMCDBComponentC.h> // for TMCDB::*PointingModel
#include <slamac.h> // for DAS2R & DPI
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <ControlExceptions.h>
#include <string>
#include <sstream>

using Control::BandPointingModel;
using ControlExceptions::IllegalParameterErrorExImpl;
using std::map;
using std::string;
using std::ostringstream;

BandPointingModel::BandPointingModel(Logging::Logger::LoggerSmartPtr logger)
     :Control::PointingModel(logger),
      antenna_m(),
      currentBand_m(3),
      band_m(10) {
    initializeModel(antenna_m);
    for (unsigned int i = 0; i < band_m.size(); i++) {
        initializeModel(band_m[i]);
    }
}

void BandPointingModel::setPointingModel(const TMCDB::PointingModel& model) {
    // A negative value means it may not be the model read from the TMCDB 
    currentBand_m = - std::abs(currentBand_m);
    PointingModel::setPointingModel(model);
}

void BandPointingModel::setPointingModel(const TMCDB::BandPointingModel& model) {
    PointingModel::setPointingModel(model.base, antenna_m, getLogger());
    for (unsigned int i = 0; i < band_m.size(); i++) {
        zeroModel(band_m[i]);
    }
    for (unsigned int i = 0; i < model.offsets.length(); i++) {
        const int band = model.offsets[i].bandNumber;
        throwIfBadBand(band, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        PointingModel::setPointingModel(model.offsets[i].terms, band_m[band-1], getLogger());
    }
}

void BandPointingModel::useBand(int band) {
    throwIfBadBand(band, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    map<string, double>& offset = band_m[band-1];
    map<string, double>::iterator iter;   
    for (iter = antenna_m.begin(); iter != antenna_m.end(); ++iter) {
        string term = iter->first;
        // I need divide by DAS2R as setCoefficient multiplies by DAS2R 
        double value = (iter->second + offset[term])/DAS2R; 
        setCoefficient(term, value);
    }
    currentBand_m = band;
}

int BandPointingModel::getCurrentBand() {
    return currentBand_m;
}

void BandPointingModel::
throwIfBadBand(int band, const char* file, int line, const char* routine) {
    if (band < 1 || band > 10) {
        ostringstream msg;
        msg << "Cannot change the band offsets as band " << band 
            << " is outside the range 1 to 10";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
        IllegalParameterErrorExImpl ex(file, line, routine);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}
