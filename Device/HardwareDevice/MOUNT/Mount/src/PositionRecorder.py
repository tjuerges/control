#!/usr/bin/env python
from ambManager import *
from Acspy.Common.TimeHelper import *
from time import sleep
import math
import sys

ABM_TO_MONITOR = "DV01"
ACU_CAN_CHANNEL = 2

class positionRecorder:
    def __init__(self):
        self._azPosn = TEData()
        self._elPosn = TEData()
        self._azEncoder = TEData()
        self._elEncoder = TEData()
        self._mgr = AmbManager(ABM_TO_MONITOR)
        self._startTime = 0;
        self._stopTime = 0;

    def monitor(self,time):
        self._mgr.monitorTEAsynch(ACU_CAN_CHANNEL, 0, 0x12, time, self._azPosn)
        self._mgr.monitorTEAsynch(ACU_CAN_CHANNEL, 0, 0x02, time, self._elPosn)
        self._mgr.monitorTEAsynch(ACU_CAN_CHANNEL, 0, 0x17, time, self._azEncoder)
        self._mgr.monitorTEAsynch(ACU_CAN_CHANNEL, 0, 0x07, time, self._elEncoder)
        if time < self._startTime or self._startTime == 0:
            self._startTime = time
        if time > self._stopTime:
            self._stopTime = time
        
    def write(self, filename):
        idx = self._startTime
        print "Start Time: ", self._startTime
        print "Stop Time:  ", self._stopTime

        print "Waiting for monitors to end:"
        while self._elEncoder.dataList[len(self._elEncoder.dataList)-1].targetTE< self._stopTime:
            time.sleep(1)
            
        print "All Monitors complete"
        
        fd = file(filename,"w+")
        try:
            while idx <= self._stopTime:
                while self._azPosn.dataList[0].targetTE < idx:
                    self._azPosn.dataList.pop(0)
                while self._elPosn.dataList[0].targetTE < idx:
                    self._elPosn.dataList.pop(0)
                while self._azEncoder.dataList[0].targetTE < idx:
                    self._azEncoder.dataList.pop(0)
                while self._elEncoder.dataList[0].targetTE < idx:
                    self._elEncoder.dataList.pop(0)
                                
                if self._azPosn.dataList[0].targetTE == idx:
                    azPosn=self.getAzPosn(self._azPosn.dataList[0].data)
                    self._azPosn.dataList.pop(0)
                else:
                    azPosn = (0,0)
                                    
                if self._elPosn.dataList[0].targetTE == idx:
                    elPosn=self.getElPosn(self._elPosn.dataList[0].data)
                    self._elPosn.dataList.pop(0)
                else:
                    elPosn = (0,0)
                                        
                if self._azEncoder.dataList[0].targetTE == idx:
                    azEncoder=self.getAzEncoder(self._azEncoder.dataList[0].data)
                    self._azEncoder.dataList.pop(0)
                else:
                    azEncoder = 0
                                            
                if self._elEncoder.dataList[0].targetTE == idx:
                    elEncoder=self.getElEncoder(self._elEncoder.dataList[0].data)
                    self._elEncoder.dataList.pop(0)
                else:
                    elencoder = (0,0)
            

                fd.write("%d\t%f\t%f\t%f\t%f\t%d\t%d\t%d\n" %
                         (idx, azPosn[1], elPosn[1], azPosn[0], elPosn[0],
                          azEncoder, elEncoder[0], elEncoder[1]))
                idx += 480000;
        except:
            pass
        fd.close()
                         
                        
        
    def getAzPosn(self, data):
        raw = struct.unpack("!ll",data)
        radians = []
        for x in range(0,2):
            radians.append( raw[x] * 2 * math.pi / 0x7FFFFFFF)
        return radians

    def getElPosn(self, data):
        raw = struct.unpack("!ll",data)
        radians = []
        for x in range(0,2):
            radians.append( raw[x] * 2 * math.pi / 0x7FFFFFFF)
        return radians

    def getAzEncoder(self,data):
        (raw,) = struct.unpack("!L",data)
        return raw

    def getElEncoder(self, data):
        raw = struct.unpack("!LL",data)
        return raw

        
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "usage: positionRecorder.py <filename>"
        sys.exit()
    
    print "Now capturing telescope pointing information"
    print "Data will be saved to %s" % sys.argv[1]
    print "Press <Ctrl>-C to exit"
    
    recorder = positionRecorder()
    epoch = getTimeStamp()
    epoch.value -= (epoch.value % 480000)
    sentTime = epoch.value + 720000 

    try:
        while True:
            while sentTime <= getTimeStamp().value + 10E7:
                recorder.monitor(sentTime)
                sentTime += 480000
            sleep(5)
    except KeyboardInterrupt :
        pass

    recorder.write(sys.argv[1])
        
