// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountStatusBuffer.h"
#include <MountImpl.h>
#include <TETimeUtil.h>
#include <acstimeTimeUtil.h> // for TimeUtil

using ACS::ThreadSyncGuard;
using std::vector;
using Control::MountImpl;
using Control::MountStatusStructure;
using Control::MountStatusBuffer;
using Control::AMBMsgDef;
using Control::AMBMsgData;

const ACS::Time RESPONSE_TIMEOUT_THRESHOLD = 5 * TETimeUtil::TE_PERIOD_ACS;

void MountStatusStructure::initialize() {
    applicationTimestamp  = 0;
    positionControlTimestamp = 0;
    focusControlTimestamp = 0;
    monitorTimestamp = 0;
    pointingModelCorrectionAz = pointingModelCorrectionEl = 0.0;
    pointingModel = false;
    auxPointingModelCorrectionAz = auxPointingModelCorrectionEl = 0.0;
    auxPointingModel = false;
    focusPointingModelCorrectionAz = focusPointingModelCorrectionEl = 0.0;
    focusPointingModel = false;
    pointingControlsModified = true;
}

MountStatusBuffer::MountStatusBuffer(MountImpl* mountReference, 
				     Logging::Logger::LoggerSmartPtr logger,
				     unsigned int initialSize) :
    Logging::Loggable(logger),
    availableQueue_m(),
    pendingQueue_m(),
    deadQueue_m()
{
    for (unsigned int idx = 0; idx < initialSize; idx++) {
        addToAvailableQueue(new MountStatusStructure());
    }
    defineTEMonitors(mountReference);
    defineTEPositionControls(mountReference);
    defineTEFocusControls(mountReference);
}
  
MountStatusBuffer::~MountStatusBuffer() {
    {
        ThreadSyncGuard guard(&pendingQueueMutex_m);
        while (!pendingQueue_m.empty()) {
            delete pendingQueue_m.front();
            pendingQueue_m.pop_front();
        }
    }
    {
        ThreadSyncGuard guard(&availableQueueMutex_m);
        while (!availableQueue_m.empty()) {
            delete availableQueue_m.front();
            availableQueue_m.pop_front();
        }
    }
    while (!deadQueue_m.empty()) {
        delete deadQueue_m.front();
        deadQueue_m.pop_front();
    }
}

vector<AMBMsgDef>& MountStatusBuffer::getTEMonitorVector() {
    return teMonitorList_m;
}

vector<AMBMsgDef>& 
MountStatusBuffer::getTEPositionControlVector() {
    return tePositionControlList_m;
}

vector<AMBMsgDef>& 
MountStatusBuffer::getTEFocusControlVector() {
    return teFocusControlList_m;
}

MountStatusStructure* MountStatusBuffer::getNextAvailable() {
    ThreadSyncGuard guard(&availableQueueMutex_m);
    if (availableQueue_m.empty()) {
        return new MountStatusStructure();
    }
    
    MountStatusStructure* returnValue = availableQueue_m.front();
    availableQueue_m.pop_front();
    return returnValue;
}

MountStatusStructure* MountStatusBuffer::getCompletedStructure() {
    
    ThreadSyncGuard guard(&pendingQueueMutex_m);
    const ACS::Time now = ::getTimeStamp();
    
    while (!pendingQueue_m.empty() && 
	   pendingQueue_m.front()->applicationTimestamp +
	   RESPONSE_TIMEOUT_THRESHOLD <=  now ) {
	//ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
	//	(LM_DEBUG,"time: %llu current time: %llu", pendingQueue_m.front()->applicationTimestamp, now));
        
	MountStatusStructure* frontStructure = pendingQueue_m.front();
	pendingQueue_m.pop_front();
	if (isComplete(frontStructure)) {
	    //ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
	    //	    (LM_DEBUG,"time: %llu COMPLETED", frontStructure->applicationTimestamp));
	    return frontStructure;
        } else {
	    ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
		    (LM_DEBUG,"time: %llu DEAD", frontStructure->applicationTimestamp));
	    deadQueue_m.push_back(frontStructure);
        }
    }
    return NULL;
}

bool MountStatusBuffer::isComplete(MountStatusStructure* mountStatus) {

    vector<AMBMsgDef>::iterator iter;
    if (mountStatus->positionControlTimestamp != 0) {
        // Check the Position Control Status
        for (iter = tePositionControlList_m.begin(); 
             iter != tePositionControlList_m.end();
             iter++) {
            if ((iter->locatorMethod(mountStatus)).ambStatus==AMBERR_PENDING) {
                // TODO. Cleanup this log
                ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                        (LM_DEBUG,"time: %llu RCA: 0x%x status: %d", 
                         mountStatus->applicationTimestamp, 
                         iter->messageRCA,
                         iter->locatorMethod(mountStatus).ambStatus
                         ));
                return false;
            }
        }
    }
    
    if (mountStatus->focusControlTimestamp != 0) {
        // Check the Focus Control Status
        for (iter = teFocusControlList_m.begin(); 
             iter != teFocusControlList_m.end();
             iter++) {
            if ((iter->locatorMethod(mountStatus)).ambStatus==AMBERR_PENDING) {
                ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
                        (LM_DEBUG,"time: %llu RCA: 0x%x status: %d", 
                         mountStatus->applicationTimestamp, 
                         iter->messageRCA,
                         iter->locatorMethod(mountStatus).ambStatus
                         ));
                return false;
            }
        }
    }

    // Check the Monitor Status
    for (iter = teMonitorList_m.begin();
         iter != teMonitorList_m.end();
         iter++) {
	if ((iter->locatorMethod(mountStatus)).ambStatus == AMBERR_PENDING) {
            ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
		    (LM_DEBUG,"time: %llu RCA: 0x%x status: %d", 
		     mountStatus->applicationTimestamp, 
		     iter->messageRCA,
		     iter->locatorMethod(mountStatus).ambStatus
                     ));
	    return false;
        }
    }
    return true;
}

void MountStatusBuffer::addToPendingQueue(MountStatusStructure* mountStatus) {
    ThreadSyncGuard guard(&pendingQueueMutex_m);
    //ACS_LOG(LM_SOURCE_INFO,__PRETTY_FUNCTION__,
//	    (LM_DEBUG,"time: %llu ADDED", mountStatus->applicationTimestamp));
    pendingQueue_m.push_back(mountStatus);
}

void 
MountStatusBuffer::addToAvailableQueue(MountStatusStructure* mountStatus) {
    ThreadSyncGuard guard(&availableQueueMutex_m);
    availableQueue_m.push_back(mountStatus);
}

void MountStatusBuffer::defineTEMonitors(MountImpl* mountReference) {
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getMonitorRCAAzPosnRsp();
        tmp.locatorMethod = MountStatusStructure::getAZ_POSN_RSP;
        teMonitorList_m.push_back(tmp);
    }
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getMonitorRCAElPosnRsp();
        tmp.locatorMethod = MountStatusStructure::getEL_POSN_RSP;
        teMonitorList_m.push_back(tmp);
    }
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getMonitorRCAAzEnc();
        tmp.locatorMethod = MountStatusStructure::getGET_AZ_ENC;
        teMonitorList_m.push_back(tmp);
    }
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getMonitorRCAElEnc();
        tmp.locatorMethod = MountStatusStructure::getGET_EL_ENC;
        teMonitorList_m.push_back(tmp);
    }
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getMonitorRCASubrefAbsPosn();
        tmp.locatorMethod = MountStatusStructure::getGET_SUBREF_ABS_POSN;
        teMonitorList_m.push_back(tmp);
    }
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getMonitorRCASubrefRotation();
        tmp.locatorMethod = MountStatusStructure::getGET_SUBREF_ROTATION;
        teMonitorList_m.push_back(tmp);
    }
}

void MountStatusBuffer::defineTEPositionControls(MountImpl* mountReference) {
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getControlRCAAzTrajCmd();
        tmp.locatorMethod = MountStatusStructure::getAZ_TRAJ_CMD;
        tePositionControlList_m.push_back(tmp);
    }
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getControlRCAElTrajCmd();
        tmp.locatorMethod = MountStatusStructure::getEL_TRAJ_CMD;
        tePositionControlList_m.push_back(tmp);
    }
}

void MountStatusBuffer::defineTEFocusControls(MountImpl* mountReference) {
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getControlRCACntlSubrefAbsPosn();
        tmp.locatorMethod = MountStatusStructure::getSET_SUBREF_ABS_POSN;
        teFocusControlList_m.push_back(tmp);
    }
    {
        AMBMsgDef tmp;
        tmp.messageRCA = mountReference->getControlRCACntlSubrefRotation();
        tmp.locatorMethod = MountStatusStructure::getSET_SUBREF_ROTATION;
        teFocusControlList_m.push_back(tmp);
    }
}

// These are the locator methods for all of the commands and Monitors
AMBMsgData& 
MountStatusStructure::getAZ_TRAJ_CMD(MountStatusStructure* mountStatus) {
    return mountStatus->AZ_TRAJ_CMD;
}

AMBMsgData& 
MountStatusStructure::getEL_TRAJ_CMD(MountStatusStructure* mountStatus) {
    return mountStatus->EL_TRAJ_CMD;
}

AMBMsgData& 
MountStatusStructure::getGET_AZ_ENC(MountStatusStructure* mountStatus) {
    return mountStatus->GET_AZ_ENC;
}

AMBMsgData& 
MountStatusStructure::getGET_EL_ENC(MountStatusStructure* mountStatus) {
    return mountStatus->GET_EL_ENC;
}

AMBMsgData& 
MountStatusStructure::getAZ_POSN_RSP(MountStatusStructure* mountStatus) {
    return mountStatus->AZ_POSN_RSP;
}

AMBMsgData& 
MountStatusStructure::getEL_POSN_RSP(MountStatusStructure* mountStatus) {
    return mountStatus->EL_POSN_RSP;
}

AMBMsgData& 
MountStatusStructure::getGET_SUBREF_ABS_POSN(MountStatusStructure* mS) {
    return mS->GET_SUBREF_ABS_POSN;
}

AMBMsgData& 
MountStatusStructure::getGET_SUBREF_ROTATION(MountStatusStructure* mS) {
    return mS->GET_SUBREF_ROTATION;
}

AMBMsgData& 
MountStatusStructure::getSET_SUBREF_ABS_POSN(MountStatusStructure* mS) {
    return mS->SET_SUBREF_ABS_POSN;
}

AMBMsgData& 
MountStatusStructure::getSET_SUBREF_ROTATION(MountStatusStructure* mS) {
    return mS->SET_SUBREF_ROTATION;
}

