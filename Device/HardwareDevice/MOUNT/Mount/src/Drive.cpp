// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#include <iostream>
#include "Drive.h"

#undef DEBUG
using namespace ACUSim;

const double Drive::AZ_LOWER_LIMIT = -270.0 ;
const double Drive::AZ_UPPER_LIMIT =  270.0 ;
const double Drive::EL_LOWER_LIMIT =  -20.0 ;
const double Drive::EL_UPPER_LIMIT =  120.0 ;
const double Drive::AZ_MAINT_STOW  =   90.0 ;
const double Drive::AZ_SURVL_STOW  =   90.0 ;
const double Drive::EL_MAINT_STOW  =   88.0 ; // This should not be 90.0
const double Drive::EL_SURVL_STOW  =   15.0 ;

//------------------------------------------------------------------------------

Drive::Drive() : 
  active_m(false)
{
  //initialize();
  //start();
}

//------------------------------------------------------------------------------

Drive::~Drive() 
{ 
  //stop();
  //cleanUp();
}

void Drive::initialize(){
    Az->debug_level = 0;
    El->debug_level = 0;

    Az->limits(AZ_LOWER_LIMIT,AZ_UPPER_LIMIT);
    El->limits(EL_LOWER_LIMIT,EL_UPPER_LIMIT);

    Az->stows(AZ_MAINT_STOW,AZ_SURVL_STOW);
    El->stows(EL_MAINT_STOW,EL_SURVL_STOW);
}

void Drive::start(){
  // Start thread to read CAN messages and stick them in queue.
  active_m = true;
  if(pthread_create(&tid,
		    NULL,
		    &Drive::updateLoop,
		    static_cast<void*>(this))!=0)
    {
      active_m = false;
    }
  std::cout << "DRIVE THREAD STARTED" << std::endl;
}

void Drive::cleanUp()
{
}

void Drive::stop(){
  if(active_m)
    {
      active_m = false;
      pthread_join(tid, NULL); 
    }
  std::cout << "DRIVE THREAD STOPPED" << std::endl;
  
}

//------------------------------------------------------------------------------

int Drive::setAzPsn(double position,double velocity)
{
    return Az->set(position,velocity);
}

//------------------------------------------------------------------------------

int Drive::setElPsn(double position,double velocity)
{
    return El->set(position,velocity);
}

//------------------------------------------------------------------------------

void Drive::getAzPsn(double& position,double& velocity) const
{
    double accel;
    Az->get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

void Drive::getElPsn(double& position,double& velocity) const
{
    double accel;
    El->get_cur(position,velocity,accel);
}

//------------------------------------------------------------------------------

void Drive::getAzStatus(unsigned long long& status) const
{
    unsigned int stow = Az->get_stow_pin() == PIN_IN ? 1 : 0;
    unsigned int zerospeed = Az->driving() ? 1 : 0;
    unsigned long auxmode = (unsigned long)Az->get_aux_mode();
    unsigned char* ptr = (unsigned char*)&status;
    auxmode = auxmode ^ 3;
    
    ptr[0] = Az->limit;
    ptr[1] = zerospeed << 3 | stow << 4;
    ptr[2] = 0;
    ptr[3] = 0;
    ptr[4] = 0;
    ptr[5] = 0;
    ptr[6] = auxmode;
    ptr[7] = 0;
}

//------------------------------------------------------------------------------

void Drive::getElStatus(unsigned long long& status) const
{
  unsigned int stow = El->get_stow_pin() == PIN_IN ? 1 : 0;

  unsigned int zerospeed = El->driving() ? 1 : 0;
  double p, v, a;
  unsigned char* ptr = (unsigned char*)&status;
  int mode = (int)El->get_aux_mode();

  //int mode = static_cast<int>(El->get_aux_mode());
  // Mode is a weird large number!!!
  //mode = 0;

  int lut[4] = {0, 3, 12, 15};
  //unsigned long lut[4] = {0, 3, 12, 15};
  unsigned long auxmode = (unsigned long long)lut[mode];
  //unsigned long auxmode = static_cast<unsigned long>(lut[mode]);
  
  unsigned long overtop = 0;
  
  auxmode = auxmode ^ 0xF;
  //unsigned long mask = 0xfUL;
  //auxmode ^= 0xF;
  //auxmode ^= mask;
  El->get_cur(p,v,a);
  if (p > 90.)
      overtop = 0x10;
  
  ptr[0] = El->limit;
  ptr[1] = zerospeed << 3 | stow << 4;
  //ptr[1] = 0;
  ptr[2] = 0;
  ptr[3] = 0;
  ptr[4] = 0;
  ptr[5] = 0;
  ptr[6] = auxmode;
  ptr[7] = overtop;
  //ptr[6] = 0;
  //ptr[7] = 0;
}

#if 0  /* DISCARD!! */
//------------------------------------------------------------------------------

int Drive::setAzMode(AZEL_Mode_t m)
{
    return Az->set_mode(m);
}

//------------------------------------------------------------------------------

int Drive::setElMode(AZEL_Mode_t m)
{
    return El->set_mode(m);
}

//------------------------------------------------------------------------------

void Drive::getAzMode(AZEL_Mode_t& m) const
{
    m = Az->get_mode();
}

//------------------------------------------------------------------------------

void Drive::getElMode(AZEL_Mode_t& m) const
{
    m = El->get_mode();
}
#endif

//------------------------------------------------------------------------------

std::vector<char> Drive::getACUMode() const
{
#ifdef DEBUG
    cout << "Drive::getACUMode" << endl;
#endif
    std::vector<char> rtnVal(2);
#ifdef DEBUG
    cout << "======================================" << endl;
    cout << "Drive::getACUMode: getting EL mode " << endl;
#endif
    int elmode = (int)El->get_mode();
    rtnVal[0] = (char)elmode;
#ifdef DEBUG
    cout << "Drive::getACUMode: got     EL mode " << elmode << endl;
    cout << "======================================" << endl;
#endif
    rtnVal[0] <<= 4;
#ifdef DEBUG
    cout << "Drive::getACUMode: getting AZ mode " << endl;
#endif
    int azmode = (int)Az->get_mode();
    rtnVal[0] |= (char)azmode;
#ifdef DEBUG
    cout << "Drive::getACUMode: got     AZ mode " << azmode << endl;
    cout << "======================================" << endl;
#endif
    rtnVal[1] = ACU_REMOTE;  // always remote
    return rtnVal;
}

//------------------------------------------------------------------------------
int Drive::setACUMode(unsigned char mode)
{
#ifdef DEBUG
    cout << "Drive::setACUMode" << endl;
    cout << "Drive::setACUMode: received mode " << (int) mode << endl;
    cout << "Drive::setACUMode: about to send Az->set_mode" << endl;
#endif
    int requested_mode = (int) mode & 0xF;
    int rc;
#ifdef DEBUG
    cout << "Drive::setACUMode: received az mode " << requested_mode << endl;
#endif
    if (requested_mode<6)
	{
	rc = Az->set_mode((AZEL_Mode_t)(requested_mode));
#ifdef DEBUG
	cout << "Drive::setACUMode: az mode set" << endl;
#endif
	}
    else
	{
	rc =1;
#ifdef DEBUG
	cout << "Drive::setACUMode: az mode not set. Wrong mode requested" << endl;
#endif
	}
    
    // right shift to get requested el mode
    mode >>= 4;

#ifdef DEBUG
    cout << "Drive::setACUMode: about to send El->set_mode" << endl;
#endif
    requested_mode = (int) mode & 0xF;
#ifdef DEBUG
    cout << "Drive::setACUMode: received el mode " << requested_mode << endl;
#endif
    if (requested_mode<6)
	{   
	rc |= El->set_mode((AZEL_Mode_t)(mode & 0xF));
#ifdef DEBUG
	cout << "Drive::setACUMode: el mode set" << endl;
#endif
	}
    else
	{
	rc |= 1;
#ifdef DEBUG
	cout << "Drive::setACUMode: el mode not set. Wrong mode requested" << endl;
#endif
	}
#ifdef DEBUG
    cout << "Drive::setACUMode: return code " << (int) rc << endl;
#endif
    return rc;	// 0-OK, 1-ERROR
}

//------------------------------------------------------------------------------

void Drive::handler()
{
    Az->tick();
    El->tick();

}

//------------------------------------------------------------------------------

// This function is static to be the pthread_create() argument.  A pointer to 
// the class object is passed to allow access to its variables and methods().
void* Drive::updateLoop(void* data)
{
    Drive* This = static_cast<Drive*>(data);
    while(This->active_m)
	{
	usleep((int)(1000000 * TIME_UNIT));
	This->handler();	
	}
    pthread_exit(NULL);
}
