// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "TrajectoryQueue.h"
#include <acsThreadBase.h> // for ACS::ThreadSyncGuard 
#include <TETimeUtil.h> // for TETimeUtil
#include <cmath> // for abs
#include <ControlExceptions.h>
#include <MountImpl.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using std::ostringstream;
using std::abs;
using log_audience::DEVELOPER;
using Control::TrajectoryQueue;

TrajectoryQueue::TrajectoryQueue(Logging::Logger::LoggerSmartPtr logger) :
    Logging::Loggable(logger)
{
    // The initial az and el are set using the setCurrentPosition
    // function. This function is called by the Mount once it has obtained the
    // actual position of the antenna.
    lastReturnedTrajectory_m.time = 0;
    lastReturnedTrajectory_m.azRate = 0;
    lastReturnedTrajectory_m.elRate = 0;
}

void TrajectoryQueue::setCurrentPosition(double az, double el) {
    AUTO_TRACE(__func__);
    lastReturnedTrajectory_m.az = az;
    lastReturnedTrajectory_m.el = el;
    lastReturnedTrajectory_m.azRate = 0;
    lastReturnedTrajectory_m.elRate = 0;
    lastReturnedTrajectory_m.time = 0;
    {
        ostringstream msg;
        msg << "Initializing queue with (Az, El) = "
            << lastReturnedTrajectory_m.az << ", " 
            << lastReturnedTrajectory_m.el << "). Rates set to zero.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }
}

void TrajectoryQueue::insert(Control::TrajectorySeq commandSeq) {
    AUTO_TRACE(__func__);
  
    if (commandSeq.length() == 0) return;

    {
        ostringstream msg;
        const int n = commandSeq.length();
        msg << "Trajectory sequence contains " 
            << n << " commands from "
            << TETimeUtil::toTimeString(commandSeq[0].time)
            << " to " << TETimeUtil::toTimeString(commandSeq[n-1].time);
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }

    for (unsigned int idx = 0; idx < commandSeq.length() - 1; idx++) {
        if (commandSeq[idx].time >= commandSeq[idx+1].time) {
            ControlExceptions::IllegalParameterErrorExImpl 
                ex(__FILE__, __LINE__, __func__);
            ex.addData("Reason",
                       "Command Sequence is not in strict time order");
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }
    }
    
    ACS::ThreadSyncGuard guard(&trajQueueMutex_m);
    if (!trajQueue_m.empty()) {
        if (commandSeq[0].time <= trajQueue_m.back().time) {
            ostringstream msg;
            msg << "Command sequence overlaps existing commands."
                << " Time of last command in queue: " 
                << TETimeUtil::toTimeString(trajQueue_m.back().time)
                << " Time of first requested command: " 
                << TETimeUtil::toTimeString(commandSeq[0].time);
            ControlExceptions::IllegalParameterErrorExImpl 
                ex(__FILE__, __LINE__, __func__);
            ex.addData("Reason", msg.str());
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }
    }
    {
        ostringstream msg;
        for (unsigned int idx = 0; idx < commandSeq.length(); idx++) {
            msg << "Inserting a trajectory with a timestamp of "
                << TETimeUtil::toTimeString(commandSeq[idx].time)
                << " (" << commandSeq[idx].time << ") an (Az, El) of ("
                << commandSeq[idx].az << ", " << commandSeq[idx].el 
                << ") & rates of (" << commandSeq[idx].azRate << ", "
                << commandSeq[idx].elRate << ") into the queue. ";
            trajQueue_m.push_back(commandSeq[idx]);
        }
        LOG_TO_AUDIENCE(LM_TRACE, __func__, msg.str(), DEVELOPER);
    }
    {
        ostringstream msg;
        const int n = trajQueue_m.size();
        msg << "Trajectory queue now contains " << n << " commands.";
        //  << TETimeUtil::toTimeString(trajQueue_m[0].time)
        //  << " to " << TETimeUtil::toTimeString(trajQueue_m[n-1].time);
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }
}

void TrajectoryQueue::flush(ACS::Time applicationTime) {
    AUTO_TRACE(__func__);
  
    {
        ostringstream msg;
        msg << "Flushing all trajectories after "
            << TETimeUtil::toTimeString(applicationTime);
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }
    
    ACS::ThreadSyncGuard guard(&trajQueueMutex_m);
    while (!trajQueue_m.empty() && 
           trajQueue_m.back().time >= applicationTime ) {
        ostringstream msg;
        msg << "Last entry in the queue has a time of " 
            << TETimeUtil::toTimeString(trajQueue_m.back().time);
        trajQueue_m.pop_back();
        msg << ". Removed it. The queue size now is " << trajQueue_m.size();
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }
    {
        ostringstream msg;
        msg << "After flushing queue contains " 
            << trajQueue_m.size() << " entries.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }
}

Control::Trajectory TrajectoryQueue::getTrajectory(ACS::Time applicationTime) {
    AUTO_TRACE(__func__);
  
    {
        ostringstream msg;
        msg << "Looking for a trajectory at time " 
            << TETimeUtil::toTimeString(applicationTime);
        LOG_TO_AUDIENCE(LM_TRACE, __func__, msg.str(), DEVELOPER);
    }
  
    ACS::ThreadSyncGuard guard(&trajQueueMutex_m);
    while (!trajQueue_m.empty() &&
           trajQueue_m.front().time < applicationTime) {
        ostringstream msg;
        msg << "Discarding a trajectory with a time of " 
            << TETimeUtil::toTimeString(trajQueue_m.front().time) 
            << " as its in the past.";
        trajQueue_m.pop_front();
        msg << " New queue size is " << trajQueue_m.size();
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }

    if (trajQueue_m.empty()) {
        // The queue is empty. Stop the antenna at the last commanded
        // position. This behavior interacts closely with the isEmpty and
        // MountImpl::writeMsgs functions.
        ostringstream msg;
        msg << "Queue is empty - returned last value with (Az, El) = ("
            << lastReturnedTrajectory_m.az << ", "
            << lastReturnedTrajectory_m.el
            << "). Rates set to zero.";
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
        lastReturnedTrajectory_m.azRate = 0;
        lastReturnedTrajectory_m.elRate = 0;
        lastReturnedTrajectory_m.time = applicationTime;
        return lastReturnedTrajectory_m;
    }
  
    if (trajQueue_m.size() < 10) { // just log if the queue is nearly empty
        ostringstream msg;
        msg << "Queue contains " << trajQueue_m.size() 
            << " trajectories with times from " 
            << TETimeUtil::toTimeString(trajQueue_m.front().time) << " to " 
            << TETimeUtil::toTimeString(trajQueue_m.back().time);
        LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
    }

    if (trajQueue_m.front().time != applicationTime) {
        // There is no command for the requested time (but there are commands
        // pending). Stop the antenna at the last commanded position.
        lastReturnedTrajectory_m.azRate = 0;
        lastReturnedTrajectory_m.elRate = 0;
        lastReturnedTrajectory_m.time = applicationTime;
        {
            ostringstream msg;
            msg << "No trajectory available at "
                << TETimeUtil::toTimeString(applicationTime)
                << ". Next trajectory is at "
                << TETimeUtil::toTimeString(trajQueue_m.front().time)
                << ". Using last value with (Az, El) = ("
                << lastReturnedTrajectory_m.az << ", "
                << lastReturnedTrajectory_m.el 
                << "). Rates set to zero.";
            LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
        }
    } else {
        lastReturnedTrajectory_m = trajQueue_m.front();
        trajQueue_m.pop_front();

// Commented out the following log message as it was producing too many
// logs. This was at the request of Jeff Kern. This will inhibit our ability to
// diagnose timing problems in the Mount component. This log message can be
// reenabled if problems are seen.
// Ralph Marson 2009-12-16.
//         {
//             ostringstream msg;
//             msg << "Found a trajectory at "
//                 << TETimeUtil::toTimeString(lastReturnedTrajectory_m.time)
// 		<< " ";
// 	    MountImpl::printAzEl(msg, lastReturnedTrajectory_m.az, 
// 				 lastReturnedTrajectory_m.el,
// 				 lastReturnedTrajectory_m.azRate,
// 				 lastReturnedTrajectory_m.elRate);
//             LOG_TO_AUDIENCE(LM_DEBUG, __func__, msg.str(), DEVELOPER);
//         }
    }
    return lastReturnedTrajectory_m;  
}

bool TrajectoryQueue::isEmpty() {
    //AUTO_TRACE(__func__);
    
    // If the last returned trajectory had a non-zero velocity then claim the
    // queue is not empty. Then getTrajectory will be called once more, by the
    // MountImpl::writeMsgs function. When getTrajectory is called and the
    // queue is empty it will return a trajectory with a zero velocity. From
    // then on this function will return true (until the queue again has more
    // commands in it).
    
    ACS::ThreadSyncGuard guard(&trajQueueMutex_m);
    if (trajQueue_m.empty() && 
	abs(lastReturnedTrajectory_m.azRate) < 1E-7 && 
	abs(lastReturnedTrajectory_m.elRate) < 1E-7) {
        // A rate of 1E-7 is about half a degree/day  
        return true;
    } else {
        return false;
    }
}

