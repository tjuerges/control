#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountController component test.
'''
from sys import argv,exit
from Acspy.Clients.SimpleClient import PySimpleClient
import ACS
import Control
import time
from Control import DeviceConfig

# Configurations
try:
    mountName = argv[1]
except IndexError:
    print "Please specify a mount component name!"
    exit(0)

# Get Az modes (current and commanded)   
mountCompCommandAzMode = "_get_setAzAxisMode().get_sync()[0]"
mountCompCurrentAzMode = "_get_azAxisMode().get_sync()[0]"
mountCompCommandElMode = "_get_setElAxisMode().get_sync()[0]"
mountCompCurrentElMode = "_get_elAxisMode().get_sync()[0]"

mountConfig = DeviceConfig("",
                           "",
                           "",
                           "",
                           [],
                           -1,
                           "")

# Test
print "Test Begin"
# Make an instance of the PySimpleClient
simpleClient = PySimpleClient()

# Get the mountc reference and configure it
mountComp = simpleClient.getComponent(mountName)
if not mountComp:
    print "Failed to get a mount reference"
    print "Check!!!"
    print "a) component name"
    print "b) if container is running"
    exit(0)

# We have a no null component, so we proceed with the test
mountComp = mountComp._narrow(Control.MountAEC)
print "Checking current az axis mode values"
azmode = eval("mountComp." + mountCompCurrentAzMode)
print "Az Actual value read is     :", azmode
elmode = eval("mountComp." + mountCompCurrentElMode)
print "El Actual value read is     :", elmode
print ""
# Go through all the az axis modes for the mount component. Return to SHUTDOWN_MODE at the end
print "Changing axis modes"
for i in range(0,3):
    print str(i)+".- Setting the azimuth axis mode to",Control.AxisMode._item(i)
    mountCompSetAzMode = "_get_setAzAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
    print str(i)+".- Setting the elevation axis mode to",Control.AxisMode._item(i)
    mountCompSetElMode = "_get_setElAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
    azmode = eval("mountComp." + mountCompSetAzMode)
    azmode = eval("mountComp." + mountCompCommandAzMode)
    print "Az Reference value set to:", azmode
    elmode = eval("mountComp." + mountCompSetElMode)
    elmode = eval("mountComp." + mountCompCommandElMode)
    print "El Reference value set to:", elmode
    time.sleep(60)
    azmode = eval("mountComp." + mountCompCurrentAzMode)
    print "Az Actual value read is  :", azmode
    elmode = eval("mountComp." + mountCompCurrentElMode)
    print "El Actual value read is  :", elmode
    print ""

simpleClient.releaseComponent(mountComp._get_name())
simpleClient.disconnect()
print "Test End"
