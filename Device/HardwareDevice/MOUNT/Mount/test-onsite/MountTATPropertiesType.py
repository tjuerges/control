#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
Mount component properties test.
'''
import types
import unittest
import ACS
from Acspy.Clients.SimpleClient import PySimpleClient
import Control
from Control import DeviceConfig

configure = 1
shutdown  = 1
def shutdownCountup():
    global shutdown
    shutdown +=1
    return shutdown

def shutdownCountdown():
    global shutdown
    shutdown -=1
    return shutdown

testCounter = 0
def testCountup():
    global testCounter
    testCounter +=1
    return testCounter

class ALCATELAntennaTestCase(unittest.TestCase):
    def __init__(self, methodName='runTest'):
        try:
            self._client=PySimpleClient.getInstance()
            self._ref=self._client.getComponent("CONTROL/MountAEC/Node0x03/Generic")
        except:
            print 'Unable to obtain component reference'
            exit(1)

        global configure
        if configure:
            print "configure"
            configuration = DeviceConfig("",
                                         "",
                                         "",
                                         "",
                                         [],
                                         -1,
                                         "")
            self._ref.configure(configuration,"")
            configure = 0
        else:
            shutdownCountup()
        unittest.TestCase.__init__(self,methodName)

    def __del__(self):
        global shutdown
        if shutdownCountdown() == 0:
            print "shutdown"
            self._ref.shutdown()
            del(self._ref)
            try:
                self._client.releaseComponent("CONTROL/MountAEC/Node0x00/Generic")
            except:
                print 'Could not release component'
        self._client.disconnect()
        del(self._client)

    def generic_existence(self,propertyName="dummy"):
        if propertyName=="dummy":
            pass
        else: 
            print ""
            print str(testCountup())+": "+propertyName
            try:
                propertyReference = eval("self._ref._get_"+propertyName+"()")
            except:
                propertyReference = None
            try:
                propertyValue = propertyReference.get_sync()
            except:
                propertyValue = None
            print "    checking result not null"
            self.assertNotEqual(propertyReference, None, propertyName+": not defined in the idl")
            self.assertNotEqual(propertyValue, None, propertyName+": no value received")
            self.assertEqual(propertyValue[1].type, 0, propertyName+": read error (completion not ok)")

    def generic_type(self,propertyName="dummy",propertyType=None):
        if propertyName=="dummy":
            pass
        else: 
            self.generic_existence(propertyName)
            propertyReference = eval("self._ref._get_"+propertyName+"()")
            propertyValue = propertyReference.get_sync()
            print "    checking property value type", propertyValue[0]
            self.assertEqual(type(propertyValue[0]) is propertyType, True, propertyName+": invalid type")

    def generic_enum_range(self,propertyName="dummy",enumItems=()):
        if propertyName=="dummy":
            pass
        else: 
            self.generic_existence(propertyName)
            propertyReference = eval("self._ref._get_"+propertyName+"()")
            propertyValue = propertyReference.get_sync()
            print "    checking property value range", propertyValue[0]
            self.assertEqual(propertyValue[0] in enumItems, True, propertyName+": Invalid range")

    def test_idleStowTime_get(self):
        propertyName = "idleStowTime"
        self.generic_type(propertyName,types.IntType)

    def test_idleStowTime_set(self):
        propertyName = "setIdleStowTime"
        self.generic_type(propertyName,types.IntType)

    def test_shutterMode_get(self):
        propertyName = "shutterMode"
        self.generic_enum_range(propertyName,Control.ShutterMode._items)

#    def test_shutterMode_set(self):
#        propertyName = "setShutterMode"
#        self.generic_enum_range(propertyName,Control.ShutterMode._items)

    def test_azAxisMode_get(self):
        propertyName = "azAxisMode"
        self.generic_enum_range(propertyName,Control.AxisMode._items)

    def test_azAxisMode_set(self):
        propertyName = "setAzAxisMode"
        self.generic_enum_range(propertyName,Control.AxisMode._items)

    def test_elAxisMode_get(self):
        propertyName = "elAxisMode"
        self.generic_enum_range(propertyName,Control.AxisMode._items)

    def test_elAxisMode_set(self):
        propertyName = "setElAxisMode"
        self.generic_enum_range(propertyName,Control.AxisMode._items)
        
    def test_azBrakeEngaged_get(self):
        propertyName = "azBrakeEngaged"
        self.generic_enum_range(propertyName,ACS.Bool._items)

#    def test_azBrakeEngaged_set(self):
#        propertyName = "setAzBrakeEngaged"
#        self.generic_enum_range(propertyName,ACS.Bool._items)
        
    def test_elBrakeEngaged_get(self):
        propertyName = "elBrakeEngaged"
        self.generic_enum_range(propertyName,ACS.Bool._items)

#    def test_elBrakeEngaged_set(self):
#        propertyName = "setElBrakeEngaged"
#        self.generic_enum_range(propertyName,ACS.Bool._items)

    def test_azStatus_get(self):
        propertyName = "azStatus"
        self.generic_existence(propertyName)

    def test_elStatus_get(self):
        propertyName = "elStatus"
        self.generic_existence(propertyName)

    def test_tolerance_set(self):
        propertyName = "setTolerance"
        self.generic_type(propertyName,types.FloatType)

    def test_onTarget_get(self):
        propertyName = "onTarget"
        self.generic_enum_range(propertyName,ACS.Bool._items)

    def test_applyAcuPointingModel_get(self):
        propertyName = "applyAcuPointingModel"
        self.generic_enum_range(propertyName,ACS.Bool._items)

    def test_applyAcuPointingModel_set(self):
        propertyName = "setApplyAcuPointingModel"
        self.generic_enum_range(propertyName,ACS.Bool._items)

    def test_recordPositionData_set(self):
        propertyName = "recordPositionData"
        self.generic_enum_range(propertyName,ACS.Bool._items)

    def test_azEncoder_get(self):
        propertyName = "azEncoder"
        self.generic_type(propertyName,types.FloatType)

    def test_elEncoder_get(self):
        propertyName = "elEncoder"
        self.generic_type(propertyName,types.FloatType)

    # Specific for MountAEC
    def test_temperatures_get(self):
        propertyName = "temperature"
        sensorNumber = 26
        sensorRange  = sensorNumber + 1
        for sensor in range(1, sensorRange):
            propertyNameSensor = propertyName + str(sensor)
            self.generic_type(propertyNameSensor,types.ListType)

if __name__ == '__main__':
    unittest.main()            
        
