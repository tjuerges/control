#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountController component test.
'''
from sys import argv,exit
from Acspy.Clients.SimpleClient import PySimpleClient
import ACS
import Control
import time
import math 
from Control import DeviceConfig
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.QoS import setObjectTimeout
from Acspy.Util.ACSCorba import getManager

# Configurations
try:
    mountName = argv[1]
except IndexError:
    print "Please specify a mount component name!"
    exit(0)

try:
    maxIterations = float(argv[2])
except IndexError:
    print "Using default of 1 iteration"
    maxIterations = 1

# Get Az modes (current and commanded)   
mountCompCommandAzMode = "_get_setAzAxisMode().get_sync()[0]"
mountCompCurrentAzMode = "_get_azAxisMode().get_sync()[0]"
mountCompCommandElMode = "_get_setElAxisMode().get_sync()[0]"
mountCompCurrentElMode = "_get_elAxisMode().get_sync()[0]"
mountCompSetTrajectoryBegin = "setTrajectory(" 
mountCompSetTrajectoryEnd   = ")" 

mountConfig = DeviceConfig("",
                           [])

# Test
print "Test Begin"
# Make an instance of the PySimpleClient
simpleClient = PySimpleClient()
setObjectTimeout(getManager(), 20000)

# Get the mountc reference and configure it
mountComp = simpleClient.getComponent(mountName)
if not mountComp:
    print "Failed to get a mount reference"
    print "Check!!!"
    print "a) component name"
    print "b) if container is running"
    exit(0)
setObjectTimeout(mountComp, 20000)

# We have a no null component, so we proceed with the test
#mountComp = mountComp._narrow(Control.MountAEC)
print "Checking current az axis mode values"
azmode = eval("mountComp." + mountCompCurrentAzMode)
print "Az Actual value read is     :", azmode
elmode = eval("mountComp." + mountCompCurrentElMode)
print "El Actual value read is     :", elmode
print ""
print "Changing axis modes"
for i in range(0,3):
    print "Setting the azimuth axis mode to",Control.AxisMode._item(i)
    mountCompSetAzMode = "_get_setAzAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
    print "Setting the elevation axis mode to",Control.AxisMode._item(i)
    mountCompSetElMode = "_get_setElAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
    azmode = eval("mountComp." + mountCompSetAzMode)
    azmode = eval("mountComp." + mountCompCommandAzMode)
    print "Az Reference value set to:", azmode
    elmode = eval("mountComp." + mountCompSetElMode)
    elmode = eval("mountComp." + mountCompCommandElMode)
    print "El Reference value set to:", elmode
    time.sleep(1)
    azmode = eval("mountComp." + mountCompCurrentAzMode)
    print "Az Actual value read is  :", azmode
    elmode = eval("mountComp." + mountCompCurrentElMode)
    print "El Actual value read is  :", elmode
    print ""

#mountComp._get_recordPositionData().set_sync(ACS.Bool._item(1))

# Parameters
JumpsAhead = 9
TimeZero  = 12219292800
TEPeriod  = 480000
maxCommands = 30
angle = 45.0
angle1 = 30.0

# Times
startTime = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod
commandTime  = startTime 
endTime   = startTime + maxCommands*TEPeriod

stroke = Control.AntPositionStrokeData(0,
                                       0,
                                       0.0,
                                       0.0,
                                       0.0,
                                       0.0,
                                       0.0,
                                       0.0)
parameters = Control.AntPositionParameters(0.0,
                                           0.0,
                                           stroke)

# Iteration
counter=0
late=0
for iterations in range(0,maxIterations):
    commands = 0
    while (endTime-commandTime)>= 0:
        print counter, iterations, commands, commandTime
        counter = counter+1
        positionCMD = "mountComp." + mountCompSetTrajectoryBegin  + str(commandTime) + "," + str(math.radians(angle))+",0.0," +str(math.radians(angle1))+",0.0, parameters" + mountCompSetTrajectoryEnd
        # print positionCMD
        result = eval(positionCMD)
        commands = commands + 1
        commandTime = commandTime + TEPeriod
    time.sleep(1.0)
    startTime = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod
    endTime = startTime + maxCommands*TEPeriod
    if commandTime - startTime < 0:
        print "ERROR ERROR ERROR ERROR  ERROR ERROR ERROR ERROR"
        print "We are late by", (startTime - commandTime)
        print "ERROR ERROR ERROR ERROR  ERROR ERROR ERROR ERROR"
        late = late+1
    else:
        print "We are OK:", (commandTime - startTime)
time.sleep(10.)
print "Sent:", counter,"positions"
print "Late:", late, "times"
#mountComp._get_recordPositionData().set_sync(ACS.Bool._item(0))
simpleClient.releaseComponent(mountComp._get_name())
simpleClient.disconnect()
print "Test End"
