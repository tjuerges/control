#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
Mount component setTrajectory test.
'''
import math
import time
import unittest
from Acspy.Nc.Consumer import Consumer
from Mount.TestCases.Common import *
import Control

class MyConsumer(Consumer):
    eventNumber=0
    def processEvent(self,
                     type_name=None,
                     event_name=None,
                     corba_any=None,
                     se=None):
            data_structure = corba_any.value()
            #print "==", self.eventNumber, data_structure.timestamp, data_structure.azPosition, data_structure.elPosition
	    self.eventNumber = self.eventNumber+1

class TestCase(CommonTestCase):
    def __init__(self, methodName='runTest',
                 componentName="CONTROL/MOUNTCOMMON"):
        CommonTestCase.__init__(self,methodName,componentName)

    def tearDown(self):
        self.reference.shutdown()
        self.HwLifeCycleDown()
    def eventHandler(self,event):
	print "+++", event.timestamp, event.azPosition, event.elPosition    
    def check_setTrajectory(self):
        self.reference.encoder()
        g = MyConsumer(Control.CHANNELNAME_CONTROLREALTIME)
        g.addSubscription(Control.MountStatusData,self.eventHandler)
        g.consumerReady()
        # Parameters
        JumpsAhead = 30
        TimeZero  = 12219292800
        TEPeriod  = 480000

        # trajectory points
        azimuth = 90.0
        elevation = 30.0
        timestamp = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod
        point1 = Control.Trajectory(math.radians(azimuth),
                                   math.radians(elevation),
                                   0.0,
                                   0.0,
                                   timestamp)

        trajectory = [point1]
        print self.reference.isMountStatusDataPublicationEnabled()
        self.reference.enableMountStatusDataPublication(True)
        print self.reference.isMountStatusDataPublicationEnabled()
        self.reference.setTrajectory(trajectory)

        g = MyConsumer(Control.CHANNELNAME_CONTROLREALTIME)
        #g.addSubscription(Control.AntPositionData,dataHandler)
        g.consumerReady()
        time.sleep(120)
        g.disconnect()
        
    
    def test_setTrajectory(self):
        self.HwLifeCycleUp()
        self.reference.encoder()
        self.check_setTrajectory()

if __name__ == '__main__':
    unittest.main()            
