#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountAEC component properties test.
'''
import Control__POA
from aecClient import *
import unittest
import types
from  MountAECTestCases import *

class PropertiesTestCase(MountAECTestCase):
    def __init__(self, methodName='runTest', componentName="CONTROL/MountAEC/Node0x03/Generic"):

        # TBD: To find the cause why this is needed
        # I'm sure there is a circular dependency somewhere
        # and it took me a while to find out this shit!
        reload(Control)
        MountAECTestCase.__init__(self,methodName,componentName)

    def setUp(self):
        try:
            self.acu = aecClient("Test",0,3)
        except:
            self.acu=None
            
    def tearDown(self):
        # Release acu after every test if defined
        if self.acu:
            del(self.acu)

    def test00_get_acu_mode_rsp(self):
        self.dummy_test()
        
    def test00_get_az_posn_rsp(self):
        self.dummy_test()

    def test00_get_el_posn_rsp(self):
        self.dummy_test()

    def test00_get_acu_error(self):
        self.dummy_test()
        
    def test00_get_az_brake(self):
        self.dummy_test()

    def test00_get_az_enc(self):
        self.dummy_test()

    def test01_get_az_motor(self):
        acu_command=propertyName="get_az_motor"
        propertyType=types.IntType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_az_motor_currents(self):
        acu_command=propertyName="get_az_motor_currents"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_az_motor_temps(self):
        acu_command=propertyName="get_az_motor_temps"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_az_motor_torque(self):
        acu_command=propertyName="get_az_motor_torque"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    # JIB (2006-03-25): This should check all servo coeffs
    # from 0 to F. Note that currently the simulation does
    # not simulate D to F coefficients
    def test00_get_az_servo_coeffs(self):
        self.dummy_test()

    def test01_get_az_motion_lim(self):
        acu_command=propertyName="get_az_motion_lim"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_az_status(self):
        acu_command=propertyName="get_az_status"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test00_get_el_brake(self):
        self.dummy_test()

    def test00_get_el_enc(self):
        self.dummy_test()

    def test01_get_el_motor(self):
        acu_command=propertyName="get_el_motor"
        propertyType=types.IntType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_el_motor_currents(self):
        acu_command=propertyName="get_el_motor_currents"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_el_motor_temps(self):
        acu_command=propertyName="get_el_motor_temps"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_el_motor_torque(self):
        acu_command=propertyName="get_el_motor_torque"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    # JIB (2006-03-25): This should check all servo coeffs
    # from 0 to F. Note that currently the simulation does
    # not simulate D to F coefficients
    def test00_get_el_servo_coeffs(self):
        self.dummy_test()

    def test01_get_el_motion_lim(self):
        acu_command=propertyName="get_el_motion_lim"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_el_status(self):
        acu_command=propertyName="get_el_status"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_stow_pin(self):
        acu_command=propertyName="get_stow_pin"
        propertyType=types.ListType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test01_get_idle_stow_time(self):
        acu_command="get_stow_time"
        propertyName="idleStowTime"
        propertyType=types.IntType
        self.verbose=1
        self.check_compare(propertyName,propertyType,acu_command)

    def test00_get_system_status(self):
        self.dummy()

    # JIB 2006-03-25: TBD! These are not yet implemented in the idl
    def test00_pt_model_coeffs(self):
        self.dummy()
        
    def test00_get_shutter(self):
        self.dummy()

    def test00_stow_pin(self):
        self.dummy()
        
    def test00_subref_abs_posn(self):
        self.dummy()

    def test00_subref_delta_posn(self):
        self.dummy()
        
    def test00_subref_limits(self):
        self.dummy()

    def test00_subref_status(self):
        self.dummy()

    def test00_get_metr_mode(self):
        self.dummy()

    def test00_get_metr_tilt(self):
        self.dummy()

    def test00_get_metr_temps(self):
        self.dummy()

    def test00_get_metr_deltas(self):
        self.dummy()

    def test00_get_power_status(self):
        self.dummy()

    def test00_get_ac_status(self):
        self.dummy()

# JIB 2006-03-25: TBD Add control points tests
        

if __name__ == '__main__':
    unittest.main()            
