#!/usr/bin/env python
from time import sleep
import Control
from Acspy.Nc.Consumer import Consumer

class MyConsumer(Consumer):
    eventNumber=0
    def processEvent(self,
                     type_name=None,
                     event_name=None,
                     corba_any=None,
                     se=None):
            data_structure = corba_any.value()
            #print "==", self.eventNumber, data_structure.azPosition, data_structure.elPosition
	    self.eventNumber = self.eventNumber+1
           
if __name__ == '__main__':

    g = MyConsumer(Control.CHANNELNAME_CONTROLREALTIME)
    #g.addSubscription(Control.AntPositionData,dataHandler)
    g.consumerReady()
    g.disconnect()
