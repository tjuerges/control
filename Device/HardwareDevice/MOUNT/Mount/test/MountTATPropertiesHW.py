#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountController component test.
'''
from sys import argv,exit
from Acspy.Clients.SimpleClient import PySimpleClient
import ACS
import Control
from Control import DeviceConfig
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.QoS import setObjectTimeout
from Acspy.Util.ACSCorba import getManager

# Configurations
try:
    mountName = argv[1]
except IndexError:
    print "Please specify a mount component name!"
    exit(0)

# Get Az modes (current and commanded)   
mountCompCommandAzMode = "_get_setAzAxisMode().get_sync()[0]"
mountCompCurrentAzMode = "_get_azAxisMode().get_sync()[0]"
mountCompCommandElMode = "_get_setElAxisMode().get_sync()[0]"
mountCompCurrentElMode = "_get_elAxisMode().get_sync()[0]"

# Get idle stow time (current and commanded)
mountCompCommandIdleStowTime = "_get_setIdleStowTime().get_sync()[0]"
mountCompCurrentIdleStowTime = "_get_idleStowTime().get_sync()[0]"

# Get shutter mode (current and commanded)
mountCompCommandShutterMode = "_get_setShutterMode().get_sync()[0]"
mountCompCurrentShutterMode = "_get_shutterMode().get_sync()[0]"

mountConfig = DeviceConfig("",
                           "",
                           "",
                           "",
                           [],
                           -1,
                           "")

# Test
print "Test Begin"
# Make an instance of the PySimpleClient
simpleClient = PySimpleClient()
setObjectTimeout(getManager(), 20000)

# Get the mountc reference and configure it
mountComp = simpleClient.getComponent(mountName)
if not mountComp:
    print "Failed to get a mount reference"
    print "Check component name or if container is running"
    exit(0)
setObjectTimeout(mountComp, 20000)

# We have a no null component, so we proceed with the test
mountComp = mountComp._narrow(Control.MountAEC)
mountComp.configure(mountConfig,"")
print ""
azmode = eval("mountComp." + mountCompCurrentAzMode)
print "Az Actual value read is     :", azmode
elmode = eval("mountComp." + mountCompCurrentElMode)
print "El Actual value read is     :", elmode
print ""
# Go through all the az axis modes for the mount component. Return to SHUTDOWN_MODE at the end
#print "Checking az axis modes"
#for i in range(0,5):
#    print str(i)+".- Setting the azimuth axis mode to",Control.AxisMode._item(i)
#    mountCompSetAzMode = "_get_setAzAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
#    azmode = eval("mountComp." + mountCompSetAzMode)
#    azmode = eval("mountComp." + mountCompCommandAzMode)
#    print "Az Reference value set to:", azmode
#    azmode = eval("mountComp." + mountCompCurrentAzMode)
#    print "Az Actual value read is  :", azmode
#mountCompSetAzMode = "_get_setAzAxisMode().set_sync(Control.AxisMode._item("+str(0)+"))"
#azmode = eval("mountComp." + mountCompSetAzMode)
#print ""

# Go through all the el axis modes for the mount component. Return to SHUTDOWN_MODE at the end
#print "Checking el axis modes"
#for i in range(0,5):
#    print str(i)+".- Setting the elevation axis mode to",Control.AxisMode._item(i)
#    mountCompSetElMode = "_get_setElAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
#    elmode = eval("mountComp." + mountCompSetElMode)
#    elmode = eval("mountComp." + mountCompCommandElMode)
#    print "El Reference value set to:", elmode
#    elmode = eval("mountComp." + mountCompCurrentElMode)
#    print "El Actual value read is  :", elmode
#mountCompSetElMode = "_get_setElAxisMode().set_sync(Control.AxisMode._item("+str(0)+"))"
#elmode = eval("mountComp." + mountCompSetElMode)
#print ""

# Get/Set idle stow time (current and commanded)
mountCompCurrentIdleStowTime = "_get_idleStowTime().get_sync()[0]"
#mountCompSetIdleStowTime = "_get_setIdleStowTime().set_sync(250)"
#mountCompCommandIdleStowTime = "_get_setIdleStowTime().get_sync()[0]"
#
print "Checking idle stow time property"
InitialIdleStowTime = eval("mountComp."+mountCompCurrentIdleStowTime)
print "Idle stow actual value read  :", InitialIdleStowTime
print ""

#idleStowTime = eval("mountComp."+mountCompSetIdleStowTime)
#idleStowTime = eval("mountComp."+mountCompCommandIdleStowTime)
#print "Idle stow time set to        :", idleStowTime
#idleStowTime = eval("mountComp."+mountCompCurrentIdleStowTime)
#print "Idle stow actual value read  :", idleStowTime
#
#mountCompSetIdleStowTime = "_get_setIdleStowTime().set_sync("+str(InitialIdleStowTime)+")"
#idleStowTime = eval("mountComp."+mountCompSetIdleStowTime)
#idleStowTime = eval("mountComp."+mountCompCommandIdleStowTime)
#print "Idle stow time set to        :", idleStowTime
#idleStowTime = eval("mountComp."+mountCompCurrentIdleStowTime)
#print "Idle stow actual value read  :", idleStowTime

# Get Metrology Temperature Sensors (current and commanded)
mountCompTemperatureSensorsBegin = "_get_temperature"
mountCompTemperatureSensorsEnd   = "().get_sync()[0]"
for sensor in range(1, 27):
    temperatures = eval("mountComp."+mountCompTemperatureSensorsBegin+str(sensor)+mountCompTemperatureSensorsEnd)
    print "Sensor:", sensor
    print "Temperature 1:", temperatures[0]
    print "Temperature 2:", temperatures[1]
    print "Temperature 3:", temperatures[2]
    print "Temperature 4:", temperatures[3]
    print ""

# Get Metrology Tilt parameters (current and commanded)
mountCompTilt1X = "_get_tilt1X().get_sync()[0]"
mountCompTilt1Y = "_get_tilt1Y().get_sync()[0]"
mountCompTilt1Temperature = "_get_tilt1Temperature().get_sync()[0]"
tilt1X = eval("mountComp."+mountCompTilt1X)
tilt1Y = eval("mountComp."+mountCompTilt1Y)
tilt1Temperature = eval("mountComp."+mountCompTilt1Temperature)
print "Tilt1:"
print "tilt1X", tilt1X
print "tilt1Y", tilt1Y
print "tilt1Temperature", tilt1Temperature
print ""

# Get Metrology Tilt parameters (current and commanded)
mountCompTilt2X = "_get_tilt2X().get_sync()[0]"
mountCompTilt2Y = "_get_tilt2Y().get_sync()[0]"
mountCompTilt2Temperature = "_get_tilt2Temperature().get_sync()[0]"
tilt2X = eval("mountComp."+mountCompTilt2X)
tilt2Y = eval("mountComp."+mountCompTilt2Y)
tilt2Temperature = eval("mountComp."+mountCompTilt2Temperature)
print "Tilt2:"
print "tilt2X", tilt2X
print "tilt2Y", tilt2Y
print "tilt2Temperature", tilt2Temperature
print ""

# Get shutter mode
mountCompCurrentShutterMode = "_get_shutterMode().get_sync()[0]"
print "Shutter mode:"
shutterMode = eval("mountComp."+mountCompCurrentShutterMode)
print "shutterMode actual value :", shutterMode
print ""

# Get brakes
print "Brakes engaged:"
mountCompCurrentAzBrakeEngaged = "_get_azBrakeEngaged().get_sync()[0]"
azBrakeEngaged = eval("mountComp."+mountCompCurrentAzBrakeEngaged)
print "azBrakeEngaged actual value :", azBrakeEngaged

mountCompCurrentElBrakeEngaged = "_get_elBrakeEngaged().get_sync()[0]"
elBrakeEngaged = eval("mountComp."+mountCompCurrentElBrakeEngaged)
print "elBrakeEngaged actual value :", elBrakeEngaged
print ""

# Get antenna positions
mountCompGetAntPositionData = "getAntPositionData()"
AntPositionData = eval("mountComp."+mountCompGetAntPositionData)
print "timestamp:               :", AntPositionData.timestamp
print "azPosition actual value  :", AntPositionData.azPosition
print "elPosition actual value  :", AntPositionData.elPosition
print "azCommanded actual value :", AntPositionData.azCommanded
print "elCommanded actual value :", AntPositionData.elCommanded
print ""

mountComp.shutdown()
simpleClient.releaseComponent(mountComp._get_name())
simpleClient.disconnect()
print "Test End"
