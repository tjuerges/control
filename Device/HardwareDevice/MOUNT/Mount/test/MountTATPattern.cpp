/*******************************************************************************
*    ALMA - Atacama Large Millimiter Array
*    (c) European Southern Observatory, 2002
*    Copyright by ESO (in the framework of the ALMA collaboration)
*    and Cosylab 2002, All rights reserved
*
*    This library is free software; you can redistribute it and/or
*    modify it under the terms of the GNU Lesser General Public
*    License as published by the Free Software Foundation; either
*    version 2.1 of the License, or (at your option) any later version.
*
*    This library is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public
*    License along with this library; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

#include "vltPort.h"
#include <math.h>

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)(void)&use_rcsId,(void *) &rcsId);

#include <cppunit/extensions/HelperMacros.h>
#include "MountC.h"
#include "MountPattern.h"
#include "acstimeTimeUtil.h"

/* 
 * A test case for the Vector class
 *
 */
class VectorTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( VectorTestCase );
    
    CPPUNIT_TEST( test_set );
    CPPUNIT_TEST( test_copy );
    CPPUNIT_TEST( test_add );
    CPPUNIT_TEST( test_setVelocity );
    CPPUNIT_TEST( test_rotation );
    CPPUNIT_TEST( test_vRotation );
    CPPUNIT_TEST( test_director );
    CPPUNIT_TEST( test_directorVelocity );

    CPPUNIT_TEST_SUITE_END();
    
  protected:
    Vector m_vector1;
    Vector m_vector2;
    double xPattern;
    double yPattern;
    double xVPattern;
    double yVPattern;
    int axis;
    double angle;
    double xV, yV, zV;
    double xVoffset, yVoffset, zVoffset;

    double sinXPattern;
    double cosXPattern;
    double sinYPattern;
    double cosYPattern;
    double sinAngle;
    double cosAngle;
    
    double latitude;
    double longitude;
    double vLatitude;
    double vLongitude;

  public:
    void setUp();

  protected:
    void test_set();
    void test_copy();
    void test_add();
    void test_setVelocity();
    void test_rotation();
    void test_vRotation();
    void test_director();
    void test_directorVelocity();
};

CPPUNIT_TEST_SUITE_REGISTRATION( VectorTestCase );

void VectorTestCase::setUp()
{
    xPattern=0.5;
    yPattern=0.7;
    xVPattern=2.0;
    yVPattern=3.0;
    angle = 0.5;

    sinAngle = sin(angle);
    cosAngle = cos(angle);

    sinXPattern = sin(xPattern);
    cosXPattern = cos(xPattern);
    sinYPattern = sin(yPattern);
    cosYPattern = cos(yPattern);

    xV = cosYPattern * cosXPattern;
    yV = cosYPattern * sinXPattern;
    zV = sinYPattern;

    xVoffset = cosXPattern * cosYPattern;
    yVoffset = cosXPattern * sinYPattern;
    zVoffset = sinXPattern;


    latitude = asin(zV);
    longitude = atan2(yV, xV);

    double slong, clong;

    vLatitude = zV / cos(latitude);
    slong = sin(longitude);
    clong = cos(longitude);
    if (fabs(slong) > fabs(clong))
	vLongitude = -(xV + sin(latitude) * clong * vLatitude) /
	    yVoffset;
    else
	vLongitude =  (yV + sin(latitude) * slong * vLatitude) /
	    xVoffset;


}

void VectorTestCase::test_set()
{
    m_vector1.set(xPattern, yPattern);
    CPPUNIT_ASSERT( m_vector1.xV == xV );
    CPPUNIT_ASSERT( m_vector1.yV == yV );
    CPPUNIT_ASSERT( m_vector1.zV == zV );
    
    xV = cosXPattern * cosYPattern;
    yV = cosXPattern * sinYPattern;
    zV = sinXPattern;
    m_vector2.set(yPattern, xPattern);

    {
    double difference_xV = fabs(xV - m_vector2.xV);
    double difference_yV = fabs(yV - m_vector2.yV);
    double difference_zV = fabs(zV - m_vector2.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
}

void VectorTestCase::test_copy()
{
    Vector vector;
    
    m_vector1.set(xPattern, yPattern);
    vector.copy(m_vector1);
    CPPUNIT_ASSERT( vector.xV == m_vector1.xV );
    CPPUNIT_ASSERT( vector.yV == m_vector1.yV );
    CPPUNIT_ASSERT( vector.zV == m_vector1.zV );
    
}

void VectorTestCase::test_add()
{
    
    xV += cosXPattern * cosYPattern;
    yV += cosXPattern * sinYPattern;
    zV += sinXPattern;

    m_vector1.set(xPattern, yPattern);
    m_vector2.set(yPattern, xPattern);
    m_vector1.add(m_vector2,1.0);

    xV += 0.5 * cosXPattern * cosYPattern;
    yV += 0.5 * cosXPattern * sinYPattern;
    zV += 0.5 * sinXPattern;
    m_vector1.add(m_vector2,0.5);

    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }

    xV -= 1.5*m_vector2.xV;
    yV -= 1.5*m_vector2.yV;
    zV -= 1.5*m_vector2.zV;

    m_vector1.add(m_vector2,-1.5);

    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }

}

void VectorTestCase::test_setVelocity()
{
    xV = -cosYPattern * sinXPattern * xVPattern 
	- sinYPattern * cosXPattern * yVPattern; 
    yV =  cosYPattern * cosXPattern * xVPattern 
        - sinYPattern * sinXPattern * yVPattern; 
    zV =  cosYPattern * yVPattern;
    m_vector1.setVelocity(xPattern, yPattern, xVPattern, yVPattern);
    
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }

    xV = -cosXPattern * sinYPattern * yVPattern 
	- sinXPattern * cosYPattern * xVPattern; 
    yV =  cosXPattern * cosYPattern * yVPattern 
        - sinXPattern * sinYPattern * xVPattern; 
    zV =  cosXPattern * xVPattern;
    m_vector2.setVelocity(yPattern, xPattern, yVPattern, xVPattern);

    {
    double difference_xV = fabs(xV - m_vector2.xV);
    double difference_yV = fabs(yV - m_vector2.yV);
    double difference_zV = fabs(zV - m_vector2.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
    
}

void VectorTestCase::test_rotation()
{
    double y1 =  cosAngle * yV + sinAngle * zV;
    zV = -sinAngle * yV + cosAngle * zV;
    yV = y1;

    m_vector1.set(xPattern, yPattern);
    m_vector1.rotation(1,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }

    xV = cosYPattern * cosXPattern;
    yV = cosYPattern * sinXPattern;
    zV = sinYPattern;
    double z1 =  cosAngle * zV + sinAngle * xV;
    xV = -sinAngle * zV + cosAngle * xV;
    zV = z1;
    m_vector1.set(xPattern, yPattern);
    m_vector1.rotation(2,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }

    xV = cosYPattern * cosXPattern;
    yV = cosYPattern * sinXPattern;
    zV = sinYPattern;
    double x1 =  cosAngle * xV + sinAngle * yV;
    yV = -sinAngle * xV + cosAngle * yV;
    xV = x1;

    m_vector1.set(xPattern, yPattern);
    m_vector1.rotation(3,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
    xV = cosYPattern * cosXPattern;
    yV = cosYPattern * sinXPattern;
    zV = sinYPattern;
    m_vector1.set(xPattern, yPattern);
    m_vector1.rotation(4,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
}

void VectorTestCase::test_vRotation()
{
    double y1 = -sinAngle * yV + cosAngle * zV;
    zV = -cosAngle * yV - sinAngle * zV;
    xV = 0.;
    yV = y1;
    m_vector1.set(xPattern, yPattern);
    m_vector1.vRotation(1,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
    xV = cosYPattern * cosXPattern;
    yV = cosYPattern * sinXPattern;
    zV = sinYPattern;
    double z1 = -sinAngle * zV + cosAngle * xV;
    xV = -cosAngle * zV - sinAngle * xV;
    yV = 0.;
    zV = z1;
    m_vector1.set(xPattern, yPattern);
    m_vector1.vRotation(2,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
    xV = cosYPattern * cosXPattern;
    yV = cosYPattern * sinXPattern;
    zV = sinYPattern;
    double x1 = -sinAngle * xV + cosAngle * yV;
    yV = -cosAngle * xV - sinAngle * yV;
    zV = 0.;
    xV = x1;
    m_vector1.set(xPattern, yPattern);
    m_vector1.vRotation(3,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
    xV = cosYPattern * cosXPattern;
    yV = cosYPattern * sinXPattern;
    zV = sinYPattern;
    m_vector1.set(xPattern, yPattern);
    m_vector1.vRotation(4,angle);
    {
    double difference_xV = fabs(xV - m_vector1.xV);
    double difference_yV = fabs(yV - m_vector1.yV);
    double difference_zV = fabs(zV - m_vector1.zV);

    CPPUNIT_ASSERT(difference_xV < 10^(-8));
    CPPUNIT_ASSERT(difference_yV < 10^(-8));
    CPPUNIT_ASSERT(difference_zV < 10^(-8));
    }
}
void VectorTestCase::test_director()
{

    double latitude1=0, longitude1=0;
    m_vector1.set(xPattern, yPattern);
    m_vector1.director(&longitude1, &latitude1);
    CPPUNIT_ASSERT_EQUAL( latitude, latitude1 );
    CPPUNIT_ASSERT_EQUAL( longitude, longitude1 );
    
}

void VectorTestCase::test_directorVelocity()
{

    double vLatitude1=0, vLongitude1=0;
    m_vector1.set(xPattern, yPattern);
    m_vector2.set(yPattern, xPattern);

    m_vector1.directorVelocity(longitude, latitude, m_vector2, 
			       &vLongitude1, &vLatitude1);

    {
    double difference_lat = fabs(vLatitude-vLatitude1);
    double difference_lon = fabs(vLongitude-vLongitude1);

    CPPUNIT_ASSERT(difference_lat < 10^(-8));
    CPPUNIT_ASSERT(difference_lon < 10^(-8));
    }
    
}


/* 
 * A test case for the Stroke class
 *
 */
class StrokeTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( StrokeTestCase );
    
    CPPUNIT_TEST( test_constructor );
    CPPUNIT_TEST( test_set );
    CPPUNIT_TEST( test_reset );
    CPPUNIT_TEST( test_get );

    CPPUNIT_TEST_SUITE_END();
    
  public:
    void setUp();

  protected:
    void test_constructor();
    void test_set();
    void test_reset();
    void test_get();
    double xStroke;
    double yStroke;
    Control::Mount::doubleSeq_var x_var;
    Control::Mount::doubleSeq_var y_var;
    Control::Mount::DurationSSeq_var dS_var;
    Control::Mount::StrokeS_var strokeS_var;
    Control::Mount::StrokeS_var strokeSCurve_var;
    acstime::Epoch start;
    acstime::Epoch currentEpochS;
    Stroke *stroke_prt;
};

CPPUNIT_TEST_SUITE_REGISTRATION( StrokeTestCase );

void StrokeTestCase::setUp()
{
    xStroke = 0.1;
    yStroke = 0.2;

    x_var = new Control::Mount::doubleSeq(4);
    x_var->length(4);
    x_var[0] = 0.1;                               
    x_var[1] = 0.2;                                
    x_var[2] = 0.3;
    x_var[3] = 0.4;

    y_var = new Control::Mount::doubleSeq(4);
    y_var->length(4);
    y_var[0] = 0.4;                              
    y_var[1] = 0.1;                    
    y_var[2] = 0.2;
    y_var[3] = 0.3;

    dS_var = new Control::Mount::DurationSSeq(4);
    dS_var->length(4);
    dS_var[0].value = 10000000LL;  //  1s
    dS_var[1].value = 20000000LL;  //  4s
    dS_var[2].value = 40000000LL;  //  8s
    dS_var[3].value = 80000000LL; // 10s

    start = TimeUtil::ace2epoch(ACE_OS::gettimeofday());

    strokeS_var = new Control::Mount::StrokeS;
    strokeS_var->xArray = x_var.in();
    strokeS_var->yArray = y_var.in();
    strokeS_var->timeRef = start;
    strokeS_var->descrOrientation = 0.78;
    strokeS_var->mode = Control::LINEAR_STROKE;
    strokeS_var->timeArray = dS_var.in();
    strokeS_var->longCenter = 0.11;
    strokeS_var->latCenter = 0.12;
    strokeS_var->polarOrientation = 0.;

    strokeSCurve_var = new Control::Mount::StrokeS;
    strokeSCurve_var->xArray = x_var.in();
    strokeSCurve_var->yArray = y_var.in();
    strokeSCurve_var->timeRef = start;
    strokeSCurve_var->descrOrientation = 0.78;
    strokeSCurve_var->mode = Control::CURVE_STROKE;
    strokeSCurve_var->timeArray = dS_var.in();
    strokeSCurve_var->longCenter = 0.11;
    strokeSCurve_var->latCenter = 0.12;
    strokeSCurve_var->polarOrientation = 0.;

}

void StrokeTestCase::test_constructor()
{

    stroke_prt = new Stroke(&xStroke, &yStroke);
    CPPUNIT_ASSERT_EQUAL( xStroke, stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( yStroke, stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0, (int)stroke_prt->get_stroke_ptr() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    xStroke = 0.2;
    yStroke = 0.1;
    CPPUNIT_ASSERT_EQUAL( xStroke, stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( yStroke, stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    delete(stroke_prt);
    
}

void StrokeTestCase::test_set()
{

    stroke_prt = new Stroke(&xStroke, &yStroke);
    stroke_prt->set(strokeS_var);
    CPPUNIT_ASSERT_EQUAL( strokeS_var.ptr(), stroke_prt->get_stroke_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeS_var->mode, stroke_prt->get_mode() );
    CPPUNIT_ASSERT_EQUAL( strokeS_var->timeRef.value, stroke_prt->get_startEpoch().value ); 
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
    delete(stroke_prt);

    stroke_prt = new Stroke(&xStroke, &yStroke);
    stroke_prt->set(strokeSCurve_var);
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var.ptr(), stroke_prt->get_stroke_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->mode, stroke_prt->get_mode() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->timeRef.value, stroke_prt->get_startEpoch().value );     
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( (double) strokeSCurve_var->timeArray[0].value, stroke_prt->get_dEnd() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->xArray[0], stroke_prt->get_xEnd() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->yArray[0], stroke_prt->get_yEnd() );
    CPPUNIT_ASSERT_EQUAL( 0, (int) stroke_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( dS_var->length(), (CORBA::ULong) stroke_prt->get_length() );
    delete(stroke_prt);

}

void StrokeTestCase::test_reset()
{

    stroke_prt = new Stroke(&xStroke, &yStroke);
    stroke_prt->set(strokeS_var);
    CPPUNIT_ASSERT_EQUAL( strokeS_var.ptr(), stroke_prt->get_stroke_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeS_var->mode, stroke_prt->get_mode() );
    CPPUNIT_ASSERT_EQUAL( strokeS_var->timeRef.value, stroke_prt->get_startEpoch().value ); 
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );

    stroke_prt->reset();
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_m_yStroke_value() );
    
    delete(stroke_prt);
}

void StrokeTestCase::test_get()
{
    stroke_prt = new Stroke(&xStroke, &yStroke);
    stroke_prt->set(strokeS_var);
    CPPUNIT_ASSERT_EQUAL( strokeS_var.ptr(), stroke_prt->get_stroke_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeS_var->mode, stroke_prt->get_mode() );
    CPPUNIT_ASSERT_EQUAL( strokeS_var->timeRef.value, stroke_prt->get_startEpoch().value ); 
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );

    double x,y;
    double xV, yV;

    x = 0.1; y = 0.1;
    xV = 0.1; yV = 0.1;
    currentEpochS.value = start.value - start.value+dS_var[0].value;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_m_xStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );

    x = 0.0; y = 0.0;
    xV = 0.0; yV = 0.0;
    currentEpochS.value = start.value;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
    CPPUNIT_ASSERT_EQUAL( x_var[0], stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[0], stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( x_var[1], stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[1], stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );

    currentEpochS.value = start.value+dS_var[0].value/2;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
    CPPUNIT_ASSERT_EQUAL( x_var[0]+0.5*x_var[1], stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[0]+0.5*y_var[1], stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( x_var[1], stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[1], stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );

    delete(stroke_prt);

    stroke_prt = new Stroke(&xStroke, &yStroke);
    stroke_prt->set(strokeSCurve_var);
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var.ptr(), stroke_prt->get_stroke_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->mode, stroke_prt->get_mode() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->timeRef.value, stroke_prt->get_startEpoch().value );     
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );

    CPPUNIT_ASSERT_EQUAL( (double)strokeSCurve_var->timeArray[0].value, stroke_prt->get_dEnd() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->xArray[0], stroke_prt->get_xEnd() );
    CPPUNIT_ASSERT_EQUAL( strokeSCurve_var->yArray[0], stroke_prt->get_yEnd() );
    CPPUNIT_ASSERT_EQUAL( 0, (int) stroke_prt->get_index() );

    x = 0.0; y = 0.0;
    xV = 0.0; yV = 0.0;
    currentEpochS.value = start.value;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );

    currentEpochS.value = start.value+dS_var[0].value/2;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
//    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );
    CPPUNIT_ASSERT_EQUAL( 0, (int) stroke_prt->get_index() );

    currentEpochS.value = start.value+dS_var[0].value;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
    CPPUNIT_ASSERT_EQUAL( x_var[0], stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[0], stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );
    CPPUNIT_ASSERT_EQUAL( 1, (int) stroke_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( (double)strokeSCurve_var->timeArray[1].value, stroke_prt->get_dEnd() );

    currentEpochS.value = start.value+dS_var[0].value+5000000;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
    CPPUNIT_ASSERT_EQUAL( x_var[0]+0.5*(x_var[1]-x_var[0]), stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[0]+0.5*(y_var[1]-y_var[0]), stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );
    CPPUNIT_ASSERT_EQUAL( 1, (int) stroke_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( (double)strokeSCurve_var->timeArray[1].value, stroke_prt->get_dEnd() );
    
    currentEpochS.value = start.value+dS_var[1].value;
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
    CPPUNIT_ASSERT_EQUAL( x_var[1], stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[1], stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );
    CPPUNIT_ASSERT_EQUAL( 2, (int) stroke_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( (double)strokeSCurve_var->timeArray[2].value, stroke_prt->get_dEnd() );

    currentEpochS.value = start.value+dS_var[1].value+(dS_var[2].value-dS_var[1].value)/2;
    double factor = 0.5; // half way between the two points
    stroke_prt->get(currentEpochS, &x, &y, &xV, &yV);
    CPPUNIT_ASSERT_EQUAL( x_var[1]+factor*(x_var[2]-x_var[1]), stroke_prt->get_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( y_var[1]+factor*(y_var[2]-y_var[1]), stroke_prt->get_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_xVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( 0., stroke_prt->get_yVStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), stroke_prt->get_m_xStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), stroke_prt->get_m_yStroke_value() );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xStroke_value(), x );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yStroke_value(), y );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_xVStroke_value(), xV );
    CPPUNIT_ASSERT_EQUAL( stroke_prt->get_yVStroke_value(), yV );
    CPPUNIT_ASSERT_EQUAL( 2, (int) stroke_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( (double)strokeSCurve_var->timeArray[2].value, stroke_prt->get_dEnd() );

    delete(stroke_prt);

}
/* 
 * A test case for the Pattern class
 *
 */
class PatternTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( PatternTestCase );
    
    CPPUNIT_TEST( test_constructor );
    CPPUNIT_TEST( test_set );
    CPPUNIT_TEST( test_reset );
    CPPUNIT_TEST( test_dummy );

    CPPUNIT_TEST_SUITE_END();
    
  public:
    void setUp();

  protected:
    void test_constructor();
    void test_set();
    void test_reset();
    void test_dummy();

    Pattern *pattern_prt;
    Control::StrokeMode strokeMode;
    double xStroke, yStroke;
    double xPattern, yPattern;
    double xOffset, yOffset;
    Control::Mount::doubleSeq_var x_var;
    Control::Mount::doubleSeq_var y_var;
    Control::Mount::DurationSSeq_var dS_var;
    Control::Mount::StrokeS strokeS;
    Control::Mount::StrokeS strokeSArc;
    Control::Mount::StrokeS strokeSCurve;
    acstime::Epoch start;
    acstime::Epoch currentEpochS;
    Control::Mount::StrokeSeq strokeSeq0;
    Control::Mount::StrokeSeq strokeSeq1;
    Control::Mount::StrokeSeq strokeSeq2;
    Control::Mount::StrokeSeq strokeSeq3;
    Control::Mount::StrokeSeq strokeSeq4;
};

CPPUNIT_TEST_SUITE_REGISTRATION( PatternTestCase );

void PatternTestCase::setUp()
{
    strokeMode = Control::LINEAR_STROKE;
    xStroke    = 0;
    yStroke    = 0;
    xPattern   = 0;
    yPattern   = 0;
    xOffset    = 0;
    yOffset    = 0;

    // The rest is to be able to set a stroke and strokeSeq
    x_var = new Control::Mount::doubleSeq(4);
    x_var->length(4);
    x_var[0] = 0.1;                               
    x_var[1] = 0.2;                                
    x_var[2] = 0.3;
    x_var[3] = 0.4;

    y_var = new Control::Mount::doubleSeq(4);
    y_var->length(4);
    y_var[0] = 0.4;                              
    y_var[1] = 0.1;                    
    y_var[2] = 0.2;
    y_var[3] = 0.3;

    dS_var = new Control::Mount::DurationSSeq(4);
    dS_var->length(4);
    dS_var[0].value = 10000000LL;  //  1s
    dS_var[1].value = 20000000LL;  //  4s
    dS_var[2].value = 40000000LL;  //  8s
    dS_var[3].value = 80000000LL; // 10s

    start = TimeUtil::ace2epoch(ACE_OS::gettimeofday());

    // strokeS_var = new Control::Mount::StrokeS;
    strokeS.xArray = x_var.in();
    strokeS.yArray = y_var.in();
    strokeS.timeRef = start;
    strokeS.descrOrientation = 0.78;
    strokeS.mode = Control::LINEAR_STROKE;
    strokeS.timeArray = dS_var.in();
    strokeS.longCenter = 0.11;
    strokeS.latCenter = 0.12;
    strokeS.polarOrientation = 0.;

    strokeSCurve.xArray = x_var.in();
    strokeSCurve.yArray = y_var.in();
    strokeSCurve.timeRef = start;
    strokeSCurve.descrOrientation = 0.78;
    strokeSCurve.mode = Control::CURVE_STROKE;
    strokeSCurve.timeArray = dS_var.in();
    strokeSCurve.longCenter = 0.11;
    strokeSCurve.latCenter = 0.12;
    strokeSCurve.polarOrientation = 0.;

    strokeSArc.xArray = x_var.in();
    strokeSArc.yArray = y_var.in();
    strokeSArc.timeRef = start;
    strokeSArc.descrOrientation = 0.78;
    strokeSArc.mode = Control::ARC_STROKE;
    strokeSArc.timeArray = dS_var.in();
    strokeSArc.longCenter = 0.11;
    strokeSArc.latCenter = 0.12;
    strokeSArc.polarOrientation = 0.;

    strokeSeq0.length(0);

    strokeSeq1.length(1);
    strokeSeq1[0] = strokeS;
 
    strokeSeq2.length(1);
    strokeSeq2[0] = strokeSCurve;

    strokeSeq3.length(1);
    strokeSeq3[0] = strokeSArc;

    strokeSeq4.length(2);
    strokeSeq4[0] = strokeS;
    strokeSeq4[1] = strokeSCurve;
    
}

void PatternTestCase::test_constructor()
{
    pattern_prt = new Pattern();
    CPPUNIT_ASSERT_EQUAL( 0, (int)pattern_prt->get_strokeSeq_ptr() );
    CPPUNIT_ASSERT_EQUAL( 0, pattern_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_xPattern_value() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_yPattern_value() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_xOffset_value() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_yOffset_value() );
    delete(pattern_prt);
}

void PatternTestCase::test_set()
{ 
    pattern_prt = new Pattern();
    pattern_prt->set(&strokeSeq0);
    CPPUNIT_ASSERT_EQUAL( 0, (int)pattern_prt->get_strokeSeq_ptr() );
    delete(pattern_prt);

    pattern_prt = new Pattern();
    pattern_prt->set(&strokeSeq1);
    CPPUNIT_ASSERT_EQUAL( (int)&strokeSeq1, (int)pattern_prt->get_strokeSeq_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeSeq1.length(), pattern_prt->get_length() );
    CPPUNIT_ASSERT_EQUAL( 0, pattern_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( strokeSeq1[0].timeRef.value, pattern_prt->get_startEpochHelper_value());
    CPPUNIT_ASSERT_EQUAL( strokeSeq1[0].descrOrientation, pattern_prt->get_descrOrientation());
    CPPUNIT_ASSERT_EQUAL( strokeSeq1[0].mode, pattern_prt->get_mode_value());
    delete(pattern_prt);

    pattern_prt = new Pattern();
    pattern_prt->set(&strokeSeq3);
    CPPUNIT_ASSERT_EQUAL( (int)&strokeSeq3, (int)pattern_prt->get_strokeSeq_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeSeq3.length(), pattern_prt->get_length() );
    CPPUNIT_ASSERT_EQUAL( 0, pattern_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].timeRef.value, pattern_prt->get_startEpochHelper_value());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].descrOrientation, pattern_prt->get_descrOrientation());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].mode, pattern_prt->get_mode_value());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].longCenter, pattern_prt->get_longCenter());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].latCenter, pattern_prt->get_latCenter());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].polarOrientation, pattern_prt->get_polarOrientation());
    delete(pattern_prt);
}

void PatternTestCase::test_reset()
{ 
    pattern_prt = new Pattern();
    pattern_prt->set(&strokeSeq3);
    CPPUNIT_ASSERT_EQUAL( (int)&strokeSeq3, (int)pattern_prt->get_strokeSeq_ptr() );
    CPPUNIT_ASSERT_EQUAL( strokeSeq3.length(), pattern_prt->get_length() );
    CPPUNIT_ASSERT_EQUAL( 0, pattern_prt->get_index() );
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].timeRef.value, pattern_prt->get_startEpochHelper_value());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].descrOrientation, pattern_prt->get_descrOrientation());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].mode, pattern_prt->get_mode_value());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].longCenter, pattern_prt->get_longCenter());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].latCenter, pattern_prt->get_latCenter());
    CPPUNIT_ASSERT_EQUAL( strokeSeq3[0].polarOrientation, pattern_prt->get_polarOrientation());

    pattern_prt->reset();
    CPPUNIT_ASSERT_EQUAL( 0, (int)pattern_prt->get_strokeSeq_ptr() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_xPattern_value() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_yPattern_value() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_xOffset_value() );
    CPPUNIT_ASSERT_EQUAL( 0., pattern_prt->get_yOffset_value() );
    delete(pattern_prt);
}

void PatternTestCase::test_dummy()
{ 
    
}

/*
 * Main function running the tests
 */ 
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int
main( int argc, char* argv[] )
{
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;

  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );        

  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );      

  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
  runner.run( controller );

  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
  outputter.write(); 

  return result.wasSuccessful() ? 0 : 1;
}

