#! /usr/bin/env python

#
# General constants
secondsBetweenSamples = 5

mgrHost = 'DV01'
acuPtcAmbCh = 0

#
# ACU Az and El
acuAmbId = 0
AZ_POSN_RSP = 0x12
EL_POSN_RSP = 0x02
GET_AZ_ENC  = 0x17
GET_EL_ENC  = 0x07

#
# PTC Displacement and Tilt
ptcAmbId = 1
GET_METR_DISPL_1 = 0x6000
GET_METR_DISPL_2 = 0x6001
GET_METR_TILT_0  = 0x5000
GET_METR_TILT_1  = 0x5001
GET_METR_TILT_2  = 0x5002
GET_METR_TILT_3  = 0x5003
GET_METR_TILT_4  = 0x5004


#
# Helper functions
from Acspy.Common.TimeHelper import *
import math
import struct
import time

def currentAzEl(mgr):
    """Get current Az and El from ACU."""
    (azPosns, time) = mgr.monitor(acuPtcAmbCh, acuAmbId, AZ_POSN_RSP)
    (elPosns, time) = mgr.monitor(acuPtcAmbCh, acuAmbId, EL_POSN_RSP)
    (azEnc, time) = mgr.monitor(acuPtcAmbCh, acuAmbId, GET_AZ_ENC)
    (elEnc, time) = mgr.monitor(acuPtcAmbCh, acuAmbId, GET_EL_ENC)

    azPos = unpackCurrentPosition(azPosns)
    elPos = unpackCurrentPosition(elPosns)
    azEnc = unpackAzEncoder(azEnc)
    elEnc = unpackElEncoders(elEnc)
    return '%s|%s|%s|%s' % (azPos, elPos, azEnc, elEnc)

def currentDisp(mgr):
    """Get current displacement from PTC."""
    (disp1, time) = mgr.monitor(acuPtcAmbCh, ptcAmbId, GET_METR_DISPL_1)
    (disp2, time) = mgr.monitor(acuPtcAmbCh, ptcAmbId, GET_METR_DISPL_2)

    d1 = unpackDisplacement(disp1)
    d2 = unpackDisplacement(disp2)
    return '%s|%s' % (d1, d2)

def currentTilt(mgr):
    """Get current tilts from PTC."""
    (tilt0, time) = mgr.monitor(acuPtcAmbCh, ptcAmbId, GET_METR_TILT_0)
    (tilt1, time) = mgr.monitor(acuPtcAmbCh, ptcAmbId, GET_METR_TILT_1)
    (tilt2, time) = mgr.monitor(acuPtcAmbCh, ptcAmbId, GET_METR_TILT_2)
    (tilt3, time) = mgr.monitor(acuPtcAmbCh, ptcAmbId, GET_METR_TILT_3)
    (tilt4, time) = mgr.monitor(acuPtcAmbCh, ptcAmbId, GET_METR_TILT_4)

    t0 = unpackTilts(tilt0)
    t1 = unpackTilts(tilt1)
    t2 = unpackTilts(tilt2)
    t3 = unpackTilts(tilt3)
    t4 = unpackTilts(tilt4)
    return '%s|%s|%s|%s|%s' % (t0, t1, t2, t3, t4)

tu = TimeUtil()
def currentTime():
    """Get current ACS time as local time string."""
    lt = time.localtime(tu.epoch2py(getTimeStamp()))
    return '%04d-%02d-%02d@%02d:%02d:%02d' \
           % (lt[0], lt[1], lt[2], lt[3], lt[4], lt[5])

def unpackAzEncoder(enc):
    az = struct.unpack('!L', enc)
    return az[0]

def unpackCurrentPosition(pos):
    ps = struct.unpack('!2l', pos)
    return ps[0] * 2 * math.pi / 0x7FFFFFFF

def unpackDisplacement(disp):
    d = struct.unpack('!h', disp)
    return d[0]

def unpackElEncoders(enc):
    es = struct.unpack('!2L', enc)
    return '%d,%d'% es

def unpackTilts(tilt):
    ts = struct.unpack('!3h', tilt)
    return '%d,%d,%d'% ts

#
# Main functions
def intro():
    """ Display introductory data. """
    print '# Press <Ctrl>-C to exit.'
    print '# Starting reporting: ' + currentTime()
    print '# Line format:'
    print '# time|azPos|elPos|azEnc0|elEnc0,elEnc1|disp1|disp2|xTilt0,yTilt0,temp0|xTilt1,yTilt1,temp1|xTilt2,yTilt2,temp2|xTilt3,yTilt3,temp3|xTilt4,yTilt4,temp4'
    
def report(mgr):
    """ Report current values for monitored values. """
    t = currentTime()
    azEl = currentAzEl(mgr)
    disp = currentDisp(mgr)
    tilt = currentTilt(mgr)
    record = '%s|%s|%s|%s' % (t, azEl, disp, tilt)
    return record

def wrapup():
    """ Display wrapup data. """
    print '# Stopping reporting: ' + currentTime()
    print '# ',


#
# Main
import ambManager

intro()
mgr = ambManager.AmbManager(mgrHost)
try:
    while True:
        print report(mgr)
        time.sleep(secondsBetweenSamples)
except KeyboardInterrupt:
    pass
wrapup()
del(mgr)

#
# -oOo-
