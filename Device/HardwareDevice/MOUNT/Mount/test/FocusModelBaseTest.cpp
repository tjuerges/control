/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2007 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2007-03-26  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>

#include <FocusModelBase.h>

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

void printModel(FocusModelBase& fm){
  Control::ModelTermSeq_var modelSequence = fm.getFocusModel();

  for (unsigned int idx = 0; idx < modelSequence->length(); idx++) {
    std::cout << "\t" <<(*modelSequence)[idx].termName << "\t\t" 
	      << (*modelSequence)[idx].termValue << std::endl;
  }
}


int main(int argc, char *argv[])
{

  std::cout << "Now starting test" << std::endl;
  
  FocusModelBase fm;

  /* Test that we can retrieve the model */
  std::cout << "Initial Focus Model:" << std::endl;
  printModel(fm);
  std::cout << "\n" << std::endl;


  /* Test that we can retrieve a single term */
  std::cout << "Term by Term Retreival:" << std::endl;
  std::cout << "\t" << "X0" << "\t\t" 
	    << fm.getFocusModelTerm("X0") << std::endl;
  std::cout << "\t" << "Y0" << "\t\t" 
	    << fm.getFocusModelTerm("Y0") << std::endl;
  std::cout << "\t" << "Z0" << "\t\t" 
	    << fm.getFocusModelTerm("Z0") << std::endl;
  try {
    std::cout << "\t" << "W0" << "\t\t" 
	    << fm.getFocusModelTerm("W0") << std::endl;
    std::cout << "ERROR: Illegal term W0 returned" << std::endl;
    return -1;
  } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    std::cout << "\tIllegal paramter exception correctly thrown" << std::endl;
  }
  std::cout << "\n" << std::endl;


  /* Test that we can set multiple terms */
  {
    std::cout << "Updating Model" << std::endl;
    Control::ModelTermSeq newModel;
    newModel.length(3);
    newModel[0].termName  = "X0";
    newModel[0].termValue = 1.0;
    newModel[1].termName = "Y0";
    newModel[1].termValue = 3.0;
    newModel[2].termName = "Z0";
    newModel[2].termValue = 2.0;
    fm.setFocusModel(newModel);
    printModel(fm);
    std::cout <<"\n" << std::endl;
  }

  
  /* Test sending an illegal term name */
  {
    std::cout << "Updating Model with illegal term" << std::endl;
    Control::ModelTermSeq newModel;
    newModel.length(2);
    newModel[0].termName  = "X0";
    newModel[0].termValue = 10.0;
    newModel[1].termName = "W0";
    newModel[1].termValue = 3.0;
    try {
      fm.setFocusModel(newModel);
      std::cout << "ERROR: Illegal parameter was accepted" << std::endl;
      return -1;
    } catch (ControlExceptions::IllegalParameterErrorExImpl& ex) {
    std::cout << "\tIllegal paramter exception correctly thrown" << std::endl;
    }
    printModel(fm);
  }


  
  return 0;

}








