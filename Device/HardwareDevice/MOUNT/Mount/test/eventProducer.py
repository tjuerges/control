#!/usr/bin/env python

import CORBA
import Control
from Acspy.Nc.Supplier import Supplier
from time import sleep

try:
    channelName = Control.CHANNELNAME_CONTROLREALTIME
except:
    channelName = "CONTROL_REALTIME"

supplier = Supplier(channelName)

strokeData   = Control.AntPositionStrokeData(Control.PT_NONE,Control.LINEAR_STROKE,0.0,0.0,0.0,0.0,0.0,0.0)
positionData = Control.AntPositionData("CONTROL/ALMA001", CORBA.FALSE, 0, 0,
                                       0, 0, 0, 0, 0, 0, long(0),strokeData)


for x in range(480000,96000000,480000):
    positionData.azPrePosition = float((x-240000) * 1.0)
    positionData.elPrePosition = float((x-240000) * 2.0)
    positionData.azPosition    = float(x * 1.0)   
    positionData.elPosition    = float(x * 2.0)
    positionData.azEncoder     = float(x * 100.0)   
    positionData.elEncoder     = float(x * 200.0)   
    positionData.azCommanded   = float(x * 1000.0)
    positionData.elCommanded   = float(x * 2000.0)  
    positionData.timestamp     = long(x)
    
    # Publish position
    supplier.publishEvent(simple_data=positionData)
    
    sleep(0.1)
    

supplier.disconnect()
    
