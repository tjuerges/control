#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountController component test.
'''



import threading
import time
from sys import argv,exit
from Acspy.Clients.SimpleClient import PySimpleClient
import ACS
import Control
from Control import DeviceConfig
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.QoS import setObjectTimeout
from Acspy.Util.ACSCorba import getManager

def totalIterations(totalTime,period):
    return int(totalTime/period)

class PeriodicRead(threading.Thread):

    def __init__(self, component, msg, seconds, iterations):
        self.component = component
        self.msg = msg
        self.seconds = seconds
        self.iterations = iterations
        threading.Thread.__init__(self)

    def run( self ):
        i=0
        while(i<=self.iterations):
            antennaData = eval("self.component."+self.msg)
            print i, antennaData.azPosition, antennaData.elPosition
            i=i+1
            time.sleep( self.seconds )

class PeriodicWrite(threading.Thread):

    def __init__(self, msg, seconds, iterations, azStartPoint, azEndPoint, elStartPoint, elEndPoint):
        self.msg = msg
        self.seconds = seconds
        self.iterations = iterations
        self.azStartPoint = azStartPoint
        self.azEndPoint = azEndPoint
        self.elStartPoint = elStartPoint
        self.elEndPoint = elEndPoint
        threading.Thread.__init__(self)

    def run( self ):
        JumpsAhead = 3
        TEPeriod = 480000
        StartPoint = 12219292800
        LoopPeriod = 10000000
        
        startTime = StartPoint*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod
        lastTime  = startTime
        i=0
        while(i<=self.iterations):
                azPoint = self.azStartPoint+(self.azEndPoint-self.azStartPoint)*float(i)/self.iterations
                elPoint = self.elStartPoint+(self.elEndPoint-self.elStartPoint)*float(i)/self.iterations
                initialTime = startTime+LoopPeriod*i
                if lastTime >= initialTime:
                    initialTime = lastTime + TEPeriod
                for j in range(0,22):
                    commandTime = initialTime + TEPeriod*j
                    result = eval(self.msg+"("+str(azPoint)+",0.0,0.0,"+str(elPoint)+",0.0,0.0,"+str(commandTime)+")")
#                    print self.msg+"("+str(azPoint)+",0.0,0.0,"+str(elPoint)+",0.0,0.0,"+str(commandTime)+")"
#                    print j, commandTime
                    lastTime = commandTime
                i=i+1
                time.sleep( self.seconds )

# Configurations
try:
    mountName = argv[1]
except IndexError:
    print "Please specify a mount component name!"
    exit(0)

try:
    totalTime = float(argv[2])
except IndexError:
    print "Please specify test total time in seconds"
    exit(0)

# Get Az modes (current and commanded)   
mountCompCommandAzMode = "_get_setAzAxisMode().get_sync()[0]"
mountCompCurrentAzMode = "_get_azAxisMode().get_sync()[0]"
mountCompCommandElMode = "_get_setElAxisMode().get_sync()[0]"
mountCompCurrentElMode = "_get_elAxisMode().get_sync()[0]"

mountConfig = DeviceConfig("",
                           "",
                           "",
                           "",
                           [],
                           -1,
                           "")

# Test
print "Test Begin"
# Make an instance of the PySimpleClient
simpleClient = PySimpleClient()
setObjectTimeout(getManager(), 20000)

# Get the mountc reference and configure it
mountComp = simpleClient.getComponent(mountName)
if not mountComp:
    print "Failed to get a mount reference"
    print "Check component name or if container is running"
    exit(0)
setObjectTimeout(mountComp, 20000)

# We have a no null component, so we proceed with the test
mountComp = mountComp._narrow(Control.Mount)
mountComp.configure(mountConfig,"")
print ""
azmode = eval("mountComp." + mountCompCurrentAzMode)
print "Az Actual value read is     :", azmode
elmode = eval("mountComp." + mountCompCurrentElMode)
print "El Actual value read is     :", elmode
print ""
# Go to TRACK_MODE
print "Going to track mode"
for i in range(0,3):
    print str(i)+".- Setting the azimuth axis mode to",Control.AxisMode._item(i)
    print str(i)+".- Setting the elevation axis mode to",Control.AxisMode._item(i)
    mountCompSetAzMode = "_get_setAzAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
    mountCompSetElMode = "_get_setElAxisMode().set_sync(Control.AxisMode._item("+str(i)+"))"
    azmode = eval("mountComp." + mountCompSetAzMode)
    azmode = eval("mountComp." + mountCompCommandAzMode)
    print "Az Reference value set to:", azmode
    elmode = eval("mountComp." + mountCompSetElMode)
    elmode = eval("mountComp." + mountCompCommandElMode)
    print "El Reference value set to:", elmode
    azmode = eval("mountComp." + mountCompCurrentAzMode)
    print "Az Actual value read is  :", azmode
    elmode = eval("mountComp." + mountCompCurrentElMode)
    print "El Actual value read is  :", elmode

# Start read and write threads
periodGetAntPositionData = 0.5
periodSetTrajectory = 1.0
azStartPoint=-0.5
azEndPoint=0.5
elStartPoint=0.1
elEndPoint=0.6

#readThread = PeriodicRead(mountComp,"getAntPositionData()",periodGetAntPositionData,totalIterations(totalTime, periodGetAntPositionData))
#readThread.start()

writeThread = PeriodicWrite("mountComp."+"setTrajectory",periodSetTrajectory, totalIterations(totalTime, periodSetTrajectory),azStartPoint, azEndPoint, elStartPoint, elEndPoint)
writeThread.start()

#readThread.join()
writeThread.join()

# Go to SHUTDOWN_MODE at the end
mountCompSetAzMode = "_get_setAzAxisMode().set_sync(Control.AxisMode._item("+str(0)+"))"
azmode = eval("mountComp." + mountCompSetAzMode)
mountCompSetElMode = "_get_setElAxisMode().set_sync(Control.AxisMode._item("+str(0)+"))"
elmode = eval("mountComp." + mountCompSetElMode)
azmode = eval("mountComp." + mountCompCurrentAzMode)
print "Az Actual value read is  :", azmode
elmode = eval("mountComp." + mountCompCurrentElMode)
print "El Actual value read is  :", elmode
print ""

mountComp.shutdown()
simpleClient.releaseComponent(mountComp._get_name())
simpleClient.disconnect()
print "Test End"
