#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
Mount component setTrajectory test.
'''
import math
import time
import unittest
from Mount.TestCases.Common import *
import Control

class TestCase(CommonTestCase):
    def __init__(self, methodName='runTest',
                 componentName="CONTROL/MOUNT"):
        CommonTestCase.__init__(self,methodName,componentName)

    def tearDown(self):
        self.reference.shutdown()
        self.HwLifeCycleDown()
       
    def check_setTrajectory(self):
        self.reference.encoder()
        # Parameters
        JumpsAhead = 50
        TimeZero  = 12219292800
        TEPeriod  = 480000

        # trajectory points
        azimuth = 90.0
        elevation = 30.0
        timestamp = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod
        point1 = Control.Trajectory(math.radians(azimuth),
                                   math.radians(elevation),
                                   0.0,
                                   0.0,
                                   timestamp)

        azimuth = 90.0
        elevation = 30.0
        timestamp = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod*10
        point2 = Control.Trajectory(math.radians(azimuth),
                                   math.radians(elevation),
                                   0.0,
                                   0.0,
                                   timestamp)
        azimuth = 90.0
        elevation = 30.0
        timestamp = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod*20
        point3 = Control.Trajectory(math.radians(azimuth),
                                   math.radians(elevation),
                                   0.0,
                                   0.0,
                                   timestamp)
        azimuth = 90.0
        elevation = 30.0
        timestamp = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod*30
        point4 = Control.Trajectory(math.radians(azimuth),
                                   math.radians(elevation),
                                   0.0,
                                   0.0,
                                   timestamp)
        azimuth = 90.0
        elevation = 30.0
        timestamp = TimeZero*10000000 +(long(time.time()*10000000)/TEPeriod)*TEPeriod+JumpsAhead*TEPeriod*40
        point5 = Control.Trajectory(math.radians(azimuth),
                                   math.radians(elevation),
                                   0.0,
                                   0.0,
                                   timestamp)
        trajectory = [point1, point2,point3, point4, point5]
        self.reference.setTrajectory(trajectory)
        index = 0
        while index < 100:
            data =  self.reference.getMountStatusData()
            print data.timestamp, data.onSource, data.azCommandedValid, data.azCommanded, data.elCommandedValid, data.elCommanded, data.azPositionsValid, data.azPosition, data.elPositionsValid, data.elPosition, data.azEncoderValid, data.azEncoder, data.elEncoderValid, data.elEncoder
            time.sleep(1.0)
            index = index+1
        pass
    
    def test_setTrajectory(self):
        self.HwLifeCycleUp()
        self.reference.encoder()
        self.check_setTrajectory()

if __name__ == '__main__':
    unittest.main()            
