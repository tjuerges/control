/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2005 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2005-09-30  created
*/

/************************************************************************
*   NAME
*   
* 
*   SYNOPSIS
*   
* 
*   DESCRIPTION
*
*   FILES
*
*   ENVIRONMENT
*
*   COMMANDS
*
*   RETURN VALUES
*
*   CAUTIONS 
*
*   EXAMPLES
*
*   SEE ALSO
*
*   BUGS   
* 
*------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <maciSimpleClient.h>
#include <PositionStreamConsumer.h>
#include <pthread.h>
#include <ace/Time_Value.h>


static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);


bool  runFlag;

void* runLoop(void* simpleClient) {
  runFlag = true;
  ACE_Time_Value t;
  
  while(runFlag) {
    t.set(1L,0L);
    static_cast<SimpleClient*>(simpleClient)->run(t);
  }
  return NULL;
}
  
int main(int argc, char *argv[])
{
  SimpleClient           sc;
  pthread_t              tID;
  pid_t                  pID;

  sc.init(argc, argv);
  if (!sc.login()) {
    fprintf(stderr,"Error in login\n");
  }

  PositionStreamConsumer psConsumer(std::string("ALMA001"));

  pthread_create(&tID, NULL, runLoop, dynamic_cast<void*>(&sc));

  /* Create a process to generate events */
  if ((pID = fork()) == 0) {
    /* Child Process */
    system("./eventProducer.py");
    exit(0);
  }

  while (psConsumer.size() < 100) {
    sleep(1);
  }
  runFlag = false;
  pthread_join(tID, NULL);
  
  sc.logout();

  ACS_SHORT_LOG((LM_INFO,"%d events collected",psConsumer.size()));


  ACS::Time startTime = psConsumer[0].timestamp;
  ACS::Time stopTime  = psConsumer[psConsumer.size()-1].timestamp;
  ACS_SHORT_LOG((LM_INFO,"Start Time: %lld",startTime));
  ACS_SHORT_LOG((LM_INFO,"Stop Time: %lld",stopTime));
  
  double Az;
  double El;
  ACS::Time midTime = (stopTime - startTime) / 2 + startTime;

  psConsumer.getEncPosition(midTime,Az,El);
  ACS_SHORT_LOG((LM_INFO,"At %lld Az Encoder Position was %f (%f)",
		 midTime, Az, Az/midTime));
  ACS_SHORT_LOG((LM_INFO,"At %lld El Encoder Position was %f (%f)",
		 midTime, El, El/midTime));

  psConsumer.getEncPosition(midTime+1,Az,El);
  ACS_SHORT_LOG((LM_INFO,"At %lld Az Encoder Position was %f (%f)",
		 midTime+1, Az, Az/(midTime+1)));
  ACS_SHORT_LOG((LM_INFO,"At %lld El Encoder Position was %f (%f)",
		 midTime+1, El, El/(midTime+1)));

  psConsumer.getCmdPosition(midTime,Az,El);
  ACS_SHORT_LOG((LM_INFO,"At %lld Az Commanded Position was %f (%f)",
		 midTime, Az, Az/midTime));
  ACS_SHORT_LOG((LM_INFO,"At %lld El Commanded Position was %f (%f)",
		 midTime, El, El/midTime));

  psConsumer.getCmdPosition(midTime+1,Az,El);
  ACS_SHORT_LOG((LM_INFO,"At %lld Az Commanded Position was %f (%f)",
		 midTime+1, Az, Az/(midTime+1)));
  ACS_SHORT_LOG((LM_INFO,"At %lld El Commanded Position was %f (%f)",
		 midTime+1, El, El/(midTime+1)));

  psConsumer.getActPosition(midTime-1,Az,El);
  ACS_SHORT_LOG((LM_INFO,"At %lld Az Actual Position was %f (%f)",
		 midTime-1, Az, Az/(midTime-1)));
  ACS_SHORT_LOG((LM_INFO,"At %lld El Actual Position was %f (%f)",
		 midTime-1, El, El/(midTime-1)));

  psConsumer.getActPosition(midTime,Az,El);
  ACS_SHORT_LOG((LM_INFO,"At %lld Az Actual Position was %f (%f)",
		 midTime, Az, Az/midTime));
  ACS_SHORT_LOG((LM_INFO,"At %lld El Actual Position was %f (%f)",
		 midTime, El, El/midTime));

  psConsumer.getActPosition(midTime+1,Az,El);
  ACS_SHORT_LOG((LM_INFO,"At %lld Az Actual Position was %f (%f)",
		 midTime+1, Az, Az/(midTime+1)));
  ACS_SHORT_LOG((LM_INFO,"At %lld El Actual Position was %f (%f)",
		 midTime+1, El, El/(midTime+1)));
  psConsumer.clear();
  ACS_SHORT_LOG((LM_INFO,"%d events after clear",psConsumer.size()));


  waitpid(pID, NULL, 0); /* Wait for child to terminate */
}








