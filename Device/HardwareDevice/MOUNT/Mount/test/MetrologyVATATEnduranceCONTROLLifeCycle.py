#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountController component ACS life cycle test.
'''
import unittest
from PyUCommonTestCases import *
import Control

class TestCase(PyUCommonTestCase):
    def __init__(self, methodName='runTest',
                 componentName="CONTROL/MountController/Generic"):

        self.name=None
        metrology_config = Control.DeviceConfig("Metrology",
                                   "IDL:alma/Control/MetrologyVA:1.0",
                                   "CONTROL/Test/cppContainer",
                                   "MetrologyVA",
                                   [],
                                   0x01,
                                   "Generic")

        self.configuration = metrology_config
        
        PyUCommonTestCase.__init__(self,methodName,self.name,self.configuration)
        
    def test_EnduranceCONTROLLifeCycle(self):
        '''
        This test does the following sequence:
        loop for a number of times
             getComponent
             configure
             shutdown
             releaseComponent
        using a MountController component
        '''
        self.check_CONTROLLifeCycle("CONTROL/Metrology/Node0x01/Generic",10,2.0)
                    
if __name__ == '__main__':
    unittest.main()            
