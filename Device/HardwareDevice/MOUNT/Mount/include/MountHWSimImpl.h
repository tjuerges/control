/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MountHWSimImpl.h
 */
#ifndef MOUNTHWSIMIMPL_H
#define MOUNTHWSIMIMPL_H

#include "MountHWSimBase.h"
#include "Drive.h"
namespace AMB {
    class MountHWSimImpl : public MountHWSimBase {
    public :
	MountHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber);

        class PositionTransformations
	{
	public:
            PositionTransformations();
            static const double defaultBitsPerDegree;
            static const double defaultDegreesPerBit;
            static const double defaultEncoderOffset;
            void initialize(double bitsPerDegree, double degreesPerBit, double encoderOffset);
            void degreesToBits(double degrees, long &bits) const;
            void bitsToDegrees(double &degrees, long bits);
            void degreesToEncoder(double degrees, unsigned long &encoder);
            void encoderToDegrees(double &degrees, unsigned long encoder);
            void degreesToEncoder(double degrees, long &encoder) const;
            void encoderToDegrees(double &degrees, long encoder);
	private:
            double bitsPerDegree_m;
            double degreesPerBit_m;
            double encoderOffset_m;
	};
    protected:
        PositionTransformations azTransf_m;
        PositionTransformations elTransf_m;
        void virtual associate(const rca_t RCA, const std::vector<CAN::byte_t>& data) const;
        virtual void setControlSetAzServoCoeffD(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffE(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetAzServoCoeffF(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffD(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffE(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetElServoCoeffF(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetSubrefAbsPosn(const std::vector<CAN::byte_t>& data);
        virtual void setControlSetSubrefDeltaPosn(const std::vector<CAN::byte_t>& data);
	virtual void setControlAcuTrkMode(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetIpAddress(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetIpGateway(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff00(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff01(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff02(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff03(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff04(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff05(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff06(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff07(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff08(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff09(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff0a(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff0b(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff0c(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff0d(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff0e(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetPtModelCoeff0f(const std::vector<CAN::byte_t>& data);
	virtual void setControlSetSubrefRotation(const std::vector<CAN::byte_t>& data);

    }; // class MountHWSimImpl
} // namespace AMB

#endif // MOUNTHWSIMIMPL_H
