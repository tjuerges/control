// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010, 2011
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef FOCUSMODEL_H
#define FOCUSMODEL_H

#include <MountPointingModel.h> 
#include <loggingLoggable.h> // for Logging::Loggable
#include <map> // for std::map

namespace TMCDB {
    class BandFocusModel;
    class FocusModel;
    class ModelTermSeq;
}

namespace Control {
    class FocusModel: public Logging::Loggable
    {
    public:
        FocusModel(double beamDeviationFactor, double focalRatio, 
                   double xlimit, double yLimit, double zLimit, 
                   double tipLimit, double tiltLimit,
                   Logging::Logger::LoggerSmartPtr logger);

        /// Use the current model to:
        /// 1. Compute the sub-reflector position and rotation 
        /// 2. Correct the supplied azimuth and elevation. 
        /// This function returns true if the difference between the last
        /// commanded subreflector position and the computed position is
        /// greater than a specified value.
        bool getFocus(double& az, double& el,
                      double& focusX, double& focusY, double& focusZ,
                      double& tip, double& tilt);

        /// Change the focus model to the one specified. All other terms are
        /// set to zero and a zero length sequence is equivalent to calling the
        /// zeroCoefficients function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setFocusModel(const TMCDB::FocusModel& model);
        
        /// Reset the focus model to one that does nothing.
        void zeroCoefficients();
        
        /// Set a focus model coefficient to a specified value. The
        /// coefficient name must be one of the allowed values (the comparison
        /// is case-insensitive). The coefficient value must be less than 5
        /// degrees and is specified in *arc-seconds*. 
        /// \exception ControlExceptions::IllegalParameterErrorEx
        ///   If the name is not recognised or the values magnitude is too big.
        void setCoefficient(std::string name, double value);

        /// Returns the current focus model. this is the sum of the base model
        /// and the offsets for the band currently being used.
        TMCDB::FocusModel* getFocusModel();

        /// Returns the specified coefficient in *arcsecs*.
        /// \exception ControlExceptions::IllegalParameterErrorEx 
        ///   If the coefficient is unknown. Matching is case-insensitive
        double getCoefficient(const std::string& name);
        
        /// Change the focus model to the one specified. This model is not
        /// loaded until the useBand function is called.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setFocusModel(const TMCDB::BandFocusModel& model);

        /// Load the focus model for the specified band. The focus model is the
        /// sum of the base model and the offsets for the specified band.  When
        /// it is loaded it replaces any model set using the
        /// setFocusModel(const TMCDB::FocusModel&) function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void useBand(int band);

        /// Returns the current band. If the current focus model does not
        /// correspond to the sum of the base mode and a band offset, because
        /// it has been set using the setFocusModel(const TMCDB::FocusModel&)
        /// function, then it returns a negative number.
        int getCurrentBand();

        /// Set the ambient temperature in degrees Celsius. This is used 
        /// by the focus model to compute the subreflector position.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        ///   If the temperature is outside the range -50 to 100
        void setAmbientTemperature(double temperature);

        /// Set user offsets in position and rotation. 
        /// \exception ControlExceptions::IllegalParameterErrorEx
        ///   If and value is out of range
        void setPositionOffset(double x, double y, double z);
        void setRotationOffset(double tip, double tilt);

        /// Get the user offsets in position and rotation.
        void getPositionOffset(double& x, double& y, double& z);
        void getRotationOffset(double& tip, double& tilt);

        /// Set the maximum error in the subreflector position. This controls
        /// how frequently the subreflector is commanded to a new position as
        /// described in the description of the getFocus function. 
        /// There are three tolerances:
        /// 1. For the z position with a default of 10um 
        /// 2. For the x and y positions with a default of 100um
        /// 3. For the tip and tilt with a default of 1 milli-degree
        /// This value is in meters.
        /// \exception IllegalParameterErrorEx
        ///   If the tolerance is far too big or less then the precision of the
        ///   subreflector positioning mechanism
        void setPositionToleranceXY(double tolerance);
        void setPositionToleranceZ(double tolerance);
        void setRotationTolerance(double tolerance);

        /// Get the maximum error in the subreflector position. 
        double getPositionToleranceXY();
        double getPositionToleranceZ();
        double getRotationTolerance();

        /// Returns true if this class will ask the mount to move the
        /// subreflector. If false then the getFocus function will always
        /// return false;
        bool isEnabled();
        
        /// Enable/disable automatic updates of the subreflector position
        void enable();
        void disable();

        /// Returns true if this class will adjust the pointing position to
        /// compensate for movements in the subreflector
        bool adjustPointing();

        /// Enable/disable automatic updates of the antenna position to
        /// compensate for subreflector movement.
        void enablePointingCorrection();
        void disablePointingCorrection();
        
        /// Command this class to manually set the subreflector position or
        /// rotation to the specified value. This will disable the focus
        /// model. If the specified values are out of range the specified value
        /// will be clipped at the relevant limit.
        void setSubrefAbsPosn(double x, double y, double z);
        void setSubrefRotation(double tip, double tilt);

        /// Force an update to the subreflector position. This needs to be done
        /// after flushing the command queue to ensure that any subreflector
        /// positioning commands that were in the queue make it to the ACU.
        void forceUpdate();

    private:
        // Same as setCoefficient except that the model used is passed in
        static void setCoefficient(std::string name, double value, 
                                   std::map<std::string, double>& model,
                                   Logging::Logger::LoggerSmartPtr logger);

        // Reset all coefficents in the supplied model to zero.
        static void zeroModel(std::map<std::string, double>& model);

        // Copies a TMCDB::ModelTermSeq to the map.
        static void setFocusModel(const TMCDB::ModelTermSeq& inModel, 
                                  std::map<std::string, double>& outModel,
                                  Logging::Logger::LoggerSmartPtr logger);

        // Returns false, and trims the supplied values if they are outside the limits
        bool enforcePositionLimits(double& x, double& y, double& z);
        bool enforceRotationLimits(double& tip, double& tilt);

        // Throws an illegal parameter exception if the band number is bad.
        // \exception ControlExceptions::IllegalParameterErrorEx
        void throwIfBadBand(int band, const char* file, int line, const char* routine);

        // True if the focus model is used when getFocus is called
        bool enabled_m;

        // True if the antenna position is adjusted to compensate for movement
        // of the subreflector
        bool doPointingCorrection_m;

        // The model we are currently using
        std::map<std::string, double> model_m;

        // This is the antenna specific focus model.
        std::map<std::string, double> antenna_m;

        // This is the band offset we are currently using
        int currentBand_m;
        
        // These are the band offsets for each band
        std::vector<std::map<std::string, double> > band_m;

        // The ambient temperature in deg C
        double temperature_m;

        // These are the user offsets. They are only used when the focus model
        // is enabled.
        double offsetX_m;
        double offsetY_m;
        double offsetZ_m;
        double offsetTip_m;
        double offsetTilt_m;
        
        // This is the "focus" pointing model. Only CA and IE are used
        PointingModel pointingModel_m;
        
        // The position and rotation tolerances
        double toleranceXY_m;
        double toleranceZ_m;
        double toleranceRotation_m;

        // This is the last command sent to the Mount;
        double lastCommandedX_m;
        double lastCommandedY_m;
        double lastCommandedZ_m;
        double lastCommandedTip_m;
        double lastCommandedTilt_m;

        // Numbers used to compute how the pointing changes as the subreflector
        // is moved. They depend on the antenna optics.
        const double beamDeviationFactor_m; // In arc-sec/millimeter
        const double focalRatio_m; // no units

        // These are the limits of motion of the subreflector.
        const double xLimit_m;
        const double yLimit_m;
        const double zLimit_m;
        const double tipLimit_m;
        const double tiltLimit_m;

        // True if the subreflector position has been manually specified
        bool manualPosition_m;
        // The manually specified position. Only used if manualPosition_m is true
        double x_m, y_m, z_m;

        // True if the subreflector rotation has been manually specified
        bool manualRotation_m;
        // The manually specified rotation. Only used if manualRotation_m is true
        double tip_m, tilt_m;

        // If true this forces the getFocus command to return true and hence
        // send a new subreflector positioning command to the ACU. Its set by
        // functions that manually reposition the subreflector, or when the
        // command queue is flushed, and reset by the getFocus command after
        // its computed the update.
        bool forceUpdate_m;

    }; // end FocusModel class
}; // end Control namespace
#endif // FOCUSMODEL_H
