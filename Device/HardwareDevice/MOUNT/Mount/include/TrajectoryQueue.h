// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef TRAJECTORYQUEUE_H
#define TRAJECTORYQUEUE_H

// Base class(es)
#include <loggingLoggable.h>

//includes for data members
#include <MountC.h> // for Control::Trajectory
#include <deque> // for std::deque

namespace Control {
    class TrajectoryQueue: public Logging::Loggable 
    {
    public:
        /// Create a class to hold trajectories. Before this class can be used
        /// it must be initialized, with the position obtained from the ACU,
        /// using the setCurrentPosition function.
        TrajectoryQueue(Logging::Logger::LoggerSmartPtr logger);
    
        // Initialize this class with the current position of the antenna.
        void setCurrentPosition(double az, double el);

        /// Insert a sequence of time-tagged positions into the queue.  
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the supplied commands are not in increasing time order or 
        /// if there is any time overlap between the start of the supplied
        /// commands and the last command already in the queue.
        void insert(Control::TrajectorySeq commandSeq);
    
        /// Remove the entries in the queue from the specified time onwards.
        void flush(ACS::Time applicationTime); 
    
        /// Get the trajectory at the specified time. This function removes
        /// entries from the queue that are before the specified time. If there
        /// are no entries in the queue for the specified time this returns the
        /// previous entry but with the velocities set to zero.
        Control::Trajectory getTrajectory(ACS::Time applicationTime); 

        /// Return true if the queue is empty and the last entry returned had a
        /// zero velocity.
        bool isEmpty();
    
    private:
        ACE_Recursive_Thread_Mutex trajQueueMutex_m;
    
        std::deque<Control::Trajectory> trajQueue_m;
    
        Trajectory lastReturnedTrajectory_m;
    }; // end TrajectoryQueue class
}; // end Control namespace
#endif // TRAJECTORYQUEUE_H
