// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef ENCODER_TRANSFORMATIONS_H
#define ENCODER_TRANSFORMATIONS_H

#include <ambDefs.h>

namespace Control {
    class EncoderTransformations {
    public: 
        typedef struct {
            double scale;
            double offset;
        } TransformationParameters;
        
        EncoderTransformations();

        void setConversionFactors(const TransformationParameters& azParameters,
                                  const TransformationParameters& elParameters);
        
        // These methods convert the raw az/el encoder value i.e., the
        // return value from GET_XX_ENC monitors, and radians
        double azEncToRad(const AmbDataMem_t* const encoder) const;
        double elEncToRad(const AmbDataMem_t* const encoder) const;        
        
        // Convert position data from the mount i.e., the return value from
        // {AZ,EL}_POSN_RSP method to radians. Its the same conversion for
        // either the azimuth or elevation axes as the ACU takes care of the
        // encoder zero point and scaling factors
        static double posToRad(const AmbDataMem_t* const encoder);
        
        // Convert the supplied position and rate to an 8-byte sequence that
        // can be sent to the ACU with the {AZ,EL}_TRAJ_CMD command. The
        // trajCmd pointer must point to 8 bytes. All positions and rates must
        // be in range.
        void toPos(const double& posn, const double& rate,
                   AmbDataMem_t* const trajCmd);

        // Convert focus positions from the mount e.g., the return value from
        // GET_SUBREF_ABS_POSN monitor point, to a position in meters.
        static void focusToPos(const AmbDataMem_t* const subrefAbsPosn, 
                               double& x, double& y, double& z);

        // Convert focus rotation from the mount e.g., the return value from
        // GET_SUBREF_ROTATION monitor point, to radians.
        static void focusToRot(const AmbDataMem_t * const subrefRotation,
                               double& tip, double& tilt);

        // Convert the supplied focus position to a 6-byte sequence that can be
        // sent to the ACU with the SET_SUBREF_ABS_POSN command. The
        // subrefAbsPosn pointer must point to at least 6 bytes. All positions
        // must be in range.
        static void toFocusPos(const double& x,const double& y,const double& z,
                               AmbDataMem_t* const subrefAbsPosn);

        // Convert the supplied focus rotation to a 6-byte sequence that can be
        // sent to the ACU with the SET_SUBREF_ROTATION command. The
        // subrefRotation pointer must point to at least 6 bytes. All angles
        // must be in range.
        static void toFocusRotation(const double& tip, const double& tilt,
                                    AmbDataMem_t* const subrefRotation);

        // Round the supplied focus positions (in meters) to the precise value
        // that can be sent to the ACU.
        static void roundFocusPos(double& x, double& y, double& z);

        // Round the supplied focus rotations (in radians) to the precise value
        // that can be sent to the ACU.
        static void roundFocusRotation(double& tip, double& tilt);

        // Convert from meters to the native units used by the focus
        // positioning commands.
        static short metersToFocus(const double& meters);

        // Convert from radians to the native units used by the focus rotation
        // commands.
        static short radToFocus(const double& rad);

    private:
        static double encToRad(const AmbDataMem_t* const encoder, 
                               const TransformationParameters& params);
        
        static unsigned long radToEnc(const double& rad,
                                      const TransformationParameters& params);

        static uint32_t radToPos(const double& rad); 
        static double focusToMeter(const uint16_t * const raw);
        static double focusToRadians(const uint16_t * const raw);
        TransformationParameters azParameters_m;
        TransformationParameters elParameters_m;
    };
}; // end Control namespace
#endif // ENCODER_TRANSFORMATIONS_H
