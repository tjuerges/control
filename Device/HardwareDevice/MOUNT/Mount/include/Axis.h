// @(#) $Id$
//
// Copyright (C) 2001
// Associated Universities, Inc. Washington DC, USA.
//
// Produced for the ALMA project
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Library General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option) any
// later version.
//
// This library is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
// details.
//
// You should have received a copy of the GNU Library General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 675 Massachusetts Ave, Cambridge, MA 02139, USA.  Correspondence concerning
// ALMA should be addressed as follows:
//
// Internet email: alma-sw-admin@nrao.edu

#ifndef AXIS_BASE_H
#define AXIS_BASE_H

#include "AMBSimACUbase.h"

// time between calls to tick()
#define TIME_UNIT   0.020

/*
 * Axis Class - track an axis motion
 */
namespace ACUSim {
class Axis
{
  public:

    // just hold these bits for a get
#define N_COEFFS    13      // number of servo coefficients
    double servo_coefficients[N_COEFFS];

    // limit bits
    unsigned char limit;

    int debug_level;

    Axis();

    // 
    virtual ~Axis(){};

    // clock tick to update axis state
    virtual void tick();
      
    // perform a state change
    virtual void transition();

    // evaluate if mode change is valid
    // This is a transition table.
    AZEL_Mode_t evaluate(AZEL_Mode_t m);

    // set default servo coefficients
    void set_servo_defaults();

    // set/get auxillary motors mode
    void set_aux_mode(AUX_Mode_t m);

    // set/get auxillary motors mode
    AUX_Mode_t get_aux_mode() const;

    // check for zero speed condition
    bool driving() const;

    // set operation mode, returns 0 (OK) ! 1 (ERROR)
    int set_mode(AZEL_Mode_t m);

    AZEL_Mode_t get_mode() const;

    AZEL_Brake_t get_brake() const;

    int set_brake(AZEL_Brake_t b);

    Stow_Pin_t get_stow_pin() const;

    void set_stow_pin(Stow_Pin_t arg);

    void get_cur(double& tp,double& tv,double& ta) const;

    void get_track(double &tp,double& tv,double& ta) const;

    void servo();

    bool inrange(double actual,double delta,double compare);

    virtual void inc_track();

    virtual void inc_cur();

    // look at error and calculate state, acceleration, drive, etc.
    void drive();

    int set(double x,double vel);

    void limits(double lower,double upper);

    void stows(double maint,double survive);
    
    virtual unsigned long long getStatus() ;
    virtual unsigned long long getStatus2() ;

  private:

  protected:
    /*
     * DRIVE_MODE - the axis motion goes through different phases
     * to drive to a point.  To drive in the shortest time, the
     * motion should accelerate at maximum acceleration and maximum
     * velocity if needed.
     */
    enum DRIVE_MODE { ACCELERATE, DECELERATE, SLEW, SERVO, STOP };
    enum ACU_Axis_Status_t
    {
	PRELIM_UP               = 0x00000001,
	PRELIM_DOWN             = 0x00000002,
	LIMIT_UP                = 0x00000004,
	LIMIT_DOWN              = 0x00000008
    };

    // current drive position, etc.
    double cur_pos;
    double cur_vel;
    double cur_acc;

    // offset from track position to decelerate
    double decel_offset, decel_vel;

    DRIVE_MODE drive_state;

    // current tracked position
    double track_pos;
    double track_vel;
    double delta_track;

    double lower_limit;
    double upper_limit;

    // stow positions
    double mstow;
    double sstow;

    // clock tick
    int count;

    AZEL_Mode_t newmode;
    AZEL_Mode_t mode;
    AZEL_Brake_t brake;
    Stow_Pin_t pin;
    AUX_Mode_t auxmode;

    // position change flag for timer loop
    bool newpos;
};
}; /* namespace */
#endif /* AXIS_BASE_H */
