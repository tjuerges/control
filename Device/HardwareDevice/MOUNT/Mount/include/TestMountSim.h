// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef TESTMOUNTSIM_H
#define TESTMOUNTSIM_H

#include <AMBDevice.h>
#include <AmbSimulationInt.h>
#include <MountImpl.h>
#include "MountS.h"

namespace Control {
    class TestMountSim : public Control::MountImpl
    {
    public:
        
        TestMountSim(const ACE_CString& name, maci::ContainerServices* pCS);
        
        ~TestMountSim();
        
        virtual void monitor(AmbRelativeAddr  RCA,
                             AmbDataLength_t& dataLength, 
                             AmbDataMem_t*    data,
                             sem_t*           synchLock,
                             Time*            timestamp,
                             AmbErrorCode_t*  status);
        
        virtual void command(AmbRelativeAddr     RCA,
                             AmbDataLength_t     dataLength,
                             const AmbDataMem_t* data,
                             sem_t*              synchLock,
                             Time*               timestamp,
                             AmbErrorCode_t*     status);
        
        virtual void monitorTE(ACS::Time        TimeEvent,
                               AmbRelativeAddr  RCA,
                               AmbDataLength_t& dataLength, 
                               AmbDataMem_t*    data,
                               sem_t*           synchLock,
                               Time*            timestamp,
                               AmbErrorCode_t*  status);
        
        virtual void commandTE(ACS::Time           TimeEvent,
                               AmbRelativeAddr     RCA,
                               AmbDataLength_t     dataLength, 
                               const AmbDataMem_t* data,
                               sem_t*              synchLock,
                               Time*               timestamp,
                               AmbErrorCode_t*     status);
        
        virtual void monitorNextTE(AmbRelativeAddr     RCA,
                                   AmbDataLength_t&    dataLength, 
                                   AmbDataMem_t*       data,
                                   sem_t*              synchLock,
                                   Time*               timestamp,
                                   AmbErrorCode_t*     status);
        
        virtual void commandNextTE(AMBSystem::AmbRelativeAddr RCA,
                                   AmbDataLength_t            dataLength, 
                                   const AmbDataMem_t*        data,
                                   sem_t*                     synchLock,
                                   ACS::Time*                 timestamp,
                                   AmbErrorCode_t*            status);
        
        virtual void flushNode(ACS::Time                      TimeEvent,
                               ACS::Time*                     timestamp,
                               AmbErrorCode_t*                status);
        
        virtual void flushRCA(ACS::Time                      TimeEvent,
                              AmbRelativeAddr                RCA,
                              ACS::Time*                     timestamp,
                              AmbErrorCode_t*                status);
    protected:
        virtual void getAmbInterfaceInstance() {};
        
    private:
        
        AMB::Device* device_m;
        AmbSimulationInt simulationIf_m;
        
        // copy and assignment are not allowed
        TestMountSim(const TestMountSim&);
        void operator= (const TestMountSim&);
    }; // end TestMountSim class
}; // end Control namespace
#endif // TESTMOUNTSIM_H
