// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MOUNTIMPL_H
#define MOUNTIMPL_H

// Base class(es)
#include <MountBase.h>
#include <acsThread.h>

// CORBA servant header
#include "MountS.h"

//includes for data members
#include <MountStatusBuffer.h>
#include <TrajectoryQueue.h>
#include <MountAxisMode.h>
#include <BandPointingModel.h>
#include <controlAlarmSender.h>
#include <EncoderTransformations.h>

// Forward declarations for classes that this component uses
namespace nc {
    class SimpleSupplier;
}

namespace Control {
    class FocusModel;
}

namespace TMCDB {
    class PointingModel;
    class FocusModel;
    class Access;
}

// Remove the following line when the code generated base classes are moved
// into the Control namespace. Its needed by the Mount*Base classes.
using Control::MountImpl;

namespace Control {
    class MountImpl: 
        public virtual POA_Control::Mount,
        public ::MountBase
    {
    public:
        
        const static ACS::Time    LOOK_AHEAD_TIME = 20000000;
        const static double       DEFAULT_TOLERANCE = 4.84814e-5; // 10 arc-sec
        const static unsigned int DEFAULT_ON_SOURCE_CRITERIA = 3; // Three consecutive points within tolerance
        const static unsigned int DEFAULT_REC_CAB_TEMP = 16; // [C]
        const static double       DEFAULT_ENGINEERING_TIMEOUT = 80.0; // seconds
        const static double       DEFAULT_ENGINEERING_POLLING_TIME = 0.2;  // seconds
        const static double       DEFAULT_CLOUDSAT_ZENITH_DISTANCE_LIMIT =
            0.019198621771937625346160598453751; // radians
        const static double       DEFAULT_CLOUDSAT_AVOID_TOLERANCE =
            0.00017; // ~ 0.01 degrees
        static const char*        DEFAULT_ANTENNA_MODEL_DESCRIPTOR;
        
    protected:
        class WriterThread : public ACS::Thread {
        public:
            WriterThread(const ACE_CString& name, const MountImpl* mountImpl);
            virtual void runLoop();
            virtual void onStop();
            
        protected:
            MountImpl* mountImpl_p;
        };
        
        class ReaderThread : public ACS::Thread {
        public:
            ReaderThread(const ACE_CString& name, const MountImpl* mountImpl);
            virtual void runLoop();
            virtual void onStop();
        protected:
            MountImpl* mountImpl_p;
        };
        
        // A class for managing the alarms that might be sent.
        AlarmSender alarms_m;
        
        // A function that sets up all the alarms that might be sent by this class.
        virtual void configureAlarms();
        
        // Alarms that this class might send
        static const int CLOUDSAT_SHUTTER_NOT_CLOSED;
        static const int CLOUDSAT_SHUTTER_NOT_REOPEN;
        static const int NC_FAILURE;
        static const int ABM_HW_COMM_ERROR;
        static const int AMBERR_RECEIVED_IN_MOUNT_STATUS;
        static const int POINTING_MODEL_UNAVAILABLE;
        static const int POINTING_MODEL_ILLEGAL_TERMS;
        static const int INCORRECT_EXECUTION_TIME_FOUND;
        
    public:
        friend class MountAxisMode;
        friend class MountStatusBuffer;
        friend class MountImpl::WriterThread;
        friend class MountImpl::ReaderThread;
        
        MountImpl(const ACE_CString& name, maci::ContainerServices* pCS);
        
        virtual ~MountImpl();
        
        virtual char* antennaModel();
        
        /* ============ MISSING ICD POINTS ========= */
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        virtual std::vector< unsigned char > getAcuError(ACS::Time& timestamp);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual LongSeq* GET_ACU_ERROR(ACS::Time& timestamp);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual Control::Mount::BrakesStatus GET_AZ_BRAKE(ACS::Time& timestamp);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual Control::Mount::BrakesStatus GET_EL_BRAKE(ACS::Time& timestamp);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual void SET_AZ_BRAKE(Control::Mount::BrakesStatus mode);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual void SET_EL_BRAKE(Control::Mount::BrakesStatus mode);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual void SET_SHUTTER(Control::Mount::ShutterMode mode);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception MountError::CLOUDSATEx
        virtual void openShutter();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual void closeShutter();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual CORBA::Boolean isShutterOpen();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual CORBA::Boolean isShutterClosed();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void SET_STOW_PIN(const LongSeq& world);
        
        /* ============ Corba Calls we actually do ourselves ========= */
        /// \exception ControlExceptions::INACTErrorEx
        MountStatusData* getMountStatusData();
        
        void enableMountStatusDataPublication(CORBA::Boolean enable);
        
        CORBA::Boolean isMountStatusDataPublicationEnabled();
        
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Boolean onSource();
        
        void flushTrajectory(ACS::Time applicationTime, bool suspendWriter);
        
        /* ========= Corba Calls Delegated to Trajectory Queue  ======= */
        
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setTrajectory(const TrajectorySeq& trajectory);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception MountError::IllegalAxisModeEx
        /// \exception MountError::TimeOutEx
        void setMaintenanceAzEl(CORBA::Double azimuth, CORBA::Double elevation);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlExceptions::CAMBErrorEx
        double timeToSource(ACS::Time startOfSource);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        double timeToSlew(double fromAz, double fromEl,
                          double toAz, double toEl); 

        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setTolerance(CORBA::Double tolerance);
        
        CORBA::Double getTolerance();
        
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Boolean isInsideCLOUDSATRegion();
        
        void avoidCLOUDSATRegion(CORBA::Boolean enable);
        
        CORBA::Boolean isCLOUDSATRegionAvoided();
        
        /* =========== Corba Calls Delegated to MountAxisMode ======= */
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAxisMode(Control::Mount::AxisMode azAxisMode,
                         Control::Mount::AxisMode elAxisMode);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAzAxisMode(Control::Mount::AxisMode azAxisMode);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setElAxisMode(Control::Mount::AxisMode elAxisMode);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        void getAxisMode(Control::Mount::AxisMode &azAxisMode,
                         Control::Mount::AxisMode &elAxisMode);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Boolean inLocalMode();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception MountError::LocalModeEx
        /// \exception MountError::TimeOutEx
        void shutdown();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Boolean inShutdownMode();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception MountError::LocalModeEx
        /// \exception MountError::TimeOutEx
        void standby();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Boolean inStandbyMode();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception MountError::LocalModeEx
        /// \exception MountError::TimeOutEx
        void encoder();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception MountError::LocalModeEx
        /// \exception MountError::TimeOutEx
        void autonomous();
        
        void autonomousAsync();

        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        CORBA::Boolean isMoveable();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception MountError::LocalModeEx
        /// \exception MountError::TimeOutEx
        virtual void survivalStow();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception MountError::LocalModeEx
        /// \exception MountError::TimeOutEx
        virtual void maintenanceStow();
        
        // ==== Public functions related to the focus model ====

        /// See the IDL file for a description of this function.
        /// \exception MountError::TMCDBUnavailableEx
        void reloadFocusModel();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setBand(short band);

        /// See the IDL file for a description of this function.
        short getFocusModelBand();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAmbientTemperature(double tempInC);

        /// See the IDL file for a description of this function.
        TMCDB::FocusModel* getFocusModel();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        double getFocusModelCoefficient(const char* name);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setFocusModel(const TMCDB::FocusModel& model);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setFocusModelCoefficient(const char* name, double value);
        
        /// See the IDL file for a description of this function.
        void enableFocusModel(bool enabled);
        
        /// See the IDL file for a description of this function.
        bool isFocusModelEnabled();
        
        /// See the IDL file for a description of this function.
        void enableFocusPointingModel(bool enabled);
        
        /// See the IDL file for a description of this function.
        bool isFocusPointingModelEnabled();
        
        /// See the IDL file for a description of this function.
        /// \exception IllegalParameterErrorEx
        void setSubreflectorPositionToleranceXY(double tolerance);

        /// See the IDL file for a description of this function.
        /// \exception IllegalParameterErrorEx
        void setSubreflectorPositionToleranceZ(double tolerance);

        /// See the IDL file for a description of this function.
        /// \exception IllegalParameterErrorEx
        void setSubreflectorRotationTolerance(double tolerance);

        /// See the IDL file for a description of this function.
        double getSubreflectorPositionToleranceXY();

        /// See the IDL file for a description of this function.
        double getSubreflectorPositionToleranceZ();
            
        /// See the IDL file for a description of this function.
        double getSubreflectorRotationTolerance();
            
        /// See the IDL file for a description of this function.
        /// \exception IllegalParameterErrorEx
        void setSubreflectorPositionOffset(double x, double y, double z);

        /// See the IDL file for a description of this function.
        void getSubreflectorPositionOffset(double& x, double& y, double& z);

        /// See the IDL file for a description of this function.
        /// \exception IllegalParameterErrorEx
        void setSubreflectorRotationOffset(double tip, double tilt);

        /// See the IDL file for a description of this function.
        void getSubreflectorRotationOffset(double& tip, double& tilt);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::HardwareErrorEx
        void getSubreflectorPosition(double& x, double& y, double& z);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlExceptions::HardwareErrorEx
        void setSubreflectorPosition(double x, double y, double z);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlExceptions::HardwareErrorEx
        void setSubreflectorRotation(double tip, double tilt);

        /// See the IDL file for a description of this function.
        void getSubreflectorCommandedPosition(double& x, double& y, double& z);

        /// See the IDL file for a description of this function.
        void getSubreflectorCommandedRotation(double& tip, double& tilt);
        
        /// See the IDL file for a description of this function.
        void getSubreflectorLimits(double& x, double&y, double& z, 
                                   double& tip, double& tilt);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::HardwareErrorEx
        void getSubreflectorRotation(double& tip, double& tilt);

        /// See the IDL file for a description of this function.
        void reportFocusModel();
        
        /// See the IDL file for a description of this function.
        void reportFocusPosition();
        
        // ==== Public functions related to the pointing model ====

        /// See the IDL file for a description of this function.
        /// \exception MountError::TMCDBUnavailableEx
        void reloadPointingModel();
        
        /// See the IDL file for a description of this function.
        short getPointingModelBand();

        /// See the IDL file for a description of this function.
        TMCDB::PointingModel* getPointingModel();
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        CORBA::Double getPointingModelCoefficient(const char* name);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setPointingModel(const TMCDB::PointingModel& model);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setPointingModelCoefficient(const char* name, double value);
        
        /// See the IDL file for a description of this function.
        void zeroPointingModel();
        
        /// See the IDL file for a description of this function.
        void enablePointingModel(bool enabled);
        
        /// See the IDL file for a description of this function.
        CORBA::Boolean isPointingModelEnabled();
        
        /// See the IDL file for a description of this function.
        void reportPointingModel();
        
        // ==== Public functions related to the aux pointing model ====

        /// See the IDL file for a description of this function.
        TMCDB::PointingModel* getAuxPointingModel();
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        CORBA::Double getAuxPointingModelCoefficient(const char* name);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAuxPointingModel(const TMCDB::PointingModel& coefficients);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAuxPointingModelCoefficient(const char* name, double value);
        
        /// See the IDL file for a description of this function.
        void zeroAuxPointingModel();
        
        /// See the IDL file for a description of this function.
        void enableAuxPointingModel(bool enabled);
        
        /// See the IDL file for a description of this function.
        CORBA::Boolean isAuxPointingModelEnabled();
        
        /* =========== ACS Lifecycle Methods ======= */
        
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void initialize();
        
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();
        
        /*===========================*/
        
//        /// \exception ControlExceptions::CAMBErrorExImpl
//        /// \exception ControlExceptions::INACTErrorExImpl
//        virtual std::vector< int > getMetrDeltas(ACS::Time& timestamp);
        
        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx,
        /// \exceprion ControlExceptions::IllegalParameterErrorEx
        virtual void setMinimumElevation(double minEl);
            
        /// See the IDL file for a description of this function.
        virtual void resetLimits();

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual void getMotionLimits(double& minAz, double& maxAz,
                                     double& minEl, double& maxEl);

        /// \exception ControlExceptions::CAMBErrorEx
        virtual void RESET_ACU_CMD(Control::Mount::ResetACUCmdOptions option);
        
        static void printAzEl(std::ostream& msg,
                              const double& az, const double& el,
                              const double& azRate, const double& elRate);
        
        static void printAzEl(std::ostream& msg,
                              const double& az, const double& el);
        
    protected:
        
        ACE_CString antennaModel_m;
        unsigned int receiverCabinTemp_m;
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual bool checkShutterStatus(Control::Mount::ShutterMode status);
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual CORBA::Boolean checkShutterOpen();
        
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        virtual CORBA::Boolean checkShutterClosed();
        
        /// \exception ControlExceptions::TimeoutEx
        virtual void stowPrototype(double azimuth, double elevation);
        
        /// \exception ControlExceptions::TimeoutEx
        virtual void survivalStowPrototype();
        
        /// \exception ControlExceptions::TimeoutEx
        virtual void maintenanceStowPrototype();
        
        /// \exception ControlExceptions::IllegalParameterErrorExImpl
        virtual void checkStowPinValue(unsigned char value);
        
        /// \exception ControlExceptions::IllegalParameterErrorExImpl
        virtual void checkAzStowPinValue(unsigned char value);
        
        /// \exception ControlExceptions::IllegalParameterErrorExImpl
        virtual void checkElStowPinValue(unsigned char value);
        
        /// \exception ControlExceptions::IllegalParameterErrorExImpl
        virtual void checkSetStowPinArguments
        (const std::vector< unsigned char >& world);
        
        /* Methods required by the MountBase class */
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        virtual std::vector<int> getAzPosnRsp(ACS::Time & timestamp);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        virtual std::vector<int> getElPosnRsp(ACS::Time& timestamp);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        virtual unsigned int getAzEnc(ACS::Time& timestamp);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        virtual unsigned int getElEnc(ACS::Time& timestamp);

//        /// \exception ControlExceptions::CAMBErrorExImpl
//        /// \exception ControlExceptions::INACTErrorExImpl
//        virtual void setAzTrajCmd(const std::vector< int >& world);
//        
//        /// \exception ControlExceptions::CAMBErrorExImpl
//        /// \exception ControlExceptions::INACTErrorExImpl
//        virtual void setElTrajCmd(const std::vector< int >& world);
//        
//        /// \exception ControlExceptions::CAMBErrorExImpl
//        /// \exception ControlExceptions::INACTErrorExImpl
//        virtual void setAzTrajCmd(const std::vector< int >& world, const acstime::Epoch& requestTime);
//        
//        /// \exception ControlExceptions::CAMBErrorExImpl
//        /// \exception ControlExceptions::INACTErrorExImpl
//        virtual void setElTrajCmd(const std::vector< int >& world, const acstime::Epoch& requestTime);
        
        /* Hardware Lifecycle Methods */
        virtual void hwConfigureAction();
        
        /// Method which returns the unique Id of the device. This overrides the
        /// method in the ambDeviceImpl base class and uses a CAN broadcast to get
        /// the ID. Its in the base class as both the Melco 12m and Vertex
        /// production antennas do not return the serial number at RCA 0. Its not
        /// known what the AEM and Melco 7m antennas will do.
        /// \exception ControlExceptions::CAMBErrorExImpl
        virtual void getDeviceUniqueId(std::string& deviceID);
        
        virtual void hwInitializeAction();
        virtual void hwOperationalAction();
        virtual void hwStopAction();
        
        // These two functions send AMB messages to the real-time layers
        virtual void queueCommands(const ACS::Time& ts, 
                                   const std::vector<AMBMsgDef>& controlList,
                                   MountStatusStructure* mountStatus);

        virtual void queueMonitors(MountStatusStructure* mountStruct);

        virtual bool enforceSoftwareLimits(Trajectory& trajectory,
                                           MountStatusStructure& mountStruct);
        virtual bool avoidCLOUDSAT(Trajectory& trajectory);
        
        EncoderTransformations* encoderTransforms_m;
        
        // These are the maximum excursions, in meters, of the subreflector on
        // each axis. These values are, from the ICD's, different for Vertex
        // and Melco antennas.
        virtual double subrefXLimit();
        virtual double subrefYLimit();
        virtual double subrefZLimit();
        virtual double subrefTipLimit();
        virtual double subrefTiltLimit();

        // The following two values differ between the 12m and 7m antennas as
        // the optics has a different geometry. The default implementation of
        // these functions is for the 12m antennas.
        //
        // The  focal ratio between the primary and secondary (I think)
        virtual double focalRatio();
        // How much the beam moves, in arcsec/mm, when the subreflector is
        // moved in x or y.
        virtual double beamDeviationFactor();

        /* Thread Access Points */
        virtual void writeMessages();
        virtual void readMessages();
        
        MountStatusBuffer mountStatusBuffer_m;
        TrajectoryQueue   trajectoryQueue_m;
        MountAxisMode     mountAxisMode_m;
        FocusModel*       focusModel_m;
        BandPointingModel pointingModel_m;
        PointingModel auxPointingModel_m;
        
        
        // virtual AmbRelativeAddr getRCAGetAcuError();
        
        /// \exception ControlExceptions::CAMBErrorEx
        virtual void sendOneByteCommand(AmbRelativeAddr address, AmbDataMem_t dataToSend=0x01);
        
        std::map<AmbErrorCode_t,std::string> stringAmbErrorCode;
        
    private:
        // copy and assignment are not allowed
        MountImpl(const MountImpl&);
        MountImpl& operator= (const MountImpl&);
        
        bool applyPointingModel(Trajectory& trajectory,
                                MountStatusStructure& mountStatus);
        
        bool applyAuxPointingModel(Trajectory& trajectory,
                                   MountStatusStructure& mountStatus);
        
        void applyFocusModel(Trajectory& trajectory,
                             MountStatusStructure& mountStatus);
        
        void reloadPointingModel(TMCDB::Access* tmcdb);
        void reloadFocusModel(TMCDB::Access* tmcdb);
        
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        void readAxisParameters(std::string elementName,
                                EncoderTransformations::TransformationParameters&
                                transformationParameters,
                                double &maximumValue,
                                double &minimumValue,
                                double &maximumRate,
                                double &minimumRate);
        
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        void readMountParameters(std::string elementName);
        void initializeTrajectoryQueueAndSubrefPosition();
        
        void createThreads();
        void destroyThreads();
        
        WriterThread* writerThread_p;
        ReaderThread* readerThread_p;
        
        ACS::Time lastWrittenTime_m; //The of the last message set written to HW
        
        // Hardware Limits
        double MountHwAzMax;
        double MountHwAzMin;
        double MountHwElMax;
        double MountHwElMin;
        double MountHwAzRateMax;
        double MountHwAzRateMin;
        double MountHwElRateMax;
        double MountHwElRateMin;

        // Current limits. These are always within the hardware limits
        double azMax_m;
        double azMin_m;
        double elMax_m;
        double elMin_m;

        /* Definition of the mountStatus and methods to simplfy dealing with it */
        MountStatusData mountStatus_m;
        ACE_Recursive_Thread_Mutex mountStatusMutex_m;
        
        // A mutex that stopps the writer thread from executing when we are
        // flushing (or vice-versa).
        ACE_Thread_Mutex flushingMutex_m;
        
        void flagAMBErrors(MountStatusStructure* returnedStatus);
        void flagIncorrectExecutionTimes(MountStatusStructure* returnedStatus);
        bool isExecutionTimeCorrect(std::string  commandName,
                                    ACS::Time timestampRequest,
                                    ACS::Time executionTime,
                                    bool monitor);
        void setCorrectValues(MountStatusStructure* returnedStatus);
        
        void checkCLOUDSAT();
        bool isInsideCLOUDSATRegion(double elevation);
        bool isCLOUDSATEnabled_m;
        bool insideCLOUDSATRegion_m;
        bool insideCLOUDSATRegionPrevious_m;
        bool transitionIn_m;
        bool reopenShutter_m;
        bool avoidCLOUDSATRegion_m;
        
        void setOnSource(bool pointingControlsModified);
        void flagIfOffSource(bool isOnSource, bool wasOnSource, ACS::Time when);
        void publishMountStatusData();
        
        nc::SimpleSupplier* mountStatusDataSupplier_p;
        void disconnectPositionSupplier();
        
        int mountStatusDataPublicationEnabled_m;
        double tolerance_m;
        double engineeringTolerance_m;
        double engineeringTimeout_m;
        double engineeringPollingTime_m;
        int engineeringRetries_m;
        int engineeringPollingTimeInUs_m;
        double cloudsatZenithDistanceLimit_m;
        double cloudsatElevationLowerLimit_m;
        double cloudsatElevationUpperLimit_m;
        
        unsigned int onSourceCounter_m;
        unsigned int onSourceCriteria_m;
        double lastSubrefCmdX_m;
        double lastSubrefCmdY_m;
        double lastSubrefCmdZ_m;
        double lastSubrefCmdTip_m;
        double lastSubrefCmdTilt_m;

    }; // end MountImpl class
}; // end Control namespace
#endif // MOUNTIMPL_H
