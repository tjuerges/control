// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) European Southern Observatory, 2007 - 2008 
// (c) Associated Universities Inc., 2009 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MOUNTPOINTINGMODEL_H
#define MOUNTPOINTINGMODEL_H

#include <loggingLoggable.h>
#include <map>

namespace TMCDB {
    class PointingModel;
    class ModelTermSeq;
}

namespace Control {
    class PointingModel : public Logging::Loggable
    {
    public:
        PointingModel(Logging::Logger::LoggerSmartPtr logger);

        /// Change the pointing model to the one specified. All other terms are
        /// set to zero and a zero length sequence is equivalent to calling the
        /// zeroCoefficients function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setPointingModel(const TMCDB::PointingModel& model);

        /// Reset the pointing model to one that does nothing.
        void zeroCoefficients();
        
        /// Set a pointing model coefficient to a specified value. The
        /// coefficient name must be one of the allowed values (the comparison
        /// is case-insensitive). The coefficient value must be less than 5
        /// degrees and is specified in *arc-seconds*. 
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// If the name is not recognised or the values magnitude is too big.
        void setCoefficient(std::string name, double value);

        /// Apply the current model to correct the supplied azimuth and
        /// elevation. Returns true if there are numerical problems that mean
        /// the pointing model could not be correctly applied. When this
        /// happens a "best effort" correction is done.
        bool applyModel(double observedAz, double observedEl, 
                        double &correctedAz, double &correctedEl);

        // Returns the current pointing model.
        TMCDB::PointingModel* getPointingModel();

        /// Returns the specified coefficient in *arcsecs*.
        /// \exception ControlExceptions::IllegalParameterErrorEx 
        ///   If the coefficient is unknown. Matching is case-insensitive
        double getCoefficient(const std::string& name);
        
        bool isEnabled() {return enabled_m;};
        void enable(){enabled_m = true;};
        void disable(){enabled_m = false;};
        
    protected:
        // Same a setCoefficient except that the model used is passed in
        static void setCoefficient(std::string name, double value, 
                                   std::map<std::string, double>& model, 
                                   Logging::Logger::LoggerSmartPtr logger);

        // Create a model with all the standard coefficents each set to zero.
        static void initializeModel(std::map<std::string, double>& model);

        // Reset all coefficents in the supplied model to zero.
        static void zeroModel(std::map<std::string, double>& model);

        // Copies a TMCDB::ModelTermSeq to the map.
        void setPointingModel(const TMCDB::ModelTermSeq& inModel, 
                              std::map<std::string, double>& outModel,
                              Logging::Logger::LoggerSmartPtr logger);

        // True if the pointing model is used when applyModel is called
        bool enabled_m;

        // The actual Model
        std::map<std::string, double> model_m;
    }; // end  MountPointingModel class
} // end Control namespace
#endif // MOUNTPOINTINGMODEL_H
