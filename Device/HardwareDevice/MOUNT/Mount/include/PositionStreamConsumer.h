// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef POSITIONSTREAMCONSUMER_H
#define POSITIONSTREAMCONSUMER_H

/// This class attaches to the control real time channel and is a consumer
/// for Position Stream Events.  In addition common functions such as sorting
/// and getting a value at a specific time are included.

#include <vector>
#include <acsncSimpleConsumer.h>
#include <MountC.h>
#include <pthread.h>
#include <acscommonC.h> // for ACS::Time

class PositionStreamConsumer :
    public nc::SimpleConsumer<Control::MountStatusData>

{
public:

    /// Construct a client that will receive the data coming from the specified
    /// antenna. This is the antenna name e.g., DV01
    PositionStreamConsumer(const std::string& antennaName);

    /// This disconnects from the notification channel
    virtual void disconnect();

    /// Add the specified data to the internal list. 
    void processData(Control::MountStatusData data);

    /// The size of the internal positions list.
    unsigned long size();

    /// Empties the internal list.
    void clear();

    /// Only accumulate that data with a timestamp that is less than or equal
    /// to the start time and strictly less than the stop time. This function
    /// will clear the existing data buffer before starting data
    /// accumulation. This ensures that the buffer only contains data in the
    /// specified time range. Use a start time of zero to means a long time in
    /// the past and a start time of 0 (or a very big number) to mean a long
    /// time in the future ie., to turn of time range filtering. The caller is
    /// responsible for ensuring that startTime <= stopTime.
    void setTimeRange(ACS::Time startTime, ACS::Time stopTime);

    /// returns true if any data has been received that is equal to or more
    /// recent than the current stop time. Always returns false if the stoptime
    /// is zero.
    bool pastStopTime();

    /// This returns true if any data has been received that is equal to or
    /// more recent than the specified stop time.
    bool pastStopTime(ACS::Time stopTime);

    /// Return the actual position (Az,El) at the requested time. A linear
    /// interpolation is done if the requested time is between data points.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    /// If the requested time is outside the range in the internal list
    void getActPosition(const ACS::Time& timestamp, double& Az, double& El);

    /// Same as the getActPosition function except it returns the commanded
    /// position.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void getCmdPosition(const ACS::Time& timestamp, double& az, double& el);

    /// Same as the getActPosition function except it returns the encoder
    /// position.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void getEncPosition(const ACS::Time& timestamp, double& az, double& el);

    /// Same as the getActPosition function except it returns the pointing
    /// model corrections (the sum of both pointing models implemented in the
    /// Mount component).
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void getPMCorrection(const ACS::Time& timestamp, double& az, double& el);

    /// Return the actual position (Az,El) over the requested time range.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    /// If the specified time range is outside the range in the internal list.
    void getActPosition(const ACS::Time& startTime, const ACS::Time& stopTime, 
                        std::vector<double>& az, std::vector<double>& el);

    /// Same as the getActPosition function except it returns the commanded
    /// position.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void getCmdPosition(const ACS::Time& startTime, const ACS::Time& stopTime, 
                        std::vector<double>& az, std::vector<double>& el);

    /// Same as the getActPosition function except it returns the timestamps
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void getTimes(const ACS::Time& startTime, const ACS::Time& stopTime, 
                  std::vector<ACS::Time>& time);

    /// Return the average az and el over the specified interval.
    /// This method internally calls getCmdPosition
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void getAvgCmdPosition(const ACS::Time& startTime, 
                           const ACS::Time& stopTime, double& az, double& el);

    /// Same as the getAvgCmdPosition function except it returns the avreage
    /// commanded position.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    void getAvgActPosition(const ACS::Time& startTime,
                           const ACS::Time& stopTime, double& az, double& el);

    /// This function returns all the position data within the requested time
    /// range. It will return a zero length vector if there is no data in the
    /// specified time range.
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    /// If startTime > stopTime
    std::vector<Control::MountStatusData> 
    getDataRange(const ACS::Time& startTime, const ACS::Time& stopTime);

    /// This function averages the data in the specified vector.
    /// For the position values {az,el}{Commanded,PrePosition,Position,Encoder}
    /// invalid data is ignored as part of this average.
    /// For the pointing model corrections {az,el}{,aux}PointingModelCorrection
    /// data is ignored if the relevant boolean is false. 
    /// The timestamp is the average of all values.
    /// The average of the onSource flag is false if any value false
    /// The average of the applyAcuPointingModel flag is true if any value is true
    /// The average of the applyAuxPointingModel flag is false if any value is false
    /// The average of the applyMetrology flag is false if any value is false
    /// The average of the pointingModel flag is false if any value is false
    /// The average of the auxPointingModel flag is false if any value is false
    /// The average of the antennaName is the first antennaName
    /// \exception ControlExceptions::IllegalParameterErrorExImpl 
    /// If the supplied vector is zero length
    static Control::MountStatusData average(const std::vector<Control::MountStatusData>& positions);

    /// This function checks the supplied vector is OK. It checks that 
    /// * The vector is non-zero length
    /// * All the timestamps are monotonically increasing by 48ms.
    /// * There is no invalid position data
    /// * The onSource, pointing model, metrology flags and antenna names are
    ///   constant
    /// \exception ControlExceptions::HardwareErrorExImpl If there is any
    /// problem. The precise reason is given in the exception
    static void checkPositions(const std::vector<Control::MountStatusData>& 
                               positions);

    /// Allows you to get any element of the internal list.
    Control::MountStatusData operator[](int);

protected:
    /// Calculate the average of the elements in a vector<double>
    /// \exception ControlExceptions::IllegalParameterErrorExImpl
    /// If vector size == 0
    void avgVector(std::vector<double>& vec, double& avg);

    class Compare_MountStatusData {
    public:
        int operator()(const Control::MountStatusData& a1,
                       const Control::MountStatusData& a2) const {
            return(a1.timestamp < a2.timestamp);
        }
    };

    class Compare_MountStatusData_AcsTime {
    public:
        int operator()(const Control::MountStatusData& a1,
                       const ACS::Time& a2) const {
            return(a1.timestamp < a2);
        }

        int operator()(const ACS::Time& a2,
                       const Control::MountStatusData& a1) const {
            return(a2 < a1.timestamp);
        }
    };
    
    void sortData();

    /// Throw a ControlExceptions::HardwareErrorExImpl exception.  The precise
    /// reason is given in the first parameter
    /// \exception ControlExceptions::HardwareErrorExImpl  
    static void throwHardwareErrorExImpl(const char* msg, int line, 
                                         const char* file, const char* func);

    std::vector<Control::MountStatusData> dataVector_m;
    pthread_mutex_t dataMtx_m;
    bool sorted_m;
    std::string antennaName_m;
    ACS::Time startTime_m;
    ACS::Time stopTime_m;
    bool pastStopTime_m;
};

// Because SimpleConsumer is not object oriented we need an event handler
// method
void PositionStreamConsumer_eventHandler(Control::MountStatusData data,
                                         void *reference);

#endif // POSITIONSTREAMCONSUMER_H
