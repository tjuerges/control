// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MOUNTAXISMODE_H
#define MOUNTAXISMODE_H

#include <loggingLoggable.h>
#include <MountC.h> // for Control::Mount::AxisMode

//Forward reference
namespace Control {
    class MountImpl;
    class MountStatusStructure;
}

const unsigned int DEFAULT_POLLING_TIMEOUT=90; // seconds

namespace Control {
    class MountAxisMode : public Logging::Loggable {
    public:
        MountAxisMode(Control::MountImpl* mountReference,
                      unsigned int pollingTimeOut=DEFAULT_POLLING_TIMEOUT);
        MountAxisMode(Control::MountImpl* mountReference,
                      Logging::Logger::LoggerSmartPtr logger,
                      unsigned int pollingTimeOut=DEFAULT_POLLING_TIMEOUT);
        ~MountAxisMode();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        /// \exception ControlExceptions::IllegalParameterErrorExImpl
        void setAxisMode(Control::Mount::AxisMode azMode,
                         Control::Mount::AxisMode elMode);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        /// \exception ControlExceptions::IllegalParameterErrorExImpl
        void setAzAxisMode(Control::Mount::AxisMode azMode);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        /// \exception ControlExceptions::IllegalParameterErrorExImpl
        void setElAxisMode(Control::Mount::AxisMode elAxisMode);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        void getAxisMode(Control::Mount::AxisMode &azAxisMode, 
                         Control::Mount::AxisMode &elAxisMode);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        bool inLocalMode();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::LocalModeExImpl
        /// \exception MountError::TimeOutExImpl
        void waitForAxisMode(Control::Mount::AxisMode axisMode);
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::LocalModeExImpl
        /// \exception MountError::TimeOutExImpl
        void shutdown();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        bool inShutdownMode();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::LocalModeExImpl
        /// \exception MountError::TimeOutExImpl
        void standby();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        bool inStandbyMode();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::LocalModeExImpl
        /// \exception MountError::TimeOutExImpl
        void encoder();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::LocalModeExImpl
        /// \exception MountError::TimeOutExImpl
        void autonomous();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception ControlExceptions::INACTErrorExImpl
        bool isMoveable();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::LocalModeExImpl
        /// \exception MountError::TimeOutExImpl
        void survivalStow();
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::LocalModeExImpl
        /// \exception MountError::TimeOutExImpl
        void maintenanceStow();
        
        /* This flag is used to show the intent of the user, it does not
           reflect the current status of the mount Axes
        */
        bool enableMountControls;
        
    private:
        
        /// \exception ControlExceptions::CAMBErrorExImpl
        /// \exception MountError::TimeOutExImpl
        void pollingForAxisMode(Control::Mount::AxisMode axisMode);
        
        unsigned int pollingTimeOut_m;
        
        Control::MountImpl* mountReference_m;
    }; // end MountAxisMode class
}; // end Control namespace
#endif // MOUNTAXISMODE_H
