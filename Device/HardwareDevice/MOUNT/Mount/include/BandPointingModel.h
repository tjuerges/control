// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) European Southern Observatory, 2007 - 2008 
// (c) Associated Universities Inc., 2009 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef BANDPOINTINGMODEL_H
#define BANDPOINTINGMODEL_H

#include <MountPointingModel.h>
#include <map>
#include <vector>

namespace TMCDB {
    class BandPointingModel;
    class PointingModel;
}

namespace Control {
    class BandPointingModel : public Control::PointingModel {
    public:
        BandPointingModel(Logging::Logger::LoggerSmartPtr logger);

        /// Change the pointing model to the one specified. All other terms are
        /// set to zero and a zero length sequence is equivalent to calling the
        /// zeroCoefficients function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setPointingModel(const TMCDB::PointingModel& model);

        /// Change the base pointing model and all offsets to the one
        /// specified. This model is not loaded until the useBand function is
        /// called.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setPointingModel(const TMCDB::BandPointingModel& model);

        /// Load the pointing model for the specified band. This pointing model
        /// is the sum of the base model and the offset for the specified band.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void useBand(int band);

        /// Returns the current band. If the current pointing model does not
        /// correspond to the sum of the base model and band offset, because
        /// it has been set using the setPointingModel function, then it
        /// returns a negative number.
        int getCurrentBand();

    private:
        // Throws an illegal parameter exception if the band number is bad.
        // \exception ControlExceptions::IllegalParameterErrorEx
        void throwIfBadBand(int band, const char* file, int line, const char* routine);

        // This is the base pointing model
        std::map<std::string, double> antenna_m;

        // This is the band offset we are currently using
        int currentBand_m;

        // These are the band offsets for each band
        std::vector<std::map<std::string, double> > band_m;
    }; // end BandPointingModel class
} // end Control namespace
#endif // BANDPOINTINGMODEL_H
