// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007, 2008, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#ifndef MOUNT_STATUS_BUFFER_H
#define MOUNT_STATUS_BUFFER_H

#include <loggingLoggable.h>
#include <deque>
#include <vector>
#include <ambDefs.h>

//Forward reference
namespace Control {
    class MountImpl;
}

namespace Control {

    // This struct contains the data that is sent to, or received from, the
    // real time laters with each command or monitor request. 
    // TODO. It may make sense to move this into a more general place like the
    // CAMBServer module as its not Mount specific.
    typedef struct {
        // When the AMB message was sent. This is retruned by teh AMB laters
        ACS::Time executionTimestamp; 
        // The execution status of this message. This must be initialized to
        // AMBERR_PENDING and is set to something else by the real-time layers
        AmbErrorCode_t  ambStatus; // 
        // The data to be sent or the monitor data returned. 
        AmbDataMem_t    value[8];
        // The actual length of the data.
        AmbDataLength_t valueLength;
    } AMBMsgData;

    // This class contains all the data for all the commands and monitors that
    // can be sent each timing event. It contains additional information to
    // allow for the alignment of commands and the corresponding monitors as
    // these AMB messages are executed at different times.
    class MountStatusStructure {
    public:
        // Set all data in the class to initial values;
        void initialize();
        
        // When this set of commands should be applied by the hardware and when
        // the monitor points shoudl be measured.
        ACS::Time       applicationTimestamp;

        // The position control timestamp is 2 TE's before the application time
        ACS::Time       positionControlTimestamp;

        // The focus control timestamp is 1 TE before the application time. Its
        // unclear if this is the correct time. A value of zero means that no
        // focus commands have been sent for this TE.
        ACS::Time       focusControlTimestamp;

        // The monitor timestamp is -.5 TE's after the application time.
        ACS::Time       monitorTimestamp;
        
        // The corrections applied by the primary pointing model 
        double          pointingModelCorrectionAz;
        double          pointingModelCorrectionEl;
        // true if the primary pointing model is enabled.
        bool            pointingModel;        

        // The corrections applied by the aux pointing model 
        double          auxPointingModelCorrectionAz;
        double          auxPointingModelCorrectionEl;
        // true if the primary pointing model is enabled.
        bool            auxPointingModel;
        
        // The pointing corrections applied by the focus model 
        double          focusPointingModelCorrectionAz;
        double          focusPointingModelCorrectionEl;
        // true if the focus model is changing the pointing 
        bool            focusPointingModel;

        // true if the commanded position has been modified because of a limit
        bool            pointingControlsModified;
        
        // The control that can be sent every TE.
        AMBMsgData  AZ_TRAJ_CMD;
        AMBMsgData  EL_TRAJ_CMD;
        AMBMsgData  SET_SUBREF_ABS_POSN;
        AMBMsgData  SET_SUBREF_ROTATION;
        
        // The values that are monitored  every TE.
        AMBMsgData  GET_AZ_ENC;  
        AMBMsgData  GET_EL_ENC;
        AMBMsgData  AZ_POSN_RSP;
        AMBMsgData  EL_POSN_RSP;
        AMBMsgData  GET_SUBREF_ABS_POSN;
        AMBMsgData  GET_SUBREF_ROTATION;
        
        // These locator methods are used to allow us to put handling of
        // monitor and commands in loops
        static AMBMsgData& getAZ_TRAJ_CMD(MountStatusStructure* ms);
        static AMBMsgData& getEL_TRAJ_CMD(MountStatusStructure* ms);
        static AMBMsgData& getGET_AZ_ENC(MountStatusStructure* ms);  
        static AMBMsgData& getGET_EL_ENC(MountStatusStructure* ms);
        static AMBMsgData& getAZ_POSN_RSP(MountStatusStructure* ms);
        static AMBMsgData& getEL_POSN_RSP(MountStatusStructure* ms);
        static AMBMsgData& getSET_SUBREF_ABS_POSN(MountStatusStructure* ms);
        static AMBMsgData& getSET_SUBREF_ROTATION(MountStatusStructure* ms);
        static AMBMsgData& getGET_SUBREF_ABS_POSN(MountStatusStructure* ms);  
        static AMBMsgData& getGET_SUBREF_ROTATION(MountStatusStructure* ms);  
    }; // end  MountStatusStructure class

    // These holds information about each monitor or command we are going to do
    // every TE
    typedef struct {
        AmbRelativeAddr  messageRCA;
        AMBMsgData& (*locatorMethod)(MountStatusStructure*) ;
    } AMBMsgDef;
    
    class MountStatusBuffer : public Logging::Loggable
    {
    public:
        static const unsigned int DEFAULT_INITIAL_BUFFER_SIZE = 100; // Corresponds to ~ 5 sec worth of TEs
        MountStatusBuffer(Control::MountImpl* mountReference, 
                          Logging::Logger::LoggerSmartPtr logger,
                          unsigned int initialSize=DEFAULT_INITIAL_BUFFER_SIZE);
        ~MountStatusBuffer();
        
        std::vector<AMBMsgDef>& getTEMonitorVector();
        std::vector<AMBMsgDef>& getTEPositionControlVector();
        std::vector<AMBMsgDef>& getTEFocusControlVector();
        
        MountStatusStructure* getNextAvailable();
        void addToPendingQueue(MountStatusStructure* mountStatus);
        
        MountStatusStructure* getCompletedStructure();
        void addToAvailableQueue(MountStatusStructure* mountStatus);
        
    private:
        std::vector<AMBMsgDef> teMonitorList_m;
        std::vector<AMBMsgDef> tePositionControlList_m;
        std::vector<AMBMsgDef> teFocusControlList_m;
        
        void defineTEMonitors(Control::MountImpl* mountReference);
        void defineTEPositionControls(Control::MountImpl* mountReference);
        void defineTEFocusControls(Control::MountImpl* mountReference);
        
        bool isComplete(MountStatusStructure* mountStatus);
                
        // This is where we store the blocks that are ready for use
        std::deque<MountStatusStructure*> availableQueue_m;
        std::deque<MountStatusStructure*> pendingQueue_m;
        std::deque<MountStatusStructure*> deadQueue_m ;
        
        ACE_Recursive_Thread_Mutex availableQueueMutex_m;
        ACE_Recursive_Thread_Mutex pendingQueueMutex_m;      
    }; // end  MountStatusBuffer class
}; // end Control namespace
#endif // MOUNT_STATUS_BUFFER_H
