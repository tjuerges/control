#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountController component ACS life cycle test.
'''
import unittest
import Control
import ControlExceptions
from PyUCommonTestCases import *

class TestCase(PyUCommonTestCase):
    def __init__(self, methodName='runTest'):
        self.name = "CONTROL/DV01"
        PyUCommonTestCase.__init__(self,methodName,self.name)

    def setUp(self):
        mount  = Control.DeviceConfig("Mount",[]) 
        ptc    = Control.DeviceConfig("PTC",[]) 
        self.config = Control.DeviceConfig("CONTROL/DV01",
                                           [Control.DeviceConfig("Mount",[]),
                                            Control.DeviceConfig("PTC",[])])
        self.reference.createSubdevices(self.config,"")
        #self.reference.controllerOperational()

    def tearDown(self):
        #self.reference.controllerShutdown()
        pass
    
    def test_proxyMethods(self):
        mount = self.client.getComponent(self.reference.getSubdeviceName("Mount"))
        ptc   = self.client.getComponent(self.reference.getSubdeviceName("PTC"))
        try:
            mount.GET_AC_STATUS()
            mount.GET_METR_DISPL_0()
            mount.GET_METR_DISPL_1()
            mount.GET_METR_EQUIP_STATUS()
            mount.GET_METR_MODE()
            mount.GET_METR_TEMPS_00()
            mount.GET_METR_TEMPS_01()
            mount.GET_METR_TEMPS_02()
            mount.GET_METR_TEMPS_03()
            mount.GET_METR_TEMPS_04()
            mount.GET_METR_TEMPS_05()
            mount.GET_METR_TEMPS_06()
            mount.GET_METR_TEMPS_07()
            mount.GET_METR_TEMPS_08()
            mount.GET_METR_TEMPS_09()
            mount.GET_METR_TEMPS_0A()
            mount.GET_METR_TEMPS_0B()
            mount.GET_METR_TEMPS_0C()
            mount.GET_METR_TEMPS_0D()
            mount.GET_METR_TEMPS_0E()
            mount.GET_METR_TEMPS_0F()
            mount.GET_METR_TEMPS_10()
            mount.GET_METR_TEMPS_11()
            mount.GET_METR_TEMPS_12()
            mount.GET_METR_TEMPS_13()
            mount.GET_METR_TILT_0()
            mount.GET_METR_TILT_1()
            mount.GET_METR_TILT_2()
            mount.GET_METR_TILT_3()
            mount.GET_METR_TILT_4()
            mount.GET_OTHER_STATUS()
            mount.GET_POWER_STATUS()
            mount.GET_PT_MODEL_COEFF_0()
            mount.GET_PT_MODEL_COEFF_1()
            mount.GET_PT_MODEL_COEFF_2()
            mount.GET_PT_MODEL_COEFF_3()
            mount.GET_PT_MODEL_COEFF_4()
            mount.GET_PT_MODEL_COEFF_5()
            mount.GET_PT_MODEL_COEFF_6()
            mount.GET_PT_MODEL_COEFF_7()
            mount.GET_PT_MODEL_COEFF_8()
            mount.GET_PT_MODEL_COEFF_9()
            mount.GET_SUBREF_ABS_POSN()
            mount.GET_SUBREF_DELTA_POSN()
            mount.GET_SUBREF_LIMITS()
            mount.GET_SUBREF_ROTATION()
            mount.GET_SUBREF_STATUS()
            mount.GET_UPS_ALARMS_1()
            mount.GET_UPS_ALARMS_2()
            mount.GET_UPS_BATTERY_OUTPUT_1()
            mount.GET_UPS_BATTERY_OUTPUT_2()
            mount.GET_UPS_BATTERY_STATUS_1()
            mount.GET_UPS_BATTERY_STATUS_2()
            mount.GET_UPS_BYPASS_VOLTS_1()
            mount.GET_UPS_BYPASS_VOLTS_2()
            mount.GET_UPS_FREQS_1()
            mount.GET_UPS_FREQS_2()
            mount.GET_UPS_INVERTER_SW_1()
            mount.GET_UPS_INVERTER_SW_2()
            mount.GET_UPS_INVERTER_VOLTS_1()
            mount.GET_UPS_INVERTER_VOLTS_2()
            mount.GET_UPS_OUTPUT_CURRENT_1()
            mount.GET_UPS_OUTPUT_CURRENT_2()
            mount.GET_UPS_OUTPUT_VOLTS_1()
            mount.GET_UPS_OUTPUT_VOLTS_2()
            mount.GET_UPS_STATUS_1()
            mount.GET_UPS_STATUS_2()
            mount.PTC_CLEAR_FAULT_CMD()
            mount.INIT_SUBREF_CMD()
            mount.RESET_PTC_CMD()
            mount.RESET_UPS_CMD_1()
            mount.RESET_UPS_CMD_2()
            mount.UPS_BATTERY_TEST_CMD_1()
            mount.UPS_BATTERY_TEST_CMD_2()
        except ControlExceptions.INACTErrorEx:
            pass
        else:
            self.fail("INACTErrorEx not thrown")
        self.reference.controllerOperational()
        try:
            self.assertEqual(len(mount.GET_SUBREF_ABS_POSN()[0]),3)
            self.assertEqual(len(mount.GET_SUBREF_DELTA_POSN()[0]),3)
            self.assertEqual(len(mount.GET_SUBREF_LIMITS()[0]),8)
            self.assertEqual(len(mount.GET_SUBREF_ROTATION()[0]),3)
            self.assertEqual(len(mount.GET_SUBREF_STATUS()[0]),5)
            self.assertEqual(mount.GET_SUBREF_ABS_POSN()[0],  ptc.GET_SUBREF_ABS_POSN()[0])
            self.assertEqual(mount.GET_SUBREF_DELTA_POSN()[0],ptc.GET_SUBREF_DELTA_POSN()[0])
            self.assertEqual(mount.GET_SUBREF_LIMITS()[0],    ptc.GET_SUBREF_LIMITS()[0])
            self.assertEqual(mount.GET_SUBREF_ROTATION()[0],  ptc.GET_SUBREF_ROTATION()[0])
            self.assertEqual(mount.GET_SUBREF_STATUS()[0],    ptc.GET_SUBREF_STATUS()[0])
        except ControlExceptions.INACTErrorEx:
            self.fail("INACTErrorEx thrown")
    
        self.reference.controllerShutdown()

        
    
if __name__ == '__main__':
    unittest.main()            
