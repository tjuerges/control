#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# @(#) $Id$
#------------------------------------------------------------------------------

'''
MountController component ACS life cycle test.
'''
import unittest
import asdmIDLTypes
import TMCDB_IDL
import Control
import ControlExceptions
from PyUCommonTestCases import *

class TestCase(PyUCommonTestCase):
    def __init__(self, methodName='runTest'):
        self.name = "CONTROL/DA41"
        PyUCommonTestCase.__init__(self,methodName,self.name)

    def setUp(self):
        mount  = Control.DeviceConfig("Mount",[]) 
        self.config = Control.DeviceConfig("CONTROL/DA41",
                                           [Control.DeviceConfig("Mount",[])])
        self.reference.createSubdevices(self.config,"")

        self.IA    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"IA",   0.1,0.0)
        self.IE    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"IE",   0.2,0.0)
        self.HASA  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HASA", 0.3,0.0)
        self.HACA  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HACA", 0.4,0.0)
        self.HESE  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESE", 0.5,0.0)
        self.HECE  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HECE", 0.6,0.0)
        self.HESA  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESA", 0.7,0.0)
        self.HASA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HASA2",0.8,0.0)
        self.HACA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HACA2",0.9,0.0)
        self.HESA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESA2",1.0,0.0)
        self.HECA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HECA2",1.1,0.0)
        self.HACA3 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HACA3",1.2,0.0)
        self.HECA3 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HECA3",1.3,0.0)
        self.HESA3 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESA3",1.4,0.0)
        self.NPAE  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"NPAE", 1.5,0.0)
        self.CA    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"CA",   1.6,0.0)
        self.AN    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"AN",   1.7,0.0)
        self.AW    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"AW",   1.8,0.0)
        coefficients = [self.IA,
                        self.IE,
                        self.HASA,
                        self.HACA,
                        self.HESE,
                        self.HECE,
                        self.HESA,
                        self.HASA2,
                        self.HACA2,
                        self.HESA2,
                        self.HECA2,
                        self.HACA3,
                        self.HECA3,
                        self.HESA3,
                        self.NPAE,
                        self.CA,
                        self.AN,
                        self.AW]
        startValidTime = asdmIDLTypes.IDLArrayTime(0)
        endValidTime = asdmIDLTypes.IDLArrayTime(0)
        antennaPointingModel = TMCDB_IDL.AntennaPointingModelIDL(0,0,0,startValidTime,endValidTime,False,"ASDMUID")
        pointingModel = TMCDB_IDL.PointingModelIDL("CONTROL/DA41","PAD001",antennaPointingModel,coefficients)
        IA    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"IA",   0.0,0.0)
        IE    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"IE",   0.0,0.0)
        HASA  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HASA", 0.0,0.0)
        HACA  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HACA", 0.0,0.0)
        HESE  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESE", 0.0,0.0)
        HECE  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HECE", 0.0,0.0)
        HESA  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESA", 0.0,0.0)
        HASA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HASA2",0.0,0.0)
        HACA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HACA2",0.0,0.0)
        HESA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESA2",0.0,0.0)
        HECA2 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HECA2",0.0,0.0)
        HACA3 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HACA3",0.0,0.0)
        HECA3 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HECA3",0.0,0.0)
        HESA3 = TMCDB_IDL.AntennaPointingModelTermIDL(0,"HESA3",0.0,0.0)
        NPAE  = TMCDB_IDL.AntennaPointingModelTermIDL(0,"NPAE", 0.0,0.0)
        CA    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"CA",   0.0,0.0)
        AN    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"AN",   0.0,0.0)
        AW    = TMCDB_IDL.AntennaPointingModelTermIDL(0,"AW",   0.0,0.0)
        self.nullCoefficients = [IA,IE,HASA,HACA,HESE,HECE,HESA,HASA2,HACA2,HESA2,HECA2,HACA3,HECA3,HESA3,NPAE,CA,AN,AW]
        self.tmcdb = self.client.getDefaultComponent("IDL:alma/TMCDB/TMCDBComponent:1.0")
        self.tmcdb.setPointingModelData(pointingModel)
        self.mount = self.client.getComponent(self.reference.getSubdeviceName("Mount"))
        self.reference.controllerOperational()

    def tearDown(self):
        self.reference.controllerShutdown()
        self.client.releaseComponent(self.mount._get_name())
        self.client.releaseComponent(self.tmcdb._get_name())
        pass

    def compareTerms(self,terms):
        for term in terms:
            if term.CoeffName == "IA":
                self.assertAlmostEqual(term.CoeffValue, self.IA.CoeffValue, 5)
            if term.CoeffName == "IE":
                self.assertAlmostEqual(term.CoeffValue, self.IE.CoeffValue, 5)
            if term.CoeffName == "HASA":
                self.assertAlmostEqual(term.CoeffValue, self.HASA.CoeffValue, 5)
            if term.CoeffName == "HACA":
                self.assertAlmostEqual(term.CoeffValue, self.HACA.CoeffValue, 5)
            if term.CoeffName == "HESE":
                self.assertAlmostEqual(term.CoeffValue, self.HESE.CoeffValue, 5)
            if term.CoeffName == "HECE":
                self.assertAlmostEqual(term.CoeffValue, self.HECE.CoeffValue, 5)
            if term.CoeffName == "HESA":
                self.assertAlmostEqual(term.CoeffValue, self.HESA.CoeffValue, 5)
            if term.CoeffName == "HASA2":
                self.assertAlmostEqual(term.CoeffValue, self.HASA2.CoeffValue, 5)
            if term.CoeffName == "HACA2":
                self.assertAlmostEqual(term.CoeffValue, self.HACA2.CoeffValue, 5)
            if term.CoeffName == "HESA2":
                self.assertAlmostEqual(term.CoeffValue, self.HESA2.CoeffValue, 5)
            if term.CoeffName == "HECA2":
                self.assertAlmostEqual(term.CoeffValue, self.HECA2.CoeffValue, 5)
            if term.CoeffName == "HACA3":
                self.assertAlmostEqual(term.CoeffValue, self.HACA3.CoeffValue, 5)
            if term.CoeffName == "HECA3":
                self.assertAlmostEqual(term.CoeffValue, self.HECA3.CoeffValue, 5)
            if term.CoeffName == "HESA3":
                self.assertAlmostEqual(term.CoeffValue, self.HESA3.CoeffValue, 5)
            if term.CoeffName == "NPAE":
                self.assertAlmostEqual(term.CoeffValue, self.NPAE.CoeffValue, 5)
            if term.CoeffName == "CA":
                self.assertAlmostEqual(term.CoeffValue, self.CA.CoeffValue, 5)
            if term.CoeffName == "AN":
                self.assertAlmostEqual(term.CoeffValue, self.AN.CoeffValue, 5)
            if term.CoeffName == "AW":
                self.assertAlmostEqual(term.CoeffValue, self.AW.CoeffValue, 5)
        
    def compareToNullTerms(self,terms):
        for term in terms:
            if term.CoeffName == "IA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "IE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "NPAE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "CA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AN":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AW":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)

    def test_PointingModel(self):
        terms = self.mount.getPointingModel()
        self.compareTerms(terms)
        self.mount.setPointingModel(self.nullCoefficients)
        terms = self.mount.getPointingModel()
        self.compareToNullTerms(terms)
        self.mount.reloadPointingModel()
        terms = self.mount.getPointingModel()
        self.compareTerms(terms)
        self.mount.setPointingModel([])
        terms = self.mount.getPointingModel()
        self.compareToNullTerms(terms)
        coefficients = [self.IA]
        self.mount.setPointingModel(coefficients)
        terms = self.mount.getPointingModel()
        for term in terms:
            if term.CoeffName == "IA":
                self.assertAlmostEqual(term.CoeffValue, self.IA.CoeffValue, 5)
            if term.CoeffName == "IE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "NPAE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "CA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AN":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AW":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
        coefficients = [self.IE]
        self.mount.setPointingModel(coefficients)
        terms = self.mount.getPointingModel()
        for term in terms:
            if term.CoeffName == "IA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "IE":
                self.assertAlmostEqual(term.CoeffValue, self.IE.CoeffValue, 5)
            if term.CoeffName == "HASA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "NPAE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "CA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AN":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AW":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
        self.mount.setPointingModelCoefficient(self.IA)
        terms = self.mount.getPointingModel()
        for term in terms:
            if term.CoeffName == "IA":
                self.assertAlmostEqual(term.CoeffValue, self.IA.CoeffValue, 5)
            if term.CoeffName == "IE":
                self.assertAlmostEqual(term.CoeffValue, self.IE.CoeffValue, 5)
            if term.CoeffName == "HASA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "NPAE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "CA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AN":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AW":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
        UT = TMCDB_IDL.AntennaPointingModelTermIDL(0,"UT",   0.0,0.0)
        try:
            self.mount.setPointingModel([UT])
        except ControlExceptions.IllegalParameterErrorEx:
            pass
        else:
            self.fail("ControlExceptions.IllegalParameterErrorEx exception not thrown")
        terms = self.mount.getPointingModel()
        for term in terms:
            if term.CoeffName == "IA":
                self.assertAlmostEqual(term.CoeffValue, self.IA.CoeffValue, 5)
            if term.CoeffName == "IE":
                self.assertAlmostEqual(term.CoeffValue, self.IE.CoeffValue, 5)
            if term.CoeffName == "HASA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "NPAE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "CA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AN":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AW":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
        try:
            self.mount.setPointingModelCoefficient(UT)
        except ControlExceptions.IllegalParameterErrorEx:
            pass
        else:
            self.fail("ControlExceptions.IllegalParameterErrorEx exception not thrown")
        terms = self.mount.getPointingModel()
        for term in terms:
            if term.CoeffName == "IA":
                self.assertAlmostEqual(term.CoeffValue, self.IA.CoeffValue, 5)
            if term.CoeffName == "IE":
                self.assertAlmostEqual(term.CoeffValue, self.IE.CoeffValue, 5)
            if term.CoeffName == "HASA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HASA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA2":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HACA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HECA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "HESA3":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "NPAE":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "CA":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AN":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
            if term.CoeffName == "AW":
                self.assertAlmostEqual(term.CoeffValue, 0.0, 5)
        
    
if __name__ == '__main__':
    unittest.main()            
