//
// ALMA - Atacama Large Millimeter Array
// (c) Nobeyama Radio Observatory - NAOJ, 2008
// (c) Associated Universities Inc., 2009, 2011
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MountA7MImpl_H
#define MountA7MImpl_H

#include <MountA7MBase.h>
#include <MountA7MS.h>

class MountA7MImpl: public MountA7MBase,
                    public virtual POA_Control::MountA7M
{
  public:
    MountA7MImpl(const ACE_CString& name, maci::ContainerServices* pCS);
    ~MountA7MImpl();

    // See the base class for a description of this function. The default is
    // overridden as the 7m antenna has a different value.
    virtual double focalRatio();

    // See the base class for a description of this function. The default is
    // overridden as the 7m antenna has a different value.
    virtual double beamDeviationFactor();

  protected:
    /// Set the cabin temperature to a default value.
    virtual void hwInitializeAction();

  private:
    // copy and assignment are not allowed
    MountA7MImpl(const MountA7MImpl&);
    MountA7MImpl& operator= (const MountA7MImpl&);
};

#endif // MountA7MIMPL_H
