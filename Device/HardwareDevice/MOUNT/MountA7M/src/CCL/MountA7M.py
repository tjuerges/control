#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 - 2009
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
"""
This module defines the MountA7M Control Command Language (CCL)
"""

import CCL.MountA7MBase
import Control

class MountA7M(CCL.MountA7MBase.MountA7MBase):
    '''
    The purpose of this document is to define the interface between
    the antenna, specifically it's control unit and ALMA's monitor and
    control (M&C) system. The ICD provides the interface definitions
    for the minimum control functionality which is identified at
    present for the control of the antenna.
    '''
    def __init__(self, \
                 antennaName = None, \
                 componentName = None, \
                 stickyFlag = False):
        '''
        '''
        # Initialize the base class. The base class contains the
        # reference to the underlying Mount component.
        CCL.MountA7MBase.MountA7MBase.__init__(self, \
                                     antennaName, \
                                     componentName, \
                                     stickyFlag);

    def __del__(self):
        CCL.MountA7MBase.MountA7MBase.__del__(self)
