/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File MountA7MHWSimImpl.cpp
 */
 
#include "MountA7MHWSimImpl.h"

using namespace AMB;

/* Empty example class -- please feel free to modify here inherited methods */
MountA7MHWSimImpl::MountA7MHWSimImpl(node_t node, const std::vector<CAN::byte_t> &serialNumber)
    : MountA7MHWSimBase::MountA7MHWSimBase(node, serialNumber)
{
  const char* __METHOD__ = "MountA7MHWSimImpl::MountA7MHWSimImpl";
  ACS_TRACE(__METHOD__);
}

