/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * WARNING! DO NOT MODIFY THIS FILE!
 *  ---------------------------------------------------------
 * | This is generated code!  Do not modify this file.       |
 * | Any changes will be lost when the file is re-generated. |
 *  ---------------------------------------------------------
 *
 * File TestMountACAHWSimImpl.cpp
*/

#include <cppunit/extensions/HelperMacros.h>
#include <TypeConversion.h>
#include "MountACAHWSimBase.h"

/**
 * A test case for the MountACAHWSimBase class
 *
 */
class MountACAHWSimImplTestCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( MountACAHWSimImplTestCase );
    CPPUNIT_TEST( test_monitorPoint_AZ_STATUS );
    CPPUNIT_TEST( test_monitorPoint_AZ_STATUS2 );
    CPPUNIT_TEST( test_monitorPoint_AZ_AUX_MODE );
    CPPUNIT_TEST_SUITE_END();

  public:
    void setUp();
    void tearDown();

  protected:
    void test_monitorPoint_AZ_STATUS();
    void test_monitorPoint_AZ_STATUS2();
    void test_monitorPoint_AZ_AUX_MODE();
    std::vector<CAN::byte_t> createVector(const unsigned short size);

    AMB::Device* sim_m;
};

CPPUNIT_TEST_SUITE_REGISTRATION( MountACAHWSimImplTestCase );

void MountACAHWSimImplTestCase::setUp()
{
    AMB::node_t node(0x0U);
    std::vector< CAN::byte_t > sn(8U, 0x0U);

    sim_m = new AMB::MountACAHWSimBase(node, sn);
}

void MountACAHWSimImplTestCase::tearDown()
{
    delete sim_m;
}

void MountACAHWSimImplTestCase::test_monitorPoint_AZ_STATUS()
{
    std::vector<CAN::byte_t> received, original;
    unsigned short size(0U);
    long long raw(0LL);

    const unsigned long baseAddress(0UL);

    ///  monitorPoint_AZ_STATUS
    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0001bU), AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS);
    size = 8U;

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS + baseAddress));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    raw = 0LL;

    original.clear();

    original.resize(8, 0U);

    for (unsigned short i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS + baseAddress, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS + baseAddress));
    CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
    for (unsigned short i(0U); i< size; i++)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

void MountACAHWSimImplTestCase::test_monitorPoint_AZ_STATUS2()
{
    std::vector<CAN::byte_t> received, original;
    unsigned short size(0U);
    long long raw(0LL);

    const unsigned long baseAddress(0UL);

    ///  monitorPoint_AZ_STATUS_2
    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00056U), AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS_2);
    size = 8U;

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS_2 + baseAddress));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    raw = 0LL;

    original.clear();

    original.resize(8, 0U);

    for (unsigned short i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS_2 + baseAddress, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_STATUS_2 + baseAddress));
    CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
    for (unsigned short i(0U); i< size; i++)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

void MountACAHWSimImplTestCase::test_monitorPoint_AZ_AUX_MODE()
{
    std::vector<CAN::byte_t> received, original;
    unsigned short size(0U);
    long long raw(0LL);

    const unsigned long baseAddress(0UL);

    ///  monitorPoint_AZ_AUX_MODE
    CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00016U), AMB::MountACAHWSimBase::monitorPoint_AZ_AUX_MODE);
    size = 1U;

    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_AUX_MODE + baseAddress));

    CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

    raw = 0LL;

    original.clear();

    AMB::TypeConversion::valueToData(original, static_cast<unsigned char>(raw), 1);

    for (unsigned short i(0U); i < size; ++i)
    {
        CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
    }

    CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_AZ_AUX_MODE + baseAddress, createVector(size)));
    received.clear();
    CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_AUX_MODE + baseAddress));
    CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
    for (unsigned short i(0U); i< size; i++)
    {
        CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
    }
}

//     ///  monitorPoint_AZ_RATEFDBK_MODE
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0004dU), AMB::MountACAHWSimBase::monitorPoint_AZ_RATEFDBK_MODE);
//     size = 1U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_RATEFDBK_MODE + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<unsigned char>(raw), 1);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_AZ_RATEFDBK_MODE + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AZ_RATEFDBK_MODE + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_EL_STATUS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0000bU), AMB::MountACAHWSimBase::monitorPoint_EL_STATUS);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_STATUS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_EL_STATUS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_STATUS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_EL_STATUS_2
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00057U), AMB::MountACAHWSimBase::monitorPoint_EL_STATUS_2);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_STATUS_2 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_EL_STATUS_2 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_STATUS_2 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_EL_AUX_MODE
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00006U), AMB::MountACAHWSimBase::monitorPoint_EL_AUX_MODE);
//     size = 1U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_AUX_MODE + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<unsigned char>(raw), 1);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_EL_AUX_MODE + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_AUX_MODE + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_EL_RATEFDBK_MODE
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0004fU), AMB::MountACAHWSimBase::monitorPoint_EL_RATEFDBK_MODE);
//     size = 1U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_RATEFDBK_MODE + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<unsigned char>(raw), 1);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_EL_RATEFDBK_MODE + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_EL_RATEFDBK_MODE + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SYSTEM_STATUS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00023U), AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SYSTEM_STATUS_2
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00055U), AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS_2);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS_2 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS_2 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SYSTEM_STATUS_2 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_MODE_RSP
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00042U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_MODE_RSP);
//     size = 1U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_MODE_RSP + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<unsigned char>(raw), 1);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_MODE_RSP + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_MODE_RSP + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_STATUS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00029U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_STATUS);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_STATUS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_STATUS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_STATUS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_LIMITS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00028U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_LIMITS);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_LIMITS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_LIMITS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_LIMITS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_00
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03060U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_00);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_00 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_00 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_00 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_01
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03061U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_01);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_01 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_01 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_01 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_02
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03062U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_02);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_02 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_02 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_02 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_03
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03063U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_03);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_03 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_03 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_03 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_04
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03064U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_04);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_04 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_04 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_04 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_05
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03065U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_05);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_05 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_05 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_05 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_06
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03066U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_06);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_06 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_06 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_06 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_07
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03067U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_07);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_07 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_07 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_07 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_08
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03068U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_08);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_08 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_08 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_08 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_09
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03069U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_09);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_09 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_09 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_09 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_0A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0306aU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_0B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0306bU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_0C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0306cU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_0D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0306dU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_0E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0306eU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_0F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0306fU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_0F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_10
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03070U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_10);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_10 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_10 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_10 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_11
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03071U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_11);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_11 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_11 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_11 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_12
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03072U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_12);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_12 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_12 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_12 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_13
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03073U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_13);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_13 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_13 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_13 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_14
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03074U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_14);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_14 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_14 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_14 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_15
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03075U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_15);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_15 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_15 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_15 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_16
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03076U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_16);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_16 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_16 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_16 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_17
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03077U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_17);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_17 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_17 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_17 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_18
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03078U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_18);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_18 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_18 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_18 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_19
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03079U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_19);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_19 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_19 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_19 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_1A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0307aU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_1B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0307bU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_1C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0307cU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_1D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0307dU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_1E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0307eU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_PT_COEFF_1F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0307fU), AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_PT_COEFF_1F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_ENCODER_POSN_1
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00051U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_1);
//     size = 6U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_1 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(6, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_1 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_1 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_SUBREF_ENCODER_POSN_2
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00052U), AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_2);
//     size = 6U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_2 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(6, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_2 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_SUBREF_ENCODER_POSN_2 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_EQUIP_STATUS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00032U), AMB::MountACAHWSimBase::monitorPoint_METR_EQUIP_STATUS);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_EQUIP_STATUS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_EQUIP_STATUS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_EQUIP_STATUS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_6
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06006U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_6);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_6 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_6 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_6 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_7
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06007U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_7);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_7 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_7 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_7 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_8
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06008U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_8);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_8 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_8 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_8 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_9
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06009U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_9);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_9 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_9 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_9 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0600aU), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_A);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0600bU), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_B);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0600cU), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_C);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0600dU), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_D);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0600eU), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_E);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0600fU), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_F);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_10
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06010U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_10);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_10 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_10 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_10 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_11
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06011U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_11);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_11 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_11 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_11 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_12
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06012U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_12);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_12 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_12 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_12 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_13
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06013U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_13);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_13 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_13 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_13 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_14
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06014U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_14);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_14 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_14 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_14 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_15
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06015U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_15);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_15 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_15 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_15 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_16
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06016U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_16);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_16 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_16 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_16 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_DISPL_17
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x06017U), AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_17);
//     size = 4U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_17 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(4, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_17 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_DISPL_17 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_1A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0401aU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_1B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0401bU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_1C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0401cU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_1D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0401dU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_1E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0401eU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_1F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0401fU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_1F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_20
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04020U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_20);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_20 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_20 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_20 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_21
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04021U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_21);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_21 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_21 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_21 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_22
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04022U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_22);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_22 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_22 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_22 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_23
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04023U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_23);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_23 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_23 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_23 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_24
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04024U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_24);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_24 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_24 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_24 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_25
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04025U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_25);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_25 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_25 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_25 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_26
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04026U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_26);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_26 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_26 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_26 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_27
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04027U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_27);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_27 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_27 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_27 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_28
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04028U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_28);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_28 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_28 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_28 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_29
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04029U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_29);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_29 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_29 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_29 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_2A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0402aU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_2B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0402bU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_2C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0402cU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_2D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0402dU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_2E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0402eU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_2F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0402fU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_2F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_30
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04030U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_30);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_30 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_30 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_30 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_31
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04031U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_31);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_31 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_31 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_31 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_32
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04032U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_32);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_32 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_32 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_32 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_33
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04033U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_33);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_33 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_33 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_33 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_34
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04034U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_34);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_34 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_34 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_34 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_35
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04035U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_35);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_35 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_35 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_35 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_36
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04036U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_36);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_36 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_36 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_36 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_37
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04037U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_37);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_37 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_37 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_37 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_38
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04038U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_38);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_38 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_38 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_38 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_39
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04039U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_39);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_39 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_39 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_39 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_3A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0403aU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_3B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0403bU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_3C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0403cU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_3D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0403dU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_3E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0403eU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_3F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0403fU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_3F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_40
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04040U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_40);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_40 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_40 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_40 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_41
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04041U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_41);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_41 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_41 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_41 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_42
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04042U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_42);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_42 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_42 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_42 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_43
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04043U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_43);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_43 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_43 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_43 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_44
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04044U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_44);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_44 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_44 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_44 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_45
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04045U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_45);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_45 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_45 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_45 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_46
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04046U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_46);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_46 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_46 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_46 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_47
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04047U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_47);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_47 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_47 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_47 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_48
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04048U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_48);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_48 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_48 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_48 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_49
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x04049U), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_49);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_49 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_49 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_49 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_4A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0404aU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_4B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0404bU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_4C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0404cU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_4D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0404dU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_4E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0404eU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_TEMPS_4F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0404fU), AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(8, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_TEMPS_4F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_00
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03080U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_00);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_00 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_00 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_00 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_01
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03081U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_01);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_01 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_01 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_01 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_02
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03082U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_02);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_02 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_02 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_02 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_03
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03083U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_03);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_03 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_03 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_03 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_04
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03084U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_04);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_04 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_04 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_04 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_05
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03085U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_05);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_05 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_05 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_05 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_06
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03086U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_06);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_06 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_06 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_06 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_07
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03087U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_07);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_07 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_07 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_07 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_08
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03088U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_08);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_08 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_08 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_08 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_09
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03089U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_09);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_09 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_09 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_09 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_0A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0308aU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_0B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0308bU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_0C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0308cU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_0D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0308dU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_0E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0308eU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_0F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0308fU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_0F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_10
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03090U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_10);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_10 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_10 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_10 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_11
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03091U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_11);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_11 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_11 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_11 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_12
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03092U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_12);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_12 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_12 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_12 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_13
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03093U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_13);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_13 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_13 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_13 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_14
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03094U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_14);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_14 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_14 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_14 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_15
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03095U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_15);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_15 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_15 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_15 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_16
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03096U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_16);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_16 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_16 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_16 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_17
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03097U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_17);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_17 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_17 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_17 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_18
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03098U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_18);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_18 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_18 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_18 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_19
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x03099U), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_19);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_19 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_19 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_19 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_1A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0309aU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1A);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1A + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_1B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0309bU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1B);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1B + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_1C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0309cU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1C);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1C + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_1D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0309dU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1D);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1D + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_1E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0309eU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1E);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1E + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_METR_COEFF_1F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0309fU), AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1F);
//     size = 8U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1F + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<double>(raw), 8);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_METR_COEFF_1F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_POWER_STATUS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00030U), AMB::MountACAHWSimBase::monitorPoint_POWER_STATUS);
//     size = 3U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_POWER_STATUS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(3, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_POWER_STATUS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_POWER_STATUS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_AC_STATUS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0002cU), AMB::MountACAHWSimBase::monitorPoint_AC_STATUS);
//     size = 1U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AC_STATUS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     AMB::TypeConversion::valueToData(original, static_cast<unsigned char>(raw), 1);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_AC_STATUS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_AC_STATUS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_FAN_STATUS
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00054U), AMB::MountACAHWSimBase::monitorPoint_FAN_STATUS);
//     size = 6U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_FAN_STATUS + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(6, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_FAN_STATUS + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_FAN_STATUS + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_UPS_OUTPUT_VOLTS_2
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0003bU), AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_VOLTS_2);
//     size = 6U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_VOLTS_2 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(6, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_VOLTS_2 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_VOLTS_2 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  monitorPoint_UPS_OUTPUT_CURRENT_2
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x00039U), AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_CURRENT_2);
//     size = 6U;

//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_CURRENT_2 + baseAddress));

//     CPPUNIT_ASSERT_EQUAL(static_cast< std::size_t >(size), received.size());

//     raw = 0LL;

//     original.clear();

//     original.resize(6, 0U);

//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(received[i], original[i]);
//     }

//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_CURRENT_2 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::monitorPoint_UPS_OUTPUT_CURRENT_2 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i< size; i++)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     /// Testing specific control points

//     ///  controlPoint_AZ_AUX_MODE
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01016U), AMB::MountACAHWSimBase::controlPoint_AZ_AUX_MODE);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_AZ_AUX_MODE + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_AZ_AUX_MODE + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_AZ_AUX_MODE + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_AZ_RATEFDBK_MODE
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0104dU), AMB::MountACAHWSimBase::controlPoint_AZ_RATEFDBK_MODE);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_AZ_RATEFDBK_MODE + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_AZ_RATEFDBK_MODE + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_AZ_RATEFDBK_MODE + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_EL_AUX_MODE
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01006U), AMB::MountACAHWSimBase::controlPoint_EL_AUX_MODE);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_EL_AUX_MODE + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_EL_AUX_MODE + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_EL_AUX_MODE + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_EL_RATEFDBK_MODE
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0104fU), AMB::MountACAHWSimBase::controlPoint_EL_RATEFDBK_MODE);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_EL_RATEFDBK_MODE + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_EL_RATEFDBK_MODE + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_EL_RATEFDBK_MODE + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_PT_DEFAULT
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01044U), AMB::MountACAHWSimBase::controlPoint_PT_DEFAULT);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_PT_DEFAULT + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_PT_DEFAULT + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_PT_DEFAULT + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_MODE_CMD
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01042U), AMB::MountACAHWSimBase::controlPoint_SUBREF_MODE_CMD);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_MODE_CMD + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_MODE_CMD + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_MODE_CMD + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_00
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02060U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_00);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_00 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_00 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_00 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_01
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02061U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_01);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_01 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_01 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_01 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_02
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02062U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_02);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_02 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_02 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_02 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_03
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02063U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_03);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_03 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_03 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_03 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_04
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02064U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_04);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_04 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_04 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_04 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_05
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02065U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_05);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_05 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_05 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_05 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_06
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02066U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_06);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_06 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_06 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_06 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_07
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02067U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_07);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_07 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_07 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_07 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_08
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02068U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_08);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_08 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_08 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_08 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_09
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02069U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_09);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_09 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_09 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_09 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_0A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0206aU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0A);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0A + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_0B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0206bU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0B);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0B + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_0C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0206cU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0C);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0C + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_0D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0206dU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0D);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0D + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_0E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0206eU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0E);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0E + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_0F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0206fU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0F);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0F + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_0F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_10
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02070U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_10);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_10 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_10 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_10 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_11
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02071U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_11);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_11 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_11 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_11 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_12
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02072U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_12);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_12 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_12 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_12 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_13
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02073U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_13);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_13 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_13 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_13 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_14
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02074U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_14);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_14 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_14 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_14 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_15
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02075U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_15);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_15 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_15 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_15 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_16
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02076U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_16);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_16 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_16 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_16 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_17
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02077U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_17);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_17 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_17 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_17 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_18
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02078U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_18);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_18 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_18 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_18 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_19
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02079U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_19);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_19 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_19 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_19 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_1A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0207aU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1A);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1A + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_1B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0207bU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1B);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1B + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_1C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0207cU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1C);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1C + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_1D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0207dU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1D);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1D + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_1E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0207eU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1E);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1E + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_COEFF_1F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0207fU), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1F);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1F + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_COEFF_1F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_SUBREF_PT_DEFAULT
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01045U), AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_DEFAULT);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_DEFAULT + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_DEFAULT + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_SUBREF_PT_DEFAULT + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_00
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02080U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_00);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_00 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_00 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_00 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_01
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02081U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_01);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_01 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_01 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_01 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_02
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02082U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_02);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_02 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_02 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_02 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_03
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02083U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_03);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_03 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_03 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_03 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_04
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02084U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_04);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_04 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_04 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_04 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_05
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02085U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_05);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_05 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_05 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_05 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_06
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02086U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_06);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_06 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_06 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_06 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_07
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02087U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_07);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_07 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_07 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_07 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_08
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02088U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_08);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_08 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_08 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_08 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_09
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02089U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_09);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_09 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_09 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_09 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_0A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0208aU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0A);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0A + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_0B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0208bU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0B);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0B + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_0C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0208cU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0C);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0C + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_0D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0208dU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0D);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0D + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_0E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0208eU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0E);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0E + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_0F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0208fU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0F);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0F + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_0F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_10
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02090U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_10);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_10 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_10 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_10 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_11
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02091U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_11);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_11 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_11 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_11 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_12
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02092U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_12);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_12 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_12 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_12 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_13
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02093U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_13);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_13 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_13 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_13 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_14
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02094U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_14);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_14 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_14 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_14 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_15
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02095U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_15);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_15 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_15 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_15 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_16
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02096U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_16);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_16 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_16 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_16 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_17
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02097U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_17);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_17 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_17 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_17 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_18
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02098U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_18);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_18 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_18 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_18 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_19
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x02099U), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_19);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_19 + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_19 + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_19 + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_1A
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0209aU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1A);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1A + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1A + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1A + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_1B
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0209bU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1B);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1B + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1B + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1B + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_1C
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0209cU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1C);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1C + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1C + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1C + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_1D
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0209dU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1D);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1D + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1D + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1D + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_1E
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0209eU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1E);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1E + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1E + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1E + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_COEFF_1F
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0209fU), AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1F);
//     size = 8U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1F + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1F + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_COEFF_1F + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_DEFAULT
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01046U), AMB::MountACAHWSimBase::controlPoint_METR_DEFAULT);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_DEFAULT + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_DEFAULT + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_DEFAULT + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_METR_CALIBRATION
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x01043U), AMB::MountACAHWSimBase::controlPoint_METR_CALIBRATION);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_CALIBRATION + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_METR_CALIBRATION + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_METR_CALIBRATION + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     ///  controlPoint_AC_TEMP
//     CPPUNIT_ASSERT_EQUAL(static_cast< AMB::rca_t >(0x0102cU), AMB::MountACAHWSimBase::controlPoint_AC_TEMP);
//     size = 1U;
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_AC_TEMP + baseAddress));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(AMB::MountACAHWSimBase::controlPoint_AC_TEMP + baseAddress, createVector(size)));
//     received.clear();
//     CPPUNIT_ASSERT_NO_THROW(received = sim_m->monitor(AMB::MountACAHWSimBase::controlPoint_AC_TEMP + baseAddress));
//     CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(size), received.size());
//     for (unsigned short i(0U); i < size; ++i)
//     {
//         CPPUNIT_ASSERT_EQUAL(static_cast< CAN::byte_t >(i + 1U), received[i]);
//     }

//     /// Testing generic points
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::monitorPoint_SERIAL_NUMBER), createVector(8U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::monitorPoint_PROTOCOL_REV_LEVEL), createVector(3U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::monitorPoint_CAN_ERROR), createVector(4U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::monitorPoint_TRANS_NUM), createVector(4U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::monitorPoint_AMBIENT_TEMPERATURE), createVector(4U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::monitorPoint_SW_REV_LEVEL), createVector(3U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::monitorPoint_HW_REV_LEVEL), createVector(3U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::RESET_AMBSI), createVector(1U)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->control(static_cast< AMB::rca_t >(AMB::MountACAHWSimBase::RESET_DEVICE), createVector(1U)));

//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::monitorPoint_SERIAL_NUMBER)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::monitorPoint_PROTOCOL_REV_LEVEL)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::monitorPoint_CAN_ERROR)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::monitorPoint_TRANS_NUM)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::monitorPoint_AMBIENT_TEMPERATURE)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::monitorPoint_SW_REV_LEVEL)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::monitorPoint_HW_REV_LEVEL)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::RESET_AMBSI)));
//     CPPUNIT_ASSERT_NO_THROW(sim_m->monitor(static_cast<AMB::rca_t>(AMB::MountACAHWSimBase::RESET_DEVICE)));

//     /// Testing error cases
//     CPPUNIT_ASSERT_THROW(sim_m->control(static_cast< AMB::rca_t >(0x99999U), createVector(8U)), CAN::Error);
//     CPPUNIT_ASSERT_THROW(sim_m->monitor(static_cast< AMB::rca_t >(0x99999U)), CAN::Error);
// }

std::vector< CAN::byte_t > MountACAHWSimImplTestCase::createVector(const unsigned short size)
{
    std::vector<CAN::byte_t> data;

    for(unsigned short count(1U); count <= size; ++count)
    {
        data.push_back(static_cast<CAN::byte_t>(count));
    }

    return data;
}

/**
 * Main function running the tests
 */
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

int main( int argc, char* argv[] )
{
    // Create the event manager and test controller
    CPPUNIT_NS::TestResult controller;

    // Add a listener that colllects test result
    CPPUNIT_NS::TestResultCollector result;
    controller.addListener( &result );

    // Add a listener that print dots as test run.
    CPPUNIT_NS::BriefTestProgressListener progress;
    controller.addListener( &progress );

    // Add the top suite to the test runner
    CPPUNIT_NS::TestRunner runner;
    runner.addTest( CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest() );
    runner.run( controller );

    // Print test in a compiler compatible format.
    CPPUNIT_NS::CompilerOutputter outputter( &result, std::cerr );
    outputter.write();

    return result.wasSuccessful() ? 0 : 1;
}
