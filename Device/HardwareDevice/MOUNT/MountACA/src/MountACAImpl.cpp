// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Nobeyama Radio Observatory - NAOJ, 2008
// (c) Associated Universities Inc., 2009, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#include "MountACAImpl.h"
#include <maciACSComponentDefines.h>

using log_audience::OPERATOR;
using ControlExceptions::CAMBErrorExImpl;
using ControlExceptions::CAMBErrorEx;
using ControlExceptions::INACTErrorExImpl;
using ControlExceptions::INACTErrorEx;
using std::string;
using std::ostringstream;
using Control::LongSeq;


MountACAImpl::MountACAImpl(const ACE_CString& name,
                           maci::ContainerServices* pCS)
    : MountACABase(name,pCS)
{
    ACS_TRACE(__func__);
    antennaModel_m = "MountACA";
}

MountACAImpl::~MountACAImpl()
{
    ACS_TRACE(__func__);
}

void MountACAImpl::hwInitializeAction(){
    ACS_TRACE(__func__);
    MountACABase::hwInitializeAction();

    // Set metrology mode to a default state
    try {
        SET_METR_CALIBRATION(1);

        LongSeq metrMode;
        metrMode.length(4);
        metrMode[0] = 0x16; // (bit 4 and bit 2 and bit 1)
        metrMode[1] = 0x3;  // (bit 1 and bit 0)
        metrMode[2] = 0;
        metrMode[3] = 0;
        SET_METR_MODE(metrMode);    // see AIV-1268
        setSubrefModeCmd(0x2); // Set the Subreflector to encoder.
    } catch (CAMBErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as there is a communications problem with the ACU.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        CAMBErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    } catch (INACTErrorEx& ex) {
        string msg = "Cannot set the metrology mode to the default value";
        msg += " as the control software is not operational.";
        LOG_TO_AUDIENCE(LM_ERROR, __func__, msg, OPERATOR);
        INACTErrorExImpl newEx(ex,  __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        // TODO Set an alarm
    }
}

/* --------------- [ MACI DLL support functions ] -----------------*/
MACI_DLL_SUPPORT_FUNCTIONS(MountACAImpl)
