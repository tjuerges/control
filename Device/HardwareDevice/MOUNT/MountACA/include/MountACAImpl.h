// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Nobeyama Radio Observatory - NAOJ, 2008
// (c) Associated Universities Inc., 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MountACAImpl_H
#define MountACAImpl_H

#include <MountACABase.h>
#include <MountACAS.h>

class MountACAImpl: public MountACABase,
                    public virtual POA_Control::MountACA
{
  public:
    MountACAImpl(const ACE_CString& name, maci::ContainerServices* pCS);
    ~MountACAImpl();

  protected:
    /// Set the cabin temperature to a default value.
    virtual void hwInitializeAction();

  private:
    // copy and assignment are not allowed
    MountACAImpl(const MountACAImpl&);
    MountACAImpl& operator= (const MountACAImpl&);
};

#endif // MOUNTACAIMPL_H
