#ifndef DTXImpl_h
#define DTXImpl_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <DTXS.h>
#include <DTXBase.h>
#include <DTXExceptions.h>
#include <monitorHelper.h>
#include <RepeatGuard.h>
#include <deque>
#include <walshDelayCalculator.h>


namespace maci
{
    class ContainerServices;
};

namespace Control
{
    class AlarmSender;
};


/* Error State Enumeration */
/* enum DTXErrorCondition { */
/*   CommunicationError         = 0x0000000000001ULL, */
/*   FrStatusMonitorTimeError   = 0x0000000000002ULL, */
/*   NoOverPower                = 0x0000000000004ULL, */
/*   NoREFCLK                   = 0x0000000000008ULL, */
/*   Ch1PLL125Unlocked          = 0x0000000000010ULL, */
/*   Ch1PLL250Unlocked          = 0x0000000000020ULL, */
/*   Ch2PLL125Unlocked          = 0x0000000000040ULL, */
/*   Ch2PLL250Unlocked          = 0x0000000000080ULL, */
/*   Ch3PLL125Unlocked          = 0x0000000000100ULL, */
/*   Ch3PLL250Unlocked          = 0x0000000000200ULL, */
/*   Ch1TTXAlarm                = 0x0000000002000ULL, */
/*   Ch2TTXAlarm                = 0x0000000004000ULL, */
/*   Ch3TTXAlarm                = 0x0000000008000ULL, */
/*   TTXAlarm                   = 0x0000000010000ULL, */
/*   TeError                    = 0x0000000020000ULL, */
/*   Ch1EOLAlm                  = 0x0000000040000ULL, */
/*   Ch1ModTempAlm              = 0x0000000080000ULL, */
/*   Ch1LsWavAlm                = 0x0000000100000ULL, */
/*   Ch1TxAlmInt                = 0x0000000200000ULL, */
/*   Ch1LsBiasAlm               = 0x0000000400000ULL, */
/*   Ch1TempAlm                 = 0x0000000800000ULL, */
/*   Ch1LockErr                 = 0x0000001000000ULL, */
/*   Ch1LsPowAlm                = 0x0000002000000ULL, */
/*   Ch1ModBiasAlm              = 0x0000004000000ULL, */
/*   Ch1TxFifoErr               = 0x0000008000000ULL, */
/*   Ch2EOLAlm                  = 0x0000010000000ULL, */
/*   Ch2ModTempAlm              = 0x0000020000000ULL, */
/*   Ch2LsWavAlm                = 0x0000040000000ULL, */
/*   Ch2TxAlmInt                = 0x0000080000000ULL, */
/*   Ch2LsBiasAlm               = 0x0000100000000ULL, */
/*   Ch2TempAlm                 = 0x0000200000000ULL, */
/*   Ch2LockErr                 = 0x0000400000000ULL, */
/*   Ch2LsPowAlm                = 0x0000800000000ULL, */
/*   Ch2ModBiasAlm              = 0x0001000000000ULL, */
/*   Ch2TxFifoErr               = 0x0002000000000ULL, */
/*   Ch3EOLAlm                  = 0x0004000000000ULL, */
/*   Ch3ModTempAlm              = 0x0008000000000ULL, */
/*   Ch3LsWavAlm                = 0x0010000000000ULL, */
/*   Ch3TxAlmInt                = 0x0020000000000ULL, */
/*   Ch3LsBiasAlm               = 0x0040000000000ULL, */
/*   Ch3TempAlm                 = 0x0080000000000ULL, */
/*   Ch3LockErr                 = 0x0100000000000ULL, */
/*   Ch3LsPowAlm                = 0x0200000000000ULL, */
/*   Ch3ModBiasAlm              = 0x0400000000000ULL, */
/*   Ch3TxFifoErr               = 0x0800000000000ULL */
/* }; */

class DTXImpl: public DTXBase,
    public MonitorHelper,
    public virtual POA_Control::DTX
{
    protected:
    ///
    class UpdateThread: public ACS::Thread
    {
        public:
        UpdateThread(const ACE_CString& name, DTXImpl& DTX);

        virtual void runLoop();


        protected:
        DTXImpl& dtxDevice_p;
    };

    public:
    friend class DTXImpl::UpdateThread;

    /**
     * Constructor
     */
    DTXImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    ~DTXImpl();

    /**
     * ACS Component Lifecycle Methods
     */
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();

    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void cleanUp();

    /**
     * External Interface
     */
    /// See the IDL file for a description of this function.
    /// \exception DTXExceptions::ResyncTeEx
    virtual CORBA::Boolean RESYNC_TE();

    /// See the IDL file for a description of this function.
    /// \exception ControlExceptions::IllegalParameterErrorEx
    /// \exception ControlExceptions::CAMBErrorEx
    /// \exception ControlExceptions::INACTErrorEx
    virtual void setWalshFunction(CORBA::Long WALSequenceNumber);

    /// See the IDL file for a description of this function.
    virtual CORBA::Long getWalshFunction();

    /// See the IDL file for a description of this function.
    virtual void setWalshFunctionDelay(CORBA::Double delayValue);

    /// See the IDL file for a description of this function.
    virtual CORBA::Double getWalshFunctionDelay();

    /// See the IDL file for a description of this function.
    virtual void enableWalshFunctionSwitching(CORBA::Boolean);

    /// See the IDL file for a description of this function.
    virtual CORBA::Boolean walshFunctionSwitchingEnabled();

    /// See the DelayEventClient.idl for a description of this
    virtual void newDelayEvent(const Control::AntennaDelayEvent&);


    protected:
    /**
     * Hardware Lifecycle Methods
     */
    virtual void hwInitializeAction();

    virtual void hwOperationalAction();

    virtual void hwStopAction();

    /**
     * AlarmHelper Class Methods
     */
    virtual void handleActivateAlarm(int code);

    virtual void handleDeactivateAlarm(int code);

    /**
     * Monitor Helper Methods
     */
    virtual void processRequestResponse(const AMBRequestStruct& response);

    /**
     * Error State Methods
     */
    void updateThreadAction();

    /**
     * DTX Methods
     */
    /// \exception DTXExceptions::ResyncTeExImpl
    virtual bool syncInternalTeToExternalTe();

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual bool turnOnDGPowerSupply(double timeout = -1);

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual bool entireFrReset(double timeout = -1);

    /// \exception ControlExceptions::CAMBErrorExImpl
    /// \exception ControlExceptions::INACTErrorExImpl
    virtual bool turnOnLasers(double timeout = -1);

    /// \exception DTXExceptions::ConfigClockEdgeExImpl
    virtual void configClockEdge();

    enum DTXAlarmConditions
    {
        CommunicationError = 1,
        FrStatusMonitorTimeError = 2,
        NoOverPower = 3,
        NoREFCLK = 4,
        Ch1PLL125Unlocked = 5,
        Ch1PLL250Unlocked = 6,
        Ch2PLL125Unlocked = 7,
        Ch2PLL250Unlocked = 8,
        Ch3PLL125Unlocked = 9,
        Ch3PLL250Unlocked = 10,
        Ch1TTXAlarm = 11,
        Ch2TTXAlarm = 12,
        Ch3TTXAlarm = 13,
        TTXAlarm = 14,
        TeError = 15,
        Ch1EOLAlm = 16,
        Ch1ModTempAlm = 17,
        Ch1LsWavAlm = 18,
        Ch1TxAlmInt = 19,
        Ch1LsBiasAlm = 20,
        Ch1TempAlm = 21,
        Ch1LockErr = 22,
        Ch1LsPowAlm = 23,
        Ch1ModBiasAlm = 24,
        Ch1TxFifoErr = 25,
        Ch2EOLAlm = 26,
        Ch2ModTempAlm = 27,
        Ch2LsWavAlm = 28,
        Ch2TxAlmInt = 29,
        Ch2LsBiasAlm = 30,
        Ch2TempAlm = 31,
        Ch2LockErr = 32,
        Ch2LsPowAlm = 33,
        Ch2ModBiasAlm = 34,
        Ch2TxFifoErr = 35,
        Ch3EOLAlm = 36,
        Ch3ModTempAlm = 37,
        Ch3LsWavAlm = 38,
        Ch3TxAlmInt = 39,
        Ch3LsBiasAlm = 40,
        Ch3TempAlm = 41,
        Ch3LockErr = 42,
        Ch3LsPowAlm = 43,
        Ch3ModBiasAlm = 44,
        Ch3TxFifoErr = 45
    };


    private:
    /// No default ctor.
    DTXImpl();

    /// No copy ctor.
    DTXImpl(const DTXImpl&);

    /// No assignment operator.
    DTXImpl& operator=(const DTXImpl&);

    /// Initialise the helper system for alarms.
    void initialiseAlarmSystem();

    /// Clean up the helper system for alarms.
    void cleanUpAlarmSystem();

    /**
     * Method to read all of the configuration data from the DTX config file
     */
    virtual void getConfigurationData();

    /*
     * This is the method that actually loads the walsh functions to
     * the hardware
     */
    virtual void loadWalshFunction(long walSeqNo);

    /**
     * Hardware queuing methods
     */
    virtual void queueFrStatusMonitor();

    virtual void queueFrTeStatusMonitor();

    virtual void queueTtxAlarmStatusMonitor();

    virtual void updateWalshFunctionDelay();

    /**
     * Hardware Processing Methods
     */
    virtual void processFrStatusMonitor(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processFrTeStatusMonitor(
        const MonitorHelper::AMBRequestStruct&);

    virtual void processTtxAlarmStatusMonitor(
        const MonitorHelper::AMBRequestStruct&);

    ACS::Time lastFrStatusMonitorTime_m;

    /// Repeat guard for avoid logging flooding
    RepeatGuard guard;

    /// This semaphore is used to tell when the update thread is really
    /// running and when it is not
    UpdateThread* updateThread_p;

    const ACS::Time updateThreadCycleTime_m;

    /* Timeouts */
    /// default timeout for PS On Command
    const double turnOnDGPowerSupplyTimeout_m;

    /// default timeout fot FR reset
    const double entireFrResetTimeout_m;

    /// default timeout to enable lasers
    const double enableLasersTimeout_m;

    /// This helper class keeps track of the delay needed for the walsh
    /// functions
    WalshDelayCalculator walshOffsetHelper_m;

    /// These are values which we read from the DTX configuration file
    double defaultPhaseOffset_m; // The offset for zero delay in the corr

    long walshFunction_m; // The currently configured Walsh Function

    bool useNegClockEdge_m; // Which side of the clock to use

    bool walshFunctionSwitchingEnabled_m;

    /// Pointer for the alarm system helper instance.
    Control::AlarmSender* alarmSender;
};
#endif

