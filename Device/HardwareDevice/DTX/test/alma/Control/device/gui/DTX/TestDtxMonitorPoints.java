/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/**
 * Test for checking the DTX monitor points. This test extends TestMonitorPoints to provide
 * for testing. 
 *
 * @author  David Hunter    dhunter@nrao.edu
 * @version 
 * @since 7.1.0
 */


package alma.Control.device.gui.DTX;

import alma.Control.DTXCompSimBase;
import alma.Control.HardwareDevice;
import alma.Control.device.gui.common.TestMonitorPoints;
import alma.acs.container.ContainerServices;


public class TestDtxMonitorPoints extends TestMonitorPoints {

    /**
     * The constructor. Just call super...
     * For some reason the ACS component client test case constructor throws exceptions, so we have to
     * pass them along because Java won't let us handle them!
     * @param name
     * @throws Exception
     */
    public TestDtxMonitorPoints(String name) throws Exception {
        super(name);
    }

    /**
     * Set things up for the test!
     *      -Tell the parent class to setup
     *      -Get the DTX component.
     *      
     * Setup must be called before running any tests!
     *
     * Precondition: There MUST be a simulated DTX up and running before calling setup. This function
     * will fail if there is no DTX running. If there is a real function, this function might finish,
     * but the tests will then fail. That is because this test depends on setting the simulator to
     * known values and then reading them back.
     */
    protected void setUp() throws Exception {
        super.setUp();
        DTXCompSimBase dtxRef=null;
        if (null != componentName) {
            ContainerServices c = getContainerServices();
            assertNotNull(c);
            org.omg.CORBA.Object compObj=c.getComponent(componentName);
            assertNotNull(compObj);
            dtxRef = (DTXCompSimBase) narrowToSimObject(compObj);
            super.setUp(dtxRef, componentName, TestDataFileLocation);
        }
        else
            fail("Error: componentName is null!");
    }

    /**
     * Just call super.teardown.
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /** Narrows a corba DTX object into a DTXCompSimBase.
     * Based off of DTX.narrow, which only works on DTXes.
     * @param obj
     * @return
     */
    public HardwareDevice narrowToSimObject(final org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        try
        {
            return (alma.Control.DTXCompSimBase)obj;
        }
        catch (ClassCastException c)
        {
            if (obj._is_a("IDL:alma/Control/DTX:1.0"))
            {
                alma.Control._DTXCompSimBaseStub stub;
                stub = new alma.Control._DTXCompSimBaseStub();
                stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
                return stub;
            }
        }
        throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
    }
    private String TestDataFileLocation = "DTXMonitorTestData.txt";
    private String componentName = "CONTROL/DA41/DTXBBpr0";
}
