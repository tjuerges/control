#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The DTXTest provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest


# Other imports
import time
from CCL.DTX import DTX
from Acspy.Common.TimeHelper import getTimeStamp

from Control import AntennaDelayEvent
from Control import CoarseDelayValue
from Control import HardwareDevice

class DTXTestCase( unittest.TestCase ):

    def setUp(self):
        self.dtx = DTX("DA41", 0, stickyFlag=True)
        self.dtx.hwStop()
        self.dtx.hwStart()
        self.dtx.hwConfigure()
        self.dtx.hwInitialize()
        self.dtx.hwOperational()
        
    def tearDown(self):
        pass

    def testInititation(self):
        '''
        Component instantiation and intialization test.
        '''
        self.assertEqual(self.dtx.getHwState(), HardwareDevice.Operational)
        self.assertAlmostEqual(self.dtx.GET_FR_PHASE_OFFSET()[0], 0.012, 8)

    def testWalshFunctionDelayStatic(self):
        '''
        Test that we can set and retreive the Walsh Function Delay
        '''
        initialDelay = self.dtx.getWalshFunctionDelay()
        self.assertAlmostEqual(self.dtx.GET_FR_PHASE_OFFSET()[0],
                               initialDelay, 8)

        
        self.dtx.setWalshFunctionDelay(initialDelay-0.001)
        self.assertAlmostEqual(self.dtx.getWalshFunctionDelay(),
                               initialDelay-0.001, 8)
        # Allow time for the updateThread to cycle ~0.25 s
        time.sleep(0.5)
        self.assertAlmostEqual(self.dtx.GET_FR_PHASE_OFFSET()[0],
                               initialDelay - 0.001, 8)

    def testWalshFunctionDelayDynamic(self):
        '''
        Test that the delay is modified correctly as the Coarse Delay value
        changes.

        The technique here is to send in a delay event then check that
        the proper value is set at the correct time.
        '''
        startTime = getTimeStamp().value + 20000000 #2 seconds
        endTime   = startTime + 100000000 # 20 seconds
        
        coarseDelays = []
        # First Delay Event is at the start and is 0
        coarseDelays.append(CoarseDelayValue(0, 0))

        # Second Delay Event is at 2 s and is 1000 steps (250 ns)
        coarseDelays.append(CoarseDelayValue(2000, 1000))

        # Third Delay Event is at 4 s and is 1016 steps (expect no change)
        coarseDelays.append(CoarseDelayValue(4000, 1016))

        # Fourth Delay Event is at 6 s and is 1064 steps (16 ns change)
        coarseDelays.append(CoarseDelayValue(6000, 1064))

        # Fourth Delay Event is at 8 s and is 1500 steps (annother 125 ns)
        coarseDelays.append(CoarseDelayValue(8000, 1500))

        antennaDelayEvent = AntennaDelayEvent(startTime, endTime, "DA41",
                                              coarseDelays, [], [])
        self.dtx._HardwareDevice__hw.newDelayEvent(antennaDelayEvent)

        timestamp = 0;
        while timestamp < endTime:
            time.sleep(0.5)
            (delayOffset, timestamp) = self.dtx.GET_FR_PHASE_OFFSET()
            relativeTime = (timestamp - startTime)/1E7

            if relativeTime < 1.75:
                self.assertAlmostEqual(delayOffset, 0.012, 8)
            elif relativeTime > 2.25 and relativeTime < 5.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 250E-9, 8)
            elif relativeTime > 6.25 and relativeTime < 7.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 264E-9, 8)
            elif relativeTime > 8.25:
                self.assertAlmostEqual(delayOffset,  0.012 - 375E-9, 8)

        # Now change the delayOffset and make sure it changes
        self.dtx.setWalshFunctionDelay(0.013)
        time.sleep(0.5)
        self.assertAlmostEqual(self.dtx.GET_FR_PHASE_OFFSET()[0],
                               0.013 - 375E-9, 8)
        

    def testQueuingOfCoarseDelays(self):
        '''
        This test checks that a second delay event correctly overwrites
        a previous one.
        '''
        startTime = getTimeStamp().value + 20000000 #2 seconds
        endTime   = startTime + 100000000 # 10 seconds
        
        coarseDelays = []
        # First Delay Event is at the start and is 0
        coarseDelays.append(CoarseDelayValue(0, 0))

        # Second Delay Event is at 2 s and is 3000 steps (750 ns)
        coarseDelays.append(CoarseDelayValue(2000, 3000))        

        # Third Delay Event is at 4 s and is 1000 steps (250 ns)
        coarseDelays.append(CoarseDelayValue(4000, 1000))

        # Fourth Delay Event is at 9 s and is 2000 steps (500 ns)
        coarseDelays.append(CoarseDelayValue(9000, 2000))

        antennaDelayEvent = AntennaDelayEvent(startTime, endTime, "DA41",
                                              coarseDelays, [], [])
        self.dtx._HardwareDevice__hw.newDelayEvent(antennaDelayEvent)

        # Second Event starts at StartTime + 4 s
        startTime2 = startTime + 40000000
        endTime2   = startTime + 10000000

        coarseDelays = []
        # First Delay Event is at the start and is 500 steps (125 ns)
        coarseDelays.append(CoarseDelayValue(0, 500))

        # Second Delay is at 3 s and is 1500 steps (375 ns)
        coarseDelays.append(CoarseDelayValue(3000, 1500))

        antennaDelayEvent = AntennaDelayEvent(startTime2, endTime2, "DA41",
                                              coarseDelays, [], [])
        self.dtx._HardwareDevice__hw.newDelayEvent(antennaDelayEvent)

        timestamp = 0;
        while timestamp < endTime:
            time.sleep(0.5)
            (delayOffset, timestamp) = self.dtx.GET_FR_PHASE_OFFSET()
            relativeTime = (timestamp - startTime)/1E7

            if relativeTime < 1.75:
                self.assertAlmostEqual(delayOffset, 0.012, 8)
            elif relativeTime > 2.25 and relativeTime < 3.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 750E-9, 8)
            elif relativeTime > 4.25 and relativeTime < 6.75:
                self.assertAlmostEqual(delayOffset, 0.012 - 125E-9, 8)
            elif relativeTime > 7.25:
                self.assertAlmostEqual(delayOffset,  0.012 - 375E-9, 8)



    def testSettingWalshFunction(self):
        '''
        Test that we can set and retrieve the Walsh function without changing
        the value on the hardware (not enabled).
        '''
        
        self.assertEqual(self.dtx.getWalshFunction(), 0)
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_A()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_B()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])

        # Check that it didn't change
        self.dtx.setWalshFunction(127)
        self.assertEqual(self.dtx.getWalshFunction(), 127)
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_A()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_B()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])
        
    def testEnablingWalshFunction(self):
        '''
        Test that the walsh function changes when we enable walsh
        funciton switching, and if enabled changes when we switch
        walsh functions.
        '''
        self.assertEqual(self.dtx.walshFunctionSwitchingEnabled(), False)
        self.dtx.setWalshFunction(127)
        # Check initial State
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_A()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_B()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])

        self.dtx.enableWalshFunctionSwitching(True)
        self.assertEqual(self.dtx.walshFunctionSwitchingEnabled(), True)
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_A()[0],
                         [0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA])
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_B()[0],
                         [0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA])
        
        self.dtx.setWalshFunction(63)
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_A()[0],
                         [0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC])
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_B()[0],
                         [0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC])

        self.dtx.enableWalshFunctionSwitching(False)
        self.assertEqual(self.dtx.walshFunctionSwitchingEnabled(), False)
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_A()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])
        self.assertEqual(self.dtx.GET_FR_PHASE_SEQ_B()[0],
                         [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF])

# MAIN Program
if __name__ == '__main__':
    suite = unittest.makeSuite(DTXTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)

    


