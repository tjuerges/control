/./!d
s/tests in [0-9]*.[0-9][0-9][0-9]s/test in -.---s/g
s/Device Serial Number: 0x[A-F,0-9]*/Device Serial Number: 0x----------------/g
s/acsFALSE/<acsBOOL>/g
s/acsTRUE/<acsBOOL>/g
s/value [0-9][0-9]*.[0-9][0-9]*/value --.---/g
s/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][ T][0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]\{1,3\}/----------T--:--:--.---/g
s/get local manager from [a-z,A-Z,0-9,-]*/get local manager from xxxx/g
s/Device Serial Number: 0x[A-F,0-9]*/Device Serial Number: 0x----------------/g
s/Ran [0-9]* test[s]* in [0-9]*.[0-9]*s/Ran - tests in -.---s/g
