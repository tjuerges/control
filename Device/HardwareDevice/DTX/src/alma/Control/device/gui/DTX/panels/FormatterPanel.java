/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.DTX.widgets.SignedNibbleMonitorPointWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.TextToCodeControlPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel provides the basic status and control information for the DTX formatter board.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 7.0.0
 */

public class FormatterPanel extends DevicePanel {

    public FormatterPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

   private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        statusRegisterPanel = new StatusRegisterPanel(logger, aDevicePM);
        resetButtonPanel = new ResetButtonPanel(logger, aDevicePM);
        temperaturePanel = new TemperaturePanel(logger, aDevicePM);
        voltageStatusPanel = new VoltageStatusPanel(logger, aDevicePM);
        tePanel = new TePanel(logger, aDevicePM);
        laserStatusPanel = new LaserStatusPanel(logger, aDevicePM);
}

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        //        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
        //                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(statusRegisterPanel, c);
        c.gridy++;
        add(resetButtonPanel, c);
        c.gridy++;
        add(temperaturePanel, c);
        c.gridx = 1;
        c.gridy = 0;
        add(voltageStatusPanel, c);
        c.gridy++;
        add(tePanel, c);
        c.gridy++;
        add(laserStatusPanel, c);

        setVisible(true);
    }

    /*
     * This panel displays the power and current of each laser.
     */
    private class LaserStatusPanel extends DevicePanel {
    
        private LaserStatusPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Laser Status"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            add (new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("Power", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LASER_PWR_TTX1), 2), c);
            c.gridy++;
            addLeftLabel("Current", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX1), 2), c);
            c.gridx=2; 
            c.gridy=0;
            add (new JLabel("C"), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LASER_PWR_TTX2), 2), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX2), 2), c);
            c.gridx=3;
            c.gridy=0;
            add (new JLabel("B"), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LASER_PWR_TTX3), 2), c);
            addRightLabel("dBm", c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX3), 2), c);
            addRightLabel("mA", c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This panel displays the reset controls for all 3 FPGAs
     */
    private class ResetButtonPanel extends DevicePanel {
    
        private ResetButtonPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("FPGA Resets"),
                      BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
    
            c.gridx = 1;
            c.gridy = 0;
    
            int[] optionCodes={1, 8, 0xF0};
            String[] optionList={"Reset Status bits", "Over ride switch position", "Reset entire chip"};
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM(
                    IcdControlPoints.FR_RESET_CH1), "Set D", optionList, optionCodes), c);
            c.gridy++; 
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM(
                    IcdControlPoints.FR_RESET_CH2), "Set C", optionList, optionCodes), c);
            c.gridy++;
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM(
                    IcdControlPoints.FR_RESET_CH3), "Set B", optionList, optionCodes), c);
            c.gridy++; 
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM(
                    IcdControlPoints.FR_RESET_ALL), "Set All", optionList, optionCodes), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * Displays the formatter status alarms
     */
    private class StatusRegisterPanel extends DevicePanel {
    
        private StatusRegisterPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Status Register"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            add (new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("250MHz PLL", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_250_CH1),
                    Status.FAILURE, "Error: Ch D 250MHz PLL lock lost!",
                    Status.GOOD, "Ch D 250MHz PLL locked"), c);
            c.gridy++;
            addLeftLabel("125MHz PLL", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_125_CH1),
                    Status.FAILURE, "Error: Ch D 125MHz PLL lock lost!",
                    Status.GOOD, "Ch D 125MHz PLL locked"), c);
            c.gridy++;
            addLeftLabel("Laser Power", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_LASER_CH1),
                    Status.OFF, "Notice: Ch D laser is off.",
                    Status.ON, "Ch D laser is on"), c);
            c.gridy++;
            addLeftLabel("TTX Alarm", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_TTX_ALM_CH1),
                    Status.FAILURE, "Error: TTX alarm on Ch D!",
                    Status.GOOD, "Ch D TTX OK"), c);
            c.gridx = 2;
            c.gridy = 0;
            add (new JLabel("C"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_250_CH2),
                    Status.FAILURE, "Error: Ch C 250MHz PLL lock lost!",
                    Status.GOOD, "Ch C 250MHz PLL locked"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_125_CH2),
                    Status.FAILURE, "Error: Ch C 125MHz PLL lock lost!",
                    Status.GOOD, "Ch C 125MHz PLL locked"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_LASER_CH2),
                    Status.OFF, "Notice: Ch C laser is off.",
                    Status.ON, "Ch C laser is on"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_TTX_ALM_CH2),
                    Status.FAILURE, "Error: TTX alarm on Ch C!",
                    Status.GOOD, "Ch C TTX OK"), c);
            c.gridx = 3;
            c.gridy = 0;
            add (new JLabel("B"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_250_CH3),
                    Status.FAILURE, "Error: Ch B 250MHz PLL lock lost!",
                    Status.GOOD, "Ch B 250MHz PLL locked"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_125_CH3),
                    Status.FAILURE, "Error: Ch B 125MHz PLL lock lost!",
                    Status.GOOD, "Ch B 250MHz PLL locked"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_LASER_CH3),
                    Status.OFF, "Notice: Ch B laser is off.",
                    Status.ON, "Ch B laser is on"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_TTX_ALM_CH3),
                    Status.FAILURE, "Error: TTX alarm on Ch B!",
                    Status.GOOD, "Ch B TTX OK"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * Displays the temperature of the formatter and 3 transponders.
     */
    private class TemperaturePanel extends DevicePanel {
    
        private TemperaturePanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Temperature"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
    
            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("FR", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TMP_FR), true, 2), c);
            c.gridy++; 
            addLeftLabel("TTX D", c);
            //TODO: When these monitor points are fixed, set temp label in DetailPanel.java
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX1), true), c);
            c.gridy++;
            addLeftLabel("TTX C", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX2), true), c);
            c.gridy++; 
            addLeftLabel("TTX B", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX3), true), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * Displays the TE status and errors.
     */
    private class TePanel extends DevicePanel {
    
        private TePanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Timing Event"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
    
            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("Te Error", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TE_STATUS_ERROR),
                    Status.GOOD, "No Te Errors",
                    Status.FAILURE, "Te Error!"), c);
            c.gridy++;
            addLeftLabel("Clock Edge", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TE_STATUS_CLK_EDGE),
                    Status.GOOD, "Using normal clock edge",
                    Status.ATYPICAL, "Using inverted clock edge"), c);
            c.gridx=2;
            c.gridy=0;
            add(new JLabel("    "), c);   //Put a bit of space between columns
            c.gridx=4;
            addLeftLabel("Current Error", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TE_STATUS_CURR_ERR)), c);
//            add(new SignedNibbleMonitorPointWidget(logger, devicePM
//                    .getMonitorPointPM(IcdMonitorPoints.FR_TE_STATUS_CURR_ERR)), c);
            c.gridy++;
            addLeftLabel("Max Error", c);
            add(new SignedNibbleMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TE_STATUS_MAX_ERR)), c);
            c.gridy++;
            addLeftLabel("Min Error", c);
            add(new SignedNibbleMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_TE_STATUS_MIN_ERR)), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * Displays the formatter and transponder voltage readouts.
     */
    private class VoltageStatusPanel extends DevicePanel {

        private VoltageStatusPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Voltages"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            add (new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("FR 1.5v", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_1_5_V_FPGA_1), 2), c);
            c.gridy++;
            addLeftLabel("TTX 1.8v", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_1_8_V_CH_1), 2), c);
            c.gridx=2;
            c.gridy=0;
            add (new JLabel("C"), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_1_5_V_FPGA_2), 2), c);
            c.gridy++; 
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_1_8_V_CH_2), 2), c);
            c.gridx=3;
            c.gridy=0;
            add (new JLabel("B"), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_1_5_V_FPGA_3), 2), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_1_8_V_CH_3), 2), c);
            c.gridx=1;
            c.gridy=10; //Put it below the other stuff. The unused cells take 0 space.
            addLeftLabel(" ", c);  //Put in a spacer
            c.gridy++;
            c.gridwidth=4;
            addLeftLabel ("Board Voltages", c);
            c.gridwidth=1;
            c.gridy++;
            addLeftLabel("3.3v", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_BOARD_VOLTAGE_3_3_V), 2), c);
            c.gridy++;
            addLeftLabel("15v", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_BOARD_VOLTAGE_15_V), 2), c);
            c.gridy=12;
            c.gridx=3;
            addLeftLabel("5v", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_BOARD_VOLTAGE_5_V), 2), c);
            c.gridy++;
            addLeftLabel("-5.2v", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_BOARD_VOLTAGE_NEG_5_2_V),
                    Status.GOOD, "-5.2 volts OK",
                    Status.FAILURE, "-5.2 volts out of range"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    private StatusRegisterPanel statusRegisterPanel;
    private ResetButtonPanel resetButtonPanel;
    private TemperaturePanel temperaturePanel;
    private VoltageStatusPanel voltageStatusPanel;
    private TePanel tePanel;
    private LaserStatusPanel laserStatusPanel;
}

//
// O_o

