/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.CombinedBooleanMonitorPointWidget;

import java.util.logging.Logger;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;


/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Summary data.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public SummaryPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(devicePM.getDeviceDescription()),
                BorderFactory.createEmptyBorder(1,1,1,1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor=GridBagConstraints.CENTER;
        c.weightx=100;
        c.weighty=100;
        c.gridx=1;
        c.gridy=1;
        addLeftLabel("TE", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_REFCLK),
                Status.FAILURE, "TE Error: no TE or TE out of sync",
                Status.GOOD, "TE present and in sync"), c);
        c.gridx+=2;
        c.gridy=0;
        addLeftLabel("Keep Alive", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_OVER_POWER),
                Status.FAILURE, "Error: Keep Alive signal not detected",
                Status.GOOD, "Keep Alive signal is present"), c);
        c.gridy++;
        addLeftLabel("125MHz PLL", c);
        add(new CombinedBooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_125_CH1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_125_CH2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_125_CH3),
                Status.FAILURE,"125MHz PLL error on one or more channels",
                Status.GOOD, "125MHz PLL locked on all channels"), c);        
        c.gridx+=2;
        c.gridy=0;
        addLeftLabel("250MHz PLL", c);
        add(new CombinedBooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_250_CH1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_250_CH2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_250_CH3),
                Status.FAILURE,"250MHz PLL error on one or more channels",
                Status.GOOD, "250MHz PLL locked on all channels"), c);
        c.gridy++;
        addLeftLabel("DG Power Supply", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.DG_PS_ON),
                Status.OFF, "Error: Digitizer Power Supply is Off!",
                Status.ON, "Digitizer Power Supply On"), c);
        c.gridy=0;
        c.gridx+=2;
        addLeftLabel("TTX Alarm", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.FR_STATUS_TTX_ALARM),
                Status.FAILURE, "TTX Error; check status of all transmitters",
                Status.GOOD, "No TTX Alarms"), c);
        c.gridy++;
        addLeftLabel("Lasers", c);
        add(new CombinedBooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_LASER_CH1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_LASER_CH2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_STATUS_LASER_CH3),
                Status.OFF,"Laser off on one or more channels",
                Status.GOOD, "Lasers OK on all channels"), c);


        this.setVisible(true);
    }
    
    protected void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        c.anchor=GridBagConstraints.EAST;
        add(new JLabel(s), c);
        c.anchor=GridBagConstraints.WEST;
        c.gridx++;
    }
}

//
// O_o
