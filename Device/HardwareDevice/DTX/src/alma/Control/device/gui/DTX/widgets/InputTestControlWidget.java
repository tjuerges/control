/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 * <code>InputTestControlWidget.java</code> This widget provides a method to set the input test data
 * in a DTX using the SET_FR_INPUT_TEST control point.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class InputTestControlWidget extends JPanel implements ActionListener {
    
    /**Construct a new InputTestControlWidget
     * 
     * @param logger the logger that all logs should be directed to.
     * @param controlPointModelD The control point for SET_DFR_XBAR ch D.
     * @param controlPointModelC The control point for SET_DFR_XBAR ch C.
     * @param controlPointModelB The control point for SET_DFR_XBAR ch B.
     * @param controlPointModelAll The control point for SET_DFR_XBAR all.
     */
    public InputTestControlWidget( Logger logger,
            ControlPointPresentationModel<Integer> controlPointModelD,
            ControlPointPresentationModel<Integer> controlPointModelC,
            ControlPointPresentationModel<Integer> controlPointModelB,
            ControlPointPresentationModel<Integer> controlPointModelAll) {
        this.logger = logger;
        this.controlPointModelD = controlPointModelD;                //Save the control point models
        this.controlPointModelC = controlPointModelC;
        this.controlPointModelB = controlPointModelB;
        this.controlPointModelAll = controlPointModelAll;
        setD=new JButton(DLabel);                                   //Setup the Ch D button
        setD.addActionListener(this);
        controlPointModelD.getContainingDevicePM().addControl(setD);
        setC=new JButton(CLabel);                                   //Setup the Ch C button
        setC.addActionListener(this);
        controlPointModelC.getContainingDevicePM().addControl(setC);
        setB=new JButton(BLabel);                                   //Setup the Ch B button
        setB.addActionListener(this);
        controlPointModelB.getContainingDevicePM().addControl(setB);
        setAll=new JButton(AllLabel);                               //Setup the All channels button
        setAll.addActionListener(this);
        controlPointModelAll.getContainingDevicePM().addControl(setAll);
        
        inputEdgeOptionMenu = new JComboBox();
        inputEdgeOptionMenu.setEditable(false);
        for (String s: inputEdgeOptionList) inputEdgeOptionMenu.addItem(s);
        controlPointModelAll.getContainingDevicePM().addControl(inputEdgeOptionMenu); 
        testModeOptionMenu = new JComboBox();                              //Setup the optionMenu
        testModeOptionMenu.setEditable(false);
        for (String s: testModeOptionList) testModeOptionMenu.addItem(s);
        controlPointModelAll.getContainingDevicePM().addControl(testModeOptionMenu);
        buildWidget();
    }
    
    /** When an actionEvent occurs (e.g the control button is pressed) check the value in the
     *  menus. Find the corresponding code in each one, or them, and send the result to the
     *  proper control point.
     */
    public void actionPerformed(ActionEvent event) {
        int count=0;
        int inputEdgeCode=0;
        int testModeCode=0;
        int outCode;
        
        String newValue=(String)inputEdgeOptionMenu.getSelectedItem();
        try{
            while (newValue != inputEdgeOptionList[count])
                count++;
            inputEdgeCode=(inputEdgeCodes[count]);
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            logger.severe("ERROR: Lower bit text given by user not found in inputEdgeOptionList!");
        }    
        newValue=(String)testModeOptionMenu.getSelectedItem();
        try{
            while (newValue != testModeOptionList[count])
                count++;
            testModeCode=(testModeCodes[count]);
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            logger.severe("ERROR: Lower bit text given by user not found in testModeOptionList!");
        }    
        
        outCode=inputEdgeCode | testModeCode;
        
        if (event.getActionCommand()==DLabel)
            controlPointModelD.setValue(outCode);
        if (event.getActionCommand()==CLabel)
            controlPointModelC.setValue(outCode);
        if (event.getActionCommand()==BLabel)
            controlPointModelB.setValue(outCode);
        if (event.getActionCommand()==AllLabel)
            controlPointModelAll.setValue(outCode);
    }
    
    /** Set up the widget gui. Add both menus and all 4 buttons.
     */
    private void buildWidget() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Set Test Data"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // Add all the points in a single column, starting at the top, with the labels on the left.
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth=4;
        add(inputEdgeOptionMenu, c);
        c.gridy++;
        add(testModeOptionMenu, c);
        c.gridwidth=1;
        c.gridy++;
        add(setD, c);
        c.gridx++;
        add(setC, c);
        c.gridx++;
        add(setB, c);
        c.gridx++;
        add(setAll, c);
    }

    private String AllLabel="Set All";
    private String BLabel="Set B";
    private String CLabel="Set C";
    private ControlPointPresentationModel<Integer> controlPointModelAll;
    private ControlPointPresentationModel<Integer> controlPointModelB;
    private ControlPointPresentationModel<Integer> controlPointModelC;
    private ControlPointPresentationModel<Integer> controlPointModelD;
    private String DLabel="Set D";
    private int[] inputEdgeCodes={0, 1, 2, 3};
    private String[] inputEdgeOptionList={"Normal Mode", "Clock BB0 at Falling edge, BB1 at Rising Edge",
                                          "Clock BB0 at Rising edge, BB1 at Falling Edge",
                                          "Clock both BBs at falling Edge"};
    private JComboBox inputEdgeOptionMenu;
    private Logger logger;
    private static final long serialVersionUID = 1L;
    private JButton setAll;
    private JButton setB;
    private JButton setC;
    private JButton setD;
    private int[] testModeCodes={0x0, 0x10, 0x20, 0x30, 0x40};
    private String[] testModeOptionList={"Normal Mode", "All input data equals 0 for 500 \u03BCs after TE",
                                         "Sample 0 of BB0 and BB1 equals 0 for 1 frame after TE",
                                         "All input data equals 1", "All input data equals 0"};
    private JComboBox testModeOptionMenu;
}
