/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.presentationModels;

import alma.Control.DTXOperations;

import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.IdlMonitorPointPresentationModel;
import alma.ControlExceptions.CAMBErrorEx;
import alma.ControlExceptions.INACTErrorEx;
import alma.common.log.ExcLog;
import alma.acs.time.TimeHelper;
import org.omg.CORBA.LongHolder;


import java.util.Date;
import java.util.logging.Logger;

/**
 * Track a read only property of a hardware device to be displayed in a UI.  Isolate the user interface 
 * from the implementation details of a hardware device in the Control system.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu   
 * @version $Id$
 * @since   ALMA 5.1.1
 */
public class GET_AMBIENT_TEMPERATURE_MonitorPointPM extends IdlMonitorPointPresentationModel<Float> {
    
    public GET_AMBIENT_TEMPERATURE_MonitorPointPM(
            Logger logger, 
            DevicePresentationModel container,
            IMonitorPoint monitorPoint
    ) {
        super(logger, container, monitorPoint);
    }
    
    /**
     * Get the current value from the hardware device property this presentation model represents.
     * @return the current value held by the hardware device.
     */
    protected Float getValueFromDevice(Date timeStamp) {
        float res = 0;
        LongHolder l = new LongHolder();
        try {
            DTXOperations dev = (DTXOperations)deviceComponent;
            try {
                res = dev.GET_AMBIENT_TEMPERATURE(l);
            } catch (INACTErrorEx ex) {
                logger.severe("INACTErrorEx: Failed to read DTX ambient temperature from GET_AMBIENT_TEMPERATURE()");
            } catch (CAMBErrorEx ex) {
                logger.severe("INACTErrorEx: Failed to read DTX ambient temperature from GET_AMBIENT_TEMPERATURE()");
            }
        } catch (IllegalArgumentException e) {
            logger.severe(
                    "GET_AMBIENT_TEMPERATURE_MonitorPointPM.getValueFromDevice()" +
                    " - illegal arguments for method" + 
                    ExcLog.details(e)
            );
            e.printStackTrace();
        }
        timeStamp.setTime(TimeHelper.utcOmgToJava(l.value));
        return res;
    }

    @Override
    protected Float getValueFromDevice() {
        return getValueFromDevice(new Date());
    }    
}

//
// O_o
