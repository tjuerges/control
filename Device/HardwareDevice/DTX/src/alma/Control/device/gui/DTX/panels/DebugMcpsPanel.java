/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.JLabel;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel provides access to the functions on the MCPS board. The MCPS board is separate from the
 * formater, and may provide helpful information if the formatter is not communicating.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 5.0.3
 */

public class DebugMcpsPanel extends DevicePanel {

    protected DebugMcpsPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        inputVoltagePanel = new InputVoltagePanel(logger, aDevicePM);
        hardwareResetPanel = new HardwareResetPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(inputVoltagePanel, c);
        c.gridy++;
        add(hardwareResetPanel, c);

        setVisible(true);
    }

    /*
     * Display the module input status. Provide for the formatter 48 volt control.
     */
    private class InputVoltagePanel extends DevicePanel {

        private InputVoltagePanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Module Power"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("48v Status", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_48_V_ON),
                    Status.OFF, "Module 48 Volts Off",
                    Status.ON, "Module 48 Volts in OK"),c);
            c.gridy++;
            addLeftLabel("125MHz Status", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_48_V_125MHZMISSING),
                    Status.FAILURE, "Formater 125MHz input clock Error",
                    Status.GOOD, "Formater 125MHz input clock OK"),c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.FR_48_V),
                    "Turn DTX On", true), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.FR_48_V),
                    "Turn DTX OFF", false), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * A place to put the reset button.
     * The reset button is isolated off on its own on this remove panel because people
     * should not push it unless they really really mean it.
     */
    private class HardwareResetPanel extends DevicePanel {

        public HardwareResetPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 0;

            add(new JLabel("Reset the formatter and reload the FPGAs"), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.FR_RELOAD_FPGA),
                    "Reload FPGAs", 99), c);
            c.gridy++;
            add(new JLabel("DO NOT push unless you really really mean it!"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    private HardwareResetPanel hardwareResetPanel;
    private InputVoltagePanel inputVoltagePanel;
}

//
// O_o

