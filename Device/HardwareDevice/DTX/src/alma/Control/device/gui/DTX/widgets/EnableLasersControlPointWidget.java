/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

/**
 * <code>EnableLasersControlPointWidget.java</code> presents the user with an interface for using the 
 * TTX_LASER_ENABLE control point on the DTX. This control provides 3 check boxes: one for each of the
 * 3 lasers on the DTX.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 7.0.0
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class EnableLasersControlPointWidget extends JPanel implements ActionListener {
    
    /**Construct a new EnableLasersControlPointWidget
     * 
     * @param logger the logger that all logs should be directed to.
     * @param controlPointModel The control point for TTX_LASER_ENABLE.
     */
    public EnableLasersControlPointWidget( Logger logger,
            ControlPointPresentationModel<Integer> controlPointModel) {
        this.logger = logger;
        this.controlPointModel = controlPointModel;
        enableLasersButton=new JButton(buttonLabel);                               //Setup the Change TE button
        enableLasersButton.addActionListener(this);
        controlPointModel.getContainingDevicePM().addControl(enableLasersButton);
        laserB=new JCheckBox("Enable B");  //Setup the reset and resync check boxes
        controlPointModel.getContainingDevicePM().addControl(laserB);
        laserC=new JCheckBox("Enable C");
        controlPointModel.getContainingDevicePM().addControl(laserC);
        laserD=new JCheckBox("Enable D");
        controlPointModel.getContainingDevicePM().addControl(laserD);
        laserB.setSelected(true);			//Default to checked
        laserC.setSelected(true);			//The primary thing these will be used for is turning on the lasers
        laserD.setSelected(true);			//since resetting the module turns them off.
        buildWidget();
    }
    
    /** When an actionEvent occurs (e.g the control button is pressed) check the value in the
     *  menu. Find the corresponding code and send it to the control point.
     */
    public void actionPerformed(ActionEvent event) {
        int laserBCode=0;
        int laserCCode=0;
        int laserDCode=0;
        int outCode=0;
        
        if (laserB.isSelected())
            laserBCode=0x4;
        if (laserC.isSelected())
            laserCCode=0x2;
        if (laserD.isSelected())
            laserDCode=0x1;
        
        outCode=laserBCode | laserCCode | laserDCode;
        controlPointModel.setValue(outCode);
    }
    
    /** Just add the option menu and control button to this panel.
     * @param buttonText The text to put on the control button.
     */
    private void buildWidget() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Control"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.weightx=100;
        c.weighty=100;
        c.gridx = 0;
        c.gridy = 0;
        add(laserD, c);
        c.gridy++;
        add(laserC, c);
        c.gridy++;
        add(laserB, c);
        c.gridy++;
        add(enableLasersButton, c);
    }
    
    private static final long serialVersionUID = 1L;
    private String buttonLabel="Set lasers";
    private JButton enableLasersButton;
    private ControlPointPresentationModel<Integer> controlPointModel;
    private Logger logger;
    private JCheckBox laserB;
    private JCheckBox laserC;
    private JCheckBox laserD;
}
