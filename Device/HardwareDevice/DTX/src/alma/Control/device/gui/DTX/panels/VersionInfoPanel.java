/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerAsByteSeqMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;
import alma.Control.device.gui.common.widgets.VersionMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel provides the module information for the DTX. Serial numbers, firmware revisions, etc.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class VersionInfoPanel extends DevicePanel {

    public VersionInfoPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        lruPanel = new LRUPanel(logger, aDevicePM);
        fpgaPanel = new FPGAPanel(logger, aDevicePM);
        digitizerInfoPanel = new DigitizerInfoPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        //        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
        //                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(lruPanel, c);
        c.gridy++;
        add(fpgaPanel, c);
        c.gridy++;
        add(digitizerInfoPanel, c);

        setVisible(true);
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class LRUPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public LRUPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("LRU Info"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("Serial Number", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LRU_CIN_LRU_SN)), c);
            c.gridy++;
            addLeftLabel("Revision", c);
            add(new CodeToTextMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LRU_CIN_LRU_REV), letters), c);
            c.gridy++;
            addLeftLabel("LRU CIN", c);
            //TODO: Fix the formatting for this point so that it comes out as 53.06.00.00
            add(new IntegerAsByteSeqMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_LRU_CIN_CIN4THLEVEL), ".", false), c);
            c.gridy++;
            addLeftLabel("Formatter SN", c);
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_FORMATTER_SN), false, false), c);
        }
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class FPGAPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public FPGAPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("FPGA Info"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("Firmware Revision"),c);
            c.gridy++;
            //TODO: Make sure this is displaying info in a way that is OK with the DTX
            //TODO: Add the IPT ID and MID as hex monitor widgets
            addLeftLabel("D", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH1_REV_MAJ),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH1_REV_MIN),    
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH1_DAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH1_MONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH1_YEAR)),c);
            c.gridy++;
            addLeftLabel("C", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_REV_MAJ),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_REV_MIN),    
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_DAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_MONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_YEAR)),c);
            c.gridy++;
            addLeftLabel("B", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH3_REV_MAJ),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH3_REV_MIN),    
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH3_DAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH3_MONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH3_YEAR)),c);
            c.gridx=2;
            c.gridy=0;
            add(new JLabel(" IPT ID "), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH1_BE),
                    false, true), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_BE),
                    false, true), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH3_BE),
                    false, true), c);
            c.gridx=3;
            c.gridy=0;
            add(new JLabel(" MID "), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH1_ID),
                    false, true), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH2_ID),
                    false, true), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_FPGA_FW_VER_CH3_ID),
                    false, true), c);
        }
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class DigitizerInfoPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public DigitizerInfoPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Digitizer"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            addLeftLabel("Digitizer SN:", c);
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DG_SN), false, true), c);

            c.gridy++;
            addLeftLabel("Firmware Revision", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DG_FW_VER_MAJOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DG_FW_VER_MINOR)), c);
            c.gridy++;
            addLeftLabel("Hardwarerevision", c);
            add(new CodeToTextMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DG_HARDWARE_REV), letters), c);
        }
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private static final String [] letters = {"0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
            "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private LRUPanel lruPanel;
    private FPGAPanel fpgaPanel;
    private DigitizerInfoPanel digitizerInfoPanel;
}

//
// O_o

