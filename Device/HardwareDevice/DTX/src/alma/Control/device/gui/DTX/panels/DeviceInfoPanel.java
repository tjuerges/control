/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.IdlMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.panels.AmbsiPanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel provides the information on the module (S/N, LRU, etc) and the Ambsi.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 5.0.3
 */

public class DeviceInfoPanel extends DevicePanel {
    
    public DeviceInfoPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        ambsiPanel = new AmbsiPanel(logger, aDevicePM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.SERIAL_NUMBER),
                devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.TRANS_NUM),
                devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_SLAVE_ERROR_CODE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.CAN_ERROR_COUNT),
                devicePM.getMonitorPointPM(IcdMonitorPoints.ERROR_CODE_LAST_CAN_ERROR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PATCH_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_PATCH_LEVEL),
                devicePM.getControlPointPM(IcdControlPoints.RESET_AMBSI));
        versionInfoPanel = new VersionInfoPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(ambsiPanel, c);
        c.gridx++;
        add(versionInfoPanel, c);

        setVisible(true);
    }
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    private AmbsiPanel ambsiPanel;
    private VersionInfoPanel versionInfoPanel;
}

//
// O_o

