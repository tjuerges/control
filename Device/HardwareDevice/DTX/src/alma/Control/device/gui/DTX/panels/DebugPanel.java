/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.DTX.widgets.DgTunner;
import alma.Control.device.gui.DTX.widgets.util.DtxDataGrabber;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.util.DtsData.IDataGrabber;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This class contains all the GUI interface elements that are normally needed only for debug purposes.
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author David Hunter      dhunter@nrao.edu
 * @author Scott Rankin      srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    JTabbedPane tabs;
    
    public DebugPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        debugControlWordsPanel = new DebugControlWordsPanel(logger, aDevicePM);
        debugWalshFunctionPanel = new DebugWalshFunctionPanel(logger, aDevicePM);
        debugPayloadDataPanel = new DebugPayloadDataPanel(logger, aDevicePM);
        debugRNGModePanel = new DebugRNGModePanel(logger, aDevicePM);
        debugInputTestRegistersPanel = new DebugInputTestRegistersPanel(logger, aDevicePM);
        debugMcpsPanel = new DebugMcpsPanel(logger, aDevicePM);
        debugI2CPanel = new DebugI2CPanel(logger, aDevicePM);
        IDataGrabber dataGrabber =  new DtxDataGrabber(logger, devicePM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_HI_CH1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_LO_CH1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_HI_CH2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_LO_CH2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_HI_CH3),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_LO_CH3),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS),
                devicePM.getControlPointPM(IcdControlPoints.FR_CAPTURE_PAYLOAD));
        debugTunningPanel = new DgTunner(logger, aDevicePM, dataGrabber);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weighty=100;
        c.weightx=100;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;

        tabs = new JTabbedPane();

        //TODO: Add more tabs here
        tabs.add("Control Words", new JScrollPane(debugControlWordsPanel));
        tabs.add("Walsh Function", new JScrollPane(debugWalshFunctionPanel));
        tabs.add("Payload", debugPayloadDataPanel);
        tabs.add("RNG", new JScrollPane(debugRNGModePanel));
        tabs.add("Test Data", new JScrollPane(debugInputTestRegistersPanel));
        tabs.add("MCPS", debugMcpsPanel);
        tabs.add("I2C", new JScrollPane(debugI2CPanel));
        tabs.add("Tune DG", new JScrollPane(debugTunningPanel));
        
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        add(tabs, c);

        this.setVisible(true);
    }
    
    private DebugControlWordsPanel debugControlWordsPanel; 
    private DebugWalshFunctionPanel debugWalshFunctionPanel;
    private DebugPayloadDataPanel debugPayloadDataPanel;
    private DebugRNGModePanel debugRNGModePanel;
    private DebugInputTestRegistersPanel debugInputTestRegistersPanel;
    private DebugMcpsPanel debugMcpsPanel;
    private DebugI2CPanel debugI2CPanel;
    private DgTunner debugTunningPanel;
}

//
// O_o
