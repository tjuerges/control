/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets.util;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis.AxisTitle;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyMinimumViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.util.Range;


import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.widgets.util.DtsData.DataStatistics;
import alma.Control.device.gui.common.widgets.util.DtsData.InsufficientDataException;

/**
 * This is class that displays PDF graphs for the DG tuning.
 * It always displays the theoretical PDF. Upon a call to resetPdf it
 * will also display the given PDF (overwriting the old one, if there is any).
 * 
 * Used for tuning the digitizer (which is part of the DTX module).
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version
 * @since ALMA 7.1.0
 * 
 */
public class PdfGrapher extends JPanel {
    /**
     * Construct an PdfGrapher with bacon and eggs.
     * @param logger
     */
    public PdfGrapher(Logger newLogger, DevicePM dPM) {
        logger=newLogger;
        setMinimumSize(new Dimension(50,50));
        devicePM = dPM;
        buildWidget();
    }

    
    /**
     * Replace the current observed PDF graph with a new one.
     * This function should not be called in the event dispatch thread.
     * It will call the (supposed to be) properly threaded revalidate once
     * it has updated the threads.
     * @param newData
     */
    public void resetPdf(DataStatistics newData){
        double[] newPdf = null;
        try {
            newPdf = newData.getPdf();
        } catch (InsufficientDataException e) {
            logger.severe("PdfGrapher, resetPdf: Error: insufficent data in given DataStatistis!");
            return;
        }
        data.removeAllPoints();
        for (int c=0; c<8; c++)
            drawBarPoint(data, c-4, newPdf[c]);
        revalidate();
    }
    
    protected void buildWidget() {
        
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Probability Density Function"),
                BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new BorderLayout());
        
        chart=new Chart2D();
        
        //Add the data trace 1st so that the graph looks better.
        data = new Trace2DLtd();
        data.setColor(DEFAULT_PDF_COLOR);
        data.setName("Observed PDF");
        chart.addTrace(data);
        add(chart);
        //Make sure the chart gets cleaned up when the GUI closes.
        devicePM.addShutdownAction(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {chart.destroy();}
        });
        
        theoreticalPdfTrace=new Trace2DLtd();
        theoreticalPdfTrace.setName("Theoretical PDF");
        //Changing the width makes both sets of bars more visible.
        WIDTH = WIDTH-0.1;
        for (int c=0; c<8; c++)
            drawBarPoint(theoreticalPdfTrace, c-4, theoreticalPdf[c]);
        WIDTH = WIDTH+0.1;
        theoreticalPdfTrace.setColor(DEFAULT_THEORETICAL_COLOR);
        chart.addTrace(theoreticalPdfTrace);
        chart.getAxisY().setAxisTitle(new AxisTitle("Probability"));
        chart.getAxisY().setPaintScale(false);
//        chart.getAxisX().setAxisTitle(new AxisTitle("Alma Value"));
        chart.getAxisX().setAxisTitle(new AxisTitle(null));
        
        chart.getAxisY().setRangePolicy(new RangePolicyMinimumViewport(new Range(0, 0.3)));
        chart.getAxisX().setRangePolicy(new RangePolicyFixedViewport(new Range(-4, 3)));
        
        //JChart2D is refusing to display only integer ticks, so we just are not showing them.
        chart.getAxisX().setPaintScale(false);
        chart.getAxisX().setMajorTickSpacing(1);
        chart.getAxisX().setMinorTickSpacing(0);
    }
    
    /**
     * This is used to draw points on to look like a bar graph instead of a line graph.
     * The constant WIDTH is used to set the width of the bars.
     * It is assumed that the last point on the trace had a Y value of 0.
     * TODO: come up with a way to display a bar chart with one set of bars filled in.
     *       (We might have to switch to JFreeChart to do this)
     * @param trace The trace to put the point on
     * @param x  The X value of the point.
     * @param y  The Y value fo the point.
     */
    private void drawBarPoint(Trace2DLtd trace, double x, double y){
        trace.addPoint(x-WIDTH/2, 0);
        trace.addPoint(x-WIDTH/2, y);
        trace.addPoint(x+WIDTH/2, y);
        trace.addPoint(x+WIDTH/2, 0);
    }

    private Chart2D chart;
    //These values will be used by the child classes of this class.
    private Trace2DLtd data;
    private DevicePM devicePM;
    private Logger logger;
    private Trace2DLtd theoreticalPdfTrace;
    //This is the theoretical PDF (probability density function), or what we expect the values to be, assuming
    //that we are seeing incoming boadband Gaussian noise. Used to calculate the offset error and
    //amplitude error.
    //From 50.00.00.00-306-A-MAN
    private final double[] theoreticalPdf = {0.0349931, 0.0785305, 0.1593980, 0.2270780,
                               0.2270780, 0.1593980, 0.0785305, 0.0349931};
    private Color DEFAULT_PDF_COLOR = Color.BLACK;
    private static final Color DEFAULT_THEORETICAL_COLOR = new Color(55f/255, 184f/255, 0f);
    //The width of the bars on the PDF chart
    private double WIDTH = 0.8;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o