/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets.util;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis.AxisTitle;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyMinimumViewport;
import info.monitorenter.gui.chart.traces.Trace2DLtd;
import info.monitorenter.util.Range;


import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.widgets.util.DtsData.DataStatistics;
import alma.Control.device.gui.common.widgets.util.DtsData.InsufficientDataException;

/**
 * This class graphs the error constellation for the DG tuning GUI. The error constellation is a graph
 * of the amplitude error (Y values) & offset error (X values) of the given DataStatistics. 
 * 
 * Used for tuning the digitizer (which is part of the DTX module).
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version
 * @since ALMA 7.1.0
 * 
 */
public class ErrorGrapher extends JPanel {
    /**
     * Construct a new ErrorGrapher.
     * @param logger
     */
    public ErrorGrapher(Logger newLogger, DevicePM dPM) {
        logger=newLogger;
        data = new Trace2DLtd[16];
        devicePM = dPM;
        chart=new Chart2D();
        //Make sure the chart gets cleaned up when the GUI closes.
        devicePM.addShutdownAction(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {chart.destroy();}
        });
        chart.setPaintLabels(false);
        for (int c=0; c<16; c++){
            data[c] = new Trace2DLtd(2);
            data[c].addPoint(0,0);
            data[c].addPoint(0,0);
            chart.addTrace(data[c]);
        }
        chart.getAxisX().setAxisTitle(new AxisTitle("Offset Error"));
        chart.getAxisY().setAxisTitle(new AxisTitle("Amplitude Error"));
        
        //Draw the target circle.
        targetTop = new Trace2DLtd();
        targetBottom = new Trace2DLtd();
        for (double x=-TARGET_RADIUS; x<=(TARGET_RADIUS); x+=TARGET_RADIUS/25){
            targetTop.addPoint(x, Math.sqrt(TARGET_RADIUS*TARGET_RADIUS-x*x));
            targetBottom.addPoint(x, -Math.sqrt(TARGET_RADIUS*TARGET_RADIUS-x*x));
        }
        //The above loop leaves off the last point, so we add it manually.
        targetTop.addPoint(TARGET_RADIUS, 0);
        targetBottom.addPoint(TARGET_RADIUS, 0);
        targetTop.setColor(TARGET_COLOR);
        targetBottom.setColor(TARGET_COLOR);
        chart.addTrace(targetTop);
        chart.addTrace(targetBottom);
        chart.getAxisY().setRangePolicy(new RangePolicyFixedViewport(new Range(-0.1, 0.1)));
        chart.getAxisX().setRangePolicy(new RangePolicyFixedViewport(new Range(-0.1, 0.1)));
        buildWidget();
    }

    /**
     * Replace the current error constellation with a new one.
     * This function should not be called in the event dispatch thread.
     * It will call the (supposed to be) properly threaded revalidate once
     * it has updated the traces.
     * TODO: Do we need to call revalidate, and is JChart2D thread safe?
     * @param newData
     */
    public void resetConstellation(DataStatistics newData){
        double[] newAmpError = null;
        double[] newOffsetError = null;
        try {
            newAmpError = newData.getAmplitudeError();
            newOffsetError = newData.getOffsetError();
        } catch (InsufficientDataException e) {
            logger.severe("ErrorGrapher, resetConstellation: Error: insufficent data in given DataStatistis!");
            return;
        }
        for (int c=0; c<16; c++){
            data[c].setColor(DEFAULT_COLOR);
            data[c].addPoint(newOffsetError[c], newAmpError[c]);
            data[c].addPoint(0,0);
        }
        revalidate();
    }
    
    public void setAutoZoom(boolean autoZoom){
        if (autoZoom){
            chart.getAxisY().setRangePolicy(new RangePolicyMinimumViewport(new Range(-0.1, 0.1)));
            chart.getAxisX().setRangePolicy(new RangePolicyMinimumViewport(new Range(-0.1, 0.1)));
        }
        else{
            chart.getAxisY().setRangePolicy(new RangePolicyFixedViewport(new Range(-0.1, 0.1)));
            chart.getAxisX().setRangePolicy(new RangePolicyFixedViewport(new Range(-0.1, 0.1)));
        }
    }
    
    protected void buildWidget() {
        
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Error Constellation"),
                BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new BorderLayout());
        add(chart);
    }

    private Chart2D chart;
    //These values will be used by the child classes of this class.
    private Trace2DLtd[] data;
    private DevicePM devicePM;
    private Logger logger;
    private Trace2DLtd targetTop;
    private Trace2DLtd targetBottom;
    private static final Color DEFAULT_COLOR = Color.BLACK;
    private static final Color TARGET_COLOR = new Color(55f/255, 184f/255, 0f);
    private static final double TARGET_RADIUS=0.05;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
