/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;
import alma.Control.device.gui.common.widgets.TextToCodeControlPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel provides access to the RNG mode monitor and control points.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 5.0.3
 */

public class DebugRNGModePanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    private RNGControlPanel rngControlPanel;
    private RNGMonitorPanel rngMonitorPanel;

    public DebugRNGModePanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        rngControlPanel = new RNGControlPanel(logger, aDevicePM);
        rngMonitorPanel = new RNGMonitorPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
//        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(rngMonitorPanel, c);
        c.gridy++;
        add(rngControlPanel, c);

        setVisible(true);
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class RNGMonitorPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public RNGMonitorPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first widget
            addLeftLabel("Ch D RNG mode:", c);
            String[] options = {"Normal Mode", "Test Mode, RNG on", "RNG Off, counting test pattern",
                    "RNG On, re-start every TE"};
            add (new CodeToTextMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_RNG_CH1_MODE), options), c);
            c.gridy++;
            addLeftLabel("Ch C RNG mode:", c);
            add (new CodeToTextMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_RNG_CH2_MODE), options), c);
            c.gridy++;
            addLeftLabel("Ch B RNG mode:", c);
            add (new CodeToTextMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_RNG_CH3_MODE), options), c);
        }
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class RNGControlPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public RNGControlPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;

            String[] options = {"Normal Mode", "Test Mode, RNG on", "RNG Off, counting test pattern",
                              "RNG On, re-start every TE"};
            int[] codes = {0, 1, 2, 3};
            add (new TextToCodeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.FR_RNG_CH1),
                    "Set RNG D", options, codes),c);
            c.gridy++;
            add (new TextToCodeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.FR_RNG_CH2),
                    "Set RNG C", options, codes),c);
            c.gridy++;
            add (new TextToCodeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.FR_RNG_CH3),
                    "Set RNG B", options, codes),c);
            c.gridy++;
            add (new TextToCodeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.FR_RNG_ALL),
                    "Set All RNGs", options, codes),c);
        }
    }
}

//
// O_o

