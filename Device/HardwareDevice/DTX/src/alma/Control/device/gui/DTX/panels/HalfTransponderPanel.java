/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.DTX.widgets.EnableLasersControlPointWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**DTX Half transponder panel
 * This panel provides information on each of the 3 TX only half transponders, found in each DTX. 
 * This includes the TTx alarms, power, temp, and enable/disable controls.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class HalfTransponderPanel extends DevicePanel {

    private LaserPanel laserPanel;
    private LasersEnabledPanel lasersEnabledPanel;
    private TxAlarmPanel txAlarmPanel;

    public HalfTransponderPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSupPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSupPanels(Logger logger, DevicePM aDevicePM) {
        laserPanel = new LaserPanel(logger, aDevicePM);
        lasersEnabledPanel = new LasersEnabledPanel(logger, aDevicePM);
        txAlarmPanel = new TxAlarmPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight=2;
        add(txAlarmPanel, c);
        c.gridheight=1;
        c.gridx = 1;
        c.gridy = 0;
        add(laserPanel, c);
        c.gridy++;
        add(lasersEnabledPanel, c);
        setVisible(true);
    }

    /*
     * Tx Alarm panel.
     * This panel displays the information provided by the 3 TTX alarm status registers. There is 1
     * status register per channel, each of which provides 10 separate boolean alarms. 
     */
    private class TxAlarmPanel extends DevicePanel {

        public TxAlarmPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Tx Alarms"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            c.gridx=0;
            c.gridy=0;
            add(new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.TTX_CLR_ALARMS),
                    "Reset TTX Alarms", 8), c);
            c.gridx = 1;
            add(new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("Eol Alarm", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_EOLALM_D),
                    Status.FAILURE, "TTX D: End of Life Error",
                    Status.GOOD, "TTX D: No EOL Alarm"),c);
            c.gridy++;
            addLeftLabel("Modulator Temp", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_MODTEMPALM_D),
                    Status.FAILURE, "TTX D: Modulator Temperature out of range",
                    Status.GOOD, "TTX D: Modulator Temperature OK"),c);
            c.gridy++;
            addLeftLabel("Laser Wavelength", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSWAVALM_D),
                    Status.FAILURE, "TTX D: Laser Wavelength out of range",
                    Status.GOOD, "TTX D: Laser Wavelength OK"),c);
            c.gridy++;
            addLeftLabel("Summary Alarm", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXALMINT_D),
                    Status.FAILURE, "TTX D: Error Detected",
                    Status.GOOD, "TTX D: No alarms"),c);
            c.gridy++;
            addLeftLabel("Laser Bias", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSBIASALM_D),
                    Status.FAILURE, "TTX D: Laser Bias current out of range",
                    Status.GOOD, "TTX D: Laser Bias current OK"),c);
            c.gridy++;
            addLeftLabel("Laser Temp", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSTEMPALM_D),
                    Status.FAILURE, "TTX D: Laser Temperature out of range",
                    Status.GOOD, "TTX D: Laser Temperature OK"),c);
            c.gridy++;
            addLeftLabel("Tx PLL Lock", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXLOCKERR_D),
                    Status.FAILURE, "TTX D: Tx PLL Lock Error",
                    Status.GOOD, "TTX D: Tx PLL Locked"),c);
            c.gridy++;
            addLeftLabel("Laser Power Alarm", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSPOWALM_D),
                    Status.FAILURE, "TTX D: Laser power out of range",
                    Status.GOOD, "TTX D: Laser power OK"),c);
            c.gridy++;
            addLeftLabel("Modulator Bias", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_MODBIASALM_D),
                    Status.FAILURE, "TTX D: Modulator Bias out of range",
                    Status.GOOD, "TTX D: Modulator Bias OK"),c);
            c.gridy++;
            addLeftLabel("Tx FIFO", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXFIFOERR_D),
                    Status.FAILURE, "TTX D: FIFO error",
                    Status.GOOD, "TTX D: FIFO OK"),c);
            c.gridx = 2;
            c.gridy = 0;
            add(new JLabel("C"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_EOLALM_C),
                    Status.FAILURE, "TTX C: End of Life Error",
                    Status.GOOD, "TTX C: No EOL Alarm"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_MODTEMPALM_C),
                    Status.FAILURE, "TTX C: Modulator Temperature out of range",
                    Status.GOOD, "TTX C: Modulator Temperature OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSWAVALM_C),
                    Status.FAILURE, "TTX C: Laser Wavelength out of range",
                    Status.GOOD, "TTX C: Laser Wavelength OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXALMINT_C),
                    Status.FAILURE, "TTX C: Error Detected",
                    Status.GOOD, "TTX C: No alarms"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSBIASALM_C),
                    Status.FAILURE, "TTX C: Laser Bias current out of range",
                    Status.GOOD, "TTX C: Laser Bias current OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSTEMPALM_C),
                    Status.FAILURE, "TTX C: Laser Temperature out of range",
                    Status.GOOD, "TTX C: Laser Temperature OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXLOCKERR_C),
                    Status.FAILURE, "TTX C: Tx PLL Lock Error",
                    Status.GOOD, "TTX C: Tx PLL Locked"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSPOWALM_C),
                    Status.FAILURE, "TTX C: Laser power out of range",
                    Status.GOOD, "TTX C: Laser power OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_MODBIASALM_C),
                    Status.FAILURE, "TTX C: Modulator Bias out of range",
                    Status.GOOD, "TTX C: Modulator Bias OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXFIFOERR_C),
                    Status.FAILURE, "TTX C: FIFO error",
                    Status.GOOD, "TTX C: FIFO OK"),c);
            c.gridx = 3;
            c.gridy = 0;
            add(new JLabel("B"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_EOLALM_B),
                    Status.FAILURE, "TTX B: End of Life Error",
                    Status.GOOD, "TTX B: No EOL Alarm"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_MODTEMPALM_B),
                    Status.FAILURE, "TTX B: Modulator Temperature out of range",
                    Status.GOOD, "TTX B: Modulator Temperature OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSWAVALM_B),
                    Status.FAILURE, "TTX B: Laser Wavelength out of range",
                    Status.GOOD, "TTX B: Laser Wavelength OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXALMINT_B),
                    Status.FAILURE, "TTX B: Error Detected",
                    Status.GOOD, "TTX B: No alarms"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSBIASALM_B),
                    Status.FAILURE, "TTX B: Laser Bias current out of range",
                    Status.GOOD, "TTX B: Laser Bias current OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSTEMPALM_B),
                    Status.FAILURE, "TTX B: Laser Temperature out of range",
                    Status.GOOD, "TTX B: Laser Temperature OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXLOCKERR_B),
                    Status.FAILURE, "TTX B: Tx PLL Lock Error",
                    Status.GOOD, "TTX B: Tx PLL Locked"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_LSPOWALM_B),
                    Status.FAILURE, "TTX B: Laser power out of range",
                    Status.GOOD, "TTX B: Laser power OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_MODBIASALM_B),
                    Status.FAILURE, "TTX B: Modulator Bias out of range",
                    Status.GOOD, "TTX B: Modulator Bias OK"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_ALARM_STATUS_TXFIFOERR_B),
                    Status.FAILURE, "TTX B: FIFO error",
                    Status.GOOD, "TTX B: FIFO OK"),c);
        }

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
    }

    /*
     * This panel displays the laser bias, power, and temp.
     */
    private class LaserPanel extends DevicePanel {

        public LaserPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Lasers"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            c.gridx = 1;
            c.gridy = 0;
            add (new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("Laser Bias", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH1), 0),c);
            c.gridy++;
            addLeftLabel("Laser Power", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH1), 0),c);
            c.gridy++;
            addLeftLabel("Laser Temp", c);
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH1), false, 2),c);
            c.gridx = 2;
            c.gridy = 0;
            add (new JLabel("C"), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH2), 0),c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH2), 0),c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH2), false, 2),c);
            c.gridx = 3;
            c.gridy = 0;
            add (new JLabel("B"), c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH3), 0),c);
            addRightLabel("\u03BCA", c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH3), 0),c);
            addRightLabel("\u03BCW", c);
            c.gridy++;
            add(new FloatMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH3), false, 2),c);
            addRightLabel("\u00B0C", c);
        }

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
    }

    /*
     * This panel displays the laser On/Off indicators and the enable/disable controls.
     */
    private class LasersEnabledPanel extends DevicePanel {

        public LasersEnabledPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Enable Lasers"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            add (new JLabel("Status    "), c);
            c.gridy++;
            addLeftLabel("TTX D", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_ENABLED_TTX1),
                    Status.OFF, "TTX D: Laser Off",
                    Status.ON, "TTX D: Laser On"),c);
            c.gridy++;
            addLeftLabel("TTX C", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_ENABLED_TTX2),
                    Status.OFF, "TTX C: Laser Off",
                    Status.ON, "TTX C: Laser On"),c);
            c.gridy++;
            addLeftLabel("TTX B", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_ENABLED_TTX3),
                    Status.OFF, "TTX B: Laser Off",
                    Status.ON, "TTX B: Laser On"),c);
            c.gridy=0;
            c.gridx++;
            c.gridheight=4;
            add(new EnableLasersControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.TTX_LASER_ENABLE)), c);
        }
        
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o

