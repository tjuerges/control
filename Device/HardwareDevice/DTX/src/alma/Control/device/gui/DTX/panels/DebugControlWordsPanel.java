/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.DTX.widgets.TestPatternControlWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.EightDipSwitchesMonitorPointWidget;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This pane contains the dip switch readouts and the test data monitors and controls
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugControlWordsPanel extends DevicePanel {

    protected DebugControlWordsPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        controlPanel = new ControlPanel(logger, aDevicePM);
        dipSwitchPanel = new DipSwitchPanel(logger, aDevicePM);
        monitorPanel = new MonitorPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(controlPanel, c);
        c.gridy++;
        add(dipSwitchPanel, c);
        c.gridx = 1;
        c.gridy = 0;
        c.gridheight=2;
        add(monitorPanel, c);

        setVisible(true);
    }

    /*
     * This panel provides access to Control Words control widget.
     */
    private class ControlPanel extends DevicePanel {

        private ControlPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
 
            c.gridx = 1;
            c.gridy = 0;
            add (new TestPatternControlWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.FR_CW_CH1),
                    devicePM.getControlPointPM(IcdControlPoints.FR_CW_CH2),                    
                    devicePM.getControlPointPM(IcdControlPoints.FR_CW_CH3),
                    devicePM.getControlPointPM(IcdControlPoints.FR_CW_ALL)), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This panel gives a readout showing the current setting of the formatter
     * hardware dip switches. 
     */
    private class DipSwitchPanel extends DevicePanel {

        private DipSwitchPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("FPGA Dip Switch Settings"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("Channel D:"), c);
            c.gridy++;
            addLeftLabel("(7)", c);
            addRightLabel("(0)", c);
            add(new EightDipSwitchesMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_SWITCH_CH1),
                    Status.OFF, "FPGA Channel D dips switches",
                    Status.ON, "FPGA Channel D dip switches"), c);
            c.gridy++;
            add(new JLabel("Channel C:"), c);
            c.gridy++;
            addLeftLabel("(7)", c);
            addRightLabel("(0)", c);
            add(new EightDipSwitchesMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_SWITCH_CH2),
                    Status.OFF, "FPGA Channel C dips switches",
                    Status.ON, "FPGA Channel C dip switches"), c);
            c.gridy++;
            add(new JLabel("Channel B:"), c);
            c.gridy++;
            addLeftLabel("(7)", c);
            addRightLabel("(0)", c);
            add(new EightDipSwitchesMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_SWITCH_CH3),
                    Status.OFF, "FPGA Channel B dips switches",
                    Status.ON, "FPGA Channel B dip switches"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This panel provides a readout showing what the Control Words are currently set to.
     * TODO: Add the readout.
     */
    private class MonitorPanel extends DevicePanel {

        public MonitorPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            String[] frameOptionList={"Normal Frame", "Alternating 0x0000FFFF x4",
                    "Alternating 0xFFFF0000 x4", "0xAAAA4444 x4", "All ones",
                    "All Zeros", "0xCCCC x8", "0x3333 x8"};
            String[] payloadOptionList={"Normal payload", "All Zeros", "All Ones",
                    "0xFFFF0000 x4", "0xCCCC x8", "0x3333 x8", "0xAAAA5555 x4",
                    "F(cntr)E(cntr)D(cntr)...1(cntr)0(cntr)"};
            
            addLeftLabel("Payload D:", c);
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH1_PAYLOAD_DATA),
                    payloadOptionList, "Error"), c);
            c.gridy++;
            addLeftLabel("Ch D parity", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH1_PARITY),
                    Status.GOOD, "Ch D: parity enabled", Status.ATYPICAL, "Ch D: parity disabled!"), c);
            c.gridy++;
            addLeftLabel("Frame D: ", c);
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH1_FRAME_DATA),
                    frameOptionList, "Error"), c);
            c.gridy++;
            addLeftLabel("Ch D Code", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH1_SCRAMBLE_CODE),
                    Status.GOOD, "Ch D: Scramble Code enabled",
                    Status.ATYPICAL, "Ch D: Scramble Code disabled!"), c);
            c.gridy++;
            addLeftLabel("Payload C:", c);
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH2_PAYLOAD_DATA),
                    payloadOptionList, "Error"), c);
            c.gridy++;
            addLeftLabel("Ch C parity", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH2_PARITY),
                    Status.GOOD, "Ch C: Parity enabled", Status.ATYPICAL, "Ch C: Parity disabled!"), c);
            c.gridy++;
            addLeftLabel("Frame C: ", c);
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH2_FRAME_DATA),
                    frameOptionList, "Error"), c);
            c.gridy++;
            addLeftLabel("Ch C Code", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH2_SCRAMBLE_CODE),
                    Status.GOOD, "Ch C: Scramble Code enabled",
                    Status.ATYPICAL, "Ch C: Scramble Code disabled!"), c);
            c.gridy++;
            addLeftLabel("Payload B:", c);
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH3_PAYLOAD_DATA),
                    payloadOptionList, "Error"), c);
            c.gridy++;
            addLeftLabel("Ch B parity", c); 
            add (new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH3_PARITY),
                    Status.GOOD, "Ch B: Parity enabled", Status.ATYPICAL, "Ch B: Parity disabled!"), c);
            c.gridy++;
            addLeftLabel("Frame B: ", c);
            add(new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH3_FRAME_DATA),
                    frameOptionList, "Error"), c);
            c.gridy++;
            addLeftLabel("Ch B Code", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_CW_CH3_SCRAMBLE_CODE),
                    Status.GOOD, "Ch B: Scramble Code enabled",
                    Status.ATYPICAL, "Ch B: Scramble Code disabled!"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    private ControlPanel controlPanel;
    private DipSwitchPanel dipSwitchPanel;
    private MonitorPanel monitorPanel;
}

//
// O_o

