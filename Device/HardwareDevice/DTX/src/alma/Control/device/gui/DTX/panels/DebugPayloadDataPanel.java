/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.DTX.widgets.util.DtxDataGrabber;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.DtsDataCaptureWidget;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.util.DtsData.IDataGrabber;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel contains the readouts and graphs showing the status of the DTX payload data.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugPayloadDataPanel extends DevicePanel {

    public DebugPayloadDataPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanel(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanel(Logger logger, DevicePM aDevicePM) {
        fifoStatusPanel = new FifoStatusPanel(logger, aDevicePM);
        displayModePanel = new DisplayModePanel(logger, aDevicePM);
//      dataGraphPanel = new DataGraphPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
//        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(fifoStatusPanel, c);
        c.gridx++;
        c.gridheight=2;
        IDataGrabber dataGrabber =  new DtxDataGrabber(logger, devicePM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_HI_CH1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_LO_CH1),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_HI_CH2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_LO_CH2),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_HI_CH3),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_LO_CH3),
                devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS),
                devicePM.getControlPointPM(IcdControlPoints.FR_CAPTURE_PAYLOAD));
        add(new DtsDataCaptureWidget(logger, devicePM, dataGrabber), c);
        c.gridheight=1;
//      add(dataGraphPanel, c);
        c.gridx = 0;
        c.gridy++;
        add(displayModePanel, c);

        setVisible(true);
    }

    /*
     * This panel displays the status of the test data FIFO.
     */
    private class FifoStatusPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public FifoStatusPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("FIFO Status"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("Fifo Empty", c);
            add(new BooleanMonitorPointWidget(logger,devicePM
                                    .getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS_CH1FIFOEMPTY),
                                    Status.ON, "Ch D FIFO is not empty",
                                    Status.OFF, "Ch D FIFO is empty"), c);
            c.gridy++;
            addLeftLabel("Fifo Full", c);
            add(new BooleanMonitorPointWidget(logger,devicePM
                                    .getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS_CH1FIFOFULL),
                                    Status.OFF, "Ch D FIFO is not full",
                                    Status.ON, "Ch D FIFO is full"), c);
            c.gridx=2;
            c.gridy=0;
            add(new JLabel("C"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,devicePM
                                    .getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS_CH2FIFOEMPTY),
                                    Status.ON, "Ch C FIFO is not empty",
                                    Status.OFF, "Ch C FIFO is empty"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,devicePM
                                    .getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS_CH2FIFOFULL),
                                    Status.OFF, "Ch C FIFO is not full",
                                    Status.ON, "Ch C FIFO is full"), c);
            c.gridx=3;
            c.gridy=0;
            add(new JLabel("B"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,devicePM
                                    .getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS_CH3FIFOEMPTY),
                                    Status.ON, "Ch B FIFO is not empty",
                                    Status.OFF, "Ch B FIFO is empty"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,devicePM
                                    .getMonitorPointPM(IcdMonitorPoints.FR_PAYLOAD_STATUS_CH3FIFOFULL),
                                    Status.OFF, "Ch B FIFO is not full",
                                    Status.ON, "Ch B FIFO is full"), c);
        }
    }

    /*
     * Display the status of the digitizer mode.
     * This is shown here because it effects the results of the payload data captures.
     */
    private class DisplayModePanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public DisplayModePanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            addLeftLabel("DG Mode", c);
            add(new BooleanMonitorPointWidget(logger,devicePM
                            .getMonitorPointPM(IcdMonitorPoints.DG_TEST_PAT_MODE_ACTIVE),
                            Status.GOOD, "The DG is in normal mode",
                            Status.ATYPICAL, "The DG is in test mode"), c);
            c.gridy++;
            addLeftLabel("Set DG Mode:", c);
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DG_TEST_PAT),
                    "Normal", false), c);
            c.gridx++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DG_TEST_PAT),
                    "Test", true), c);
        }
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class DataGraphPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public DataGraphPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first Widget
            addLeftLabel("Data Graph", c);
            //TODO: Add Widget here

            //Move to the next row and add another Widget
            c.gridy++;
            addLeftLabel("Widgets go here", c);
            //TODO: Add Widget here
        }
    }
    
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    private DataGraphPanel dataGraphPanel;
    private DisplayModePanel displayModePanel;
    private FifoStatusPanel fifoStatusPanel;
}

//
// O_o

