/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel provides basic status information for the digitizer.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DigitizerPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DigitizerPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        //        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Pane Title"),
        //                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 1;
        c.gridy = 0;
        addLeftLabel("DG Temperature", c);
        add(new FloatMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.DG_TEMP), true, 2), c);
        c.gridy++;
        addLeftLabel("DG 3.3 volt input:", c);
        add(new FloatMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.DG_3_3_V), true, 2), c);
        c.gridy++;
        addLeftLabel("DG 5 volt input:", c);
        add(new FloatMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.DG_5_V), true, 2), c);
        c.gridy++;
        addLeftLabel("DG Power Supply: ", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DG_PS_ON),
                Status.OFF, "Error: Digitizer Power Supply is Off!",
                Status.ON, "Digitizer Power Supply On"), c);
        c.gridy++;
        addLeftLabel("DG EEPROM: ", c);
        add(new BooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DG_EE_FLAG),
                Status.GOOD, "No DG EEPROM Errors.",
                Status.FAILURE, "DG EEPROM Error! Service this DG."), c);
        c.gridy++;
        c.gridx--;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.DG_PS_ON_OFF),
                "Turn DG On", true), c);
        c.gridx++;
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.DG_PS_ON_OFF),
                "Turn DG Off", false), c);
        c.gridy++;
        c.gridx--;
        c.gridwidth=3;
        c.insets = new Insets(15,15,15,15);
        add(new BooleanControlPointWidget(logger, devicePM
                .getControlPointPM(IcdControlPoints.DG_RESET), "Reset Digitizer", 0xAB), c);
        
        setVisible(true);
    }
}

//
// O_o

