/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

package alma.Control.device.gui.DTX.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.FloatControlPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;

/** 
 * @author  dhunter
 * @version $Id$
 * @since    
 */

/**
 * This panel provides an interface to the Polarity 1 reference voltage controls on the digitizer.
 */
public class DgReferenceVoltagePanelP1 extends DevicePanel {
    public DgReferenceVoltagePanelP1(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }
    
    @Override
    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Reference Voltage, Polarity One"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        c.fill=GridBagConstraints.NONE;
        add(bP, c);
        c.gridy++;
        add(cP, c);
    }
    
    private void buildSubPanels(Logger logger, DevicePM dPM){
        bP = new ButtonPanel(logger, dPM);
        cP = new ControlPanel(logger, dPM);
    }
    
    private class ButtonPanel extends DevicePanel{
        public ButtonPanel (Logger logger, DevicePM dPM){
            super(logger, dPM);
            buildPanel();
        }
        @Override
        protected void buildPanel() {
            setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 0;
            c.gridy = 0;
            c.fill=GridBagConstraints.NONE;
            c.gridwidth=2;
            add (new JLabel("Adjust Amplitude Error"), c);
            c.gridwidth=1;
            c.gridy++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VMAG2), "+4.13mV", 0x02), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VMAG2), "+0.150uV", 0x01), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VMAG2), "-0.150uV", 0x11), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VMAG2), "-4.13mV", 0x12), c);
            c.gridy++;
            c.gridx=0;
            c.gridwidth=2;
            add (new JLabel("Adjust OffsetError"), c);
            c.gridwidth=1;
            c.gridy++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VOFF2), "+4.13mV", 0x02), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VOFF2), "+0.150uV", 0x01), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VOFF2), "-0.150uV", 0x11), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VOFF2), "-4.13mV", 0x12), c);
        }
    }
    
    private class ControlPanel extends DevicePanel{
        public ControlPanel(Logger logger, DevicePM dPM){
            super(logger, dPM);
            buildPanel();
        }
        
        @Override
        protected void buildPanel(){
            setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 1;
            c.gridy = 0;
            c.fill=GridBagConstraints.NONE;
            addLeftLabel ("Current Amplitude:", c);
            add (new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DG_VMAG2_WD)), c);
            c.gridy++;
//            addLeftLabel("Set Amplitude: ", c);
            c.gridx--;
            c.gridwidth=2;
            add (new FloatControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_VMAG2_WD), "Set Amplitude"), c);
            c.gridx++;
            c.gridwidth=1;
            c.gridy++;
            addLeftLabel ("Current Offset: ", c);
            add (new IntegerMonitorPointWidget(logger, 
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DG_VOFF2_WD)), c);
            c.gridy++;
            c.gridx--;
            c.gridwidth=2;
            add (new FloatControlPointWidget(logger, 
                    devicePM.getControlPointPM(IcdControlPoints.DG_VOFF2_WD), "Set Magnitude"), c);
        }
    }
    
    private ButtonPanel bP;
    private ControlPanel cP;
    //TODO: serialVersionUID
    private static final long serialVersionUID = 1L;
    
}

