/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.ByteSeqControlPointWidget;
import alma.Control.device.gui.common.widgets.DtsI2CReadWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel provides access to the DTX manual I2C interface and the test ITUs function.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 5.0.3
 */

public class DebugI2CPanel extends DevicePanel {
    public DebugI2CPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }
    
    protected void buildPanel(){
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(frI2CPanel, c);
        c.gridx++;
        add(dgCmdPanel, c);
        
        setVisible(true);
    }
    
    protected void buildSubPanels(Logger logger, DevicePM aDevicePM){
        dgCmdPanel = new DgCmdPanel(logger, aDevicePM);
        frI2CPanel = new FrI2CPanel(logger, aDevicePM);
    }

    private class DgCmdPanel extends DevicePanel{
        public DgCmdPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        @Override
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                    ("DG Cmd Interface"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth=2;
            String[] theLabels = {"Cmd", "Data"};
            add(new ByteSeqControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_CMD), "Send DG Cmd", 2,
                    theLabels), c);
            c.gridy++;
            c.gridx++;
            c.gridwidth=1;
            addLeftLabel("DG Mem: ", c);
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DG_MEM), false, true), c);
            c.gridy++;
            c.gridx--;
            c.gridwidth=2;
            c.insets = new Insets(20,0,0,20);
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DG_RESET), "DG Null Reset", 0x00), c);
        }
    }
    
    private class FrI2CPanel extends DevicePanel {
        private FrI2CPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                    ("TRX I2C interface"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 0;
            c.gridy = 0;
            add(new ByteSeqControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.TTX_I2C_CMD_CH1), "Send D", 8),c );
            c.gridx++;
            add(new ByteSeqControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.TTX_I2C_CMD_CH2), "Send C", 8), c);
            c.gridx++;
            add(new ByteSeqControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.TTX_I2C_CMD_CH3), "Send B", 8), c);
            c.gridy++;
            c.gridx=0;
            c.gridwidth=3;
            add(new DtsI2CReadWidget(logger, devicePM, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TTX_I2C_DATA)), c);
        }
    }
    private DgCmdPanel dgCmdPanel;
    private FrI2CPanel frI2CPanel;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o

