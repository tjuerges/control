/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.FloatControlPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;
import alma.Control.device.gui.common.widgets.ByteSeqControlPointWidget;
import alma.Control.device.gui.common.widgets.ByteSeqMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel contains the phase offset and phase sequence monitor and control points.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 7.0.0
 */

public class DebugWalshFunctionPanel extends DevicePanel {

    public DebugWalshFunctionPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanel(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanel(Logger logger, DevicePM aDevicePM) {
        phaseOffsetPanel = new PhaseOffsetPanel(logger, aDevicePM);
        phaseSequencePanel = new PhaseSequencePanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(phaseOffsetPanel, c);
        c.gridy++;
        add(phaseSequencePanel, c);

        setVisible(true);
    }

    /*
     * Display the phase offset monitor and control.
     */
    private class PhaseOffsetPanel extends DevicePanel{

        public PhaseOffsetPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Phase Offset"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            addLeftLabel("Current Phase Offset", c);
            add(new FloatMonitorPointWidget(logger,devicePM
                                    .getMonitorPointPM(IcdMonitorPoints.FR_PHASE_OFFSET), true, 6), c);
            c.gridy++;
            c.gridwidth=2;
            c.gridx--;
            c.anchor=GridBagConstraints.CENTER;
            //TODO: Make sure that the units and step sizes are OK
            add(new FloatControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.FR_PHASE_OFFSET), .008, 1000000,
                    "Change Phase Offset (\u03BCS)"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This panel displays the phase sequence control and read back.
     */
    private class PhaseSequencePanel extends DevicePanel {

        public PhaseSequencePanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Phase Sequence"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            c.gridheight=3;
            add(new ByteSeqControlPointWidget(logger,devicePM.
                    getControlPointPM(IcdControlPoints.FR_PHASE_SEQ_A), "Set A", 8), c);
            c.gridx++;
            add(new ByteSeqControlPointWidget(logger,devicePM.
                    getControlPointPM(IcdControlPoints.FR_PHASE_SEQ_B), "Set B", 8), c);
            c.gridx+=2;
            c.gridheight=1;
            add (new JLabel("Current read back:"), c);
            c.gridy++;
            addLeftLabel("A", c);
            add(new ByteSeqMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_PHASE_SEQ_A), ".", true), c);
            c.gridy++;
            addLeftLabel("B", c);
            add(new ByteSeqMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.FR_PHASE_SEQ_B), ".", true), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    private PhaseOffsetPanel phaseOffsetPanel;
    private PhaseSequencePanel phaseSequencePanel;
}

//
// O_o

