/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

package alma.Control.device.gui.DTX.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;

/** 
 * @author  dhunter
 * @version $Id$
 * @since    
 */

/**
 * This panel provides an interface to the 250MHz delay controls on the digitizer.
 */

public class Dg250MHzDelayPanel extends DevicePanel {

    public Dg250MHzDelayPanel(Logger logger, DevicePM dPM) {
        super(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }
    
    @Override
    protected void buildPanel() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("250 MHz Delay"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        c.fill=GridBagConstraints.NONE;
        add(bP, c);
        c.gridy++;
        add(cP, c);
    }
    
    protected void buildSubPanels(Logger logger, DevicePM dPM){
        bP = new ButtonPanel(logger, dPM);
        cP = new ControlPanel(logger, dPM);
    }
    
    private class ButtonPanel extends DevicePanel {
        public ButtonPanel (Logger logger, DevicePM dPM){
            super(logger, dPM);
            buildPanel();
        }
        
        protected void buildPanel() {
//            setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 0;
            c.gridy = 0;
            c.fill=GridBagConstraints.NONE;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_250MHZ_DELAY), "+0.25v", 0x02), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_250MHZ_DELAY), "+0.05v", 0x01), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_250MHZ_DELAY), "-0.05v", 0x11), c);
            c.gridx++;
            add (new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DG_250MHZ_DELAY), "-0.25v", 0x12), c);
        }
    }
    
    private class ControlPanel extends DevicePanel {
        public ControlPanel (Logger logger, DevicePM dPM){
            super(logger, dPM);
            buildPanel();
        }
        
        protected void buildPanel() {
//            setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 1;
            c.gridy = 0;
            c.fill=GridBagConstraints.NONE;
            addLeftLabel("Current Value:", c);
            add (new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DG_250MHZ_WD)), c);
            c.gridy++;
            addLeftLabel("Set Value:", c);
//            add (new IntegerControlPointWidget(logger,
//                    devicePM.getControlPointPM(IcdControlPoints.DG_250MHZ_WD)), c);
        }
    }
    
    private ButtonPanel bP;
    private ControlPanel cP;
    //TODO: serialVersionUID
    private static final long serialVersionUID = 1L;
    
}

