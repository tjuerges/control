/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

/**
 * <code>TestPatternControlWidget.java</code> This widget provides a method to set the test data
 * in a DTX using the SET_FR_CW_CHi control point.
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class TestPatternControlWidget extends JPanel implements ActionListener {
    
    /**Construct a new TestPatternControlWidget
     * 
     * @param logger the logger that all logs should be directed to.
     * @param controlPointModelD The control point for SET_DFR_XBAR ch D.
     * @param controlPointModelC The control point for SET_DFR_XBAR ch C.
     * @param controlPointModelB The control point for SET_DFR_XBAR ch B.
     * @param controlPointModelAll The control point for SET_DFR_XBAR all.
     */
    public TestPatternControlWidget( Logger logger,
            ControlPointPresentationModel<Integer> controlPointModelD,
            ControlPointPresentationModel<Integer> controlPointModelC,
            ControlPointPresentationModel<Integer> controlPointModelB,
            ControlPointPresentationModel<Integer> controlPointModelAll) {
        this.logger = logger;
        this.controlPointModelD = controlPointModelD;                //Save the control point models
        this.controlPointModelC = controlPointModelC;
        this.controlPointModelB = controlPointModelB;
        this.controlPointModelAll = controlPointModelAll;
        setD=new JButton(DLabel);                                   //Setup the Ch D button
        setD.addActionListener(this);
        controlPointModelD.getContainingDevicePM().addControl(setD);
        setC=new JButton(CLabel);                                   //Setup the Ch C button
        setC.addActionListener(this);
        controlPointModelC.getContainingDevicePM().addControl(setC);
        setB=new JButton(BLabel);                                   //Setup the Ch B button
        setB.addActionListener(this);
        controlPointModelB.getContainingDevicePM().addControl(setB);
        setAll=new JButton(AllLabel);                               //Setup the All channels button
        setAll.addActionListener(this);
        controlPointModelAll.getContainingDevicePM().addControl(setAll);
        
        paritySwitch=new JCheckBox("Enable Parity", true);  //Setup the swap switches
        controlPointModelAll.getContainingDevicePM().addControl(paritySwitch);
        scrambleCodeSwitch=new JCheckBox("Use Scramble Code", true);
        controlPointModelAll.getContainingDevicePM().addControl(scrambleCodeSwitch);
        
        payloadOptionMenu = new JComboBox();
        payloadOptionMenu.setEditable(false);
        for (String s: payloadOptionList) payloadOptionMenu.addItem(s);
        controlPointModelAll.getContainingDevicePM().addControl(payloadOptionMenu); 
        frameOptionMenu = new JComboBox();                              //Setup the optionMenu
        frameOptionMenu.setEditable(false);
        for (String s: frameOptionList) frameOptionMenu.addItem(s);
        controlPointModelAll.getContainingDevicePM().addControl(frameOptionMenu);
        buildWidget();
    }
    
    /** When an actionEvent occurs (e.g the control button is pressed) check the value in the
     *  menu. Find the corresponding code and send it to the control point.
     */
    public void actionPerformed(ActionEvent event) {
        int count=0;
        int payloadOptionCode=0;
        int frameOptionCode=0;
        int parityCode=0x8;
        int scrambleCode=0x80;
        int outCode;
        
        String newValue=(String)payloadOptionMenu.getSelectedItem();
        try{
            while (newValue != payloadOptionList[count])
                count++;
            payloadOptionCode=(payloadOptionCodes[count]);
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            logger.severe("ERROR: Lower bit text given by user not found in payloadOptionList!");
        }    
        newValue=(String)frameOptionMenu.getSelectedItem();
        try{
            while (newValue != frameOptionList[count])
                count++;
            frameOptionCode=(frameOptionCodes[count]);
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            logger.severe("ERROR: Lower bit text given by user not found in frameOptionList!");
        }    
        
        
        if (paritySwitch.isSelected())
            parityCode=0x0;
        if (scrambleCodeSwitch.isSelected())
            scrambleCode=0x0;
        
        outCode=payloadOptionCode | frameOptionCode | parityCode | scrambleCode;
        
        if (event.getActionCommand()==DLabel)
            controlPointModelD.setValue(outCode);
        if (event.getActionCommand()==CLabel)
            controlPointModelC.setValue(outCode);
        if (event.getActionCommand()==BLabel)
            controlPointModelB.setValue(outCode);
        if (event.getActionCommand()==AllLabel)
            controlPointModelAll.setValue(outCode);
    }
    
    /** Just add the option menu and control button to this panel.
     * @param buttonText The text to put on the control button.
     */
    private void buildWidget() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Set Test Data"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // Add all the points in a single column, starting at the top, with the labels on the left.
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth=4;
        add(payloadOptionMenu, c);
        c.gridy++;
        add(frameOptionMenu, c);
        c.gridwidth=1;
        c.gridy++;
        add(setD, c);
        c.gridx++;
        add(setC, c);
        c.gridx++;
        add(setB, c);
        c.gridx++;
        add(setAll, c);
        c.gridy++;
        c.gridx=0;
        c.gridwidth=2;
        add(paritySwitch, c);
        c.gridx+=2;
        add(scrambleCodeSwitch, c);
    }

    private String AllLabel="Set All";
    private String BLabel="Set B";
    private String CLabel="Set C";
    private ControlPointPresentationModel<Integer> controlPointModelD;
    private ControlPointPresentationModel<Integer> controlPointModelC;
    private ControlPointPresentationModel<Integer> controlPointModelB;
    private ControlPointPresentationModel<Integer> controlPointModelAll;
    private String DLabel="Set D";
    private int[] frameOptionCodes={0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80};
    private String[] frameOptionList={"Normal Frame", "Alternating 0x0000FFFF x4",
                                      "Alternating 0xFFFF0000 x4", "0xAAAA4444 x4", "All ones",
                                      "All Zeros", "0xCCCC x8", "0x3333 x8"};
    private JComboBox frameOptionMenu;
    private Logger logger;
    private JCheckBox paritySwitch;
    private int[] payloadOptionCodes={0, 1, 2, 3, 4, 5, 6, 7};
    private String[] payloadOptionList={"Normal payload from framebuilder", "All Zeros", "All Ones",
                                        "0xFFFF0000 x4", "0xCCCC x8", "0x3333 x8", "0xAAAA5555 x4",
                                        "F(cntr)E(cntr)D(cntr)...1(cntr)0(cntr)"};
    private JComboBox payloadOptionMenu;
    private static final long serialVersionUID = 1L;
    private JButton setAll;
    private JButton setB;
    private JButton setC;
    private JButton setD;
    private JCheckBox scrambleCodeSwitch;
}
