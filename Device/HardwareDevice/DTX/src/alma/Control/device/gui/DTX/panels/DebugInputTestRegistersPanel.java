/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IcdControlPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.DTX.widgets.InputTestControlWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel provides access and control of the formater input test registers.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 5.0.3
 */

public class DebugInputTestRegistersPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DebugInputTestRegistersPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        inputTestRegistersMonitorPanel = new InputTestRegistersMonitorPanel(logger, aDevicePM);
        inputTestRegistersControlPanel = new InputTestRegistersControlPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
//        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(inputTestRegistersMonitorPanel, c);
        c.gridy++;
        add(inputTestRegistersControlPanel, c);

        setVisible(true);
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class InputTestRegistersMonitorPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public InputTestRegistersMonitorPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            String clockEdgeOptions[] = {"  Normal Mode",
                                         "Clock BB0 at falling edge, BB1 at rising.",
                                         "Clock BB0 at rising edge, BB0 at falling.",
                                         "Clock both BBs at falling edge"};
            String testModeOptions[] = {"Normal Mode",
                                        "All input data equals 0 for 500\u03BCs after TE",
                                        "Sample 0 of BB0 and BB1 equals 0 for 1 frame after TE",
                                        "All input data equals 1",
                                        "All input data equals 0"};
            int testModeCodes[] = {0x00, 0x10, 0x20, 0x30, 0x40};
            addLeftLabel("Channel D", c);
            c.gridx++;
            c.gridy++;
            addLeftLabel("Clock Edge:", c);
            add (new CodeToTextMonitorPointWidget(logger,
                       devicePM.getMonitorPointPM(IcdMonitorPoints.FR_INPUT_TEST_CH1),
                       clockEdgeOptions, "Error", 0x0F, null), c);
            c.gridy++;
            addLeftLabel("Test Mode:", c);
            add (new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_INPUT_TEST_CH1),
                    testModeOptions, "Error", 0xF0, testModeCodes), c);
            c.gridy++;
            addLeftLabel (" ", c);
            c.gridy++;
            c.gridx--;
            addLeftLabel("Channel C:", c);
            c.gridx++;
            c.gridy++;
            addLeftLabel("Clock Edge:", c);
            add (new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_INPUT_TEST_CH2),
                    clockEdgeOptions, "Error", 0x0F, null), c);
            c.gridy++;
            addLeftLabel("Test Mode:", c);
            add (new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_INPUT_TEST_CH2),
                    testModeOptions, "Error", 0xF0, testModeCodes), c);
            c.gridy++;
            c.gridx--;
            addLeftLabel("Channel B", c);
            c.gridx++;
            c.gridy++;
            addLeftLabel("Clock Edge:", c);
            add (new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_INPUT_TEST_CH3),
                    clockEdgeOptions, "Error", 0x0F, null), c);
            c.gridy++;
            addLeftLabel("Test Mode:", c);
            add (new CodeToTextMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.FR_INPUT_TEST_CH3),
                    testModeOptions, "Error", 0xF0, testModeCodes), c);
        }
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class InputTestRegistersControlPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public InputTestRegistersControlPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            add (new InputTestControlWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.FR_INPUT_TEST_CH1),
                    devicePM.getControlPointPM(IcdControlPoints.FR_INPUT_TEST_CH2),                    
                    devicePM.getControlPointPM(IcdControlPoints.FR_INPUT_TEST_CH3),
                    devicePM.getControlPointPM(IcdControlPoints.FR_INPUT_TEST_ALL)), c);
        }
    }
    
    private InputTestRegistersMonitorPanel inputTestRegistersMonitorPanel;
    private InputTestRegistersControlPanel inputTestRegistersControlPanel;
}

//
// O_o

