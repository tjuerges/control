/*
ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2010 
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*/

package alma.Control.device.gui.DTX.widgets;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.*;

import alma.Control.device.gui.DTX.panels.Dg250MHzDelayPanel;
import alma.Control.device.gui.DTX.panels.DgReferenceVoltagePanelP0;
import alma.Control.device.gui.DTX.panels.DgReferenceVoltagePanelP1;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.DTX.widgets.util.ErrorGrapher;
import alma.Control.device.gui.DTX.widgets.util.FftGrapher;
import alma.Control.device.gui.DTX.widgets.util.PdfGrapher;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.util.DtsData.DataPacket;
import alma.Control.device.gui.common.widgets.util.DtsData.DataStatistics;
import alma.Control.device.gui.common.widgets.util.DtsData.IDataGrabber;

/** 
 * @author  dhunter
 * @version $Id$
 * @since    
 */

/**
 * This widget provides a tuning interface for the digitizer (DG) sub-module of the DTX. This tuner consists
 * of 4 parts: an error constellation, a PDF graph, a FFT spectrum, and a control panel. There are also some
 * controls for adjusting the 3 graphs. Each component is explained in its respective class.
 */


public class DgTunner extends JPanel {
    public DgTunner(Logger newLogger, DevicePM aDevicePM, IDataGrabber newDataGrabber){
        devicePM = aDevicePM;
        logger=newLogger;
        dataGrabber = newDataGrabber;
        errorConstellation = new ErrorGrapher(logger, aDevicePM);
        pdfGrapher = new PdfGrapher(logger, aDevicePM);
        controlPanel = new DgControlPanel(logger, aDevicePM);
        delayPanel = new Dg250MHzDelayPanel(logger, aDevicePM);
        dgReferenceVoltagePanelP0 = new DgReferenceVoltagePanelP0(logger, aDevicePM);
        dgReferenceVoltagePanelP1 = new DgReferenceVoltagePanelP1(logger, aDevicePM);
        fftGraph = new FftGrapher(logger, aDevicePM);
        okButton = new JButton("OK");
        okButton.addActionListener(new OkButtonAction());
        aDevicePM.addControl(okButton);
        buildWidget();
    }
    
    protected void buildWidget(){
        removeAll();
        if (showWarning)
            buildWarningWidget();
        else
            buildRealWidget();
        revalidate();
    }
    
    /**
     * Display a warning and some instructions about the use of the DG tuner. The user can 
     * acknowledge the warning and get to the tuner by clicking OK.
     * TODO: insert link to documentation.
     */
    private void buildWarningWidget(){
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
//        c.fill=GridBagConstraints.HORIZONTAL;
        c.weightx=1;
        JLabel l = new JLabel("WARNING:");
        l.setForeground(Color.RED);
        add(l, c);
        c.gridy++;
        JTextArea warning = new JTextArea("The DG tunner can drastically effect the functioning " +
                "of the digitizer. Missuse of the tunner will result in corrupted data going to " +
                "the corrleator. In addition, the tunner will substantially increase the CAN " +
                "traffic. It should only be used on an antennas of which is not in use for " +
                "observing.");
        warning.setLineWrap(true);
        warning.setEditable(false);
        warning.setColumns(35);
        warning.setWrapStyleWord(true);
        add (warning, c);
        c.gridy++;
        add (new JLabel(""), c);
        c.gridy++;
        add (new JLabel("Use:"), c);
        c.gridy++;
        JTextArea use = new JTextArea("Please see the documentation for usage instructions. Do not " +
                 "use without a full understanding of how the tunner should be used.");
        use.setLineWrap(true);
        use.setEditable(false);
        use.setColumns(35);
        use.setWrapStyleWord(true);
        add (use, c);
        c.gridy++;
        add(new JLabel("Note:"), c);
        c.gridy++;
        JTextArea note = new JTextArea("This gui requires a large window. If the graphs are " +
                 "improperly drawn you should enlarge the window.");
        note.setLineWrap(true);
        note.setEditable(false);
        note.setColumns(35);
        note.setWrapStyleWord(true);
        add(note, c);
        c.gridy++;
        c.weightx=0;
//        c.fill=GridBagConstraints.NONE;
        add (okButton, c);
    }
    
    private void buildRealWidget() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Digitizer Tunning"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add (pdfGrapher, c);
        c.gridy++;
        c.weighty=40;
        c.gridheight=2;
        add (errorConstellation, c);
        c.gridx=1;
        c.gridy=0;
        c.weightx=0;
        c.weighty=100;
        c.gridwidth=2;
        c.gridheight=1;
        add (fftGraph, c);
        c.weighty=0;
        c.gridy++;
        c.fill=GridBagConstraints.NONE;
        c.gridwidth=1;
        add (controlPanel, c);
        c.gridx++;
        add (delayPanel, c);
        c.gridx--;
        c.gridy++;
        c.gridwidth=2;
        add (dgReferenceVoltagePanelP1, c);
        dgReferenceLocation = (GridBagConstraints) c.clone();
        setVisible(true);
    }
    
    private class runGraphs implements Runnable, ActionListener{
        public void run(){
            DataPacket d;
            DataStatistics s;
            devicePM.addShutdownAction(this);
            while(running){
                d = new DataPacket();
                dataGrabber.captureData(d, (Integer)numSamples.getValue(), 0);
                //Select the proper polarity
                if (polarity0.isSelected())
                    s = new DataStatistics(d, false);
                else
                    s = new DataStatistics(d, true);
                pdfGrapher.resetPdf(s);
                errorConstellation.resetConstellation(s);
                fftGraph.resetFft(d, polarity1.isSelected());
            }
            
            devicePM.removeShutdownAction(this);
        }

        //If the GUI is closed we need to do some clean up. This action is registered
        //with the device PM so that it will be called to do the needed cleanup.
        @Override
        public void actionPerformed(ActionEvent e) {
            running=false;
            dataGrabber.shutdown();
        }
    }

    private class ChangePolarityAction implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if (polarity0.isSelected()){
                remove(dgReferenceVoltagePanelP1);
                add(dgReferenceVoltagePanelP0, dgReferenceLocation);
            }
            else{
                remove(dgReferenceVoltagePanelP0);
                add(dgReferenceVoltagePanelP1, dgReferenceLocation);
            }
            revalidate();
            repaint();  //Yes, things really don't work without this!
        }
    }

    /*
     * This class provides all the controls for tuning the DG.
     */
    private class DgControlPanel extends DevicePanel {

        public DgControlPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            startStop = new JButton("Start");
            dPM.addControl(startStop);
            numSamples = new JSpinner(new SpinnerNumberModel(5, 1, 500, 1));
            startStop.addActionListener(new StartStopAction());
            autoZoom = new JCheckBox("Auto Zoom");
            autoZoom.addActionListener(new AutoZoomAction());
            buildPanel();
        }
        
        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.anchor = GridBagConstraints.CENTER;
            c.gridx = 0;
            c.gridy = 0;
            c.fill=GridBagConstraints.NONE;
            ButtonGroup buttons = new ButtonGroup();
            polarity0 = new JRadioButton("Polarity 0", false);
            polarity1 = new JRadioButton("Polarity 1", true);
            buttons.add(polarity0);
            buttons.add(polarity1);
            //The action only happens when a button is clicked. Being radio buttons, these buttons can
            //change value even when they are not clicked. So we add actions to both of them.
            polarity0.addActionListener(new ChangePolarityAction());
            polarity1.addActionListener(new ChangePolarityAction());
            add(polarity0, c);
            c.gridy++;
            add(polarity1, c);
            c.gridy++;
            add (startStop, c);
            c.gridy++;
            add (autoZoom, c);
            c.gridy++;
            add (new JLabel("# of reads:"), c);
            c.gridy++;
            add (numSamples, c);
            setVisible(true);
        }
        
        private class AutoZoomAction implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
               errorConstellation.setAutoZoom(autoZoom.isSelected());
            }
        }
        
        private class StartStopAction implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (running){
                    running=false;
                    dataGrabber.shutdown();
                    startStop.setText("Start");
                }
                else{
                    running=true;
                    Thread t = new Thread(new runGraphs());
                    t.start();
                    startStop.setText("Stop");
                }
            }
        }
        private JCheckBox autoZoom;
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
        private JButton startStop;
    }
    
    private class OkButtonAction implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            showWarning=false;
            buildWidget();
        }
        
    }

    private DgControlPanel controlPanel;
    private IDataGrabber dataGrabber;
    private Dg250MHzDelayPanel delayPanel;
    private DevicePM devicePM;
    //We use this when we switch out between polarities.
    private GridBagConstraints dgReferenceLocation;
    private DgReferenceVoltagePanelP0 dgReferenceVoltagePanelP0;
    private DgReferenceVoltagePanelP1 dgReferenceVoltagePanelP1;
    private ErrorGrapher errorConstellation;
    private FftGrapher fftGraph;
    private Logger logger;
    private JSpinner numSamples;
    private JButton okButton;
    private PdfGrapher pdfGrapher;
    //TODO: make these buttons actually do something!
    private JRadioButton polarity0;
    private JRadioButton polarity1;
    private boolean running = false;
    //TODO: serialVersionUID
    private static final long serialVersionUID = 1L;
    private boolean showWarning = true;
}
