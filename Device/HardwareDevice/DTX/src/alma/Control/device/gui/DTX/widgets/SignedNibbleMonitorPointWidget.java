/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.StatusColor;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 *        SignedNibbleMonitorPointWidget offers a JPanel containing a textual
 *        representation of an integer (in hex or dec format) from a monitor
 *        point with range and communication verification/alarms.
 *        
 *        This monitor point assumes that the given integer contains only 1 nibble of data, of which
 *        is a signed 2s complement value. It examens the data and sign extends it if needed.
 *        
 *        This widget is a work around for a bug in the DTX spreadsheet (COMP-3672). All instances
 *        of this widget should be replaced with a normal int widget when that ticket is resolved.
 * 
 */
public class SignedNibbleMonitorPointWidget extends MonitorPointWidget {

    /**Construct a new SignedNibbleMonitorPointWidget
     * @param logger The logger that all logs should be written to.
     * @param monitorPointModel The monitor point to display.
     */
    public SignedNibbleMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel) {
        super(logger, true);
        monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));
        toolTipText = getOperatingRangeToolTipText(monitorPointModel);

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0");
        if (monitorPoint.supportsRangeChecking())
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        else
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setToolTipText(toolTipText);
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
        if (q.booleanValue()) {
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        } else {
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        }
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        int outValue=(Integer)getCachedValue(monitorPoint);
        if (outValue>7)            //Do a sign extend if needed
            outValue=outValue|0xFFFFFFF0;
        String newValue="";
        newValue=String.format("%d", outValue);
        widgetLabel.setText(newValue);
    }

    private IMonitorPoint monitorPoint;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private String toolTipText;
    private JLabel widgetLabel;
}

//
// O_o
