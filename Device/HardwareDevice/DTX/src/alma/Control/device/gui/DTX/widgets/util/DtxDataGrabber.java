/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets.util;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.util.DtsData.DataPacket;
import alma.Control.device.gui.common.widgets.util.DtsData.IDataGrabber;

import java.awt.EventQueue;
import java.util.logging.Logger;
import javax.swing.JProgressBar;

/**
 * @author David Hunter     dhunter@nrao.edu
 * @version 
 * @since ALMA 7.0.1
 * 
 * DataGrabber provides a widget that reads from the DTX & DRX I2C interface using the
 * GET_I2C_DATA as specified in the ICD for said devices. 
 */

/* We extend the monitor point widget because this is the place that the monitor points are checked. And we
 * use some of the inherited classes to do that. This is not a traditional monitor point because it is never
 * displayed.
 */
public class DtxDataGrabber extends MonitorPointWidget implements IDataGrabber {
    
    /**
     * Creates an I2C interface widget attached to the given monitor points in the given device.
     * @param logger
     * @param dp
     * @param payloadDOlder
     * @param payloadDNewer
     * @param payloadCOlder
     * @param payloadCNewer
     * @param payloadBOlder
     * @param payloadBNewer
     * @param payloadStatus,
     * @param captureDataControl
     * 
     * All of the monitor points must be connected to the proper points in the DTX or DRX. 
     */
    public DtxDataGrabber(Logger logger, DevicePresentationModel dp,
          MonitorPointPresentationModel<int[]> payloadDOlder, MonitorPointPresentationModel<int[]> payloadDNewer,
          MonitorPointPresentationModel<int[]> payloadCOlder, MonitorPointPresentationModel<int[]> payloadCNewer,
          MonitorPointPresentationModel<int[]> payloadBOlder, MonitorPointPresentationModel<int[]> payloadBNewer,
          MonitorPointPresentationModel<Integer> payloadStatus,
          ControlPointPresentationModel<Boolean> captureDataControl){
        super(logger, true);
        
        //Setup D bits
        getLowerBitsD = addMonitorPointPM(payloadDOlder, new int[8]);
        dp.moveMonitorPointToIdleGroup(getLowerBitsD);
        getUpperBitsD = addMonitorPointPM(payloadDNewer, new int[8]);
        dp.moveMonitorPointToIdleGroup(getUpperBitsD);
        
        //Setup C bits
        getLowerBitsC = addMonitorPointPM(payloadCOlder, new int[8]);
        dp.moveMonitorPointToIdleGroup(getLowerBitsC);
        getUpperBitsC = addMonitorPointPM(payloadCNewer, new int[8]);
        dp.moveMonitorPointToIdleGroup(getUpperBitsC);
        
        //Setup B bits
        getLowerBitsB = addMonitorPointPM(payloadBOlder, new int[8]);
        dp.moveMonitorPointToIdleGroup(getLowerBitsB);
        getUpperBitsB = addMonitorPointPM(payloadBNewer, new int[8]);
        dp.moveMonitorPointToIdleGroup(getUpperBitsB);
        
        //Setup the rest.
        getStatus=addMonitorPointPM(payloadStatus, Integer.valueOf(0));
        dp.moveMonitorPointToIdleGroup(getStatus);
        devicePresentationModel=dp;
        this.captureData = captureDataControl;
    }
    /*
     * Required by parent class. Unused because this point has no range checking.
     * @see alma.Control.device.gui.common.widgets.MonitorPointWidget#updateAttention(java.lang.Boolean)
     */
    @Override
    public void updateAttention(Boolean value) {
        //Not applicable to this widget
    }

    /**Required by parent class; unused because this widget only reads upon request.
     * @see alma.Control.device.gui.common.widgets.MonitorPointWidget#updateValue(alma.Control.device.gui.common.IMonitorPoint)
     */
    @Override
    public void updateValue(IMonitorPoint source) {
        // Nothing to do in this case.
    }
    
    /**
     * Captures the given number of data chunks from the device FIFO and puts it in the given DataPacket. 
     * Watches for FIFO empty and returns the % of the requested that that was actually captured.
     * 
     * WARNING: This function should never be run in the swing dispatch thread. It can take anywhere from
     * less than a second to tens of minutes to run. It will dispatch updates to the progress bar so that
     * the user knows something is going on.
     * 
     * The FIFO empty alarm is monitored, and the function halts if the FIFO becomes empty. The data that was
     * successfully captured will still be available for use.
     * 
     * Data capture: the data is captured in 384 bit collections. This amounts to 3 channels with 128 bits
     * per channel. 2 can transactions are taken per channel (64 bits per transaction).
     * 
     * Can traffic: this function can generate a lot of traffic on the CAN bus. Currently it is limited to
     * 7 transactions a TE.
     * 
     * @param Amount of dataRequested, where 1 = 128 bits per channel (1 set of low & high bits, 3 channels)
     * @return % of requested data that was successfully captured.
     */
    public int captureData(DataPacket bagOData, int dataRequested, int captureLocation){
        stopCapture=false;
        long dLow, dHigh, cLow, cHigh, bLow, bHigh, l;
        int[] dLowBytes, dHighBytes, cLowBytes, cHighBytes, bLowBytes, bHighBytes;
        
        //Capture some fresh data
        if(captureLocation==0)
            captureData.setValue(false);
        else
            captureData.setValue(true);
        //Sleep 1 TE to let the data capture command go through
        try {
            Thread.sleep(48);
        } catch (InterruptedException e) {
            logger.severe("DataGrabber, captureData Error: failed to sleep 48ms after sending " +
                          "the fill FIFO function.");
        }
        for (int count=0; count<dataRequested; count++){
            //If the fifo is empty there is no more data to get, so we stop the capture and return.
            //And if the stopCapture flag has been set we stop the capture and return.
            if (fifoIsEmpty() || stopCapture)
                return 100*count/dataRequested;
            //Request a poll of each monitor point.
            devicePresentationModel.manuallyPollIdleMonitorPoint(getLowerBitsD);
            devicePresentationModel.manuallyPollIdleMonitorPoint(getUpperBitsD);
            devicePresentationModel.manuallyPollIdleMonitorPoint(getLowerBitsC);
            devicePresentationModel.manuallyPollIdleMonitorPoint(getUpperBitsC);
            devicePresentationModel.manuallyPollIdleMonitorPoint(getLowerBitsB);
            devicePresentationModel.manuallyPollIdleMonitorPoint(getUpperBitsB);
            if (captureProgressBar != null){
                final int newBarSize = 100*count/dataRequested;
                EventQueue.invokeLater(new Runnable(){
                    @Override
                    public void run() {captureProgressBar.setValue(newBarSize);}
                });
            }
            //Sleep 1 TE to limit the can traffic.
            try {
                Thread.sleep(48);
            } catch (InterruptedException e) {
                logger.severe("DataGrabber, captureData Error: failed to sleep 48ms between reads.");
            }
            //get the data
            dLowBytes=(int[])getCachedValue(getLowerBitsD);
            dHighBytes=(int[])getCachedValue(getUpperBitsD);
            cLowBytes=(int[])getCachedValue(getLowerBitsC);
            cHighBytes=(int[])getCachedValue(getUpperBitsC);
            bLowBytes=(int[])getCachedValue(getLowerBitsB);
            bHighBytes=(int[])getCachedValue(getUpperBitsB);
            //Reset the conversion variables
            dLow=0;
            dHigh=0;
            cLow=0;
            cHigh=0;
            bLow=0;
            bHigh=0;
            //Convert the data into longs
            for (int c=0; c<8; c++){
                l=dLowBytes[c];
                dLow |= l<<(c*8);
                l=dHighBytes[c];
                dHigh |= l<<(c*8);
                l=cLowBytes[c];
                cLow |= l<<(c*8);
                l=cHighBytes[c];
                cHigh |= l<<(c*8);
                l=bLowBytes[c];
                bLow |= l<<(c*8);
                l=bHighBytes[c];
                bHigh |= l<<(c*8);
            }
            //Save the cached data from the prior poll request.
            bagOData.addRawData(dLow, dHigh, cLow, cHigh, bLow, bHigh);
        }
        return 100;
    }
    
    /**
     * This allows the user to provide a progress bar. If one is given, it will be updated with the % complete
     * when captureData(..) is running. If a new one is given it will replace the old one. If null is given
     * then no progress bar will be used. Note that when the constructor runs the progress bar defaults to
     * none.
     * 
     * The progress bar will be updated via EventQueue.invokeLater, so it should be thread safe.
     * 
     * @param newProgressBar The progress bar to update when data is being captures.
     */
    public void setProgressBar(JProgressBar newProgressBar){
        captureProgressBar=newProgressBar;
    }
    
    @Override
    public void shutdown() {
        stopCapture=true;
    }
    /**
     * Check if any of the device data capture FIFOs is empty.
     * Precondition: the getStatus monitor point is in the idle group.
     * @return true if the any of the 3 FIFOs are empty, false if it is not.
     */
    private boolean fifoIsEmpty(){
        devicePresentationModel.manuallyPollIdleMonitorPoint(getStatus);
        int status = (Integer)getCachedValue(getStatus);
        //And out everything except the FIFO empty alarms.
        if ((status & 0x15) > 0)
            return true;
        else
            return false;
    }

    
    //All sorts of variables, alphabetized by last name.
    private ControlPointPresentationModel<Boolean> captureData;
    private JProgressBar captureProgressBar=null;
    private IMonitorPoint getLowerBitsB;
    private IMonitorPoint getLowerBitsC;
    private IMonitorPoint getLowerBitsD;
    private IMonitorPoint getStatus;
    private IMonitorPoint getUpperBitsB;
    private IMonitorPoint getUpperBitsC;
    private IMonitorPoint getUpperBitsD;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private boolean stopCapture;
    DevicePresentationModel devicePresentationModel;  //We need this for manually polling monitor points.
}

//
// O_o
