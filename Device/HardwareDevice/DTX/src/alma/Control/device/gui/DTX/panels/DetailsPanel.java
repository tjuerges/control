/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.panels;

import alma.Control.device.gui.DTX.IdlMonitorPoints;
import alma.Control.device.gui.DTX.IcdMonitorPoints;
import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized in panels and tabs to group related data together. 
 * 
 * This class contains the Detail data. It also provides a Show/Hide button for the data.
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DetailsPanel extends DevicePanel {

    public DetailsPanel(Logger logger, DevicePM aDevicePM) {
    	super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        formatterPanel = new FormatterPanel(logger, aDevicePM);
        digitizerPanel = new DigitizerPanel(logger, aDevicePM);
        halfTransponderPanel = new HalfTransponderPanel(logger, aDevicePM);
        debugPanel = new DebugPanel(logger, aDevicePM);
        deviceInfoPanel = new DeviceInfoPanel(logger, aDevicePM);
    }

    protected void buildPanel()
    {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"),
                    BorderFactory.createEmptyBorder(1,1,1,1)));

        customizeMonitorPoints();
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weighty=1;
        c.weightx=100;
        c.fill=GridBagConstraints.BOTH;
        c.anchor=GridBagConstraints.NORTHWEST;
        add(new ShowHideButton(), c);
        c.weightx=100;
        c.weighty=100;
        c.gridy++;
        c.fill = GridBagConstraints.BOTH;
        c.anchor=GridBagConstraints.CENTER;
        tabs = new JTabbedPane();
        tabs.add("Formatter", new JScrollPane(formatterPanel));
        tabs.add("Digitizer", new JScrollPane(digitizerPanel));
        tabs.add("Half Transponder", new JScrollPane(halfTransponderPanel));
        tabs.add("Debug", debugPanel);
        tabs.add("Device Info", new JScrollPane(deviceInfoPanel));
        c.gridy++;
        add(tabs, c);

        this.setVisible(true);
    }
//TODO: Check the offset stuff on TTX_LASER_TMP.
    private void customizeMonitorPoints () {
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.DG_TEMP).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.DG_TEMP).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_FR).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_FR).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH1).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH1).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH2).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH2).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH3).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_TMP_CH3).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX1).getMonitorPoint().setDisplayScale(1000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX1).getMonitorPoint().setDisplayUnits("mA");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX2).getMonitorPoint().setDisplayScale(1000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX2).getMonitorPoint().setDisplayUnits("mA");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX3).getMonitorPoint().setDisplayScale(1000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_LASER_BIAS_TTX3).getMonitorPoint().setDisplayUnits("mA");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH1).getMonitorPoint().setDisplayScale(1000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH1).getMonitorPoint().setDisplayUnits("\u03BCA");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH1).getMonitorPoint().setDisplayScale(1000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH1).getMonitorPoint().setDisplayUnits("\u03BCW");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH2).getMonitorPoint().setDisplayScale(1000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH2).getMonitorPoint().setDisplayUnits("\u03BCA");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH2).getMonitorPoint().setDisplayScale(1000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH2).getMonitorPoint().setDisplayUnits("\u03BCW");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH3).getMonitorPoint().setDisplayScale(1000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_BIAS_CH3).getMonitorPoint().setDisplayUnits("\u03BCA");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH3).getMonitorPoint().setDisplayScale(1000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TTX_LASER_PWR_CH3).getMonitorPoint().setDisplayUnits("\u03BCW");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PHASE_OFFSET).getMonitorPoint().setDisplayScale(1000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_PHASE_OFFSET).getMonitorPoint().setDisplayUnits("ms");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX1).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX1).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX2).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX2).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX3).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IcdMonitorPoints.FR_TMP_TTX3).getMonitorPoint().setDisplayUnits("\u00B0C");
    }
    
    private class ShowHideButton extends JButton implements ActionListener {

        private String hideActionCommand;
        private String hideButtonText;
        private String showActionCommand;
        private String showButtonText;

        public ShowHideButton() {
            hideButtonText = "Hide Details";
            showButtonText = "Show Details";
            hideActionCommand = "HIDE_DETAILS";
            showActionCommand = "SHOW_DETAILS";

            setText(hideButtonText);
            setActionCommand(hideActionCommand);
            addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == hideActionCommand) {
                tabs.setVisible(false);
                setText(showButtonText);
                setActionCommand(showActionCommand);
            } else if (e.getActionCommand() == showActionCommand) {
                tabs.setVisible(true);
                setText(hideButtonText);
                setActionCommand(hideActionCommand);
            }
        }
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    private JTabbedPane tabs;

    private FormatterPanel formatterPanel;
    private DigitizerPanel digitizerPanel;
    private HalfTransponderPanel halfTransponderPanel;
    private DebugPanel debugPanel;
    private DeviceInfoPanel deviceInfoPanel;

}

//
// O_o
