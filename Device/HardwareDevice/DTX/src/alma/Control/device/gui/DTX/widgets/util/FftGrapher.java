/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DTX.widgets.util;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis.AxisTitle;
import info.monitorenter.gui.chart.traces.Trace2DSimple;

import java.awt.Color;
import java.awt.BorderLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import alma.Control.device.gui.DTX.presentationModels.DevicePM;
import alma.Control.device.gui.common.widgets.util.DtsData.DataPacket;
import alma.Control.device.gui.common.widgets.util.DtsData.InsufficientDataException;

/**
 * This class takes an FFT of the given data sample and graphs it.
 * 
 * Used for tuning the digitizer (which is part of the DTX module).
 * 
 * @author David Hunter dhunter@nrao.edu
 * @version
 * @since ALMA 7.1.0
 * 
 */
public class FftGrapher extends JPanel {
    /**
     * Construct a new FftGrapher.
     * @param logger
     */
    public FftGrapher(Logger newLogger, DevicePM dPM) {
        logger=newLogger;
//        data = new Trace2DSimple();
//        devicePM = dPM;
//        chart=new Chart2D();
//        devicePM.addChartToStop(chart);
//        chart.setPaintLabels(false);
//        chart.getAxisX().setAxisTitle(new AxisTitle("Frequency (GHz)"));
//        chart.getAxisY().setAxisTitle(new AxisTitle("Amplitude"));
        buildWidget();
    }

    /**
     * Replace the current FFT trace with a new one.
     * TODO: Do we need to call revalidate, and is JChart2D thread safe?
     * @param newData
     * @param polarity The polarity to display (false for 0, true for 1)
     * 
     * TODO: Uncomment and fully update code when we have a way to do the FFT.
     */
    public void resetFft(DataPacket newData, boolean polarity){
//        byte[] dataToGraph = null;
//        //Get the data to graph
//        try {
//            dataToGraph = newData.getAlmaValues1DPol0();
//        } catch (InsufficientDataException e) {
//            logger.severe("FftGrapher, resetFft: Error: insufficent data in given DataStatistis!");
//            return;
//        }
//        double[] fftOfData = new double[dataToGraph.length];
//        //Do FFT
//        doFft(dataToGraph, fftOfData, dataToGraph.length);
//        
//        //Graph data
//        data.setColor(DEFAULT_COLOR);
//        data.removeAllPoints();
//        for (int c=0; c<fftOfData.length; c++)
//            data.addPoint(c, fftOfData[c]);
//        revalidate();
    }
    
    protected void buildWidget() {
        
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("FFT Spectrum"),
                BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new BorderLayout());
//        add(chart);
        add(new JLabel("The FFT spectrum is not yet functional."));
    }

    /**
     * JNI interface deceleration for the FFT function.
     * There needs to be a corresponding C function that implements the FFT using FFTW.
     * It should be located at CONTROL/Device/HardwareDevice/DTX/src/fft.c
     * @param arr
     * @param arrrg
     * @return
     */
    //Problem: JNI has been causing problems in AlmaSW, so we should not use it...
    //TODO: Come up with another way to do the FFT.
//    private native int doFft(byte[] arr, double[] arrrg, int size);
    
    private Chart2D chart;
    //These values will be used by the child classes of this class.
    private Trace2DSimple data;
    private DevicePM devicePM;
    private Logger logger;
    private static final Color DEFAULT_COLOR = Color.BLACK;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
