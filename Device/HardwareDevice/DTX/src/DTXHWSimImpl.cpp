
/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File DTXHWSimImpl.cpp
 *
 * $Id$
 */

#include "DTXHWSimImpl.h"
#include <LogToAudience.h>

using namespace AMB;

/* Please use this class to implement complex functionality for the
 * DTXHWSimBase helper functions. Use AMB::TypeConvertion methods
 * for convenient type convertions. */

DTXHWSimImpl::DTXHWSimImpl(node_t node, const std::vector<CAN::byte_t>& serialNumber)
	: DTXHWSimBase::DTXHWSimBase(node, serialNumber)
{
    setDefaults();
}

void DTXHWSimImpl::setDefaults()
{
     if (state_m.find(monitorPoint_TTX_ALARM_STATUS) != state_m.end()) {
        unsigned char raw[6] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
        std::vector<CAN::byte_t> defVal(raw, raw + sizeof(raw)/sizeof(*raw));
        *(state_m.find(monitorPoint_TTX_ALARM_STATUS)->second) = defVal;
     } else
        throw CAN::Error("Trying to set monitorPoint_TTX_ALARM_STATUS. Member not found.");
}

void DTXHWSimImpl::setControlSetDgPsOnOff(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_DG_PS_ON_OFF");
    if (state_m.find(controlPoint_DG_PS_ON_OFF) != state_m.end())
        *(state_m.find(controlPoint_DG_PS_ON_OFF)->second) = data;
    else
        throw CAN::Error("Trying to set controlPoint_DG_PS_ON_OFF. Member not found.");
    // setting data value also to getter
    if (state_m.find(monitorPoint_DG_PS_ON_OFF) != state_m.end())
        *(state_m.find(monitorPoint_DG_PS_ON_OFF)->second) = data;
    else
        throw CAN::Error("Trying to set monitorPoint_DG_PS_ON_OFF. Member not found.");
        
}

void DTXHWSimImpl::setControlFrResetAll(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_FR_RESET_ALL");
    if (state_m.find(controlPoint_FR_RESET_ALL) != state_m.end())
        *(state_m.find(controlPoint_FR_RESET_ALL)->second) = data;
    else
        throw CAN::Error("Trying to set controlPoint_FR_RESET_ALL. Member not found.");
    // after a FULL RESET we set FR_STATUS to 0xFF80
    unsigned char fullResetData = 0xF0;
    if (state_m.find(monitorPoint_FR_STATUS) != state_m.end()){
      if(data[0]==fullResetData){
        unsigned char aux[2] = {0xFF,0x80};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
        *(state_m.find(monitorPoint_FR_STATUS)->second) = result;
      }
    } else
        throw CAN::Error("Trying to set monitorPoint_FR_STATUS. Member not found.");
    // after a FULL RESET we set FR_TE_STATUS to 0xF0000000
    if (state_m.find(monitorPoint_FR_TE_STATUS) != state_m.end()){
      if (data[0]==fullResetData){
        unsigned char aux[4] = {0xF0,0x00,0x00,0x00};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
        *(state_m.find(monitorPoint_FR_TE_STATUS)->second) = result;
      }
    } else
        throw CAN::Error("Trying to set monitorPoint_FR_TE_STATUS. Member not found.");
}

void DTXHWSimImpl::setControlTtxLaserEnable(const std::vector<CAN::byte_t>& data)
{
    checkSize(data,1,"controlPoint_TTX_LASER_ENABLE");
    if (state_m.find(controlPoint_TTX_LASER_ENABLE) != state_m.end())
        *(state_m.find(controlPoint_TTX_LASER_ENABLE)->second) = data;
    else
        throw CAN::Error("Trying to set controlPoint_TTX_LASER_ENABLE. Member not found.");
    // after Enabling Lasers we set FR_STATUS to 0xFFFF
    unsigned char enableLasersData = 0x7;
    unsigned char disableLasersData = 0x0;
    
    if (state_m.find(monitorPoint_FR_STATUS) != state_m.end()){
      if (data[0]==enableLasersData){
        unsigned char aux[2] = {0xFF,0xFF};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
        *(state_m.find(monitorPoint_FR_STATUS)->second) = result;
      }
      else if (data[0]==disableLasersData){
        unsigned char aux[2] = {0xFF,0x80};
        std::vector<CAN::byte_t> result(aux, aux + sizeof(aux)/sizeof(*aux));
        *(state_m.find(monitorPoint_FR_STATUS)->second) = result;
      }
    } else
        throw CAN::Error("Trying to set monitorPoint_FR_STATUS. Member not found.");
}

void DTXHWSimImpl::setControlSetFrPhaseSeqA(const std::vector<unsigned char>& world)
{
   associate(monitorPoint_FR_PHASE_SEQ_A, world);
   DTXHWSimBase::setControlSetFrPhaseSeqA(world);
}

void DTXHWSimImpl::setControlSetFrPhaseSeqB(const std::vector<unsigned char>& world)
{
   associate(monitorPoint_FR_PHASE_SEQ_B, world);
   DTXHWSimBase::setControlSetFrPhaseSeqB(world);
 }

void DTXHWSimImpl::setControlSetFrPhaseOffset(const std::vector<unsigned char>& world)
{
   associate(monitorPoint_FR_PHASE_OFFSET, world);
   DTXHWSimBase::setControlSetFrPhaseOffset(world);
}

void DTXHWSimImpl::associate(const AMB::rca_t RCA,
     const std::vector< CAN::byte_t >& data)
 {
   AUTO_TRACE(__PRETTY_FUNCTION__);
     std::map< AMB::rca_t, std::vector< CAN::byte_t >* >::iterator
         position(AMB::CommonHWSim::state_m.find(RCA));
     if(position != AMB::CommonHWSim::state_m.end())
     {
         std::vector< CAN::byte_t >* vector((*position).second);
         vector->assign(data.begin(), data.end());
     }
     else
     {
         std::ostringstream msg;
         msg << "Could not find the data for RCA = "
             << RCA
             << " in AMB::CommonHWSim::state_m.";
         LOG_TO_DEVELOPER(LM_ERROR, msg.str());
     }
 }

