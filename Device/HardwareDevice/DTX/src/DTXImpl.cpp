//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <DTXImpl.h>
#include <string>
#include <vector>
#include <algorithm>
#include <ostream>
#include <controlAlarmSender.h>
#include <loggingMACROS.h>
#include <TMCDBAccessIFC.h>
#include <TETimeUtil.h>
#include <acstimeTimeUtil.h>
#include <walshFunction.h>
#include <xmlTmcdbComponent.h>
#include <configDataAccess.h>


/**
 *-----------------------
 * DTX Constructor
 *-----------------------
 */
DTXImpl::DTXImpl(const ACE_CString& name, maci::ContainerServices* cs):
    DTXBase(name, cs),
    MonitorHelper(),
    guard(10000000ULL * 10ULL, 0),
    updateThreadCycleTime_m(5 * TETimeUtil::TE_PERIOD_ACS),
    turnOnDGPowerSupplyTimeout_m(1.0),
    entireFrResetTimeout_m(3.0),
    enableLasersTimeout_m(1.0),
    walshOffsetHelper_m(16E-9, updateThreadCycleTime_m),
    defaultPhaseOffset_m(0.012),
    walshFunction_m(0),
    walshFunctionSwitchingEnabled_m(false),
    alarmSender(0)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

/**
 *-----------------------
 * DTX Destructor
 *-----------------------
 */
DTXImpl::~DTXImpl()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

/**
 *---------------------------------
 * ACS Component Lifecycle Methods
 *---------------------------------
 */
void DTXImpl::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    initialiseAlarmSystem();

    diagnosticModeEnabled = true;

    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());

    try
    {
        DTXBase::initialize();
        MonitorHelper::initialize(compName.in(), getContainerServices());
    }
    catch(acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    std::string threadName(compName.in());
    threadName += "WriterThread";
    updateThread_p = getContainerServices()->getThreadManager()-> create<
        UpdateThread, DTXImpl > (threadName.c_str(), *this);
    updateThread_p->setSleepTime(updateThreadCycleTime_m);

}

void DTXImpl::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    /* Shutdown the threads, the threads were suspended by the
     * baseclass call to hwStopAction so we just need to terminate them
     * here.
     */
    updateThread_p->terminate();

    try
    {
        MonitorHelper::cleanUp();
        DTXBase::cleanUp();
    }
    catch(acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        cleanUpAlarmSystem();
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    cleanUpAlarmSystem();
}

/**
 *----------------------------
 * Hardware Lifecycle Methods
 *----------------------------
 */
void DTXImpl::hwInitializeAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    /* During this method we should:
     * resetAnyErrorFlags
     * terminateAllAlarms
     * make sure the updateThread is suspended
     * Turn on the power supply
     * configure the clock edge
     * Load the nominal delay value
     * Reset the Formatter
     */

    /* Load the configuration data from the TMCDB */
    getConfigurationData();
    walshFunction_m = 0;
    /// Bug fix for AIV-4125.
    defaultPhaseOffset_m = 0.012;
    // Load the WalshFunction and the LO offset.
    try
    {
        maci::SmartPtr< TMCDB::Access > accessRef;
        accessRef = getContainerServices()->
            getDefaultComponentSmartPtr< TMCDB::Access >(
                "IDL:alma/TMCDB/Access:1.0");

        const std::string antennaName(
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName());

        try
        {
            walshFunction_m = accessRef->getAntennaWalshFunctionSeqNumber(
                antennaName.c_str());
        }
        catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
        {
            ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", "Setting the Walsh function index failed.");
            nex.log();
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", "Setting the Walsh function index failed.");
            nex.log();
        }
        catch(const ControlExceptions::INACTErrorExImpl& ex)
        {
            ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "Setting the Walsh function index failed.");
            nex.log();
        }
        catch(const TmcdbErrType::TmcdbErrorEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access failed somehow.  Cannot "
                "set the Walsh function!");
        }
        catch(const TmcdbErrType::TmcdbNoSuchRowEx& ex)
        {
            LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access did not return a Walsh "
                "function index.  Cannot set the Walsh function!");
        }
        catch(...)
        {
            LOG_TO_OPERATOR(LM_ERROR, "An unexpected error happened during "
                "the retrieval of the Walsh function index from the TMCDB or "
                "the setting of it.  Cannot set the Walsh function!");
        }
    }
    catch(const maciErrType::NoDefaultComponentExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "TMCDB Access component is not available. "
            "Cannot set the Walsh function!");
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Got no reference for the TMCDB Access "
            "component.  Cannot set the Walsh function!");
    }

    /// Thomas, May 10, 2011
    ///
    /// Bug fix for AIV-4125.
    /// Make sure that the Wlash sequence in the DTX is cleared.
    /// enableWalshFunctionSwitching will send [0ULL, 0ULL] to the DTX if
    /// the parameter is false.
    enableWalshFunctionSwitching(false);

    if(alarmSender != 0)
    {
        alarmSender->terminateAllAlarms();
    }

    // Call the base class implementation
    DTXBase::hwInitializeAction();

    updateThread_p ->suspend();

    // Turn on the Digitizer's power supply
    try
    {
        if(!turnOnDGPowerSupply())
        {
            LOG_TO_OPERATOR(LM_ERROR, "DG power supply did not turn ON when "
                "requested.");
            // report error
            setError("Failed to turn on Digitizers Power Supply.");
            return;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failure while turning ON DG power supply.");
        // report error
        setError("CAN BUS Communication Error.");
        return;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State while turning ON DG power "
            "supply.");
        // report error
        setError("Command not accepted in current state.");
        return;
    }

    // set configured clock edge to time in
    try
    {
        configClockEdge();
    }
    catch(const DTXExceptions::ConfigClockEdgeExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failure while configuring clock edge.");
        // report error
        setError("Failed to configure TE Clock Edge from CDB.");
        return;
    }

    // Reset the FR (entire reset)
    /// Thomas, May 10, 2011
    ///
    /// Bug fix for AIV-4125.
    /// I added the reset procedure as provided by Paula Metzer and
    /// David Hunter.
    try
    {
        ACS::Time timestamp(0ULL);
        unsigned char frCwCh1(0xffU);
        unsigned char frCwCh2(0xffU);
        unsigned char frCwCh3(0xffU);
        unsigned short tries(0U);
        while(tries < 10U)
        {
            setFrCwAll(0xffU);
            entireFrReset();

            frCwCh1 = getFrCwCh1(timestamp);
            frCwCh2 = getFrCwCh2(timestamp);
            frCwCh3 = getFrCwCh3(timestamp);

            if((frCwCh1 == 0U)
            && (frCwCh2 == 0U) && (frCwCh3 == 0U))
            {
                break;
            }
            else
            {
                ++tries;
            }
        }

        if(tries >= 10U)
        {
            LOG_TO_OPERATOR(LM_ERROR, "FR Reset not successfuly"
                "completed!  The DTX hardware is probably defect!!!");
            setError("Failed to reset FR.");
            return;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failure while request entire FR Reset.");
        // report error
        setError("CAN BUS Communication Error.");
        return;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to request entire FR Reset.");
        // report error
        setError("Command not accepted in current state.");
        return;
    }

    /// Bug fix for AIV-4125.
    walshOffsetHelper_m.setPhaseOffset(defaultPhaseOffset_m);
    walshOffsetHelper_m.initialize();
}

void DTXImpl::hwOperationalAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Call the base class implementation
    DTXBase::hwOperationalAction();

    // turn on the lasers
    try
    {
        if(!turnOnLasers())
        {
            LOG_TO_OPERATOR(LM_ERROR, "Lasers failed to turn On.");
            // report error
            setError("Failed to turn On Lasers.");
            return;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failure while setting on Lasers.");
        // report error
        setError("CAN BUS Communication Error.");
        return;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to request Set On Lasers.");
        // report error
        setError("Command not accepted in current state.");
        return;
    }

    acstime::Epoch startTime = TimeUtil::ace2epoch(ACE_OS::gettimeofday());
    lastFrStatusMonitorTime_m = TETimeUtil::rtMonitorTime(startTime, 5).value;
    MonitorHelper::resume();
    updateThread_p->resume();
}

void DTXImpl::hwStopAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    AmbErrorCode_t flushStatus;
    ACS::Time flushTime;

    /* Suspend the threads */
    updateThread_p->suspend();

    if(isStartup())
    {
        /* Flush all commands */
        flushNode(0, &flushTime, &flushStatus);
        if(flushStatus != AMBERR_NOERR)
        {
            LOG_TO_DEVELOPER(LM_DEBUG, "Communication failure flushing "
                "commands to device");
        }
        else
        {
            std::ostringstream msg;
            msg << "All commands and monitors flushed at: "
                << flushTime;
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
    }

    if(alarmSender != 0)
    {
        alarmSender->terminateAllAlarms();
    }

    MonitorHelper::suspend();
    // Call the base class implementation
    DTXBase::hwStopAction();
}

/**
 *----------------------------
 * Delay Client Method
 *----------------------------
 */
void DTXImpl::newDelayEvent(const Control::AntennaDelayEvent& event)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    walshOffsetHelper_m.newDelayEvent(event);
}

/**
 *----------------------------
 * Monitoring Dispatch Method
 *----------------------------
 */
void DTXImpl::processRequestResponse(const AMBRequestStruct& response)
{

    if(response.Status == AMBERR_FLUSHED)
    {
        if(guard.checkAndIncrement() == true)
        {
            LOG_TO_DEVELOPER(LM_DEBUG, "AMBERR_FLUSHED");
        }

        /* Nothing to do, just return */
        return;
    }

    if(response.RCA == getMonitorRCAFrTeStatus())
    {
        /* Response from Fr Te Status Monitor  */
        processFrTeStatusMonitor(response);
        return;
    }
    else if(response.RCA == getMonitorRCAFrStatus())
    {
        /* Response from Fr Status Monitor  */
        processFrStatusMonitor(response);
        return;
    }
    else if(response.RCA == getMonitorRCATtxAlarmStatus())
    {
        /* Response from Ttx Status Monitor  */
        processTtxAlarmStatusMonitor(response);
        return;
    }

    /* This is an error */
    std::ostringstream msg;
    msg << "Unhandled request detected, RCA 0x"
        << std::hex
        << response.RCA;
    LOG_TO_DEVELOPER(LM_ERROR, msg.str());
}

/* ================ Hardware Queueing Methods  ================= */
// TE related
void DTXImpl::queueFrStatusMonitor()
{
    MonitorHelper::AMBRequestStruct* monReq;

    while(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value
        + (updateThreadCycleTime_m * 2) > lastFrStatusMonitorTime_m)
    {
        lastFrStatusMonitorTime_m += TETimeUtil::TE_PERIOD_ACS;

        monReq = getRequestStruct();

        monReq->RCA = getMonitorRCAFrStatus();
        monReq->TargetTime = lastFrStatusMonitorTime_m;

        monitorTE(monReq->TargetTime, monReq->RCA, monReq->DataLength,
            monReq->Data, monReq->SynchLock, &monReq->Timestamp,
            &monReq->Status);

        queueRequest(monReq);
    }
}

// NOT TE related
void DTXImpl::queueFrTeStatusMonitor()
{
    MonitorHelper::AMBRequestStruct* monReq;

    monReq = getRequestStruct();

    monReq->RCA = getMonitorRCAFrTeStatus();

    monitor(monReq->RCA, monReq->DataLength, monReq->Data, monReq->SynchLock,
        &monReq->Timestamp, &monReq->Status);

    queueRequest(monReq);
}

void DTXImpl::queueTtxAlarmStatusMonitor()
{
    MonitorHelper::AMBRequestStruct* monReq;

    monReq = getRequestStruct();

    monReq->RCA = getMonitorRCATtxAlarmStatus();

    monitor(monReq->RCA, monReq->DataLength, monReq->Data, monReq->SynchLock,
        &monReq->Timestamp, &monReq->Status);

    queueRequest(monReq);
}

void DTXImpl::updateWalshFunctionDelay()
{

    double newOffsetCommand;
    if(walshOffsetHelper_m.isUpdateNeeded(newOffsetCommand))
    {
        try
        {
            setCntlFrPhaseOffset(newOffsetCommand);
        }
        catch(const ControlExceptions::CAMBErrorExImpl& ex)
        {
            if(guard.checkAndIncrement() == true)
            {
                LOG_TO_OPERATOR(LM_ERROR, "Error setting phase offset on DTX.");
            }
        }
        catch(const ControlExceptions::INACTErrorExImpl& ex)
        {
            if(guard.checkAndIncrement() == true)
            {
                LOG_TO_OPERATOR(LM_ERROR, "INAactive Exception caught "
                    "updating delay");
            }
        }
    }
}

/* ================ Hardware Processing Methods =================*/
// TE Related
void DTXImpl::processFrStatusMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    /* Check Status */
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "FrStatus Monitor Failed (Status = "
                << response.Status
                << ")";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }

        alarmSender->activateAlarm(CommunicationError);
    }
    else
    {
        alarmSender->deactivateAlarm(CommunicationError);
    }

    /* Check Time */
    if((response.Timestamp - response.TargetTime)
        > (TETimeUtil::TE_PERIOD_ACS / 2))
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "FR Status Monitor at incorrect time ( "
                << (response.Timestamp - response.TargetTime) * 1E-7
                << "s off)";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        alarmSender->activateAlarm(FrStatusMonitorTimeError);
    }
    else
    {
        alarmSender->deactivateAlarm(FrStatusMonitorTimeError);
    }

    bool inError = false;

    // First Byte
    if((response.Data[0] & 0x01) == 0x00)
    {
        alarmSender->activateAlarm(NoOverPower);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(NoOverPower);
    }
    if((response.Data[0] & 0x02) == 0x00)
    {
        alarmSender->activateAlarm(NoREFCLK);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(NoREFCLK);
    }
    if((response.Data[0] & 0x04) == 0x00)
    {
        alarmSender->activateAlarm(Ch1PLL250Unlocked);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1PLL250Unlocked);
    }
    if((response.Data[0] & 0x08) == 0x00)
    {
        alarmSender->activateAlarm(Ch1PLL125Unlocked);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1PLL125Unlocked);
    }
    if((response.Data[0] & 0x10) == 0x00)
    {
        alarmSender->activateAlarm(Ch2PLL250Unlocked);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2PLL250Unlocked);
    }
    if((response.Data[0] & 0x20) == 0x00)
    {
        alarmSender->activateAlarm(Ch2PLL125Unlocked);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2PLL125Unlocked);
    }
    if((response.Data[0] & 0x40) == 0x00)
    {
        alarmSender->activateAlarm(Ch3PLL250Unlocked);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3PLL250Unlocked);
    }
    if((response.Data[0] & 0x80) == 0x00)
    {
        alarmSender->activateAlarm(Ch3PLL125Unlocked);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3PLL125Unlocked);
    }
    // Second Byte
    if((response.Data[1] & 0x08) == 0x00)
    {
        alarmSender->activateAlarm(Ch1TTXAlarm);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1TTXAlarm);
    }
    if((response.Data[1] & 0x10) == 0x00)
    {
        alarmSender->activateAlarm(Ch2TTXAlarm);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2TTXAlarm);
    }
    if((response.Data[1] & 0x20) == 0x00)
    {
        alarmSender->activateAlarm(Ch3TTXAlarm);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3TTXAlarm);
    }
    if((response.Data[1] & 0x40) == 0x00)
    {
        alarmSender->activateAlarm(TTXAlarm);
        inError = true;
    }
    else
    {
        alarmSender->deactivateAlarm(TTXAlarm);
    }

    if(inError)
    {
        if(guard.checkAndIncrement() == true)
        {

            LOG_TO_DEVELOPER(LM_DEBUG, "FR_STATUS reported an Alarm => "
                "Checking TTX Alarms and TE Alarms");
        }

        queueFrTeStatusMonitor();
        queueTtxAlarmStatusMonitor();
    }
}

// Not TE Related
void DTXImpl::processTtxAlarmStatusMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    /* Check Status */
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "TtxStatus Monitor Failed (Status = "
                << response.Status
                << ")";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }
        alarmSender->activateAlarm(CommunicationError);
    }
    else
    {
        alarmSender->deactivateAlarm(CommunicationError);
    }

    // First Byte
    if(!(response.Data[0] & 0x01))
    {
        alarmSender->activateAlarm(Ch1EOLAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1EOLAlm);
    }
    if(!(response.Data[0] & 0x02))
    {
        alarmSender->activateAlarm(Ch1ModTempAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1ModTempAlm);
    }
    if(!(response.Data[0] & 0x20))
    {
        alarmSender->activateAlarm(Ch1LsWavAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1LsWavAlm);
    }
    // Second Byte
    if(!(response.Data[1] & 0x01))
    {
        alarmSender->activateAlarm(Ch1TxAlmInt);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1TxAlmInt);
    }
    if(!(response.Data[1] & 0x02))
    {
        alarmSender->activateAlarm(Ch1LsBiasAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1LsBiasAlm);
    }
    if(!(response.Data[1] & 0x04))
    {
        alarmSender->activateAlarm(Ch1TempAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1TempAlm);
    }
    if(!(response.Data[1] & 0x08))
    {
        alarmSender->activateAlarm(Ch1LockErr);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1LockErr);
    }
    if(!(response.Data[1] & 0x20))
    {
        alarmSender->activateAlarm(Ch1LsPowAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1LsPowAlm);
    }
    if(!(response.Data[1] & 0x40))
    {
        alarmSender->activateAlarm(Ch1ModBiasAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1ModBiasAlm);
    }
    if(!(response.Data[1] & 0x80))
    {
        alarmSender->activateAlarm(Ch1TxFifoErr);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch1TxFifoErr);
    }
    // Third Byte
    if(!(response.Data[2] & 0x01))
    {
        alarmSender->activateAlarm(Ch2EOLAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2EOLAlm);
    }
    if(!(response.Data[2] & 0x02))
    {
        alarmSender->activateAlarm(Ch2ModTempAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2ModTempAlm);
    }
    if(!(response.Data[2] & 0x20))
    {
        alarmSender->activateAlarm(Ch2LsWavAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2LsWavAlm);
    }
    // Fourth Byte
    if(!(response.Data[3] & 0x01))
    {
        alarmSender->activateAlarm(Ch2TxAlmInt);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2TxAlmInt);
    }
    if(!(response.Data[3] & 0x02))
    {
        alarmSender->activateAlarm(Ch2LsBiasAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2LsBiasAlm);
    }
    if(!(response.Data[3] & 0x04))
    {
        alarmSender->activateAlarm(Ch2TempAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2TempAlm);
    }
    if(!(response.Data[3] & 0x08))
    {
        alarmSender->activateAlarm(Ch2LockErr);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2LockErr);
    }
    if(!(response.Data[3] & 0x20))
    {
        alarmSender->activateAlarm(Ch2LsPowAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2LsPowAlm);
    }
    if(!(response.Data[3] & 0x40))
    {
        alarmSender->activateAlarm(Ch2ModBiasAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2ModBiasAlm);
    }
    if(!(response.Data[3] & 0x80))
    {
        alarmSender->activateAlarm(Ch2TxFifoErr);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch2TxFifoErr);
    }
    // Fifth Byte
    if(!(response.Data[4] & 0x01))
    {
        alarmSender->activateAlarm(Ch3EOLAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3EOLAlm);
    }
    if(!(response.Data[4] & 0x02))
    {
        alarmSender->activateAlarm(Ch3ModTempAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3ModTempAlm);
    }
    if(!(response.Data[4] & 0x20))
    {
        alarmSender->activateAlarm(Ch3LsWavAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3LsWavAlm);
    }
    // Sixth Byte
    if(!(response.Data[5] & 0x01))
    {
        alarmSender->activateAlarm(Ch3TxAlmInt);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3TxAlmInt);
    }
    if(!(response.Data[5] & 0x02))
    {
        alarmSender->activateAlarm(Ch3LsBiasAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3LsBiasAlm);
    }
    if(!(response.Data[5] & 0x04))
    {
        alarmSender->activateAlarm(Ch3TempAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3TempAlm);
    }
    if(!(response.Data[5] & 0x08))
    {
        alarmSender->activateAlarm(Ch3LockErr);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3LockErr);
    }
    if(!(response.Data[5] & 0x20))
    {
        alarmSender->activateAlarm(Ch3LsPowAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3LsPowAlm);
    }
    if(!(response.Data[5] & 0x40))
    {
        alarmSender->activateAlarm(Ch3ModBiasAlm);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3ModBiasAlm);
    }
    if(!(response.Data[5] & 0x80))
    {
        alarmSender->activateAlarm(Ch3TxFifoErr);
    }
    else
    {
        alarmSender->deactivateAlarm(Ch3TxFifoErr);
    }
}

void DTXImpl::processFrTeStatusMonitor(
    const MonitorHelper::AMBRequestStruct& response)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    /* Check Status */
    if(response.Status != AMBERR_NOERR)
    {
        if(guard.checkAndIncrement() == true)
        {
            std::ostringstream msg;
            msg << "FrTeStatus Monitor Failed (Status = "
                << response.Status
                << ")";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }

        alarmSender->activateAlarm(CommunicationError);
    }
    else
    {
        alarmSender->deactivateAlarm(CommunicationError);
    }

    // First Byte
    if(response.Data[0] & 0x01)
    {
        alarmSender->activateAlarm(TeError);
    }
    else
    {
        alarmSender->deactivateAlarm(TeError);
    }
}

/* ===================  Threads   ========================= */
void DTXImpl::updateThreadAction()
{
    /* Queue Status Monitor */
    queueFrStatusMonitor();
    updateWalshFunctionDelay();
}

DTXImpl::UpdateThread::UpdateThread(const ACE_CString& name, DTXImpl& DTX):
    ACS::Thread(name),
    dtxDevice_p(DTX)
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void DTXImpl::UpdateThread::runLoop()
{
    dtxDevice_p.updateThreadAction();
}

/**
 *-------------
 * DTX Methods
 *-------------
 */
void DTXImpl::setWalshFunction(CORBA::Long WALSequenceNumber)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(walshFunctionSwitchingEnabled_m == true)
    {
        loadWalshFunction(WALSequenceNumber);
        walshFunction_m = WALSequenceNumber;
    }
    else
    {
        try
        {
            /* Check to make sure it's valid */
            WalshFunction wf = WalshGenerator::getWalshFunction(
                WALSequenceNumber);
            walshFunction_m = WALSequenceNumber;
        }
        catch(const std::out_of_range& ex)
        {
            throw ControlExceptions::IllegalParameterErrorExImpl(__FILE__,
                __LINE__, __PRETTY_FUNCTION__).getIllegalParameterErrorEx();
        }
    }
}

void DTXImpl::loadWalshFunction(long WALSequenceNumber)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        /// Thomas, May 12, 2011
        ///
        /// Bug fix for AIV-4125 and AIV.
        /// The DTX firmware as of 2011-05-10 interprets the Walsh sequence
        /// (see p. 5 ff., ALMA Memo 565) different from the FLOOG.  For
        /// simplicity I explain the difference here with two parts that
        /// contain only 16 bits and not 64 bits.  64 bits and not 128
        /// because we cannot send more than 64 bits in one go over the
        /// CAN-bus.
        ///
        /// FLOOG calls the 64 bit parts "1" and "2", DTX calls them "A" and
        /// "B".  I use the DTX nomenclature.
        ///
        /// Sequence    part A                             part B
        ///      (00111100)(00100000)               (00000101)(11101111)
        /// Bit#: 76543210  76543210                 76543210  76543210
        /// Byte# as received by the device:
        ///          2         1                        2         1
        ///
        /// The DTX and FLOOG use the received bytes and bits in the following
        /// order over time:
        ///          DTX                                 FLOOG
        /// part A, byte #2, bit #0             part A, byte #1, bit #7
        /// part A, byte #2, bit #1             part A, byte #1, bit #6
        ///      A,      #2,     #2                  A,      #1,     #5
        ///      A        2       3                  A        1       4
        ///           A 2 4                               A 1 3
        ///           A 2 5                               A 1 2
        ///           A 2 6                               A 1 1
        ///           A 2 7                               A 1 0
        ///           A 1 0                               A 2 7
        ///           A 1 1                               A 2 6
        ///            ...                                 ...
        ///           A 1 7                               A 2 0
        ///           B 2 0                               B 1 7
        ///            ...                                 ...
        ///           B 2 7                               B 1 0
        ///           B 1 0                               B 2 7
        ///            ...                                 ...
        ///           B 1 7                               B 2 0
        ///
        /// From the above it becomes clear that two transformations have to
        /// be applied to the bits sent to either the FLOOG or to the DTX in
        /// order to align them.  The first transformation is to swap bytes
        /// #1 and #2 will yields if applied to the DTX:
        ///          DTX                                 FLOOG
        ///    A 2 0   ->   A 1 0                         A 1 7
        ///    A 2 1   ->   A 1 1                         A 1 6
        ///     ...    ->    ...                           ...
        ///    A 2 7   ->   A 1 7                         A 1 0
        ///    A 1 0   ->   A 2 0                         A 2 7
        ///     ...    ->    ...                           ...
        ///    B 2 0   ->   B 1 0                         B 1 7
        ///     ...    ->    ...                           ...
        ///    B 1 7   ->   B 2 7                         B 2 0
        ///
        /// Still, this does not result in the exact same sequence in both
        /// devices because the DTX and the FLOOG start with opposite bits
        /// within each byte.  An additional bit-reversal within every byte
        /// takes care of that:
        ///          DTX                                 FLOOG
        ///    A 1 0   ->   A 1 7                         A 1 7
        ///    A 1 1   ->   A 1 6                         A 1 6
        ///     ...    ->    ...                           ...
        ///    A 1 7   ->   A 1 0                         A 1 0
        ///    A 2 0   ->   A 2 7                         A 2 7
        ///     ...    ->    ...                           ...
        ///    B 1 0   ->   B 1 7                         B 1 7
        ///     ...    ->    ...                           ...
        ///    B 2 7   ->   B 2 0                         B 2 0
        ///
        /// TODO
        /// The DTX firmware or the FLOOG could be updated to take care of this
        /// but it is at this time very unlikely that this will ever happen.
        /// Neither Paula Metzner nor Sylas are for ALMA anymore.
        ///
        WalshFunction wf(WalshGenerator::getWalshFunction(WALSequenceNumber));
        std::vector< unsigned char > firstHalf(wf.firstVector());
        std::vector< unsigned char > secondHalf(wf.secondVector());
        std::reverse(firstHalf.begin(), firstHalf.end());
        std::reverse(secondHalf.begin(), secondHalf.end());
        setCntlFrPhaseSeqA(firstHalf);
        setCntlFrPhaseSeqB(secondHalf);
    }
    catch(std::out_of_range& ex)
    {
        throw ControlExceptions::IllegalParameterErrorExImpl(__FILE__,
            __LINE__, __PRETTY_FUNCTION__).getIllegalParameterErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getINACTErrorEx();
    }
}

CORBA::Long DTXImpl::getWalshFunction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return walshFunction_m;
}

void DTXImpl::setWalshFunctionDelay(double newDelayValue)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    walshOffsetHelper_m.setPhaseOffset(newDelayValue);
}

CORBA::Double DTXImpl::getWalshFunctionDelay()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return walshOffsetHelper_m.getPhaseOffset();
}

void DTXImpl::enableWalshFunctionSwitching(CORBA::Boolean enable)
{
    walshFunctionSwitchingEnabled_m = enable;
    if(walshFunctionSwitchingEnabled_m == true)
    {
        /* Turn on Walsh Functions */
        loadWalshFunction(walshFunction_m);
    }
    else
    {
        /// Thomas, May 10, 2011
        ///
        /// Bug fix for AIV-4125.
        /// Turn the Walsh sequence off by sending all 0s to the DTX.
        /// A std::vector is always automatically cleared.
        std::vector< unsigned char > zero(8);
        setCntlFrPhaseSeqA(zero);
        setCntlFrPhaseSeqB(zero);
    }
}

CORBA::Boolean DTXImpl::walshFunctionSwitchingEnabled()
{
    return walshFunctionSwitchingEnabled_m;
}

CORBA::Boolean DTXImpl::RESYNC_TE()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        return  syncInternalTeToExternalTe();
    }
    catch(const DTXExceptions::ResyncTeExImpl& ex)
    {
        throw DTXExceptions::ResyncTeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getResyncTeEx();
    }
}

bool DTXImpl::syncInternalTeToExternalTe()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    bool invertedClockEdgeUsed = false;
    ACS::Time timestamp(0ULL);

    try
    {
        // Should we reset the TE registers before requesting the status?
        // Get status of Timing Event registers
        std::vector< unsigned char > FrTeStatus = getFrTeStatus(timestamp);
        std::ostringstream msg;
        msg << "getFrTeStatus() returned: [0x"
            << std::hex
            << static_cast< unsigned short >(FrTeStatus[0])
            <<"][0x"
            << static_cast< unsigned short >(FrTeStatus[1])
            << "][0x"
            << static_cast< unsigned short >(FrTeStatus[2])
            << "][0x"
            << static_cast< unsigned short >(FrTeStatus[3])
            << "]";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        // Check if inverted clock edge is being used to clock in Timing Event
        invertedClockEdgeUsed = getFrTeStatusClkEdge(timestamp);

        // Check if TE error flag is set => max or min error triggered
        // If it is set, TE should be clocked in using the opposite edge of clock
        if(getFrTeStatusError(timestamp))
        {
            // use the opposite edge of clock
            invertedClockEdgeUsed = !invertedClockEdgeUsed;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to get status of TE registers from "
            "FR.");
        throw DTXExceptions::ResyncTeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to get status of TE "
            "registers from FR.");
        throw DTXExceptions::ResyncTeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // resync TE
    try
    {
        // We assume that inverted edge = negative clock edge
        unsigned char ResyncTEWord = invertedClockEdgeUsed ? 0x6 : 0x4;
        setFrTeReset(ResyncTEWord);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to send resync TE command to FR.");
        throw DTXExceptions::ResyncTeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to send resync TE command to "
            "FR.");
        throw DTXExceptions::ResyncTeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // Check if we are now in Sync
    try
    {
        std::vector< unsigned char > FrStatus(getFrStatus(timestamp));
        std::ostringstream msg;
        msg << "getFrStatus() returned: [0x"
            << std::hex
            << static_cast< unsigned short >(FrStatus[0])
            <<"][0x"
            << static_cast< unsigned short >(FrStatus[1])
            << "]";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        return getFrStatusRefclk(timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to get status of TE registers from "
            "FR.");
        throw DTXExceptions::ResyncTeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to get status of TE "
            "registers from FR.");
        throw DTXExceptions::ResyncTeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

bool DTXImpl::turnOnDGPowerSupply(double timeout)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp(0ULL);

    // check if PS was already ON
    try
    {
        if(getDgPsOnOff(timestamp))
        {
            // Do nothing if PS was already on.
            return true;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to access DG power supply status.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to access DG power supply "
            "status.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // turn ON the PS
    try
    {
        setCntlDgPsOnOff(true);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR,  "Failed to send DG Power Supply On "
            "command.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to send DG Power Supply On "
            "command.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // check if timeout should be default one
    if(timeout == -1)
        timeout = turnOnDGPowerSupplyTimeout_m;

    // convert timeout to micro second
    const useconds_t timeoutInUs = static_cast< useconds_t > (timeout * 1E6);

    // wait timeout seconds before checking PS status
    usleep(timeoutInUs);

    // Check if PS is now ON
    try
    {
        if(getDgPsOnOff(timestamp))
        {
            LOG_TO_OPERATOR(LM_DEBUG, "DG Power Supply is now ON");
            return true;
        }
        else
        {
            // the alarm and/or error should be set by the caller
            std::ostringstream msg;
            msg << "TIMEOUT: DG Power Supply did not turn ON within expected time: "
                << timeout
                << "[s].";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());

            return false;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR,  "Failed to access DG Power Supply Status.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to access DG Power Supply "
            "Status.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

bool DTXImpl::entireFrReset(double timeout)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp(0ULL);

    // request complete reset
    try
    {
        unsigned char FrResetAllWord = 0xF0; // 0xF0: bits 7-4 reset entire chip
        setFrResetAll(FrResetAllWord);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to send FR Reset command.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to send FR Reset command.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // check if timeout should be default one
    if(timeout == -1)
        timeout = entireFrResetTimeout_m;

    // convert timeout to micro second
    const useconds_t timeoutInUs = static_cast< useconds_t > (timeout * 1E6);

    // wait timeout seconds before checking PS status
    usleep(timeoutInUs);

    // Check FR status
    try
    {
        std::vector< unsigned char > FrStatus;
        FrStatus = getFrStatus(timestamp);

        std::ostringstream msg;
        msg << "getFrStatus(): [0x"
            << std::hex
            << static_cast< unsigned short >(FrStatus[0])
            <<"][0x"
            << static_cast< unsigned short >(FrStatus[1])
            << "]";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        if(FrStatus[0] == 0xFF)
        {
            // OXFF => Keep Alive Present, TE present and synced, 250 & 125 MHZ of CH1-3 in lock
            LOG_TO_DEVELOPER(LM_DEBUG, "FR Reset Successful.");
            return true;
        }
        else
        {
            msg.str("");
            msg << "FR Reset Not Successful after "
                << timeout
                << "[s]. Check Alarms.";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
            return false;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to access FR Status.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to access FR Status.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

}

bool DTXImpl::turnOnLasers(double timeout)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timestamp(0ULL);

    // enable Lasers
    try
    {
        unsigned char ttxLaserEnableWord = 0x7;
        setTtxLaserEnable(ttxLaserEnableWord);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to Enable Half-Transponders Lasers.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to Enable Half-Transponders "
            "Lasers.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // check if timeout should be default one
    if(timeout == -1)
    {
        timeout = enableLasersTimeout_m;
    }

    // convert timeout to micro second
    const useconds_t timeoutInUs = static_cast< useconds_t >(timeout * 1E6);

    // wait timeout seconds before checking PS status
    usleep(timeoutInUs);

    // reset TTX alarms
    try
    {
        unsigned char ttxClrAlarmsWord = 0xFF;
        setTtxClrAlarms(ttxClrAlarmsWord);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to send clear alarms command.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to send clear alarms "
            "command.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // wait until alarms are deactivated (tested to be near 0.3s)
    usleep(300000);

    // clear TTX alarm registers (ignore the result)
    try
    {
        getTtxAlarmStatus(timestamp);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to access TTX alarm status.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to access TTX alarm status.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // get FR status.
    try
    {
        std::vector< unsigned char > FrStatus;
        FrStatus = getFrStatus(timestamp);

        std::ostringstream msg;
        msg << "getFrStatus() returned: [0x"
            << std::hex
            << static_cast< unsigned short >(FrStatus[0])
            <<"][0x"
            << static_cast< unsigned short >(FrStatus[1])
            << "]";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        if(FrStatus[1] == 0xff)
        {
            LOG_TO_DEVELOPER(LM_DEBUG, "FR Laser Status All OK.");
            return true;
        }
        else
        {
            msg.str("");
            msg << "Lasers failed to turn ON within expected time: "
                << timeout
                << "[s].";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
            return false;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to get Status from FR.");
        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to get Status from FR.");
        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

}

void DTXImpl::configClockEdge()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    unsigned char frTeResetWord;
    if(useNegClockEdge_m)
    {
        frTeResetWord = 0x02;
    }
    else
    {
        frTeResetWord = 0x00;
    }

    try
    {
        setFrTeReset(frTeResetWord);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed set clock edge: CAMB error.");
        throw DTXExceptions::ConfigClockEdgeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed set clock edge: Invalid State.");
        throw DTXExceptions::ConfigClockEdgeExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void DTXImpl::getConfigurationData()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // The serial number got cached as part of hwConfigureAction in the base
    //class
    std::string deviceID = HardwareDeviceImpl::getSerialNumber();

    // Get the schema and the instance file for this device
    XmlTmcdbComponent tmcdb(getContainerServices());

    std::string xsd = tmcdb.getConfigXsd("DTX");
    std::string xml = tmcdb.getConfigXml(deviceID);

    if(xml.size() == 0)
    {
        // no configuration available for selected device
        // log a warning and return
        std::ostringstream msg;
        msg << "Could not access config data for DTX with S/N: "
            << deviceID;
        LOG_TO_OPERATOR(LM_WARNING, msg.str());

        defaultPhaseOffset_m = 0.012;
        useNegClockEdge_m = false;

        return;
    }
    else if(xsd.size() == 0)
    {
        // no schema available for selected device
        // log a warning and return
        LOG_TO_OPERATOR(LM_WARNING, "Could not access schema DTX.xsd on "
            "TMCDB_DATA directory");

        defaultPhaseOffset_m = 0.012;
        useNegClockEdge_m = false;

        return;
    }

    try
    {
        std::auto_ptr< ConfigDataAccessor > configData(
            new ConfigDataAccessor(xml, xsd));
        std::auto_ptr< const ControlXmlParser > clkEdge_element =
            configData->getElement("USE_NEG_CLOCK_EDGE");

        if(clkEdge_element->getStringAttribute("value") == "true")
        {
            useNegClockEdge_m = true;
        }
        else
        {
            useNegClockEdge_m = false;
        }

        std::ostringstream msg;
        msg << "Device with S/N "
            << deviceID
            << " will use the "
            << (useNegClockEdge_m ? "negative" : "positive")
            << " clock edge";
        LOG_TO_OPERATOR(LM_DEBUG, msg.str());
    }
    catch(const ControlExceptions::XmlParserErrorExImpl &ex)
    {
        DTXExceptions::ConfigDelayExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "Failure while accessing config data for S/N:" << deviceID;
        nex.addData("Message", msg.str().c_str());
        nex.log();
        throw nex;
    }
}

void DTXImpl::handleActivateAlarm(int code)
{
    DTXBase::setError(alarmSender->createErrorMessage());
}

void DTXImpl::handleDeactivateAlarm(int code)
{
    if(alarmSender->isAlarmSet() == true)
    {
        DTXBase::setError(alarmSender->createErrorMessage());
    }
    else
    {
        DTXBase::clearError();
    }
}

void DTXImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSender.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = CommunicationError;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CAN BUS Communication Error.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = FrStatusMonitorTimeError;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription =
        "FR Status Monitor Executed at incorrect time.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = NoOverPower;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription =
        "OverPower not set => Keep Alive signal not present.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = NoREFCLK;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="REFLCK not set => TE out of sync.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1PLL125Unlocked;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="250MHZ PLL not in lock for CH1.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1PLL250Unlocked;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="125MHZ PLL not in lock for CH1.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2PLL125Unlocked;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="250MHZ PLL not in lock for CH2.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2PLL250Unlocked;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="125MHZ PLL not in lock for CH2.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3PLL125Unlocked;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="250MHZ PLL not in lock for CH3.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3PLL250Unlocked;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="125MHZ PLL not in lock for CH3.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1TTXAlarm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="Ch1 TTX Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2TTXAlarm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="Ch2 TTX Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3TTXAlarm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="Ch3 TTX Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = TTXAlarm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="TTX Alarm Active (CH1|CH2|CH3).";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = TeError;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="FR_TE_STATUS TE error triggered.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1EOLAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Laser End Of Life Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1ModTempAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Modulator Temperature Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1LsWavAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Laser Wavelength Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1TxAlmInt;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Tx Summary Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1LsBiasAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Laser Bias Current Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1TempAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Laser Temperature Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1LockErr;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Loss of TxPLL Lock Indicator.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1LsPowAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Laser Power Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1ModBiasAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Modulator Bias Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch1TxFifoErr;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH1 Mux FIFO Error Indicator.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2EOLAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Laser End Of Life Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2ModTempAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Modulator Temperature Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2LsWavAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Laser Wavelength Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2TxAlmInt;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Tx Summary Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2LsBiasAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Laser Bias Current Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2TempAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Laser Temperature Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2LockErr;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Loss of TxPLL Lock Indicator.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2LsPowAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Laser Power Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2ModBiasAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Modulator Bias Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch2TxFifoErr;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH2 Mux FIFO Error Indicator.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3EOLAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Laser End Of Life Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3ModTempAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Modulator Temperature Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3LsWavAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Laser Wavelength Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3TxAlmInt;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Tx Summary Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3LsBiasAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Laser Bias Current Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3TempAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Laser Temperature Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3LockErr;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Loss of TxPLL Lock Indicator.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3LsPowAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Laser Power Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3ModBiasAlm;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Modulator Bias Alarm.";
      alarmInformation.push_back(alarmInfo);
    }

    {
      Control::AlarmInformation alarmInfo;
      alarmInfo.alarmCode = Ch3TxFifoErr;
      alarmInfo.alarmTerminateCount = 3;
      alarmInfo.alarmDescription ="CH3 Mux FIFO Error Indicator.";
      alarmInformation.push_back(alarmInfo);
    }

    alarmSender->initializeAlarms(
        Control::CharacteristicControlDeviceImpl::getAlarmFamilyName("DTX"),
            Control::CharacteristicControlDeviceImpl::getAlarmMemberName(),
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void DTXImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(DTXImpl)
