#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a DTX device.
"""

import CCL.DTXBase
from CCL import StatusHelper
import CCL.SIConverter
import os

class DTX(CCL.DTXBase.DTXBase):
    '''
    The DTX class inherits from the code generated DTXBase
    class and adds specific methods.
    '''
    def __init__(self, antennaName = None, instance = None,
                 componentName = None, stickyFlag = False):
        '''
        The constructor creates a DTX object by making use of
        the DTXBase constructor.
        EXAMPLE:
        from CCL.DTX import DTX
        #Directly through component name
        dtx0 = DTX(componentName="CONTROL/DV01/DTXBBpr0")
        #or specifying the antenna name and the instance number of DTX.
        #Instance can be 0, 1, 2 or 3
        dtx0 = DTX("DV01", 0)
        '''
        
        if componentName == None:
            if ((antennaName == None) or (instance == None)):
                raise NameError, "missing component name or the antennaName and instance number"
            if (isinstance(antennaName,list) == True):
                if (len(antennaName) != 0):
                    componentName=[]
                    for idx in range (0,len(antennaName)):
                        componentName.append("CONTROL/"+antennaName[idx]+"/DTXBBpr"+str(instance))
            else:
                componentName = "CONTROL/"+antennaName+"/DTXBBpr"+str(instance)

        CCL.DTXBase.DTXBase.__init__(self, None, componentName, stickyFlag)

    def __del__(self):
        CCL.DTXBase.DTXBase.__del__(self)

    #
    # RESYNC INTERNAL TE TO EXTERNAL TE
    #
    def RESYNC_TE(self):
        '''
        Re-synchronizes the the internal TE to an external TE. Returns
        True if successful.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].RESYNC_TE()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def setWalshFunction(self, WALSequenceNo):
        '''
        This method initializes the Walsh function to the specified
        value.  Although the Walsh function may be specified at any
        time it is now applied until the enableWalshFunction method is
        called.
        '''
        for key, val in self._devices.iteritems():
            self._devices[key].setWalshFunction(WALSequenceNo)
            
    def getWalshFunction(self):
        '''
        This method returns the Walsh function currently specified
        for the device.  This value may be nonzero even if the
        Walsh functions are not enabled.
        The return value is the WAL Sequence number of the current
        Walsh function.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getWalshFunction()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def enableWalshFunctionSwitching(self, enable = True):
        '''
        This method turns on or off Walsh Function Switching in this
        DTX.
        '''
        for key, val in self._devices.iteritems():
            self._devices[key].enableWalshFunctionSwitching(enable)

    def walshFunctionSwitchingEnabled(self):
        '''
        This method reports if the Walsh Function Switching is enabled or not
        for this DTX.  Use the enable Walsh Function Switching method to
        change the state.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName]= self._devices[key].walshFunctionSwitchingEnabled()
        if len(self._devices) == 1:
            return result.values()[0]
        return result

    def setWalshFunctionDelay(self, delay):
        '''
        This configures the nominal Walsh function delay for this DTX
        module.  That is the delay when no delay is present in the correlator.
        The value is specified in seconds although strings (i.e. 16 us) are
        acceptable.
        '''
        delay = CCL.SIConverter.toSeconds(delay)
        for key, val in self._devices.iteritems():
            self._devices[key].setWalshFunctionDelay(delay)
 
    def getWalshFunctionDelay(self):
        '''
        This method returns the nominal Walsh function delay for this DTX
        module.  That is the delay when no delay is present in the correlator.
        The return value is in seconds.
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].getWalshFunctionDelay()
        if len(self._devices) == 1:
            return result.values()[0]
        return result


    def STATUS(self):
        '''
        This method prints the status of DTX
        '''
        os.system("clear")
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

            ####################### PS Status ########################
            try:
                psOnOff = self._devices[key].GET_DG_PS_ON_OFF()[0]
                psOnOff = (psOnOff == 1) and "On" or "Off"
            except:
                psOnOff = "N/A"
 
            ####################### 1.5V ########################
            try:
                fr1p5VChB = self._devices[key].GET_FR_1_5_V_FPGA_3()[0]
                fr1p5VChB = "%.2f" % fr1p5VChB
            except:
                fr1p5VChB = "N/A"
            
            try:
                fr1p5VChC = self._devices[key].GET_FR_1_5_V_FPGA_2()[0]
                fr1p5VChC = "%.2f" % fr1p5VChC
            except:
                fr1p5VChC = "N/A"

            try:
                fr1p5VChD = self._devices[key].GET_FR_1_5_V_FPGA_1()[0]
                fr1p5VChD = "%.2f" % fr1p5VChD
            except:
                fr1p5VChD = "N/A"

            ####################### 1.8V ########################
            try:
                fr1p8VChB = self._devices[key].GET_FR_1_8_V_CH_3()[0]
                fr1p8VChB = "%.2f" % fr1p8VChB
            except:
                fr1p8VChB = "N/A"
            
            try:
                fr1p8VChC = self._devices[key].GET_FR_1_8_V_CH_2()[0]
                fr1p8VChC = "%.2f" % fr1p8VChC
            except:
                fr1p8VChC = "N/A"

            try:
                fr1p8VChD = self._devices[key].GET_FR_1_8_V_CH_1()[0]
                fr1p8VChD = "%.2f" % fr1p8VChD
            except:
                fr1p8VChD = "N/A"

            ####################### Main Power Status ########################
            try:
                fr48Von = self._devices[key].GET_FR_48_V_ON()[0]
                fr48Von = (fr48Von == 1) and "On" or "Off"
            except:
                fr48Von = "N/A"

            ####################### 3.3V DG Board Voltage ########################
            try:
                dg3p3V = self._devices[key].GET_DG_3_3_V()[0]
                dg3p3V = "%.2f" % dg3p3V
            except:
                dg3p3V = "N/A"

            ####################### 5V DG Board Voltage ########################
            try:
                dg5V = self._devices[key].GET_DG_5_V()[0]
                dg5V = "%.2f" % dg5V
            except:
                dg5V = "N/A"

            ####################### Laser Bias ########################
            try:
                ttxLsrBiasChB = self._devices[key].GET_TTX_LASER_BIAS_CH3()[0]
                ttxLsrBiasChB = "%.1f" % (ttxLsrBiasChB * 1000)
            except:
                ttxLsrBiasChB = "N/A"
            
            try:
                ttxLsrBiasChC = self._devices[key].GET_TTX_LASER_BIAS_CH2()[0]
                ttxLsrBiasChC = "%.1f" % (ttxLsrBiasChC * 1000)
            except:
                ttxLsrBiasChC = "N/A"

            try:
                ttxLsrBiasChD = self._devices[key].GET_TTX_LASER_BIAS_CH1()[0]
                ttxLsrBiasChD = "%.1f" % (ttxLsrBiasChD * 1000)
            except:
                ttxLsrBiasChD = "N/A"

            ####################### Laser Power ########################
            try:
                ttxLsrPwrChB = self._devices[key].GET_TTX_LASER_PWR_CH3()[0]
                ttxLsrPwrChB = "%.2f" % (ttxLsrPwrChB * 1000)
            except:
                ttxLsrPwrChB = "N/A"
            
            try:
                ttxLsrPwrChC = self._devices[key].GET_TTX_LASER_PWR_CH2()[0]
                ttxLsrPwrChC = "%.2f" % (ttxLsrPwrChC * 1000)
            except:
                ttxLsrPwrChC = "N/A"

            try:
                ttxLsrPwrChD = self._devices[key].GET_TTX_LASER_PWR_CH1()[0]
                ttxLsrPwrChD = "%.2f" % (ttxLsrPwrChD * 1000)
            except:
                ttxLsrPwrChD = "N/A"

            ####################### Laser Temp ########################
            try:
                ttxLsrTmpChB = self._devices[key].GET_TTX_LASER_TMP_CH3()[0]
                ttxLsrTmpChB = "%.1f" % (ttxLsrTmpChB - 273.16)
            except:
                ttxLsrTmpChB = "N/A"
            
            try:
                ttxLsrTmpChC = self._devices[key].GET_TTX_LASER_TMP_CH2()[0]
                ttxLsrTmpChC = "%.1f" % (ttxLsrTmpChC - 273.16)
            except:
                ttxLsrTmpChC = "N/A"

            try:
                ttxLsrTmpChD = self._devices[key].GET_TTX_LASER_TMP_CH1()[0]
                ttxLsrTmpChD = "%.1f" % (ttxLsrTmpChD - 273.16)
            except:
                ttxLsrTmpChD = "N/A"

            ####################### Laser On ########################
            try:
                ttxLsrOnChB = self._devices[key].GET_TTX_LASER_ENABLED_TTX3()[0]
                ttxLsrOnChB = (ttxLsrOnChB == 1) and "On" or "Off"
            except:
                ttxLsrOnChB = "N/A"

            try:
                ttxLsrOnChC = self._devices[key].GET_TTX_LASER_ENABLED_TTX2()[0]
                ttxLsrOnChC = (ttxLsrOnChC == 1) and "On" or "Off"
            except:
                ttxLsrOnChC = "N/A"

            try:
                ttxLsrOnChD = self._devices[key].GET_TTX_LASER_ENABLED_TTX1()[0]
                ttxLsrOnChD = (ttxLsrOnChD == 1) and "On" or "Off"
            except:
                ttxLsrOnChD = "N/A"

            ####################### FR Status ########################
            try:
                frTeOk = self._devices[key].GET_FR_STATUS_REFCLK()[0]
                frTeOk = (frTeOk == 1) and "Ok" or "BAD"
            except:
                frTeOk = "N/A"

            try:
                frKeepalive = self._devices[key].GET_FR_STATUS_OVER_POWER()[0]
                frKeepalive = (frKeepalive == 1) and "Ok" or "BAD"
            except:
                frKeepalive = "N/A"

            try:
                fr125miss = self._devices[key].GET_FR_48_V_125MHZMISSING()[0]
                fr125miss = (fr125miss == 1) and "MISS" or "Ok"
            except:
                fr125miss = "N/A"

            ####################### FR 125Mhz Lock ########################
            try:
                fr125lockChB = self._devices[key].GET_FR_STATUS_125_CH3()[0]
                fr125lockChC = self._devices[key].GET_FR_STATUS_125_CH2()[0]
                fr125lockChD = self._devices[key].GET_FR_STATUS_125_CH1()[0]
                fr125lock = (fr125lockChB == fr125lockChC == fr125lockChD == 1) and "Locked" or "UNLOCK"
            except:
                fr125lock = "N/A"
                
            ####################### FR 250Mhz Lock ########################
            try:
                fr250lockChB = self._devices[key].GET_FR_STATUS_250_CH3()[0]
                fr250lockChC = self._devices[key].GET_FR_STATUS_250_CH2()[0]
                fr250lockChD = self._devices[key].GET_FR_STATUS_250_CH1()[0]
                fr250lock = (fr250lockChB == fr250lockChC == fr250lockChD == 1) and "Locked" or "UNLOCK"
            except:
                fr250lock = "N/A"

            ####################### FR TE Status ########################
            try:
                frTeStClkEdge = self._devices[key].GET_FR_TE_STATUS_CLK_EDGE()[0]
                frTeStClkEdge = (frTeStClkEdge == 1) and "FALL" or "Rise"
            except:
                frTeStClkEdge = "N/A"

            try:
                frTeStCurrErr = self._devices[key].GET_FR_TE_STATUS_CURR_ERR()[0]
                frTeStCurrErr = "%i" % frTeStCurrErr
            except:
                frTeStCurrErr = "N/A"

            try:
                frTeStEarlyErr = self._devices[key].GET_FR_TE_STATUS_MIN_ERR()[0]
                frTeStEarlyErr = "%i" % frTeStEarlyErr
            except:
                frTeStEarlyErr = "N/A"

            try:
                frTeStLateErr = self._devices[key].GET_FR_TE_STATUS_MAX_ERR()[0]
                frTeStLateErr = "%i" % frTeStLateErr
            except:
                frTeStLateErr = "N/A"

            ####################################################################

            elements = []
            #elements.append(StatusHelper.Group("Temperature",fr1p5VChB))




            elements.append(StatusHelper.Line("DG PS On/Off:",[StatusHelper.ValueUnit(psOnOff,label="PS")]))

            elements.append( StatusHelper.Separator("Channel B") )

            values = []
            values.append(StatusHelper.ValueUnit(ttxLsrOnChB))
            elements.append(StatusHelper.Line("Laser State", values))

            values = []
            values.append(StatusHelper.ValueUnit(ttxLsrBiasChB,"mA", label=" Bias"))
            values.append(StatusHelper.ValueUnit(ttxLsrPwrChB,"mW", label=" Power"))
            values.append(StatusHelper.ValueUnit(ttxLsrTmpChB,"C", label=" Temp"))
            elements.append(StatusHelper.Line("Laser Values", values))

            values = []
            values.append(StatusHelper.ValueUnit(fr1p5VChB,"V", label=" FR 1.5"))
            values.append(StatusHelper.ValueUnit(fr1p8VChB,"V", label=" FR 1.8"))
            elements.append(StatusHelper.Line("Voltages", values))

            elements.append( StatusHelper.Separator("Channel C") )

            values = []
            values.append(StatusHelper.ValueUnit(ttxLsrOnChC))
            elements.append(StatusHelper.Line("Laser State", values))

            values = []
            values.append(StatusHelper.ValueUnit(ttxLsrBiasChC,"mA", label=" Bias"))
            values.append(StatusHelper.ValueUnit(ttxLsrPwrChC,"mW", label=" Power"))
            values.append(StatusHelper.ValueUnit(ttxLsrTmpChC,"C", label=" Temp"))
            elements.append(StatusHelper.Line("Laser Values", values))

            values = []
            values.append(StatusHelper.ValueUnit(fr1p5VChC,"V", label=" FR 1.5"))
            values.append(StatusHelper.ValueUnit(fr1p8VChC,"V", label=" FR 1.8"))
            elements.append(StatusHelper.Line("Voltages", values))

            elements.append( StatusHelper.Separator("Channel D") )

            values = []
            values.append(StatusHelper.ValueUnit(ttxLsrOnChD))
            elements.append(StatusHelper.Line("Laser State", values))

            values = []
            values.append(StatusHelper.ValueUnit(ttxLsrBiasChD,"mA", label=" Bias"))
            values.append(StatusHelper.ValueUnit(ttxLsrPwrChD,"mW", label=" Power"))
            values.append(StatusHelper.ValueUnit(ttxLsrTmpChD,"C", label=" Temp"))
            elements.append(StatusHelper.Line("Laser Values", values))

            values = []
            values.append(StatusHelper.ValueUnit(fr1p5VChD,"V", label=" FR 1.5"))
            values.append(StatusHelper.ValueUnit(fr1p8VChD,"V", label=" FR 1.8"))
            elements.append(StatusHelper.Line("Voltages", values))

            elements.append( StatusHelper.Separator("TE Status") )

            values = []
            values.append(StatusHelper.ValueUnit(frTeOk,label=" TE"))
            values.append(StatusHelper.ValueUnit(frKeepalive,label=" KeepAlive"))
            elements.append(StatusHelper.Line("State", values))

            values = []
            values.append(StatusHelper.ValueUnit(fr125miss,label=" Present"))
            values.append(StatusHelper.ValueUnit(fr125lock))
            elements.append(StatusHelper.Line("125Mhz State", values))

            values = []
            values.append(StatusHelper.ValueUnit(fr250lock))
            elements.append(StatusHelper.Line("250Mhz State", values))

            elements.append( StatusHelper.Separator("TE Errors") )
            elements.append(StatusHelper.Line("TE Errors:",[StatusHelper.ValueUnit(frTeStClkEdge,label="Edge"),
                                                            StatusHelper.ValueUnit(frTeStCurrErr,label="Curr Err"),
                                                            StatusHelper.ValueUnit(frTeStEarlyErr,label="Early"),
                                                            StatusHelper.ValueUnit(frTeStLateErr,label="Late")]))

            #frame = StatusHelper.Frame(compName + "   Ant: " + antName,[lala,lKeepAlive,lteSynced])
            frame = StatusHelper.Frame(compName + "   Ant: " + antName,elements)
            frame.printScreen()

            '''
            print "%4s - DTX%i:                 DG PS: %3s" %(antennaName,count,j)
            print "      1.5V B: %4.2f V         1.8V B: %4.2f V          48V: %3s "  %(a[0],b[0],c48)
            print "           C: %4.2f V              C: %4.2f V         3.3V: %4.2f V " %(a[1],b[1],c[1])
            print "           D: %4.2f V              D: %4.2f V           5V: %4.2f V \n" %(a[2],b[2],c[2])
            print "  LsrBias: B: %4.1f [mA]   LsrPwr: B: %4.2f [mW] LsrTmp: B: %4.1f [C] LsrOn: B: %s " %(e[2],f[2],g[2],h[2])
            print "           C: %4.1f [mA]           C: %4.2f [mW]         C: %4.1f [C]        C: %s " %(e[1],f[1],g[1],h[1])
            print "           D: %4.1f [mA]           D: %4.2f [mW]         D: %4.1f [C]        D: %s" %(e[0],f[0],g[0],h[0])
            print "       TE: %3s      KeepAlv: %3s    125MHz: %4s %6s   250MHz: %6s" %(TE,KA,i125,cl125,cl250)
            print " TE: Edge: %4s  ERROR:Curr: %2i      Early: %2i             Late: %2i" %(edge,p[1],p[3],p[2])   
            print "        TEST MODES:     RNG: OFF        CW: Normal\n"
            #Proper status has to be get with the CCL commands once they work, all RNG OFF => OFF, all cw Normal => normal"
            count=count+1
            '''
