//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <PDAImpl.h>
#include <sstream>
#include <loggingMACROS.h> // for AUTO_TRACE
// For the container services.
#include <maciContainerServices.h>
#include <xmlTmcdbComponent.h>
#include <configDataAccess.h>


PDAImpl::PDAImpl(const ACE_CString& name, maci::ContainerServices* cs):
    PDABase(name, cs)
{
}

PDAImpl::~PDAImpl()
{
}

/**
 *----------------------------
 * Hardware Lifecycle Methods
 *----------------------------
 */
void PDAImpl::hwInitializeAction()
{
    // Load the configuration data from the TMCDB.
    getConfigurationData();

    // Call the base class implementation.
    PDABase::hwInitializeAction();
}

void PDAImpl::hwOperationalAction()
{
    // Call the base class implementation
    PDABase::hwOperationalAction();

    // Set the PDA to operational mode
    setCntlStatus(0);

    std::ostringstream msg;
    //Checking EDFA Mode
    if(m_edfaMode == 0)
    {
        // Setting current mode:
        setCntlEdfaMode(m_edfaMode);

        //Setting LD1 and LD2 values
        setCntlLd1Current(m_currentLD1);
        setCntlLd2Current(m_currentLD2);

        msg << "EDFA mode: constant current, LD1 = "
            << m_currentLD1
            << ", LD2 = "
            << m_currentLD2;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    if(m_edfaMode == 1)
    {
        // Setting constant power mode:
        setCntlEdfaMode(m_edfaMode);

        // Setting power:
        setCntlEdfaPower(m_power);

        msg << "EDFA mode: constant power = "
            << m_power;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    if(m_edfaMode == 2)
    {
        // Seting gain mode:
        setCntlEdfaMode(m_edfaMode);

        msg << "EDFA mode: constant gain = "
            << m_gain;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    if((m_edfaMode < 0) && (m_edfaMode > 2))
    {
        LOG_TO_DEVELOPER(LM_DEBUG, "EDFA mode: Not Using TMCDB "
            "configuration!");
    }
}

void PDAImpl::getConfigurationData()
{
    // The serial number got cached as part of hwConfigureAction in the base
    //class
    const std::string deviceID(HardwareDeviceImpl::getSerialNumber());
    // Get the schema and the instance file for this device
    XmlTmcdbComponent tmcdb(getContainerServices());

    const std::string xsd(tmcdb.getConfigXsd("PDA"));
    const std::string xml(tmcdb.getConfigXml(deviceID));

    std::ostringstream msg;
    if(xml.empty() == true)
    {
        // No configuration available for selected device.
        // Log a warning and return.
        msg << "Could not access config data for PDA with S/N 0x"
            << deviceID
            << ".";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());

        return;
    }
    else if(xsd.size() == 0)
    {
        // no schema available for selected device
        // log a warning and return
        LOG_TO_DEVELOPER(LM_WARNING, "Could not access schema PDA.xsd in the "
            "$TMCDB_DATA directory.");

        return;
    }

    try
    {
        ConfigDataAccessor* configData(new ConfigDataAccessor(xml, xsd));

        // Setting unreal EDFA mode
        m_edfaMode = -1;

        msg.str("");

        // Reading EDFA mode
        std::auto_ptr< const ControlXmlParser > modeElement(
            configData->getElement("edfaMode"));
        const std::string mode((*modeElement).getStringAttribute("value"));
        if(mode == "current")
        {
            // Reading values
            std::auto_ptr< const ControlXmlParser > currentElement(
                configData->getElement("current"));
            m_edfaMode = 0;
            m_currentLD1 = static_cast< double >(
                (*currentElement).getDoubleAttribute("LD1"));
            m_currentLD2 = static_cast< double >(
                (*currentElement).getDoubleAttribute("LD2"));

            // Logs
            msg.str("");
            msg << "Device S/N 0x"
                << deviceID
                << " will use EDFA mode: \""
                << mode
                << "\", LD1 = "
                << m_currentLD1
                << ", LD2 = "
                << m_currentLD2
                << ".";
        }
        else if(mode == "power")
        {
            // Reading values
            std::auto_ptr< const ControlXmlParser > powerElement(
                configData->getElement("power"));
            m_edfaMode = 1;
            m_power = static_cast< double >(
                (*powerElement).getDoubleAttribute("value"));

            // Logs
            msg.str("");
            msg << "Device S/N 0x"
                << deviceID
                << " will use EDFA mode: \""
                << mode
                << "\", power = "
                << m_power
                << ".";
        }
        else if(mode == "gain")
        {
            // Reading values:
            std::auto_ptr< const ControlXmlParser > gainElement(
                configData->getElement("gain"));
            m_edfaMode = 2;
            m_gain = static_cast< double >(
                (*gainElement).getDoubleAttribute("value"));

                // Logs
                msg.str("");
                msg << "Device S/N 0x"
                    << deviceID
                    << " will use EDFA mode: \""
                    << mode
                    << "\", gain = "
                    << m_gain
                    << ".";
        }
        else
        {
            msg << "Could not determine the EDFA mode!";
        }

        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

    }
    catch(const ControlExceptions::XmlParserErrorExImpl& ex)
    {
        ControlExceptions::XmlParserErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        msg.str("");
        msg << "Failure while accessing config data for S/N 0x"
            << deviceID;
        nex.addData("Detail", msg.str());
        nex.log();

        LOG_TO_OPERATOR(LM_WARNING, msg.str());

        throw nex;
    }
}

long long unsigned PDAImpl::rawToWorldVendorSN(
    const std::vector< unsigned int >& data) const
{
    const unsigned long long ret(data[0] * 100000ULL + data[1]);
    return ret;
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(PDAImpl)
