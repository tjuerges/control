#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


"""
This module is part of the Control Command Language.
It contains complementary functionality for PDA devices.
"""

import CCL.PDA

class LFRD(CCL.PDA.PDA):
    '''
    The Low Frequency Reference Distributor class inherits from the
    PDA implementation and refines it for the LFRD device.
    '''
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The constructor creates a LFRD object using default constructor.
        EXAMPLE:
        import CCL.LFRD
        lfrd = CCL.LFRD.LFRD()
        '''
        try:
            if componentName is None:
                componentName = "CONTROL/AOSTiming/LFRD"
            CCL.PDA.PDA.__init__(self, componentName, stickyFlag)
        except Exception, e:
            print 'LFRD component is not running \n' + str(e)

    def __del__(self):
        CCL.PDA.PDA.__del__(self)

    def _getStatusLabel(self):
        return "Low Frequency Reference Distributor (LFRD)"
