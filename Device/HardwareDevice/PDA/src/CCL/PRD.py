#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


"""
This module is part of the Control Command Language.
It contains complementary functionality for PDA devices.
"""

import CCL.PDA
from ControlExceptions import IllegalParameterErrorEx

class PRD(CCL.PDA.PDA):
    '''
    The Photonic Refernce Distributor  class inherits from the
    PDA implementation and refines it for the PRD device.
    '''
    def __init__(self, photonicReference = None,
                 componentName = None, stickyFlag = False):
        '''
        The constructor creates a PRD object using default constructor.
        You can specify which PRD you would like either by number (1-6)
        or by PhotonicReference (i.e. PhotonicReference2)
        EXAMPLE:
        import CCL.PRD
        # To get the PRD associated with photonic reference 3
        prd = CCL.PRD.PRD(3)
        or 
        prd = CCL.PRD.PRD("PhotonicReference3")
        '''

        if componentName is None:
            if photonicReference is None:
                raise IllegalParameterErrorEx('You must specify either a '
                                              + ' photonic reference');
            if isinstance(photonicReference, int):
                photonicReference= 'PhotonicReference%d' % photonicReference

            componentName = "CONTROL/CentralLO/%s/PRD" % photonicReference

        name = componentName.split('/')[2]
        self._prdNo = int(name[len(name)-1])

        try:
            CCL.PDA.PDA.__init__(self, componentName, stickyFlag)
        except Exception, e:
            print 'PRD component is not running \n' + str(e)

    def __del__(self):
        CCL.PDA.PDA.__del__(self)

    def _getStatusLabel(self):
        return  "Photonic Reference Distributor %d (PRD)" % self._prdNo
