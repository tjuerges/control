#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


"""
This module is part of the Control Command Language.
It contains complementary functionality for PDA devices.
"""

import CCL.PDABase
from CCL import StatusHelper
from ControlExceptions import IllegalParameterErrorEx

class PDA(CCL.PDABase.PDABase):
    '''
    The PDA class inherits from the code generated PDABase
    class and adds specific methods.
    '''
    def __init__(self, componentName = None, stickyFlag = False):
        '''
        The constructor creates a PDA object using PDABase constructor.
        EXAMPLE:
        import CCL.PDA
        mld = CCL.PDA.PDA("CONTROL/CentalLO/MLD")
        prd3 = CCL.PDA.PDA("CONTROL/CentalLO/PhotonicReference3/PRD")
        '''
        try:
            # The PDAs are not assigned to any antenna but to the CentralLO.
            # Therefore the antennaName is set to None.
            CCL.PDABase.PDABase.__init__(self, \
                                         antennaName = None, \
                                         componentName = componentName, \
                                         stickyFlag = stickyFlag)
        except Exception, e:
            print 'PDA component is not running \n' + str(e)
        pass

    def __del__(self):
        CCL.PDABase.PDABase.__del__(self)

    def setEDFAMode(self, mode):
        '''
        This is a helper method to allow the modification of the operating
        mode of the EDFA.  It allows either string or numeric input.
        Valid Options are:
        0, CC, Constant current, current
        1, CP, Constant Output Power, power
        2, CG, Constant Gain, gain
        3, Off

        Exmples:
        pda.setEDFAMode(1) # Set the EDFA to Constant Power Mode
        pda.setEDFAMode("CG") # Set the EDFA to constant gain mode
        pda.setEDFAMode("gain") # Same as above
        pda.setEDFAMode("Constant Output Power") # for the very verbose
        '''

        if isinstance(mode, str):
            if mode.upper() == 'CC' or mode.upper() == 'CURRENT' or \
                   mode.upper() == 'CONSTANT CURRENT':
                mode = 0
            elif mode.upper() == 'CP' or mode.upper() == 'POWER' or \
                   mode.upper() == 'CONSTANT OUTPUT POWER':
                mode = 1
            elif mode.upper() == 'CG' or mode.upper() == 'GAIN' or \
                   mode.upper() == 'CONSTANT GAIN':
                mode = 2
            elif mode.upper() == 'Off':
                mode = 3
            else:
                raise IllegalParameterErrorEx('The mode ' + mode +
                                              ' is not recognized')

        if isinstance(mode, int):
            return self.SET_EDFA_MODE(mode)
        else:
            raise IllegalParameterErrorEx('Illegal entry parameter must be' +
                                          ' either an integer or a string')



    def _getStatusLabel(self):
        '''
        This abstract method should be over ridden with the proper
        label for the status text.
        '''
        return "PDA Generic (Should be overridden)"
        
    def _getStatusBlock(self, device):
        '''
        This method returns the default status section for the display
        it can be overridden as neccessary by the derived methods.
        '''
        elements = []

        try:
            if device.GET_MODULE_MODE_STATUS()[0]:
                label = 'Mode: Standby  '
            else:
                label = 'Mode: Operative'
        except:
            label =     'Mode:  NA      '

        values = []
        for alarm in [('Fault',device.GET_FAULT_STATUS),
                      ('Mute Mode',device.GET_MUTE_MODE_STATUS),
                      ('Interlock',device.GET_KEY_SWITCH_STATUS)]:
            try:
                if alarm[1]()[0]:
                    values.append(StatusHelper.ValueUnit('T', label=alarm[0]))
                else:
                    values.append(StatusHelper.ValueUnit('F', label=alarm[0]))
            except:
                    values.append(StatusHelper.ValueUnit('NA', label=alarm[0]))
        elements.append(StatusHelper.Line(label, values))


        alarmLines = []
        values = []
        for alarm in [(' In',device.GET_INPUT_POWER_LOS_STATUS),
                      ('Out',device.GET_OUTPUT_POWER_LOS_STATUS)]:
            try:
                if alarm[1]()[0]:
                    values.append(StatusHelper.ValueUnit('T', label=alarm[0]))
                else:
                    values.append(StatusHelper.ValueUnit('F', label=alarm[0]))
            except:
                    values.append(StatusHelper.ValueUnit('NA', label=alarm[0]))
        values.append(StatusHelper.ValueUnit('       '))
        values.append(StatusHelper.ValueUnit('       '))
        alarmLines.append(StatusHelper.Line("Low Signal Level",values))

        
        values = []
        for alarm in [('LD1',device.GET_LD1_TEMP_ALARM),
                      ('LD2',device.GET_LD2_TEMP_ALARM),
                      ('LD3',device.GET_LD3_TEMP_ALARM),
                      ('EDFA',device.GET_EDFA_UNIT_OVERTEMP_STATUS)]:
            try:
                if alarm[1]()[0]:
                    values.append(StatusHelper.ValueUnit('T', label=alarm[0]))
                else:
                    values.append(StatusHelper.ValueUnit('F', label=alarm[0]))
            except:
                    values.append(StatusHelper.ValueUnit('NA', label=alarm[0]))
        alarmLines.append(StatusHelper.Line("Temperature", values))

        values = []
        for alarm in [('LD1',device.GET_LD1_CURRENT_ALARM),
                      ('LD2',device.GET_LD2_CURRENT_ALARM),
                      ('LD3',device.GET_LD3_CURRENT_ALARM)]:
            try:
                if alarm[1]()[0]:
                    values.append(StatusHelper.ValueUnit('T', label=alarm[0]))
                else:
                    values.append(StatusHelper.ValueUnit('F', label=alarm[0]))
            except:
                    values.append(StatusHelper.ValueUnit('NA', label=alarm[0]))

        values.append(StatusHelper.ValueUnit('       '))
        alarmLines.append(StatusHelper.Line("LD Current", values))

        elements.append(StatusHelper.Group("Alarms", alarmLines))


        signalLines = []
        values = []
        for signal in [('Input', device.GET_OPT_IN),
                       ('Output', device.GET_OPT_OUT)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f"%signal[1]()[0],
                                                     'dBm', signal[0]))
            except:
                values.append(StatusHelper.ValueUnit("NA", 'dBm', signal[0]))
        
        signalLines.append(StatusHelper.Line("Current Level ", values))

        values = []
        for signal in [('Input', device.GET_INPUT_LOS_THRESHOLD),
                       ('Output', device.GET_OUTPUT_LOS_THRESHOLD)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f"%signal[1]()[0],
                                                     'dBm', signal[0]))
            except:
                values.append(StatusHelper.ValueUnit("NA", 'dBm', signal[0]))

        signalLines.append(StatusHelper.Line("Threshold Level", values))
        elements.append(StatusHelper.Group("Signal Levels",signalLines))
        
        return elements
                        
    def _getLDBlock(self, device):
        '''
        This method returns a group containing information about the Laser
        Diodes.
        '''
        lines = []

        values = []
        for diode in [("LD1", device.GET_LD1_OUTPUT),
                      ("LD2", device.GET_LD2_OUTPUT),
                      ("LD3", device.GET_LD3_OUTPUT)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f" % diode[1]()[0],
                                                     'mw', diode[0]))
            except:
                values.append(StatusHelper.ValueUnit("NA", 'mw', diode[0]))
                
        lines.append(StatusHelper.Line("Output Power", values))


        values = []
        for diode in [("LD1", device.GET_LD1_CURRENT),
                      ("LD2", device.GET_LD2_CURRENT),
                      ("LD3", device.GET_LD3_CURRENT)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f" %
                                                     (diode[1]()[0]),
                                                     'ma', diode[0]))
            except:
                values.append(StatusHelper.ValueUnit("NA", 'ma', diode[0]))
                
        lines.append(StatusHelper.Line("Current", values))

        values = []
        for diode in [("LD1", device.GET_LD1_PELTIER),
                      ("LD2", device.GET_LD2_PELTIER),
                      ("LD3", device.GET_LD3_PELTIER)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f" %
                                                     (diode[1]()[0] * 1e3),
                                                     'ma', diode[0]))
            except:
                values.append(StatusHelper.ValueUnit("NA", 'ma', diode[0]))
                
        lines.append(StatusHelper.Line("Peltier Cur.", values))


        values = []
        for diode in [("LD1", device.GET_LD1_TEMP),
                      ("LD2", device.GET_LD2_TEMP),
                      ("LD3", device.GET_LD3_TEMP)]:
            try:
                values.append(StatusHelper.ValueUnit("%2.2f" %
                                                     (diode[1]()[0] -272.15),
                                                     'C', diode[0]))
            except:
                values.append(StatusHelper.ValueUnit("NA", 'C', diode[0]))
                
        lines.append(StatusHelper.Line("Temperature", values))

        return StatusHelper.Group("Pump Laser Diodes", lines)

    def _getEDFABlock(self, device):
        '''
        This method returns the Status Block for the EDFA
        '''
        lines = []

        try:
            mode = device.GET_EDFA_MODE()[0]
            print " Mode is: ", mode
            if mode == 0:
                label = "Mode:  Constant Current"
                value = []
            elif mode == 1:
                label = "Mode:  Constant Power"
                print label
                try:
                    value = [StatusHelper.ValueUnit("%2.2f" %
                                            device.GET_EDFA_CP_SETTING()[0],
                                                    'dBm', 'Set Value')]
                except:
                    value = [StatusHelper.ValueUnit("NA", 'dBm', 'Set Value')]
            elif mode == 2:
                label = "Mode:  Constant Gain"
                try:
                    value = [StatusHelper.ValueUnit("%2.2f" %
                                           device.GET_EDFA_CG_SETTING()[0],
                                                    'dB', 'Set Value')]
                except:
                    value = [StatusHelper.ValueUnit("NA", 'dB', 'Set Value')]
            elif mode == 3:
                label = "Mode:  Off"
                value = []
        except:
            label = "Mode:  NA"
            value = []
        lines.append(StatusHelper.Line(label, value))

        try:
            lines.append(StatusHelper.Line("Measured Gain",
             [StatusHelper.ValueUnit("%2.2f" % device.GET_EDFA_GAIN()[0],
                                      'dB')]))
        except:
            lines.append(StatusHelper.Line("Measured Gain",
              [StatusHelper.ValueUnit("NA", 'dB')]))

        try:
            lines.append(StatusHelper.Line("Temperature",
                 [StatusHelper.ValueUnit("%2.2f" % device.GET_TEMP()[0],'C')]))
        except:
            lines.append(StatusHelper.Line("Temperature",
                 [StatusHelper.ValueUnit("NA", 'C')]))
        return StatusHelper.Group("Erbium-doped Fiber Amplifier", lines)

        

    def STATUS(self):
        '''
        Method to print relevant status information to the screen.

        Get component name, no antenna name since it is not assigned to
        an antenna.
        '''
        for key, val in self._devices.iteritems():
            elements = []

            # Do the Status Section
            for line in self._getStatusBlock(self._devices[key]):
                elements.append( line )
        
            elements.append(self._getLDBlock(self._devices[key]))
            elements.append(self._getEDFABlock(self._devices[key]))

            statusFrame = StatusHelper.Frame(self._getStatusLabel() , elements)
            statusFrame.printScreen()

