#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef PDAImpl_h
#define PDAImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


// Base class
#include <PDABase.h>
#include <PDAS.h>
#include <vector>


// Forward declaration of maci::ContainerServices.
namespace maci
{
    class ContainerServices;
};


class PDAImpl: public PDABase, public virtual POA_Control::PDA
{
    public:
    /// Constructor.
    PDAImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /// Destructor.
    virtual ~PDAImpl();


    protected:
    /// Control hardware lifecycle method.
    virtual void hwInitializeAction();
    virtual void hwOperationalAction();

    virtual long long unsigned rawToWorldVendorSN(
        const std::vector< unsigned int >& data) const;


    private:
    /// Default constructor. Never instanciate this class directly!
    PDAImpl();

    /// ALMA coding standards: copy constructor is disabled.
    PDAImpl(const PDAImpl&);

    /// ALMA coding standards: assignment operator is disabled.
    PDAImpl& operator=(const PDAImpl&);

    virtual void getConfigurationData();

    /// EDFA Mode = 0, 1, 2.
    int m_edfaMode;

    double  m_currentLD1;

    double  m_currentLD2;

    double  m_power;

    double  m_gain;
};
#endif
