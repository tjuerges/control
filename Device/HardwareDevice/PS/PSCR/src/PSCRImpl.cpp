/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */

/**
 * <P>
 * 		PSCR Power Supply Unit Implementation
 * <P>
 *
 * This is the Common class for all the others real implementations of power supplies.
 * @see  PSCRBase
 */

#include <PSCRImpl.h>

using std::string;

/*------------PSCR Constructor-----------------*/
PSCRImpl::PSCRImpl(const ACE_CString& name, maci::ContainerServices* cs) :
    PSCRBase(name, cs), updateThreadCycleTime_m(10000000), testModeState(false) {
    //Monitoring done each 1 second. Since each unit is 100E-9 sec, we need 10000000 units :)

    ACS_TRACE("PSCRImpl::PSCRImpl");

    alarmHandlerPSCR_m=new AlarmHandler();
}

/*------------PSCR Destructor-------------------*/
PSCRImpl::~PSCRImpl() {
	ACS_TRACE("PSCRImpl::~PSCRImpl");
	delete(alarmHandlerPSCR_m);
}


/**
 * --------------------------------------------------------------
 *      Component Lifecycle Methods
 * --------------------------------------------------------------
 */

void PSCRImpl::initialize() {

    ACS_TRACE("PSCRImpl::initialize");

    try {
        PSCRBase::initialize(); //Initialize call to the base class
    } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
	throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSCRImpl::initialize");
    }

    const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    std::string threadName(compName.in());
    threadName += "MonitoringThread";
    updateThread_p = getContainerServices()->getThreadManager()->create<UpdateThread,PSCRImpl>(
        threadName.c_str(), *this
    );

    //The thread sleeptime is 1 sec
    updateThread_p->setSleepTime(updateThreadCycleTime_m);

    alarmHandlerPSCR_m->addAlarmToList(
        new FloatAlarm(1,
                       "PSCR",
                       getComponent()->getName(),
                       "PSCR is about to be shutdown due to overtemperature",
                       new GetTemperatureCelsiusCommand(this),
                       0,
                       PSCRImpl::HW_SHUTDOWN_TEMP*0.97)
    );
}

void PSCRImpl::cleanUp() {

    ACS_TRACE("PSCRImpl::cleanUp");

    updateThread_p->terminate();//Terminates the Thread

    try {
        // Really calling PSUImpl.cleanUp()
        PSCRBase::cleanUp();
    } catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
	throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSCRImpl::cleanUp");
    }
}

/*
 * ------------------------------
 * Hardware Lifecycle Methods. States
 * ------------------------------
 */

void PSCRImpl::hwInitializeAction() {

    ACS_TRACE("PSCRImpl::hwInitializeAction");

    alarmHandlerPSCR_m->clearAllAlarmFlags();
    PSCRBase::hwInitializeAction();
    updateThread_p->suspend(); //suspend the execution of the Thread
}

void PSCRImpl::hwOperationalAction() {

    ACS_TRACE("PSCRImpl::hwOperationalAction");

    PSCRBase::hwOperationalAction();
    updateThread_p->resume(); //Here we continue the execution of the resumed thread
}

void PSCRImpl::hwStopAction() {

    ACS_TRACE("PSCRImpl::hwStopAction");

    //we suspend the monitor control point  thread
    updateThread_p->suspend();
    //Here we clean up PSCR alarms
    alarmHandlerPSCR_m->clearAllAlarmFlags();
    //and call stop on the base class :) This call cleans up PSUImpl alarms
    PSCRBase::hwStopAction();
}

void PSCRImpl::processStatusMonitors() {

    ACS_TRACE("PSCRImpl::processStatusMonitors");

    const string fnName = "PSCRImpl::processStatusMonitors()";

    if (isReady() == false) {
	ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
	"PSCRBase::processStatusMonitors()");
	ex.addData("ErrorCode", static_cast< int>(ControlExceptions::INACTError));
	throw ex;
    }
    PSUImpl::handler_m->evaluateAlarmStateList();
    PSCRImpl::alarmHandlerPSCR_m->evaluateAlarmStateList();
}

//This method will be called in a loop by the Thread
void PSCRImpl::updateThreadAction() {

    ACS_TRACE("PSCRImpl::updateThreadAction");

    const string fnName = "PSCRImpl::updateThreadAction";

    /* Queue Status Monitor */
    try {
	processStatusMonitors();
    } catch (const ControlExceptions::CAMBErrorExImpl& ex) {
	ostringstream message;
	message << "Received no answer or erroneus one from the CANBus.";
	ControlExceptions::CAMBErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
	localEx.addData("Detail", message.str());
        localEx.log();
    } catch(const ControlExceptions::INACTErrorExImpl& ex) {
	ostringstream message;
	message << "PSCR device is not ready. Please wait....";
	ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
	localEx.addData("Detail", message.str());
	localEx.log();
    } catch(...) {
        ACS_TRACE("PSCRImpl::updateThreadAction() caught an unknown exception.");
    }
}

/* ---------> Thread Monitor Constructor*/
PSCRImpl::UpdateThread::UpdateThread(const ACE_CString& name, PSCRImpl& PSCR) :
    ACS::Thread(name), pscrDevice_p(PSCR) {

    ACS_TRACE("PSCRImpl::UpdateThread::UpdateThread()");
}

/* ---------> runLoop implementation */
void PSCRImpl::UpdateThread::runLoop() {

    ACS_TRACE("PSCRImpl::UpdateThread::runLoop()");

    pscrDevice_p.updateThreadAction();
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(PSCRImpl)


