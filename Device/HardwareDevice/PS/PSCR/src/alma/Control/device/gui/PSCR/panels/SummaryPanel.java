/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSCR.panels;

import alma.Control.device.gui.PSCR.presentationModels.DevicePM;
import alma.Control.device.gui.PSU.IcdControlPoints;
import alma.Control.device.gui.PSU.IcdMonitorPoints;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanToggleWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Summary data.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author Steve Harrington  sharring@nrao.edu
 * @author Justin Kenworthy	 jkenwort@nrao.edu
 * 
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public SummaryPanel(Logger logger, DevicePM dPM) {
    	super(logger, dPM);
    	buildPanel();
    }
    
    protected void buildPanel() {
        final String [][] labelGrid = {
        	{null, "Modules", "Current Limits"},
        	{"Shutdown Commanded", "AC Status", "Voltage Limits"},
        	{"Global", "DC Status", "Over Temperature"},
        	{null, "Fan", "Fan Warning"}
        };

        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                (devicePM.getDeviceDescription()),BorderFactory.createEmptyBorder(1,1,1,1)));
        
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        
        c.ipadx = 5;
        c.ipady = 5;
        
        //Add label grid
        for (int i = 0; i < labelGrid.length; i++) {        	
        	c.gridy = i;
        	c.gridx = 1;
        	
        	for (int j = 0; j < labelGrid[i].length; j++) {
        		if (labelGrid[i][j] != null) {
        			add(new JLabel(labelGrid[i][j]), c);
        		}
        		c.gridx += 2;  //Leave space for monitor point
        	}	        
	    } 
        
        c.gridy = 0;        
        c.gridx = 0;
        /* Outputs */
        add(new BooleanMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.PS_SHUTDOWN),
                Status.GOOD,
                "Outputs On",
                Status.OFF,
                "Outputs Shutdown"),
        	c
        );

        c.gridx = 1;
        /* Output Control Button */
        // TODO: Chose new labels that make sense in context.  User testing shows these labels are BAD!
        add(new BooleanToggleWidget(
        		logger,
        		devicePM.getMonitorPointPM(IcdMonitorPoints.PS_SHUTDOWN),
        		devicePM.getControlPointPM(IcdControlPoints.SHUTDOWN),
        		"Enable Outputs",
        		"Disable Outputs"),
        	c
        );
        
        c.gridx = 2; 
        /* Modules */
        add(new BooleanMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.MODULES_ALL_OK), 
                Status.FAILURE,
                "At least one module reports an error",
                Status.GOOD,
                "All modules OK"),
        	c
        );   
        
        c.gridx = 4; 
        /* Current Limits */
        add(new BooleanMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.CURRENT_LIMITS_OK), 
                Status.FAILURE,
                "Current Limits Exceeded",
                Status.GOOD,
                "Current Limits OK"),
        	c
        );
                     
        c.gridy = 1;
        c.gridx = 0; 
        /* Shutdown Command Received */
        add(new BooleanMonitorPointWidget(
                logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.SHUTDOWN_CMD_RECEIVED), 
                Status.GOOD,
                "Shutdown command not received",
                Status.WARNING,
                "Shutdown command received"),
        	c
        );
        
              
        c.gridx = 2;
        /* AC Status */
        add(new BooleanMonitorPointWidget(
                logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.AC_STATUS_OK), 
                Status.FAILURE,
                "AC Status not OK",
                Status.GOOD,
                "AC Status OK"),
        	c
        );
              
        c.gridx = 4; 
        /* Voltage Limits */
        add(new BooleanMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.VOLTAGE_LIMITS_OK), 
                Status.FAILURE,
                "Voltage Limits Exceeded",
                Status.GOOD,
                "Voltage Limits OK"),
        	c
        );        
        
        c.gridy = 2;      
        c.gridx = 0;
        /* Global */
        add(new BooleanMonitorPointWidget(
                logger, 
                devicePM.getMonitorPointPM(IcdMonitorPoints.GLOBAL_WARNING),
                Status.GOOD,
                "Power Supply OK",
                Status.WARNING,
                "Power Supply Warning"), 
            c
        );
        
        c.gridx = 2; 
        /* DC Status */
        add(new BooleanMonitorPointWidget(
                logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DC_STATUS_OK), 
                Status.FAILURE,
                "DC Status not OK",
                Status.GOOD,
                "DC Status OK"),
        	c
        );
     
        c.gridx = 4; 
        /* Over Temperature */
        add(new BooleanMonitorPointWidget(
                logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.OVER_TEMP_OK), 
                Status.FAILURE,
                "Power supply over temperature",
                Status.GOOD,
                "Temperature OK"),
        	c
        );
          
        c.gridy = 3;
        c.gridx = 2;
        /* Fan */
        add(new BooleanMonitorPointWidget(
                logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FAN_STATUS_OK), 
                Status.FAILURE,
                "Fan failure",
                Status.GOOD,
                "Fan OK"),
        	c
        );
        
        c.gridx = 4;
        /* Fan Warning */
        add(new BooleanMonitorPointWidget(
                logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.FAN_WARNING_OK), 
                Status.FAILURE,
                "Fan Warning",
                Status.GOOD,
                "Fan OK"),
        	c
        );
        
        setVisible(true);
    }
}

//
// O_o
