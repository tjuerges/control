/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSCR.panels;

import alma.Control.device.gui.PSU.IdlMonitorPoints;
import alma.Control.device.gui.PSU.IcdMonitorPoints;
import alma.Control.device.gui.PSU.IcdControlPoints;
import alma.Control.device.gui.PSCR.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.AmbsiPanel;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized in panels and tabs to group related data together. 
 * 
 * This class contains the Detail data. It also provides a Show/Hide button for the data.
 * TODO: replace PSSAS with the proper module name
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DetailsPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    JTabbedPane tabs;
    
    private ModulesPanel modulesPanel; 
    private LambdaVegaPanel lambdaVegaPanel; 
    private AmbsiPanel ambsiPanel; 

    public DetailsPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        customizeMonitorPoints();
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        modulesPanel = new ModulesPanel(logger, aDevicePM);
        lambdaVegaPanel = new LambdaVegaPanel(logger, aDevicePM);
        ambsiPanel = new AmbsiPanel(logger, aDevicePM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.SERIAL_NUMBER),
                devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.TRANS_NUM),
                devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_SLAVE_ERROR_CODE),
                devicePM.getMonitorPointPM(IcdMonitorPoints.CAN_ERROR_COUNT),
                devicePM.getMonitorPointPM(IcdMonitorPoints.ERROR_CODE_LAST_CAN_ERROR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PATCH_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MAJOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_MINOR_REV_LEVEL),
                devicePM.getMonitorPointPM(IcdMonitorPoints.PROTOCOL_PATCH_LEVEL),
                devicePM.getControlPointPM(IcdControlPoints.RESET_AMBSI));
    }

    protected void buildPanel()
    {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Details"),
                                                          BorderFactory.createEmptyBorder(1,1,1,1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.NORTHWEST;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=1;        //Setup proper sizing for the button
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(new ShowHideButton(), c);
        c.weighty=100;		//Have the tabbed panel take up the rest of the space.


        tabs = new JTabbedPane();
        tabs.add("Modules", new JScrollPane (modulesPanel));
        tabs.add("LambdaVega", new JScrollPane (lambdaVegaPanel));
        tabs.add("Device Info", new JScrollPane (ambsiPanel));

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weighty=100;
        c.weightx=100;

        c.gridy++;
        add(tabs, c);

        this.setVisible(true);
    }
    
    /**
     * This function sets any customizations needed for displaying the monitor data in a user friendly format.
     * E.g degree C instead of K.
     */
    private void customizeMonitorPoints() {
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayUnits("\u00B0C");
    }
    
    private class ShowHideButton
    extends JButton
    implements ActionListener {

        private String hideActionCommand;
        private String hideButtonText;
        private String showActionCommand;
        private String showButtonText;

        public ShowHideButton() {
            hideButtonText = "Hide Details";
            showButtonText = "Show Details";
            hideActionCommand = "HIDE_DETAILS";
            showActionCommand = "SHOW_DETAILS";

            setText(hideButtonText);
            setActionCommand(hideActionCommand);
            addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == hideActionCommand) {
                tabs.setVisible(false);
                setText(showButtonText);
                setActionCommand(showActionCommand);
            } else if (e.getActionCommand() == showActionCommand) {
                tabs.setVisible(true);
                setText(hideButtonText);
                setActionCommand(hideActionCommand);
            }
        }
    }
}

//
// O_o
