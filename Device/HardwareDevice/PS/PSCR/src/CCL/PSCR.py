#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# pburgos  2008-06-29  created
# srankin  2009-08-03  Modified for PSCR
#
import CCL.HardwareDevice

import CCL.PSCRBase
from CCL import StatusHelper
from CCL.logging import getLogger

class PSCR(CCL.PSCRBase.PSCRBase):
    '''
    The PSCRclass inherits from the code generated PSCRBase
    class and adds specific methods.
    '''
    def __init__(self, \
                 componentName = "CONTROL/AOSTiming/PSCR", \
                 stickyFlag = False):
        '''
        The constructor creates a PSCR object by making use of
        the PSCRBase constructor. The stickyFlag can be set to
        True if the component should be instantiated in a sticky
        way.
        
        There is only one instance of the PSCR in ALMA.  If you 
        do not provide a component name, you will still receive
        an PSCR component.  The option to pass a component name
        is preserved to support automatically generated tests. 
         
        EXAMPLE:
        from CCL.PSCR import PSCR
        pscr = PSCR()
        '''
        # Dictionary of device instances
        self._devices = {}

        # Group of component names.
        if (isinstance(componentName,list) == True):
            if (len(componentName) != 0):
                for idx in range (0,len(componentName)):
                    self._devices[componentName[idx]]=""
        # One component
        else:
            self._devices[componentName] = ""
        
        #PSCRBase Constructor
        CCL.PSCRBase.PSCRBase.__init__(self, None, componentName, stickyFlag)
        # Initialize the base class
        for key, val in self._devices.iteritems():
            CCL.HardwareDevice.HardwareDevice.__init__(self, key, stickyFlag);
        self._devices[key]= self._HardwareDevice__hw

        self.__logger = getLogger()
        
    def __del__(self):
        CCL.PSCRBase.PSCRBase.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the PSCR
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
        
            # Invoke the generic WCA status helper
            elements = self._PSU__STATUSHelper(key)
            
            ############################### PSCR specific ###################################################
            elements.append( StatusHelper.Separator("PSCR specific") )
            #GET_MID_1_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_1_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 voltage",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_1_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_1_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 current",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_1_MAX_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_1_MAX_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 max voltage seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_1_MIN_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_1_MIN_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 min voltage seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_1_MAX_CURRENT_SEEN()
            try:
               value,t = self._devices[key].GET_MID_1_MAX_CURRENT_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 max current seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_3_VOLTAGE() 
            try:
               value,t = self._devices[key].GET_MID_3_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 3 voltage",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_3_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_3_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 3 current",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_3_MAX_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_3_MAX_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 3 max voltage seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_3_MIN_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_3_MIN_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 3 min voltage seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_3_MAX_CURRENT_SEEN()
            try:
               value,t = self._devices[key].GET_MID_3_MAX_CURRENT_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 3 max current seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_4_VOLTAGE() 
            try:
               value,t = self._devices[key].GET_MID_4_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 4 voltage",[StatusHelper.ValueUnit(value)] ))
             #GET_MID_4_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_4_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 4 current",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_4_MAX_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_4_MAX_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 4 max voltage seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_4_MIN_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_4_MIN_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 4 min voltage seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_4_MAX_CURRENT_SEEN()
            try:
               value,t = self._devices[key].GET_MID_4_MAX_CURRENT_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 4 max current seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_5_VOLTAGE() 
            try:
               value,t = self._devices[key].GET_MID_5_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 5 voltage",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_5_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_5_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 5 current",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_5_MAX_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_5_MAX_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 5 max voltage seen",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_5_MIN_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_5_MIN_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 5 min voltage seen",[StatusHelper.ValueUnit(value)] ))
             #GET_MID_5_MAX_CURRENT_SEEN()
            try:
               value,t = self._devices[key].GET_MID_5_MAX_CURRENT_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 5 max current seen",[StatusHelper.ValueUnit(value)] ))


            ############## LAST SHUTDOWN STATE ####################
            elements.append( StatusHelper.Separator("State at last Shutdown") )
            #GET_MID_1_SHUTDOWN_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_1_SHUTDOWN_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 shutdown voltage.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_MID_1_SHUTDOWN_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_1_SHUTDOWN_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 shutdown current.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_MID_3_SHUTDOWN_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_3_SHUTDOWN_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 3 shutdown voltage.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_MID_3_SHUTDOWN_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_3_SHUTDOWN_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 3 shutdown current.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_MID_4_SHUTDOWN_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_4_SHUTDOWN_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 4 shutdown voltage.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_MID_4_SHUTDOWN_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_4_SHUTDOWN_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 4 shutdown current.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_MID_5_SHUTDOWN_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_5_SHUTDOWN_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 5 shutdown voltage.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_MID_5_SHUTDOWN_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_5_SHUTDOWN_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 5 shutdown current.",[StatusHelper.ValueUnit(value)] ))
            
            #GET_AMBIENT_TEMP_AT_SHUTDOWN()
            try:
               value,t = self._devices[key].GET_SHUTDOWN_AMBIENT_TEMPERATURE()
               temperature = (value[0]+value[1]*(2**8))/2.0 + 0.25*(value[3]-value[2])/(value[3]*1.0)
            except:
               temperature = "N/A"
            elements.append( StatusHelper.Line("temp. at shutdown.",[StatusHelper.ValueUnit(temperature)] ))

            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
        #TODO  __del__ method


