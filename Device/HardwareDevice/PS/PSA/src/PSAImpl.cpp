/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PSAImpl.cpp
 *
 *
 * $Id$
 *
 *
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/05/14	PSAImpl.cpp created
 *	pburgos		2008/06/10	Alarms Added
 *------------------------------------------------------------
 */

/**
 * <P>
 * 		PSA Power Supply Unit Implementation
 * <P>
 *
 * This is the Common class for all the others real implementations of power supplies.
 * @see  PSABase
 */

#include <PSAImpl.h>

using std::string;

/*------------PSA Constructor-----------------*/

PSAImpl::PSAImpl(const ACE_CString& name, maci::ContainerServices* cs) :
	PSABase(name, cs), updateThreadCycleTime_m(50000000), testModeState(false)
	{//Monitoring done each 5 second. Since each unit is 100E-9 sec, we need 50000000 units :)
	ACS_TRACE("PSAImpl::PSAImpl");
	alarmHandlerPSA_m=new AlarmHandler();
}

/*------------PSA Destructor-------------------*/

PSAImpl::~PSAImpl() {
	ACS_TRACE("PSAImpl::~PSAImpl");
	delete(alarmHandlerPSA_m);
}
/**
 * --------------------------------------------------------------
 *      Component Lifecycle Methods
 * --------------------------------------------------------------
 */

//---------->   Initialize

void PSAImpl::initialize() {
	ACS_TRACE("PSAImpl::initialize");
	try {
		PSABase::initialize(); //Initialize call to the base class
	} catch (const acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
		throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSAImpl::initialize");
	}

	const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
	std::string threadName(compName.in());
	threadName += "MonitoringThread";
	updateThread_p = getContainerServices()->getThreadManager()->create<UpdateThread,PSAImpl>(
	    threadName.c_str(), *this);
	updateThread_p->setSleepTime(updateThreadCycleTime_m); //The thread sleeptime is 1 sec

	alarmHandlerPSA_m->addAlarmToList(new FloatAlarm(1,
									"PSA",
									getComponent()->getName(),
									"PSA is about to be shutdown due to overtemperature",
									new GetTemperatureCelsiusCommand(this),
									0,
									PSAImpl::HW_SHUTDOWN_TEMP*0.997));
} //end initialize()


//--------->  CleanUp

void PSAImpl::cleanUp() {

	ACS_TRACE("PSAImpl::cleanUp");
	updateThread_p->terminate();//Terminates the Thread
	try {
		PSABase::cleanUp();//In fact here I'm calling to cleanUp in PSUImpl
	} catch (const acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
		throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSAImpl::cleanUp");

	}

}

/*
 * ------------------------------
 * Hardware Lifecycle Methods. States
 * ------------------------------
 */

//----------> Hardware initialization

void PSAImpl::hwInitializeAction() {
	ACS_TRACE("PSAImpl::hwInitializeAction");
	alarmHandlerPSA_m->clearAllAlarmFlags();
	PSABase::hwInitializeAction();
	updateThread_p->suspend(); //suspend the execution of the Thread
	}
//----------> Hardware Operational

void PSAImpl::hwOperationalAction() {
	ACS_TRACE("PSAImpl::hwOperationalAction");
	PSABase::hwOperationalAction();
	updateThread_p->resume(); //Here we continue the execution of the resumed thread

}

//---------> Hardware Stop

void PSAImpl::hwStopAction() {
	ACS_TRACE("PSAImpl::hwStopAction");
	updateThread_p->suspend(); //we suspend the monitor control point  thread
	alarmHandlerPSA_m->clearAllAlarmFlags();//Here we clean up PSA alarms
	PSABase::hwStopAction(); //and call stop on the base class :) This call cleans up PSUImpl alarms

}

void PSAImpl::processStatusMonitors() {
	ACS_TRACE("PSAImpl::processStatusMonitors");
	const string fnName = "PSAImpl::processStatusMonitors()";

	if (isReady() == false) {
		ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
		"PSABase::processStatusMonitors()");
		ex.addData("ErrorCode", static_cast< int>(ControlExceptions::INACTError));
		throw ex;
	}
	PSUImpl::handler_m->evaluateAlarmStateList();
	PSAImpl::alarmHandlerPSA_m->evaluateAlarmStateList();


}
//This method will be called in a loop by the Thread
void PSAImpl::updateThreadAction() {
	ACS_TRACE("PSAImpl::updateThreadAction");
	//Get antenna name from component name
	const string antName = HardwareDeviceImpl::componentToAntennaName(
	    CORBA::String_var(acscomponent::ACSComponentImpl::name()).in());
	const string fnName = "PSAImpl::updateThreadAction";

	/* Queue Status Monitor */
	try {
		processStatusMonitors();
	} catch (const ControlExceptions::CAMBErrorExImpl& ex) {
		ostringstream message;
		message << "Received no answer or erroneus one "
		<< "from the CANBus."
		<< " AntennaName ="
		<< antName;
		ControlExceptions::CAMBErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PSA device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	} catch(...) {
		ACS_TRACE("PSAImpl::updateThreadAction() caught an unknown exception.");
	}

}
/* ---------> Thread Monitor Constructor*/
PSAImpl::UpdateThread::UpdateThread(const ACE_CString& name, PSAImpl& PSA) :
	ACS::Thread(name), psaDevice_p(PSA) {
	ACS_TRACE("PSAImpl::UpdateThread::UpdateThread()");
}
/* ---------> runLoop implementation */
void PSAImpl::UpdateThread::runLoop() {
	ACS_TRACE("PSAImpl::UpdateThread::runLoop()");
	psaDevice_p.updateThreadAction();
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(PSAImpl)


