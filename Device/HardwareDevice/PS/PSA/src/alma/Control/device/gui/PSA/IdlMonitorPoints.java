
/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 */

package alma.Control.device.gui.PSA;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.PSA.presentationModels.MonitorPointPM;

//
// This is a temporary hack!
//

public enum IdlMonitorPoints implements IMonitorPoint {
    // This is an empty list for this device.
    // Note GET_AMBIENT_TEMPERATURE is covered by the shared PSU idl monitor points.
    ;

    private IdlMonitorPoints (Class typeClass,
                              String newDisplayName, 
                              String operatingMode, 
                              int expectedBooleanValue)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.expectedBooleanValue = (expectedBooleanValue == 1) ? true: false;
        this.operatingMode = operatingMode;
        this.pmClass = MonitorPointPM.class;
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.units = units;
        this.pmClass = MonitorPointPM.class;
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              int arrayLength,
                              String operatingMode, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.units = units;
        this.pmClass = MonitorPointPM.class; 
        this.arrayLength = arrayLength;
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              Double rangeLowerBound, 
                              Double rangeUpperBound, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
        this.units = units;
        this.pmClass = MonitorPointPM.class; 
    }

    private IdlMonitorPoints (Class typeClass, 
                              int arrayLength,
                              String newDisplayName, 
                              String operatingMode, 
                              Double rangeLowerBound, 
                              Double rangeUpperBound, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
        this.units = units;
        this.pmClass = MonitorPointPM.class; 
        this.arrayLength = arrayLength;
    }

    private IdlMonitorPoints (Class typeClass, 
                              int arrayLength,
                              String newDisplayName, 
                              String operatingMode, 
                              Double rangeLowerBound, 
                              Double rangeUpperBound)
     {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
        this.pmClass = MonitorPointPM.class; 
        this.arrayLength = arrayLength;
    }
    
    /*
     * Workaround for code generation systems tendency to convert "0.0" to "0".
     */
    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              Integer rangeLowerBound, 
                              Double rangeUpperBound, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound;
        this.pmClass = MonitorPointPM.class; 
    }

    private IdlMonitorPoints (Class typeClass, 
                              int arrayLength,
                              String newDisplayName, 
                              String operatingMode, 
                              Integer rangeLowerBound, 
                              Double rangeUpperBound, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound;
        this.pmClass = MonitorPointPM.class; 
        this.arrayLength = arrayLength;
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              Double rangeLowerBound, 
                              Double rangeUpperBound)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound;
        this.rangeUpperBound = rangeUpperBound;
        this.pmClass = MonitorPointPM.class; 
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              Integer rangeLowerBound, 
                              Integer rangeUpperBound, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = units;
        this.pmClass = MonitorPointPM.class; 
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              Integer rangeLowerBound, 
                              Integer rangeUpperBound)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.pmClass = MonitorPointPM.class; 
    }

    private IdlMonitorPoints (Class typeClass, 
                              int arrayLength,
                              String newDisplayName, 
                              String operatingMode, 
                              Integer rangeLowerBound, 
                              Integer rangeUpperBound)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.pmClass = MonitorPointPM.class; 
        this.arrayLength = arrayLength;
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              Long rangeLowerBound, 
                              Long rangeUpperBound, 
                              String units)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.units = units;
        this.pmClass = MonitorPointPM.class; 
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode, 
                              Long rangeLowerBound, 
                              Long rangeUpperBound)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.rangeLowerBound = rangeLowerBound.doubleValue();
        this.rangeUpperBound = rangeUpperBound.doubleValue();
        this.pmClass = MonitorPointPM.class; 
    }

    private IdlMonitorPoints (Class typeClass, 
                              String newDisplayName, 
                              String operatingMode)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.pmClass = MonitorPointPM.class; 
    }
    
    private IdlMonitorPoints (Class typeClass, 
                              int arrayLength,
                              String newDisplayName, 
                              String operatingMode)
    {
        this.typeClass = typeClass;
        this.displayName = newDisplayName;
        this.operatingMode = operatingMode;
        this.pmClass = MonitorPointPM.class; 
        this.arrayLength = arrayLength;
    }
    
    public int getArrayLength() {
        return arrayLength;
    }

    public String getDisplayName () {
        return displayName;
    }
    
    public double getDisplayOffset() {
        return this.displayOffset;
    }

    public double getDisplayScale() {
        return this.displayScale;
    }

    public String getDisplayUnits() {
        if (displayUnits == null || displayUnits == "")
            return units;
        else
            return displayUnits;
    }

    public Boolean getExpectedBooleanValue() {
        return expectedBooleanValue;
    }
    
    public String getOperatingMode() {
        return operatingMode;
    }
    
    public Class getPmClass() {
        return pmClass;
    }
    
    public String getLowerGraphYLabel() {
        return lowerGraphYLabel;
    }

    public Double getRangeLowerBound() {
        return rangeLowerBound;
    }

    public Double getRangeUpperBound() {
        return rangeUpperBound;
    }
    
    public Class getTypeClass() {
        return typeClass;
    }
    
    public String getUnits() {
        return units;
    }

    public String getUpperGraphYLabel() {
        return upperGraphYLabel;
    }

    public void setDisplayName(String newDisplayName) {
        this.displayName = newDisplayName;
    }
    
    public void setDisplayOffset (double newOffset) {
        this.displayOffset = newOffset;
    }

    public void setDisplayScale (double newScale) {
        this.displayScale = newScale;
    }

    public void setDisplayUnits (String newDisplayUnits) {
        this.displayUnits = newDisplayUnits;
    }

    public void setLowerGraphYLabel(String newLowerGraphYLabel) {
        this.lowerGraphYLabel = newLowerGraphYLabel;
    }

    public void setRangeLowerBound(Double newRangeLowerBound) {
        this.rangeLowerBound = newRangeLowerBound;
    }
    
    public void setRangeUpperBound(Double newRangeUpperBound) {
        this.rangeUpperBound = newRangeUpperBound;
    }
    
    public void setUpperGraphYLabel(String newUpperGraphYLabel) {
        this.upperGraphYLabel = newUpperGraphYLabel;
    }

    public boolean supportsRangeChecking() {
        return ((null != rangeLowerBound) && (null != rangeUpperBound));
    }
    
    public boolean supportsUnits() {
        return (!units.equals(""));
    }
    
    private int arrayLength = 0;
    private String displayName = "Error: common name not set in constructor!";
    private double displayOffset = 0;
    private double displayScale = 1;
    private String displayUnits = "";
    private Boolean expectedBooleanValue = null;
    private final String operatingMode;
    private final Class pmClass;
    private String lowerGraphYLabel = "";
    // Doubles are used below to prevent loss of precision.
    private Double rangeLowerBound = null;
    private Double rangeUpperBound = null;    
    private final Class typeClass;
    private String units = "";
    private String upperGraphYLabel = "";

}

//
// O_o
        
