/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/**
 * Test for checking the PSA monitor points. This test extends TestMonitorPoints to provide
 * for testing. 
 *
 * @author  David Hunter    dhunter@nrao.edu
 * @version 
 * @since 
 */


package alma.Control.device.gui.PSA;

import alma.Control.PSACompSimBase;
import alma.Control.HardwareDevice;
import alma.Control.device.gui.common.TestMonitorPoints;
import alma.acs.container.ContainerServices;


public class TestPSAMonitorPoints extends TestMonitorPoints {

    /**
     * The constructor. Just call super...
     * For some reason the ACS component client test case constructor throws exceptions, so we have to
     * pass them along because Java won't let us handle them!
     * @param name
     * @throws Exception
     */
    public TestPSAMonitorPoints(String name) throws Exception {
        super(name);
    }

    /**
     * Set things up for the test!
     *      -Tell the parent class to setup
     *      -Get the PSA component.
     *      
     * Setup must be called before running any tests!
     *
     * Precondition: There MUST be a simulated PSA up and running before calling setup. This function
     * will fail if there is no PSA running.
     */
    protected void setUp() throws Exception {
        super.setUp();
        PSACompSimBase PSARef=null;
        if (null != componentName) {
            ContainerServices c = getContainerServices();
            assertNotNull(c);
            org.omg.CORBA.Object compObj=c.getComponent(componentName);
            assertNotNull(compObj);
            PSARef = (PSACompSimBase) narrowToSimObject(compObj);
            super.setUp(PSARef, componentName, TestDataFileLocation);
        }
        else
            fail("Error: componentName is null!");
    }
    
    /** Narrows a corba PSA object into a PSACompSimBase.
     * Based off of PSA.narrow, which only works on PSAes.
     * @param obj
     * @return
     */
    public HardwareDevice narrowToSimObject(final org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        try
        {
            return (alma.Control.PSACompSimBase)obj;
        }
        catch (ClassCastException c)
        {
            if (obj._is_a("IDL:alma/Control/PSA:1.0"))
            {
                alma.Control._PSACompSimBaseStub stub;
                stub = new alma.Control._PSACompSimBaseStub();
                stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
                return stub;
            }
        }
        throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
    }
    private String TestDataFileLocation = "PSAMonitorTestData.txt";
    private String componentName = "CONTROL/DA41/PSA";
}
