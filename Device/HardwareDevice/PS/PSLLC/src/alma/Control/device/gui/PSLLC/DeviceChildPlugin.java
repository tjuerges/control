/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSLLC;

import alma.Control.device.gui.PSLLC.panels.DetailsPanel;
import alma.Control.device.gui.PSLLC.panels.SummaryPanel;
import alma.Control.device.gui.PSLLC.presentationModels.DevicePM;
import alma.Control.device.gui.common.widgets.StatusBarWidget;
import alma.common.gui.chessboard.ChessboardDetailsPlugin;
import alma.common.gui.chessboard.ChessboardDetailsPluginListener;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

import java.awt.BorderLayout;
import java.util.logging.Logger;

/**
 * Implement the abstract methods of the ChessboardDetailsPlugin class.  This creates a class that may be 
 * used as child plug-in to the Operator Master Client that will display control and monitor points for a 
 * hardware device.  This allows the operator to control and monitor a hardware device in real time.   
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington sharring@nrao.edu
 * @since   ALMA 5.0.3
 */
public class DeviceChildPlugin extends ChessboardDetailsPlugin {
    
    /**
     * Constructor.
     * 
     * @param antennaNames
     *            names of the antennas for which this plug-in is being
     *            constructed.
     * @param listener
     *            a <code>ChessboardDetailsPluginListener</code> which will be
     *            notified when this plug-in closes.
     * @param pluginContainerServices
     *            container pluginContainerServices for things like getComponent calls, etc.
     * @param pluginTitle
     *            the unique title of the plug-in (to be used in the title bar of
     *            the plug-in window on screen) and by the docking framework of
     *            EXEC in order to give the plug-in a unique name.
     * @param selectionTitle
     *            the title of the cell that was selected in the chessboard when
     *            this details plug-in was launched; this may not be unique if,
     *            for example, multiple instances of a details display have been
     *            shown for a single cell in the chessboard; say, cell S1 might
     *            have selectionTitle of 'S1', pluginTitle values (if 3
     *            instances of the details display have been shown) of: 'S1 -
     *            1', 'S1 - 2', 'S1 - 3', etc.
     */
    public DeviceChildPlugin(
        String antennaNames[],
        ChessboardDetailsPluginListener listener,
        PluginContainerServices services, 
        String pluginTitle,
        String selectionTitle
    ) {
        super(
            antennaNames, 
            listener, 
            services, 
            pluginTitle, 
            selectionTitle
        );
        this.setServices(services);
        this.antennaNames = antennaNames;
        this.pluginTitle = pluginTitle;
        this.selectionTitle = selectionTitle;
        
        buildDevicePresentationModels();
        buildPanel();
    }

    public String getPluginName() { 
        return PLUGIN_NAME; 
    }
    
    public String getTitle() { 
        return pluginTitle; 
    }

    public void replaceContents(String[] newAntennaNames) {
        assert (null != newAntennaNames && newAntennaNames.length > 0) : "newAntennaNames array malformed";
        this.antennaNames = newAntennaNames;
        buildDevicePresentationModels();
    }
    
    public boolean runRestricted(boolean restricted) {
        this.restricted = restricted;
        devicePM.setRunRestricted(restricted);
        devicePM.setControlsEnabled(restricted);
        return this.restricted;
    }
    
    /**
     * Setter for the container pluginContainerServices for the plug-in - this is used
     * for things like getting a logger, getting components, etc.
     * 
     * @param pluginContainerServices
     */
    public void setServices(PluginContainerServices services)
    {
        // TODO: Move up to super class
        super.setServices(services);
        this.pluginContainerServices = services;
        initializeLogger();
    }

    public void specializedStart() {
        if(null != devicePM) 
            devicePM.start();
    }
    
    @Override
    public void specializedStop() {
        if(null != devicePM)
            devicePM.stop();
    }


    /**
     * Build the plug-in device presentation model.
     */
    private void buildDevicePresentationModels() {
        
        // TODO: find number of presentation models to build.
        this.devicePM = new DevicePM(
                antennaNames[0], 
                pluginContainerServices
        );
    }
    
    /**
     * Build the plug-in GUI panel.
     */
    private void buildPanel() {
        setLayout(new BorderLayout());

        // Add sub-panels.
        summaryPanel = new SummaryPanel(logger, devicePM);
        this.add(summaryPanel, BorderLayout.NORTH);

        detailsPanel = new DetailsPanel(logger, devicePM);
        this.add(detailsPanel, BorderLayout.CENTER);

        statusBar = new StatusBarWidget(logger, devicePM);
        this.add(statusBar, BorderLayout.SOUTH);

        this.setVisible(true);
    }

    private void initializeLogger() {
        // TODO: Move up to super class
        logger = (pluginContainerServices == null) ? 
            Logger.getLogger(STAND_ALONE_LOGGER_NAME) : 
            pluginContainerServices.getLogger();
    }

    /**
     * Subclasses ChessboardDetailsPlugin must implement this to replace the contents of a previously
     * displayed plug-in display.
     * 
     * @param newAntennaNames
     *            the name(s) of the antennas to display over the previous
     *            display.
     */
    // TODO - move to DevicePM.java
    private final static String PLUGIN_NAME = "Power Supply LLC: ";
    // TODO - move to DevicePM.java
    private final static String STAND_ALONE_LOGGER_NAME = "PSLLCLogger";
    
    private String[] antennaNames;
    private DetailsPanel detailsPanel;
    private DevicePM devicePM;
    private Logger logger;
    private PluginContainerServices pluginContainerServices;
    private String pluginTitle;
    private boolean restricted;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private SummaryPanel summaryPanel;
    private StatusBarWidget statusBar;
}
     
