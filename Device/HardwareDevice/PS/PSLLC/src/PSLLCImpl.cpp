/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PSLLCImpl.cpp
 *
 *
 * $Id$
 *
 *
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/05/14	PSLLCImpl.cpp created
 *------------------------------------------------------------
 */

/**
 * <P>
 * 		PSLLC Power Supply Unit Implementation
 * <P>
 *
 * This is the Common class for all the others real implementations of power supplies.
 * @see  PSLLCBase
 */

#include <PSLLCImpl.h>
#include <maciACSComponentDefines.h>

using std::string;

/*------------PSLLC Constructor-----------------*/

PSLLCImpl::PSLLCImpl(const ACE_CString& name, maci::ContainerServices* cs) :
	PSLLCBase(name, cs), updateThreadCycleTime_m(50000000), testModeState(false)
	{//Monitoring done each 1 second. Since each unit is 100E-9 sec, we need 10000000 units :)
	ACS_TRACE("PSLLCImpl::PSLLCImpl");
	alarmHandlerPSLLC_m=new AlarmHandler();
}

/*------------PSLLC Destructor-------------------*/

PSLLCImpl::~PSLLCImpl() {
	ACS_TRACE("PSLLCImpl::~PSLLCImpl");
	delete(alarmHandlerPSLLC_m);
}
/**
 * --------------------------------------------------------------
 *      Component Lifecycle Methods
 * --------------------------------------------------------------
 */

//---------->   Initialize

void PSLLCImpl::initialize() {
	ACS_TRACE("PSLLCImpl::initialize");
	try {
		PSLLCBase::initialize(); //Initialize call to the base class
	} catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
		throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSLLCImpl::initialize");
	}

	const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
	std::string threadName(compName.in());
	threadName += "MonitoringThread";
	updateThread_p = getContainerServices()->getThreadManager()->create<UpdateThread,PSLLCImpl>(
	    threadName.c_str(), *this);
	updateThread_p->setSleepTime(updateThreadCycleTime_m); //The thread sleeptime is 1 sec

	alarmHandlerPSLLC_m->addAlarmToList(new FloatAlarm(1,
									"PSLLC",
									getComponent()->getName(),
									"PSLLC is about to be shutdown due to overtemperature",
									new GetTemperatureCelsiusCommand(this),
									0,
									PSLLCImpl::HW_SHUTDOWN_TEMP*0.97));
} //end initialize()


//--------->  CleanUp

void PSLLCImpl::cleanUp() {

	ACS_TRACE("PSLLCImpl::cleanUp");
	updateThread_p->terminate();//Terminates the Thread
	try {
		/* Clear all the alarms not BACI if any is activated*/
		PSLLCBase::cleanUp();
	} catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
		throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSLLCImpl::cleanUp");

	}

}

/*
 * ------------------------------
 * Hardware Lifecycle Methods. States
 * ------------------------------
 */

//----------> Hardware initialization

void PSLLCImpl::hwInitializeAction() {
	ACS_TRACE("PSLLCImpl::hwInitializeAction");
	alarmHandlerPSLLC_m->clearAllAlarmFlags();
	PSLLCBase::hwInitializeAction();
	updateThread_p->suspend(); //suspend the execution of the Thread
	}
//----------> Hardware Operational

void PSLLCImpl::hwOperationalAction() {
	ACS_TRACE("PSLLCImpl::hwOperationalAction");
	PSLLCBase::hwOperationalAction();
	updateThread_p->resume(); //Here we continue the execution of the resumed thread
}

//---------> Hardware Stop

void PSLLCImpl::hwStopAction() {
	ACS_TRACE("PSLLCImpl::hwStopAction");
	updateThread_p->suspend(); //we suspend the monitor control point  thread
	alarmHandlerPSLLC_m->clearAllAlarmFlags();//Here we clean up PSLLC alarms
	PSLLCBase::hwStopAction(); //and call stop on the base class :)
}
void PSLLCImpl::processStatusMonitors() {
	ACS_TRACE("PSLLCImpl::processStatusMonitors");
	const string fnName = "PSLLCImpl::processStatusMonitors()";

	if (isReady() == false) {
		ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
		"PSLLCImpl::processStatusMonitors()");
		ex.addData("ErrorCode", static_cast< int>(ControlExceptions::INACTError));
		throw ex;
	}
	PSUImpl::handler_m->evaluateAlarmStateList();
	PSLLCImpl::alarmHandlerPSLLC_m->evaluateAlarmStateList();


}
//This method will be called in a loop by the Thread
void PSLLCImpl::updateThreadAction() {
	ACS_TRACE("PSLLCImpl::updateThreadAction");
	//Get antenna name from component name
	const string antName = HardwareDeviceImpl::componentToAntennaName(
	    CORBA::String_var(acscomponent::ACSComponentImpl::name()).in());
	const string fnName = "PSLLCImpl::updateThreadAction";

	/* Queue Status Monitor */
	try {
		processStatusMonitors();
	} catch (const ControlExceptions::CAMBErrorExImpl& ex) {
		ostringstream message;
		message << "Received no answer or erroneus one "
		<< "from the CANBus."
		<< " AntennaName ="
		<< antName;
		ControlExceptions::CAMBErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PSLLC device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	} catch(...) {
		ACS_TRACE("PSLLCImpl::updateThreadAction() caught an unknown exception.");
	}

}
/* ---------> Thread Monitor Constructor*/
PSLLCImpl::UpdateThread::UpdateThread(const ACE_CString& name, PSLLCImpl& PSLLC) :
	ACS::Thread(name), psllcDevice_p(PSLLC) {
	ACS_TRACE("PSLLCImpl::UpdateThread::UpdateThread()");
}
/* ---------> runLoop implementation */
void PSLLCImpl::UpdateThread::runLoop() {
	ACS_TRACE("PSLLCImpl::UpdateThread::runLoop()");
	psllcDevice_p.updateThreadAction();
}


/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(PSLLCImpl)

