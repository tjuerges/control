#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# pburgos  2008-06-29  created
#
import CCL.HardwareDevice

import CCL.PSLLCBase
from CCL import StatusHelper
from CCL.logging import getLogger

class PSLLC(CCL.PSLLCBase.PSLLCBase):
    '''
    The PSLLCclass inherits from the code generated PSLLCBase
    class and adds specific methods.
    '''
    def __init__(self, instanceNumber = None, stickyFlag = False):
        '''
        The constructor creates a PSLLC object by making use of
        the PSLLCBase constructor. The stickyFlag can be set to
        True if the component should be instantiated in a sticky
        way.
        EXAMPLE:
        from CCL.PSLLC import PSLLC
        psllc = PSLLC(1)
        
        This last case instantiates CONTROL/CentralLO/PSLLC1
        '''
        # Dictionary of device instances
        self._devices = {}
        if instanceNumber !=None:
            componentName=[]
            self._devices["CONTROL/CentralLO/PSLLC"+str(instanceNumber)]=""
            componentName=componentName + ["CONTROL/CentralLO/PSLLC"+str(instanceNumber)]
            antennaName=None
        else:
            componentName = None
            self._devices[componentName] = ""
            
            #At the End, I always send componentName . antennaName must be None   
            antennaName = None
            
            #PSLLCBase Constructor
        try:
            CCL.PSLLCBase.PSLLCBase.__init__(self, antennaName,componentName, stickyFlag)
        except Exception, e:
            print 'PSLLC component Can not be instanciated because \n' + str(e)

        # Initialize the base class
        for key, val in self._devices.iteritems():
            CCL.HardwareDevice.HardwareDevice.__init__(self, key, stickyFlag);
        self._devices[key]= self._HardwareDevice__hw
        self.__logger = getLogger()

    def __del__(self):
        CCL.PSLLCBase.PSLLCBase.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the PSLLC
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
        
            # Invoke the generic PSU status helper
            elements = self._PSU__STATUSHelper(key)
            
            ############################### PSLLC specific ###################################################
            elements.append( StatusHelper.Separator("PSLLC specific") )
            
            
                
                 
            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
        #TODO  __del__ method
    
