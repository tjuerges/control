
#ifndef PSLLCCompSimImpl_h
#define PSLLCCompSimImpl_h
/**
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 *
 */

#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <PSLLCCompSimBase.h>
#include <boost/shared_ptr.hpp>


namespace maci
{
    class ContainerServices;
};

namespace AMB
{
    class PSLLCHWSimImpl;
};


/**
 * Please use this class to implement alternative components, extending
 * the PSLLCCompSimBase class.
 */

class PSLLCCompSimImpl: public PSLLCCompSimBase
{
    public:
    PSLLCCompSimImpl(const ACE_CString& name, maci::ContainerServices* cs);

    virtual ~PSLLCCompSimImpl()
    {
    };


    private:
    // Thomas asks: Why is no default constructor declared here?
    PSLLCCompSimImpl(const PSLLCCompSimImpl&);
    PSLLCCompSimImpl& operator=(const PSLLCCompSimImpl&);

    /// boost::share_ptr to the simulated object.
    boost::shared_ptr< AMB::PSLLCHWSimImpl > simulationObject;
};
#endif
