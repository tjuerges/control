/* @(#) $Id$
 *
 *
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/05/18	PSLLCImpl.h created
 *	pburgos		2008/06/29	PSLLCImpl.h Alarms added
 *------------------------------------------------------------
 */
#ifndef PSLLCIMPL_H_
#define PSLLCIMPL_H_
// Libraries
#include <PSLLCS.h>  //For CORBA
#include <PSLLCBase.h>   //AutoGenerated Base Class

class PSLLCImpl : public PSLLCBase, virtual public POA_Control::PSLLC {
protected:
			/* ------------ Writer Inner Thread Class -------------- */
			class UpdateThread : public ACS::Thread {
				public:
					UpdateThread(const ACE_CString& name, PSLLCImpl& PSLLC);
					virtual void runLoop();
				protected:
					PSLLCImpl& psllcDevice_p;
			};
	//Constructor
public:
	friend class PSLLCImpl::UpdateThread;
	AlarmHandler* alarmHandlerPSLLC_m;
	PSLLCImpl(const ACE_CString& name, maci::ContainerServices* cs);
	//Destructor
	virtual ~PSLLCImpl();
	//Component Lifecycle
	// \exception acsErrTypeLifeCycle::LifeCycleExImpl
	virtual void initialize();

	// \exception acsErrTypeLifeCycle::LifeCycleExImpl
	virtual void cleanUp();

	//Hardware Lifecycle
	virtual void hwInitializeAction();
	virtual void hwOperationalAction();
	virtual void hwStopAction();
	ACS::Time updateThreadCycleTime_m;
protected:
	void updateThreadAction();
private:
	static const float HW_SHUTDOWN_TEMP=32;
	// \exception ControlExceptions::INACTErrorExImpl
	virtual void processStatusMonitors();
	UpdateThread* updateThread_p;
	bool testModeState;
};


#endif /*PSLLCIMPL_H_*/

