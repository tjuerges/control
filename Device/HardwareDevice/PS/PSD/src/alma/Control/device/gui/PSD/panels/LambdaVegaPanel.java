/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSD.panels;

import alma.Control.device.gui.PSD.presentationModels.DevicePM;
import alma.Control.device.gui.PSU.IcdMonitorPoints;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Detail data.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author Steve Harrington  sharring@nrao.edu
 * @author Justin Kenworthy  jkenwort@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class LambdaVegaPanel extends DevicePanel {

	// TODO - serial version UID
    private static final long serialVersionUID = 1L;    
    
    private LambdaVegaSerialNumberPanel lambdaVegaSerialNumberPanel;
    private LambdaVegaBufferPanel lambdaVegaBufferPanel;
    private LambdaVegaErrorPanel lambdaVegaErrorPanel;
    
    public LambdaVegaPanel(Logger logger, DevicePM aDevicePM) {
       	super(logger, aDevicePM);
       	buildSubPanels(logger, aDevicePM);
       	buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        lambdaVegaSerialNumberPanel = new LambdaVegaSerialNumberPanel(logger, aDevicePM);
       	lambdaVegaBufferPanel = new LambdaVegaBufferPanel(logger, aDevicePM);
       	lambdaVegaErrorPanel = new LambdaVegaErrorPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.ipadx = 5;
		c.ipady = 5;
		
		c.gridy = 0;
		c.gridx = 0;
		add(lambdaVegaSerialNumberPanel, c);

        c.gridy = 1;
        add(lambdaVegaBufferPanel, c);
        
        c.gridy = 2;
		add(lambdaVegaErrorPanel, c);

        setVisible(true);
    }
    
    public class LambdaVegaSerialNumberPanel extends DevicePanel {
    	public LambdaVegaSerialNumberPanel(Logger logger, DevicePM dPM) {
    	 	super(logger, dPM);
    	 	buildPanel();
    	}
    	
    	protected void buildPanel() {
    		add(new JLabel("Serial Number"));    		
            setVisible(true);
    	}
    }
    
    public class LambdaVegaBufferPanel extends DevicePanel {
    	    	
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;    

        public LambdaVegaBufferPanel(Logger logger, DevicePM dPM) {
    		super(logger, dPM);
    		buildPanel();
    	}
    	
    	protected void buildPanel() {
    		final String [] columnLabels = {
    	        	null,
    	        	"Length",
    	        	"Unit ID",
    	        	"Module ID",
    	        	"Command ID",
    	        	"Message"
    	        };
    	        
	        final String [] rowLabels = {
	        	null,
	        	"RS-232 RX Buffer",
	        	"RS-232 TX Buffer"
	        };
    		
    		 setBorder(
	                BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Buffers"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)
   	                )
    	     );    		 
    		 
    		 setLayout(new GridBagLayout());
    		 GridBagConstraints c = new GridBagConstraints();
    		 c.fill = GridBagConstraints.HORIZONTAL;
    		 
    		 c.ipadx = 5;
    		 c.ipady = 5;
    		
    		 // Add column headers.
	        c.gridy = 0;
	        for (int i = 1; i < columnLabels.length; i++) {
	            c.gridx = i;
	            if (columnLabels[i] != null) {
	            	add(new JLabel(columnLabels[i]), c);
	            }
	        }
	      
	        // Add row headers.
	        c.gridx = 0;
	        for (int j = 1; j < rowLabels.length; j++) {
	            c.gridy = j;
	            if (rowLabels[j] != null) {
	            	add(new JLabel(rowLabels[j]), c);
	            }
	        }  
	        
	        c.gridy = 1;
	        c.gridx = 1;
	        add(
	        		new IntegerMonitorPointWidget(
	        				logger,
	        				devicePM.getMonitorPointPM(IcdMonitorPoints.MESSAGE_LENGTH_RX)
	        			),
    				c
    			);
	        
	        c.gridx = 2;
	        add(
	        		new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_UNIT_IDENTIFIER_RX)
		                ),
		        	c
		        );
	        
	        c.gridx = 3;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_MODULE_IDENTIFIER_RX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 4;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_COMMAND_IDENTIFIER_RX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 5;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_1_RX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 6;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_2_RX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 7;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_3_RX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 8;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_4_RX)					
		                ),
		        	c
		        );
	        
	        c.gridy = 2;
	        c.gridx = 1;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.MESSAGE_LENGTH_TX)
		                ),
		        	c
		        );
		        
	        c.gridx = 2;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_UNIT_IDENTIFIER_TX)
		                ),
		        	c
		        );
		        
	        c.gridx = 3;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_MODULE_IDENTIFIER_TX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 4;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.INTERNAL_COMMAND_IDENTIFIER_TX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 5;
	        c.gridx = 5;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_1_TX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 6;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_2_TX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 7;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_3_TX)					
		                ),
		        	c
		        );
	        
	        c.gridx = 8;
	        add(
		        	new IntegerMonitorPointWidget(
		                    logger,
		                    devicePM.getMonitorPointPM(IcdMonitorPoints.PAYLOAD_4_TX)					
		                ),
		        	c
		        );
		       
	        
	        setVisible(true);
    	}
    }
    
    public class LambdaVegaErrorPanel extends DevicePanel {
    	
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;    

       public LambdaVegaErrorPanel(Logger logger, DevicePM dPM) {
    		super(logger, dPM);
    		buildPanel();
    	}
    	
    	protected void buildPanel() {
			final String [] rowLabels = {
		    		"RS-232 Error Count",
		    		"Mid-Level Function Error",
		    		"Low-Level Function Error",
		    		"LambdaVega Error Count",
		    		"Last Error"    		
		    	};
			
			 setBorder(
		                BorderFactory.createCompoundBorder(
	                    BorderFactory.createTitledBorder("Errors"),
	                    BorderFactory.createEmptyBorder(1, 1, 1, 1)
	   	                )
	    	     );
			 
	    	 setLayout(new GridBagLayout());
    		 GridBagConstraints c = new GridBagConstraints();
    		 c.fill = GridBagConstraints.HORIZONTAL;
    		 
    		 c.ipadx = 5;
    		 c.ipady = 5;
    		 
    		// Add row headers.
 	        c.gridx = 0;
 	        for (int j = 0; j < rowLabels.length; j++) {
 	            c.gridy = j;
 	            if (rowLabels[j] != null) {
 	            	add(new JLabel(rowLabels[j]), c);
 	            }
 	        } 
    		 
 	        c.gridx = 1;
 	        c.gridy = 0;
 	        add(
					new IntegerMonitorPointWidget(
				            logger,
				            devicePM.getMonitorPointPM(IcdMonitorPoints.RS232_ERROR_COUNT)					
				        ),
					c
				);
 	       
 	        c.gridy = 1;
 	        add(
					new IntegerMonitorPointWidget(
				            logger,
				            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_LEVEL_FUNCTION_WHERE_ERROR_OCURRED)					
				        ),
					c
				);
 	        
 	        c.gridy = 2;
 	        add(
					new IntegerMonitorPointWidget(
				            logger,
				            devicePM.getMonitorPointPM(IcdMonitorPoints.LOW_LEVEL_FUNCTION_WHERE_ERROR_OCURRED)					
				        ),
					c
				);
 	        
 	        c.gridy = 3;
 	        add(
					new IntegerMonitorPointWidget(
				            logger,
				            devicePM.getMonitorPointPM(IcdMonitorPoints.LAMBDA_VEGA_ERROR_COUNT)					
				        ),
					c
				);
 	        
 	        c.gridy = 4;
 	        add(
					new IntegerMonitorPointWidget(
				            logger,
				            devicePM.getMonitorPointPM(IcdMonitorPoints.LAST_ERROR_RECEIVED_FROM_LAMBDA)					
				        ),
					c
				);
 	       
    		setVisible(true);
    	}
    }
}

//
// O_o
