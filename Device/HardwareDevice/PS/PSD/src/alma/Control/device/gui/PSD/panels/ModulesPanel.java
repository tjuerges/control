/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSD.panels;

import alma.Control.device.gui.PSD.IcdMonitorPoints;
import alma.Control.device.gui.PSD.presentationModels.DevicePM;
import alma.Control.device.gui.PSU.IcdControlPoints;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.FloatMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Detail data.
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author Justin Kenworthy  jkenwort@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class ModulesPanel extends DevicePanel {

    public ModulesPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);      
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        monitorPanel = new ModulesMonitorPanel(logger, aDevicePM);
        controlPanel = new ModulesControlPanel(logger, aDevicePM);
    }
    
    protected void buildPanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        // Add sub-panels.
        add(monitorPanel);
        add(controlPanel);

        setVisible(true);
    }

    private class ModulesControlPanel extends DevicePanel {
    
        //TODO - serial version UID
        private static final long serialVersionUID = 1L;
    
        public ModulesControlPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }
    
        protected void buildPanel() {
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            add(new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.RESET_LATCHES), "Reset Latches", true));
            add(new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.RESET_MAX_MIN), "Reset Max/Min", true));
            add(new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.RESET_WARNING), "Reset Warning", true));
        }
    }

    private class ModulesMonitorPanel extends DevicePanel {
    
        // TODO - serial version UID	
        private static final long serialVersionUID = 1L;
    
        public ModulesMonitorPanel(Logger logger, DevicePM dPM) {
            super(logger, dPM);
            buildPanel();
        }
    
        protected void buildPanel() {
            logger.entering("DetailPanel", "buildPanel");
            final String [] columnLabels = {null, " 24V A ", " 24V B ",};
            final String [] rowLabels = {null, "Current ", "Max current ", "Shutdown current ", "Voltage ",
                                         "Min voltage ", "Max voltage ", "Shutdown voltage ",};
            final String [] rowUnits = {null,
                    IcdMonitorPoints.MID_1_CURRENT.getUnits(),
                    IcdMonitorPoints.MID_1_MAX_CURRENT_SEEN.getUnits(),
                    IcdMonitorPoints.MID_1_SHUTDOWN_CURRENT.getUnits(),
                    IcdMonitorPoints.MID_1_VOLTAGE.getUnits(),
                    IcdMonitorPoints.MID_1_MIN_VOLTAGE_SEEN.getUnits(),
                    IcdMonitorPoints.MID_1_MAX_VOLTAGE_SEEN.getUnits(),
                    IcdMonitorPoints.MID_1_SHUTDOWN_VOLTAGE.getUnits(),};
    
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.HORIZONTAL;
            c.insets = new Insets(1, 2, 1, 1);
            // Add row headers.
            c.gridx = 0;
            for (int j = 1; j < rowLabels.length; j++) {
                c.gridy = j;
                if (rowLabels[j] != null) {
                    add(new JLabel(rowLabels[j]), c);
                }
            }
    
            // Add row units.
            c.gridx = 3;
            for (int j = 1; j < rowUnits.length; j++) {
                c.gridy = j;
                if (rowUnits[j] != null) {
                    add(new JLabel(rowUnits[j]), c);
                }
            }
            c.fill = GridBagConstraints.NONE;
            c.anchor = GridBagConstraints.EAST;
            
            // Add column headers.
            c.gridy = 0;
            for (int i = 1; i < columnLabels.length; i++) {             
                c.gridx = i;
                if (columnLabels[i] != null) {
                    add(new JLabel(columnLabels[i]), c);
                }
            }
            // Add 24V A module cells.
            c.gridx = 1;
            c.gridy = 1;
            /* current */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_2_CURRENT), SIG_FIGS), c);
            c.gridy = 2;
            /* max current */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_2_MAX_CURRENT_SEEN), SIG_FIGS), c);
            c.gridy = 3;
            /* shutdown current */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_2_SHUTDOWN_CURRENT), SIG_FIGS), c);
            c.gridy = 4;
            /* voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_2_VOLTAGE), SIG_FIGS), c);
    
            c.gridy = 5;
            /* min voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_2_MIN_VOLTAGE_SEEN), SIG_FIGS), c);
    
            c.gridy = 6;
            /* max voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_2_MAX_VOLTAGE_SEEN), SIG_FIGS), c);
    
            c.gridy = 7;
            /* shutdown voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_2_SHUTDOWN_VOLTAGE), SIG_FIGS), c);
    
            // Add 24V B module cells.
            c.gridx = 2;
            c.gridy = 1;
            /* current */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_1_CURRENT), SIG_FIGS), c);
            c.gridy = 2;
            /* max current */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_1_MAX_CURRENT_SEEN), SIG_FIGS), c);
            c.gridy = 3;
            /* shutdown current */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_1_SHUTDOWN_CURRENT), SIG_FIGS), c);
            c.gridy = 4;
            /* voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_1_VOLTAGE), SIG_FIGS), c);
            c.gridy = 5;
            /* min voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_1_MIN_VOLTAGE_SEEN), SIG_FIGS), c);
            c.gridy = 6;
            /* max voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_1_MAX_VOLTAGE_SEEN), SIG_FIGS), c);
    
            c.gridy = 7;
            /* shutdown voltage */
            add(new FloatMonitorPointWidget(logger,
                            devicePM.getMonitorPointPM(IcdMonitorPoints.MID_1_SHUTDOWN_VOLTAGE), SIG_FIGS), c);
            setVisible(true);
        }
    }

    private ModulesControlPanel controlPanel;
    private ModulesMonitorPanel monitorPanel;
    private static final int SIG_FIGS = 1;

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o

