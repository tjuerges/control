/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PSDImpl.cpp
 *
 *
 * $Id$
 *
 *
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/05/18	PSDImpl.cpp created
 *	pburgos		2008/06/29	PSDImpl.cpp Alarms added
 *------------------------------------------------------------
 */

/**
 * <P>
 * 		PSD Power Supply Digital Unit Implementation
 * <P>
 *
 * This is the Common class for all the others real implementations of power supplies.
 * @see  PSDBase
 */

#include <PSDImpl.h>

using std::string;

/*------------PSD Constructor-----------------*/

PSDImpl::PSDImpl(const ACE_CString& name, maci::ContainerServices* cs) :
	PSDBase(name, cs),updateThreadCycleTime_m(50000000), testModeState(false)
	{//Monitoring done each 5 second. Since each unit is 100E-9 sec, we need 50000000 units :)
	ACS_TRACE("PSDImpl::PSDImpl");
	alarmHandlerPSD_m=new AlarmHandler();
}

/*------------PSD Destructor-------------------*/

PSDImpl::~PSDImpl() {
	ACS_TRACE("PSDImpl::~PSDImpl");
	delete(alarmHandlerPSD_m);
}
/**
 * --------------------------------------------------------------
 *      Component Lifecycle Methods
 * --------------------------------------------------------------
 */

//---------->   Initialize

void PSDImpl::initialize() {
	ACS_TRACE("PSDImpl::initialize");
	try {
		PSDBase::initialize(); //Initialize call to the base class
	} catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
		throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSDImpl::initialize");
	}

	const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
	std::string threadName(compName.in());
	threadName += "MonitoringThread";
	updateThread_p = getContainerServices()->getThreadManager()->create<UpdateThread,PSDImpl>(
	    threadName.c_str(), *this);
	updateThread_p->setSleepTime(updateThreadCycleTime_m); //The thread sleeptime is 1 sec

	alarmHandlerPSD_m->addAlarmToList(new FloatAlarm(1,
									"PSD",
									getComponent()->getName(),
									"PSD is about to be shutdown due to overtemperature",
									new GetTemperatureCelsiusCommand(this),
									0,
									PSDImpl::HW_SHUTDOWN_TEMP*0.997));
	alarmHandlerPSD_m->addAlarmToList(new FloatAlarm(2,
									"PSD",
									getComponent()->getName(),
									"PSD MID 1 Voltage is out of range",
									new GetMid1VoltageCommand::GetMid1VoltageCommand(this),
									PSDImpl::MID_1_MIN_VOLTAGE,
									PSDImpl::MID_1_MAX_VOLTAGE));
	alarmHandlerPSD_m->addAlarmToList(new FloatAlarm(3,
									"PSD",
									getComponent()->getName(),
									"PSD MID 1 Current is out of range",
									new GetMid1CurrentCommand::GetMid1CurrentCommand(this),
									PSDImpl::MID_1_MIN_CURRENT,
									PSDImpl::MID_1_MAX_CURRENT));
	alarmHandlerPSD_m->addAlarmToList(new FloatAlarm(4,
									"PSD",
									getComponent()->getName(),
									"PSD MID 2 Voltage is out of range",
									new GetMid2VoltageCommand::GetMid2VoltageCommand(this),
									PSDImpl::MID_2_MIN_VOLTAGE,
									PSDImpl::MID_2_MAX_VOLTAGE));
	alarmHandlerPSD_m->addAlarmToList(new FloatAlarm(5,
									"PSD",
									getComponent()->getName(),
									"PSD MID 2 Current is out of range",
									new GetMid2CurrentCommand::GetMid2CurrentCommand(this),
									PSDImpl::MID_2_MIN_CURRENT,
									PSDImpl::MID_2_MAX_CURRENT));


} //end initialize()


//--------->  CleanUp

void PSDImpl::cleanUp() {

	ACS_TRACE("PSDImpl::cleanUp");
	updateThread_p->terminate();//Terminates the Thread
	try {
		/* Clear all the alarms not BACI if any is activated*/
		PSDBase::cleanUp();
	} catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex) {
		throw acsErrTypeLifeCycle::LifeCycleExImpl(ex,__FILE__,__LINE__,"PSDImpl::cleanUp");

	}

}

/*
 * ------------------------------
 * Hardware Lifecycle Methods. States
 * ------------------------------
 */

//----------> Hardware initialization

void PSDImpl::hwInitializeAction() {
	ACS_TRACE("PSDImpl::hwInitializeAction");
	alarmHandlerPSD_m->clearAllAlarmFlags();
	PSDBase::hwInitializeAction();
	updateThread_p->suspend(); //suspend the execution of the Thread
	}
//----------> Hardware Operational

void PSDImpl::hwOperationalAction() {
	ACS_TRACE("PSDImpl::hwOperationalAction");
	PSDBase::hwOperationalAction();
	updateThread_p->resume(); //Here we continue the execution of the resumed thread

}

//---------> Hardware Stop

void PSDImpl::hwStopAction() {
	ACS_TRACE("PSDImpl::hwStopAction");
	updateThread_p->suspend(); //we suspend the monitor control point  thread
	alarmHandlerPSD_m->clearAllAlarmFlags();//Here we clean up PSD alarms
	PSDBase::hwStopAction(); //and call stop on the base class :)
}
void PSDImpl::processStatusMonitors() {
	ACS_TRACE("PSDImpl::processStatusMonitors");
	const string fnName = "PSDImpl::processStatusMonitors()";

	if (isReady() == false) {
		ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
		"PSDBase::processStatusMonitors()");
		ex.addData("ErrorCode", static_cast< int>(ControlExceptions::INACTError));
		throw ex;
	}
	PSUImpl::handler_m->evaluateAlarmStateList();
	PSDImpl::alarmHandlerPSD_m->evaluateAlarmStateList();


}
//This method will be called in a loop by the Thread
void PSDImpl::updateThreadAction() {
	ACS_TRACE("PSDImpl::updateThreadAction");
	//Get antenna name from component name
	const string antName = HardwareDeviceImpl::componentToAntennaName(
	    CORBA::String_var(acscomponent::ACSComponentImpl::name()).in());
	const string fnName = "PSDImpl::updateThreadAction";

	/* Queue Status Monitor */
	try {
		processStatusMonitors();
	} catch (const ControlExceptions::CAMBErrorExImpl& ex) {
		ostringstream message;
		message << "Received no answer or erroneus one "
		<< "from the CANBus."
		<< " AntennaName ="
		<< antName;
		ControlExceptions::CAMBErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PSD device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	} catch(...) {
		ACS_TRACE("PSDImpl::updateThreadAction() caught an unknown exception.");
	}

}
/* ---------> Thread Monitor Constructor*/
PSDImpl::UpdateThread::UpdateThread(const ACE_CString& name, PSDImpl& PSD) :
	ACS::Thread(name), psdbeDevice_p(PSD) {
	ACS_TRACE("PSDImpl::UpdateThread::UpdateThread()");
}
/* ---------> runLoop implementation */
void PSDImpl::UpdateThread::runLoop() {
	ACS_TRACE("PSDImpl::UpdateThread::runLoop()");
	psdbeDevice_p.updateThreadAction();
}

/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(PSDImpl)

