/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 *
 * $Id$
 *
 *
 *	who			when			what
 *------------------------------------------------------------
 *	nsaez		2008/06/23	PSDCommand.cpp created. Uses Command Pattern
 *------------------------------------------------------------
 */
//Command stuff
#include <PSDCommand.h>


//###
GetMid2VoltageCommand::GetMid2VoltageCommand(PSDImpl* _psdImpl) {
	ACS_TRACE("GetMid2VoltageCommand::GetMid2VoltageCommand()");
	psdImpl_m=_psdImpl;
	currentValue_m=0;
}
void GetMid2VoltageCommand::execute() {
	ACS_TRACE("GetMid2VoltageCommand::execute()");
	string fnName = "GetMid2VoltageCommand::execute()";
	string antName = psdImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psdImpl_m->GET_MID_2_VOLTAGE(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
			<< antName
			<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	} catch(const ControlExceptions::CAMBErrorExImpl& ex) {
		ACS_TRACE("Failed to get GetMid2Voltage");
	}

}

GetMid2VoltageCommand::~GetMid2VoltageCommand() {
	ACS_TRACE("GetMid2VoltageCommand::~GetMid2VoltageCommand()");
}

//###
//###
GetMid2CurrentCommand::GetMid2CurrentCommand(PSDImpl* _psdImpl) {
	ACS_TRACE("GetMid2CurrentCommand::GetMid2CurrentCommand()");
	psdImpl_m=_psdImpl;
	currentValue_m=0;
}
void GetMid2CurrentCommand::execute() {
	ACS_TRACE("GetMid2CurrentCommand::execute()");
	string fnName = "GetMid2CurrentCommand::execute()";
	string antName = psdImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psdImpl_m->GET_MID_2_CURRENT(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}catch(const ControlExceptions::CAMBErrorExImpl& ex) {
		ACS_TRACE("Failed to get GetMid2Current");
	}
}

GetMid2CurrentCommand::~GetMid2CurrentCommand() {
	ACS_TRACE("GetMid2CurrentCommand::~GetMid2CurrentCommand()");
}

//###
//###
GetMid1CurrentCommand::GetMid1CurrentCommand(PSDImpl* _psdImpl) {
	ACS_TRACE("GetMid1CurrentCommand::GetMid1CurrentCommand()");
	psdImpl_m=_psdImpl;
	currentValue_m=0;
}
void GetMid1CurrentCommand::execute() {
	ACS_TRACE("GetMid1CurrentCommand::execute()");
	string fnName = "GetMid1CurrentCommand::execute()";
	string antName = psdImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psdImpl_m->GET_MID_1_CURRENT(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}catch(const ControlExceptions::CAMBErrorExImpl& ex) {
		ACS_TRACE("Failed to get GetMid1Current");
	}
}

GetMid1CurrentCommand::~GetMid1CurrentCommand() {
	ACS_TRACE("GetMid1CurrentCommand::~GetMid1CurrentCommand()");
}

//###
//###
GetMid1VoltageCommand::GetMid1VoltageCommand(PSDImpl* _psdImpl) {
	ACS_TRACE("GetMid1VoltageCommand::GetMid1VoltageCommand()");
	psdImpl_m=_psdImpl;
	currentValue_m=0;
}
void GetMid1VoltageCommand::execute() {
	ACS_TRACE("GetMid1VoltageCommand::execute()");
	string fnName = "GetMid1VoltageCommand::execute()";
	string antName = psdImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psdImpl_m->GET_MID_1_VOLTAGE(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}catch(const ControlExceptions::CAMBErrorExImpl& ex) {
		ACS_TRACE("Failed to get GetMid1Voltage");
	}
}

GetMid1VoltageCommand::~GetMid1VoltageCommand() {
	ACS_TRACE("GetMid1VoltageCommand::~GetMid1VoltageCommand()");
}

//###


