#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# pburgos  2008-06-28  created
# pburgos  2008-07-01  small bug fixed . Import missing
import CCL.HardwareDevice
import CCL.PSDBase
from CCL import StatusHelper
from CCL.logging import getLogger

class PSD(CCL.PSDBase.PSDBase):
    '''
    The PSDclass inherits from the code generated PSDBase
    class and adds specific methods.
    '''
    def __init__(self, antennaName = None, componentName = None, stickyFlag = False):
        '''
        The constructor creates a PSD object by making use of
        the PSDBase constructor. The stickyFlag can be set to
        True if the component should be instantiated in a sticky
        way.
        EXAMPLE:
        from CCL.PSD import PSD
        psabe = PSD("DV01")
        '''
       
    
# Dictionary of device instances
        self._devices = {}

        # Group of antenna or component names.
        if ((isinstance(antennaName,list) == True) |
            (isinstance(componentName,list) == True)):
            #antenna names
            if (isinstance(antennaName,list) == True):
                if (len(antennaName) != 0):
                    componentName=[]
                    for idx in range (0,len(antennaName)):
                        self._devices["CONTROL/"+antennaName[idx]+"/PSD"]=""
                        componentName=componentName+["CONTROL/"+antennaName[idx]+"/PSD"]
            #component names
            if (isinstance(componentName,list) == True):
                if (len(componentName) != 0):
                    for idx in range (0,len(componentName)):
                        self._devices[componentName[idx]]=""
        # One component
        else:
            if not((antennaName == None) ^ (componentName == None)):
                raise NameError, "missing antennaName or componentName"

            if antennaName != None:
                componentName=[]
                self._devices["CONTROL/"+antennaName+"/PSD"]=""
                componentName=componentName + ["CONTROL/"+antennaName+"/PSD"]
                antennaName=None

            else:
                componentName != None
                self._devices[componentName] = ""
        
        #At the End, I always send componentName . antennaName must be None   
        antennaName = None
        
        #PSDBase Constructor
        CCL.PSDBase.PSDBase.__init__(self, antennaName,componentName, stickyFlag)
        # Initialize the base class
        for key, val in self._devices.iteritems():
            CCL.HardwareDevice.HardwareDevice.__init__(self, key, stickyFlag);
        self._devices[key]= self._HardwareDevice__hw

        self.__logger = getLogger()

    def __del__(self):
        CCL.PSDBase.PSDBase.__del__(self)

    def STATUS(self):
        '''
        Displays the current status of the PSD
        '''
        result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
        
            # Invoke the generic WCA status helper
            elements = self._PSU__STATUSHelper(key)
            
            ############################### PSD specific ###################################################
            elements.append( StatusHelper.Separator("PSD specific") )
            #GET_MID_1_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_1_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 voltage",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_1_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_1_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 current",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_2_VOLTAGE() 
            try:
               value,t = self._devices[key].GET_MID_2_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 2 voltage",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_2_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_2_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 2 current",[StatusHelper.ValueUnit(value)] ))

            #GET_MID_1_MAX_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_1_MAX_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 max seen voltage.",[StatusHelper.ValueUnit(value)] ))
            #GET_MID_1_MIN_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_1_MIN_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 min seen voltage.",[StatusHelper.ValueUnit(value)] ))
             #GET_MID_1_MAX_CURRENT_SEEN()
            try:
               value,t = self._devices[key].GET_MID_1_MAX_CURRENT_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 max seen current.",[StatusHelper.ValueUnit(value)] ))
            #GET_MID_2_MAX_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_2_MAX_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 2 max seen voltage.",[StatusHelper.ValueUnit(value)] ))
            #GET_MID_2_MIN_VOLTAGE_SEEN()
            try:
               value,t = self._devices[key].GET_MID_2_MIN_VOLTAGE_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 2 min seen voltage.",[StatusHelper.ValueUnit(value)] ))
            #GET_MID_2_MAX_CURRENT_SEEN()
            try:
               value,t = self._devices[key].GET_MID_2_MAX_CURRENT_SEEN()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 2 max seen current.",[StatusHelper.ValueUnit(value)] ))

            elements.append( StatusHelper.Separator("State at last Shutdown") )
            #GET_MID_1_SHUTDOWN_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_1_SHUTDOWN_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 shutdown voltage.",[StatusHelper.ValueUnit(value)] ))
            #GET_MID_1_SHUTDOWN_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_1_SHUTDOWN_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 1 shutdown current.",[StatusHelper.ValueUnit(value)] ))
            #GET_MID_2_SHUTDOWN_VOLTAGE()
            try:
               value,t = self._devices[key].GET_MID_2_SHUTDOWN_VOLTAGE()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 2 shutdown voltage.",[StatusHelper.ValueUnit(value)] ))
            #GET_MID_2_SHUTDOWN_CURRENT()
            try:
               value,t = self._devices[key].GET_MID_2_SHUTDOWN_CURRENT()
            except:
               value = "N/A"
            elements.append( StatusHelper.Line("Mid 2 shutdown current.",[StatusHelper.ValueUnit(value)] ))
            #GET_AMBIENT_TEMP_AT_SHUTDOWN()
            try:
               value,t = self._devices[key].GET_SHUTDOWN_AMBIENT_TEMPERATURE()
               temperature = (value[0]+value[1]*(2**8))/2.0 + 0.25*(value[3]-value[2])/(value[3]*1.0)
            except:
               temperature = "N/A"
            elements.append( StatusHelper.Line("temp. at shutdown.",[StatusHelper.ValueUnit(temperature)] ))

            statusFrame = StatusHelper.Frame(compName + "   Ant: " + antName, elements)
            statusFrame.printScreen()
       #TODO  __del__ method

