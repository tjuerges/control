/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 *
 * $Id$
 *
 *
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/06/23	PSDCommand.h created. Uses Command Pattern
 *------------------------------------------------------------
 */
#ifndef PSDCOMMAND_H_
#define PSDCOMMAND_H_
#include <acstimeTimeUtil.h>
#include <PSUCommand.h>
#include <PSDImpl.h>
class PSDImpl;

class GetMid2VoltageCommand : public FloatCommand { 
public:
	PSDImpl* psdImpl_m;
	GetMid2VoltageCommand(PSDImpl*);
	virtual ~GetMid2VoltageCommand();
	virtual void execute();
};
class GetMid2CurrentCommand : public FloatCommand { 
public:
	PSDImpl* psdImpl_m;
	GetMid2CurrentCommand(PSDImpl*);
	virtual ~GetMid2CurrentCommand();
	virtual void execute();
};
class GetMid1CurrentCommand : public FloatCommand { 
public:
	PSDImpl* psdImpl_m;
	GetMid1CurrentCommand(PSDImpl*);
	virtual ~GetMid1CurrentCommand();
	virtual void execute();
};
class GetMid1VoltageCommand : public FloatCommand { 
public:
	PSDImpl* psdImpl_m;
	GetMid1VoltageCommand(PSDImpl*);
	virtual ~GetMid1VoltageCommand();
	virtual void execute();
};

#endif /*PSDCOMMAND_H_*/
