#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# Component Test Case for PSD
# Author: Pablo Burgos
# Jul 2008

import struct
#import ambManager
import time
from Acspy.Nc.Consumer import Consumer
import com.cosylab.acs.jms
import re
import os
import unittest
import CCL.HardwareDevice
from Acspy.Clients.SimpleClient import PySimpleClient

from CCL.PSD import PSD
regex=re.compile(r"family=\"([a-zA-Z0-9/#_]*)\".*member=\"([a-zA-Z0-9/#_]*)\".*code=\"([0-9]*)\".*descriptor>(\w*)<",re.DOTALL)
       
class alarmMonitor:
    def __init__(self):
        self.alarmList=[]
        self.numAlarms = 0
    def newData(self,dataEvent):
        self.alarmList.append(dataEvent.text)
        self.numAlarms+=1
    def getLastAlarmReceived(self):
        alarmStuff=regex.search(self.alarmList.pop())
        print alarmStuff.groups()
        return alarmStuff.groups()
    
class AlarmPSDTestCase(unittest.TestCase):
    def setUp(self):
        print "Getting a reference to PSD on Simulation CDB"
        self.psdbe=PSD(componentName="CONTROL/DA41/PSD",stickyFlag=True)
        print self.psdbe.getHwState()
        if (self.psdbe.getHwState()=="Operational"):
            self.psdbe.hwStop()
            self.psdbe.hwStart()
            
        else:
            self.psdbe.hwStart()
            
        print self.psdbe.getHwState()
        self.psdbe.hwConfigure()
        self.psdbe.hwInitialize()
        self.psdbe.hwOperational()
        self.simpleClient = PySimpleClient()
        self.simulator = self.simpleClient.getComponent("CONTROL/DA41/PSD")
        time.sleep(4)
        print "Enabling monitoring"
        if not (self.simulator.isMonitoring()):
                self.simulator.monitoringOn()
        print "Getting a Reference for Alarm Monitoring"
        time.sleep(4)
        self.am= alarmMonitor()
        self.c=Consumer("CMW.ALARM_SYSTEM.ALARMS.SOURCES.ALARM_SYSTEM_SOURCES")
        self.c.addSubscription(com.cosylab.acs.jms.ACSJMSMessageEntity, self.am.newData)
        self.c.consumerReady()
        time.sleep(14)
        print "Cleaning All related Alarms"
        time.sleep(10)
        self.simulator.setSimValue(0x00001,[0x0,0xFF,0x00,0x0])
        time.sleep(10)
        
    def tearDown(self):
        print "All alarms going off"
        self.simulator.setSimValue(0x00001,[0x0,0xFF,0x00,0x0])
        
        time.sleep(4)
        del self.am        #Delete alarmonitor reference
        time.sleep(4)
        del self.c
        time.sleep(4)
        self.simulator.monitoringOff()
        self.psdbe.hwStop()
        time.sleep(4)
        del self.psdbe
        time.sleep(4)
        del self.simulator
        
    def testGeneralStatusAlarm(self):
        print "Testing Global Warning alarm...Sending 1"
        self.simulator.setSimValue(0x00001,[0x01,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]== "1", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm state wrong"
        
        time.sleep(4)
        
        print "Testing Global Warning alarm...Sending 0...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="1", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing PS Shutdown....Sending 1....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x02,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="2", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing PS Shutdown.......Sending 0...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="2", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing SHUTDOWN CMD received....Sending 1....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x04,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="3", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing SHUTDOWN CMD received.......Sending 0...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="3", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing over temp ok received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0xFE,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="4", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing SHUTDOWN CMD received.......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="4", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing fan status ok received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0xFD,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="5", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing fan status ok received.......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="5", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing ac status ok received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0xFB,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="6", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing ac status ok received.......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="6", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing dc status ok received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0xF7,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="7", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing dc status ok received.......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="7", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing current limits ok received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0xEF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="8", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing current limits ok received.......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="8", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing modules all good received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0xDF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="9", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing modules all good received......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="9", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing voltage limits ok received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0xBF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="10", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing voltage limits ok received.......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="10", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing FAN Warning ok received....Sending 0....Alarms must pop up"
        self.simulator.setSimValue(0x00001,[0x00,0x7F,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="11", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing FAN Warning ok received.......Sending 1...Alarm should go off.."
        self.simulator.setSimValue(0x00001,[0x00,0xFF,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="11", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
            
        time.sleep(4)
        
        print "Testing shutdown early alarm alarm received...Sending 33 celsius....an Alarm must pop up"
        self.simulator.setSimValue(0x30003,[0x42,0x00,0xFF,0xFF])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="1", "incorrect alarm detected"
        assert alarm[3]=="ACTIVE", "Alarm wrong state"
        
        time.sleep(4)
        
        print "Testing shutdown early alarm...Sending 31 celsius....an Alarm must terminate"
        self.simulator.setSimValue(0x30003,[0x3E,0x00,0x00,0x00])
        time.sleep(4)
        alarm=self.am.getLastAlarmReceived()
        assert alarm[1]=="CONTROL/DA41/PSD","Alarm not detected"
        assert alarm[2]=="1", "incorrect alarm detected"
        assert alarm[3]=="TERMINATE", "Alarm wrong state"
class CCLPSDTestCase(unittest.TestCase):
    def setUp(self):
        print "Getting a reference to psdbe on Simulation CDB"
        self.psdbe=PSD(componentName="CONTROL/DA41/PSD",stickyFlag=True)
        print self.psdbe.getHwState()
        if (self.psdbe.getHwState()=="Operational"):
            self.psdbe.hwStop()
            time.sleep(4)
            self.psdbe.hwStart()
            
        else:
            self.psdbe.hwStart()
        print "State= " + str(self.psdbe.getHwState())
        time.sleep(4)
        print "Configure"
        self.psdbe.hwConfigure()
        time.sleep(4)
        print "Initialize"
        self.psdbe.hwInitialize()
        time.sleep(4)
        print "Operational"
        self.psdbe.hwOperational()
        time.sleep(4)
        print "State= " + str(self.psdbe.getHwState())
        self.simpleClient = PySimpleClient()
        self.simulator = self.simpleClient.getComponent("CONTROL/DA41/PSD")
        time.sleep(4)
        print "Enabling monitoring"
        if not (self.simulator.isMonitoring()):
                self.simulator.monitoringOn()
        print "Cleaning All related Alarms"
        self.simulator.setSimValue(0x00001,[0x0,0xFF,0x00,0x0])
        time.sleep(4)
    def tearDown(self):
        print "All alarms going off"
        self.simulator.setSimValue(0x00001,[0x0,0xFF,0x00,0x0])
        self.simulator.monitoringOff()
        self.psdbe.hwStop()
        time.sleep(4)
        del self.psdbe
        del self.simulator
        time.sleep(4)



        
    def testInternalLowMidCommErrorCCL(self):
        print "Setting low comm error to NO ERROR"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0x00,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        assert lowCommError=="No Low Level Internal Comm Error Reported","Bad Error State reported"
        
        print "Setting low comm error to ERR_RX_0"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0xFF,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        print lowCommError
        assert lowCommError=="ERR_RX_0 Timed Out Waiting for Response","Bad Error State reported"
     
        print "Setting low comm error to ERR_RX_1"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0xFE,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        print lowCommError
        assert lowCommError=="ERR_RX_1 Message Header Greater than 8 Bytes","Bad Error State reported"
        
        print "Setting low comm error to ERR_RX_2"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0xFD,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        print lowCommError
        assert lowCommError=="ERR_RX_2 Timed Out Waiting for Message Byte","Bad Error State reported"

        print "Setting low comm error to ERR_RX_3"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0xFC,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        print lowCommError
        assert lowCommError=="ERR_RX_3 Bad CRC Check","Bad Error State reported"
        
        print "Setting low comm error to ERR_RX_4"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0xFB,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        print lowCommError
        assert lowCommError=="ERR_RX_4 Response CID did not Match Transmitted CID","Bad Error State reported"        

        print "Setting low comm error to ERR_RX_5"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0xFA,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        print lowCommError
        assert lowCommError=="ERR_RX_5 Message Overrun","Bad Error State reported"        

        print "Setting low comm error to ERR_TX_0"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0xF9,0x0,0x00])
        time.sleep(15)
        lowCommError=self.psdbe.getReportedLowInternalCommError()
        print lowCommError
        assert lowCommError=="ERR_TX_0 Timed Out Transmitting Message","Bad Error State reported"    
                         
        print "Setting Mid comm error to NO ERROR"
        self.simulator.setSimValue(0x00102,[0x0,0x00,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="No Mid Level Internal Comm Error Reported","Bad Error State reported"
        
        
        print "Setting Mid comm error to ERR_MODE"
        self.simulator.setSimValue(0x00102,[0x0,0xF8,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_MODE Unrecognized Operating Mode(NodeID)","Bad Error State reported"
        
        print "Setting Mid comm error to ERR_VOLT"
        self.simulator.setSimValue(0x00102,[0x0,0xF7,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_VOLT Error During Voltage Monitor","Bad Error State reported"
        
        print "Setting Mid comm error to ERR_CURR"
        self.simulator.setSimValue(0x00102,[0x0,0xF6,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_CURR Error During Current Monitor","Bad Error State reported"
        
        print "Setting Mid comm error to ERR_STAT_0"
        self.simulator.setSimValue(0x00102,[0x0,0xF5,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_STAT_0 Error While Reading Global Status Byte","Bad Error State reported"
        
        print "Setting Mid comm error to ERR_STAT_1"
        self.simulator.setSimValue(0x00102,[0x0,0xF4,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_STAT_1 Error While Reading Module Status Bits","Bad Error State reported"
        
        print "Setting Mid comm error to ERR_SN"
        self.simulator.setSimValue(0x00102,[0x0,0xF3,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_SN Error Reading Vega Serial Number","Bad Error State reported"
        
        print "Setting Mid comm error to ERR_SD"
        self.simulator.setSimValue(0x00102,[0x0,0xF2,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_SD Error Issuing Shutdown Command","Bad Error State reported"
        
        print "Setting Mid comm error to ERR_SU"
        self.simulator.setSimValue(0x00102,[0x0,0xF1,0x00,0x0,0x00])
        time.sleep(15)
        midCommError=self.psdbe.getReportedMidInternalCommError()
        print midCommError
        assert midCommError=="ERR_SU Error Issuing Start-Up Command","Bad Error State reported"
 
                    

def suite():
    suite = unittest.TestSuite()
    suite.addTest(AlarmPSDTestCase("testGeneralStatusAlarm"))
    suite.addTest(CCLPSDTestCase("testInternalLowMidCommErrorCCL"))
    return suite
if __name__=="__main__":
    unittest.main(defaultTest='suite')
   

