/* @(#) $Id$
 * 
 * 
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PSUAlarm.h
 */
#ifndef PSUALARM_H_
#define PSUALARM_H_
#include <list>
#include <string>
//next includes for Alarm System
#include <ACSAlarmSystemInterfaceFactory.h>
#include <AlarmSystemInterface.h>
#include <FaultState.h>
#include <faultStateConstants.h>

#include <acstimeTimeUtil.h>
#include <PSUCommand.h>

using namespace std;
class AlarmState;
class Command;
class Alarm{
public:
	Alarm();
	virtual ~Alarm();
	int alarmCode_m;
	AlarmState* alarmState_m;
	string family_m;
	string member_m;
	string errorMessage_m;
	Command* command_m;
	void setCurrentState(AlarmState* _alarmState);
	void on();
	void off();
	string getErrorMessage();
	int getAlarmCode();
	virtual void evaluateAlarmState()=0;
	//void sendAlarm(bool isActive);
	void sendAlarm(bool _isActive);
private:
  
        acsalarm::AlarmSystemInterface* alarmSource_m;

	
};
class BooleanAlarm: public Alarm{
public:
	~BooleanAlarm();
	bool normalValue_m;
	BooleanAlarm(int _alarmCode,
			string _family, 
			string _member,
			string _errorMessage,
			Command* _command,
			bool normalValue);
	
	virtual void evaluateAlarmState();
	
};
class FloatAlarm: public Alarm{
public:
	~FloatAlarm();
	float minRange_m;
	float maxRange_m;
	FloatAlarm(int _alarmCode,
			string _family, 
			string _member, 
			string _errorMessage,
			Command* _command,
			float minRange,
			float maxRange);
	
	virtual void evaluateAlarmState();
};
class AlarmHandler{
public:
	list<Alarm*> alarmList_m;
	void addAlarmToList(Alarm* _alarm);
	void removeAlarmFromList(Alarm* _alarm);
	void evaluateAlarmStateList();
	Alarm* getAlarm(int _alarmCode);
	AlarmHandler();
	~AlarmHandler();
	void clearAllAlarmFlags();
	
	
};

class AlarmState{
public:
	virtual void on (Alarm*){};//do nothing
	virtual void off(Alarm*){};//do nothing
        virtual ~AlarmState(){};
};
class OffState: public AlarmState{
public:
	OffState();
	virtual void on(Alarm* _alarm);
        virtual ~OffState(){};
};
class OnState: public AlarmState{
public:
	OnState();
	virtual void off(Alarm* _alarm);
        virtual ~OnState(){};
};
#endif /*PSUALARM_H_*/
