/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 *
 * $Id$
 *
 *
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/06/23	PSUCommand.h created. Uses Command Pattern
 *------------------------------------------------------------
 */
#ifndef PSUCOMMAND_H_
#define PSUCOMMAND_H_
#include <acstimeTimeUtil.h>
#include <PSUImpl.h>
class PSUImpl;
class Command {
public:
	virtual void execute()=0;
        virtual ~Command(){};
};

class BooleanCommand : public Command{
public:
	bool currentValue_m;
	virtual bool getCurrentValue();
	virtual void execute();
        virtual ~BooleanCommand(){};
};

class FloatCommand : public Command{
public:
	float currentValue_m;
	virtual float getCurrentValue();
	virtual void execute();
        virtual ~FloatCommand(){};

};
class GlobalWarningCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	GlobalWarningCommand(PSUImpl*);
	virtual ~GlobalWarningCommand();
	virtual void execute();
};
class PSShutdownCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	PSShutdownCommand(PSUImpl*);
	virtual ~PSShutdownCommand();
	virtual void execute();
};
class ShutdownReceivedCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	ShutdownReceivedCommand(PSUImpl*);
	virtual ~ShutdownReceivedCommand();
	virtual void execute();
};

class OverTemperatureCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	OverTemperatureCommand(PSUImpl*);
	virtual ~OverTemperatureCommand();
	virtual void execute();
};

class FanStatusCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	FanStatusCommand(PSUImpl*);
	virtual ~FanStatusCommand();
	virtual void execute();
};

class ACStatusCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	ACStatusCommand(PSUImpl*);
	virtual ~ACStatusCommand();
	virtual void execute();
};

class DCStatusCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	DCStatusCommand(PSUImpl*);
	virtual ~DCStatusCommand();
	virtual void execute();
};

class CurrentLimitsCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	CurrentLimitsCommand(PSUImpl*);
	virtual ~CurrentLimitsCommand();
	virtual void execute();
};

class InternalModuleCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	InternalModuleCommand(PSUImpl*);
	virtual ~InternalModuleCommand();
	virtual void execute();
};

class VoltageLimitsCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	VoltageLimitsCommand(PSUImpl*);
	virtual ~VoltageLimitsCommand();
	virtual void execute();
};
class FanWarningCommand : public BooleanCommand {
public:
	PSUImpl* psuImpl_m;
	FanWarningCommand(PSUImpl*);
	virtual ~FanWarningCommand();
	virtual void execute();
};


class GetTemperatureCelsiusCommand : public FloatCommand {
public:
	PSUImpl* psuImpl_m;
	GetTemperatureCelsiusCommand(PSUImpl*);
	virtual ~GetTemperatureCelsiusCommand();
	virtual void execute();
};
#endif /*PSUCOMMAND_H_*/
