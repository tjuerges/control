/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 *
 * $Id$
 *
 *
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/06/23	PSUCommand.cpp created. Uses Command Pattern
 *------------------------------------------------------------
 */
//Command stuff
#include <PSUCommand.h>

bool BooleanCommand::getCurrentValue() {
	ACS_TRACE("BooleanCommand::getCurrentValue()");
	return currentValue_m;
}
void BooleanCommand::execute() {
	ACS_TRACE("BooleanCommand::execute()");

}

float FloatCommand::getCurrentValue() {
	ACS_TRACE("FloatCommand::getCurrentValue()");
	return currentValue_m;
}
void FloatCommand::execute() {
	ACS_TRACE("FloatCommand::execute()");

}
GlobalWarningCommand::GlobalWarningCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("GlobalWarningCommand::GlobalWarningCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void GlobalWarningCommand::execute() {
	ACS_TRACE("GlobalWarningCommand::execute()");
	const string fnName = "GlobalWarningCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_GLOBAL_WARNING(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

GlobalWarningCommand::~GlobalWarningCommand() {
	ACS_TRACE("GlobalWarningCommand::~GlobalWarningCommand()");
}

PSShutdownCommand::PSShutdownCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("GlobalWarningCommand::GlobalWarningCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void PSShutdownCommand::execute() {
	ACS_TRACE("GlobalWarningCommand::execute()");
	const string fnName = "PSShutdownCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_PS_SHUTDOWN(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}
}

PSShutdownCommand::~PSShutdownCommand() {
	ACS_TRACE("PSShutdownCommand::~PSShutdownCommand()");
}

ShutdownReceivedCommand::ShutdownReceivedCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("ShutdownReceivedCommand::ShutdownReceivedCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void ShutdownReceivedCommand::execute() {
	ACS_TRACE("ShutdownReceivedCommand::execute()");
	const string fnName = "ShutdownReceivedCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_SHUTDOWN_CMD_RECEIVED(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

ShutdownReceivedCommand::~ShutdownReceivedCommand() {
	ACS_TRACE("ShutdownReceivedCommand::~ShutdownReceivedCommand()");
}

OverTemperatureCommand::OverTemperatureCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("OverTemperatureCommand::OverTemperatureCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void OverTemperatureCommand::execute() {
	ACS_TRACE("OverTemperatureCommand::execute()");
	const string fnName = "OverTemperatureCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_OVER_TEMP_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}
}

OverTemperatureCommand::~OverTemperatureCommand() {
	ACS_TRACE("OverTemperatureCommand::~OverTemperatureCommand()");
}

FanStatusCommand::FanStatusCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("FanStatusCommand::FanStatusCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void FanStatusCommand::execute() {
	ACS_TRACE("FanStatusCommand::execute()");
	const string fnName = "FanStatusCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_FAN_STATUS_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

FanStatusCommand::~FanStatusCommand() {
	ACS_TRACE("FanStatusCommand::~FanStatusCommand()");
}

ACStatusCommand::ACStatusCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("ACStatusCommand::ACStatusCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void ACStatusCommand::execute() {
	ACS_TRACE("ACStatusCommand::execute()");
	const string fnName = "ACStatusCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_AC_STATUS_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

ACStatusCommand::~ACStatusCommand() {
	ACS_TRACE("ACStatusCommand::~ACStatusCommand()");
}

DCStatusCommand::DCStatusCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("DCStatusCommand::DCStatusCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void DCStatusCommand::execute() {
	ACS_TRACE("DCStatusCommand::execute()");
	const string fnName = "DCStatusCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_DC_STATUS_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

DCStatusCommand::~DCStatusCommand() {
	ACS_TRACE("DCStatusCommand::~DCStatusCommand()");
}

CurrentLimitsCommand::CurrentLimitsCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("CurrentLimitsCommand::CurrentLimitsCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void CurrentLimitsCommand::execute() {
	ACS_TRACE("GlobalWarningCommand::execute()");
	const string fnName = "CurrentLimitsCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_CURRENT_LIMITS_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

CurrentLimitsCommand::~CurrentLimitsCommand() {
	ACS_TRACE("CurrentLimitsCommand::~CurrentLimitsCommand()");
}

InternalModuleCommand::InternalModuleCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("InternalModuleCommand::InternalModuleCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void InternalModuleCommand::execute() {
	ACS_TRACE("InternalModuleCommand::execute()");
	const string fnName = "InternalModuleCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_MODULES_ALL_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

InternalModuleCommand::~InternalModuleCommand() {
	ACS_TRACE("InternalModuleCommand::~InternalModuleCommand()");
}

VoltageLimitsCommand::VoltageLimitsCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("VoltageLimitsCommand::VoltageLimitsCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void VoltageLimitsCommand::execute() {
	ACS_TRACE("VoltageLimitsCommand::execute()");
	const string fnName = "VoltageLimitsCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_VOLTAGE_LIMITS_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

VoltageLimitsCommand::~VoltageLimitsCommand() {
	ACS_TRACE("VoltageLimitsCommand::~VoltageLimitsCommand()");
}

FanWarningCommand::FanWarningCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("FanWarningCommand::FanWarningCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=false;
}
void FanWarningCommand::execute() {
	ACS_TRACE("FanWarningCommand::execute()");
	const string fnName = "FanWarningCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	ACS::Time ts;
	try {
		currentValue_m=psuImpl_m->GET_FAN_WARNING_OK(ts);
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

FanWarningCommand::~FanWarningCommand() {
	ACS_TRACE("FanWarningCommand::~FanWarningCommand()");
}


GetTemperatureCelsiusCommand::GetTemperatureCelsiusCommand(PSUImpl* _psuImpl) {
	ACS_TRACE("GetTemperatureCelsiusCommand::GetTemperatureCelsiusCommand()");
	psuImpl_m=_psuImpl;
	currentValue_m=0;
}
void GetTemperatureCelsiusCommand::execute() {
	ACS_TRACE("GetTemperatureCelsiusCommand::execute()");
	const string fnName = "GetTemperatureCelsiusCommand::execute()";
	const string antName = psuImpl_m->getComponent()->getName();
	try {
		currentValue_m=psuImpl_m->GET_AMBIENT_TEMPERATURE();
	} catch(const ControlExceptions::INACTErrorExImpl& ex) {
		ostringstream message;
		message << "PS device on  "
		<< antName
		<< " is not ready. Please wait....";
		ControlExceptions::INACTErrorExImpl localEx(ex,__FILE__,__LINE__,fnName.c_str());
		localEx.addData("Detail", message.str());
		localEx.log();
	}

}

GetTemperatureCelsiusCommand::~GetTemperatureCelsiusCommand() {
	ACS_TRACE("GetTemperatureCelsiusCommand::~GetTemperatureCelsiusCommand()");
}

