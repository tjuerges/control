
/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSU;

import alma.Control.device.gui.PSU.presentationModels.DevicePM;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.common.gui.chessboard.ChessboardDetailsPluginFactory;
import alma.common.gui.chessboard.ChessboardPlugin;
import alma.common.gui.chessboard.ChessboardStatusProvider;

/**
 * A plug-in to the Operator Master Client that will display a Chessboard.  This allows a user to 
 * select a hardware device they are interested in by antenna, and display a panel containing 
 * control and monitor points for that hardware device.  
 * 
 * See: http://almasw.hq.eso.org/almasw/bin/view/EXEC/SubsystemPlugins for a description of the 
 * Operator Master Client plug-in interface.
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington  sharring@nrao.edu
 * @since   ALMA 5.0.3
 */
public class OmcPlugin extends ChessboardPlugin {
    
    // TODO: serial version UID
    private static final long serialVersionUID = 1L;

    public OmcPlugin() {
        super(true);
        
        //
        // Temporary setting for development
        System.setProperty("alma.common.log.ExcLog", "true");
    }

    /**
     * Create and return a reference to a chessboard details provider.  This is an object that 
     * allows the chessboard to launch a device specific UI.
     * 
     * @return the chessboard's details provider.
     */
    @Override
    protected ChessboardDetailsPluginFactory getDetailsProvider() {
        detailsProvider = new DetailsPluginFactory();
        return detailsProvider;
    }

    /**
     * Create and return a reference to a chessboard status provider.  This is an object that 
     * allows the chessboard to display availability of devices in antenna in a UI. 
     * 
     * @return the chessboard's status provider.
     */
    @Override
    protected ChessboardStatusProvider getStatusProvider() {
        statusProvider = new HardwareDeviceChessboardPresentationModel(
            pluginContainerServices, 
            DevicePM.getDeviceName()
        );
        return statusProvider;
    }    
}

//
// O_o
        
