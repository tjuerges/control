/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSU.presentationModels;

import alma.Control.PSUHelper;
import alma.Control.device.gui.PSU.IcdControlPoints;
import alma.Control.device.gui.PSU.IcdMonitorPoints;
import alma.Control.device.gui.PSU.IdlControlPoints;
import alma.Control.device.gui.PSU.IdlMonitorPoints;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IControlPoint;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.exec.extension.subsystemplugin.PluginContainerServices;

/**
 * Model the details of a device to be displayed in a user interface. 
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington  sharring@nrao.edu
 * @since   ALMA 5.0.3
 * 
 * TODO: Add public methods to move monitor points between short and long polling groups
 * TODO: Add public methods to get list of monitor points in short and long polling groups
 * TODO: Split out Generic code
 * TODO: Add ExcLog to all logged exceptions.
 */
public class DevicePM extends DevicePresentationModel {

    /**
     * @param antennaName the name of the antenna containing this device.
     * @param services the OMC container services.
     */
    public DevicePM(String antennaName, PluginContainerServices services) {
        super(antennaName, services);
    }

    /**
     * Return a string description of the device for use in GUI displays.
     * 
     * TODO: This is duplicated among several classes.  Find a way to move this to a superclass while 
     *       allowing access to DEVICE_NAME to the <device>OmcPlugin classes.
     */
    public String getDeviceDescription() {
      return DEVICE_NAME + ":" + antennaName + ":" + instance;
    }

    /**
     * Return a string name of the device for use in GUI displays.
     * 
     * TODO: This is duplicated among several classes.  Find a way to move this to a superclass while 
     *       allowing access to DEVICE_NAME to the <device>ChessboardPlugin classes.
     */
    public static String getDeviceName() {
      return DEVICE_NAME;
    }
    
    protected void createIcdControlPointPresentationModels() {
        ControlPointPM newCP;
        
        for (IcdControlPoints controlPoint: IcdControlPoints.values()) {
            if (Boolean.class == controlPoint.getTypeClass() || boolean.class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Boolean>(logger, this, controlPoint, boolean.class);
            else if (Boolean[].class == controlPoint.getTypeClass() || boolean[].class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Boolean[]>(logger, this, controlPoint, boolean.class);

            else if (Byte.class == controlPoint.getTypeClass() || byte.class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Byte>(logger, this, controlPoint, byte.class);
            else if (Byte[].class == controlPoint.getTypeClass() || byte[].class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Byte[]>(logger, this, controlPoint, byte.class);

            else if (Double.class == controlPoint.getTypeClass() || double.class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Double>(logger, this, controlPoint, double.class);
            else if (Double[].class == controlPoint.getTypeClass() || double[].class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Double[]>(logger, this, controlPoint, double.class);

            else if (Float.class == controlPoint.getTypeClass() || float.class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Float>(logger, this, controlPoint, float.class);
            else if (Float[].class == controlPoint.getTypeClass() || float[].class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Float[]>(logger, this, controlPoint, float.class);

            else if (Integer.class == controlPoint.getTypeClass() || int.class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Integer>(logger, this, controlPoint, int.class);
            else if (Integer[].class == controlPoint.getTypeClass() || int[].class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Integer[]>(logger, this, controlPoint, int.class);

            else if (Long.class == controlPoint.getTypeClass() || long.class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Long>(logger, this, controlPoint, long.class);
            else if (Long[].class == controlPoint.getTypeClass() || long[].class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Long[]>(logger, this, controlPoint, long.class);

            else if (Short.class == controlPoint.getTypeClass() || short.class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Short>(logger, this, controlPoint, short.class);
            else if (Short[].class == controlPoint.getTypeClass() || short[].class == controlPoint.getTypeClass())
                newCP = new ControlPointPM<Short[]>(logger, this, controlPoint, short.class);

            else {
                if (controlPoint.getArrayLength() == 0) {
                    // This control point does not take parameters.
                    newCP = new ControlPointPM(logger, this, controlPoint, null);
                } else {
                    logger.warning(
                        "DevicePM.createControlPointPresentationModels(): " +
                        "Unknown type found while creating new ControlPointPresentationModel: " +
                        controlPoint.getTypeClass()
                    );
                    newCP = null;
                }
            }
            controlPointPMs.put(controlPoint, newCP);
        }
    }

    protected void createIdlControlPointPresentationModels() {
        IdlPMBuilder idlPMBuilder = new IdlPMBuilder(logger, this);
        idlPMBuilder.createControlPointPresentationModels();
        for (IdlControlPoints controlPoint: idlPMBuilder.controlPointPMs.keySet()) {
            controlPointPMs.put(controlPoint, idlPMBuilder.controlPointPMs.get(controlPoint));
        }
    }
    
    protected void createIcdMonitorPointPresentationModels() {
        // The newMP holds several types of presentation models.
        MonitorPointPM newMP;
        
        for (IcdMonitorPoints monitorPoint: IcdMonitorPoints.values()) {
            if (Boolean.class == monitorPoint.getTypeClass() || boolean.class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Boolean>(logger, this, monitorPoint);
            else if (Boolean[].class == monitorPoint.getTypeClass() || boolean[].class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Boolean[]>(logger, this, monitorPoint);

            else if (Byte.class == monitorPoint.getTypeClass() || byte.class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Byte>(logger, this, monitorPoint);
            else if (Byte[].class == monitorPoint.getTypeClass() || byte[].class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Byte[]>(logger, this, monitorPoint);

            else if (Double.class == monitorPoint.getTypeClass() || double.class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Double>(logger, this, monitorPoint);
            else if (Double[].class == monitorPoint.getTypeClass() || double[].class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Double[]>(logger, this, monitorPoint);

            else if (Float.class == monitorPoint.getTypeClass() || float.class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Float>(logger, this, monitorPoint);
            else if (Float[].class == monitorPoint.getTypeClass() || float[].class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Float[]>(logger, this, monitorPoint);

            else if (Integer.class == monitorPoint.getTypeClass() || int.class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Integer>(logger, this, monitorPoint);
            else if (Integer[].class == monitorPoint.getTypeClass() || int[].class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Integer[]>(logger, this, monitorPoint);

            else if (Long.class == monitorPoint.getTypeClass() || long.class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Long>(logger, this, monitorPoint);
            else if (Long[].class == monitorPoint.getTypeClass() || long[].class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Long[]>(logger, this, monitorPoint);

            else if (Short.class == monitorPoint.getTypeClass() || short.class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Short>(logger, this, monitorPoint);
            else if (Short[].class == monitorPoint.getTypeClass() || short[].class == monitorPoint.getTypeClass())
                newMP = new MonitorPointPM<Short[]>(logger, this, monitorPoint);

            else {
                logger.warning("DevicePM.createMonitorPointPresentationModels(): " +
                    "Unknown type found while creating new MonitorPointPresentationModel: " +
                    monitorPoint.getTypeClass()
                );
                newMP = null;
            }
            monitorPointPMs.put(monitorPoint, newMP);
        } 
    }

    protected void createIdlMonitorPointPresentationModels() {
        IdlPMBuilder idlPMBuilder = new IdlPMBuilder(logger, this);
        idlPMBuilder.createMonitorPointPresentationModels();
        for (IdlMonitorPoints monitorPoint: idlPMBuilder.monitorPointPMs.keySet()) {
            monitorPointPMs.put(monitorPoint, idlPMBuilder.monitorPointPMs.get(monitorPoint));
        }
    }
    
    protected void setComponentName() {
        componentName = HardwareDeviceChessboardPresentationModel.getComponentNameForDevice(
            pluginContainerServices,
            antennaName,
            DEVICE_NAME);
    }
    
    protected void setDeviceComponent() {
        if (null == componentName) {
            setComponentName();
        }
        
        if (null == deviceComponent && null != componentName) {
            try {
                org.omg.CORBA.Object corbaObject = 
                    this.pluginContainerServices.getComponentNonSticky(componentName);
                this.deviceComponent = PSUHelper.narrow(corbaObject);
                
                for (IControlPoint controlPoint: controlPointPMs.keySet())
                    if (null != controlPointPMs.get(controlPoint)) {    
                        controlPointPMs.get(controlPoint).setDeviceComponent(this.deviceComponent);
                    } else {
                        logger.warning(
                            "DevicePM.setDeviceComponent() " + 
                            "failed to find controlPointPM for " +
                            controlPoint
                        );                      
                    }
                for (IMonitorPoint monitorPoint: monitorPointPMs.keySet())
                    if (null != monitorPointPMs.get(monitorPoint)) {
                        monitorPointPMs.get(monitorPoint).setDeviceComponent(this.deviceComponent);
                    } else {
                        logger.warning(
                            "DevicePM.setDeviceComponent() " + 
                            "failed to find monitorPointPM for " +
                            monitorPoint
                        );
                    }
            } catch (AcsJContainerServicesEx ex) {
                logger.warning("DevicePM.setDeviceComponent() - " + 
                               "unable to obtain PSU component: "
                               + componentName);
                this.deviceComponent = null;

                for (IControlPoint controlPoint: controlPointPMs.keySet())
                    controlPointPMs.get(controlPoint).setDeviceComponent(null);
                for (IMonitorPoint monitorPoint: monitorPointPMs.keySet())
                    monitorPointPMs.get(monitorPoint).setDeviceComponent(null);
                ex.printStackTrace();
                // TODO: Should this exception be propagated up?
            }
        }
    }

    private static final String DEVICE_NAME = "PSU";
}

//
// O_o
        
