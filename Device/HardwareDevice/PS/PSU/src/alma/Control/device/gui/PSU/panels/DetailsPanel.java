/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.PSU.panels;

import alma.Control.device.gui.PSU.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Detail data.
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author Steve Harrington  sharring@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DetailsPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DetailsPanel(Logger logger, DevicePM pm) {
    	super(logger, pm);
    }

    protected void buildPanel()
    {
        this.setBorder(
                BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Details"),
                    BorderFactory.createEmptyBorder(1,1,1,1)
                )
        );
        
        // This class will not be instantiated.  Don't bother filling out.
        // It exists to allow PSUExecPlugin.jar to build.
        // Other power supply GUIs will use classes in this .jar.

        this.setVisible(true);
    }
}

//
// O_o
