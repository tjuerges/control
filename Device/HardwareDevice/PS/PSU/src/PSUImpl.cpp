//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// pburgos  2008/05/14    PSUImpl.cpp created
// pburgos  2008/06/10    Alarms Added
// pburgos  2008/09/23    Internal Error codes getters added


/**
 * <P>
 *         PSUImpl Power Supply Unit Implementation
 * <P>
 *
 * This is the Common class for all the others real implementations of power supplies.
 * @see  PSUImplBase
 */


#include <PSUImpl.h>
#include <loggingMACROS.h>


/*------------PSUImpl Constructor-----------------*/
PSUImpl::PSUImpl(const ACE_CString& _name, maci::ContainerServices* cs):
    PSUBase(_name, cs), //updateThreadCycleTime_m(10000000),
    handler_m(new AlarmHandler),
    testModeState(false)
{
    //Monitoring done each 1 second. Since each unit is 100E-9 sec, we need 10000000 units :)

    ACS_TRACE(__PRETTY_FUNCTION__);
    //Here I create a reference to AlarmHandler and start ading alarms

}

/*------------PSUImpl Destructor-------------------*/
PSUImpl::~PSUImpl()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    delete handler_m;
}

/**
 * --------------------------------------------------------------
 *      Component Lifecycle Methods
 * --------------------------------------------------------------
 */
//---------->   Initialize
void PSUImpl::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        PSUBase::initialize(); //Initialize call to the base class
    }
    catch(acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    const CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    std::string threadName(compName.in());
    threadName += "MonitoringThread";

    handler_m->addAlarmToList(new BooleanAlarm(2, "PSU",
        getComponent()->getName(), "PS Shutdown Reported",
        new PSShutdownCommand(this), false));
    handler_m->addAlarmToList(new BooleanAlarm(3, "PSU",
        getComponent()->getName(), "Shutdown command has been received",
        new ShutdownReceivedCommand(this), false));
    handler_m->addAlarmToList(new BooleanAlarm(4, "PSU",
        getComponent()->getName(), "Over Temperature Detected",
        new OverTemperatureCommand(this), true));
    handler_m->addAlarmToList(new BooleanAlarm(5, "PSU",
        getComponent()->getName(), "Fan Status reported an error",
        new FanStatusCommand(this), true));
    handler_m->addAlarmToList(new BooleanAlarm(6, "PSU",
        getComponent()->getName(), "AC Status reported an error",
        new ACStatusCommand(this), true));
    handler_m->addAlarmToList(new BooleanAlarm(7, "PSU",
        getComponent()->getName(), "DC Status reported an error",
        new DCStatusCommand(this), true));
    handler_m->addAlarmToList(new BooleanAlarm(8, "PSU",
        getComponent()->getName(), "Current Limits out of range",
        new CurrentLimitsCommand(this), true));
    handler_m->addAlarmToList(new BooleanAlarm(10, "PSU",
        getComponent()->getName(), "Voltage Limits out of Operational Range",
        new VoltageLimitsCommand(this), true));
}

//--------->  CleanUp
void PSUImpl::cleanUp()
{

    ACS_TRACE(__PRETTY_FUNCTION__);
    //updateThread_p->terminate();//Terminates the Thread
    try
    {
        /* Clear all the alarms not BACI if any is activated*/
        //PSUImpl::clearAllAlarms();
        handler_m->clearAllAlarmFlags();
        //handler_m->~AlarmHandler();
        PSUBase::cleanUp();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

/*
 * ------------------------------
 * Hardware Lifecycle Methods. States
 * ------------------------------
 */

//----------> Hardware initialization

void PSUImpl::hwInitializeAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    //resetErrorFlags();
    handler_m->clearAllAlarmFlags();
    PSUBase::hwInitializeAction();
    //updateThread_p->suspend(); //suspend the execution of the Thread
    //PSUImpl::clearAllAlarms();
}
//----------> Hardware Operational

void PSUImpl::hwOperationalAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    PSUBase::hwOperationalAction();
    //updateThread_p->resume(); //Here we continue the execution of the resumed thread
}

//---------> Hardware Stop

void PSUImpl::hwStopAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
    //updateThread_p->suspend(); //we suspend the monitor control point  thread
    PSUBase::hwStopAction(); //and call stop on the base class :)
    //resetErrorFlags(); //clean the state of all alarms in our helper class
    handler_m->clearAllAlarmFlags();
}

void PSUImpl::processStatusMonitors()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(isReady() == false)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("ErrorCode",
            static_cast< int >(ControlExceptions::INACTError));
        throw ex;
    }

    handler_m->evaluateAlarmStateList();
}

//-------------->AdHoc Methods
char* PSUImpl::GET_SHUTDOWN_ERROR_MSG()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time ts(0ULL);
    int errorCode = 0;
    try
    {
        errorCode = PSUBase::getShutdownErrorCode(ts);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl localEx(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        localEx.log();

    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "Failed to get Shutdown Error Code Monitor "
            "Point.");
    }
    return CORBA::string_dup(PSUConstantDataManager::getFailsafeTriggerMessage(
        errorCode));

}
char* PSUImpl::GET_REPORTED_LAMBDA_ERROR()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time ts(0ULL);
    int errorCode = 0;
    try
    {
        errorCode = PSUBase::getLastErrorReceivedFromLambda(ts);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl localEx(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        localEx.log();

    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "Failed to get Reported Lambda Error "
            "Monitor Point.");
    }
    return CORBA::string_dup(
        PSUConstantDataManager::getReportedLambdaErrorCode(errorCode));

}

char* PSUImpl::GET_REPORTED_LOW_INTERNAL_COMM_ERROR()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time ts(0ULL);
    int errorCode = 0;
    try
    {
        errorCode = PSUBase::getLowLevelFunctionWhereErrorOcurred(ts);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl localEx(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        localEx.log();

    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "Failed to get Reported Low Internal Comm "
            "Error Monitor Point.");
    }
    return CORBA::string_dup(
        PSUConstantDataManager::getReportedLowLevelInternalCommunicationErrorMsg(
            errorCode));

}

char * PSUImpl::GET_REPORTED_MID_INTERNAL_COMM_ERROR()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time ts(0ULL);
    int errorCode = 0;
    try
    {
        errorCode = PSUBase::getMidLevelFunctionWhereErrorOcurred(ts);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl localEx(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        localEx.log();

    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "Failed to get Reported Mid Internal Comm "
            "Error Monitor Point.");
    }
    return CORBA::string_dup(
        PSUConstantDataManager::getReportedMidLevelInternalCommunicationErrorMsg(
            errorCode));

}
float PSUImpl::GET_AMBIENT_TEMP_AT_SHUTDOWN()
{
    // This method allow us to read and calculate
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time ts(0ULL);
    std::vector< unsigned char > v;
    int temp = 0;
    int sign = 0;
    int countRem = 0;
    int countPerC = 1;
    try
    {
        /*Next instrucction just to update state variables on Base class
         referring to this monitor point. See generated code.
         */
        v = PSUBase::getShutdownAmbientTemperature(ts);
        temp = getTempR0(ts);
        sign = getTempR1(ts);
        countRem = getCountRemaining(ts);
        countPerC = getCountPerC(ts);

    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl localEx(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        localEx.log();

    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "Failed to get Shutdown Ambient "
            "Temperature Monitor Point.");
    }

    return PSUBase::ds1820Temp(temp, sign, countRem, countPerC);

}

float PSUImpl::GET_AMBIENT_TEMPERATURE()
{
    // This method allow us to read and calculate
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACS::Time ts(0ULL);
    float ret(-999.9);
    try
    {
        ret = PSUBase::getAmbientTemperature(ts);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl localEx(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        localEx.log();
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "Failed to get Ambient Temperature Monitor "
            "Point.");
    }

    return ret;
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(PSUImpl)
