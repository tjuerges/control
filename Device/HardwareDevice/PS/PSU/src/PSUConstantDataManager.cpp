/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * File PSUImpl.cpp
 *
 *
 * $Id$
 *
 *
 *      who                     when                    what
 *------------------------------------------------------------
 *      pburgos         2008/05/20      PSUConstantDataManager.cpp created
 *      pburgos			2008/09/23		Internal Error Codes added
 *------------------------------------------------------------
 */
//Constructor
#include "PSUConstantDataManager.h"
char * PSUConstantDataManager::getFailsafeTriggerMessage(int errorCode) {
	switch (errorCode) {
	case 0x01:
		return "AMBSI1 temperature exceeds a pre-defined limit";
		break;
	case 0x02:
		return "Lambda status bits reported a failure.";
		break;
	case 0x03:
		return "Lambda non-communicative. It has failed to respond to internal communications 3 times in a row.";
		break;
	case 0x04:
		return "9 bad message CRC in a row";
		break;
	case 0x20:
		return "Sub-Module 1 voltage output has exceeded safe  operation range 3 times in a row";
	case 0x21:
		return "Sub-Module 1 voltage output is insufficient. Out of safe operation range";
	case 0x22:
		return "Sub-Module 1 current output has exceeded safe  operation range 3 times in a row";
	case 0x40:
		return "Sub-Module 2 voltage output has exceeded safe  operation range 3 times in a row";
	case 0x41:
		return "Sub-Module 2 voltage output is insufficient. Out of safe operation range";
	case 0x42:
		return "Sub-Module 2 current output has exceeded safe  operation range 3 times in a row";
	case 0x60:
		return "Sub-Module 3 voltage output has exceeded safe  operation range 3 times in a row";
	case 0x61:
		return "Sub-Module 3 voltage output  is insufficient. Out of safe operation range";
	case 0x62:
		return "Sub-Module 3 current output has exceeded safe  operation range 3 times in a row";
	case 0x80:
		return "Sub-Module 4 voltage output has exceeded safe  operation range 3 times in a row";
	case 0x81:
		return "Sub-Module 4 voltage output is insufficient. Out of safe operation range";
	case 0x82:
		return "Sub-Module 4 current output has exceeded safe  operation range 3 times in a row";
	case 0xA0:
		return "Sub-Module 5 voltage output has exceeded safe  operation range 3 times in a row";
	case 0xA1:
		return "Sub-Module 5 voltage output is insufficient. Out of safe operation range";
	case 0xA2:
		return "Sub-Module 5 current output has exceeded safe  operation range 3 times in a row";
	default:
		return "All normal. No shutdown code recorded";
	}
}
char * PSUConstantDataManager::getReportedLambdaErrorCode(int errorCode) {
	switch (errorCode) {
	case 0x00:
		return "Sub-Module CPU: ERROR";
	case 0x01:
		return "Sub-Module CPU: Unrecognize command";
	case 0x02:
		return "Sub-Module CPU: Bad CRC";
	case 0x03:
		return "Sub-Module CPU: Buffer Overrun";
	case 0x04:
		return "Sub-Module CPU: Framing Error";
	case 0x05:
		return "Sub-Module CPU: Invalid Command";
	case 0x06:
		return "Sub-Module CPU: Timeout";
	case 0x07:
		return "Sub-Module CPU: Trailing Garbage error";
	case 0x0B:
		return "Sub-Module CPU: EEPROM write 8 fail";
	case 0x0C:
		return "Sub-Module CPU: EEPROM write 16 fail";
	case 0x0D:
		return "Sub-Module CPU: EEPROM lock fail";
	case 0x65:
		return "Vega System CPU: Wrong Message";
	case 0x66:
		return "Vega System CPU: Wrong Group Msg";
	case 0x67:
		return "Vega System CPU: Wrong Module";
	case 0x68:
		return "Vega System CPU: Wrong CMD for Sc";
	case 0x69:
		return "Vega System CPU: Wrong Data Type";
	case 0x6A:
		return "Vega System CPU: UART Receive CRC error";
	case 0x6B:
		return "Vega System CPU: Module Time out";
	case 0x6C:
		return "Vega System CPU: Wrong Module CID";
	case 0x6D:
		return "Vega System CPU: Wrong Module MID";
	case 0x6E:
		return "Vega System CPU: EEPROM write failed";
	case 0x6F:
		return "Vega System CPU: Module not present";
	case 0xC9:
		return "Buffer CPU: Software UART Buffer Overrun Error";
	case 0xCA:
		return "Buffer CPU: Software UART Receive CRC Error";
	case 0xCB:
		return "Buffer CPU: Timeout Error";
	case 0xCD:
		return "Buffer CPU: Hardware UART Buffer Overrun Error";
	case 0xCE:
		return "Buffer CPU: Hardware UART Receive CRC Error";
	case 0xFF:
		return "No Lambda Error Reported";
	default:
		return "No Lambda Error Reported";
	}
}
char * PSUConstantDataManager::getReportedMidLevelInternalCommunicationErrorMsg(
		int errorCode) {
	switch (errorCode) {
	case 0:
		return "No Mid Level Internal Comm Error Reported";
	case 248: // -8 in 2's complement
		return "ERR_MODE Unrecognized Operating Mode(NodeID)";
	case 247:
		return "ERR_VOLT Error During Voltage Monitor";
	case 246:
		return "ERR_CURR Error During Current Monitor";
	case 245:
		return "ERR_STAT_0 Error While Reading Global Status Byte";
	case 244:
		return "ERR_STAT_1 Error While Reading Module Status Bits";
	case 243:
		return "ERR_SN Error Reading Vega Serial Number";
	case 242:
		return "ERR_SD Error Issuing Shutdown Command";
	case 241:
		return "ERR_SU Error Issuing Start-Up Command";
	default:
		return "No Mid Level Internal Comm Error Reported";
	}
}
char * PSUConstantDataManager::getReportedLowLevelInternalCommunicationErrorMsg(
		int errorCode) {
	switch (errorCode) {
	case 0:
		return "No Low Level Internal Comm Error Reported";
	case 255: // -1 in 2's complement
		return "ERR_RX_0 Timed Out Waiting for Response";
	case 254: // -2 in 2's complement
		return "ERR_RX_1 Message Header Greater than 8 Bytes";
	case 253:  // -3 in 2's complement
		return "ERR_RX_2 Timed Out Waiting for Message Byte";
	case 252:  // -4
		return "ERR_RX_3 Bad CRC Check";
	case 251:
		return "ERR_RX_4 Response CID did not Match Transmitted CID";
	case 250:
		return "ERR_RX_5 Message Overrun";
	case 249:
		return "ERR_TX_0 Timed Out Transmitting Message";
	default:
		return "No Low Level Internal Comm Error Reported";
	}

}

