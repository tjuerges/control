#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#   who         when        what
#   ===         ====        ====
#   pburgos     may-25-2008 First CCL implementation for PSU
#   pburgos     may-28-2008 First Status method implementation
#   pburgos     sep-24-2008 Internal comm error mid and low implemented

"""
This module is part of the Control Command Language.
It contains complementary functionality for a PSU device.
"""
import CCL.HardwareDevice
import CCL.PSUBase
from CCL import StatusHelper

from Acspy.Common.Log import getLogger
import ACSLog
from log_audience import OPERATOR

class PSU(CCL.PSUBase.PSUBase):
    '''
    The PSU class inherits from the code generated PSUBase
    class and adds specific methods.
    '''
    def __init__(self, antennaName = None, componentName = None,
                 stickyFlag = False):
        '''
        The constructor creates a PSU object by making use of
        the PSUBase constructor. The stickyFlag can be set to
        True if the component should be instantiated in a sticky
        way.
        EXAMPLE:
        from CCL.PSU import PSU
        psu = PSU("DV01")
        '''
        '''
        Dictionary of device instances
        self._devices = {}

        # Group of antenna or component names.
        if ((isinstance(antennaName,list) == True) |
            (isinstance(componentName,list) == True)):
            #antenna names is None
            #if (isinstance(antennaName,list) == True):
            #    if (len(antennaName) != 0):
            #        for idx in range (0,len(antennaName)):
            #            self._devices["CONTROL/"+antennaName[idx]+"/PSU"]=""
            #component names
            if (isinstance(componentName,list) == True):
                if (len(componentName) != 0):
                    for idx in range (0,len(componentName)):
                        self._devices[componentName[idx]]=""
        # One component
        else:
            if not((antennaName == None) ^ (componentName == None)):
                raise NameError, "missing antennaName or componentName"
            
            #at this point antennaName=None
            #if antennaName != None:
            #    self._devices["CONTROL/"+antennaName+"/PSU"]=""

            if componentName != None:
                self._devices[componentName] = ""

        
        print "PSU constructor"
        if (antennaName==None):
                print "antennaName= None"
        else:
            print "antennaName="
            print antennaName
        if (componentName==None):
            print "componentName= None"
        else:
            print "componentName="
            print componentName
        '''
        CCL.PSUBase.PSUBase.__init__(self, antennaName, componentName, stickyFlag)
        self.logger = getLogger("-audienceLogger")

    def __del__(self):
        CCL.PSUBase.PSUBase.__del__(self)

    def getShutdownErrorMsg(self):
        '''
        Allow you to get a readable message about the shutdown reason
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GET_SHUTDOWN_ERROR_MSG()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def getAmbientTempAtShutdown(self):
        '''
        Allow you to get a readable message about the shutdown reason
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GET_AMBIENT_TEMP_AT_SHUTDOWN()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def getAmbientTemperatureOp(self):
        '''
        Allow you to get a readable message about the shutdown reason
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GET_AMBIENT_TEMPERATURE()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def getReportedLambdaError(self):
        '''
        Allow you to get a readable message about the shutdown reason
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GET_REPORTED_LAMBDA_ERROR()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    def getReportedLowInternalCommError(self):
        '''
        Allow you to get a readable message about the shutdown reason
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GET_REPORTED_LOW_INTERNAL_COMM_ERROR()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    def getReportedMidInternalCommError(self):
        '''
        Allow you to get a readable message about the shutdown reason
        '''
        result = {}
        for key, val in self._devices.iteritems():
            antName = key.rsplit("/")[1]
            result[antName] = self._devices[key].GET_REPORTED_MID_INTERNAL_COMM_ERROR()
        if len(self._devices) == 1:
            return result.values()[0]
        return result
    
    def _PSU__STATUSHelper(self,key):
        '''
        This method provides the status part that is common to all PSs.
        A list of elements is returned that should be used within higher
        level STATUS() methods. 

        '''
            # List of status elements
        elements = []

        ############################### General #####################
        elements.append( StatusHelper.Separator("General") )

        # GET_STATUS()
        try:
            status,t = self._devices[key].GET_STATUS()
            generalStatus = status[0]
            lambdaVegaStatus = status[1]
            shutdownErrorCodeStatus = status[2]
            aliveCounterStatus = status[3]
            
            if (generalStatus & 0x01 == 0x01):
                globalWarningVal = "ERR"
            else :
                globalWarningVal = "ok"
                
            if (generalStatus & 0x02 == 0x02):
                psShutdownVal = "OFF"
            else:
                psShutdownVal = "on"
                
            if (generalStatus & 0x04== 0x04):
                shutdownCmdReceivedVal = "YES"
            else:
                shutdownCmdReceivedVal = "no"
                
            if (lambdaVegaStatus & 0x01 == 0x01):
                overTempOkVal = "ok"
            else:
                overTempOkVal = "ERR"
               
            if( lambdaVegaStatus & 0x02 == 0x02):
                fanStatusOkVal = "ok"
            else:
                fanStatusOkVal = "ERR"
            
            if (lambdaVegaStatus & 0x04 == 0x04):
                acStatusOKVal = "ok"
            else:
                acStatusOKVal = "ERR"
            
            if (lambdaVegaStatus & 0x08 == 0x08):
                dcStatusOkVal = "ok"
            else:
                dcStatusOkVal = "ERR"
            
            if (lambdaVegaStatus & 0x10 == 0x10):
                currentLimitsOkVal = "ok"
            else:
                currentLimitsOkVal = "ERR"
                
            if (lambdaVegaStatus & 0x20 == 0x20):
                modulesAllOkVal = "ok"
            else:
                modulesAllOkVal = "ERR"
                
            if (lambdaVegaStatus & 0x40 == 0x40):
                voltageLimitsOkVal = "ok"
            else:
                voltageLimitsOkVal = "ERR"
                
            if (lambdaVegaStatus & 0x80 == 0x80):
                fanWarningOkVal = "ok"
            else:
                fanWarningOkVal = "ERR"
        except:
            globalWarningVal = "N/A"
            psShutdownVal = "N/A"
            shutdownCmdReceivedVal = "N/A"
            overTempOkVal = "N/A"
            fanStatusOkVal = "N/A"
            acStatusOKVal = "N/A"
            dcStatusOkVal = "N/A"
            currentLimitsOkVal ="N/A"
            modulesAllOkVal ="N/A"
            voltageLimitsOkVal ="N/A"
            fanWarningOkVal ="N/A"
            
        #  Top level Status Line
        try:
            ambientTempVal = "%2.2f" % self._devices[key].GET_AMBIENT_TEMPERATURE()[0] - 273.15
        except:
            ambientTempVal = "N/A"

        elements.append(StatusHelper.Line("Global Warning: " +globalWarningVal,
              [StatusHelper.ValueUnit(modulesAllOkVal,label="Modules All Ok"),
               StatusHelper.ValueUnit(ambientTempVal, 'C', "Amb. Temp.")]))


        lines = []

        lines.append(StatusHelper.Line("Enabled",
                     [StatusHelper.ValueUnit(psShutdownVal,
                                             label="Current State"),
                      StatusHelper.ValueUnit(shutdownCmdReceivedVal,
                                             label="Shutdown Cmd")]))
        # Shutdown Status Line
        try:
            shutdownTemp = self._devices[key].GET_AMBIENT_TEMP_AT_SHUTDOWN()
        except:
            shutdownTemp = "N/A"
        lines.append(StatusHelper.Line("Temp. Monitor",
                     [StatusHelper.ValueUnit(overTempOkVal, label="Over Temp"),
                      StatusHelper.ValueUnit(shutdownTemp,
                                             label="Shutdown Temp")]))
        lines.append( StatusHelper.Line("Limits",
              [StatusHelper.ValueUnit(currentLimitsOkVal, label = 'Current'),
               StatusHelper.ValueUnit(voltageLimitsOkVal, label = 'Voltage')]))

        
        #Supply Status 
        lines.append( StatusHelper.Line("Status",
              [StatusHelper.ValueUnit(acStatusOKVal, label = 'AC'),
               StatusHelper.ValueUnit(dcStatusOkVal, label = 'DC')]))

        # Fan Status
        lines.append( StatusHelper.Line("Fan",
              [StatusHelper.ValueUnit(fanStatusOkVal, label="Status"),
               StatusHelper.ValueUnit(fanWarningOkVal, label="Warning")]))
        

        elements.append(StatusHelper.Group("Power Supply Status", lines))


        # getShutdownErrorMsg()
        try:
           shutdownErrorMsgVal= self._devices[key].GET_SHUTDOWN_ERROR_MSG()
        except:
           shutdownErrorMsgVal = "N/A"
        elements.append( StatusHelper.Line("Shutdown Error Message",
                            [StatusHelper.ValueUnit(shutdownErrorMsgVal)]))   
                
        # getReportedLambdaError()
        try:
            reportedLambdaErrorVal = self._devices[key].GET_REPORTED_LAMBDA_ERROR()
        except:
            reportedLambdaErrorVal  = "N/A"
        elements.append( StatusHelper.Line("Reported Lambda Error ", [StatusHelper.ValueUnit(reportedLambdaErrorVal)]))

        # getReportedLowInternalCommError()
        try:
            reportedLowInternalCommErrorVal = self._devices[key].GET_REPORTED_LOW_INTERNAL_COMM_ERROR()
        except:
            reportedLowInternalCommErrorVal  = "N/A"
        elements.append( StatusHelper.Line("Reported Low Internal Comm Error     ", [StatusHelper.ValueUnit(reportedLowInternalCommErrorVal)]))
        
        # getReportedMidInternalCommError()
        try:
            reportedMidInternalCommErrorVal = self._devices[key].GET_REPORTED_MID_INTERNAL_COMM_ERROR()
        except:
            reportedMidInternalCommErrorVal  = "N/A"
        elements.append( StatusHelper.Line("Reported Mid Internal Comm Error     ", [StatusHelper.ValueUnit(reportedMidInternalCommErrorVal)]))
        
        # getAliveCounter()
        try:
            aliveCounterVal,t = self._devices[key].GET_ALIVE_COUNTER()
        except:
            aliveCounterVal = "N/A"
        elements.append( StatusHelper.Line("Alive Counter", [StatusHelper.ValueUnit(aliveCounterVal)]) )  
                                         
        return elements
        
    def _midHelper(self, name, methodList):
        '''
        helper method to create uniform Device specific outputs
        The method list should be in the following order:
        voltage
        maxVoltage
        minVoltage
        sdVoltage
        current
        maxCurrent
        sdCurrent
        '''
        lines = []
        labels = [('V', None), ('V', 'Max'), ('V', 'Min'), ('V', 'SD'),
                  ('A', None), ('A', 'Max'), ('A', 'SD')]


        # Populate the Voltage Line
        values = []
        for idx in range(4):
            try:
                prettyValue = "%2.2f" % methodList[idx]()[0]
                values.append(StatusHelper.ValueUnit( prettyValue,
                                                     labels[idx][0],
                                                     labels[idx][1]))
            except:
                values.append(StatusHelper.ValueUnit("NA",
                                                     labels[idx][0],
                                                     labels[idx][1]))

        lines.append(StatusHelper.Line("Voltage",values))

        values = []
        for idx in range(4,7):
            try:
                prettyValue = "%2.2f" % methodList[idx]()[0]
                values.append(StatusHelper.ValueUnit( prettyValue,
                                                     labels[idx][0],
                                                     labels[idx][1]))
            except:
                values.append(StatusHelper.ValueUnit("NA",
                                                     labels[idx][0],
                                                     labels[idx][1]))
            if idx == 6:
                values.append(StatusHelper.ValueUnit(''))                

        lines.append(StatusHelper.Line("Current",values))

        return StatusHelper.Group(name,lines)


    def setShutdown(self):
        for key, val in self._devices.iteritems():
                listNames = key.rsplit("/")
                antName = listNames[1]
                compName = listNames[2]
                try:
                    self._devices[key].SET_SHUTDOWN(0x1)
                    self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO,antName+"/"+compName + 
                                          ": Power Supply has been turned on",
                                          OPERATOR, antName)
                except:
                    print "Shutdown not done for component=" + compName
                    self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName +
                                      ": Command Shutdown NOT executed.",
                                      OPERATOR, antName)
    def setStartup(self):
        for key, val in self._devices.iteritems():
                listNames = key.rsplit("/")
                antName = listNames[1]
                compName = listNames[2]
                try:
                    self._devices[key].SET_SHUTDOWN(0x0)
                    self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName + 
                                          ": Power Supply has been turned on",
                                          OPERATOR, antName)
                except:
                    print "Startup not done for component " + compName
                    self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName +
                                      ": Command Startup NOT executed.",
                                      OPERATOR, antName)
            
            
    def setResetWarning(self):
        for key, val in self._devices.iteritems():
                listNames = key.rsplit("/")
                antName = listNames[1]
                compName = listNames[2]
                try:
                    self._devices[key].SET_RESET_WARNING(0x1)
                    self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName + 
                                          ": Power Supply has been turned on",
                                          OPERATOR, antName)
                except:
                    print "Startup not done for component " + compName
                    self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName +
                                      ": Command Reset  NOT executed.",
                                      OPERATOR, antenna=antName)

    def setResetLatches(self):
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
            try:
                self._devices[key].SET_RESET_LATCHES(0x1)
                self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName + 
                                      ": Power Supply has been turned on",
                                      OPERATOR, antName)
            except:
                print "Startup not done for component " + compName
                self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName +
                                  ": Command Reset  NOT executed.",
                                  OPERATOR, antenna=antName)
                    
    def setResetMaxMin(self):
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]
            try:
                self._devices[key].SET_RESET_MAX_MIN(0x1)
                self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName + 
                                      ": Power Supply has been turned on",
                                      OPERATOR, antName)
            except:
                print "Startup not done for component " + compName
                self.logger.logNotSoTypeSafe( ACSLog.ACS_LOG_INFO, antName+"/"+compName +
                                  ": Command Reset  NOT executed.",
                                  OPERATOR, antenna=antName)
        
    
    

