/** @(#) $Id$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 *
 * $Id$
 * 
 * 
 *	who			when			what
 *------------------------------------------------------------
 *	pburgos		2008/06/15	PSUAlarm.cpp created. Uses State Pattern
 *------------------------------------------------------------
 */
#include <PSUAlarm.h>

Alarm::Alarm()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    alarmSource_m =
        ACSAlarmSystemInterfaceFactory::createSource("ALARM_SYSTEM_SOURCES");
}

Alarm::~Alarm(){
	ACS_TRACE("Alarm::~Alarm()");
}
void Alarm::setCurrentState(AlarmState* _alarmState){
	ACS_TRACE("Alarm::setCurrentState()");
	alarmState_m=_alarmState;
}

void Alarm::on(){
	ACS_TRACE("Alarm::on()");
	alarmState_m->on(this);
}
void Alarm::off(){
	ACS_TRACE("Alarm::off()");
	alarmState_m->off(this);
}
string Alarm::getErrorMessage(){
	ACS_TRACE("Alarm::getErrorMessage()");
	return errorMessage_m;
}

int Alarm::getAlarmCode(){
	ACS_TRACE("Alarm::getAlarmCode()");
	return alarmCode_m;
	
}



BooleanAlarm::BooleanAlarm(int _alarmCode,
			string _family, 
			string _member,
			string _errorMessage, 
			Command* _command,
			bool _normalValue): Alarm(){
	ACS_TRACE("BooleanAlarm::BooleanAlarm()");
	alarmCode_m=_alarmCode;
	family_m=_family;
	member_m=_member;
	errorMessage_m=_errorMessage;
	command_m=_command;
	normalValue_m=_normalValue;
	alarmState_m= new OffState(); //Yes, all alarms are created in OFF state
	
}
BooleanAlarm::~BooleanAlarm(){
	ACS_TRACE("BooleanAlarm::~BooleanAlarm()");
}
void BooleanAlarm::evaluateAlarmState(){
	ACS_TRACE("BooleanAlarm::evaluateAlarmState()");
	command_m->execute();
	if( (((BooleanCommand*)command_m)->getCurrentValue())!= normalValue_m ) {
		on();
	}else {
		off();
	}
	
}
FloatAlarm::FloatAlarm(int _alarmCode,
						string _family, 
						string _member,
						string _errorMessage,
						Command* _command,
						float _minRange,
						float _maxRange):Alarm(){
	ACS_TRACE("FloatAlarm::FloatAlarm()");
	alarmCode_m=_alarmCode;
	family_m=_family;
	member_m=_member;
	errorMessage_m=_errorMessage;
	command_m=_command;
	minRange_m=_minRange;
	maxRange_m=_maxRange;
	alarmState_m= new OffState(); //Yes, all alarms are created in OFF state
	
}
FloatAlarm::~FloatAlarm(){
	ACS_TRACE("FloatAlarm::~FloatAlarm()");
}
void FloatAlarm::evaluateAlarmState(){
	ACS_TRACE("FloatAlarm::evaluateAlarmState()");
	command_m->execute();
	if(   ( (((FloatCommand*)command_m)->getCurrentValue()) > maxRange_m )
			||
			( (((FloatCommand*)command_m)->getCurrentValue()) < minRange_m ) ) {
		on();
	}else {
		off();
	}
}
AlarmHandler::AlarmHandler(){
	ACS_TRACE("AlarmHandler::AlarmHandler()");
	//here the alarm list must be initialized
	//alarmList_m= new std::list<Alarm>;
	
}
AlarmHandler::~AlarmHandler(){
	ACS_TRACE("AlarmHandler::~AlarmHandler()");
	//TODO get rid of all objects Alarm created on the heap before call next delete function
	//We need to iterate through the list and delete every alarm before destroy AlarmHandler reference
	//delete(alarmList_m);
	list<Alarm*>::iterator i;
		for (i = alarmList_m.begin(); i != alarmList_m.end(); ++i)
		 {
		      (*i)->~Alarm();
		 }

}

void AlarmHandler::addAlarmToList(Alarm* _alarm){
	ACS_TRACE("AlarmHandler::addAlarmToList()");
	alarmList_m.push_back(_alarm);
	
}
void AlarmHandler::removeAlarmFromList(Alarm* _alarm){
	ACS_TRACE("AlarmHandler::removeAlarmFromList()");
}
void AlarmHandler::evaluateAlarmStateList(){
	ACS_TRACE("AlarmHandler::evaluateAlarmStateList()");
	list<Alarm*>::iterator i;
	for (i = alarmList_m.begin(); i != alarmList_m.end(); ++i)
	 {
	      (*i)->evaluateAlarmState();
	 }
}
void AlarmHandler::clearAllAlarmFlags(){
	ACS_TRACE("AlarmHandler::clearAllAlarmFlags()");
	list<Alarm*>::iterator i;
	for (i = alarmList_m.begin(); i != alarmList_m.end(); ++i)
	 {
	      (*i)->off();
	 }

}

Alarm* AlarmHandler::getAlarm(int _alarmCode){
	ACS_TRACE("AlarmHandler::getAlarm()");
	//TODO implement a real method
	return alarmList_m.front();
}

//AlarmState::AlarmState(){}
void Alarm::sendAlarm(bool _isActive){
	ACS_TRACE("PSUImpl::sendAlarm");
	string family = family_m; // FF
	string member = member_m;//getComponent()->getName(); // FM, the name of the component
	int alarmCode= alarmCode_m;
	auto_ptr<acsalarm::FaultState> faultState =
			ACSAlarmSystemInterfaceFactory::createFaultState(family, member,
					alarmCode);
	faultState->setUserTimestamp(auto_ptr<acsalarm::Timestamp>(new acsalarm::Timestamp()));
	if (_isActive) {
		faultState->setDescriptor(faultState::ACTIVE_STRING);
	} else {
		faultState->setDescriptor(faultState::TERMINATE_STRING);
	}

	// Send the fault. We must use the "ALARM_SYSTEM_SOURCES" name.
	//ACSAlarmSystemInterfaceFactory::createSource("ALARM_SYSTEM_SOURCES")->push(*faultState);
	alarmSource_m->push(*faultState);
}
OffState::OffState(){}
OnState::OnState(){}
void OffState::on (Alarm* _alarm){
	ACS_TRACE("OffState::on");
	_alarm->sendAlarm(true);
	_alarm->setCurrentState(new OnState);
	delete this;
	
	
}
void OnState::off (Alarm* _alarm){
	ACS_TRACE("OnState::off");
	_alarm->sendAlarm(false);
	_alarm->setCurrentState(new OffState());
	delete this;	
}


