/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.StatusIndicator;

import java.util.logging.Logger;

/**
 * <code>MetaframeWarningWidget</code> displays a single metaframe warning. The warning should be
 * on if metaframe C != specifiedDelay+3 or metaframes D, C, or B are not = to each other.
 * 
 * @author David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 *
 * TODO: Factor out common behavior with other widgets to a superclass.
 * TODO: Once the specified delay is fixed to give a single int, update to use it.
 */
public class MetaframeWarningWidget extends MonitorPointWidget {

/**Construct a new MetaframeWarningWidget.
 * 
 * @param logger
 * @param monitorPointModelD
 * @param monitorPointModelC
 * @param monitorPointModelB
 * @param monitorPointModelMsb
 * @param monitorPointModelMid
 * @param monitorPointModelLsb
 */
    public MetaframeWarningWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModelD,
            MonitorPointPresentationModel<Integer> monitorPointModelC,
            MonitorPointPresentationModel<Integer> monitorPointModelB,
            MonitorPointPresentationModel<Integer> specifiedDelayMP){
        
        super(logger, true);

        this.falseStatus = Status.FAILURE;
        this.falseToolTipText = "Error: Metaframe not known";
        this.trueStatus = Status.GOOD;
        this.trueToolTipText = "All metaframes OK";

        delayD = addMonitorPointPM(monitorPointModelD, Integer.valueOf(0));
        delayC = addMonitorPointPM(monitorPointModelC, Integer.valueOf(0));
        delayB = addMonitorPointPM(monitorPointModelB, Integer.valueOf(0));
        specifiedDelay = addMonitorPointPM(specifiedDelayMP, Integer.valueOf(0));

        buildWidget();
    }

    /**
     * buildWidget is a private method which is used to construct the entire widget
     * to be displayed on a panel
     */
    private void buildWidget() {
        statusIndicator = new StatusIndicator(falseStatus, falseToolTipText);
        add(statusIndicator);
        addPopupMenuToComponent(statusIndicator);
    }

    @Override
    protected void updateAttention(Boolean value) {
        // TODO: remove
    }

    @Override
    protected void updateValue(IMonitorPoint aMonitorPoint) {
        int specDelay=0;
        specDelay+=(Integer)getCachedValue(specifiedDelay);
        int delayDVal=(Integer)getCachedValue(delayD);
        int delayCVal=(Integer)getCachedValue(delayC);
        int delayBVal=(Integer)getCachedValue(delayB);
        
        if (delayCVal != specDelay+3)
            statusIndicator.setStatus(falseStatus, "Error: Delay C does not equal Specified Delay+3");
        else if (delayCVal != delayDVal)
            statusIndicator.setStatus(falseStatus, "Error: Metaframe Delay D does not equal Delay C");
        else if (delayCVal != delayBVal)
            statusIndicator.setStatus(falseStatus, "Error: Metaframe Delay B does not equal Delay C");
        else
            statusIndicator.setStatus(trueStatus, trueToolTipText);
    }

    private IMonitorPoint delayB;
    private IMonitorPoint delayC;
    private IMonitorPoint delayD;
    private StatusIndicator statusIndicator;
    private Status falseStatus;
    private String falseToolTipText;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private IMonitorPoint specifiedDelay;
    private Status trueStatus;
    private String trueToolTipText;  
}

//
// O_o

