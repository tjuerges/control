/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdControlPoints;
import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.DRX.widgets.XbarControlWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel displays the DFR Xbar stats, and the Set Xbar control.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version 
 * @since 7.0.0
 */

public class DebugXbarPanel extends DevicePanel {

    public DebugXbarPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        xbarControl = new XbarControl(logger, aDevicePM);
        xbarStatus = new XbarStatus(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(xbarStatus, c);
        c.gridy++;
        add(xbarControl, c);
        setVisible(true);
    }

    /*
     * Just display the DFR xbar status
     * TODO: Add XBAR display once COMP-3671 allows for it.
     */
    private class XbarStatus extends DevicePanel {

        public XbarStatus(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
        
        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("D"), c);
            c.gridx++;
            add(new JLabel("C"), c);
            c.gridx++;
            add(new JLabel("B"), c);
            c.gridy++;
            c.gridx -=2;
            addLeftLabel("Swap 32 Channels In Time: ", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_D_SWAP_32_IN_TIME),
                    Status.GOOD, "Ch D is set to normal mode",
                    Status.ATYPICAL, "Ch D is swapping 32 channels in time!"), c);
            c.gridy++;
            addLeftLabel("Swap 16 Channels In Time:", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_D_SWAP_16_IN_TIME),
                    Status.GOOD, "Ch D is set to normal mode",
                    Status.ATYPICAL, "Ch D is swapping 16 channels in time!"), c);
            c.gridy--;
            c.gridx++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_C_SWAP_32_IN_TIME),
                    Status.GOOD, "Ch C is set to normal mode",
                    Status.ATYPICAL, "Ch C is swapping 32 channels in time!"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_C_SWAP_16_IN_TIME),
                    Status.GOOD, "Ch C is set to normal mode",
                    Status.ATYPICAL, "Ch C is swapping 16 channels in time!"), c);
            c.gridy--;
            c.gridx++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_B_SWAP_32_IN_TIME),
                    Status.GOOD, "Ch B is set to normal mode",
                    Status.ATYPICAL, "Ch B is swapping 32 channels in time!"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_B_SWAP_16_IN_TIME),
                    Status.GOOD, "Ch B is set to normal mode",
                    Status.ATYPICAL, "Ch B is swapping 16 channels in time!"), c);
            c.gridx-=2;
            c.gridy++;
            String[] xBarCodes = {"Normal Mode", "BB0 duplicated on both BBs", "BB1 duplicated on both BBs",
                         "Swap Basebands!"};
            c.anchor=GridBagConstraints.EAST;
            addLeftLabel("Ch D:  ", c);
            c.gridwidth=3;
            add (new CodeToTextMonitorPointWidget(logger,
                               devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_D_BASEBANDS),
                               xBarCodes), c);
            c.gridwidth=1;
            c.gridy++;
            addLeftLabel("Ch C:  ", c);
            c.gridwidth=3;
            add (new CodeToTextMonitorPointWidget(logger,
                               devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_C_BASEBANDS),
                               xBarCodes), c);
            c.gridy++;
            c.gridwidth=1;
            addLeftLabel("Ch B:  ", c);
            c.gridwidth=3;
            add (new CodeToTextMonitorPointWidget(logger,
                               devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_XBAR_B_BASEBANDS),
                               xBarCodes), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * Just display the DFR xbar control
     */
    private class XbarControl extends DevicePanel {

        public XbarControl(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 0;
            add(new XbarControlWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DFR_XBAR_D),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_XBAR_C),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_XBAR_B),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_XBAR_ALL)), c);
        }
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    private XbarControl xbarControl;
    private XbarStatus xbarStatus;
}

//
// O_o

