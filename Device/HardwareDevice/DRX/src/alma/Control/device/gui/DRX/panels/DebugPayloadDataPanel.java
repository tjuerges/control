/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.DRX.IcdControlPoints;
import alma.Control.device.gui.DRX.widgets.util.DrxDataGrabber;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.DtsDataCaptureWidget;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.util.DtsData.IDataGrabber;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel displays the payload data information, the DFR Control Registers, and some FIFO info
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugPayloadDataPanel extends DevicePanel {

    public DebugPayloadDataPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        controlRegisters = new ControlRegisters(logger, aDevicePM);
        fifoPane = new FifoPane(logger, aDevicePM);
        payloadGraphPane = new PayloadGraphPane(logger, aDevicePM);
        polarityPane = new PolarityPane(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth=2;
        add(controlRegisters, c);
      c.gridx=0;
      c.gridy=1;
      c.gridwidth=1;
        IDataGrabber dataGrabber =  new DrxDataGrabber(logger, devicePM,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PAYLOAD_HI_D),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PAYLOAD_LO_D),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PAYLOAD_HI_C),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PAYLOAD_LO_C),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PAYLOAD_HI_B),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_PAYLOAD_LO_B),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_D),
                devicePM.getControlPointPM(IcdControlPoints.DFR_CAPTURE_PAYLOAD));
        add(new DtsDataCaptureWidget(logger, devicePM, dataGrabber), c);
        

//        add(polarityPane, c);
//
//        c.gridx = 2;
//        c.gridy = 0;
//        c.gridwidth=1;
//        add(fifoPane, c);
//
//        c.gridx = 1;
//        c.gridy = 1;
//        c.gridwidth=2;
//        add(payloadGraphPane, c);
        
        setVisible(true);
    }

    /*
     * This panel displays the info from the DFR Control Register.
     * Don't confuse it with the DFR Status register, which is on the DeformatterStatusPanel
     */
    private class ControlRegisters extends DevicePanel {

        public ControlRegisters(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
          setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("DFR Control Registers"), BorderFactory.createEmptyBorder(1, 1, 1,
                    1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            c.gridx = 1;
            c.gridy = 0;
            add (new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("Sync Word Found", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_D_SYNC),
                    Status.FAILURE, "Channel D Sync Word Error",
                    Status.GOOD, "Channel D Sync Word OK"), c);
            c.gridy++;
            addLeftLabel("Test FIFO Empty", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_D_FIFOEMPTY),
                     Status.ON, "Channel D Test FIFO is not empty",
                     Status.OFF, "Channel D Test FIFO is Empty"), c);
            c.gridy++;
            addLeftLabel("Test FIFO Full", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_D_FIFOFULL),
                    Status.OFF, "Channel D Test FIFO is not full",
                    Status.ON, "Channel D Test FIFO is Full"), c);
            c.gridx = 2;
            c.gridy = 0;
            add (new JLabel("C"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_C_SYNC),
                    Status.FAILURE, "Channel C Sync Word Error",
                    Status.GOOD, "Channel C Sync Word OK"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_C_FIFOEMPTY),
                    Status.ON, "Channel C Test FIFO is not empty",
                    Status.OFF,"Channel C Test FIFO is Empty"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_C_FIFOFULL),
                    Status.OFF, "Channel C Test FIFO is not full",
                    Status.ON, "Channel C Test FIFO is Full"), c);
            c.gridx = 3;
            c.gridy = 0;
            add (new JLabel("B"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_B_SYNC),
                    Status.FAILURE, "Channel B Sync Word Error",
                    Status.GOOD, "Channel B Sync Word OK"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_B_FIFOEMPTY),
                    Status.ON, "Channel B Test FIFO is not empty",
                    Status.OFF, "Channel B Test FIFO is Empty"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_CONTROL_REG_B_FIFOFULL),
                    Status.OFF, "Channel B Test FIFO is not full",
                    Status.ON, "Channel B Test FIFO is Full"), c);
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This panel displays the raw payload data for each polarity, and provides the controls to capture it.
     * TODO: Add M&C widgets
     */
    private class PolarityPane extends DevicePanel {

        public PolarityPane(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }


        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first Widget
            addLeftLabel("Raw Payload Data", c);
            //TODO: Add Widget here

            //Move to the next row and add another Widget
            c.gridy++;
            addLeftLabel("Widgets go here", c);
            //TODO: Add Widget here
        }


        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This pane displays the FIFO empty and full status, along with sync word found errors.
     */
    private class FifoPane extends DevicePanel {

        public FifoPane(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first Widget
            addLeftLabel("Fifo Control", c);
            //TODO: Add Widget here

            //Move to the next row and add another Widget
            c.gridy++;
            addLeftLabel("Widgets go here", c);
            //TODO: Add Widget here
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }

    /*
     * This would graph the payload data, but we don't yet support that.
     * TODO: Add M&C widgets
     */
    private class PayloadGraphPane extends DevicePanel {

        public PayloadGraphPane(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first Widget
            addLeftLabel("The Payload Data Graph", c);
            //TODO: Add Widget here

            //Move to the next row and add another Widget
            c.gridy++;
            addLeftLabel("Widget goes here", c);
            //TODO: Add Widget here
        }

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
    }
    
    private ControlRegisters controlRegisters;
    private FifoPane fifoPane;
    private PayloadGraphPane payloadGraphPane;
    private PolarityPane polarityPane;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

}

//
// O_o

