/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.StatusColor;
import alma.Control.device.gui.common.widgets.StatusIndicator;
import alma.Control.device.gui.common.widgets.Status;

import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since ALMA 7.0.0
 * 
 *        DfrFrameAlignerMonitorPointWidget offers a means of displaying the
 *        proper text message for an error code. For example, if a monitor point
 *        reads 0x2 the widget could display "Form error."
 */
public class DfrFrameAlignerMonitorPointWidget extends MonitorPointWidget {
    
    public DfrFrameAlignerMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel) {
        super(logger, true);
        monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));
        buildWidget();
    }
    
    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText(defaultText);
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setVisible(true);
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
        statusIndicator = new StatusIndicator(Status.ATYPICAL,
        "Switch override is ON");
        add(statusIndicator);
        addPopupMenuToComponent(statusIndicator);
    }
    
    @Override
    protected void updateAttention(Boolean q) {
    }
    
    @Override
    // TODO: Check if there is a better way to look up values. Maybe a hash?
    protected void updateValue(IMonitorPoint source) {
        int monitorValue = (Integer) getCachedValue(monitorPoint);
        int addressOfText = -1;
        int mask = 0x70;
        try {
            int count = 0;
            while ((monitorValue & mask) != rngCodes[count])
                count++;
            addressOfText = count;
            try {
                widgetLabel.setText(rngDataLabels[addressOfText]);
            } catch (ArrayIndexOutOfBoundsException eeek) {
                addressOfText = -1;
            } // Go on to next check.
        } catch (ArrayIndexOutOfBoundsException Eeeek) {
            // Leave value at -1, setting the value to default.
        }
        if (addressOfText == -1) {
            mask = 0x0F;
            try {
                int count = 0;
                while ((monitorValue & mask) != testDataCodes[count])
                    count++;
                addressOfText = count;
                try {
                    widgetLabel.setText(testDataLabels[addressOfText]);}
                catch (ArrayIndexOutOfBoundsException eeek) {
                    widgetLabel.setText(defaultText);}
            }
            catch (ArrayIndexOutOfBoundsException Eeeek) {
                widgetLabel.setText(defaultText);}
        }

        if ((monitorValue & 0x80) == 0x80) {
            statusIndicator.setStatus(Status.ATYPICAL, "Switch override is ON");
        } else {
            statusIndicator.setStatus(Status.GOOD, "Switch override is off");
        }
    }
    
    private String defaultText = "Normal Mode";
    private IMonitorPoint monitorPoint;
    private int[] rngCodes = { 0x10, 0x20, 0x30 };
    private String[] rngDataLabels = { "Random numbers", "Counting data", "Random with re-seed every TE" };
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private StatusIndicator statusIndicator;
    private int[] testDataCodes = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0xA, 0xB, 0xC, 0xD, 0xE };
    private String[] testDataLabels = { "Normal Mode", "All 0x3C", "All 0xC3",
            "All 0x3", "All 0xC", "All 0x5", "0xAAAA5555...", "Normal Mode",
            "All 0x0", "All 0x1", "All 0xA", "0x5555AAAA...",
            "Cntr & cntr & cntr...", "FEDCBA9801234567BA987654456789AB",
            "0123456789ABCDEFFEDCBA9876543210" };
    private JLabel widgetLabel;
}

//
// O_o
