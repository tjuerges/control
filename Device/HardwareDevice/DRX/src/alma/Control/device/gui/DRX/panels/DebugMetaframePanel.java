/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdControlPoints;
import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.DRX.widgets.MetaframeReadWriteWidget;
import alma.Control.device.gui.DRX.widgets.SpecifiedMetaframeMonitorPointWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel displays the DRX Debug Metaframe data and EEPROM read/write controls.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugMetaframePanel extends DevicePanel {

    public DebugMetaframePanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildPanel();
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
//        c.anchor = GridBagConstraints.CENTER;
//        c.fill = GridBagConstraints.BOTH;
//        c.weighty=100;
//        c.weightx=100;
        c.gridx = 1;
        c.gridy = 0;

        addLeftLabel("Delay D", c);
        add( new IntegerMonitorPointWidget(logger, devicePM
                 .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_D)), c);
        c.gridy++;
        addLeftLabel("Delay C", c);
        add( new IntegerMonitorPointWidget(logger, devicePM
                 .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_C)), c);
        c.gridy++;
        addLeftLabel("Delay B", c);
        add( new IntegerMonitorPointWidget(logger, devicePM
                 .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_B)), c);
        c.gridy++;
        addLeftLabel("Raw Delay", c);
        add( new IntegerMonitorPointWidget(logger, devicePM
                 .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_RAW)), c);
        c.gridy++;
        addLeftLabel("Specified Delay:", c);
        add(new IntegerMonitorPointWidget(logger,
                 devicePM.getMonitorPointPM(IcdMonitorPoints.SPECIFIED_METAFRAME_STORED_DELAY)), c);
        c.gridx++;
        c.gridy=0;
        c.gridheight=5;
        add(new MetaframeReadWriteWidget(logger, 
                 devicePM.getControlPointPM(IcdControlPoints.DFR_EEPROM_FETCH),
                 devicePM.getControlPointPM(IcdControlPoints.DFR_EEPROM_PROG),
                 devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_EEPROM_DATA)), c);
        setVisible(true);
    }
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o

