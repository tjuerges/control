/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdControlPoints;
import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.TextToCodeControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This panel displays the DRX Debug Frame Aligner sub-panel
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version
 * @since 5.0.3
 */

public class DebugAlignmentPanel extends DevicePanel {

    public DebugAlignmentPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        fifoPointer = new FifoPointer(logger, aDevicePM);
        frameAligner = new FrameAligner(logger, aDevicePM);
        frameControl = new FrameControl(logger, aDevicePM);
        syncResetPanel = new SyncResetPanel(logger, aDevicePM);
        syncStatusPanel = new SyncStatusPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        // setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
        // BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 100;
        c.weightx = 100;
        c.gridx = 0;
        c.gridy = 0;

        add(frameAligner, c);
        c.gridy++;
        add(syncStatusPanel, c);
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth=2;
        add(frameControl, c);
        c.gridwidth=1;
        c.gridy++;  
        add(syncResetPanel, c);
        c.gridx++;
        add(fifoPointer, c);

        setVisible(true);
    }

    /*
     * This panel displays the Frame Aligner status and controls
     */
    private class FrameAligner extends DevicePanel {

        public FrameAligner(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Frame Aligner"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("Bit Aligner Manual Enable", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_D_BITALIGN),
                    Status.GOOD, "Channel D Bit Aligner is on Automatic",
                    Status.ATYPICAL, "Channel D Bit Aligner is on Manual"), c);
            c.gridy++;
            addLeftLabel("This Channel in Sync", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_D_THISINSYNC),
                    Status.FAILURE, "Ch D is out of Sync with Ch C",
                    Status.GOOD, "Ch D is in Sync with Ch C"), c);
            c.gridy++;
            addLeftLabel("Channel C in Sync", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_D_CINSYNC),
                    Status.FAILURE, "Error: Channel C can not sync with the input data",
                    Status.GOOD, "Channel C is in sync with the input data"), c);
            c.gridy++;
            addLeftLabel("Frame Aligned", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_D_FRAMEALIGN),
                    Status.FAILURE, "Channel D Frame out of Alignment",
                    Status.GOOD, "Channel D Frame Aligned"), c);
            c.gridy++;
            addLeftLabel("C Count less than this", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_D_LESS),
                    Status.GOOD, "The Ch C frame count is not less than Ch D frame count",
                    Status.FAILURE, "The Ch C fame count is less than the Ch D frame count"), c);
            c.gridy++;
            addLeftLabel("C Count greater than this", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_D_GREATER),
                    Status.GOOD, "The Ch C frame count is not greater than the Ch D frame count",
                    Status.FAILURE, "The Ch C frame count is greater than the Ch D frame count"), c);
            c.gridx = 2;
            c.gridy = 0;
            add(new JLabel("B"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_B_BITALIGN),
                    Status.GOOD,"Channel B Bit Aligner is on Automatic",
                    Status.ATYPICAL, "Channel B Bit Aligner is on Manual"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_B_THISINSYNC),
                    Status.FAILURE, "Ch B is out of Sync with Ch C",
                    Status.GOOD, "Ch B is in Sync with Ch C"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_B_CINSYNC),
                    Status.FAILURE, "Error: Channel C can not sync with the input data",
                    Status.GOOD, "Channel C is in sync with the input data"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_B_FRAMEALIGN),
                    Status.FAILURE, "Channel B Frame out of Alignment",
                    Status.GOOD, "Channel B Frame Aligned"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_B_LESS),
                    Status.GOOD, "The Ch C frame count is not less than Ch B frame count",
                    Status.FAILURE, "The Ch C fame count is less than the Ch B frame count"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_FRAME_ALIGNER_B_GREATER),
                    Status.GOOD, "The Ch C frame count is not greater than the Ch B frame count",
                    Status.FAILURE, "The Ch C frame count is greater than the Ch B frame count"), c);
        }

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
    }

    /*
     * Frame control sub-panel. This panel displays the frame aligner controls!
     */
    private class FrameControl extends DevicePanel {

        public FrameControl(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Frame Control"),
                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 0;
            c.gridy = 0;

            String[] s = {"Disable External Control", "External Force Retard", "External Force Advance"};
            int[] i = {0, 3, 5};
            add(new TextToCodeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DFR_FRAME_ALIGNER_D),
                    "Set D", s, i), c);
            c.gridy++;
            add(new TextToCodeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DFR_FRAME_ALIGNER_B),
                    "Set B", s, i), c);
            c.gridy++;
            add(new TextToCodeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DFR_FRAME_ALIGNER_ALL),
                    "Set D & B", s, i), c);
        }

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
    }

    
    /*
     * This panel provides the control to initialize one or all of the FIFO read pointers.
     */
    private class FifoPointer extends DevicePanel {

        public FifoPointer(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("Fifo Pointer Initialization"), BorderFactory.createEmptyBorder(1,
                    1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 0;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_D), "Reset D", true), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_C), "Reset C", true), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_B), "Reset B", true), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_CONTROL_REGISTER_ALL), "Reset All", true), c);
        }

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
    }

 
    /*
     * This panel provides the reset buttons for the sync status.
     */
    private class SyncResetPanel extends DevicePanel {

        public SyncResetPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Sync Resets"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));

            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_D), "Reset D"), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_C), "Reset C"), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_B), "Reset B"), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.DFR_RESET_SYNC_ALL), "Reset All"), c);
        }

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
    }
    
    private FifoPointer fifoPointer;
    private FrameAligner frameAligner;
    private FrameControl frameControl;
    private SyncResetPanel syncResetPanel;
    private SyncStatusPanel syncStatusPanel;

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
}

//
// O_o

