/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdControlPoints;
import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.DRX.widgets.DfrFrameAlignerMonitorPointWidget;
import alma.Control.device.gui.DRX.widgets.TestDataControlWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.Status;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanToggleWidget;
import alma.Control.device.gui.common.widgets.EightDipSwitchesMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel contains the Test Data related M&C points, and is intended to be displayed within the debug
 * tab.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugTestDataPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public DebugTestDataPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        scrambleCodes = new ScrambleCodes(logger, aDevicePM);
        switchSettings = new SwitchSettings(logger, aDevicePM);
        testData = new TestData(logger, aDevicePM);
        voltageStatusPanel = new VoltageStatusPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        // setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
        // BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 100;
        c.weightx = 100;
        c.gridx = 0;
        c.gridy = 0;

        add(testData, c);
        c.gridy++;
        add(scrambleCodes, c);
        c.gridx = 1;
        c.gridy = 0;
        add(switchSettings, c);
        c.gridy++;
        add(voltageStatusPanel, c);

        setVisible(true);
    }

    /*
     * This panel displays the Test Data M&C points These include the PC Board switch override, the current
     * status, and the Test Data set control TODO: Add M&C widgets
     */
    private class TestData extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public TestData(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Test Data Status"),
                      BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;

            //Set up label array for test data widget.
 
            addLeftLabel("Test Data D: ", c);
            add(new DfrFrameAlignerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_TEST_DATA_D)), c);
            c.gridy++;
           
            addLeftLabel("Test Data C: ", c);
            add(new DfrFrameAlignerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_TEST_DATA_C)), c);
            c.gridy++;
            addLeftLabel("Test Data B: ", c);
            add(new DfrFrameAlignerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_TEST_DATA_B)), c);
            c.gridy++;
            c.gridx--;
            c.gridwidth=2;
            add(new TestDataControlWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DFR_TEST_DATA_D),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_TEST_DATA_C),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_TEST_DATA_B),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_TEST_DATA_ALL)), c);
        }
    }

    /*
     * This panel contains the scramble code controls. It displays the current scramble code status, and has
     * the controls to set the scramble code.
     */
    private class ScrambleCodes extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public ScrambleCodes(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Scramble Codes"),
            BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            // The first Widget
            addLeftLabel("Ch D: ", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SCRAMBLE_CODE_OFF_D),
                    Status.GOOD, "Ch D scramble mode on",
                    Status.FAILURE, "Ch D scramble mode off"),c);
            c.gridx++;
            add(new BooleanToggleWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SCRAMBLE_CODE_OFF_D),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_SCRAMBLE_MODE_D),
                    "Turn Off", "Turn On"), c);
            c.gridx--;
            c.gridy++;
            addLeftLabel("Ch C: ", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SCRAMBLE_CODE_OFF_C),
                    Status.GOOD, "Ch D scramble mode on",
                    Status.FAILURE, "Ch D scramble mode off"),c);
            c.gridx++;
            add(new BooleanToggleWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SCRAMBLE_CODE_OFF_C),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_SCRAMBLE_MODE_C),
                    "Turn Off", "Turn On"), c);
            c.gridx--;
            c.gridy++;
            addLeftLabel("Ch B: ", c);
            add(new BooleanMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SCRAMBLE_CODE_OFF_B),
                    Status.GOOD, "Ch D scramble mode on",
                    Status.FAILURE, "Ch D scramble mode off"),c);
            c.gridx++;
            add(new BooleanToggleWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_SCRAMBLE_CODE_OFF_B),
                    devicePM.getControlPointPM(IcdControlPoints.DFR_SCRAMBLE_MODE_B),
                    "Turn Off", "Turn On"), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DFR_SCRAMBLE_MODE_ALL),
                    "Turn all off", true), c);
            c.gridx=0;
            c.gridwidth=2;
            add(new BooleanControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.DFR_SCRAMBLE_MODE_ALL),
                    "Turn all on", false), c);
        }
    }

    /*
     * This panel displays the settings of the on-board switches for each FPGA. TODO: Add M&C widgets
     */
    private class SwitchSettings extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public SwitchSettings(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("FPGA Dip Switch Settings"),
                    BorderFactory.createEmptyBorder(1, 1,1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // TODO: Make sure that the LSB(0) and MSB(7) labels are correct
            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("Channel D:"), c);
            c.gridy++;
            addLeftLabel("(7)", c);
            addRightLabel("(0)", c);
            add(new EightDipSwitchesMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_SWITCH_D),
                    Status.ON, "FPGA Channel D dip switches",
                    Status.OFF, "FPGA Channel D dips switches"), c);
            c.gridy++;
            add(new JLabel("Channel C:"), c);
            c.gridy++;
            addLeftLabel("(7)", c);
            addRightLabel("(0)", c);
            add(new EightDipSwitchesMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_SWITCH_C),
                    Status.ON, "FPGA Channel C dip switches",
                    Status.OFF, "FPGA Channel C dips switches"), c);
            c.gridy++;
            add(new JLabel("Channel B:"), c);
            c.gridy++;
            addLeftLabel("(7)", c);
            addRightLabel("(0)", c);
            add(new EightDipSwitchesMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_SWITCH_B),
                    Status.ON, "FPGA Channel B dip switches",
                    Status.OFF, "FPGA Channel B dips switches"), c);
        }
    }

    /*
     * The DFR Voltage pane. Displays the 1.8 and 1.5 volt readouts for the FPGAs We put it here because
     * there was no clear spot to put it and there was extra space here. It can be moved elsewhere if
     * needed.
     */
    private class VoltageStatusPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public VoltageStatusPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("DFR Voltage Status"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("1.8", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_VOLTAGE_STATUS_B_1P8VD),
                    Status.FAILURE, "Channel D 1.8 volts out of range",
                    Status.GOOD, "Channel D 1.8 Volts OK"), c);
            c.gridy++;
            addLeftLabel("1.5", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_VOLTAGE_STATUS_B_1P5VD),
                    Status.FAILURE, "Channel D 1.5 volts out of range",
                    Status.GOOD, "Channel D 1.5 Volts OK"), c);
            c.gridx = 2;
            c.gridy = 0;
            add(new JLabel("C"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_VOLTAGE_STATUS_B_1P8VC),
                    Status.FAILURE, "Channel C 1.8 volts out of range",
                    Status.GOOD, "Channel C 1.8 Volts OK"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_VOLTAGE_STATUS_B_1P5VC),
                    Status.FAILURE, "Channel C 1.5 volts out of range",
                    Status.GOOD, "Channel C 1.5 Volts OK"), c);
            c.gridx = 3;
            c.gridy = 0;
            add(new JLabel("D"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_VOLTAGE_STATUS_B_1P8VB),
                    Status.FAILURE, "Channel B 1.8 volts out of range",
                    Status.GOOD, "Channel B 1.8 Volts OK"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_VOLTAGE_STATUS_B_1P5VB),
                    Status.FAILURE, "Channel B 1.5 volts out of range",
                    Status.GOOD, "Channel B 1.5 Volts OK"), c);
        }
    }
    
    private ScrambleCodes scrambleCodes;
    private SwitchSettings switchSettings;
    private TestData testData;
    private VoltageStatusPanel voltageStatusPanel;
}

//
// O_o

