/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IdlMonitorPoints;
import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.
 * Summary data is always visible on the GUI. Detail data is organized to keep
 * related data together and separate unrelated data.
 * 
 * This class contains the Detail data. It also provides a Show/Hide button for
 * the data.
 * 
 * Tabed panels are used to group related data together and separate unrelated
 * data. This panel contains a JTabbedPane and adds several panels with DRX details to it.
 * 
 * This panel also contains a function which sets up the display units, customizeMonitorPoints(). This
 * function sets customizes the display units. For example, temperature monitor points are told to display
 * in Celsius instead of Kelvin. 
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DetailsPanel extends DevicePanel {

    public DetailsPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        customizeMonitorPoints();
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        debugPanel = new DebugPanel(logger, aDevicePM);
        deformatterPanel = new DeformatterStatusPanel(logger, aDevicePM);
        deviceInfoPanel = new DeviceInfoPanel(logger, aDevicePM);
        transponderPanel = new TransponderPanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        this.setBorder(
            BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Details"), 
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.NORTHWEST;
        c.weighty = 1;
        c.weightx = 100;
        c.gridx = 0;
        c.gridy = 0;
        add(new ShowHideButton(), c);
        c.anchor=GridBagConstraints.CENTER;
        c.weighty=100;
        tabs = new JTabbedPane();
        tabs.add("Deformatter", new JScrollPane(deformatterPanel));
        tabs.add("Transponder", new JScrollPane(transponderPanel));
        tabs.add("Debug", debugPanel);
        tabs.add("Device Info", new JScrollPane(deviceInfoPanel));
        c.anchor = GridBagConstraints.CENTER;
        c.gridy++;
        add(tabs, c);

        this.setVisible(true);
    }
    
    /**
     * Setup the display settings for the monitor points that need it.
     * All internal values should be in SI units. But some values should be displayed
     * in something other than SI. E.g most users expect temperature in C.
     */
    //TODO: Check the TRX temp and ensure the spreadsheet is switched to K.
    private void customizeMonitorPoints () {
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayOffset(-273.15);
        devicePM.getMonitorPointPM(IdlMonitorPoints.GET_AMBIENT_TEMPERATURE).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_D).getMonitorPoint().setDisplayScale(1000000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_C).getMonitorPoint().setDisplayScale(1000000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.SIGNAL_AVG_B).getMonitorPoint().setDisplayScale(1000000000);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TRX_TEMPERATURE_B).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TRX_TEMPERATURE_B).getMonitorPoint().setDisplayOffset(-273.14);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TRX_TEMPERATURE_C).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TRX_TEMPERATURE_C).getMonitorPoint().setDisplayOffset(-273.14);
        devicePM.getMonitorPointPM(IcdMonitorPoints.TRX_TEMPERATURE_D).getMonitorPoint().setDisplayUnits("\u00B0C");
        devicePM.getMonitorPointPM(IcdMonitorPoints.TRX_TEMPERATURE_D).getMonitorPoint().setDisplayOffset(-273.14);
    }

    private class ShowHideButton extends JButton implements ActionListener {

        public ShowHideButton() {
            hideButtonText = "Hide Details";
            showButtonText = "Show Details";
            hideActionCommand = "HIDE_DETAILS";
            showActionCommand = "SHOW_DETAILS";

            setText(hideButtonText);
            setActionCommand(hideActionCommand);
            addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == hideActionCommand) {
                tabs.setVisible(false);
                setText(showButtonText);
                setActionCommand(showActionCommand);
            } else if (e.getActionCommand() == showActionCommand) {
                tabs.setVisible(true);
                setText(hideButtonText);
                setActionCommand(hideActionCommand);
            }
        }

        private String hideActionCommand;
        private String hideButtonText;
        // TODO - serial version UID
        private static final long serialVersionUID = 1L;
        private String showActionCommand;
        private String showButtonText;
    }
    
    private DebugPanel debugPanel;
    private DeformatterStatusPanel deformatterPanel;
    private DeviceInfoPanel deviceInfoPanel;
    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    JTabbedPane tabs;
    private TransponderPanel transponderPanel;

}

//
// O_o
