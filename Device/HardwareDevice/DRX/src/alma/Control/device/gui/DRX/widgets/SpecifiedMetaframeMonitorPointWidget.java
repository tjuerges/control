/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;
import alma.Control.device.gui.common.widgets.StatusColor;

import java.util.logging.Logger;

import javax.swing.JLabel;

/**
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 *        This point provides a workaround for a the Specified Metaframe delay because it was
 *        incorrectly entered into the spreadsheet.
 * 
 *        TODO: Once the spreadsheet is fixed, delete it.
 */
public class SpecifiedMetaframeMonitorPointWidget extends MonitorPointWidget {

    /**Construct a new SpecifiedMetaframeMonitorPointWidget
     * @param logger The logger that all logs should be written to.
     * @param monitorPointModel Least significant byte of specified metaframe
     * @param midMonitorPointModel The middle byte of the specified metaframe
     * @param msbMonitorPointModel Most significant bit of specified metaframe
     */
    public SpecifiedMetaframeMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Integer> monitorPointModel,
            MonitorPointPresentationModel<Integer> midMonitorPointModel,
            MonitorPointPresentationModel<Integer> msbMonitorPointModel) {
        super(logger, true);
        specifiedDelayLsb = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));
        specifiedDelayMid = addMonitorPointPM(midMonitorPointModel, Integer.valueOf(0));
        specifiedDelayMsb = addMonitorPointPM(msbMonitorPointModel, Integer.valueOf(0));
        toolTipText = getOperatingRangeToolTipText(msbMonitorPointModel);

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0");
        widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        widgetLabel.setToolTipText(toolTipText);
        add(widgetLabel);
        addPopupMenuToComponent(widgetLabel);
    }

    @Override
    protected void updateAttention(Boolean q) {
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        int output=0;
        output+=(Integer)getCachedValue(specifiedDelayLsb);
        output+=(int)((Integer)getCachedValue(specifiedDelayMid))<<8;
        output+=(int)((Integer)getCachedValue(specifiedDelayMsb))<<16;
        widgetLabel.setText(String.format("%d", output));
    }

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
	private IMonitorPoint specifiedDelayLsb;
	private IMonitorPoint specifiedDelayMid;
	private IMonitorPoint specifiedDelayMsb;
	private String toolTipText;
	private JLabel widgetLabel;
}

//
// O_o
