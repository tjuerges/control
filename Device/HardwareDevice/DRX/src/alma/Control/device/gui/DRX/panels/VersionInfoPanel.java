/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerAsByteSeqMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.VersionMonitorPointWidget;
import alma.Control.device.gui.common.widgets.ByteSeqMonitorPointWidget;
import alma.Control.device.gui.common.widgets.CodeToTextMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * This panel provides the version information on the DRX. SN, firmware version, etc.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class VersionInfoPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public VersionInfoPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        dFRModuleCodesPanel = new DFRModuleCodesPanel(logger, aDevicePM);
        lruPanel = new LruPanel(logger, aDevicePM);
        trxRevisions = new TrxRevisions(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(lruPanel, c);
        c.gridy++;
        add(dFRModuleCodesPanel, c);
        c.gridy++;
        add(trxRevisions, c);

        setVisible(true);
    }

    /*
     * This panel displays the module SN and relsted information.
     * TODO: Add M&C widgets
     */
    private class LruPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public LruPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("LRU Info"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));

            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first Widget
            addLeftLabel("Serial Number", c);
            add(new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_LRU_CIN_SN)), c);
            c.gridy++;
            addLeftLabel("Revision", c);
            //This method is a little bit dirty, but it saves writing a new widget.
            String [] letters = {"0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
                                 "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            add(new CodeToTextMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_LRU_CIN_REV), letters), c);
            c.gridy++;
            addLeftLabel("LRU CIN", c);
            add(new IntegerAsByteSeqMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_LRU_CIN_4THLEVEL),".", false),c);
        }
    }

    /*
     * This panel displays the Module revision and codes for each FPGA.
     * TODO: Add M&C widgets
     */
    private class DFRModuleCodesPanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public DFRModuleCodesPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("DFR Module Codes"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            add(new JLabel("Firmware Version  "),c);
            c.gridy++;
            //TODO: Make sure this is displaying info in a way that is OK with the DRX
            //TODO: Add the IPT ID and MID as hex monitor widgets
            addLeftLabel("D", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_D_MAJOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_D_MINOR),                        devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_D_DAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_D_MONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_D_YEAR)),c);
            c.gridy++;
            addLeftLabel("C", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_C_MAJOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_C_MINOR),    
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_C_DAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_C_MONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_C_YEAR)),c);
            c.gridy++;
            addLeftLabel("B", c);
            add(new VersionMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_B_MAJOR),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_B_MINOR),    
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_B_DAY),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_B_MONTH),
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_B_YEAR)),c);
            c.gridx=2;
            c.gridy=0;
            add(new JLabel("IPT ID  "), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_D_BE),
                    false, true), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_C_BE),
                    false, true), c);
            c.gridy++; 
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_B_BE),
                    false, true), c);
            c.gridx=3;
            c.gridy=0;
            add(new JLabel("MID"), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_D_TYPEID),
                    false, true), c);
            c.gridy++; 
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_C_TYPEID),
                    false, true), c);
            c.gridy++;
            add(new IntegerMonitorPointWidget(logger,
                    devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_FPGA_FW_VER_B_TYPEID),
                    false, true), c);
        }
    }

    /*
     * This panel provides the version information for each transponder.
     * TODO: Add M&C widgets
     */
    private class TrxRevisions extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public TrxRevisions(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("TRX Revisions"),
                    BorderFactory.createEmptyBorder(1, 1, 1, 1)));

            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            // c.fill = GridBagConstraints.HORIZONTAL;

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first Widget
            addLeftLabel("Access to the TRX versions", c);
            //TODO: Install TRX widget display
            c.gridy++;
            addLeftLabel("Will go here", c);
        }
    }
    
    private DFRModuleCodesPanel dFRModuleCodesPanel;
    private LruPanel lruPanel;
    private TrxRevisions trxRevisions;
}

//
// O_o

