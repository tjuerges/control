/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.widgets.MonitorPointWidget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * <code>MetaframeReadWriteWidget.java</code> is used to read and write the metaframe value stored in
 * the DRX EEPROM
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 7.0.0
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class MetaframeReadWriteWidget extends MonitorPointWidget implements ActionListener {

    public MetaframeReadWriteWidget(Logger logger,
            ControlPointPresentationModel<int[]> setEepromAddressControlPointModel,
            ControlPointPresentationModel<int[]> setEepromDataControlPointModel,
            MonitorPointPresentationModel<Integer> monitorPointModel){
        super(logger);
        this.logger = logger;
        this.setEepromFetchAddress = setEepromAddressControlPointModel;
        this.setEepromData = setEepromDataControlPointModel;
        this.monitorPoint = addMonitorPointPM(monitorPointModel, Integer.valueOf(0));

        display = new JLabel("<no data>");
        fetchMetaframeButton = new JButton("Fetch Metaframe");
        fetchMetaframeButton.addActionListener(this);
        //TODO: Should the fetch EEPROM button lock out with the other controls?
        //controlPointModel.getContainingDevicePM().addControl(this.fetchMetaframeButton);
        newMetaframeInput = new JFormattedTextField(NumberFormat.getIntegerInstance());
        newMetaframeInput.setValue(0);
        setEepromFetchAddress.getContainingDevicePM().addControl(this.newMetaframeInput);
        
        changeMetaframeButton = new JButton("Set Metaframe");
        changeMetaframeButton.addActionListener(this);
        setEepromData.getContainingDevicePM().addControl(this.changeMetaframeButton);
        
        buildWidget();
    }

    /* Get the data from the spinners and send it using setValue.
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==this.fetchMetaframeButton) { 
            getCurrentMetaframe();
        } else {
            setNewMetaframe();
        }
    }

    private void buildWidget() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 100;
        c.weightx = 100;
        c.gridx = 0;
        c.gridy = 0;
        add (new JLabel("Current Metaframe:"), c);
        c.gridx++;
        add(display, c);
        c.gridx--;
        c.gridy++;
        add(fetchMetaframeButton, c);
        c.gridy++;
        add (new JLabel("New Metaframe:"), c);
        c.gridx++;
        add(newMetaframeInput, c);
        c.gridx--;
        c.gridy++;
        add(changeMetaframeButton, c);
        c.gridx++;
        add (new JLabel("                      "), c);//Keeps the grid bag spacing correct.
    }

    @Override
    protected void updateAttention(Boolean q) {
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        //The value can only be updated right after sending the address to read.
    }

    /**Sends the value to be read to the control point. Then wait a bit and updates the display.
     */
    private void getCurrentMetaframe() {
        int metaframe=0;
        int[] output = new int[2];
        output[1]=0x00;        //Setup to get the MSB
        output[0]=0x10;
        setEepromFetchAddress.setValue(output);
        try{                //We need to wait at least 80 us. 
            Thread.sleep(1);    //This should be more than enough.
            metaframe=((Integer)getCachedValue(monitorPoint))<<16;
        }catch (InterruptedException e){
            display.setText("Error! Try again.");
            logger.severe("MetaframeReadWriteWidget: getCurrentMetaframe, failure to sleep");
            return;
        }
        output[1]=0x01;        //Setup to get the middle byte
        setEepromFetchAddress.setValue(output);
        try{                //We need to wait at least 80 us. 
            Thread.sleep(1);    //This should be more than enough.
            metaframe+=((Integer)getCachedValue(monitorPoint))<<8;
        }catch (InterruptedException e){
            display.setText("Error! Try again.");
            logger.severe("MetaframeReadWriteWidget: getCurrentMetaframe, failure to sleep");
            return;
        }
        output[1]=0x02;        //Setup to get the LSB
        setEepromFetchAddress.setValue(output);
        try{                //We need to wait at least 80 us. 
            Thread.sleep(1);    //This should be more than enough.
            metaframe+=(Integer)getCachedValue(monitorPoint);
        }catch (InterruptedException e){
            display.setText("Error! Try again.");
            logger.severe("MetaframeReadWriteWidget: getCurrentMetaframe, failure to sleep");
            return;
        }
        display.setText(Integer.toString(metaframe));
    }
    
    /* Write the new metaframe to the EEPROM in the DRX.
     * Uses the EEPROM programming RCA given in the ICD
     * The metaframe is stored at addresses 0x1000-0x1002
     */
    private void setNewMetaframe()
    {
//        int data =(Integer)newMetaframeInput.getValue();
        NumberFormat n;
        n = NumberFormat.getIntegerInstance();
        int data;
        try {
            data = (n.parse(newMetaframeInput.getText().trim())).intValue();
        } catch (ParseException e) {
            logger.severe("Error: impossible parse exception.");
            e.printStackTrace();
            return;
        }
        int output[] = new int[5];
        output[0] = 0x10;
        output[1] = 0x00;
        output[2] = (data & 0x00FF0000)>>16;
        output[3] = 0;
        output[4] = 0;
        setEepromData.setValue(output);
        try {
            Thread.sleep(15);   //Wait as per ICD
        } catch (InterruptedException e) {
            logger.severe("MetaframeReadWriteWidget: setNewMetaframe, failure to sleep");
            e.printStackTrace();
        }
        output[1] = 0x01;
        output[2] = (data & 0x0000FF00)>>8;
        setEepromData.setValue(output);
        try {
            Thread.sleep(15);   //Wait as per ICD
        } catch (InterruptedException e) {
            logger.severe("MetaframeReadWriteWidget: setNewMetaframe, failure to sleep");
            e.printStackTrace();
        }
        output[1] = 0x02;
        output[2] = data & 0x000000FF;
        setEepromData.setValue(output);
    }

    private JButton changeMetaframeButton;
    private JLabel display;
    private JButton fetchMetaframeButton;
    private Logger logger;
    private IMonitorPoint monitorPoint;
    private JFormattedTextField newMetaframeInput;
    private static final long serialVersionUID = 1L;
    private ControlPointPresentationModel<int[]> setEepromFetchAddress;
    private ControlPointPresentationModel<int[]> setEepromData;
}
