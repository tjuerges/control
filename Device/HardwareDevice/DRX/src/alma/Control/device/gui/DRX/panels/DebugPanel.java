/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;

import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This class contains template for a tab panel.
 * TODO: Replace DRX with the proper module name.
 * TODO: Replace DebugPanel with the proper panel name
 * TODO: Update this comment to reflect what the panel does.
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author David Hunter      dhunter@nrao.edu
 * @author Scott Rankin      srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DebugPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    JTabbedPane tabs;

    public DebugPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        debugAlignmentPanel = new DebugAlignmentPanel(logger, aDevicePM);
        debugMetaframePanel = new DebugMetaframePanel(logger, aDevicePM);
        debugPayloadDataPanel = new DebugPayloadDataPanel(logger, aDevicePM);
        debugTestDataPanel = new DebugTestDataPanel(logger, aDevicePM);
        debugXbarPanel = new DebugXbarPanel(logger, aDevicePM);
    }

    protected void buildPanel()
    {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        
        tabs = new JTabbedPane();

        tabs.add("Test Data", new JScrollPane(debugTestDataPanel));
        tabs.add("Payload Data", debugPayloadDataPanel);
        tabs.add("Alignment", new JScrollPane(debugAlignmentPanel));
        tabs.add("XBar", new JScrollPane(debugXbarPanel));
        tabs.add("Metaframe", new JScrollPane(debugMetaframePanel));
        
        add(tabs, c);
        this.setVisible(true);
    }
    
    private DebugAlignmentPanel debugAlignmentPanel;
    private DebugMetaframePanel debugMetaframePanel;
    private DebugPayloadDataPanel debugPayloadDataPanel;
    private DebugTestDataPanel debugTestDataPanel;
    private DebugXbarPanel debugXbarPanel;
}

//
// O_o
