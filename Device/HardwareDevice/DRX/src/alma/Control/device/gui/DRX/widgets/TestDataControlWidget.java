/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.widgets;

import alma.Control.device.gui.common.ControlPointPresentationModel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

/**
 * <code>TestDataControlWidget.java</code> presents the user with an interface for controling the 
 * SET_DRF_TEST_DATA on the DRX. The choices are saved as an array of strings in this widget
 * 
 * @author  David Hunter  dhunter@nrao.edu
 * @author  Scott Rankin  srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 5.0.4
 * 
 * TODO: Document
 * TODO: Factor out common behavior with other widgets to a superclass.
 */

public class TestDataControlWidget extends JPanel implements ActionListener {
    
    private static final long serialVersionUID = 1L;
    private Logger logger;
    private ControlPointPresentationModel<Integer> controlPointModelD;
    private ControlPointPresentationModel<Integer> controlPointModelC;
    private ControlPointPresentationModel<Integer> controlPointModelB;
    private ControlPointPresentationModel<Integer> controlPointModelAll;
    private JButton setD, setC, setB, setAll;
    private String DLabel="Set D";
    private String CLabel="Set C";
    private String BLabel="Set B";
    private String AllLabel="Set All";
    private JComboBox optionMenu;
    private JCheckBox pcSwitchOverride;
    private int[] optionCodes={0x00, 0x10, 0x20, 0x30, 1, 2, 3, 4, 5, 6, 8, 9, 0xA, 0xB, 0xC, 0xD, 0xE};
    private String[] optionList={"Normal Mode", "Random data", "Counting data",
                                   "Random with re-seed every TE", "All x3C", "All xC3", "All 3",
                                   "All C", "All 5", "xAAAA5555AAAA5555AAAA5555AAAA5555", "All 0",
                                   "All 1", "All A", "x5555AAAA5555AAAA5555AAAA5555AAAA",
                                   "cntr & cntr & cntr...", "FEDCBA9801234567BA987654456789AB",
                                   "0123456789ABCDEFFEDCBA9876543210"};
    
    
    /**Construct a new TestDataControlWidget
     * 
     * @param logger the logger that all logs should be directed to.
     * @param controlPointModelD The control point for SET_DFR_TEST_DATA ch D.
     * @param controlPointModelC The control point for SET_DFR_TEST_DATA ch C.
     * @param controlPointModelB The control point for SET_DFR_TEST_DATA ch B.
     * @param controlPointModelAll The control point for SET_DFR_TEST_DATA all.
     */
    public TestDataControlWidget( Logger logger,
            ControlPointPresentationModel<Integer> controlPointModelD,
            ControlPointPresentationModel<Integer> controlPointModelC,
            ControlPointPresentationModel<Integer> controlPointModelB,
            ControlPointPresentationModel<Integer> controlPointModelAll) {
        this.logger = logger;
        this.controlPointModelD = controlPointModelD;                //Save the control point models
        this.controlPointModelC = controlPointModelC;
        this.controlPointModelB = controlPointModelB;
        this.controlPointModelAll = controlPointModelAll;
        setD=new JButton(DLabel);                                   //Setup the Ch D button
        setD.addActionListener(this);
        controlPointModelD.getContainingDevicePM().addControl(setD);
        setC=new JButton(CLabel);                                   //Setup the Ch C button
        setC.addActionListener(this);
        controlPointModelC.getContainingDevicePM().addControl(setC);
        setB=new JButton(BLabel);                                   //Setup the Ch B button
        setB.addActionListener(this);
        controlPointModelB.getContainingDevicePM().addControl(setB);
        setAll=new JButton(AllLabel);                               //Setup the All channels button
        setAll.addActionListener(this);
        controlPointModelAll.getContainingDevicePM().addControl(setAll);
        
        pcSwitchOverride=new JCheckBox("Override PC Board Switch Control");  //Setup the override checkbox
        controlPointModelAll.getContainingDevicePM().addControl(pcSwitchOverride);
        
        optionMenu = new JComboBox();                              //Setup the optionMenu
        optionMenu.setEditable(false);
        for (String s: optionList) optionMenu.addItem(s);
        controlPointModelAll.getContainingDevicePM().addControl(optionMenu);
        buildWidget();
    }
    
    /** When an actionEvent occurs (e.g the control button is pressed) check the value in the
     *  menu. Find the corresponding code and send it to the control point.
     */
    public void actionPerformed(ActionEvent event) {
        int count=0;
        int optionCode=0;
        int checkBoxCode=0;
        int outCode;
        
        String newValue=(String)optionMenu.getSelectedItem();
        try{
            while (newValue != optionList[count])
                count++;
            optionCode=(optionCodes[count]);
        }
        catch (ArrayIndexOutOfBoundsException Eeeek){
            logger.severe("ERROR: Lower bit text given by user not found in optionList!");
        }        
        
        if (pcSwitchOverride.isSelected())
            checkBoxCode=0x80;
        
        outCode=optionCode | checkBoxCode;
        
        if (event.getActionCommand()==DLabel)
            controlPointModelD.setValue(outCode);
        if (event.getActionCommand()==CLabel)
            controlPointModelC.setValue(outCode);
        if (event.getActionCommand()==BLabel)
            controlPointModelB.setValue(outCode);
        if (event.getActionCommand()==AllLabel)
            controlPointModelAll.setValue(outCode);
    }
    
    /** Just add the option menu and control button to this panel.
     * @param buttonText The text to put on the control button.
     */
    private void buildWidget() {
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Set Test Data"),
                BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // Add all the points in a single column, starting at the top, with the labels on the left.
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth=4;
        add(optionMenu, c);
        c.gridwidth=1;
        c.gridy++;
        add(setD, c);
        c.gridx++;
        add(setC, c);
        c.gridx++;
        add(setB, c);
        c.gridx++;
        add(setAll, c);
        c.gridy++;
        c.gridx=0;
        c.gridwidth=4;
        add(pcSwitchOverride, c);
    }
}
