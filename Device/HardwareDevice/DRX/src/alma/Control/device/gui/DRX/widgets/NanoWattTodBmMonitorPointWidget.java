/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2007
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.widgets;

import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.device.gui.common.MonitorPointPresentationModel;
import alma.Control.device.gui.common.widgets.*;

import java.util.logging.Logger;
import javax.swing.JLabel;
import java.lang.Math;

/**
 * @author Scott Rankin srankin@nrao.edu
 * @author David Hunter dhunter@nrao.edu
 * @version $Id$
 * @since ALMA 5.0.4
 * 
 *        TODO: Document TODO: Factor out common behavior with BooleanMonitorPointWidget to a superclass.
 *        TODO: Add indication of out of range data.
 */
public class NanoWattTodBmMonitorPointWidget extends MonitorPointWidget {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    private boolean includeUnits;
    private int precision;
    private String toolTipText;
    private JLabel widgetLabel;
    private IMonitorPoint monitorPoint;

    public NanoWattTodBmMonitorPointWidget(Logger logger, MonitorPointPresentationModel<Float> model) {
        this(logger, model, false);
    }

    public NanoWattTodBmMonitorPointWidget(Logger logger,
            MonitorPointPresentationModel<Float> monitorPointModel, boolean includeUnits) {
        super(logger, true);

        precision = 4;

        this.includeUnits = includeUnits;

        monitorPoint = addMonitorPointPM(monitorPointModel, Float.valueOf(0.0F));
        toolTipText = "Operating range: -16.02dBm to -0.969dBm";

        buildWidget();
    }

    protected void buildWidget() {
        widgetLabel = new JLabel();
        widgetLabel.setText("0.0");
        widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        widgetLabel.setToolTipText(toolTipText);
        add(widgetLabel);

        addPopupMenuToComponent(widgetLabel);
    }

    private float dropInsignificantDigits(Float value) {
        // TODO: Remove hack.
        // This hack works around the current non-generic GeneratedDevicePresentationModelEvent class.
        float scale = (float) Math.pow(10, precision - 1);
        return Math.round(value.floatValue() * scale) / scale;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    @Override
    protected void updateAttention(Boolean q) {
        if (q.booleanValue()) {
            widgetLabel.setForeground(StatusColor.FAILURE.getColor());
        } else {
            widgetLabel.setForeground(StatusColor.NORMAL.getColor());
        }
    }

    @Override
    protected void updateValue(IMonitorPoint source) {
        String units = "";
        if (includeUnits == true && source.supportsUnits()) {
            units = " dBm";
        }

        widgetLabel.setText(Float.toString(dropInsignificantDigits(
                                    convertNanoWattTodBm((Float) getCachedValue(monitorPoint))))+ units);
    }
    
    //Note that this conversion is for optical power. And that the control software handles the point watts
    private float convertNanoWattTodBm(float f){
        return (float) (10*Math.log10(f*1000.0));
    }
}

//
// O_o
