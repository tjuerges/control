/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.DRX.widgets.MetaframeWarningWidget;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.CombinedBooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.util.logging.Logger;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This panel displays the Summary data.
 * 
 * @author David Hunter      dhunter@nrao.edu
 * @author Scott Rankin      srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    public SummaryPanel(Logger logger, DevicePM aDevicePM) {
    	super(logger, aDevicePM);
    	buildPanel();
    }
    
    protected void buildPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder
                (devicePM.getDeviceDescription()),BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor=GridBagConstraints.CENTER;
        c.weightx=100;
        c.weighty=100;
        c.gridx = 1;
        c.gridy = 0;
        addLeftLabel("TRX Alarm", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_TRXALM),
                Status.FAILURE, "TRX Error",
                Status.GOOD, "No TRX Alarms"), c);
        c.gridy++;
        addLeftLabel("TE Alarm", c);
        add(new BooleanMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_TEERROR),
                Status.FAILURE, "TE Error",
                Status.GOOD, "No TE Errors"), c);
        c.gridx=3;
        c.gridy=0;
        addLeftLabel("System Clock", c);
        add(new CombinedBooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYSCLK),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYSCLK),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYSCLK),
                Status.FAILURE,"Error on one or more System Clock PLL Locks",
                Status.GOOD, "System Clock PLL Lock OK on all Chanels"), c);
        c.gridy++;
        addLeftLabel("Data Clock", c);
        add(new CombinedBooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_DATAINPCLK),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_DATAINPCLK),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_DATAINPCLK),
                Status.FAILURE,"Data clock error on one or more channels",
                Status.GOOD, "No data clock errors on any chanels"), c);
        c.gridx=6;
        c.gridy=0;
        addLeftLabel("Parity Error", c);
        add(new CombinedBooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_PARITYERR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_PARITYERR),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_PARITYERR),
                Status.FAILURE,"Parity error on one or more channels",
                Status.GOOD, "No parity errors on any chanels"), c);
        c.gridy++;
        addLeftLabel("Sync Pattern", c);
        add(new CombinedBooleanMonitorPointWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYNCDETECTED),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYNCDETECTED),
                devicePM.getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYNCDETECTED),
                Status.FAILURE,"Error: Sync pattern lost on one or more channels",
                Status.GOOD, "Sync Pattern detected on all channels"), c);
        c.gridy=0;
        c.gridx+=2;
        addLeftLabel("Metaframe", c);
        add(new MetaframeWarningWidget(logger,
                devicePM.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_D),
                devicePM.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_C),
                devicePM.getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_B),
                devicePM.getMonitorPointPM(IcdMonitorPoints.SPECIFIED_METAFRAME_STORED_DELAY)), c);
        
        c.gridy++;
        addLeftLabel("Delay C", c);
        add( new IntegerMonitorPointWidget(logger, devicePM
                .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_C)), c);
        this.setVisible(true);
    }
    
    /*
     * A simple helper function that allows you to add a label to the left of the current location with
     * grid bag layout.
     */
    protected void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        c.anchor=GridBagConstraints.EAST;
        add(new JLabel(s), c);
        c.anchor=GridBagConstraints.WEST;
        c.gridx++;
    }

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
}

//
// O_o
