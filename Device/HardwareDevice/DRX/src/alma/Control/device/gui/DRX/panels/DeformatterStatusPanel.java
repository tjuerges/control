/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DRX.panels;

import alma.Control.device.gui.DRX.IcdControlPoints;
import alma.Control.device.gui.DRX.IcdMonitorPoints;
import alma.Control.device.gui.DRX.presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.LongMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.TextToCodeControlPointWidget;
import alma.Control.device.gui.common.widgets.DtsResetTeControlPointWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized to in panels and tabs to group related data together.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * This panel provides the general status of the deformatter. Included is the status register, parity info,
 * sync status, metaframe delay, and TE status.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DeformatterStatusPanel extends DevicePanel {

    public DeformatterStatusPanel(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
        metaframePanel = new MetaframePanel(logger, aDevicePM);
        parityTimerPanel = new ParityTimerPanel(logger, aDevicePM);
        resetButtonPanel = new ResetButtonPanel(logger, aDevicePM);
        statusRegPanel = new StatusRegPanel(logger, aDevicePM);
        syncStatusPanel = new SyncStatusPanel(logger, aDevicePM);
        tePanel = new TePanel(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight=2;
        add(statusRegPanel, c);
        c.gridy+=2;
        c.gridheight=1;
        c.gridwidth=2;
        add(tePanel, c);
        c.gridwidth=1;
        c.gridy++; 
        add (resetButtonPanel, c);
        c.gridy = 0;
        c.gridx = 1;
        add(syncStatusPanel, c);
        c.gridy++;
        add(metaframePanel, c);
        c.gridy+=2;
        add(parityTimerPanel, c);

        setVisible(true);
    }

    /*
     * This panel contains the Metaframe Delay readouts
     */
    private class MetaframePanel extends DevicePanel {
    
        public MetaframePanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            this.setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("Metaframe Delay"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
    
            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
    
            addLeftLabel("Delay D", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_D)),c);
            c.gridy++;
            addLeftLabel("Delay C", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_C)),c);
            c.gridy++;
            addLeftLabel("Delay B", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.METAFRAME_DELAY_B)),c);
        }
    
    	// TODO - serial version UID
    	private static final long serialVersionUID = 1L;
    }

    /*
     * This panel contains all the parity timer information.
     * 
     * Known bug: The parity accumulators are specified in the ICD as unsigned 64 bit numbers. The C++ control
     * code thinks of them as such. However, Java does not support unsigned ints, and the C++/Java interface
     * does not support Java arbitrary precision types. As a result, if the last bit of the counter is set
     * the number will overflow and become negative. This is a low impact bug because it would take an
     * estimated 46764362479.287613 years for the overflow to occur.
     * (Jira ticket COMP-COMP-3671 tracks the ICD fix for this bug)
     */
    private class ParityTimerPanel extends DevicePanel {
    
        public ParityTimerPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            this.setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("Parity Status"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
    
            c.gridx=0;
            c.gridy=0;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_ALL),"Reset All"), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_D)," Reset D "), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_C)," Reset C "), c);
            c.gridy++;
            add(new BooleanControlPointWidget(logger, devicePM
                    .getControlPointPM(IcdControlPoints.RESET_DFR_PARITY_B)," Reset B "), c);
            c.gridx+=2;
            c.gridy=0;
            c.fill = GridBagConstraints.NONE;
            add (new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("1s Timer", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_D_TIME)),c);            
            c.gridy++;
            addLeftLabel("1s Errors", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_D_PARITY)),c);
            c.gridy++;
            addLeftLabel("Accumulators", c);
            add( new LongMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_COUNTER_D)),c);
            c.gridx++;
            c.gridy = 0;
            add (new JLabel("C"), c);
            c.gridy++;
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_C_TIME)),c);            
            c.gridy++;
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_C_PARITY)),c);
            c.gridy++;
            add( new LongMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_COUNTER_C)),c);
            c.gridx++;
            c.gridy = 0;
            add (new JLabel("B"), c);
            c.gridy++;
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_B_TIME)),c);            
            c.gridy++;
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_TIMER_B_PARITY)),c);
            c.gridy++;
            add( new LongMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_PARITY_COUNTER_B)),c);
        }
    
    	// TODO - serial version UID
    	private static final long serialVersionUID = 1L;
    }

    /*
     * The status register and FPGA reset buttons are stored in this panel,
     */
    private class ResetButtonPanel extends DevicePanel {

        public ResetButtonPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            this.setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("FPGA Resets"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 0;
            c.gridy = 0;
            String[] resetOptions = {"Clear Status Registers", "Master Reset"};
            int[] resetCodes = {1, 0xFF};
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM
                    (IcdControlPoints.DFR_RESET_D), " Reset D ", resetOptions, resetCodes), c); 
            c.gridy++;
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM
                    (IcdControlPoints.DFR_RESET_C), " Reset C ", resetOptions, resetCodes), c);
            c.gridy++;
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM
                    (IcdControlPoints.DFR_RESET_B), " Reset B ", resetOptions, resetCodes), c);
            c.gridy++;
            add(new TextToCodeControlPointWidget(logger, devicePM.getControlPointPM
                    (IcdControlPoints.DFR_RESET_ALL), "Reset All", resetOptions, resetCodes), c);
        }

		// TODO - serial version UID
		private static final long serialVersionUID = 1L;
    }


    /*
     * Pane to display the contents of the DFR Status register
     */
    private class StatusRegPanel extends DevicePanel {
    
        public StatusRegPanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }
    
        protected void buildPanel() {
            this.setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("FPGA Status"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;
    
            add(new JLabel("D"), c);
            c.gridy++;
            addLeftLabel("System Clock PLL Lock", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYSCLK),
                    Status.FAILURE,"Channel D system clock PLL lock lost",
                    Status.GOOD, "Channel D system clock PLL locked"),c);
            c.gridy++;
            addLeftLabel("Data Clock PLL Lock", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_DATAINPCLK),
                    Status.FAILURE, "Channel D data clock PLL lock lost",
                    Status.GOOD, "Channel D data clock PLL locked"),c);
            c.gridy++;
            addLeftLabel("Half Transponder Lock", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_TRXLOCKED),
                    Status.FAILURE, "Channel D half transponder lock lost",
                    Status.GOOD, "Channel D half transponder locked"),c);
            c.gridy++;
            addLeftLabel("Input Frame Error", c);
            c.gridy++;
            addLeftLabel("FIFO Increment/Decrement", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_FIFOALM),
                    Status.FAILURE, "Channel D FIFO increment / decrement error",
                    Status.GOOD, "Channel D FIFO increment / decrement OK"), c);
            c.gridy++;
            addLeftLabel("Parity Error", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_PARITYERR),
                    Status.FAILURE, "Parity error on channel D ",
                    Status.GOOD, "No parity errors on channel D"), c);
            c.gridy++;
            addLeftLabel("Sync Pattern Detected", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_SYNCDETECTED),
                    Status.FAILURE, "Channel D sync pattern error",
                    Status.GOOD,"Channel D sync pattern detected"),c);
            c.gridy++;
            addLeftLabel("Frame Aligned to Center", c);
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_D_FRAMEALIGNED),
                    Status.FAILURE, "Channel D frame alignment error",
                    Status.GOOD,"Channel D frame alignment OK"),c);
            c.gridy++;
            addLeftLabel("I/O Frame Error", c);
            
            c.gridx = 2;
            c.gridy = 0;
            add(new JLabel("C"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYSCLK),
                    Status.FAILURE, "Channel C system clock PLL lock lost",
                    Status.GOOD, "Channel C system clock PLL locked"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_DATAINPCLK),
                    Status.FAILURE, "Channel C data clock PLL lock lost",
                    Status.GOOD,"Channel C data clock PLL locked"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_TRXLOCKED),
                    Status.FAILURE, "Channel C half transponder lock lost",
                    Status.GOOD, "Channel C half transponder locked"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_INPFRAMEERR),
                    Status.FAILURE, "Channel C FIFO input frame error",
                    Status.GOOD, "Channel C FIFO input frame OK"), c);
            c.gridy++;
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_PARITYERR),
                    Status.FAILURE, "Parity error on channel C",
                    Status.GOOD, "No parity errors on channel D"),c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_SYNCDETECTED),
                    Status.FAILURE, "Channel C sync pattern error",
                    Status.GOOD, "Channel C sync pattern detected"),c);
            c.gridy++;
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_C_OUTFRAMERR),
                    Status.FAILURE, "Channel C output frame error",
                    Status.GOOD, "Channel C output frame OK"),c);
            c.gridx = 3;
            c.gridy = 0;
            add(new JLabel("B"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYSCLK),
                    Status.FAILURE, "Channel B system clock PLL lock lost",
                    Status.GOOD, "Channel B system clock PLL locked"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_DATAINPCLK),
                    Status.FAILURE, "Channel B data clock PLL lock lost",
                    Status.GOOD, "Channel B data clock PLL locked"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_TRXLOCKED),
                    Status.FAILURE, "Channel B half transponder lock lost",
                    Status.GOOD, "Channel B half transponder locked"), c);
            c.gridy++;
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_FIFOALM),
                    Status.FAILURE, "Channel B FIFO increment / decrement error",
                    Status.GOOD, "Channel B FIFO increment / decrement OK"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_PARITYERR),
                    Status.FAILURE, "Parity error on channel B",
                    Status.GOOD, "No parity errors on channel B"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_SYNCDETECTED),
                    Status.FAILURE, "Channel B sync pattern error",
                    Status.GOOD, "Channel B sync pattern detected"), c);
            c.gridy++;
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.DFR_STATUS_B_FRAMEALIGNED),
                    Status.FAILURE, "Channel B frame alignment error",
                    Status.GOOD, "Channel B frame alignment OK"),c);
        }
    
    	// TODO - serial version UID
    	private static final long serialVersionUID = 1L;
    }

    /*
     * This panel contains all of the TE related widgets
     */
    private class TePanel extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public TePanel(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            this.setBorder(BorderFactory.createCompoundBorder(BorderFactory
                    .createTitledBorder("Timing Event"), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;
            c.weightx=10;
            c.weighty=10;
            addLeftLabel("TE Error Flag", c);
            //This point is opposite of the other points in the DRX; 1 is error and 0 is good!
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TE_ERRS_TEERR),
                    Status.GOOD, "Timing Event OK",
                    Status.FAILURE, "Timing Event Error"), c);
            c.gridy++;
            addLeftLabel("Clock Edge Status", c);
            //For this point neither point is a failure. However, the inverted clock edge is only expected
            //during debugging. 
            add(new BooleanMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TE_ERRS_CLKEDGE),
                    Status.GOOD, "Using regular clock edge to clock in timing event",
                    Status.ATYPICAL, "Using inverted clock edge to clock in timing event"), c);
            c.gridy=0;
            c.gridx=3;
            addLeftLabel("Current Error", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TE_ERRS_CURRENTERR)),c);
            c.gridy++;
            addLeftLabel("  Late Error", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TE_ERRS_LATEERR)),c);
            c.gridy++;
            addLeftLabel("  Early Error", c);
            add( new IntegerMonitorPointWidget(logger, devicePM
                    .getMonitorPointPM(IcdMonitorPoints.TE_ERRS_EARLYERR)),c);
            
            c.gridx++;
            c.gridy=0;
            c.gridheight=3;
            c.weightx=100;
            c.weighty=100;
            add(new DtsResetTeControlPointWidget(logger,
                    devicePM.getControlPointPM(IcdControlPoints.RESET_TE_ERRS)), c);
        }
    }

	// TODO - serial version UID
	private static final long serialVersionUID = 1L;
	
	private MetaframePanel metaframePanel;
	private ParityTimerPanel parityTimerPanel;
	private ResetButtonPanel resetButtonPanel;
	private StatusRegPanel statusRegPanel;
	private SyncStatusPanel syncStatusPanel;
	private TePanel tePanel;
}

//
// O_o

