#! /usr/bin/env python
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It contains complementary functionality for a DRX device.
"""

import CCL.DRXBase
from CCL import StatusHelper
from math import log

class DRX(CCL.DRXBase.DRXBase):
    '''
    The DRX class inherits from the code generated DRXBase
    class and adds specific methods.
    '''
    def __init__(self, antennaName = None, instance = None, componentName = None, stickyFlag = False):
        '''
        The constructor creates a DRX object by making use of
        the DRXBase constructor.
        EXAMPLE:
        from CCL.DRX import DRX
        #Directly through component name
        drx0 = DRX(componentName="CONTROL/DV01/DRXBBpr0")
        #or specifying the antenna name and the instance number of DRX.
        #Instance can be 0, 1, 2 or 3
        drx0 = DRX("DV01", 0)
        '''
        
        if componentName == None:
            if ((antennaName == None) or (instance == None)):
                raise NameError, "missing component name or the antennaName and instance number"
            if (isinstance(antennaName,list) == True):
                if (len(antennaName) != 0):
                    componentName=[]
                    for idx in range (0,len(antennaName)):
                        componentName.append("CONTROL/"+antennaName[idx]+"/DRXBBpr"+str(instance))
            else:
                componentName = "CONTROL/"+antennaName+"/DRXBBpr"+str(instance)
                
        CCL.DRXBase.DRXBase.__init__(self, None, componentName, stickyFlag)

    def __del__(self):
        CCL.DRXBase.DRXBase.__del__(self)

    def __getDFRMode(self, value):
        if value == 0:
            return "Normal"
        elif value == 128:
            return "Normal Overide"
        elif value == 144:
            return "Simulated"
        return "Unkown %d" % value

    def STATUS(self):
        '''
        This method prints the status of DRX
        '''
	result = {}
        for key, val in self._devices.iteritems():
            listNames = key.rsplit("/")
            antName = listNames[1]
            compName = listNames[2]

	    elements = []
            #
            # GET_DFR_TEST_DATA_B()
            # GET_DFR_TEST_DATA_C()
            # GET_DFR_TEST_DATA_D()
            #
            try:
                dfr_value_b = self._devices[key].GET_DFR_TEST_DATA_B()[0]
                dfr_value_b = self.__getDFRMode(dfr_value_b)
                dfr_value_c = self._devices[key].GET_DFR_TEST_DATA_C()[0]
                dfr_value_c = self.__getDFRMode(dfr_value_c)
                dfr_value_d = self._devices[key].GET_DFR_TEST_DATA_D()[0]
                dfr_value_d = self.__getDFRMode(dfr_value_d)
                
            except:
                dfr_value_b = "N/A"
                dfr_value_c = "N/A"
                dfr_value_d = "N/A"
                
            dfr_status_l = StatusHelper.Line("DFR DATA:", [StatusHelper.ValueUnit(dfr_value_b, label="B"), 
                                                                StatusHelper.ValueUnit(dfr_value_c, label="C"),
                                                                StatusHelper.ValueUnit(dfr_value_d, label="D")])

            #
            # GET_SIGNAL_AVG_B()
            # GET_SIGNAL_AVG_C()
            # GET_SIGNAL_AVG_D()
            #
            try:
                nw_value_b = "%.3f" % ( self._devices[key].GET_SIGNAL_AVG_B()[0] * 1e9 )
                nw_value_c = "%.3f" % ( self._devices[key].GET_SIGNAL_AVG_C()[0] * 1e9 )
                nw_value_d = "%.3f" % ( self._devices[key].GET_SIGNAL_AVG_D()[0] * 1e9 )
            except:
                nw_value_b = "N/A"
                nw_value_c = "N/A"
                nw_value_d = "N/A"
                
            signal_avg_nw_status_l = StatusHelper.Line("SIGNAL_AVG_W:", [StatusHelper.ValueUnit(nw_value_b, "nW", label="B"), 
                                                                          StatusHelper.ValueUnit(nw_value_c, "nW", label="C"),
                                                                          StatusHelper.ValueUnit(nw_value_d, "nW", label="D")])
            

            #
            # GET_SIGNAL_AVG_dBm_B()
            # GET_SIGNAL_AVG_dBm_C()
            # GET_SIGNAL_AVG_dBm_D()
            #
            value_b = "N/A"
            value_c = "N/A"
            value_d = "N/A"
            try:            
                if (nw_value_b != "N/A"):
                    if (nw_value_b < 1):
                        value_b = "%.3f" % (-60.0)
                    elif (nw_value_b == 0):
                        value_b = 0
                    else:
                        value_b = "%.3f" % (10.0 * log(1e3 * float(nw_value_b) * 1e-9, 10))
            except OverflowError:
                value_b = "-INF"
            except ValueError:
                value_b = "-INF"
            try:            
                if (nw_value_c != "N/A"):
                    if (nw_value_c < 1):
                        value_c = "%.3f" % (-27.0)
                    elif (nw_value_c == 0):
                        value_c = 0
                    else:
                        value_c = "%.3f" % (10.0 * log(1e3 * float(nw_value_c) * 1e-9, 10))
            except OverflowError:
                value_c = "-INF"
            except ValueError:
                value_b = "-INF"
            
            try:            
                if (nw_value_d != "N/A"):
                    if (nw_value_d < 1):
                        value_d = "%.3f" % (-27.0)
                    elif (nw_value_d == 0):
                        value_d = 0
                    else:
                        value_d = "%.3f" % (10.0 * log(1e3 * float(nw_value_d) * 1e-9, 10))
            except OverflowError:
                value_d = "-INF"
            except ValueError:
                value_b = "-INF"
                
               
	    signal_avg_dbm_status_l = StatusHelper.Line("SIGNAL_AVG_dBm:", [StatusHelper.ValueUnit(value_b, "dBm", label="B"), 
                                                                            StatusHelper.ValueUnit(value_c, "dBm", label="C"),
                                                                            StatusHelper.ValueUnit(value_d, "dBm", label="D")])

            #
            # GET_DFR_PARITY_COUNTER_B()
            # GET_DFR_PARITY_COUNTER_C()
            # GET_DFR_PARITY_COUNTER_D()
            #
            try:
                value_b = "%ld" % self._devices[key].GET_DFR_PARITY_COUNTER_B()[0]
                value_c = "%ld" % self._devices[key].GET_DFR_PARITY_COUNTER_C()[0]
                value_d = "%ld" % self._devices[key].GET_DFR_PARITY_COUNTER_D()[0]
            except:
                value_b = "N/A"
                value_c = "N/A"
                value_d = "N/A"

            dfr_parity_counter_status_l = StatusHelper.Line("PARITY_COUNTER:", [StatusHelper.ValueUnit(value_b, label="B"), 
                                                                                StatusHelper.ValueUnit(value_c, label="C"),
                                                                                StatusHelper.ValueUnit(value_d, label="D")])
            
            #
            # GET_DFR_PARITY_COUNTER_B()
            # GET_DFR_PARITY_COUNTER_C()
            # GET_DFR_PARITY_COUNTER_D()
            #
            try:
                value_b = "%ld" % self._devices[key].GET_DFR_PARITY_COUNTER_B()[0]
                value_c = "%ld" % self._devices[key].GET_DFR_PARITY_COUNTER_C()[0]
                value_d = "%ld" % self._devices[key].GET_DFR_PARITY_COUNTER_D()[0]
            except:
                value_b = "N/A"
                value_c = "N/A"
                value_d = "N/A"

            dfr_parity_counter_status_l = StatusHelper.Line("PARITY_COUNTER:", [StatusHelper.ValueUnit(value_b, label="B"), 
                                                                                StatusHelper.ValueUnit(value_c, label="C"),
                                                                                StatusHelper.ValueUnit(value_d, label="D")])
             
            #
            # GET_METAFRAME_DELAY_B()
            # GET_METAFRAME_DELAY_C()
            # GET_METAFRAME_DELAY_D()
            #
            try:
                value_b = "%d" % self._devices[key].GET_METAFRAME_DELAY_B()[0]
                value_c = "%d" % self._devices[key].GET_METAFRAME_DELAY_C()[0]
                value_d = "%d" % self._devices[key].GET_METAFRAME_DELAY_D()[0]
            except:
                value_b = "N/A"
                value_c = "N/A"
                value_d = "N/A"

            metaframe_delay_status_l = StatusHelper.Line("METAFRAME_DELAY:", [StatusHelper.ValueUnit(value_b, label="B"), 
                                                                              StatusHelper.ValueUnit(value_c, label="C"),
                                                                              StatusHelper.ValueUnit(value_d, label="D")])
            
	    elements.append(dfr_status_l)
	    elements.append(signal_avg_dbm_status_l)
	    elements.append(signal_avg_nw_status_l)
            elements.append(dfr_parity_counter_status_l)
            elements.append(metaframe_delay_status_l)
	    statusFrame = StatusHelper.Frame(compName, elements)
	    statusFrame.printScreen()


