//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <DRXImpl.h>
#include <string>
#include <ostream>
#include <TETimeUtil.h>
#include <acstimeTimeUtil.h>
#include <loggingMACROS.h>


/**
 *-----------------------
 * DRX Constructor
 *-----------------------
 */
DRXImpl::DRXImpl(const ACE_CString& name, maci::ContainerServices* cs):
    DRXBase(name, cs)

{
}

/**
 *-----------------------
 * DRX Destructor
 *-----------------------
 */
DRXImpl::~DRXImpl()
{
}

/**
 *---------------------------------
 * ACS Component Lifecycle Methods
 *---------------------------------
 */
void DRXImpl::initialize()
{
    diagnosticModeEnabled = true;

    try
    {
        DRXBase::initialize();
    }
    catch (acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

/**
 *----------------------------
 * Hardware Lifecycle Methods
 *----------------------------
 */
void DRXImpl::hwInitializeAction()
{
    // Call the base class implementation
    DRXBase::hwInitializeAction();

    // Set configured clock edge to time in
    try
    {
        configClockEdge();
    }
    catch(const DRXExceptions::ConfigClockEdgeExImpl& ex)
    {
        // Report error.
        LOG_TO_OPERATOR(LM_WARNING, "Failure while configuring clock edge.");
    }
}

void DRXImpl::hwOperationalAction()
{
    // Call the base class implementation
    DRXBase::hwOperationalAction();

    // Complete reset of DFR.
    try
    {
        LOG_TO_DEVELOPER(LM_DEBUG, "Sending DFR_RESET_ALL(128)");

        setDfrResetAll(128);

        sleep(3);
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Failed to reset DFR.");

        throw ControlExceptions::CAMBErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        LOG_TO_OPERATOR(LM_ERROR, "Invalid State to reset DFR");

        throw ControlExceptions::INACTErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

void DRXImpl::hwStopAction()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    AmbErrorCode_t flushStatus(AMBERR_PENDING);
    ACS::Time flushTime(0ULL);

    if(isStartup() == true)
    {
        // Flush all commands.
        flushNode(0, &flushTime, &flushStatus);
        if(flushStatus != AMBERR_NOERR)
        {
            LOG_TO_OPERATOR(LM_WARNING, "Communication failure flushing "
                "commands to device");
        }
        else
        {
            std::ostringstream msg;
            msg << "All commands and monitors flushed at: "
                << flushTime;
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
    }

    // Call the base class implementation.
    DRXBase::hwStopAction();
}

void DRXImpl::configClockEdge()
{
    CORBA::String_var compName(acscomponent::ACSComponentImpl::name());
    const std::string fullName(std::string("alma/") + compName.in());
    // Get the reference of the component
    CDB::DAL_ptr dal_p(getContainerServices()->getCDB());
    CDB::DAO_ptr dao_p(dal_p->get_DAO_Servant(fullName.c_str()));

    /**
     * TODO: The DRX now automatically picks the right clock edge.
     * Perhaps this code can be entirely removed.
     */
    // get value from CDB
    std::string useNegClkEdge;
    try
    {
        useNegClkEdge = dao_p->get_field_data("USE_NEG_CLOCK_EDGE");

        std::string msg("Use Negative Clock Edge: ");
        msg .append(useNegClkEdge);
        LOG_TO_DEVELOPER(LM_DEBUG, msg);
    }
    catch(const cdbErrType::WrongCDBDataTypeEx& ex)
    {
        DRXExceptions::ConfigClockEdgeExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to get clock edge config from CDB: Bad "
            "Data Type.");

        throw nex;
    }
    catch(const cdbErrType::CDBFieldDoesNotExistEx& ex)
    {
        DRXExceptions::ConfigClockEdgeExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to get clock edge config from CDB: "
            "Field does not exist.");

        throw nex;
    }

    try
    {
        if(useNegClkEdge == "true")
        {
            unsigned char ResetTeWord = 0x02;
            setResetTeErrs(ResetTeWord);

            LOG_TO_DEVELOPER(LM_DEBUG, "Comparing TE with Negative Clk Edge: "
                "FR_TE_RESET(0x2)");
        }
        else if(useNegClkEdge == "false")
        {
            unsigned char ResetTeWord = 0x00;
            setResetTeErrs(ResetTeWord);

            LOG_TO_DEVELOPER(LM_DEBUG, "Comparing TE with Positive Clk Edge: "
                "FR_TE_RESET(0x00)");
        }
        else
        {
            DRXExceptions::ConfigClockEdgeExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", "Invalid data in USE_NEG_CLOCK_EDGE");

            throw ex;
        }
    }
    catch(const ControlExceptions::CAMBErrorExImpl& ex)
    {
        DRXExceptions::ConfigClockEdgeExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed set clock edge: CAMB error.");

        throw nex;
    }
    catch(const ControlExceptions::INACTErrorExImpl& ex)
    {
        DRXExceptions::ConfigClockEdgeExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed set clock edge: Invalid hwState.");

        throw nex;
    }
}


/**
 *----------------------------
 * MACI DLL support functions
 *----------------------------
 */
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(DRXImpl)
