#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
#
# @(#) $Id$
#------------------------------------------------------------------------------
'''
DESCRIPTION
The DRXTest provides a brief functional test of the
component. It is foreseen to be used only for offline
tests and in simulation mode.
'''

# Import unit test
import unittest

# Import system and OS
import sys
import os

# Import the needed ACS modules
import Acspy.Common.QoS
from Acspy.Clients.SimpleClient import PySimpleClient

# Other imports
from time import sleep
import exceptions

# Automated test class
class automated( unittest.TestCase ):

    def setUp(self):

        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()

    def tearDown(self):

        self.client.releaseComponent(self.componentName)
        self.client.disconnect()
        del(self.client)

    def initDRX(self):

        # Define the component name to be used
        self.componentName = "CONTROL/DA41/DRXBBpr2"
        print "Instantiating DRX component..."

        try:
            # Get the component reference
            self.drx = self.client.getComponent(self.componentName)

            # Initialize the component
            print "Initializing component via HW lifecycle..."
            self.drx.hwStart()
            self.drx.hwConfigure()
            self.drx.hwInitialize()
            self.drx.hwOperational()

            # Wait for 2 seconds to start everything up
            sleep(2)

            # Stop the Componnent
            self.drx.hwStop()

        except Exception, e:
            print "Exception: ", str(e)

    def test01(self):
        '''
        Component instantiation and intialization test.
        '''
        self.initDRX()
        sleep(1)
        self.client.releaseComponent(self.componentName)
	sleep(1)

# MAIN Program
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()


