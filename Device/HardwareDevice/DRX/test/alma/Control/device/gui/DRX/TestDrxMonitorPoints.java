/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/**
 * Test for checking the DRX monitor points. This test extends TestMonitorPoints to provide
 * for testing. 
 *
 * @author  David Hunter    dhunter@nrao.edu
 * @version 
 * @since 
 */


package alma.Control.device.gui.DRX;

import alma.Control.DRXCompSimBase;
import alma.Control.HardwareDevice;
import alma.Control.device.gui.common.TestMonitorPoints;
import alma.acs.container.ContainerServices;


public class TestDrxMonitorPoints extends TestMonitorPoints {

    /**
     * The constructor. Just call super...
     * For some reason the ACS component client test case constructor throws exceptions, so we have to
     * pass them along because Java won't let us handle them!
     * @param name
     * @throws Exception
     */
    public TestDrxMonitorPoints(String name) throws Exception {
        super(name);
    }

    /**
     * Set things up for the test!
     *      -Tell the parent class to setup
     *      -Get the DRX component.
     *      
     * Setup must be called before running any tests!
     *
     * Precondition: There MUST be a simulated DRX up and running before calling setup. This function
     * will fail if there is no DRX running.
     */
    protected void setUp() throws Exception {
        super.setUp();
        DRXCompSimBase drxRef=null;
        if (null != componentName) {
            ContainerServices c = getContainerServices();
            assertNotNull(c);
            org.omg.CORBA.Object compObj=c.getComponent(componentName);
            assertNotNull(compObj);
            drxRef = (DRXCompSimBase) narrowToSimObject(compObj);
            super.setUp(drxRef, componentName, TestDataFileLocation);
        }
        else
            fail("Error: componentName is null!");
    }
    
    /** Narrows a corba DRX object into a DRXCompSimBase.
     * Based off of DRX.narrow, which only works on DRXes.
     * @param obj
     * @return
     */
    public HardwareDevice narrowToSimObject(final org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        try
        {
            return (alma.Control.DRXCompSimBase)obj;
        }
        catch (ClassCastException c)
        {
            if (obj._is_a("IDL:alma/Control/DRX:1.0"))
            {
                alma.Control._DRXCompSimBaseStub stub;
                stub = new alma.Control._DRXCompSimBaseStub();
                stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
                return stub;
            }
        }
        throw new org.omg.CORBA.BAD_PARAM("Narrow failed");
    }
    private String TestDataFileLocation = "DRXMonitorTestData.txt";
    private String componentName = "CONTROL/DA41/DRXBBpr0";
}
