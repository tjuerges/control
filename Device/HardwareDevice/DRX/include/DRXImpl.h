#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif

#ifndef DRXImpl_h
#define DRXImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


// Base class
#include <DRXBase.h>
#include <DRXS.h>
#include <DRXExceptions.h>

class DRXImpl: public DRXBase, public virtual POA_Control::DRX
{
    public:
    /**
     * Constructor
     */
    DRXImpl(const ACE_CString& name, maci::ContainerServices* cs);

    /**
     * Destructor
     */
    virtual ~DRXImpl();

    /**
     * ACS Component Lifecycle Methods
     */
    /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
    virtual void initialize();


    protected:
    /**
     * Hardware Lifecycle Methods
     */
    virtual void hwInitializeAction();
    virtual void hwOperationalAction();
    virtual void hwStopAction();

    /// \exception DRXExceptions::ConfigClockEdgeExImpl
    virtual void configClockEdge();
};
#endif
