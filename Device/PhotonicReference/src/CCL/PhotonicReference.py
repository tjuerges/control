#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2011
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
# $Source$
#


import CCL.logging
import CCL.HardwareController
import CCL.CVR
import CCL.LS
import CCLExceptionsImpl
import Control
import string


class PhotonicReference(CCL.HardwareController.HardwareController):
    def __init__(self, photonicReference = None,
                 componentName = None, stickyFlag = False):
        '''
        The constructor creates a PhotonicReference object using default
        constructor.  You can specify which LS you would like either by number
        (1-6) or by PhotonicReference (i.e. PhotonicReference2)
        EXAMPLE:
        import CCL.PhotonicReference
        # To get the PhotonicReference associated with photonic reference 3
        pr = CCL.PhotonicReference.PhotonicReference(3)
        or 
        pr = CCL.PhotonicReference.PhotonicReference("PhotonicReference3")
        '''
        self.__logger = CCL.logging.getLogger()

        if componentName is None:
            if photonicReference is None:
                raise IllegalParameterErrorEx('You must specify either a '
                                              + ' photonic reference');
            if isinstance(photonicReference, int):
                photonicReference = 'PhotonicReference%d' % photonicReference

            componentName = "CONTROL/CentralLO/%s" % photonicReference

        name = componentName.split('/')[2]
        self._prNo = int(name[len(name) - 1])
        self._componentName = componentName

        try:
            CCL.HardwareController.HardwareController.__init__(self,
                componentName, stickyFlag)
        except Exception, e:
            print 'PhotonicReference component is not running \n' + str(e)

    def __del__(self):
        CCL.HardwareController.HardwareController.__del__(self)

    def status(self):
        '''
        Get the combined status, formed by asking the underlying
        controllers for their state and combining them.
    
        See ScriptImpl.CCLObject for the complete documentation of this
        method.
        '''
        devStates = {Control.HardwareController.Shutdown: 'Shutdown',
                     Control.HardwareController.Operational: 'Operational',
                     Control.HardwareController.Degraded: 'Degraded'}
        return devStates[self._HardwareController__controller.getState()]

    def setFrequency(self, frequency):
        '''
        Set LS LO reference frequency to target. This will also adjust CVR
        frequency and amplitude accordingly.
        The parameter frequency is in Hertz!
        '''
        self._HardwareController__controller.setFrequency(frequency)

    def getFrequency(self):
        '''
        Get the last set LS LO reference frequency.
        '''
        return self._HardwareController__controller.getFrequency()

    def queueFrequencies(self, frequencies, callback):
        '''
        Queue a set of the photonic reference frequency changes. After each
        frequency change is completed the report function of the provided
        callback is called. If the photonic frequency does not lock
        subsequent frequency changes are aborted.
        \exception ControlExceptions::INACTErrorEx if the CVR or laser
        synthesizer is not operational.
        \exception ControlExceptions::IllegalParameterErrorEx if any start
        time is in the past or any frequency is incorrect.
        '''
        return self._HardwareController__controller.queueFrequencies(
            frequencies, callback)

    def abortFrequencyChanges(self):
        '''
        Abandons all pending frequency changes and, if a frequency change
        is in progress, aborts it.
        '''
        return self._HardwareController__controller.abortFrequencyChanges()

    def timeToTune(self, frequencies):
        '''
        Estimate the time for the photonic reference to tune to the
        specified sequence of frequencies starting with the
        current state the system.
        \exception ControlExceptions::INACTErrorEx if the CVR or laser
        synthesizer is not operational.
        \exception ControlExceptions::IllegalParameterErrorEx if any
        frequency is incorrect.
        '''
        return self._HardwareController__controller.queueFrequencies(
            frequencies)
