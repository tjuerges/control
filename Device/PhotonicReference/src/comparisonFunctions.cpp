//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Mar 21, 2011  created
//


#include <typdefsPhotonicReference.h>
// For std::fabs.
#include <cmath>
// For epsilon().
#include <limits>


bool Control::compareStartTimes(Control::FrequencyPackage begin,
    Control::FrequencyPackage end)
{
    return (begin.startTime < end.startTime);
}

bool Control::compareFrequencyPackage(
    Control::FrequencyPackage start, Control::FrequencyPackage end)
{
    return ((std::fabs(start.lsFrequency - end.lsFrequency)
            < std::numeric_limits< double >::epsilon())
        && (std::fabs(start.cvrAmplitude - end.cvrAmplitude)
            < std::numeric_limits< double >::epsilon())
        && (start.cvrHarmonic == end.cvrHarmonic));
}
