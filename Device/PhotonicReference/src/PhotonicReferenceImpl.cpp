//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jul 1, 2009  created
//


#include <PhotonicReferenceImpl.h>
#include <algorithm>
// For std::string.
#include <string>
// For std::[i|o]stringstream.
#include <sstream>
// For retrieving the CVR and LS references.
#include <map>
// For the Control exceptions.
#include <ControlExceptions.h>
// For the ControlCommon exceptions.
#include <ControlCommonExceptions.h>
// For the LSCommon exceptions.
#include <LSCommonExceptions.h>
// For maciErrType exceptions.
#include <maciErrType.h>
// For audience logs, etc.
#include <loggingMACROS.h>
// For the container services.
#include <maciContainerServices.h>
// For maci::SmartPtr
#include <acsComponentSmartPtr.h>
// For the Control::ArrayMonitor.
#include <ControlArrayInterfacesC.h>
// For ACE_Time_Value
#include <Time_Value.h>
// For ACE_OS::select
#include <OS_NS_sys_select.h>
// For CVR and LS references.
#include <CVRC.h>
#include <LSCommonC.h>
#include <LORTMC.h>
// For ACS::Time and ACS::TimeIntervalSeq.
#include <acscommonC.h>
// For acscomponent::ACSComponentImpl::name()
#include <acscomponentImpl.h>
// For Control::ProgressCallbackImpl.
#include <progressCallbackImpl.h>
// For TETimeUtil::ACS_ONE_SECOND stuff.
#include <TETimeUtil.h>
// xmlTmcdbComponent
#include <xmlTmcdbComponent.h>
#include <configDataAccess.h>
// For currentFrequenciesMutex.
#include <Guard_T.h>
#include <Mutex.h>


Control::PhotonicReferenceImpl::PhotonicReferenceImpl(const ACE_CString& name,
    maci::ContainerServices* _cs):
    Control::HardwareControllerImpl(name, _cs),
    m_fullName(name.c_str()),
    cs(_cs),
    //MasterLaserFrequency(192668674807197.937500),
    m_lsFrequency(0.0),
    m_frequencyTolerance(1.0), // Hz
    m_LSoffsetFreq(-125e6), // Hz
    DefaultTeOffset(2ULL), // TEs
    queueingThread(0),
    oneSecond(static_cast< double >(TETimeUtil::ACS_ONE_SECOND)),
    oneTE(static_cast< double >(TETimeUtil::TE_PERIOD_ACS)
        / TETimeUtil::ACS_ONE_SECOND),
    lsSleepTime(static_cast< double >(TETimeUtil::TE_PERIOD_ACS) /
        TETimeUtil::ACS_ONE_SECOND),
    configData(0),
    numberOfSlaveLasers(0U),
    defaultHardwareLsMaxTuningTime(20ULL * TETimeUtil::ACS_ONE_SECOND),
    defaultSimulationLsMaxTuningTime(5ULL
        * (TETimeUtil::ACS_ONE_SECOND / 10ULL)),
    defaultLsMinTuningTime(5ULL
        * (TETimeUtil::ACS_ONE_SECOND / 10ULL))
{
    const size_t pos(m_fullName.find_last_of('/'));
    if(pos == std::string::npos)
    {
        m_referenceName = m_fullName;
    }
    else
    {
        m_referenceName = m_fullName.substr(pos + 1);
    }

    std::istringstream noStr(m_fullName.substr(m_fullName.length() - 1));
    noStr >> m_prNo;
    LOG_TO_DEVELOPER(LM_DEBUG, std::string("PhotonicReference number string: ")
        + noStr.str());
}

Control::PhotonicReferenceImpl::~PhotonicReferenceImpl()
{
}

void Control::PhotonicReferenceImpl::execute()
{

    // Get the schema and the instance file for this device
    XmlTmcdbComponent tmcdb(getContainerServices());

    const std::string xsd(tmcdb.getConfigXsd("photonicReference"));
    const std::string xml(tmcdb.getConfigXml("photonicReference"));

    if(xml.empty() == true)
    {
        // no configuration available for selected device
        // log a warning and return

        LOG_TO_OPERATOR(LM_WARNING, "Could not access a config data file for "
            "ANY PhotonicReference.");
    }
    else if(xsd.empty() == true)
    {
        // no schema available for selected device
        // log a warning and return
        LOG_TO_OPERATOR(LM_WARNING, "Could not access the schema file "
            "\"photonicReference.xsd\" in the $TMCDB_DATA directory.");
    }
    else
    {
        try
        {
            configData = new ConfigDataAccessor(xml, xsd);
            list = configData->getElements("LSnumber");
        }
        catch(...)
        {
            LOG_TO_OPERATOR(LM_WARNING, "Could not access the TMCDB data for "
                "the LS hardware.");
        }
    }

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    const std::string myName(tmpName.in());
    Control::PhotonicReferenceImpl& selfPtr(*this);
    queueingThread = getContainerServices()->getThreadManager()->
        create< Control::QueueingThread, Control::PhotonicReferenceImpl >(
            std::string(myName + "QueueingThread").c_str(), selfPtr);

    if(queueingThread != 0)
    {
        queueingThread->resume();
    }
}

void Control::PhotonicReferenceImpl::cleanUp()
{
    // Abort any currently running tuning.  The call will check itself if
    // there is a thread object.
    abortFrequencyChanges();
    // At least try to give the thread time to wrap it up.
    sleep(0.5);
    // Make it go away.
    delete queueingThread;
    queueingThread = 0;

    Control::HardwareControllerImpl::cleanUp();

    delete configData;
    configData = 0;
}

bool Control::PhotonicReferenceImpl::isAssigned()
{
    return (m_assignedArray_sp.isNil() == false);
}

void Control::PhotonicReferenceImpl::assign(const char* arrayName)
{
    if(currentState_m == Control::HardwareController::Shutdown)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "This Photonic Reference component is inactive.  "
            "Did you forget to call controllerOperational()?");
        ex.log();
        throw ex.getINACTErrorEx();
    }

    // Arrays are named 'Array001', 'Array002', ...; on the other hand
    // the respective array *component* names are 'CONTROL/Array001',
    // 'CONTROL/Array002', etc. Adding the prefix to the arrayName
    // parameter to get the array component name.
    std::string arrayCompName = "CONTROL/";
    arrayCompName += arrayName;

    // Get a persistent non-sticky reference to the ArrayMonitor
    // to which we have just been assigned.
    // Note: We are using the array monitor here rather than the array
    // command because array command is not a component.  We
    // need to refactor these parent classes a bit.
    try
    {
        m_assignedArray_sp = getContainerServices()->
            getComponentNonStickySmartPtr< Control::ArrayMonitor >(
                arrayCompName.c_str());
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        std::ostringstream msg;
        msg << "Unable to assign "
            << m_referenceName
            << " to array "
            << arrayCompName
            << ".  It appears the array does not exist!";

        nex.addData("Detail", msg.str());
        nex.log();
        throw nex.getCannotGetComponentEx();
    }

    std::ostringstream msg;
    msg << "Photonic Reference "
        << m_referenceName
        << "assigned to array "
        << arrayName;
    LOG_TO_OPERATOR(LM_INFO, msg.str());
}

void Control::PhotonicReferenceImpl::unassign()
{
    if(currentState_m == Control::HardwareController::Shutdown)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "This Photonic Reference component is inactive.  "
            "Did you forget to call controllerOperational()?");
        ex.log();
        throw ex.getINACTErrorEx();
    }

    // Release the parent reference on the array
    m_assignedArray_sp.release();
}

char* Control::PhotonicReferenceImpl::getArray()
{
    if(currentState_m == Control::HardwareController::Shutdown)
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "This Photonic Reference component is inactive.  "
            "Did you forget to call controllerOperational()?");
        ex.log();
        throw ex.getINACTErrorEx();
    }

    if(isAssigned() == false)
    {
        // The antenna is completely unassigned
        CORBA::String_var returnValue(CORBA::string_dup("Unassigned"));
        return returnValue._retn();
    }

    CORBA::String_var returnValue(CORBA::string_dup(
        m_assignedArray_sp->getArrayName()));
    return returnValue._retn();
}

CORBA::Double Control::PhotonicReferenceImpl::getFrequency()
{
    return m_lsFrequency;
}

bool Control::PhotonicReferenceImpl::getComponentReferences(
    Control::CVR_ptr& _cvr, Control::LSCommon_ptr& _ls)
{
    // First get the CVR and the LS references.
    // CVR is first.
    std::string lruType("CVR");
    _cvr = getSubdeviceReference< Control::CVR >(lruType);

    // Now get the LS reference
    // (using LSCommon as common base instance with all required methods)
    lruType = "LS";
    _ls = getSubdeviceReference< Control::LSCommon >(lruType);

    // See if LS is an LORTM (needed for phaseLock, step 1)
    Control::LORTM_var lortm(Control::LORTM::_nil());
    lortm = Control::LORTM::_narrow(_ls);

    return (CORBA::is_nil(lortm.in()) == false);
}

void Control::PhotonicReferenceImpl::setFrequency(
    CORBA::Double targetFrequency)
{
    try
    {
        Control::PhotonicReference::SubscanInformationSeq_var frequencies(
            new Control::PhotonicReference::SubscanInformationSeq);
        frequencies->length(1);
        frequencies[0].startTime = 0ULL;
        frequencies[0].frequency = targetFrequency;

        Control::ProgressCallbackImpl callback(cs);
        try
        {
            queueFrequencies(frequencies, callback.getExternalReference());
        }
        catch(const ControlExceptions::INACTErrorEx& ex)
        {
            ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);

            throw nex;
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);

            throw nex;
        }

        try
        {
            callback.waitForCompletion(20.0);
        }
        catch(const ControlCommonExceptions::OSErrorEx ex)
        {
            // Tell the thread to abort the locking procedure.  I cannot simply
            // call abortFrequencyChanges or queueingThread->setAbortFlag(1U)
            // and then be good with it and leave.  Both trigger the sending
            // of a completion via the callback object which has been allocated
            // here on the stack and which will be long gone when the thread
            // finally sends it.  Therefore wait for another round for the
            // completion, but this time it will be the abort completion.
            queueingThread->setAbortFlag(1U);
            callback.waitForCompletion(1.0);

            LSCommonExceptions::TimeOutExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);

            std::ostringstream msg;
            msg << "An OS exception has been thrown:  "
                << nex.getData("Detail")
                << "  This means that the requested frequency of "
                << std::fixed
                << setprecision(9)
                << targetFrequency / 1e9
                << "[GHz] has not been set.";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
            nex.addData("Detail", msg.str());

            throw nex;
        }
        catch(const ControlExceptions::TimeoutEx ex)
        {
            // Tell the thread to abort the locking procedure.  I cannot simply
            // call abortFrequencyChanges or queueingThread->setAbortFlag(1U)
            // and then be good with it and leave.  Both trigger the sending
            // of a completion via the callback object which has been allocated
            // here on the stack and which will be long gone when the thread
            // finally sends it.  Therefore wait for another round for the
            // completion, but this time it will be the abort completion.
            queueingThread->setAbortFlag(1U);
            callback.waitForCompletion(1.0);

            LSCommonExceptions::TimeOutExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);

            std::ostringstream msg;
            msg << "A time out happened:  "
                << nex.getData("Detail")
                << "  This means that the requested frequency of "
                << std::fixed
                << setprecision(9)
                << targetFrequency / 1e9
                << "[GHz] has not been set.";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
            nex.addData("Detail", msg.str());

            throw nex;
        }
    }
    catch(ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string msg("INACTError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);

        throw nex.getINACTErrorEx();
    }
    catch(LSCommonExceptions::TimeOutExImpl& ex)
    {
        LSCommonExceptions::TimeOutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string msg("Timeout Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);

        throw nex.getTimeOutEx();
    }
    catch(ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);

        const std::string msg("IllegalParameterError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);

        throw nex.getIllegalParameterErrorEx();
    }
    catch(ControlExceptions::XmlParserErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);

        const std::string msg("XmlParserError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);

        throw nex.getIllegalParameterErrorEx();
    }
    catch(std::exception& ex)
    {
        ControlCommonExceptions::LockErrorExImpl nex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string msg(std::string("Exception caught: ") + ex.what());
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);
        nex.log();

        throw nex.getLockErrorEx();
    }
    catch(...)
    {
        ControlCommonExceptions::LockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string msg("Unexpected exception caught: No further detail "
            "is available.");
        LOG_TO_OPERATOR(LM_ERROR, msg);
        ex.addData("Detail", msg);
        ex.log();

        throw ex.getLockErrorEx();
    }
}

void Control::PhotonicReferenceImpl::setCurrentFrequency(
    const Control::FrequencyQueue::iterator& iter)
{
    // Work on a copy of the currentFrequencies member.
    Control::FrequencyQueue tmpCurrentFrequencies(getCurrentFrequencies());

    // Check if the new frequency had already been set in the past.  Then
    // do not modify the history.
    if(std::search(tmpCurrentFrequencies.begin(), tmpCurrentFrequencies.end(),
        iter, iter + 1, Control::compareFrequencyPackage)
    == tmpCurrentFrequencies.end())
    {
        // The new frequency is not in the history, hence add it to the
        // history.

        // Make sure that the history contains only numberOfSlaveLasers - 1
        // elements.
        while(tmpCurrentFrequencies.size() >= numberOfSlaveLasers)
        {
            tmpCurrentFrequencies.pop_front();
        };

        // Add the current frequency to the list of pretuned slave laser
        // frequencies.
        tmpCurrentFrequencies.push_back(*iter);

        // Now write the new deque of current frequencies back to the official one.
        currentFrequenciesMutex.acquire();
        currentFrequencies = tmpCurrentFrequencies;
        currentFrequenciesMutex.release();
    }
}

Control::FrequencyQueue
    Control::PhotonicReferenceImpl::getCurrentFrequencies()
{
    currentFrequenciesMutex.acquire();
    const Control::FrequencyQueue ret(currentFrequencies);
    currentFrequenciesMutex.release();

    return ret;
}

ACS::TimeIntervalSeq* Control::PhotonicReferenceImpl::timeToTune(
    const Control::DoubleSeq& _frequencies)
{
    const unsigned int size(_frequencies.length());
    Control::FrequencyPackage package;
    Control::FrequencyQueue frequencies(size);
    for(unsigned int i(0U); i < size; ++i)
    {
        std::memset(&package, 0, sizeof(Control::FrequencyPackage));
        package.lsFrequency = _frequencies[i];
        frequencies.at(i) = package;
    }

    // Operate on a copy of the current frequencies deque.
    Control::FrequencyQueue tmpCurrentFrequencies(getCurrentFrequencies());

    // Use the current LS S/N for looking up the frequency settings.
    CORBA::String_var sn;
    // Check if the LS is in simulation mode.
    bool isSimulated(false);
    // When there is no frequency in the deque, obviously there is no LS
    // reference, too.  Then I have to get the reference "manually".  This can
    // happen if timeToTune is executed before queueFrequencies is executed for
    // the first time and successfully locked.  The successful locking will add
    // the target frequency to the deque, hence it won't be empty anymore.
    if(tmpCurrentFrequencies.empty() == true)
    {
        Control::CVR_var cvr(Control::CVR::_nil());
        Control::LSCommon_var ls(Control::LSCommon::_nil());
        getComponentReferences(cvr.out(), ls.out());
        if((CORBA::is_nil(cvr) == true)
        || (CORBA::is_nil(ls) == true))
        {
            ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            ex.addData("Detail", "CVR reference or LS reference is nil!");
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }

        sn = ls->getSerialNumberStr();
        isSimulated = ls->isSimulated();
    }
    else
    {
        // The deque is not empty, therefore the first element has an LS
        // reference, too.
    Control::LSCommon_ptr _ls = getSubdeviceReference< Control::LSCommon >("LS");
        sn = _ls->getSerialNumberStr();
        isSimulated = _ls->isSimulated();
//        sn = (*(tmpCurrentFrequencies.begin())).ls->getSerialNumberStr();
//        isSimulated = (*(tmpCurrentFrequencies.begin())).ls->isSimulated();
    }

    // Look up the target frequencies.
    unsigned short numberOfSlaveLasers(0U);
    try
    {
        numberOfSlaveLasers = lookupForQueueing(sn.in(), frequencies);
    }
    catch(ControlExceptions::XmlParserErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);

        const std::string msg("XmlParserError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);

        throw nex.getIllegalParameterErrorEx();
    }
    catch(ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);

        const std::string msg("IllegalParameterError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, msg);
        nex.addData("Detail", msg);

        throw nex.getIllegalParameterErrorEx();
    }

    // Allocate up the returned sequence.
    ACS::TimeIntervalSeq_var ret(new ACS::TimeIntervalSeq);
    ret->length(size);

    // Determine if one of the slave lasers has already been tuned to the
    // target frequency.
    unsigned short index(0U);
    ACS::TimeInterval time(0ULL);
    Control::FrequencyQueue::iterator found(tmpCurrentFrequencies.end());
    for(Control::FrequencyQueue::iterator iter(frequencies.begin());
        iter != frequencies.end(); ++iter, ++index)
    {
        if(tmpCurrentFrequencies.empty() == false)
        {
            found = std::search(tmpCurrentFrequencies.begin(),
                tmpCurrentFrequencies.end(), iter, iter + 1,
                Control::compareFrequencyPackage);
            if(found != tmpCurrentFrequencies.end())
            {
                // Did not find the wanted frequency, add it to the list of
                // current frequencies.  Locking of this one will take max.
                // timeToLockUntunedSlaveLaser.
                time = (*found).timeToLockPretunedSlaveLaser;
            }
            else
            {
                // Did not find the wanted frequency, add it to the list of
                // current frequencies.  Locking of this one will take max.
                // timeToLockUntunedSlaveLaser.
                time = (*tmpCurrentFrequencies.begin()).
                    timeToLockUntunedSlaveLaser;
            }

            if(tmpCurrentFrequencies.size() == numberOfSlaveLasers)
            {
                // The deque is not empty and contains already
                // numberOfSlaveLasers elements.  Therefore remove the
                // first element before adding the next one.
                tmpCurrentFrequencies.pop_front();
            }
        }
        else
        {
            // The deque is empty, therefore use the standard value, which
            // depends on the LS type and whether the.conponent is in
            // simulation mode.
            if(isSimulated == true)
            {
                time = (*iter).timeToLockUntunedSlaveLaser =
                    defaultSimulationLsMaxTuningTime;
            }
            else
            {
                time = (*iter).timeToLockUntunedSlaveLaser =
                    defaultHardwareLsMaxTuningTime;
            }

            (*iter).timeToLockPretunedSlaveLaser =
                defaultLsMinTuningTime;
        }

        ret[index] = time;

        tmpCurrentFrequencies.push_back(*iter);
    }

    return ret._retn();
}

// Helper method
void Control::PhotonicReferenceImpl::sleep(double seconds)
{
    ACE_Time_Value sleepTime;
    sleepTime.set(seconds);
    ACE_OS::select(0,0,0,0, sleepTime);
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::PhotonicReferenceImpl)
