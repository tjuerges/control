//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Feb 10, 2011  created
//


#include <queueingThread.h>
// For std::deque
#include <deque>
// For std::make_pair
#include <utility>
// For std::string.
#include <string>
// For std::[i|o]stringstream.
#include <sstream>
// For Control::FrequencyQueue.
#include <typdefsPhotonicReference.h>
// For ACS::Thread
#include <acsThread.h>
// For the PR reference.
#include <PhotonicReferenceImpl.h>
// For the Control exceptions.
#include <ControlExceptions.h>
// For the ControlCommonCallbacks.
#include <ControlCommonCallbacksC.h>
// For the ControlCommon exceptions.
#include <ControlCommonExceptions.h>
// For the LSCommon exceptions.
#include <LSCommonExceptions.h>
// For maciErrType exceptions.
#include <maciErrType.h>
// For audience logs, etc.
#include <loggingMACROS.h>
// For ::getTimeStamp().
#include <acsutilTimeStamp.h>
// For errno.
#include <cerrno>
// For std::fabs.
#include <cmath>

#include <LORTMC.h>
#include<TETimeUtil.h>

Control::QueueingThread::QueueingThread(const ACE_CString& name,
    Control::PhotonicReferenceImpl& pr):
    ACS::Thread(name),
    photonicReference(pr),
    abortFlag(0U)
{
    // Take the mutex.  This ensures that the run loop is waiting for the
    // photonic reference to call go.
    mutex.acquire();
}

/// Destructor.
Control::QueueingThread::~QueueingThread()
{
    // Just to be defensive, release the mutex and let the thread, if it is
    // still running, stop.
    setAbortFlag(1U);
    exit();
    mutex.release();
}

inline unsigned int Control::QueueingThread::readAbortFlag()
{
    return __sync_fetch_and_add(&abortFlag, 0U);
}

void Control::QueueingThread::setAbortFlag(unsigned int value)
{
    unsigned int foo __attribute__((unused)) = __sync_val_compare_and_swap(
        &abortFlag, abortFlag, value);
}

template< typename ValueType, typename DeviceType, typename MethodPtr >
bool Control::QueueingThread::checkMonitorPoint(ValueType targetValue,
    double timeout, DeviceType device, MethodPtr ptr, ValueType& value)
{
    ACS::Time timestamp(0ULL);
    bool timedOut(false);
    double diff(0.0);

    ACS::Time startTime(::getTimeStamp());
    do
    {
        // Reading a monitor point twice forces an update of the parent of the
        // dependent monitor point.
        ((device)->*(ptr))(timestamp);
        value = ((device)->*(ptr))(timestamp);

        diff = (::getTimeStamp() - startTime) / photonicReference.oneSecond;

        if(value == targetValue)
        {
            break;
        }
        else if(diff >= timeout)
        {
            // Time out!
            timedOut = true;
            break;
        }

        photonicReference.sleep(photonicReference.oneTE);
    }
    while((readAbortFlag() == 0U)
    && (exitRequested() == false)
    && (value != targetValue));

    std::ostringstream msg;
    msg << std::fixed
        << setprecision(3)
        << "Waited for target value = "
        << targetValue
        << ", current value = "
        << value
        << ", time out = "
        << timeout
        << "[s], elapsed time = "
        << diff
        << "[s].";
    LOG_TO_DEVELOPER(LM_INFO, msg.str());

    return timedOut;
}

template< typename CompletionType >
void Control::QueueingThread::sendCompletion(
    const Control::ProgressCallback_var& callback,
    const CompletionType& completion)
{
    if((CORBA::is_nil(photonicReference.callback_m) == false)
    &&(photonicReference.callback_m->_non_existent() == false))
    {
        try
        {
            photonicReference.callback_m->report(completion);
        }
        catch(...)
        {
            STATIC_LOG_TO_OPERATOR(LM_ERROR, "Could not report the status of "
                "the current LS locking process.");
            STATIC_LOG_TO_DEVELOPER(LM_ERROR, "The callback->report call "
                "threw an exception.");
        }
    }
    else
    {
        STATIC_LOG_TO_OPERATOR(LM_ERROR, "Could not report the status of "
            "the current LS locking process.");
        STATIC_LOG_TO_DEVELOPER(LM_ERROR, "The callback pointer is nil!");
    }
}

void Control::QueueingThread::abortAndSendCompletion(
    const Control::ProgressCallback_var& callback, const std::string& file,
    unsigned int line, const std::string& method)
{
    ControlCommonExceptions::LockErrorCompletion completion(file.c_str(), line,
        method.c_str());
    completion.addData("Detail", "Locking aborted.");
    completion.log();

    sendCompletion(callback, completion);

    exit();
    STATIC_LOG_TO_DEVELOPER(LM_INFO, "Abort request received, leaving.");
}

void Control::QueueingThread::go(const Control::FrequencyQueue& _frequencies)
{
    updateMutex.acquire();
    if(frequencies.empty() == true)
    {
        frequencies = _frequencies;
    }
    else
    {
        frequencies.insert(frequencies.end(), _frequencies.begin(),
            _frequencies.end());
    }
    updateMutex.release();

    // Print out the content of the received frequency package.
    std::ostringstream msg;
    for(Control::FrequencyQueue::const_iterator iter(_frequencies.begin());
        iter != _frequencies.end(); ++iter)
    {
        msg << "\nTime stamp = "
            << (*iter).startTime
            << "[100ns], "
            << (*iter).lsFrequency / 1e9
            << "[GHz], CVR harmonic = "
            << (*iter).cvrHarmonic
            << ", CVR amplitude = "
            << (*iter).cvrAmplitude
            << "[dBm].";
    }
    LOG_TO_DEVELOPER(LM_INFO, msg.str());

    // Before actually starting anything, check if the thread had previously
    // been aborted.  Only then let ACS restart the thread.

    // The thread had exited before, hence I have to restart it.
    if(check() == false)
    {
        // Clear the abort flag.
        setAbortFlag(0U);

        // Let ACS restart the thread.
        restart();
    }

    // Release the mutex on which the thread waits.
    mutex.release();
}

void Control::QueueingThread::run()
{
    bool quickTuning(false);
    ACE_Time_Value timeout(0);
    int mutexTimedOut(0);
    Control::FrequencyQueue tempQueue;
    Control::FrequencyQueue::iterator iter(frequencies.end());
    std::ostringstream msg;
    Control::CVR_ptr  _cvr = photonicReference.getSubdeviceReference< Control::CVR >("CVR");
    Control::LSCommon_ptr _ls = photonicReference.getSubdeviceReference< Control::LSCommon >("LS");
    Control::ProgressCallback_var _callback = photonicReference.callback_m;
    Control::LORTM_var lortm(Control::LORTM::_nil());
    lortm = Control::LORTM::_narrow(_ls);
    bool isLortm = false;
    if(CORBA::is_nil(lortm.in())==false) {
        isLortm = true;
    }


    // Exit the lock thread if the abort flag has been set or if the
    // thread has been marked as stopped.
    while(exitRequested() == false)
    {
        // Wait until timeout (= now + 1 TE) for the mutex acquisition.
        timeout.set(photonicReference.oneTE);
        timeout += ACE_OS::gettimeofday();
        mutexTimedOut = mutex.acquire(timeout);

        if((mutexTimedOut == -1) && (errno == ETIME))
        {
            // The mutex timed out, just continue with the loop if the abort
            // flag has not been set.
            // Check the abort flag often!
            if(readAbortFlag() != 0U)
            {
                // There is no completion to send yet.
                exit();
            }

            continue;
        }

        // Got the mutex, start doing stuff.  I keep running as long as there
        // is data in the frequencies deque.

        // This is reached only once per thread start.  Copy the frequency
        // deque once, then only update it from within the loop below as long
        // as new frequency packages are available.
        updateMutex.acquire();
        Control::FrequencyQueue frequencyQueue(
            frequencies);
        frequencies.clear();
        updateMutex.release();

        while(frequencyQueue.empty() == false)
        {
            // Always work on a copy of everything.  This allows for an
            // update of the references at runtime of the run method.  If an
            // update is available, frequencies will be non-empty.
            updateMutex.acquire();
            if(frequencies.empty() == false)
            {
                // Just swap the current contents of frequency with the
                // temporary empty object.  This is, according to Meyers in
                // "Effective STL" the fastest way to copy data from one STL
                // container to another of the same type.
                // tempQueue is guaranteed to be empty() == true.
                tempQueue.swap(frequencies);
                // Now frequencies is empty() == true and tempQueue is not.
            }
            updateMutex.release();

            if(tempQueue.empty() == false)
            {
                // Copy data from tempQueue to the end of the frequencyQueue.
                frequencyQueue.insert(frequencyQueue.end(),
                    tempQueue.begin(), tempQueue.end());
                // Everything has been copied, clear the tempQueue for the next
                // iteration.
                tempQueue.clear();
            }

            // Get an iterator which represents the first element in the deque
            // of frequency packages.
            iter = frequencyQueue.begin();
            //(*iter).cvr = _cvr;
            //(*iter).ls = _ls;
            (*iter).isLORTM = isLortm;
                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
            // Remove it from the deque.
            frequencyQueue.pop_front();

            // Do the locking.
            try
            {
                // Check if one of the slave lasers is already tuned close
                // to the target frequency.  If one of the slave lasers
                // had already been pretuned, wake up at the proper target
                // time - timeToLockPretunedSlaveLaser to have the LS
                // locked at the target time.  If none of the slave lasers
                // had been pretuned yet, wake up at target
                // time - timeToLockUntunedSlaveLaser to give the tuning
                // procedure enough time.  This is obviously done on a
                // best effort base because there is not much that can be
                // done when the target time for a previously untuned
                // frequency is less than timeToLockUntunedSlaveLaser.  In
                // that case the target time will be missed.  This should
                // have been dealt with accordingly by the client when it
                // received the tuning times from timeToTune.
                const ACS::Time now(::getTimeStamp());
                long long waitTime((*iter).startTime - now);

                // Copy the currentFrequencies deque and try to find the
                // current frequency in the list of already tuned frequencies.
                // If a match is found, quickTuning will be performed.
                const Control::FrequencyQueue tmpCurrentFrequencies(
                    photonicReference.getCurrentFrequencies());
                if(std::search(tmpCurrentFrequencies.begin(),
                    tmpCurrentFrequencies.end(), iter, iter + 1,
                    Control::compareFrequencyPackage)
                != tmpCurrentFrequencies.end())
                {
                    quickTuning = true;
                }
                else
                {
                    quickTuning = false;
                }

                msg.str("");
                msg << "Will sleep for "
                    << static_cast< double >(waitTime)
                        / photonicReference.oneSecond
                    << "[s] until the LS is tuned to the next frequency.  "
                        "startTime = "
                    << (*iter).startTime
                    << "[100ns], now = "
                    << now
                    << "[100ns].";
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());

                if((waitTime > 0LL) && (now < (*iter).startTime))
                {
                    ACE_Time_Value queueingWakeUpTime;
                    queueingWakeUpTime.set(static_cast< double >(waitTime)
                        / photonicReference.oneSecond);
                    ACE_OS::select(0, 0, 0, 0, queueingWakeUpTime);

                    msg.str("");
                    msg << "Woke up: "
                        << ::getTimeStamp()
                        << "[100ns].\n";
                    STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                }
                else
                {
                    ///
                    /// TODO
                    /// Thomas, Feb 23, 2011
                    /// We are supposed to sleep for a negative time.  This
                    /// cannot be and therefore this frequency is ignored.
                    /// What better could I do?
                    msg.str("");
                    msg << "Received a request for which the start time "
                        "(target time - time allowed for LS tuning) is "
                        "already in the past: startTime = "
                        << (*iter).startTime
                        << "[100ns], startTime - tuning time = "
                        << (*iter).startTime - waitTime
                        << "[100ns], now = "
                        << now
                        << "[100ns], frequency = "
                        << (*iter).lsFrequency / 1e9
                        << "[GHz].  This request will be ignored.";

                    ControlCommonExceptions::LockErrorCompletion completion(
                        __FILE__, __LINE__, __PRETTY_FUNCTION__);
                    completion.addData("Detail", msg.str());
                    completion.log();
                    sendCompletion(_callback, completion);
                    continue;
                }

                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }

                // LS PHASE LOCK PROCESS, ref:
                // Production Laser Synthesizer ICD 2009-05-14, Appendix A,
                // page 29. Additional ref: email from xx on yy modifying
                // standby procedure.
                // Assume same procedure for LORTM.
                // Note that all dependent monitor points must be read twice to
                // ensure that an up-to-date value is returned.  We are caching
                // monitor points and only allow updates of dependent MPs every
                // 2s (as of 2009-11-04 ). Reading the same MP twice in a row
                // forces an update and allows reading the real value.
                msg.str("");
                msg << "Starting PHASELOCK process for "
                    << photonicReference.m_referenceName
                    << " at "
                    << std::fixed
                    << std::setprecision(9)
                    << (*iter).lsFrequency * 1e-9
                    << "[GHz].";
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                ACS::Time ts(0ULL);
                // Step 0: check if we are already locked at the requested
                // frequency.
                _ls = photonicReference.getSubdeviceReference< Control::LSCommon >("LS");
                if (CORBA::is_nil(_ls)) {
                STATIC_LOG_TO_DEVELOPER(LM_INFO, "ISNILLLLLL");
exit();
}
                _ls->GET_PHASELOCK_GET_STATUS_LOCKED(ts);
                bool islocked(_ls->GET_PHASELOCK_GET_STATUS_LOCKED(ts));
                double deltaFreq(
                    photonicReference.m_lsFrequency - (*iter).lsFrequency);
                if((islocked == true)
                && (std::fabs(deltaFreq)
                    < photonicReference.m_frequencyTolerance))
                {
                    // Requested target frequency is the same as the current
                    // frequency or it lies within the tolerance and the LS is
                    // locked.
                    msg.str("");
                    msg << "LS is already locked at "
                        << std::fixed
                        << setprecision(9)
                        << photonicReference.m_lsFrequency * 1e-9
                        << "[GHz], no need to relock.  New frequency = "
                        << (*iter).lsFrequency * 1e-9
                        << "[GHz], tolerance = "
                        << setprecision(6)
                        << photonicReference.m_frequencyTolerance * 1e-6
                        << "[MHz], difference = "
                        << (photonicReference.m_lsFrequency
                            - (*iter).lsFrequency) * 1e-6
                        << "[MHz], CVR harmonic = "
                        << (*iter).cvrHarmonic
                        << ", CVR amplitude = "
                        << (*iter).cvrAmplitude
                        << "[dBm].";
                    // Report back that the locking was successful.
                    ACSErrTypeOK::ACSErrOKCompletion completion;
                    completion.addData("Detail", msg.str());
                    sendCompletion(_callback, completion);

                    STATIC_LOG_TO_OPERATOR(LM_INFO, msg.str());
                    continue;
                }

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                // Clear all pending errors.
                _ls->SET_SYSTEM_CLEAR_ERRORS();
                photonicReference.m_lsFrequency = 0.0;

                // Step 1. Set the LS to Standby state if it is not already
                // Operational or Standby state. Always set the LORTM to
                // Standby.

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                CORBA::ULong state(99U);
                if((*iter).isLORTM == false)
                {
                    _ls->GET_SYSTEM_GET_STATUS_SYSTEM_STATE(ts);
                    state = _ls->GET_SYSTEM_GET_STATUS_SYSTEM_STATE(ts);
                }

                #define STANDBY_STATE 2U
                #define PHASE_LOCKING 3U
                #define OPERATIONAL_STATE 4U

                bool operationalOrStandby(
                    (state == OPERATIONAL_STATE) || (state == STANDBY_STATE));
                bool sendStandby(
                    (*iter).isLORTM || (operationalOrStandby == false));

                msg.str("");
                if((*iter).isLORTM == true)
                {
                    msg << "Setting to Standby because LS is an LORTM";
                }
                else
                {
                    switch(state)
                    {
                        case OPERATIONAL_STATE:
                        {
                            msg << "System in Operational state, not setting "
                                "to Standby";
                        }
                        break;

                        case STANDBY_STATE:
                        {
                            msg << "System already in Standby state, not "
                                "setting to Standby";
                        }
                        break;

                        default:
                        {
                            msg << "Before setting to Standby, state = "
                                << state;
                        }
                    }
                }

                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                bool timedOut(false);
                if(sendStandby == true)
                {
                    STATIC_LOG_TO_DEVELOPER(LM_INFO, "Send "
                        "SET_SYSTEM_STANDBY_MODE_REQUEST");

                    _ls->SET_SYSTEM_STANDBY_MODE_REQUEST();

                    timedOut = checkMonitorPoint(
                        CORBA::ULong(STANDBY_STATE), 10.0, _ls,
                        &Control::LSCommon::GET_SYSTEM_GET_STATUS_SYSTEM_STATE,
                        state);

                    // Check the abort flag often!
                    if(readAbortFlag() != 0U)
                    {
                        abortAndSendCompletion(_callback, __FILE__,
                            __LINE__, __PRETTY_FUNCTION__);
                        break;
                    }
                    // Check for timeout
                    else if(timedOut == true)
                    {
                        LSCommonExceptions::TimeOutExImpl ex(__FILE__,
                            __LINE__, __PRETTY_FUNCTION__);

                        const std::string out("Timeout going into STANDBY "
                            "mode, error code = LSCommonExceptions::TimeOut.");
                        STATIC_LOG_TO_OPERATOR(LM_ERROR, out);
                        ex.addData("Detail", out);
                        throw ex;
                    }
                }

                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }

                // Step 2: Send PHASELOCK_COMMAND_TUNING_INIT command.
                // Calculate the slave laser frequency.
                const double refLaserFreq(
                    _ls->GET_PHASELOCK_REF_LASER_FREQUENCY(ts));
                const double tuningFreq(refLaserFreq - (*iter).lsFrequency);

                msg.str("");
                msg << "Send PHASELOCK_COMMAND_TUNING_INIT: reference laser "
                    "frequency = "
                    << std::fixed
                    << setprecision(9)
                    << refLaserFreq * 1.0e-9
                    << "[GHz], tuning frequency = "
                    << tuningFreq * 1.0e-12
                    << "[THz].";
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());

                _ls->SET_PHASELOCK_COMMAND_TUNING_INIT(tuningFreq);

                // Step 3: Wait for "Tuning Ready" in PHASELOCK_GET_STATUS
                STATIC_LOG_TO_DEVELOPER(LM_INFO, "Waiting for PHASELOCK "
                    "TUNING_READY");

                bool tuningReady(false);
                timedOut = checkMonitorPoint(bool(true), 30.0, _ls,
                    &Control::LSCommon::GET_PHASELOCK_GET_STATUS_TUNING_READY,
                    tuningReady);

                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }
                // Check for timeout
                else if(timedOut == true)
                {
                    LSCommonExceptions::TimeOutExImpl ex(__FILE__, __LINE__,
                        __PRETTY_FUNCTION__);

                    const std::string out("Timeout waiting for "
                        "PHASELOCK STATUS TUNING_READY after "
                        "PHASELOCK_COMMAND_TUNING_INIT, error code = "
                        "LSCommonExceptions::TimeOut.");
                    STATIC_LOG_TO_OPERATOR(LM_ERROR, out);
                    ex.addData("Detail", out);
                    throw ex;
                }

                // Step 4: Send PHASELOCK_COMMAND_TUNING_UNLOCK command.
                STATIC_LOG_TO_DEVELOPER(LM_INFO, "Send TUNING_UNLOCK");

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                _ls->SET_PHASELOCK_COMMAND_TUNING_UNLOCK();

                // Step 5 Wait for "RF input Ready" in PHASELOCK_GET_STATUS
                bool rfInputReady(false);
                timedOut = checkMonitorPoint(bool(true), 30.0, _ls,
                    &Control::LSCommon::GET_PHASELOCK_GET_STATUS_RF_INPUT_READY,
                    rfInputReady);

                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }
                // Check for timeout
                else if(timedOut == true)
                {
                    LSCommonExceptions::TimeOutExImpl ex(__FILE__, __LINE__,
                        __PRETTY_FUNCTION__);

                    const std::string out("Timeout on RF input ready after "
                        "PHASELOCK UNLOCK, error code = "
                        "LSCommonExceptions::TimeOut.");
                    STATIC_LOG_TO_OPERATOR(LM_ERROR, out);
                    ex.addData("Detail", out);
                    throw ex;
                }

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                // Step 6:  Set the CVR frequency and amplitude.
                (*iter).cvrFrequency = (
                    (*iter).lsFrequency - photonicReference.m_LSoffsetFreq)
                        / (*iter).cvrHarmonic;

                _cvr->SET_FREQUENCY((*iter).cvrFrequency);
                _cvr->SET_AMPLITUDE((*iter).cvrAmplitude);

                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }

                msg.str("");
                msg << "Set CVR frequency = "
                    << setprecision(9)
                    <<  (*iter).cvrFrequency * 1e-9
                    << "[GHz], harmonic = "
                    << (*iter).cvrHarmonic
                    << ", amplitude = "
                    << setprecision(1)
                    << (*iter).cvrAmplitude
                    << "[dBm].";
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());

                // Wait for cvr to stabilize. This takes as much as 11ms
                // Ref: AIV-1325, Bill Shillue comment
                photonicReference.sleep(0.011);

                // Step 7: Send PHASELOCK_COMMAND_TUNING_FINALIZE command.

                // This wait must be long or the simulators will error out;
                // don't know if the real hardware requires it. A sleep of 1.5s
                // will cause problems, but 2.0 seconds seems to work.
                // A long wait should not be a problem here because locking
                // with the real hardware takes more than 10 seconds, unless
                // fast switching.
                STATIC_LOG_TO_DEVELOPER(LM_INFO, "Sending "
                    "PHASELOCK_COMMAND_TUNING_FINALIZE");

                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                _ls->SET_PHASELOCK_COMMAND_TUNING_FINALIZE();

                timedOut = checkMonitorPoint(
                    CORBA::ULong(OPERATIONAL_STATE), 60.0, _ls,
                    &Control::LSCommon::GET_SYSTEM_GET_STATUS_SYSTEM_STATE,
                    state);

                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }
                // Check for timeout
                else if(timedOut == true)
                {
                    LSCommonExceptions::TimeOutExImpl ex(__FILE__, __LINE__,
                            __PRETTY_FUNCTION__);

                    std::string out("Timeout on operational LS after sending "
                        "SET_PHASELOCK_COMMAND_TUNING_FINALIZE, error code = "
                        "LSCommonExceptions::TimeOut.");

                    #define LOCKING_STATE 2U
                    if(state == LOCKING_STATE)
                    {
                        out += " Hardware indicates still locking";
                    }

                    STATIC_LOG_TO_OPERATOR(LM_ERROR, out);
                    ex.addData("Detail", out);
                    throw ex;
                 }

                // Step 8:  Double check the state to make sure it didn't
                // bounce out.
                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }

                // Reading the status twice forces an update of the parent of
                // the dependent monitor point.
                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                _ls->GET_SYSTEM_GET_STATUS_SYSTEM_STATE(ts);
                state = _ls->GET_SYSTEM_GET_STATUS_SYSTEM_STATE(ts);

                if(state != OPERATIONAL_STATE)
                {
                    LSCommonExceptions::TimeOutExImpl ex(__FILE__, __LINE__,
                        __PRETTY_FUNCTION__);

                    std::string out("LS fell out of operational after "
                        "locking.");
                    if(state == PHASE_LOCKING)
                    {
                        // Hardware timeout!
                        out += "Error code = LSCommonExceptions::TimeOut.  "
                            "Hardware indicates still locking";
                    }

                    STATIC_LOG_TO_OPERATOR(LM_ERROR, out);
                    ex.addData("Detail", out);
                    throw ex;
                }

                // Step 9: Check to see if we are locked.
                // Check the abort flag often!
                if(readAbortFlag() != 0U)
                {
                    abortAndSendCompletion(_callback, __FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    break;
                }

                // Reading the status twice forces an update of the parent of
                // the dependent monitor point.
                msg.str("");
                msg <<std::endl<<"Line: "<<__LINE__<<std::endl
                    <<"CVR: "<<_cvr<<std::endl
                    <<"LS: "<<_ls<<std::endl
                    <<"Callback: "<<_callback<<std::endl
                    <<"startTime: "<<TETimeUtil::toTimeDateString((*iter).startTime)<<std::endl
                    <<"timeToLockPretunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockPretunedSlaveLaser)<<std::endl
                    <<"timeToLockUntunedSlaveLaser" << TETimeUtil::toTimeDateString((*iter).timeToLockUntunedSlaveLaser)<<std::endl
                    <<"lsFrequency: "<<(*iter).lsFrequency<<std::endl
                    <<"cvrFrequency: "<<(*iter).cvrFrequency<<std::endl
                    <<"cvrAmplitude: "<<(*iter).cvrAmplitude<<std::endl
                    <<"cvrHarmonic: "<<(*iter).cvrHarmonic<<std::endl
                    <<"isLORTM: "<<(*iter).isLORTM<<std::endl;
                STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                _ls->GET_PHASELOCK_GET_STATUS_LOCKED(ts);
                islocked = _ls->GET_PHASELOCK_GET_STATUS_LOCKED(ts);

                if(islocked == false)
                {
                    ControlCommonExceptions::LockErrorExImpl ex(__FILE__,
                        __LINE__, __PRETTY_FUNCTION__);

                    STATIC_LOG_TO_OPERATOR(LM_ERROR, "LS is Operational but "
                        "not locked.");

                    const unsigned int maxErrs(6U);
                    unsigned int errCount(0);
                    msg.str("");
                    msg << "LS failed to lock.  Error stack follows:\n";
                    do
                    {
                        state = _ls->GET_SYSTEM_GET_ERROR(ts);
                        if(state != 0)
                        {
                             ++errCount;
                             msg << " LS error state = "
                                  << state
                                  << ".\n";
                        }
                        else
                        {
                            // Break out if the current error returned was 0.
                            break;
                        }
                    }
                    while(errCount < maxErrs);

                    if(errCount >= maxErrs)
                    {
                        msg << "LS error extraction terminated, too many "
                            "errors.";
                    }

                    STATIC_LOG_TO_DEVELOPER(LM_INFO, msg.str());
                    ex.addData("Detail", msg.str());
                    throw ex;
                }

                // Frequency in Hz
                //(*iter).ls = Control::LSCommon::_nil();
                photonicReference.setCurrentFrequency(iter);
                photonicReference.m_lsFrequency = (*iter).lsFrequency;
                photonicReference.m_cvrAmplitude = (*iter).cvrAmplitude;
                photonicReference.m_harmonic = (*iter).cvrHarmonic;

                // Report back that the locking was successful.
                msg.str("");
                msg << "LS successfully locked. LS frequency = "
                    << setprecision(9)
                    << (*iter).lsFrequency * 1e-9
                    << "[GHz], CVR frequency = "
                    << (*iter).cvrFrequency * 1e-9
                    << "[GHz], CVR harmonic = "
                    << (*iter).cvrHarmonic
                    << ", CVR amplitude = "
                    << (*iter).cvrAmplitude
                    << "[dBm].";
                STATIC_LOG_TO_OPERATOR(LM_INFO, msg.str());

                ACSErrTypeOK::ACSErrOKCompletion completion;
                completion.addData("Detail", msg.str());
                sendCompletion(_callback, completion);
            }
            catch(ControlCommonExceptions::AsynchronousFailureExImpl& ex)
            {
                // This exceptions means that the lock procedure ran into a
                // time out.
                ControlCommonExceptions::AsynchronousFailureCompletion
                    completion(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
                sendCompletion(_callback, completion);

                msg.str("");
                msg << ex.getData("Detail");
                STATIC_LOG_TO_DEVELOPER(LM_ERROR, msg.str())

                STATIC_LOG_TO_OPERATOR(LM_ERROR, "OUCH!  The LS locking "
                    "thread in the photonic reference just caught an "
                    "AsynchronousFailureCompletion exception.  Trying to "
                    "continue anyway but please have somebody investigate this!");
            }
            catch(LSCommonExceptions::TimeOutExImpl& ex)
            {
                // This exceptions means that the lock procedure failed to
                // lock the LS.
                LSCommonExceptions::TimeOutCompletion completion(ex, __FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
                sendCompletion(_callback, completion);

                msg.str("");
                msg << ex.getData("Detail");
                STATIC_LOG_TO_DEVELOPER(LM_ERROR, msg.str())

                STATIC_LOG_TO_OPERATOR(LM_ERROR, "OUCH!  The LS locking "
                    "thread in the photonic reference just caught a "
                    "timeout exception.  Trying to continue anyway but please "
                    "have somebody investigate this!");
            }
            catch(...)
            {
                LSCommonExceptions::TimeOutCompletion completion(__FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
                sendCompletion(_callback, completion);

                msg.str("");
                msg << "Unexpected exception was thrown during the locking "
                    "procedure of the LS.";
                STATIC_LOG_TO_DEVELOPER(LM_ERROR, msg.str())

                STATIC_LOG_TO_OPERATOR(LM_ERROR, "OUCH!  The LS locking "
                    "thread in the photonic reference just caught an "
                    "unexpected exception.  Trying to continue anyway but "
                    "please have somebody investigate this!");
            }
        }
    };

    STATIC_LOG_TO_DEVELOPER(LM_INFO, "Thread has been stopped.");
}
