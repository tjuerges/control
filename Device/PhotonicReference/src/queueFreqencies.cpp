//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Feb 10, 2011  created
//


/// This file contains everything that deals with setting frequencies in
// subscans or the abandoning of current and future setting of those.


#include <PhotonicReferenceImpl.h>
// For Control::FrequencyQueue.
#include <typdefsPhotonicReference.h>
// For std::string.
#include <string>
// For std::[i|o]stringstream.
#include <sstream>
// For std::min_element.
#include <algorithm>
// For boost::bind.
#include <boost/bind.hpp>
// For Control::ProgressCallback.
#include <ControlCommonCallbacksC.h>
// For the ACEErr::Completion.
#include <acserrC.h>
// For TETimeUtil::ceilTE.
#include <TETimeUtil.h>


void Control::PhotonicReferenceImpl::queueFrequencies(
    const Control::PhotonicReference::SubscanInformationSeq& _frequencies,
    const Control::ProgressCallback_ptr callback)
{
    try
    {
        std::ostringstream msg;
        Control::CVR_var cvr(Control::CVR::_nil());
        Control::LSCommon_var ls(Control::LSCommon::_nil());
        const bool isLORTM(getComponentReferences(cvr.inout(), ls.inout()));
        if((CORBA::is_nil(cvr) == true)
        || (CORBA::is_nil(ls) == true))
        {
            ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            msg.str("");
            msg << "CVR reference or LS reference is nil!";
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }

        // Set the minimum and maximum time that it takes to tune a slave
        // laser.  In simulation it takes only about 0.5s, the real hardware
        // needs 20s for the tuning.
        const ACS::Time timeToLockPretunedSlaveLaser(
            defaultLsMinTuningTime);
        ACS::Time timeToLockUntunedSlaveLaser(defaultHardwareLsMaxTuningTime);
        if(ls->isSimulated() == true)
        {
            timeToLockUntunedSlaveLaser = defaultSimulationLsMaxTuningTime;
        }

        // First thing to do: check frequency ranges and set harmonic and
        // amplitude
        CORBA::String_var sn(ls->getSerialNumberStr());
        msg << "LS serial number: 0x"
            << sn.in();
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());

        // Copy the frequencies and their time stamps to my own deque which
        // will also contain the harmonic and the CVR amplitude.
        const unsigned int size(_frequencies.length());
        Control::FrequencyPackage package;
        Control::FrequencyQueue frequencies(size);

        for(unsigned int i(0U); i < size; ++i)
        {
            std::memset(&package, 0, sizeof(Control::FrequencyPackage));

            // It is OK if a start time is 0ULL.  But only when the frequency
            // package contains only a single element.
            if(_frequencies[i].startTime != 0ULL)
            {
                package.startTime = _frequencies[i].startTime;
            }
            else
            {
                if(i == 0U)
                {
                    // Set the start time to (DefaultTeOffset + 8) = 10 TEs.
                    package.startTime = TETimeUtil::ceilTE(::getTimeStamp()
                        + (8ULL + DefaultTeOffset) * TETimeUtil::TE_PERIOD_ACS);
                }
                else
                {
                    ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    msg.str("");
                    msg << "A start time of 0ULL is only allowed when the "
                        "frequency package contains a single element!";
                    ex.addData("Detail", msg.str());
                    ex.log();
                    throw ex;
                }
            }

            package.lsFrequency = _frequencies[i].frequency;

            // Copy the references to the individual frequency packages.  This
            // allows for the references to change.
            // Strange enough, the callback reference has to be duplicated.
            // Otherwise the ptr becomes invalid as soon as this scope is left.
            //package.callback = Control::ProgressCallback::_duplicate(callback);
            callback_m = Control::ProgressCallback::_duplicate(callback);
            //package.cvr = cvr;
            //package.ls = ls;
            package.isLORTM = isLORTM;
            package.timeToLockPretunedSlaveLaser =
                timeToLockPretunedSlaveLaser;
            package.timeToLockUntunedSlaveLaser = timeToLockUntunedSlaveLaser;
            frequencies.at(i) = package;
        }

        const ACS::Time minStartTime((*std::min_element(frequencies.begin(),
            frequencies.end(), &compareStartTimes)).startTime);
        // Check if the next (in time) frequency package is too close
        // (<= DefaultTeOffset TEs) to now.
        const ACS::Time nextTE(TETimeUtil::ceilTE(::getTimeStamp()
            + DefaultTeOffset * TETimeUtil::TE_PERIOD_ACS));
        if(minStartTime <= nextTE)
        {
            // The earliest startTime is before the next TE.
            ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            msg.str("");
            msg << "The earliest startTime ("
                << minStartTime
                << "[100ns]) is too close to the next TE ("
                << nextTE
                << "[100ns]).";
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex;
        }
        else
        {
            msg.str("");
            msg << "Earliest startTime = "
                << minStartTime
                << "[100ns].";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        // Look up the frequencies and set the number of slave lasers.
        numberOfSlaveLasers = lookupForQueueing(sn.in(), frequencies);

        // Phew, got all the references and the frequencies are OK.  Now
        // try to lock the LS.
        if(queueingThread != 0)
        {
            msg.str("");
            msg<<"***Size of QUEUE is "<<frequencies.size()<<std::endl;
            LOG_TO_DEVELOPER(LM_INFO, msg.str());
            queueingThread->go(frequencies);
        }
        else
        {
            ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", "Cannot queue the subscan frequencies "
                "because a software resource has not been properly "
                "initialised. Somebody from computing needs to look into "
                "this!  Information for computing: The pointer for the "
                "queueing thread is 0.");
            throw ex.getINACTErrorEx();
        }
    }
    catch(ControlExceptions::INACTErrorExImpl& ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string o("INACTError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, o);
        nex.addData("Detail", o);

        throw nex.getINACTErrorEx();
    }
    catch(ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);

        const std::string o("IllegalParameterError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, o);
        nex.addData("Detail", o);

        throw nex.getIllegalParameterErrorEx();
    }
    catch(ControlExceptions::XmlParserErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);

        const std::string o("XmlParserError Exception caught: "
            + std::string(ex.getData("Detail").c_str()));
        LOG_TO_OPERATOR(LM_ERROR, o);
        nex.addData("Detail", o);

        throw nex.getIllegalParameterErrorEx();
    }
    catch(std::exception& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string o(std::string("Exception caught: ") + ex.what());
        LOG_TO_OPERATOR(LM_ERROR, o);
        nex.addData("Detail", o);

        throw nex.getIllegalParameterErrorEx();
    }
    catch(...)
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        const std::string o("Unexpected exception caught: No further detail "
            "is available.");
        LOG_TO_OPERATOR(LM_ERROR, o);
        ex.addData("Detail", o);
        throw ex.getIllegalParameterErrorEx();
    }
}

/// Abandons all pending frequency changes and, if a frequency change
/// is in progress, aborts it.
void Control::PhotonicReferenceImpl::abortFrequencyChanges()
{
    if(queueingThread != 0)
    {
        LOG_TO_DEVELOPER(LM_DEBUG, "Aborting queueing thread.");
        // Setting the abort flag to anything else than 0U signals an abort.
        queueingThread->setAbortFlag(1U);
    }
    else
    {
        LOG_TO_DEVELOPER(LM_DEBUG, "No queueing thread available.");
    }
}

unsigned short Control::PhotonicReferenceImpl::lookupForQueueing(
    const std::string& sn, Control::FrequencyQueue& frequencies)
{
    // This is the number of independent slave lasers that will be set later.
    unsigned short independentSlaveLasers(1U);

    std::string snXml;
    std::ostringstream msg;
    try
    {
        for(Control::FrequencyQueue::iterator frequencyIter(
            frequencies.begin()); frequencyIter != frequencies.end();
            ++frequencyIter)
        {
            (*frequencyIter).cvrHarmonic = 5;
            (*frequencyIter).cvrAmplitude = 18.0;
            double lsLow(65e9);
            double lsHigh(123e9);
            double min(0);
            double max(0);

            // Check current LS S/N.
            for(std::vector< ControlXmlParser >::iterator xmlIter(
                list->begin()); xmlIter != list->end(); ++xmlIter)
            {
                snXml = (*xmlIter).getStringAttribute("value");

                if((snXml.compare(sn) == 0) || (snXml.compare("default") == 0))
                {
                    if(snXml.compare("default") == 0)
                    {
                        // Fallback on last element of the XML.
                        msg.str("");
                        msg << "No proper configuration for LS S/N 0x"
                            << sn
                            << " found, using default values.";
                        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
                    }
                    else
                    {
                        msg.str("");
                        msg << "Obtaining configuration for LS S/n 0x"
                            << sn
                            << ", Photonic Reference #"
                            << m_prNo
                            << ".";
                        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                    }

                    // Return the number of independent slave lasers.
                    std::istringstream stringNumber;
                    try
                    {
                        stringNumber.str((*xmlIter).getStringAttribute(
                            "independentSlaveLasers"));
                        stringNumber >> independentSlaveLasers;
                    }
                    catch(const ControlExceptions::XmlParserErrorExImpl& ex)
                    {
                        // Do nothing.  It is OK when there is no entry for
                        // this LS.  Then the default will be used.
                        msg.str("");
                        msg << "Could not find a value for the attribute \""
                            "independentSlaveLasers\" for LS S/n 0x"
                            << sn
                            << ", Photonic Reference #"
                            << m_prNo
                            << ".  The default value = "
                            << independentSlaveLasers
                            << " will be used ";
                        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                    }

                    // Assign the current configuration of LS.
                    std::auto_ptr< std::vector< ControlXmlParser > >
                        freq_val_list((*xmlIter).getElements(
                            "frequencyvalues"));
                    for(std::vector< ControlXmlParser >::const_iterator
                        frequencyValuesIter(freq_val_list->begin());
                        frequencyValuesIter != freq_val_list->end();
                        ++frequencyValuesIter)
                    {
                        min = (*frequencyValuesIter).getDoubleAttribute(
                            "minvalue");
                        max = (*frequencyValuesIter).getDoubleAttribute(
                            "maxvalue");

                        if((*frequencyIter).lsFrequency < max)
                        {
                            (*frequencyIter).cvrAmplitude =
                                (*frequencyValuesIter).getDoubleAttribute(
                                    "cvrAmplitude");
                            (*frequencyIter).cvrHarmonic =
                                (*frequencyValuesIter).getLongAttribute(
                                    "harmonic");
                            lsLow = (*frequencyValuesIter).getDoubleAttribute(
                                "lslow");
                            lsHigh =
                                (*frequencyValuesIter).getDoubleAttribute(
                                    "lshigh");

                            msg.str("");
                            msg << "LS("
                                << m_prNo
                                << ") settings for target frequency = "
                                << (*frequencyIter).lsFrequency
                                << "[GHz], valid frequency interval = ["
                                << min / 1e9
                                << "; "
                                << max / 1e9
                                << "][GHz]: CVR amplitude = "
                                << (*frequencyIter).cvrAmplitude
                                << "[dBm], CVR harmonic = "
                                << (*frequencyIter).cvrHarmonic
                                << ", minimum LS frequency = "
                                << lsLow / 1e9
                                << "[GHz], maximum LS frequency = "
                                << lsHigh / 1e9
                                << "[GHz].";
                            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                            checkTargetRange((*frequencyIter).lsFrequency,
                                lsLow, lsHigh);
                            break;
                        }
                    }

                    break;
                }
            }
        }
    }
    catch(const ControlExceptions::XmlParserErrorExImpl& ex)
    {
        ControlExceptions::XmlParserErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        msg.str("");
        msg << "Failure while accessing TMCDB configuration data for "
            "this Photonic Reference!";
        nex.addData("Detail", msg.str());
        nex.log();

        throw nex;
    }

    return independentSlaveLasers;
}

// Helper method; checks targetFreq and throws exception if out of range.
void Control::PhotonicReferenceImpl::checkTargetRange(double targetFrequency,
    double lsLow, double lsHigh)
{
    if((targetFrequency < lsLow) || (targetFrequency > lsHigh))
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);

        std::ostringstream msg;
        msg << std::fixed
          << "LS target frequency ("
          << setprecision(3)
          << targetFrequency / 1e9
          << "[GHz] is not within range ("
          <<  lsLow/ 1e9
          << "[GHz] - "
          << lsHigh / 1e9
          << "[GHz]) for this LS.";
        ex.addData("Detail", msg.str());
        ex.log();

        throw ex;
    }
}
