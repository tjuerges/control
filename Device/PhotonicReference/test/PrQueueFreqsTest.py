import atexit
import os
import readline
import rlcompleter
#
readline.parse_and_bind('tab: complete')
#
historyPath = os.path.expanduser("~/.python_history")
#
def save_history(historyPath = historyPath):
    import readline
    readline.write_history_file(historyPath)
#
if os.path.exists(historyPath):
    readline.read_history_file(historyPath)
#
atexit.register(save_history)
del atexit, readline, rlcompleter, save_history, historyPath


import sys
import time
import random
import Control
import ControlExceptionsImpl
import ControlCommonCallbacks.ProgressCallbackImpl
import CCL.PhotonicReference
import CCL.logging
import Acspy.Clients.SimpleClient
import Acspy.Common.TimeHelper


# List of photonic reference numbers to test
#prnumlist = [1, 2, 3, 4, 5, 6]
# 1=LORTM,2=LS,3=LSPP
prnumlist = [1, 2, 3]
pr = {}
logger = None
sc = None


def makelist(x):
    if type(x) == list:
        return x
    else:
        return [x]

def setUpOne(prnum):
    global logger
    "Set up a single PhotonicReference"
    prName = "PhotonicReference%d" % prnum
    cvrName = "CVR"
    lsName = "LS"
    prdName = "PRD"
    logger = CCL.logging.getLogger()
    # Create DeviceConfig for CVR, LS.and PRD.
    cvrDeviceConfig = Control.DeviceConfig(cvrName, [])
    lsDeviceConfig = Control.DeviceConfig(lsName, [])
    prdDeviceConfig = Control.DeviceConfig(prdName, [])
    # Create the DeviceConfig for the controller. 
    prDeviceConfig = Control.DeviceConfig(prName,
        [prdDeviceConfig, cvrDeviceConfig, lsDeviceConfig])
    # Start the Controller.
    reference = CCL.PhotonicReference.PhotonicReference(prName,
        stickyFlag = True)
    if reference == None:
        logger.logInfo("Trouble:pr is None")
    # If already operational then we are done 
    if reference.getState() == Control.HardwareController.Operational:
        return reference
    # Create the subdevices.
    reference._HardwareController__controller.createSubdevices(
        prDeviceConfig, "")
    # Bring the controller to operational.  This should bring the subdevices
    # to operational, too.  This is checked later.
    reference.controllerOperational()
    # Not operational?  Something is wrong.
    if reference.getState() != Control.HardwareController.Operational:
        print "Could not bring %s operational" % prName
        cleanUpOne(reference)
        del reference
        print "FAILED"
        sys.exit(-1)
    return reference

def cleanUpOne(reference):
    "Shut down a single PhotonicReference"
    # Be nice, shut the controller down.
    try:
        reference.controllerShutdown()
    except Exception, e:
        print "Exception in PhotonicReference controllerShutdown:", e.message

def checkDeviceList(reference, check):
    global sc
    # Retrieve the controller subdevice list.
    deviceList = reference._HardwareController__controller.getSubdeviceList()
    #deviceList = reference.getSubdeviceList()
    for i in deviceList:
        referenceName = i.ReferenceName
        fullName = i.FullName
        # Check if device role name and full name match. 
        if referenceName == check.ReferenceName and \
            fullName == check.FullName:
            # Check if subdevice is operational.
            try:
                dev = sc.getComponentNonSticky(fullName)
                if dev == None:
                    str = "The component for '%s' is None" % fullName
                    raise Exception(str)
                # If it is not operational, this is wrong.
                if dev.getHwState() != Control.HardwareDevice.Operational:
                    str = ("Subdevice %s is not operational!" % fullName)
                    raise Exception(str)
            except Exception, e:
                print e.message
                del dev
                return False
            del dev
            return True
    return False

def setUp(prnumList = None):
    """Sets up a one or more photonic references. 
    The input is by photonic reference number, either a single number, or a
    list of numbers. The default is to use all of PRs defined at the top
    of this file.
    """
    global prnumlist, pr

    if prnumList == None:
        prnumList = prnumlist
    else:
        prnumList = makelist(prnumList)
    for prnum in prnumList:
        if pr.has_key(prnum) == False:
            pr[prnum] = setUpOne(prnum)

def cleanUp():
    global pr

    for i in pr.values():
        cleanUpOne(i)
        del i

def test1():
    global logger, prnumlist, pr

    failed = False
    thisPrFailed = False
    for prnum in prnumlist:
        thisPrFailed = False
        prName = "PhotonicReference%d" % prnum
        for j in ["CVR", "LS", "PRD"]:
            fullName = "CONTROL/CentralLO/%s/%s" % (prName, j)
            check = Control.DeviceNameMap(ReferenceName = j,
                FullName = fullName)
            try :
                if checkDeviceList(pr[prnum], check) == False:
                    print "checkDeviceList failed for %s" % prName
                    thisPrFailed = True
                    break
            except Exception, e:
                print "checkDeviceList failed for %s: %s" % (prName, e.message)
                thisPrFailed = True
                break
        if thisPrFailed == False:
            try:
                pr[prnum].setFrequency(77.456e9)
            except Exception, e:
                print "Exception on %s.setFrequency(): %s" \
                    % (prName, e.message)
                thisPrFailed = True
                break
            seq = []
            now = Acspy.Common.TimeHelper.getTimeStamp().value
            for i in range(5):
                startTime = now + int(5e7 + i * 1e7)
                frequency = (random.random() * 1e8) + 31e9
                info = Control.PhotonicReference.SubscanInformation(
                    startTime = startTime,
                    frequency = frequency)
                print "Start time = %d, frequency = %d." % (startTime,
                    frequency)
                seq.append(info)
            cb = ControlCommonCallbacks.ProgressCallbackImpl.ProgressCallbackImpl()
            pr[prnum].queueFrequencies(seq, cb.getExternalInterface())
            for i in range(len(seq)):
                try:
                    print "Waiting for callback."
                    cb.waitForCompletion(10)
                    completion = cb.getCompletionAndClearStatus()
                    if completion != None:
                        if completion.isErrorFree() == False:
                            print "Completion signals that a problem occured."
                            if len(completion.previousError) > 0:
                                print "Got completion: ", \
                                    completion.previousError
                            else:
                                print "Completion is still empty."
                            thisPrFailed = True
                            break
                        else:
                            startTime = seq[i].startTime
                            now = Acspy.Common.TimeHelper.getTimeStamp().value
                            difference = now - startTime
                            print "The completion returned OK, time = " \
                            "%d[100ns], startTime = %d[100ns], difference " \
                            "now - startTime = %d[us]." \
                            % (now, startTime, (now - startTime) / 1e6)
                    else:
                        print "A completion has not been returned yet."
                        thisPrFailed = True
                        break
                except ControlExceptionsImpl, ex:
                    ex.log(logger)
                    thisPrFailed = True
                    break
                except Exception, e:
                    print "Exception on %s.queueFrequencies(): %s" \
                        % (prName, e.message)
                    thisPrFailed = True
                    break
            if thisPrFailed == True:
                break
        if thisPrFailed == True:
            break
    if thisPrFailed == True:
        failed = True
    return failed

def runtests():
    setUp()
    f = test1()
    #cleanUp()
    if f == False:
        print "PASSED"
    else :
        print "FAILED"
    if __name__ == '__main__':
        if f == False:
            sys.exit(0)
        else:
            sys.exit(-1)
    else:
        return f

def checkFreqs(prs = [1, 2]) :
    """Checks some selected frequencies for requested set of 
    photonic references. The default is 1 and 2, which tests
    LS1(LORTM) and LS2(LS). The frequencies are selected to sample
    frequencies that give different CVR harmonics and amplitudes for
    the LORTM and LS devices.
    Use the log to look at the selected CVR harmonics and amplitudes."""
    global prnumlist, pr

    prs = makelist(prs)
    prnumlist = prs
    setUp()
    failed = False

    for prnum in prnumlist:
        thisPrFailed = False
        prName = "PhotonicReference%d" % prnum
        for j in ["CVR", "LS", "PRD"]:
            fullName = "CONTROL/CentralLO/%s/%s" % (prName, j)
            check = Control.DeviceNameMap(ReferenceName = j, FullName = fullName)
            try :
                if checkDeviceList(pr[prnum], check) == False:
                    m = "checkDeviceList failed for %s" % prName,
                    print m
                    thisPrFailed = True
                    failed = True
            except Exception, e:
                m = "checkDeviceList failed for %s" % prName
                print m, e.message
                failed = True
    print "Device list check completed"
    if failed:
        return
    for prnum in prnumlist:
        prName = "PhotonicReference%d" % prnum
        print "Testing freqs for", prName
        f = 0.0
        try:
            pr[prnum].setFrequency(30e9)
            for i in range(66, 126, 10):
                f = i * 1e9
                pr[prnum].setFrequency(f)
            f = 118e9
            pr[prnum].setFrequency(f)
            f = 121e9
            pr[prnum].setFrequency(f)
        except Exception, e:
            print "Exception on %s.setFrequency(%f)" % (prName, f * 1e-9), e.message
    print "Done with checkFreqs()"

sc = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
setUp()
PhotonicReference = pr[2]
#
logger = CCL.logging.getLogger()
seq = []
now = Acspy.Common.TimeHelper.getTimeStamp().value
for i in range(5):
    startTime = now + int(5e7 + i * 1.5 * 1e7)
    frequency = (random.random() * 1e8) + 71e9
    info = Control.PhotonicReference.SubscanInformation(
        startTime = startTime,
        frequency = frequency)
    print "Start time = %d, frequency = %d." % (startTime, frequency)
    seq.append(info)

cb = ControlCommonCallbacks.ProgressCallbackImpl.ProgressCallbackImpl()
PhotonicReference.queueFrequencies(seq, cb.getExternalInterface())
for i in range(len(seq)):
    try:
        if i == 3:
            PhotonicReference.abortFrequencyChanges()
        print "Waiting for callback."
        cb.waitForCompletion(10)
        completion = cb.getCompletionAndClearStatus()
        if completion != None:
            if completion.isErrorFree() == False:
                print "Completion signals that a problem occured."
                if len(completion.previousError) > 0:
                    print "Got completion: ", completion.previousError
                else:
                    print "Completion is still empty."
            else:
                startTime = seq[i].startTime
                now = Acspy.Common.TimeHelper.getTimeStamp().value
                difference = now - startTime
                print "The completion returned OK, time = %d[100ns], " \
                    "startTime = %d[100ns], difference now - startTime = " \
                    "%d[us]." % (now, startTime, (now - startTime) / 1e6)
        else:
            print "No completion has been returned yet."
    except ControlExceptionsImpl, ex:
        ex.log(logger)
        failed = True
    except Exception, e:
        print "Exception on PhotonicReference.queueFrequencies(): %s" \
            % (e.message)
        failed = True

sc.disconnect()
