#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef PhotonicReferenceImpl_h
#define PhotonicReferenceImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jul 1, 2009  created
//


// Base class(es)
#include <hardwareControllerImpl.h>
// CORBA servant header
#include <PhotonicReferenceS.h>
// For error strings, etc.
#include <string>
// For std::ostringstream.
#include <sstream>
#include <iomanip>
// For Control::FrequencyQueue.
#include <typdefsPhotonicReference.h>
// For maci::SmartPtr
#include <acsComponentSmartPtr.h>
// For the Control::ArrayMonitor.
#include <ControlArrayInterfacesC.h>
// For the Control::CVR_ptr and the Control::LSCommon_ptr.
#include <CVRC.h>
#include <LSCommonC.h>
// For Control::ProgressCallback.
#include <ControlCommonCallbacksC.h>
// For Control::QueueingThread.
#include <queueingThread.h>
// xmlTmcdbComponent
#include <xmlTmcdbComponent.h>
#include <configDataAccess.h>
// For the currentFrequenciesMutex.
#include <Mutex.h>


// Forward declarations for classes that this component uses.
namespace maci
{
    class ContainerServices;
};


namespace Control
{
    /// The PhotonicReference component is a hardware controller for the
    /// CVR/LS component pairs.
    class PhotonicReferenceImpl: public virtual POA_Control::PhotonicReference,
        public Control::HardwareControllerImpl
    {
        public:
        /// The constructor for any ACS C++ component must have this signature.
        PhotonicReferenceImpl(const ACE_CString& name,
            maci::ContainerServices* cs);

        /// The Destructor must be virtual because this class contains virtual
        /// functions.
        virtual ~PhotonicReferenceImpl();

        /// ACS lifecycle method.
        /// \exception No exception is thrown during execution of this method.
        virtual void execute();

        /// ACS lifecycle method.
        /// \exception No exception is thrown during execution of this method.
        virtual void cleanUp();

        /// See the PhotonicReference.idl file for a description of this
        /// function.
        virtual bool isAssigned();

        /// See the PhotonicReference.idl file for a description of this
        /// function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual void assign(const char* arrayName);

        /// See the PhotonicReference.idl file for a description of this
        /// function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual void unassign();

        /// See the PhotonicReference.idl file for a description of this
        /// function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual char* getArray();

        /// Retrieves current output frequency of the Laser Synthesizer
        /// \return LS frequency
        virtual CORBA::Double getFrequency();

        /// Set output frequency of the Laser Synthetizer.
        /// \param frequency The target frequency for the LS.
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception LSCommonExceptions::TimeOutEx
        /// \exception ControlCommonExceptions::LockErrorEx
        virtual void setFrequency(CORBA::Double targetFrequency);

        /// Queue a set of the photonic reference frequency changes. After each
        /// frequency change is completed the report function of the provided
        /// callback is called. If the photonic frequency does not lock
        /// subsequent frequency changes are aborted.
        /// \exception ControlExceptions::INACTErrorEx if the CVR or laser
        /// synthesizer is not operational.
        /// \exception ControlExceptions::IllegalParameterErrorEx if any start
        /// time is in the past or any frequency is incorrect.
        virtual void queueFrequencies(
            const Control::PhotonicReference::SubscanInformationSeq&
                frequencies,
            const Control::ProgressCallback_ptr callback);

        /// Abandons all pending frequency changes and, if a frequency change
        /// is in progress, aborts it.
        virtual void abortFrequencyChanges();

        /// Estimate the time for the photonic reference to tune to the
        /// specified sequence of frequencies starting with the
        /// current state the system.
        /// \exception ControlExceptions::INACTErrorEx if the CVR or laser
        /// synthesizer is not operational.
        /// \exception ControlExceptions::IllegalParameterErrorEx if any
        /// frequency is incorrect.
        virtual ACS::TimeIntervalSeq* timeToTune(
            const Control::DoubleSeq& frequencies);


        private:
        /// \ref Control::QueueingThread::run is a friend.  This allows access
        /// to all private members.
        friend void Control::QueueingThread::run();

        /// \ref Control::QueueingThread::checkMonitorPoint uses \ref sleep.
        /// Therefore it is a friend.
        template< typename ValueType, typename DeviceType, typename MethodPtr >
        friend bool Control::QueueingThread::checkMonitorPoint(
            ValueType targetValue, double timeout, DeviceType device,
            MethodPtr ptr, ValueType& ret);

        /// No default constructor.
        PhotonicReferenceImpl();

        /// No copy constructor.
        PhotonicReferenceImpl(const PhotonicReferenceImpl&);

        /// No assignment operator.
        PhotonicReferenceImpl& operator=(const PhotonicReferenceImpl&);

        /// New version of \ref lookup which handles multiple frequencies.
        /// For an LS given by the LSnum and the targetFreq, set the harmonic
        /// and CVRamplitude.
        /// \return number of slave lasers which this LS has.
        unsigned short lookupForQueueing(const std::string& sn,
            Control::FrequencyQueue& frequencies);

        /// Check that the requested frequency is within range.
        /// \exception ControlExceptions::IllegalParameterErrorExImpl is
        /// thrownif it is not within range.
        void checkTargetRange(double targetFreq, double lsLow, double lsHigh);

        /// Sleeps for \ref seconds.
        void sleep(double seconds);

        bool getComponentReferences(Control::CVR_ptr& cvr,
            Control::LSCommon_ptr& ls);

        /// Add the current set of frequency parameters to
        ///  \ref currentFrequencies.
        void setCurrentFrequency(
            const Control::FrequencyQueue::iterator& iter);

        /// \return a copy of \ref currentFrequencies.
        Control::FrequencyQueue getCurrentFrequencies();

        /// The full name of this Photonic Reference.
        std::string m_fullName;

        /// Keeps a pointer to the \ref maci::ContainerServices.
        maci::ContainerServices* cs;

        /// The short name of this Photonic Reference (e.g. PhotonicReference3).
        std::string m_referenceName;

        /// Number of the PhotonicReference (used for tuning lookup)
        int m_prNo;

        /// Reference to the currently assigned array (if there is one).
        maci::SmartPtr< Control::ArrayMonitor > m_assignedArray_sp;

        /// Current LS frequency.
        double m_lsFrequency;

        /// Target frequency tolerance.
        const double m_frequencyTolerance;

        /// Laser Synthesizer offset lock frequency, in Hz
        /// LS = CVR*harmonic + LSoffsetFreq
        const double m_LSoffsetFreq;

        /// Default offset in TEs which is added to \ref ::getTimeStamp().
        /// This time stamp then serves two purposes:
        /// - As a default start time when a sequence of length = 1 is passed
        /// to \ref queueFrequencies.
        /// - And as a time stamp in the future which has to be earlier than
        /// every start time in the list of frequency packages when the length
        /// of the sequence is != 1.
        const ACS::Time DefaultTeOffset;

        /// Harmonic number, see above.
        int m_harmonic;

        /// Amplitude of the CVR, in dBm
        double m_cvrAmplitude;

        /// Pointer to the thread that does the LS & CVR locking.
        Control::QueueingThread* queueingThread;

        /// Represents one second in ALMA time units.
        const double oneSecond;

        /// Represent a TE in ALMA time units.
        const double oneTE;

        /// Interval during that the run thread sleeps, expecting that a
        /// monitor point has changed its value.
        const double lsSleepTime;

        /// TMCDB data accessor.
        ConfigDataAccessor* configData;

        /// TMCDB parser?
        std::auto_ptr< std::vector< ControlXmlParser > >list;

        /// Maintains the current setting of the slave lasers.
        Control::FrequencyQueue currentFrequencies;

        /// The number of slaveLasers in the current LS.
        unsigned short numberOfSlaveLasers;

        /// This mutex protects \ref currentFrequencies.
        ACE_Mutex currentFrequenciesMutex;

        /// Default maximum time for LS locking when using real hardware.
        const ACS::Time defaultHardwareLsMaxTuningTime;

        /// Default maximum time for LS locking when in simulation.
        const ACS::Time defaultSimulationLsMaxTuningTime;

        /// Default minimum time for LS locking.
        const ACS::Time defaultLsMinTuningTime;

public:
        Control::ProgressCallback_var callback_m;
    };
};
#endif
