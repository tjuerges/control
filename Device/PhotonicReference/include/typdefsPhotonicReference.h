#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef typdefsPhotonicReference_h
#define typdefsPhotonicReference_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Feb 17, 2011  created
//


#include <deque>
#include <acscommonC.h>
// For Control::CVR_ptr.
#include <CVRC.h>
// For Control::LSCommon_ptr.
#include <LSCommonC.h>
// For the ControlCommonCallbacks.
#include <ControlCommonCallbacksC.h>


namespace Control
{
    /// typedef for an LS frequency package that contains the start time,
    /// the target frequency, the CVR harmonic and the CVR amplitude.
    typedef struct
    {
        //Control::CVR_var cvr;
        //Control::LSCommon_var ls;
        //Control::ProgressCallback_var callback;
        ACS::Time startTime;
        ACS::Time timeToLockPretunedSlaveLaser;
        ACS::Time timeToLockUntunedSlaveLaser;
        double lsFrequency;
        double cvrFrequency;
        double cvrAmplitude;
        unsigned int cvrHarmonic;
        bool isLORTM;
    } FrequencyPackage;

    /// typedef for a freuquency package deque.
    typedef std::deque<
        Control::FrequencyPackage > FrequencyQueue;

    /// Compares the startTimes in a \ref FrequencyQueue and returns false
    /// if begin < end.
    bool compareStartTimes(Control::FrequencyPackage begin,
        Control::FrequencyPackage end);

    bool compareFrequencyPackage(Control::FrequencyPackage begin,
        Control::FrequencyPackage end);
};
#endif
