#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef QueueingThread_h
#define QueueingThread_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Feb 10, 2011  created
//


// For std::queue
#include <deque>
// For std::pair
#include <utility>
// For ACS::Thread
#include <acsThread.h>
// For Control::PhotonicReference::SubscanInformationSeq.
#include <PhotonicReferenceC.h>
// For CVR.
#include <CVRC.h>
// For LSCommon.
#include <LSCommonC.h>
// For ACS::Time.
#include <acscommonC.h>
// For Control::FrequencyQueue.
#include <typdefsPhotonicReference.h>
// For the mutex on which the run method waits..
#include <Mutex.h>


namespace Control
{
    class PhotonicReferenceImpl;
};


namespace Control
{
    class QueueingThread: public ACS::Thread
    {
        public:
        /// Constructor.
        QueueingThread(const ACE_CString& name,
            Control::PhotonicReferenceImpl& pr);

        /// Destructor.
        virtual ~QueueingThread();

        /// Overloading the run method of \ref ACS::Thread.  This allows for
        /// better control over when this thread runs, when it stops and
        /// what it does in general.  This is unfortunately necessary due to
        /// the nature of its task.
        virtual void run();

        /// Allows the thread to execute.  Releases \ref mutex and sets the
        /// according member variables to the parameter values.
        void go(const Control::FrequencyQueue& frequencies);

        /// Sets the \ref abortFlag to \param value.
        void setAbortFlag(unsigned int value);


        private:
        /// No default ctor.
        QueueingThread();

        /// No copy ctor.
        QueueingThread(const QueueingThread&);

        /// No assignment operator.
        QueueingThread& operator=(const QueueingThread&);

        /// \return The value of \ref abortFlag which is read atomically.
        unsigned int readIsActiveFlag();

        /// \return The value of \ref abortFlag which is read atomically.
        unsigned int readAbortFlag();

        /// \return The value of \ref updateFlag which is read atomically.
        unsigned int readUpdateFlag();

        /// Set the \ref isActiveFlag atomically to \param value.
        void setIsActiveFlag(unsigned int value);

        /// Set the \ref updateFlag atomically to \param value.
        void setUpdateFlag(unsigned int value);

        /// Template method which allows to pass the a device reference and a
        /// monitor point method as parameters.  This significantly reduces
        ///  clutter in the locking method.
        /// \par device is a component reference, \par ptr is the pointer to
        /// a monitor point method of \ref device. \par value will contain the
        /// value of the monitor point.
        /// \return true if the call timed out after \par timeout seconds.
        template< typename ValueType, typename DeviceType, typename MethodPtr >
        bool checkMonitorPoint(ValueType targetValue, double timeout,
            DeviceType device, MethodPtr ptr, ValueType& value);

        /// Send a \ref ControlCommonExceptions::LockErrorCompletion via a
        /// callback.
        template< typename CompletionType >
        void sendCompletion(const Control::ProgressCallback_var& callback,
            const CompletionType& completion);

        /// Call \ref exit and send a completion if \ref callback is != 0.
        /// The completion will contain information that the locking procedure
        /// has been aborted.
        void abortAndSendCompletion(
            const Control::ProgressCallback_var& callback,
            const std::string& file, unsigned int line,
            const std::string& method);

        /// Reference to \ref Control::PhotonicReferenceImpl.
        Control::PhotonicReferenceImpl& photonicReference;

        /// Mutex on which the thread waits to do its stuff.
        ACE_Mutex mutex;

        /// Mutex on which the thread waits when something needs to be updated.
        ACE_Mutex updateMutex;

        /// Abort flag for a locking process in progress.
        volatile unsigned int abortFlag;

        /// Member variable which contains information for the \ref run
        /// method.  This is set in \ref go.
        Control::FrequencyQueue frequencies;
    };
};
#endif
