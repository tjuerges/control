#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef CentralLOImpl_h
#define CentralLOImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jul 1, 2009  created
//


// Base class(es)
#include <hardwareControllerImpl.h>

// For error strings, etc.
#include <string>
// A map will contain the error messages and is used to keep track of the
// AmbManager, rtLog and lkmLoader references.
#include <map>
// A std::list contains the current error keys as std::strings which serve as
// keys for the error message map.
#include <list>

// For the valid host names vector.
#include <vector>

// Protection Mutex for the start up and stop of the AmbManager.
#include <Mutex.h>

// CORBA servant header
#include <CentralLOS.h>

// For maci::SmartPtr
#include <acsComponentSmartPtr.h>

// For maciErrType exceptions.
#include <maciErrType.h>

// For AUTO_TRACE.
#include <loggingMACROS.h>

#include <lkmLoaderC.h>
#include <rtlogC.h>
#include <ambManagerC.h>
#include <ArrayTimeC.h>


// Forward declarations for classes that this component uses.
namespace maci
{
    class ContainerServices;
};


namespace Control
{
    /// The CentralLO component is a hardware controller for the Central LO
    /// rack.
    /// controllerOperational is called in init pass 1 and not in init pass 2
    /// as for the other hardware controllers.
    class CentralLOImpl: public virtual POA_Control::CentralLO,
        public Control::HardwareControllerImpl
    {
        public:
        /// The constructor for any ACS C++ component must have this signature.
        CentralLOImpl(const ACE_CString& name, maci::ContainerServices* cs);

        /// The Destructor must be virtual because this class contains virtual
        /// functions.
        virtual ~CentralLOImpl();

        /// Load the kernel modules on DMC, DMC2, DMC3, DMC4, LMC then
        /// start the rtlog component on all of those computers.
        virtual void controllerOperational();

        /// Implemented so that all component references (lkmLoad, rtLog)) get
        /// released. This avoids unpleasant results during
        /// HardwareController::cleanUp().
        virtual void controllerShutdown();

        /// ACS component lifecycle method. Sole purpose is to release
        /// componet references.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();

        /// Try to load the kernel modules and start the rtLogger on DMC, DMC2,
        /// DMC3, DMC4 and LMC.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void initRtSystemDmcLmc(const char* host);

        /// Try to stop the rtLogger and remove the kernel modules on
        /// DMC, DMC2, DMC3, DMC4 and LMC.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void stopRtSystemDmcLmc(const char* host);

        // Interfaces for AmbManager start, stop and querying if it is
        // running.

        /// Start the AmbManager instance. See CentralLO.midl for a more
        /// detailed description.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void startAmbManager(const char* host);

        /// Stop the AmbManager instance. See CentralLO.midl for a more detailed
        /// description.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void stopAmbManager(const char* host);

        /// Query if the AmbManager is up and running. See CentralLO.midl for a
        /// more detailed description.
        /// \return bool: true if the AmbManager is up and running.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        bool isAmbManagerRunning(const char* host);

        /// Returns a list of all available photonic references.
        /// \return List containing valid PhotonicReferences.
        virtual Control::DeviceNameList* getPhotonicReferences();

        /// Synchronise the ArrayTime on DMC* and LMC.
        /// \exception ControlExceptions::InvalidRequestEx will be thrown if the
        /// synchronisation failed for any reason.
        virtual void synchroniseRemoteArrayTime();


        protected:
        /// Populate \ref validHostNames with the names of the hosts.
        virtual void initValidHostNames();

        /// Initialises the \ref errorMessages map.
        void initErrorMap();

        /// List of valid host names.
        std::vector< std::string > validHostNames;

        /// Map of error strings. The entries are filled in in the constructor.
        std::map< std::string, std::string > errorMessages;


        private:
        /// No copy constructor.
        CentralLOImpl(const CentralLOImpl&);

        /// No assignment operator.
        CentralLOImpl& operator=(const CentralLOImpl&);

        /// Bring the lkmLoader and rtLog components on the DMC, DMC2, DMC3,
        /// DMC4 and LMC computers up.
        /// \param host: host name.  Valid values are "DMC1-4" and "LMC".
        void getInitComponentsForDmcLmc(const std::string& host);

        /// Helper method which executes the AmbManager start up on the target
        /// host.
        /// \param host: host name.  Valid values are DMC, DMC3, DMC3,
        /// DMC4 and LMC.
        /// \exception maciErrType::CannotGetComponentExImpl
        void doStartAmbManager(const std::string& host);

        /// Perform the ArrayTime synchronisation.
        /// \exception ControlExceptions::InvalidRequestEx
        void doSynchroniseRemoteArrayTime(const std::string& host);

        /// Fetch a component reference of type \component. If the component
        /// reference cannot be acquired, an exception will be thrown.
        ///
        /// \param componentName The full name of the component, e.g.
        /// CONTROL/DA41/DTXBBpr2.
        /// \param componentType The CORBA component type, e.g.
        /// Control::MasterClock
        /// \exception maciErrType::CannotGetComponentExImpl
        template< typename componentType >
        maci::SmartPtr< componentType > getComponentReference(
            const std::string& fullName)
        {
            AUTO_TRACE(__PRETTY_FUNCTION__);

            maci::SmartPtr< componentType > reference;
            try
            {
                reference = cs->getComponentSmartPtr< componentType >(
                    fullName.c_str());
            }
            catch(const maciErrType::CannotGetComponentExImpl& ex)
            {
                maciErrType::CannotGetComponentExImpl nex(ex, __FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
                std::ostringstream output;
                output << "Got no reference for the "
                    << fullName
                    << " component.";
                nex.addData("Detail", output.str());
                nex.log();
                throw nex;
            }

            return reference;
        };

        /// Check if the given host is in the list of the valid hostnames.  If
        /// the host name is invalid, ControlExceptions::IllegalParameterErrorEx
        /// will be thrown.
        /// \param host is the host name to be checked.
        /// \param logException If true, then the exception, which will be
        /// thrown in case of an illegal host name, will be logged.
        /// \exception ControlExceptions::IllegalParameterErrorEx is thrown if
        /// the host name is not valid.
        void checkHostName(const std::string& host,
            bool logException = false) const;

        /// Sets the error bit in \ref currentErrors according to
        /// \param component which is the key for the error string in
        /// \ref errorMessages.
        /// \param errorIsActive sets (true) or clears the error (false).
        void setErrorState(const std::string& component,
            const bool errorIsActive);

        /// Returns a \return std::string which contains all current error
        /// messages concatenated.
        std::string getErrors();

        /// List of std::strings reflecting the current error state. The
        /// strings serve as keys for \ref errorMessages.
        std::list< std::string > currentErrors;

        /// Keep the container services pointer stored locally.
        maci::ContainerServices* cs;

        /// The name of this component.
        const std::string myName;

        struct HostReferences
        {
            /* HostReferences(): */
/*                 ambManagerRef_sp(Control::AmbManager::_nil()), */
/*                 rtLogRef_sp(ACS_RT::rtlog::_nil()), */
/*                 lkmLoaderRef_sp(ACS_RT::lkmLoader::_nil()) */
/*             { */
/*             }; */


            maci::SmartPtr< Control::AmbManager > ambManagerRef_sp;
            maci::SmartPtr< ACS_RT::rtlog > rtLogRef_sp;
            maci::SmartPtr< ACS_RT::lkmLoader > lkmLoaderRef_sp;
            maci::SmartPtr< Correlator::ArrayTime > arrayTimeRef_sp;
        };

        // Typedef for easier readability.
        typedef std::map< const std::string, HostReferences >
            HostMapType;

        // Typedef for better readability.
        typedef HostMapType::value_type HostMapValuePair;

        /// Access mutex which protects the references.
        ACE_Mutex hostReferencesMapMutex;

        /// Map for the lkmLoader references.
        HostMapType hostReferencesMap;
    };
};
#endif
