#ifndef CentralLOTCLOImpl_h
#define CentralLOTCLOImpl_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 * who      when     what
 * tjuerges  Sep 8, 2009  created
 */


// Base class(es)
#include <CentralLOImpl.h>


// Forward declarations for classes that this component uses.
namespace maci
{
    class ContainerServices;
};


namespace Control
{
    /// Special version of the CentralLOImpl class for the tCLOTS @ OSF.
    class CentralLOTCLOImpl: public CentralLOImpl
    {
        public:
        /// The constructor for any ACS C++ component must have this signature.
        CentralLOTCLOImpl(const ACE_CString& name, maci::ContainerServices* cs);

        /// The Destructor must be virtual because this class contains virtual
        /// functions.
        virtual ~CentralLOTCLOImpl();

        protected:
        /// Populate \ref validHostNames with the names of the hosts.
        virtual void initValidHostNames();
    };
};
#endif
