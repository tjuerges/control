#ifndef CentralLOSimImpl_h
#define CentralLOSimImpl_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 * who      when     what
 * tjuerges  Jul 1, 2009  created
 */


// Base class(es)
#include <CentralLOImpl.h>


// Forward declarations for classes that this component uses
namespace maci
{
    class ContainerServices;
}


namespace Control
{
    class CentralLOSimImpl: public Control::CentralLOImpl
    {
        public:
        CentralLOSimImpl(const ACE_CString& name,
            maci::ContainerServices* containerServices);
        virtual ~CentralLOSimImpl();

        protected:
        /// Overload \ref CentralLOImpl::controllerOperational.  This version
        /// makes sure that a simulated CentralLO component won't try to load
        /// the lkmLoader or the rtLog components.
        virtual void controllerOperational();


        private:
        CentralLOSimImpl();
        CentralLOSimImpl(const CentralLOSimImpl&);
        CentralLOSimImpl& operator=(const CentralLOSimImpl&);
    };
};
#endif
