#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


import sys
import string
import Control;
import CCL.CentralLO
from Acspy.Clients.SimpleClient import PySimpleClient


instances = ["1", "2", "3", "4", "5", "6"]


def setUpPrDeviceConfig(instance):
    prName = "PhotonicReference" + instance
    cvrName = "CVR"
    lsName = "LS"
    prdName = "PRD"

    # Create DeviceConfigs for CVR, LS and PRD.
    cvrDeviceConfig =  Control.DeviceConfig(cvrName, [])
    lsDeviceConfig =  Control.DeviceConfig(lsName, [])
    prdDeviceConfig =  Control.DeviceConfig(prdName, [])
    # Create the DeviceConfig for the controller. 
    prDeviceConfig = Control.DeviceConfig(prName, [cvrDeviceConfig, lsDeviceConfig, prdDeviceConfig])
    return prDeviceConfig


def setUp():
    global instances

    cloName = "CentralLO"
    pssasName = "PSSAS"
    psllcName = "PSLLC"
    mldName = "MLD"
    lfrdName = "LFRD"

    # Create subdevice configurations for all six PhotonicReference
    # controllers.
    prDeviceConfig = []
    for i in instances:
        prDeviceConfig.append(setUpPrDeviceConfig(i))

    # Create DeviceConfigs for the MLD and the LFRD.
    mldDeviceConfig = Control.DeviceConfig(mldName, [])
    lfrdDeviceConfig =  Control.DeviceConfig(lfrdName, [])

    # Create the PS devices.
    pssas = []
    psllc = []
    # Two PSSAS.
    for i in ["1", "2"]:
        devConfig = Control.DeviceConfig(pssasName + i, []) 
        pssas.append(devConfig)
    # Six PSLLC.
    for i in instances:
        devConfig = Control.DeviceConfig(psllcName + i, []) 
        psllc.append(devConfig)

    # Create the controller DeviceConfig, append the PSSASes, the PSLLCs,
    # the PhotonicReferences, the LFRD and the MLD.
    cloDeviceConfig = []
    for i in pssas:
        cloDeviceConfig.append(i)
    for i in psllc:
        cloDeviceConfig.append(i)
    for i in prDeviceConfig:
        cloDeviceConfig.append(i)

    cloDeviceConfig.append(mldDeviceConfig)
    cloDeviceConfig.append(lfrdDeviceConfig)

    deviceConfig = Control.DeviceConfig(cloName, cloDeviceConfig)

    # Start the Controller.
    clo = CCL.CentralLO.CentralLO(stickyFlag = True)
    # Create the subdevices.
    clo._HardwareController__controller.createSubdevices(deviceConfig, "")
    # Bring the controller to operational.  This should bring the subdevices
    # to operational, too.  This is checked later.
    clo.controllerOperational()
    # Not operational?  Something is wrong.
    if clo.getState() != Control.HardwareController.Operational:
        cleanUp(clo)
        print "CentralLO controller is not operational!"
        sys.exit(-1)
    return clo

def cleanUp(clo):
    # Be nice, shut the controller down.
    clo.controllerShutdown()
    del clo


def checkPrDeviceList(sc, pr, check):
    # Retrieve the controller subdevice list.
    deviceList = pr.getSubdeviceList()
    for i in deviceList:
        referenceName = i.ReferenceName
        fullName = i.FullName
        # Check if device role name and full name match. 
        if referenceName == check.ReferenceName and \
            fullName == check.FullName:
            # Check if subdevice is operational.
            try:
                dev = sc.getComponentNonSticky(fullName)
                # It is not operational, this is wrong.
                if dev.getHwState() != Control.HardwareDevice.Operational:
                    str = ("Subdevice %s is not operational!" % fullName)
                    raise Exception(str)
            except Exception, e:
                print e.message
                del dev
                return False
            del dev 
            return True
    return False

def checkDeviceList(sc, clo, check):
    global instances

    # Retrieve the controller subdevice list.
    deviceList = clo._HardwareController__controller.getSubdeviceList()

    for i in deviceList:
        referenceName = i.ReferenceName
        fullName = i.FullName
        # Check if device role name and full name match. 
        if referenceName == check.ReferenceName and \
            fullName == check.FullName:
            # Check if subcontroller is operational.
            try:
                pr = sc.getComponentNonSticky(fullName)
                
                # It is not operational, this is wrong.
                if pr.getState() != Control.HardwareController.Operational:
                    str = ("Controller %s is not Operational!" % fullName)
                    raise Exception(str)
            except Exception, e:
                print e.message
                del pr
                return False

            # Check the subdevices inside the subcontroller.
            instance = ""
            try:
                instance = string.split(referenceName, "PhotonicReference")
                instance = str(int(instance[1]))
                instances.index(instance)
            except Exception, e:
                print "Could not find the correct instance for the subcontroller. %s" % e.message
                return False
            for j in ["CVR", "LS"]:
                check = Control.DeviceNameMap(\
                    ReferenceName = j,\
                    FullName = fullName + "/" + j)
            if checkPrDeviceList(sc, pr, check) == False:
                del pr
                return False
            del pr
        return True
    return False


failed = False
clo = setUp()

sc = PySimpleClient.getInstance()

for instance in instances:
    check = Control.DeviceNameMap(\
        ReferenceName = "PhotonicReference" +  instance,\
        FullName = "CONTROL/CentralLO/PhotonicReference" + instance)
    if checkDeviceList(sc, clo, check) == False:
        print "checkDeviceList failed. device = ", check
        failed = True

if failed == False:
    # We made this far, now test the list of photonic references returned by the
    # 
    prList = clo.getPhotonicReferences()
    for check in prList: 
        if checkDeviceList(sc, clo, check) == False:
            print "checkDeviceList for PhotonicReferences failed. device = ", check
            failed = True

sc.disconnect()
del sc

cleanUp(clo)

if failed == False:
    print "PASSED"
    sys.exit(0)
else:
    print "FAILED"
    sys.exit(-1)
