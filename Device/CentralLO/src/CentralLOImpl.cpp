//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jul 1, 2009  created
//


#include <CentralLOImpl.h>

// For std::ostringstream.
#include <sstream>

// For std::string.
#include <string>

// For the std::find algorithm.
#include <algorithm>

// For std::pair and std::make_pair.
#include <memory>

// For the accumulate algorithm.
#include <numeric>

 // For to_upper.
#include <boost/algorithm/string.hpp>

// For the Control exceptions.
#include <ControlExceptions.h>

// For ArrayTime exceptions.
#include <CorrErr.h>

// For ACSErrTypeCppNative::ACSErrTypeCppNativeExImpl.
#include <ACSErrTypeCppNative.h>

// For maciErrType::CannotGetComponentExImpl
#include <maciErrType.h>

// For exception error traces
#include <acserr.h>

// For AUTO_TRACE.
#include <loggingMACROS.h>

// For audience logs.
#include <LogToAudience.h>

// For the container services.
#include <maciContainerServices.h>

// For the permanently loaded components
#include <lkmLoaderC.h>
#include <rtlogC.h>
#include <ambManagerC.h>

// For the ArrayTime synchronisation.
#include <ControlTimeSourceInterfacesC.h>

// For the mutex which protects all AmbManager related.
#include <Guard_T.h>


Control::CentralLOImpl::CentralLOImpl(const ACE_CString& name,
    maci::ContainerServices* _cs):
    Control::HardwareControllerImpl(name, _cs),
    cs(_cs),
    myName(name.c_str())
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    initValidHostNames();
    initErrorMap();
}

void Control::CentralLOImpl::initErrorMap()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        // Set up the map of possible error messages.
        std::string componentName("lkmLoader");
        std::string first("The " + componentName + " component (");
        std::string second(") could not be instanciated. This is not a "
            "fatal error. Please check: "
            "1. The CDB entry for the lkm file points to a valid and "
            "existing file. "
            "2. No kernel modules have been loaded before. "
            "3. The real-time computer did not suffer from a kernel Oops.\n");
        std::string errorString;

        std::string hostName("DMC");
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
            errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC2";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
            errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC3";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
            errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC4";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
            errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "LMC";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
            errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }


        componentName = "rtlog";
        first = "The " + componentName + " component (";
        second = ") could not be "
            "instanciated. This is not a fatal error but real time logs will "
            "not be visible for this computer!\n";

        hostName = "DMC";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC2";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC3";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC4";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "LMC";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }


        componentName = "ArrayTime";
        first = "The " + componentName + " component (";
        second = ") could "
            "not be instanciated. This is not a fatal error but this computer "
            "will not run with synchronised time.  This can impact phase "
            "stability/fringes!\n";

        hostName = "DMC";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC2";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC3";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "DMC4";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }

        hostName = "LMC";
        try
        {
            checkHostName(hostName);
            errorString = first + hostName + second;
            errorMessages.insert(
                std::make_pair("CONTROL/" + hostName + "/" + componentName,
                    errorString));
        }
        catch(const ControlExceptions::IllegalParameterErrorEx& ex)
        {
            std::ostringstream msg;
            msg << "The host "
                << hostName
                << " is not configured.  Ignoring it.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
    }
    catch(...)
    {
        const std::string msg("Cannot initialise the error message map. It "
            "will not be possible to retrieve clear text error messages for "
            "this component.");
        LOG_TO_OPERATOR(LM_WARNING, msg);
    }
}

void Control::CentralLOImpl::initValidHostNames()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    validHostNames.push_back("DMC");
    validHostNames.push_back("DMC2");
    ///
    /// TODO
    /// Thomas, Aug 26, 2009
    /// Activate the DMCs
    ///
//    validHostNames.push_back("DMC3");
//    validHostNames.push_back("DMC4");
    validHostNames.push_back("LMC");
}

Control::CentralLOImpl::~CentralLOImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void Control::CentralLOImpl::checkHostName(const std::string& host,
    bool logException) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Try to find the host name in the validHostNames vector.
    //
    // An exception will be thrown if the host name cannot be found in the list
    // of valid host names.
    std::vector< std::string >::const_iterator pos(
        std::find(validHostNames.begin(), validHostNames.end(), host));
    if(pos == validHostNames.end())
    {
        ControlExceptions::IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The given host name ("
            << host
            << ") is invalid!";
        ex.addData("Detail", output.str());

        if(logException == true)
        {
            ex.log();
        }

        throw ex.getIllegalParameterErrorEx();
    }
}

/// Sole purpose: release component references.
void Control::CentralLOImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    for(HostMapType::iterator iter(hostReferencesMap.begin());
        iter != hostReferencesMap.end(); ++iter)
    {
        if(isAmbManagerRunning((*iter).first.c_str()) == true)
        {
            if((*iter).second.ambManagerRef_sp.isNil() == false)
             {
                 try
                 {
                     (*iter).second.ambManagerRef_sp->disableAccess();
                 }
                 catch(...)
                 {
                    std::ostringstream msg;
                    msg << "Caught an unexpected exception, ignoring it.";
                    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                 }
             }
        }
    }

    Control::HardwareControllerImpl::cleanUp();

    for(HostMapType::iterator iter(hostReferencesMap.begin());
        iter != hostReferencesMap.end(); ++iter)
    {
        stopRtSystemDmcLmc((*iter).first.c_str());
    }
}

void Control::CentralLOImpl::controllerShutdown()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    for(HostMapType::iterator iter(hostReferencesMap.begin());
        iter != hostReferencesMap.end(); ++iter)
    {
        if(isAmbManagerRunning((*iter).first.c_str()) == true)
        {
            if((*iter).second.ambManagerRef_sp.isNil() == false)
             {
                 try
                 {
                     (*iter).second.ambManagerRef_sp->disableAccess();
                 }
                 catch(...)
                 {
                    std::ostringstream msg;
                    msg << "Caught an unexpected exception, ignoring it.";
                    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                 }
             }
        }
    }

    Control::HardwareControllerImpl::controllerShutdown();

    for(HostMapType::iterator iter(hostReferencesMap.begin());
        iter != hostReferencesMap.end(); ++iter)
    {
        stopRtSystemDmcLmc((*iter).first.c_str());
    }
}

void Control::CentralLOImpl::controllerOperational()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // 1. Start the lkmLoader and rtlog on the DMC[1-4] & LMC computers.
    // 2. Start things on DMCs and LMC.
    hostReferencesMapMutex.acquire();
    for(std::vector< std::string >::iterator iter(validHostNames.begin());
        iter != validHostNames.end(); ++iter)
    {
        std::ostringstream msg;
        msg << "Trying to init host "
            << (*iter)
            << ".";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        getInitComponentsForDmcLmc(*iter);
    }
    hostReferencesMapMutex.release();

    Control::HardwareControllerImpl::controllerOperational();

    // Now synchronise the ArrayTime components on each of the remote computers.
    for(std::vector< std::string >::iterator iter(validHostNames.begin());
        iter != validHostNames.end(); ++iter)
    {
        try
        {
            doSynchroniseRemoteArrayTime(*iter);
        }
        catch(const ControlExceptions::InvalidRequestExImpl& ex)
        {
            ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.log();
        }
    }
}

void Control::CentralLOImpl::initRtSystemDmcLmc(const char* hostname)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string host(boost::algorithm::to_upper_copy(
        std::string(hostname)));
    checkHostName(host, true);

    try
    {
        ACE_Guard< ACE_Mutex > guard(hostReferencesMapMutex);
        getInitComponentsForDmcLmc(host);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
    }
}

void Control::CentralLOImpl::synchroniseRemoteArrayTime()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::vector< ControlExceptions::InvalidRequestExImpl > errorTrace;

    for(std::vector< std::string >::iterator iter(validHostNames.begin());
        iter != validHostNames.end(); ++iter)
    {
        try
        {
            doSynchroniseRemoteArrayTime(*iter);
        }
        catch(ControlExceptions::InvalidRequestExImpl& ex)
        {
            errorTrace.push_back(ex);
        }
    }

    if(errorTrace.empty() == false)
    {
        for(std::vector<
            ControlExceptions::InvalidRequestExImpl >::iterator iter(
                errorTrace.begin()); iter != errorTrace.end(); ++iter)
        {
            (*iter).log();
        }
    }
}

void Control::CentralLOImpl::doSynchroniseRemoteArrayTime(
    const std::string& host)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        checkHostName(host);
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex;
    }

    // Fetch the already available ArrayTime reference for the host.
    ACE_Guard< ACE_Mutex > guard(hostReferencesMapMutex);
    HostMapType::iterator iter(hostReferencesMap.find(host));
    if(iter == hostReferencesMap.end())
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream out;
        out << "No ArrayTime component available for host "
            << host
            << ".  Will not synchronise the time!";
        ex.addData("Detail", out.str());
        ex.log();
        throw ex;
    }

    // Get a Control::TimeSource reference.
    ///
    /// TODO
    /// Thomas, Aug 31, 2009
    /// Remove the TimeSource reference stuff as soon as
    /// ArrayTime::synchronizeTime does not require a TimeSource anymore.
    ///
    Control::TimeSource_var timeSource(Control::TimeSource::_nil());
    try
    {
        timeSource = cs->getComponentNonSticky< Control::TimeSource >(
            "CONTROL/AOSTiming/TimeSource");
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream out;
        out << "Could not get the TimeSource component reference. "
            "Will not synchronise the ArrayTime on "
            << host
            << ".";
        nex.addData("Detail", out.str());
        nex.log();
        throw nex;
    }

    try
    {
        (*iter).second.arrayTimeRef_sp->synchronizeTime(timeSource);
    }
    catch(const CorrErr::ArrayTimeInternalEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream out;
        out << "The ArrayTime component on host "
            << host
            << " threw an exception. Will not synchronise the ArrayTime on it.";
        nex.addData("Detail", out.str());
        nex.log();
        throw nex;
    }
    catch(const CorrErr::RoundTripTimeErrorEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream out;
        out << "The ArrayTime component on host "
            << host
            << " threw an exception. Will not synchronise the ArrayTime on it.";
        nex.addData("Detail", out.str());
        nex.log();
        throw nex;
    }
    catch(const CorrErr::SynchronizeTimeOutEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream out;
        out << "The ArrayTime component on host "
            << host
            << " threw an exception. Will not synchronise the ArrayTime on it.";
        nex.addData("Detail", out.str());
        nex.log();
        throw nex;
    }
}

void Control::CentralLOImpl::stopRtSystemDmcLmc(const char* hostname)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string host(boost::algorithm::to_upper_copy(
        std::string(hostname)));
    checkHostName(host);

    ACE_Guard< ACE_Mutex > guard(hostReferencesMapMutex);
    HostMapType::iterator iter(hostReferencesMap.find(host));
    if(iter == hostReferencesMap.end())
    {
        return;
    }

    if((*iter).second.ambManagerRef_sp.isNil() == false)
    {
        (*iter).second.ambManagerRef_sp->disableAccess();
        (*iter).second.ambManagerRef_sp.release();
    }

    if((*iter).second.arrayTimeRef_sp.isNil() == false)
    {
        (*iter).second.arrayTimeRef_sp.release();
    }

    if((*iter).second.rtLogRef_sp.isNil() == false)
    {
        (*iter).second.rtLogRef_sp.release();
    }

    if((*iter).second.lkmLoaderRef_sp.isNil() == false)
    {
        (*iter).second.lkmLoaderRef_sp.release();
    }
}

void Control::CentralLOImpl::getInitComponentsForDmcLmc(
    const std::string& host)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string componentName("CONTROL/" + host + "/lkmLoader");

    HostMapType::iterator iter(hostReferencesMap.find(host));
    if(iter == hostReferencesMap.end())
    {
        // There is no entry of that kind in the host references map yet.
        // Create one.
        HostReferences empty;
        hostReferencesMap.insert(HostMapValuePair(host, empty));

        // Update the iterator.
        iter = hostReferencesMap.find(host);
    }

    try
    {
        if((*iter).second.lkmLoaderRef_sp.isNil() == false)
        {
            std::ostringstream msg;
            msg << "The lkmLoader component ("
                << componentName
                << ") has already been activated.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
        else
        {
            // Get Reference to the lkmLoader component.
            (*iter).second.lkmLoaderRef_sp =
                getComponentReference< ACS_RT::lkmLoader >(componentName);

            // Clear error for this component.
            setErrorState(componentName, false);
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the lkmLoader could not be started up.
        // Put this component into degraded state and log the exception.
        if(getState() != Control::HardwareController::Shutdown)
        {
            setControllerState(Control::HardwareController::Degraded);
        }

        setErrorState(componentName, true);
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorMessages[componentName]);
        nex.log();
    }

    // Get a reference to the rtlog component.
    componentName = "CONTROL/" + host + "/rtlog";
    try
    {
        if((*iter).second.rtLogRef_sp.isNil() == false)
        {
            std::ostringstream msg;
            msg << "The rtLog component ("
                << componentName
                << ") has already been activated.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
        else
        {
            (*iter).second.rtLogRef_sp =
                getComponentReference< ACS_RT::rtlog >(componentName);

            // Clear error for this component.
            setErrorState(componentName, false);
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when rtlog could not be started up. Put
        // this component into degraded state and log the exception.
        if(getState() != Control::HardwareController::Shutdown)
        {
            setControllerState(Control::HardwareController::Degraded);
        }

        setErrorState(componentName, true);
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorMessages[componentName]);
        nex.log();
    }

    // Get a reference to the ArrayTime component.
    componentName = "CONTROL/" + host + "/ArrayTime";
    try
    {
        if(CORBA::is_nil(&*((*iter).second.arrayTimeRef_sp)) == false)
        {
            std::ostringstream msg;
            msg << "The ArrayTime component ("
                << componentName
                << ") has already been activated.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        }
        else
        {
            (*iter).second.arrayTimeRef_sp =
                getComponentReference< Correlator::ArrayTime >(componentName);

            // Clear error for this component.
            setErrorState(componentName, false);
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when rtlog could not be started up. Put
        // this component into degraded state and log the exception.
        if(getState() != Control::HardwareController::Shutdown)
        {
            setControllerState(Control::HardwareController::Degraded);
        }

        setErrorState(componentName, true);
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorMessages[componentName]);
        nex.log();
    }
}

void Control::CentralLOImpl::doStartAmbManager(const std::string& host)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string componentCode("ambManagerImpl");
    const std::string componentType("IDL:alma/Control/AmbManager:1.0");
    const std::string componentName("CONTROL/" + host + "/AmbManager");
    const std::string containerName("CONTROL/" + host + "/cppContainer");

    maci::ComponentSpec spec;
    spec.component_name = componentName.c_str();
    spec.component_code = componentCode.c_str();
    spec.component_type = componentType.c_str();
    spec.container_name = containerName.c_str();

    HostMapType::iterator iter(hostReferencesMap.find(host));
    if(iter == hostReferencesMap.end())
    {
        // There is no entry of that kind in the host references map yet.
        // Create one.
        HostReferences empty;
        hostReferencesMap.insert(HostMapValuePair(host, empty));

        // Update the iterator.
        iter = hostReferencesMap.find(host);
    }

    // Thomas, Jul 23, 2008
    // Okay, okay, I know. Two nested try/catch blocks is way uncool but the
    // C++ standard lacks a catch for collected exceptions.
    try
    {
        try
        {
            (*iter).second.ambManagerRef_sp =
                cs->getDynamicComponentSmartPtr< Control::AmbManager >(
                    spec, false);
        }
        catch(const maciErrType::NoPermissionExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::IncompleteComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::InvalidComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const
            maciErrType::ComponentSpecIncompatibleWithActiveComponentExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "The AmbManager component could not be "
                "started.  Are the real-time Kernel modules loaded?");
            throw nex;
        }
        catch(...)
        {
            maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", "An unexpected exception has been caught. "
                "Developers: check the source code of maci::ContainerServices::"
                "getDynamicComponentSmartPtr(...)!");
            throw ex;
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the AmbManager component could not be started
        // up. Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);

        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The component called '"
            << componentName
            << "' could not be started. This is not a fatal error. Direct "
            "AMB access on host '"
            << host
            << "' will not be available.  Component name = "
            << componentName
            << ", component code = "
            << componentCode
            << ", component type = "
            << componentType
            << ", container name = "
            << containerName
            << ".";
        nex.addData("Detail", msg.str());
        throw nex;
    }
}

void Control::CentralLOImpl::startAmbManager(const char* hostname)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if((currentState_m != Control::HardwareController::Operational)
    && (currentState_m != Control::HardwareController::Degraded))
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Controller is not in Operational or in Degraded "
            "state.");
        ex.log();
        throw ex.getINACTErrorEx();
    }

    const std::string host(boost::algorithm::to_upper_copy(
        std::string(hostname)));

    if(isAmbManagerRunning(host.c_str()) == true)
    {
        ControlExceptions::DeviceBusyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
        std::ostringstream output;
        output << "The AmbManager for the computer '"
            << tmpName.in()
            << "' is already running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    try
    {
        ACE_Guard< ACE_Mutex > guard(hostReferencesMapMutex);
        doStartAmbManager(host);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getInvalidRequestEx();
    }
}

void Control::CentralLOImpl::stopAmbManager(const char* hostname)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string host(boost::algorithm::to_upper_copy(
        std::string(hostname)));

    if(isAmbManagerRunning(host.c_str()) == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The AmbManager for this computer '"
            << host
            << "' is not running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    ACE_Guard< ACE_Mutex > guard(hostReferencesMapMutex);
    HostMapType::iterator iter(hostReferencesMap.find(host));
    if(iter == hostReferencesMap.end())
    {
        return;
    }

    (*iter).second.ambManagerRef_sp->disableAccess();
    (*iter).second.ambManagerRef_sp.release();
}

bool Control::CentralLOImpl::isAmbManagerRunning(const char* hostname)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string host(boost::algorithm::to_upper_copy(
        std::string(hostname)));
    checkHostName(host, true);

    ACE_Guard< ACE_Mutex > guard(hostReferencesMapMutex);
    HostMapType::iterator iter(hostReferencesMap.find(host));
    if(iter == hostReferencesMap.end())
    {
        return false;
    }

    return (!(*iter).second.ambManagerRef_sp.isNil());
}

void Control::CentralLOImpl::setErrorState(const std::string& error,
    const bool errorIsActive)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(errorIsActive == false)
    {
        currentErrors.remove(error);
    }
    else
    {
        currentErrors.push_back(error);
    }

    setError(getErrors());
}


std::string Control::CentralLOImpl::getErrors()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string errorString;

    for(std::list< std::string >::const_iterator iter(currentErrors.begin());
        iter != currentErrors.end(); ++iter)
    {
        errorString.append(errorMessages[(*iter)]);
    }

    return errorString;
}

Control::DeviceNameList* Control::CentralLOImpl::getPhotonicReferences()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    //    struct DeviceNameMap {
    //      string ReferenceName; //! The (short) reference name
    //      string FullName;      //! The full name of the component

    // Get subdevice list.
    Control::DeviceNameList_var subdeviceList(getSubdeviceList());

    // CORBA sequences do not allow to erase or insert elements.  Therefore
    // create an intermediate vector that will contain the data until the
    // to be returned sequence has been created from it.
    typedef std::vector< std::pair< std::string, std::string > > DeviceNameVec;
    DeviceNameVec deviceNames;
    for(unsigned int i(0U); i < subdeviceList->length(); ++i)
    {
        deviceNames.push_back(std::make_pair(
            std::string(subdeviceList[i].ReferenceName),
            std::string(subdeviceList[i].FullName)));
    }

    // Intermediate result vector.
    DeviceNameVec photonicReferences;

    // Find all occurences in the list that contain
    // CONTROL/CentralLO/PhotonicReference? and push them into the intermediate
    // result vector.
    const std::string searchFor("CONTROL/CentralLO/PhotonicReference");
    const std::string::size_type size(searchFor.size() + 1);
    std::string fullName;
    for(DeviceNameVec::const_iterator iter(deviceNames.begin());
        iter != deviceNames.end(); ++iter)
    {
        if(((*iter).second.find(searchFor) == 0)
        && ((*iter).second.size() == size))
        {
            photonicReferences.push_back(*iter);
        }
    }

    // Now create the result list with max. photonicReferences.size() elements
    // and have it resized to photonicReferences.size() elements.
    Control::DeviceNameList_var photonicReferencesList(
        new Control::DeviceNameList);
    photonicReferencesList->length(photonicReferences.size());
    unsigned int i(0U);
    for(DeviceNameVec::const_iterator iter(photonicReferences.begin());
        iter != photonicReferences.end(); ++iter, ++i)
    {
        photonicReferencesList[i].ReferenceName = CORBA::string_dup(
            (*iter).first.c_str());
        photonicReferencesList[i].FullName = CORBA::string_dup(
            (*iter).second.c_str());
    }

    return photonicReferencesList._retn();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::CentralLOImpl)
