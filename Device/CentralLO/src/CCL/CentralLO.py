#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#


import CCL.logging
import CCL.HardwareController
import CCLExceptionsImpl
import Acspy.Common.ErrorTrace


class CentralLO(CCL.HardwareController.HardwareController):
    def __init__(self, stickyFlag = False):
        '''
        The constructor creates a CCL.CentralLO object. Which communicates
        with the CentralLO component in the ARTM computer.
        '''
        CCL.HardwareController.HardwareController.__init__(
           self, "CONTROL/CentralLO", stickyFlag)

        self.__logger = CCL.logging.getLogger()
        self.__logger.logDebug("Creating CentralLO CCL Object.")

    def __del__(self):
        CCL.HardwareController.HardwareController.__del__(self)

    def initRtSystemDmcLmc(self, host = None):
        '''
        Start the lkmLoader and the rtLogger components on LMC, DMC1-4. 
        '''
        if host == None:
            ex = CCLExceptionsImpl.BadParameterErrorExImpl()
            ex.setData("Detail", "You must provide a host name!  Valid names "
                "are DMC, DMC2, DMC3, DMC4 and LMC.")
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);
            raise ex

        return self._HardwareController__controller.initRtSystemDmcLmc(host)

    def stopRtSystemDmcLmc(self, host = None):
        '''
        Stop the lkmLoader and the rtLogger components on LMC, DMC1-4. 
        '''
        if host == None:
            ex = CCLExceptionsImpl.BadParameterErrorExImpl()
            ex.setData("Detail", "You must provide a host name!  Valid names "
                "are DMC, DMC2, DMC3, DMC4 and LMC.")
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);
            raise ex

        return self._HardwareController__controller.initRtSystemDmcLmc(host)

    def isAmbManagerRunning(self, host = None):
        '''
        Check if the AmbManager component for CentralLO instance is up and
        running.
        '''
        if host == None:
            ex = CCLExceptionsImpl.BadParameterErrorExImpl()
            ex.setData("Detail", "You must provide a host name!  Valid names "
                "are DMC, DMC2, DMC3, DMC4 and LMC.")
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);
            raise ex

        return self._HardwareController__controller.isAmbManagerRunning(host)

    def startAmbManager(self, host = None):
        '''
        Start the AmbManager component for the CentralLO rack. If the CentralLO
        is not in controllerOperational state, the start will fail with an
        exception.
        '''
        if host == None:
            ex = CCLExceptionsImpl.BadParameterErrorExImpl()
            ex.setData("Detail", "You must provide a host name!  Valid names "
                "are DMC, DMC2, DMC3, DMC4 and LMC.")
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);
            raise ex

        self._HardwareController__controller.startAmbManager(host)

    def stopAmbManager(self, host = None):
        '''
        Stop the AmbManager component.
        '''
        if host == None:
            ex = CCLExceptionsImpl.BadParameterErrorExImpl()
            ex.setData("Detail", "You must provide a host name!  Valid names "
                "are DMC, DMC2, DMC3, DMC4 and LMC.")
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);
            raise ex

        self._HardwareController__controller.stopAmbManager(host)

    def getSubdeviceList(self):
        '''
        Return a list of all devices installed at Antenna
        '''
        device_list = []
        device_map = self._HardwareController__controller.getSubdeviceList()
        for device in device_map:
            dev = str(device)
            device_list.append(dev.split('\'')[1])
            print dev.split('\'')[1]

        return device_list

    def getPhotonicReferences(self):
        '''
        Return then list of available Photonic References.
        '''
        return self._HardwareController__controller.getPhotonicReferences()

    def synchroniseRemoteArrayTime(self):
        '''
        Try to synchronise the ArrayTime components on DMC* and LMC.
        '''
        return self._HardwareController__controller.synchroniseRemoteArrayTime()
