/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2009
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id$
 *
 * who      when     what
 * tjuerges  Sep 8, 2009  created
 */


#include <CentralLOTCLOImpl.h>
// For AUTO_TRACE.
#include <loggingMACROS.h>
// For maci::ContainerServices
#include <acsContainerServices.h>


Control::CentralLOTCLOImpl::CentralLOTCLOImpl(const ACE_CString& name,
    maci::ContainerServices* _cs):
    Control::CentralLOImpl(name, _cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    validHostNames.clear();
    errorMessages.clear();

    initValidHostNames();
    initErrorMap();
}

Control::CentralLOTCLOImpl::~CentralLOTCLOImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void Control::CentralLOTCLOImpl::initValidHostNames()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    validHostNames.push_back("DMC");
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::CentralLOTCLOImpl)
