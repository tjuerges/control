/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.[device name];

import alma.Control.device.gui.common.panels.DevicePanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized in panels and tabs to group related data together. 
 * 
 * This class contains the Detail data. It also provides a Show/Hide button for the data.
 * TODO: replace [device name] with the proper module name
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class DetailsPanel extends [device name]Panel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    JTabbedPane tabs;

    public DetailsPanel(Logger logger, DevicePM dPM) {
    	super(logger, dPM);
    }

    protected void buildPanel()
    {
        logger.entering("DetailPanel", "buildPanel");

        this.setBorder(
                BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Details"),
                    BorderFactory.createEmptyBorder(1,1,1,1)
                )
        );

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.NORTHWEST;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=1;		//Setup proper sizing for the button
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;
        add(new ShowHideButton(), c);
        c.weighty=100;		//Have the tabbed panel take up the rest of the space.


        tabs = new JTabbedPane();
        //TODO Add the various details tabs here.
        //Sample: add a panel without scroll bars
        tabs.add("Tab1", new PanelOne(logger, devicePM));
        //Sample: add a panel with auto show/hide scroll bars
        tabs.add("Tab2", new JScrollPane(new PanelTwo(logger, devicePM)));

        //Add the ambsi pane; every device should have one...
        tabs.add("AMBSI", new JScrollPane(new AmbsiPanel(logger, devicePM)));

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.weighty=100;
        c.weightx=100;

        c.gridy++;
        add(tabs, c);

        this.setVisible(true);
        logger.exiting("DetailPanel", "buildPanel");
    }

    private class ShowHideButton
    extends JButton
    implements ActionListener {

        private String hideActionCommand;
        private String hideButtonText;
        private String showActionCommand;
        private String showButtonText;

        public ShowHideButton() {
            hideButtonText = "Hide Details";
            showButtonText = "Show Details";
            hideActionCommand = "HIDE_DETAILS";
            showActionCommand = "SHOW_DETAILS";

            setText(hideButtonText);
            setActionCommand(hideActionCommand);
            addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand() == hideActionCommand) {
                tabs.setVisible(false);
                setText(showButtonText);
                setActionCommand(showActionCommand);
            } else if (e.getActionCommand() == showActionCommand) {
                tabs.setVisible(true);
                setText(hideButtonText);
                setActionCommand(hideActionCommand);
            }
        }
    }
}

//
// O_o
