/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.[device name].panels;

import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.[device name].presentationModels.DevicePM;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * This class contains template for a tab panel.
 * TODO: Replace [device name] with the proper module name.
 * TODO: Replace TabbedPanel with the proper panel name
 * TODO: Replace PanelOne and PanelTwo with actual panel names.
 * TODO: Update this comment to reflect what the panel does.
 * 
 * Tabed panels are used to group related data together and separate unrelated data.  These are created 
 * as required for each GUI.
 * 
 * @author David Hunter      dhunter@nrao.edu
 * @author Scott Rankin      srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class TabbedPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    JTabbedPane tabs;

    public TabbedPanel(Logger logger, DevicePM dPM) {
    	super(logger, dPM);
        buildSubPanels(logger, dPM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM) {
      panelOne = new PanelOne (logger, aDevicePM);
      panelTwo = new PanelTwo (logger, aDevicePM);
    }

    protected void buildPanel()
    {
        setBorder(BorderFactory.createEtchedBorder(1));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weighty=100;
        c.weightx=100;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;

        tabs = new JTabbedPane();

        //TODO: Add the proper tabs here
        //Sample: add a panel without scroll bars
        tabs.add("Tab1", panelOne);
        //Sample: add a panel with auto show/hide scroll bars
        tabs.add("Tab2", new JScrollPane(panelTwo));
        
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        add(tabs, c);

        this.setVisible(true);
    }

    PanelOne panelOne;
    PanelTwo panelTwo;
}

//
// O_o

