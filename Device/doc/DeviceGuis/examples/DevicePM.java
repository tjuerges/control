/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.DeviceName;

import alma.Control.DeviceNameHelper;
import alma.Control.device.gui.common.DevicePresentationModel;
import alma.Control.device.gui.common.IControlPoint;
import alma.Control.device.gui.common.IMonitorPoint;
import alma.Control.gui.hardwaredevice.common.HardwareDeviceChessboardPresentationModel;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;



/**
 * Model the details of a device to be displayed in a user interface. 
 * 
 * @author  Scott Rankin  srankin@nrao.edu
 * @author  Steve Harrington  sharring@nrao.edu
 * @since   ALMA 5.0.3
 * 
 * TODO: Add public methods to move monitor points between short and long polling groups
 * TODO: Add public methods to get list of monitor points in short and long polling groups
 * TODO: Split out Generic code
 * TODO: Add ExcLog to all logged exceptions.
 */
public class DevicePM extends DevicePresentationModel {
    
    public static String DEVICE_NAME = "DeviceName0";

    /**
     * @param antennaName the name of the antenna containing this device.
     * @param services the OMC container services.
     */
    public DevicePM(String antennaName, PluginContainerServices services) {
        super(antennaName, services);
    }

    protected void createIcdControlPointPresentationModels() {
        logger.entering("DevicePM", "createIcdControlPointPresentationModels()");
        
        DeviceNameControlPointPresentationModel newCP;
        
        for (DeviceNameControlPoints controlPoint: DeviceNameControlPoints.values()) {
            if (Boolean.class == controlPoint.getTypeClass() || boolean.class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Boolean>(logger, this, controlPoint, boolean.class);
            else if (Boolean[].class == controlPoint.getTypeClass() || boolean[].class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Boolean[]>(logger, this, controlPoint, boolean.class);

            else if (Byte.class == controlPoint.getTypeClass() || byte.class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Byte>(logger, this, controlPoint, byte.class);
            else if (Byte[].class == controlPoint.getTypeClass() || byte[].class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Byte[]>(logger, this, controlPoint, byte.class);

            else if (Double.class == controlPoint.getTypeClass() || double.class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Double>(logger, this, controlPoint, double.class);
            else if (Double[].class == controlPoint.getTypeClass() || double[].class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Double[]>(logger, this, controlPoint, double.class);

            else if (Float.class == controlPoint.getTypeClass() || float.class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Float>(logger, this, controlPoint, float.class);
            else if (Float[].class == controlPoint.getTypeClass() || float[].class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Float[]>(logger, this, controlPoint, float.class);

            else if (Integer.class == controlPoint.getTypeClass() || int.class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Integer>(logger, this, controlPoint, int.class);
            else if (Integer[].class == controlPoint.getTypeClass() || int[].class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Integer[]>(logger, this, controlPoint, int.class);

            else if (Long.class == controlPoint.getTypeClass() || long.class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Long>(logger, this, controlPoint, long.class);
            else if (Long[].class == controlPoint.getTypeClass() || long[].class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Long[]>(logger, this, controlPoint, long.class);

            else if (Short.class == controlPoint.getTypeClass() || short.class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Short>(logger, this, controlPoint, short.class);
            else if (Short[].class == controlPoint.getTypeClass() || short[].class == controlPoint.getTypeClass())
                newCP = new DeviceNameControlPointPresentationModel<Short[]>(logger, this, controlPoint, short.class);

            else {
                if (controlPoint.getArrayLength() == 0) {
                    // This control point does not take parameters.
                    newCP = new DeviceNameControlPointPresentationModel(logger, this, controlPoint, null);
                } else {
                    logger.warning(
                        "DevicePM.createControlPointPresentationModels(): " +
                        "Unknown type found while creating new ControlPointPresentationModel: " +
                        controlPoint.getTypeClass()
                    );
                    newCP = null;
                }
            }
            controlPointPMs.put(controlPoint, newCP);
        }

        logger.exiting("DevicePM", "createIcdControlPointPresentationModels()");
    }

    protected void createIdlControlPointPresentationModels() {
        logger.entering("DevicePM", "createIdlControlPointPresentationModels()");
        
        DeviceNameIdlPresentationModelBuilder idlPMBuilder = new DeviceNameIdlPresentationModelBuilder(logger, this);
        idlPMBuilder.createControlPointPresentationModels();
        for (DeviceNameIdlControlPoints controlPoint: idlPMBuilder.controlPointPMs.keySet()) {
            controlPointPMs.put(controlPoint, idlPMBuilder.controlPointPMs.get(controlPoint));
        }
        
        logger.exiting("DevicePM", "createIdlControlPointPresentationModels()");        
    }
    
    protected void createIcdMonitorPointPresentationModels() {
        logger.entering("DevicePM", "createIcdMonitorPointPresentationModels()");
        
        // The newMP holds several types of presentation models.
        DeviceNameMonitorPointPresentationModel newMP;
        
        for (DeviceNameMonitorPoints monitorPoint: DeviceNameMonitorPoints.values()) {
            if (Boolean.class == monitorPoint.getTypeClass() || boolean.class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Boolean>(logger, this, monitorPoint);
            else if (Boolean[].class == monitorPoint.getTypeClass() || boolean[].class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Boolean[]>(logger, this, monitorPoint);

            else if (Byte.class == monitorPoint.getTypeClass() || byte.class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Byte>(logger, this, monitorPoint);
            else if (Byte[].class == monitorPoint.getTypeClass() || byte[].class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Byte[]>(logger, this, monitorPoint);

            else if (Double.class == monitorPoint.getTypeClass() || double.class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Double>(logger, this, monitorPoint);
            else if (Double[].class == monitorPoint.getTypeClass() || double[].class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Double[]>(logger, this, monitorPoint);

            else if (Float.class == monitorPoint.getTypeClass() || float.class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Float>(logger, this, monitorPoint);
            else if (Float[].class == monitorPoint.getTypeClass() || float[].class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Float[]>(logger, this, monitorPoint);

            else if (Integer.class == monitorPoint.getTypeClass() || int.class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Integer>(logger, this, monitorPoint);
            else if (Integer[].class == monitorPoint.getTypeClass() || int[].class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Integer[]>(logger, this, monitorPoint);

            else if (Long.class == monitorPoint.getTypeClass() || long.class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Long>(logger, this, monitorPoint);
            else if (Long[].class == monitorPoint.getTypeClass() || long[].class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Long[]>(logger, this, monitorPoint);

            else if (Short.class == monitorPoint.getTypeClass() || short.class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Short>(logger, this, monitorPoint);
            else if (Short[].class == monitorPoint.getTypeClass() || short[].class == monitorPoint.getTypeClass())
                newMP = new DeviceNameMonitorPointPresentationModel<Short[]>(logger, this, monitorPoint);

            else {
                logger.warning("DevicePM.createMonitorPointPresentationModels(): " +
                    "Unknown type found while creating new MonitorPointPresentationModel: " +
                    monitorPoint.getTypeClass()
                );
                newMP = null;
            }
            monitorPointPMs.put(monitorPoint, newMP);
        } 

        logger.exiting("DevicePM", "createIcdMonitorPointPresentationModels()");
    }

    protected void createIdlMonitorPointPresentationModels() {
        logger.entering("DevicePM", "createIdlMonitorPointPresentationModels()");
        
        DeviceNameIdlPresentationModelBuilder idlPMBuilder = new DeviceNameIdlPresentationModelBuilder(logger, this);
        idlPMBuilder.createMonitorPointPresentationModels();
        for (DeviceNameIdlMonitorPoints monitorPoint: idlPMBuilder.monitorPointPMs.keySet()) {
            monitorPointPMs.put(monitorPoint, idlPMBuilder.monitorPointPMs.get(monitorPoint));
        }
        
        logger.exiting("DevicePM", "createIdlMonitorPointPresentationModels()");        
    }
    
    protected void setComponentName() {
        logger.entering("DevicePM", "setComponentName");

        componentName = HardwareDeviceChessboardPresentationModel.getComponentNameForDevice(
            pluginContainerServices,
            antennaName,
            DEVICE_NAME);

        logger.exiting("DevicePM", "setComponentName");
    }
    
    protected void setDeviceComponent() {
        logger.entering("DevicePM", "setDeviceComponent");

        if (null == componentName) {
            setComponentName();
        }
        
        if (null == deviceComponent && null != componentName) {
            try {
                org.omg.CORBA.Object corbaObject = 
                    this.pluginContainerServices.getComponentNonSticky(componentName);
                this.deviceComponent = DeviceNameHelper.narrow(corbaObject);
                
                for (IControlPoint controlPoint: controlPointPMs.keySet())
                    if (null != controlPointPMs.get(controlPoint)) {    
                        controlPointPMs.get(controlPoint).setDeviceComponent(this.deviceComponent);
                    } else {
                        logger.warning(
                            "DevicePM.setDeviceComponent() " + 
                            "failed to find controlPointPM for " +
                            controlPoint
                        );                      
                    }
                for (IMonitorPoint monitorPoint: monitorPointPMs.keySet())
                    if (null != monitorPointPMs.get(monitorPoint)) {
                        monitorPointPMs.get(monitorPoint).setDeviceComponent(this.deviceComponent);
                    } else {
                        logger.warning(
                            "DevicePM.setDeviceComponent() " + 
                            "failed to find monitorPointPM for " +
                            monitorPoint
                        );
                    }
            } catch (AcsJContainerServicesEx ex) {
                logger.warning("DevicePM.setDeviceComponent() - " + 
                               "unable to obtain DeviceName component: "
                               + componentName);
                this.deviceComponent = null;

                for (IControlPoint controlPoint: controlPointPMs.keySet())
                    controlPointPMs.get(controlPoint).setDeviceComponent(null);
                for (IMonitorPoint monitorPoint: monitorPointPMs.keySet())
                    monitorPointPMs.get(monitorPoint).setDeviceComponent(null);
                ex.printStackTrace();
                // TODO: Should this exception be propagated up?
            }
        }

        logger.exiting("DevicePM", "setDeviceComponent");
    }

}

//
// O_o
        
