/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.[device name];
import alma.Control.device.gui.[device name].[device name]ControlPoints;
import alma.Control.device.gui.[device name].[device name]MonitorPoints;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanToggleWidget;
import alma.Control.device.gui.common.widgets.Status;

import java.util.logging.Logger;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;


/**
 * Hardware device GUI data is divided into Summary data and Detail data.  Summary data is always visible 
 * on the GUI.  Detail data is organized to keep related data together and separate unrelated data.
 * 
 * This class contains the Summary data.
 * TODO: Replace [device name] with the proper device name
 * 
 * @author Scott Rankin      srankin@nrao.edu
 * @author David Hunter      dhunter@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */
public class SummaryPanel extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;
    
    public SummaryPanel(Logger logger, [device name]DevicePresentationModel presentationModel) {
    	super(logger, presentationModel);
    }
    
    protected void buildPanel() {
        logger.entering("SummaryPanel", "buildPanel");
        this.setBorder(BorderFactory.createCompoundBorder( BorderFactory.createTitledBorder
                    (devicePM.getDeviceDescription()), BorderFactory.createEmptyBorder(1,1,1,1)));
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor=GridBagConstraints.CENTER;
        c.weightx=100;
        c.weighty=100;
        c.gridx=1;
        c.gridy=0;
        addLeftLabel("Widgets go here", c);
        //TODO: Add widgets
        this.setVisible(true);
        logger.exiting("SummaryPanel", "buildPanel");
    }

    /*
     * A simple helper function that allows you to add a label to the left of the current location with
     * grid bag layout.
     */
    private void addLeftLabel(String s, GridBagConstraints c) {
        c.gridx--;
        add(new JLabel(s), c);
        c.gridx++;
    }
}

//
// O_o
