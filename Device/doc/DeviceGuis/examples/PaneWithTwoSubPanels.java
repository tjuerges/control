/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.device.gui.[device name].panels;

import alma.Control.device.gui.[device name].IcdMonitorPoints;
import alma.Control.device.gui.[device name].presentationModels.DevicePM;
import alma.Control.device.gui.common.panels.DevicePanel;
import alma.Control.device.gui.common.widgets.IntegerMonitorPointWidget;
import alma.Control.device.gui.common.widgets.BooleanControlPointWidget;
import alma.Control.device.gui.common.widgets.BooleanMonitorPointWidget;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

/**
 * Hardware device GUI data is divided into Summary data and Detail data. Summary data is always visible on
 * the GUI. Detail data is organized in panels and tabs to group related data together.
 * 
 * TODO: Replace [device name] with the proper device name
 * TODO: Replace PaneWithTwoSubPanels with the proper name
 * TODO: Update this comment to reflect what this pane does.
 * 
 * Tabbed panels are used to group related data together and separate unrelated data. These are created as
 * required for each GUI.
 * 
 * @author David Hunter dhunter@nrao.edu
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 5.0.3
 */

public class PaneWithTwoSubPanels extends DevicePanel {

    // TODO - serial version UID
    private static final long serialVersionUID = 1L;

    public PaneWithTwoSubPanels(Logger logger, DevicePM aDevicePM) {
        super(logger, aDevicePM);
        buildSubPanels(logger, aDevicePM);
        buildPanel();
    }

    private void buildSubPanels(Logger logger, DevicePM aDevicePM){
        //TODO: rename sub-panels as appropriate
        subPanel1Name = new SubPanel1Name(logger, aDevicePM);
        subPanel2Name = new SubPanel2Name(logger, aDevicePM);
    }

    protected void buildPanel() {
        setBorder(BorderFactory.createEtchedBorder(1));
//        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                BorderFactory.createEmptyBorder(1, 1, 1, 1)));

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.BOTH;
        c.weighty=100;
        c.weightx=100;
        c.gridx = 0;
        c.gridy = 0;

        add(subPanel1Name, c);
        c.gridy++;
        add(subPanel2Name, c);

        setVisible(true);
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class SubPanel1Name extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public SubPanel1Name(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();

            // Add all the points in a single column, starting at the top, with the labels on the left.
            c.gridx = 1;
            c.gridy = 0;

            //The first widget
            addLeftLabel("myWidget", c);
            //TODO: Add widget here

            //Move to the next row and add another widget
            c.gridy++;
            addLeftLabel("Another Widget", c);
            //TODO: Add widget here
        }
    }

    /*
     * A template for sub-panels. Copy for the number of needed sub-panels, renaming as needed.
     * TODO: Add M&C widgets
     */
    private class SubPanel2Name extends DevicePanel {

        // TODO - serial version UID
        private static final long serialVersionUID = 1L;

        public SubPanel2Name(Logger logger, DevicePM aDevicePM) {
            super(logger, aDevicePM);
            buildPanel();
        }

        protected void buildPanel() {
            setBorder(BorderFactory.createEtchedBorder(1));
//          setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("PanelTitle"),
//                  BorderFactory.createEmptyBorder(1, 1, 1, 1)));
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            c.gridx = 1;
            c.gridy = 0;

            //The first widget
            addLeftLabel("myWidget", c);
            //TODO: Add widget here

            //Move to the next row and add another widget
            c.gridy++;
            addLeftLabel("Another Widget", c);
            //TODO: Add widget here
        }
    }

    SubPanel1Name subPanel1Name;
    SubPanel2Name subPanel2Name;
}

//
// O_o
