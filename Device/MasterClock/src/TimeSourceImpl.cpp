//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <TimeSourceImpl.h>

#include <sstream>
#include <LogToAudience.h>

//Used to access other components, activate "OffShoot"s, etc.
#include <acsContainerServices.h>

#include <maciErrType.h>
#include <TETimeUtil.h>

// For component references.
#include <CRDC.h>

// Alarm system stuff
#include <vector>
#include <controlAlarmSender.h>

// For std::abs(LL)
#include <cmath>


//#define TIMESOURCE_DEBUG 1


Control::TimeSourceImpl::TimeSourceImpl(const ACE_CString& name,
    maci::ContainerServices* _cs):
    acscomponent::ACSComponentImpl(name, _cs),
    myName(name.c_str()),
    cs(_cs),
    timeOffsetThread_p(0),
    alarmSender(0),
    OneTwentyFiveMHz(125000000LL),
    Allowed125MHzJitter(10LL),
    AllowedAccumulated125MHzJitter(10000LL),
    crdResetTime(0ULL),
    previousMaserVsGpsCounter(0LL),
    accumulated125MHzJitter(0LL),
    calculate125MHzJitterFirstRun(true)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

Control::TimeSourceImpl::~TimeSourceImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void Control::TimeSourceImpl::execute()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    acscomponent::ACSComponentImpl::execute();

    initialiseAlarmSystem();

    std::ostringstream msg;

    std::string lruType("CONTROL/AOSTiming/CRD");
    try
    {
        m_crd_sp = cs->getComponentNonStickySmartPtr< Control::CRD >(
            lruType.c_str());

        msg << "Reference to "
            << lruType
            << " component successfully retrieved.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        Control::TimeSourceImpl::alarmSender->activateAlarm(CrdNotAvailable);

        acsErrTypeLifeCycle::LifeCycleExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        msg.str("");
        msg << "Got no reference for the "
            << lruType
            << " component.";

        nex.addData("Detail", msg.str());
        nex.log();
        throw nex;
    }

    try
    {
        crdResetTime = getCrdResetTime();
        msg.str("");
        msg << "Read RESET_TIME = "
            << crdResetTime
            << " from the CRD hardware.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        // No need to check if the CRD is set, because the check has been done
        // when the thread execution started and no change is expected during
        // the brief time period until I get to this line.
        alarmSender->activateAlarm(CrdNotAvailable);

        msg.str("");
        msg << "Could not retrieve the RESET_TIME value from the "
            "CRD hardware because a CAN bus error happened.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        // No need to check if the CRD is set, because the check has been done
        // when the thread execution started and no change is expected during
        // the brief time period until I get to this line.
        alarmSender->activateAlarm(CrdNotAvailable);

        msg.str("");
        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        msg << "Could not retrieve the RESET_TIME value from the "
            "CRD because the CRD component is not in the operational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());
    }

    // To be on the safe side, reset the previous and the accumulated jitter.
    previousMaserVsGpsCounter = 0LL;
    accumulated125MHzJitter = 0LL;
    calculate125MHzJitterFirstRun = true;

    // Create the thread which monitors the drift of the 125[MHz] reference
    // clock.
    Control::TimeSourceImpl* selfPtr(this);
    timeOffsetThread_p = getContainerServices()->getThreadManager()->
        create< Control::Crd125MHzVsGpsOffsetThread, Control::TimeSourceImpl* >(
            std::string(myName + "TimeOffsetThread").c_str(), selfPtr);

    if(timeOffsetThread_p != 0)
    {
        timeOffsetThread_p->setSleepTime(60ULL * TETimeUtil::ACS_ONE_SECOND);
        timeOffsetThread_p->resume();
    }
    else
    {
        Control::TimeSourceImpl::alarmSender->activateAlarm(NoMonitorThread);

        msg.str("");
        msg << "Failed to create the thread which monitors the Maser counter "
            "offset.  Will continue without it.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
}

void Control::TimeSourceImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(timeOffsetThread_p != 0)
    {
        getContainerServices()->getThreadManager()->stop(
            timeOffsetThread_p->getName());
        getContainerServices()->getThreadManager()->terminate(
            timeOffsetThread_p->getName());
        delete timeOffsetThread_p;
        timeOffsetThread_p = 0;
    }

    cleanUpAlarmSystem();

    acscomponent::ACSComponentImpl::cleanUp();
}

void Control::TimeSourceImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSender.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    {
        Control::AlarmInformation ai;
        ai.alarmCode = CrdNotAvailable;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = CannotDetermineTimeAtNextTE;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = Allowed125MHzJitterExceeded;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = AllowedAccumulated125MHzJitterExceeded;
        alarmInformation.push_back(ai);
    }

    alarmSender->initializeAlarms("TimeSource", "CentralLO",
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void Control::TimeSourceImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}

ACS::Time Control::TimeSourceImpl::timeAtNextTE()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACS::Time timeStamp(0ULL);
    ACS::Time crdTicksSinceReset(0ULL);
    ACS::Time time(0ULL);

    try
    {
        // Make sure that the CRD RESET_TIME is available even if it was
        // not when this component failed to read it during execute().
        if(crdResetTime == 0ULL)
        {
            if(checkCrdAvailability() == false)
            {
                LOG_TO_OPERATOR(LM_ERROR, "The CRD RESET_TIME cannot be "
                    "read from the CRD hardware because the monitor request "
                    "thre an exception.  Check the CRD component and try "
                    "again!");

                ControlExceptions::MasterClockErrorExImpl ex(__FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
                ex.addData("Detail", "Could not retrieve the RESET_TIME "
                    "value from the CRD hardware because the CRD component or "
                    "the CRD hardware is not operational.");
                ex.log();

                throw ex;
            }
            else
            {
                crdResetTime = getCrdResetTime();
            }
        }

        #ifdef TIMESOURCE_DEBUG
        std::ostringstream msg;
        msg << "Time stamp of the master counter reset = "
            << crdResetTime
            << "[100ns].";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif

        // Read the number of 125[MHz] ticks since reset of the CRD.
        try
        {
            crdTicksSinceReset = m_crd_sp->GET_MASER_COUNTER(timeStamp);
        }
        catch(const ControlExceptions::CAMBErrorEx& ex)
        {
            ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", "Could not retrieve the MASER_COUNTER value "
                "from the CRD hardware because a CAN bus error happened.");
            throw nex;
        }
        catch(const ControlExceptions::INACTErrorEx& ex)
        {
            ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__,
                __LINE__, __PRETTY_FUNCTION__);
            nex.addData("Detail", "Could not retrieve the MASER_COUNTER value "
                "from the CRD because the CRD component is not in the "
                "operational state.");
            throw nex;
        }

        #ifdef TIMESOURCE_DEBUG
        msg.str("");
        msg << "CRD 125[MHz] ticks since master counter reset = "
            << crdTicksSinceReset
            << ".";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif

        // Convert the number of 125[MHz] ticks to ACS time and add the
        // CRD RESET_TIME.
        const ACS::Time crdTime(
            (crdTicksSinceReset * TETimeUtil::ACS_ONE_SECOND
                / OneTwentyFiveMHz) + crdResetTime);

        #ifdef TIMESOURCE_DEBUG
        msg.str("");
        msg << "Calculated CRD time = ""
            << crdTime
            << "[100ns].";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif

        // Time of next TE
        time = ((crdTime / TETimeUtil::TE_PERIOD_ACS) + 1ULL)
            * TETimeUtil::TE_PERIOD_ACS;

        #ifdef TIMESOURCE_DEBUG
        msg.str("");
        msg << "Time of next TE = "
            << time
            << "[100ns].";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        #endif
    }
    catch(const ControlExceptions::MasterClockErrorExImpl& ex)
    {
        if(alarmSender->isAlarmSet(CannotDetermineTimeAtNextTE) == false)
        {
            alarmSender->activateAlarm(CannotDetermineTimeAtNextTE);
        }

        throw ControlExceptions::MasterClockErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getMasterClockErrorEx();
    }
    catch(...)
    {
        if(alarmSender->isAlarmSet(CannotDetermineTimeAtNextTE) == false)
        {
            alarmSender->activateAlarm(CannotDetermineTimeAtNextTE);
        }

        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "An unexpected exception occured.");
        ex.log();
        throw ex.getMasterClockErrorEx();
    }

    // Clear alarm if it was previously set.
    if(alarmSender->isAlarmSet(CannotDetermineTimeAtNextTE) == true)
    {
        alarmSender->deactivateAlarm(CannotDetermineTimeAtNextTE);
    }

    return time;
}

ACS::Time Control::TimeSourceImpl::getCrdResetTime()
{
    if(checkCrdAvailability() == false)
    {
        return 0ULL;
    }

    ACS::Time timeStamp(0ULL);
    return (m_crd_sp->GET_RESET_TIME(timeStamp));
}

bool Control::TimeSourceImpl::checkCrdAvailability() const
{
    bool ret(true);

    try
    {
        if(m_crd_sp->getHwState() != Control::HardwareDevice::Operational)
        {
            // Do not set the alarm if it is already set.
            if(alarmSender->isAlarmSet(CrdNotAvailable) == false)
            {
                alarmSender->activateAlarm(CrdNotAvailable);
            }

            ret = false;
        }
        else
        {
            // Do not clear the alarm if it is already cleared.
            if(alarmSender->isAlarmSet(CrdNotAvailable) == true)
            {
                alarmSender->deactivateAlarm(CrdNotAvailable);
            }
        }
    }
    catch(...)
    {
        // Do not set the alarm if it is already set.
        if(alarmSender->isAlarmSet(CrdNotAvailable) == false)
        {
            alarmSender->activateAlarm(CrdNotAvailable);
        }

        ret = false;
    }

    return ret;
}

void Control::TimeSourceImpl::calculate125MHzJitter(long long& jitter)
{
    // Calculate the current jitter:
    //
    // The MaserVsGpsCounter value in the CRD is latched by the 1PPS GPS
    // signal and is monotonically incrementing.  Three scenarios are
    // possible:
    // 1. Everything is OK, no problem.
    // 2. The reference clock is ticking too fast ->
    //    too many ticks -> problem!
    // 3. The reference clock is ticking too slow ->
    //    too few ticks -> problem.
    //
    // GPS 1PPS:   --|----------|----------|----------|----------|------->
    //               |          |          |          |          |
    // maserVsGpsCounter value when read:
    //               |          |          |          |          |
    //               500e6      625e6      755e6      875e6      1000e6
    //               |          |          |          |          |
    // Delta:        <  125e6  ><  130e6  ><  120e6  ><  125e6  >
    //               |          |          |          |          |
    // Meaning in terms of reference clock:
    //               |          |          |          |          |
    //               OK         OK         too fast,  too slow,  OK
    //               |          |          |          |          |
    //               OK         OK         too many   too few
    //                                     ticks      ticks
    //
    // The thread runs every 60s and calculates the jitter:
    // - Read the nth MaserVsGpsCounter C_n, it is returned together with the
    //   nth time stamp ts_n but the timestamp is discarded:
    //   C_n = crdRef->GET_MASER_VS_GPS_COUNTER(ts_n)
    //
    // - We expect 60 * 125MHz ticks during the time interval between C_n-1
    //   and C_n:
    //   jitter = C_n -C_n-1 - (60 * 125e6)
    //
    // - Store the current C_n as the previous C_n-1 for the next iteration.
    //

    #ifdef TIMESOURCE_DEBUG
    std::ostringstream msg;
    #endif
    ACS::Time timeStamp(0ULL);

    // Get the current Maser/GPS counter from the CRD. The value is latched
    // in the CRD every 1PPS which is received from the GPS clock.  The value
    // grows monotonically with every 1s interval and is only reset if the
    // the CRD is resynchronised.
    // A try/catch is not necessary since the caller takes care of the error
    // handling.
    const long long maserVsGpsCounter(
        m_crd_sp->GET_MASER_VS_GPS_COUNTER(timeStamp));

    #ifdef TIMESOURCE_DEBUG
    msg << "maserVsGpsCounter = "
        << maserVsGpsCounter
        << "\ntime stamp = "
        << timeStamp;
    #endif

    // It makes no sense to calculate the jitter if no previous data exists.
    // So run it once and only store the Maser_vs_GPS_Counter and then run it
    // in normal fashion.
    if(calculate125MHzJitterFirstRun == false)
    {
        // The jitter is the difference of Maser_VS_GPS_Counter value and the
        // previous Maser_VS_GPS_Counter value minus 60*125e6.  If the CRD is
        // working perfect, the difference should result to 0.  If the CRD is
        // running fast, the difference will be positive, if it is running slow, it
        // will be negative.
        jitter = maserVsGpsCounter - previousMaserVsGpsCounter;
        jitter -= (60ULL * 125000000ULL);
        #ifdef TIMESOURCE_DEBUG
        msg << "\nJitter = "
            << jitter / 125e6;
        LOG_TO_DEVELOPER(LM_TRACE, msg.str());
        #endif
    }
    else
    {
        calculate125MHzJitterFirstRun = false;
    }

    // Store the current Maser_vs_GPS_Counter for the next run.
    previousMaserVsGpsCounter = maserVsGpsCounter;
}

void Control::TimeSourceImpl::check125MHzJitter()
{
    // Do not execute if the CRD is not available.
    if(checkCrdAvailability() == false)
    {
        // No need to send a log message to the Operator.  If the CRD is not
        // available, an alarm has been triggered and the operator will already
        // know.
        return;
    }

    // Calculate the current jitter.
    long long jitter(0ULL);
    std::ostringstream msg;

    try
    {
        calculate125MHzJitter(jitter);
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        // No need to check if the CRD is set, because the check has been done
        // when the thread execution started and no change is expected during
        // the brief time period until I get to this line.
        alarmSender->activateAlarm(CrdNotAvailable);

        msg << "Could not retrieve the MASER_VS_GPS_COUNTER value from the "
            "CRD hardware because a CAN bus error happened.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());

        return;
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        // No need to check if the CRD is set, because the check has been done
        // when the thread execution started and no change is expected during
        // the brief time period until I get to this line.
        alarmSender->activateAlarm(CrdNotAvailable);

        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        msg << "Could not retrieve the MASER_VS_GPS_COUNTER value from the "
            "CRD because the CRD component is not in the operational state.";
        LOG_TO_OPERATOR(LM_ERROR, msg.str());

        return;
    }
    catch(...)
    {
        alarmSender->activateAlarm(CrdNotAvailable);

        ControlExceptions::MasterClockErrorExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        msg << "Caught an unexpected exception.  Will assume that there is a "
            "problem accessing the CRD.  Will not monitor the jitter "
            "of the 125[MHz] reference clock this time but try next "
            "time again.";
        // Log the exception and send a log to the operator.
        ex.addData("Detail", msg.str());
        ex.log();
        LOG_TO_OPERATOR(LM_ERROR, msg.str());

        return;
    }

    // The accumulated jitter is the integration of the jitter over time
    // and.tells if there is a drift of the reference clock in one
    // direction, i.e. too few 125MHz ticks per second or too many.
    accumulated125MHzJitter += jitter;

    #ifdef TIMESOURCE_DEBUG
    msg << "accumulated jitter = "
        << accumulated125MHzJitter / 125e6;
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    #endif

    // The absolute value of the current jitter exceeds what is allowed.
    // Trigger an alarm and log the current values for the operators.
    if(std::llabs(jitter) > Allowed125MHzJitter)
    {
        // Do not set the alarm if it is already set.
        if(alarmSender->isAlarmSet(Allowed125MHzJitterExceeded)
            == false)
        {
            alarmSender->activateAlarm(Allowed125MHzJitterExceeded);

            msg.str("");
            msg << "The absolute value of the 125[MHz] reference clock "
                "jitter is bigger than "
                << Allowed125MHzJitter
                << " counts: jitter = "
                << jitter
                << " counts.";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }
    }
    else
    {
        if(alarmSender->isAlarmSet(Allowed125MHzJitterExceeded)
            == true)
        {
            alarmSender->deactivateAlarm(Allowed125MHzJitterExceeded);
        }
    }

    // The absolute value of the accumulated jitter has exceeded the limit.
    // Trigger an alarm and log the current values for the operators.
    if(std::llabs(accumulated125MHzJitter) > AllowedAccumulated125MHzJitter)
    {
        // Do not set the alarm if it is already set.
        if(alarmSender->isAlarmSet(
            AllowedAccumulated125MHzJitterExceeded) == false)
        {
            alarmSender->activateAlarm(
                AllowedAccumulated125MHzJitterExceeded);

            msg.str("");
            msg << "The accumulated jitter of the 125[MHz] reference clock "
                "is bigger than "
                << AllowedAccumulated125MHzJitter
                <<" counts: accumulated jitter = "
                << accumulated125MHzJitter
                << " counts.";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
        }
    }
    else
    {
        if(alarmSender->isAlarmSet(
            AllowedAccumulated125MHzJitterExceeded) == false)
        {
            alarmSender->deactivateAlarm(
                AllowedAccumulated125MHzJitterExceeded);
        }
    }
    ///
    /// TODO
    /// Thomas, Jan 7, 2010
    /// All this could lead to an alternating alarm, one minute on, the
    /// other minute off. Obviously I have not implemented protection
    /// based on hysteresis, so the quick fix would be to increase the
    /// value of Allowed125MHzJitter.
}

Control::Crd125MHzVsGpsOffsetThread::Crd125MHzVsGpsOffsetThread(
    const ACE_CString& name, Control::TimeSourceImpl* _ts):
    ACS::Thread(name),
    ts(_ts)
{
}

Control::Crd125MHzVsGpsOffsetThread::~Crd125MHzVsGpsOffsetThread()
{
}

void Control::Crd125MHzVsGpsOffsetThread::runLoop()
{
    ts->check125MHzJitter();
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::TimeSourceImpl)
