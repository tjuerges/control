//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <MasterClockImpl.h>

#include <sstream>
#include <cmath>

// For ACE_Time_Value
#include <Time_Value.h>
// For ACE_OS::select
#include <ace/OS_NS_sys_select.h>
// For the mutex which protects the LkmLoader (NTP).
#include <Guard_T.h>

//Used to access other components, activate "OffShoot"s, etc.
#include <acsContainerServices.h>

#include <LogToAudience.h>

// For ACS::Time.
#include <acsutilTimeStamp.h>

// For component references.
#include <GPSC.h>
#include <CRDC.h>
#include <lkmLoaderC.h>

// Exceptions.
#include <acserr.h>
#include <cdbErrType.h>
#include <maciErrType.h>
#include <ControlExceptions.h>

// Alarm system stuff
#include <vector>
#include <controlAlarmSender.h>

// CORRCommon headers
#include <teHandler.h>
#include <TETimeUtil.h>
#include <rtToolsFifoCmd.h>


//#define MASTERCLOCK_DEBUG 1


Control::MasterClockImpl::MasterClockImpl(const ACE_CString& name,
    maci::ContainerServices* _cs):
    acscomponent::ACSComponentImpl(name, _cs),
    timeIsSynchronised(false),
    cs(_cs),
    alarmSender(0),
    myName(name.c_str()),
    MaxGPSAccessTime(TETimeUtil::ACS_ONE_SECOND),
    rtToolsTimeout(10000),
    crdFirmwareOk(false)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

Control::MasterClockImpl::~MasterClockImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void Control::MasterClockImpl::execute()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    acscomponent::ACSComponentImpl::execute();

    initialiseAlarmSystem();
}

void Control::MasterClockImpl::cleanUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(lkmLoaderForNTPRef_sp.isNil() == false)
    {
        lkmLoaderForNTPRef_sp.release();
    }

    releaseComponents();

    cleanUpAlarmSystem();

    acscomponent::ACSComponentImpl::cleanUp();
}

inline ACS::Time Control::MasterClockImpl::timeOfNext1pps(
    const ACS::Time& timeStamp, const long long& offset) const
{
    // If an offset is given:
    // 1. subtract the offset from the time stamp.
    // 2. Calculate the pps event.
    // 3. add the offset again.
    return ((timeStamp - offset) / TETimeUtil::ACS_ONE_SECOND
        * TETimeUtil::ACS_ONE_SECOND
        + TETimeUtil::ACS_ONE_SECOND + offset);
}

inline void Control::MasterClockImpl::waitUntil(
    const ACS::Time& timeStamp) const
{
    const long long timeToWait(static_cast< long long >(
        timeStamp - ::getTimeStamp()));

    // It does not make much sense to wait for just 10us = 1e-5s or less.
    if(timeToWait < (TETimeUtil::ACS_ONE_SECOND / 1e5))
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "Cannot wait for a too small or negative amount of time! "
            << "timeToWait = "
            << timeToWait
            << std::endl;
        ex.addData("Detail", output.str());
        ex.log();
        throw ex;
    }

    ACE_Time_Value waitFor(0ULL);
    // timeToWait is in 100ns (1e-7s). Convert it to s.
    waitFor.set(static_cast< double >(timeToWait) * 1e-7);
    ACE_OS::select(0, 0, 0, 0, waitFor);
}

void Control::MasterClockImpl::waitForNextTick() const
{
    // Make an effort for starting as close as possible right after the
    // beginning of a 48ms window.
    ACS::Time startAt(::getTimeStamp()), now(startAt);

    if((now % TETimeUtil::TE_PERIOD_ACS) > 10000ULL)  // 1ms
    {
        // Try to start at next tick.
        startAt = now - now % TETimeUtil::TE_PERIOD_ACS
            + TETimeUtil::TE_PERIOD_ACS;

        // Sleep is not very precise, this loop ensures we are not starting
        // before startAt.
        ACE_Time_Value onems(0, 1000U);
        do
        {
           // Sleep for 1ms.
            ACE_OS::select(0, 0, 0, 0, onems);

            // The Linux select call may modify the timeout value.  Reset it.
            onems.set(0.001);

            // Read the time again.
            now = ::getTimeStamp();
        }
        while(startAt > now);
    }
}

long long Control::MasterClockImpl::gpsClockUTCVsLocalClockOffset() const
{
    ACS::Time timeStampBefore(0ULL), timeStampAfter(0ULL), gpsTime(0ULL);

    timeStampBefore = ::getTimeStamp();
    gpsTime = getGPSClockUTCTime();
    timeStampAfter = ::getTimeStamp();

    #ifdef MASTERCLOCK_DEBUG
    {
        std::ostringstream output;
        output << "Local time = "
            << ((timeStampAfter + timeStampBefore) >> 1ULL)
            << ", GPS time = "
            << gpsTime
            << ", timeStampBefore = "
            << timeStampBefore
            << ", timeStampAfter = "
            << timeStampAfter
            << ".";
        LOG_TO_DEVELOPER(LM_INFO, output.str());
    }
    #endif

    // If the access time for the GPS query is too long, throw an exception.
    // We can then safely assume that there is something fishy with the GPS
    // hardware or the serial line communication.
    if((timeStampAfter - timeStampBefore) > MaxGPSAccessTime)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The execution time of the GPS query is too long. Allowed "
            << "are "
            << MaxGPSAccessTime / 1e7
            << "s. Current execution time was = "
            << ((timeStampAfter - timeStampBefore) / 1e7)
            << "s."
            << std::endl;
        ex.addData("Detail", output.str());
        ex.log();
        throw ex;
    }

    // localOffset =
    //    time stamp after - time stamp before
    //    ------------------------------------ + time stamp before - GPS time
    //                     2
    //
    // => localOffset = local time - GPS time
    // => local time = GPS time + local offset
    // => GPS time = local time - local offset
    //
    // Let's hope the computer never returns a time which uses bit #64 and
    // that bit #64 is never toggled between the two calls to ::getTimeStamp().
    //
    // Time stamps start 1582-10-15T00:00:00 UTC. ALMA would have to be
    // operational until the year 1582 + 29247 = 30829 to notice a problem
    // with bit #64. It will probably not last that long.
    long long localOffset(static_cast< long long >(timeStampAfter));
    localOffset -= static_cast< long long >(timeStampBefore);
    localOffset /= 2LL;
    localOffset += static_cast< long long >(timeStampBefore);
    localOffset -= static_cast< long long >(gpsTime);

    #ifdef MASTERCLOCK_DEBUG
    std::ostringstream output;
    output << "Local offset = "
        << localOffset
        << "[100ns]. Local time = "
        << ((timeStampAfter + timeStampBefore) >> 1ULL)
        << ", GPS time = "
        << gpsTime
        << ", timeStampBefore = "
        << timeStampBefore
        << ", timeStampAfter = "
        << timeStampAfter
        << "."
        << std::endl;
    LOG_TO_DEVELOPER(LM_DEBUG, output.str());
    #endif

    return localOffset;
}

void Control::MasterClockImpl::teHandlerResync() const
{
    teHandlerCmdIdx cmd(teHandlerCMD_RESYNC);
    teHandlerClock_t clk;
    teHandlerFifoCmdStat_t response(TE_HANDLER_CMD_STAT_UNKNOWN);

    try
    {
        m_cmdFifo->sendRecvCmd(&cmd, sizeof(teHandlerCmdIdx),
            static_cast< void* >(&response), sizeof(teHandlerFifoCmdStat_t),
            rtToolsTimeout);
        if(response != TE_HANDLER_CMD_STAT_OK)
        {
            ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream output;
            output << "teHandler did not resync properly. teHandler error = "
                << teHandlerErr2String(response)
                << ".";
            ex.addData("Detail", output.str());
            ex.log();
            throw ex;
        }
    }
    catch(const rtToolsFifoCmdErr_t& ex)
    {
        ControlExceptions::MasterClockErrorExImpl nex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "Failed to retrieve the CLK data structure from the TE "
            "handler, FIFO exception. rtTools error = "
            << rtToolsEx2String(ex)
            << ".";
        nex.addData("Detail", output.str());
        nex.log();
        throw nex;
    }
    catch(...)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Failed to retrieve the CLK data structure from "
            "the TE handler, expected exception.");
        ex.log();
        throw ex;
    }

    // Wait until the teHandler is back in HARD mode. Timeout is 5s, wake up
    // every 0.1s.
    const ACS::Time timeStep(TETimeUtil::ACS_ONE_SECOND / 10ULL);
    ACS::Time timeDelta(0ULL);
    try
    {
        // Wait only if the teHandler is not in SOFT mode.
        getTEClock(clk);
        if(clk.mode == TE_HANDLER_MODE_SOFT)
        {
            #ifdef MASTERCLOCK_DEBUG
            std::ostringstream msg;
            msg << "teHandler is in SOFT mode. Not waiting.";
            LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            #endif
            return;
        }

        while(timeDelta < (5ULL * TETimeUtil::ACS_ONE_SECOND))
        {
            waitUntil(::getTimeStamp() + timeStep);
            getTEClock(clk);

            if(clk.mode == TE_HANDLER_MODE_HARD)
            {
                #ifdef MASTERCLOCK_DEBUG
                std::ostringstream msg;
                msg << "Successful teHandler resync.";
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                #endif
                break;
            }
            else
            {
                timeDelta += timeStep;
                #ifdef MASTERCLOCK_DEBUG
                std::ostringstream msg;
                msg << "Waited "
                    << timeDelta
                    << "[100s].";
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                #endif
            }
        };

        if(timeDelta >= TETimeUtil::ACS_ONE_SECOND)
        {
            ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream output;
            output << "Time out while waiting for the teHandler to resync with "
                "the TE events. teHandler clk.mode = "
                << clk.mode
                << "."
                << std::endl;
            ex.addData("Detail", output.str());
            ex.log();
            throw ex;

        }
    }
    catch(const rtToolsFifoCmdErr_t& ex)
    {
        ControlExceptions::MasterClockErrorExImpl nex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "Failed to retrieve the CLK data structure from the TE "
            "handler, FIFO exception. rtTools error = "
            << rtToolsEx2String(ex)
            << "."
            << std::endl;
        nex.addData("Detail", output.str());
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::MasterClockErrorExImpl& ex)
    {
        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex;
    }
    catch(...)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Failed to retrieve the CLK data structure from "
            "the TE handler, expected exception.");
        ex.log();
        throw ex;
    }
}

void Control::MasterClockImpl::getTEClock(teHandlerClock_t &clk) const
{
    teHandlerCmdIdx cmd(teHandlerCMD_GETCLK);

    try
    {
        m_cmdFifo->sendRecvCmd(&cmd, sizeof(teHandlerCmdIdx),
            static_cast< void* >(&clk), sizeof(teHandlerClock_t),
            rtToolsTimeout);
    }
    catch(const rtToolsFifoCmdErr_t& e)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "Failed to retrieve the CLK data structure from the TE "
            "handler. rtTools error = "
            << rtToolsEx2String(e)
            << "."
            << std::endl;
        ex.addData("Detail", output.str());
        ex.log();
        throw ex;
    }
    catch(...)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Failed to retrieve the CLK data structure from "
            "the TE handler");
        ex.log();
        throw ex;
    }
}

void Control::MasterClockImpl::setUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        getComponents();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex;
    }

    // Create the TE handler command fifo
    try
    {
        std::ostringstream msg;
        msg << "Resetting the TE handler command fifo.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        m_cmdFifo.reset(new rtToolsFifoCmd(TE_HANDLER_CMD_FIFO_DEV));
    }
    catch(...)
    {
        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Failed to create rtToolsFifoCmd instance");
        ex.log();
        throw ex;
    }

    ///
    /// Thomas, Mar 10, 2011
    ///
    ///
    // Check what firmware revision the CRD has.  This is important to know
    // because Sylas Ashton reported that the current firmware does return a
    // wrong latched value after a reset and before the first 1pps.
    crdFirmwareOk = false;
    try
    {
        ACS::Time timestamp(0ULL);
        Control::LongSeq_var moduleCodes(new Control::LongSeq);
        moduleCodes = m_crd_sp->GET_CRD_MODULE_CODES(timestamp);
        if(moduleCodes->length() == 8)
        {
            unsigned char majorRevision((moduleCodes[2] >> 4U) & 0x0fU);
            unsigned char minorRevision(moduleCodes[2] & 0x0fU);
            if((majorRevision <= 1U) && (minorRevision >= 3U)
            || (majorRevision > 1U))
            {
                crdFirmwareOk = true;
            }
        }
    }
    catch(...)
    {
        // It does not matter if this was successful because it only means
        // that an additional safety check cannot be performed during time
        // synchronisation.
    }
}

void Control::MasterClockImpl::getComponents()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(m_gps_sp.isNil() == true)
    {
        try
        {
            // Get Reference to the GPS component.
            m_gps_sp = getComponentReference< Control::GPS >(
                "CONTROL/AOSTiming/GPS");
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "The GPS component could not be instanciated.");
            throw nex;
        }
    }

    // Check that the GPScomponent is in the correct hwstate.
    if(m_gps_sp->getHwState() != Control::HardwareDevice::Operational)
    {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "The GPS component is not in the operational "
            "hardware state. Cannot continue with the time synchronisation.");
        ex.log();
        throw ex;
    }

    if(m_crd_sp.isNil() == true)
    {
        // Get a reference to the CRD component.
        try
        {
            m_crd_sp = getComponentReference< Control::CRD >(
                "CONTROL/AOSTiming/CRD");
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "The CRD component could not be instanciated.");
            throw nex;
        }
    }

    // Check the hardware state of the CRD component. It should be
    // >= Control::HardwareDevice::Configure.
    const Control::HardwareDevice::HwState crdHwState(m_crd_sp->getHwState());
    switch(crdHwState)
    {
        case Control::HardwareDevice::Undefined:
        case Control::HardwareDevice::Stop:
        case Control::HardwareDevice::Start:
            {
                m_gps_sp.release();
                maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
                ex.addData("Detail", "The CRD component is not in the correct "
                    "hardware state. Cannot continue with the time "
                    "synchronisation.");
                ex.log();
                throw ex;
            };
            break;
        case Control::HardwareDevice::Configure:
        case Control::HardwareDevice::Initialize:
        case Control::HardwareDevice::Operational:
        case Control::HardwareDevice::Simulation:
        case Control::HardwareDevice::Diagnostic:
            break;
        default:
            break;
    };
}

void Control::MasterClockImpl::releaseComponents()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(m_gps_sp.isNil() == false)
    {
        m_gps_sp.release();
    }

    if(m_crd_sp.isNil() == false)
    {
        m_crd_sp.release();
    }
}

ACS::Time Control::MasterClockImpl::syncArrayTime()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Clear all alarms.  This is a fresh start.
    alarmSender->forceTerminateAllAlarms();

    try
    {
        setUp();
    }
    catch(const ControlExceptions::MasterClockErrorExImpl& ex)
    {
        alarmSender->activateAlarm(SetUpFailed);

        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "No time synchronisation has been done!");
        throw nex.getMasterClockErrorEx();
    };

    // Invalidate the current status.  This is used by doStartNtpServer.
    timeIsSynchronised = false;

    // No instantiation of crucial variables inside the try. Just in case.
    ACS::Time gpsTime(0ULL), executeCRDsyncTime(0ULL), resetTime(0ULL),
        realResetTime(0ULL), realT0(0ULL), T0(0ULL),
        currentlyLatchedCounterValue(0ULL), tmpTimeStamp(0ULL);
    unsigned triedCrdResyncTwice(0U);

    // The offset between the local clock and the target time system corrected
    // GPS clock in 100ns.
    long long localOffset(0LL);

    // Counter for the SETT0 step. Try maximum twice, See explanation below.
    unsigned char setT0syncCounter(2U);

    // Necessary variables for the SET T0 step.
    teHandlerFifoCmdStat_t reply(TE_HANDLER_CMD_STAT_OK);
    teHandlerClock_t clk;
    rtToolsCmdBuffer< teHandlerSetT0Cmd_t > cmd(teHandlerCMD_SETT0);

    try
    {
        do
        {
           ++triedCrdResyncTwice;

            // Determine the offset of the local clock vs. GPS. The local offset
            // is stored in a long long because it can be negative.
            //
            // It is important to be consistent how the offset is added or
            // subtracted. I applied the rules which I have explained in
            // the gpsClockUTCVsLocalClockOffset method.
            //
            // Reminder:
            //
            // localOffset = local time - UTC time on GPS clock
            // local time = UTC time on GPS clock + local offset
            // UTC time on GPS clock = local time - local offset
            localOffset = gpsClockUTCVsLocalClockOffset();

            // Try to estimate the difference between the local computer clock
            // and the GPS clock (base is UTC) which has to be 0 after the
            // synchronisation.
            // The estimation can only be done if the CRD had been
            // resynchronised before. Otherwise the RESET_TIME property in the
            // CRD component will return 0ULL which means "I have no clue what
            // time it is". It is checked for in estimateTimeShift.
            estimateTimeShift(localOffset);

            // Okay, the offset of the local clock (localOffset) is small
            // enough. Otherwise gpsClockUTCVsLocalClockOffset would have
            // thrown an exception.
            //
            // Now wait for the next 6pps event. The timeOfNext{1,6}pps
            // methods take care of proper calculation of the ?pps event in
            // the GPS time domain.  This puts us right on the spot where the
            // {1,6}pps signal is about to become active or where it just
            // became active.
            waitUntil(timeOfNext6pps(::getTimeStamp(), localOffset));

            // Now get the GPS Time for the current GPS second.
            gpsTime = getGPSClockUTCTime();

            // Calculate the next 6pps event. The local offset would put us
            // right on the money, but... - see below.
            //
            // IMPORTANT
            // ---------
            // It is very important that the CRD component runs on the same
            // computer this component (MasterClock) runs on. Otherwise there
            // will be a discrepancy between the two local clocks thus the CRD
            // resync will be executed at the wrong time.
            executeCRDsyncTime = timeOfNext6pps(::getTimeStamp(), localOffset);
            // The CRD component executes this command request via commandTE.
            // The execution time of the commandTE is close to the next 6pps
            // but it is not on a local time 48ms boundary since it is
            // calculated in the GPS time domain. The AMB interface will check
            // for this and execute it in the next matching TE slot instead.
            // To be sure that the command will be executed before the next
            // 6pps and not after, always round down to the start of the TE.
            // Subtract some TEs to have enough time for the CORBA invocation.
            executeCRDsyncTime = ((executeCRDsyncTime /
                TETimeUtil::TE_PERIOD_ACS) - 5ULL) * TETimeUtil::TE_PERIOD_ACS;

            // The "real" reset time, i.e. the anticipated time when the CRD
            // will be resynched, shall be in the target time system. So
            // calculate it based on the GPS time we just read.
            // Do not pass any offset since the given time stamp is already
            // from the GPS clock UTC time domain.
            realResetTime = timeOfNext6pps(gpsTime);

            // Tell the CRD to resync.
            resetTime = m_crd_sp->maserCounterReset(executeCRDsyncTime);
            // Now check if the reported resetTime has anything to do with the
            // expected reset time.
            if(std::abs(
                static_cast< long long >(resetTime)
                - static_cast< long long >(executeCRDsyncTime))
            > static_cast< long long >(TETimeUtil::TE_PERIOD_ACS))
            {
                // Just log a message that it is unfortunate that those time
                // stamps are not the same. Well, bad things happen but I am
                // not convinced that this is really so bad that it justifies
                // an exception. Long story made short, just log it for now.
                std::ostringstream msg;
                msg << "The time stamp on which the CRD has been resynched does "
                    "not concur with the expected time stamp. Expected = "
                    << executeCRDsyncTime
                    << "[100ns], real = "
                    << resetTime
                    << "[100ns], difference real - expected = "
                    << (resetTime - executeCRDsyncTime)
                    << "[100ns].";
                LOG_TO_OPERATOR(LM_INFO, msg.str());
            }

            ///
            /// Thomas, Mar 10, 2011
            ///
            ///
            // Check if there actually was a 1PPS signal.  Sylas reported that
            // the EVLA does - on occasion - miss one or two 1PPS.  Sylas
            // also found out that the first latched value that is read after
            // a reset before the next 1PPS is wrong.  This requires a firmware
            // update.  The firmware that contain the corrected code are
            // 1.3 and up.
            if(crdFirmwareOk == true)
            {
                // Now wait 6TEs until the CRD has recognised the 1pps and
                // reset the maser vs. gps counter register.
                waitUntil(::getTimeStamp() + 6ULL * TETimeUtil::TE_PERIOD_ACS);
                currentlyLatchedCounterValue =
                    m_crd_sp->GET_MASER_VS_GPS_COUNTER(tmpTimeStamp);
                if(currentlyLatchedCounterValue == 0ULL)
                {
                    break;
                }
                // If the latched counter is not == 0ULL, the CRD missed the
                // 1PPS and I shall try again until it failed for a second
                // time.
                else if(triedCrdResyncTwice < 2U)
                {
                    continue;
                }
                // Now I am in trouble.  The CRD has missed the 1PPS signal
                // twice!  This is serious and needs to be investigated.
                else
                {
                    ControlExceptions::MasterClockErrorExImpl ex(__FILE__,
                        __LINE__, __PRETTY_FUNCTION__);
                    ex.addData("Detail", "Failed to resynchronise the CRD "
                        "with the GPS clock twice!  The CRD has missed the "
                        "1PPS signal during both resynchronisation tries. "
                        "This is a hardware issue and has to be investigated "
                        "because the ALMA Master clock is no functioning "
                        "properly.");
                    ex.log();
                    throw ex;
                }
            }

            // The reported CRD resetTime contains the local time plus an
            // offset and reflects the execution time of the CRD resync. Store
            // the reset time which we calculated above in the CRD memory.
            m_crd_sp->SET_RESET_TIME(realResetTime);

            // Everything is OK, the CRD has been resynch'ed, leave the loop.
            break;
        }
        while(triedCrdResyncTwice < 2U);

        // ---------------------------------------------------------------------
        //               teHandler will run in FW mode from here on
        //                          until we resync it.
        // ---------------------------------------------------------------------

        // Send log message which informs everybody that the LORR is to be
        // expected out of TE phase. It is not crucial if this call takes some
        // time.
        std::ostringstream msg;
        msg << "The phase of the TE signal has been realigned with the 1pps "
            "signal of the GPS clock. Please expect the LORR to report a "
            "condition about the TE phase. The LORR needs to resync itself "
            "with the new TE phase. The reset time of the CRD has been set to "
            << realResetTime
            << "[100ns] (target time system).";
        LOG_TO_OPERATOR(LM_INFO, msg.str());

        // Wait until the actual 6pps happens plus 4 TEs. This should give the
        // teHandler enough time to go out of whack, i.e. into FW mode.
        waitUntil(timeOfNext6pps(::getTimeStamp(), localOffset)
            + 4ULL * TETimeUtil::TE_PERIOD_ACS);

        // Now we have to resync the teHandler with the new TE phase. Important
        // things to remember:
        //
        // - The local clock is not as accurate as before, i.e. in HARD mode.
        //   It may or may not have a bigger offset than before due to the FW
        //   mode.
        // - The teHandler is running FW, therefore waiting on a TE tick does
        //   not work unless we resync the teHandler with the TE events.
        // - After the teHandler resync we have to recalculate the local clock
        //   offset because the teHandler set the local clock.

        // Resync the teHandler with the TE events.
        teHandlerResync();

        // ---------------------------------------------------------------------
        //             teHandler will run in HARD mode from here on.
        // ---------------------------------------------------------------------

        // Determine the offset of the local clock vs. the UTC time on the GPS
        // clock. This is safe again because the teHandler is back on TEs and
        // can precisely count the time and thus sets the local clock accurate.
        localOffset = gpsClockUTCVsLocalClockOffset();

        // Try to set the teHandler T0 twice. Only if it fails twice
        // an error will be reported. If it fails once it is with very high
        // probability a TE_HANDLER_CMD_STAT_WRONG_TICK error. This happens
        // occasionally and a subsequent sync is usually successful. So no
        // real reason to worry.
        while(setT0syncCounter > 0U)
        {
            // Decrement the SET T0 synchronisation counter.
            --setT0syncCounter;

            // T0 = time of next 6pps.
            //
            // Calculate the real T0 time according to the UTC time on the GPS
            // clock. Keep it for the teHandler.
            realT0 = timeOfNext6pps(getGPSClockUTCTime());

            // Now calculate the next 6pps based on our local clock.
            T0 = timeOfNext6pps(::getTimeStamp(), localOffset);

            // Wait for the next 6pps. This ensures that the teHandler can see
            // TE events and the TE event is in this moment aligned with the
            // 1pps. We wait for the time stamp which is one TE before the 6pps.
            // By this we can perfectly sync everything waiting for the more
            // precise TE events using the teHandler.
            waitUntil(T0 - TETimeUtil::TE_PERIOD_ACS);

            // -----------------------------------------------------------------
            //          Quick! All of this must not take longer than 1TE!
            // -----------------------------------------------------------------

            // Read the current TE.
            getTEClock(clk);

            // Now wait for the TE which is the on the 6pps event.
            waitForNextTick();

            // We waited for another tick, so the tick for which we set T0 is
            // clk.ticks + 1.
            //
            // Important!!!!  The teHandler expects the time stamp of the
            // following TE tick, not the current.
            //
            // Set the teHandler T0 to be target time system and
            // not local time anymore so that this computer runs from now on on
            // the target time. That is why cmd->value is set to realT0.
            cmd->value = realT0 + TETimeUtil::TE_PERIOD_ACS;
            cmd->tick = (clk.ticks + 1);

            // Here we are exactly on realT0.  The teHandler accepts
            // new values for the time stamp and the ticks only for its own
            // current TE, i.e. add one to ticks above.

            // Set the teHandler T0 to be target time system and
            // not local time anymore so that this computer runs from now on on
            // the target time. That is why cmd->value is set to realT0.
            m_cmdFifo->sendRecvCmd(cmd.pack(), cmd.size(), &reply,
                sizeof(teHandlerFifoCmdStat_t), rtToolsTimeout);

            // -----------------------------------------------------------------
            //      Done; phew! Let's hope it did not take longer than 1TE.
            // -----------------------------------------------------------------

            // Check the response.
            if(reply != TE_HANDLER_CMD_STAT_OK)
            {
                ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
                    __PRETTY_FUNCTION__);
                ex.addData("Detail", "Failed to set the time of the TE "
                    "handler.");
                std::ostringstream output;
                output << "Failed to set the time of the TE handler. teHandler "
                    "error = "
                    << teHandlerErr2String(reply)
                    << ". T0 according to the local clock = "
                    << T0
                    << "[100ns], real T0 according to GPS/target time system = "
                    << realT0
                    << "."
                    << std::endl;
                ex.addData("Detail", output.str());

                if(setT0syncCounter == 0U)
                {
                    // The synchronisation with the teHandler (SET T0) failed
                    // twice. This is an error condition.
                    ex.log();
                    throw ex;
                }
                else
                {
                    // The synchronisation with the teHandler (SET T0) failed
                    // for the first time. Just report it and try again.
                    ex.addData("Detail", "The synchronisation with the "
                        "teHandler failed. This step will be repeated one more "
                        "time.");
                    ex.log();
                    continue;
                }
            }
            else
            {
                // Set T0 was successful. No reason to do it again, leave loop.
                std::ostringstream msg;
                msg << "teHandler time stamp = "
                    << realT0
                    << "[100ns].";
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
                break;
            }
        };
        // We made it, yeah!
        timeIsSynchronised = true;

        if(isNtpServerRunning() == true)
        {
            stopNtpServer();
        }

        // Start the NTP server.
        doStartNtpServer();
    }
    catch(const ControlExceptions::MasterClockErrorExImpl& ex)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getMasterClockErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getMasterClockErrorEx();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // This exceptions is thrown when the NTP server start faile.  This is
        // not fatal, so do not release the other components and just log the
        // exception.
        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
    }
    catch(const CORBA::SystemException& ex)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl nex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "A CORBA::SystemException has been caught.");
        nex.log();
        throw nex.getMasterClockErrorEx();
    }
    catch(...)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unknown exception caught.");
        ex.log();
        throw ex.getMasterClockErrorEx();
    }

    releaseComponents();

    std::ostringstream msg;
    msg << "The MasterClock has successfully set the CRD reset time to "
        << realResetTime
        << "[100ns] and T0 for the ArrayTime to T0 = "
        << realT0
        << "[100ns]. From now on all time information on this computer will "
            "be target time system.";
    LOG_TO_OPERATOR(LM_INFO, msg.str());

    return realResetTime;
}

CORBA::Boolean Control::MasterClockImpl::isSynchronised()
{
    return timeIsSynchronised;
}

void Control::MasterClockImpl::doStartNtpServer()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(timeIsSynchronised == false)
    {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "The time has not been successfully synchronised. "
            "Starting of the NTP server is not allowed.  Call syncArrayTime() "
            "first!");
        throw ex;
    }

    const std::string componentCode("lkmLoaderImpl");
    const std::string componentType("IDL:alma/ACS_RT/lkmLoader:1.0");
    const std::string componentName("CONTROL/AOSTiming/lkmLoaderForNTP");
    const std::string containerName(maci::COMPONENT_SPEC_ANY);

    maci::ComponentSpec spec;
    spec.component_name = componentName.c_str();
    spec.component_code = componentCode.c_str();
    spec.component_type = componentType.c_str();
    spec.container_name = containerName.c_str();

    alarmSender->deactivateAlarm(CannotActivateNtpServer);

    // Thomas, Jul 23, 2008
    // Okay, okay, I know. Two nested try/catch blocks is way uncool but the
    // C++ standard lacks a catch for collected exceptions.
    try
    {
        try
        {
            CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
            lkmLoaderForNTPRef_sp =
                cs->getCollocatedComponentSmartPtr< ACS_RT::lkmLoader >(
                    spec, false, tmpName.in());
        }
        catch(const maciErrType::NoPermissionExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::IncompleteComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::InvalidComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const
            maciErrType::ComponentSpecIncompatibleWithActiveComponentExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "The lkmLoader for NTP component could not "
                "be started.");
            throw nex;
        }
        catch(...)
        {
            maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream msg;
            msg << "An unexpected exception has been caught. "
                "Developers: check the source code of maci::ContainerServices::"
                "getColocatedComponentSmartPtr(...)!";
            ex.addData("Detail", msg.str());
            throw ex;
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        alarmSender->activateAlarm(CannotActivateNtpServer);

        ///
        /// TODO
        /// Thomas, Aug 30, 2009
        /// Should the MasterClock or even the parent (AOSTiming) be set in
        /// error state or degraded state if the NTP server start up failed?
        ///

        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The component called '"
            << componentName
            << "' could not be started. This is not a fatal error. The NTP "
                "server on host \"ARTM\" will not be available. Component "
            "name = "
            << componentName
            << ", component code = "
            << componentCode
            << ", component type = "
            << componentType
            << ", container name = "
            << containerName
            << ".";
        nex.addData("Detail", msg.str());
        throw nex;
    }
}

void Control::MasterClockImpl::startNtpServer()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(isNtpServerRunning() == true)
    {
        ControlExceptions::DeviceBusyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The NTP server on \"ARTM\" is already running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    try
    {
        ACE_Guard< ACE_Mutex > guard(referencesMutex);
        doStartNtpServer();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getInvalidRequestEx();
    }
}

void Control::MasterClockImpl::stopNtpServer()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(isNtpServerRunning() == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The NTP server on \"ARTM\" is not running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    if(lkmLoaderForNTPRef_sp.isNil() == false)
    {
        lkmLoaderForNTPRef_sp.release();
    }
}

CORBA::Boolean Control::MasterClockImpl::isNtpServerRunning()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    ACE_Guard< ACE_Mutex > guard(referencesMutex);
    return (! lkmLoaderForNTPRef_sp.isNil());
}

void Control::MasterClockImpl::estimateTimeShift(
const ACS::Time& localOffset) const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Try to estimate how big the time difference between the CRD time
    // and the GPS time is. The base is UTC.
    try
    {
        ACS::Time timeStamp(0ULL);

        // Get the UTC time from the GPS clock for the current second.
        ACS::Time utcTimeAtNext1pps(getGPSClockUTCTime());
        // Round it up.
        utcTimeAtNext1pps = utcTimeAtNext1pps / TETimeUtil::ACS_ONE_SECOND
            * TETimeUtil::ACS_ONE_SECOND + TETimeUtil::ACS_ONE_SECOND;

        // This should not take longer than one second.
        ACS::Time crdTimeAtNext1pps(m_crd_sp->GET_RESET_TIME(timeStamp));

        // Only report the estimated time shift if the CRD has been
        // resynchronised before.
        if(crdTimeAtNext1pps != 0ULL)
        {
            // This is the time stamp at the begin of the current second in
            // 125MHz ticks.  Convert 125MHz ticks to 100ns by dividing it by
            // 125e6 (1s in 125MHz ticks) and multiplying it with
            // 10e6(1s in 100ns).
            crdTimeAtNext1pps += (m_crd_sp->GET_MASER_VS_GPS_COUNTER(timeStamp)
                / 125000000ULL * 10000000ULL);
            // Add one second in 125MHz...
            crdTimeAtNext1pps += TETimeUtil::ACS_ONE_SECOND;

            // Estimate the difference on the begin of the next full second.
            const double estimatedTimeShift(
                (static_cast< double >(crdTimeAtNext1pps)
                    - static_cast< double >(utcTimeAtNext1pps))
                / static_cast< double >(TETimeUtil::ACS_ONE_SECOND));

            // Log the estimated time shift.
            std::ostringstream out;
            // Set the float representation.
            out.precision(10);
            out.setf(ios::fixed,ios::floatfield);

            out << "The estimated time shift will be (CRD - GPS clock) = "
                << estimatedTimeShift
                << "s.";
            LOG_TO_OPERATOR(LM_INFO, out.str());
        }
        else
        {
            LOG_TO_OPERATOR(LM_WARNING, "Cannot estimate the time shift "
                "bacause the CRD hardware does not have a valid T0 stored in "
                "memory.");
        }
    }
    catch(...)
    {

        return;
    }
}

ACS::Time Control::MasterClockImpl::getGPSClockUTCTime() const
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Read the GPS time property.
    ACSErr::Completion_var completion;
    ACS::Time gps_time(0ULL);
    try
    {
        gps_time = m_gps_sp->GPS_Time()->get_sync(completion.out());
    }
    catch(const CORBA::SystemException&)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "GPS clock could not be contacted.  No GPS time "
            "available!");
        ex.log();
        throw ex;
    }
    catch(...)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unexpected exception caught.  No GPS time "
            "available!");
        ex.log();
        throw ex;
    }

    ACSErr::CompletionImpl c(completion.in());
    if(c.isErrorFree() == false)
    {
        c.log();
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Failed to read the GPS time.  No GPS time "
            "available!");
        ex.log();
        throw ex;
    }
    else if(gps_time == 0ULL)
    {
        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "GPS time = 0[100ns].  No GPS time available!");
        ex.log();
        throw ex;
    }
    else
    {
        std::ostringstream msg;
        msg << "GPS time = "
            << gps_time
            << "[100ns].";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }

    return gps_time;
}

std::string Control::MasterClockImpl::rtToolsEx2String(
    const rtToolsFifoCmdErr_t& ex) const
{
    std::string ret;

    switch(ex)
    {
        case RT_TOOLS_CMD_OPEN_ERR:
        {
            ret = "RT_TOOLS_CMD_OPEN_ERR";
        }
        break;

        case RT_TOOLS_CMD_EMPTY_ERR:
        {
            ret = "RT_TOOLS_CMD_EMPTY_ERR";
        }
        break;

        case RT_TOOLS_CMD_DESTROY_ERR:
        {
            ret = "RT_TOOLS_CMD_DESTROY_ERR";
        }
        break;

        case RT_TOOLS_CMD_CLOSE_ERR:
        {
            ret = "RT_TOOLS_CMD_CLOSE_ERR";
        }
        break;

        case RT_TOOLS_CMD_SEM_TIMEOUT_ERR:
        {
            ret = "RT_TOOLS_CMD_SEM_TIMEOUT_ERR";
        }
        break;

        case RT_TOOLS_CMD_WRITE_ERR:
        {
            ret = "RT_TOOLS_CMD_WRITE_ERR";
        }
        break;

        case RT_TOOLS_CMD_READ_ERR:
        {
            ret = "RT_TOOLS_CMD_READ_ERR";
        }
        break;

        case RT_TOOLS_CMD_SEM_POST_ERR:
        {
            ret = "RT_TOOLS_CMD_SEM_POST_ERR";
        }
        break;

        case RT_TOOLS_CMD_TIMEOUT_ERR:
        {
            ret = "RT_TOOLS_CMD_TIMEOUT_ERR";
        }
        break;

        default:
        {
        }
    };

    return ret;
}

std::string Control::MasterClockImpl::teHandlerErr2String(
    const teHandlerFifoCmdStat_t& error) const
{
    std::string ret;

    switch(error)
    {
        case TE_HANDLER_CMD_STAT_OK:
        {
            ret = "TE_HANDLER_CMD_STAT_OK";
        }
        break;

        case TE_HANDLER_CMD_STAT_WRONG_TICK:
        {
            ret = "TE_HANDLER_CMD_STAT_WRONG_TICK";
        }
        break;

        case TE_HANDLER_CMD_STAT_INVALID_T0:
        {
            ret = "TE_HANDLER_CMD_STAT_INVALID_T0";
        }
        break;

        case TE_HANDLER_CMD_STAT_RESYNC_FAILED:
        {
            ret = "TE_HANDLER_CMD_STAT_RESYNC_FAILED";
        }
        break;

        case TE_HANDLER_CMD_STAT_UNKNOWN:
        {
            ret = "TE_HANDLER_CMD_STAT_UNKNOWN";
        }
        break;

        default:
        {
        }
    };

    return ret;
}

void Control::MasterClockImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSende.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    {
        Control::AlarmInformation ai;
        ai.alarmCode = CannotActivateNtpServer;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = SetUpFailed;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = TimeSynchFailed;
        alarmInformation.push_back(ai);
    }

    alarmSender->initializeAlarms("MasterClock", "CentralLO",
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void Control::MasterClockImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::MasterClockImpl)
