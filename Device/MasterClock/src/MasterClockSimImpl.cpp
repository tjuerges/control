//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Jul 21, 2008  created
//


#include <MasterClockSimImpl.h>
#include <sstream>
#include <LogToAudience.h>
#include <acserr.h>
#include <TETimeUtil.h>

// For component references.
#include <CRDC.h>

// Alarm system stuff
#include <controlAlarmSender.h>


Control::MasterClockSimImpl::MasterClockSimImpl(const ACE_CString& _name,
    maci::ContainerServices* _cs):
    Control::MasterClockImpl(_name, _cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

Control::MasterClockSimImpl::~MasterClockSimImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

void Control::MasterClockSimImpl::setUp()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        getComponents();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        releaseComponents();

        throw ControlExceptions::MasterClockErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
}

ACS::Time Control::MasterClockSimImpl::syncArrayTime()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        setUp();
    }
    catch(const ControlExceptions::MasterClockErrorExImpl& ex)
    {
        alarmSender->activateAlarm(SetUpFailed);

        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "No time synchronisation has been done!");
        throw nex.getMasterClockErrorEx();
    };

    // Invalidate the current status.  This is used by doStartNtpServer.
    timeIsSynchronised = false;

    // No instantiation of crucial variables inside the try. Just in case.
    ACS::Time gpsTime(0ULL), executeCRDsyncTime(0ULL), resetTime(0ULL),
        realResetTime(0ULL), realT0(0ULL);

    // The offset between the local clock and the target time system corrected
    // GPS clock in 100ns.
    long long localOffset(0LL);

    try
    {
        // Determine the offset of the local clock vs. GPS. The local offset
        // is stored in a long long because it can be negative.
        //
        // It is important to be consistent how the offset is added or
        // subtracted. I applied the rules which I have explained in
        // the gpsClockUTCVsLocalClockOffset method.
        //
        // Reminder:
        //
        // localOffset = local time - UTC time on GPS clock
        // local time = UTC time on GPS clock + local offset
        // UTC time on GPS clock = local time - local offset
        localOffset = gpsClockUTCVsLocalClockOffset();

        // Try to estimate the difference between the local computer clock
        // and the GPS clock (base is UTC) which has to be 0 after the
        // synchronisation.
        // The estimation can only be done if the CRD had been resynchronised
        // before. Otherwise the RESET_TIME property in the CRD component will
        // return 0ULL which means "I have no clue what time it is". It is
        // checked for in estimateTimeShift.
        estimateTimeShift(localOffset);

        // Okay, the offset of the local clock (localOffset) is small enough.
        // Otherwise gpsClockUTCVsLocalClockOffset would have thrown an
        // exception.
        //
        // Now wait for the next 6pps event. The timeOfNext{1,6}pps methods take
        // care of proper calculation of the ?pps event in the GPS time domain.
        // This puts us right on the spot where the {1,6}pps signal is about to
        // become active or where it just became active.
        waitUntil(timeOfNext6pps(::getTimeStamp(), localOffset));

        // Now get the GPS Time for the current GPS second.
        gpsTime = getGPSClockUTCTime();

        // Calculate the next 6pps event. The local offset would put us right
        // on the money, but... - see below.
        //
        // IMPORTANT
        // ---------
        // It is very important that the CRD component runs on the same computer
        // this component (MasterClock) runs on. Otherwise there will be a
        // discrepancy between the two local clocks thus the CRD resync will be
        // executed at the wrong time.
        executeCRDsyncTime = timeOfNext6pps(::getTimeStamp(), localOffset);
        // The CRD component executes this command request via commandTE.
        // The execution time of the commandTE is close to the next 6pps but
        // it is not on a local time 48ms boundary since it is calculated in the
        // GPS time domain. The AMB interface will check for this and execute
        // it in the next matching TE slot instead. To be sure that the command
        // will be executed before the next 6pps and not after, always round
        // down to the start of the TE.
        // Subtract some TEs to have enough time for the CORBA invocation.

        // ATTENTION:  Do not subtract any TEs here because the simulator will
        // execute the request right away.
        //executeCRDsyncTime = ((executeCRDsyncTime / TETimeUtil::TE_PERIOD_ACS)
        //    - 5ULL) * TETimeUtil::TE_PERIOD_ACS;

        // The "real" reset time, i.e. the anticipated time when the CRD will be
        // resynched, shall be in the target time system. So calculate it based
        // on the GPS time we just read.
        // Do not pass any offset since the given time stamp is already from
        // the GPS clock UTC time domain.
        realResetTime = timeOfNext6pps(gpsTime);

        // Tell the CRD to resync.
        resetTime = m_crd_sp->maserCounterReset(executeCRDsyncTime);
        // Now check if the reported resetTime has anything to do with the
        // expected reset time.
        if(std::abs(
            static_cast< long long >(resetTime)
            - static_cast< long long >(executeCRDsyncTime))
        > static_cast< long long >(TETimeUtil::TE_PERIOD_ACS))
        {
            // Just log a message that it is unfortunate that those time stamps
            // are not the same. Well, bad things happen but I am not convinced
            // that this is really so bad that it justifies an exception.
            // Long story made short, just log it for now.
            std::ostringstream msg;
            msg << "The time stamp on which the CRD has been resynched does "
                "not concur with the expected time stamp. Expected = "
                << executeCRDsyncTime
                << "[100ns], real = "
                << resetTime
                << "[100ns], difference real - expected = "
                << (resetTime - executeCRDsyncTime)
                << "[100ns].";
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        }

        // The reported CRD resetTime contains the local time plus an offset and
        // reflects the execution time of the CRD resync. Store the reset time
        // which we calculated above in the CRD memory.
        m_crd_sp->SET_RESET_TIME(realResetTime);

        // We made it, yeah!
        timeIsSynchronised = true;
    }
    catch(const ControlExceptions::MasterClockErrorExImpl& ex)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        throw ControlExceptions::MasterClockErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getMasterClockErrorEx();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        throw ControlExceptions::MasterClockErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getMasterClockErrorEx();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // This exceptions is thrown when the NTP server start faile.  This is
        // not fatal, so do not release the other components and just log the
        // exception.
        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
    }
    catch(const CORBA::SystemException& ex)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl nex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "A CORBA::SystemException has been caught.");
        nex.log();
        throw nex.getMasterClockErrorEx();
    }
    catch(...)
    {
        alarmSender->activateAlarm(TimeSynchFailed);

        releaseComponents();

        ControlExceptions::MasterClockErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Unspecified exception caught.");
        ex.log();
        throw ex.getMasterClockErrorEx();
    }

    releaseComponents();

    std::ostringstream msg;
    msg << "The MasterClock has successfully set the CRD reset time to "
        << realResetTime
        << "[100ns] and T0 for the ArrayTime to T0 = "
        << realT0
        << "[100ns]. From now on all time information on this computer will "
            "be target time system.";
    LOG_TO_OPERATOR(LM_INFO, msg.str());

    return realResetTime;
}

#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::MasterClockSimImpl)
