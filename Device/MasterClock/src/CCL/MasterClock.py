#! /usr/bin/env python
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# who       when      what
# --------  --------  ----------------------------------------------
# rsoto   2007-07-30  created 
#
# $Id$
#


"""
This module is part of the Control Command Language.
Defines the GPS class.
"""

import CCL.logging
import Acspy.Clients.SimpleClient
import Acspy.Common.ErrorTrace
import ControlExceptions


class MasterClock:
    def __init__(self, componentName = None):
        '''
        The MasterClock class is a python proxy to the MASTERCLOCK
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponent.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.MasterClock
        # create the object
        mclk = CCL.MasterClock.MasterClock()
        # Synchronize Array Time and returns
        # the time of reset in ALMA timeService units
        resetTime = mclk.syncArrayTime()
        # Get the difference betwen GPS and ALMA array time
        mclk.GET_OFFSET();
        # destroy the component
        del(mclk)
        '''
        # initialize the base class
        if componentName == None:
            componentName = "CONTROL/AOSTiming/MasterClock"
            
        self.__logger = CCL.logging.getLogger()
        self.__logger.logDebug("Creating MasterClock CCL Object.")

        self.__client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
        self.__mclk = self.__client.getComponent(componentName);

    def __del__(self):
        self.__client.releaseComponent(self.__mclk._get_name())
        self.__client.disconnect()

    def syncArrayTime(self):
        '''
        Synchronize the reference generator to the GPS 1 PPS and reset
        counters.  This command returns only after the synchronization 
        is complete, and that may require up to six seconds.        
        The reset is done in a manner so a Timing Event will always 
        coincide with daily 00:00:00 time.  This is done so that given any 
        particular time, the time of the preceding TE can be easily 
        derived.
        Return time of reset in ALMA timeService units (100 ns).
    
        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        result = 0
        timeStamp = 0
        try:
            timeStamp = self.__mclk.syncArrayTime()
        except ControlExceptions.MasterClockErrorEx, ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);
            raise
        return timeStamp

    def isSynchronised(self):
        '''
        Returns True if the MasterClock has been successfully synchronised.
        '''
        return self.__mclk.isSynchronised();

    def isNtpServerRunning(self):
        '''
        Check if the lkmLoader component for the NTP server is up and
        running.
        '''
        return self.__mclk.isNtpServerRunning()

    def startNtpServer(self):
        '''
        Start the lkmloader  component for the NTP server.
        '''
        try:
            self.__mclk.startNtpServer()
        except (ControlExceptions.DeviceBusyEx,
            ControlExceptions.INACTErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);

    def stopNtpServer(self):
        '''
        Stop the lkmLoader component for the NTP server.
        '''
        try:
            self.__mclk.stopNtpServer()
        except (ControlExceptions.DeviceBusyEx,
            ControlExceptions.INACTErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(
                self.__logger);

