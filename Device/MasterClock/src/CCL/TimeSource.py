#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# who       when      what
# --------  --------  ----------------------------------------------
# tshen/rsoto   2007-07-30  created 
#
# $Id$
#

"""
This module is part of the Control Command Language.
Defines the TimeSource class.
"""

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import Acspy.Common.Err


class TimeSource:
    def __init__(self, componentName = None):
        '''
        The TimeSource class is a python proxy to the CONTROL/TIMESOURCE
        component. The component can be running before creating this
        proxy but if it is not it will be started i.e., the object
        reference is obtained using a call to getComponent.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.TimeSource
        # create the object
        timesource = CCL.TimeSource.TimeSource()
	    # get the time of the next TE from CRD
	    nextTEtime = timesource.timeAtNextTE()
        # destroy the component
        del(timesource)
        '''
        # initialize the base class
        if componentName == None:
            componentName = "CONTROL/AOSTiming/TimeSource"
            
        self.__client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
        self.__timesource = self.__client.getComponent(componentName);

    def __del__(self):
        self.__client.releaseComponent(self.__timesource._get_name())
        self.__client.disconnect()

    def timeAtNextTE(self):
        '''
   	    Get the time of the very next TE from CRD  
        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        return self.__timesource.timeAtNextTE()
