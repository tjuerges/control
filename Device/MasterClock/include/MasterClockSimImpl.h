#ifndef MasterClockSimImpl_h
#define MasterClockSimImpl_h

/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * tjuerges  Jul 21, 2008  created
 */


#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif


#include <MasterClockImpl.h>


namespace maci
{
    class ContainerServices;
};

namespace ControlExceptions
{
    class MasterClockErrorExImpl;
};


namespace Control
{
    class MasterClockSimImpl: public Control::MasterClockImpl
    {
        public:
        MasterClockSimImpl(const ACE_CString& _name,
            maci::ContainerServices* _cs);

        virtual ~MasterClockSimImpl();

        /// This method overloads MasterClockImpl::syncArrayTime to allow
        /// testing without real hardware.
        /// \exception ControlExceptions::MasterClockErrorEx
        virtual ACS::Time syncArrayTime();

        protected:
        /// This method overloads MasterClockImpl::setUp to allow testing
        /// without real hardware.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        virtual void setUp();

        private:
        /// No default constructor.
        MasterClockSimImpl();

         /// No copy constructor.
        MasterClockSimImpl(const MasterClockSimImpl&);

        /// No assignment operator.
        MasterClockSimImpl& operator=(const MasterClockSimImpl&);
    };
};
#endif
