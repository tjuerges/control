#ifndef MasterClock_h
#define MasterClock_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <string>
// Protection Mutex for the start up and stop of the LkmLoader of the NTP
// server.
#include <Mutex.h>
//Contains the defintion of the standard superclass for C++ components
#include <acscomponentImpl.h>
// For maci::SmartPtr
#include <acsComponentSmartPtr.h>
// For exceptions:
#include <cdbErrType.h>
#include <maciErrType.h>
#include <ControlExceptions.h>
//CORBA servant header
#include <MasterClockS.h>
// CORRCommon headers
#include <teHandler.h>
#include <rtToolsFifoCmd.h>
#include <TETimeUtil.h>


namespace maci
{
    class containerServices;
};

namespace ACS_RT
{
    class lkmLoader;
}


namespace Control
{
    /// Forward declarations.
    class GPS;
    class CRD;
    class AlarmSender;


    class MasterClockImpl:
        public acscomponent::ACSComponentImpl,
        public virtual POA_Control::MasterClock
    {
        public:
        /// Constructor
        /// \param poa Poa which will activate this and also all other
        /// components.
        /// \param name component's name. This is also the name that will
        /// be used to find the configuration data for the component in the
        /// Configuration Database.
        MasterClockImpl(const ACE_CString& _name, maci::ContainerServices* _cs);

        /// Destructor
        virtual ~MasterClockImpl(void);

        /// ACS component lifecycle execute method which initialises the alarms.
        virtual void execute();

        /// ACS component lifecycle cleanUp method which calls
        /// releaseComponents() and cleans the alarms up.
        virtual void cleanUp();

        /// This method sets the TE handler time to the UTC time retrieved from
        /// the GPS reference clock.
        /// \return teTime: the time the TE handler was set to, which is the
        /// 6pps event.
        /// \exception ControlExceptions::MasterClockErrorEx
        virtual ACS::Time syncArrayTime();

        /// Check if the MasterClock is synchronised.
        /// \return True if the MasterClock has been successfully synchronised.
        virtual CORBA::Boolean isSynchronised();

        /// Start the NTP server on ARTM.  Only one
        /// instance can be active at a time. In case another
        /// instance shall be started ControlExceptions::DeviceBusyEx will be
        /// thrown.  In case of any other error
        /// ControlExceptions::InvalidRequestEx will be thrown.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        virtual void startNtpServer();

        /// Stop the NTP server on ARTM.
        /// If the NTP server is not running
        /// ControlExceptions::InvalidRequestEx will be thrown.  If the
        /// instance gives problem while shutting it down,
        /// ControlExceptions::DeviceBusyEx will be thrown.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        virtual void stopNtpServer();

        /// \return true if the NTP server - or better the LkmLoader which
        /// starts it - is active.
        virtual CORBA::Boolean isNtpServerRunning();


        /// Allow the simulation method synchArrayTime full access to these
        /// members.
        protected:
        /// Estimate by how much the CRD time will shift when synchronised with
        /// the GPS hardware, i.e. the UTC time on the GPS clock.
        /// \param localOffset: ACS;:Time which contains the offset between
        /// local UTC and UTC on the GPS clock.
        void estimateTimeShift(const ACS::Time& localOffset) const;

        /// Use ACE_OS::select to wait until an absolute ACS::Time.
        /// \param timeStamp: Time stamp to wait for.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        void waitUntil(const ACS::Time& timeStamp) const;

        /// Calculate the ACS::Time of the next 6pps TE event.
        ///
        /// \param now: Calculate 6pps event after this time.
        /// \param offset: An offset in [100ns] between local time and target
        /// time system.  It is used to convert the given time stamp to the UTC
        /// time space on the GPS clock so that the calculated TE matches one
        /// in the GPS clock.
        /// VERY IMPORTANT!
        /// ---------------
        /// First convert from/to the target time system, add/subtract offsets.
        /// Then convert to next 6pps boundary.
        inline ACS::Time timeOfNext6pps(const ACS::Time& timeStamp,
            const long long& offset = 0LL) const
        {
            // If an offset is given:
            // 1. subtract the offset from the time stamp.
            // 2. Calculate the pps event.
            // 3. add the offset again.
            return ((timeStamp - offset) / TETimeUtil::ACS_SIX_SECONDS
                * TETimeUtil::ACS_SIX_SECONDS
                + TETimeUtil::ACS_SIX_SECONDS + offset);
        };

        /// This method returns the actual UTC time stamp of the GPS clock.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        ACS::Time getGPSClockUTCTime() const;

        /// Determine the offset of the local clock (running on whatever time
        /// system) from the UTC time on the GPS clock.  The value of
        /// \ref offsetGPS2localTime is taken into account.
        /// Target time system = GPS clock UTC + offsetGPS2localTime[100ns].
        /// Offset = GPS clock UTC (converted to target time system)
        ///              - Local clock.
        /// \return Offset in 100ns.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        long long gpsClockUTCVsLocalClockOffset() const;

        /// Current status of time synchronisation.  The \ref syncArrayTime
        /// method sets this to false right at the start.  If anything fails
        /// during the sync, the status is properly set because any failure
        /// during the sync means that the time is not synchronised.  After the
        /// successful sync \ref syncArrayTime sets this to true.
        /// \ref doStartNtpServer checks if it is true.
        bool timeIsSynchronised;

        /// This method gets references for GPS and CRD. Called from setUp.
        /// \exception maciErrType::CannotGetComponentExImpl
        void getComponents();

        /// This method releases the CRD and the GPS component references.
        void releaseComponents();

        /// Initialise the helper system for alarms.
        void initialiseAlarmSystem();

        /// Clean up the helper system for alarms.
        void cleanUpAlarmSystem();

        /// Set everything up for the time synchronisation:
        /// - Get component references.
        /// - Create RT fifo data.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        virtual void setUp();

        /// Fetch a component reference of type \component. If the component
        /// reference cannot be acquired, an exception will be thrown.
        ///
        /// \param componentName The full name of the component.
        /// \param componentType The CORBA component type.
        /// \param componentVar The CORBA reference type as _var.
        /// \exception maciErrType::CannotGetComponentExImpl
        template< typename componentType >
            maci::SmartPtr< componentType > getComponentReference(
            const std::string& componentName)
        {
            AUTO_TRACE(__PRETTY_FUNCTION__);

            maci::SmartPtr< componentType > reference;
            try
            {
                reference = cs->getComponentSmartPtr< componentType >(
                    componentName.c_str());
            }
            catch(const maciErrType::CannotGetComponentExImpl& ex)
            {
                maciErrType::CannotGetComponentExImpl nex(ex, __FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
                std::ostringstream output;
                output.str("Got no reference for the ");
                output << componentName
                    << " component.";
                nex.addData("Detail", output.str());
                nex.log();
                throw nex;
            }

            return reference;
        };

        /// Pointer to the maci::ContainerServices.
        maci::ContainerServices* cs;

        ///  Reference to the CRD component.
        maci::SmartPtr< Control::CRD > m_crd_sp;

        ///  Reference to the GPS component.
        maci::SmartPtr< Control::GPS > m_gps_sp;

        /// Reference to the LkmLoader component which starts and stops the
        /// NTP server.
        maci::SmartPtr< ACS_RT::lkmLoader > lkmLoaderForNTPRef_sp;

        /// Configuration value which determines the offset between GPS and the
        /// local time.
        long long offsetGPS2localTime;

        /// Pointer for the alarm system helper instance.
        Control::AlarmSender* alarmSender;

        /// Enums for alarm conditions.
        enum MasterClockAlarmCondition
        {
            SetUpFailed = 1,
            TimeSynchFailed,
            CannotActivateNtpServer
        };


        /// Private use only. Disallow any constructors but the public one and
        /// do not allow the simulation to access the RT stuff.
        private:
        /// No default constructor.
        MasterClockImpl();

        /// No copy constructor.
        MasterClockImpl(const MasterClockImpl&);

        ///No assignment operator.
        MasterClockImpl& operator=(const MasterClockImpl&);

        /// Calculate the ACS::Time of the next 1pps GPS event.
        ///
        /// \param now: Calculate 1pps event after this time.
        /// \param offset: The offset in [100ns] between local time and UTC
        /// time on the GPS clock. It is used to convert the given time stamp
        /// to GPS clock time space, i.e. subtract or add any offsets.
        /// VERY IMPORTANT!
        /// ---------------
        /// First convert from/to target time system, add/subtract offsets.
        /// Then convert to next 1pps boundary.
        ACS::Time timeOfNext1pps(const ACS::Time& timeStamp,
            const long long& offset = 0LL) const;

        /// Wait for the next TE event.
        void waitForNextTick() const;

        /// Tell the teHandler to recognise TE events which may have been phase
        /// shifted due to the CRD resync. This forces the teHandler to resync
        /// on the some future TEs and triggers its transition from FW to HARD.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        void teHandlerResync() const;

        /// Read the teHandler clock structure.
        /// \exception ControlExceptions::MasterClockErrorExImpl
        void getTEClock(teHandlerClock_t &clk) const;

        /// Convert an rtTools FIFO communication exception into a human
        /// readable string.
        /// \param ex: rtTools FIFO communication exception.
        /// \return String representation of the exception.
        std::string rtToolsEx2String(const rtToolsFifoCmdErr_t& ex) const;

        /// Convert a teHandler error number into a human readable string.
        /// \param error: teHandler error number.
        /// \return String representation of the error.
        std::string teHandlerErr2String(const teHandlerFifoCmdStat_t& error)
            const;

        /// Helper method which executes the LkmLoader for NTP start up on the
        /// ARTM.
        /// \exception maciErrType::CannotGetComponentExImpl
        void doStartNtpServer();


        /// Name of the instance of this component. Used for CDB access.
        std::string myName;

        /// Maximum access time when the GPS hardware is queried.
        const ACS::Time MaxGPSAccessTime;

        /// teHandler command FIFO
        std::auto_ptr< rtToolsFifoCmd > m_cmdFifo;

        /// Time out [ms] for rtTools FIFO communication.
        const int rtToolsTimeout;

        /// Access mutex which protects the LkmLoader (NTP) reference.
        ACE_Mutex referencesMutex;

        /// Signals that the CRD firmware reports a correct latched value
        /// after a reset and before the first 1pps.
        bool crdFirmwareOk;
    };
};
#endif
