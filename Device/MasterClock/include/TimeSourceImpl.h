#ifndef TimeSource_h
#define TimeSource_h
#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


//Contains the defintion of the standard superclass for C++ components
#include <acscomponentImpl.h>
// For maci::SmartPtr
#include <acsComponentSmartPtr.h>

#include <ControlExceptions.h>

//CORBA servant header
#include "ControlTimeSourceInterfacesS.h"

// For the time offset thread.
#include <acsThread.h>


namespace maci
{
    class containerServices;
};


namespace Control
{
    /// Forward declarations.
    class CRD;
    class AlarmSender;
    class TimeSourceImpl;


    /// Thread which keeps track of the diversion between the local clock
    /// and the GPS clock.
    class Crd125MHzVsGpsOffsetThread: public ACS::Thread
    {
        public:
        /// Constructor.
        Crd125MHzVsGpsOffsetThread(const ACE_CString& name,
            Control::TimeSourceImpl* _ts);

        /// Destructor.
        virtual ~Crd125MHzVsGpsOffsetThread();

        /// Executed every 60s and calls
        /// \ref Control::TimeSourceImpl::calculate125MHzJitter.
        virtual void runLoop();


        private:
        /// Pointer to the \ref Control::TimeSourceImpl class.
        Control::TimeSourceImpl* ts;
    };


    class TimeSourceImpl: public virtual acscomponent::ACSComponentImpl,
        public virtual POA_Control::TimeSource
    {
        public:
        /// Constructor
        TimeSourceImpl(const ACE_CString& _name, maci::ContainerServices* _cs);

        /// Destructor
        virtual ~TimeSourceImpl();

        /// ACS component lifecycle execute method which initialises the
        /// alarms.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl thrown if the CRD
        /// component cannot be acquired.
        virtual void execute();

        /// ACS component lifecycle cleanUp method which cleans up the alarms.
        virtual void cleanUp();

        /// \return ACS::Time stamp of the next TE.
        /// \exception ControlExceptions::MasterClockErrorEx
        ACS::Time timeAtNextTE();


        private:
        /// No default constructor.
        TimeSourceImpl();

        /// No copy constructor.
        TimeSourceImpl(const TimeSourceImpl&);

        /// No assignment operator.
        TimeSourceImpl& operator=(const TimeSourceImpl&);

        /// Initialise the helper system for alarms.
        void initialiseAlarmSystem();

        /// Clean up the helper system for alarms.
        void cleanUpAlarmSystem();

        /// Reads the CRD RESET_TIME.
        /// \return CRD RESET_TIME as ACS::Time.
        ACS::Time getCrdResetTime();

        /// Checks if the CRD component is in HwOperational state.
        /// \return True if the CRD component is in HwOperational state.
        bool checkCrdAvailability() const;

        /// Checks the current jitter and the accumulated jitter against
        /// \ref Allowed125MHzJitter and \ref AllowedAccumulated125MHzJitter
        /// and triggers alarms if necessary.  This method is called
        /// by \ref Crd125MHzVsGpsOffsetThread.
        void check125MHzJitter();

        /// Calculate the jitter of the 125[MHz] signal.
        /// \param jitter contains the current jitter.
        void calculate125MHzJitter(long long& jitter);

        /// Name of the instance of this component. Used for CDB access.
        std::string myName;

        /// Pointer to the maci::ContainerServices.
        maci::ContainerServices* cs;

        /// Reference to the CRD device.
        maci::SmartPtr< Control::CRD > m_crd_sp;

        /// Pointer to the thread which keeps track of the offset between the
        /// local clock and the GPS clock.
        ACS::Thread* timeOffsetThread_p;

        /// Pointer for the alarm system helper instance.
        Control::AlarmSender* alarmSender;

        /// Enums for alarm conditions.
        enum TimeSourceAlarmCondition
        {
            CrdNotAvailable = 1,
            NoMonitorThread,
            CannotDetermineTimeAtNextTE,
            Allowed125MHzJitterExceeded,
            AllowedAccumulated125MHzJitterExceeded
        };

        /// Constant for 125MHz.
        const long long OneTwentyFiveMHz;

        /// Allowed jitter in the 125MHz maser counter in the CRD module.
        const long long Allowed125MHzJitter;

        /// Allowed accumulated offset from the 125MHz maser vs. GPS counter
        /// in the CRD module.
        const long long AllowedAccumulated125MHzJitter;

        /// CRD reset time.  Necessary to have this on because the
        /// MaserVsGpsCounter in the CRD is monotonically increasing.
        ACS::Time crdResetTime;

        /// The value of the Maser_vs_GPS_Counter monitor point of the CRD.
        /// This value is updated once every 60s by the
        /// \ref Crd125MHzVsGpsOffsetThread which calls
        /// \ref calculate125MHzJitter.
        long long previousMaserVsGpsCounter;

        /// Accumulated offset of 125MHz ticks from one GPS second.
        long long accumulated125MHzJitter;

        /// Set after the calculate125MHzJitter method has been run once.
        bool calculate125MHzJitterFirstRun;

        /// Allow the Crd125MHzVsGpsOffsetThread to call
        /// \ref calculate125MHzJitter.
        friend void Crd125MHzVsGpsOffsetThread::runLoop();
    };
};
#endif
