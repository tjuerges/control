/*******************************************************************************
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2008
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */


#include <sstream>
#include <ctime>
#include <maciSimpleClient.h>
#include <ControlTimeSourceInterfacesC.h>
#include <CRDC.h>
#include <acscommonC.h>
#include <ControlExceptions.h>
#include <TETimeUtil.h>
#include <logging.h>


int main(int argc, char** argv)
{
    // create instance of SimpleClient and init() it.
    maci::SimpleClient client;
    if(client.init(argc, argv) == 0)
    {
        ACE_DEBUG((LM_DEBUG, "Cannot init client"));
        return -1;
    }
    else
    {
        client.login();
    }

    const std::string crdName("CONTROL/AOSTiming/CRD");
    Control::CRD_var crd(Control::CRD::_nil());
    try
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Getting %s...",
            crdName.c_str()));

        crd = client.getComponent< Control::CRD > (crdName.c_str(), 0, true);
        if(CORBA::is_nil(crd.in()) == false)
        {
            CORBA::String_var tmpName(crd->name());
            ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Got %s.",
                tmpName.in()));
        }
        else
        {
            throw ControlExceptions::MasterClockErrorExImpl(__FILE__, __LINE__,
                "main");
        }
    }
    catch(...)
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_ERROR, "Could not access the "
            "%s component.", crdName.c_str()));
        client.logout();
        return -1;
    }

    const std::string timeSourceName("CONTROL/AOSTiming/TimeSource");
    Control::TimeSource_var timeSource(Control::TimeSource::_nil());
    try
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Getting %s...",
            timeSourceName.c_str()));

        timeSource = client.getComponent< Control::TimeSource > (
            timeSourceName.c_str(), 0, true);
        if(CORBA::is_nil(timeSource.in()) == false)
        {
            CORBA::String_var tmpName(timeSource->name());
            ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Got %s.",
                tmpName.in()));
        }
        else
        {
            throw ControlExceptions::MasterClockErrorExImpl(__FILE__, __LINE__,
                "main");
        }
    }
    catch(...)
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_ERROR, "Could not access the "
                "%s component.", timeSourceName.c_str()));
        CORBA::String_var tmpName(crd->name());
        client.releaseComponent(tmpName.in());
        client.logout();
        return -1;
    }

    int ret(0);
    try
    {
        crd->hwConfigure();
        crd->hwInitialize();
        crd->hwOperational();

        // Get the timeAtNextTE property
        timespec tp, tp1, tp2;
        clock_getres(CLOCK_REALTIME, &tp);
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Clock resolution is %ld "
            "%ld.", tp.tv_sec, tp.tv_nsec));
        clock_gettime(CLOCK_REALTIME, &tp1);
        const ACS::Time TEtime(timeSource->timeAtNextTE());
        clock_gettime(CLOCK_REALTIME, &tp2);
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Time at next TE = %llu",
            TEtime));

        tp.tv_sec = tp2.tv_sec - tp1.tv_sec;
        tp.tv_nsec = tp2.tv_nsec - tp1.tv_nsec;
        if(tp.tv_nsec < 0)
        {
            tp.tv_sec -= 1;
            tp.tv_nsec += 1000000000;
        }

        const double tdiff(tp.tv_sec * 1000.0 + tp.tv_nsec / 1000000.0);
        const double maxTimeAllowed(TETimeUtil::TE_PERIOD_ACS / 10000.0);

        ACS_LOG(LM_SOURCE_INFO, "main", (LM_INFO, "Duration to retrieve time "
            "is %.3f[ms], maximum allowed time = %.3f[ms].", tdiff,
            maxTimeAllowed));
    }
    catch(const ControlDeviceExceptions::HwLifecycleEx& ex)
    {
        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            "main");
        nex.addData("Detail", "Could not bring the CRD component to "
            "hwOperational state.");
        nex.log();
        ret = -1;
    }
    catch(const ControlExceptions::MasterClockErrorEx& ex)
    {
        ControlExceptions::MasterClockErrorExImpl nex(ex, __FILE__, __LINE__,
            "main");
        std::ostringstream output;
        output << "Caught a MasterClock exception while trying to execute "
            << timeSourceName
            << "/timeAtNextTE(). The error is: \n";
        nex.log();
        ret = -1;
    }
    catch(...)
    {
        ACS_LOG(LM_SOURCE_INFO, "main", (LM_ERROR, "An unexpected exception "
            "occured."));
        ret = -1;
    }

    CORBA::String_var tmpName(timeSource->name());
    client.releaseComponent(tmpName.in());
    crd->hwStop();
    tmpName = crd->name();
    client.releaseComponent(tmpName.in());
    client.logout();
    return ret;
}
