#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# $Id$
#
# who       when      what
# --------  --------  ----------------------------------------------
# bgustafs 2005-01-31 created
#


'''
Define a test class for the TimeSource component.
'''

import Acspy.Clients.SimpleClient
import Acspy.Common.TimeHelper
import Control
import unittest
import time


class TimeSourceTestCase(unittest.TestCase):
    def __init__(self, methodName = 'runTest'):
        self._client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
        # Get the GPS reference.
        self._gpsref = self._client.getComponent("CONTROL/AOSTiming/GPS")
        self._gpsref.hwConfigure()
        self._gpsref.hwInitialize()
        self._gpsref.hwOperational()
        # Get the CRD reference.
        self._crdref = self._client.getComponent("CONTROL/AOSTiming/CRD")
        self._crdref.hwConfigure()
        self._crdref.hwInitialize()
        self._crdref.hwOperational()
        # Get the masterClock reference.
        self._mcref = self._client.getComponent("CONTROL/AOSTiming/MasterClock")
        # Get the TimeSource reference.
        self._timeSourceref = self._client.getComponent("CONTROL/AOSTiming/TimeSource")
        self._out = ""
        unittest.TestCase.__init__(self, methodName)

    def __del__(self):
        print self._out
        try:
            self._client.releaseComponent(self._timeSourceref.name())
            self._client.releaseComponent(self._mcref.name())
            self._gpsref.hwStop()
            self._crdref.hwStop()
            self._client.releaseComponent(self._crdref.name())
            self._client.releaseComponent(self._gpsref.name())
        except:
            pass
        
        self._client.disconnect()

    #-------------------------------------------------
    # Test the commands
    def test1timeAtNextTE(self):
        try:
            # First synchronise the MasterClock which in turn syncs the CRD.
            # The TimeSource component uses the CRD component to estimate the
            # time stamp at the next TE.
            newTime = self._mcref.syncArrayTime()
        except Exception, ex:
            print "Exception: ", ex
            return

        # Try this test maximum five times.
        tries = 5
        timeDiff = -1.0
        timeSourceNextTE = 0
        localNextTE = 0
        while (timeDiff < 0) and (tries > 0):
            tries -= 1
            timeSourceNextTE = 0
            localNextTE = 0
            # Get the time at next TE
            try:
                timeSourceNextTE = self._timeSourceref.timeAtNextTE()
            except Exception, ex:
                print "Exception: ", ex
                return
            # Calculate the time of the next TE
            localNextTE = ((Acspy.Common.TimeHelper.getTimeStamp().value /
                            480000) + 1) * 480000
            timeDiff = (timeSourceNextTE - localNextTE) * 1e-7
            # Accept up to 0.2 second
            time.sleep(abs(timeDiff))
        if timeDiff <= 0.2:
            self._out += "Test passed."
        else:
            self._out += "Test failed, time diff = " + str(timeDiff)
            self._out += "s, TimeSource next TE = " + str(timeSourceNextTE)
            self._out += ", local next TE = " + str(localNextTE)

if __name__ == '__main__':
    unittest.main()
