s|[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}[ T][0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}.[0-9]\{1,3\}|----------T--:--:--.---|g
s|ManagerReference generated using localhost address.*|ManagerReference generated using localhost address|g
s|Local file logger: Cache saved to.*|Local file logger: Cache saved|g
s|Clock resolution is [0-9] [0-9]\+|Clock resolution is x x|g
s|Duration to retrieve time is [0-9]\+\.[0-9]\{3\}\[ms\], maximum allowed time = 48\.000\[ms\]|Duration to retrieve time is xx.xxx\[ms\], maximum allowed time = 48\.000\[ms\]|g
s|Time at next TE = [0-9]\+|Time at next TE = x|g
s/get local manager from [a-z,A-Z,0-9,-]*/get local manager from <--->/g
s/test in [0-9]*.[0-9][0-9][0-9]s/test in -.---s/g
s/Time at next TE = [0-9]* actual time = [0-9]* diff = [0-9]*/Time at next TE = ------- actual time = ------ diff = ----/g
s/Array time synchronized at time = [0-9]* actual time = [0-9]*/Array time synchronized at time = ------- actual time = -------/g
s|is [0-9]+.[0-9]{3}[ms]|is xxx.xxx[ms]|g