#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2002 
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration)
# and Cosylab 2002, All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
# MA 02111-1307  USA
#
# $Id$
#
# who       when      what
# --------  --------  ----------------------------------------------
# bgustafs 2005-09-01 created
#

'''
Define a test class for the MasterClock
'''

from ACS import acsFALSE
from ACS import acsTRUE
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.TimeHelper import getTimeStamp
import Control
import unittest

class MasterClockTestCase(unittest.TestCase):
    def __init__(self, methodName = 'runTest'):
        self._client = PySimpleClient.getInstance()
        # Get the GPS reference.
        self._gpsref = self._client.getComponent("CONTROL/AOSTiming/GPS")
        self._gpsref.hwConfigure()
        self._gpsref.hwInitialize()
        self._gpsref.hwOperational()
        # Get the CRD reference.
        self._crdref = self._client.getComponent("CONTROL/AOSTiming/CRD")
        self._crdref.hwConfigure()
        self._crdref.hwInitialize()
        self._crdref.hwOperational()

        self._ref = self._client.getComponent("CONTROL/AOSTiming/MasterClock")
        self._out = ""
        unittest.TestCase.__init__(self, methodName)

    def __del__(self):
        print self._out
        try:
            self._client.releaseComponent("CONTROL/AOSTiming/MasterClock")
            self._gpsref.hwStop()
            self._crdref.hwStop()
            self._client.releaseComponent("CONTROL/AOSTiming/CRD")
            self._client.releaseComponent("CONTROL/AOSTiming/GPS")
        except:
            pass
        
        self._client.disconnect()

    #-------------------------------------------------
    # Test the commands
    def test1syncArrayTime(self):
        sixSeconds = int(6 * 1e7)

        # Get actual time
        actualTime = getTimeStamp().value

        # Synchronize the array time
        time = 0
        try:
            time = self._ref.syncArrayTime()

            self._out += ("Array time synchronized at time = " + str(time) +
                          " actual time = ")
            self._out += str(actualTime) + "\n"

            # Give the sync at least 18 seconds. It loads components, waits
            # twice for the 6pps, etc.
            synchTime = (actualTime / sixSeconds) * sixSeconds + 3 * sixSeconds
            # OK if less than 24 seconds
            if (synchTime - time) < 3 * sixSeconds :
                self._out += "Test passed"
            else:
                self._out += ("Test failed, actual time = " + str(actualTime) +
                              ". Array synched at time ")
                self._out += (str(time) + ". Difference synch - actual = " +
                              str(time - actualTime))

        except Exception, ex:
            print "Exception: ", ex

if __name__ == '__main__':
    unittest.main()
