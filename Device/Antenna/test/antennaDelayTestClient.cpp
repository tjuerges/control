/*******************************************************************************
* ALMA - Atacama Large Millimiter Array
* (c) Associated Universities Inc., 2006
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
*
* "@(#) $Id$"
*
* who       when      what
* --------  --------  ----------------------------------------------
* jkern  2006-05-30  created
*/

/************************************************************************
 *
 *----------------------------------------------------------------------
 */


#include <hardwareDeviceImpl.h>
#include <AntennaDelayTestClientS.h>
#include <stdio.h>

class AntennaDelayTestClientImpl:
  public Control::HardwareDeviceImpl,
  public virtual POA_Control::AntennaDelayTestClient

{
 public:
  AntennaDelayTestClientImpl(const ACE_CString& name,
			 maci::ContainerServices *containerServices)
    throw(CORBA::SystemException);

  void newDelayEvent(const Control::AntennaDelayEvent& event)
    throw(CORBA::SystemException);

 protected:
  virtual void hwStartAction();
  virtual void hwConfigureAction();
  virtual void hwInitializeAction();
  virtual void hwOperationalAction();
  virtual void hwStopAction();

  long delayEventCounter_m;
};

AntennaDelayTestClientImpl::
AntennaDelayTestClientImpl(const ACE_CString& name,
			   maci::ContainerServices *containerServices)
  throw(CORBA::SystemException) :
  Control::HardwareDeviceImpl(name, containerServices),
  delayEventCounter_m(0)
{}

void
AntennaDelayTestClientImpl::newDelayEvent(const Control::AntennaDelayEvent& event)
  throw(CORBA::SystemException)
{
  ACS_TRACE("AntennaDelayTestClientImpl::newDelayEvent");
  delayEventCounter_m++;
  ACS_LOG(LM_SOURCE_INFO,"AntennaDelayTestClientImpl::newDelayEvent",
	  (LM_INFO,"Delay Event Number %d recieved.", delayEventCounter_m));

}

void AntennaDelayTestClientImpl::hwStartAction(){
  ACS_TRACE("AntennaDelayTestClientImpl::hwStartAction");
}

void AntennaDelayTestClientImpl::hwConfigureAction(){
  ACS_TRACE("AntennaDelayTestClientImpl::hwConfigureAction");
}

void AntennaDelayTestClientImpl::hwInitializeAction(){
  ACS_TRACE("AntennaDelayTestClientImpl::hwInitializeAction");
}

void AntennaDelayTestClientImpl::hwOperationalAction(){
  ACS_TRACE("AntennaDelayTestClientImpl::hwOperationalAction");
}

void AntennaDelayTestClientImpl::hwStopAction(){
  ACS_TRACE("AntennaDelayTestClientImpl::hwStopAction");
}


/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(AntennaDelayTestClientImpl)
