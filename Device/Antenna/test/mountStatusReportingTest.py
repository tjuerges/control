#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#


from Acspy.Clients.SimpleClient import PySimpleClient
from Control import DeviceConfig
from Control import ModelTerm
import unittest
import pickle
import Acspy.Util.ACSCorba
import Acspy.Common.QoS

from TMCDB_IDL import AntennaPointingModelTermSeq
from TMCDB_IDL import AntennaPointingModelTermIDL

emptyModel = []

primaryModel = [AntennaPointingModelTermIDL(1, "IA", - 179.93, 0.0),
                AntennaPointingModelTermIDL(1, "IE", + 244.76, 0.0),
                AntennaPointingModelTermIDL(1, "HACA", + 4.95, 0.0),
                AntennaPointingModelTermIDL(1, "HASA", + 0.69, 0.0),
                AntennaPointingModelTermIDL(1, "HASA2", + 0.73, 0.0),
                AntennaPointingModelTermIDL(1, "HASA3", + 0.38, 0.0),
                AntennaPointingModelTermIDL(1, "HESE", + 8.45, 0.0),
                AntennaPointingModelTermIDL(1, "HECE", + 30.33, 0.0),
                AntennaPointingModelTermIDL(1, "HESA3", + 0.94, 0.0),
                AntennaPointingModelTermIDL(1, "HECA3", - 0.34, 0.0),
                AntennaPointingModelTermIDL(1, "NPAE", + 12.14, 0.0),
                AntennaPointingModelTermIDL(1, "CA", + 317.73, 0.0),
                AntennaPointingModelTermIDL(1, "AN", - 37.96, 0.0),
                AntennaPointingModelTermIDL(1, "AW", + 16.73, 0.0)]
    
auxModel = [AntennaPointingModelTermIDL(1, "IA", - 1.0, 0.0),
            AntennaPointingModelTermIDL(1, "IE", + 2.00, 0.0),
            AntennaPointingModelTermIDL(1, "HACA", + 3.00, 0.0)]

focusModel = [ModelTerm('fooTerm', 1.0),
              ModelTerm('barTerm', 2.0)]

def configureSimulator(simulator, primaryModel, auxModel):
    # Initial Conditions
    simulator.setGlobalData("primaryModel", pickle.dumps(primaryModel))
    simulator.setGlobalData("auxModel", pickle.dumps(auxModel))
    simulator.setGlobalData('focusModel', pickle.dumps(focusModel));
    simulator.setGlobalData("usePointingModel", pickle.dumps(False))
    simulator.setGlobalData("useAuxPointingModel", pickle.dumps(False))
    simulator.setGlobalData('useFocusModel', pickle.dumps(False))
    simulator.setGlobalData('reportedAntenna', pickle.dumps(''))
    simulator.setGlobalData('reportedFocusModelAntenna', pickle.dumps(''))
    simulator.setGlobalData('reportedFocusAntenna', pickle.dumps(''))
    simulator.setGlobalData('reporteduseAuxModel', pickle.dumps(False));
    simulator.setGlobalData('reportedModel', pickle.dumps([]));
    simulator.setGlobalData('reportedAuxModel', pickle.dumps([]));
    simulator.setGlobalData('focusZero', pickle.dumps([]));
    simulator.setGlobalData('focusOffset', pickle.dumps([]));


    # Method Behavior
    code = """LOGGER.logInfo('createSubdevices called...');
              None"""
    simulator.setMethod('CONTROL/DA41/Mount', 'createSubdevices', code, 0)

    code = """import pickle
              pickle.loads(getGlobalData('primaryModel'))"""
    simulator.setMethod('CONTROL/DA41/Mount', 'getPointingModel', code, 0)

    code = """import pickle
              pickle.loads(getGlobalData('auxModel'))"""
    simulator.setMethod('CONTROL/DA41/Mount', 'getAuxPointingModel', code, 0)

    code = """import pickle
              pickle.loads(getGlobalData('usePointingModel'))"""
    simulator.setMethod('CONTROL/DA41/Mount', 'isPointingModelEnabled',
                        code, 0)

    code = """import pickle
              pickle.loads(getGlobalData('useAuxPointingModel'))"""
    simulator.setMethod('CONTROL/DA41/Mount', 'isAuxPointingModelEnabled',
                        code, 0)

    code = """import pickle;
setGlobalData('usePointingModel',pickle.dumps(parameters[0]));
None"""
    simulator.setMethod('CONTROL/DA41/Mount', 'enablePointingModel', code, 0)

    code = """import pickle;
setGlobalData('useAuxPointingModel',pickle.dumps(parameters[0]));
None"""
    simulator.setMethod('CONTROL/DA41/Mount', 'enableAuxPointingModel', code, 0)

    code = """import pickle
              pickle.loads(getGlobalData('useFocusModel'))"""
    simulator.setMethod('CONTROL/DA41/Mount', 'isFocusModelEnabled',
                        code, 0)

    code = """import pickle;
setGlobalData('useFocusModel',pickle.dumps(parameters[0]));
None"""
    simulator.setMethod('CONTROL/DA41/Mount', 'enableFocusModel', code, 0)

    code = """import pickle
              pickle.loads(getGlobalData('focusModel'))"""
    simulator.setMethod('CONTROL/DA41/Mount', 'getFocusModel', code, 0)

    code = """(1.0, 2.0, 3.0)"""
    simulator.setMethod('CONTROL/DA41/Mount', 'getFocusZeroPoint', code, 0)

    code = """(3.0, 4.0, 5.0)"""
    simulator.setMethod('CONTROL/DA41/Mount', 'getFocusOffset', code, 0)

    code = """import pickle;
LOGGER.logInfo('New Pointing model reported');
setGlobalData('reportedAntenna',pickle.dumps(parameters[0]));
setGlobalData('reporteduseAuxModel',pickle.dumps(parameters[1]));
setGlobalData('reportedModel',pickle.dumps(parameters[2]));
setGlobalData('reportedAuxModel',pickle.dumps(parameters[3]));
None"""
    simulator.setMethod('CONTROL/Array001', 'reportPointingModel', code, 0)


    code = """import pickle;
setGlobalData('reportedFocusModelAntenna', pickle.dumps(parameters[0]));
None"""
    simulator.setMethod('CONTROL/Array001', 'reportFocusModel', code, 0)

    code = """import pickle;
setGlobalData('reportedFocusAntenna', pickle.dumps(parameters[0]));
setGlobalData('useFocusModel', pickle.dumps(parameters[1]));
setGlobalData('focusZero',pickle.dumps([parameters[2],parameters[3],parameters[4]]));
setGlobalData('focusOffset',pickle.dumps([parameters[5],parameters[6],parameters[7]]));
None"""
    simulator.setMethod('CONTROL/Array001', 'reportFocusPosition', code, 0)
#def initializeAntenna(ant):
 
class MountStatusReportingTest(unittest.TestCase):
    def setUp(self):
        configureSimulator(simulator, primaryModel, auxModel)
        dc = DeviceConfig("CONTROL/DA41", [DeviceConfig("Mount", [])])
        ant.createSubdevices(dc, "")
        ant.controllerOperational()

    def tearDown(self):
        ant.releaseSubdevices()
        if ant.isAssigned():
            ant.unassign()
        ant.controllerShutdown()
            
    def test1(self):
        # The antenna is not currently assigned check that
        # nothing gets reported.
        array.reportPointingModel("", False, emptyModel, emptyModel)
        self.compareReports(['', False, [], []])

    def test2(self):
        # Assign the Antenna to an array and ensure that it reports
        ant.assign("Array001")
        array.reportPointingModel("DA41", False, emptyModel, emptyModel)
        self.compareReports(['DA41', False, [], []])
        
    def test3(self):
        # Enable the Pointing Model and ensure we get it back
        ant.assign("Array001")
        mount.enablePointingModel(True)
        array.reportPointingModel("DA41", False, primaryModel, emptyModel)
        self.compareReports(['DA41', False, primaryModel, []])

    def test4(self):
        # Enable the Aux Pointing Model and ensure we get it back
        ant.assign("Array001")
        mount.enableAuxPointingModel(True)
        array.reportPointingModel("DA41", True, emptyModel, auxModel)
        self.compareReports(['DA41', True, [], auxModel])
        
    def test5(self):
        # No Focus Model enabled ensure we get a null string
        ant.assign("Array001")
        array.reportFocusModel("DA41")
        self.compareFocusReport(['', 'DA41', False, [], []])

    def test6(self):
        # Enable Focus model and ensure we get it back
        pass

    def test7(self):
        # Get the focus positions and ensure they match
        ant.assign("Array001")
        array.reportFocusPosition("DA41", False, 10.0e-7, 2.0e-6, 3.0e-6, 3.0e-6, 4.0e-6, 5.0e-6)
        self.compareFocusReport(['DA41', '', False, [10.0e-7, 2.0e-6, 3.0e-6],
                                 [3.0e-6, 4.0e-6, 5.0e-6]])

    def test8(self):
        # Get the focus positions and ensure they match
        ant.assign("Array001")
        mount.enableFocusModel(True)
        array.reportFocusPosition("DA41", True, 10.0e-7, 2.0e-6, 3.0e-6, 3.0e-6, 4.0e-6, 5.0e-6)
        self.compareFocusReport(['DA41', '', True, [10.0e-7, 2.0e-6, 3.0e-6],
                                 [3.0e-6, 4.0e-6, 5.0e-6]])

    def compareFocusReport(self, expected):
        rFocusName = pickle.loads(simulator.
                                  getGlobalData('reportedFocusAntenna'))
        rFocusModelName = pickle.loads(simulator.
                          getGlobalData('reportedFocusModelAntenna'))
        rUseFocusModel = pickle.loads(simulator.
                                      getGlobalData('useFocusModel'))
        rFocusZero = pickle.loads(simulator.getGlobalData('focusZero'))
        rFocusOffset = pickle.loads(simulator.
                                    getGlobalData('focusOffset'))

        self.failIf(rFocusName != expected[0], 'Incorrect Focus Name')
        self.failIf(rFocusModelName != expected[1],
                    'Incorrect Focus Model Name');
        self.failIf(rUseFocusModel != expected[2], 'Use Focus Model wrong');
        for index in range(len(expected[3])):
            self.assertAlmostEquals(rFocusZero[index], expected[3][index], 7,
                'Focus Zero Incorrect')
        for index in range(len(expected[3])):
            self.assertAlmostEquals(rFocusOffset[index], expected[4][index], 7,
                'Focus Offset Incorrect')

    def compareReports(self, expected):

        rName = pickle.loads(simulator.getGlobalData('reportedAntenna'))
        rUse = pickle.loads(simulator.getGlobalData('reporteduseAuxModel'))
        rModel = pickle.loads(simulator.getGlobalData('reportedModel'))
        rAux = pickle.loads(simulator.getGlobalData('reportedAuxModel'))

        self.failIf(rName != expected[0], ('Incorrect Reported Name.  Reported name = %s, expected = %s.' % (rName, expected[0])))
        self.failIf(rUse != expected[1], ('Incorrect Aux Model State.  Reported = %s, expected = %s.' % (str(rUse), str(expected[1]))))
        self.failIf(self.comparePointingModel(rModel, expected[2]),
                    'Pointing Model Incorrect')
        self.failIf(self.comparePointingModel(rAux, expected[3]),
                    'Aux Pointing Model Incorrect')

    def comparePointingModel(self, model1, model2):
        if len(model1) != len(model2):
            return True

        for idx in range(len(model1)):
            if model1[idx].PointingModelId != model2[idx].PointingModelId:
                return True

            if model1[idx].CoeffName != model2[idx].CoeffName:
                return True

            if model1[idx].CoeffValue / model2[idx].CoeffValue < 0.99:
                return True

            if model1[idx].CoeffError != model2[idx].CoeffError:
                return True

        return False

# **************************************************
# MAIN Program
if __name__ == '__main__':
    client = PySimpleClient()
    simulator = client.getComponent("SIMULATOR")
    #Acspy.Common.QoS.setObjectTimeout(simulator, 20000)
    ant = client.getComponent("CONTROL/DA41")
    #Acspy.Common.QoS.setObjectTimeout(ant, 20000)
    array = client.getComponent("CONTROL/Array001")
    #Acspy.Common.QoS.setObjectTimeout(array, 20000)
    mount = client.getComponent("CONTROL/DA41/Mount")
    #Acspy.Common.QoS.setObjectTimeout(mount, 20000)

    unittest.main()

    client.releaseComponent("CONTROL/DA41/Mount")
    client.releaseComponent("CONTROL/Array001")
    client.releaseComponent("CONTROL/DA41")
    client.releaseComponent("SIMULATOR")

    client.disconnect()
