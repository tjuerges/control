#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#

import unittest
from Acspy.Clients.SimpleClient import PySimpleClient
import ScriptExec
from ScriptExec.configurator import Configurator
from ScriptExec.rm import ResourceManager
import CCL
import Control
import math
from ScriptExec.sb import SBObjectifier
from CCL.Antenna import Antenna

class CCLAntennaTest(unittest.TestCase):
    """Tests for the ccl.antenna class.
    
    The ccl.antenna class is really just a thin wrapper over the Antenna
    device (or at least, this is how it should be.) There's not very much
    logic involved in this component. However, it is important that the
    communication with the Antenna device is working properly and that there's
    not mismatch in the use of its interface. These types of errors are easy
    to do in Python. For this reason, we apply the following testing strategy:
    for each device that a ccl object uses a mock object is created, using the
    same interface that the real device, but extending it to add aproppiate
    testing methods.  
    """
    
    def setUp(self):
        self.client = PySimpleClient("CCLAntennaTest")
        self.conf = Configurator.get_debug_configurator()
        self.rm = ResourceManager(self.client, self.conf)
        self.rm.acquire_resources()
        
#        antenna_comp = self.rm.get_resource('AntSim01')
#        print "Antenna: " + str(antenna_comp)
#        mccompname = antenna_comp.getSubdeviceName('MountController')
#        mountcont_comp = self.rm.get_component(mccompname)
#        print "MountController: " + str(mountcont_comp)
#        self.antenna = Antenna(antenna_comp, self.rm.get_logger(), mountcont_comp)
        self.antenna = Antenna('AntSim01', self.rm.get_logger())
    
    def tearDown(self):
        self.rm.release_resources()
        self.client.disconnect()
    
    def testAntennas(self): pass
        
    def testTrack(self):
        """Test CCL.antenna._set_track_mode().
        """
        self.antenna._set_track_mode()
        
        # check that the MountController device is really in track mode
        mcdev = self.antenna.mountcontroller
        mode_prop = mcdev._get_mode()
        (mode, c) = mode_prop.get_sync()
        self.assertEquals(Control.Track, mode)
        
    def testSetHorizonDirection(self):
        """Test CCL.antenna.setDirection() passing an 'azel' keyword argument to
        move the antenna in horizon coordinates.
        """
        # Get a reference to the MountController mock device to peek at how things
        # are going using its extended testing interface.
        mc_dev = self.antenna.mountcontroller
        mc_dev.resetTestState()
        
        self.antenna.setDirection(azel=(math.radians(30.0), math.radians(15.0)))
        self.assertEquals("Antenna has been moved in horizon directions: 0.5, 0.3", 
                          mc_dev.getTestState())
        
    def testSetEquatorialDirection(self):
        
        # Get a reference to the Antenna mock device to peek at how things
        # are going using its extended testing interface.
        ant_dev = self.antenna.antenna
        ant_dev.resetTestState()
        
        # Get source object. The easiest way to do this is to read one from
        # a scheduling block using the SBObjectifier.
        f1 = open("schedblks/oneTargetSB.xml")
        xml = f1.read()
        f1.close()
        objectifier = SBObjectifier(xml)
        sbObj = objectifier.getSB()
        target = sbObj.SchedBlock.Target
        sourceId = target.fieldSourceId.getValue()
        source = objectifier.getFieldSource(sourceId)
        
        self.antenna.setDirection(source)
        expstr = "Antenna has been moved in equatorial coordinates. "
        RA = float(source.sourceCoordinates.longitude.getValue())
        Dec = float(source.sourceCoordinates.latitude.getValue())
        expstr = expstr + ("RA = %.1f DEC = %.1f" % (RA, Dec))
        self.assertEquals(expstr, ant_dev.getTestState())
      
    def testGetDirection(self):
        
        # Get a reference to the MountController mock device to peek at how things
        # are going using its extended testing interface.
        mc_dev = self.antenna.mountcontroller
        mc_dev.resetTestState()
        
        self.antenna.setDirection(azel=(math.radians(30.0), math.radians(15.0)))
        self.assertEquals("Antenna has been moved in horizon directions: 0.5, 0.3", 
                          mc_dev.getTestState())
        
        (az, el) = self.antenna.getDirection()
        self.assertAlmostEquals(az, 0.5, 1)
        self.assertAlmostEquals(el, 0.3, 1)

    def testGetPointingError(self):
        """In this case this is a very simple test case. The deviation properties
        are initialized both as 0.0, so the only thing we're testing is the right
        communication with the properties.
        """
        # Get a reference to the MountController mock device to peek at how things
        # are going using its extended testing interface.
        mc_dev = self.antenna.mountcontroller
        mc_dev.resetTestState()
        
        (azerr, elerr) = self.antenna.getPointingError()
        self.assertEquals(azerr, 0.0)
        self.assertEquals(elerr, 0.0)
            
def suite():
    suite = unittest.TestSuite()
    suite.addTest(CCLAntennaTest("testAntennas"))
    suite.addTest(CCLAntennaTest("testTrack"))
    suite.addTest(CCLAntennaTest("testSetHorizonDirection"))
    suite.addTest(CCLAntennaTest("testSetEquatorialDirection"))
    suite.addTest(CCLAntennaTest("testGetDirection"))
    suite.addTest(CCLAntennaTest("testGetPointingError"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')

#
# ___oOo___