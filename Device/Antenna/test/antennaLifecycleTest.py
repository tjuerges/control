#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2006-11-20  created
#

#************************************************************************
#   NAME antennLifecycleTest
# 
#   SYNOPSIS: Simple test to test basic lifecycle functionality.  Configuration
#             and management of subdevices is assumed to have been tested by
#             the HardwareController tests.
#
#------------------------------------------------------------------------
#


import unittest
import maciErrType
import ControlExceptions
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.QoS import setObjectTimeout
import Acspy.Util
import Acspy.Common.ErrorTrace
import Acspy.Common.Log


class lifecycleTest(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        self.stateEvents = []
        unittest.TestCase.__init__(self, methodName)

    # --------------------------------------------------
    # Test Definitions
    def test01(self):
        '''
        Very simple test to ensure that the antenna component can be
        loaded
        '''
        self.failIf(_ref == None,
                    "Unable to load and narrow component")
            
    def test02(self):
        '''
        Verify that the component throws exceptions when not operational
        and methods are called
        '''
        from Control import HardwareController
        state = _ref.getState() 
        self.failIf(state != HardwareController.Shutdown,
                    "Component initalized to incorrect state: %s" % state);

        self.failUnlessRaises(ControlExceptions.INACTErrorEx,
                              _ref.assign, "TestArray01")
        self.failUnlessRaises(ControlExceptions.INACTErrorEx,
                              _ref.unassign)
        self.failUnlessRaises(ControlExceptions.INACTErrorEx,
                              _ref.getName)
        self.failUnlessRaises(ControlExceptions.INACTErrorEx,
                              _ref.getBadResources)
        

    def test03(self):
        '''
        Verify that the antenna can be configured and brought into
        the operational state (Contoller Lifecycle test)
        '''
        self.makeOperational()
        self.shutdown()

    def test04 (self):
        '''
        Verify that the getName method returns the antenna name and not
        the component name
        '''
        self.makeOperational()
        
        name = _ref.getName()
        self.failIf(name != "DA41",
                    "Improper name (%s) returned" % name)
        self.shutdown()


    def test05(self):
        '''
        Test the ability to assign and unassign an Antenna to the Array.
        '''
        self.makeOperational()
        self.failIf(_ref.isAssigned(),
                    "Antenna is already assigned to an array")
        # This should fail since we we use an non-Sticky refernce
        try:
            _ref.assign("Array001")
            self.failIf(_ref.isAssigned(),
                    "Antenna incorrectly assigned to array")
        except maciErrType.CannotGetComponentEx, ex:
            # Assignent failed.  This is expected.
            pass

        # Now it should succeed
        _client.getComponent("CONTROL/Array001")
        _ref.assign("Array001")
        self.failIf(not _ref.isAssigned(),
                    "Antenna failed to be assigned to an array")
        _ref.unassign()
        self.failIf(_ref.isAssigned(),
                    "Antenna failed to release from the array")
        _client.releaseComponent("CONTROL/Array001")
        self.shutdown()

    def test06(self):
        '''
        Test the Antenna Lifecycle
        '''
        from Control import AntennaOperational
        from Control import AntennaNoError
        self.makeOperational()
        state = _ref.getAntennaState()
        self.failIf(state.antennaName != "DA41",
                    "Incorrect Antenna name in state event: %s" % 
                    state.antennaName)
        self.failIf(state.newState != AntennaOperational,
                    "Antenna state did not report AntennaOperational." + 
                    "Reported %s" % state.newState);
        self.failIf(state.newSubstate != AntennaNoError,
                    "Antenna substate did not report AntennaNoError. " + 
                    "Reported %s" % state.newSubstate);
        self.shutdown()

    def test07(self):
        '''
        Test that antenna state events are produced and published on the
        Control Notification Channel
        '''
        from Acspy.Nc.Consumer import Consumer
        from Control import AntennaStateEvent
        from Control import CHANNELNAME_CONTROLSYSTEM
        from Control import AntennaOperational
        from Control import AntennaShutdown
        from Control import AntennaNoError
        import time
            
        consumer = Consumer(CHANNELNAME_CONTROLSYSTEM)
        consumer.addSubscription(AntennaStateEvent, self.stateEventRx)
        consumer.consumerReady()

        self.makeOperational()
        self.shutdown()

        time.sleep(1)
        consumer.disconnect()

        
        self.failIf(len(self.stateEvents) != 2,
                    ("Improper number of events collected, received %d " + 
                     "should have been 2.") % len(self.stateEvents))

        # The first event should have been an Antenna Operational event
        self.failIf(self.stateEvents[0].antennaName != "DA41",
                    "Incorrect Antenna name in state event: %s" % 
                    self.stateEvents[0].antennaName)
        self.failIf(self.stateEvents[0].newState != AntennaOperational,
                    "Antenna state did not report AntennaOperational." + 
                    "Reported %s" % self.stateEvents[0].newState);
        self.failIf(self.stateEvents[0].newSubstate != AntennaNoError,
                    "Antenna substate did not report AntennaNoError. " + 
                    "Reported %s" % self.stateEvents[0].newSubstate);

        # The second was the antenna shutting down
        self.failIf(self.stateEvents[0].antennaName != "DA41",
                    "Incorrect Antenna name in state event: %s" % 
                    self.stateEvents[1].antennaName)
        self.failIf(self.stateEvents[1].newState != AntennaShutdown,
                    "Antenna state did not report AntennaShutdown." + 
                    "Reported %s" % self.stateEvents[1].newState);
        self.failIf(self.stateEvents[1].newSubstate != AntennaNoError,
                    "Antenna substate did not report AntennaNoError. " + 
                    "Reported %s" % self.stateEvents[1].newSubstate);

    #----------------------------
    # Utility Methods
    def makeOperational(self):
        from Control import HardwareController
        from Control import DeviceConfig
        deviceConfig = DeviceConfig("CONTROL/DA41", [])
        _ref.createSubdevices(deviceConfig, "")
        _ref.controllerOperational()
        state = _ref.getState()
        self.failIf(state != HardwareController.Operational,
                    "Component failed to become operational. Current state %s"
                    % state)

    def shutdown(self):
        from Control import HardwareController
        _ref.controllerShutdown()
        state = _ref.getState()
        self.failIf(state != HardwareController.Shutdown,
                    "Component failed to shutdown.  Current state: %s" % state)

    def stateEventRx(self, event):
        self.stateEvents.append(event)
        

# **************************************************
# MAIN Program
if __name__ == '__main__':
    setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)

    _client = PySimpleClient.getInstance()
    try:
        _ref = _client.getComponent("CONTROL/DA41")
    except maciErrType.maciErrTypeEx, ex:
        Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log(Acspy.Common.Log.getLogger());
        _ref = None
        print "Reference failed to narrow"
        raise

    setObjectTimeout(_ref, 20000)

    unittest.main()

    if _ref != None:
        _client.releaseComponent(_ref._get_name())

    _client.disconnect()
