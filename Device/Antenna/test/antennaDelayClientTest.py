#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2007-06-05  created
#


import unittest
from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Common.QoS import setObjectTimeout
from time import sleep
import Acspy.Util
import os


class delayClientTest(unittest.TestCase):
    #-------------------------------------------------
    # Test Definitions
    def test01(self):
        from Control import HardwareController
        from Control import DeviceConfig
        from CCL.SimpleDelayServer import SimpleDelayServer

        # Set up the Hardware
        delayClientConfig = DeviceConfig("AntennaDelayTestClient", []) 
        deviceConfig = DeviceConfig("CONTROL/DA41", [delayClientConfig])
        _ant.createSubdevices(deviceConfig, "")
        _ant.controllerOperational()
        state = _ant.getState()
        self.failIf(state != HardwareController.Operational,
                    "Component failed to become operational. Current state %s"
                    % state)
        _ant.assign("Array001")

        # Now we want to get the delay server up and running
        sds = SimpleDelayServer(arrayName = "Array001")
        sds.addAntenna("DA41")
        sds.setAntennaDelay("DA41", [1E-7, 0])

        sleep(180)

        # Shutdown in an orderly way
        _ant.controllerShutdown()
        state = _ant.getState()
        self.failIf(state != HardwareController.Shutdown,
                    "Component failed to shutdown.  Current state: %s" % state)
        
        cmd = "grep \"Delay Event Number\" ../test/tmp/container-0.log"
        (stdin, stdout) = os.popen2(cmd)
        data = stdout.readlines()
        self.failIf(len(data) < 3, \
                    "Apparently no delays were found")

# **************************************************
# MAIN Program
if __name__ == '__main__':
    setObjectTimeout(Acspy.Util.ACSCorba.getManager(), 20000)

    _client = PySimpleClient.getInstance()

    _ant = None
    _array = None

    try:
        _ant = _client.getComponent("CONTROL/DA41")
        _array = _client.getComponent("CONTROL/Array001")
    except:
        print "Reference failed to narrow"
        raise

    setObjectTimeout(_ant, 20000)
    setObjectTimeout(_array, 20000)
    
    unittest.main()

    if _ant != None:
        _client.releaseComponent(_ant._get_name())
    if _array != None:
        _client.releaseComponent("CONTROL/Array001")

    _client.disconnect()
