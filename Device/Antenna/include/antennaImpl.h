#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef antennaImpl_h
#define antennaImpl_h
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//


#include <AntennaS.h>
#include <hardwareControllerImpl.h>
#include <string>
// for AntennaDelayDistributor
#include <delayReceiver.h>
// For maci::SmartPtr
#include <acsComponentSmartPtr.h>
// For maciErrType::CannoGetComponentExImpl
#include <maciErrType.h>

#include <ControlDataInterfacesC.h>

// Protection Mutex for the start up and stop of the AmbManager.
#include <Mutex.h>

#include <acsComponentSmartPtr.h> // for maci::SmartPtr


// Forward declarations for classes that this component uses
namespace maci
{
    class ContainerServices;
}

namespace nc
{
    class SimpleSupplier;
}

namespace ACS_RT
{
    class lkmLoader;
    class rtlog;
}

namespace Correlator
{
    class ArrayTime;
}

namespace Control
{
    class AmbManager;
    class AlarmSender;
    class SAS;
    class LLC;
    class AntennaCallback;
    class BooleanCallback;
    class LLCPositionResetCallback;
    class DurationCallback;
};


namespace Control
{
    class AntennaImpl: public virtual POA_Control::Antenna,
        public Control::HardwareControllerImpl
    {
        public:
        // ========  Constructor & Destructor Interface =================

        /// This constructor connects to the notification channel.
        AntennaImpl(const ACE_CString& name,
            maci::ContainerServices *containerServices);

        /// The destructor disconnects from the notification channel.
        virtual ~AntennaImpl();

        // ====== Control::HardwareController Interface =================

        /// This function does all the following:
        /// 1. Gives this antenna its name e.g., "DV01". This is also used by
        /// the delay distributer (so that it can only receives its events)
        /// 2. creates all the subdevices (components) that are the children of
        /// this antenna. Normally this will starts these components. These are
        /// primarily components that correspond to the hardware installed in
        /// this antenna. Do not assume all these components will run in the
        /// same container or even on the same computer.
        /// 3. Assembles, from within these child components, all those that
        /// will be notified by the delay distributer.
        /// \exception ControlDeviceExceptions::IllegalConfigurationEx
        virtual void createSubdevices(
            const Control::DeviceConfig& configuration, const char* parentName);

        /// Start or stop the flagging of the data.
        /// \param startStop: true = start, false = stop the flagging of data.
        /// \param timeStamp: The begin or end of the data flagging.
        /// \param componentName: Something like "CONTROL/Willy/IFProc0" or
        /// "CONTROL/Foo/DRXBBPr3".
        /// \param description: This is a free format string which contains the
        /// reason for the flagging of the data.
        virtual void flagData(bool startStop, ACS::Time timeStamp,
            const char* componentName, const char* description);

        /// Method to configure the correct Walsh Function for this antenna.
        /// \param walshFunctionIndex the indexof the Walsh function to apply.
        /// \exception ControlExceptions::IllegalParameterErrorEx,
        /// \exception ControlExceptions::CAMBErrorEx,
        /// \exception ControlExceptions::INACTErrorEx
        virtual void setWalshFunction(CORBA::Long walshFunctionIndex);

        /// Method to set the frequency offset mode.  The allowed values are
        /// defined in \ref ControlDataInterfaces.idl.
        virtual void setFrequencyOffsettingMode(
            Control::LOOffsettingMode mode);

        /// Method to set the frequency index.
        void setFrequencyOffsettingIndex(CORBA::Long frequencyIndex);


        // ========= Control::AntennaMonitor interface ===================

        /// See the ControlAntennaInterfaces.idl file for a description of this
        /// function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual char* getName();

        /// See the ControlAntennaInterfaces.idl file for a description of this
        /// function.
        virtual Control::AntennaStateEvent* getAntennaState();

        /// See the ControlAntennaInterfaces.idl file for a description of this
        /// function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual Control::BadResourceSeq* getBadResources();

        /// See the ControlAntennaInterfaces.idl file for a description of this
        /// function.
        virtual bool isAssigned();

        // ========== Control::Antenna Interface ============

        /// See the Antenna.idl file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::HardwareErrorEx
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::TimeoutEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlCommonExceptions::AsynchronousFailureEx
        /// \exception ControlCommonExceptions::OSErrorEx
        /// \exception maciErrType::CannotGetComponentEx
        virtual void assign(const char* arrayName);

        virtual void assignAsynch(const char* arrayName,
            Control::AntennaCallback* cb);

        /// See the Antenna.idl file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual void unassign();

        /// See the Antenna.idl file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual char* getArray();

        /// See the Antenna.idl file for a description of this function.
        virtual CORBA::Long numDelayEventsReceived();

        /// See the Antenna.idl file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx
        virtual char* getMountControllerName();

        /// This method tries to resync the ArrayTime and all other LRUs which need
        /// to be informed about a change in the TE phase.
        /// \exception ControlExceptions::ArrayTimeErrorEx
        virtual void synchroniseArrayTime();

        /// Mount Status reporting methods
        /// \exception ControlExceptions::INACTErrorEx
        virtual void reportPointingModel();

        /// \exception ControlExceptions::INACTErrorEx
        virtual void reportFocusModel();

        /// \exception ControlExceptions::INACTErrorEx
        virtual void reportSubreflectorPosition();

        /// \exception ControlExceptions::INACTErrorEx
        virtual void reportCalDeviceData(const CalDeviceData&);


        // Interfaces for AmbManager start, stop and querying if it is
        // running.

        /// Start the AmbManager instance. See Antenna.midl for a more detailed
        /// description.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ControlExceptions::INACTErrorEx
        void startAmbManager();

        /// Stop the AmbManager instance. See Antenna.midl for a more detailed
        /// description.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::DeviceBusyEx
        void stopAmbManager();

        /// Query if the AmbManager is up and running. See Antenna.midl for a
        /// more detailed description.
        /// \return bool: true if the AmbManager is up and running.
        bool isAmbManagerRunning();


        /// @fn void selectPhotonicReference(in int photonicReferenceId)
        /// This method selects which input to the SAS is used by the
        /// antenna.  Valid choices are 1 through 6 inclusive.
        ///
        /// Execept for debugging the Asynchronous method should be used.
        ///
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        virtual void selectPhotonicReference(const char* photonicRefId);

        ///  Asynchronous version of selectPhotonicReference
        virtual void selectPhotonicReferenceAsynch(const char* photonicRefId,
                                                   Control::AntennaCallback*);

        /// getSelectedPhotonicReference()
        ///
        /// Method returns the currently selected Photonic reference.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        virtual char* getSelectedPhotonicReference();

        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        /// \exception  ControlCommonExceptions::AsynchronousFailureEx
        virtual void optimizeSASPolarization(bool forceCalibration);

        /// Asynchronous version os optimizeSASPolarization(boolean)
        virtual void optimizeSASPolarizationAsynch(bool forceCalibration,
                                                   Control::AntennaCallback*);

        /// @fn void optimizePol1ReferenceAsynch()
        /// This method optimizes the Polarization adjuster 1 of the SAS
        virtual void optimizeSASPol1Asynch(bool forceCalibration,
                                           Control::AntennaCallback*);


        /// @fn void optimizePol2ReferenceAsynch()
        /// This method optimizes the Polarization adjuster 2 of the SAS
        virtual void optimizeSASPol2Asynch(bool forceCalibration,
                                           Control::AntennaCallback*);

        /// method to enable / disable line length correction in the LLC
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        virtual void enableLLCCorrection(bool enable);

        /// Method to enable / disable line length correction in the LLC
        virtual void enableLLCCorrectionAsynch(bool enable, Control::AntennaCallback* cb);

        /// Method to get the current state of the length correction
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        virtual bool isLLCCorrectionEnabled();

        /// Method to get the current state of the length correction
        virtual void isLLCCorrectionEnabledAsynch(Control::BooleanCallback* cb);

        /// Method to estimate the remaining time before a reset of the LLC
        /// is required. The returned value is in seconds.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        virtual float getLLCTimeToReset();

        /// Asynchronous method to estimate the time remaining befer a
        /// reset of the LLC is required.
        virtual void getLLCTimeToResetAsynch(Control::DurationCallback* cb);

        /// Method to reset the LLC stretcher to the optimum position
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        /// \exception ControlExceptions::TimeoutEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::LLC::LLCPositionResetData_t resetLLCStretcher();

        /// Asynchronous method to reset the LLC stretcher to the optimum
        /// position
        virtual void resetLLCStretcherAsynch(Control::LLCPositionResetCallback* cb);

        /// Method to reset the LLC stretcher to a specific position
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        /// \exception ControlExceptions::TimeoutEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::LLC::LLCPositionResetData_t setLLCStretcherPosition(float position);

        /// Asynchronous method to reset the LLC stretcher to a specific position
        virtual void setLLCStretcherPositionAsynch(float position,
                                                 Control::LLCPositionResetCallback* cb);

        /// This method returns if a polarization calibration is required by
        /// the LLC controlled by this controller.
        /// \exception ControlExceptions::InvalidRequestEx
        /// \exception ControlExceptions::HardwareErrorEx
        /// \exception ControlExceptions::TimeoutEx
        virtual bool getLLCPolarizationCalRequired();

        /// Asynchronous version of the getPolarizationCalibrationRequired
        virtual void getLLCPolarizationCalRequiredAsynch(Control::BooleanCallback* cb);

        /// Method to perform a polarization calibration.  No asynchronous
        /// version is provided since this is expected to take a long time
        /// (5 min)
        ///
        /// If the forceCalibration flag is False, the calibration is only
        /// performed if required.
        virtual void doLLCPolarizationCalibration(ACS::Time, bool, Control::AntennaCallback*);

        protected:
        /// Throw ControlExceptions::INACTErrorEx if the antenna controller
        /// is in \ref Control::HardwareController::Shutdown.
        /// \exception ControlExceptions::INACTErrorEx
        void checkIfShutdown(const std::string& file,
            const unsigned int line, const std::string& function) const;

        /// Get a reference(non-sticky) to the SAS. Switch the optical port to
        /// To match the photonic reference, and optimize the polarization
        /// if necesary.
        /// \exception ControlExceptions::INACTErrorEx
        /// \exception ControlExceptions::CAMBErrorEx
        /// \exception ControlExceptions::TimeoutEx
        /// \exception maciErrType::CannotGetComponentEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlCommonExceptions::AsynchronousFailureEx
        /// \exception ControlCommonExceptions::OSErrorEx
        void configureSAS(const Control::ResourceId&);

        /// Methods for interacting (in C++) with the subdevices
        ACSErr::CompletionImpl waitForSASOptimization();
        maci::SmartPtr<Control::SAS> getSASReference();
        maci::SmartPtr<Control::LLC> getLLCReference();
        // ============ HardwareController Interface ============

        void startStopFlagging(bool startStop, ACS::Time timeStamp,
            const std::string& componentName, const std::string& description);

        virtual void setControllerState(
            Control::HardwareController::ControllerState newState);

        virtual void controllerOperational();

        virtual void controllerShutdown();

        // ================= ControlDevice Interface ============
        /// \exception ControlDeviceExceptions::SubstateExceptionExImpl
        virtual void setError(const std::string& errorDescription);

        /// \exception ControlDeviceExceptions::SubstateExceptionExImpl
        virtual void clearError();

        // ================ ACS Component Lifecycle =============
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void initialize();

        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();


        private:
        /// No default constructor.
        AntennaImpl();

        /// No copy constructor.
        AntennaImpl(const AntennaImpl&);

        /// No assignment operator.
        AntennaImpl& operator=(const AntennaImpl&);

        virtual void publishState();

        /// This method gets references for lkmLoader rtlog and ArrayTime.
        /// \exception maciErrType::CannotGetComponentExImpl
        void getInitComponents();

        /// Synchronise the ArrayTime component.
        /// \exception ControlExceptions::ArrayTimeErrorExImpl
        void synchArrayTime();

        /// Synchronise the local LORR with the probably phase shifted incoming
        /// TE signal.
        /// \exception ControlExceptions::ArrayTimeErrorExImpl
        void resynchroniseLORR();

        /// Sleep for sleepTime seconds. This method uses ACE_OS::select which
        /// provides microsecond resolution.
        /// \param sleepTime Time to sleep [s].
        void waitFor(const double sleepTime) const;

        /// Helper method which executes the AmbManager start up.
        /// \exception maciErrType::CannotGetComponentExImpl
        void doStartAmbManager();

        /// Initialise the helper system for alarms.
        void initialiseAlarmSystem();

        /// Clean up the helper system for alarms.
        void cleanUpAlarmSystem();

        /// Pointer for the alarm system helper instance.  Keep it protected, then
        /// inheriting classes are allowed to use it.
        Control::AlarmSender* alarmSender;

        /// Enums for alarm conditions.  Keep it protected, then
        /// inheriting classes are allowed to use it.
        enum AntennaAlarmConditions
        {
            NoKernelModules,
            ClockNotSynchronised,
            LorrNotSynchronised,
            FailedSubDevices
        };

        /// Fetch a component reference of type \component. If the component
        /// reference cannot be acquired, an exception will be thrown.
        ///
        /// \param componentName The full name of the component, e.g.
        /// CONTROL/DA41/DTXBBpr2.
        /// \param componentType The CORBA component type, e.g.
        /// Control::MasterClock
        /// \exception maciErrType::CannotGetComponentExImpl
        template< typename componentType >
        maci::SmartPtr< componentType > getComponentReference(
            const std::string& fullName)
        {
            AUTO_TRACE(__PRETTY_FUNCTION__);

            maci::SmartPtr< componentType > reference;
            try
            {
                maci::ContainerServices* cs(getContainerServices());
                reference = cs->getComponentSmartPtr< componentType >(
                    fullName.c_str());
            }
            catch(const maciErrType::CannotGetComponentExImpl& ex)
            {
                maciErrType::CannotGetComponentExImpl nex(ex, __FILE__,
                    __LINE__, __PRETTY_FUNCTION__);
                std::ostringstream output;
                output << "Did not get a reference for the "
                    << fullName
                    << " component.";
                nex.addData("Detail", output.str());
                nex.log();
                throw nex;
            }

            return reference;
        };

        AntennaDelayDistributor delayDistributor_m;

        /// The name of this Antenna.
        std::string antennaName_m;

        /// Simple supplier used to send state events
        nc::SimpleSupplier* stateEventSupplier_p;

        /// References to the components always started by this component.
        maci::SmartPtr< ACS_RT::lkmLoader > m_lkmLoaderRef_sp;
        maci::SmartPtr< ACS_RT::rtlog > m_rtLogRef_sp;
        maci::SmartPtr< Correlator::ArrayTime > m_arrayTimeRef_sp;

        /// Reference to the currently assigned array (if there is one).
        maci::SmartPtr< Control::ArrayMonitor > m_assignedArray_sp;

        /// Reference of the AmbManager instance.
        maci::SmartPtr< Control::AmbManager > m_ambManagerRef_sp;



        const ACS::Time SAS_OPTIMIZATION_TIMEOUT;

        /// Mutex which protects AmbManager start up and stop.
        ACE_Mutex m_ambManagerState;
    };
};
#endif //!ANTENNAIMPL_H
