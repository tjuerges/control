//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Dec 1, 2010  created
//


#include <antennaImpl.h>
#include <string>
#include <sstream>
#include <map>
#include <loggingMACROS.h>
#include <acsComponentSmartPtr.h>
#include <simpleCallbackImpl.h>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <LLCC.h>


maci::SmartPtr< Control::LLC > Control::AntennaImpl::getLLCReference()
{
    const std::string currentComponent("LLC");
    maci::SmartPtr< Control::LLC > llc;

    try
    {
        // Calling Control::ControlDeviceImpl::getSubdeviceName does not
        // compile because it is a template class call.
        CORBA::String_var deviceName(this->getSubdeviceName(
            currentComponent.c_str()));
        llc = getContainerServices()-> getComponentNonStickySmartPtr<
            Control::LLC >(deviceName);
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        const std::string msg = "Could not find a " + currentComponent
            + " component. Is your configuration correct?";
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", msg);
        nex.log();
        throw nex.getInvalidRequestEx();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        const std::string msg = currentComponent
            + " reference is null. Is your "
                "configuration correct?";
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", msg);
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    return llc;
}

void Control::AntennaImpl::enableLLCCorrectionAsynch(bool enable,
    AntennaCallback* cb)
{
    try
    {
        enableLLCCorrection(enable);
        cb->report(antennaName_m.c_str(), ACSErrTypeOK::ACSErrOKCompletion());
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), c);
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), c);
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
    }
}

bool Control::AntennaImpl::isLLCCorrectionEnabled()
{
    maci::SmartPtr< Control::LLC > llc;
    try
    {
        llc = getLLCReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the LLC is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    try
    {
        return llc->lengthCorrectionEnabled();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
}

void Control::AntennaImpl::isLLCCorrectionEnabledAsynch(BooleanCallback* cb)
{
    try
    {
        cb->report(antennaName_m.c_str(), isLLCCorrectionEnabled(),
            ACSErrTypeOK::ACSErrOKCompletion());
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), false, c);
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), false, c);
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), false, c);
    }
}

//FIXME: Deprecate this
float Control::AntennaImpl::getLLCTimeToReset()
{
    maci::SmartPtr< Control::LLC > llc;
    try
    {
        llc = getLLCReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the LLC is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    try
    {
        return 120;
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
}

void Control::AntennaImpl::getLLCTimeToResetAsynch(DurationCallback* cb)
{
    try
    {
        const ACS::Time timeToReset(static_cast< ACS::Time >(1e7
            * getLLCTimeToReset()));
        cb->report(antennaName_m.c_str(), timeToReset,
            ACSErrTypeOK::ACSErrOKCompletion());
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), 0, c);
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), 0, c);
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), 0, c);
    }
}

Control::LLC::LLCPositionResetData_t Control::AntennaImpl::resetLLCStretcher()
{
    maci::SmartPtr< Control::LLC > llc;
    try
    {
        llc = getLLCReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the LLC is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    try
    {
        return llc->setStretcherToOptimumPosition();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::TimeoutEx &ex)
    {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getTimeoutEx();
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    }
}

void Control::AntennaImpl::resetLLCStretcherAsynch(
    Control::LLCPositionResetCallback* cb)
{
    Control::LLC::LLCPositionResetData_t resetData;
    try
    {
        resetData = resetLLCStretcher();
        cb->report(antennaName_m.c_str(), resetData,
            ACSErrTypeOK::ACSErrOKCompletion());
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(const ControlExceptions::TimeoutEx& ex)
    {
        ControlExceptions::TimeoutCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorCompletion c(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event, probably a bug.");
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
}

Control::LLC::LLCPositionResetData_t
    Control::AntennaImpl::setLLCStretcherPosition(float position)
{
    maci::SmartPtr< Control::LLC > llc;
    try
    {
        llc = getLLCReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the LLC is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }
    try
    {
        return llc->setStretcherToPosition(position);
    }
    catch(const ControlExceptions::TimeoutEx &ex)
    {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getTimeoutEx();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    }
}

void Control::AntennaImpl::setLLCStretcherPositionAsynch(float position,
    Control::LLCPositionResetCallback* cb)
{
    Control::LLC::LLCPositionResetData_t resetData;
    try
    {
        resetData = setLLCStretcherPosition(position);
        cb->report(antennaName_m.c_str(), resetData,
            ACSErrTypeOK::ACSErrOKCompletion());
    }
    catch(const ControlExceptions::TimeoutEx &ex)
    {
        ControlExceptions::TimeoutCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorCompletion c(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event, probably a bug.");
        c.log();
        cb->report(antennaName_m.c_str(), resetData, c);
    }
}

bool Control::AntennaImpl::getLLCPolarizationCalRequired()
{
    maci::SmartPtr< Control::LLC > llc;

    try
    {
        llc = getLLCReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the LLC is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }
    try
    {
        return llc->polarizationCalRequired();
    }
    catch(const ControlExceptions::TimeoutEx &ex)
    {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getTimeoutEx();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
}

void Control::AntennaImpl::getLLCPolarizationCalRequiredAsynch(
    BooleanCallback* cb)
{
    bool required = false;

    try
    {
        required = getLLCPolarizationCalRequired();
        cb->report(antennaName_m.c_str(), required,
            ACSErrTypeOK::ACSErrOKCompletion());
    }
    catch(const ControlExceptions::TimeoutEx &ex)
    {
        ControlExceptions::TimeoutCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), required, c);
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), required, c);
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.log();
        cb->report(antennaName_m.c_str(), required, c);
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), required, c);
    }
}

void Control::AntennaImpl::doLLCPolarizationCalibration(ACS::Time timeout,
    bool forceCalibration, AntennaCallback* cb)
{
    maci::SmartPtr< Control::LLC > llc;
    try
    {
        llc = getLLCReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "It appears the LLC is no longer available.");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    try
    {
        if(llc->startPolarizationOptimization(forceCalibration) == true)
        {
            SimpleCallbackImpl localCb(
                acscomponent::ACSComponentImpl::getContainerServices());
            SimpleCallback_var cb_var = localCb._this();
            llc->waitForPolarizationCalibration(timeout,
                cb_var);
            localCb.waitForCompletion(timeout + 1000000000); // +10 s
        }
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::TimeoutExImpl& ex)
    {
        ControlExceptions::TimeoutCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlCommonExceptions::OSErrorExImpl& ex)
    {
        ControlCommonExceptions::OSErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlCommonExceptions::AsynchronousFailureExImpl& ex)
    {
        ControlCommonExceptions::AsynchronousFailureCompletion c(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    cb->report(antennaName_m.c_str(), ACSErrTypeOK::ACSErrOKCompletion());
}
