#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#


import CCL.logging
import CCL.HardwareController
import Control
import CCLExceptionsImpl

#--- Included Device CCL Objects ---
import CCL.DGCK
import CCL.OpticalTelescope


class Antenna(CCL.HardwareController.HardwareController):
    def __init__(self, antennaName = None, stickyFlag = True):
        '''
        The constructor creates a CCL.Antenna object. Which communicates
        with the antenna component on the given antenna.

        TODO:
        This instance is always instantiated as a sticky component. Change
        it to be non-sticky.
        '''
        self.__logger = CCL.logging.getLogger()

        if antennaName == None:
            ex = CCLExceptionsImpl.BadParameterErrorExImpl()
            ex.setData("Detail", "You must provide an antenna name!")
            ex.log(self.__logger)
            raise ex
            
        CCL.HardwareController.HardwareController.__init__(
           self, "CONTROL/" + antennaName, stickyFlag)

        self.__logger.logDebug("Creating Antenna CCL Object for antenna '"
                               + antennaName + "'")

    def __del__(self):
        CCL.HardwareController.HardwareController.__del__(self)

    def setBand(self, band):
        '''
        Switches the receiver in the antenna to use the specified band.

        Normally this is not necessary as the setSkyFrequency
        command will do this.  However, frequencies between 84-90 GHz
        can be observed using either the band 2 or band 3
        receivers. Using this function prior to calling the
        setSkyFrequency will determine which band is used in this
        situation.  This method returns immediately, even though the
        actual process of tuning may take some time.
        
        Parameters:
        band - A member of ControlSB::ReceiverBandType enumeration.
        
        Notes:
        - This method hasn't been implemented, as there is no Receiver 
        device driver specified yet.
        
        See also:
        - ICD/OFFLINE/idl/ControlSB.idl for documentation about the
        ReceiverBandType enumeration.
        '''
        errHlp = CCLExceptionsImpl.NotYetImplementedExImpl()
        errHlp.log(self.__logger)
        raise errHlp

    def getSkyFrequency(self):
        '''
        Gets the frequency from the antenna receiver.

        Returns: antenna receiver frequency in Hz.

        Notes:
        - This method hasn't been implemented, as there is no Receiver 
        device driver specified yet.            
        '''
        errHlp = CCLExceptionsImpl.NotYetImplementedExImpl()
        errHlp.log(_se)
        raise errHlp

    def setSkyFrequency(self, freq):
        """Tunes the receiver to the given frequency.
    
        This method will switch the receiver to use an appropiate band
        (if necessary). This method returns immediately, even though the
        actual process of tuning may take some time.
        
        Parameters:
        freq - The desired frequency, in Hz.
        
        Notes:
        - This method hasn't been implemented, as there is no Receiver device 
        driver specified yet.            
        """
        errHlp = CCLExceptionsImpl.NotYetImplementedExImpl()
        errHlp.log(self.__logger)
        raise errHlp

    def isLocked(self):
        """Is the antenna receiver phase locked?

        Returns: 1 if the antenna receiver is phase locked; 0 if not.

        Notes:
        - This method hasn't been implemented, as there is no Receiver device 
        driver specified yet.                    
        """
        errHlp = CCLExceptionsImpl.NotYetImplementedExImpl()
        errHlp.log(self.__logger)
        raise errHlp

    def getReceiver(self):
        """Get the Receiver for this antenna.

        Returns the CCL Receiver object for the current
        antenna receiver. Using this Receiver object, a script may gain access
        to the LO chain, FTS, Baseband Processor, and other elements of
        both the "Front End" and "Back End".
        
        Returns: Receiver CCL object.

        Notes:
        - This method hasn't been implemented, as there is no Receiver device 
        driver specified yet.                    
        """
        errHlp = CCLExceptionsImpl.NotYetImplementedExImpl()
        errHlp.log(self.__logger)
        raise errHlp

    def getNutator(self):
        """Get the Nutator for this antenna.

        This method returns a Nutator CCL object for this antenna.
        
        Returns: Nutator CCL object.

        Notes:
        - This method hasn't been implemented, as there is no Receiver device 
        driver specified yet.                    
        """
        errHlp = CCLExceptionsImpl.NotYetImplementedExImpl()
        errHlp.log(self.__logger)
        raise errHlp

    def getNumDelayEventsReceived(self):
        '''
        This method will return the number of delay events that have
        been recieved since this antenna was assigned to an array.
        '''
        return self._HardwareController__controller.numDelayEventsReceived()

    def getOptTelescope(self):
        '''
        Get the Optical Telescope for this antenna.

        Returns: CCL.OptTelescope object.
        '''
        optTelImpl = self._HardwareController__controller.getSubdeviceName(
           "OpticalTelescope")
        return CCL.OpticalTelescope.OpticalTelescope(optTelImpl)

    def getDGCK(self):
        '''
        Get the Digitizer Clock for this antenna

        Return: CCL.DGCK object.
        '''
        dgckCompName = self._HardwareController__controller.getSubdeviceName(
             "DGCK")
        return CCL.DGCK.DGCK(componentName = dgckCompName)

    def status(self):
        '''
        Get the combined status, formed by asking the underlying
        controllers for their state and combining them.
    
        See ScriptImpl.CCLObject for the complete documentation of this
        method.
        '''
        devStates = {Control.HardwareController.Shutdown: 'Shutdown',
                     Control.HardwareController.Operational: 'Operational',
                     Control.HardwareController.Degraded: 'Degraded'}
        st = self._HardwareController__controller.getState()
        return devStates[st]

    def synchroniseArrayTime(self):
        '''
        Synchronise the Antenna ABM with the TimeSource component which
        runs in the ARTM.
        '''
        self._HardwareController__controller.synchroniseArrayTime()

    def isAmbManagerRunning(self):
        '''
        Check if the AmbManager component for this antenna instance is up and
        running.
        '''
        return self._HardwareController__controller.isAmbManagerRunning()

    def startAmbManager(self):
        '''
        Start the AmbManager component for the antenna. If the antenna is not in
        controllerOperational state, the start will fail with an exception.
        '''
        self._HardwareController__controller.startAmbManager()

    def stopAmbManager(self):
        '''
        Stop the AmbManager component.
        '''
        self._HardwareController__controller.stopAmbManager()

    def getSubdeviceList(self):
        '''
        Return a list of all devices installed at Antenna
        '''
        device_list = []
        device_map = self._HardwareController__controller.getSubdeviceList()
        for device in device_map:
            dev = str(device)
            device_list.append(dev.split('\'')[1])
            print dev.split('\'')[1]
        return device_list

    def setWalshFunction(self, walshFunctionIndex):
        '''
        Set the Walsh function index in all DTXes and the FLOOG.
        '''
        self._HardwareController__controller.setWalshFunction(walshFunctionIndex)

    def setFrequencyOffsettingMode(self, mode):
        '''
        Set the frequency offset mode in all LO2s and the FLOOG.
        '''
        self._HardwareController__controller.setFrequencyOffsettingMode(mode)

    def setFrequencyOffsettingIndex(self, frequencyIndex):
        '''
        Set the frequency offset mode in all LO2s and the FLOOG.
        '''
        self._HardwareController__controller.setFrequencyOffsettingIndex(frequencyIndex)
