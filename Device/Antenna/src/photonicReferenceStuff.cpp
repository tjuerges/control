//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Dec 1, 2010  created
//


#include <antennaImpl.h>
#include <sstream>
#include <loggingMACROS.h>
#include <simpleCallbackImpl.h>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <SASC.h>


// this dude:
// InvalidRequestEx
// HardwareErrorEx
// IllegalParameterErrorEx
void Control::AntennaImpl::selectPhotonicReference(const char* photoRef)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    maci::SmartPtr< Control::SAS > sas;
    try
    {
        sas = getSASReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the SAS is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    try
    {
        CORBA::String_var currentReference;
        try
        {
            currentReference = sas->getPhotonicReference();
        }
        catch(const ControlExceptions::HardwareErrorEx& ex)
        {
            // Hardware returned something bogus
            currentReference = CORBA::string_dup("NeedToSetIt");
        }

        // Check to see if the port is different if so set it
        if(std::strcmp(photoRef, currentReference.in()))
        {
            sas->setPhotonicReference(photoRef);
        }
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::IllegalParameterErrorExImpl& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    }
}

void Control::AntennaImpl::selectPhotonicReferenceAsynch(const char* photoRef,
    Control::AntennaCallback* cb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try
    {
        selectPhotonicReference(photoRef);
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorCompletion c(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    cb->report(antennaName_m.c_str(), ACSErrTypeOK::ACSErrOKCompletion());
}

// this dude:
// InvalidRequestEx
// HardwareErrorEx
// IllegalParameterErrorEx
char* Control::AntennaImpl::getSelectedPhotonicReference()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    maci::SmartPtr< Control::SAS > sas;
    try
    {
        sas = getSASReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg = "It appears the SAS is no longer available.";
        nex.addData("Detail", msg);
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    try
    {
        CORBA::String_var pr(sas->getPhotonicReference());
        return pr._retn();
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
}

