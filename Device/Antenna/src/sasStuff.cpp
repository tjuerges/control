//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Dec 1, 2010  created
//


#include <antennaImpl.h>
#include <string>
#include <sstream>
#include <map>
#include <loggingMACROS.h>
#include <acsComponentSmartPtr.h>
#include <simpleCallbackImpl.h>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <SASC.h>


/// The following two method should be turned into a template, in a common
/// place in control, since similar functionality is need in the FrontEnd.
maci::SmartPtr< Control::SAS > Control::AntennaImpl::getSASReference()
{
    const std::string currentComponent("SAS");
    maci::SmartPtr< Control::SAS > sas;

    try
    {
        // Calling Control::ControlDeviceImpl::getSubdeviceName does not
        // compile because it is a template class call.
        CORBA::String_var deviceName(this->getSubdeviceName(
            currentComponent.c_str()));
        sas = getContainerServices()->getComponentNonStickySmartPtr<
            Control::SAS >(deviceName);
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        const std::string msg = "Could not find a " + currentComponent
            + " component. Is your configuration correct?";
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", msg);
        nex.log();
        throw nex.getInvalidRequestEx();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        const std::string msg = currentComponent + " reference is null. Is "
            "your configuration correct?";
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", msg);
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    return sas;
}

/// As you read through the following code you will notice the exception
/// mayhem.  Nothing I could really do about it, just package and re-throw. I
/// know its a mess and I duplicated exception handling, but if something
/// breaks (which it probably will) we will have more sensible error messages,
/// hence a better time debugging.
void Control::AntennaImpl::configureSAS(const Control::ResourceId& photoRef)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        selectPhotonicReference(photoRef.ResourceName.in());
    }
    catch(const ControlExceptions::InvalidRequestEx &ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getInvalidRequestEx();
    }
    catch(const ControlExceptions::HardwareErrorEx &ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::IllegalParameterErrorEx &ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getIllegalParameterErrorEx();
    }

    /// By now the correct photonic reference is set. Lets optimize.
    /// \note to myself or the future maintainer:
    ///   - The optimization will be done in a synchronous manner, this is
    ///     intended. Since we do not want to start the observation without
    ///     knowing the result of this.
    try
    {
        optimizeSASPolarization(false);
    }
    catch(const ControlExceptions::InvalidRequestEx &ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getInvalidRequestEx();
    }
    catch(const ControlExceptions::HardwareErrorEx &ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::TimeoutExImpl& ex)
    {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getTimeoutEx();
    }
    catch(const ControlCommonExceptions::OSErrorExImpl& ex)
    {
        ControlCommonExceptions::OSErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getOSErrorEx();
    }
    catch(const ControlCommonExceptions::AsynchronousFailureExImpl& ex)
    {
        ControlCommonExceptions::AsynchronousFailureExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getAsynchronousFailureEx();
    }

    std::ostringstream msg;
    msg << "Antenna: "
        << antennaName_m
        << " has set the SAS photonic reference to: "
        << photoRef.ResourceName.in()
        << ". SAS polarization has been optimized.";
    LOG_TO_OPERATOR(LM_INFO, msg.str());
}

void Control::AntennaImpl::optimizeSASPolarization(bool forceCalibration)
{
    maci::SmartPtr< Control::SAS > sas;

    try
    {
        sas = getSASReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the SAS is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    try
    {
        // We are running the show now. Abort any calibrations that is being
        // done.
        sas->abortPolarizationCalibration();
        sas->startPol1Calibration(forceCalibration);

    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getHardwareErrorEx();
    }

    // Wait and check for the completion of startPol1Calibration
    ACSErr::CompletionImpl completion(waitForSASOptimization());
    if(completion.isErrorFree() == false)
    {
        ControlCommonExceptions::AsynchronousFailureExImpl nex(completion,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "There was a problem reported during "
            "startPol1Calibration");
        nex.log();
        throw nex.getAsynchronousFailureEx();
    }

    try
    {
        sas->startPol2Calibration(forceCalibration);
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.log();
        throw nex.getHardwareErrorEx();
    }
    ACSErr::CompletionImpl completion2 = waitForSASOptimization();
    if(!completion2.isErrorFree())
    {
        ControlCommonExceptions::AsynchronousFailureExImpl nex(completion2,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "There was a problem reported during "
            "startPol2Calibration");
        nex.log();
        throw nex.getAsynchronousFailureEx();
    }
}

void Control::AntennaImpl::optimizeSASPol1Asynch(bool forceCalibration,
    Control::AntennaCallback* cb)
{
    maci::SmartPtr< Control::SAS > sas;
    try
    {
        sas = getSASReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "It appears the SAS is no longer available.");
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    try
    {
        if(sas->startPol1Calibration(forceCalibration) == false)
        {
            cb->report(antennaName_m.c_str(),
                ACSErrTypeOK::ACSErrOKCompletion());
            return;
        }
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    cb->report(antennaName_m.c_str(), waitForSASOptimization());
}

void Control::AntennaImpl::optimizeSASPol2Asynch(bool forceCalibration,
    Control::AntennaCallback* cb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    maci::SmartPtr< Control::SAS > sas;
    try
    {
        sas = getSASReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "It appears the SAS is no longer available.");
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    try
    {
        if(sas->startPol2Calibration(forceCalibration) == false)
        {
            cb->report(antennaName_m.c_str(),
                ACSErrTypeOK::ACSErrOKCompletion());
            return;
        }
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    cb->report(antennaName_m.c_str(), waitForSASOptimization());
}

void Control::AntennaImpl::optimizeSASPolarizationAsynch(bool forceCalibration,
    Control::AntennaCallback* cb)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    maci::SmartPtr< Control::SAS > sas;
    try
    {
        sas = getSASReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        const std::string msg = "It appears the SAS is no longer available.";
        c.addData("Detail", msg);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    try
    {
        if(sas->startPol1Calibration(forceCalibration) == true)
        {
            ACSErr::CompletionImpl completion = waitForSASOptimization();
            if(completion.isErrorFree() == false)
            {
                cb->report(antennaName_m.c_str(), completion);
                return;
            }
        }

        if(sas->startPol2Calibration(forceCalibration) == true)
        {
            ACSErr::CompletionImpl completion = waitForSASOptimization();
            if(completion.isErrorFree() == false)
            {
                cb->report(antennaName_m.c_str(), completion);
                return;
            }
        }
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    cb->report(antennaName_m.c_str(), ACSErrTypeOK::ACSErrOKCompletion());
}

ACSErr::CompletionImpl Control::AntennaImpl::waitForSASOptimization()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Create a callback object
    SimpleCallbackImpl cb(
        acscomponent::ACSComponentImpl::getContainerServices());
    // get the corba object that will be passed tp the SAS
    maci::SmartPtr< Control::SAS > sas;
    try
    {
        sas = getSASReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "It appears the SAS is no longer available.");
        return c;
    }

    // register the cb in the SAS
    ///
    /// TODO
    /// Thomas, May 26, 2011
    /// The duplication of the callback object should not be necessary but in
    /// fact is.  Until it has been investigated why, when the callback object
    /// is not duplicated, the CORBA::is_nil call inside the callback class
    /// segfaults, this should remain here.  Once we have found a remedy, the
    /// call to duplicate can be removed and the returned object be directly
    /// used.
    SimpleCallback_var cb_var = Control::SimpleCallback::_duplicate(
        cb.getExternalReference());
    sas->waitForCalibration(cb_var,
        SAS_OPTIMIZATION_TIMEOUT);
    try
    {
        cb.waitForCompletion(SAS_OPTIMIZATION_TIMEOUT); // 60 Seconds
    }
    catch(const ControlExceptions::TimeoutExImpl& ex)
    {
        return ControlExceptions::TimeoutCompletion(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }
    catch(const ControlCommonExceptions::OSErrorExImpl& ex)
    {
        return ControlCommonExceptions::OSErrorCompletion(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
    }
    catch(const ControlCommonExceptions::AsynchronousFailureExImpl& ex)
    {
        return ControlCommonExceptions::AsynchronousFailureCompletion(ex,
            __FILE__, __LINE__, __PRETTY_FUNCTION__);
    }

    return ACSErrTypeOK::ACSErrOKCompletion();
}

void Control::AntennaImpl::enableLLCCorrection(bool enable)
{
    maci::SmartPtr< Control::LLC > llc;
    try
    {
        llc = getLLCReference();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "It appears the LLC is no longer available.");
        nex.log();
        throw nex.getInvalidRequestEx();
    }

    try
    {
        llc->enableLengthCorrection(enable);
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getHardwareErrorEx();
    }
}

