//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Dec 1, 2010  created
//


#include <antennaImpl.h>
#include <string>
#include <sstream>
#include <map>
#include <loggingMACROS.h>
#include <acsComponentSmartPtr.h>
//#include <simpleCallbackImpl.h>
#include <ControlArrayInterfacesC.h>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <MountC.h>
#include <CReceiverBand.h>


char* Control::AntennaImpl::getMountControllerName()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    const std::string mcName("CONTROL/" + antennaName_m + "/MountController");
    CORBA::String_var retValue(mcName.c_str());
    return retValue._retn();
}

void Control::AntennaImpl::reportPointingModel()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    if(isAssigned() == false)
    {
        // Nothing to do here since we have no parent to report to
        return;
    }
    Control::ArrayCommand_var arrayCommand_ref(Control::ArrayCommand::_narrow(
        GetImpl(m_assignedArray_sp)));

    if(CORBA::is_nil(arrayCommand_ref) == true)
    {
        std::ostringstream msg;
        msg << "Unable to narrow parent reference";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str());
        return;
    }

    std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.find("Mount"));
    if(iter == subdevice_m.end())
    {
        std::ostringstream msg;
        msg << "Request for pointing model on antenna " << antennaName_m
            << " which does not seem to have a mount component.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        return;
    }

    // Get the Mount Component
    if(CORBA::is_nil(iter->second) == true)
    {
        std::ostringstream msg;
        msg << "Request for pointing model on antenna " << antennaName_m
            << " which failed to start a mount component.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        return;
    }

    Control::Mount_var mount_ref(Control::Mount::_narrow(iter->second));
    if(CORBA::is_nil(mount_ref) == true)
    {
        std::ostringstream msg;
        msg << "Can not narrow Mount component to get pointing model on "
            << "antenna " << antennaName_m << ".";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
        return;
    }

    TMCDB::PointingModel_var pointingModel(new TMCDB::PointingModel);
    TMCDB::PointingModel_var auxPointingModel(new TMCDB::PointingModel);
    if(mount_ref->isPointingModelEnabled())
    {
        pointingModel = mount_ref->getPointingModel();
    }
    else
    {
        pointingModel->length(0);
    }

    if(mount_ref->isAuxPointingModelEnabled() == true)
    {
        auxPointingModel = mount_ref->getAuxPointingModel();
    }
    else
    {
        auxPointingModel->length(0);
    }

    const int bandNum = std::abs(mount_ref->getPointingModelBand());
    const ReceiverBandMod::ReceiverBand bandEnum = CReceiverBand::from_int(
        bandNum - 1);
    arrayCommand_ref->reportPointingModel(antennaName_m.c_str(), bandEnum,
        pointingModel.in(), auxPointingModel.in());
}

void Control::AntennaImpl::reportFocusModel()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    if(isAssigned() == false)
    {
        // Nothing to do here since we have no parent to report to
        return;
    }
    Control::ArrayCommand_var arrayCommand_ref(Control::ArrayCommand::_narrow(
        GetImpl(m_assignedArray_sp)));
    if(CORBA::is_nil(arrayCommand_ref) == true)
    {
        LOG_TO_DEVELOPER(LM_WARNING, "Unable to narrow parent reference";);
        return;
    }

    std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.find("Mount"));
    if(iter == subdevice_m.end())
    {
        std::ostringstream msg;
        msg << "Request for focus model on antenna "
            << antennaName_m
            << " which does not seem to have a mount component.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        return;
    }

    // Get the Mount Component
    if(CORBA::is_nil(iter->second) == true)
    {
        std::ostringstream msg;
        msg << "Request for focus model on antenna "
            << antennaName_m
            << " which failed to start a mount component.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        return;
    }

    Control::Mount_var mount_ref(Control::Mount::_narrow(iter->second));
    if(CORBA::is_nil(mount_ref) == true)
    {
        std::ostringstream msg;
        msg << "Can not narrow Mount component to get focus model on antenna "
            << antennaName_m
            << ".";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
        return;
    }

    TMCDB::FocusModel_var focusModel(new TMCDB::FocusModel);
    if(mount_ref->isFocusModelEnabled() == true)
    {
        focusModel = mount_ref->getFocusModel();
    }
    else
    {
        focusModel->length(0);
    }

    const int bandNum = std::abs(mount_ref->getFocusModelBand());
    const ReceiverBandMod::ReceiverBand bandEnum = CReceiverBand::from_int(
        bandNum - 1);
    arrayCommand_ref->reportFocusModel(antennaName_m.c_str(), bandEnum,
        focusModel.in());
}

void Control::AntennaImpl::reportSubreflectorPosition()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    if(isAssigned() == false)
    {
        // Nothing to do here since we have no parent to report to
        return;
    }

    Control::ArrayCommand_var arrayCommand_ref(Control::ArrayCommand::_narrow(
        GetImpl(m_assignedArray_sp)));
    if(CORBA::is_nil(arrayCommand_ref) == true)
    {
        LOG_TO_DEVELOPER(LM_WARNING, "Unable to narrow parent reference");
        return;
    }

    std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.find("Mount"));
    if(iter == subdevice_m.end())
    {
        std::ostringstream msg;
        msg << "Request for subreflector position on antenna "
            << antennaName_m
            << " which does not seem to have a mount component.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        return;
    }

    // Get the Mount Component
    if(CORBA::is_nil(iter->second) == true)
    {
        std::ostringstream msg;
        msg << "Request for subreflector on antenna "
            << antennaName_m
            << " which failed to start a mount component.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        return;
    }

    Control::Mount_var mount_ref(Control::Mount::_narrow(iter->second));
    if(CORBA::is_nil(mount_ref) == true)
    {
        std::ostringstream msg;
        msg << "Can not narrow Mount component to get pointing model on "
            "antenna "
            << antennaName_m
            << ".";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
        return;
    }

    double x(0.0);
    double y(0.0);
    double z(0.0);
    double xOffset(0.0);
    double yOffset(0.0);
    double zOffset(0.0);
    const bool useFocusModel = mount_ref->isFocusModelEnabled();

    mount_ref->getSubreflectorPosition(x, y, z);
    mount_ref->getSubreflectorPositionOffset(xOffset, yOffset, zOffset);

    arrayCommand_ref->reportSubreflectorPosition(antennaName_m.c_str(),
        useFocusModel, x, y, z, xOffset, yOffset, zOffset);
}
