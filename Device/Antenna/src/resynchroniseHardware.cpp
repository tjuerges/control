//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  May 1, 2008  created


#include <string>
#include <antennaImpl.h>
#include <TETimeUtil.h>
#include <acsutilTimeStamp.h>
#include <CorrErr.h>
#include <ControlTimeSourceInterfacesC.h>
#include <LORRC.h>
// For AUTO_TRACE.
#include <loggingMACROS.h>
// For maci::SmartPtr
#include <acsComponentSmartPtr.h>
#include <ArrayTimeC.h>
#include <TETimeUtil.h>

// Alarm system stuff.
#include <controlAlarmSender.h>


void Control::AntennaImpl::synchroniseArrayTime()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    /**
     * 1. Synchronise the LORR with the incoming TE signal.
     * 2. If 1. was successful, i.e. no exception, synchronise the ArrayTime
     * component to the new TE phase.
     */

    ///
    /// TODO
    /// in the relevant components:
    /// Thomas, Apr 30, 2008
    ///
    /// Resync all of the following devices with the changed TE after ArrayTime
    /// synch was successful. In paranthesis is the number of LRUs per antenna
    /// given.
    /// - DTX (4)
    /// - TPD (2) inside the IFDC/IFproc.
    /// - LO2 (4)
    /// - FLOOG (1) resynch necessary?
    try
    {
        resynchroniseLORR();
        synchArrayTime();
    }
    catch(const ControlExceptions::ArrayTimeErrorExImpl& ex)
    {
        throw ControlExceptions::ArrayTimeErrorExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__).getArrayTimeErrorEx();
    }
}

void Control::AntennaImpl::resynchroniseLORR()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    const std::string lruType("LORR");

    std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.find(lruType));
    if(iter == subdevice_m.end())
    {
        if(alarmSender->isAlarmSet(LorrNotSynchronised) == false)
        {
            alarmSender->activateAlarm(LorrNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "No "
            << lruType
            << " component configured. Not resynchronising.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex;
    }
    else if(CORBA::is_nil((*iter).second) == true)
    {
        if(alarmSender->isAlarmSet(LorrNotSynchronised) == false)
        {
            alarmSender->activateAlarm(LorrNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "The device which is configured as "
            << lruType
            << " is not narrowable to a LORR. Not resynchronising.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex;
    }

    try
    {
        Control::LORR_var lruRef(Control::LORR::_nil());
        lruRef = Control::LORR::_narrow((*iter).second);
        if(CORBA::is_nil(lruRef.in()) == true)
        {
            std::ostringstream output;
            output << "Could not narrow the "
                << lruType
                << " component. Not resynchronising it.";
            ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", output.str());
            // Do not log here.  The exception will be logged one level higher,
            // i.e. in the following catch block of this method.
            throw ex.getINACTErrorEx();
        }

        // Now put the LORR into Configure state so that Control and Monitor
        // points which are, according to the spreadsheet, available in startup
        // can be executed.
        lruRef->hwConfigure();

        // Clear the TE related error flags first.
        lruRef->SET_CLEAR_FLAGS();
        // Tell the LORR to synchronise with the incoming TE signal.
        lruRef->SET_RESYNC_TE();

        /**
         * Now wait for one TE (or a bit more) so that the LORR has enough time
         *  to update the status flags.
         */
        waitFor(static_cast< double >(TETimeUtil::TE_PERIOD_ACS) * 1e-7);

        /**
         * Check the TE related status flags. Those are according to the ICD
         * (bits start at #0):
         * - bit #6: If the TE detected on the composite is fewer that 6000000
         *   cycles of the 125MHz clock (TE too short).
         * - bit #7: If the TE detected on the composite is more that 6000000
         *   cycles of the 125MHz clock (TE too long).
         */
        ACS::Time timeStamp(0ULL);
        Control::LongSeq_var lorrStatus(lruRef->GET_STATUS(timeStamp));

        // Put the LORR back into Start state.
        lruRef->hwStop();
        lruRef->hwStart();

        // Check the status of the 125MHz signal.  Bit set means trouble.
        if((lorrStatus[0] & (1L << 5L)) != 0L)
        {
            // We are in trouble.
            ControlExceptions::ANTErrorExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            std::ostringstream output;
            output << "The "
                << lruType
                << " reports an unsynchronised TE  signal. Please check "
                    "that the LORR is in good shape and that the incoming "
                    "TE signal is alive.";
            ex.addData("Detail", output.str());
            // Do not log here.  The exception will be logged one level higher,
            // i.e. in the following catch block of this method.
            throw ex;
        }
        else
        {
            if(alarmSender->isAlarmSet(LorrNotSynchronised) == true)
            {
                alarmSender->deactivateAlarm(LorrNotSynchronised);
            }
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        if(alarmSender->isAlarmSet(LorrNotSynchronised) == false)
        {
            alarmSender->activateAlarm(LorrNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "Could not get a reference for the "
            << lruType
            << " component. Not resynchronising it.";
        nex.addData("Detail", output.str());
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        if(alarmSender->isAlarmSet(LorrNotSynchronised) == false)
        {
            alarmSender->activateAlarm(LorrNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "A problem occured while CAN-bus communication with the "
            << lruType
            << ". Cannot synchronise the "
            << lruType
            << " with the incoming TE signal!";
        nex.addData("Detail", output.str());
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        if(alarmSender->isAlarmSet(LorrNotSynchronised) == false)
        {
            alarmSender->activateAlarm(LorrNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << lruType
            << " is not operational yet. Cannot synchronise the "
            << lruType
            << " with the incoming TE signal!";
        nex.addData("Detail", output.str());
        nex.log();
        throw nex;
    }
    catch(const ControlExceptions::ANTErrorExImpl& ex)
    {
        if(alarmSender->isAlarmSet(LorrNotSynchronised) == false)
        {
            alarmSender->activateAlarm(LorrNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "Caught a general Antenna exception. Cannot synchronise the "
            << lruType
            << " with the incoming TE signal!";
        nex.addData("Detail", output.str());
        nex.log();
        throw nex;
    }
    catch(...)
    {
        if(alarmSender->isAlarmSet(LorrNotSynchronised) == false)
        {
            alarmSender->activateAlarm(LorrNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream output;
        output << "Caught an unexpected exception. Cannot synchronise the "
            << lruType
            << " with the incoming TE signal!";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex;
    }
}

void Control::AntennaImpl::synchArrayTime()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // 1. Check if the ArrayTime component reference is valid.
    //    If it is valid, then:
    // 2. Get a TIMESOURCE reference; running on lo-art-1.
    // 3. Resynchronise the teHandler via ArrayTime::resynchronize(). This will
    //    tell the teHandler to make the transition from FW to HARD mode.
    // 4. Set the T0 value in the teHandler via ArrayTime::synchronizeTime(
    //    Control::TimeSource).

    if(m_arrayTimeRef_sp.isNil() == true)
    {
        if(alarmSender->isAlarmSet(ClockNotSynchronised) == false)
        {
            alarmSender->activateAlarm(ClockNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "The ArrayTime component is not active. Cannot"
            "resynchronise it with the TE phase.");
        ex.log();
        throw ex;
    }

    ///
    /// TODO
    /// Thomas, Mar 30, 2009
    ///
    /// Remove getting the TimeSource reference as soon as Rodrigo has removed
    /// the parameter from ArrayTime.midl.
    maci::SmartPtr< Control::TimeSource > timeSourceRef;
    try
    {
      timeSourceRef = getComponentReference< Control::TimeSource >(
          "CONTROL/AOSTiming/TimeSource");
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        if(alarmSender->isAlarmSet(ClockNotSynchronised) == false)
        {
            alarmSender->activateAlarm(ClockNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not get a TimeSource component reference."
            "Thus the ArrayTime component cannot be resynchronised to the TE"
            "phase.");
        throw nex;
    }

    try
    {
        m_arrayTimeRef_sp->resynchronize();
        ///
        /// TODO
        /// Thomas, Mar 30, 2009
        ///
        /// Use the commented out call as soon as Rodrigo has removed the
        /// parameter in ArrayTime.idl.
        /// m_arrayTimeRef_sp->synchronizeTime();

        m_arrayTimeRef_sp->synchronizeTime(
            maci::SmartPtr< Control::TimeSource >::GetPointer(timeSourceRef));

        if(alarmSender->isAlarmSet(ClockNotSynchronised) == true)
        {
            alarmSender->deactivateAlarm(ClockNotSynchronised);
        }
    }
    catch(const CorrErr::ArrayTimeInternalEx& ex)
    {
        if(alarmSender->isAlarmSet(ClockNotSynchronised) == false)
        {
            alarmSender->activateAlarm(ClockNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not resynchronise the ArrayTime with the "
            "TE signal.");
        throw nex;
    }
    catch(const CorrErr::RoundTripTimeErrorEx& ex)
    {
        if(alarmSender->isAlarmSet(ClockNotSynchronised) == false)
        {
            alarmSender->activateAlarm(ClockNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not synchronise the ArrayTime with the "
            "TE signal. The synchronisation happened on a wrong TE.");
        throw nex;
    }
    catch(const CorrErr::SynchronizeTimeOutEx& ex)
    {
        if(alarmSender->isAlarmSet(ClockNotSynchronised) == false)
        {
            alarmSender->activateAlarm(ClockNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Could not resynchronise the ArrayTime with the "
            "TE signal. No further information is available.");
        throw nex;
    }
    catch(const ControlExceptions::ArrayTimeErrorExImpl& ex)
    {
        if(alarmSender->isAlarmSet(ClockNotSynchronised) == false)
        {
            alarmSender->activateAlarm(ClockNotSynchronised);
        }

      throw;
    }
    catch(...)
    {
        if(alarmSender->isAlarmSet(ClockNotSynchronised) == false)
        {
            alarmSender->activateAlarm(ClockNotSynchronised);
        }

        ControlExceptions::ArrayTimeErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception. The Array Time "
            "has not been synchronised!");
        throw ex;
    }
}

void Control::AntennaImpl::waitFor(const double sleepTime) const
{
    ACE_Time_Value waitFor(0ULL);
    waitFor.set(sleepTime);
    ACE_OS::select(0, 0, 0, 0, waitFor);
}
