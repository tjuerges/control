//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
// who      when     what
// tjuerges  Dec 1, 2010  created
//


#include <antennaImpl.h>
#include <string>
#include <sstream>
#include <map>
#include <loggingMACROS.h>
#include <acsComponentSmartPtr.h>
#include <ControlDataInterfacesC.h>
#include <DTXC.h>
#include <FLOOGC.h>
#include <LO2C.h>


void Control::AntennaImpl::setWalshFunction(CORBA::Long walshFunctionIndex)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string componentName("FLOOG");

    std::stringstream name;
    name << componentName;

    std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.find(name.str()));

    if((iter == subdevice_m.end())
    || (CORBA::is_nil((*iter).second) == true))
    {
        std::ostringstream msg;
        msg << "The FLOOG component \""
            << name.str()
            << "\" is not available on antenna \""
            << antennaName_m
            << "\".";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
    }
    else
    {
        Control::FLOOG_var floogRef(Control::FLOOG::_narrow((*iter).second));

        if(CORBA::is_nil(floogRef) == true)
        {
            std::ostringstream msg;
            msg << "Cannot narrow the FLOOG component \""
                << name.str()
                << "\" for setting the Walsh function index on antenna \""
                << antennaName_m
                << "\".";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
        }
        else
        {
            try
            {
                floogRef->SetWalshFunction(walshFunctionIndex);
            }
            catch(...)
            {
                std::ostringstream msg;
                msg << "Could not set the Walsh function index in the FLOOG "
                    "component \""
                    << name.str()
                    << "\" on antenna \""
                    << antennaName_m
                    << "\".";
                LOG_TO_OPERATOR(LM_ERROR, msg.str());
            }
        }
    }

    componentName = "DTXBBpr";
    for(unsigned int index(0); index < 4; ++index)
    {
        name.str("");
        name << componentName
            << index;

        iter = subdevice_m.find(name.str());

        if((iter == subdevice_m.end())
        || (CORBA::is_nil((*iter).second) == true))
        {
            std::ostringstream msg;
            msg << "The DTX component "
                << name.str()
                << " is not available on antenna "
                << antennaName_m
                << ".";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }
        else
        {
            Control::DTX_var dtxRef(Control::DTX::_narrow((*iter).second));

            if(CORBA::is_nil(dtxRef) == true)
            {
                std::ostringstream msg;
                msg << "Cannot narrow the DTX component \""
                    << name.str()
                    << "\" for setting the Walsh function index on antenna \""
                    << antennaName_m
                    << "\".";
                LOG_TO_OPERATOR(LM_ERROR, msg.str());
            }
            else
            {
                try
                {
                    dtxRef->setWalshFunction(walshFunctionIndex);
                }
                catch(...)
                {
                    std::ostringstream msg;
                    msg << "Could not set the Walsh function index in the DTX "
                        "\" component "
                        << name.str()
                        << "\" on antenna \""
                        << antennaName_m
                        << "\".";
                    LOG_TO_OPERATOR(LM_ERROR, msg.str());
                }
            }
        }
    }
}


void Control::AntennaImpl::setFrequencyOffsettingMode(
    Control::LOOffsettingMode mode)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string componentName("FLOOG");

    std::stringstream name;
    name << componentName;

    std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.find(name.str()));

    if((iter == subdevice_m.end())
    || (CORBA::is_nil((*iter).second) == true))
    {
        std::ostringstream msg;
        msg << "The FLOOG component \""
            << name.str()
            << "\" is not available on antenna \""
            << antennaName_m
            << "\".";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
    }
    else
    {
        Control::FLOOG_var floogRef(Control::FLOOG::_narrow((*iter).second));

        if(CORBA::is_nil(floogRef) == true)
        {
            std::ostringstream msg;
            msg << "Cannot narrow the FLOOG component \""
                << name.str()
                << "\" for setting the offset mode on antenna \""
                << antennaName_m
                << "\".";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
        }
        else
        {
            try
            {
                floogRef->setFrequencyOffsettingMode(mode);
            }
            catch(...)
            {
                std::ostringstream msg;
                msg << "Could not set the offset mode in the FLOOG "
                    "component \""
                    << name.str()
                    << "\" on antenna \""
                    << antennaName_m
                    << "\".";
                LOG_TO_OPERATOR(LM_ERROR, msg.str());
            }
        }
    }


    componentName = "LO2BBpr";
    for(unsigned int index(0); index < 4; ++index)
    {
        name.str("");
        name << componentName
            << index;
        iter = subdevice_m.find(name.str());
        if((iter == subdevice_m.end())
        || (CORBA::is_nil((*iter).second) == true))
        {
            std::ostringstream msg;
            msg << "The LO2 component \""
                << name.str()
                << "\" is not available on antenna \""
                << antennaName_m
                << "\".";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }
        else
        {
            Control::LO2_var lo2Ref(Control::LO2::_narrow((*iter).second));
            if(CORBA::is_nil(lo2Ref) == true)
            {
                std::ostringstream msg;
                msg << "Cannot narrow the LO2 component \""
                    << name.str()
                    << "\" for setting the offset mode on antenna \""
                    << antennaName_m
                    << "\".";
                LOG_TO_OPERATOR(LM_ERROR, msg.str());
            }
            else
            {
                try
                {
                    lo2Ref->setFrequencyOffsettingMode(mode);
                }
                catch(...)
                {
                    std::ostringstream msg;
                    msg << "Could not set the offset mode in the LO2 "
                        "component \""
                        << name.str()
                        << "\" on antenna \""
                        << antennaName_m
                        << "\".";
                    LOG_TO_OPERATOR(LM_ERROR, msg.str());
                }
            }
        }
    }
}

void Control::AntennaImpl::setFrequencyOffsettingIndex(
    CORBA::Long frequencyIndex)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    std::string componentName("FLOOG");

    std::stringstream name;
    name << componentName;

    std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.find(name.str()));

    if((iter == subdevice_m.end())
    || (CORBA::is_nil((*iter).second) == true))
    {
        std::ostringstream msg;
        msg << "The FLOOG component \""
            << name.str()
            << "\" is not available on antenna \""
            << antennaName_m
            << "\".";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
    }
    else
    {
        Control::FLOOG_var floogRef(Control::FLOOG::_narrow((*iter).second));

        if(CORBA::is_nil(floogRef) == true)
        {
            std::ostringstream msg;
            msg << "Cannot narrow the FLOOG component \""
                << name.str()
                << "\" for setting the offset index on antenna \""
                << antennaName_m
                << "\".";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
        }
        else
        {
            try
            {
                floogRef->SetFrequencyOffsetFactor(frequencyIndex);
            }
            catch(...)
            {
                std::ostringstream msg;
                msg << "Could not set the offset index in the FLOOG "
                    "component \""
                    << name.str()
                    << "\" on antenna \""
                    << antennaName_m
                    << "\".";
                LOG_TO_OPERATOR(LM_ERROR, msg.str());
            }
        }
    }


    componentName = "LO2BBpr";
    for(unsigned int index(0); index < 4; ++index)
    {
        name.str("");
        name << componentName
            << index;
        iter = subdevice_m.find(name.str());
        if((iter == subdevice_m.end())
        || (CORBA::is_nil((*iter).second) == true))
        {
            std::ostringstream msg;
            msg << "The LO2 component \""
                << name.str()
                << "\" is not available on antenna \""
                << antennaName_m
                << "\".";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }
        else
        {
            Control::LO2_var lo2Ref(Control::LO2::_narrow((*iter).second));
            if(CORBA::is_nil(lo2Ref) == true)
            {
                std::ostringstream msg;
                msg << "Cannot narrow the LO2 component \""
                    << name.str()
                    << "\" for setting the offset index on antenna \""
                    << antennaName_m
                    << "\".";
                LOG_TO_OPERATOR(LM_ERROR, msg.str());
            }
            else
            {
                try
                {
                    lo2Ref->SetFrequencyOffsetFactor(frequencyIndex);
                }
                catch(...)
                {
                    std::ostringstream msg;
                    msg << "Could not set the offset index in the LO2 "
                        "component \""
                        << name.str()
                        << "\" on antenna \""
                        << antennaName_m
                        << "\".";
                    LOG_TO_OPERATOR(LM_ERROR, msg.str());
                }
            }
        }
    }
}
