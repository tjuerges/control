//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// $Id$
// $Source$
//
//


#include <antennaImpl.h>
#include <map>

// Exceptions.
#include <maciErrType.h>
#include <ACSErrTypeCommon.h>

// For ACS_TRACE.
#include <loggingMACROS.h>

// For audience logs.
#include <sstream>
#include <loggingMACROS.h>

// For the mutex which protects all AmbManager related.
#include <Guard_T.h>

// For ACS::ThreadSyncGuard.
#include <acsThreadBase.h>

#include <acsComponentSmartPtr.h>

#include <lkmLoaderC.h>
#include <rtlogC.h>
#include <ArrayTimeC.h>
#include <ambManagerC.h>
#include <ControlArrayInterfacesC.h>
#include <ControlBasicInterfacesC.h>

#include <acsncSimpleSupplier.h>

// Alarm system stuff
#include <vector>
#include <controlAlarmSender.h>

#include <simpleCallbackImpl.h>

#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>


Control::AntennaImpl::AntennaImpl(const ACE_CString& name,
    maci::ContainerServices* containerServices):
    Control::HardwareControllerImpl(name, containerServices),
    alarmSender(0),
    delayDistributor_m(),
    antennaName_m(),
    stateEventSupplier_p(0),
    SAS_OPTIMIZATION_TIMEOUT(600000000) //60 Seconds
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // Create Simple event supplier
    stateEventSupplier_p = new nc::SimpleSupplier(
        Control::CHANNELNAME_CONTROLSYSTEM, this);
}

Control::AntennaImpl::~AntennaImpl()
{
    ACS_TRACE(__PRETTY_FUNCTION__);
}

void Control::AntennaImpl::checkIfShutdown(const std::string& file,
    const unsigned int line, const std::string& function) const
{
    if(currentState_m == Control::HardwareController::Shutdown)
    {
        ControlExceptions::INACTErrorExImpl ex(file.c_str(), line,
            function.c_str());
        ex.log();
        throw ex.getINACTErrorEx();
    }
}

void Control::AntennaImpl::initialiseAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // alarmSender is expected to be = 0 since the instanciation of this
    // class.  Therefore do not check if it has been set before but just get a
    // new instance of the AlarmSende.
    alarmSender = new Control::AlarmSender;

    std::vector< Control::AlarmInformation > alarmInformation;

    {
        Control::AlarmInformation ai;
        ai.alarmCode = NoKernelModules;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = ClockNotSynchronised;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = LorrNotSynchronised;
        alarmInformation.push_back(ai);
    }

    {
        Control::AlarmInformation ai;
        ai.alarmCode = FailedSubDevices;
        alarmInformation.push_back(ai);
    }

    alarmSender->initializeAlarms(
        Control::CharacteristicControlDeviceImpl::getAlarmFamilyName("Antenna"),
        Control::CharacteristicControlDeviceImpl::getAlarmMemberName(),
        alarmInformation);

    alarmSender->forceTerminateAllAlarms();
}

void Control::AntennaImpl::cleanUpAlarmSystem()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(alarmSender != 0)
    {
        // Disable all alarms for 60s.  This allows to terminate all alarms.
        alarmSender->inhibitAlarms(static_cast< ACS::TimeInterval >(60 * 1e7));
        alarmSender->forceTerminateAllAlarms();
        alarmSender = 0;
    }
}

void Control::AntennaImpl::assignAsynch(const char* arrayName,
    Control::AntennaCallback* cb)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        assign(arrayName);
    }
    catch(const ControlExceptions::CAMBErrorEx& ex)
    {
        ControlExceptions::CAMBErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::INACTErrorEx& ex)
    {
        ControlExceptions::INACTErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorCompletion c(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlExceptions::TimeoutEx& ex)
    {
        ControlExceptions::TimeoutCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlCommonExceptions::AsynchronousFailureEx& ex)
    {
        ControlCommonExceptions::AsynchronousFailureCompletion c(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const ControlCommonExceptions::OSErrorEx& ex)
    {
        ControlCommonExceptions::OSErrorCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(const maciErrType::CannotGetComponentCompletion& ex)
    {
        maciErrType::CannotGetComponentCompletion c(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        cb->report(antennaName_m.c_str(), c);
        return;
    }
    catch(...)
    {
        ACSErrTypeCommon::UnknownCompletion c(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        c.addData("Detail", "This is a severe event. Proably a bug");
        c.log();
        cb->report(antennaName_m.c_str(), c);
        return;
    }

    cb->report(antennaName_m.c_str(), ACSErrTypeOK::ACSErrOKCompletion());
}

void Control::AntennaImpl::assign(const char* arrayName)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Arrays are named 'Array001', 'Array002', ...; on the other hand
    // the respective array *component* names are 'CONTROL/Array001',
    // 'CONTROL/Array002', etc. Adding the prefix to the arrayName
    // parameter to get the array component name.
    std::string arrayCompName("CONTROL/");
    arrayCompName += arrayName;

    // Get a persistent non-sticky reference to the ArrayMonitor
    // to which we have just been assigned.
    // Note: We are using the array monitor here rather than the array
    // command because array command is not a component.  We
    // need to refactor these parent classes a bit.
    try
    {
        m_assignedArray_sp = getContainerServices()->
            getComponentNonStickySmartPtr< Control::ArrayMonitor >(
                arrayCompName.c_str());
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);

        std::ostringstream msg;
        msg << "Unable to assign "
            << antennaName_m
            << " to array "
            << arrayCompName
            << ".  It appears the array does not exist!";
        nex.addData("Detail", msg.str());
        throw nex.getCannotGetComponentEx();
    }

    /// \note Since the ArrayComponent is not a control device
    /// we cannot simply set the parent.  For now just
    /// comment this out and keep track of the assigned flag
    /// on our own.
    /// setParent(arrayCompName.c_str());
    delayDistributor_m.assignArray(arrayName);

    // \note 2010-10-27 ntroncos: implementation of
    // http://jira.alma.cl/browse/CSV-179?focusedCommentId=84351#action_84351
    // In basic terms:
    //  1- Get the SAS and the LLC
    //  2- Optimize the SAS
    //  <strike>3- check the LLC if its Optimized, rasie alarm if not.</strike>
    //     The LLC alarm will be delegated to the LLC. It self will enable/disable
    //     an alarm. The antenna will only cause a check in the LLC that may
    //     promt such alarm.
    // Exceptional flow:
    // If SAS is not available/operation:
    //   - Raise exception if the photonice reference seq > 0
    //If cannot set the photonic reference:
    //   - Raise exception if the photonice reference seq > 0
    // If LLC is no available/Operational:
    //   - Raise exception if the photonice reference seq > 0
    // The case photonice reference seq = 0 is when not photonic reference was selected.
    // (Holography, Opticalpointing, Engineering) In such case we skipe over this.

    // Yes, yes the exceptions are a mess, I did not want to add to the mess,
    // so instead of creating a Control::AntennaAsigmentEx I rethrow whatever
    // I got.
    Control::ArrayMonitor_var arrayMonitor_ref(Control::ArrayMonitor::_narrow(
        GetImpl(m_assignedArray_sp)));

    if(CORBA::is_nil(arrayMonitor_ref) == true)
    {
        LOG_TO_DEVELOPER(LM_WARNING, "Unable to narrow parent reference");
        return;
    }

    Control::ResourceSeq_var photonicRef_var(new Control::ResourceSeq);
    photonicRef_var = arrayMonitor_ref->getPhotonicReferences();

    try
    {
        if(photonicRef_var->length() != 0)
        {
            // Use the first one if more than 1 photonic reference is
            // configured.
            configureSAS(photonicRef_var.in()[0]);

            // The LLC handles the alarm on its own.
            if(getLLCPolarizationCalRequired())
            {
                LOG_TO_OPERATOR(LM_WARNING, "LLC Polarization calibration is "
                    "required.");
            }
        }
    }
    catch(const ControlExceptions::CAMBErrorEx &ex)
    {
        ControlExceptions::CAMBErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getCAMBErrorEx();
    }
    catch(const ControlExceptions::INACTErrorEx &ex)
    {
        ControlExceptions::INACTErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getINACTErrorEx();
    }
    catch(const ControlExceptions::HardwareErrorEx& ex)
    {
        ControlExceptions::HardwareErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getHardwareErrorEx();
    }
    catch(const ControlExceptions::InvalidRequestEx& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getInvalidRequestEx();
    }
    catch(const ControlExceptions::IllegalParameterErrorEx& ex)
    {
        ControlExceptions::IllegalParameterErrorExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getIllegalParameterErrorEx();
    }
    catch(const ControlExceptions::TimeoutEx& ex)
    {
        ControlExceptions::TimeoutExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getTimeoutEx();
    }
    catch(const ControlCommonExceptions::AsynchronousFailureEx& ex)
    {
        ControlCommonExceptions::AsynchronousFailureExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getAsynchronousFailureEx();
    }
    catch(const ControlCommonExceptions::OSErrorEx& ex)
    {
        ControlCommonExceptions::OSErrorExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Failed to assign antenna when operating over "
            "SAS and LLC");
        nex.log();
        throw nex.getOSErrorEx();
    }

    std::ostringstream msg;
    msg << "Antenna "
        << antennaName_m
        << " assigned to array "
        << arrayName;
    LOG_TO_OPERATOR(LM_INFO, msg.str());
}

void Control::AntennaImpl::unassign()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    delayDistributor_m.unassignArray();

    // Release the parent reference on the array
    m_assignedArray_sp.release();
}

char* Control::AntennaImpl::getArray()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    CORBA::String_var returnValue;
    if(isAssigned() != false)
    {
        returnValue = CORBA::string_dup(m_assignedArray_sp->getArrayName());
    }
    else
    {
        // The antenna is completely unassigned
        returnValue = CORBA::string_dup("Unassigned");
    }

    return returnValue._retn();
}

void Control::AntennaImpl::flagData(bool startStop, ACS::Time timeStamp,
    const char* componentName, const char* description)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if(isAssigned() == true)
    {
        Control::ArrayCommand_var arrayCommandRef(
            Control::ArrayCommand::_narrow(GetImpl(m_assignedArray_sp)));
        if(CORBA::is_nil(arrayCommandRef) == true)
        {
            LOG_TO_OPERATOR(LM_WARNING, "Unable to narrow reference to Array. "
                "Flagging of data is not possible");
        }
        else
        {
            arrayCommandRef->flagData(startStop, timeStamp, componentName,
                description);
        }
    }
}

CORBA::Long Control::AntennaImpl::numDelayEventsReceived()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    return delayDistributor_m.getNumEvents();
}

char* Control::AntennaImpl::getName()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    CORBA::String_var retValue(CORBA::string_dup(antennaName_m.c_str()));
    return retValue._retn();
}

Control::BadResourceSeq* Control::AntennaImpl::getBadResources()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    Control::BadResourceSeq_var badResource(new Control::BadResourceSeq);
    ACS::ThreadSyncGuard guard(&(Control::HardwareControllerImpl::badDeviceMutex_m));
    badResource->length(badSubdeviceList_m.size());

    unsigned int idx(0U);
    for(std::vector< std::string >::const_iterator iter(
        badSubdeviceList_m.begin()); iter != badSubdeviceList_m.end();
        ++iter, ++idx)
    {
        (*badResource)[idx] = CORBA::string_dup((*iter).c_str());
    }

    return badResource._retn();
}

bool Control::AntennaImpl::isAssigned()
{
    return(!m_assignedArray_sp.isNil());
}

Control::AntennaStateEvent* Control::AntennaImpl::getAntennaState()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    Control::AntennaStateEvent_var ase(new Control::AntennaStateEvent);
    ase->antennaName = CORBA::string_dup(antennaName_m.c_str());
    ase->eventTime = ::getTimeStamp();

    if(inErrorState_m)
    {
        ase->newSubstate = Control::AntennaError;
    }
    else
    {
        ase->newSubstate = Control::AntennaNoError;
    }

    switch(currentState_m)
    {
        case Control::HardwareController::Shutdown:
        {
            ase->newState = Control::AntennaShutdown;
        }
        break;

        case Control::HardwareController::Degraded:
        {
            ase->newState = Control::AntennaDegraded;
        }
        break;

        case Control::HardwareController::Operational:
        {
            ase->newState = Control::AntennaOperational;
        }
        break;

        default:
        {
            ase->newState = Control::AntennaOperational;
            ase->newSubstate = Control::AntennaError;
        }
    }

    return ase._retn();
}

void Control::AntennaImpl::createSubdevices(
    const Control::DeviceConfig& configuration, const char* parentName)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareControllerImpl::createSubdevices(configuration, parentName);
        if(alarmSender->isAlarmSet(FailedSubDevices) == true)
        {
            alarmSender->deactivateAlarm(FailedSubDevices);
        }
    }
    catch(const ControlDeviceExceptions::IllegalConfigurationEx& ex)
    {
        // Create a string which contains a list of the bad subdevices.
        std::ostringstream badSubdevices;
        for(std::vector< std::string >::const_iterator iter(
            badSubdeviceList_m.begin()); iter != badSubdeviceList_m.end();
            ++iter)
        {
            badSubdevices << *iter
                << ", ";
        }

        ///
        /// TODO
        /// Thomas, Jun 7, 2010
        /// Here the above string should be used to set the alarmDescription.
        /// At the moment there is no methode to set the description after the
        /// alarms have been created.
        if(alarmSender->isAlarmSet(FailedSubDevices) == false)
        {

            alarmSender->activateAlarm(FailedSubDevices);
        }

        throw ControlDeviceExceptions::IllegalConfigurationExImpl(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__).getIllegalConfigurationEx();
    }

    // Currently we get passed the Component name, change to the antenna name
    // and throw and exception if this changes
    std::string configName(configuration.Name);
    if(configName.rfind('/') == std::string::npos)
    {
        ControlDeviceExceptions::IllegalConfigurationExImpl ex(__FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        ex.log();
        throw ex.getIllegalConfigurationEx();
    }

    antennaName_m = configName.substr(configName.rfind('/') + 1);
    delayDistributor_m.setAntennaName(antennaName_m.c_str());

    // Now go through the list of subdevices and add them to the
    // delayDistributor if they are clients
    for(std::map< std::string, Control::ControlDevice_ptr >::iterator iter(
        subdevice_m.begin()); iter != subdevice_m.end(); ++iter)
    {
        if(CORBA::is_nil(iter->second) == false)
        {
            Control::DelayClient_var delayClient(Control::DelayClient::_narrow(
                iter->second));
            if(CORBA::is_nil(delayClient) == false)
            {
                delayDistributor_m.attachClient(iter->first, delayClient.in());
            }
        }
    }
}

void Control::AntennaImpl::setControllerState(
    Control::HardwareController::ControllerState newState)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    Control::HardwareControllerImpl::setControllerState(newState);
    // Now send out an event with the new state
    publishState();
}

void Control::AntennaImpl::setError(const std::string& errorDescription)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    Control::CharacteristicControlDeviceImpl::setError(errorDescription);
    // Now send out an event with the new state
    publishState();
}

void Control::AntennaImpl::clearError()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    Control::CharacteristicControlDeviceImpl::clearError();
    // Now send out an event with the new state
    publishState();
}

void Control::AntennaImpl::publishState()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    // There is no point in trying to publish the state when the pointer has
    // not been set is is already 0 again.
    if(stateEventSupplier_p == 0)
    {
        LOG_TO_DEVELOPER(LM_ERROR, "The event publisher is not available "
            "inside component. Therefore the current event could not be "
            "published. Continuing with normal operations.");
        return;
    }

    Control::AntennaStateEvent_var ase(getAntennaState());
    try
    {
        stateEventSupplier_p->publishData< Control::AntennaStateEvent >(ase);
    }
    catch(const acsncErrType::PublishEventFailureExImpl& ex)
    {
        acsncErrType::PublishEventFailureExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught an "
            "\"acsncErrType::PublishEventFailureExImpl\" exception during an "
            "event publishing.  Please check if the \"CONTROL_SYSTEM\" "
            "notification channel is still up and running.  Continuing with "
            "normal operations.");
        nex.log();
    }
    catch(const ACSErr::ACSbaseExImpl& ex)
    {
        acsncErrType::PublishEventFailureExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", "Caught an "
            "\"ACSErr::ACSbaseExImpl\" exception during an "
            "event publishing.  Please check if the \"CONTROL_SYSTEM\" "
            "notification channel is still up and running.  Continuing with "
            "normal operations.");
        nex.log();
    }
    catch(...)
    {
        acsncErrType::PublishEventFailureExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Caught an unexpected exception!  Please check "
            "if the \"CONTROL_SYSTEM\" notification channel is still up and "
            "running.  Continuing with normal operations.");
        ex.log();
    }
}

void Control::AntennaImpl::controllerOperational()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        // If the lkmLoader component cannot be loaded, an exception will
        // be thrown and the antenna state be set to Shutdown. Also the
        // error state will be set.
        // If any other component could not be loaded, it will not be fatal
        // but the antenna will be put into degraded state.
        getInitComponents();
    }
    catch(maciErrType::CannotGetComponentExImpl& ex)
    {
        ex.log();
        return;
    }

    // No need to resynch if the ArrayTime component is not available.
    if(m_arrayTimeRef_sp.isNil() == false)
    {
        try
        {
            synchroniseArrayTime();
        }
        catch(const ControlExceptions::ArrayTimeErrorEx& ex)
        {
            // There was a problem sync'ing the ArrayTime component or the
            // LORR for the first time.  Signal this by setting the Controller
            // to Degraded state.
            setControllerState(Control::HardwareController::Degraded);
        }
    }
    else
    {
        LOG_TO_OPERATOR(LM_ERROR, "No valid ArrayTime reference was received "
            "from the manager. Not resynchronising any LRUs or the ArrayTime "
            "component.  Check the availability of the LORR and the ArrayTime "
            "components!");
    }

    Control::HardwareControllerImpl::controllerOperational();
}

void Control::AntennaImpl::controllerShutdown()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(isAmbManagerRunning() == true)
    {
        try
        {
            stopAmbManager();
        }
        catch(const ControlExceptions::DeviceBusyEx& ex)
        {
            // No point in dealing with exceptions which happened during the
            // shut down of the AmbManager.  We are shutting down ourselves
            // now.  Just log an error message.
            LOG_TO_OPERATOR(LM_WARNING, "The AmbManager was not shut down "
                "cleanly.  Please check the state of the real-time kernel "
                "modules!");
        }
    }

    Control::HardwareControllerImpl::controllerShutdown();

    // Check if the pointer has been set to 0.  This would be true when the
    // component had already executed cleanUp of this component.  The cleanUp
    // call makes it in the inheritance hierarchy up to
    // Control::HardwareDeviceImpl::cleanUp, which calls
    //  -> Control::HardwareDeviceImpl::hwStop which in turn calls
    //     -> Control::HardwareDeviceImpl::hwStopAction which calls
    //        -> all the hwStop methods in the inheriting classes.
    if(alarmSender != 0)
    {
        alarmSender->forceTerminateAllAlarms();
    }
}

void Control::AntennaImpl::initialize()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    try
    {
        Control::HardwareControllerImpl::initialize();
    }
    catch(const acsErrTypeLifeCycle::LifeCycleExImpl& ex)
    {
        throw acsErrTypeLifeCycle::LifeCycleExImpl(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
    }

    // Start alarms.
    initialiseAlarmSystem();
}

void Control::AntennaImpl::cleanUp()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(stateEventSupplier_p != 0)
    {
        stateEventSupplier_p->disconnect();
        stateEventSupplier_p = 0;
    }

    if(isAmbManagerRunning() == true)
    {
        try
        {
            stopAmbManager();
        }
        catch(const ControlExceptions::DeviceBusyEx& ex)
        {
            // No point in dealing with exceptions which happened during the
            // shut down of the AmbManager.  We are shutting down ourselves
            // now.  Just log an error message.
            std::ostringstream msg;
            msg << "The AmbManager was not shut down cleanly.  Please check "
                "the state of the real-time kernel modules!";
            LOG_TO_OPERATOR(LM_WARNING, msg.str());
        }
    }

    m_arrayTimeRef_sp.release();

    Control::HardwareControllerImpl::cleanUp();

    // Stop alarms.
    cleanUpAlarmSystem();

    // If we have an assigned array release it
    m_assignedArray_sp.release();

    // We need to release the references here. It cannot be done in the
    // destructor as, by that time, this component does not have any connection
    // to the manager and hence cannot tell the manager to release the
    // references. The references are released in reverse order.
    m_rtLogRef_sp.release();
    m_lkmLoaderRef_sp.release();
}

void Control::AntennaImpl::getInitComponents()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    std::string componentName(tmpName.in());
    componentName += "/lkmLoader";
    try
    {
        m_lkmLoaderRef_sp = getComponentReference< ACS_RT::lkmLoader >(
            componentName);
        if(alarmSender->isAlarmSet(NoKernelModules) == true)
        {
            alarmSender->deactivateAlarm(NoKernelModules);
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        if(alarmSender->isAlarmSet(NoKernelModules) == false)
        {
            alarmSender->activateAlarm(NoKernelModules);
        }

        // It is fatal when the lkmLoader component could not be started up.
        // Put this component into shutdown state and throw an exception.
        setControllerState(Control::HardwareController::Shutdown);
        std::ostringstream errorString;
        errorString << "The lkmLoader component for antenna '"
            << antennaName_m
            << "' could not be instanciated. This is a fatal error! Please "
                "check:\n"
            "1. The CDB entry for the lkm file points to a valid and existing "
            "file.\n"
            "2. No kernel modules have been loaded before.\n"
            "3. The real-time computer did not suffer from a kernel Oops.\n";
        setError(errorString.str());
        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        nex.addData("Detail", errorString.str());
        throw nex;
    }

    // Get a reference to the rtlog component.
    componentName = tmpName.in();
    componentName += "/rtlog";
    try
    {
        m_rtLogRef_sp = getComponentReference< ACS_RT::rtlog >(componentName);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the rtLog component could not be started up.
        // Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);

        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The component called '"
            << componentName
            << "' could not be started. This is not a fatal error. Log "
                "messages from the real-time code running on antenna '"
            << antennaName_m
            << "' will not be available.";
        nex.addData("Detail", msg.str());
        nex.log();
    }

    // Get a reference to the ArrayTime component.
    componentName = tmpName.in();
    componentName += "/ArrayTime";
    try
    {
        m_arrayTimeRef_sp = getComponentReference< Correlator::ArrayTime >(
            componentName);
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the ArrayTime component could not be started
        // up.  Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);

        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The component called '"
            << componentName
            << "' could not be started. This is not a fatal error. Precise "
                "timing for the equipment on antenna '"
            << antennaName_m
            << "' will not be available.";
        nex.addData("Detail", msg.str());
        nex.log();
    }
}

void Control::AntennaImpl::doStartAmbManager()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
    const std::string componentName(tmpName.in() + std::string("/AmbManager"));
    const std::string componentCode("ambManagerImpl");
    const std::string componentType("IDL:alma/Control/AmbManager:1.0");
    const std::string containerName("*");

    maci::ComponentSpec spec;
    spec.component_name = componentName.c_str();
    spec.component_code = componentCode.c_str();
    spec.component_type = componentType.c_str();
    spec.container_name = containerName.c_str();

    // Thomas, Jul 23, 2008
    // Okay, okay, I know. Two nested try/catch blocks are way uncool but the
    // C++ standard lacks a catch for collected exceptions.
    try
    {
        try
        {
            ACE_Guard< ACE_Mutex > guard(m_ambManagerState);
            m_ambManagerRef_sp
                = getContainerServices()->getCollocatedComponentSmartPtr<
                    Control::AmbManager >(spec, false, tmpName.in());
        }
        catch(const maciErrType::NoPermissionExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::IncompleteComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::InvalidComponentSpecExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const
            maciErrType::ComponentSpecIncompatibleWithActiveComponentExImpl& ex)
        {
            throw maciErrType::CannotGetComponentExImpl(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
        }
        catch(const maciErrType::CannotGetComponentExImpl& ex)
        {
            maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            nex.addData("Detail", "The AmbManager component could not be "
                "started.  Are the real-time Kernel modules loaded?");
            throw nex;
        }
        catch(...)
        {
            maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__,
                __PRETTY_FUNCTION__);
            ex.addData("Detail", "An unexpected exception has been caught. "
                "Developers: check the source code of maci::"
                "ContainerServices::getCollocatedComponentSmartPtr(...)!");
            ex.log();
            throw ex;
        }
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        // It is not fatal when the AmbManager component could not be started
        // up. Put this component into degraded state and log the exception.
        setControllerState(Control::HardwareController::Degraded);

        maciErrType::CannotGetComponentExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        std::ostringstream msg;
        msg << "The component called '"
            << componentName
            << "' could not be started. This is not a fatal error. Direct "
                "AMB access on antenna '"
            << tmpName.in()
            << "' will not be available.";

        nex.addData("Detail", msg.str());

        msg << " Component name = "
            << componentName
            << ", component code = "
            << componentCode
            << ", component type = "
            << componentType
            << ", container name = "
            << containerName
            << ".";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        throw nex;
    }
}

void Control::AntennaImpl::startAmbManager()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if((currentState_m != Control::HardwareController::Operational)
    && (currentState_m != Control::HardwareController::Degraded))
    {
        ControlExceptions::INACTErrorExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        ex.addData("Detail", "Cannot start the AmbManager as long as the "
            "Controller is not in Operational or in Degraded state.");
        ex.log();
        throw ex.getINACTErrorEx();
    }

    if(isAmbManagerRunning() == true)
    {
        ControlExceptions::DeviceBusyExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
        std::ostringstream output;
        output << "The AmbManager for this antenna '"
            << tmpName.in()
            << "' is already up and running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getDeviceBusyEx();
    }

    try
    {
        doStartAmbManager();
    }
    catch(const maciErrType::CannotGetComponentExImpl& ex)
    {
        ControlExceptions::InvalidRequestExImpl nex(ex, __FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        throw nex.getInvalidRequestEx();
    }
}

void Control::AntennaImpl::stopAmbManager()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    if(isAmbManagerRunning() == false)
    {
        ControlExceptions::InvalidRequestExImpl ex(__FILE__, __LINE__,
            __PRETTY_FUNCTION__);
        CORBA::String_var tmpName(acscomponent::ACSComponentImpl::name());
        std::ostringstream output;
        output << "The AmbManager for this antenna '"
            << tmpName.in()
            << "' is not running.";
        ex.addData("Detail", output.str());
        ex.log();
        throw ex.getInvalidRequestEx();
    }

    {
        ACE_Guard< ACE_Mutex > guard(m_ambManagerState);
        m_ambManagerRef_sp->disableAccess();
        m_ambManagerRef_sp.release();
    }
}

bool Control::AntennaImpl::isAmbManagerRunning()
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    ACE_Guard< ACE_Mutex > guard(m_ambManagerState);
    return (!m_ambManagerRef_sp.isNil());
}


void Control::AntennaImpl::reportCalDeviceData(const CalDeviceData& data)
{
    ACS_TRACE(__PRETTY_FUNCTION__);

    checkIfShutdown(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    if(isAssigned() == false)
    {
        // Nothing to do here since we have no parent to report to
        return;
    }

    Control::ArrayCommand_var arrayCommand_ref(Control::ArrayCommand::_narrow(
        GetImpl(m_assignedArray_sp)));
    if(CORBA::is_nil(arrayCommand_ref) == true)
    {
        LOG_TO_DEVELOPER(LM_WARNING, "Unable to narrow parent reference");
        return;
    }

    arrayCommand_ref->reportCalDeviceData(data);
}


#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(Control::AntennaImpl)
