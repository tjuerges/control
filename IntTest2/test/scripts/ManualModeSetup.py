#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2007-12-10  created
#
from CCL.Global import *
from Acspy.Clients.SimpleClient import PySimpleClient
from xml.dom.ext.reader import PyExpat
import Control
import glob
import unittest
import xmlentity
import atexit

"""
A utility to setup the environment so scripts in manual mode can
be run without a lot of work. Only two instructions are needed to
bootstrap the environment:

from ManualModeSetup import *
createArray(['ALMA01', 'ALMA02'])

The first imports this module and the second creates a manual mode
array from a list of antennas.
After this it is possible to start manual mode interactions, e.g.

beginExecution()
...
endExecution(Control.SUCCESS, 'The End')
"""

client = PySimpleClient("ManualModeSetup")
archconn = client.getComponent("ARCHIVE_CONNECTION")
archop = archconn.getOperational("ManualModeSetup")
archid = client.getComponent("ARCHIVE_IDENTIFIER")
simulator = client.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0")
# It is necessary to hold a reference to the MasterScheduler because the
# ManualArray uses getComponentNonSticky().
sched = client.getComponent("SCHEDULING_MASTERSCHEDULER")
master = client.getComponent('CONTROL/MASTER')

def setupManualModeEnv():
    """Sets up the manual mode environment.
    It checks if the manual mode project has been loaded, loading one
    otherwise. It configures the IDL simulator so the MasterScheduler
    returs something meaningful when startManualModeSession() is
    invoked, and initializes the CONTROL subsystem.
    This function will be called when importing this module.
    """
    global client, archconn, archop, archid, simulator, sched, master

    ids = checkManualModeProject()
    if len(ids) == 0:
        ids = loadManualModeProject()
    code =  "from asdmIDLTypes import IDLEntityRef\n"
    code += "entities = []\n"
    code += "ent = IDLEntityRef('"+ids['SchedBlock']+"','X00000000', 'SchedBlock', '1.0')\n"
    code += "entities.append(ent)\n"
    code += "ent = IDLEntityRef('"+ids['ProjectStatus']+"','X00000000', 'ProjectStatus', '1.0')\n"
    code += "entities.append(ent)\n"
    code += "entities"
    simulator.setMethod("SCHEDULING_MASTERSCHEDULER", "startManualModeSession",
                             code, 0)
    code =  "from Control import AntennaStateEvent\n"
    code += "import Control\n"
    code += "state = Control.AntennaOperational\n"
    code += "substate = Control.AntennaNoError\n"
    code += "AntennaStateEvent('CONTROL/ALMA01', 0L, state, substate)"
    simulator.setMethod("CONTROL/ALMA01", "getAntennaState", code, 0)
    code =  "from Control import AntennaStateEvent\n"
    code += "import Control\n"
    code += "state = Control.AntennaOperational\n"
    code += "substate = Control.AntennaNoError\n"
    code += "AntennaStateEvent('CONTROL/ALMA02', 0L, state, substate)"
    simulator.setMethod("CONTROL/ALMA02", "getAntennaState", code, 0)
    
    master.startupPass1()
    master.startupPass2()

def clearManualModeEnv():
    """Cleans up the manual mode environment. It shuts down the CONTROL
    subsystem and releases all used components.
    """
    global client, archconn, archop, archid, simulator, sched, master
    master.shutdownPass1()
    master.shutdownPass2()
    client.releaseComponent(master._get_name())
    client.releaseComponent(sched._get_name())
    client.releaseComponent(simulator._get_name())
    client.releaseComponent(archid._get_name())
    client.releaseComponent(archconn._get_name())
    client.disconnect()

def createArray(antennas):
    """Creates a manual mode array.
    Parameters:
    antennas     List of antenna names.
    """
    global client, master
    arrayId = master.createManualArray(antennas)
    setArrayName(arrayId.arrayName)

def loadManualModeProject():
    """Manual mode requires a dummy manual mode project to be stored in the ARCHIVE
    so scheduling can create session and scheduling block entity ids.
    This manual mode project is any project with project name and PI name
    set to 'manual mode'.

    Returns: Map Entity Name -> UID
    """
    global archop, archid
    reader = PyExpat.Reader()
    ids = {}

    files = glob.glob('ManualModeProject/*.xml')
    for fname in files:
        xmlfile = open(fname)
        xml = xmlfile.read()
        uids = archid.getUIDs(1)

        # Get the entity name
        dom = reader.fromString(xml)
        entName = str(dom._get_childNodes()[0]._get_name())
        print "Storing ", entName, "; UID: ", uids[0]
        ids[entName] = uids[0]

        # Store the XML
        archop.store(xmlentity.XmlEntityStruct(xml, uids[0], entName, '1.0', '0L'))

        xmlfile.close()

    return ids
   

def checkManualModeProject():
    """Checks if there is a manual mode project already stored in the
    ARCHIVE.

    Returns: Map Entity Name -> UID
    """
    global archop
    ids = {}
    query = "/sbl:SchedBlock/sbl:pIName[text()='manual mode']/.."
    cursor = archop.query(query, 'SchedBlock')
    if cursor.hasNext():
        ids['SchedBlock'] = cursor.next().identifier
    else:
        return {}
    query = "/ps:ProjectStatus"
    cursor = archop.query(query, 'ProjectStatus')
    if cursor.hasNext():
        ids['ProjectStatus'] = cursor.next().identifier
    else:
        return {}
    return ids

atexit.register(clearManualModeEnv)
setupManualModeEnv()
