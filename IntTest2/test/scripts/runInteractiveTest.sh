#!/bin/bash

##############################################################################
# ALMA - Atacama Large Millimiter Array
# (c) European Southern Observatory, 2002
# Copyright by ESO (in the framework of the ALMA collaboration),
# All rights reserved
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA 02111-1307  USA
#
# $Id$
#

# Documentation about the test goes here.
#
#

export CLASSPATH=$CLASSPATH:../lib/IntTest2.jar

declare TEST_NAME=InteractiveTest
declare TEST_CLASS=alma.Control.IntTest.InteractiveTest
declare TEST_SUITE=ALL

# The SchedBlock XML file to execute.
declare SCHEDBLOCK=schedblks/SFI_BL_NEW_APDM.xml
declare TEST_POSTFIX=${SCHEDBLOCK#*\/}
TEST_POSTFIX=${TEST_POSTFIX%.xml}
# The test timeout. If the execution last beyond this the test will give up
# and declare failure.
declare TEST_LOG="tmp/${TEST_NAME}_${TEST_POSTFIX}.log"
declare TIMEOUT=240

if test $# -ge 1; then
  SCHEDBLOCK=$1
  if test $# -ge 2; then
    TIMEOUT=$2
  fi
fi  

printf "###############################################\n"
printf "%s SchedBlock=%s: " "$TEST_NAME" "$SCHEDBLOCK"
acsStartJava -Dschedblock="$SCHEDBLOCK" -Dtimeout="$TIMEOUT" \
  -endorsed junit.textui.TestRunner "$TEST_CLASS" &> "$TEST_LOG"

RESULT=$?
if [ "$RESULT" = "0" ]; then
    printf "OK\n"
else
    printf "ERROR\n"
fi
exit "$RESULT"

# __oOo__
