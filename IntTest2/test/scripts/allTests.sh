printf "###############################################\n"
./scripts/testEnv start
./scripts/runInteractiveTest.sh ./schedblks/StandardInterferometry.xml 60000; RETURN=$?
./scripts/runInteractiveTest.sh ./schedblks/StandardTotalPower.xml 60000; let "RETURN&=$?"
printf "###############################################\n"
./scripts/testEnv stop
rm -f *.gclog
exit "$RETURN"
