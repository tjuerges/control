package alma.Control.IntTest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.Common.Name;
import alma.Control.Common.Util;
import alma.Control.ArrayIdentifier;
import alma.Control.AutomaticArray2;
import alma.Control.AutomaticArray2Helper;
import alma.Control.CorrelatorType;
import alma.Control.ExecutionState;
import alma.Control.ExecutionStateHelper;
import alma.Control.Master2;
import alma.Control.Master2Helper;
import alma.Control.SBExecState;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.TMCDB.Access;
import alma.TMCDB.AccessHelper;
import alma.TMCDB.TMCDBComponent;
import alma.TMCDB.TMCDBComponentHelper;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.acs.container.archive.UIDLibrary;
import alma.acs.entityutil.EntityDeserializer;
import alma.acs.entityutil.EntitySerializer;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.bulkdata.BulkStore;
import alma.bulkdata.BulkStoreHelper;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SchedBlockEntityT;
import alma.xmlentity.XmlEntityStruct;
import alma.xmlstore.ArchiveConnection;
import alma.xmlstore.ArchiveConnectionHelper;
import alma.xmlstore.ArchiveInternalError;
import alma.xmlstore.Identifier;
import alma.xmlstore.IdentifierHelper;
import alma.xmlstore.IdentifierJ;
import alma.xmlstore.IdentifierOperations;
import alma.xmlstore.Operational;
import alma.xmlstore.OperationalPackage.IllegalEntity;

public abstract class SBExecTest extends ComponentClientTestCase {

    protected static final String MASTER_CURL = "CONTROL/MASTER";
    protected static final String TMCDB_CURL = "IDL:alma/TMCDB/Access:1.0";
    protected static final String[] STATE_NAMES = {"INACCESSIBLE", "OPERATIONAL"};
    protected static final String[] SUBSTATE_NAMES = {"STARTING_UP_PASS1", 
                                                   "STARTED_UP_PASS1", 
                                                   "STARTING_UP_PASS2",
                                                   "WAITING", 
                                                   "SHUTTING_DOWN_PASS1", 
                                                   "SHUT_DOWN_PASS1", 
                                                   "SHUTTING_DOWN_PASS2",
                                                   "STOPPED",
                                                   "NOERROR", 
                                                   "ERROR"};
    
    protected ContainerServices container = null;
    
    protected Logger logger;
    protected ArrayIdentifier arrayId;
    protected AutomaticArray2 array;
    protected Master2 master;
    protected ExecutionState execState;
    protected Simulator simulator;
    
    // The archive's components
    protected ArchiveConnection archConnectionComp;
    protected Operational archOperationComp;
    protected Identifier archIdentifierComp;
    protected BulkStore archBulkStore;

    protected Access tmcdb;

    
    public SBExecTest(String name) throws Exception {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        
        container = getContainerServices();
        assert container != null;
        logger = container.getLogger();

        this.simulator = 
            SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        
        // Get the Master2 component.
        logger.info("Getting the Master2 component...");
        master = Master2Helper.narrow(container.getComponent(MASTER_CURL));
        assertNotNull(master);

        try {
            tmcdb = AccessHelper.narrow(container.getDefaultComponent(TMCDB_CURL));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error accessing '" + TMCDB_CURL + "': " + ex.toString());
            fail();            
        }        
        
        logger.info("Initializing the ARCHIVE...");
        initArchive();
        
    }

    protected void tearDown() throws Exception {
        releaseArchive();
        container.releaseComponent(MASTER_CURL);
        super.tearDown();
    }
    
    protected String performArrayObservation(String schedBlkFileName, int timeout)
            throws Exception {

        // Read a Scheduling Block file.
        SchedBlock testSchedBlk = getSchedBlockFromFile(schedBlkFileName);

        // Load this Scheduling Block into the Archive.
        String sbUID = null;
        sbUID = loadSBIntoArchive(testSchedBlk);
        logger.info("Scheduling Block UID: " + sbUID);

        // Initialize the Master2 component.
        master.startupPass1();
        master.startupPass2();

        ArrayIdentifier arrayId = null;
        // Ask the Master2 to create an AutomaticArray component.
        String[] antennaNames = null;
        antennaNames = new String[1];
        antennaNames[0] = "DV01";
        String[] photRefNames = null;
        photRefNames = new String[1];
        photRefNames[0] = "PhotonicReference2";
        try {
            arrayId = master.createAutomaticArray(antennaNames, photRefNames, CorrelatorType.BL);
        } catch (Exception ex) {
            logger.severe("Error creating Automatic Array: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }

        // Get a reference to the recently created AutomaticArray.
        try {
            array = AutomaticArray2Helper.narrow(container
                    .getComponent(arrayId.arrayComponentName));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Cannot access AutomaticArray component: "
                    + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        } catch (Exception ex) {
            logger.severe("Error while accessing '" + arrayId.arrayName
                    + "' AutomaticArray component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }

        // Check the state of the AutomaticArray.
        SystemState state = array.getArrayState();
        SystemSubstate substate = array.getArraySubstate();
        logger.info("Array State = " + STATE_NAMES[state.value()]);
        logger.info("Array Substate = " + SUBSTATE_NAMES[substate.value()]);

        assertEquals(SystemState.OPERATIONAL, state);
        assertEquals(SystemSubstate.NOERROR, substate);

        // Get the ExecutionState
        String execStateName = array.getExecutionStateName();
        try {
            execState = ExecutionStateHelper.narrow(container
                    .getComponent(execStateName));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Cannot access ExecutionState component: "
                    + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        } catch (Exception ex) {
            logger.severe("Error while accessing '" + execStateName
                    + "' ExecutionState component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }
        assertNotNull(execState);

        // Create the two entity references required to perform an observation
        String entityId = testSchedBlk.getSchedBlockEntity().getEntityId();
        String partId = "X00000000";
        String entityTypeName = testSchedBlk.getSchedBlockEntity()
                .getEntityTypeName();
        String documentVersion = testSchedBlk.getSchedBlockEntity()
                .getDocumentVersion();
        logger.fine("SchedBlock EntityId = " + entityId + "; PartId = "
                + partId + "; EntityTypeName = " + entityTypeName
                + "; DocumentVersion = " + documentVersion);

        IDLEntityRef sbEntRef = new IDLEntityRef(testSchedBlk
                .getSchedBlockEntity().getEntityId(), "X00000000", testSchedBlk
                .getSchedBlockEntity().getEntityTypeName(), testSchedBlk
                .getSchedBlockEntity().getDocumentVersion());
        // Usually SCHEDULING will create a valid IDLEntityRef for the session.
        // It is hardcoded in this test case.
        IDLEntityRef sessionEntRef = new IDLEntityRef(
                "uid://X0000000000000000/X00000000/X0", "X00000000", "Session",
                "1.0");
        // ... and action!
        array.observe(sbEntRef, sessionEntRef, 0L);

        // Wait with a timeout for the arrival of the ExecBlockEndedEvent
        // in the ExecutionState component.
        int sleepInterval = 1000; // sleep interval in milliseconds
        int timeoutCount = (int) 1000.0 * timeout / sleepInterval; // timeout
                                                                    // in cycle
                                                                    // counts
        int count = 0;
        SBExecState executionState = execState.getExecState();
        logger.info("Execution state = " + executionState.toString());
        while (count < timeoutCount && executionState != SBExecState.SB_DONE
                && executionState != SBExecState.SB_ERROR) {
            Thread.sleep(sleepInterval);
            count++;
            executionState = execState.getExecState();
            logger.info("Execution state = " + executionState.toString());
        }

        String schedBlkUID;
        if (executionState == SBExecState.SB_DONE) {
            schedBlkUID = execState.getLastExecUID();
            logger.info("Execution finished. ASDM UID = " + schedBlkUID);
        } else if (executionState == SBExecState.SB_ERROR) {
            logger.severe("Execution finished with an error:\n" + 
                    Util.getNiceErrorTraceString(execState.getLastError()[0]));
            schedBlkUID = null;
        } else {
            logger.severe("Timeout when waiting for the scheduling block execution to finish");
            schedBlkUID = null;
        }

        // Release the ExecutionState component
        container.releaseComponent(execStateName);

        // Release the AutomaticArray component.
        container.releaseComponent(arrayId.arrayComponentName);

        // Shutdown the Master2 component.
        master.shutdownPass1();
        master.shutdownPass2();

        return schedBlkUID;

    }

    /**
     * Initialize the Archive components.
     * 
     * @throws Exception
     */
    protected void initArchive() throws Exception {

        archConnectionComp = ArchiveConnectionHelper.narrow(container
                .getComponent(Name.ArchiveComponent));
        assertNotNull(archConnectionComp);

        archOperationComp = archConnectionComp
                .getOperational("TestDataCaptureInterface");
        assertNotNull(archOperationComp);

        archIdentifierComp = IdentifierHelper.narrow(container
                .getComponent(Name.ArchiveIdentifierComponent));
        assertNotNull(archIdentifierComp);

        archBulkStore = BulkStoreHelper.narrow(container.getComponent("ARCHIVE_BULKSTORE"));
        assertNotNull(archBulkStore);
        archBulkStore.init();
        
        logger.info("Connection to the ALMA Archive has been constructed.");
    }

    /**
     * Release Archive components.
     * 
     * @throws Exception
     */
    protected void releaseArchive() throws Exception {
        container.releaseComponent("ARCHIVE_BULKSTORE");
        container.releaseComponent(Name.ArchiveComponent);
        container.releaseComponent(Name.ArchiveIdentifierComponent);
    }

    /**
     * Load a scheduling block into the Archive.
     * 
     * @param schedBlock
     *            APDM SchedBlock object
     * @return UID used to store the SchedBlock into the Archive
     * @throws Exception
     */
    protected String loadSBIntoArchive(SchedBlock schedBlock) throws Exception {

        try {
            EntitySerializer serializer = EntitySerializer
                    .getEntitySerializer(logger);
            XmlEntityStruct ent = serializer.serializeEntity(schedBlock,
                    schedBlock.getSchedBlockEntity());
            archOperationComp.store(ent);
            return ent.entityId;
        } catch (IllegalEntity ex1) {
            logger.severe("Illegal entity: " + ex1.toString());
            fail();
        } catch (ArchiveInternalError ex2) {
            logger.severe("Archive internal error: " + ex2.toString());
            fail();
        }

        return null;
    }

    /**
     * Read a file containing a scheduling block XML and creates a SchedBlock
     * object from it.
     * 
     * @param filePath
     *            Path of the scheduling block XML file
     * @return SchedBlock object
     * @throws Exception
     */
    protected SchedBlock getSchedBlockFromFile(String filePath) throws Exception {

        String dirName;
        String fileName;
        int loc;
        if ((loc = filePath.lastIndexOf('/')) >= 0) {
            fileName = filePath.substring(loc + 1);
            dirName = filePath.substring(0, loc);
        } else {
            fileName = filePath;
            dirName = ".";
        }

        String xmlDoc = readSBFile(dirName, fileName);
        SchedBlock schedBlock = SchedBlock
                .unmarshalSchedBlock(new StringReader(xmlDoc));
        SchedBlockEntityT entity = schedBlock.getSchedBlockEntity();

        UIDLibrary uidlib = new UIDLibrary(logger);
        uidlib.replaceUniqueEntityId(entity, container
                .getTransparentXmlWrapper(IdentifierJ.class,
                        archIdentifierComp, IdentifierOperations.class));

        schedBlock.setSchedBlockEntity(entity);
        return schedBlock;
    }

    /**
     * Read SB from a file.
     * 
     * @param dirName
     *            Directory name
     * @param fileName
     *            File name
     * @return XML document
     * @throws Exception
     */
    private String readSBFile(String dirName, String fileName) throws Exception {

        // Check that the directory exists
        File dir = new File(dirName);
        if (!dir.isDirectory())
            throw new Exception("Directory " + dirName + " does not exist.");

        // Check that the file exists
        File file = new File(dir, fileName);
        if (!file.exists())
            throw new Exception("File " + fileName + "in directory " + dirName
                    + " does not exist.");

        // Read file contents
        BufferedReader in = null;
        StringBuffer xmlDoc = null;
        String line = null;
        try {
            in = new BufferedReader(new FileReader(file));
            xmlDoc = new StringBuffer();
            line = in.readLine();
            while (line != null) {
                xmlDoc.append(line + "\n");
                line = in.readLine();
            }
            in.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return new String(xmlDoc);
    }

    /**
     * Get an XML entity from the Archive.
     * 
     * @param uid
     *            SB UID
     * @return
     * @throws Exception
     */
    private XmlEntityStruct getEntityFromArchive(String uid) throws Exception {
        return archOperationComp.retrieveDirty(uid);
    }

    /**
     * Get scheduling block from the Archive.
     * 
     * @param uid
     *            SchedBlock UID
     * @return SchedBlock object
     * @throws Exception
     */
    protected SchedBlock getSBFromArchive(String uid) throws Exception {

        XmlEntityStruct ent = archOperationComp.retrieveDirty(uid);
        EntityDeserializer deserializer = EntityDeserializer
                .getEntityDeserializer(logger);
        SchedBlock schedBlock = (SchedBlock) deserializer.deserializeEntity(
                ent, SchedBlock.class);

        return schedBlock;
    }

}
