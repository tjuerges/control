/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.IntTest;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.asdm.ASDM;
import alma.asdm.Archiver;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.hla.runtime.asdm.types.EntityId;

/**
 * Holography CONTROL Integration Test Case.
 * 
 * @author rhiriart
 *
 */
public class HolographySBExecTest extends SBExecTest {

	public HolographySBExecTest(String name) throws Exception {
		super(name);
	}

	protected void setUp() throws Exception {
        super.setUp();		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "HG"; // Default test suite.
        try {
            if (testSuite.equals("HG")) {
//                suite.addTest(new HolographySBExecTest("testBasicSBExecution"));
                suite.addTest(new HolographySBExecTest("testHolographySBExecution"));
            }
        } catch (Exception ex) {
            System.err.println("Error when creating SBExecutionTest: "
                    + ex.toString());
        }
        return suite;
    }

	public void testBasicSBExecution() throws Exception {
        SchedBlock inSchedBlk = getSchedBlockFromFile("schedblks/THG_NEW_APDM.xml");
        String uid = loadSBIntoArchive(inSchedBlk);
        logger.info("Loaded SchedBlock into Archive. UID = " + uid);
        
        SchedBlock outSchedBlk = getSBFromArchive(uid);
        
        // Check some fields in the scheduling block		
	}
	
	public void testHolographySBExecution() throws Exception {

		String code;
		
        code =
            "from Control import AntennaStateEvent\n" +
            "import Control\n" +
            "state = Control.AntennaOperational\n" +
            "substate = Control.AntennaNoError\n" +
            "AntennaStateEvent('CONTROL/DV01', 0L, state, substate)";
        simulator.setMethod("CONTROL/DV01", "getAntennaState", code, 0);
        
        code = "None";        
        simulator.setMethod("CONTROL/DV01", "createSubdevices", code, 0);
        
        code = "LOGGER.logInfo('getTowerPosition called...')\n";        
        code += "(1000.0, 1200.0)";
        simulator.setMethodIF("IDL:alma/Control/TowerHolography:1.0", 
                              "getTowerPosition", code, 0.0);
        
        code = "LOGGER.logInfo('getTowerXYZPosition called...')\n";
        code += "(1000.0, 1200.0, 1400.0)";
        simulator.setMethodIF("IDL:alma/Control/TowerHolography:1.0", 
                              "getTowerXYZPosition", code, 0.0);
		
        code = "LOGGER.logInfo('getHolographyReceiver called...')\n";
        code += "holoRx = SELF.getComponent(comp_idl_type='IDL:alma/Control/HOLORXImpl:1.0', is_dynamic=1)\n";
        code += "holoRx._get_name()";
        simulator.setMethodIF("IDL:alma/Control/TowerHolography:1.0", 
                              "getHolographyReceiver", code, 0.0);
        
        code = "LOGGER.logInfo('getSubscanData called...')\n";
        code += "import random\n";
        code += "from Control import HolographySubScanData\n";
        code += "from Control import PositionData\n";
        code += "from Control import HolographyData\n";
        code += "from Acspy.Common.TimeHelper import getTimeStamp\n";
        code += "holographyData = HolographyData(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, True)\n";
        code += "from asdmIDLTypes import IDLAngle\n";
        code += "positionData = PositionData(IDLAngle(random.uniform(0.0, 1.0)), IDLAngle(random.uniform(0.0, 1.0)))\n";
        code += "startTime = getTimeStamp().value\n";
        code += "expTime = 1.0E7\n";
        code += "holographySubScanData = HolographySubScanData('DV01', 'SSN', positionData, startTime, 0L, 0L, [], [positionData], [positionData])\n";
//        code += "holographySubScanData = HolographySubScanData('DV01', 'SSN', positionData, 0L, 0L, 0L, [holographyData], [positionData], [positionData])\n";
        code += "holographySubScanData";
        simulator.setMethodIF("IDL:alma/Control/TowerHolography:1.0", 
                              "getSubscanData", code, 0.0);
        
        String asdmUID;
        asdmUID = performArrayObservation("schedblks/THG_NEW_APDM.xml", 60);
        if (asdmUID != null) {
            
            // Get the ASDM from the Archive and check its contents
            EntityId asdmEntId = new EntityId(asdmUID);
            Archiver archiver = new Archiver(container);
            ASDM asdm = ASDM.fromArchive(archiver, asdmEntId);
            
            alma.asdm.AntennaTable antennaTable = asdm.getAntenna();
            // etc...
            
        } else {
            fail("ASDM UID is null. The observation was not performed.");
        }

	}
	
    /**
     * Set up a test TMCDB configuration.
     * 
     * None: This is a temporary hack, while a way to configure the TMCDB
     * for test cases is implemented.
     * 
     */
    protected void setupTMCDBConfiguration() {
        
        StartupAntennaIDL[] sai = new StartupAntennaIDL[1];
        sai[0] = new StartupAntennaIDL();
        sai[0].antennaName = "DV01";
        sai[0].padName = "padName";
        sai[0].frontEndName = "";
        sai[0].frontEndAssembly = new AssemblyLocationIDL[0];
        sai[0].antennaAssembly = new AssemblyLocationIDL[3];
        AssemblyLocationIDL hrxConfig = 
            new AssemblyLocationIDL("Holography Reciever",
                                    "HoloRx", 
                                    0x1c, 
                                    0, 
                                    0);
        AssemblyLocationIDL hdspConfig = 
            new AssemblyLocationIDL("Holography DSP",
                                    "HoloDSP", 
                                    0x1d, 
                                    0, 
                                    0);
        AssemblyLocationIDL mcConfig = 
            new AssemblyLocationIDL("MountController & Mount",
                                    "MountController", 
                                    0, 
                                    0, 
                                    0);
        sai[0].antennaAssembly[0] = hrxConfig;
        sai[0].antennaAssembly[1] = hdspConfig;
        sai[0].antennaAssembly[2] = mcConfig;
        tmcdb.setStartupAntennasInfo(sai);
        
        AntennaIDL ai = new AntennaIDL();
        ai.AntennaName = "DV01";
        ai.AntennaType = "";
        ai.DishDiameter = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        ai.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.XOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.YOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ZOffset = new alma.asdmIDLTypes.IDLLength(0.0);
        ai.ComponentId = 0;
        tmcdb.setAntennaInfo("DV01", ai);
        
        PadIDL pi = new PadIDL();
        pi.PadName = "padName";
        pi.CommissionDate = new alma.asdmIDLTypes.IDLArrayTime(0);
        pi.XPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.YPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        pi.ZPosition = new alma.asdmIDLTypes.IDLLength(0.0);
        tmcdb.setAntennaPadInfo("DV01", pi);        
    }    
}
