package alma.Control.IntTest;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.Control.CorrelatorType;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.AutomaticArray2Helper;
import alma.Control.ExecutionStateHelper;
import alma.Control.SBExecState;
import alma.Control.Common.Util;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.TMCDB_IDL.AntennaIDL;
import alma.TMCDB_IDL.AssemblyLocationIDL;
import alma.TMCDB_IDL.PadIDL;
import alma.TMCDB_IDL.StartupAntennaIDL;
import alma.acs.nc.SimpleSupplier;
import alma.asdm.ASDM;
import alma.asdm.Archiver;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.bulkdata.BulkStore;
import alma.bulkdata.BulkStoreHelper;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.hla.runtime.asdm.types.EntityId;
import alma.hla.runtime.asdm.types.Interval;
import alma.telcal.CHANNELNAME_TELCALPUBLISHER;
import alma.telcal.TelCalReducedEvent;

public class InteractiveTest extends SBExecTest {
    
    static TestSuite suite;
    static String testSuite;
    static String schedBlockToRun;
    static int timeout;
    private SimpleSupplier publisher;
    private String execStateName;
    
    public InteractiveTest(String name) throws Exception {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        // The ASDM PointingTable is binary, so it is needed to initialize
        // ARCHIVE BulkStore.
        BulkStore bulkStore;
        bulkStore = BulkStoreHelper.narrow(getContainerServices().getComponent("ARCHIVE_BULKSTORE"));
        bulkStore.init();
        publisher = new SimpleSupplier(CHANNELNAME_TELCALPUBLISHER.value, container);
    }

    protected void tearDown() throws Exception {
        publisher.disconnect();
        getContainerServices().releaseComponent("ARCHIVE_BULKSTORE");
        super.tearDown();
    }
        
    public void testSFIBLSBExecution() throws Exception {
        
        startMaster();
        createAutomaticArray();
        setupSimulators();
        
        String asdmUID;
        asdmUID = performArrayObservation(schedBlockToRun, timeout);
        if (asdmUID != null) {
            // Get the ASDM from the Archive and check its contents
            EntityId asdmEntId = new EntityId(asdmUID);
            Archiver archiver = new Archiver(container);
            ASDM asdm = ASDM.fromArchive(archiver, asdmEntId);
            
            alma.asdm.AntennaTable antennaTable = asdm.getAntenna();
            // etc...
        } else {
            fail("ASDM UID is null. The observation was not performed.");
        }

        releaseAutomaticArray();
        shutdownMaster();
    }
        
    // TODO: Move this to the CDB alma/simulated directories
    public void setupSimulators() {
        
        String code;
        long timestamp1;
        long timestamp2;
        long timestamp3;
        
        // Set MountController behavior
        String[] mountControllers = new String[] {
                "CONTROL/DV01/MountController",
                "CONTROL/DA41/MountController",
                "CONTROL/ALMA03/MountController",
                "CONTROL/ALMA04/MountController"};
        for (String mcn : mountControllers) {
            logger.info("Setting behaviour for '"+mcn+"'");
            code = "LOGGER.logInfo('allocate() called')\n";
            code += "from Acssim.Goodies import setGlobalData\n";
            code += "antennaName = parameters[0]\n";
            code += "setGlobalData('"+mcn+":antennaName', antennaName)\n";
            code += "None";
            simulator.setMethod(mcn, "allocate", code, 0.0);            
            
            code = "LOGGER.logInfo('isPointingModelEnabled called')\n";
            code += "True";
            simulator.setMethod(mcn, "isPointingModelEnabled", code, 0.0);
            
            code = "LOGGER.logInfo('"+mcn+": waitUntilOnSourceCB called')\n";
            code += "callback = parameters[0]\n";
            code += "import ACSErr\n";
            code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
            code += "from Acssim.Goodies import getGlobalData\n";
            code += "antennaName = getGlobalData('"+mcn+":antennaName')\n";
            code += "LOGGER.logInfo('"+mcn+": reporting callback for antenna ' + antennaName)\n";
            code += "callback.report(antennaName, compl)\n";
            code += "None";
            simulator.setMethod(mcn, "waitUntilOnSourceCB", code, 0.0);
            
            // Timestamps to simulate 3 pointing samples, with a missing sample between samples 2 and 3.
            timestamp1 = Util.arrayTimeToACSTime(Util.getArrayTime().getAsLong(Interval.NANOSECOND));
            timestamp2 = timestamp1 + 480000L; // simulating the next sample, 48ms later
            timestamp3 = timestamp2 + 960000L; // simulating a missing sample
            code =
                "LOGGER.logInfo('getPointingDataTable called')\n" +
                "import Control\n" +
                "from Control import MountController\n" + 
                "eq = Control.EquatorialDirection(0, 0)\n" + 
                "hz = Control.HorizonDirection(0, 0)\n" +
                "of = Control.Offset(0.0, 0.0)\n" +
                "pd1 = MountController.PointingData(eq, of, eq, of, hz, hz, hz, hz, True, eq,"+timestamp1+", False)\n" +                 
                "pd2 = MountController.PointingData(eq, of, eq, of, hz, hz, hz, hz, True, eq,"+timestamp2+", False)\n" +                 
                "pd3 = MountController.PointingData(eq, of, eq, of, hz, hz, hz, hz, True, eq,"+timestamp3+", False)\n" +                 
                "[pd1,pd2,pd3]";
            simulator.setMethod(mcn, "getPointingDataTable", code, 0);
            simulator.setMethod(mcn, "getSelectedPointingDataTable", code, 0);
            
            code  =
                "LOGGER.logInfo('reportPointingModel called')\n" +
                "antennaName = getGlobalData('"+mcn+":antennaName')\n" +
                "LOGGER.logInfo('Accessing component "+arrayId.arrayName+"')\n" +
                "array = SELF.getComponent('" + arrayId.arrayComponentName + "')\n" +
                "import ReceiverBandMod\n" +
                "from TMCDB import ModelTerm\n" +
                "array.reportPointingModel(antennaName, ReceiverBandMod.ALMA_RB_01, [ModelTerm('FOO', 0.0)], [ModelTerm('FOO', 0.0)])\n" +
                "None";
            simulator.setMethod(mcn, "reportPointingModel", code, 0);            
        }
        
        // Set AntLOController behavior
        
        code = "LOGGER.logInfo('getReceiverInfo called...')\n";
        code +=       "('RX0123', 'Dewar-01', 299.99)";
        simulator.setMethodIF("IDL:alma/Control/AntLOController:1.0",
                              "getReceiverInfo", code, 0);
        
        code = "LOGGER.logInfo('getFrequency called...')\n";
        code += "import NetSidebandMod\n";
        code +=       "('ALMA_RB_03', 32E6, NetSidebandMod.LSB, 6.0E9, 5.8E9, 6.2E9, 6.1E9, True, True)";
        simulator.setMethodIF("IDL:alma/Control/AntLOController:1.0",
                              "getFrequencies", code, 0);
        
        code = "LOGGER.logInfo('getSignalPaths called...')\n";
        code +=       "(True, False, False, False, False, True)";
        simulator.setMethodIF("IDL:alma/Control/AntLOController:1.0",
                              "getSignalPaths", code, 0);
        
        // Setup the CVR Frequency property to return a valid figure
        
        code = "from ACSImpl.DevIO import DevIO\n";
        code += "devio = DevIO(13.42E9)\n";
        code += "setGlobalData('devio', devio)\n";
        code += "None";
        simulator.setMethod("CONTROL/CentralLO/PhotonicReference1/CVR", "initialize", code, 0);        
        simulator.setMethod("CONTROL/CentralLO/PhotonicReference2/CVR", "initialize", code, 0);        
        code = "getGlobalData('devio')";
        simulator.setMethod("CONTROL/CentralLO/PhotonicReference1/CVR", "Frequency", code, 0);
        simulator.setMethod("CONTROL/CentralLO/PhotonicReference2/CVR", "Frequency", code, 0);
    }

    private void startMaster() {
        // Initialize the Master component.
        master.startupPass1();
        master.startupPass2();        
    }
    
    private void shutdownMaster() {
        // Shutdown the Master component.
        master.shutdownPass1();
        master.shutdownPass2();
    }
    
    private void createAutomaticArray() {

        // Ask the Master to create an AutomaticArray component.
        String[] antennaNames = null;
        antennaNames = new String[2];
        antennaNames[0] = "DV01";
        antennaNames[1] = "DA41";
        	
        String[] photRefNames = null;
        photRefNames = new String[1];
        photRefNames[0] = "PhotonicReference2";
       
        try {
            arrayId = master.createAutomaticArray(antennaNames, photRefNames, CorrelatorType.BL);
        } catch (Exception ex) {
            logger.severe("Error creating Automatic Array: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }

        // Get a reference to the recently created AutomaticArray.
        try {
            array = AutomaticArray2Helper.narrow(container.getComponent(arrayId.arrayComponentName));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Cannot access AutomaticArray component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        } catch (Exception ex) {
            logger.severe("Error while accessing '" + arrayId.arrayName + 
                            "' AutomaticArray component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }

        // Check the state of the AutomaticArray.
        SystemState state = array.getArrayState();
        SystemSubstate substate = array.getArraySubstate();
        logger.info("Array State = " + STATE_NAMES[state.value()]);
        logger.info("Array Substate = " + SUBSTATE_NAMES[substate.value()]);

        assertEquals(SystemState.OPERATIONAL, state);
        assertEquals(SystemSubstate.NOERROR, substate);
        
        // Get the ExecutionState
        execStateName = array.getExecutionStateName();
        try {
            execState = ExecutionStateHelper.narrow(container.getComponent(execStateName));
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Cannot access ExecutionState component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        } catch (Exception ex) {
            logger.severe("Error while accessing '" + execStateName + 
                    "' ExecutionState component: " + ex.toString());
            master.shutdownPass1();
            master.shutdownPass2();
            fail();
        }
        assertNotNull(execState);
    }
    
    private void releaseAutomaticArray() {
        
        // Release the ExecutionState component
        container.releaseComponent(execStateName);
        
        // Release the AutomaticArray component.
        container.releaseComponent(arrayId.arrayComponentName);
    }
    
    @Override
    protected String performArrayObservation(String schedBlkFileName, int timeout) 
        throws Exception {
        
        // Read a Scheduling Block file.
        SchedBlock testSchedBlk = getSchedBlockFromFile(schedBlkFileName);
        
        // Load this Scheduling Block into the Archive.
        String sbUID = null;
        sbUID = loadSBIntoArchive(testSchedBlk);
        logger.info("Scheduling Block UID: " + sbUID);

        
        // Create the two entity references required to perform an observation
        String entityId = testSchedBlk.getSchedBlockEntity().getEntityId();
        String partId = "X00000000";
        String entityTypeName = testSchedBlk.getSchedBlockEntity().getEntityTypeName();
        String documentVersion = testSchedBlk.getSchedBlockEntity().getDocumentVersion();
        logger.fine("SchedBlock EntityId = " + entityId +
                      "; PartId = " + partId +
                      "; EntityTypeName = " + entityTypeName + 
                      "; DocumentVersion = " + documentVersion);
        
        IDLEntityRef sbEntRef = new IDLEntityRef(testSchedBlk.getSchedBlockEntity().getEntityId(),
                                                 "X00000000", 
                                                 testSchedBlk.getSchedBlockEntity().getEntityTypeName(),
                                                 testSchedBlk.getSchedBlockEntity().getDocumentVersion());
        // Usually SCHEDULING will create a valid IDLEntityRef for the session.
        // It is hardcoded in this test case.
        IDLEntityRef sessionEntRef = new IDLEntityRef("uid://X0000000000000000/X00000000/X0",
                                                      "X00000000", 
                                                      "Session",
                                                      "1.0");
        // ... and action!
        array.observe(sbEntRef, sessionEntRef, 0L);

        // Wait with a timeout for the arrival of the ExecBlockEndedEvent
        // in the ExecutionState component.
        int sleepInterval = 1000; // sleep interval in milliseconds
        int timeoutCount = (int) 1000.0 * timeout / sleepInterval; // timeout
                                                                    // in cycle
                                                                    // counts
        int count = 0;
        boolean telCalReducedEventSent = false; // send the TelCalReducedEvent only once
        SBExecState executionState = execState.getExecState();
        logger.info("Execution state = " + executionState.toString());
        while (count < timeoutCount && executionState != SBExecState.SB_DONE
                && executionState != SBExecState.SB_ERROR) {
            Thread.sleep(sleepInterval);
            count++;
            executionState = execState.getExecState();
            // DataCapturer will wait for the TelCalReducedEvent to be sent.
            if ( (executionState == SBExecState.SB_ARCHIVING) && !telCalReducedEventSent ) {
                logger.info("Sending TelCalReducedEvent");
                TelCalReducedEvent event = new TelCalReducedEvent();
                event.execBlockId = new IDLEntityRef();
                event.execBlockId.entityId = execState.getLastExecUID();
                event.execBlockId.entityTypeName = "";
                event.execBlockId.instanceVersion = "";
                event.execBlockId.partId = "";
                event.finishedAt = 0;
                event.scanNum = 0;
                publisher.publishEvent(event);
                telCalReducedEventSent = true;
            }
            
            logger.info("Execution state = " + executionState.toString());
        }

        String execBlkUID;
        if (executionState == SBExecState.SB_DONE) {
            execBlkUID = execState.getLastExecUID();
            logger.info("Execution finished. ASDM UID = " + execBlkUID);
        } else if (executionState == SBExecState.SB_ERROR) {
            logger.severe("Execution finished with an error");
            execBlkUID = null;
        } else {
            logger
                    .severe("Timeout when waiting for the scheduling block execution to finish");
            execBlkUID = null;
        }
        
        return execBlkUID;

    }
    
    public static Test suite() {
        suite = new TestSuite();
        testSuite = System.getProperty("suite");
        schedBlockToRun = System.getProperty("schedblock");
        timeout = Integer.parseInt(System.getProperty("timeout"));
        if (testSuite == null)
            testSuite = "Default"; // Default test suite.
        try {
            suite.addTest(new InteractiveTest("testSFIBLSBExecution"));
        } catch (Exception ex) {
            System.err.println("Error when creating SBExecutionTest: "
                    + ex.toString());
        }
        return suite;
    }
    
}
