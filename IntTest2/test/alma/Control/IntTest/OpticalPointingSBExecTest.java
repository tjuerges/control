/**
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.IntTest;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.asdm.ASDM;
import alma.asdm.Archiver;
import alma.hla.runtime.asdm.types.EntityId;

/**
 * Optical Pointing CONTROL Integration Test Case.
 * 
 * @author rhiriart
 *
 */
public class OpticalPointingSBExecTest extends SBExecTest {
    
	public OpticalPointingSBExecTest(String name) throws Exception {
		super(name);
	}

	protected void setUp() throws Exception {
        super.setUp();		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "OP"; // Default test suite.
        try {
            if (testSuite.equals("OP")) {
                suite.addTest(new OpticalPointingSBExecTest(
                        "testOpticalPointingSBExecution"));
            }
        } catch (Exception ex) {
            System.err.println("Error when creating OpticalPointingSBExecTest: "
                    + ex.toString());
        }
        return suite;
    }
    
	public void testOpticalPointingSBExecution() throws Exception {        
        String asdmUID;
        asdmUID = performArrayObservation("schedblks/OP_NEW_APDM.xml", 60);
        if (asdmUID != null) {            
            // Get the ASDM from the Archive and check its contents
            EntityId asdmEntId = new EntityId(asdmUID);
            Archiver archiver = new Archiver(container);
            ASDM asdm = ASDM.fromArchive(archiver, asdmEntId);
            
            alma.asdm.AntennaTable antennaTable = asdm.getAntenna();
            // etc...
            
        } else {
            fail("ASDM UID is null. The observation was not performed.");
        }
	}
}
