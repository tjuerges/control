// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.logging.Logger;

import alma.Control.ObservingModeTester;
import alma.Control.ObservingModeTesterOperations;

import alma.ACS.ComponentStates;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.AntennaCharacteristics;
import alma.Control.BDDStreamInfo;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.DSBbasebandSpec;
import alma.Control.LocalOscillator;
import alma.Control.ResourceId;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

public class ObservingModeTesterImpl 
        implements ComponentLifecycle, ObservingModeTesterOperations {

    private Logger logger;
    private LocalOscillatorImpl loObsMode;
    private ContainerServices container = null;

    // //////////////////////////////////////////////////////////////////
    // Inner classes
    // //////////////////////////////////////////////////////////////////

    private static class FakeArray implements ObservingModeArray {
        private ResourceManager resMng;
        private String[] antNames = {"ALMA01", "ALMA02", "ALMA03", "ALMA04"};
        private int corrConfigID = 0;
        private int scan = 0;
        private int subScan = 0;

        // Constructor
        public FakeArray() {
        }

        // Constructor
        public FakeArray(String[] ants) {
            antNames = ants;
        }

        @Override   
        public IDLEntityRef getASDMEntRef() {
            return new IDLEntityRef();
        }

        @Override   
        public String[] getAntennas() {
            return antNames;
        }

        @Override   
        public AntennaCharacteristics[] getAntennaConfigurations() {
            AntennaCharacteristics[] ac = 
                    new AntennaCharacteristics[antNames.length];
            for (int i = 0; i < antNames.length; i++) {
                ac[i] = new AntennaCharacteristics();
                ac[i].antennaName = antNames[i];
                ac[i].dishDiameter = new IDLLength(0.0);
                ac[i].xPosition = new IDLLength(0.0);
                ac[i].yPosition = new IDLLength(0.0);
                ac[i].zPosition = new IDLLength(0.0);
                ac[i].xOffset = new IDLLength(0.0);
                ac[i].yOffset = new IDLLength(0.0);
                ac[i].zOffset = new IDLLength(0.0);
                ac[i].dateOfCommission = 0;
                ac[i].padId = "PadID";
                ac[i].padxPosition = new IDLLength(0.0);
                ac[i].padyPosition = new IDLLength(0.0);
                ac[i].padzPosition = new IDLLength(0.0);
                ac[i].cableDelay = 0;
            }
            return ac;            
        }

        @Override   
        public int getCorrelatorConfigID() {
            return corrConfigID;
        }
        @Override   
        public void incrCorrelatorConfigID() {
            corrConfigID++;
        }
        @Override   
        public String getDataCapturerName() {
            return "CONTROL/Array001/DC000";
        }

        @Override   
        public int getScanNumber() {
            return scan;
        }

        @Override   
        public int getSubScanNumber() {
            return subScan;
        }

        @Override   
        public void incrScanNumber() {
            scan++;
        }
        @Override   
        public void incrSubScanNumber() {
            subScan++;
        }
        @Override   
        public void resetScanNumber() {
            scan = 0;
        }
        @Override   
        public void setScan(int s) {
            scan = s;
        }
        @Override   
        public void resetSubScanNumber() {
            subScan = 0;
        }
        @Override   
        public void setSubScan(int ssn) {
            subScan = ssn;
        }

        @Override   
        public String getConfigurationName() {
            return "ALMA";
        }

        @Override   
        public String getArrayName() {
            return "Array001";
        }

        @Override  
        public String getArrayComponentName() {
            return "Array001";
        }
        @Override   
        public boolean sbHasBeenAborted() {
            return true;
        }    
        @Override   
        public boolean sbHasBeenStopped() {
            return true;
        } 
        @Override   
        public boolean sbHasToStop() {
            return true;
        } 
        @Override   
        public void setExecBlockStartTime(long execBlockStartTime) {
        }
        @Override   
        public void setScript(String scriptName) {
        }
        @Override   
        public void setProjectName(String projectName) {
        }
        @Override   
        public void setPiName(String name) {
        }
        @Override   
        public void setSchedBlockUid(String name) {
        }
        @Override   
        public void setExecBlockUid(String name) {
        }
        @Override   
        public void setExecBlockAllowedTime(long time) {
        }
        @Override   
        public void setCorrMode(short mode) {
        }
        @Override   
        public void setCorrConfigID(int id) {
        }
        public void setApState(String state) {
        }
        @Override   
        public void setUsesCorr(boolean use) {
        }
        @Override   
        public void setSubScanDuration(long time) {
        }
        @Override   
        public void setScanStartTime(long time) {
        }
        @Override   
        public void setScanIntentData(alma.Control.ScanIntentData[] sid) {
        }


        // Any resource name can be used, but none will be found
        // in a lookup from the resource manager (don't know why). 
        // The component name must match the one from the CDB.  
        @Override   
        public ResourceId[] getPhotonicReferences() {
            ResourceId[] rids = new ResourceId[1];
            rids[0] = new alma.Control.ResourceId();  
            String loc = "CONTROL/CentralLO/";
            String photonicRef = "PhotonicReference2";
            String resName  = photonicRef;
            String compName = loc + photonicRef;
            rids[0].ResourceName  = resName;
            rids[0].ComponentName = compName;
            try {
                resMng.getResource(resName);
            } catch (Exception e) {
                String m = "PhotonicReference resource (\"" + 
                            resName + "\") not found in ResourceManager";
                // The lookup always fails, so skip the log message            
                //logger.info(m);
            } 
            return rids;
        }        

        @Override   
        public String getCorrelatorObservationControlComponentName() {
            return "CORR/OBSERVATION_CONTROL";
        }

        @Override   
        public BDDStreamInfo getCorrelatorBDDStreamInfo() {
            return new BDDStreamInfo();
        }
     }

    // //////////////////////////////////////////////////////////////////
    // Constructors
    // //////////////////////////////////////////////////////////////////

    public ObservingModeTesterImpl() {
    }

    // -----------------------------------------------------------
    // Implementation of ACSComponent and ComponentLifecycle
    // -----------------------------------------------------------

    @Override   
    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }

    @Override   
    public String name() {
        return container.getName();
    }

    @Override   
    public void initialize(ContainerServices containerServices)
        throws ComponentLifecycleException {
        container = containerServices;
        logger    = container.getLogger();
    }
    @Override   
    public void execute() throws ComponentLifecycleException {
        logger.fine("execute()");
    }
    @Override   
    public void cleanUp() {
    }
    @Override   
    public void aboutToAbort() {
        cleanUp();
    }

    // -----------------------------------------------------------
    // Implementation of IDL methods of ObservingModeTester
    // -----------------------------------------------------------
    @Override   
    public void modifySimulationCode(alma.ACSSim.Simulator sim) 
            throws ContainerServicesEx, ObservingModeErrorEx {
        modifyPythonSimCode(sim, container);
    }

    @Override   
    public alma.Control.LocalOscillator getTestLocalOscillator() 
            throws ContainerServicesEx, ObsModeInitErrorEx {
        LocalOscillatorImpl loimpl =
            new LocalOscillatorImpl(container, new FakeArray());
        logger.info("ObservingModeTester.getTestLocalOscillator()");
        LocalOscillator lo = loimpl.getLocalOscillator();
        if (lo == null) logger.severe("lo is null!!");
        return lo;
    }

    @Override   
    public String alignSpectralSpec(String spectralSpec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        return LocalOscillatorImpl.alignSpectralSpec(spectralSpec, logger);
    }
    // -----------------------------------------------------------
    // Implementation of other public methods of ObservingModeTester
    // -----------------------------------------------------------
    /// Uses four antennas, ALMA01... ALMA04
    static public ObservingModeArray getFakeArray() {
        return new FakeArray();
    }
    // Uses antenna names as specified
    static public ObservingModeArray getFakeArray(String[] antNames) {
        return new FakeArray(antNames);
    }

    static public alma.ACSSim.Simulator modifyPythonSimCode(
        ContainerServices container) 
                throws ContainerServicesEx, ObservingModeErrorEx {
        String simidl = "IDL:alma/ACSSim/Simulator:1.0";
        alma.ACSSim.Simulator sim = null;
        Logger logger = container.getLogger();
        try {
            sim = SimulatorHelper.narrow(container.getDefaultComponent(simidl));
        } catch (AcsJContainerServicesEx jex) {
            jex.log(logger);
            throw jex.toContainerServicesEx();
        }
        modifyPythonSimCode(sim, container);
        return sim;
    }
    static public alma.ACSSim.Simulator modifyPythonSimCode(
            alma.ACSSim.Simulator sim, ContainerServices container) 
                throws ContainerServicesEx, ObservingModeErrorEx {
        Logger logger = container.getLogger();
        if (sim == null) {
            logger.severe("****Null sim!!!!!***");
            AcsJObservingModeErrorEx ex = new AcsJObservingModeErrorEx();
            //ex.setProperty("Detail", "Null sim");
            //throw ex; 
            ObservingModeUtilities.throwObservingModeErrorEx("Null sim", ex, logger);        
        }
        logger.info("ObservingModeTester.modifySimulationCode()");
        logger.info("Beginning code modification");

        String initcode = "";
        String code     = "";
        String setcode  = "";

        // getWVRIntegrationTime()
        // The simulator will return random numbers for the integ time and
        // the comparison for all integ times the same will barf.
        // This code returns the same integ time and fixes this problem.
        code  = "LOGGER.logInfo('AntInterferometry.getWVRIntegrationTime ";
        code += "called...')\n";
        code += "1000000000";  // Return value
        sim.setMethodIF(
            "IDL:alma/Control/AntInterferometryController:1.0",
             "getWVRIntegrationTime", code, 0);

        // getReceiverInfo()
        code = "LOGGER.logInfo('getReceiverInfo called...')\n";
        code += "('RX0123', 'Dewar-01', 299.99)";
        sim.setMethodIF("IDL:alma/Control/AntLOController:1.0",
                "getReceiverInfo", code, 0);

        // getFrequencies()
        // The return values correspond to the out parameters:
        // (1) Band name
        // (2) FLOOG frequency
        // (3) Sideband
        // (4) LO2 1st freq
        // (5) LO2 2nd freq
        // (6) LO2 3rd freq
        // (7) LO2 4th freq
        // (8) ABbands USB select
        // (9) CDbands USB select
        code = "import NetSidebandMod\n";
        code += "LOGGER.logInfo('getFrequencies called...')\n";
        code += "('Band3', 32E6, NetSidebandMod.LSB, " +
                  "6.0E9, 5.8E9, 6.2E9, 6.1E9, True, False)";
        sim.setMethodIF("IDL:alma/Control/AntLOController:1.0",
                "getFrequencies", code, 0);

        // getSignalPaths()
        code = "";
        code += "LOGGER.logInfo('getSignalPaths called...')\n";
        code += "(True, False, False, False, False, True)";
        sim.setMethodIF("IDL:alma/Control/AntLOController:1.0",
                "getSignalPaths", code, 0);

        // Setup the CVR Frequency property to return what is set.
        // This is done by having it return a static DevIO property.
        // A DevIO property has a read() and a write(); we use the write()
        // and the LOimpl code uses get_sync(), leaving to the device to
        // internally do the read() in its get_sync() implementation.
        initcode = "";
        initcode += "from ACSImpl.DevIO import DevIO\n";
        initcode += "LOGGER.logInfo('Initializing CVR...')\n";
        initcode += "value = DevIO(12.345E9)\n";
        initcode += "setGlobalData('CVRfreq', value)\n";
        initcode += "return None";
        sim.setMethod("CONTROL/CentralLO/PhotonicReference2/CVR",     
                "initialize", initcode, 0);
        code = "";
        //code +="LOGGER.logInfo('FREQUENCY called...')\n";
        code += "return getGlobalData('CVRfreq')";
        sim.setMethod("CONTROL/CentralLO/PhotonicReference2/CVR",     
                "FREQUENCY",    code, 0);
        setcode = "";
        setcode += "from ACSImpl.DevIO import DevIO\n";        
        setcode += "freq = parameters[0]\n";         
        setcode += "getGlobalData('CVRfreq').write(freq)\n";
        setcode += "LOGGER.logInfo('SET_FREQUENCY(%.2f) called' %(1e-9*freq))\n";
        setcode += "return None";
        sim.setMethod("CONTROL/CentralLO/PhotonicReference2/CVR",
              "SET_FREQUENCY",setcode, 0);
        
        // Setup the LS Frequency property to return what is set
        code  = "freq = parameters[0]\n";         
        code += "setGlobalData('LSfreq', freq)\n";
        code +="LOGGER.logInfo('PhotonicRef.setFrequency called...')\n";
        code += "return None";
        sim.setMethod("CONTROL/CentralLO/PhotonicReference2",
                      "setFrequency",code, 0);
        code  = "freq = getGlobalData('LSfreq')\n";
        code += "return freq";
        sim.setMethod("CONTROL/CentralLO/PhotonicReference2",
                "getFrequency",code,0);

        // setFrequenciesAsynch()
        String[] ants = {"ALMA01", "ALMA02", "ALMA03", "ALMA04"};
        for (String ant: ants) {
            String comp = "CONTROL/" + ant + "/AntLOController";
            code  = "from ACSErrTypeOKImpl import ACSErrOKCompletionImpl\n";
            code += "from time import sleep\n";
            code += "freq = parameters[1]\n";         
            code += "mstr = 'setFrequenciesAsynch(%.2f) called on " + ant;
            code += "' %(1e-9*freq)\n";
            code += "LOGGER.logInfo(mstr)\n";
            code += "cb = parameters[12]\n";
            // This sleep prevents a race where the ant reports back too soon,
            // causing the test to fail.
            code += "sleep(2)\n";
            code += "cb.report('"+ant+"', ACSErrOKCompletionImpl())\n";
            //code += "mstr = 'Callback completed for setFreq for " + ant + "'\n";
            //code += "LOGGER.logInfo(mstr)\n";
            code += "return None";
            sim.setMethod(comp,"setFrequenciesAsynch",code,0);
        }
        // enableLLCCorrectionAsynch()
        for (String ant: ants) {
            String comp = "CONTROL/" + ant + "/AntInterferometryController";
            code  = "from ACSErrTypeOKImpl import ACSErrOKCompletionImpl\n";
            code += "from time import sleep\n";
            code += "tf = parameters[0]\n";         
            code += "mstr = 'enableLLCCorrection(%d) called on ";
            code += ant + "' %tf\n";
            code += "LOGGER.logInfo(mstr)\n";
            code += "cb = parameters[1]\n";
            // This sleep prevents a race where the ant reports back too soon,
            // causing the test to fail.
            code += "sleep(2)\n";
            code += "cb.report('" + ant + "', ACSErrOKCompletionImpl())\n";
            code += "mstr = 'Callback completed for enableLLCCorrection for ";
            code += ant + "'\n";
            code += "LOGGER.logInfo(mstr)\n";
            code += "return None";
            sim.setMethod(comp,"enableLLCCorrectionAsynch",code,0);
        }
                     
        code =
            "import Control\n" +
            "from Control import MountController\n" + 
            "eq = Control.EquatorialDirection(0, 0)\n" + 
            "hz = Control.HorizonDirection(0, 0)\n" +
            "of = MountController.Offset(0.0, 0.0)\n" +
            "pd = MountController.PointingData(" +
                  "eq, of, eq, of, hz, hz, hz, hz, True, eq, 0, False)\n" + 
            "[pd]";
        sim.setMethodIF("IDL:alma/Control/SingleFieldInterferometry:1.0",
                "getPointingDataTable", code, 0);        
        logger.info("Completed code modification");
        return sim;
    }

}
