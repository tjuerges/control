# Optical Pointing Test Script
# $Id$
#

import Control
import CCL.OpticalPointing
import ModeControllerExceptions
import random
from math import degrees, radians
from CCL.Global import *
from CCL.logging import getLogger

logger = getLogger()

logger.logInfo('Beginning the execution of the optical pointing script.')

beginExecution()

# Get the Array and the Scheduling Block.
array = getArray()
# logger.logInfo("Got the array object. Array status: " + str(array.status()))

# Check that we have a single antenna in the array
antennaNames = array.antennas()
logger.logInfo("This array contains antennas " + str(antennaNames))
if len(antennaNames) != 1:
    errorMsg = "Array contains more than one antenna."
    logCritical(errorMsg)
    endExecution(Control.FAIL, errorMsg)

# objectifier = getSBObjectifier()
# sb = objectifier.getSB()
sb = getSB()

# Log some information from the scheduling block
logger.logInfo("This scheduling block contains " + \
        str(len(sb.SchedBlock.FieldSource)) + \
        " stars")
# logger.logInfo("SchedBlock: "+ str(sb))

# Optical Pointing observing parameters that come from the SB
EXP_TIME = 5.0                # exposure time, in seconds
DARK_EXP_TIME = 5.0           # exposure time, in seconds
OBS_TIME = sb.SchedBlock.OpticalCameraSpec.attributes.getNamedItem('filter').value
SNR_THRESHOLD = 10.0          # signal to noise ratio parameter
SOURCE_TRACKING_ERROR = radians(1.0/60/60) # maximum source tracking error
TRACK_TIMEOUT = 120            # maximum allowed time, in seconds, for
                              # the antenna to go "on-source".

# get the optical pointing observing mode object. The control
# subsystem must be operational for this to work. More specifically the
# relevant antenna, mount controller, optical telescope and
# frame-grabber components must be operational.
op = array.getOpticalPointingObservingMode()

# initialize the hardware (open the shutter etc.).
op.initializeHardware()

# Put the IR filter in for daytime observing
if OBS_TIME == 'day':
    logger.logInfo('Configuring optical telescope for daytime observations.')
    op.getOpticalPointingModeController().getOpticalTelescope().dayTimeObserving()
else:
    logger.logInfo('Optical telescope configured for night time observations.')

# Do a longer dark exposure. By default its only 1 second.
op.doDarkExposure(DARK_EXP_TIME)

# set the timeout and tolerance for antenna motion.
opmc = op.getOpticalPointingModeController()
mc = opmc.getMountController()
mc.setTimeout(TRACK_TIMEOUT)
mc.setTolerance(SOURCE_TRACKING_ERROR)

maspyr2radpsec = radians(1.0/(3600.0*1000.0))*(1/(365.25*24.0*3600.0))
scan = 1;

randomList = sb.getTargets()
logger.logInfo('Randomize the targets')
random.shuffle(randomList)

for target in randomList:
    source = sb.getFieldSource(target)
    ra = source.sourceCoordinates.longitude.getValue()
    dec = source.sourceCoordinates.latitude.getValue()
    pmRA = source.pMRA.getValue() * maspyr2radpsec
    pmDec = source.pMDec.getValue() * maspyr2radpsec
    parallax = source.parallax.getValue()*radians(1.0/(3600.0*1000.0))
    logger.logInfo("Scan #" + str(scan) + \
            " is '" + source.sourceName.getValue() + \
            "' at an (RA, Dec) of (" + str(ra) + ", " + str(dec) + ") degrees")
	    
    if mc.isObservableEquatorial(radians(ra), radians(dec)):
        (az, el) = mc.toAzElEquatorial(radians(ra), radians(dec))
        logger.logInfo("Its approximate (Az, El) is (" + str(degrees(az)) + \
                ", " + str(degrees(el)) + ") degrees")
        try:
            op.beginScan(source);
            data = opmc.doSourceExposure(EXP_TIME, radians(ra), radians(dec), pmRA, pmDec, parallax);
            outfl="%.0f"%((data.endTime + data.startTime)/2.0)
            op.sendSubscanData(data)
            op.endScan();
        except (ModeControllerExceptions.BadDataEx), e:
            logger.logWarning("Skipping this star as its parameters could not be determined")
            op.endScan();
        except (ModeControllerExceptions.TimeoutEx), e:
            logger.logWarning("Skipping this star as the antenna could not point at it")
            op.endScan();
            mc.setTrackMode()
        except (ModeControllerExceptions.HardwareFaultEx), e:
            logger.logWarning("Skipping this star as the was a problem with the hardware. Lets hope its intermittant.")
            op.endScan();
    else:
        (az, el) = mc.toAzElEquatorial(radians(ra), radians(dec))
        logger.logWarning("Skipping this star as its below the horizon. The (Az, El) is (" + str(degrees(az)) + \
                ", " + str(degrees(el)) + ") degrees")
    scan += 1;
    
logger.logInfo('Shutting down the optical telescope and antenna')
op.shutdownHardware()
endExecution(Control.SUCCESS, 'optical pointing')
