# Tower Holography Test Script.
# $Id$
#

from CCL.Global import *
from CCL.ScanIntent import ScanIntent
from CCL.logging import getLogger
from math import ceil, degrees, floor, pi, radians
import CCL.HolographyRaster
import CCL.TowerHolography
import Control
import ModeControllerExceptions
import SubscanIntentMod
import asdmIDLTypes

logger = getLogger()

beginExecution()

array = getArray()
schedBlk =  getSB()

thOM = array.getTowerHolographyObservingMode()
thMC = thOM.getTowerHolographyController()

# This is somewhat temporary until the APDM is modified to explicity
# state the number of rows.

nRow    = None;
rowSize = None;

# Uncomment these lines to use hard coded values for nrows and rowsize
#nRow = 180;
#rowSize = 1.24;

# Get the observing parameters from the SchedBlock
rowsCal = schedBlk.SchedBlock.HolographyParameters.rowsCal.getValue()
calTime = schedBlk.SchedBlock.HolographyParameters.calTime.getValue()
speed = schedBlk.SchedBlock.HolographyParameters.speed.getValue()/3600.0
startFraction = schedBlk.SchedBlock.HolographyParameters. \
               startFraction.getValue()
frequency = schedBlk.SchedBlock.HolographyParameters.frequency.getValue() * 1E9
nRow = schedBlk.SchedBlock.HolographyParameters.nRows.getValue()
rowSize = schedBlk.SchedBlock.HolographyParameters.rowSize.getValue()


if (schedBlk.SchedBlock.HolographyParameters.attributes. \
	get('scanDirection').value == 'AzScan'):
   verticalScans = False;
else:
   verticalScans = True;

# TODO: The tower position should come from the Telescope
# configuration data base (TCDB) and be available as geocentric
# (X,Y,Z) positions which are converted to an (AZ, El) direction in
# this script (in conjunction with the antenna/pad XYZ positions).

# AEC tower position
#thMC.setTowerPosition (radians(154.59),radians(8.28))
thMC.setTowerPosition (radians(154.57958),radians(8.314028))
(towerAz, towerEl) = thMC.getTowerPosition()

# Set up the hardware
if (frequency <= 90E9):
   thMC.initializeHardware(True)
else:
   thMC.initializeHardware(False)


startRow = floor(startFraction * nRow)
logger.logInfo('Holography scan contains ' + str(nRow) + ' sub-scans')

scanIntent = ScanIntent("MAP_ANTENNA_SURFACE")
thOM.beginScan(scanIntent, "HolographyScan", int(nRow))

if (startRow != 0):
        logInfo('Starting at row ' + startRow)

for row in range(startRow, nRow):
     if (row % rowsCal == 0) or (row == startRow):
          # Now we need to do a phase cal (Always do one at the start)
          thOM.beginSubscan([SubscanIntentMod.REFERENCE])
          calTimeout = thMC.startPhaseCal(duration=calTime)
          try:
               subScanData = thMC.getSubscanData(timeout=1.2*calTimeout)
          except ModeControllerExceptions.TimeoutEx:
               logCritical('Timeout getting subscan data.Timeout was: %d' \
                           % calTimeout)
               thOM.endSubscan()
               continue
          subScanData.subscanName = "Phase Cal: %d" % (row/rowsCal)
          thOM.sendSubscanData(subScanData)
          thOM.endSubscan()

     thOM.beginSubscan([SubscanIntentMod.SCANNING])

     perpOffset = (row-nRow/2.0 + 0.5)/nRow * rowSize
     logger.logInfo('Perpendicular offset ' + str(perpOffset) + ' degrees')
     if (row % 2 == 0):
        direction = True
     else:
        direction = False

     if (verticalScans):
        strokeDuration = thMC.startVerticalSubscan(radians(perpOffset),
                                                     forward=direction,
                                                     width=radians(rowSize),
                                                     velocity=radians(speed))
     else:
        strokeDuration = thMC.startHorizontalSubscan(radians(perpOffset),
                                                     forward=direction,
                                                     width=radians(rowSize),
                                                     velocity=radians(speed))
     try:
          subScanData = thMC.getSubscanData(timeout=strokeDuration+2)
     except ModeControllerExceptions.TimeoutEx:
          logCritical('Timeout getting subscan data.')
          array.endSubscan()
          continue
     subScanData.subscanName = "Row %d" % row
     subScanData.towerDirection = Control.PositionData(
                                    asdmIDLTypes.IDLAngle(towerAz),
                                    asdmIDLTypes.IDLAngle(towerEl))

     thOM.sendSubscanData(subScanData)
     thOM.endSubscan()

#Finish up with a phaseCal
thOM.beginSubscan([SubscanIntentMod.REFERENCE])
calTimeout=thMC.startPhaseCal(duration=calTime);
try:
     subScanData = thMC.getSubscanData(timeout=calTimeout+2);
except ModeControllerExceptions.TimeoutEx:
     logger.logCritical('Timeout getting subscan data.Timeout was: %d' % calTimeout)
     thOM.endSubscan()
else:
     subScanData.subscanName = \
                             "Phase Cal: %d"%(nRow/rowsCal+1)
     thOM.sendSubscanData(subScanData)
     thOM.endSubscan()

thOM.endScan()
thMC.shutdownHardware()
endExecution(Control.SUCCESS, 'holography test end')
