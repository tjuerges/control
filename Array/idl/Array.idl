/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * $Id$
 */
#ifndef _ARRAY_IDL_
#define _ARRAY_IDL_

#include <ControlCommonInterfaces.idl>
#include <SingleFieldInterferometryObservingMode.idl>
#include <TowerHolographyObservingMode.idl>
#include <TowerHolography7mObservingMode.idl>
#include <OpticalPointingObservingMode.idl>
#include <TotalPowerObservingMode.idl>
#include <AstronomicalHolography.idl>
#include <asdmIDLTypes.idl>
#include <ArrayExceptions.idl>
#include <ObservingModeExceptions.idl>
#include <DataCapture.idl>
#include <ArrayStatus.idl>

#pragma prefix "alma"

module Control {

  /**
   * The interface that the ExecutionState component uses to update the execution
   * state in the array.
   */
  interface ExecutionStateUpdateable {
  
    /**
     * Used by the Execution State component to signal that the
     * Data Capture component is ready to receive data.
     */
    void dataCaptureReady();

    /**
     * Used by the ExecutionState component to signal that DataCapture
     * has received the correlator subscan data and therefore the endSubscan()
     * function in the AutomaticArray can terminate.
     */
    void subScanDataReady();

    /**
     * Used by the ExecutionState component to signal that DataCapture
     * has finish archiving the ASDM.
     */
    void asdmArchived(in offline::ASDMArchivedEvent event);
  
  };

  /**
   * AutomaticArray2 interface.
   * This will replace AutomaticArray.
   */
  interface AutomaticArray2 : Controller, ExecutionStateUpdateable, Control::ArrayStatus,
  	Control::AutomaticArray {

    /**
     * Set the list of antennas that make up this array.
     */
    void setAntennaList(in Control::AntennaSeq list);

    void setBulkDataDistributors(in string corrBulkDataDistr, in string totalPowerBulkDataDistr);

    /**
     * Used by the script executor component to report an error,
     * that is not fatal, in the execution of a script.
     */
    void reportScriptError(in string message);

    /**
     * Return the name of the Data Capture component assigned to the execution of this 
     * scheduling block, if any.
     * If there is no scheduling block executing, an empty string is returned.
     * @throws InaccessibleException Thrown if the Control System is 
     * in an inaccessible state.
     */    
    string getDCName() raises (Control::InaccessibleException);

	/**
	 * Get the name of the ExecutionState component.
	 */
	string getExecutionStateName();    	
    	
    /**
     * Set Telescope Configuration information.
     */
    void setConfigInfo(in string configurationName,
                       in Control::AntennaCharacteristicsSeq antennaCharacs,
                       in Control::WeatherStationCharacteristicsSeq weatherStCharacs,
                       in Control::SiteData siteData,
                       in ResourceSeq photonicReferences,
                       in CorrelatorType corrType);

	string getRunningSB();

    //////////////////
    // CCL commands //
    //////////////////

    void beginExecution()
        raises (ExecutionException, ArrayExceptions::UnfinishedExecBlockEx);

    void endExecution(in Control::Completion statusCode, in string message) 
      raises (ExecutionException);

    /////////////////////////////////////////////////////////////////////
    // Offshoot interface for data capture CCL methods
    /////////////////////////////////////////////////////////////////////
    
    Control::SingleFieldInterferometryObservingMode getSFIOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
    
    Control::TowerHolographyObservingMode getTowerHolographyOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
    
    Control::TowerHolography7mObservingMode getTowerHolography7mOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
    
    Control::OpticalPointingObservingMode getOpticalPointingOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
        
    Control::TotalPowerObservingMode getTotalPowerOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);

    AstronomicalHolography getAstronomicalHolographyOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
  };



  interface ManualArray : Controller, ExecutionStateUpdateable, Control::ArrayStatus,
    Control::ManualArrayMonitor, Control::ManualArrayCommand {

    /**
     * Set the list of antennas that make up this array.
     */
    void setAntennaList(in Control::AntennaSeq list);

    void setBulkDataDistributors(in string corrBulkDataDistr, in string totalPowerBulkDataDistr);
    
    /**
     * Get the name of the ExecutionState component.
     */
    string getExecutionStateName();     

    /**
     * Get the ScriptExecutor component.
     *
     * This function will create dinamically, and then configure the
     * ScripExecutor component.
     * @param conf ScriptExecutor configuration structure.
     */
    string getExecutor();
    
    /**
     * Set Telescope Configuration information.
     */
    void setConfigInfo(in string configurationName,
                       in Control::AntennaCharacteristicsSeq antennaCharacs,
                       in Control::WeatherStationCharacteristicsSeq weatherStCharacs,
                       in Control::SiteData siteData,
                       in ResourceSeq photonicReferences,
                       in CorrelatorType corrType);

    //////////////////
    // CCL commands //
    //////////////////

    /**
     * TODO - we may need a way for the user to specify an SB for manual mode. One idea was to 
     * have two different "flavors" of beginExecution, one taking arguments and the other not
     * taking any arguments. However, CORBA IDL does not allow this form of polymorphism.
     * Instead, we may want to have a setSB() method or something like that, which can
     * be called when the user uses the \sb macro of the CCL.
     */
    void beginExecution()
        raises (ExecutionException, ArrayExceptions::UnfinishedExecBlockEx);

    void endExecution(in Control::Completion statusCode, in string message) 
      raises (ExecutionException);

	asdmIDLTypes::IDLEntityRef getASDMEntityRef();

    /////////////////////////////////////////////////////////////////////
    // Offshoot interface for data capture CCL methods
    /////////////////////////////////////////////////////////////////////
    
    Control::SingleFieldInterferometryObservingMode getSFIOffshoot()
    	raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
    
    Control::TowerHolographyObservingMode getTowerHolographyOffshoot()
    	raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
    
    Control::TowerHolography7mObservingMode getTowerHolography7mOffshoot()
    	raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
    
    Control::OpticalPointingObservingMode getOpticalPointingOffshoot()
    	raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);

    Control::TotalPowerObservingMode getTotalPowerOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
        
    AstronomicalHolography getAstronomicalHolographyOffshoot()
        raises(ObservingModeExceptions::ObsModeInitErrorEx, AbortionException);
    /**
     * Return the name of the Data Capture component assigned to the execution of this 
     * scheduling block, if any.
     * If there is no scheduling block executing, an empty string is returned.
     * @throws InaccessibleException Thrown if the Control System is 
     * in an inaccessible state.
     */    
    string getDCName() raises (Control::InaccessibleException);
    
  };

}; // End of module Control

#endif
