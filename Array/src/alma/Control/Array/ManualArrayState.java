package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.offline.ASDMArchivedEvent;

public interface ManualArrayState extends ArrayState {
    void beginExecution(IDLEntityRef asdmEntityRef, IDLEntityRef sbEntRef,
                        IDLEntityRef sessionEntRef) 
        throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx, AcsJUnfinishedExecBlockEx;
    void endExecution() throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx;
    IDLEntityRef getASDMEntRef();
    IDLEntityRef getASDMUID() throws AcsJSBExecutionErrorEx;
    void dataCaptureReady();
    void asdmArchived(ASDMArchivedEvent event);
    void subScanDataReady();

}
