/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.AbortionException;
import alma.Control.AntennaStateEvent;
import alma.Control.CalDeviceData;
import alma.Control.OpticalPointingObservingMode;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.TotalPowerObservingMode;
import alma.Control.TowerHolographyObservingMode;
import alma.Control.TowerHolography7mObservingMode;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ReceiverBandMod.ReceiverBand;
import alma.TMCDB.ModelTerm;

public interface ArrayState extends ArrayStatus {

    public int getStateValue();
    
    public int getSubstateValue();
    
    public void shutdownPass1() throws AcsJInvalidRequestEx;
    
    public void shutdownPass2() throws AcsJInvalidRequestEx;
    
    public void startupPass1() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx;
    
    public void startupPass2() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx;
    
    public void waitShutdownPass1() throws AcsJInvalidRequestEx;

    public AntennaStateEvent[] getAntennaStates() throws InvalidRequestException;
    
    SingleFieldInterferometryObservingMode getSFIOffshoot()
        throws ObsModeInitErrorEx, AbortionException;
    
    TowerHolographyObservingMode getTowerHolographyOffshoot()
        throws ObsModeInitErrorEx, AbortionException;
    
    TowerHolography7mObservingMode getTowerHolography7mOffshoot()
        throws ObsModeInitErrorEx, AbortionException;
    
    OpticalPointingObservingMode getOpticalPointingOffshoot()
        throws ObsModeInitErrorEx, AbortionException;
    
    TotalPowerObservingMode getTotalPowerOffshoot()
        throws ObsModeInitErrorEx, AbortionException;

    alma.Control.AstronomicalHolography getAstronomicalHolographyOffshoot()
        throws ObsModeInitErrorEx, AbortionException;

    /* Antenna Status Reporting Methods */
    public void reportPointingModel(String antennaName, ReceiverBand band,
            ModelTerm[] pointingModel, ModelTerm[] auxModel);

    public void reportFocusModel(String antennaName, ReceiverBand band,
            ModelTerm[] focusModel);

    public void reportSubreflectorPosition
        (String antennaName, boolean focusTrackingEnabled,
         double x, double y, double z, 
         double xOffset, double yOffset, double zOffset);

    public void reportCalDeviceData(CalDeviceData data);
    
	public void flagData(boolean startStop, long timeStamp,
			String componentName, String description);
	
	
}
