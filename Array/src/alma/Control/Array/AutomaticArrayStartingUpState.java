/*
 * "@(#) $Id$"
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2002, 2008
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */

package alma.Control.Array;

import java.util.Map;

import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.Antenna;
import alma.Control.BDDStreamInfo;
import alma.Control.CreatedAutomaticArrayEvent;
import alma.Control.ExecutionState;
import alma.Control.ScriptExecutor;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.ResourceState;
import alma.Control.Common.State;
import alma.Control.Common.StaticResource;
import alma.Control.Common.Util;
import alma.ResourceExceptions.wrappers.AcsJBadResourceEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJUnknownResourceEx;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJException;
import alma.acs.nc.SimpleSupplier;
import alma.xmlstore.ArchiveConnection;
import alma.xmlstore.Identifier;

public class AutomaticArrayStartingUpState extends AutomaticArrayStateBase 
    implements AutomaticArrayState {
    
    private ArrayInternal array;

    ////////////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////////////    
    
    public AutomaticArrayStartingUpState(ArrayInternal array) {
        super(array, State.Inaccessible_StartingUpPass1);
        this.array = array;
    }

    ////////////////////////////////////////////////////////////////////
    // Overriden methods
    ////////////////////////////////////////////////////////////////////    

    @Override
        public void startupPass1() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx {
        
        setState(State.Inaccessible_StartingUpPass1);

        ContainerServices cont = array.getContainerServices();
        Resource<?> resource = null;
        
        ResourceManager resMng = ResourceManager.getInstance(cont);
                
        // Add ARCHIVE_CONNECTION resource.
        resource = new StaticResource<ArchiveConnection>(cont, "ARCHIVE_CONNECTION", 
                                                         "alma.xmlstore.ArchiveConnectionHelper", true);
        resMng.addResource(resource);
        // Add ARCHIVE_IDENTIFIER resource.
        resource = new StaticResource<Identifier>(cont, "ARCHIVE_IDENTIFIER",
                                                  "alma.xmlstore.IdentifierHelper", true);
        resMng.addResource(resource);

        // Add the Antenna resources.
        String[] antennas = array.getAntennas();
        for(int i=0; i<antennas.length; i++) {
            StaticResource<Antenna> ar = null;
            ar = new StaticResource<Antenna>(cont, "CONTROL/" + antennas[i],
                                             "alma.Control.AntennaHelper");
            resMng.addResource(ar);
        }
        // Add the ScriptExecutor dynamic resource.
        String scriptExecName = array.getScriptExecutorName();
        resource = new DynamicResource<ScriptExecutor>(cont, scriptExecName,
                                                        "IDL:alma/Control/ScriptExecutor:1.0", true);
        resMng.addResource(resource);
        // Add the ExecutionState dynamic resource.
        String execStateName = array.createExecutionStateName();
        DynamicResource<ExecutionState> esres = 
            new DynamicResource<ExecutionState>(cont, execStateName,
                                                "IDL:alma/Control/ExecutionState:1.0", true) {
            @Override
            public void startup() {
                getComponent().setParentName(array.getArrayComponentName());
                getComponent().startupPass1();
                getComponent().startupPass2();
                setState(ResourceState.OPERATIONAL);
            }
            
            @Override
            public void shutdown() {
                getComponent().shutdownPass1();
                getComponent().shutdownPass2();
                setState(ResourceState.INITIALIZED);
            }
            
        };        
        resMng.addResource(esres);
        
        //        AsyncResource2<ExecutionState> asyncres = 
        //            new AsyncResource2<ExecutionState>(esres, array.getContainerServices(), 200,
        //                    logger) {
        //            @Override
        //            public ResourceState startupAsync() throws Exception {
        //                getComponent().setParentName(array.name());
        //                getComponent().startupPass1();
        //                getComponent().startupPass2();
        //                return ResourceState.OPERATIONAL;
        //            }
        //            @Override
        //            public void shutdown() {
        //                getComponent().shutdownPass1();
        //                getComponent().shutdownPass2();
        //            }
        //            @Override
        //            public ResourceState status() {
        //                try {
        ////                    return getComponent().getExecState().toString();
        //                    return ResourceState.OPERATIONAL;
        //                } catch (AcsJNotYetAcquiredEx e) {
        //                    // TODO Auto-generated catch block
        //                    e.printStackTrace();
        //                    return ResourceState.UNINITIALIZED;
        //                }
        //            }
        //        };        
        //        resMng.addResource(asyncres);
                
        // Add the SimpleSupplier resource
        resource = new PublisherResource(cont, "CONTROL_SYSTEM", true);
        resMng.addResource(resource);
        
        // Acquire all resources.
        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            initEx.setProperty("Message", msg);
            logger.fine(msg);
            throw initEx;
        }

        // Set the Antenna components in the Antenna map. (Their values are originally
        // null.)
        Resource<Antenna> ar = null;
        Map<String, Antenna> antennaMap = array.getAntennaMap();
        for(int i=0; i<antennas.length; i++) {
            try {
                ar = resMng.getResource("CONTROL/" + antennas[i]);
            } catch (AcsJUnknownResourceEx ex) {
                // Should never get to this point.
                assert false: "Inconsistent antenna list.";
            } catch (AcsJBadResourceEx ex) {
                String msg = "Error setting up antennas. Antenna '"+antennas[i]+ "' has ";
                msg = msg + "not been retrieved properly.";
                AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
                initEx.setProperty("Message", msg);
                logger.fine(msg);
                throw initEx;                
            }
            try {
                antennaMap.put(antennas[i], ar.getComponent());
            } catch (AcsJNotYetAcquiredEx ex) {
                String msg = "Error setting up antennas. Antenna '"+antennas[i]+"' is ";
                msg = msg + "null in the ResourceManager.";
                AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
                initEx.setProperty("Message", msg);
                logger.fine(msg);
                throw initEx;                
            }
        }
        
        // Configure the ScriptExecutor
        //        String an = array.name();
        //        ArrayConf arrayConfig = new ArrayConf();
        //        arrayConfig.arrayName = an.replaceFirst("CONTROL/", "");
        //        arrayConfig.arrayCompName = an;
        //        arrayConfig.antennas = antennas;
        //
        //        ScriptExecConf scriptExecConf = new ScriptExecConf();
        //        scriptExecConf.temp = (short) 1;
        //        scriptExecConf.arrayConfig = arrayConfig;

        Resource<ScriptExecutor> scr = null;
        try {
            scr = resMng.getResource(scriptExecName);
        } catch (AcsJUnknownResourceEx e) {
            // Should never get to this point.
            assert false: "Internal error getting resource '"+scriptExecName+"'.";
        } catch (AcsJBadResourceEx ex) {
            String msg = "Error getting resource '"+scriptExecName+ "'.";
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            initEx.setProperty("Message", msg);
            logger.fine(msg);
            throw initEx;                
        }
        ScriptExecutor scexec = null;
        try {
            scexec = scr.getComponent();
        } catch (AcsJNotYetAcquiredEx ex) {
            String msg = "Error getting the ScriptExecutor component. ";
            msg = msg + "The resource has not yet been acquired.";
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            initEx.setProperty("Message", msg);
            logger.fine(msg);
            throw initEx;                
        }
        
        scexec.configure(array.getArrayName(), "<SchedBlock/>");
        
        // Wait for the ExecutionState to complete its initialization.
        // There's not very much to do regarding the state.
        // TODO This should be completed when the ExecutionState component gets
        // rewritten.
        //        MonitoredResource.ResourceChange state = asyncres.pollState();
        
        // Send the CreatedAutomaticArrayEvent event
        try {
            Resource<SimpleSupplier> pubres = resMng.getResource("CONTROL_SYSTEM");
            SimpleSupplier publisher = pubres.getComponent();
            CreatedAutomaticArrayEvent ev = new CreatedAutomaticArrayEvent();
            ev.arrayName = array.getArrayName();
            ev.dataCaptureName = "?";
            ev.antennaList = array.getAntennas();
            ev.creationTime = Util.arrayTimeToACSTime(Util.getArrayTime().get());
            ev.correlatorArrayStream = new BDDStreamInfo();
            ev.correlatorArrayStream.distributor = array.getCorrelatorBulkDataDistributor();
            ev.correlatorArrayStream.spectralFlow = CorrelatorBDDFlowNumbers.getSpectralFlowNumber();
            ev.correlatorArrayStream.channelAvgFlow = CorrelatorBDDFlowNumbers.getChannelAverageFlowNumber();
            ev.correlatorArrayStream.wvrFlow = CorrelatorBDDFlowNumbers.getWVRFlowNumber();
            ev.totalPowerArrayStream = new BDDStreamInfo();
            ev.totalPowerArrayStream.distributor = array.getTotalPowerBulkDataDistributor();
            ev.totalPowerArrayStream.spectralFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            ev.totalPowerArrayStream.channelAvgFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            ev.totalPowerArrayStream.wvrFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            logger.finer("Sending CreateAutomaticArrayEvent; correlatorBulkDataDistributor=" +
            		ev.correlatorArrayStream.distributor + "; totalPowerBulkDataDistributor=" +
            		ev.totalPowerArrayStream.distributor);
            try {
                publisher.publishEvent(ev);
            } catch (AcsJException e) {
                e.log(logger);
            }
        } catch (AcsJUnknownResourceEx ex) {
            String msg = "Error getting notification channel publisher resource. " +
                "This is an internal error";
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            initEx.setProperty("Message", msg);
            logger.fine(msg);
            throw initEx;            
        } catch (AcsJBadResourceEx ex) {
            String msg = "Error getting notification channel publisher resource. " +
                "This is an internal error";
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            initEx.setProperty("Message", msg);
            logger.fine(msg);
            throw initEx;            
        } catch (AcsJNotYetAcquiredEx ex) {
            String msg = "Error getting notification channel publisher resource. " +
                "This is an internal error";
            AcsJInitializationErrorEx initEx = new AcsJInitializationErrorEx(ex);
            initEx.setProperty("Message", msg);
            logger.fine(msg);
            throw initEx;            
        }
        
        setState(State.Inaccessible_StartedUpPass1);        
    }

    @Override
        public void startupPass2() throws AcsJInvalidRequestEx, AcsJInitializationErrorEx {
        array.setState("OPERATIONAL_STATE");
    }
}
