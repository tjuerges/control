package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.Common.State;

public class ManualArrayShutdownState extends ManualArrayStateBase 
    implements ManualArrayState {
    
    public ManualArrayShutdownState(ArrayInternal array) {
        super(array, State.Inaccessible_Stopped);
    }

    public void startupPass1() throws AcsJInvalidRequestEx {
        array.setState("STARTINGUP_STATE");
        array.startupPass1();
    }

}
