/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA.
 *
 * $Id$
 */

package alma.Control.Array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.ArrayExceptions.InvalidRequestEx;
import alma.ArrayExceptions.UnfinishedExecBlockEx;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.Control.AbortionException;
import alma.Control.Antenna;
import alma.Control.AntennaCharacteristics;
import alma.Control.AntennaShutterStatus;
import alma.Control.AntennaStateEvent;
import alma.Control.AntennaStatus;
import alma.Control.AtmosphericConditions;
import alma.Control.CalDeviceData;
import alma.Control.Completion;
import alma.Control.CorrelatorStatus;
import alma.Control.CorrelatorType;
import alma.Control.ErrorEnum;
import alma.Control.ExecBlockStatus;
import alma.Control.ExecutionException;
import alma.Control.IDLError;
import alma.Control.IDLResult;
import alma.Control.InaccessibleException;
import alma.Control.InvalidRequest;
import alma.Control.LocalOscillatorStatus;
import alma.Control.ManualArrayOperations;
import alma.Control.OpticalPointingObservingMode;
import alma.Control.PointingStatus;
import alma.Control.ResourceEnum;
import alma.Control.ResourceId;
import alma.Control.ScanStatus;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.SiteData;
import alma.Control.SubscanStatus;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.TotalPowerObservingMode;
import alma.Control.TowerHolographyObservingMode;
import alma.Control.TowerHolography7mObservingMode;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.Common.Name;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.Util;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ReceiverBandMod.ReceiverBand;
import alma.SchedulingExceptions.InvalidOperationEx;
import alma.TMCDB.ModelTerm;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.offline.ASDMArchivedEvent;
import alma.scheduling.Array;
import alma.scheduling.ArrayHelper;

public class ManualArrayImpl2 
    implements ManualArrayOperations, ComponentLifecycle, ArrayInternal 
{
    private static final String SCHED_BLOCK = "SchedBlock";
    private static final String PROJECT_STATUS = "ProjectStatus";
    private static final String CONTROL_COMPONENT_PREFIX = "CONTROL/";
    private static final String SCHEDULING_COMPONENT_PREFIX = "SCHEDULING/";
    
    public ManualArrayState SHUTDOWN_STATE;
    public ManualArrayState STARTINGUP_STATE;
    public ManualArrayState OPERATIONAL_STATE;   
    public ManualArrayState SHUTTINGDOWN_STATE;   
    
    private ContainerServices container;
    private AntennaCharacteristics[] antennaCharacteristics;
    private WeatherStationCharacteristics[] weatherStationCharacteristics;
    private SiteData siteData;
    private String configurationName;
    
    // private String name; // Not sure for what this is used.
    private String parentName;
    private String name;
    private int dataCapturerNumber;
    private Map<String, Antenna> antennas;    
    private ResourceManager resMng;
    private Logger logger;
    private ManualArrayState state;

    private String dataCaptureName;
    private String scriptExecutorName;
    private String execStateName;

    private String correlatorBulkDataDistributor;
    private String totalPowerBulkDataDistributor;
    private ResourceId[] photonicReferences;
    private CorrelatorType correlatorType;
    
    private IDLEntityRef schedBlockEntRef;

    /**
     * Constructor.
     *
     */
    public ManualArrayImpl2() {
        this.antennas = new HashMap<String, Antenna>();
        // this.name = "";            // TODO: check what should go here, and who
                                      // needs this property.
    }
    
    ////////////////////////////////////////////////////////////////////
    // ComponentLifecycle operations
    ////////////////////////////////////////////////////////////////////

    public void initialize(ContainerServices containerServices) 
        throws ComponentLifecycleException {
        
        this.container = containerServices;
        this.resMng = ResourceManager.getInstance(container);
        this.logger = container.getLogger();
        
        SHUTDOWN_STATE = new ManualArrayShutdownState(this);
        STARTINGUP_STATE = new ManualArrayStartingUpState(this);
        OPERATIONAL_STATE = new ManualArrayOperationalState(this);   
        SHUTTINGDOWN_STATE = new ManualArrayShuttingDownState(this);   
        this.state = SHUTDOWN_STATE;
        
        this.dataCapturerNumber = 0;
        this.dataCaptureName = String.format("%s/DC%03d", name(), dataCapturerNumber++);
        this.scriptExecutorName = name() + "/SCRIPTEXEC";
        this.name = name().replaceFirst("CONTROL/", "");

        logger.fine("initialize called");
    }
    
    public void aboutToAbort() {
        logger.fine("aboutToAbort called");
    }

    public void cleanUp() {
        logger.fine("cleanUp called");
    }

    public void execute() throws ComponentLifecycleException {
        logger.fine("execute called");
    }

    ////////////////////////////////////////////////////////////////////
    // ACSComponentOperations operations
    ////////////////////////////////////////////////////////////////////

    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }

    public String name() {
        return container.getName();
    }
    
    ////////////////////////////////////////////////////////////////////
    // ControllerOperations operations
    ////////////////////////////////////////////////////////////////////    
    
    public IDLError[] getBadResourcesIDL() {
        String[] errors = resMng.getErroneousResources();
        IDLError[] idlErrors = new IDLError[errors.length];
        for(int i=0; i<errors.length; i++) {
            // (RHV) I don't quite like to keep enumerations of all possible
            // resources and all possible errors. I think it is preferable to
            // just retrieve a polymorphic list of exceptions.
            // In order to maintain the IDL interface, I'm doing sort of a
            // conversion here, but I hope to modify this later.
            idlErrors[i] = new IDLError(errors[i],
                                        ResourceEnum.MANUAL_ARRAY,
                                        ErrorEnum.INTERNAL_ERROR,
                                        0L,
                                        "");
        }
        return idlErrors;
    }

    public String getComponentName() {
        return name();
    }

    public int getResourceTypeValue() {
        return 0;
    }

    public int getStateValue() {
        return state.getStateValue();
    }

    public int getSubstateValue() {
        return state.getSubstateValue();
    }

    public IDLResult reportRemoteError(String resourceName,
            ResourceEnum resourceType, ErrorEnum errorType, long time,
            String message) {
        return null;
    }

    public void reportRemoteStateChange(String resourceName,
            ResourceEnum resourceType, SystemState newState,
            SystemSubstate newSubstate) {
    }

    public void setParentName(String name) {
        this.parentName = name;
    }

    public void shutdownPass1() {
        try {
            state.shutdownPass1();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);            
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void shutdownPass2() {
        try {
            state.shutdownPass2();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);            
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void startupPass1() {
        try {
            state.startupPass1();
        } catch (AcsJInitializationErrorEx ex) {
            logger.severe("Initialization error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInitializationErrorEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void startupPass2() {
        try {
            state.startupPass2();
        } catch (AcsJInitializationErrorEx ex) {
            logger.severe("Initialization error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInitializationErrorEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void waitShutdownPass1() {
        try {
            state.waitShutdownPass1();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                    Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    ////////////////////////////////////////////////////////////////////
    // ManualArrayOperations operations
    ////////////////////////////////////////////////////////////////////    

    public void configure(IDLEntityRef sb)
        throws InaccessibleException, InvalidRequest {
        logger.info("Configured manual array with sched block " + sb.entityId);
        schedBlockEntRef = sb;
    }
    
    public void setBulkDataDistributors(String corrBulkDataDistr, String totalPowerBulkDataDistr) {
        correlatorBulkDataDistributor = corrBulkDataDistr;
        totalPowerBulkDataDistributor = totalPowerBulkDataDistr;
    }

    public String getExecutor() {
        return scriptExecutorName;
    }

    public void setAntennaList(String[] list) {
        for(int i=0; i<list.length; i++)
            antennas.put(list[i], null);
    }

    public void setConfigInfo(String configurationName,
            AntennaCharacteristics[] antennaCharacs,
            WeatherStationCharacteristics[] weatherStCharacs,
            SiteData siteData,
            ResourceId[] photonicReferences,
            CorrelatorType correlatorType) {
        this.antennaCharacteristics = antennaCharacs;
        this.weatherStationCharacteristics = weatherStCharacs;
        this.siteData = siteData;
        this.configurationName = configurationName;
        this.photonicReferences = photonicReferences;
        this.correlatorType = correlatorType;
    }

   public void beginExecution() throws ExecutionException, UnfinishedExecBlockEx
   {
       if (schedBlockEntRef == null) {
           String msg = "Scheduling Block entity reference is null. ";
           msg += "Manual arrays need to be configured using the SCHEDULING ";
           msg += "Array panel if an ASDM is going to be produced. ";
           logger.severe(msg);
           ExecutionException execEx =
               new ExecutionException(getName(),
                                      Util.arrayTimeToACSTime(Util.getArrayTime().get()),
                                      msg);
           throw execEx;
           
       }       
        
       String schedulingArrayName   = null;
       Object schedulingArrayObject = null;
       try {
            // lookup scheduler's array, starting with working out its name
            schedulingArrayName = getName();
            if (schedulingArrayName.startsWith(CONTROL_COMPONENT_PREFIX)) {
                schedulingArrayName = schedulingArrayName.substring(CONTROL_COMPONENT_PREFIX.length());
            }
            schedulingArrayName = SCHEDULING_COMPONENT_PREFIX + schedulingArrayName;
            schedulingArrayObject = container.getComponentNonSticky(schedulingArrayName);
       } catch (AcsJContainerServicesEx e) {
           String msg = "Error looking up the scheduling array component named: " + schedulingArrayName;
           logger.severe(msg);
           ExecutionException execEx = new ExecutionException(getName(),
                   Util.arrayTimeToACSTime(Util.getArrayTime().get()), 
                   e.toString());
           throw execEx;
       }
        
       IDLEntityRef sessionEntRef = null;
       if (schedulingArrayObject != null) {
           Array schedulingArray = ArrayHelper.narrow(schedulingArrayObject);
           try {
               sessionEntRef =
                   schedulingArray.startManualModeSession(schedBlockEntRef.entityId);
           } catch (InvalidOperationEx e) {
               String msg = "Scheduling Array returned invalid operation exception.";
               logger.severe(msg);
               ExecutionException execEx = new ExecutionException(getName(),
                       Util.arrayTimeToACSTime(Util.getArrayTime().get()), 
                       e.toString());
               throw execEx;
           }
          
           try {
               IDLEntityRef ref = state.getASDMUID();
               logger.finer("ASDM Entity ref: " + ref);
               state.beginExecution(ref, schedBlockEntRef, sessionEntRef);
            } catch (AcsJInvalidRequestEx ex) {
                logger.severe("Invalid request error:\n" + 
                              Util.getNiceErrorTraceString(ex.getErrorTrace()));
                ex.log(logger);
                ExecutionException execEx = new ExecutionException(getName(),
                           Util.arrayTimeToACSTime(Util.getArrayTime().get()), 
                           ex.toString());
                throw execEx;
            } catch (AcsJSBExecutionErrorEx ex) {
                logger.severe("SB execution error:\n" + 
                              Util.getNiceErrorTraceString(ex.getErrorTrace()));
                ex.log(logger);
                ExecutionException execEx = new ExecutionException(getName(),
                           Util.arrayTimeToACSTime(Util.getArrayTime().get()), 
                           ex.toString());
                throw execEx;
            } catch (AcsJUnfinishedExecBlockEx ex) {
                throw ex.toUnfinishedExecBlockEx();
            }   
        }
   }
   
    public void endExecution(Completion statusCode, String message)
        throws ExecutionException {
        try {
            state.endExecution();
        } catch (AcsJInvalidRequestEx ex) {
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        } catch (AcsJSBExecutionErrorEx ex) {
            logger.severe("SB execution error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        }
    }

    public IDLEntityRef getASDMEntityRef() {
        IDLEntityRef eref = null;
        eref = state.getASDMEntRef();
        return eref;
    }
    
    public SingleFieldInterferometryObservingMode getSFIOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getSFIOffshoot();
    }

    public TowerHolographyObservingMode getTowerHolographyOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getTowerHolographyOffshoot();
    }

    public TowerHolography7mObservingMode getTowerHolography7mOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getTowerHolography7mOffshoot();
    }

    public OpticalPointingObservingMode getOpticalPointingOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getOpticalPointingOffshoot();
    }
    
    public TotalPowerObservingMode getTotalPowerOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getTotalPowerOffshoot();
    }
    
    public alma.Control.AstronomicalHolography getAstronomicalHolographyOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getAstronomicalHolographyOffshoot();
    }

    public String getStaffId() {
        return "StaffID"; // TODO Implement this.
    }

    public ResourceId[] getAntennaComponents() {
        ResourceId[] resIds = new ResourceId[antennas.size()];
        Iterator<String> iter = antennas.keySet().iterator();
        int i = 0;
        while (iter.hasNext()) {
            String an = iter.next();
            resIds[i] = new ResourceId();
            resIds[i].ResourceName = an;
            resIds[i].ComponentName = Name.ControlPrefix + an;
            i++;
        }
        return resIds;
    }

    public AntennaStateEvent[] getAntennaStates() {
        try {
            return state.getAntennaStates();
        } catch (InvalidRequestException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String[] getAntennas() {
        List<String> antennaNames = new ArrayList<String>();
        for (String an : antennas.keySet()) {
            antennaNames.add(an);
        }
        Collections.sort(antennaNames);
        int i = 0;
        for (String an : antennaNames) {
            logger.info("Antenna " + i + ": " + an);
            i++;
        }
        return antennaNames.toArray(new String[0]);
    }

    public String getArrayComponentName() {
        return name();
    }

    public String getArrayName() {
        return name;
    }

    public SystemState getArrayState() {
        return SystemState.from_int(state.getStateValue());
    }

    public SystemSubstate getArraySubstate() {
        return SystemSubstate.from_int(state.getSubstateValue());
    }

    public String[] getBadResources() {
        return resMng.getErroneousResources();
    }

    public String getName() {
        return name();
    }

    public boolean isAutomatic() {
        return false;
    }

    public boolean isBusy() {
        return false;
    }

    public boolean isManual() {
        return true;
    }

    ////////////////////////////////////////////////////////////////////
    // ManualArrayInternal operations
    ////////////////////////////////////////////////////////////////////    

    public CorrelatorType getCorrelatorType() {
    	return correlatorType;
    }
    
    public String createExecutionStateName() {
        execStateName = name() + "/EXECSTATE";
        return execStateName;
    }
    
    public String getExecutionStateName() {
        return execStateName;
    }
    
    public String getTotalPowerBulkDataDistributor() {
        return totalPowerBulkDataDistributor;
    }
    
    public String getCorrelatorBulkDataDistributor() {
        return correlatorBulkDataDistributor;
    }    
    
    public ResourceId[] getPhotonicReferences() {
        return photonicReferences;
    }

    public String createDataCaptureName() {
        dataCaptureName = String.format("%s/DC%03d", name(), dataCapturerNumber++);
        return dataCaptureName;
    }

    public String getScriptExecutorName() {
        return scriptExecutorName;
    }

    public void setState(String stateName) {
        if (stateName.equals("SHUTDOWN_STATE"))
            this.state = SHUTDOWN_STATE;
        else if (stateName.equals("STARTINGUP_STATE"))
            this.state = STARTINGUP_STATE;
        else if (stateName.equals("OPERATIONAL_STATE"))
            this.state = OPERATIONAL_STATE;
        else if (stateName.equals("SHUTTINGDOWN_STATE"))
            this.state = SHUTTINGDOWN_STATE;
    }

    public Logger getLogger() {
        return logger;
    }

    public ContainerServices getContainerServices() {
        return container;
    }

    public Map<String, Antenna> getAntennaMap() {
        return antennas;
    }

    public String getDataCapturerName() {
        return dataCaptureName;
    }

    public String getDCName() throws InaccessibleException {
        return dataCaptureName;
    }
    
    public AntennaCharacteristics[] getAntennaConfigurations() {
        return antennaCharacteristics;
    }
    
    public String getConfigurationName() {
        return configurationName;
    }    
    
    public SiteData getSiteData() {
        return siteData;
    }

    public WeatherStationCharacteristics[] getWeatherStationConfigurations() {
        return weatherStationCharacteristics;
    }

    public void reportPointingModel(String antennaName, ReceiverBand band,
            ModelTerm[] pointingModel, ModelTerm[] auxPointingModel) {
        state.reportPointingModel(antennaName, band, pointingModel,
                auxPointingModel);
    }

    public void reportFocusModel(String antennaName, ReceiverBand band,
            ModelTerm[] focusModel) {
        state.reportFocusModel(antennaName, band, focusModel);
    }

    public void reportSubreflectorPosition(String AntennaName,
            boolean focusTrackingEnabled, double x, double y, double z,
            double xOffset, double yOffset, double zOffset) {
        state.reportSubreflectorPosition(AntennaName, focusTrackingEnabled, x,
                y, z, xOffset, yOffset, zOffset);
    }

    public void reportCalDeviceData(CalDeviceData data) {
        state.reportCalDeviceData(data);
    }
    
    ////////////////////////////////////////////////////////////////////
    // ArrayCommand additions to support Array Status GUI
    ////////////////////////////////////////////////////////////////////

    public void closeAntennaShutters() {
        // TODO: build out stub.
    }
    
    public void moveAntennasToMaintenanceStow() {
        // TODO: build out stub.
    }

    public void moveAntennasToSurvivalStow() {
        // TODO: build out stub.
    }

    public void openAntennaShutters() {
        // TODO: build out stub.
    }

    public void setAntennaDrivesToStandby() {
        // TODO: build out stub.
    }

	@Override
	public void flagData(boolean startStop, long timeStamp,
			String componentName, String description) {
		state.flagData(startStop, timeStamp, componentName, description);
	}    

    @Override
    public boolean isScanStatusAvailable() {
        return state.isScanStatusAvailable();
    }

    @Override
    public ScanStatus getScanStatus() throws InvalidRequestEx {
        try {
            return state.getScanStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isAntennaShutterStatusAvailable() {
        return state.isAntennaShutterStatusAvailable();
    }
    
    @Override
    public AntennaShutterStatus getAntennaShutterStatus() throws InvalidRequestEx {
        try {
            return state.getAntennaShutterStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isAntennaStatusAvailable() {
        return state.isAntennaStatusAvailable();
    }

    @Override
    public AntennaStatus getAntennaStatus()  throws InvalidRequestEx {
        try {
            return state.getAntennaStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isAtmosphericConditionsAvailable() {
        return state.isAtmosphericConditionsAvailable();
    }

    @Override
    public AtmosphericConditions getAtmosphericConditions() throws InvalidRequestEx {
        try {
            return state.getAtmosphericConditions();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isCorrelatorStatusAvailable() {
        return state.isCorrelatorStatusAvailable();
    }

    @Override
    public CorrelatorStatus getCorrelatorStatus() throws InvalidRequestEx {
        try {
            return state.getCorrelatorStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isExecBlockStatusAvailable() {
        return state.isExecBlockStatusAvailable();
    }

    @Override
    public ExecBlockStatus getExecBlockStatus() throws InvalidRequestEx {
        try {
            return state.getExecBlockStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isLocalOscillatorStatusAvailable() {
        return state.isLocalOscillatorStatusAvailable();
    }

    @Override
    public LocalOscillatorStatus getLocalOscillatorStatus() throws InvalidRequestEx {
        try {
            return state.getLocalOscillatorStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isPointingStatusAvailable() {
        return state.isPointingStatusAvailable();
    }

    @Override
    public PointingStatus getPointingStatus() throws InvalidRequestEx {
        try {
            return state.getPointingStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isSubscanStatusAvailable() {
        return state.isSubscanStatusAvailable();
    }

    @Override
    public SubscanStatus getSubscanStatus() throws InvalidRequestEx {
        try {
            return state.getSubscanStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }
    
    ////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////

    public void asdmArchived(ASDMArchivedEvent event) {
        logger.finest("ASDM archived notification received");
        state.asdmArchived(event);
    }

    public void dataCaptureReady() {
        // TODO Auto-generated method stub
        
    }

    public void subScanDataReady() {
        logger.finest("Subscan data ready notification received");
        state.subScanDataReady();
    }
}

//
// __oOo__
