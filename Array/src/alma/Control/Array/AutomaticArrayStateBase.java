package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.Control.Common.State;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.offline.ASDMArchivedEvent;

public class AutomaticArrayStateBase extends ArrayStateBase 
    implements AutomaticArrayState {

    public AutomaticArrayStateBase(ArrayInternal array, State state) {
        super(array, state);
    }

    public void beginExecution()
        throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx, AcsJUnfinishedExecBlockEx {
        String msg = "beginExecution() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    public void endExecution() throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx {
        String msg = "endExecution() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;        
    }
    
    public IDLEntityRef getASDMEntityRef() {
        return null;
    }

    public IDLEntityRef getSchedBlockEntityRef() {
        return null;
    }

    public IDLEntityRef getSessionEntityRef() {
        return null;
    }
    
    public boolean isRunningSB() {
        return false;
    }

    public void observe(IDLEntityRef sbId, IDLEntityRef sessionId, long when)
        throws AcsJSBExecutionErrorEx {}

    public void observeNow(IDLEntityRef sbId, IDLEntityRef sessionId)
        throws AcsJSBExecutionErrorEx {}

    public void stop() {}
    
    public void stopNow() {}

    public void asdmArchived(ASDMArchivedEvent event) {}

    public void dataCaptureReady() {}

    public void subScanDataReady() {}

}
