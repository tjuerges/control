package alma.Control.Array;

public class InvalidRequestException extends Exception {
    
    private String state;
    
    public InvalidRequestException(String state) {
        this.state = state;
    }

    public InvalidRequestException(String state, String message) {
        super(message);
        this.state = state;
    }

    public InvalidRequestException(String state, Throwable cause) {
        super(cause);
        this.state = state;
    }

    public InvalidRequestException(String state, String message, Throwable cause) {
        super(message, cause);
        this.state = state;
    }    
}
