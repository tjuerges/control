package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.DestroyedManualArrayEvent;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.State;
import alma.Control.Common.Util;
import alma.ResourceExceptions.wrappers.AcsJBadResourceEx;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJUnknownResourceEx;
import alma.acs.exceptions.AcsJException;
import alma.acs.nc.SimpleSupplier;

public class ManualArrayShuttingDownState extends ManualArrayStateBase
        implements ArrayState {

    public ManualArrayShuttingDownState(ArrayInternal array) {
        super(array, State.Inaccessible_ShuttingDownPass1);
    }

    @Override
    public void shutdownPass1() throws AcsJInvalidRequestEx {
        ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
        
        // Send the DestroyedManualArrayEvent event
        try {
            Resource<SimpleSupplier> pubres = resMng.getResource("CONTROL_SYSTEM");
            SimpleSupplier publisher = pubres.getComponent();
            DestroyedManualArrayEvent ev = new DestroyedManualArrayEvent();
            ev.arrayName = array.getArrayName();
            ev.destructionTime = Util.arrayTimeToACSTime(Util.getArrayTime().get());
            try {
                publisher.publishEvent(ev);
            } catch (AcsJException e) {
                e.log(logger);
            }
        } catch (AcsJUnknownResourceEx ex) {
            String msg = "Error getting notification channel publisher resource. " +
                "This is an internal error";
            AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx(ex);
            invEx.setProperty("Message", msg);
            logger.fine(msg);
            throw invEx;            
        } catch (AcsJBadResourceEx ex) {
            String msg = "Error getting notification channel publisher resource. " +
                "This is an internal error";
            AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx(ex);
            invEx.setProperty("Message", msg);
            logger.fine(msg);
            throw invEx;            
        } catch (AcsJNotYetAcquiredEx ex) {
            String msg = "Error getting notification channel publisher resource. " +
                "This is an internal error";
            AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx(ex);
            invEx.setProperty("Message", msg);
            logger.fine(msg);
            throw invEx;            
        }
        
        resMng.releaseResources();
        resMng.freeAllResources();
        setState(State.Inaccessible_ShuttingDownPass2);
    }

    @Override
    public void shutdownPass2() throws AcsJInvalidRequestEx {
        array.setState("SHUTDOWN_STATE");
    }
    
    
}
