package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.offline.ASDMArchivedEvent;

public interface AutomaticArrayState extends ArrayState {

    IDLEntityRef getASDMEntityRef();
    IDLEntityRef getSchedBlockEntityRef();
    IDLEntityRef getSessionEntityRef();
    boolean isRunningSB();
    void observe(IDLEntityRef sbId, IDLEntityRef sessionId, long when) throws AcsJSBExecutionErrorEx;
    void observeNow(IDLEntityRef sbId, IDLEntityRef sessionId) throws AcsJSBExecutionErrorEx;
    void stop();
    void stopNow();
    void dataCaptureReady();
    void asdmArchived(ASDMArchivedEvent event);
    void subScanDataReady();
    void beginExecution() throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx, AcsJUnfinishedExecBlockEx;
    void endExecution() throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx;

}
