package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.Control.Common.State;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.offline.ASDMArchivedEvent;

public class ManualArrayStateBase extends ArrayStateBase implements ManualArrayState {
    
    protected IDLEntityRef asdmEntRef;

    public ManualArrayStateBase(ArrayInternal array, State state) {
        super(array, state);
    }

    public void beginExecution(IDLEntityRef asdmEntRef, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef) 
    	throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx, AcsJUnfinishedExecBlockEx
    {
    	String msg = "beginExecution() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }
    
    public void endExecution()
        throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx {
        
        String msg = "endExecution() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;        
    }

    @Override
    public IDLEntityRef getASDMEntRef() {
        return asdmEntRef;
    }

    public void dataCaptureReady() {}
    
    public void asdmArchived(ASDMArchivedEvent event) {}
    
    public void subScanDataReady() {}

}
