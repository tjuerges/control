package alma.Control.Array;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.Control.BDDStreamInfo;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.State;
import alma.Control.Common.Util;
import alma.DataCaptureExceptions.SettingDataCapturerIdErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.offline.ASDMArchivedEvent;
import alma.offline.DataCapturer;
import alma.offline.DataCapturerId;

public class ManualArrayOperationalState extends ManualArrayStateBase implements
        ManualArrayState {

    private final long ASDM_ARCHIVED_TIMEOUT = 600000;
    private Lock dcLock = new ReentrantLock();
    private Condition dcCV = dcLock.newCondition();
    private boolean asdmArchived;
    private IDLEntityRef sbEntRef;
    private IDLEntityRef sessionEntRef;
    private String currentDataCapturerName;
    
    @Override
    public void asdmArchived(ASDMArchivedEvent event) {
        logger.finest("asdmArchived called...");
        try {
            logger.finer("Received ASDM archived notification.");
            dcLock.lock();
            asdmArchived = true;
            dcCV.signal();
        } finally {
            dcLock.unlock();
        }        
    }

    @Override
    public void dataCaptureReady() {
        logger.finest("dataCaptureReady called...");
    }

    @Override
    public void subScanDataReady() {
        logger.finest("subScanDataReady called...");
    }

    
    public ManualArrayOperationalState(ArrayInternal array) {
        super(array, State.Operational_NoError);
    }
    
    @Override
    public void beginExecution(IDLEntityRef asdmEntRef, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef) 
    	throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx, AcsJUnfinishedExecBlockEx
    {
        if (isInsideExecBlock()) {
            String msg = "Previous execution block hasn't been properly finished. ";
            msg += "Previous beginExecution() command hasn't been matched with ";
            msg += "a corresponding endExecution() command.";
            AcsJUnfinishedExecBlockEx ex = new AcsJUnfinishedExecBlockEx();
            ex.setProperty("Details", msg);
            logger.warning(msg);
            throw ex;
        }
        setInsideExecBlock(true);
        
        this.asdmEntRef = asdmEntRef;
        this.sbEntRef = sbEntRef;
        this.sessionEntRef = sessionEntRef;
        this.asdmArchived = false;
        
        // Create DataCapturer
        ContainerServices container = array.getContainerServices();
        currentDataCapturerName = array.createDataCaptureName();
        ResourceManager.getInstance(container).addResource(
                new DynamicResource<DataCapturer>(container, currentDataCapturerName,
                        "IDL:alma/offline/DataCapturer:1.0"));
        try {
            ResourceManager.getInstance(container).acquireResource(currentDataCapturerName);
        } catch (AcsJResourceErrorEx ex) {
            AcsJSBExecutionErrorEx execEx = new AcsJSBExecutionErrorEx(ex);
            throw execEx;
        }
        
        try {
            DataCapturer dc;
            dc = ResourceManager.getInstance(container).getComponent(currentDataCapturerName);
            DataCapturerId infoDataCapturer = new DataCapturerId();
            infoDataCapturer.array = array.getArrayName();
            infoDataCapturer.name = currentDataCapturerName;
            infoDataCapturer.session = sessionEntRef;
            infoDataCapturer.schedBlock = sbEntRef;
            infoDataCapturer.correlatorArrayStream = new BDDStreamInfo();
            infoDataCapturer.correlatorArrayStream.distributor = array.getCorrelatorBulkDataDistributor();
            infoDataCapturer.correlatorArrayStream.spectralFlow = CorrelatorBDDFlowNumbers.getSpectralFlowNumber();
            infoDataCapturer.correlatorArrayStream.channelAvgFlow = CorrelatorBDDFlowNumbers.getChannelAverageFlowNumber();
            infoDataCapturer.correlatorArrayStream.wvrFlow = CorrelatorBDDFlowNumbers.getWVRFlowNumber();
            infoDataCapturer.totalPowerArrayStream = new BDDStreamInfo();
            infoDataCapturer.totalPowerArrayStream.distributor = array.getTotalPowerBulkDataDistributor();
            infoDataCapturer.totalPowerArrayStream.spectralFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            infoDataCapturer.totalPowerArrayStream.channelAvgFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            infoDataCapturer.totalPowerArrayStream.wvrFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            logger.info("corr bdd = " + infoDataCapturer.correlatorArrayStream.distributor);
            logger.info("tp bdd = " + infoDataCapturer.totalPowerArrayStream.distributor);
            dc.setDataCapturerId(infoDataCapturer);
        } catch (AcsJResourceExceptionsEx ex) {
            // This should not happen, unless there's a bug in the ResourceManager.
            AcsJSBExecutionErrorEx execEx = new AcsJSBExecutionErrorEx(ex);
            throw execEx;
        } catch (SettingDataCapturerIdErrorEx ex) {
            AcsJSBExecutionErrorEx execEx = new AcsJSBExecutionErrorEx(ex.errorTrace);
            throw execEx;
        } catch (org.omg.CORBA.SystemException ex) {
            AcsJSBExecutionErrorEx execEx = new AcsJSBExecutionErrorEx(ex);
            throw execEx;
        }
        
    	SchedBlock sb = getSBFromArchive(sbEntRef.entityId);
        
    	beginExecution(asdmEntRef, sbEntRef, sessionEntRef, sb);
    }
    
    @Override
    public void endExecution() throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx {

        if (!isInsideExecBlock()) {
            String msg = "No matching beginExecution has been commanded.";
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Details", msg);
            logger.warning(msg);
            throw ex;
        }
        
        endExecution(asdmEntRef, sbEntRef, sessionEntRef);
        
        // Block until the ASDM has been archived.
        logger.info("Waiting for ASDM to be archived...");
        
        try {
            dcLock.lock();
            boolean ready;
            while (!asdmArchived) {
                try {
                    ready = dcCV.await(ASDM_ARCHIVED_TIMEOUT, TimeUnit.MILLISECONDS);
                } catch (InterruptedException ex) {
                    String msg = "Thread waiting for ASDM to be archived has been " +
                                 "interrupted. This is an internal error.";
                    AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                    exHlp.setProperty("Message", msg);
                    logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                    exHlp.log(logger);
                    return;                    
                }
                if (!ready) {
                    logger.severe("ASDMArchived event hasn't been received " +
                            "but timeout (" + ASDM_ARCHIVED_TIMEOUT / 1000 + "s) " +
                            "has been reached.");
                    break;
                } else
                    logger.info("The ASDM has been archived.");                        
            }                
        } finally {
            dcLock.unlock();
        }

        // Release DataCapturer
        ContainerServices container = array.getContainerServices();
        ResourceManager.getInstance(container).releaseResource(currentDataCapturerName);
        currentDataCapturerName = null;
        setInsideExecBlock(false);
    }
    
    @Override
    public void shutdownPass1() throws AcsJInvalidRequestEx {
        deactivateOffshoots();
        array.setState("SHUTTINGDOWN_STATE");
        array.shutdownPass1();
    }
    
    public IDLEntityRef getASDMEntityRef() {
        logger.finer("Manual mode getASDMEntityRef called...");
        return this.asdmEntRef;
    }
}
