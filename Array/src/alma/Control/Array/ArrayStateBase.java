/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import alma.ACSErr.Completion;
import alma.ACSErr.ErrorTrace;
import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.BasebandNameMod.BasebandName;
import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.Control.AbortionException;
import alma.Control.AntennaCharacteristics;
import alma.Control.AntennaShutterStatus;
import alma.Control.AntennaStateEvent;
import alma.Control.AntennaStatus;
import alma.Control.ArrayMountController;
import alma.Control.AstronomicalHolography;
import alma.Control.AstronomicalHolographyPOATie;
import alma.Control.AtmosphericConditions;
import alma.Control.BDDStreamInfo;
import alma.Control.CalDeviceData;
import alma.Control.CorrelatorStatus;
import alma.Control.CorrelatorType;
import alma.Control.ExecBlockEndedEvent;
import alma.Control.ExecBlockStartedEvent;
import alma.Control.ExecBlockStatus;
import alma.Control.LocalOscillator;
import alma.Control.LocalOscillatorStatus;
import alma.Control.MountController;
import alma.Control.MountControllerHelper;
import alma.Control.OpticalPointing;
import alma.Control.OpticalPointingHelper;
import alma.Control.OpticalPointingObservingMode;
import alma.Control.OpticalPointingObservingModeHelper;
import alma.Control.OpticalPointingObservingModePOATie;
import alma.Control.PointingStatus;
import alma.Control.ResourceId;
import alma.Control.ScanIntentData;
import alma.Control.ScanStatus;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.SingleFieldInterferometryObservingModeHelper;
import alma.Control.SingleFieldInterferometryObservingModePOATie;
import alma.Control.SubscanStatus;
import alma.Control.TotalPowerObservingMode;
import alma.Control.TotalPowerObservingModeHelper;
import alma.Control.TotalPowerObservingModePOATie;
import alma.Control.TowerHolography;
import alma.Control.TowerHolographyHelper;
import alma.Control.TowerHolographyObservingMode;
import alma.Control.TowerHolographyObservingModeHelper;
import alma.Control.TowerHolographyObservingModePOATie;
import alma.Control.TowerHolography7m;
import alma.Control.TowerHolography7mHelper;
import alma.Control.TowerHolography7mObservingMode;
import alma.Control.TowerHolography7mObservingModeHelper;
import alma.Control.TowerHolography7mObservingModePOATie;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.State;
import alma.Control.Common.Util;
import alma.Control.MountControllerPackage.PointingData;
import alma.Control.ObservingModes.AstronomicalHolographyImpl;
import alma.Control.ObservingModes.ObservingModeArray;
import alma.Control.ObservingModes.ObservingModeBaseImpl;
import alma.Control.ObservingModes.OpticalPointingObservingModeImpl;
import alma.Control.ObservingModes.SingleFieldInterferometryObservingModeImpl;
import alma.Control.ObservingModes.TotalPowerObservingModeImpl;
import alma.Control.ObservingModes.TowerHolographyObservingModeImpl;
import alma.Control.ObservingModes.TowerHolography7mObservingModeImpl;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.DataCaptureExceptions.wrappers.AcsJDataErrorEx;
import alma.DataCaptureExceptions.wrappers.AcsJTableUpdateErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.MountFaultEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.ObservingModeExceptions.AntennasDisagreeEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.PolarizationTypeMod.PolarizationType;
import alma.ReceiverBandMod.ReceiverBand;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.TMCDB.ModelTerm;
import alma.acs.container.ContainerServices;
import alma.acs.entityutil.EntityDeserializer;
import alma.acs.entityutil.EntityException;
import alma.acs.entityutil.EntitySerializer;
import alma.acs.exceptions.AcsJException;
import alma.acs.logging.AcsLogger;
import alma.acs.logging.RepeatGuardLogger;
import alma.acs.nc.SimpleSupplier;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.offline.DataCapturer;
import alma.offline.FlagData;
import alma.offline.FocusData;
import alma.xmlentity.XmlEntityStruct;
import alma.xmlstore.ArchiveConnection;
import alma.xmlstore.Identifier;
import alma.xmlstore.Operational;
import alma.xmlstore.IdentifierPackage.NotAvailable;

public class ArrayStateBase implements ArrayState, ObservingModeArray {

    protected Logger logger;
    private RepeatGuardLogger cantTalkToDataCapturerRepeatGuard;
    private int corrConfigID = 0;
    private int scan = 0;
    private int subScan = 0;
    
    /**
     * Is the array inside an execution block?
     * 
     * Being "inside an execution block" means that beginExecution() has been
     * called, and endExecution() hasn't been called yet. These two functions
     * demark the process of acquiring data, to be finally stored in an ASDM.
     * ASDM and Execution Block are sometimes used interchangely, although
     * (it seems) ASDM refers to the final product of an Execution Block.
     * 
     * We use this flag to detect when beginExecution() is being called without
     * finishing the previous execution block with endExecution().
     */
    private boolean insideExecBlock;
    
    protected boolean execBlockStartedEventSent;
    protected boolean execBlockEndedEventSent;    
    protected State extState;
    protected ArrayInternal array;    
    protected SingleFieldInterferometryObservingModeImpl sfiObsMode;
    protected SingleFieldInterferometryObservingModePOATie sfiObsModeTie;
    protected SingleFieldInterferometryObservingMode sfiOffshoot;
    protected TowerHolographyObservingModePOATie thObsModeTie;
    protected TowerHolographyObservingMode thOffshoot;
    protected TowerHolography7mObservingModePOATie th7mObsModeTie;
    protected TowerHolography7mObservingMode th7mOffshoot;
    protected OpticalPointingObservingModePOATie opObsModeTie;
    protected OpticalPointingObservingMode opOffshoot;
    protected TotalPowerObservingModeImpl tpObsMode;
    protected TotalPowerObservingModePOATie tpObsModeTie;
    protected TotalPowerObservingMode tpOffshoot;
    protected AstronomicalHolographyPOATie ahObsModeTie;
    protected AstronomicalHolography ahOffshoot;

    /**
     * This flag is used to prevent that a SB that is stopped before any
     * offshoot has been created just run to completion. In this case the SB
     * will be stopped when it tries to get any offshoot.
     */
    private boolean hasBeenStopped;
    
    /** This flag is used to prevent that the SB is executed when it has been
     * aborted during the time that spans from the start of the SBExecutor thread
     * to the runSource2 call in the ScriptExecutor component. */
    private boolean hasBeenAborted;

    // Status variables
    
    // Scan Status
    private ScanIntentData[] scanIntentData;
    private long scanStartTime;
    // Subscan Status
    private long subScanDuration;
    // Correlator Status
    private boolean usesCorr;
    private String apState;
    private short corrMode;
    // ExecBlock Status
    private long execBlockAllowedTime;
    private String execBlockUid;
    private String schedBlockUid;
    private String piName;
    private String projectName;
    private String script;
    private long execBlockStartTime;
    
    ////////////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////////////
    
    public ArrayStateBase(ArrayInternal array, State state) {
        this.extState = state;
        this.array = array;
        this.logger = array.getLogger();

	AcsLogger acsLogger = AcsLogger.fromJdkLogger(logger, null);
	this.cantTalkToDataCapturerRepeatGuard =
	    RepeatGuardLogger.createCounterBasedRepeatGuardLogger(acsLogger, 1);
    }

    ////////////////////////////////////////////////////////////////////
    // Public interface
    ////////////////////////////////////////////////////////////////////    

    public int getStateValue() {
        return extState.getStateValue();
    }

    public int getSubstateValue() {
        return extState.getSubstateValue();
    }

    public void shutdownPass1() throws AcsJInvalidRequestEx {
        String msg = "shutdownPass1() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    public void shutdownPass2() throws AcsJInvalidRequestEx {
        String msg = "shutdownPass2() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    public void startupPass1() throws AcsJInitializationErrorEx, AcsJInvalidRequestEx {
        String msg = "startupPass1() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    public void startupPass2() throws AcsJInitializationErrorEx, AcsJInvalidRequestEx {
        String msg = "startupPass2() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    public void waitShutdownPass1() throws AcsJInvalidRequestEx {
        String msg = "waiShutdownPass1() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    public AntennaStateEvent[] getAntennaStates() throws InvalidRequestException {
        String msg = "getAntennaStates() cannot be executed when the state is "
            + extState.toString();
        throw new InvalidRequestException(extState.toString(), msg);
    }    

    ////////////////////////////////////////////////////////////////////
    // Offshoot interface
    ////////////////////////////////////////////////////////////////////    

    public SingleFieldInterferometryObservingMode getSFIOffshoot() 
        throws ObsModeInitErrorEx, AbortionException {
        
        if (sbHasBeenStopped()) {
            throw new AbortionException();
        }
        if (sfiOffshoot == null) {
            sfiObsMode = new SingleFieldInterferometryObservingModeImpl
                (array.getContainerServices(), this);
            sfiObsModeTie = 
                new SingleFieldInterferometryObservingModePOATie(sfiObsMode);
            try {
                sfiOffshoot = 
                    SingleFieldInterferometryObservingModeHelper
                    .narrow(array.getContainerServices().activateOffShoot(sfiObsModeTie));
            } catch (AcsJContainerServicesEx ex) {
                AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
                throw exhlp.toObsModeInitErrorEx();
            }
        }
        return sfiOffshoot;
    }

    public TowerHolographyObservingMode getTowerHolographyOffshoot()
        throws ObsModeInitErrorEx, AbortionException {

        if (sbHasBeenStopped()) {
            throw new AbortionException();        
        }
        if (thOffshoot == null) {
            TowerHolographyObservingModeImpl thObsMode = 
            	new TowerHolographyObservingModeImpl
                (array.getContainerServices(), this);
            thObsModeTie = new TowerHolographyObservingModePOATie(thObsMode);
            try {
                thOffshoot = 
                    TowerHolographyObservingModeHelper
                    .narrow(array.getContainerServices().activateOffShoot(thObsModeTie));
            } catch (AcsJContainerServicesEx ex) {
                AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
                throw exhlp.toObsModeInitErrorEx();
            }            
        }
        return thOffshoot;
    }

    public TowerHolography7mObservingMode getTowerHolography7mOffshoot()
        throws ObsModeInitErrorEx, AbortionException {

        if (sbHasBeenStopped()) {
            throw new AbortionException();        
        }
        if (th7mOffshoot == null) {
            TowerHolography7mObservingModeImpl th7mObsMode = 
            	new TowerHolography7mObservingModeImpl
                (array.getContainerServices(), this);
            th7mObsModeTie = new TowerHolography7mObservingModePOATie(th7mObsMode);
            try {
                th7mOffshoot = 
                    TowerHolography7mObservingModeHelper
                    .narrow(array.getContainerServices().activateOffShoot(th7mObsModeTie));
            } catch (AcsJContainerServicesEx ex) {
                AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
                throw exhlp.toObsModeInitErrorEx();
            }            
        }
        return th7mOffshoot;
    }

    public OpticalPointingObservingMode getOpticalPointingOffshoot()
        throws ObsModeInitErrorEx, AbortionException {

        if (sbHasBeenStopped()) {
            throw new AbortionException();
        }
        if (opOffshoot == null) {
            OpticalPointingObservingModeImpl opObsMode = 
            	new OpticalPointingObservingModeImpl
                (array.getContainerServices(), this);
            opObsModeTie = new OpticalPointingObservingModePOATie(opObsMode);
            try {
                opOffshoot = 
                    OpticalPointingObservingModeHelper
                    .narrow(array.getContainerServices().activateOffShoot(opObsModeTie));
            } catch (AcsJContainerServicesEx ex) {
                AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
                throw exhlp.toObsModeInitErrorEx();
            }            
        }
        return opOffshoot;
    }

    public TotalPowerObservingMode getTotalPowerOffshoot()
    	throws ObsModeInitErrorEx, AbortionException {
    	
        if (sbHasBeenStopped()) {
            throw new AbortionException();
        }
        if (tpOffshoot == null) {
            tpObsMode = new TotalPowerObservingModeImpl
                (array.getContainerServices(), this,
                 array.getTotalPowerBulkDataDistributor());
            tpObsModeTie = new TotalPowerObservingModePOATie(tpObsMode);
            try {
                tpOffshoot = 
                    TotalPowerObservingModeHelper
                    .narrow(array.getContainerServices().activateOffShoot(tpObsModeTie));
            } catch (AcsJContainerServicesEx ex) {
                AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
                throw exhlp.toObsModeInitErrorEx();
            }            }
        return tpOffshoot;
    }
           
    public AstronomicalHolography getAstronomicalHolographyOffshoot()
    	throws ObsModeInitErrorEx, AbortionException {
    	
        if (sbHasBeenStopped()) {
            throw new AbortionException();
        }
        if (ahOffshoot == null) {
            AstronomicalHolographyImpl ahObsMode = new AstronomicalHolographyImpl
                (array.getContainerServices(), this);
            ahObsModeTie = new AstronomicalHolographyPOATie(ahObsMode);
            try {
                ahOffshoot = 
                    alma.Control.AstronomicalHolographyHelper
                    .narrow(array.getContainerServices().activateOffShoot(ahObsModeTie));
            } catch (AcsJContainerServicesEx ex) {
                AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
                throw exhlp.toObsModeInitErrorEx();
            }            }
        return ahOffshoot;
    }

    ////////////////////////////////////////////////////////////////////
    // ObservingModeArray methods
    ////////////////////////////////////////////////////////////////////    

    public IDLEntityRef getASDMEntRef() {
        logger.finer("ArrayStateBase getASDMEntRef called...");
        return null;
    }

    public String[] getAntennas() {
        return array.getAntennas();
    }

    public AntennaCharacteristics[] getAntennaConfigurations() {
        return array.getAntennaConfigurations();
    }

    public String getConfigurationName() {
        return array.getConfigurationName();
    }
    
    public int getCorrelatorConfigID() {
        return corrConfigID;
    }

    public String getDataCapturerName() {
        return array.getDataCapturerName();
    }

    public int getScanNumber() {
        return scan;
    }

    public int getSubScanNumber() {
        return subScan;
    }

    public void incrCorrelatorConfigID() {
        corrConfigID++;
    }

    public void incrScanNumber() {
        scan++;
    }

    public void incrSubScanNumber() {
        subScan++;
    }

    public String getArrayName() {
        return array.getArrayName();
    }

    public String getArrayComponentName() {
        return array.getArrayComponentName();
    }
    
    public void resetScanNumber() {
        scan = 0;
    }

    public void resetSubScanNumber() {
        subScan = 0;
    }
        
    ////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////    
    
    // ...
    
    ////////////////////////////////////////////////////////////////////
    // Protected methods
    ////////////////////////////////////////////////////////////////////    
    
    void setState(State state) {
        this.extState = state;
    }

    public IDLEntityRef getASDMUID() throws AcsJSBExecutionErrorEx {
        
        ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
        
        IDLEntityRef entRef = new IDLEntityRef();        
        try {
            Resource<Identifier> res = resMng.getResource("ARCHIVE_IDENTIFIER");
            Identifier archId = res.getComponent();
            
            String[] ids = archId.getUIDs((short) 1);
            // assert (ids == null || ids.length == 0); 
            entRef.entityId = ids[0];
            // TODO: Check if hardcode these values is right thing to do.
            entRef.partId = "X00000000";
            entRef.entityTypeName = "ASDM";
            entRef.instanceVersion = "1";

        } catch (AcsJResourceExceptionsEx ex) {    
            String msg = "Resource error when getting 'ARCHIVE_IDENTIFIER' component ";
            msg += "from ResourceManager.";
            logger.finer(msg + " Exception: " + ex.toString());
            AcsJSBExecutionErrorEx sbExecEx = new AcsJSBExecutionErrorEx(ex);
            sbExecEx.setProperty("Message", msg);
            throw sbExecEx;            
            
        } catch (NotAvailable ex) {   
            String msg = "There are no more UID available from the 'ARCHIVE_IDENTIFIER' component.";
            logger.finer(msg + " Exception: " + ex.toString());
            AcsJSBExecutionErrorEx sbExecEx = new AcsJSBExecutionErrorEx(ex);
            sbExecEx.setProperty("Message", msg);
            throw sbExecEx;   
        }
        
        return entRef;
    }
    
    protected void beginExecution(IDLEntityRef asdmEntRef, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef, SchedBlock sb) 
        throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx, AcsJUnfinishedExecBlockEx {
               
        ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
        DataCapturer dataCapturer = null;
        try {
            Resource<DataCapturer> dcres = resMng.getResource(array.getDataCapturerName());
            dataCapturer = dcres.getComponent();
        } catch(AcsJResourceExceptionsEx ex) {
            String msg = "Error getting OFFLINE/DataCapturer resource. "
                + "This resource should have been acquired during the array "
                + "initialization. This is an internal error.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp;
        }
        
        long now = Util.arrayTimeToACSTime(Util.getArrayTime().get());
        
        sendExecBlockStartedEvent(now, sbEntRef, sessionEntRef, asdmEntRef);
        
        resetScanNumber();
        resetSubScanNumber();
        execBlockEndedEventSent = false;
        setExecBlockUid(asdmEntRef.entityId);
        setPiName(sb.getPIName());
        setSchedBlockUid(sbEntRef.entityId);
        setProjectName(sb.getObsProjectRef().getEntityId()); // just for now
        setScript(sb.getObsProcedure().getObsProcScript());
        setExecBlockAllowedTime((long)sb.getObsUnitControl().getMaximumTime().getContent());
        setExecBlockStartTime(now);
        
        EntitySerializer entser = EntitySerializer.getEntitySerializer(logger);
        XmlEntityStruct sbEntity = null;
        try {
            sbEntity = entser.serializeEntity(sb, sb.getSchedBlockEntity());
        } catch (EntityException ex) {
            String msg = "Error serializing SchedBlock object.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp;
        }
        
        try {
            dataCapturer.startSB(array.getArrayName(), now);
            dataCapturer.setSBConfig(sbEntity, sessionEntRef, asdmEntRef,
                    array.getAntennaConfigurations(),
                    array.getWeatherStationConfigurations(),
                    array.getSiteData());
        } catch (TableUpdateErrorEx ex) {
            String msg = "Error starting SB execution in OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp;
        } catch (DataErrorEx ex) {
            String msg = "Error starting SB execution in OFFLINE/DataCapturer.";            
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp;
        }
    }

    public SchedBlock getSBFromArchive(String sbId) throws AcsJSBExecutionErrorEx  
	{        
		SchedBlock apdmSB = null;
		ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
		try {
			Resource<ArchiveConnection> res = resMng.getResource("ARCHIVE_CONNECTION");
			ArchiveConnection archConn = res.getComponent();
			Operational archOp = archConn.getOperational(array.getArrayComponentName());

			// Read the SB from the ARCHIVE.
			XmlEntityStruct xml = archOp.retrieveDirty(sbId);
			logger.info("SchedBlock " + sbId + " read from the archive.");

			// Deserialize the SB.
			EntityDeserializer ed = EntityDeserializer.getEntityDeserializer(logger);
			Class<?> schedBlockClass = Class.forName("alma.entity.xmlbinding.schedblock.SchedBlock"); 
			apdmSB = (SchedBlock) ed.deserializeEntity(xml, schedBlockClass);
			logger.info("SchedBlock " + sbId + " deserialized.");
		} catch(AcsJResourceExceptionsEx ex) {
			String msg = "Resource error when getting 'ARCHIVE_CONNECTION' component ";
			msg += "from ResourceManager.";
			logger.finer(msg + " Exception: " + ex.toString());
			AcsJSBExecutionErrorEx sbExecEx = new AcsJSBExecutionErrorEx(ex);
			sbExecEx.setProperty("Message", msg);
			throw sbExecEx;                        
		} catch(Exception ex) {
                    String msg = "Cannot retrieve the scheduling block (" + 
                            sbId + ") from the archive.";
			logger.finer(msg + " Exception: " + ex.toString());
			AcsJSBExecutionErrorEx sbExecEx = new AcsJSBExecutionErrorEx(ex);
			sbExecEx.setProperty("Message", msg);
			throw sbExecEx;
		}

		assert (apdmSB != null): "Deserialized SchedBlock is null.";
		return apdmSB;
	}
    
    protected void endExecution(IDLEntityRef asdmEntRef, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef)
    	throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx{
        Completion compl = new Completion(0, 0, 0, new alma.ACSErr.ErrorTrace[0]);
    	endExecution(asdmEntRef, sbEntRef, sessionEntRef, compl);
    }
    
    protected void endExecution(IDLEntityRef asdmEntRef, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef, Completion compl) 
        throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx {

        deactivateOffshoots();
        
        ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
        DataCapturer dataCapturer = null;
        try {
            Resource<DataCapturer> dcres = resMng.getResource(array.getDataCapturerName());
            dataCapturer = dcres.getComponent();
        } catch(AcsJResourceExceptionsEx ex) {
            String msg = "Error getting OFFLINE/DataCapturer resource. "
                + "This resource should have been acquired during the array "
                + "initialization. This is an internal error.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp;
        }
        
        long now = Util.arrayTimeToACSTime(Util.getArrayTime().get());
        try {
            // TODO: a new parameter should be introduced saying to DataCapturer
            // whether to save ASDM to disk or not.
            dataCapturer.endSB(array.getArrayName(), now, getScanNumber());
        } catch (TableUpdateErrorEx ex) {
            String msg = "Error ending SB execution in OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp;
        } catch (DataErrorEx ex) {
            String msg = "Error ending SB execution in OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp;
        }

        setExecBlockUid("-");
        setPiName("-");
        setSchedBlockUid("-");
        setProjectName("-"); // just for now
        setScript("-");
        setExecBlockAllowedTime(0);
        setExecBlockStartTime(-1);

        // Completion compl = new Completion(0, 0, 0, new alma.ACSErr.ErrorTrace[0]);
        this.sendExecBlockEndedEvent(compl, sbEntRef, sessionEntRef, asdmEntRef);
    }

	public void flagData(boolean startStop, long timeStamp,
			String componentName, String description) {
		try {
			DataCapturer dc =
				ResourceManager.getInstance(array.getContainerServices())
			                   .getComponent(getDataCapturerName());
			FlagData fd = new FlagData();
			fd.time = timeStamp;
			fd.flag = startStop;
			fd.reason = description;
			// get the antenna name
			String antennaName = componentName.replaceFirst("CONTROL/", "");
			antennaName = antennaName.replaceFirst("/.*", "");
			fd.antennaName = new String[] {antennaName};
			// get polarization
			fd.polarization = new PolarizationType[0];
			// get baseband
			fd.baseband = new BasebandName[0];
			dc.sendFlagData(fd);
		} catch (AcsJResourceExceptionsEx ex) {
			// Fine. If there is not DataCapture Resource, just ignore the call.
		} catch (TableUpdateErrorEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DataErrorEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    protected void deactivateOffshoots() {

        logger.fine("Deactivating offshoots.");
        alma.acs.container.ContainerServices cs = array.getContainerServices();
        if (sfiOffshoot != null) {
            logger.fine("Deactivating SFI Offshoot.");
            ObservingModeBaseImpl.shutdownObservingMode(sfiOffshoot, sfiObsModeTie, cs);
            sfiObsMode = null;
            sfiObsModeTie = null;
            sfiOffshoot = null;
        }

        if (thOffshoot != null) {
            logger.fine("Deactivating TH Offshoot.");
            ObservingModeBaseImpl.shutdownObservingMode(thOffshoot, thObsModeTie, cs);
            thObsModeTie = null;
            thOffshoot = null;
        }

        if (th7mOffshoot != null) {
            logger.fine("Deactivating TH7m Offshoot.");
            ObservingModeBaseImpl.shutdownObservingMode(th7mOffshoot, th7mObsModeTie, cs);
            th7mObsModeTie = null;
            th7mOffshoot = null;
        }

        if (opOffshoot != null) {
            logger.fine("Deactivating OP Offshoot.");
            ObservingModeBaseImpl.shutdownObservingMode(opOffshoot, opObsModeTie, cs);
            opObsModeTie = null;
            opOffshoot = null;
        }

        if (tpOffshoot != null) {
            logger.fine("Deactivating TP Offshoot.");
            ObservingModeBaseImpl.shutdownObservingMode(tpOffshoot, tpObsModeTie, cs);
            tpObsMode = null;
            tpObsModeTie = null;
            tpOffshoot = null;
        }
        
        if (ahOffshoot != null) {
            logger.fine("Deactivating astronomical holography offshoot.");
            ObservingModeBaseImpl.shutdownObservingMode(ahOffshoot, ahObsModeTie, cs);
            ahObsModeTie = null;
            ahOffshoot = null;
        }
   }

    protected void sendExecBlockStartedEvent(long startTime, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef, IDLEntityRef asdmEntRef) {
        
        ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
        Resource<SimpleSupplier> res = null;
        SimpleSupplier publisher = null;
        try {
            res = resMng.getResource("CONTROL_SYSTEM");
            publisher = res.getComponent();
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Error sending ExecBlockEndedEvent. There was a problem ";
            msg += "accessing the 'CONTROL_SYSTEM' notification channel resource ";
            msg += "in the ResourceManager. ";
            msg += "This is an internal error, check the error trace for details. ";
            msg += "Consequences: SCHEDULING won't be able to update the ";
            msg += "status of the SB execution.";
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            exHlp.log(logger);
            return;            
        }
        ExecBlockStartedEvent event = new ExecBlockStartedEvent();
        event.execId = asdmEntRef;
        event.sbId = sbEntRef;
        event.sessionId = sessionEntRef;
        event.arrayName = array.getArrayName();
        event.startTime = startTime;
        logger.info("Sending ExecBlockStartedEvent");
        try {
            publisher.publishEvent(event);
        } catch (AcsJException e) {
            e.log(logger);
        }
        execBlockStartedEventSent = true;
    }

    protected void sendExecBlockEndedEvent(Completion compl, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef, IDLEntityRef asdmEntRef) {
        sendExecBlockEndedEvent(compl, sbEntRef, sessionEntRef, asdmEntRef, false);
    }

    protected void sendExecBlockEndedEvent(Completion compl, IDLEntityRef sbEntRef,
            IDLEntityRef sessionEntRef, IDLEntityRef asdmEntRef, boolean aborted) {

        if (execBlockEndedEventSent) {
            logger.warning("ExecBlockEndedEvent has already been sent. It won't " +
                    "be sent again.");
            return;
        }

        if (!execBlockStartedEventSent) {
            logger.warning("ExecBlockStartedEvent has not been sent, although the system " +
                    "has been asked to send the ExecBlockEndedEvent. The former should " +
                    "*always* be sent before the latter. Does your script begin with " +
                    "beginExecution()? The system will send the missing ExecBlockStartedEvent " +
                    "for you for now... Just don't forget to correct this in the script." +
                    " If the SB has been aborted, on the other hand, then maybe the " +
                    "beginExecution() was never reached and you can forget about my last comment.");
            sendExecBlockStartedEvent(Util.arrayTimeToACSTime(Util.getArrayTime().get()),
                    sbEntRef, sessionEntRef, asdmEntRef);
        }
                
        ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
        Resource<SimpleSupplier> res = null;
        SimpleSupplier publisher = null;
        try {
            res = resMng.getResource("CONTROL_SYSTEM");
            publisher = res.getComponent();
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Error sending ExecBlockEndedEvent. There was a problem ";
            msg += "accessing the 'CONTROL_SYSTEM' notification channel resource ";
            msg += "in the ResourceManager. ";
            msg += "This is an internal error, check the error trace for details. ";
            msg += "Consequences: SCHEDULING won't be able to update the ";
            msg += "status of the SB execution.";
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
            exHlp.setProperty("Message", msg);
            exHlp.log(logger);
            return;            
        }
        ExecBlockEndedEvent event = new ExecBlockEndedEvent();
        event.execId = asdmEntRef;
        event.sbId = sbEntRef;
        event.sessionId = sessionEntRef;
        event.arrayName = array.getArrayName();
        event.dataCapturerId = array.getDataCapturerName();
        alma.Control.Completion c;
        if (compl.previousError.length > 0) {
            logger.finer("Sending error completion");
            c = alma.Control.Completion.FAIL;
            event.errorTrace = compl.previousError;
        } else {
            logger.finer("Sending successful completion");
            c = alma.Control.Completion.SUCCESS;
            event.errorTrace = new ErrorTrace[0];
        }
        if (aborted) {
            c = alma.Control.Completion.ABORTED;
            event.errorTrace = new ErrorTrace[0];
        }
        event.status = c;
        
        event.endTime = Util.arrayTimeToACSTime(Util.getArrayTime().get());

        logger.info("Sending ExecBlockEndedEvent");
        try {
            publisher.publishEvent(event);
        } catch (AcsJException e) {
            e.log(logger);
        }

        execBlockEndedEventSent = true;
    }

    public synchronized boolean sbHasBeenStopped() {
        return hasBeenStopped;
    }

    protected synchronized void setStopFlag(boolean stop) {
        hasBeenStopped = stop;
        if (stop) {
	        if (thOffshoot != null) {
	        	thOffshoot.abortScan();
	        }
	        if (th7mOffshoot != null) {
	        	th7mOffshoot.abortScan();
	        }
	        if (sfiOffshoot != null) {
	        	sfiOffshoot.abortScan();
	        }
	        if (opOffshoot != null) {
	        	opOffshoot.abortScan();
	        }
	        if (tpOffshoot != null) {
	        	tpOffshoot.abortScan();
	        }
	        if (ahOffshoot != null) {
	        	ahOffshoot.abortScan();
	        }
        } else {
	        if (thOffshoot != null) {
	        	thOffshoot.resetAbortScanFlag();
	        }
	        if (th7mOffshoot != null) {
	        	th7mOffshoot.resetAbortScanFlag();
	        }
	        if (sfiOffshoot != null) {
	        	sfiOffshoot.resetAbortScanFlag();
	        }
	        if (opOffshoot != null) {
	        	opOffshoot.resetAbortScanFlag();
	        }
	        if (tpOffshoot != null) {
	        	tpOffshoot.resetAbortScanFlag();
	        }
	        if (ahOffshoot != null) {
	        	ahOffshoot.resetAbortScanFlag();
	        }        	
        }
    }

    public synchronized boolean sbHasBeenAborted() {
        return hasBeenAborted;
    }

    protected synchronized void setAbortionFlag(boolean aborted) {
        hasBeenAborted = aborted;
    }
    
    public boolean sbHasToStop() {
        return sbHasBeenStopped() || sbHasBeenAborted();
    }
    
    protected synchronized void resetAbortAndStopFlags() {
        setStopFlag(false);
        setAbortionFlag(false);
    }
    
     /* Antenna Status Reporting Methods */
    public void reportPointingModel(String antennaName, ReceiverBand band,
            ModelTerm[] pointingModel, ModelTerm[] auxPointingModel) {

         try {
            ResourceManager resMng = ResourceManager.getInstance(array
                    .getContainerServices());
            Resource<DataCapturer> dcres = resMng.getResource(array
                    .getDataCapturerName());
            DataCapturer dataCapturer = dcres.getComponent();
            dataCapturer.sendAntennaPointingModel(antennaName, band,
                    pointingModel, auxPointingModel);
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Cannot put the pointing model into the ASDM";
            msg += " as a reference to the data capture component cannot be obtained.";
            cantTalkToDataCapturerRepeatGuard.log(Level.WARNING, msg, ex);
       } catch (DataErrorEx ex) {
            String msg = "Cannot put the pointing model into the ASDM";
            msg += " as the pointing model is incorrect.";
            AcsJDataErrorEx jex = new AcsJDataErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot put the pointing model into the ASDM";
            msg += " as there is a problem storing this data.";
            AcsJTableUpdateErrorEx jex = new AcsJTableUpdateErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        }
    }

    public void reportFocusModel(String antennaName, ReceiverBand band,
            ModelTerm[] focusModel) {

        try {
            ResourceManager resMng = ResourceManager.getInstance(array
                    .getContainerServices());
            Resource<DataCapturer> dcres = resMng.getResource(array
                    .getDataCapturerName());
            DataCapturer dataCapturer = dcres.getComponent();
            dataCapturer.sendAntennaFocusModel(antennaName, band, focusModel);
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Cannot put the focus model into the ASDM";
            msg += " as a reference to the data capture component cannot be obtained.";
            cantTalkToDataCapturerRepeatGuard.log(Level.WARNING, msg, ex);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot put the focus model into the ASDM";
            msg += " as there is a problem storing this data.";
            AcsJTableUpdateErrorEx jex = new AcsJTableUpdateErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        } catch (DataErrorEx ex) {
            String msg = "Cannot put the focus model into the ASDM";
            msg += " as the data is incorrect.";
            AcsJDataErrorEx jex = new AcsJDataErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        }
    }

    public void reportSubreflectorPosition(String antennaName,
            boolean focusTrackingEnabled, double x, double y, double z,
            double xOffset, double yOffset, double zOffset) {

        try {
            ResourceManager resMng = ResourceManager.getInstance(array
                    .getContainerServices());
            Resource<DataCapturer> dcres = resMng.getResource(array
                    .getDataCapturerName());
            DataCapturer dataCapturer = dcres.getComponent();
            FocusData focusData = new FocusData(antennaName, x, y, z,
                    focusTrackingEnabled, xOffset, yOffset, zOffset);
            dataCapturer.sendFocusData(focusData);
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Cannot put the subreflector position into the ASDM";
            msg += " as a reference to the data capture component cannot be obtained.";
            cantTalkToDataCapturerRepeatGuard.log(Level.WARNING, msg, ex);
        } catch (DataErrorEx ex) {
            String msg = "Cannot put the subreflector position into the ASDM";
            msg += " as the data is incorrect.";
            AcsJDataErrorEx jex = new AcsJDataErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot put the subreflector position into the ASDM";
            msg += " as there is a problem storing this data.";
            AcsJTableUpdateErrorEx jex = new AcsJTableUpdateErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        }
    }

    public void reportCalDeviceData(CalDeviceData data) {
        ResourceManager resMng = ResourceManager.getInstance(array.getContainerServices());
        DataCapturer dataCapturer = null;
        try {
            Resource<DataCapturer> dcres = resMng.getResource(array.getDataCapturerName());
            dataCapturer = dcres.getComponent();
            dataCapturer.sendCalDeviceData(data);
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Cannot put the calibration device data into the ASDM";
            msg += " as a reference to the data capture component cannot be obtained.";
            cantTalkToDataCapturerRepeatGuard.log(Level.WARNING, msg);
        } catch (DataErrorEx ex) {
            String msg = "Cannot put the calibration device data into the ASDM";
            msg += " as the data is incorrect.";
            AcsJDataErrorEx jex = new AcsJDataErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot put the calibration device data into the ASDM";
            msg += " as there is a problem storing this data.";
            AcsJDataErrorEx jex = new AcsJDataErrorEx(ex);
            jex.setProperty("Message", msg);
            jex.log(logger);
        }
    }

    public boolean isInsideExecBlock() {
        return insideExecBlock;
    }

    public void setInsideExecBlock(boolean insideExecBlock) {
        this.insideExecBlock = insideExecBlock;
    }

    @Override
    public ResourceId[] getPhotonicReferences() {
        return array.getPhotonicReferences();
    }

    /**
     * Get the Correlator Bulk Data Distributor Streaming Information.
     * 
     * @see alma.Control.ObservingModes.ObservingModesArray#getCorrelatorBDDStreamInfo
     */
	@Override
	public BDDStreamInfo getCorrelatorBDDStreamInfo() {
		BDDStreamInfo bddStreamInfo = new BDDStreamInfo();
		bddStreamInfo.distributor = array.getCorrelatorBulkDataDistributor();
		/*
		 * For now the flows are static. When we get to the point of implementing multiple
		 * subarrays (per array), then a different way to manage the flows will need to be
		 * implemented. One option is use different flows in the same distributor for each
		 * subarray. Another is to use different distributors.
		 */
		bddStreamInfo.channelAvgFlow = CorrelatorBDDFlowNumbers.getChannelAverageFlowNumber();
		bddStreamInfo.spectralFlow = CorrelatorBDDFlowNumbers.getSpectralFlowNumber();
		bddStreamInfo.wvrFlow = CorrelatorBDDFlowNumbers.getWVRFlowNumber();
		return bddStreamInfo;
	}

	/**
	 * Get the Correlator ObservationControl component name.
	 * 
	 * @see alma.Control.ObservingModes.ObservingModesArray#getCorrelatorObservationControlComponentName
	 */
	@Override
	public String getCorrelatorObservationControlComponentName()
			throws AcsJBadParameterEx {
		CorrelatorType type = array.getCorrelatorType();
		if (type == null) throw new AcsJBadParameterEx();
		if (type == CorrelatorType.BL) {
			return "CORR/OBSERVATION_CONTROL"; // for BL correlator
		} else if (type == CorrelatorType.ACA) {
			return "ACACORR/OBSERVATION_CONTROL"; // for ACA correlator
		}
		throw new AcsJBadParameterEx();
	}

    @Override
    public boolean isScanStatusAvailable() {
        if (!isInsideExecBlock()) return false;
        if (scanIntentData == null) return false;
        return true;
    }

    @Override
    public ScanStatus getScanStatus() throws AcsJInvalidRequestEx {
        if (!isScanStatusAvailable()) {
            String msg = "Scan Status is not available.";
            AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
            invEx.setProperty("State", extState.toString());
            invEx.setProperty("Message", msg);
            throw invEx;
        }
        ScanStatus status = new ScanStatus();
        status.scanNum = (short)scan;
        status.subscanCount = (short)subScan;
        status.intent = scanIntentData[0].scanIntent; // which one should be used?
        status.timeRemaining = 0;
        return status;
    }

    @Override
    public boolean isSubscanStatusAvailable() {
        return false;
    }

    @Override
    public SubscanStatus getSubscanStatus() throws AcsJInvalidRequestEx {
        String msg = "getSubscanStatus() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    @Override
    public boolean isAntennaShutterStatusAvailable() {
        return true;
    }

    @Override
    public AntennaShutterStatus getAntennaShutterStatus()
            throws AcsJInvalidRequestEx {
        // For now this information is available only through the
        // ArrayMountController or the MountController.
        // It should be exposed through the Antenna, I think.
        ArrayMountController[] amcs = null;
        MountController mc = null;
        try {
            amcs = getArrayMountControllers();
            mc = getMountController();
        } catch (ContainerServicesEx ex) {
            ex.printStackTrace();
        }
        AntennaShutterStatus status = new AntennaShutterStatus();
        status.closedCount = 0;
        status.openCount = 0;
        if (amcs.length > 0) {
            for (ArrayMountController amc : amcs) {
                status.closedCount += amc.getShutterClosedCount();
                status.openCount += amc.getShutterOpenCount();
            }
        } else if (mc != null) {
            try {
                if (mc.isShutterClosed()) status.closedCount = 1;
            } catch (UnallocatedEx ex) {
                ex.printStackTrace();
            } catch (MountFaultEx ex) {
                ex.printStackTrace();
            }
            try {
                if (mc.isShutterOpen()) status.openCount = 1;
            } catch (UnallocatedEx ex) {
                ex.printStackTrace();
            } catch (MountFaultEx ex) {
                ex.printStackTrace();
            }
        } else {
            // get it from the antennas
        }
        return status;
    }

    @Override
    public boolean isAntennaStatusAvailable() {
        return false;
    }

    @Override
    public AntennaStatus getAntennaStatus() throws AcsJInvalidRequestEx {
        String msg = "getAntennaStatus() cannot be executed when the state is "
            + extState.toString();
        AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
        invEx.setProperty("State", extState.toString());
        invEx.setProperty("Message", msg);
        throw invEx;
    }

    @Override
    public boolean isAtmosphericConditionsAvailable() {
        return true;
    }

    @Override
    public AtmosphericConditions getAtmosphericConditions()
            throws AcsJInvalidRequestEx {
        AtmosphericConditions status = new AtmosphericConditions();
        status.lastTau = 3.14;
        status.wh2o = 2.71;
        return status;
    }

    @Override
    public boolean isCorrelatorStatusAvailable() {
        return true;
    }

    @Override
    public CorrelatorStatus getCorrelatorStatus() throws AcsJInvalidRequestEx {
        CorrelatorStatus status = new CorrelatorStatus();
        status.apState = ""; // from where?
        status.corrMode = 0; // from where?
        status.corrType = array.getCorrelatorType();
        if (status.corrType != CorrelatorType.NONE)
            status.usesCorr = true;
        else
            status.usesCorr = false;
        return status;
    }

    @Override
    public boolean isExecBlockStatusAvailable() {
        if (!isInsideExecBlock()) return false;
        return true;
    }

    @Override
    public ExecBlockStatus getExecBlockStatus() throws AcsJInvalidRequestEx {
        if (!isExecBlockStatusAvailable()) {
            String msg = "ExecBlock Status is not available.";
            AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
            invEx.setProperty("Message", msg);
            throw invEx;            
        }
        ExecBlockStatus status = new ExecBlockStatus();
        status.allowedTime = execBlockAllowedTime;
        status.antennaRequiredCount = 0;
        status.execBlockUid = execBlockUid;
        status.piName = piName;
        status.projectName = projectName;
        status.schedBlockUid = schedBlockUid;
        status.script = script;
        status.startTime = execBlockStartTime;
        return status;
    }

    @Override
    public boolean isLocalOscillatorStatusAvailable() {
        if (!isInsideExecBlock()) return false;
        if ( (sfiOffshoot == null) &&
                (tpOffshoot == null) &&
                (ahOffshoot == null))
               return false;
        return true;
    }

    @Override
    public LocalOscillatorStatus getLocalOscillatorStatus()
            throws AcsJInvalidRequestEx {
        if (!isPointingStatusAvailable()) {
            String msg = "Local Oscillator Status is not available.";
            AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
            invEx.setProperty("Message", msg);
            throw invEx;            
        }
        CalibrationDevice acdState = CalibrationDevice.NONE;
        double skyFrequency = 0.0;
        try {
            LocalOscillator lo = getLocalOscillator();
            acdState = lo.getCalibrationDevice();
            skyFrequency = lo.averageSkyFrequency();
        } catch (ContainerServicesEx ex) {
            ex.printStackTrace();
        } catch (AntennasDisagreeEx ex) {
            ex.printStackTrace();
        }
        LocalOscillatorStatus status = new LocalOscillatorStatus();
        status.acdState = acdState;
        status.frontEndsLockedCount = 0;
        status.maxDewerTemp = 0.0f;
        status.skyFrequency = skyFrequency;
        return status;
    }

    @Override
    public boolean isPointingStatusAvailable() {
        if (!isInsideExecBlock()) return false;
        if ( (sfiOffshoot == null) &&
             (tpOffshoot == null) &&
             (ahOffshoot == null) &&
             (opOffshoot == null) &&
             (th7mOffshoot == null) &&
             (thOffshoot == null))
            return false;
        return true;
    }

    @Override
    public PointingStatus getPointingStatus() throws AcsJInvalidRequestEx {
        if (!isPointingStatusAvailable()) {
            String msg = "Pointing Status is not available.";
            AcsJInvalidRequestEx invEx = new AcsJInvalidRequestEx();
            invEx.setProperty("Message", msg);
            throw invEx;            
        }
        short antennasOnSourceCount = 0;
        double az = 0.0;
        double el = 0.0;
        double ra = 0.0;
        double dec = 0.0;
        double xeloffset = 0.0; // cross elevation offset
        double eloffset = 0.0; // elevation offset
        // First try with the ObservingModes that have an ArrayMountController.
        ArrayMountController[] amcs = null;
        try {
            amcs = getArrayMountControllers();
        } catch (ContainerServicesEx ex) {
            ex.printStackTrace();
        }
        if (amcs.length > 0) {
            int count = 0;
            for (ArrayMountController amc : amcs) {
                PointingData[] pdt = amc.getPointingData();
                for (PointingData pd : pdt) {
                    count++;
                    az += pd.measured.az;
                    el += pd.measured.el;
                    ra += pd.measuredTarget.ra;
                    dec += pd.measuredTarget.dec;
                    xeloffset += pd.horizon.lng;
                    eloffset += pd.horizon.lat;
                    if (pd.onSource) antennasOnSourceCount++;
                }
            }
            az = az / count;
            el = el / count;
            ra = ra / count;
            dec = dec / count;
            xeloffset = xeloffset / count;
            eloffset = eloffset / count;
        } else {
            // If none of the ObservingModes with ArrayMountController are active,
            // try with the ones that have a MountController.
            try {
                MountController mc = null;
                mc = getMountController();
                PointingData pd = mc.getPointingData();
                az += pd.measured.az;
                el += pd.measured.el;
                ra += pd.measuredTarget.ra;
                dec += pd.measuredTarget.dec;
                xeloffset += pd.horizon.lng;
                eloffset += pd.horizon.lat;
                if (pd.onSource) antennasOnSourceCount++;
            } catch (ContainerServicesEx ex) {
                ex.printStackTrace();
            } catch (UnallocatedEx ex) {
                ex.printStackTrace();
            } catch (MountFaultEx ex) {
                ex.printStackTrace();
            }
        }
        PointingStatus status = new PointingStatus();
        status.ra = ra;
        status.dec = dec;
        status.az = az;
        status.el = el;
        status.crossElOffset = xeloffset;
        status.elOffset = eloffset;
        status.crossElVelocity = 0.0;
        status.elVelocity = 0.0;
        status.sourceName = "";
        status.antennasOnSourceCount = antennasOnSourceCount;
        status.maximumPointingError = 0.0;
        status.timeToSet = 0;
        return status;
    }

    /**
     * Get the currently active ArrayMountControllers. It could be more than one if
     * more than one subarray has been created in the active ObservingMode.
     * 
     * Only the Single Field Interferometry, Total Power and Astronomical Holography
     * Observing Modes have ArrayMountControllers. Optical Pointing and Tower
     * Holography have a single MountController instead.
     * 
     * @return ArrayMountController array. If there is no current ObservingMode active,
     * an empy array is returned.
     * @throws ContainerServicesEx In case of ACS problems creating the Offshot.
     */
    protected ArrayMountController[] getArrayMountControllers() throws ContainerServicesEx {
        List<ArrayMountController> retVal = new ArrayList<ArrayMountController>();
        if (sfiOffshoot != null) {
            String[] subArrays = sfiOffshoot.getSubarrays();
            for (String sarr : subArrays) {
                try {
                    ArrayMountController amc = sfiOffshoot.getSubarrayMountController(sarr);
                    retVal.add(amc);
                } catch (IllegalParameterErrorEx ex) {
                    ex.printStackTrace();
                    logger.severe("Error retrieving SubarrayMountController. No subarray named " +
                    		sarr + " exists in the SFI Observing Mode.");
                }
            }
        } else if (tpOffshoot != null) {
            ArrayMountController amc = tpOffshoot.getArrayMountController();
            retVal.add(amc);
        } else if (ahOffshoot != null) {
            ArrayMountController amc = ahOffshoot.getArrayMountController();
            retVal.add(amc);            
        }
        return retVal.toArray(new ArrayMountController[0]);
    }
    
    /**
     * Get the currently active MountController.
     * 
     * This fucntion only returns the MountController associated with either the
     * Tower Holography or Optical Pointing Observing Modes. For the other Observing Modes,
     * use getArrayMountControllers().
     *  
     * @return MountController, or null if no MountController is active.
     * @throws ContainerServicesEx In case of ACS problems getting a reference to the MountController.
     */
    protected MountController getMountController() throws ContainerServicesEx {
        MountController retVal = null;
        ContainerServices cs = array.getContainerServices();
        if (thOffshoot != null) {
            try {
                String modeCtrlName = thOffshoot.getTowerHolographyModeController();
                TowerHolography modeCtrl =
                    TowerHolographyHelper.narrow(cs.getComponentNonSticky(modeCtrlName));
                String mountCtrlName = modeCtrl.getMountController();
                retVal =
                    MountControllerHelper.narrow(cs.getComponentNonSticky(mountCtrlName));
            } catch (AcsJContainerServicesEx ex) {
                ex.printStackTrace();
                throw ex.toContainerServicesEx();
            }  
        } else if (th7mOffshoot != null) {
            try {
                String modeCtrlName = th7mOffshoot.getTowerHolographyModeController();
                TowerHolography7m modeCtrl =
                    TowerHolography7mHelper.narrow(cs.getComponentNonSticky(modeCtrlName));
                String mountCtrlName = modeCtrl.getMountController();
                retVal =
                    MountControllerHelper.narrow(cs.getComponentNonSticky(mountCtrlName));
            } catch (AcsJContainerServicesEx ex) {
                ex.printStackTrace();
                throw ex.toContainerServicesEx();
            }  
        } else if (opOffshoot != null) {
            try {
                String modeCtrlName = opOffshoot.getOpticalPointingModeController();
                OpticalPointing modeCtrl =
                    OpticalPointingHelper.narrow(cs.getComponentNonSticky(modeCtrlName));
                String mountCtrlName = modeCtrl.getMountController();
                retVal =
                    MountControllerHelper.narrow(cs.getComponentNonSticky(mountCtrlName));
            } catch (AcsJContainerServicesEx ex) {
                ex.printStackTrace();
                throw ex.toContainerServicesEx();
            }  
        }
        return retVal;
    }
    
    protected LocalOscillator getLocalOscillator() throws ContainerServicesEx {
        LocalOscillator retVal = null;
        if (sfiOffshoot != null) {
            retVal = sfiOffshoot.getLocalOscillator();
        } else if (tpOffshoot != null) {
            retVal = tpOffshoot.getLocalOscillator();
            
        } else if (ahOffshoot != null) {
            retVal = ahOffshoot.getLocalOscillator();
        }
        return retVal;
    }

    public void setCorrConfigID(int corrConfigID) {
        this.corrConfigID = corrConfigID;
    }

    public void setScan(int scan) {
        this.scan = scan;
    }

    public void setSubScan(int subScan) {
        this.subScan = subScan;
    }

    public void setScanIntentData(ScanIntentData[] scanIntentData) {
        this.scanIntentData = scanIntentData;
    }

    public void setScanStartTime(long scanStartTime) {
        this.scanStartTime = scanStartTime;
    }

    public void setSubScanDuration(long subScanDuration) {
        this.subScanDuration = subScanDuration;
    }

    public void setUsesCorr(boolean usesCorr) {
        this.usesCorr = usesCorr;
    }

    public void setApState(String apState) {
        this.apState = apState;
    }

    public void setCorrMode(short corrMode) {
        this.corrMode = corrMode;
    }

    public void setExecBlockAllowedTime(long execBlockAllowedTime) {
        this.execBlockAllowedTime = execBlockAllowedTime;
    }

    public void setExecBlockUid(String execBlockUid) {
        this.execBlockUid = execBlockUid;
    }

    public void setSchedBlockUid(String schedBlockUid) {
        this.schedBlockUid = schedBlockUid;
    }

    public void setPiName(String piName) {
        this.piName = piName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public void setExecBlockStartTime(long execBlockStartTime) {
        this.execBlockStartTime = execBlockStartTime;
    }
}

// __oOo__
