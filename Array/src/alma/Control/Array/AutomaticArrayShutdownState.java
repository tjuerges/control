package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.Common.State;

public class AutomaticArrayShutdownState extends AutomaticArrayStateBase 
    implements AutomaticArrayState {
    
    public AutomaticArrayShutdownState(ArrayInternal array) {
        super(array, State.Inaccessible_Stopped);
    }

    public void startupPass1() throws AcsJInitializationErrorEx, AcsJInvalidRequestEx {
        array.setState("STARTINGUP_STATE");
        array.startupPass1();
    }

}
