/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Array;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.ACSErr.Completion;
import alma.ACSErr.ErrorTrace;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.ArrayExceptions.wrappers.SBExecutionErrorAcsJCompletion;
import alma.Control.Antenna;
import alma.Control.AntennaStateEvent;
import alma.Control.BDDStreamInfo;
import alma.Control.SBExecCallback;
import alma.Control.SBExecCallbackHelper;
import alma.Control.SBExecCallbackPOA;
import alma.Control.ScriptExecutor;
import alma.Control.ScriptExecutor2;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.State;
import alma.Control.Common.Util;
import alma.DataCaptureExceptions.SettingDataCapturerIdErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.offline.ASDMArchivedEvent;
import alma.offline.DataCapturer;
import alma.offline.DataCapturerId;

// Notes:
// The Completion is sent to SCHEDULING in the endExecution() call. May be
// it should be sent only at the end of the execution.

/**
 * AutomaticArray operational state.
 * 
 * When the AutomaticArray is in the operational state, it delegates most
 * of its functionality over this class.
 * 
 * @author rhiriart
 *
 */
public class AutomaticArrayOperationalState extends AutomaticArrayStateBase
    implements AutomaticArrayState {
    
    private String cn = getClass().getName();

    private ArrayInternal array;
    private IDLEntityRef sbEntRef;
    private IDLEntityRef sessionEntRef;
    private IDLEntityRef asdmEntRef;
    private Runnable executor;
    private Thread execThread;
    private boolean dataCaptureIsReady;
    private boolean asdmArchived;
    private SchedBlock schedBlock;
    
    // This lock and associated condition variables are for
    // waiting for certain conditions during SB execution.
    private Lock lock = new ReentrantLock();
    private Condition cv = lock.newCondition();

    private class SBExecCallbackImpl extends SBExecCallbackPOA {

        private Logger logger;
        private Completion completion;
        
        public SBExecCallbackImpl(Logger logger) {
            this.logger = logger;
        }
        
        synchronized public void report(String arrayName, Completion completion) {
            logger.info("Receiving callback report for array " + arrayName);
            if (arrayName.equals(array.getArrayName())) {
                this.completion = completion;
                logger.info("Reporting SB execution completion in array " + array.getArrayName());
                notify();
            }
        }
        
        public Completion getCompletion() {
            return completion;
        }
        
        synchronized public Completion waitForCompletion() throws Exception {
            while (completion == null) // guard for spurious wake-ups
                wait(0);
            return completion;
        }
    }
    
    private class DCDestructor extends Thread {

        private final long ASDM_ARCHIVED_TIMEOUT = 600000;

        private String dataCapturerName;
        private ResourceManager resMng;
        private ContainerServices container;
        private Lock dcLock = new ReentrantLock();
        private Condition dcCV = dcLock.newCondition();
        private boolean asdmArchived;

        public DCDestructor(String dcn, AutomaticArrayOperationalState opState) {
            this.dataCapturerName = dcn;
            this.container = opState.array.getContainerServices();
            this.resMng = ResourceManager.getInstance(container);
        }
        
        public void run() {
            
            // Wait for the ASDM to be archived. This is necessary to avoid
            // destroying DataCapturer before it has finish archiving the ASDM. If this
            // happens, the ASDM is not archived.
            logger.info("Waiting for ASDM to be archived...");
            
            try {
                dcLock.lock();
                boolean ready;
                while (!asdmArchived) {
                    try {
                        ready = dcCV.await(ASDM_ARCHIVED_TIMEOUT, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException ex) {
                        String msg = "Thread waiting for ASDM to be archived has been " +
                                     "interrupted. This is an internal error.";
                        AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                        exHlp.setProperty("Message", msg);
                        logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                        exHlp.log(logger);
                        SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                        sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                        deactivateOffshoots();
                        destroyDataCapturer();
                        return;                    
                    }
                    if (!ready) {
                        logger.severe("ASDMArchived event hasn't been received " +
                                "but timeout (" + ASDM_ARCHIVED_TIMEOUT / 1000 + "s) " +
                                "has been reached.");
                        break;
                    } else
                        logger.info("The ASDM has been archived.");                        
                }                
            } finally {
                dcLock.unlock();
            }
                            
            logger.info("Destroying DataCapturer");
            destroyDataCapturer();

        }

        private void destroyDataCapturer() {
            resMng.freeResource(dataCapturerName);
        }

        public void asdmArchived() {
            try {
                logger.finer("Received ASDM archived notification.");
                dcLock.lock();
                asdmArchived = true;
                dcCV.signal();
            } finally {
                dcLock.unlock();
            }        
        }        
    }
    
    private Map<String, DCDestructor> dcDestructors;
    
    /**
     * This internal class creates a DataCapturer component and
     * executes a Scheduling Block.
     */
    private class SBExecutor implements Runnable {

        private final long DATACAPTURE_READY_TIMEOUT = 5000;
        
        private AutomaticArrayOperationalState opState;
        private ContainerServices container;
        private ResourceManager resMng;
        
        
        public SBExecutor(AutomaticArrayOperationalState opState) {
            this.opState = opState;
            this.container = opState.array.getContainerServices();
            this.resMng = ResourceManager.getInstance(container);
        }
        
        public void run() {
            
            logger.info("Beginning SB execution.");
            
            if (sbHasToStop()) // First check point for the stop/abortion flag
                return;
            
            opState.execBlockStartedEventSent = false;
            opState.execBlockEndedEventSent = false;
            
            logger.info("Creating DataCapturer");
            try {
                createDataCapturer();
            } catch (AcsJResourceErrorEx ex) {
                String msg = "Error creating DataCapturer component.";
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                exHlp.setProperty("Message", msg);
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                exHlp.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                return;
            } catch (SettingDataCapturerIdErrorEx ex) {
                String msg = "Error setting ID in DataCapturer component.";
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                exHlp.setProperty("Message", msg);
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                exHlp.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                return;
            }
            
            logger.info("Waiting for DataCapturer to be ready...");
            
            try {
                lock.lock();
                boolean ready;
                while (!dataCaptureIsReady) {
                    try {
                        ready = cv.await(DATACAPTURE_READY_TIMEOUT, TimeUnit.MILLISECONDS);                    
                    } catch (InterruptedException ex) {
                        String msg = "Thread waiting for DataCapturer to be ready has been " +
                                     "interrupted. This is an internal error.";
                        AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                        exHlp.setProperty("Message", msg);
                        logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                        exHlp.log(logger);
                        SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                        opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                        destroyDataCapturer();
                        return;                    
                    }
                    if (!ready) {
                        logger.warning("DataCapturerReady event hasn't been received " +
                                 "but timeout (" + DATACAPTURE_READY_TIMEOUT / 1000 + "s) " +
                                 "has been reached. Execution will continue, DataCapturer ready " +
                                 "or not.");
                        break;
                    } else
                        logger.info("DataCapturer is ready. Continue with SB execution.");            
                }                
            } finally {
                lock.unlock();
            }
                        
            // Get the SB and the execution script.
            logger.info("Getting SB from the ARCHIVE.");
            SchedBlock apdmSchedBlock = null;
            try {
                apdmSchedBlock = getSBFromArchive(opState.sbEntRef.entityId);
                opState.schedBlock = apdmSchedBlock;
            } catch (AcsJSBExecutionErrorEx ex) {
                String msg = "Error getting SB from the ARCHIVE.";
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                exHlp.setProperty("Message", msg);
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                exHlp.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                destroyDataCapturer();
                return;
            }
            String executionScript = null;
            try {
                executionScript = opState.getObsScript(apdmSchedBlock);
            } catch (AcsJSBExecutionErrorEx ex) {
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(ex.getErrorTrace()));
                ex.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(ex);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                destroyDataCapturer();
                return;
            }
            
            // Execute SB.
            
            // Get the ScriptExecutor component.
            ContainerServices container = opState.array.getContainerServices();
            ResourceManager resMng = ResourceManager.getInstance(container);
            String scriptExecName = opState.array.getScriptExecutorName();
            Resource<ScriptExecutor> res = null;
            ScriptExecutor script = null;
            try {
                res = resMng.getResource(scriptExecName);
                script = res.getComponent();
            } catch (AcsJResourceExceptionsEx ex) {
                String msg = "Error getting ScriptExecutor resource from ResourceManager. ";
                msg += "It seems that the AutomaticArray component has not been properly initialized. ";
                msg += "This is an internal error. Contact your administrator.";
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                exHlp.setProperty("Message", msg);
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                exHlp.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                destroyDataCapturer();
                return;
            }

            // Set the scheduling block in the ScriptExecutor.
            StringWriter buffer = new StringWriter();
            try {
                apdmSchedBlock.marshal(buffer);
            } catch (MarshalException ex) {
                String msg = "Error converting SchedBlock entity to XML. ";
                msg += "The SchedBlock entity seems to be corrupted. ";
                msg += "This is an internal error. Contact your administrator.";
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                exHlp.setProperty("Message", msg);
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                exHlp.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                destroyDataCapturer();
                return;
            } catch (ValidationException ex) {
                String msg = "Error converting SchedBlock entity to XML. ";
                msg += "The SchedBlock entity seems to be corrupted. ";
                msg += "This is an internal error. Contact your administrator.";
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                exHlp.setProperty("Message", msg);
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                exHlp.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                destroyDataCapturer();
                return;
            }
            String schedBlk = buffer.toString();
            script.setSB(schedBlk);
            
            // Execute the observation script.
            Completion compl = null;
            try {
                if (!sbHasToStop()) { // Second checkpoint for the abortion flag.
                                      // Check that the stop or abort flag haven't
                                      // been set after this
                                      // thread started but before this point.
                    logger.info("Executing SB in ScriptExecutor " + script.name());
                    
                    SBExecCallbackImpl cbImpl = new SBExecCallbackImpl(logger);
                    SBExecCallback cb = null;
                    cb = SBExecCallbackHelper.narrow(container.activateOffShoot(cbImpl));
                    logger.info("activated callback: " + cb.toString());
                    
                    script.runObservationScriptAsync(executionScript, cb);
                    
                    compl = cbImpl.waitForCompletion();                    
                    logger.finer("Execution ended in ScriptExecutor " + script.name());
                
                    container.deactivateOffShoot(cbImpl);
                } else {
                    logger.info("SB had been aborted, so it won't be executed.");
                }
            } catch (Exception ex) {
                String msg = "Execution error";
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx(ex);
                exHlp.setProperty("Message", msg);
                logger.severe("Error:\n" + Util.getNiceErrorTraceString(exHlp.getErrorTrace()));
                exHlp.log(logger);
                SBExecutionErrorAcsJCompletion complHlp = new SBExecutionErrorAcsJCompletion(exHlp);
                opState.sendExecBlockEndedEvent(complHlp.toCorbaCompletion());
                deactivateOffshoots();
                destroyDataCapturer();
                return;
            }            
            
            // If beginExecution() was called from the script, but endExecution() was
            // not called because the script failed before getting to this point, then
            // endExecution() should be called.
            if ( isInsideExecBlock() ) {
                endExecutionForUnfinishedSchedBlock(compl);
            }
            // In this case the subsequent checks for sending the ExecBlockEndedEvent
            // protect the case that the above call to endExecution() fails for some
            // reason.
            
            // Schedule DataCapturer to be destructed.
            logger.finer("Scheduling DataCapturer to be destroyed.");
            DCDestructor dcd = new DCDestructor(array.getDataCapturerName(), opState);
            dcDestructors.put(array.getDataCapturerName(), dcd);
            dcd.start();
                        
            // Deactivate offshoots in the case the endExecution is never called
            // in the script.
            // This could not be necesary, because of the above call to
            // endExecutionForUnfinishedSchedBlock(), but it's probably a good idea
            // to continue protecting this important case here.
            deactivateOffshoots();
            
            // If the ExecBlockStartedEvent hasn't been sent by now, then the script
            // didn't start with a beginExecution() call. This is a failure.
            if (!opState.execBlockStartedEventSent) {
                // TODO Reuse the previous Completion?
                String msg = "The ExecBlockStartedEvent should have been sent by now, but " +
                "it hasn't. The SB script didn't call beginExecution(). This is " +
                "a failure.";
                logger.severe(msg);
                long now = Util.arrayTimeToACSTime(Util.getArrayTime().get());
                opState.sendExecBlockStartedEvent(now, sbEntRef, sessionEntRef, asdmEntRef);
                AcsJSBExecutionErrorEx errHlp = new AcsJSBExecutionErrorEx();
                errHlp.setProperty("Details", msg);
                Completion c = new Completion();
                c.code = -1; // not used
                c.type = -1; // not used
                c.timeStamp = 0L; // not used
                c.previousError = new ErrorTrace[1];
                c.previousError[0] = errHlp.getErrorTrace();
                opState.sendExecBlockEndedEvent(c);
                return;
            }
                        
            // Send the ExecBlockEndedEvent event in the case the script
            // didn't finish with an endExecution(). This happens in case of errors
            // during the SB execution.
            if (!opState.execBlockEndedEventSent) {
                logger.warning("The ExecBlockEndedEvent should have been sent by now, but " +
                        "it hasn't. Probably the SB script is not calling endExecution(). " +
                        "Sending the ExecBlockEndedEvent anyway.");
                opState.sendExecBlockEndedEvent(compl);
            }
            
            resetAbortAndStopFlags();
        }
        
        private void createDataCapturer() 
            throws AcsJResourceErrorEx, SettingDataCapturerIdErrorEx {
            
            String dataCapturerName = opState.array.createDataCaptureName();
            DynamicResource<DataCapturer> resource = 
                new DynamicResource<DataCapturer>(container,
                                                  dataCapturerName,
                                                  "IDL:alma/offline/DataCapturer:1.0");
            resMng.acquireResource(resource);
            DataCapturer dc = resource.getComponent();
            
            DataCapturerId infoDataCapturer = new DataCapturerId();
            infoDataCapturer.array = array.getArrayName();
            infoDataCapturer.name = dataCapturerName;
            infoDataCapturer.session = getSessionEntityRef();
            infoDataCapturer.schedBlock = getSchedBlockEntityRef();
            infoDataCapturer.correlatorArrayStream = new BDDStreamInfo();
            infoDataCapturer.correlatorArrayStream.distributor = array.getCorrelatorBulkDataDistributor();
            infoDataCapturer.correlatorArrayStream.spectralFlow = CorrelatorBDDFlowNumbers.getSpectralFlowNumber();
            infoDataCapturer.correlatorArrayStream.channelAvgFlow = CorrelatorBDDFlowNumbers.getChannelAverageFlowNumber();
            infoDataCapturer.correlatorArrayStream.wvrFlow = CorrelatorBDDFlowNumbers.getWVRFlowNumber();
            infoDataCapturer.totalPowerArrayStream = new BDDStreamInfo();
            infoDataCapturer.totalPowerArrayStream.distributor = array.getTotalPowerBulkDataDistributor();
            infoDataCapturer.totalPowerArrayStream.spectralFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            infoDataCapturer.totalPowerArrayStream.channelAvgFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            infoDataCapturer.totalPowerArrayStream.wvrFlow = CorrelatorBDDFlowNumbers.INVALID_FLOW_NUMBER;
            
            dc.setDataCapturerId(infoDataCapturer);
        }
        
        private void destroyDataCapturer() {
            resMng.freeResource(opState.array.getDataCapturerName());
        }        
    }
    
    ////////////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////////////    
    
    public AutomaticArrayOperationalState(ArrayInternal array) {
        super(array, State.Operational_NoError);
        this.array = array;
        this.dcDestructors = new HashMap<String, DCDestructor>();
        this.asdmEntRef = new IDLEntityRef();
        this.asdmEntRef.entityId = "";
        this.asdmEntRef.entityTypeName = "";
        this.asdmEntRef.instanceVersion = "";
        this.asdmEntRef.partId = "";        
    }

    ////////////////////////////////////////////////////////////////////
    // Overriden methods
    ////////////////////////////////////////////////////////////////////    
    
    @Override
    public void shutdownPass1() throws AcsJInvalidRequestEx {
        if (execThread != null && execThread.isAlive())
            execThread.interrupt();
        deactivateOffshoots();
        array.setState("SHUTTINGDOWN_STATE");
        array.shutdownPass1();
    }

    @Override
    public AntennaStateEvent[] getAntennaStates() {
        Map<String, Antenna> antennas = array.getAntennaMap();
        AntennaStateEvent[] states = new AntennaStateEvent[antennas.size()];
        Iterator<Antenna> iter = antennas.values().iterator();
        int i = 0;
        while(iter.hasNext()) {
            states[i] = iter.next().getAntennaState();
            i++;
        }
        return states;
    }

    @Override
    public void observe(IDLEntityRef sbId, IDLEntityRef sessionId, long when)
        throws AcsJSBExecutionErrorEx {
        
        String fn = "observe";
        String lp = "{"+cn+"::"+fn+"} ";
        logger.fine(lp+"ENTRY; sbId.endityId="+sbId.entityId+
                ", sbId.entityTypeName="+sbId.entityTypeName+
                ", sbId.instanceVersion="+sbId.instanceVersion+
                ", sbId.partId="+sbId.partId+
                "; sessionId.entityId="+sessionId.entityId+
                ", sessionId.entityTypeName="+sessionId.entityTypeName+
                ", sessionId.instanceVersion="+sessionId.instanceVersion+
                ", sessionId.partId"+sessionId.partId+
                "; when="+when);
        
        setAbortionFlag(false);
        setStopFlag(false);
        
        this.sbEntRef = sbId;
        this.sessionEntRef = sessionId;
        
        this.dataCaptureIsReady = false;
        this.asdmArchived = false;
        
        this.asdmEntRef = getASDMUID();

        int attempts = 10;
        while(attempts > 0) {
            if (!isRunningSB()) {
                executor = new SBExecutor(this);
                execThread = array.getContainerServices()
                                  .getThreadFactory()
                                  .newThread(executor);
                execThread.start();
                return;
            } else {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    logger.severe("AutomaticArrayOperationalState has been interrupted" +
                            "while waiting for execution thread to be idle.");
                    break;
                }
                attempts--;
            }
        }

        AcsJSBExecutionErrorEx ex = new AcsJSBExecutionErrorEx();
        ex.setProperty("Message", "This array is already running an SB. " + 
                "Only one SB can be running in a given array at a certain time.");
        throw ex;

    }

    @Override
    public void observeNow(IDLEntityRef sbId, IDLEntityRef sessionId)
        throws AcsJSBExecutionErrorEx {
        observe(sbId, sessionId, 0L);
    }

    @Override
    public void stop() {
        setStopFlag(true);
        if (thOffshoot != null) {
        	thOffshoot.abortScan();
        }
        if (th7mOffshoot != null) {
        	th7mOffshoot.abortScan();
        }
        if (sfiOffshoot != null) {
        	sfiOffshoot.abortScan();
        }
        if (opOffshoot != null) {
        	opOffshoot.abortScan();
        }
        if (tpOffshoot != null) {
        	tpOffshoot.abortScan();
        }
        if (ahOffshoot != null) {
        	ahOffshoot.abortScan();
        }
    }

    @Override
    public void stopNow() {
        // Get the ScriptExecutor
        ContainerServices container = array.getContainerServices();
        ResourceManager resMng = ResourceManager.getInstance(container);
        String scriptExecName = array.getScriptExecutorName();
        ScriptExecutor script = null;
        try {
            script = resMng.getComponent(scriptExecName);
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Error getting ScriptExecutor resource from ResourceManager. ";
            msg += "It seems that the AutomaticArray component has not been properly initialized. ";
            msg += "This is an internal error.";
            throw new AssertionError(msg);
        }
        // Abort the running SB
        setAbortionFlag(true);
        
        // The way to handle this in the future will be:
        // (1) use the flags to check at certain points and throw an Abortion
        //     execution,
        // (2) abortion will abort the data execution, either from correlator,
        //     or from square law detection,
        // (3) wait for a while, and kill the script execution process, if
        //     still alive
        // The science hierarchy (obs. mode, controller, devices) will implement
        // an abort() function.
        
        // This will kill the script execution process
        if (isRunningSB())
            script.abort();
        
        // Send the ExecBlockEndedEvent, if it hasn't been sent before
        sendAbortedExecBlockEndedEvent();
    }

    @Override
    public boolean isRunningSB() {
        return (execThread != null) && execThread.isAlive(); 
    }

    @Override
    public void dataCaptureReady() {
        try {
            lock.lock();
            dataCaptureIsReady = true;
            cv.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void asdmArchived(ASDMArchivedEvent event) {
        
        logger.info("Received ASDM archived notification for DataCapturer '" +
                event.workingDCId.name + "'.");
        DCDestructor dcd;
        String dcn = event.workingDCId.name;
        dcd = dcDestructors.get(dcn);
        if (dcd != null) {
            dcd.asdmArchived();
            try {
                dcd.join();
            } catch (InterruptedException ex) {
                // This shouldn't happen...
                ex.printStackTrace();
            }
            dcDestructors.remove(dcn);
        } else {
            logger.fine("Received ASDMArchivedEvent with invalid " +
                    "DataCapturerId. The ID is '" + dcn + 
                    "', which doesn't map to any DataCapturer started by this array.");
        }
    }

    @Override
    public void subScanDataReady() {
        logger.finest("subScanDataReady called...");
    }

    @Override
    public void beginExecution()
        throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx, AcsJUnfinishedExecBlockEx {

        // It doesn't make any sense to call beginExecution if there's no
        // currently running SB.
        if (execThread == null || !execThread.isAlive()) {
            String msg = "There is no currently running SB. ";
            msg += "This method can only be called if a SB is running, i.e., ";
            msg += "the observe() or observeNow() method has been called and ";
            msg += "beginExecution() is invoked as a callback from an obs. script.";
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Message", msg);
            throw ex;
        }
        
        if (isInsideExecBlock()) {
            String msg = "Previous execution block hasn't been properly finished. ";
            msg += "Previous beginExecution() command hasn't been matched with ";
            msg += "a corresponding endExecution() command.";
            AcsJUnfinishedExecBlockEx ex = new AcsJUnfinishedExecBlockEx();
            ex.setProperty("Details", msg);
            logger.warning(msg);
            throw ex;
        }
        setInsideExecBlock(true);
        
        beginExecution(getASDMEntityRef(), 
                       getSchedBlockEntityRef(), 
                       getSessionEntityRef(),
                       schedBlock);
    }
    
    @Override
    public void endExecution()
    	throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx {
    	endExecution(new Completion(0, 0, 0, new alma.ACSErr.ErrorTrace[0]));
    }

    public void endExecution(Completion compl)
    	throws AcsJInvalidRequestEx, AcsJSBExecutionErrorEx {
        
        // It doesn't make any sense to call endExecution if there's no
        // currently running SB.
        if (execThread == null || !execThread.isAlive()) {
            String msg = "There is no currently running SB. ";
            msg += "This method can only be called if a SB is running, i.e., ";
            msg += "the observe() or observeNow() method has been called and ";
            msg += "endExecution() is invoked as a callback from an obs. script.";
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Message", msg);
            throw ex;
        }
        
        if (!isInsideExecBlock()) {
            String msg = "No matching beginExecution has been commanded.";
            AcsJInvalidRequestEx ex = new AcsJInvalidRequestEx();
            ex.setProperty("Details", msg);
            logger.warning(msg);
            throw ex;
        }
        
        endExecution(getASDMEntityRef(), 
                     getSchedBlockEntityRef(), 
                     getSessionEntityRef(),
                     compl);

        setInsideExecBlock(false);
    }

    /**
     * Call endExecution() but don't propagate the exceptions. This function is used
     * to end the execution in SchedBlocks where beginExecution() was called from the script but
     * endExecution() was not, because either the script failed before this point, or because
     * the script is simply wrong. In this case, we try to finish the execution cleanly.
     * As this function is called from the error handling routines, if there is an error when
     * calling endExecution, we simply log the problem and carry on.
     * 
     * @param compl ACSErr Completion
     */
    public void endExecutionForUnfinishedSchedBlock(Completion compl) {
        try {
            endExecution(compl);
        } catch (AcsJInvalidRequestEx ex) {
            logger.warning("Ending execution for unfinished SchedBlock failed.");
            ex.log(logger);
        } catch (AcsJSBExecutionErrorEx ex) {
            logger.warning("Ending execution for unfinished SchedBlock failed.");
            ex.log(logger);
        }
    }
    
    public IDLEntityRef getASDMEntityRef() {
        return asdmEntRef;
    }

    public IDLEntityRef getSchedBlockEntityRef() {
        return sbEntRef;
    }

    public IDLEntityRef getSessionEntityRef() {
        return sessionEntRef;
    }

    ////////////////////////////////////////////////////////////////////
    // ObservingModeArray methods
    ////////////////////////////////////////////////////////////////////
    
    public IDLEntityRef getASDMEntRef() {
        return asdmEntRef;
    }
    
    ////////////////////////////////////////////////////////////////////
    // Protected methods
    ////////////////////////////////////////////////////////////////////    

    protected void sendExecBlockEndedEvent(Completion compl) {
        
        assert sbEntRef != null && sessionEntRef != null && asdmEntRef != null;
        sendExecBlockEndedEvent(compl, sbEntRef, sessionEntRef, asdmEntRef);
    }
    
    protected void sendAbortedExecBlockEndedEvent() {
        assert sbEntRef != null && sessionEntRef != null && asdmEntRef != null;
        Completion compl = new Completion();
        compl.code = -1;
        compl.type = -1;
        compl.timeStamp = 0L;
        compl.previousError = new ErrorTrace[0];
        sendExecBlockEndedEvent(compl, sbEntRef, sessionEntRef, asdmEntRef, true);        
    }
    
    private String getObsScript(SchedBlock sb) throws AcsJSBExecutionErrorEx {
        
        String executionScript = null;
        String obsProcScript = sb.getObsProcedure().getObsProcScript();
        if (obsProcScript.length() == 0){
            String msg = "There is no observing script in this SB. " +
                         "The element ObsProcScript in the SB is required to have " +
                         "either a complete Python script or the name of an installed " +
                         "Python script.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx();
            exHlp.setProperty("Message", msg);
            throw exHlp;
        }
        if (!sb.hasStandardMode()) {
            String msg = "StandardMode has not been set." +
                         "The element StandardMode in the SB is required to have content, " +
                         "which should be either 'true' or 'false'.";
            logger.fine(msg);
            AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx();
            exHlp.setProperty("Message", msg);
            throw exHlp;
        }
        if (sb.getStandardMode()) {
            // For a standard mode, the ObsProcScript is the name of the standard script.
            String scriptName = obsProcScript;
            String fullPathFilename = Util.findFile(scriptName);
            if (fullPathFilename == null) {
                String msg = "There is no such standard script as " + scriptName + ". " +
                             "The standard script specified in the SB has not been found " +
                             "in the system.";
                logger.fine(msg);
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx();
                exHlp.setProperty("Message", msg);
                throw exHlp;
            }
            try {
                executionScript = Util.readFile(fullPathFilename);
            } catch (IllegalArgumentException err) {
                String msg = "Error reading standard script " + fullPathFilename;
                logger.fine(msg);
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx();
                exHlp.setProperty("Message", msg);
                throw exHlp;
            }
            if (executionScript.length() == 0) {
                String msg = "There is no text in standard script " + fullPathFilename;
                logger.fine(msg);
                AcsJSBExecutionErrorEx exHlp = new AcsJSBExecutionErrorEx();
                exHlp.setProperty("Message", msg);
                throw exHlp;
            }
            logger.fine("Using standard script:\n" + executionScript);
        } else {
            // For a non-standard mode the script is in the scheduling block.
            executionScript = obsProcScript;
            logger.fine("Using non-standard script from Scheduling Block:\n" + executionScript);
        }   
        
        return executionScript;
    }
    
}

// __oOo__
