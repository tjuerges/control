/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */
package alma.Control.Array;

/**
 * This class manages the bulk data stream flow numbers that
 * the correlators must use to send the different types of data to
 * the bulk data distributors.
 * <P>
 * In the Bulk Data System, data is sent in streams, which incorporate
 * one or more independent flows. The correlators use different flows to
 * send full spectral data, channel average data, and water vapor radiometry
 * data.
 * <P>
 * CONTROL decides which flow numbers must be used for each different type
 * of data, and pass this information to CORR, ACACORR, TELCAL and PIPELINE
 * by means of the BDDStreamInfo structure. CONTROL assumes this reponsibility
 * for the sake of the future implementation of subarrays. It is likely that subarrays be
 * implemented using a different set of flows for each subarray, each array using
 * a single Stream (and in consequence, a single Distributor). Other architectures are
 * possible, and the BDDStreamInfo structure is flexible in this sense.
 * <P>
 * For now the management of the flow numbers is defined statically in this class.
 * 
 * @author rhiriart
 *
 */
public class CorrelatorBDDFlowNumbers {

	/** Channel average data flow number */
	private static final short CHANNEL_AVG_FLOW_NUMBER = 1;
	
	/** Full spectral data flow number */
	private static final short SPECTRAL_FLOW_NUMBER = 2;
	
	/** Water vapor radiometry data flow number */
	private static final short WVR_FLOW_NUMBER = 3;
	
	/** Invalid flow number */
	public static final short INVALID_FLOW_NUMBER = -1;
	
	/**
	 * Get the channel average data flow number.
	 * @return Channel average flow number
	 */
	public static short getChannelAverageFlowNumber() {
		return CHANNEL_AVG_FLOW_NUMBER;
	}
	
	/** 
	 * Get the full resolution spectral data flow number.
	 * @return Spectral data flow number
	 */
	public static short getSpectralFlowNumber() {
		return SPECTRAL_FLOW_NUMBER;
	}
	
	/**
	 * Get the water vapor radiometry data flow number.
	 * @return WVR flow number
	 */
	public static short getWVRFlowNumber() {
		return WVR_FLOW_NUMBER;
	}
}
