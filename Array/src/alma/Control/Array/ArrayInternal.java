package alma.Control.Array;

import java.util.Map;
import java.util.logging.Logger;

import alma.Control.Antenna;
import alma.Control.AntennaCharacteristics;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.SiteData;
import alma.Control.WeatherStationCharacteristics;
import alma.acs.container.ContainerServices;

public interface ArrayInternal {

    String getDataCapturerName();
    
    String createDataCaptureName();
    
    String getScriptExecutorName();
    
    /** Get the ExecutionState name for this automatic array. */
    String createExecutionStateName();
    
    String[] getAntennas();

    Map<String, Antenna> getAntennaMap();
    
    void setState(String state);
    
    void shutdownPass1();

    void shutdownPass2();

    void startupPass1();

    void startupPass2();

    void waitShutdownPass1();
        
    String getArrayComponentName();
    
    String getArrayName();
    
    ContainerServices getContainerServices();
    
    Logger getLogger();
    
    AntennaCharacteristics[] getAntennaConfigurations();
    
    WeatherStationCharacteristics[] getWeatherStationConfigurations();
    
    SiteData getSiteData();
    
    String getConfigurationName();
    
    String getTotalPowerBulkDataDistributor();
    
    String getCorrelatorBulkDataDistributor();
    
    ResourceId[] getPhotonicReferences();
    
    CorrelatorType getCorrelatorType();

}
