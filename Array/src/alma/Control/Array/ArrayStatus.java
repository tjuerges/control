/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 */
package alma.Control.Array;

import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.AntennaShutterStatus;
import alma.Control.AntennaStatus;
import alma.Control.AtmosphericConditions;
import alma.Control.CorrelatorStatus;
import alma.Control.ExecBlockStatus;
import alma.Control.LocalOscillatorStatus;
import alma.Control.PointingStatus;
import alma.Control.ScanStatus;
import alma.Control.SubscanStatus;

/**
 * This interface defines several functions that report array level
 * status data.
 * These functions are used by the Array Status GUI.
 */
public interface ArrayStatus {

    boolean isScanStatusAvailable();
    ScanStatus getScanStatus() throws AcsJInvalidRequestEx;;

    boolean isSubscanStatusAvailable();
    SubscanStatus getSubscanStatus() throws AcsJInvalidRequestEx;;
    
    boolean isAntennaShutterStatusAvailable();
    AntennaShutterStatus getAntennaShutterStatus() throws AcsJInvalidRequestEx;;

    boolean isAntennaStatusAvailable();
    AntennaStatus getAntennaStatus() throws AcsJInvalidRequestEx;;

    boolean isAtmosphericConditionsAvailable();
    AtmosphericConditions getAtmosphericConditions() throws AcsJInvalidRequestEx;;

    boolean isCorrelatorStatusAvailable();
    CorrelatorStatus getCorrelatorStatus() throws AcsJInvalidRequestEx;;

    boolean isExecBlockStatusAvailable();
    ExecBlockStatus getExecBlockStatus() throws AcsJInvalidRequestEx;;

    boolean isLocalOscillatorStatusAvailable();
    LocalOscillatorStatus getLocalOscillatorStatus() throws AcsJInvalidRequestEx;;

    boolean isPointingStatusAvailable();
    PointingStatus getPointingStatus() throws AcsJInvalidRequestEx;;

}
