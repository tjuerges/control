/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.ArrayExceptions.InvalidRequestEx;
import alma.ArrayExceptions.UnfinishedExecBlockEx;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.Control.AbortionException;
import alma.Control.Antenna;
import alma.Control.AntennaCharacteristics;
import alma.Control.AntennaShutterStatus;
import alma.Control.AntennaStateEvent;
import alma.Control.AntennaStatus;
import alma.Control.AtmosphericConditions;
import alma.Control.AutomaticArray2Operations;
import alma.Control.CalDeviceData;
import alma.Control.Completion;
import alma.Control.CorrelatorStatus;
import alma.Control.CorrelatorType;
import alma.Control.ErrorEnum;
import alma.Control.ExecBlockStatus;
import alma.Control.ExecutionException;
import alma.Control.IDLError;
import alma.Control.IDLResult;
import alma.Control.InaccessibleException;
import alma.Control.InvalidRequest;
import alma.Control.LocalOscillatorStatus;
import alma.Control.OpticalPointingObservingMode;
import alma.Control.PointingStatus;
import alma.Control.ResourceEnum;
import alma.Control.ResourceId;
import alma.Control.ScanStatus;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.SiteData;
import alma.Control.SubscanStatus;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.TotalPowerObservingMode;
import alma.Control.TowerHolographyObservingMode;
import alma.Control.TowerHolography7mObservingMode;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.Common.Name;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.Util;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ReceiverBandMod.ReceiverBand;
import alma.TMCDB.ModelTerm;
import alma.acs.component.ComponentLifecycle;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.offline.ASDMArchivedEvent;

/**
 * This class implements the AutomaticArray2 component
 * (IDL:alma/Control/AutomaticArray2:1.0).
 * 
 * The <CODE>AutomaticArray2Impl</CODE> class maintains a few state variables 
 * on its own:
 * <OL>
 *   <LI>The antennas
 *   <LI>Information about the antennas, in the form of an array of
 *       <CODE>AntennaCharacteristics</CODE>.
 *   <LI>Information about weather station.
 *   <LI>Information about the site.
 * </OL>
 * 
 * Besides this, it works with the following classes to implement its
 * functionality:
 * <OL>
 *   <LI>A ResourceManager singleton object, which is used to manage
 *       shared references to external ACS components.
 *   <LI>Classes that represent the state of the AutomaticArray.
 *   <LI>Observing mode Offshoot classes.
 * </OL>
 *  
 * The resource manager provides a way to manage references to external
 * ACS components which are used by more than one of the classes that implement the
 * AutomaticArray. As it is a Singleton, we avoid having to pass
 * references all over the code. The ResourceManager also provide
 * a way to handle critical and non-critical components, static and
 * dynamic components, and a way to perform asynchronous initialization
 * operations over the external components.
 *  
 * The state classes are part of an application of the State design
 * pattern. The state machine for the AutomaticArray consist of the following
 * states:
 * <UL>
 *   <LI>SHUTDOWN
 *   <LI>STARTING_UP
 *   <LI>OPERATIONAL
 *   <LI>SHUTTING_DOWN
 *   <LI>ERROR
 * </UL>
 * Initially the AutomaticArray is in the SHUTDOWN state. The transitions
 * are as follows:
 * <UL>
 *   <LI><CODE>startingUpPass1()</CODE>: SHUTDOWN to STARTING_UP
 *   <LI><CODE>startingUpPass2()</CODE>: STARTING_UP to OPERATIONAL
 *   <LI><CODE>shuttingDownPass1()</CODE>: {OPERATIONAL, ERROR} to SHUTTING_DOWN
 *   <LI><CODE>shuttingDownPass2()</CODE>: SHUTTING_DOWN to SHUTDOWN
 *   <LI>Certain exceptions trigger the transition OPERATIONAL to ERROR  
 * </UL>
 * 
 * Note that this is the "internal" or implementation-level state machine
 * for this component. This component also present an "external" state machine
 * defined in the <CODE>Controller</CODE> interface.
 * 
 * One of the responsibilities of the AutomaticArray is to direct the
 * execution of Scheduling Blocks. As part of the execution, the AutomaticArray
 * will send events to CONTROL external notification channel and call
 * OFFLINE/DataCapturer methods. Execution of SBs is implemented in the
 * OPERATIONAL state (class <CODE>AutomaticArrayOperationalState</CODE>).
 * Calls to DataCapturer are implemented in the OPERATIONAL state (see
 * <CODE>beginExecution()</CODE> and <CODE>endExecution()</CODE>) and in the 
 * observing mode classes, which are implemented as ACS Offshoots. 
 * 
 * @version $Id$
 * @author rhiriart
 * @see alma.Control.Common.ResourceManager
 * @see alma.Control.Array.AutomaticArrayShutdownState
 * @see alma.Control.Array.AutomaticArrayStartingUpState
 * @see alma.Control.Array.AutomaticArrayOperationalState
 * @see alma.Control.Array.AutomaticArrayShuttingDownState
 * @see alma.Control.Array.SingleFieldInterferometryObservingModeImpl
 * @see alma.Control.Array.TowerHolographyObservingModeImpl
 * @see alma.Control.Array.LOObservingModeImpl
 */
public class AutomaticArrayImpl2 implements AutomaticArray2Operations,
        ComponentLifecycle, ArrayInternal {

    private AutomaticArrayState SHUTDOWN_STATE;
    private AutomaticArrayState STARTINGUP_STATE;
    private AutomaticArrayState OPERATIONAL_STATE;  
    private AutomaticArrayState SHUTTINGDOWN_STATE;
    
    private ContainerServices container;
    private String name;
    private String parentName;
    
    /** Map with the Antenna components. The keys are the antenna names
     * (without CONTROL prefix).
     */
    private Map<String, Antenna> antennas;    
    private ResourceManager resMng;
    private Logger logger;
    private AutomaticArrayState state;
    
    private String scriptExecutorName;
    private int dataCapturerNumber;
    private String dataCapturerName;
    private String execStateName;
    private AntennaCharacteristics[] antennaCharacteristics;
    private WeatherStationCharacteristics[] weatherStationCharacteristics;
    private SiteData siteData;
    private String configurationName;
    
    private TotalPowerObservingMode tpOffshoot;
    private String correlatorBulkDataDistributor;
    private String totalPowerBulkDataDistributor;
    private ResourceId[] photonicReferences;
    private CorrelatorType correlatorType;
    
    public AutomaticArrayImpl2() {
        this.antennas = new HashMap<String, Antenna>();
        this.name = "";     
    }
        
    ////////////////////////////////////////////////////////////////////
    // ComponentLifecycle operations
    ////////////////////////////////////////////////////////////////////

    public void initialize(ContainerServices containerServices)
        throws ComponentLifecycleException {
        
        this.container = containerServices;
        this.logger = container.getLogger();
        
        logger.fine("initialize called");
        
        SHUTDOWN_STATE = new AutomaticArrayShutdownState(this);
        STARTINGUP_STATE = new AutomaticArrayStartingUpState(this);
        OPERATIONAL_STATE = new AutomaticArrayOperationalState(this);   
        SHUTTINGDOWN_STATE = new AutomaticArrayShuttingDownState(this);           
        
        this.resMng = ResourceManager.getInstance(container);        
        this.scriptExecutorName = name() + "/SCRIPTEXEC";
        this.state = SHUTDOWN_STATE;
        this.name = name().replaceFirst("CONTROL/", "");
        this.dataCapturerNumber = 0;
    }
    
    public void aboutToAbort() {
        logger.finest("aboutToAbort called");
    }

    public void cleanUp() {
        logger.finest("cleanUp called");
    }

    public void execute() throws ComponentLifecycleException {
        logger.finest("execute called");
    }
    
    ////////////////////////////////////////////////////////////////////
    // ACSComponentOperations operations
    ////////////////////////////////////////////////////////////////////
    
    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }

    public String name() {
        return container.getName();
    }

    ////////////////////////////////////////////////////////////////////
    // AutomaticArray operations
    ////////////////////////////////////////////////////////////////////

    public void setBulkDataDistributors(String corrBulkDataDistr, String totalPowerBulkDataDistr) {
        correlatorBulkDataDistributor = corrBulkDataDistr;
        totalPowerBulkDataDistributor = totalPowerBulkDataDistr;
    }
    
    public void setAntennaList(String[] list) {
        for(int i=0; i<list.length; i++)
            antennas.put(list[i], null);
    }

    public void setConfigInfo(String configurationName,
            AntennaCharacteristics[] antennaCharacs,
            WeatherStationCharacteristics[] weatherStCharacs,
            SiteData siteData,
            ResourceId[] photonicReferences,
            CorrelatorType correlatorType) {
        this.antennaCharacteristics = antennaCharacs;
        this.weatherStationCharacteristics = weatherStCharacs;
        this.siteData = siteData;
        this.configurationName = configurationName;
        this.photonicReferences = photonicReferences;
        this.correlatorType = correlatorType;
    }
    
    public void beginExecution() throws ExecutionException, UnfinishedExecBlockEx {
        try {
            state.beginExecution();
        } catch (AcsJInvalidRequestEx ex) {
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        } catch (AcsJSBExecutionErrorEx ex) {
            logger.severe("SB execution error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        } catch (AcsJUnfinishedExecBlockEx ex) {
            throw ex.toUnfinishedExecBlockEx();
        }
    }
    
    public void endExecution(Completion statusCode, String message)
        throws ExecutionException {
        try {
            state.endExecution();
        } catch (AcsJInvalidRequestEx ex) {
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        } catch (AcsJSBExecutionErrorEx ex) {
            logger.severe("SB execution error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        }
    }

    public String getDCName() throws InaccessibleException {
        return dataCapturerName;
    }

    public String getExecutionStateName() {
        return execStateName;
    }

    public SingleFieldInterferometryObservingMode getSFIOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getSFIOffshoot();
    }

    public TowerHolographyObservingMode getTowerHolographyOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getTowerHolographyOffshoot();
    }

    public TowerHolography7mObservingMode getTowerHolography7mOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getTowerHolography7mOffshoot();
    }

    public OpticalPointingObservingMode getOpticalPointingOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getOpticalPointingOffshoot();
    }
    
    public TotalPowerObservingMode getTotalPowerOffshoot()
    	throws ObsModeInitErrorEx, AbortionException {
        return state.getTotalPowerOffshoot();
    }
    
    public alma.Control.AstronomicalHolography getAstronomicalHolographyOffshoot()
        throws ObsModeInitErrorEx, AbortionException {
        return state.getAstronomicalHolographyOffshoot();
    }

   public void reportScriptError(String message) {
        // Don't quite understand this method. Who should this report to?
    }

    public void dataCaptureReady() {
        state.dataCaptureReady();
    }
    
    public void subScanDataReady() {
        state.subScanDataReady();
    }

    public void asdmArchived(ASDMArchivedEvent event) {
        logger.info("ASDM archived notification from ExecutionState has been received.");
        state.asdmArchived(event);
    }
    
    ////////////////////////////////////////////////////////////////////
    // Controller operations
    ////////////////////////////////////////////////////////////////////
    
    public IDLError[] getBadResourcesIDL() {
        String[] errors = resMng.getErroneousResources();
        IDLError[] idlErrors = new IDLError[errors.length];
        for(int i=0; i<errors.length; i++) {
            // (RHV) I don't quite like to keep enumerations of all possible
            // resources and all possible errors. I think it is preferrable to
            // just retrieve a polymorphic list of exceptions.
            // In order to maintain the IDL interface, I'm doing sort of a
            // conversion here, but I hope to modify this later.
            idlErrors[i] = new IDLError(errors[i],
                                        ResourceEnum.AUTOMATIC_ARRAY,
                                        ErrorEnum.INTERNAL_ERROR,
                                        0L,
                                        "");
        }
        return idlErrors;
    }

    public String getComponentName() {
        return name();
    }

    public int getResourceTypeValue() {
        return 0;
    }

    public int getStateValue() {
        return state.getStateValue();
    }

    public int getSubstateValue() {
        return state.getSubstateValue();
    }

    public IDLResult reportRemoteError(String resourceName,
            ResourceEnum resourceType, ErrorEnum errorType, long time,
            String message) {
        // (RHV) Not quite understand this method.
        return null;
    }

    public void reportRemoteStateChange(String resourceName,
            ResourceEnum resourceType, SystemState newState,
            SystemSubstate newSubstate) {
        // (RHV) Not quite understand this method.
    }

    public void setParentName(String name) {
        this.parentName = name;
    }

    public void shutdownPass1() {
        try {
            state.shutdownPass1();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);            
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void shutdownPass2() {
        try {
            state.shutdownPass2();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void startupPass1() {
        try {
            state.startupPass1();
        } catch (AcsJInitializationErrorEx ex) {
            logger.severe("Initialization error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInitializationErrorEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void startupPass2() {
        try {
            state.startupPass2();
        } catch (AcsJInitializationErrorEx ex) {
            logger.severe("Initialization error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInitializationErrorEx();
            // However, the IDL for this component should be changed
            // first.
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    public void waitShutdownPass1() {
        try {
            state.waitShutdownPass1();
        } catch (AcsJInvalidRequestEx ex) {
            logger.severe("Invalid request error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
            // An ACSError exception should be thrown:
            // throw ex.toInvalidRequestEx();
            // However, the IDL for this component should be changed
            // first.
        }
    }

    ////////////////////////////////////////////////////////////////////
    // AutomaticArrayCommand operations
    ////////////////////////////////////////////////////////////////////
    
    public void observe(IDLEntityRef sbId, IDLEntityRef sessionId, long when)
            throws InaccessibleException, InvalidRequest {
        try {
            state.observe(sbId, sessionId, when);
        } catch (AcsJSBExecutionErrorEx ex) {
            logger.severe("SB execution error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        }
    }

    public void observeNow(IDLEntityRef sbId, IDLEntityRef sessionId)
            throws InaccessibleException, InvalidRequest {
        try {
            state.observeNow(sbId, sessionId);
        } catch (AcsJSBExecutionErrorEx ex) {
            logger.severe("SB execution error:\n" + 
                          Util.getNiceErrorTraceString(ex.getErrorTrace()));
            ex.log(logger);
        }
    }

    public void stop() throws InaccessibleException, InvalidRequest {
        state.stop();
    }

    public void stopNow() throws InaccessibleException, InvalidRequest {
        state.stopNow();
    }

    ////////////////////////////////////////////////////////////////////
    // AutomaticArrayMonitor operations
    ////////////////////////////////////////////////////////////////////
    
    public IDLEntityRef getExecBlock() throws InaccessibleException {
        return state.getASDMEntityRef();
    }

    public IDLEntityRef getSB() throws InaccessibleException {
        return state.getSchedBlockEntityRef();
    }

    public IDLEntityRef getSession() throws InaccessibleException {
        return state.getSessionEntityRef();
    }

    public ResourceId[] getAntennaComponents() {
        ResourceId[] resIds = new ResourceId[antennas.size()];
        Iterator<String> iter = antennas.keySet().iterator();
        int i = 0;
        while (iter.hasNext()) {
            String an = iter.next();
            resIds[i] = new ResourceId();
            resIds[i].ResourceName = an;
            resIds[i].ComponentName = Name.ControlPrefix + an;
            i++;
        }
        return resIds;
    }

    public AntennaStateEvent[] getAntennaStates() {
        try {
            return state.getAntennaStates();
        } catch (InvalidRequestException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String[] getAntennas() {
        List<String> antennaNames = new ArrayList<String>();
        for (String an : antennas.keySet()) {
            antennaNames.add(an);
        }
        Collections.sort(antennaNames);
        int i = 0;
        for (String an : antennaNames) {
            logger.info("Antenna " + i + ": " + an);
            i++;
        }
        return antennaNames.toArray(new String[0]);
    }

    public String getArrayComponentName() {
        return name();
    }

    public String getArrayName() {
        return name;
    }

    public SystemState getArrayState() {
        return SystemState.from_int(state.getStateValue());
    }

    public SystemSubstate getArraySubstate() {
        return SystemSubstate.from_int(state.getSubstateValue());
    }

    public String[] getBadResources() {
        return resMng.getErroneousResources();
    }

    public String getName() {
        return name();
    }

    public boolean isBusy() {
        return state.isRunningSB();
    }

    public String getRunningSB() {
        return state.getSchedBlockEntityRef().entityId;
    }
    
    public boolean isAutomatic() {
        return true;
    }
    
    public boolean isManual() {
        return false;
    }

    ////////////////////////////////////////////////////////////////////
    // ArrayInternal operations
    ////////////////////////////////////////////////////////////////////

    public String getTotalPowerBulkDataDistributor() {
        return totalPowerBulkDataDistributor;
    }
    
    public String getCorrelatorBulkDataDistributor() {
        return correlatorBulkDataDistributor;
    }

    public ResourceId[] getPhotonicReferences() {
        return photonicReferences;
    }

    public CorrelatorType getCorrelatorType() {
    	return correlatorType;
    }
    
    /**
     * Creates a name for the DataCapturer component.
     * 
     * Everytime this function is called a new name is created. The name for
     * DataCapturer is created by glueing the array's name with an
     * identifier which is unique for the life of the array. The identifier
     * has the form DCXXX with XXX a number. For example:
     *   CONTROL/Array001/DC000
     *   CONTROL/Array001/DC001
     *   ...
     *   
     * Note: This convention is temporary. The DataCapturer name will come
     * from the TMCDB in the future.
     */
    public String createDataCaptureName() {
        // dataCapturerName = Name.genDataCaptureName();
        // dataCapturerName = name() + "/DATACAPTURER";
        dataCapturerName = String.format("%s/DC%03d", name(), dataCapturerNumber++);
        return dataCapturerName;
    }

    public String getScriptExecutorName() {
        return scriptExecutorName;
    }

    public void setState(String stateName) {
        // This possibly could be changed to use Java reflection.
        if (stateName.equals("SHUTDOWN_STATE"))
            this.state = SHUTDOWN_STATE;
        else if (stateName.equals("STARTINGUP_STATE"))
            this.state = STARTINGUP_STATE;
        else if (stateName.equals("OPERATIONAL_STATE"))
            this.state = OPERATIONAL_STATE;
        else if (stateName.equals("SHUTTINGDOWN_STATE"))
            this.state = SHUTTINGDOWN_STATE;
    }

    public Logger getLogger() {
        return logger;
    }

    public ContainerServices getContainerServices() {
        return container;
    }

    public Map<String, Antenna> getAntennaMap() {
        return antennas;
    }

    ////////////////////////////////////////////////////////////////////
    // AutomaticArrayInternal operations
    ////////////////////////////////////////////////////////////////////
    
    public String createExecutionStateName() {
        // execStateName = Name.genExecutionStateName();
        execStateName = name() + "/EXECSTATE";
        return execStateName;
    }

    public String getDataCapturerName() {
        if (dataCapturerName == null) return createDataCaptureName();
        return dataCapturerName;
    }

    public AntennaCharacteristics[] getAntennaConfigurations() {
        return antennaCharacteristics;
    }
    
    public String getConfigurationName() {
        return configurationName;
    }
    
    public WeatherStationCharacteristics[] getWeatherStationConfigurations() {
        return weatherStationCharacteristics;
    }
    
    public SiteData getSiteData() {
        return siteData;
    }

    public void reportPointingModel(String antennaName, ReceiverBand band,
            ModelTerm[] pointingModel, ModelTerm[] auxPointingModel) {

        state.reportPointingModel(antennaName, band, pointingModel,
                auxPointingModel);
    }

    public void reportFocusModel(String antennaName, ReceiverBand band,
            ModelTerm[] focusModel) {
        state.reportFocusModel(antennaName, band, focusModel);
    }

    public void reportSubreflectorPosition(String antennaName,
            boolean focusTrackingEnabled, double x, double y, double z,
            double xOffset, double yOffset, double zOffset) {

        state.reportSubreflectorPosition(antennaName, focusTrackingEnabled, x,
                y, z, xOffset, yOffset, zOffset);
    }

    public void reportCalDeviceData(CalDeviceData data) {
        state.reportCalDeviceData(data);
    }
    
	@Override
	public void flagData(boolean startStop, long timeStamp,
			String componentName, String description) {
		state.flagData(startStop, timeStamp, componentName, description);
	}

    ////////////////////////////////////////////////////////////////////
    // ArrayCommand additions to support Array Status GUI
    ////////////////////////////////////////////////////////////////////

    public void closeAntennaShutters() {
        // TODO: build out stub.
    }

    public void moveAntennasToMaintenanceStow() {
        // TODO: build out stub.
    }

    public void moveAntennasToSurvivalStow() {
        // TODO: build out stub.
    }

    public void openAntennaShutters() {
        // TODO: build out stub.
    }

    public void setAntennaDrivesToStandby() {
        // TODO: build out stub.
    }

    @Override
    public boolean isScanStatusAvailable() {
        return state.isScanStatusAvailable();
    }

    @Override
    public ScanStatus getScanStatus() throws InvalidRequestEx {
        try {
            return state.getScanStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isAntennaShutterStatusAvailable() {
        return state.isAntennaShutterStatusAvailable();
    }
    
    @Override
    public AntennaShutterStatus getAntennaShutterStatus() throws InvalidRequestEx {
        try {
            return state.getAntennaShutterStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isAntennaStatusAvailable() {
        return state.isAntennaStatusAvailable();
    }

    @Override
    public AntennaStatus getAntennaStatus()  throws InvalidRequestEx {
        try {
            return state.getAntennaStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isAtmosphericConditionsAvailable() {
        return state.isAtmosphericConditionsAvailable();
    }

    @Override
    public AtmosphericConditions getAtmosphericConditions() throws InvalidRequestEx {
        try {
            return state.getAtmosphericConditions();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isCorrelatorStatusAvailable() {
        return state.isCorrelatorStatusAvailable();
    }

    @Override
    public CorrelatorStatus getCorrelatorStatus() throws InvalidRequestEx {
        try {
            return state.getCorrelatorStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isExecBlockStatusAvailable() {
        return state.isExecBlockStatusAvailable();
    }

    @Override
    public ExecBlockStatus getExecBlockStatus() throws InvalidRequestEx {
        try {
            return state.getExecBlockStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isLocalOscillatorStatusAvailable() {
        return state.isLocalOscillatorStatusAvailable();
    }

    @Override
    public LocalOscillatorStatus getLocalOscillatorStatus() throws InvalidRequestEx {
        try {
            return state.getLocalOscillatorStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isPointingStatusAvailable() {
        return state.isPointingStatusAvailable();
    }

    @Override
    public PointingStatus getPointingStatus() throws InvalidRequestEx {
        try {
            return state.getPointingStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }

    @Override
    public boolean isSubscanStatusAvailable() {
        return state.isSubscanStatusAvailable();
    }

    @Override
    public SubscanStatus getSubscanStatus() throws InvalidRequestEx {
        try {
            return state.getSubscanStatus();
        } catch (AcsJInvalidRequestEx e) {
            e.printStackTrace();
            throw e.toInvalidRequestEx();
        }
    }
}

// __oOo__
