/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui;

import alma.Control.ArrayStatusGui.panels.TopLevelPanel;
import alma.Control.ArrayStatusGui.properties.PropertyCollection;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.util.logging.Level;

import javax.swing.JPanel;

/**
 * Contain all run time information needed by an Array Status GUI.
 */
public class Gui {

    private IPropertyCollection properties;
    private IRuntimeEnvironment runtimeEnv;
    private JPanel topLevelPanel;
    
    public Gui(IRuntimeEnvironment runtimeEnv) {

        final String METHOD_NAME = getClass().getName() + ".Gui()";
        
        this.runtimeEnv = runtimeEnv;
        
        // DEBUG: Comment out "setLevel()" before release.
        this.runtimeEnv.getLogger().setLevel(Level.ALL);        
        this.runtimeEnv.getLogger().fine(METHOD_NAME + ": entering.");
        
        this.properties = new PropertyCollection(runtimeEnv);
        this.topLevelPanel = new TopLevelPanel(runtimeEnv, properties);
        
        this.runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
    }
    
    public JPanel getTopLevelPanel() {
        return topLevelPanel;
    }

} // ArrayStatusGui

//
// O_o
