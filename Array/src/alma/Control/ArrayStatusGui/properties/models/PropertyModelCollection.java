/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.properties.models;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyNames;
import alma.Control.CommonGui.properties.models.IPropertyModelCollection;
import alma.Control.CommonGui.properties.models.PropertyModel;

import java.util.HashMap;

import javax.swing.JComponent;

/**
 * All models for data displayed in the Array Status GUI.
 */
public class PropertyModelCollection implements IPropertyModelCollection {

    private HashMap<ASGPropertyNames, PropertyModel> models = 
                new HashMap<ASGPropertyNames, PropertyModel>();

    public PropertyModelCollection (IRuntimeEnvironment runtimeEnv) {
        models.put(ASGPropertyNames.ACD_STATE, new ACDState(runtimeEnv));
        models.put(ASGPropertyNames.AIRMASS, new Airmass(runtimeEnv));
        models.put(ASGPropertyNames.ANTENNAS_DEGRADED, new AntennasDegraded(runtimeEnv));
        models.put(ASGPropertyNames.ANTENNAS_OFFLINE, new AntennasOffline(runtimeEnv));
        models.put(ASGPropertyNames.ANTENNAS_OPERATIONAL, new AntennasOperational(runtimeEnv));
        models.put(ASGPropertyNames.ANTENNAS_REQUIRED, new AntennasRequired(runtimeEnv));
        models.put(ASGPropertyNames.ANTENNAS_SHUTTER_CLOSED, new AntennasShutterClosed(runtimeEnv));
        models.put(ASGPropertyNames.ANTENNAS_SHUTTER_OPEN, new AntennasShutterOpen(runtimeEnv));
        models.put(ASGPropertyNames.ANTENNAS_TOTAL, new AntennasTotal(runtimeEnv));
        models.put(ASGPropertyNames.AZIMUTH, new Azimuth(runtimeEnv));
        models.put(ASGPropertyNames.BL_APP_STATE, new BLAppState(runtimeEnv));
        models.put(ASGPropertyNames.BL_CORRELATOR_MODE, new BLCorrelatorMode(runtimeEnv));
        models.put(ASGPropertyNames.COMMUNICATION_STATUS, new CommunicationStatus(runtimeEnv));
        models.put(ASGPropertyNames.CROSS_EL_OFFSET, new CrossElOffset(runtimeEnv));
        models.put(ASGPropertyNames.CROSS_EL_VELOCITY, new CrossElVelocity(runtimeEnv));
        models.put(ASGPropertyNames.DECLINATION, new Declination(runtimeEnv));
        models.put(ASGPropertyNames.EL_OFFSET, new ElOffset(runtimeEnv));
        models.put(ASGPropertyNames.EL_VELOCITY, new ElVelocity(runtimeEnv));
        models.put(ASGPropertyNames.ELAPSED_TIME, new ElapsedTime(runtimeEnv));        
        models.put(ASGPropertyNames.ELEVATION, new Elevation(runtimeEnv));        
        models.put(ASGPropertyNames.EXEC_BLOCK_NAME, new ExecBlockName(runtimeEnv));
        models.put(ASGPropertyNames.FEND_LOCK, new FENDLock(runtimeEnv));
        models.put(ASGPropertyNames.LAST_TAU, new LastTau(runtimeEnv));
        models.put(ASGPropertyNames.MAX_ALLOWED_TIME, new MaxAllowedTime(runtimeEnv));
        models.put(ASGPropertyNames.MAX_DEWAR_TEMP, new MaxDewarTemp(runtimeEnv));
        models.put(ASGPropertyNames.MAX_ERROR, new MaxError(runtimeEnv));
        models.put(ASGPropertyNames.ON_SOURCE_COUNT, new OnSourceCount(runtimeEnv));
        models.put(ASGPropertyNames.PI_NAME, new PIName(runtimeEnv));
        models.put(ASGPropertyNames.PROJECT_NAME, new ProjectName(runtimeEnv));
        models.put(ASGPropertyNames.RIGHT_ASCENSION, new RightAscension(runtimeEnv));
        models.put(ASGPropertyNames.SCAN_COUNT, new ScanCount(runtimeEnv));
        models.put(ASGPropertyNames.SCAN_INTENT, new ScanIntent(runtimeEnv));
        models.put(ASGPropertyNames.SCHED_BLOCK_NAME, new SchedBlockName(runtimeEnv));
        models.put(ASGPropertyNames.SCRIPT_NAME, new ScriptName(runtimeEnv));
        models.put(ASGPropertyNames.SKY_FREQ, new SkyFreq(runtimeEnv));
        models.put(ASGPropertyNames.SOURCE_NAME, new SourceName(runtimeEnv));
        models.put(ASGPropertyNames.START_TIME, new StartTime(runtimeEnv));
        models.put(ASGPropertyNames.STATUS_DETAILS, new StatusDetails(runtimeEnv));
        models.put(ASGPropertyNames.STATUS_SUMMARY, new StatusSummary(runtimeEnv));
        models.put(ASGPropertyNames.SUBSCAN_COUNT, new SubscanCount(runtimeEnv));
        models.put(ASGPropertyNames.SUBSCAN_DURATION, new SubscanDuration(runtimeEnv));
        models.put(ASGPropertyNames.SUBSCAN_REMAINING, new SubscanRemaining(runtimeEnv));
        models.put(ASGPropertyNames.SUBSCAN_TOTAL, new SubscanTotal(runtimeEnv));
        models.put(ASGPropertyNames.TIME_TO_SET, new TimeToSet(runtimeEnv));
        models.put(ASGPropertyNames.WH20, new WH2O(runtimeEnv));
    }
    
    @Override
    public JComponent getDisplayWidget(IPropertyNames name) {
        return models.get(name).getDisplayWidget();
    }
    
    @Override
    public PropertyModel getModel(IPropertyNames name) {
        return models.get(name);
    }
    
} // ModelCollection

//
// O_o
