/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.properties.models;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyChangeListener;
import alma.Control.CommonGui.properties.IPropertyNames;
import alma.Control.CommonGui.properties.models.PropertyModel;
import alma.Control.CommonGui.widgets.LabelWidget;
import alma.Control.CommonGui.widgets.LabelWidgetEmphasis;

import javax.swing.JComponent;

/**
 * Model the Antenna Required Count for use in a CONTROL GUI.
 */
public class AntennasRequired extends PropertyModel implements IPropertyChangeListener<Long> {
    
    public AntennasRequired (IRuntimeEnvironment runtimeEnv) {     
        super(runtimeEnv, ASGPropertyNames.ANTENNAS_REQUIRED);
        value = "00";
    }
    
    /* (non-Javadoc)
     * @see alma.Control.CommonGui.properties.ControlGuiModel#makeDisplayWidget()
     */
    @Override
    protected JComponent makeDisplayWidget() {
        return new LabelWidget(value);
    }

    /**
     * Handle changes to properties, including changes to emphasis as needed.
     *  
     * @param pName
     * @param pValue
     */
    public void propertyChanged(IPropertyNames pName, Long pValue) {
        if (name.equals(pName)) {
            value = pValue.toString();
            // TODO: Add logic to compute emphasis correctly.
            if (pValue < 0) {
                emphasis = LabelWidgetEmphasis.WARNING;
            } else {
                emphasis = LabelWidgetEmphasis.NONE;
            }
        } else if (pName.equals(ASGPropertyNames._UPDATE_FAILURE)) {
            // Warn the user that the data they are looking at is questionable. 
            // At this time, we don't know if this is a transient failure or real failure.
            emphasis = LabelWidgetEmphasis.DEGRADED;
        }
        notifyListeners();
    }
}

//
//O_o
