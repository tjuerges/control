/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import java.sql.Date;

import alma.ArrayExceptions.InvalidRequestEx;
import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.sources.PropertySource;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ScanIntentMod.ScanIntent;

/**
 * Model the details of scan to be displayed in a user interface.

 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class ScanStatus extends ArrayPropertySource {

    public ScanStatus(IRuntimeEnvironment runtimeEnv) throws AcsJContainerServicesEx {
        super(runtimeEnv);
        addProperty(ASGPropertyNames.SCAN_INTENT, String.class, "Unknown");
        addProperty(ASGPropertyNames.SCAN_COUNT, Long.class, new Long(0));
        addProperty(ASGPropertyNames.SUBSCAN_COUNT, Long.class, new Long(0));
        createDataRetrievalThread();
    }

    private void createDataRetrievalThread() {
        Thread dataPoller = new Thread(new Runnable() {
            public void run() {
              try {
                while (true) {
                  Thread.sleep(1000);
                  readFromSource();
                }
              }
              catch(InterruptedException ex) {
                  ex.printStackTrace();
              }
            }
          });
        dataPoller.setDaemon(true);
        dataPoller.start();
    }

    private void readFromSource() {
        if (array.isScanStatusAvailable()) {
            try {
                alma.Control.ScanStatus status = array.getScanStatus();
                setProperty(ASGPropertyNames.SCAN_INTENT, status.intent.toString());
                setProperty(ASGPropertyNames.SCAN_COUNT, status.scanNum);
                setProperty(ASGPropertyNames.SUBSCAN_COUNT, status.subscanCount);
            } catch (InvalidRequestEx ex) {
                ex.printStackTrace();
            }
        }
    }
} // class ScanStatus

//
// O_o
