/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import alma.ArrayExceptions.InvalidRequestEx;
import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

/**
 * Model the details of an array to be displayed in a user interface.
 * 
 * These values are grouped together because they are read as a group from 
 * the same interface.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class AtmosphericConditions extends ArrayPropertySource {

    public AtmosphericConditions(IRuntimeEnvironment runtimeEnv) throws AcsJContainerServicesEx {
        super(runtimeEnv);
        addProperty(ASGPropertyNames.LAST_TAU, String.class, "0.0");
        addProperty(ASGPropertyNames.WH20, String.class, "0.0");
        createDataRetrievalThread();
    }

    private void createDataRetrievalThread() {
        Thread dataPoller = new Thread(new Runnable() {
            public void run() {
              try {
                while (true) {
                  Thread.sleep(1000);
                  readFromSource();
                }
              }
              catch(InterruptedException ex) {
                  ex.printStackTrace();
              }
            }
          });
        dataPoller.setDaemon(true);
        dataPoller.start();
    }

    private void readFromSource() {
        if (array.isAtmosphericConditionsAvailable()) {
            try {
                alma.Control.AtmosphericConditions atm = array.getAtmosphericConditions();
                setProperty(ASGPropertyNames.LAST_TAU, Double.toString(atm.lastTau));
                setProperty(ASGPropertyNames.WH20, Double.toString(atm.wh2o));
            } catch (InvalidRequestEx ex) {
                ex.printStackTrace();
            }
        }
    }
} // class AtmosphericConditions

//
// O_o
