/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.sources.PropertySource;

/**
 * Model the details of array pointing to be displayed in a user interface.

 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class PointingStatus extends PropertySource {

//    private PropertyChangeMonitor<Long> antennasOnSourceCount = new PropertyChangeMonitor<Long>(0L);
//    private PropertyChangeMonitor<Double> azimuth = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> crossElOffset = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> crossElVelocity = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> declination = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> elevation = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> elOffset = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> elVelocity = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> maximumPointingError = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<Double> rightAscension = new PropertyChangeMonitor<Double>(0.0);
//    private PropertyChangeMonitor<String> sourceName = new PropertyChangeMonitor<String>("");
//    private PropertyChangeMonitor<Date> timeToSet = new PropertyChangeMonitor<Date>(new Date(0));
    
    public PointingStatus(IRuntimeEnvironment runtimeEnv) {
        super(runtimeEnv);
        // TODO Auto-generated constructor stub
    }

//    public long getAntennasOnSourceCount() {
//        return antennasOnSourceCount.getValue();
//    }
//
//    public double getAz() {
//        return azimuth.getValue();
//    }
//
//    public double getCrossElOffset() {
//        return crossElOffset.getValue();
//    }
//
//    public double getCrossElVelocity() {
//        return crossElVelocity.getValue();
//    }
//
//    public double getDec() {
//        return declination.getValue();
//    }
//
//    public double getEl() {
//        return elevation.getValue();
//    }
//
//    public double getElOffset() {
//        return elOffset.getValue();
//    }
//
//    public double getElVelocity() {
//        return elVelocity.getValue();
//    }
//
//    public double getMaximumPointingError() {
//        return maximumPointingError.getValue();
//    }
//
//    public double getRa() {
//        return rightAscension.getValue();
//    }
//
//    public String getSourceName() {
//        return sourceName.getValue();
//    }
//
//    public Date getTimeToSet() {
//        return timeToSet.getValue();
//    }
//
//    public void setAntennasOnSourceCount(long count) {
//        antennasOnSourceCount.setValue(count);
//    }
//
//    public void setAz(double az) {
//        azimuth.setValue(az);
//    }
//
//    public void setCrossElOffset(double offset) {
//        crossElOffset.setValue(offset);
//    }
//
//    public void setCrossElVelocity(double velocity) {
//        crossElVelocity.setValue(velocity);
//    }
//
//    public void setDec(double dec) {
//        declination.setValue(dec);
//    }
//
//    public void setEl(double el) {
//        elevation.setValue(el);
//    }
//
//    public void setElOffset(double offset) {
//        elOffset.setValue(offset);
//    }
//
//    public void setElVelocity(double velocity) {
//        elVelocity.setValue(velocity);
//    }
//
//    public void setMaximumPointingError(double error) {
//        maximumPointingError.setValue(error);
//    }
//
//    public void setRa(double ra) {
//        rightAscension.setValue(ra);
//    }
//
//    public void setSourceName(String name) {
//        sourceName.setValue(name);
//    }
//
//    public void setTimeToSet(Date time) {
//        timeToSet.setValue(time);
//    }

} // class PointingStatus

//
// O_o
