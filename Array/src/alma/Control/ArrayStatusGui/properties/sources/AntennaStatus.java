/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.sources.PropertySource;

import java.util.Random;

/**
 * Model the details of antennas to be displayed in a user interface.
 *
 * These values are grouped together because they are read as a group from 
 * the same interface.
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class AntennaStatus extends PropertySource {

    public AntennaStatus(IRuntimeEnvironment runtimeEnv) {
        super(runtimeEnv);
        
        // List all properties read from this source.
        addProperty(ASGPropertyNames.ANTENNAS_DEGRADED, Long.class, 0L);
        addProperty(ASGPropertyNames.ANTENNAS_OFFLINE, Long.class, 0L);
        addProperty(ASGPropertyNames.ANTENNAS_OPERATIONAL, Long.class, 0L);
        addProperty(ASGPropertyNames.ANTENNAS_TOTAL, Long.class, 0L);
        
        createTestDataGeneratorThread();
    }

    /**
     * Create a daemon thread to generate data for testing.
     */
    private void createTestDataGeneratorThread() {
        // DEBUG - generate random values.
        Thread generateRandomTestData = new Thread(new Runnable() {
            public void run() {
              try {
                while (true) {
                  Thread.sleep(1000);
                  readFromSource();
                }
              }
              catch(InterruptedException ex) {}
            }
          });
        generateRandomTestData.setDaemon(true);
        generateRandomTestData.start();
    }
    
    /**
     * Read from an ALMA data source, split up any compound data and pass each chunk along to a 
     * separate PropertyChangeMonitor.
     */
    public void readFromSource() {
        // DEBUG - generate random values.
        Random generator = new Random();
        Double delta = generator.nextGaussian() * 5;
        Long lDelta = delta.longValue();
        Long antennasDegraded = 5L + lDelta;
        Long antennasOffline = 10L + lDelta;
        Long antennasTotal = 50L + lDelta;
        Long antennasOperational = antennasTotal - antennasOffline - antennasDegraded;
        
        // Set values read from source in PropertyChangeMonitors.
        setProperty(ASGPropertyNames.ANTENNAS_DEGRADED, (Object)antennasDegraded);
        setProperty(ASGPropertyNames.ANTENNAS_OFFLINE, (Object)antennasOffline);
        setProperty(ASGPropertyNames.ANTENNAS_OPERATIONAL, (Object)antennasOperational);
        setProperty(ASGPropertyNames.ANTENNAS_TOTAL, (Object)antennasTotal);
    }
    
} // class AntennaStatus

//
// O_o
