/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.sources.PropertySource;

/**
 * Model the details of subscan to be displayed in a user interface.

 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class SubscanStatus extends PropertySource {

//    private PropertyChangeMonitor<Date> subscanDuration = new PropertyChangeMonitor<Date>(new Date(0));
//    private PropertyChangeMonitor<Long> subscanNumber = new PropertyChangeMonitor<Long>(0L);

    public SubscanStatus(IRuntimeEnvironment runtimeEnv) {
        super(runtimeEnv);
        // TODO Auto-generated constructor stub
    }

//    public Date getDuration() {
//        return subscanDuration.getValue();
//    }
//
//    public long getSubscanNumber() {
//        return subscanNumber.getValue();
//    }
//
//    public void setDuration(Date duration) {
//        subscanDuration.setValue(duration);
//    }
//
//    public void setSubscanNumber(long number) {
//        subscanNumber.setValue(number);
//    }

} // class SubscanStatus

//
// O_o
