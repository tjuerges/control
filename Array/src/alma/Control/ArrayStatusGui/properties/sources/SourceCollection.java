/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyNames;
import alma.Control.CommonGui.properties.sources.IPropertySourceCollection;
import alma.Control.CommonGui.properties.sources.PropertySource;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;

import java.util.HashMap;

/**
 * Data sources for data displayed in the Array Status GUI.
 * <p>
 * These values group together data read from the same interface in CONTROL.  
 * 
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class SourceCollection implements IPropertySourceCollection {

    /*
     * A list of data sources for all properties displayed in the Array Status GUI.
     */

    private AntennaShutterStatus antennaShutterStatus;
    private AntennaStatus antennaStatus;
    private AtmosphericConditions atmosphericConditions;
//    private CorrelatorStatus correlatorStatus;
    private ExecBlockStatus execBlockStatus;
//    private LocalOscillatorStatus localOscillatorStatus;
//    private PointingStatus pointingStatus;
    private ScanStatus scanStatus;
//    private SubscanStatus subscanStatus;
    
    private HashMap<IPropertyNames, PropertySource> sources = 
        new HashMap<IPropertyNames, PropertySource>();

    public SourceCollection(IRuntimeEnvironment runtimeEnv) {
        // arrayName = aName;

        try {
            
            // 
            // Create an instance of each data source, then register it as the source of the 
            // properties it gets from CONTROL.
            antennaShutterStatus = new AntennaShutterStatus(runtimeEnv);
            for (IPropertyNames name: antennaShutterStatus.propertiesSupplied()) {
                sources.put(name, antennaShutterStatus);
            }
    
            antennaStatus = new AntennaStatus(runtimeEnv);
            for (IPropertyNames name: antennaStatus.propertiesSupplied()) {
                sources.put(name, antennaStatus);
            }
    
            atmosphericConditions = new AtmosphericConditions(runtimeEnv);
            for (IPropertyNames name: atmosphericConditions.propertiesSupplied()) {
                sources.put(name, atmosphericConditions);
            }
    
    //        correlatorStatus = new CorrelatorStatus(runtimeEnv);
    //        for (IPropertyNames name: correlatorStatus.propertiesSupplied()) {
    //            sources.put(name, correlatorStatus);
    //        }
    
            execBlockStatus = new ExecBlockStatus(runtimeEnv);
            for (IPropertyNames name: execBlockStatus.propertiesSupplied()) {
                sources.put(name, execBlockStatus);
            }
    
    //        localOscillatorStatus = new LocalOscillatorStatus(runtimeEnv);
    //        for (IPropertyNames name: localOscillatorStatus.propertiesSupplied()) {
    //            sources.put(name, localOscillatorStatus);
    //        }
    
    //        pointingStatus = new PointingStatus(runtimeEnv);
    //        for (IPropertyNames name: pointingStatus.propertiesSupplied()) {
    //            sources.put(name, pointingStatus);
    //        }
    
            scanStatus = new ScanStatus(runtimeEnv);
            for (IPropertyNames name: scanStatus.propertiesSupplied()) {
                sources.put(name, scanStatus);
            }
    
    //        subscanStatus = new SubscanStatus(runtimeEnv);
    //        for (IPropertyNames name: subscanStatus.propertiesSupplied()) {
    //            sources.put(name, subscanStatus);
    //        }

        } catch (AcsJContainerServicesEx ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public PropertySource getSource(IPropertyNames name) {
        return sources.get(name);
    }
    
} // class SourceCollection

//
// O_o
