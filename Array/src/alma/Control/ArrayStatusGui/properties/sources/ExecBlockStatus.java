/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.sources.PropertySource;

import java.util.Date;
import java.util.Random;

/**
 * Model the details of exec blocks to be displayed in a user interface.

 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class ExecBlockStatus extends PropertySource {

    public ExecBlockStatus(IRuntimeEnvironment runtimeEnv) {
        super(runtimeEnv);
        
        // List all properties read from this source.
        addProperty(ASGPropertyNames.MAX_ALLOWED_TIME, Date.class, new Date(0));
        addProperty(ASGPropertyNames.ANTENNAS_REQUIRED, Long.class, 0L);
        addProperty(ASGPropertyNames.EXEC_BLOCK_NAME, String.class, "");
        addProperty(ASGPropertyNames.PI_NAME, String.class, "");
        addProperty(ASGPropertyNames.PROJECT_NAME, String.class, "");
        addProperty(ASGPropertyNames.SCHED_BLOCK_NAME, String.class, "");
        addProperty(ASGPropertyNames.SCRIPT_NAME, String.class, "");
        addProperty(ASGPropertyNames.START_TIME, Date.class, new Date(0));
        
        createTestDataGeneratorThread();
    }

    /**
     * Create a daemon thread to generate data for testing.
     */
    private void createTestDataGeneratorThread() {
        // DEBUG - generate random values.
        Thread generateRandomTestData = new Thread(new Runnable() {
            public void run() {
              try {
                while (true) {
                  Thread.sleep(1000);
                  readFromSource();
                }
              }
              catch(InterruptedException ex) {}
            }
          });
        generateRandomTestData.setDaemon(true);
        generateRandomTestData.start();
    }

    /**
     * Read from an ALMA data source, split up any compound data and pass each chunk along to a 
     * separate PropertyChangeMonitor.
     */
    public void readFromSource() {
        // DEBUG - generate random values.
        Random generator = new Random();
        Double delta = (Math.floor(((generator.nextGaussian()) * 100))/100);
        Date maxAllowedTime = new Date(delta.longValue());
        Long antennasRequired = 30L + delta.longValue();
        String execBlockName = "ExecBlock-" + delta.toString();
        String piName = "PI-" + delta.toString();
        String projectName = "Project-" + delta.toString();
        String schedBlockName = "SchedBlock-" + delta.toString();         
        String scriptName = "ScriptName-" + delta.toString();         
        Date startTime = new Date(delta.longValue() - 3L);
        
        // Set values read from source in PropertyChangeMonitors.
        setProperty(ASGPropertyNames.MAX_ALLOWED_TIME, (Object)maxAllowedTime);
        setProperty(ASGPropertyNames.ANTENNAS_REQUIRED, (Object)antennasRequired);
        setProperty(ASGPropertyNames.EXEC_BLOCK_NAME, (Object)execBlockName);
        setProperty(ASGPropertyNames.PI_NAME, (Object)piName);
        setProperty(ASGPropertyNames.PROJECT_NAME, (Object)projectName);
        setProperty(ASGPropertyNames.SCHED_BLOCK_NAME, (Object)schedBlockName);
        setProperty(ASGPropertyNames.SCRIPT_NAME, (Object)scriptName);
        setProperty(ASGPropertyNames.START_TIME, (Object)startTime);
    }

} // class ExecBlockStatus

//
// O_o
