/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.properties.sources;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.sources.PropertySource;

/**
 * Model the details of a correlator to be displayed in a user interface.

 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */

public class CorrelatorStatus extends PropertySource {

    public CorrelatorStatus(IRuntimeEnvironment runtimeEnv) {
        super(runtimeEnv);
        // TODO Auto-generated constructor stub
    }

//    private PropertyChangeMonitor<String> apState = new PropertyChangeMonitor<String>("");
//    private PropertyChangeMonitor<Long> corrMode = new PropertyChangeMonitor<Long>(0L) ;
//    private PropertyChangeMonitor<String> corrType = new PropertyChangeMonitor<String>("");
//    private PropertyChangeMonitor<Boolean> usesCorr = new PropertyChangeMonitor<Boolean>(false);
//
//    public String getApState() {
//        return apState.getValue();
//    }
//
//    public long getCorrMode() {
//        return corrMode.getValue();
//    }
//
//    public String getCorrType() {
//        return corrType.getValue();
//    }
//
//    public boolean getUsesCorr() {
//        return usesCorr.getValue();
//    }
//
//    public void setApState(String apState) {
//        this.apState.setValue(apState);
//    }
//
//    public void setCorrMode(long corrMode) {
//        this.corrMode.setValue(corrMode);
//    }
//
//    public void setCorrType(String corrType) {
//        this.corrType.setValue(corrType);
//    }
//
//    public void setUsesCorr(boolean usesCorr) {
//        this.usesCorr.setValue(usesCorr);
//    }

} // class CorrelatorStatus

//
// O_o
