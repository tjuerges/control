/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.properties;

import alma.Control.ArrayStatusGui.properties.models.PropertyModelCollection;
import alma.Control.ArrayStatusGui.properties.sources.SourceCollection;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.properties.IPropertyChangeListener;
import alma.Control.CommonGui.properties.IPropertyCollection;
import alma.Control.CommonGui.properties.IPropertyNames;
import alma.Control.CommonGui.properties.sources.PropertySource;

import javax.swing.JComponent;

/**
 * All guiProperties contained in the Array Status GUI.
 */
public class PropertyCollection implements IPropertyCollection {
    
    private PropertyModelCollection models;
    private SourceCollection sources;

    public PropertyCollection (IRuntimeEnvironment runtimeEnv) {        
        models = new PropertyModelCollection(runtimeEnv);
        sources = new SourceCollection(runtimeEnv);
        linkModelsToSources();
    }
    
    @Override
    public JComponent getDisplayWidget(IPropertyNames name) {
        return models.getDisplayWidget(name);
    }
    
    private void linkModelsToSources() {
        
        IPropertyChangeListener listener;
        PropertySource source;
        
        for (ASGPropertyNames name: ASGPropertyNames.values()) {
            // TODO: Decide how to address errors.
            try {
                listener = (IPropertyChangeListener) models.getModel(name);
            } catch (Exception e) {
                // Debug
                // System.out.println("SAR: name = " + name + " not found in call to models.getModel(name).");
                listener = null;
            } try {    
                source = sources.getSource(name);
            } catch (Exception e) {
                // Debug
                // System.out.println("SAR: name = " + name + " not found in call to sources.getSource(name).");
                source = null;
            } try {
                source.registerListenerForProperty(name, listener);
            } catch (Exception e) {
                // Debug
                // System.out.println(METHOD_NAME + ": name = " + name + " not found in call to source.registerListenerForProperty(name, model).");
            }
        }
    }

} // ModelCollection

//
// O_o
