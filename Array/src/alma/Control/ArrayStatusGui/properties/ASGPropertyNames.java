/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.properties;

import alma.Control.CommonGui.properties.IPropertyNames;

/**
 * ASGPropertyNames of all guiProperties used in this CONTROL GUI.
 */
public enum ASGPropertyNames implements IPropertyNames {

    _UPDATE_FAILURE, 
    ACD_STATE, 
    AIRMASS, 
    ANTENNAS_DEGRADED, 
    ANTENNAS_OFFLINE, 
    ANTENNAS_OPERATIONAL, 
    ANTENNAS_REQUIRED, 
    ANTENNAS_SHUTTER_CLOSED, 
    ANTENNAS_SHUTTER_OPEN, 
    ANTENNAS_TOTAL, 
    AZIMUTH, BL_APP_STATE, 
    BL_CORRELATOR_MODE, 
    COMMUNICATION_STATUS, 
    CROSS_EL_OFFSET, 
    CROSS_EL_VELOCITY, 
    DECLINATION, 
    EL_OFFSET, 
    EL_VELOCITY, 
    ELAPSED_TIME, 
    ELEVATION, 
    EXEC_BLOCK_NAME, 
    FEND_LOCK, 
    LAST_TAU, 
    MAX_ALLOWED_TIME, 
    MAX_DEWAR_TEMP, 
    MAX_ERROR, 
    ON_SOURCE_COUNT, 
    PI_NAME, 
    PROJECT_NAME, 
    RIGHT_ASCENSION, 
    SCAN_COUNT, 
    SCAN_INTENT, 
    SCHED_BLOCK_NAME, 
    SCRIPT_NAME, 
    SKY_FREQ, 
    SOURCE_NAME, 
    START_TIME, 
    STATUS_DETAILS, 
    STATUS_SUMMARY, 
    SUBSCAN_COUNT, 
    SUBSCAN_DURATION, 
    SUBSCAN_REMAINING, 
    SUBSCAN_TOTAL, 
    TIME_TO_SET, 
    WH20

} // enum ASGPropertyNames
