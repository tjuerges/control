/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.environment;

import alma.Control.CommonGui.environment.CommonEnvironment;

/**
 * A bag of data containing information from the run time environment of the Array Status GUI.
 * <p>
 * This exists to pass the environment around within the GUI while minimizing coupling between 
 * parts.  It also massages some data to make it more easily usable with an Array Status GUI.
 * <p>
 * Clients of this class should keep a reference to a IGuiEnvironment, not this class.  This 
 * prevents buggy clients from changing the contents of this class.  Only the code that creates 
 * an instance of this class should change the contents.
 * <p>
 * TODO: Look for ways assign responsibilities to more appropriate classes.
 */

public class ASGEnvironment extends CommonEnvironment {
    
    public ASGEnvironment () {
    }
    
    /**
     * Customize the inherited implementation of the <code>IGuiEnvironment</code> interface.  See 
     * <code>IGuiEnvironment</code> for method header comments.
     */

    /**
     * Customize the inherited implementation of the <code>IGuiEnvironmentBuilder</code> interface.  
     * See <code>IGuiEnvironmentBuilder</code> for method comments. 
     */

} // ASGEnvironment

// 
// O_o
