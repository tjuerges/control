/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui;

import java.util.logging.Logger;

import javax.swing.JFrame;

import alma.Control.ArrayStatusGui.environment.ASGEnvironment;
import alma.acs.component.client.ComponentClient;
import alma.acs.gui.standards.GuiStandards;
import alma.acs.logging.ClientLogManager;

/**
 * Insulate the ArrayStatusGui class from the UNIX environment.
 * <p>
 * Provide command line argument checking and processing.
 * <p>
 * Provide all information required by ArrayStatusGui.
 * <p>
 * Pass any commands from UNIX to the ArrayStatusGui.
 * <p>
 * Own the Window Manager frame that contains the ArrayStatusGui.
 */
public class UnixWrapper {
    
    private static String arrayName;
    private static JFrame frame;
    private static final int GUI_CONSTRUCTION_DELAY = 2000;
    private static final int INSUFFICIENT_ARGUMENTS=-1;
    private static final int REQUIRED_ARGUMENTS=1;
    private final static String STANDALONE_LOGGER_NAME = "ArrayStatusGuiLogger";

    @SuppressWarnings("unused")
    private static UnixWrapper wrapper;
    
    /**
     * Ensure required command line arguments are supplied.
     * <p>
     * TODO: add more detailed checking.
     * 
     * @param args to check.
     */
    private static void checkArgs(String[] args) {
        if (args.length !=  REQUIRED_ARGUMENTS) {
            showInsufficientArgumentsMessage();
        } 
    }
    public static void main(String[] args) {
        checkArgs(args);
        arrayName = args[0];
        wrapper = new UnixWrapper();
    }
    
    private static void showInsufficientArgumentsMessage() {
        System.out.println("Insufficient arguments.");
        showUseMessage();
    }

    /**
     * Show program use information and exit.
     */
    private static void showUseMessage() {
        System.out.println("");
        System.out.println("use: UnixWrapper <array>");
        System.out.println("     <array>  = An active array displayed on the OMC");
        System.out.println();
        System.out.println("example: UnixWrapper Array002");
        System.exit(INSUFFICIENT_ARGUMENTS);
    }
    
    private Gui gui;
    private ASGEnvironment runtimeEnv;
    private ComponentClient acsClient;
    
    public UnixWrapper() {
        
        final String METHOD_NAME = getClass().getName() + ".UnixWrapper()";

        buildEnvironment();        
        runtimeEnv.getLogger().fine(METHOD_NAME + ": buildEnvironment() complete.");
        //Schedule a job for the event-dispatching thread.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGui();
            }
        });     
        waitForGuiToBuild();
        frame.pack();
        frame.setVisible(true);        
        runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
    }
    
    /**
     * Build up the run time environment for this GUI.
     */
    private void buildEnvironment() {
        
        final String METHOD_NAME = getClass().getName() + ".buildEnvironment()";

        runtimeEnv = new ASGEnvironment();
        
        runtimeEnv.setLogger(getAcsLogger());
        runtimeEnv.getLogger().fine(METHOD_NAME + ": entering.");

        runtimeEnv.setArrayName(arrayName);
        runtimeEnv.getLogger().fine(METHOD_NAME + ": Array Name: " + runtimeEnv.getArrayName());        

        // TODO: Find a way to get ALMA user roles in processes that run outside the OMC.
        // For now, GUIs that run outside the OMC are prohibited from changing the state of 
        // the system.
        runtimeEnv.setRunRestricted(true);
                
        runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
        
        try {
            String managerLoc = System.getProperty("ACS.manager");
            if (managerLoc != null) {
                acsClient =
                    new ComponentClient(runtimeEnv.getLogger(), managerLoc,
                            "ArrayStatusGui.UnixWrapper");
                runtimeEnv.setContainerServices(acsClient.getContainerServices());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * Build out the components of this class.
     */
    private void createAndShowGui() {
        
        final String METHOD_NAME = getClass().getName() + ".createAndShowGui()";
        runtimeEnv.getLogger().fine(METHOD_NAME + ": entering.");
        
        GuiStandards.enforce();
        if (null == runtimeEnv) {
            System.out.println("Context is Null!");
        }
        frame = new JFrame("Array Status GUI : " + runtimeEnv.getArrayName());
        gui = new Gui(runtimeEnv);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(gui.getTopLevelPanel());
        // 
        // The frame will be packed and setVisible on the user thread after enough time passes 
        // for all children to be created.
        
        runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
    }
    
    private Logger getAcsLogger() {
        return ClientLogManager.getAcsLogManager().getLoggerForApplication(
                "ArrayStatusGui.UnixWrapper", true);
    }

    private void waitForGuiToBuild() {

        final String METHOD_NAME = getClass().getName() + ".waitForGuiToBuild()";
        runtimeEnv.getLogger().fine(METHOD_NAME + ": entering.");

        // TODO: improve this temporary code to force the frame to re-pack after all components 
        // are created, WITHOUT a sleep.
        try {
            // Sleep long enough for all child widgets to be drawn.
            Thread.sleep(GUI_CONSTRUCTION_DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
    }
        
} // UnixWrapper

//
// O_o
