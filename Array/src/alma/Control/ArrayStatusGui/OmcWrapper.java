/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui;

import alma.Control.ArrayStatusGui.environment.ASGEnvironment;
import alma.acs.gui.standards.GuiStandards;
import alma.exec.extension.subsystemplugin.PluginContainerServices;
import alma.exec.extension.subsystemplugin.SessionProperties;
import alma.exec.extension.subsystemplugin.SubsystemPlugin;

import java.util.logging.Logger;

import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 * Insulate the ArrayStatusGui class from the OMC.
 * <p>
 * Provide all information required by ArrayStatusGui.
 * <p>
 * Pass any commands from the the OMC to the ArrayStatusGui.
 */
public class OmcWrapper extends JPanel implements SubsystemPlugin {
    
    private static final long serialVersionUID = 1L;
    private final static String STANDALONE_LOGGER_NAME = "ArrayStatusGuiLogger";
    private Gui gui;
    private ASGEnvironment runtimeEnv;
    private PluginContainerServices omcContainerServices;
    
    // The following variable is used to ensure GUI initialization waits for the OMC to set the 
    // PluginContainerServices variable so the rest of the GUI may be initialized with valid data.
    private boolean omcContainerServicesSet = false;
    
    private SessionProperties omcSessionProperties;
    
    public OmcWrapper() {
        
        final String METHOD_NAME = getClass().getName() + ".OmcWrapper()";

        // Initialization of this class must wait until the OMC calls <code>setServices()</code>.
        // The waiting logic must run on a thread to allow the OMC to do other work before it 
        // calls <code>setServices()</code>.
        SwingWorker worker = new SwingWorker() {

            @Override
            protected Object doInBackground() throws Exception {
                
                while (!omcContainerServicesSet) {
                    try {
                        // sleep one second between tests.
                        System.out.println("omcContainerServicesSet not yet set.");
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Decide what to do if this sleep is interrupted.
                        e.printStackTrace();
                    } 
                }
                buildEnvironment();
                runtimeEnv.getLogger().fine(METHOD_NAME + ": buildEnvironment() complete.");
                addChildren();
                return null;
            }
        };
        worker.execute();
                
        runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
    }
    
    /**
     * Build out the components of this class.
     */
    private void addChildren() {
        final String METHOD_NAME = getClass().getName() + ".addChildren()";
        runtimeEnv.getLogger().fine(METHOD_NAME + ": entering.");

        GuiStandards.enforce();
        gui = new Gui(runtimeEnv);
        add(gui.getTopLevelPanel());
        
        runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
    }

    /**
     * Build up the run time environment for this GUI.
     */
    private void buildEnvironment() {

        final String METHOD_NAME = getClass().getName() + ".buildEnvironment()";

        runtimeEnv = new ASGEnvironment();
        omcSessionProperties = omcContainerServices.getSessionProperties();

        runtimeEnv.setLogger(getAcsLogger());
        runtimeEnv.getLogger().fine(METHOD_NAME + ": entering.");

        runtimeEnv.setArrayName(trimArrayName(omcSessionProperties.getArrayName()));
        runtimeEnv.getLogger().fine(METHOD_NAME + ": Array Name: " + runtimeEnv.getArrayName());        

        runtimeEnv.setRunRestricted(true);    // TODO: get from OMC.

        runtimeEnv.getLogger().fine(METHOD_NAME + ": exiting.");
    
        runtimeEnv.setContainerServices(omcContainerServices);
        
    }

    private Logger getAcsLogger() {
        return (omcContainerServices == null) ? 
                Logger.getLogger(STANDALONE_LOGGER_NAME) : 
                omcContainerServices.getLogger();
    }
    
    @Override
    public boolean runRestricted (boolean restricted) throws Exception {
        runtimeEnv.setRunRestricted(restricted);
        return runtimeEnv.runRestricted();
    }
    
    /**
     * Implement the SubsystemPlugin interface.
     */
    public void setServices (PluginContainerServices omcContainerServices) {
        this.omcContainerServices = omcContainerServices;
        omcContainerServicesSet = true;
    }
    
    public void start() throws Exception {
        // TODO: implement
    }
    
    public void stop() throws Exception {
        // TODO: implement
    }
    
    /**
     * Trim the "CONTROL/" prefix from an array name.
     * @return
     */
    private String trimArrayName(String fullName) {
        return fullName.substring(fullName.indexOf('/') + 1);
    }
    
} // OmcWrapper

//
// O_o
