/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;

/**
 * Contain all status data displayed by an Array Status GUI.
 */
public class StatusPanel extends GuiPanel {
    
    private static final long serialVersionUID = 1L;
    
    public StatusPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties);
    }
    
    protected void addChildren() {
        setBorder(BorderFactory.createLineBorder(Color.black));
        setLayout(new BorderLayout());
        add(new StatusSummaryPanel(runtimeEnv, properties), BorderLayout.LINE_START);
        add(new StatusDetailsPanel(runtimeEnv, properties), BorderLayout.CENTER);
        add(new CommunicationStatusPanel(runtimeEnv, properties), BorderLayout.LINE_END);
    }
        
} // StatusPanel

//
// O_o
