/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

/**
 * Contain all data sub-panels displayed by an Array Status GUI.
 * 
 * Display sub-panels in a grid per the example in http://jira.alma.cl/browse/COMP-4158
 */
public class DataPanel extends GuiPanel {
    
    private static final long serialVersionUID = 1L;
    
    public DataPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties);
    }
    
    protected void addChildren() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        
        c.gridx = 0;
        c.gridy = 0;
        add(new ProjectPanel(runtimeEnv, properties), c);
        c.gridy = 1;
        add(new ScanPanel(runtimeEnv, properties), c);
        c.gridy = 2;
        add(new AntennasPanel(runtimeEnv, properties), c);
        
        c.gridx = 1;
        c.gridy = 0;
        add(new SourcePanel(runtimeEnv, properties), c);
        c.gridy = 1;
        add(new FrontEndPanel(runtimeEnv, properties), c);
        c.gridy = 2;
        add(new CorrelatorPanel(runtimeEnv, properties), c);
    }
        
} // DataPanel

//
// O_o
