/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiGroupingPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

/**
 * Contain and arrange all Project widgets displayed by an Array Status GUI.
 */
public class ProjectPanel extends GuiGroupingPanel {
    
    private static final long serialVersionUID = 1L;
    
    public ProjectPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties, "Project");
    }
    
    protected void addChildren() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        
        // Add labels down columns, then across rows.
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("Name : ", JLabel.TRAILING), c);
        c.gridy = 1;
        add(new JLabel("Sched Block : ", JLabel.TRAILING), c);
        c.gridy = 2;
        add(new JLabel("Exec Block : ", JLabel.TRAILING), c);
        c.gridy = 3;
        add(new JLabel("PI : ", JLabel.TRAILING), c);
        c.gridy = 4;
        add(new JLabel("Script : ", JLabel.TRAILING), c);
        c.gridy = 5;
        add(new JLabel(" "), c);
        c.gridy = 6;
        add(new JLabel("Start Time : ", JLabel.TRAILING), c);
        c.gridy = 7;
        add(new JLabel("Elapsed Time : ", JLabel.TRAILING), c);
        c.gridy = 8;
        add(new JLabel("Max Allowed : ", JLabel.TRAILING), c);

        // Add widgets down columns, then across rows.
        c.gridx = 1;
        c.gridy = 0;
        add(properties.getDisplayWidget(ASGPropertyNames.PROJECT_NAME), c);
        c.gridy = 1;
        add(properties.getDisplayWidget(ASGPropertyNames.SCHED_BLOCK_NAME), c);
        c.gridy = 2;
        add(properties.getDisplayWidget(ASGPropertyNames.EXEC_BLOCK_NAME), c);
        c.gridy = 3;
        add(properties.getDisplayWidget(ASGPropertyNames.PI_NAME), c);
        c.gridy = 4;
        add(properties.getDisplayWidget(ASGPropertyNames.SCRIPT_NAME), c);
        c.gridy = 5;
        add(new JLabel(" "), c);
        c.gridy = 6;
        add(properties.getDisplayWidget(ASGPropertyNames.START_TIME), c);
        c.gridy = 7;
        add(properties.getDisplayWidget(ASGPropertyNames.ELAPSED_TIME), c);
        c.gridy = 8;
        add(properties.getDisplayWidget(ASGPropertyNames.MAX_ALLOWED_TIME), c);
    }
        
} // ProjectPanel

//
// O_o
