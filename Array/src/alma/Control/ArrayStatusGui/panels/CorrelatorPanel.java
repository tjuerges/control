/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiGroupingPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

/**
 * Contain and arrange all Correlator widgets displayed by an Array Status GUI.
 */
public class CorrelatorPanel extends GuiGroupingPanel {
    
    private static final long serialVersionUID = 1L;
    
    public CorrelatorPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties, "BL Correlator");
    }
    
    protected void addChildren() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        
        // Add labels down columns, then across rows.
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("Current Mode : ", JLabel.TRAILING), c);
        c.gridy = 1;
        add(new JLabel("AP State : ", JLabel.TRAILING), c);
        
        // Add widgets down columns, then across rows.
        // Debug text in place of real data.
        c.gridx = 1;
        c.gridy = 0;
        add(properties.getDisplayWidget(ASGPropertyNames.BL_CORRELATOR_MODE), c);       
        c.gridy = 1;
        add(properties.getDisplayWidget(ASGPropertyNames.BL_APP_STATE), c);       
    }
        
} // CorrelatorPanel

//
// O_o
