/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiGroupingPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

/**
 * Contain and arrange all Source data widgets displayed by an Array Status GUI.
 */
public class SourcePanel extends GuiGroupingPanel {
    
    private static final long serialVersionUID = 1L;
    
    public SourcePanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties, "Source");
    }
    
    protected void addChildren() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        
        // Add labels down columns, then across rows.
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("Name : ", JLabel.TRAILING), c);
        c.gridy = 1;
        add(new JLabel("RA : ", JLabel.TRAILING), c);
        c.gridy = 2;
        add(new JLabel("Az : ", JLabel.TRAILING), c);
        c.gridy = 3;
        add(new JLabel("On Source : ", JLabel.TRAILING), c);
        c.gridy = 4;
        add(new JLabel("Last Tau : ", JLabel.TRAILING), c);
        c.gridy = 5;
        add(new JLabel("Time to Set : ", JLabel.TRAILING), c);
        c.gridy = 7;
        add(new JLabel("Offset : ", JLabel.TRAILING), c);
        c.gridy = 8;
        add(new JLabel("Velocity : ", JLabel.TRAILING), c);
        
        c.gridx = 1;
        c.gridy = 6;
        add(new JLabel("Cross-El", JLabel.CENTER), c);
        
        c.gridx = 2;
        c.gridy = 1;
        add(new JLabel("Dec : ", JLabel.TRAILING), c);
        c.gridy = 2;
        add(new JLabel("El : ", JLabel.TRAILING), c);
        c.gridy = 3;
        add(new JLabel("Max Error : ", JLabel.TRAILING), c);
        c.gridy = 4;
        add(new JLabel("WH2O : ", JLabel.TRAILING), c);
        c.gridy = 5;
        add(new JLabel("Airmass : ", JLabel.TRAILING), c);
        
        c.gridx = 2;
        c.gridy = 6;
        add(new JLabel("El", JLabel.CENTER), c);
        
        // Add widgets down columns, then across rows.
        c.gridx = 1;
        c.gridy = 0;
        add(properties.getDisplayWidget(ASGPropertyNames.SOURCE_NAME), c);
        c.gridy = 1;
        add(properties.getDisplayWidget(ASGPropertyNames.RIGHT_ASCENSION), c);
        c.gridy = 2;
        add(properties.getDisplayWidget(ASGPropertyNames.AZIMUTH), c);
        c.gridy = 3;
        add(properties.getDisplayWidget(ASGPropertyNames.ON_SOURCE_COUNT), c);        
        c.gridy = 4;
        add(properties.getDisplayWidget(ASGPropertyNames.LAST_TAU), c);        
        c.gridy = 5;
        add(properties.getDisplayWidget(ASGPropertyNames.TIME_TO_SET), c);        
        c.gridy = 7;
        add(properties.getDisplayWidget(ASGPropertyNames.CROSS_EL_OFFSET), c);        
        c.gridy = 8;
        add(properties.getDisplayWidget(ASGPropertyNames.CROSS_EL_VELOCITY), c);        
        
        c.gridx = 2;
        c.gridy = 7;
        add(properties.getDisplayWidget(ASGPropertyNames.EL_OFFSET), c);        
        c.gridy = 8;
        add(properties.getDisplayWidget(ASGPropertyNames.EL_VELOCITY), c);        

        c.gridx = 3;
        c.gridy = 1;
        add(properties.getDisplayWidget(ASGPropertyNames.DECLINATION), c);
        c.gridy = 2;
        add(properties.getDisplayWidget(ASGPropertyNames.ELEVATION), c);
        c.gridy = 3;
        add(properties.getDisplayWidget(ASGPropertyNames.MAX_ERROR), c);
        c.gridy = 4;
        add(properties.getDisplayWidget(ASGPropertyNames.WH20), c);        
        c.gridy = 5;
        add(properties.getDisplayWidget(ASGPropertyNames.AIRMASS), c);
    }
        
} // SourcePanel

//
// O_o
