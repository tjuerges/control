/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * Contain all menus displayed by an Array Status GUI.
 */
public class MenuPanel extends GuiPanel {
    
    private static final long serialVersionUID = 1L;
        
    // TODO: fix action listener!
    private ActionListener testActionListener = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            System.out.println("Menu item clicked" + e.toString());
        }
    };
        
    public MenuPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties);
    }
    
    protected void addChildren() {
        FlowLayout layout = new FlowLayout();
        layout.setAlignment(FlowLayout.LEADING);
        setLayout(layout);

        JMenu topMenu = new JMenu("Antenna Commands");
        topMenu.setMnemonic(KeyEvent.VK_A);
//        topMenu.setEnabled(!runtimeEnv.runRestricted());
        
        JMenuItem maintenanceStow = new JMenuItem("Maintenance Stow");
        maintenanceStow.setMnemonic(KeyEvent.VK_M);
        maintenanceStow.addActionListener(testActionListener);
        topMenu.add(maintenanceStow);
        
        JMenuItem survivalStow = new JMenuItem("Survival Stow");
        survivalStow.setMnemonic(KeyEvent.VK_S);
        survivalStow.addActionListener(testActionListener);
        topMenu.add(survivalStow);
        
        JMenuItem standby = new JMenuItem("Standby");
        standby.setMnemonic(KeyEvent.VK_T);
        standby.addActionListener(testActionListener);
        topMenu.add(standby);
        
        JMenuItem openShutters = new JMenuItem("Open Shutters");
        openShutters.setMnemonic(KeyEvent.VK_O);
        openShutters.addActionListener(testActionListener);
        topMenu.add(openShutters);
        
        JMenuItem closeShutters = new JMenuItem("Close Shutters");
        closeShutters.setMnemonic(KeyEvent.VK_C);
        closeShutters.addActionListener(testActionListener);
        topMenu.add(closeShutters);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(topMenu);
        add(menuBar);
    }
    
} // MenuPanel

//
// O_o
