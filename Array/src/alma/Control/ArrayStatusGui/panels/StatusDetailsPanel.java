/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.FlowLayout;

/**
 * Display an Array Status GUI's status detail.
 */
public class StatusDetailsPanel extends GuiPanel {
    
    private static final long serialVersionUID = 1L;
    
    public StatusDetailsPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties);
    }
    
    protected void addChildren() {
        FlowLayout layout = new FlowLayout();
        layout.setAlignment(FlowLayout.LEADING);
        setLayout(layout);
        add(properties.getDisplayWidget(ASGPropertyNames.STATUS_DETAILS));
    }
        
} // StatusDetailsPanel

//
// O_o
