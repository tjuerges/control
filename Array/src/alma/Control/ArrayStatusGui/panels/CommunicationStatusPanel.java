/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

/**
 * Display an Array Status GUI's communications status.
 */
public class CommunicationStatusPanel extends GuiPanel {
    
    private static final long serialVersionUID = 1L;
    
    public CommunicationStatusPanel (IRuntimeEnvironment runtimeEnv, 
                                     IPropertyCollection properties) {
        super(runtimeEnv, properties);
    }
    
    protected void addChildren() {
        add(properties.getDisplayWidget(ASGPropertyNames.COMMUNICATION_STATUS));
    }
        
} // CommunicationStatusPanel

//
// O_o
