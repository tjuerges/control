/*
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * @author  Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since   ALMA 8.0.0
 */

package alma.Control.ArrayStatusGui.panels;

import alma.Control.ArrayStatusGui.properties.ASGPropertyNames;
import alma.Control.CommonGui.environment.IRuntimeEnvironment;
import alma.Control.CommonGui.panels.GuiGroupingPanel;
import alma.Control.CommonGui.properties.IPropertyCollection;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

/**
 * Contain and arrange all Antennas widgets displayed by an Array Status GUI.
 */
public class AntennasPanel extends GuiGroupingPanel {
    
    private static final long serialVersionUID = 1L;

    public AntennasPanel(IRuntimeEnvironment runtimeEnv, IPropertyCollection properties) {
        super(runtimeEnv, properties, "Antennas");
    }
    
    protected void addChildren() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        
        // Add labels down columns, then across rows.
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("Total : ", JLabel.TRAILING), c);
        c.gridy = 1;
        add(new JLabel("Required : ", JLabel.TRAILING), c);
        c.gridy = 3;
        add(new JLabel("Shutter Open : ", JLabel.TRAILING), c);
        
        c.gridx = 2;
        c.gridy = 0;
        add(new JLabel("Operational : ", JLabel.TRAILING), c);
        c.gridy = 1;
        add(new JLabel("Degraded : ", JLabel.TRAILING), c);
        c.gridy = 2;
        add(new JLabel("Offline : ", JLabel.TRAILING), c);
        c.gridy = 3;
        // TODO: Find a better way to control layout than the spaces in the label below.
        add(new JLabel("  Shutter Closed : ", JLabel.TRAILING), c);
        
        // Add widgets down columns, then across rows.
        c.gridx = 1;
        c.gridy = 0;
        add(properties.getDisplayWidget(ASGPropertyNames.ANTENNAS_TOTAL), c);
        c.gridy = 1;
        add(properties.getDisplayWidget(ASGPropertyNames.ANTENNAS_REQUIRED), c);
        c.gridy = 3;
        add(properties.getDisplayWidget(ASGPropertyNames.ANTENNAS_SHUTTER_OPEN), c);

        c.gridx = 3;
        c.gridy = 0;
        add(properties.getDisplayWidget(ASGPropertyNames.ANTENNAS_OPERATIONAL), c);
        c.gridy = 1;
        add(properties.getDisplayWidget(ASGPropertyNames.ANTENNAS_DEGRADED), c);
        c.gridy = 2;
        add(properties.getDisplayWidget(ASGPropertyNames.ANTENNAS_OFFLINE), c);
        c.gridy = 3;
        add(properties.getDisplayWidget(ASGPropertyNames.ANTENNAS_SHUTTER_CLOSED), c);
    }
        
} // AntennasPanel

//
// O_o
