# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 - 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

# --------------------------------------------
# --------- General helper functions ---------
# --------------------------------------------

# TotalPower new imports
import NetSidebandMod
import PolarizationTypeMod
import StokesParameterMod
import BasebandNameMod

import Control
import ControlSB
import Correlator
import asdmIDLTypes
import PyDataModelEnumeration.PyBasebandName
import PyDataModelEnumeration.PyAccumMode
import PyDataModelEnumeration.PyCorrelationMode
import PyDataModelEnumeration.PySidebandProcessingMode
import PyDataModelEnumeration.PySwitchingMode
import PyDataModelEnumeration.PyACAPolarization
import PyDataModelEnumeration.PyNetSideband
import PyDataModelEnumeration.PyWindowFunction
import PyDataModelEnumeration.PyCorrelationBit
import PyDataModelEnumeration.PyStokesParameter
import PyDataModelEnumeration.PyReceiverSideband
import PyDataModelEnumeration.PyAtmPhaseCorrection

#
# Needed imports for source creation helper functions
#
import xmlentity
from Acspy.Util.XmlObjectifier import XmlObject
from math import floor, pi, degrees


def create_holography_scan_data():
    scanData = Control.HolographyScanData("name",
                                          asdmIDLTypes.IDLFrequency(0.0),
                                          0,
                                          0,
                                          0,
                                          ["type"],
                                          asdmIDLTypes.IDLLength(0.0),
                                          asdmIDLTypes.IDLLength(0.0),
                                          asdmIDLTypes.IDLLength(0.0),
                                          asdmIDLTypes.IDLLength(0.0))
    return scanData

def create_holography_subscan_data():
    holographyData = Control.HolographyData(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, False)
    positionData = Control.PositionData(asdmIDLTypes.IDLAngle(0.0),
                                        asdmIDLTypes.IDLAngle(0.0))
    subScanData = Control.HolographySubScanData("AntSimVA1",
                                                "SubScanName",
                                                positionData,
                                                0,
                                                0,
                                                0,
                                                [holographyData],
                                                [positionData],
                                                [positionData])
    return subScanData

def create_dummy_source():
    coord = ControlSB.J2000Coord( 0.3, 0.7 )
    circlePatt = ControlSB.CirclePattern( coord )
    spiralPatt = ControlSB.SpiralPattern( coord )
    absPatt = ControlSB.AbsolutePointingPattern( [coord] )
    offPatt = ControlSB.OffsetPointingPattern( [coord] )
    rectPatt = ControlSB.RectanglePattern( coord, # J2000Coord phaseCenter;
                                           0.0, # double longitudeLength;
                                           0.0, # double longitudeStep;
                                           0.0, # double latitudeLength;
                                           0.0, # double latitudeStep;
                                           ControlSB.Longitude, # ScanDirection direction;
                                           0.0, # double scanVelocity;
                                           0.0 ) # double orientiation;
    source = ControlSB.Source("sourceName",   # string sourceName
                              coord,  # J2000Coord coord;
                              0.0, # double sourceVelocity;
                              "sourceEphemeris", # string sourceEphemeris;
                              0.1, # double pMRa;
                              0.1, # double pMDec;
                              1, # boolean nonSiderealMotion;
                              ControlSB.Emphemeris, # SolarSystem solarSystemObject;
                              coord, # J2000Coord referenceCoord;
                              0.0, # double intTimeReference;
                              0.0, # double timeSource;
                              [], # SourcePropertiesSeq sourcePropertiesList;
                              ControlSB.Circle, # FieldPattern pattern;
                              circlePatt, # CirclePattern circle;
                              spiralPatt, # SpiralPattern spiral;
                              absPatt, # AbsolutePointingPattern absPointing;
                              offPatt, # OffsetPointingPattern offPointing;
                              rectPatt, # RectanglePattern rectangle;
                              0.0) # double parallax;
    return source

def create_dummy_spectral_spec():
    spectralSpec = ControlSB.SpectralSpec(0.0, # double integrationTime;
                                          0.0, # double dynamicRange;
                                          0.0, # double restFrequency;
                                          "transitionName", # string transitionName;
                                          ControlSB.NotUsed, # ReceiverBandType receiverBand;
                                          0.0) # double LO1;
    return spectralSpec

def create_dummy_corr_conf():
    corrConf = Correlator.CorrelatorConfiguration(0, # long IntegrationDuration;
                                                  0, # long SubScanDuration;
                                                  []) # BaseBandConfigSeq BaseBands;
    return corrConf


# ----------------------------------------------------
# --------- Source creation helper functions ---------
# ----------------------------------------------------

#
# Creates an empty FieldSource XMLEntity object
#
def create_default_field_source():
    '''
    This function returns a default FieldSource structure starting
    from the deafault XML string. It converts the XML string into
    a XMLObject that is returned for further processing.

    This function is used by the create_source_X functions.
    '''
    # Create the XML structure with default values
    defaultFieldSource = "<?xml version='1.0' encoding='UTF-8'?>\
    <FieldSource xmlns='Alma/ObsPrep/SchedBlock'\
        xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'\
        almatype='APDM::FieldSource' entityPartId=''\
        solarSystemObject='Moon' xsi:type='FieldSource'>\
        <sourceCoordinates system='J2000'>\
            <ns1:longitude xmlns:ns1='Alma/ValueTypes' unit='rad'>0.0</ns1:longitude>\
            <ns2:latitude xmlns:ns2='Alma/ValueTypes' unit='rad'>0.0</ns2:latitude>\
        </sourceCoordinates>\
        <sourceName>Unknown</sourceName>\
        <sourceVelocity referenceSystem='lsr'>\
            <ns3:centerVelocity xmlns:ns3='Alma/ValueTypes' unit='m/s'>0.0</ns3:centerVelocity>\
        </sourceVelocity>\
        <sourceEphemeris>None</sourceEphemeris>\
        <pMRA unit='rad/s'>0.0</pMRA>\
        <pMDec unit='rad/s'>0.0</pMDec>\
        <nonSiderealMotion>false</nonSiderealMotion>\
        <parallax unit='arcsec'>0.0</parallax>\
        <name>''</name>\
        <SourceProperty almatype='APDM::SourceProperty'>\
            <sourceFrequency unit='Hz'>0.0</sourceFrequency>\
            <sourceFluxI unit='Jy'>0.0</sourceFluxI>\
            <sourceDiameter unit='rad'>0.0</sourceDiameter>\
        </SourceProperty>\
    </FieldSource>"

    # Transform the XML string into a XMLObject
    obj = XmlObject(defaultFieldSource)

    # Return the XMLObject
    return obj

#
# Creating a source object from RA and Dec
#
def create_source_RADec(RA,
                        Dec,
                        system="J2000",
                        pMRA=0,
                        pMDec=0,
                        sourceName="",
                        sourceFrequency=0,
                        sourceFluxI=0,
                        sourceDiameter=0):
    '''
    This helper function creates a FieldSource structure based on
    the coordinates (RA and Dec) of the source object. Optionally
    the coordinate system (system), the proper motion (pMRA, pMDec),
    the object sourceName and its properties (sourceFrequency,
    sourceFluxI, sourceDiameter) can be set. If no object sourceName
    is defined a standard name will be generated (format: "JHHMM+DDd",
    e.g. "J0354+225").
    
    Units and/or possible values:
    RA and Dec: [rad]
    system: "J2000", ...
    pMRA and pMDec: [rad/s].
    sourceFrequency: [Hz]
    sourceFluxI: [Jy]
    sourceDiameter: [rad]
    '''
    
    # Get a default source structure
    st = create_default_field_source()
    
    # Fill in the respective mandatory and optional fields
    st.FieldSource.sourceCoordinates.longitude.setValue(RA)
    st.FieldSource.sourceCoordinates.latitude.setValue(Dec)
    st.FieldSource.sourceCoordinates.attributes.getNamedItem("system").value = system
    st.FieldSource.pMRA.setValue(pMRA)
    st.FieldSource.pMDec.setValue(pMDec)
    st.FieldSource.SourceProperty.sourceFrequency.setValue(sourceFrequency)
    st.FieldSource.SourceProperty.sourceFluxI.setValue(sourceFluxI)
    st.FieldSource.SourceProperty.sourceDiameter.setValue(sourceDiameter)

    # Set the sourceName field
    if sourceName == "":
        # Generate source name in PKS-style 'JHHMM+DDd' e.g. 'J0354+223'
        hours = RA * (24 / (2 * pi))
        HH = floor(hours)
        MM = floor((hours - HH) * 60)
        DD = floor(degrees(Dec))
        d  = floor((degrees(Dec) - DD) * 10)
        sourceName = "J%02d%02d%+02d%1d" % (HH, MM, DD, d)
    st.FieldSource.sourceName.setValue(sourceName)

    # Return the structure
    return st


#
# Creating a source object for a planet
#
def create_source_planet(sourceName,
                         sourceFrequency=0,
                         sourceFluxI=0,
                         sourceDiameter=0):
    '''
    This helper function creates a FieldSource structure for
    a given planet. Optionally its properties (sourceFrequency,
    sourceFluxI, sourceDiameter) can be set. 
    
    Units and/or possible values:
    sourceName: "Mercury", "Venus", "Moon", "Mars", "Jupiter",
                "Saturn", "Uranus", "Neptune", "Pluto", "Ephemeris", "Sun"
    sourceFrequency: [GHz]
    sourceFluxI: [Jy]
    sourceDiameter: [arcsec]
    '''

    # Ignore differences in case.
    sourceName = sourceName.capitalize();

    # Predefined list of solar system objects
    solarSystemObjects = ['Mercury', 'Venus', 'Moon', 'Mars',
                          'Jupiter', 'Saturn', 'Uranus', 'Neptune',
                          'Pluto', 'Ephemeris', 'Sun']
    if sourceName not in solarSystemObjects:
        print "Invalid source object, valid names are:\n", solarSystemObjects
        return
    
    # Get a default source structure
    st = create_default_field_source()
    
    # nonSiderealMotion is 'true' in this case
    st.FieldSource.nonSiderealMotion.setValue("true")
    st.FieldSource.getAttributeNode('solarSystemObject').value = sourceName
    st.FieldSource.sourceName.setValue(sourceName)
    # Fill in the respective mandatory and optional fields
    st.FieldSource.SourceProperty.sourceFrequency.setValue(sourceFrequency)
    st.FieldSource.SourceProperty.sourceFluxI.setValue(sourceFluxI)
    st.FieldSource.SourceProperty.sourceDiameter.setValue(sourceDiameter)
    # Return the structure
    return st

#
# Creating a source object for fixed position
#
def create_source_AzEl(az, el, sourceName=""):
    '''
    This helper function creates a FieldSource structure based on
    the coordinates (Az and El) of the source object.
    
    Units and/or possible values:
    Az and El: [rad]
    '''
    
    # Get a default source structure
    st = create_default_field_source()
    
    # Fill in the respective mandatory and optional fields
    st.FieldSource.sourceCoordinates.longitude.setValue(az)
    st.FieldSource.sourceCoordinates.latitude.setValue(el)
    st.FieldSource.sourceCoordinates.attributes.getNamedItem("system").value = 'azel'

    # Return the structure
    return st

def create_default_spectral_spec():
    '''
    This function returns a default SpectralSpec structure starting
    from a default XML string. It converts the XML string into
    a XMLObject that is returned for further processing.
    '''

    defaultSpectralSpec = """<?xml version="1.0" encoding="UTF-8"?>
<SpectralSpec almatype="APDM::SpectralSpec" entityPartId="1200930231241/10747984 2104306688">
        <name>Simple setup (100.075GHz)</name>
        <integrationTime unit="sec">10.0</integrationTime>
        <dynamicRange>0.0</dynamicRange>
        <lO1Frequency unit="GHz">93.09499699775009</lO1Frequency>
        <BLCorrelatorConfiguration
            almatype="APDM::BLCorrelatorConfiguration"
            aPCDataSets="AP_UNCORRECTED" receiverType="TSB">
            <integrationDuration unit="sec">1.008</integrationDuration>
            <subScanDuration unit="sec">1.008</subScanDuration>
            <channelAverageDuration unit="sec">1.008</channelAverageDuration>
            <dumpDuration unit="sec">1.008</dumpDuration>
            <BLBaseBandConfig almatype="APDM::BLBaseBandConfig"
                cAM="NORMAL" dataProducts="CROSS_AND_AUTO"
                baseBandName="BB_1" sideBandSeparationMode="NONE" switchingType="NO_SWITCHING">
                <dwellTime unit="msec">48.0</dwellTime>
                <deadTime unit="sec">0.0</deadTime>
                <numberOfPositions>0</numberOfPositions>
                <lO2Frequency unit="GHz">9.98</lO2Frequency>
                <centerFrequency unit="Ghz">100.07499699775009</centerFrequency>
                <BLSpectralWindow almatype="APDM::BLSpectralWindow"
                    sideBand="USB" windowFunction="HANNING"
                    polnProducts="XX" correlationBits="BITS_2x2">
                    <centerFrequency unit="GHz">3.0</centerFrequency>
                    <spectralAveragingFactor>1</spectralAveragingFactor>
                    <name>BLSW</name>
                    <effectiveBandwidth unit="MHz">2000</effectiveBandwidth>
                    <effectiveNumberOfChannels>256</effectiveNumberOfChannels>
                    <associatedSpectralWindowNumberInPair>1</associatedSpectralWindowNumberInPair>
                    <useThisSpectralWindow>true</useThisSpectralWindow>
                    <correlationNyquistOversampling>false</correlationNyquistOversampling>
                    <quantizationCorrection>true</quantizationCorrection>
                    <ChannelAverageRegion almatype="APDM::ChannelAverageRegion">
                        <startChannel>10</startChannel>
                        <numberChannels>246</numberChannels>
                    </ChannelAverageRegion>
                </BLSpectralWindow>
            </BLBaseBandConfig>
        </BLCorrelatorConfiguration>
        <FrequencySetup almatype="APDM::FrequencySetup" receiverBand="ALMA_RB_03">
            <restFrequency unit="GHz">100.075</restFrequency>
            <transitionName>Cont(100.075 GHz)</transitionName>
        </FrequencySetup>
</SpectralSpec>"""

    # Transform the XML string into a XMLObject
    obj = XmlObject(defaultSpectralSpec)

    # Return the XMLObject
    return obj.SpectralSpec

def create_default_corr_conf():
    
    defaultCorrConf = """<BLCorrelatorConfiguration
            almatype="APDM::BLCorrelatorConfiguration"
            aPCDataSets="AP_UNCORRECTED" receiverType="TSB">
            <integrationDuration unit="sec">1.008</integrationDuration>
            <subScanDuration unit="sec">1.008</subScanDuration>
            <channelAverageDuration unit="sec">1.008</channelAverageDuration>
            <dumpDuration unit="sec">1.008</dumpDuration>
            <BLBaseBandConfig almatype="APDM::BLBaseBandConfig"
                cAM="NORMAL" dataProducts="CROSS_AND_AUTO"
                baseBandName="BB_1" sideBandSeparationMode="NONE" switchingType="NO_SWITCHING">
                <dwellTime unit="msec">48.0</dwellTime>
                <deadTime unit="sec">0.0</deadTime>
                <numberOfPositions>0</numberOfPositions>
                <lO2Frequency unit="GHz">9.98</lO2Frequency>
                <centerFrequency unit="Ghz">100.07499699775009</centerFrequency>
                <BLSpectralWindow almatype="APDM::BLSpectralWindow"
                    sideBand="USB" windowFunction="HANNING"
                    polnProducts="XX" correlationBits="BITS_2x2">
                    <centerFrequency unit="GHz">3.0</centerFrequency>
                    <spectralAveragingFactor>1</spectralAveragingFactor>
                    <name>BLSW</name>
                    <effectiveBandwidth unit="MHz">2000</effectiveBandwidth>
                    <effectiveNumberOfChannels>256</effectiveNumberOfChannels>
                    <associatedSpectralWindowNumberInPair>1</associatedSpectralWindowNumberInPair>
                    <useThisSpectralWindow>true</useThisSpectralWindow>
                    <correlationNyquistOversampling>false</correlationNyquistOversampling>
                    <quantizationCorrection>true</quantizationCorrection>
                    <ChannelAverageRegion almatype="APDM::ChannelAverageRegion">
                        <startChannel>10</startChannel>
                        <numberChannels>246</numberChannels>
                    </ChannelAverageRegion>
                </BLSpectralWindow>
            </BLBaseBandConfig>
        </BLCorrelatorConfiguration>"""

    # Transform the XML string into a XMLObject
    obj = XmlObject(defaultCorrConf)

    # Return the XMLObject
    return obj.BLCorrelatorConfiguration


def aggregate(object):
    """
    Utility function provided to get a uniform access to those elements that
    can occur more than once in the XML document. If there's more than one,
    XMLObjectifier will generate a list of objects. However, if there's only
    one, then just the object will be generated. 
        
    This function checks whether the object is a list, and if it is not, 
    it wraps it in a list.
    """
    if type(object) == type([]):
        return object
    else:
        return [object]

def ACA_corr_config_to_IDL(corrConf):
    '''
    Converts the ACA Correlator Configuration defined in the APDM (SchedBlock) to
    the Correlator::CorrelatorConfiguration IDL structure required by CORR.
    '''
    baseBandConfigs = aggregate(corrConf.ACABaseBandConfig)
    nBaseBandConfigs = len(baseBandConfigs)
    baseBandConfigsIDL = []
    for bb in baseBandConfigs:
        
        basebandName = PyDataModelEnumeration.PyBasebandName.fromString(bb.getAttribute('baseBandName'))
        CAM = PyDataModelEnumeration.PyAccumMode.fromString(bb.getAttribute('cAM'))
        dataProducts = PyDataModelEnumeration.PyCorrelationMode.fromString(bb.getAttribute('dataProducts')) 
        sideBandSeparationMode = PyDataModelEnumeration.PySidebandProcessingMode.fromString(bb.getAttribute('sideBandSeparationMode'))
         
        spectralWindows = []
        for sw in aggregate(bb.ACASpectralWindow):
            
            centerFrequencyMHz = float(sw.centerFrequency.getValue() * 1.0E3) # TODO deal with different units
            effectiveBandwidthMHz = float(sw.effectiveBandwidth.getValue() * 1.0E3) # TODO deal with different units
            effectiveNumberOfChannels = long(sw.effectiveNumberOfChannels.getValue())
            spectralAveragingFactor = long(sw.spectralAveragingFactor.getValue())
            sideBand = PyDataModelEnumeration.PyNetSideband.fromString(sw.getAttribute('sideBand'))
            
            # This attribute associates spectral windows pairs in DSB reception
            # for sideband separation. The OT is not associating spectral windows pairs.
            # This value is set to 0, no sideband separation, for now.
            associatedSpectralWindowNumberInPair = long(0)
           
            if sw.useThisSpectralWindow.getValue():
                useThisSpectralWindow = True
            else:
                useThisSpectralWindow = False
	    useThisSpectralWindow = True #TODO:CPA:fix the above if sentence that is not working 
            windowFunction = PyDataModelEnumeration.PyWindowFunction.fromString(sw.getAttribute('windowFunction'))
            channelAverageRegions = []
            for ca in aggregate(sw.ChannelAverageRegion):
                startChannel = long(ca.startChannel.getValue())
                numberChannels = long(ca.numberChannels.getValue())
                channelAverageRegion = Correlator.ChannelAverageRegion(startChannel,
                                                                       numberChannels)
                channelAverageRegions.append(channelAverageRegion)
            if sw.frqChProfReproduction.getValue():    
                frqChProfReproduction = True
            else:                
                frqChProfReproduction = False
            correlationBits = PyDataModelEnumeration.PyCorrelationBit.BITS_2x2 # BL specific
            correlationNyquistOversampling = False # BL specific
            #polnProductsSeq = [PyDataModelEnumeration.PyStokesParameter.XX,
            #                   PyDataModelEnumeration.PyStokesParameter.XY,
            #                   PyDataModelEnumeration.PyStokesParameter.YX,
            #                   PyDataModelEnumeration.PyStokesParameter.YY] # BL specific
            polnProducts = sw.getAttribute("polnProducts").split(',')
            polnProductsSeq = []
            for poln in polnProducts:
                polnProductsSeq.append(PyDataModelEnumeration.PyStokesParameter.fromString(poln.strip()))
            quantizationCorrection = False # BL specific
            
            idlSpectralWindow = Correlator.SpectralWindow(centerFrequencyMHz,
                                                          effectiveBandwidthMHz,
                                                          effectiveNumberOfChannels,
                                                          spectralAveragingFactor,
                                                          sideBand,
                                                          associatedSpectralWindowNumberInPair,
                                                          useThisSpectralWindow,
                                                          windowFunction,
                                                          channelAverageRegions,
                                                          frqChProfReproduction,
                                                          correlationBits,
                                                          correlationNyquistOversampling,
                                                          polnProductsSeq,
                                                          quantizationCorrection)
            spectralWindows.append(idlSpectralWindow)
        
        switchingType = PyDataModelEnumeration.PySwitchingMode.fromString(bb.getAttribute('switchingType'))
        dwellTimes = aggregate(bb.dwellTime)
        deadTimes = aggregate(bb.deadTime)
        numberOfPositions = len(dwellTimes)
        idlDwellTimes = []
        idlDeadTimes = []
        for dwt in dwellTimes:
            idlDwellTimes.append(long(dwt.getValue()))
        for ddt in deadTimes:
            idlDeadTimes.append(long(ddt.getValue()))
        binSwitchingMode = Correlator.BinSwitching_t(switchingType,
                                                     numberOfPositions,
                                                     idlDwellTimes,
                                                     idlDeadTimes)
        polarizationMode = PyDataModelEnumeration.PyACAPolarization.ACA_STANDARD
        centerFreqOfResidualDelayMHz = float(bb.centerFreqOfResidualDelay.getValue()*1000)
                
        baseBandConfigIDL = Correlator.BaseBandConfig(basebandName,
                                                      CAM,
                                                      dataProducts,
                                                      sideBandSeparationMode,
                                                      spectralWindows,
                                                      binSwitchingMode,
                                                      polarizationMode,
                                                      centerFreqOfResidualDelayMHz)
        baseBandConfigsIDL.append(baseBandConfigIDL)
        
    dumpDuration = long(16E4) # BL specific. Dump duration is 16 ms for ACA Correlator.
    integrationDuration = long(corrConf.integrationDuration.getValue()*1E7)
    channelAverageDuration = long(corrConf.channelAverageDuration.getValue()*1E7)
    subScanDuration = long(corrConf.subScanDuration.getValue()*1E7)
    receiverType = PyDataModelEnumeration.PyReceiverSideband.fromString(corrConf.getAttribute('receiverType'))
    baseBands = baseBandConfigsIDL

    apcDataSets = corrConf.getAttribute('aPCDataSets')
    APCDataSetSeq = []
    for apcds in apcDataSets.split('+'):
        APCDataSetSeq.append(PyDataModelEnumeration.PyAtmPhaseCorrection.fromString(apcds.strip()))
    
    doD180modulation = corrConf.ACAPhaseSwitchingConfiguration.doD180modulation.getValue()
    doD180demodulation = corrConf.ACAPhaseSwitchingConfiguration.doD180demodulation.getValue()
        
    d180Duration = long(corrConf.ACAPhaseSwitchingConfiguration.d180Duration.getValue())
    ACAPhaseSwConfig = Correlator.ACAPhaseSwitchingConfigurations(doD180modulation,
                                                                  doD180demodulation,
                                                                  d180Duration)
    correlatorConfiguration = Correlator.CorrelatorConfiguration(dumpDuration,
        integrationDuration,
        channelAverageDuration,
        subScanDuration,
        receiverType,
        baseBands,
        APCDataSetSeq,
        ACAPhaseSwConfig)
    return correlatorConfiguration

def BL_corr_config_to_IDL(corrConf):
    '''
    Converts the BL Correlator Configuration defined in the APDM (SchedBlock) to
    the Correlator::CorrelatorConfiguration IDL structure required by CORR.
    '''
    baseBandConfigs = aggregate(corrConf.BLBaseBandConfig)
    nBaseBandConfigs = len(baseBandConfigs)
    baseBandConfigsIDL = []
    for bb in baseBandConfigs:
        
        basebandName = PyDataModelEnumeration.PyBasebandName.fromString(bb.getAttribute('baseBandName'))
        CAM = PyDataModelEnumeration.PyAccumMode.fromString(bb.getAttribute('cAM'))
        dataProducts = PyDataModelEnumeration.PyCorrelationMode.fromString(bb.getAttribute('dataProducts')) 
        sideBandSeparationMode = PyDataModelEnumeration.PySidebandProcessingMode.fromString(bb.getAttribute('sideBandSeparationMode'))
         
        spectralWindows = []
        for sw in aggregate(bb.BLSpectralWindow):
            
            factor = 1.0
            if sw.centerFrequency.getAttribute('unit') == 'MHz':
                factor = 1.0
            elif sw.centerFrequency.getAttribute('unit') == 'GHz': 
                factor = 1.0E3
            centerFrequencyMHz = float(sw.centerFrequency.getValue() * factor) # TODO deal with different units

            factor = 1.0
            if sw.effectiveBandwidth.getAttribute('unit') == 'MHz':
                factor = 1.0
            elif sw.effectiveBandwidth.getAttribute('unit') == 'GHz': 
                factor = 1.0E3
            effectiveBandwidthMHz = float(sw.effectiveBandwidth.getValue() * factor) # TODO deal with different units

            effectiveNumberOfChannels = long(sw.effectiveNumberOfChannels.getValue())
            spectralAveragingFactor = long(sw.spectralAveragingFactor.getValue())
            sideBand = PyDataModelEnumeration.PyNetSideband.fromString(sw.getAttribute('sideBand'))
            
            # This attribute associates spectral windows pairs in DSB reception
            # for sideband separation. The OT is not associating spectral windows pairs.
            # This value is set to 0, no sideband separation, for now.
            associatedSpectralWindowNumberInPair = long(0)
            if sw.useThisSpectralWindow.getValue():
                useThisSpectralWindow = True
            else:
                useThisSpectralWindow = False
            windowFunction = PyDataModelEnumeration.PyWindowFunction.fromString(sw.getAttribute('windowFunction'))
            channelAverageRegions = []
            for ca in aggregate(sw.ChannelAverageRegion):
                startChannel = long(ca.startChannel.getValue())
                numberChannels = long(ca.numberChannels.getValue())
                channelAverageRegion = Correlator.ChannelAverageRegion(startChannel,
                                                                       numberChannels)
                channelAverageRegions.append(channelAverageRegion)
            frqChProfReproduction = False # ACA specific
            correlationBits = PyDataModelEnumeration.PyCorrelationBit.fromString(sw.getAttribute("correlationBits"))
            if sw.correlationNyquistOversampling.getValue() == "true":
                correlationNyquistOversampling = True
            else:
                correlationNyquistOversampling = False
                
                
            polnProducts = sw.getAttribute("polnProducts").split(',')
            polnProductsSeq = []
            for poln in polnProducts:
                polnProductsSeq.append(PyDataModelEnumeration.PyStokesParameter.fromString(poln.strip()))
            quantizationCorrection = sw.quantizationCorrection.getValue()
            
            idlSpectralWindow = Correlator.SpectralWindow(centerFrequencyMHz,
                                                          effectiveBandwidthMHz,
                                                          effectiveNumberOfChannels,
                                                          spectralAveragingFactor,
                                                          sideBand,
                                                          associatedSpectralWindowNumberInPair,
                                                          useThisSpectralWindow,
                                                          windowFunction,
                                                          channelAverageRegions,
                                                          frqChProfReproduction,
                                                          correlationBits,
                                                          correlationNyquistOversampling,
                                                          polnProductsSeq,
                                                          quantizationCorrection)
            spectralWindows.append(idlSpectralWindow)
        
        switchingType = PyDataModelEnumeration.PySwitchingMode.fromString(bb.getAttribute('switchingType'))
        dwellTimes = aggregate(bb.dwellTime)
        deadTimes = aggregate(bb.deadTime)
        numberOfPositions = len(dwellTimes)
        idlDwellTimes = []
        idlDeadTimes = []
        for dwt in dwellTimes:
            idlDwellTimes.append(long(dwt.getValue()))
        for ddt in deadTimes:
            idlDeadTimes.append(long(ddt.getValue()))
        binSwitchingMode = Correlator.BinSwitching_t(switchingType,
                                                     numberOfPositions,
                                                     idlDwellTimes,
                                                     idlDeadTimes)
        polarizationMode = PyDataModelEnumeration.PyACAPolarization.ACA_STANDARD # ACA specific
        centerFreqOfResidualDelayMHz = 0.0 # ACA specific
                
        baseBandConfigIDL = Correlator.BaseBandConfig(basebandName,
                                                      CAM,
                                                      dataProducts,
                                                      sideBandSeparationMode,
                                                      spectralWindows,
                                                      binSwitchingMode,
                                                      polarizationMode,
                                                      centerFreqOfResidualDelayMHz)
        baseBandConfigsIDL.append(baseBandConfigIDL)
        
    dumpDuration = long(round(corrConf.dumpDuration.getValue()*1E3)*1E4)
    integrationDuration = long(round(corrConf.integrationDuration.getValue()*1E3)*1E4)
    channelAverageDuration = long(round(corrConf.channelAverageDuration.getValue()*1E3)*1E4)
    subScanDuration = long(round(corrConf.subScanDuration.getValue()*1E3)*1E4)
    receiverType = PyDataModelEnumeration.PyReceiverSideband.fromString(corrConf.getAttribute('receiverType'))
    baseBands = baseBandConfigsIDL
    
    apcDataSets = corrConf.getAttribute('aPCDataSets')
    APCDataSetSeq = []
    for apcds in apcDataSets.split('+'):
        APCDataSetSeq.append(PyDataModelEnumeration.PyAtmPhaseCorrection.fromString(apcds.strip()))

    # ACA specific
    ACAPhaseSwConfig = Correlator.ACAPhaseSwitchingConfigurations(False,
                                                                  False,
                                                                  0)
    correlatorConfiguration = Correlator.CorrelatorConfiguration(dumpDuration,
        integrationDuration,
        channelAverageDuration,
        subScanDuration,
        receiverType,
        baseBands,
        APCDataSetSeq,
        ACAPhaseSwConfig)
    return correlatorConfiguration

def corr_config_to_idl(corrConf):
    """
    Transform the correlator configuration from the Python XML binding class
    to IDL.        
    """
    baseBandConfigs = aggregate(corrConf.BaseBandConfig)
    nBaseBandConfigs = len(baseBandConfigs)
    baseBandConfigsIDL = []
    for bb in range(nBaseBandConfigs):
        # Spectral Windows      
        spectralSubbandSets = aggregate(baseBandConfigs[bb].SpectralSubbandSet)  
        nSpectSubBandSets = len(spectralSubbandSets)
        spectralWindowsIDL = []
        for ssbs in range(nSpectSubBandSets):
            
            if spectralSubbandSets[ssbs].centerFrequency.getAttribute('unit') == 'MHz' :
                centerFrequencyMHZ = float(spectralSubbandSets[ssbs].centerFrequency.getValue())
            elif spectralSubbandSets[ssbs].centerFrequency.getAttribute('unit') == 'GHz' :
                centerFrequencyMHZ = float(spectralSubbandSets[ssbs].centerFrequency.getValue()) * 1000.0
            else:
                raise "getCorrelatorConfigurationIDL only knows about MHz and GHz for centerFrequency"
            tFBMode = int(spectralSubbandSets[ssbs].tFBMode.getValue())
            # correlatorFraction
            fraction = int(spectralSubbandSets[ssbs].correlatorFraction.getValue())
            if fraction == 100:
                correlatorFraction = Correlator.FRACTION_1_1
            elif fraction == 50:
                correlatorFraction = Correlator.FRACTION_1_2
            elif fraction == 25:
                correlatorFraction = Correlator.FRACTION_1_4
            elif fraction == 12:
                correlatorFraction = Correlator.FRACTION_1_8
            elif fraction == 6:
                correlatorFraction = Correlator.FRACTION_1_16
            elif fraction == 3:
                correlatorFraction = Correlator.FRACTION_1_32
            else:
                raise "Wrong correlator fraction: " + str(fraction)
            numOverlappedChannels = int(spectralSubbandSets[ssbs].numOverlapChannels.getValue())
            spectralAveragingFactor = int(spectralSubbandSets[ssbs].spectralAveragingFactor.getValue())
            
            spectralWindowIDL = Correlator.SpectralWindow(centerFrequencyMHZ, \
                                                          tFBMode, \
                                                          correlatorFraction, \
                                                          numOverlappedChannels, \
                                                          spectralAveragingFactor)
            spectralWindowsIDL.append(spectralWindowIDL)

        baseBandIndex = int(baseBandConfigs[bb].baseBandIndex.getValue())

        #Now that the cAM is correctly set in the CORR Config, the
        # the following fails so we check by hand
        #cAM = Correlator.eCAM._item(int(baseBandConfigs[bb].cAM.getValue()))
        if int(baseBandConfigs[bb].cAM.getValue()) == 16:
            cAM = Correlator.eCAM._item(1)
        else:
            cAM = Correlator.eCAM._item(0)

        # dataProducts
        dp = baseBandConfigs[bb].getAttribute("dataProducts")
        if dp == "AUTO_CROSS_PRODUCTS":
            dataProducts = Correlator.AUTO_CROSS_PRODUCTS
        elif dp == "AUTO_ONLY_PRODUCTS":
            dataProducts = Correlator.AUTO_ONLY_PRODUCTS
        elif dp == "CROSS_ONLY_PRODUCTS":
            dataProducts = Correlator.CROSS_ONLY_PRODUCTS
        else:
            raise "Invalid data product: " + dp
        binMode = int(baseBandConfigs[bb].binMode.getValue())

        timeUnit = baseBandConfigs[bb].bin0Duration.getAttribute('unit')
        if timeUnit == 'msec':
            bin0Duration = int(float(baseBandConfigs[bb].bin0Duration.getValue()) * 10000)
        elif timeUnit == 'sec':
            bin0Duration = int(float(baseBandConfigs[bb].bin0Duration.getValue()) * 10000000)
        else:
            raise "Bin 0 Duration must be in msec or sec, not in "+timeUnit

        timeUnit = baseBandConfigs[bb].bin1Duration.getAttribute('unit')
        if timeUnit == 'msec':
            bin1Duration = int(float(baseBandConfigs[bb].bin1Duration.getValue()) * 10000)
        elif timeUnit == 'sec':
            bin1Duration = int(float(baseBandConfigs[bb].bin1Duration.getValue()) * 10000000)
        else:
            raise "Bin 1 Duration must be in either msec or sec, not in "+timeUnit
        numSwitchCycles = int(baseBandConfigs[bb].numSwitchCycles.getValue())
        aPCDataSets = Correlator.eAPCData._item(int(baseBandConfigs[bb].aPCDataSets.getValue()))
        if baseBandConfigs[bb].quantizationCorrection.getValue():
            quantCorr = 1
        else:
            quantCorr = 0
        if baseBandConfigs[bb].fFT.getValue():
            fFT = 1
        else:
            fFT = 0
        # windowFunction
        wf = baseBandConfigs[bb].getAttribute("windowFunction").upper()
        if wf == "UNIFORM":
            windowFunction = Correlator.UNIFORM
        elif wf == "HANNING":
            windowFunction = Correlator.HANNING
        elif wf == "HAMMING":
            windowFunction = Correlator.HAMMING
        elif wf == "BARTLETT":
            windowFunction = Correlator.BARTLETT
        elif wf == "BLACKMAN":
            windowFunction = Correlator.BLACKMAN
        elif wf == "BLACKMAN_HARRIS":
            windowFunction = Correlator.BLACKMAN_HARRIS
        elif wf == "WELCH":
            windowFunction = Correlator.WELCH
        else:
            raise "Invalid window function: " + wf
              
        timeUnit = baseBandConfigs[bb].channelAverageDuration.getAttribute('unit')
        if timeUnit != 'msec':
            raise "Channel Average Duration must be in msec, not in "+timeUnit
        channelAvgDuration = int(float(baseBandConfigs[bb].channelAverageDuration.getValue()) * 10000)    

        # channelAvgRegions
        channelAvgBands = aggregate(baseBandConfigs[bb].ChannelAverageBands)
        nChannelAvgBands = len(channelAvgBands)
        channelAvgRegionsIDL = []
        for cab in range(nChannelAvgBands):
            startChannel = int(channelAvgBands[cab].startChannel.getValue())
            numberChannels = int(channelAvgBands[cab].numberChannels.getValue())
            spectralWindowIndex = int(channelAvgBands[cab].subbandSetIndex.getValue())
            channelAvgBandIDL = Correlator.ChannelAverageRegion(startChannel, \
                                                                numberChannels, \
                                                                spectralWindowIndex)
            channelAvgRegionsIDL.append(channelAvgBandIDL)                                                                   
            
#         sc = aggregate(baseBandConfigs[bb].ChannelAverageBands.startChannel)
#         nc = aggregate(baseBandConfigs[bb].ChannelAverageBands.numberChannels)
#         sbi = aggregate(baseBandConfigs[bb].ChannelAverageBands.subbandSetIndex)
#         for ca in range(len(sc)):
#             startChannel = int(sc[ca].getValue())
#             numberChannels = int(nc[ca].getValue())
#             spectralWindowIndex = int(sbi[ca].getValue())            
#             channelAvgBandIDL = Correlator.ChannelAverageRegion(startChannel, \
#                                                                 numberChannels, \
#                                                                 spectralWindowIndex)
#             channelAvgRegionsIDL.append(channelAvgBandIDL)                                                                   
                
        baseBandConfigIDL = Correlator.BaseBandConfig(baseBandIndex, \
                                                          cAM, \
                                                          dataProducts, \
                                                          binMode, \
                                                          bin0Duration, \
                                                          bin1Duration, \
                                                          numSwitchCycles, \
                                                          aPCDataSets, \
                                                          quantCorr, \
                                                          fFT, \
                                                          windowFunction, \
                                                          channelAvgDuration, \
                                                          channelAvgRegionsIDL, \
                                                          spectralWindowsIDL)
        baseBandConfigsIDL.append(baseBandConfigIDL)

    integrationDuration = int(corrConf.integrationDuration.getValue())
    subScanDuration = int(corrConf.subScanDuration.getValue())
       
    corrConfIDL = Correlator.CorrelatorConfiguration(integrationDuration, \
                                                         subScanDuration, \
                                                         baseBandConfigsIDL)
    return corrConfIDL


def totalPower_default_idl_config():
    """
    Transform the total power configuration from the Python XML binding class
    to IDL.        
    """

    """
    For now this class will use a defaul configuration until I find a XML
    format to give the Total Power Configuration.
    """
    baseBandConfigsIDL = []
    

    #Baseband index
    baseBandIndex = 1
    
    #Baseband Name
    baseBandName = BasebandNameMod.BB_1
    
    #Bin0 duration  TODO I don't know what this means, 0 DON'T work
    bin0Duration = 480000000 

    #Bin1 duration TODO I don't know what this means, 0 maybe works
    bin1Duration = 0
    
    #number SwitchCycles
    numSwitchCycles = 1 
    
    #Window Function
    windowFunction  = Control.HANNING

    #Polarizations (using only polarization 0 for now)
    polarizations = []
    pol = 0
    numCorr = 1  # No idea 
    corrTypes = [StokesParameterMod.XX]     # I have NO idea what this means
    corrProduct = [[PolarizationTypeMod.X]] # I have NO idea what this means
                                 
    centerFrequency    = 111.0  # Still not having any idea of this
    refFrequency       = 0.0  
    totalBandwidth     = 2.0 
    effectiveBandwidth = 0.0
    netSideband = NetSidebandMod.LSB
    spectSetup = [ Control.SpectralSetup(centerFrequency, refFrequency, totalBandwidth, \
                                       effectiveBandwidth, netSideband) ]
    
    polarization = Control.TotalPowerPolarization(pol, \
                                                 numCorr, \
                                                 corrTypes, \
                                                 corrProduct, \
                                                 spectSetup)
    polarizations.append(polarization)
    
                    
    baseBandConfigIDL = Control.TotalPowerBaseBandConfig(baseBandIndex, \
                                                         baseBandName, \
                                                         bin0Duration, \
                                                         bin1Duration, \
                                                         numSwitchCycles, \
                                                         windowFunction, \
                                                         polarizations)

    baseBandConfigsIDL.append(baseBandConfigIDL)

        
    # creating the TotalPowerConfiguration IDL structure 
    integrationDuration = 5000
    subScanDuration     = 10000 # Count of integrations

       
    tpConfIDL = Control.TotalPowerConfiguration(integrationDuration, \
                                                         subScanDuration, \
                                                         baseBandConfigsIDL)
    return tpConfIDL


def totalPower_default_idl_config2(duration):
    """
    Transform the total power configuration from the Python XML binding class
    to IDL.        
    """

    """
    For now this class will use a defaul configuration until I find a XML
    format to give the Total Power Configuration.
    """
    baseBandConfigsIDL = []
    

    #Baseband index
    baseBandIndex = 1
    
    #Baseband Name
    baseBandName = BasebandNameMod.BB_1
    
    #Bin0 duration  TODO I don't know what this means, 0 DON'T work
    bin0Duration = 480000000 

    #Bin1 duration TODO I don't know what this means, 0 maybe works
    bin1Duration = 0
    
    #number SwitchCycles
    numSwitchCycles = 1 
    
    #Window Function
    windowFunction  = Control.HANNING

    #Polarizations (using only polarization 0 for now)
    polarizations = []
    pol = 0
    numCorr = 1  # No idea 
    corrTypes = [StokesParameterMod.XX]     # I have NO idea what this means
    corrProduct = [[PolarizationTypeMod.X]] # I have NO idea what this means
                                 
    centerFrequency    = 111.0  # Still not having any idea of this
    refFrequency       = 0.0  
    totalBandwidth     = 2.0 
    effectiveBandwidth = 0.0
    netSideband = NetSidebandMod.LSB
    spectSetup = [ Control.SpectralSetup(centerFrequency, refFrequency, totalBandwidth, \
                                       effectiveBandwidth, netSideband) ]
    
    polarization = Control.TotalPowerPolarization(pol, \
                                                 numCorr, \
                                                 corrTypes, \
                                                 corrProduct, \
                                                 spectSetup)
    polarizations.append(polarization)
    
                    
    baseBandConfigIDL = Control.TotalPowerBaseBandConfig(baseBandIndex, \
                                                         baseBandName, \
                                                         bin0Duration, \
                                                         bin1Duration, \
                                                         numSwitchCycles, \
                                                         windowFunction, \
                                                         polarizations)

    baseBandConfigsIDL.append(baseBandConfigIDL)

        
    # creating the TotalPowerConfiguration IDL structure 
    integrationDuration = 5000
    subScanDuration     = duration # Count of integrations

       
    tpConfIDL = Control.TotalPowerConfiguration(integrationDuration, \
                                                         subScanDuration, \
                                                         baseBandConfigsIDL)
    return tpConfIDL


def totalPower_default_idl_config3(duration):
    """
    Transform the total power configuration from the Python XML binding class
    to IDL.        
    """

    """
    For now this class will use a defaul configuration until I find a XML
    format to give the Total Power Configuration.
    """
    baseBandConfigsIDL = []
    

    #Baseband index
    baseBandIndex = 1
    
    #Baseband Name
    baseBandName = BasebandNameMod.BB_1
    
    #Bin0 duration  TODO I don't know what this means, 0 DON'T work
    bin0Duration = 480000000 

    #Bin1 duration TODO I don't know what this means, 0 maybe works
    bin1Duration = 0
    
    #number SwitchCycles
    numSwitchCycles = 1 
    
    #Window Function
    windowFunction  = Control.HANNING

    #Polarizations (using only polarization 0 for now)
    polarizations = []
    pol = 0
    numCorr = 1  # No idea 
    corrTypes = [StokesParameterMod.XX]     # I have NO idea what this means
    corrProduct = [[PolarizationTypeMod.X]] # I have NO idea what this means
                                 
    centerFrequency    = 111.0  # Still not having any idea of this
    refFrequency       = 0.0  
    totalBandwidth     = 2.0 
    effectiveBandwidth = 0.0
    netSideband = NetSidebandMod.LSB
    spectSetup = [ Control.SpectralSetup(centerFrequency, refFrequency, totalBandwidth, \
                                       effectiveBandwidth, netSideband) ]
    
    polarization = Control.TotalPowerPolarization(pol, \
                                                 numCorr, \
                                                 corrTypes, \
                                                 corrProduct, \
                                                 spectSetup)
    polarizations.append(polarization)
    
                    
    baseBandConfigIDL = Control.TotalPowerBaseBandConfig(baseBandIndex, \
                                                         baseBandName, \
                                                         bin0Duration, \
                                                         bin1Duration, \
                                                         numSwitchCycles, \
                                                         windowFunction, \
                                                         polarizations)

    baseBandConfigsIDL.append(baseBandConfigIDL)

    baseBandConfigIDL2 = Control.TotalPowerBaseBandConfig(2, \
                                                         BasebandNameMod.BB_2, \
                                                         bin0Duration, \
                                                         bin1Duration, \
                                                         numSwitchCycles, \
                                                         windowFunction, \
                                                         polarizations)
    baseBandConfigsIDL.append(baseBandConfigIDL2)
    

        
    # creating the TotalPowerConfiguration IDL structure 
    integrationDuration = 5000
    subScanDuration     = duration # Count of integrations

       
    tpConfIDL = Control.TotalPowerConfiguration(integrationDuration, \
                                                         subScanDuration, \
                                                         baseBandConfigsIDL)
    return tpConfIDL

def spectral_spec_to_idl(spectSpec):
    """
    Transform the spectral specification from the Python XML binding class
    to IDL.
    """

    try:
        integrationTime = float(spectSpec.integrationTime.getValue())
    except:
        integrationTime = 0.0
    try:    
        dynamicRange = float(spectSpec.dynamicRange.getValue())
    except:
        dynamicRange = 0.0
    restFrequency = float(spectSpec.FrequencySetup.restFrequency.getValue())
    try:
        transitionName = spectSpec.FrequencySetup.transitionName.getValue()
    except:
        transitionName = ""

    # I think this has something to do with the new enumeration framework
    try:
        receiverBand = ControlSB.ReceiverBandType._item(int(spectSpec.FrequencySetup.getAttribute("receiverBand")))
    except:
        receiverBand = ControlSB.ReceiverBandType._item(0)

    # Check whether lO1Frequency has no content in XML.
    if len(spectSpec.lO1Frequency.childNodes) == 0:
        LO1 = -1.0
    else:
        LO1 = float(spectSpec.lO1Frequency.getValue())

    spectSpecIDL = ControlSB.SpectralSpec(integrationTime, \
                                          dynamicRange, \
                                          restFrequency, \
                                          transitionName, \
                                          receiverBand, \
                                          LO1)
    return spectSpecIDL

# __oOo__
