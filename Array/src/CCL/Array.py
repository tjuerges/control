#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import Control
import ArrayExceptions
import ObservingModeExceptions
import CCLExceptionsImpl
import log_audience
import ACSLog

from CCL.Antenna import Antenna
from CCL.logging import getLogger
from CCL.OpticalPointingObservingMode import OpticalPointingObservingMode
from CCL.SingleFieldInterferometryObservingMode import SFIObservingMode
from CCL.TowerHolographyObservingMode import TowerHolographyObservingMode
from CCL.TowerHolography7mObservingMode import TowerHolography7mObservingMode
from CCL.TotalPowerObservingMode import TotalPowerObservingMode
from CCL.Container import getComponent
from CCL.CalResults import CalResults

class Array:

    def __init__(self, arrayName):
        self.logger = getLogger()
        arrayComponentName = 'CONTROL/' + arrayName;
        self.logger.logDebug("Getting component "+arrayComponentName)
        self._arrayName = arrayComponentName
        self._array = getComponent(arrayComponentName)

    def __del__(self): pass

    def antennas(self):
        return self._array.getAntennas()

    def getAntenna(self, antennaName):
        if not antennaName in self.antennas():
            helper = CCLExceptionsImpl.WrongParameterExImpl()
            helper.addData('Detail', "Antenna '" + antenna_name + "' doesn't belong to this array.")
            raise helper
        return Antenna(antennaName)

    def beginExecution(self):
        self._array.beginExecution()

    def endExecution(self, completion, message):
        self._array.endExecution(completion, message)

    def getSingleFieldInterferometryObservingMode(self):
        return SFIObservingMode(self._array.getSFIOffshoot(), self.logger)

    def getTowerHolographyObservingMode(self):
        return TowerHolographyObservingMode(self._array.getTowerHolographyOffshoot(),
                                                self.logger)
    def getTowerHolography7mObservingMode(self):
        return TowerHolography7mObservingMode(self._array.getTowerHolography7mOffshoot(),
                                                self.logger)
    def getOpticalPointingObservingMode(self):
        return OpticalPointingObservingMode(self._array.getOpticalPointingOffshoot(),
                                                self.logger)

    def getTotalPowerObservingMode(self):
        return TotalPowerObservingMode( \
                   self._array.getTotalPowerOffshoot(),
                   self.logger)

    def getAstronomicalHolographyObservingMode(self):
        import CCL.AstronomicalHolography
        return CCL.AstronomicalHolography.AstronomicalHolography\
               (self._array.getAstronomicalHolographyOffshoot(),
                self.logger)

    def getCalResults(self):
        return CalResults(self._array.getExecutionStateName())

    def getExecBlockUID(self):
        """
        Returns the Execution Block UID, or None if the array is not currently
        executing an Execution Block (i.e., between beginExecution() and
        endExecution()).
        """
        entref = self._array.getASDMEntityRef()
        asdmUID = None
        if entref is not None:
            asdmUID = entref.entityId
        return asdmUID

    def STATUS(self): pass

    def logToOperator(self, msg, level= ACSLog.ACS_LOG_INFO):
        '''
        Helper method to log to send a log message to the operator
        the default log level is INFO but can be overridden.
        '''
        self.logger.logNotSoTypeSafe(level, msg,
                                                  log_audience.OPERATOR,
                                                  self._arrayName,
                                                  None)

    def logToScience(self, msg, level= ACSLog.ACS_LOG_INFO):
        '''
        Helper method to log to send a log message to the operator
        the default log level is INFO but can be overridden.
        '''
        self.logger.logNotSoTypeSafe(level, msg,
                                                  log_audience.SCILOG,
                                                  self._arrayName,
                                                  None)

    def logScriptID(self, idTag):
        '''
        Helper method to enable the simple logging of the script that
        is being exectuted to the SCILOG audience.  This is designed to
        work with the RCS ID Tag
        '''
        idTagSplit = idTag.split()
        msg = "Executing script %s (Version: %s %s %s) on %s" % \
              (idTagSplit[1].split(',')[0], idTagSplit[2], idTagSplit[3],
               idTagSplit[4], self._arrayName)
        self.logToScience(msg)

        


        
