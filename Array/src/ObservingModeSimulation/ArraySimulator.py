#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2010-03-12  created
#

#************************************************************************
#   NAME
# 
#   SYNOPSIS
# 
#   DESCRIPTION
#
#   FILES
#
#   ENVIRONMENT
#
#   RETURN VALUES
#
#   CAUTIONS
#
#   EXAMPLES
#
#   SEE ALSO
#
#   BUGS     
#
#------------------------------------------------------------------------
#
import math
import ObservingModeSimulation.SingleFieldInterferometrySimulator 
from CCL.SubscanList import SubscanList
from CCL.utils import aggregate
        
class SimulatedArray:
    '''
    This method takes the place of the usual array class and returns
    simulated observing modes.
    '''
    def __init__(self):
        self.__obsmode = None

        # The ArraySimulator keeps track of time for us
        self.__elapsedTime = 0

        #
        self.__antennaList = ["DV01", "DV02", "PM03"]

        # What we want to do here is have a sequence of Subscans
        # and the elapsed time at which they started and stopped
        self.__scanNumber = 0
        self.__subscanNumer = 0

        # One blank entry to make the scan number == the index
        self.__scanList = [{}]
        self.__subscanData = {}
        self.__sourceList = {}

        self.__reportSettings ={}
        self.__reportSettings['ShowScans'] = True
        self.__reportSettings['ShowSubscans'] = True
        self.__reportSettings['ShowSourceSummary'] = False
        self.__reportSettings['ShowOffsets'] = True

        self.__endState = None
        self.__endMessage = None
    
    def beginExecution(self):
        pass

    def endExecution(self, state, message):
        self.__endState = state
        self.__endMessage = message
    
    def getSingleFieldInterferometryObservingMode(self):
        self.__obsmode = ObservingModeSimulation.\
                         SingleFieldInterferometrySimulator.\
                         SingleFieldInterferometrySimulator(self)
        return self.__obsmode

    #def getTotalPowerObservingMode(self):
        #global _verbose
        #self.__obsmode = SimulationObsMode()
        #return self.__obsmode

    def getAntennas(self):
        return self.__antennaList

    def setAntennas(self, antennaList):
        self.__antennaList = aggregate(antennaList)
        

    def getElapsedTime(self):
        return self.__elapsedTime

    def incrementElapsedTime(self, elapsedTime):
        self.__elapsedTime += elapsedTime

    def prepareReport(self):
        if self.__obsmode is not None:
            scanInfo = self.__obsmode.getScanInfo()

    def generateReport(self):
        report = []

        numSubscans = 0
        for scan in self.__scanList[1:]:
            numSubscans += len(scan['SubscanSequence'])

        report.append("Execution Complete: Elapsed Time %s" %
                      self.__elapsedTimeToString(self.__elapsedTime))
        if self.__endState is not None:
            report.append("\tState:   " + str(self.__endState))
        if self.__endMessage is not None:
            report.append("\tMessage: " + self.__endMessage)
        report.append("\tTotal of: %d Scans (%d Subscans)" %
                      (len(self.__scanList[1:]), numSubscans))
        
        if self.__reportSettings['ShowScans']:
            report.append("\nScan Summary:")
            for scan in self.__scanList[1:]:
                report += self.__generateScanLine(scan)


        if self.__reportSettings['ShowSourceSummary']:
            report.append("\nSource Summary:")
            report += self.__generateSourceSummary()
        
        for line in report:
            print line

        return report

    def __generateSourceSummary(self):
        sourceFormat = "\t%-20s%30s"
        sourceLines = []
        sourceLines.append(sourceFormat %("Source Name",
                                          "Integration Time"))
        sourceLines.append(60*"-")

        for source in self.__sourceList.keys():
            sourceLines.append(sourceFormat %
                               (source.sourceName,
                                self.__elapsedTimeToString(self.__sourceList[source])))
        return sourceLines


    def __generateScanLine(self, scan):
        '''
        The scan lines have a column each for the following:
        Start Time
        Scan Intent
        Source
        '''
        scanLines = []
        scanFormat = "%10s\t%-20s\t%-20s\t%15s"
        scanLines.append(scanFormat % 
                         (self.__elapsedTimeToString(scan['StartTime']),
                          scan['Intent'][0].getScanIntent(),
                          scan['Source'],
                          ("%2d Subscans" % len(scan['SubscanSequence']))))
                          
        for intent in scan['Intent'][1:]:
            scanLines.append(scanFormat % ('',intent.getScanIntent(), '', ''))
        
        if self.__reportSettings['ShowSubscans']:
            scanLines += self.__generateSubscanLines(scan)

        return scanLines
        

    def __generateSubscanLines(self, scan):

        subscanFormat = "\t%10s   %-15s"
        subscanLines = []
        for subscan in scan['SubscanSequence']:
            lines = [subscanFormat % (subscan.duration, subscan.intent[0])]

            if self.__reportSettings['ShowOffsets']:
                offsetLines = self.__formatOffset(subscan.pointingOffset)
                lines[0] += offsetLines[0]
                
                for offsetLine in offsetLines[1:]:
                    lines.append((subscanFormat % ("","")) + offsetLine)

                
            subscanLines += lines

        return subscanLines
        
    def beginScan(self, scanIntents):
        self.__scanNumber += 1
        self.__scanList.append({})
        self.__scanList[self.__scanNumber]['ScanNumber'] =self.__scanNumber 
        self.__scanList[self.__scanNumber]['Intent'] = aggregate(scanIntents)
        self.__scanList[self.__scanNumber]['StartTime'] = self.getElapsedTime()
        self.__scanList[self.__scanNumber]['SubscanSequence'] = []
        self.__scanList[self.__scanNumber]['Source'] = None
        self.__subscanNumber = 0
        

    def endScan(self):
        self.__scanList[self.__scanNumber]['EndTime'] = self.getElapsedTime()

    def doSubscanSequence(self, subscanSequence):
        for subscan in subscanSequence.subscans:
            self.__elapsedTime += subscan.duration

            # Check if the source is already in the source list
            src = subscanSequence.sources[subscan.sourceId]
            if src not in self.__sourceList.keys():
                self.__sourceList[src] = 0
            #subscan.sourceId = src
            self.__sourceList[src] += subscan.duration
            self.__scanList[self.__scanNumber]['SubscanSequence'].\
                                    append(subscan)
            if self.__scanList[self.__scanNumber]['Source'] == None:
                self.__scanList[self.__scanNumber]['Source'] = \
                                                     src.sourceName


    def __formatOffset(self, pointingOffset):
        if len(pointingOffset) == 0:
            return ["%-15s: None" % ("Default Offset")]

        offsetStrings = []
        for array in pointingOffset:
            if abs(array.offset.longitudeVelocity) > 1E-9 or \
                   abs(array.offset.latitudeVelocity) > 1E-9:
                offsetStrings.append("%-15s: %5s %5s %5s %5s" %
                 ((array.name+" Offset"),
                self.__radiansToString(array.offset.longitudeOffset),
                self.__radiansToString(array.offset.latitudeOffset),
                self.__radianVelocityToString(array.offset.longitudeVelocity),
                self.__radianVelocityToString(array.offset.latitudeVelocity)))
            elif abs(array.offset.longitudeOffset) > 1E-9 or \
                     abs(array.offset.latitudeOffset) > 1E-9:
                offsetStrings.append("%-15s: %5s %5s" % (
                 (array.name + " Offset"),
                self.__radiansToString(array.offset.longitudeOffset),
                self.__radiansToString(array.offset.latitudeOffset)))
            else:
                offsetStrings.append("%-15s: None"% (array.name + " Offset"))

        return offsetStrings
                                     
    def __radiansToString(self, value):
        deg = math.degrees(value)
        if abs(deg) > 1  or abs(deg) < 1E-9:
            return "% 4.2f\xb0" % deg
        elif abs(deg) > 0.167:
            return "% 4.2f'" % (deg * 60)
        else:
            return "% 4.2f\"" % (deg * 3600)

    def __radianVelocityToString(self, value):
        deg = math.degrees(value)
        if abs(deg) > 1  or abs(deg) < 1E-9:
            return "% 4.2f\xb0/s" % deg
        elif abs(deg) > 0.167:
            return "% 4.2f'/s" % (deg * 60)
        else:
            return "% 4.2f\"/s" % (deg * 3600)

    
    def __elapsedTimeToString(self, time):
        # Elapsed times are kept in seconds convert to a mm:ss
        return "%d:%02d" % (time/60, time%60)



#
# ___oOo___
