/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.ScanStatus;

import java.util.Date;

import junit.framework.TestCase;

/**
 * Test the <code>ScanStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class ScanStatusPMTests extends TestCase {

    public ScanStatusPMTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals("", scanStatus.getScanIntent());
        assertEquals(0, scanStatus.getScanNumber());
        assertEquals(0, scanStatus.getSubscanCount());
        assertEquals(new Date(0), scanStatus.getTimeToSet());
    }

    public void testScanIntent() {
        scanStatus.setScanIntent(SCAN_INTENT);
        assertEquals(SCAN_INTENT, scanStatus.getScanIntent());
    }

    public void testScanNumber() {
        scanStatus.setScanNumber(SCAN_NUMBER);
        assertEquals(SCAN_NUMBER, scanStatus.getScanNumber());
    }

    public void testSubscanCount() {
        scanStatus.setSubscanCount(SUBSCAN_COUNT);
        assertEquals(SUBSCAN_COUNT, scanStatus.getSubscanCount());
    }

    public void testTimeToSet() {
        scanStatus.setTimeToSet(TIME_TO_SET);
        assertEquals(TIME_TO_SET, scanStatus.getTimeToSet());
    }

    @Override
    protected void setUp() {
        scanStatus = new ScanStatus();
    }
    
    // Test values
    private static final String SCAN_INTENT = "CALIBRATE_ATMOSPHERE";
    private static final int SCAN_NUMBER = 11;
    private static final int SUBSCAN_COUNT = 23;
    private static final Date TIME_TO_SET = new Date();

    private ScanStatus scanStatus;
    
} // class ScanStatusPMTests

//
// O_o
