/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.SingleValue;

import java.util.Date;

import junit.framework.TestCase;

/**
 * Test the <code>SingleValue</code> class.
 *
 * Note: type conversions are used in tests to resolve compile time ambiguity.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class SingleValuePMTests extends TestCase {

    public SingleValuePMTests(String testName) {
        super(testName);
    }

    public void testBooleanValues() {
    	booleanValue.setValue(FALSE);
    	assertEquals(FALSE, (boolean)booleanValue.getValue());
    	booleanValue.setValue(TRUE);
    	assertEquals(TRUE, (boolean)booleanValue.getValue());
    }

    public void testDateValues() {
    	dateValue.setValue(EARLIER);
    	assertEquals(EARLIER, (Date)dateValue.getValue());
    	dateValue.setValue(LATER);
    	assertEquals(LATER, (Date)dateValue.getValue());
    }

    public void testDoubleValues() {
    	doubleValue.setValue(DOUBLE_FIVE);
    	assertEquals(DOUBLE_FIVE, (double)doubleValue.getValue());
    	doubleValue.setValue(DOUBLE_ZERO);
    	assertEquals(DOUBLE_ZERO, (double)doubleValue.getValue());
    }

    public void testLongValues() {
    	longValue.setValue(LONG_FIVE);
    	assertEquals(LONG_FIVE, (long)longValue.getValue());
    	longValue.setValue(LONG_ZERO);
    	assertEquals(LONG_ZERO, (long)longValue.getValue());
    }

    public void testStringValues() {
    	stringValue.setValue(SHORT_STRING);
    	assertEquals(SHORT_STRING, (String)stringValue.getValue());
    	stringValue.setValue(LONGER_STRING);
    	assertEquals(LONGER_STRING, (String)stringValue.getValue());
    }

    @Override
    protected void setUp() {
    	// Set initial values to something other than the first value set in the specific tests.
    	booleanValue = new SingleValue<Boolean>(TRUE);
    	dateValue = new SingleValue<Date>(LATER);
    	doubleValue = new SingleValue<Double>(DOUBLE_ZERO);
    	longValue = new SingleValue<Long>(LONG_ZERO);
    	stringValue = new SingleValue<String>(LONGER_STRING);
    }
    
    // Test values
    private static final boolean FALSE = false;
    private static final boolean TRUE = true;
    private static final Date EARLIER = new Date(0);
    private static final Date LATER = new Date();
    private static final double DOUBLE_ZERO = 0.0;
    private static final double DOUBLE_FIVE = 5.0;
    private static final long LONG_ZERO = 0L; 
    private static final long LONG_FIVE = 5L;
    private static final String SHORT_STRING = "foo";
    private static final String LONGER_STRING = "A longer string.";
    
    private SingleValue<Boolean> booleanValue;
    private SingleValue<Date> dateValue;
    private SingleValue<Double> doubleValue;
    private SingleValue<Long> longValue;
    private SingleValue<String> stringValue; 

} // class SingleValuePMTests

//
// O_o
