/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.ArrayStatus;

import junit.framework.TestCase;

/**
 * Test the <code>ArrayStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class ArrayStatusPMTests extends TestCase {

    public ArrayStatusPMTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(ARRAY_NAME, arrayStatus.getArrayName());
        assertNotNull(arrayStatus.getAntennaShutterStatusPM());
        assertNotNull(arrayStatus.getAntennaStatusPM());
        assertNotNull(arrayStatus.getAtmosphericConditionsPM());
        assertNotNull(arrayStatus.getCorrelatorStatusPM());
        assertNotNull(arrayStatus.getExecBlockStatusPM());
        assertNotNull(arrayStatus.getLocalOscillatorStatusPM());
        assertNotNull(arrayStatus.getPointingStatusPM());
        assertNotNull(arrayStatus.getScanStatusPM());
        assertNotNull(arrayStatus.getSubscanStatusPM());
    }

    @Override
    protected void setUp() {
        arrayStatus = new ArrayStatus(ARRAY_NAME);
    }
    
    // Test values
    private static final String ARRAY_NAME = "Array001";

    private ArrayStatus arrayStatus;
    
} // class ArrayStatusPMTests

//
// O_o
