/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.ExecBlockStatus;

import java.util.Date;

import junit.framework.TestCase;

/**
 * Test the <code>ExecBlockStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class ExecBlockStatusPMTests extends TestCase {

    public ExecBlockStatusPMTests(String testName) {
        super(testName);
    }

    public void testAllowedTime() {
        execBlockStatus.setAllowedTime(ALLOWED_TIME);
        assertEquals(ALLOWED_TIME, execBlockStatus.getAllowedTime());
    }

    public void testAntennaRequredCount() {
        execBlockStatus.setAntennaRequiredCount(ANTENNA_REQUIRED_COUNT);
        assertEquals(ANTENNA_REQUIRED_COUNT, execBlockStatus.getAntennaRequiredCount());
    }

    public void testConstructor() {
        assertEquals(new Date(0), execBlockStatus.getAllowedTime());
        assertEquals(0, execBlockStatus.getAntennaRequiredCount());
        assertEquals("", execBlockStatus.getExecBlockUid());
        assertEquals("", execBlockStatus.getSchedBlockUid());
        assertEquals("", execBlockStatus.getPiName());
        assertEquals("", execBlockStatus.getProjectName());
        assertEquals("", execBlockStatus.getScript());
        assertEquals(new Date(0), execBlockStatus.getStartTime());
    }

    public void testExecBlockUid() {
        execBlockStatus.setExecBlockUid(EXEC_BLOCK_UID);
        assertEquals(EXEC_BLOCK_UID, execBlockStatus.getExecBlockUid());
    }

    public void testPiName() {
        execBlockStatus.setPiName(PI_NAME);
        assertEquals(PI_NAME, execBlockStatus.getPiName());
    }

    public void testProjectName() {
        execBlockStatus.setProjectName(PROJECT_NAME);
        assertEquals(PROJECT_NAME, execBlockStatus.getProjectName());
    }

    public void testSchedBlockUid() {
        execBlockStatus.setSchedBlockUid(SCHED_BLOCK_UID);
        assertEquals(SCHED_BLOCK_UID, execBlockStatus.getSchedBlockUid());
    }

    public void testScript() {
        execBlockStatus.setScript(SCRIPT);
        assertEquals(SCRIPT, execBlockStatus.getScript());
    }

    public void testStartTime() {
        execBlockStatus.setStartTime(START_TIME);
        assertEquals(START_TIME, execBlockStatus.getStartTime());
    }

    @Override
    protected void setUp() {
        execBlockStatus = new ExecBlockStatus();
    }
    
    // Test values
    private static final Date ALLOWED_TIME = new Date();
    private static final int ANTENNA_REQUIRED_COUNT = 42;
    private static final String EXEC_BLOCK_UID = "TBD_EXEC_BLOCK";
    private static final String PI_NAME = "TBD_PI_NAME";
    private static final String PROJECT_NAME = "TBD_PI_NAME";
    private static final String SCHED_BLOCK_UID = "TBD_SCHED_BLOCK";
    private static final String SCRIPT = "TBD_SCRIPT";
    private static final Date START_TIME = new Date();

    private ExecBlockStatus execBlockStatus;
    
} // class ExecBlockStatusPMTests

//
// O_o
