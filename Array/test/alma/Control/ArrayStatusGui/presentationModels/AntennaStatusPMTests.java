/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.AntennaStatus;

import junit.framework.TestCase;

/**
 * Test the <code>AntennaStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class AntennaStatusPMTests extends TestCase {

    public AntennaStatusPMTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(0, antennaStatus.getDegradedCount());
        assertEquals(0, antennaStatus.getOfflineCount());
        assertEquals(0, antennaStatus.getOperationalCount());
        assertEquals(0, antennaStatus.getTotalCount());
    }

    public void testDegradedCount() {
        antennaStatus.setDegradedCount(DEGRADED_COUNT);
        assertEquals(DEGRADED_COUNT, antennaStatus.getDegradedCount());
    }

    public void testOfflineCount() {
        antennaStatus.setOfflineCount(OFFLINE_COUNT);
        assertEquals(OFFLINE_COUNT, antennaStatus.getOfflineCount());
    }

    public void testOperationalCount() {
        antennaStatus.setOperationalCount(OPERATIONAL_COUNT);
        assertEquals(OPERATIONAL_COUNT, antennaStatus.getOperationalCount());
    }

    public void testTotalCount() {
        antennaStatus.setTotalCount(TOTAL_COUNT);
        assertEquals(TOTAL_COUNT, antennaStatus.getTotalCount());
    }

    @Override
    protected void setUp() {
        antennaStatus = new AntennaStatus();
    }
    
    // Test values
    private static final int DEGRADED_COUNT = 2;
    private static final int OFFLINE_COUNT = 1;
    private static final int OPERATIONAL_COUNT = 45;
    private static final int TOTAL_COUNT = 48;

    private AntennaStatus antennaStatus;

} // class AntennaStatusPMTests

//
// O_o
