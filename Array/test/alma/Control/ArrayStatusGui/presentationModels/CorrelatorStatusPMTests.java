/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.CorrelatorStatus;

import junit.framework.TestCase;

/**
 * Test the <code>CorrelatorStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class CorrelatorStatusPMTests extends TestCase {

    public CorrelatorStatusPMTests(String testName) {
        super(testName);
    }

    public void testApState() {
        correlatorStatus.setApState(AP_STATE);
        assertEquals(AP_STATE, correlatorStatus.getApState());
    }

    public void testConstructor() {
        assertEquals("", correlatorStatus.getApState());
        assertEquals(0, correlatorStatus.getCorrMode());
        assertEquals("", correlatorStatus.getCorrType());
        assertEquals(false, correlatorStatus.getUsesCorr());
    }

    public void testCorrMode() {
        correlatorStatus.setCorrMode(CORR_MODE);
        assertEquals(CORR_MODE, correlatorStatus.getCorrMode());
    }

    public void testCorrType() {
        correlatorStatus.setCorrType(CORR_TYPE);
        assertEquals(CORR_TYPE, correlatorStatus.getCorrType());
    }

    public void testUsesCorr() {
        correlatorStatus.setUsesCorr(USES_CORR);
        assertEquals(USES_CORR, correlatorStatus.getUsesCorr());
    }

    @Override
    protected void setUp() {
        correlatorStatus = new CorrelatorStatus();
    }
    
    // Test values
    private static final String AP_STATE = "Uncorrected";
    private static final int CORR_MODE = 17;
    private static final String CORR_TYPE = "BL";
    private static final boolean USES_CORR = true;

    private CorrelatorStatus correlatorStatus;

} // class CorrelatorStatusPMTests

//
// O_o
