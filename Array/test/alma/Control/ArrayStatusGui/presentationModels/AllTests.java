package alma.Control.ArrayStatusGui.presentationModels;
	
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Tests for alma.Control.ArrayStatusGui.sources");
        suite.addTestSuite(AntennaShutterStatusPMTests.class);
        suite.addTestSuite(AntennaStatusPMTests.class);
        suite.addTestSuite(ArrayStatusPMTests.class);
        suite.addTestSuite(AtmosphericConditionsPMTests.class);
        suite.addTestSuite(CorrelatorStatusPMTests.class);
        suite.addTestSuite(ExecBlockStatusPMTests.class);
        suite.addTestSuite(LocalOscillatorStatusPMTests.class);
        suite.addTestSuite(PointingStatusPMTests.class);
        suite.addTestSuite(ScanStatusPMTests.class);
        suite.addTestSuite(SingleValuePMTests.class);
        suite.addTestSuite(SubscanStatusPMTests.class);
        return suite;
    }
}
