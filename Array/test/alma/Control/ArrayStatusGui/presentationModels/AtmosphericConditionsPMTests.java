/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.AtmosphericConditions;

import junit.framework.TestCase;

/**
 * Test the <code>AtmosphericConditions</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class AtmosphericConditionsPMTests extends TestCase {

    public AtmosphericConditionsPMTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(0.0, atmosphericConditions.getLastTau(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, atmosphericConditions.getWh2o(), ACCEPTABLE_DIFFERENCE);
    }

    public void testLastTau() {
        atmosphericConditions.setLastTau(LAST_TAU);
        assertEquals(LAST_TAU, atmosphericConditions.getLastTau(), ACCEPTABLE_DIFFERENCE);
        
    }

    public void testWh2o() {
        atmosphericConditions.setWh2o(WH20);
        assertEquals(WH20, atmosphericConditions.getWh2o(), ACCEPTABLE_DIFFERENCE);

    }

    @Override
    protected void setUp() {
        atmosphericConditions = new AtmosphericConditions();
    }
    
    // Test values
    private static final double ACCEPTABLE_DIFFERENCE = 0.00001;
    private static final double LAST_TAU = 0.1;
    private static final double WH20 = 0.2;

    private AtmosphericConditions atmosphericConditions;

} // class AtmosphericConditionsPMTests

//
// O_o
