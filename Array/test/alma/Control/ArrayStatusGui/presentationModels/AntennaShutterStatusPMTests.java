/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.AntennaShutterStatus;

import junit.framework.TestCase;

/**
 * Test the <code>AntennaShutterStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class AntennaShutterStatusPMTests extends TestCase {

    public AntennaShutterStatusPMTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(0, antennaShutterStatus.getClosedCount());
        assertEquals(0, antennaShutterStatus.getOpenCount());
    }

    public void testShutterClosedCount() {
        antennaShutterStatus.setClosedCount(CLOSED_COUNT);
        assertEquals(CLOSED_COUNT, antennaShutterStatus.getClosedCount());
    }

    public void testShutterOpenCount() {
        antennaShutterStatus.setOpenCount(OPEN_COUNT);
        assertEquals(OPEN_COUNT, antennaShutterStatus.getOpenCount());
    }

    @Override
    protected void setUp() {
        antennaShutterStatus = new AntennaShutterStatus();
    }
    
    // Test values
    private static final int CLOSED_COUNT = 3;
    private static final int OPEN_COUNT = 45;

    private AntennaShutterStatus antennaShutterStatus;

} // class AntennaShutterStatusPMTests

//
// O_o
