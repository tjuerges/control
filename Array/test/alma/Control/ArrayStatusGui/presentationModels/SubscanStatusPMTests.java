/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.SubscanStatus;

import java.util.Date;

import junit.framework.TestCase;

/**
 * Test the <code>ScanStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class SubscanStatusPMTests extends TestCase {

    public SubscanStatusPMTests(String testName) {
        super(testName);
    }

    public void testConstructor() {
        assertEquals(0, subscanStatus.getSubscanNumber());
        assertEquals(new Date(0), subscanStatus.getDuration());
    }

    public void testDuration() {
        subscanStatus.setDuration(DURATION);
        assertEquals(DURATION, subscanStatus.getDuration());
    }

    public void testSubscanNumber() {
        subscanStatus.setSubscanNumber(SUBSCAN_NUMBER);
        assertEquals(SUBSCAN_NUMBER, subscanStatus.getSubscanNumber());
    }

    @Override
    protected void setUp() {
        subscanStatus = new SubscanStatus();
    }
    
    // Test values
    private static final Date DURATION = new Date();
    private static final int SUBSCAN_NUMBER = 13;

    private SubscanStatus subscanStatus;
    
} // class ScanStatusPMTests

//
// O_o
