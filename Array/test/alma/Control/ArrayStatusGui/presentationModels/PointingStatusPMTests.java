/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.PointingStatus;

import java.util.Date;

import junit.framework.TestCase;

/**
 * Test the <code>PointingStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class PointingStatusPMTests extends TestCase {

    public PointingStatusPMTests(String testName) {
        super(testName);
    }

    public void testAntennasOnSourceCount() {
        pointingStatus.setAntennasOnSourceCount(ANTENNAS_ON_SOURCE_COUNT);
        assertEquals(ANTENNAS_ON_SOURCE_COUNT, pointingStatus.getAntennasOnSourceCount());
    }

    public void testAz() {
        pointingStatus.setAz(AZ);
        assertEquals(AZ, pointingStatus.getAz(), ACCEPTABLE_DIFFERENCE);
    }

    public void testConstructor() {
        assertEquals(0, pointingStatus.getAntennasOnSourceCount());
        assertEquals(0.0, pointingStatus.getAz(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getCrossElOffset(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getCrossElVelocity(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getDec(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getEl(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getElOffset(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getElVelocity(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getMaximumPointingError(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, pointingStatus.getRa(), ACCEPTABLE_DIFFERENCE);
        assertEquals("", pointingStatus.getSourceName());
        assertEquals(new Date(0), pointingStatus.getTimeToSet());
    }

    public void testCrossElOffset() {
        pointingStatus.setCrossElOffset(CROSS_EL_OFFSET);
        assertEquals(CROSS_EL_OFFSET, pointingStatus.getCrossElOffset(), ACCEPTABLE_DIFFERENCE);
    }

    public void testCrossElVelocity() {
        pointingStatus.setCrossElVelocity(CROSS_EL_VELOCITY);
        assertEquals(CROSS_EL_VELOCITY, pointingStatus.getCrossElVelocity(), ACCEPTABLE_DIFFERENCE);
    }

    public void testDec() {
        pointingStatus.setDec(DEC);
        assertEquals(DEC, pointingStatus.getDec(), ACCEPTABLE_DIFFERENCE);
    }

    public void testEl() {
        pointingStatus.setEl(EL);
        assertEquals(EL, pointingStatus.getEl(), ACCEPTABLE_DIFFERENCE);
    }

    public void testElOffset() {
        pointingStatus.setElOffset(EL_OFFSET);
        assertEquals(EL_OFFSET, pointingStatus.getElOffset(), ACCEPTABLE_DIFFERENCE);
    }

    public void testElVelocity() {
        pointingStatus.setElVelocity(EL_VELOCITY);
        assertEquals(EL_VELOCITY, pointingStatus.getElVelocity(), ACCEPTABLE_DIFFERENCE);
    }

    public void testMaximumPointingError() {
        pointingStatus.setMaximumPointingError(MAXIMUM_POINTING_ERROR);
        assertEquals(MAXIMUM_POINTING_ERROR, pointingStatus.getMaximumPointingError(), ACCEPTABLE_DIFFERENCE);
    }

    public void testRa() {
        pointingStatus.setRa(RA);
        assertEquals(RA, pointingStatus.getRa(), ACCEPTABLE_DIFFERENCE);
    }

    public void testSourceName() {
        pointingStatus.setSourceName(SOURCE_NAME);
        assertEquals(SOURCE_NAME, pointingStatus.getSourceName());
    }

    public void testTimeToSet() {
        pointingStatus.setTimeToSet(TIME_TO_SET);
        assertEquals(TIME_TO_SET, pointingStatus.getTimeToSet());
    }

    @Override
    protected void setUp() {
        pointingStatus = new PointingStatus();
    }
    
    // Test values
    private static final double ACCEPTABLE_DIFFERENCE = 0.00001;
    private static final int ANTENNAS_ON_SOURCE_COUNT = 42;
    private static final double AZ = 90.0;
    private static final double CROSS_EL_OFFSET = 0.3;
    private static final double CROSS_EL_VELOCITY = 0.4;
    private static final double DEC = 10.0;
    private static final double EL = 45.0;
    private static final double EL_OFFSET = 0.5;
    private static final double EL_VELOCITY = 0.6;
    private static final double MAXIMUM_POINTING_ERROR = 0.0001;
    private static final double RA = 11.0;
    private static final String SOURCE_NAME = "TBD_SOURCE_NAME";
    private static final Date TIME_TO_SET = new Date();

    private PointingStatus pointingStatus;

} // class PointingStatusPMTests

//
// O_o
