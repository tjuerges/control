/* ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ArrayStatusGui.presentationModels;

import alma.Control.ArrayStatusGui.sources.LocalOscillatorStatus;

import junit.framework.TestCase;

/**
 * Test the <code>LocalOscillatorStatus</code> class.
 *
 * 2010-06-30
 * Expected values for these tests are based on the an IDL simulation.
 * These must be updated when a real implementation is available.
 *
 * @author Scott Rankin srankin@nrao.edu
 * @version $Id$
 * @since 8.0.0
 */
public class LocalOscillatorStatusPMTests extends TestCase {

    public LocalOscillatorStatusPMTests(String testName) {
        super(testName);
    }

    public void testAcdState() {
        localOscillatorStatus.setAcdState(ACD_STATE);
        assertEquals(ACD_STATE, localOscillatorStatus.getAcdState());
    }

    public void testConstructor() {
        assertEquals("", localOscillatorStatus.getAcdState());
        assertEquals(0, localOscillatorStatus.getFrontEndLockedCount());
        assertEquals(0.0, localOscillatorStatus.getMaxDewerTemp(), ACCEPTABLE_DIFFERENCE);
        assertEquals(0.0, localOscillatorStatus.getSkyFrequency(), ACCEPTABLE_DIFFERENCE);
    }

    public void testFrontEndLockedCount() {
        localOscillatorStatus.setFrontEndLockedCount(FRONT_END_LOCKED_COUNT);
        assertEquals(FRONT_END_LOCKED_COUNT, localOscillatorStatus.getFrontEndLockedCount());
    }

    public void testMaxDewerTemp() {
        localOscillatorStatus.setMaxDewerTemp(MAX_DEWER_TEMP);
        assertEquals(MAX_DEWER_TEMP, localOscillatorStatus.getMaxDewerTemp(), ACCEPTABLE_DIFFERENCE);
    }

    public void testSkyFrequency() {
        localOscillatorStatus.setSkyFrequency(SKY_FREQUENCY);
        assertEquals(SKY_FREQUENCY, localOscillatorStatus.getSkyFrequency(), ACCEPTABLE_DIFFERENCE);
    }

    @Override
    protected void setUp() {
        localOscillatorStatus = new LocalOscillatorStatus();
    }
    
    // Test values
    private static final double ACCEPTABLE_DIFFERENCE = 0.00001;
    private static final String ACD_STATE = "AMBIENT_LOAD";
    private static final int FRONT_END_LOCKED_COUNT = 42;
    private static final double MAX_DEWER_TEMP = 2.3;
    private static final double SKY_FREQUENCY = 3.2;

    private LocalOscillatorStatus localOscillatorStatus;

} // class LocalOscillatorStatusPMTests

//
// O_o
