package alma.Control.ArrayStatusGui;
	
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Tests for alma.Control.ArrayStatusGui");
        suite.addTestSuite(MainPanelTests.class);
        // The following tests must be re-worked for new code structure.
        // suite.addTest(alma.Control.ArrayStatusGui.presentationModels.AllTests.suite());
        return suite;
    }
}
