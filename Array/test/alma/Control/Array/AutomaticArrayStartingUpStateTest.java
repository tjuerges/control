/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Array;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.ACSErr.ErrorTrace;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.ArrayExceptions.wrappers.AcsJInitializationErrorEx;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.Control.Antenna;
import alma.Control.AntennaCharacteristics;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.SiteData;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.Common.ResourceManager;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

public class AutomaticArrayStartingUpStateTest extends ComponentClientTestCase {

    private class FakeArray implements AutomaticArrayInternal {

        private ContainerServices container;
        private String name;
        private Map<String, Antenna> antennas;
        
        protected String state;
        protected String methodJustCalled;
        
        public FakeArray(ContainerServices container) {
            this.container = container;
            this.name = "ARRAY_001";
            this.antennas = new HashMap<String, Antenna>();
            this.antennas.put("ALMA01", null);
            this.antennas.put("ALMA02", null);
            this.antennas.put("ALMA03", null);
            this.antennas.put("ALMA04", null);
            
            this.state = "Undefined";
            this.methodJustCalled = "Undefined";
        }
        
        public String createExecutionStateName() {
            return "EXECSTATE";
        }

        public String createDataCaptureName() {
            return "DataCapture001";
        }

        public Map<String, Antenna> getAntennaMap() {
            return antennas;
        }

        public String[] getAntennas() {
            return antennas.keySet().toArray(new String[0]);
        }

        public ContainerServices getContainerServices() {
            return container;
        }

        public Logger getLogger() {
            return container.getLogger();
        }

        public String getScriptExecutorName() {
            return "SCRIPT";
        }

        public void setState(String state) {
            this.state = state;
        }

        public void shutdownPass1() {
            this.methodJustCalled = "shutdownPass1";
        }

        public void shutdownPass2() {
            this.methodJustCalled = "shutdownPass2";
        }

        public void startupPass1() {
            this.methodJustCalled = "startupPass1";
        }

        public void startupPass2() {
            this.methodJustCalled = "startupPass1";
        }

        public void waitShutdownPass1() {
            this.methodJustCalled = "waitShutdownPass1";            
        }

        public String getDataCapturerName() {
            return "DataCapturer001";
        }

        public AntennaCharacteristics[] getAntennaConfigurations() {
            // TODO Auto-generated method stub
            return null;
        }

        public SiteData getSiteData() {
            // TODO Auto-generated method stub
            return null;
        }

        public WeatherStationCharacteristics[] getWeatherStationConfigurations() {
            // TODO Auto-generated method stub
            return null;
        }

        public void resetOffshoots() {}

        public String getConfigurationName() {
            return "Test";
        }

        public String getCorrelatorBulkDataDistributor() {
            return "distr1";
        }

        public String getTotalPowerBulkDataDistributor() {
            return "distr2";
        }

        @Override
        public String getArrayComponentName() {
            return name;
        }

        @Override
        public String getArrayName() {
            return name;
        }

        @Override
        public ResourceId[] getPhotonicReferences() {
            // TODO Auto-generated method stub
            return null;
        }

		@Override
		public CorrelatorType getCorrelatorType() {
			// TODO Auto-generated method stub
			return null;
		}        
    }
    
    private Logger logger;
    private FakeArray array;
    private AutomaticArrayStartingUpState state;
    private Simulator simulator;
    private ResourceManager resMng;
    
    public AutomaticArrayStartingUpStateTest() throws Exception {
        super(AutomaticArrayStartingUpStateTest.class.getName());
    }

    public AutomaticArrayStartingUpStateTest(String test) throws Exception {
        super(test);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        this.array = new FakeArray(getContainerServices());
        this.logger = getContainerServices().getLogger();
        this.state = new AutomaticArrayStartingUpState(this.array);
        this.simulator = 
            SimulatorHelper.narrow(getContainerServices().getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        this.resMng = ResourceManager.getInstance(getContainerServices());
    }

    protected void tearDown() throws Exception {
        getContainerServices().releaseComponent(simulator.name());
        resMng.releaseResources();
        resMng.freeAllResources();
        super.tearDown();
    }
    
    public void testStartUp() throws Exception {
        
        String code = "LOGGER.logInfo('Configure called...')\n";
        code += "arrayConf = parameters[0].arrayConfig\n";
        code += "LOGGER.logInfo('Array name: ' + arrayConf.arrayName)\n";
        code += "LOGGER.logInfo('Antenna names: ' + str(arrayConf.antennas))\n";
        code += "None";
        simulator.setMethod("SCRIPT", "configure", code, 0.0);
        
        state.startupPass1();
        state.startupPass2();

        assertEquals("OPERATIONAL_STATE", array.state);
        
        String[] br = resMng.getErroneousResources();
        assertEquals(0, br.length);
    }
    
    /**
     * Test what happens with the <code>AutomaticArrayStartingUpState</code>
     * class when the call to the <code>configure()</code> method in the 
     * <code>ScriptExecutor</code> throws a <code>ResourceErrorEx</code> 
     * exception. The <code>AutomaticArrayStartingUpState</code> class should
     * throw an <code>InitializationErrorEx</code>.
     */ 
    public void testStartUpPass1ConfigureException() {
        
        String code = "import ScriptExecutorExceptionsImpl\n";
        code += "raise ScriptExecutorExceptionsImpl.ResourceErrorExImpl";
        simulator.setMethod("SCRIPT", "configure", code, 0.0);
        
        try {
            state.startupPass1();
        } catch (AcsJInvalidRequestEx ex) {
            fail("Received wrong exception: " + ex); // wrong exception!
        } catch (AcsJInitializationErrorEx ex) {
            logger.info("Good... got an AcsJInitializationErrorEx exception");
            assertTrue(true); // fine, we were waiting for this exception
            ErrorTrace errorTrace = ex.getErrorTrace();
            logger.info("1st error trace msg: '" + errorTrace.shortDescription + "'");
            assertEquals(1, errorTrace.previousError.length);
            ErrorTrace errorTrace2 = errorTrace.previousError[0];
            logger.info("2nd error trace msg: '" + errorTrace2.shortDescription + "'");
            // Finally, check the type of the previous error. It should be a ResourceErrorEx
            // (type=10100, code=7)
            assertEquals(10100, errorTrace2.errorType);
            assertEquals(7, errorTrace2.errorCode);
            return;
        }
        
        fail("No exception was received"); // wrong, no exception was thrown
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "1"; // Default test suite.
        try {
            if (testSuite.equals("1")) {
                suite.addTest(new AutomaticArrayStartingUpStateTest("testStartUp"));
            } else if (testSuite.equals("2")) {
                suite.addTest(new AutomaticArrayStartingUpStateTest("testStartUpPass1ConfigureException"));
            }
        } catch (Exception ex) {
            System.err.println("Error when creating AutomaticArrayStartingUpStateTest: "
                    + ex.toString());
        }
        return suite;
    }
    
}
