/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Array;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.ACSErr.Completion;
import alma.ACSErr.ErrorTrace;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.ArrayExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ArrayExceptions.wrappers.AcsJSBExecutionErrorEx;
import alma.ArrayExceptions.wrappers.AcsJUnfinishedExecBlockEx;
import alma.Control.Antenna;
import alma.Control.AntennaCharacteristics;
import alma.Control.CorrelatorType;
import alma.Control.ExecBlockEndedEvent;
import alma.Control.ExecBlockStartedEvent;
import alma.Control.ResourceId;
import alma.Control.ScriptExecutor;
import alma.Control.SiteData;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.StaticResource;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.acs.container.archive.UIDLibrary;
import alma.acs.entityutil.EntitySerializer;
import alma.acs.nc.CorbaNotificationChannel;
import alma.acs.nc.SimpleSupplier;
import alma.acs.nc.Receiver;
import alma.asdmIDLTypes.IDLAngle;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SchedBlockEntityT;
import alma.offline.ASDMArchivedEvent;
import alma.offline.DataCapturer;
import alma.offline.DataCapturerId;
import alma.xmlentity.XmlEntityStruct;
import alma.xmlstore.ArchiveConnection;
import alma.xmlstore.ArchiveInternalError;
import alma.xmlstore.Identifier;
import alma.xmlstore.IdentifierJ;
import alma.xmlstore.IdentifierOperations;
import alma.xmlstore.Operational;
import alma.xmlstore.OperationalPackage.IllegalEntity;

public class AutomaticArrayOperationalStateTest extends ComponentClientTestCase {

    private class FakeArray implements AutomaticArrayInternal {

        private ContainerServices container;

        private String name;

        private Map<String, Antenna> antennas;

        private String dcn;

        private int dc;

        protected String state;

        protected String methodJustCalled;

        public FakeArray(ContainerServices container) {
            this.container = container;
            this.name = "ARRAY_001";
            this.antennas = new HashMap<String, Antenna>();
            this.antennas.put("ALMA01", null);
            this.antennas.put("ALMA02", null);
            this.antennas.put("ALMA03", null);
            this.antennas.put("ALMA04", null);

            this.state = "Undefined";
            this.methodJustCalled = "Undefined";

            this.dc = 0;
        }

        public String createExecutionStateName() {
            return "Array001/EXECSTATE";
        }

        public String createDataCaptureName() {
            dcn = String.format("Array001/DC%03d", dc++);
            return dcn;
        }

        public Map<String, Antenna> getAntennaMap() {
            return antennas;
        }

        public String[] getAntennas() {
            return antennas.keySet().toArray(new String[0]);
        }

        public ContainerServices getContainerServices() {
            return container;
        }

        public Logger getLogger() {
            return container.getLogger();
        }

        public String getScriptExecutorName() {
            return "Array001/SCRIPTEXEC";
        }

        public void setState(String state) {
            this.state = state;
        }

        public void shutdownPass1() {
            this.methodJustCalled = "shutdownPass1";
        }

        public void shutdownPass2() {
            this.methodJustCalled = "shutdownPass2";
        }

        public void startupPass1() {
            this.methodJustCalled = "startupPass1";
        }

        public void startupPass2() {
            this.methodJustCalled = "startupPass1";
        }

        public void waitShutdownPass1() {
            this.methodJustCalled = "waitShutdownPass1";
        }

        public String getDataCapturerName() {
            if (dcn == null)
                return createDataCaptureName();
            else
                return dcn;
        }

        public AntennaCharacteristics[] getAntennaConfigurations() {
            String[] antennaNames = { "ALMA01", "ALMA02", "ALMA03", "ALMA04" };
            AntennaCharacteristics[] ac = new AntennaCharacteristics[antennaNames.length];
            for (int i = 0; i < antennaNames.length; i++) {
                ac[i] = new AntennaCharacteristics();
                ac[i].antennaName = antennaNames[i];
                ac[i].dishDiameter = new IDLLength(0.0);
                ac[i].xPosition = new IDLLength(0.0);
                ac[i].yPosition = new IDLLength(0.0);
                ac[i].zPosition = new IDLLength(0.0);
                ac[i].xOffset = new IDLLength(0.0);
                ac[i].yOffset = new IDLLength(0.0);
                ac[i].zOffset = new IDLLength(0.0);
                ac[i].dateOfCommission = 0;
                ac[i].padId = "PadID";
                ac[i].padxPosition = new IDLLength(0.0);
                ac[i].padyPosition = new IDLLength(0.0);
                ac[i].padzPosition = new IDLLength(0.0);
                ac[i].cableDelay = 0;
            }
            return ac;
        }

        public SiteData getSiteData() {
            SiteData site = new SiteData();
            site.telescopeName = "";
            site.arrayConfigurationName = "";
            site.minBaseRange = new IDLLength(0.0);
            site.maxBaseRange = new IDLLength(0.0);
            site.baseRmsMinor = new IDLLength(0.0);
            site.baseRmsMajor = new IDLLength(0.0);
            site.basePa = new IDLAngle(0.0);
            site.siteLongitude = new IDLAngle(0.0);
            site.siteLatitude = new IDLAngle(0.0);
            site.siteAltitude = new IDLLength(0.0);
            site.releaseDate = 0;
            return site;
        }

        public WeatherStationCharacteristics[] getWeatherStationConfigurations() {
            return new WeatherStationCharacteristics[0];
        }

        public void resetOffshoots() {}

        public String getConfigurationName() {
            return "Test";
        }

        public String getCorrelatorBulkDataDistributor() {
            return "distr1";
        }

        public String getTotalPowerBulkDataDistributor() {
            return "distr2";
        }

        @Override
        public String getArrayComponentName() {
            return name;
        }

        @Override
        public String getArrayName() {
            return name;
        }

        @Override
        public ResourceId[] getPhotonicReferences() {
            // TODO Auto-generated method stub
            return null;
        }

		@Override
		public CorrelatorType getCorrelatorType() {
			// TODO Auto-generated method stub
			return null;
		}
    }

    private Logger logger;

    private FakeArray array;

    private AutomaticArrayOperationalState state;

    private Simulator simulator;

    private ResourceManager resMng;

    private boolean eventReceived;

    private ExecBlockEndedEvent event;

    private boolean execBlkStartedEventReceived;

    private ExecBlockStartedEvent execBlkStartedEvent;

    // The archive's components
    private ArchiveConnection archConnectionComp;

    private Operational archOperationComp;

    private Identifier archIdentifierComp;

    // //////////////////////////////////////////////////////////////////
    // Constructors
    // //////////////////////////////////////////////////////////////////

    public AutomaticArrayOperationalStateTest(String test) throws Exception {
        super(test);
    }

    public AutomaticArrayOperationalStateTest() throws Exception {
        super(AutomaticArrayOperationalStateTest.class.getName());
    }

    // //////////////////////////////////////////////////////////////////
    // Test fixture methods
    // //////////////////////////////////////////////////////////////////

    protected void setUp() throws Exception {

        super.setUp();
        ContainerServices cont = getContainerServices();

        this.array = new FakeArray(cont);
        this.logger = cont.getLogger();
        this.state = new AutomaticArrayOperationalState(this.array);
        this.simulator = SimulatorHelper.narrow(cont
                .getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        this.resMng = ResourceManager.getInstance(cont);

        // Add ARCHIVE_CONNECTION resource.
        Resource<ArchiveConnection> archRes = null;
        Resource<Identifier> idRes = null;
        archRes = new StaticResource<ArchiveConnection>(cont,
                "ARCHIVE_CONNECTION", "alma.xmlstore.ArchiveConnectionHelper",
                true);
        resMng.addResource(archRes);
        // Add ARCHIVE_IDENTIFIER resource.
        idRes = new StaticResource<Identifier>(cont, "ARCHIVE_IDENTIFIER",
                "alma.xmlstore.IdentifierHelper", true);
        resMng.addResource(idRes);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        this.archConnectionComp = archRes.getComponent();
        assertNotNull(archConnectionComp);

        this.archOperationComp = archConnectionComp
                .getOperational("AutomaticArrayOperationalStateTest");
        assertNotNull(archOperationComp);

        this.archIdentifierComp = idRes.getComponent();
        assertNotNull(archIdentifierComp);

        this.eventReceived = false;
        this.event = null;

        this.execBlkStartedEventReceived = false;
        this.execBlkStartedEvent = null;

        logger.info("Connection to the ALMA Archive has been constructed.");
    }

    protected void tearDown() throws Exception {
        getContainerServices().releaseComponent(simulator.name());
        resMng.releaseResources();
        resMng.freeAllResources();
        super.tearDown();
    }

    // //////////////////////////////////////////////////////////////////
    // Test cases
    // //////////////////////////////////////////////////////////////////

    public void testGetSBFromArchive() throws Exception {

        // Read a Scheduling Block file.
        SchedBlock fileSB = getSchedBlockFromFile("schedblks/test.xml");

        // Load this Scheduling Block into the Archive.
        String sbUID = null;
        sbUID = loadSBIntoArchive(fileSB);
        logger.info("Scheduling Block UID: " + sbUID);

        SchedBlock retrievedSB = state.getSBFromArchive(sbUID);

        // StringWriter buf1 = new StringWriter();
        // fileSB.marshal(buf1);
        // StringWriter buf2 = new StringWriter();
        // retrievedSB.marshal(buf2);
        //        
        // System.out.println(buf1.toString());
        // System.out.println(buf2.toString());

        // There seems to be no easy way to compare to SchedBlock objects
        assertEquals(retrievedSB.getName(), fileSB.getName());

    }

    public void testGetASDMUID() throws Exception {

        IDLEntityRef entRef = state.getASDMUID();
        assertTrue(entRef.entityId.matches("uid://X\\w+/X\\w+/X\\w+"));
    }

    public synchronized void testSendExecBlockEndedEvent() {

        // Add the SimpleSupplier resource
        Resource<SimpleSupplier> pubRes = null;
        pubRes = new PublisherResource(getContainerServices(),
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubRes);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
                "CONTROL_SYSTEM",
                getContainerServices());
        receiver.attach("alma.Control.ExecBlockEndedEvent", this);
        receiver.begin();

        IDLEntityRef sbEntRef = new IDLEntityRef();
        IDLEntityRef sessionEntRef = new IDLEntityRef();
        IDLEntityRef asdmEntRef = new IDLEntityRef();
        logger.info("Sending ExecBlockEndedEvent event.");
        Completion compl = new Completion();
        compl.code = -1;
        compl.type = -1;
        compl.timeStamp = 0L;
        compl.previousError = new ErrorTrace[0];
        state.sendExecBlockEndedEvent(compl, sbEntRef, sessionEntRef,
                asdmEntRef);

        while (!eventReceived)
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Interrupted when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }

        assertEquals(array.getDataCapturerName(), event.dataCapturerId);
    }

    public synchronized void receive(ExecBlockEndedEvent event) {
        logger.info("Received ExecBlockEndedEvent.");
        this.event = event;
        eventReceived = true;
        notify();
    }

    public synchronized void receive(ExecBlockStartedEvent event) {
        logger.info("Received ExecBlockEndedEvent.");
        execBlkStartedEvent = event;
        execBlkStartedEventReceived = true;
        notify();
    }

    public synchronized void testSBExecution() throws Exception {

        // Add the SimpleSupplier resource
        Resource<SimpleSupplier> pubRes = null;
        pubRes = new PublisherResource(getContainerServices(),
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubRes);
        // Add the ScriptExecutor resource
        String scriptExecName = array.getScriptExecutorName();
        Resource<ScriptExecutor> resource;
        resource = new DynamicResource<ScriptExecutor>(getContainerServices(),
                scriptExecName, "IDL:alma/Control/ScriptExecutor:1.0", true);
        resMng.addResource(resource);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
                "CONTROL_SYSTEM",
                getContainerServices());
        receiver.attach("alma.Control.ExecBlockEndedEvent", this);
        receiver.begin();

        // Read a Scheduling Block file.
        SchedBlock fileSB = getSchedBlockFromFile("schedblks/test.xml");

        // Load this Scheduling Block into the Archive.
        String sbUID = null;
        sbUID = loadSBIntoArchive(fileSB);
        logger.info("Scheduling Block UID: " + sbUID);

        // Set the behavior of the simulated ScriptExecutor
        // String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        // code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
        // code += "import ACSErr\n";
        // code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        // code += "result = 'This is the result, dude.'\n";
        // code += "(compl, result)";
        // simulator.setMethod(scriptExecName, "runSource", code, 0.0);
        // String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        // code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
        // code += "result = 'This is the result, dude.'\n";
        // code += "result";
        // simulator.setMethod(scriptExecName, "runSource2", code, 0.0);

        String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        code += "script = parameters[0]\n";
        code += "callback = parameters[1]\n";
        code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        code += "callback.report(compl)\n";
        code += "None";
        simulator.setMethod(scriptExecName, "runObservationScriptAsync", code, 0.0);

        // Execute SB.
        IDLEntityRef sbEntRef = new IDLEntityRef();
        sbEntRef.entityId = sbUID;
        IDLEntityRef sessionEntRef = new IDLEntityRef();
        try {
            state.observe(sbEntRef, sessionEntRef, 0L);
        } catch (AcsJSBExecutionErrorEx ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
            fail();
        }

        state.dataCaptureReady();

        long timeout, start, span;
        timeout = 5000;
        start = System.currentTimeMillis();
        while (!eventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockEndedEvent");
        }

        ASDMArchivedEvent e = createDummyASDMArchivedEvent(null);
        state.asdmArchived(e);
    }

    /**
     * Executes three SB executions back to back.
     * 
     * The array should be ready to execute another SB as soon as the
     * ExecBlockEndedEvent is sent.
     * 
     * Currently there is a problem with DataCapturer, which can't be released
     * at this point because it stores the ASDM in a different thread. For this
     * reason there is a separate collection of threads in the array that waits
     * for the ASDMArchivedEvent and releases the proper DataCapturer.
     */
    public synchronized void testBack2BackSBExecutions() throws Exception {

        // Add the SimpleSupplier resource
        Resource<SimpleSupplier> pubRes = null;
        pubRes = new PublisherResource(getContainerServices(),
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubRes);
        // Add the ScriptExecutor resource
        String scriptExecName = array.getScriptExecutorName();
        Resource<ScriptExecutor> resource;
        resource = new DynamicResource<ScriptExecutor>(getContainerServices(),
                scriptExecName, "IDL:alma/Control/ScriptExecutor:1.0", true);
        resMng.addResource(resource);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
                "CONTROL_SYSTEM",
                getContainerServices());
        receiver.attach("alma.Control.ExecBlockEndedEvent", this);
        receiver.begin();

        // Read a Scheduling Block file.
        SchedBlock fileSB = getSchedBlockFromFile("schedblks/test.xml");

        // Load this Scheduling Block into the Archive.
        String sbUID = null;
        sbUID = loadSBIntoArchive(fileSB);
        logger.info("Scheduling Block UID: " + sbUID);

        // Set the behavior of the simulated ScriptExecutor
        // String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        // code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
        // code += "import ACSErr\n";
        // code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        // code += "result = 'This is the result, dude.'\n";
        // code += "(compl, result)";
        // simulator.setMethod(scriptExecName, "runSource", code, 0.0);
        // String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        // code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
        // code += "result = 'This is the result, dude.'\n";
        // code += "result";
        // simulator.setMethod(scriptExecName, "runSource2", code, 0.0);

        String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        code += "script = parameters[0]\n";
        code += "callback = parameters[1]\n";
        code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        code += "callback.report(compl)\n";
        code += "None";
        simulator.setMethod(scriptExecName, "runSourceAsync", code, 0.0);

        // Execute SB.
        IDLEntityRef sbEntRef = new IDLEntityRef();
        sbEntRef.entityId = sbUID;
        IDLEntityRef sessionEntRef = new IDLEntityRef();
        try {
            state.observe(sbEntRef, sessionEntRef, 0L);
        } catch (AcsJSBExecutionErrorEx ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
            fail();
        }

        state.dataCaptureReady();

        long timeout, start, span;
        timeout = 5000;
        start = System.currentTimeMillis();
        while (!eventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockEndedEvent");
        }
        eventReceived = false;

        // Execute second SB.
        try {
            state.observe(sbEntRef, sessionEntRef, 0L);
        } catch (AcsJSBExecutionErrorEx ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
            fail();
        }

        state.dataCaptureReady();

        timeout = 5000;
        start = System.currentTimeMillis();
        while (!eventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockEndedEvent");
        }
        eventReceived = false;

        ASDMArchivedEvent e = createDummyASDMArchivedEvent("Array001/DC000");
        state.asdmArchived(e);
        e = createDummyASDMArchivedEvent("Array001/DC001");
        state.asdmArchived(e);

        // Execute third SB.
        try {
            state.observe(sbEntRef, sessionEntRef, 0L);
        } catch (AcsJSBExecutionErrorEx ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
            fail();
        }

        state.dataCaptureReady();

        timeout = 5000;
        start = System.currentTimeMillis();
        while (!eventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockEndedEvent");
        }

        e = createDummyASDMArchivedEvent("Array001/DC002");
        state.asdmArchived(e);
    }

    public synchronized void testBadSBExecution() throws Exception {

        // Add the SimpleSupplier resource
        Resource<SimpleSupplier> pubRes = null;
        pubRes = new PublisherResource(getContainerServices(),
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubRes);
        // Add the ScriptExecutor resource
        String scriptExecName = array.getScriptExecutorName();
        Resource<ScriptExecutor> resource;
        resource = new DynamicResource<ScriptExecutor>(getContainerServices(),
                scriptExecName, "IDL:alma/Control/ScriptExecutor:1.0", true);
        resMng.addResource(resource);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
                "CONTROL_SYSTEM",
                getContainerServices());
        receiver.attach("alma.Control.ExecBlockEndedEvent", this);
        receiver.begin();

        // Read a Scheduling Block file.
        SchedBlock fileSB = getSchedBlockFromFile("schedblks/test.xml");

        // Load this Scheduling Block into the Archive.
        String sbUID = null;
        sbUID = loadSBIntoArchive(fileSB);
        logger.info("Scheduling Block UID: " + sbUID);

        // Set the behavior of the simulated ScriptExecutor
        // String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        // code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
        // code += "import ScriptExecutorExceptionsImpl\n";
        // code += "raise ScriptExecutorExceptionsImpl.SyntaxErrorExImpl()";
        // simulator.setMethod(scriptExecName, "runSource", code, 0.0);
//        String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
//        code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
//        code += "import ScriptExecutorExceptionsImpl\n";
//        code += "raise ScriptExecutorExceptionsImpl.ExecutionErrorExImpl()";
//        simulator.setMethod(scriptExecName, "runSource2", code, 0.0);
        
        String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        code += "script = parameters[0]\n";
        code += "callback = parameters[1]\n";
        code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        code += "callback.report(compl)\n";
        code += "None";
        simulator.setMethod(scriptExecName, "runSourceAsync", code, 0.0);
        
        // Execute SB.
        IDLEntityRef sbEntRef = new IDLEntityRef();
        sbEntRef.entityId = sbUID;
        IDLEntityRef sessionEntRef = new IDLEntityRef();
        try {
            state.observe(sbEntRef, sessionEntRef, 0L);
        } catch (AcsJSBExecutionErrorEx ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
            fail();
        }

        state.dataCaptureReady();

        long timeout, start, span;
        timeout = 5000;
        start = System.currentTimeMillis();
        while (!eventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockStartedEvent");
        }

        ASDMArchivedEvent e = createDummyASDMArchivedEvent(null);
        state.asdmArchived(e);

        assertEquals(alma.Control.Completion.FAIL.value(), event.status.value());
        // TODO: check the error trace after the event has been mofidied
        // in the ICD.

    }

    /**
     * 
     * @throws Exception
     */
    public synchronized void testMissingBeginExecution() throws Exception {

        // Add the SimpleSupplier resource
        Resource<SimpleSupplier> pubRes = null;
        pubRes = new PublisherResource(getContainerServices(),
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubRes);
        // Add the ScriptExecutor resource
        String scriptExecName = array.getScriptExecutorName();
        Resource<ScriptExecutor> resource;
        resource = new DynamicResource<ScriptExecutor>(getContainerServices(),
                scriptExecName, "IDL:alma/Control/ScriptExecutor:1.0", true);
        resMng.addResource(resource);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
                "CONTROL_SYSTEM",
                getContainerServices());
        receiver.attach("alma.Control.ExecBlockStartedEvent", this);
        receiver.attach("alma.Control.ExecBlockEndedEvent", this);
        receiver.begin();

        // Read a Scheduling Block file.
        SchedBlock fileSB = getSchedBlockFromFile("schedblks/test.xml");

        // Load this Scheduling Block into the Archive.
        String sbUID = null;
        sbUID = loadSBIntoArchive(fileSB);
        logger.info("Scheduling Block UID: " + sbUID);

        // Set the behavior of the simulated ScriptExecutor
        // String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        // code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
        // code += "import ACSErr\n";
        // code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        // code += "result = 'This is the result, dude.'\n";
        // code += "(compl, result)";
        // simulator.setMethod(scriptExecName, "runSource", code, 0.0);
//        String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
//        code += "LOGGER.logInfo('SB: ' + parameters[0])\n";
//        code += "result = 'This is the result, dude.'\n";
//        code += "result";
//        simulator.setMethod(scriptExecName, "runSource2", code, 0.0);
        String code = "LOGGER.logInfo('Executing Scheduling Block.')\n";
        code += "script = parameters[0]\n";
        code += "callback = parameters[1]\n";
        code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        code += "callback.report(compl)\n";
        code += "None";
        simulator.setMethod(scriptExecName, "runSourceAsync", code, 0.0);
        
        // Execute SB.
        IDLEntityRef sbEntRef = new IDLEntityRef();
        sbEntRef.entityId = sbUID;
        IDLEntityRef sessionEntRef = new IDLEntityRef();
        try {
            state.observe(sbEntRef, sessionEntRef, 0L);
        } catch (AcsJSBExecutionErrorEx ex) {
            // TODO Auto-generated catch block
            ex.printStackTrace();
            fail();
        }

        state.dataCaptureReady();

        if (eventReceived)
            fail("ExecBlockEndedEvent should not be sent before ExecBlockStartedEvent");

        // Wait for the ExecBlockStartedEvent to arrive
        long timeout, start, span;
        timeout = 5000;
        start = System.currentTimeMillis();
        while (!execBlkStartedEventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockStartedEvent");
        }

        // Wait for the ExecBlockEndedEvent to arrive
        timeout = 5000;
        start = System.currentTimeMillis();
        while (!eventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockEndedEvent");
        }

        ASDMArchivedEvent e = createDummyASDMArchivedEvent(null);
        state.asdmArchived(e);
    }

    /**
     * beginExecution() should only be called from a running observing script.
     * If it is invoked directly it should throw an invalid reques exception.
     * 
     */
    public void testInvalidBeginExecution() {
        try {
            state.beginExecution();
        } catch (AcsJInvalidRequestEx ex) {
            return;
        } catch (AcsJSBExecutionErrorEx ex) {
            ex.printStackTrace();
        } catch (AcsJUnfinishedExecBlockEx ex) {
            ex.printStackTrace();
        }
        fail("beginExecution() shouldn't have succeeded.");
    }

    /**
     * Test the operational state beginExecution() method. The happy case.
     */
    public synchronized void testBeginExecution() throws Exception {

        // Add the SimpleSupplier resource
        Resource<SimpleSupplier> pubRes = null;
        pubRes = new PublisherResource(getContainerServices(),
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubRes);
        // Add DataCapturer resource
        Resource<DataCapturer> dcres = new DynamicResource<DataCapturer>(array
                .getContainerServices(), array.getDataCapturerName(),
                "IDL:alma/offline/DataCapturer:1.0");
        resMng.addResource(dcres);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        // Install some behavior in the DataCapturer simulator
        String code = "LOGGER.logInfo('startSBExecution() called')\n";
        code += "None";
        simulator.setMethod(array.getDataCapturerName(), "startSBExecution",
                code, 0.0);

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
                "CONTROL_SYSTEM",
                getContainerServices());
        receiver.attach("alma.Control.ExecBlockStartedEvent", this);
        receiver.begin();

        IDLEntityRef asdmEntRef = new IDLEntityRef();
        IDLEntityRef sbEntRef = new IDLEntityRef();
        IDLEntityRef sessionEntRef = new IDLEntityRef();
        SchedBlock fileSB = getSchedBlockFromFile("schedblks/test.xml");
        state.beginExecution(asdmEntRef, sbEntRef, sessionEntRef, fileSB);

        // Wait for the ExecBlockStartedEvent to arrive
        long timeout, start, span;
        timeout = 5000;
        start = System.currentTimeMillis();
        while (!execBlkStartedEventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockStartedEvent");
        }

    }

    /**
     * Test the operational state endExecution() method. The happy case.
     */
    public synchronized void testEndExecution() throws Exception {

        // Add the SimpleSupplier resource
        Resource<SimpleSupplier> pubRes = null;
        pubRes = new PublisherResource(getContainerServices(),
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubRes);
        // Add DataCapturer resource
        Resource<DataCapturer> dcres = new DynamicResource<DataCapturer>(array
                .getContainerServices(), array.getDataCapturerName(),
                "IDL:alma/offline/DataCapturer:1.0");
        resMng.addResource(dcres);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }

        // Install some behavior in the DataCapturer simulator
        String code = "LOGGER.logInfo('endSBExecution() called')\n";
        code += "None";
        simulator.setMethod(array.getDataCapturerName(), "endSBExecution",
                code, 0.0);

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
                "CONTROL_SYSTEM",
                getContainerServices());
        receiver.attach("alma.Control.ExecBlockEndedEvent", this);
        receiver.begin();

        IDLEntityRef asdmEntRef = new IDLEntityRef();
        IDLEntityRef sbEntRef = new IDLEntityRef();
        IDLEntityRef sessionEntRef = new IDLEntityRef();
        state.endExecution(asdmEntRef, sbEntRef, sessionEntRef);

        // Wait for the ExecBlockEndedEvent to arrive
        long timeout, start, span;
        timeout = 5000;
        start = System.currentTimeMillis();
        while (!eventReceived) {
            wait(timeout);
            span = System.currentTimeMillis() - start;
            if (span > timeout)
                fail("Timeout when waiting for ExecBlockEndedEvent");
        }
    }

    // //////////////////////////////////////////////////////////////////
    // Private methods
    // //////////////////////////////////////////////////////////////////

    /**
     * Load a scheduling block into the Archive.
     * 
     * @param schedBlock
     *            APDM SchedBlock object
     * @return UID used to store the SchedBlock into the Archive
     * @throws Exception
     */
    private String loadSBIntoArchive(SchedBlock schedBlock) throws Exception {

        try {
            EntitySerializer serializer = EntitySerializer
                    .getEntitySerializer(logger);
            XmlEntityStruct ent = serializer.serializeEntity(schedBlock,
                    schedBlock.getSchedBlockEntity());
            archOperationComp.store(ent);
            return ent.entityId;
        } catch (IllegalEntity ex1) {
            logger.severe("Illegal entity: " + ex1.toString());
            fail();
        } catch (ArchiveInternalError ex2) {
            logger.severe("Archive internal error: " + ex2.toString());
            fail();
        }

        return null;
    }

    /**
     * Read a file containing a scheduling block XML and creates a SchedBlock
     * object from it.
     * 
     * @param filePath
     *            Path of the scheduling block XML file
     * @return SchedBlock object
     * @throws Exception
     */
    private SchedBlock getSchedBlockFromFile(String filePath) throws Exception {

        String dirName;
        String fileName;
        int loc;
        if ((loc = filePath.lastIndexOf('/')) >= 0) {
            fileName = filePath.substring(loc + 1);
            dirName = filePath.substring(0, loc);
        } else {
            fileName = filePath;
            dirName = ".";
        }

        String xmlDoc = readSBFile(dirName, fileName);
        SchedBlock schedBlock = SchedBlock
                .unmarshalSchedBlock(new StringReader(xmlDoc));
        SchedBlockEntityT entity = schedBlock.getSchedBlockEntity();

        UIDLibrary uidlib = new UIDLibrary(logger);
        uidlib.replaceUniqueEntityId(entity, getContainerServices()
                .getTransparentXmlWrapper(IdentifierJ.class,
                        archIdentifierComp, IdentifierOperations.class));

        schedBlock.setSchedBlockEntity(entity);
        return schedBlock;
    }

    /**
     * Read SB from a file.
     * 
     * @param dirName
     *            Directory name
     * @param fileName
     *            File name
     * @return XML document
     * @throws Exception
     */
    private String readSBFile(String dirName, String fileName) throws Exception {

        // Check that the directory exists
        File dir = new File(dirName);
        if (!dir.isDirectory())
            throw new Exception("Directory " + dirName + " does not exist.");

        // Check that the file exists
        File file = new File(dir, fileName);
        if (!file.exists())
            throw new Exception("File " + fileName + "in directory " + dirName
                    + " does not exist.");

        // Read file contents
        BufferedReader in = null;
        StringBuffer xmlDoc = null;
        String line = null;
        try {
            in = new BufferedReader(new FileReader(file));
            xmlDoc = new StringBuffer();
            line = in.readLine();
            while (line != null) {
                xmlDoc.append(line + "\n");
                line = in.readLine();
            }
            in.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return new String(xmlDoc);
    }

    private ASDMArchivedEvent createDummyASDMArchivedEvent(String dcn) {

        String dataCapturerName;
        if (dcn == null)
            dataCapturerName = array.getDataCapturerName();
        else
            dataCapturerName = dcn;
        ASDMArchivedEvent event = new ASDMArchivedEvent();
        event.archivedAt = 0L;
        event.asdmId = new IDLEntityRef();
        event.asdmId.entityId = "";
        event.asdmId.entityTypeName = "";
        event.asdmId.instanceVersion = "";
        event.asdmId.partId = "";
        event.status = "";
        event.workingDCId = new DataCapturerId();
        event.workingDCId.array = "";
        event.workingDCId.name = dataCapturerName;
        event.workingDCId.schedBlock = new IDLEntityRef();
        event.workingDCId.schedBlock.entityId = "";
        event.workingDCId.schedBlock.entityTypeName = "";
        event.workingDCId.schedBlock.instanceVersion = "";
        event.workingDCId.schedBlock.partId = "";
        event.workingDCId.session = new IDLEntityRef();
        event.workingDCId.session.entityId = "";
        event.workingDCId.session.entityTypeName = "";
        event.workingDCId.session.instanceVersion = "";
        event.workingDCId.session.partId = "";
        return event;
    }

    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "1"; // Default test suite.
        try {
            if (testSuite.equals("1")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testGetSBFromArchive"));
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testGetASDMUID"));
            } else if (testSuite.equals("2")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testSendExecBlockEndedEvent"));
            } else if (testSuite.equals("3")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testSBExecution"));
            } else if (testSuite.equals("4")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testBadSBExecution"));
            } else if (testSuite.equals("5")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testInvalidBeginExecution"));
            } else if (testSuite.equals("6")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testBeginExecution"));
            } else if (testSuite.equals("7")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testEndExecution"));
            } else if (testSuite.equals("8")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testBack2BackSBExecutions"));
            } else if (testSuite.equals("9")) {
                suite.addTest(new AutomaticArrayOperationalStateTest(
                        "testMissingBeginExecution"));
            }
        } catch (Exception ex) {
            System.err
                    .println("Error when creating AutomaticArrayStartingUpStateTest: "
                            + ex.toString());
        }
        return suite;
    }
}
