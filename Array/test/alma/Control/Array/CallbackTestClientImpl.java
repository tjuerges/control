package alma.Control.Array;


import java.util.logging.Logger;
//import alma.Control.CallbackTestClientPOA;
//import alma.ACS.ComponentStates;
//import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ContainerServices;
import alma.acs.component.ComponentLifecycleException;
import alma.acs.component.ComponentImplBase;

import alma.Control.CallbackTestClientOperations;
import alma.Control.AntModeControllerCB;
import alma.Control.IsObservableCB;

import alma.ModeControllerExceptions.wrappers.AcsJUnallocatedEx;
import alma.ModeControllerExceptions.wrappers.UnallocatedAcsJCompletion;

import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.IllegalParameterErrorAcsJCompletion;


import alma.ACSErrTypeOK.wrappers.ACSErrOKAcsJCompletion;
import alma.acs.exceptions.AcsJCompletion;

public class CallbackTestClientImpl extends ComponentImplBase
  implements CallbackTestClientOperations 
{
    public void initialize(ContainerServices containerServices) 
        throws ComponentLifecycleException 
    {
        super.initialize(containerServices);
    }

    /* Tests form the AntModeController Callback */
    public void AntModeCBSuccess(String objName,
                                       AntModeControllerCB cb) {
        ACSErrOKAcsJCompletion okCompletion = new ACSErrOKAcsJCompletion();
        cb.report(objName, okCompletion.toCorbaCompletion());

    }

    public void AntModeCBFailure(String objName,
                                       AntModeControllerCB cb) {

        AcsJUnallocatedEx ex = new AcsJUnallocatedEx();
        
        UnallocatedAcsJCompletion errCompletion =
            new UnallocatedAcsJCompletion(ex);
        cb.report(objName, errCompletion.toCorbaCompletion());

    }

    public void IsObservableCBSuccess(String objName,
                                      boolean returnValue,
                                      IsObservableCB cb) {
        
        ACSErrOKAcsJCompletion okCompletion = new ACSErrOKAcsJCompletion();
        cb.report(objName, returnValue, okCompletion.toCorbaCompletion());
    }

    /* Test a simple false callback scenario */
    public void IsObservableCBUnallocatedEx(String objName,
                                            IsObservableCB cb){

        AcsJUnallocatedEx ex = new AcsJUnallocatedEx();
        
        UnallocatedAcsJCompletion errCompletion =
            new UnallocatedAcsJCompletion(ex);
        cb.report(objName, true, errCompletion.toCorbaCompletion());

    }


    /* Test a simple failure callback scenario */
    public void IsObservableCBParameterEx(String objName,
                                          IsObservableCB cb){

        AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
        
        IllegalParameterErrorAcsJCompletion errCompletion =
            new IllegalParameterErrorAcsJCompletion(ex);
        cb.report(objName, true, errCompletion.toCorbaCompletion());
    }

}
