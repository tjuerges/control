/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */

package alma.Control.Array;

import java.util.logging.Logger;

import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.AntennaCharacteristics;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.SiteData;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.AbortionException;
import alma.Control.AutomaticArray2;
import alma.Control.AutomaticArray2Helper;
import alma.Correlator.CorrelatorConfiguration;
import alma.CorrelatorCalibrationMod.CorrelatorCalibration;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.SubScanDataFlowErrorEx;
import alma.SubscanIntentMod.SubscanIntent;
import alma.ScanIntentMod.ScanIntent;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.component.client.ComponentClientTestCase;
import alma.asdmIDLTypes.IDLAngle;
import alma.asdmIDLTypes.IDLLength;

/**
 * Test the AutomaticArray component in-container.
 * 
 */
public class AutomaticArrayTest extends ComponentClientTestCase {

    private Logger logger;
    private String arrayName;
    private AutomaticArray2 array;
    protected Simulator simulator;
    
    public AutomaticArrayTest() throws Exception {
        super(AutomaticArrayTest.class.getName());
    }

    /**
     * Utility function. Creates an AutomaticArray dinamically.
     * 
     * @param antennaNames Antenna names.
     */
    private void createAutomaticArray(String[] antennaNames) {

        ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
        spec.setComponentName("Array001");
        spec.setComponentType("IDL:alma/Control/AutomaticArray2:1.0");
        
        try {
            org.omg.CORBA.Object obj = getContainerServices().getDynamicComponent(spec, false);
            array = AutomaticArray2Helper.narrow(obj);
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error creating AutomaticArray component: "
                    + ex.toString());
            ex.log(logger);
            fail();
        }

        array.setParentName(this.getName());
        array.setAntennaList(antennaNames);       

        logger.info("Setting Array configuration data");
        AntennaCharacteristics[] ac = new AntennaCharacteristics[antennaNames.length];
        for(int i = 0; i < antennaNames.length; i++) {
            ac[i] = new AntennaCharacteristics();
            ac[i].antennaName = antennaNames[i];
            ac[i].dishDiameter = new IDLLength(0.0);
            ac[i].xPosition = new IDLLength(0.0);
            ac[i].yPosition = new IDLLength(0.0);
            ac[i].zPosition = new IDLLength(0.0);
            ac[i].xOffset = new IDLLength(0.0);
            ac[i].yOffset = new IDLLength(0.0);
            ac[i].zOffset = new IDLLength(0.0);
            ac[i].dateOfCommission = 0;
            ac[i].padId = "PadID";
            ac[i].padxPosition = new IDLLength(0.0);
            ac[i].padyPosition = new IDLLength(0.0);
            ac[i].padzPosition = new IDLLength(0.0);
            ac[i].cableDelay = 0;
        }
        WeatherStationCharacteristics[] wsc = new  WeatherStationCharacteristics[0];
        SiteData site = new SiteData();
        site.telescopeName = "";
        site.arrayConfigurationName = "";
        site.minBaseRange = new IDLLength(0.0);
        site.maxBaseRange = new IDLLength(0.0);
        site.baseRmsMinor = new IDLLength(0.0);
        site.baseRmsMajor = new IDLLength(0.0);
        site.basePa = new IDLAngle(0.0);
        site.siteLongitude = new IDLAngle(0.0);
        site.siteLatitude = new IDLAngle(0.0);
        site.siteAltitude = new IDLLength(0.0);
        site.releaseDate = 0;
        array.setConfigInfo("Test", ac, wsc, site, new ResourceId[0], CorrelatorType.BL);

        array.setBulkDataDistributors("distr1", "distr2");
    }

    /**
     * Shuts down the AutomaticArray created with createAutomaticArray() and
     * releases this component.
     */
    private void destroyAutomaticArray() {
        
        getContainerServices().releaseComponent(arrayName);
        
    }

    private void initializeAutomaticArray() {
        
        logger.info("Initializing the array. startupPass1...");
        array.startupPass1();
        if (!(array.getArrayState() == SystemState.INACCESSIBLE &&
              array.getArraySubstate() == SystemSubstate.STARTED_UP_PASS1)) {
            logger.severe("Error in array state");
            fail();
        }

        logger.info("Continue initializing array. startupPass2...");
        array.startupPass2();
        if (array.getArrayState() != SystemState.OPERATIONAL ||
                array.getArraySubstate() != SystemSubstate.NOERROR) {
            logger.severe("Error in array state");
            fail();
        }        
    }
    
    private void shutdownAutomaticArray() {
        array.shutdownPass1();
        array.shutdownPass2();        
    }
    /**
     * Test case fixture setup.
     */
    protected void setUp() throws Exception {
        
        System.out.println("ComponentClientTestCase.setUp()...");
        super.setUp();

        // Get the Logger.
        System.out.println("Getting the logger...");
        logger = getContainerServices().getLogger();
        
        this.simulator = 
            SimulatorHelper.narrow(getContainerServices().getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
  
    }

    /**
     * Test case fixture clean up.
     */
    protected void tearDown() throws Exception {
        getContainerServices().releaseComponent(simulator.name());
        super.tearDown();
    }

    public void testCreateAutomaticArray() {

        String[] antennaNames = {"ALMA01"};
        createAutomaticArray(antennaNames);
        initializeAutomaticArray();
        
        // Good... the array has been created fine.
        
        shutdownAutomaticArray();        
        destroyAutomaticArray();
    }
    
    /**
     * A simple test for the SFI Offshoot. Just getting the offshoot and
     * calling one of its functions.
     */
    public void notestSFIOffshoot() throws Exception {

        String[] antennaNames = {"ALMA01"};
        createAutomaticArray(antennaNames);
        initializeAutomaticArray();
        
        SingleFieldInterferometryObservingMode obsMode = null;
		try {
			obsMode = array.getSFIOffshoot();
		} catch (ObsModeInitErrorEx ex) {
			ex.printStackTrace();
			fail("Observation Mode Initialization Error.");
		}
        
        shutdownAutomaticArray();
        destroyAutomaticArray();
    }
    
//    /**
//     * The AutomaticArray implements a "soft abortion", i.e., an abortion that
//     * gets executed when any of the observing modes gets to endSubscan(). 
//     */
//    public void notestStopScanSFIOffshoot() throws Exception {
//
//        String code;
//        code = "LOGGER.logInfo('getReceiverInfo called...')\n";
//        code +=       "('RX0123', 'Dewar-01', 299.99)";
//        simulator.setMethodIF("IDL:alma/Control/AntLOController:1.0",
//                              "getReceiverInfo", code, 0);
//        
//        code = "LOGGER.logInfo('getFrequency called...')\n";
//        code += "import NetSidebandMod\n";
//        code +=       "('Band3', 32E6, NetSidebandMod.LSB, 6.0E9, 5.8E9, 6.2E9, 6.1E9)";
//        simulator.setMethodIF("IDL:alma/Control/AntLOController:1.0",
//                              "getFrequencies", code, 0);
//        
//        code = "LOGGER.logInfo('getSignalPaths called...')\n";
//        code +=       "(True, False, False, False, False, True)";
//        simulator.setMethodIF("IDL:alma/Control/AntLOController:1.0",
//                              "getSignalPaths", code, 0);        
//        
//        // Setup the CVR Frequency property to return a valid figure
//        code = "from ACSImpl.DevIO import DevIO\n";
//        code += "devio = DevIO(17.135E9)\n";
//        code += "setGlobalData('devio', devio)\n";
//        code += "None";
//        simulator.setMethod("CONTROL/AOSTiming/CVR", "initialize", code, 0);        
//        code = "getGlobalData('devio')";
//        simulator.setMethod("CONTROL/AOSTiming/CVR", "Frequency", code, 0);
//        
//        code =
//            "import Control\n" +
//            "from Control import MountController\n" + 
//            "eq = Control.EquatorialDirection(0, 0)\n" + 
//            "hz = Control.HorizonDirection(0, 0)\n" +
//            "of = MountController.Offset(0.0, 0.0)\n" +
//            "pd = MountController.PointingData(eq, of, eq, of, hz, hz, hz, hz, True, eq, 0, False)\n" + 
//            "[pd]";
//        simulator.setMethodIF("IDL:alma/Control/SingleFieldInterferometry:1.0",
//                "getPointingDataTable", code, 0);        
//        
//        String[] antennaNames = {"ALMA01"};
//        createAutomaticArray(antennaNames);
//        initializeAutomaticArray();
//        
//        SingleFieldInterferometryObservingMode obsMode = array.getSFIOffshoot();
//        
//        obsMode.beginScan(new ScanIntent[] {ScanIntent.TARGET});
//
//        int confId = obsMode.configureSubscans(new CorrelatorConfiguration[] {Util.createDummyCorrelatorConfiguration()},
//                new int[0], CorrelatorCalibration.NONE)[0];
//        
//        obsMode.beginSubscan(confId, Util.createDummyField(), Util.createDummySpectralSpec(),
//                Util.createDummyCorrelatorConfiguration(),
//                new SubscanIntent[] { SubscanIntent.ON_SOURCE });
//        
//        array.stop();
//
//        // Start a thread that will wait a little bit and then call
//        // subScanDataReady.
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException ex) {
//                    ex.printStackTrace();
//                } // sleep 1.0 second
//                array.subScanDataReady(); // Call subscan data ready so the condition
//                                          // variable's wait inside endSubscan returns.
//            }
//        }).start();
//        
//        try {
//            obsMode.endSubscan();
//        } catch (AbortionException ex) {
//            // This exception was expected.
//            shutdownAutomaticArray();
//            destroyAutomaticArray();
//            return;
//        } catch (SubScanDataFlowErrorEx ex) {
//            shutdownAutomaticArray();
//            destroyAutomaticArray();
//            logger.severe(Utils.getNiceErrorTraceString(ex.errorTrace, 0));
//            fail("Unexpected Exception.");            
//        } catch (Exception ex) {
//            shutdownAutomaticArray();
//            destroyAutomaticArray();
//            fail("Completely unexpected Exception.");                        
//        }
//
//        shutdownAutomaticArray();
//        destroyAutomaticArray();
//        fail("Abortion exception was not received.");
//    }

    /**
     * The AutomaticArray implements a "soft abortion", i.e., an abortion that
     * gets executed when any of the observing modes gets to endSubscan().
     * This function could be called before the getSFIOffshoot function has been
     * reached.
     */
    public void notestStopScanBeforeGetSFIOffshoot() throws Exception {
        
        String[] antennaNames = {"ALMA01"};
        createAutomaticArray(antennaNames);
        initializeAutomaticArray();
        
        array.stop();

        try {
            SingleFieldInterferometryObservingMode obsMode = array.getSFIOffshoot();
        } catch (AbortionException ex) {
            // This exception was expected.
            shutdownAutomaticArray();
            destroyAutomaticArray();
            return;
        }

        shutdownAutomaticArray();
        destroyAutomaticArray();
        fail("Abortion exception was not received.");
    }
    
}

