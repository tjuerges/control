/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2005 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * $Id$
 */
package alma.Control.Array;

import java.lang.Math;
import java.util.logging.Logger;

import si.ijs.maci.ComponentSpec;
import alma.Control.CorrelatorType;
import alma.Control.LocalOscillator;
import alma.Control.ResourceId;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.SiteData;
import alma.Control.SkyDelayServerHelper;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.AutomaticArray2;
import alma.Control.AutomaticArray2Helper;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.component.client.ComponentClientTestCase;
import alma.Control.AntennaCharacteristics;
import alma.asdmIDLTypes.IDLLength;
import alma.asdmIDLTypes.IDLAngle;
import alma.Control.WeatherStationCharacteristics;

/**
 * The Array component (either AutomaticArray or ManualArray) creates
 * ACS Offshoots as a way to extend its interface with methods that are
 * specific for each observing mode.
 * 
 * This class test the creation and use of these offshoots.
 * 
 * The offshoot classes, which are named following the pattern "XXXObservingMode",
 * are Java classes that use C++ "ModeControllers" (ACS components).
 * This test case is not meant to test the ModeControllers. They will be simulated
 * with mock objects or by use of the ACS simulator, as necessary.
 * 
 * @author rhiriart
 *
 */
public class OffshootTest extends ComponentClientTestCase {

    private Logger logger;
    private String arrayName;
    private AutomaticArray2 array;
    
    public OffshootTest() throws Exception {
        super(OffshootTest.class.getName());
    }

    /**
     * Utility function. Create an AutomaticArray dinamically.
     * 
     * @param antennaNames Antenna names.
     */
    private void createAutomaticArray(String[] antennaNames) {

        ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
        spec.setComponentType("IDL:alma/Control/AutomaticArray:1.0");
        
        try {
            org.omg.CORBA.Object obj = getContainerServices().getDynamicComponent(spec, false);
            array = AutomaticArray2Helper.narrow(obj);
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error creating AutomaticArray component: "
                    + ex.toString());
            ex.log(logger);
            fail();
        }

        array.setParentName(this.getName());
        array.setAntennaList(antennaNames);       

        logger.info("Setting Array configuration data");
        AntennaCharacteristics[] ac = new AntennaCharacteristics[antennaNames.length];
        for(int i = 0; i < antennaNames.length; i++) {
            ac[i] = new AntennaCharacteristics();
            ac[i].antennaName = antennaNames[i];
            ac[i].dishDiameter = new IDLLength(0.0);
            ac[i].xPosition = new IDLLength(0.0);
            ac[i].yPosition = new IDLLength(0.0);
            ac[i].zPosition = new IDLLength(0.0);
            ac[i].xOffset = new IDLLength(0.0);
            ac[i].yOffset = new IDLLength(0.0);
            ac[i].zOffset = new IDLLength(0.0);
            ac[i].dateOfCommission = 0;
            ac[i].padId = "PadID";
            ac[i].padxPosition = new IDLLength(0.0);
            ac[i].padyPosition = new IDLLength(0.0);
            ac[i].padzPosition = new IDLLength(0.0);
            ac[i].cableDelay = 0;
        }
        WeatherStationCharacteristics[] wsc = new  WeatherStationCharacteristics[0];
        SiteData site = new SiteData();
        site.telescopeName = "";
        site.arrayConfigurationName = "";
        site.minBaseRange = new IDLLength(0.0);
        site.maxBaseRange = new IDLLength(0.0);
        site.baseRmsMinor = new IDLLength(0.0);
        site.baseRmsMajor = new IDLLength(0.0);
        site.basePa = new IDLAngle(0.0);
        site.siteLongitude = new IDLAngle(0.0);
        site.siteLatitude = new IDLAngle(0.0);
        site.siteAltitude = new IDLLength(0.0);
        site.releaseDate = 0;
        array.setConfigInfo("Test", ac, wsc, site, new ResourceId[0], CorrelatorType.BL);

        logger.info("Initializing the array. startupPass1...");
        array.startupPass1();
        if (!(array.getArrayState() == SystemState.INACCESSIBLE &&
              array.getArraySubstate() == SystemSubstate.STARTED_UP_PASS1)) {
            logger.severe("Error in array state");
            fail();
        }

        logger.info("Continue initializing array. startupPass2...");
        array.startupPass2();
        if (array.getArrayState() != SystemState.OPERATIONAL ||
                array.getArraySubstate() != SystemSubstate.NOERROR) {
            logger.severe("Error in array state");
            fail();
        }
    }

    /**
     * Shuts down the AutomaticArray created with createAutomaticArray() and
     * releases this component.
     */
    private void destroyAutomaticArray() {
        
        array.shutdownPass1();
        array.shutdownPass2();
        getContainerServices().releaseComponent(arrayName);
        
    }
    
    /**
     * Test case fixture setup.
     */
    protected void setUp() throws Exception {
        
        System.out.println("ComponentClientTestCase.setUp()...");
        super.setUp();

        // Get the Logger.
        System.out.println("Getting the logger...");
        logger = getContainerServices().getLogger();

        // Create an AutomaticArray with one simulated antenna.
        String[] antennaNames = new String[1];
        antennaNames[0] = "AntennaMock01";
        createAutomaticArray(antennaNames);
        
    }

    /**
     * Test case fixture clean up.
     */
    protected void tearDown() throws Exception {
        
        destroyAutomaticArray();
        super.tearDown();
    }

    /**
     * A simple test for the SFI Offshoot. Just getting the offshoot and
     * calling one of its functions.
     */
    public void testSFIOffshoot() throws Exception {
        
        //logger.info("testSFIOffshoot");
        SingleFieldInterferometryObservingMode obsMode = array.getSFIOffshoot();
    }
    
//    /**
//     * Another simple test to check if it is possible to get the LO
//     * Observing Mode Offshoot withoout problems.
//     */
//    public void testLOOffshoot() throws Exception {
//        
//        // logger.info("testLOOffshoot");
//        SingleFieldInterferometryObservingMode sfiObsMode = array.getSFIOffshoot();
//        
//        LocalOscillator loObsMode = sfiObsMode.getLOObservingMode();
//        
//        // Test something in the LO Observing Mode.
//        assertEquals(0, loObsMode.numberOfSolutions());
//    }
    
//    public void testSFIOffshootSetDirection() throws Exception {
//        
//        // logger.info("testSFIOffshootSetDirection");
//        SingleFieldInterferometryObservingMode obsMode = array.getSFIOffshoot();
//        double commandedRA = 15.0 * Math.PI / 180.0;
//        double commandedDec = 30.0 * Math.PI / 180.0;
//        logger.info("Moving antenna...");
//        obsMode.setPointingDirection(commandedRA, commandedDec, 0.0, 0.0, 0.0);
//        logger.info("done.");
//    }

}
