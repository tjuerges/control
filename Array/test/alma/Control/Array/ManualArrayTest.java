/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Array;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.util.logging.Logger;

import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.AntennaCharacteristics;
import alma.Control.BDDStreamInfo;
import alma.Control.CorrelatorType;
import alma.Control.ManualArray;
import alma.Control.ManualArrayHelper;
import alma.Control.ResourceId;
import alma.Control.SiteData;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.Common.Name;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.acs.container.archive.UIDLibrary;
import alma.acs.entityutil.EntitySerializer;
import alma.asdmIDLTypes.IDLAngle;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.entity.xmlbinding.schedblock.SchedBlock;
import alma.entity.xmlbinding.schedblock.SchedBlockEntityT;
import alma.offline.ASDMArchivedEvent;
import alma.offline.DataCapturerId;
import alma.scheduling.MasterSchedulerIF;
import alma.scheduling.MasterSchedulerIFHelper;
import alma.xmlentity.XmlEntityStruct;
import alma.xmlstore.ArchiveConnection;
import alma.xmlstore.ArchiveConnectionHelper;
import alma.xmlstore.Identifier;
import alma.xmlstore.IdentifierHelper;
import alma.xmlstore.IdentifierJ;
import alma.xmlstore.IdentifierOperations;
import alma.xmlstore.Operational;

public class ManualArrayTest extends ComponentClientTestCase {

    ContainerServices container;
    Logger logger;
    Simulator simulator;
    
    MasterSchedulerIF masterScheduler;
    ArchiveConnection archConnectionComp;
    Operational archOperational;
    Identifier archIdentifierComp;
    
    public ManualArrayTest(String name) throws Exception {
        super(name);
    }

    protected void setUp() throws Exception {
        super.setUp();
        
        container = getContainerServices();
        logger = container.getLogger();
        simulator = SimulatorHelper.narrow(container.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0"));
        
        masterScheduler = MasterSchedulerIFHelper.narrow(container.getComponent("SCHEDULING_MASTERSCHEDULER"));
        
        archConnectionComp = ArchiveConnectionHelper.narrow(container
                .getComponent("ARCHIVE_CONNECTION"));
        assertNotNull(archConnectionComp);

        archOperational = archConnectionComp
                .getOperational("ManualArrayTest");
        assertNotNull(archOperational);

        archIdentifierComp = IdentifierHelper.narrow(container
                .getComponent("ARCHIVE_IDENTIFIER"));
        assertNotNull(archIdentifierComp);

    }

    protected void tearDown() throws Exception {
        container.releaseComponent(archConnectionComp.name());
        container.releaseComponent(archIdentifierComp.name());
        container.releaseComponent(masterScheduler.name());
        container.releaseComponent(simulator.name());
        super.tearDown();
    }

    public void testActivateManualArray() throws Exception {
        ComponentQueryDescriptor compSpec = new ComponentQueryDescriptor();
        compSpec.setComponentType("IDL:alma/Control/ManualArray:1.0");
        ManualArray array = ManualArrayHelper.narrow(container.getDynamicComponent(compSpec, false));
        
        array.startupPass1();
        array.startupPass2();
        array.shutdownPass1();
        array.shutdownPass2();
        
        container.releaseComponent(array.name());
    }
    
    /**
     * In manual mode, DataCapturer is created in beginExecution and released in
     * endExecution().
     */
    public void testBeginEndExecution() throws Exception {
        
        String sbUID = getSBIntoTheArchive("./schedblks/SchedBlock-BL.xml");

        String code =
            "from asdmIDLTypes import IDLEntityRef\n" +
            "sessionEntRef = IDLEntityRef('uid://X00/X372/X15', 'X00000000', 'ProjectStatus', '1.0')\n" +
            "return sessionEntRef";
        simulator.setMethod("SCHEDULING_MASTERSCHEDULER", "startManualModeSession", code, 0);
        
        code =
            "import offline\n" +
            "import asdmIDLTypes\n" +
            "event = offline.ASDMArchivedEvent(workingDCId=offline.DataCapturerId(name='CONTROL/Array001/DC000', " +
            "array='CONTROL/Array001', " +
            "session=asdmIDLTypes.IDLEntityRef(entityId='uid://X0000000000000000/X00000000', " +
            "partId='X00000000', entityTypeName='Session', instanceVersion='1.0'), " +
            "schedBlock=asdmIDLTypes.IDLEntityRef(entityId='uid://X00/X375/X2', partId='X00000000', " +
            "entityTypeName='SchedBlock', instanceVersion='1')), status='complete', " +
            "asdmId=asdmIDLTypes.IDLEntityRef(entityId='uid://X00/X372/X15', partId='X00000000', " +
            "entityTypeName='ASDM', instanceVersion='1'), archivedAt=0L)\n" +
            "supplyEventByInstance('Array', 'CONTROL_SYSTEM', event)\n" +
            "None";
        simulator.setMethodIF("IDL:alma/offline/DataCapturer:1.0", "endSBExecution", code, 0);
        
        simulator.setGlobalData("dataCapturerStarted", "false");
        simulator.setGlobalData("dataCapturerReleased", "false");
        
        code =
            "setGlobalData('dataCapturerStarted', 'true')\n" +
            "None";
        simulator.setMethodIF("IDL:alma/offline/DataCapturer:1.0", "initialize", code, 0);
        
        code =
            "setGlobalData('dataCapturerReleased', 'true')\n" +
            "None";
        simulator.setMethodIF("IDL:alma/offline/DataCapturer:1.0", "cleanUp", code, 0);
        
        ComponentQueryDescriptor compSpec = new ComponentQueryDescriptor();
        compSpec.setComponentType("IDL:alma/Control/ManualArray:1.0");
        ManualArray array = ManualArrayHelper.narrow(container.getDynamicComponent(compSpec, false));

        array.setParentName(this.getName());
        String[] antennaNames = {"ALMA01", "ALMA02"};
        array.setAntennaList(antennaNames);       

        logger.info("Setting Array configuration data");
        AntennaCharacteristics[] ac = new AntennaCharacteristics[antennaNames.length];
        for(int i = 0; i < antennaNames.length; i++) {
            ac[i] = new AntennaCharacteristics();
            ac[i].antennaName = antennaNames[i];
            ac[i].dishDiameter = new IDLLength(0.0);
            ac[i].xPosition = new IDLLength(0.0);
            ac[i].yPosition = new IDLLength(0.0);
            ac[i].zPosition = new IDLLength(0.0);
            ac[i].xOffset = new IDLLength(0.0);
            ac[i].yOffset = new IDLLength(0.0);
            ac[i].zOffset = new IDLLength(0.0);
            ac[i].dateOfCommission = 0;
            ac[i].padId = "PadID";
            ac[i].padxPosition = new IDLLength(0.0);
            ac[i].padyPosition = new IDLLength(0.0);
            ac[i].padzPosition = new IDLLength(0.0);
            ac[i].cableDelay = 0;
        }
        WeatherStationCharacteristics[] wsc = new  WeatherStationCharacteristics[0];
        SiteData site = new SiteData();
        site.telescopeName = "";
        site.arrayConfigurationName = "";
        site.minBaseRange = new IDLLength(0.0);
        site.maxBaseRange = new IDLLength(0.0);
        site.baseRmsMinor = new IDLLength(0.0);
        site.baseRmsMajor = new IDLLength(0.0);
        site.basePa = new IDLAngle(0.0);
        site.siteLongitude = new IDLAngle(0.0);
        site.siteLatitude = new IDLAngle(0.0);
        site.siteAltitude = new IDLLength(0.0);
        site.releaseDate = 0;
        array.setConfigInfo("Test", ac, wsc, site, new ResourceId[0], CorrelatorType.BL);        
        array.setBulkDataDistributors("corrBDD", "tpBDD");
        
        array.startupPass1();
        array.startupPass2();
        
        IDLEntityRef sbEntRef = new IDLEntityRef(sbUID, "X00000000", "SchedBlock", "1.0");
        array.configure(sbEntRef);
        
        array.beginExecution();
        
        // DataCapturer should have been activated now
        assertEquals("true", simulator.getGlobalData("dataCapturerStarted"));
        
        ASDMArchivedEvent ev = 
            new ASDMArchivedEvent(new DataCapturerId("CONTROL/Array001/DC000", "CONTROL/Array001",
                    new IDLEntityRef("uid://X0000000000000000/X00000000", "X00000000", "Session", "1.0"),
                    new IDLEntityRef("uid://X00/X375/X2", "X00000000", "SchedBlock", "1"),
                    new BDDStreamInfo("d", (short)0, (short)0, (short)0),
                    new BDDStreamInfo("d", (short)0, (short)0, (short)0)),
                    "complete",
                    new IDLEntityRef("uid://X00/X372/X15", "X00000000", "ASDM", "1"),
                    0L);
        array.asdmArchived(ev);
        array.endExecution(alma.Control.Completion.SUCCESS, "Yep!");
        
        // DataCapturer should have been deactivated
        assertEquals("true", simulator.getGlobalData("dataCapturerReleased"));
        
        array.shutdownPass1();
        array.shutdownPass2();
        
        container.releaseComponent(array.name());        
    }
    
    protected String getSBIntoTheArchive(String schedBlkFileName) throws Exception {
        
        // Read the SchedBlock from a file
        File f = new File(schedBlkFileName);
        BufferedReader br = new BufferedReader(new FileReader(f));
        StringBuffer sb = new StringBuffer();
        String line = br.readLine();
        while(line != null) {
            sb.append(line + "\n");
            line = br.readLine();
        }
        br.close();
        
        // Unmarshall the SchedBloc, i.e., create the SchedBlock class
        // from XML (using Castor)
        SchedBlock schedBlock =
            SchedBlock.unmarshalSchedBlock(new StringReader(new String(sb)));
        SchedBlockEntityT entity = schedBlock.getSchedBlockEntity();

        // Use the UID Library to replace the UIDs in the entity
        UIDLibrary uidlib = new UIDLibrary(logger);
        uidlib.replaceUniqueEntityId(entity,
                container.getTransparentXmlWrapper(IdentifierJ.class,
                archIdentifierComp, IdentifierOperations.class));
        schedBlock.setSchedBlockEntity(entity);
        
        // Serialize the entity, i.e., construct an XmlEntityStruct from
        // the SchedBlock class
        EntitySerializer serializer = EntitySerializer.getEntitySerializer(logger);
        XmlEntityStruct ent = serializer.serializeEntity(schedBlock,
                                  schedBlock.getSchedBlockEntity());
        archOperational.store(ent);
        
        // Finally, store it into the ARCHIVE
        return ent.entityId;
    }
}
