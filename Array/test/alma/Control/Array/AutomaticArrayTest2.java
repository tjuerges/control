package alma.Control.Array;

import java.util.logging.Logger;

import alma.Control.AntennaCharacteristics;
import alma.Control.CorrelatorType;
import alma.Control.ResourceId;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.SiteData;
import alma.Control.SystemState;
import alma.Control.SystemSubstate;
import alma.Control.TotalPowerObservingMode;
import alma.Control.WeatherStationCharacteristics;
import alma.Control.AbortionException;
import alma.Control.AutomaticArray2;
import alma.Control.AutomaticArray2Helper;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;
import alma.asdmIDLTypes.IDLAngle;
import alma.asdmIDLTypes.IDLLength;
import junit.framework.TestCase;

public class AutomaticArrayTest2 extends TestCase {

	static final int NTESTS = 3;
	static int testCount = NTESTS;
	static ComponentClient compClient = null;
	static boolean startedACS = false;
	
	StartACSTest acsenv = null;
	ContainerServices container = null;
	
    private Logger logger;
    private String arrayName;
    private AutomaticArray2 array;
	
	public AutomaticArrayTest2(String name) {
		super(name);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
		ContainerSpec[] cont = new ContainerSpec[1];
//		cont[0] = new ContainerSpec("CONTROL/ACC/javaContainer", "java");
		cont[0] = new ContainerSpec("CONTROL/ACC/pythonContainer", "py");
		String[] env = {"ACS_CDB=."};
		acsenv = new StartACSTest(cont, env);
		if (!acsenv.isACSRunning()) {
			acsenv.startACS(null);
			acsenv.startContainers(null);
			acsenv.startContainer("CONTROL/ACC/javaContainer", "java", null, null);
			Thread.sleep(5000);
			startedACS = true;
		}
		else {
			System.out.println("--- ACS is already running.");
			if (testCount == NTESTS)
				startedACS = false;
		}
			
		String managerLoc = System.getenv("MANAGER_REFERENCE");
		System.out.println("--- managerLoc = " + managerLoc);
		if (compClient == null)
			compClient = new ComponentClient(null, managerLoc, "AutomaticArrayTest2");
		container = compClient.getContainerServices();
		logger = container.getLogger();
	}

	protected void tearDown() throws Exception {
		testCount--;
		logger.info("tearDown: testCount="+testCount);
		if (startedACS && testCount == 0) {
			acsenv.shutdownContainers(null);
			acsenv.shutdownACS(null);			
		}
		super.tearDown();
	}

    public void testCreateAutomaticArray() {

    	logger.info("testCreateAutomaticArray");
        String[] antennaNames = {"ALMA01"};
        createAutomaticArray(antennaNames);
        initializeAutomaticArray();
        
        // Good... the array has been created fine.
        
        shutdownAutomaticArray();        
        destroyAutomaticArray();
    }

    public void testSFIOffshoot() throws Exception {

    	logger.fine("starting testSFIOffshoot");
        String[] antennaNames = {"ALMA01"};
        createAutomaticArray(antennaNames);
        initializeAutomaticArray();
        
        SingleFieldInterferometryObservingMode obsMode = null;
		try {
			obsMode = array.getSFIOffshoot();
		} catch (ObsModeInitErrorEx ex) {
			ex.printStackTrace();
			fail("Observation Mode Initialization Error.");
		}
        
        shutdownAutomaticArray();
        destroyAutomaticArray();
    }

    public void testTPOffshoot() throws Exception {

    	logger.fine("Starting testTPOffshoot");
        String[] antennaNames = {"ALMA01"};
        createAutomaticArray(antennaNames);
        initializeAutomaticArray();
        
        TotalPowerObservingMode obsMode = null;
		try {
			obsMode = array.getTotalPowerOffshoot();
		} catch (ObsModeInitErrorEx ex) {
			ex.printStackTrace();
			fail("Observation Mode Initialization Error.");
		}
        
        shutdownAutomaticArray();
        destroyAutomaticArray();
    }

    
    /**
     * Utility function. Creates an AutomaticArray dinamically.
     * 
     * @param antennaNames Antenna names.
     */
    private void createAutomaticArray(String[] antennaNames) {

        ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
        spec.setComponentName("Array001");
        spec.setComponentType("IDL:alma/Control/AutomaticArray2:1.0");
        
        try {
            org.omg.CORBA.Object obj = container.getDynamicComponent(spec, false);
            array = AutomaticArray2Helper.narrow(obj);
        } catch (AcsJContainerServicesEx ex) {
            logger.severe("Error creating AutomaticArray component: "
                    + ex.toString());
            ex.log(logger);
            fail();
        }

        array.setParentName(this.getName());
        array.setAntennaList(antennaNames);       

        logger.info("Setting Array configuration data");
        AntennaCharacteristics[] ac = new AntennaCharacteristics[antennaNames.length];
        for(int i = 0; i < antennaNames.length; i++) {
            ac[i] = new AntennaCharacteristics();
            ac[i].antennaName = antennaNames[i];
            ac[i].dishDiameter = new IDLLength(0.0);
            ac[i].xPosition = new IDLLength(0.0);
            ac[i].yPosition = new IDLLength(0.0);
            ac[i].zPosition = new IDLLength(0.0);
            ac[i].xOffset = new IDLLength(0.0);
            ac[i].yOffset = new IDLLength(0.0);
            ac[i].zOffset = new IDLLength(0.0);
            ac[i].dateOfCommission = 0;
            ac[i].padId = "PadID";
            ac[i].padxPosition = new IDLLength(0.0);
            ac[i].padyPosition = new IDLLength(0.0);
            ac[i].padzPosition = new IDLLength(0.0);
            ac[i].cableDelay = 0;
        }
        WeatherStationCharacteristics[] wsc = new  WeatherStationCharacteristics[0];
        SiteData site = new SiteData();
        site.telescopeName = "";
        site.arrayConfigurationName = "";
        site.minBaseRange = new IDLLength(0.0);
        site.maxBaseRange = new IDLLength(0.0);
        site.baseRmsMinor = new IDLLength(0.0);
        site.baseRmsMajor = new IDLLength(0.0);
        site.basePa = new IDLAngle(0.0);
        site.siteLongitude = new IDLAngle(0.0);
        site.siteLatitude = new IDLAngle(0.0);
        site.siteAltitude = new IDLLength(0.0);
        site.releaseDate = 0;
        array.setConfigInfo("Test", ac, wsc, site, new ResourceId[0], CorrelatorType.BL);

    }

    /**
     * Shuts down the AutomaticArray created with createAutomaticArray() and
     * releases this component.
     */
    private void destroyAutomaticArray() {
        
        container.releaseComponent(arrayName);
        
    }

    private void initializeAutomaticArray() {
        
        logger.info("Initializing the array. startupPass1...");
        array.startupPass1();
        if (!(array.getArrayState() == SystemState.INACCESSIBLE &&
              array.getArraySubstate() == SystemSubstate.STARTED_UP_PASS1)) {
            logger.severe("Error in array state");
            fail();
        }

        logger.info("Continue initializing array. startupPass2...");
        array.startupPass2();
        if (array.getArrayState() != SystemState.OPERATIONAL ||
                array.getArraySubstate() != SystemSubstate.NOERROR) {
            logger.severe("Error in array state");
            fail();
        }        
    }
    
    private void shutdownAutomaticArray() {
        array.shutdownPass1();
        array.shutdownPass2();        
    }
}
