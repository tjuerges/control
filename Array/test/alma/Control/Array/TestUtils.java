/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.Array;

import alma.ControlSB.SpectralSpec;
import alma.Correlator.BaseBandConfig;
import alma.Correlator.CorrelatorConfiguration;
import alma.xmlentity.XmlEntityStruct;

public class TestUtils {
    
//    static public CorrelatorConfiguration createDummyCorrelatorConfiguration() {
//        
//        BaseBandConfig bbc = 
//            new BaseBandConfig(alma.BasebandNameMod.BasebandName.BB_1,
//                               alma.AccumModeMod.AccumMode.NORMAL,
//                               alma.CorrelationModeMod.CorrelationMode.AUTO_ONLY,
//                               alma.SidebandProcessingModeMod.SidebandProcessingMode.FREQUENCY_OFFSET_REJECTION,
//                               new alma.Correlator.SpectralWindow[0],
//                               new alma.Correlator.BinSwitching_t(alma.SwitchingModeMod.SwitchingMode.NO_SWITCHING,
//                                       2, new long[] {0L}, new long[] {0L}),
//                               alma.ACAPolarizationMod.ACAPolarization.ACA_STANDARD,
//                               0.0);
//        BaseBandConfig[] bbcs = {bbc};
//        CorrelatorConfiguration config = 
//            new CorrelatorConfiguration(480000L, // dump duration (100ns)
//                                        50000000L, // integration duration (100ns)
//                                        50000000L, // channel average duration (100ns)
//                                        50000000L, // subscan duration (100ns)
//                                        alma.ReceiverSidebandMod.ReceiverSideband.DSB,
//                                        bbcs,
//                                        new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[]
//                                        {alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED},
//                                        new alma.Correlator.ACAPhaseSwitchingConfigurations(true, true, 0L));
//        return config;
//    }
    
    static public SpectralSpec createDummySpectralSpec() {
        SpectralSpec spec = new SpectralSpec(0.0,
                                             0.0,
                                             0.0,
                                             "TransitionName",
                                             alma.ControlSB.ReceiverBandType.NotUsed,
                                             0.0);
        return spec;
    }
        
    static public XmlEntityStruct createDummySource() {
        
        String xml = "<?xml version='1.0' encoding='UTF-8'?>" +
        "<FieldSource almatype='APDM::FieldSource' entityPartId=''>" +
        "<sourceCoordinates system='J2000'>" +
        "<ns6:longitude xmlns:ns6='Alma/ValueTypes' unit='rad'>0.0</ns6:longitude>" +
        "<ns7:latitude xmlns:ns7='Alma/ValueTypes' unit='rad'>0.0</ns7:latitude>" +
        "</sourceCoordinates>" +
        "<sourceName>Unknown</sourceName>" +
        "<sourceVelocity referenceSystem=''>" +
        "<ns8:centerVelocity xmlns:ns8='Alma/ValueTypes' unit='km/s'>0.0</ns8:centerVelocity>" +
        "</sourceVelocity>" +
        "<sourceEphemeris>None</sourceEphemeris>" +
        "<pMRA unit='mas/yr'>0.0</pMRA>" +
        "<pMDec unit='mas/yr'>0.0</pMDec>" +
        "<nonSiderealMotion>false</nonSiderealMotion>" +
        "<parallax unit='arcsec'>0.0</parallax>" +
        "<name>''</name>" +
        "<SourceProperty almatype='APDM::SourceProperty'>" +
        "<sourceFrequency unit='GHz'>0.0</sourceFrequency>" +
        "<sourceFluxI unit='Jy'>0.0</sourceFluxI>" +
        "<sourceDiameter unit='arcsec'>0.0</sourceDiameter>" +
        "</SourceProperty>" +
        "<solarSystemObject>''</solarSystemObject>" +
        "</FieldSource>";
        
        XmlEntityStruct src = new XmlEntityStruct(xml, "", "FieldSource","1.0", "");
     
        return src;
    }
}
