/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id: SFIObservingModeTest.java,v 1.13 2008/03/26 16:14:15 rhiriart Exp
$"
 */

package alma.Control.Array;

import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Iterator;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;

import alma.Control.CallbackTestClient;
import alma.Control.CallbackTestClientHelper;

import alma.Control.ObservingModes.AntModeControllerCBImpl;
import alma.Control.ObservingModes.IsObservableCBImpl;
import alma.ControlExceptions.IllegalParameterErrorEx;

public class CallbackTestImpl extends ComponentClientTestCase {

    public CallbackTestImpl(String test) throws Exception {
        super(test);
    }

    public CallbackTestImpl() throws Exception {
        super(CallbackTestImpl.class.getName());
    }

    private CallbackTestClient cbClient;
    private Logger logger;
    private ContainerServices cont;
    private final long WAIT_TIMEOUT = 200;

    ////////////////////////////////////////////////////////////////////
    // Test fixture methods
    ////////////////////////////////////////////////////////////////////

    protected void setUp() throws Exception {
        super.setUp();
        this.cont = getContainerServices();
        this.logger = cont.getLogger();

        this.cbClient = CallbackTestClientHelper.
            narrow(cont.getComponent("CONTROL/CallbackTestClient"));
    }

    protected void tearDown() throws Exception {
        cont.releaseComponent(cbClient.name());
        super.tearDown();
    }

    ////////////////////////////////////////////////////////////////////
    // Test cases
    ////////////////////////////////////////////////////////////////////
    public void testAntModeControllerSucess() throws Exception {
        
        AntModeControllerCBImpl cbObj = new AntModeControllerCBImpl(cont);

        cbObj.addExpectedResponse("DA41");
        cbClient.AntModeCBSuccess("DA41",cbObj.getExternalInterface());

        cbObj.addExpectedResponse("DV01");
        cbClient.AntModeCBSuccess("DV01",cbObj.getExternalInterface());
        
        HashMap<String, alma.ACSErr.Completion> completions;
        completions = cbObj.waitForCompletion(WAIT_TIMEOUT);

        assertEquals(completions.size(), 2);
        Iterator<String> cit = completions.keySet().iterator();

        while(cit.hasNext()) {
            String ant = cit.next();
            alma.ACSErr.Completion compl = completions.get(ant);

            assertNotNull(compl);
            assertEquals(compl.code, 0);
            assertEquals(compl.type, 0);

        }

    }

    public void testAntModeControllerFailure() throws Exception {
        
        AntModeControllerCBImpl cbObj = new AntModeControllerCBImpl(cont);

        cbObj.addExpectedResponse("DA41");
        cbClient.AntModeCBFailure("DA41",cbObj.getExternalInterface());

        cbObj.addExpectedResponse("DV01");
        cbClient.AntModeCBFailure("DV01",cbObj.getExternalInterface());
        
        HashMap<String, alma.ACSErr.Completion> completions;
        completions = cbObj.waitForCompletion(WAIT_TIMEOUT);

        Iterator<String> cit = completions.keySet().iterator();

        while(cit.hasNext()) {
            String ant = cit.next();
            alma.ACSErr.Completion compl = completions.get(ant);

            assertNotNull(compl);

            // Code 6 of Type 10006 is the unallocated exception.
            assertEquals(6, compl.code);
            assertEquals(10006, compl.type);

        }

    }


    public void testIsObservableTrue() throws Exception {
        
        IsObservableCBImpl cbObj = new IsObservableCBImpl(cont);

        cbObj.addExpectedResponse("DA41");
        cbClient.IsObservableCBSuccess("DA41",true,
                                       cbObj.getExternalInterface());

        cbObj.addExpectedResponse("DV01");
        cbClient.IsObservableCBSuccess("DV01", true,
                                       cbObj.getExternalInterface());
        
        assertEquals(2, cbObj.getResponse(WAIT_TIMEOUT));
    }

    public void testIsObservableFalse() throws Exception {
        
        IsObservableCBImpl cbObj = new IsObservableCBImpl(cont);

        cbObj.addExpectedResponse("DA41");
        cbClient.IsObservableCBSuccess("DA41", false,
                                       cbObj.getExternalInterface());

        cbObj.addExpectedResponse("DV01");
        cbClient.IsObservableCBSuccess("DV01", false,
                                       cbObj.getExternalInterface());
        
        assertEquals(0, cbObj.getResponse(WAIT_TIMEOUT));
    }

   public void testIsObservableSplit() throws Exception {
        
        IsObservableCBImpl cbObj = new IsObservableCBImpl(cont);

        cbObj.addExpectedResponse("DA41");
        cbClient.IsObservableCBSuccess("DA41", true,
                                       cbObj.getExternalInterface());

        cbObj.addExpectedResponse("DV01");
        cbClient.IsObservableCBSuccess("DV01", false,
                                       cbObj.getExternalInterface());
        
        assertEquals(1, cbObj.getResponse(WAIT_TIMEOUT));
    }

   public void testIsObservableUnallocated() throws Exception {
        
        IsObservableCBImpl cbObj = new IsObservableCBImpl(cont);

        cbObj.addExpectedResponse("DA41");
        cbClient.IsObservableCBSuccess("DA41", true,
                                       cbObj.getExternalInterface());

        cbObj.addExpectedResponse("DV01");
        cbClient.IsObservableCBUnallocatedEx("DV01",
                                             cbObj.getExternalInterface());
        
        assertEquals(1, cbObj.getResponse(WAIT_TIMEOUT));
    }

    public void testIsObservableParameter() throws Exception {
        
         IsObservableCBImpl cbObj = new IsObservableCBImpl(cont);

         cbObj.addExpectedResponse("DA41");
         cbClient.IsObservableCBSuccess("DA41", true,
                                        cbObj.getExternalInterface());
         cbObj.addExpectedResponse("DV01");
         cbClient.IsObservableCBParameterEx("DV01",
                                            cbObj.getExternalInterface());
         try {
             cbObj.getResponse(WAIT_TIMEOUT);
         } catch (IllegalParameterErrorEx ex) {
             /* This is expected */
             return;
         }
         fail("Failed to throw IllegalParameterErrorEx");
    }

}
