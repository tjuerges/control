package alma.Control.Array;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class StartACSTest {
	
	private ContainerSpec[] containers;
	private String[] environment;
	
	private class Plug extends Thread {
		BufferedReader in;
		BufferedWriter out;
		public Plug(OutputStream out, InputStream in) {
			this.in = new BufferedReader(new InputStreamReader(in));
			this.out = new BufferedWriter(new OutputStreamWriter(out));
		}
		public void run() {
	        String line = "";
	        try {
	        	while ((line = in.readLine()) != null) {
	        		out.write(line + "\n");
	        		out.flush();
	        	}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}

	public StartACSTest(ContainerSpec[] containers, String[] env) {
		
		this.containers = containers;
		
		// Form the environment usign all the environment variables in the
		// current environment + the new ones supplied in environment.
		Map<String, String> newenv = new HashMap<String, String>();
		for (int i=0; i<env.length; i++) {
			String[] s = env[i].split("=");
			newenv.put(s[0], s[1]);
		}
		
		Map<String, String> currenv = System.getenv();
		Iterator<String> ceiter = currenv.keySet().iterator();
		while (ceiter.hasNext()) {
			String envvar = ceiter.next();
			if (!newenv.containsKey(envvar) && !envvar.equals("DISPLAY"))
				newenv.put(envvar, currenv.get(envvar));
		}

		Iterator<String> neiter = newenv.keySet().iterator();
		environment = new String[newenv.size()];
		int i = 0;
		while (neiter.hasNext()) {
			String envvar = neiter.next();
			environment[i] = envvar + "=" + newenv.get(envvar);
			i++;
		}		
	}
	

	private void plugTogether(OutputStream out, InputStream in) {
		Plug thread = new Plug(out, in);
		thread.start();
	}

	public void startACS(OutputStream out) {
		startACS(out, out);
	}
	
	public void startACS(OutputStream out, OutputStream err) {
		System.out.println("--- Starting ACS... ");
		String command = "acsStart";
		startProcess(command, out, err);
		System.out.println("--- ACS start done");		
	}
	
	public void shutdownACS(OutputStream out) {
		shutdownACS(out, out);
	}
	
	public void shutdownACS(OutputStream out, OutputStream err) {
		System.out.println("--- Shutting down ACS... ");
		String command = "acsStop";
		startProcess(command, out, err);
		System.out.println("--- ACS shutdown done");		
	}

	public void startContainers(OutputStream out) {
		startContainers(out, out);
	}
	
	public void startContainers(OutputStream out, OutputStream err) {
		System.out.println("--- Starting up containers...");
		for(int i=0; i<containers.length; i++) {
			System.out.println("--- Starting container process '" + containers[i].getName() + "'");
			startProcessAsync(containers[i].getStartupCommand(), out, err);
			System.out.println("--- Done starting container.");
		}
	}

	public void shutdownContainers(OutputStream out) {
		shutdownContainers(out, out);
	}

	public void shutdownContainers(OutputStream out, OutputStream err) {
		System.out.println("--- Shutting down containers...");
		for(int i=0; i<containers.length; i++) {
			System.out.println("--- Shutting down container process '" + containers[i].getName() + "'");
			startProcess(containers[i].getShutdownCommand(), out, err);
			System.out.println("--- Done shutting down container.");
		}		
	}

	public void startContainer(String name, String type, OutputStream out, OutputStream err) {
		System.out.println("--- Starting container process '" + name + "'");
		startProcessAsync("acsStartContainer -"+type+" "+name , out, err);
		System.out.println("--- Done starting container.");		
	}
	
	public void shutdownContainer(String name, OutputStream out, OutputStream err) {
		System.out.println("--- Shutdown container process '" + name + "'");
		startProcess("acsStopContainer "+name, out, err);
		System.out.println("--- Done shutting down container.");		
	}
	
	public boolean isACSRunning() {
		String acsdata = System.getenv("ACSDATA");
		File instdir = new File(acsdata + "/tmp/ACS_INSTANCE.0");
		return instdir.exists();
	}
	
	private void startProcess(String process, OutputStream out, OutputStream err) {
		try {
			Process acs = Runtime.getRuntime().exec(process, environment);
			if (out != null)
				plugTogether(out, acs.getInputStream());
			if (err != null)
				plugTogether(err, acs.getErrorStream());
			acs.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void startProcessAsync(String process, OutputStream out, 
			                       OutputStream err) {
		try {
			Process acs = Runtime.getRuntime().exec(process, environment);
			if (out != null)
				plugTogether(out, acs.getInputStream());
			if (err != null)
				plugTogether(err, acs.getErrorStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ContainerSpec[] cont = new ContainerSpec[2];
		cont[0] = new ContainerSpec("CONTROL/ACC/javaContainer", "java");
		cont[1] = new ContainerSpec("CONTROL/ACC/pythonContainer", "py");
		String[] env = {"ACS_CDB=/export/home/pumice/rhiriart/config"};
		StartACSTest a = new StartACSTest(cont, env);
		if (!a.isACSRunning()) {
			a.startACS(System.out, System.err);
			a.startContainers(System.out, System.err);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			a.shutdownContainers(System.out, System.err);
			a.shutdownACS(System.out, System.err);
		}
		else {
			System.out.println("--- ACS is already running.");
		}
	}
	
}
