<?xml version="1.0" encoding="iso-8859-1"?>
<!--
$Id$
ALMA - Atacama Large Millimeter Array
(c) Associated Universities Inc., 2010

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
-->

<!-- 
These focus models are loaded when the Mount component starts i.e,
when the control subsystem goes operational. The overall focus
model is the sum of the coefficients for the current antenna and band.

The nomenclature used is that described by Richard Hills in his note
"Control of Antenna Focus - Detailed Requirements" dated Jan-19-2010. 
The names for the terms are: 
XR, XC, XS, XTA
YR, YC, YS, YTA
ZR, ZC, ZS, ZTA
ALPHA, BETA (X-tip & Y-tilt)
Parsing of these term names is case insensitive. 

All coefficients are in SI units i.e., either meters, radians or
meters/deg K. The reference elevation (El_R) is 50 degrees and the
reference temperature (T_R) is 273 deg K (0 deg C).

There is at most one entry for each antenna and band. Only non-zero
terms need to be included as missing terms are assumed to be
zero. Missing antennas and bands are assumed to have a focus model
with all zero coefficients.

The contribution from the band specific coefficients are usually
changed whenever the observing frequency changes (this is not always
done as described in Richard's note). Bands must be in the range 
"1" to "10" and band "0", used to designate optical pointing in the
PointingModel.xml file, is not allowed.
-->

<FocusModel>
    <Antenna name="DV01">
        <Term name="XR"  value="-0.49E-3"/>
        <Term name="YR"  value="-2.73E-3"/>
        <Term name="ZR"  value="-6.04E-3"/>
        <Term name="ZS"  value="+2.06E-3"/>
        <Term name="YC"  value="-3.50E-3"/>
        <Term name="ZTA" value="+0.00E-3"/>
    </Antenna>

    <Antenna name="DA41">
        <Term name="XR"  value="+0.01E-3"/>
        <Term name="XS"  value="-0.02E-3"/>
        <Term name="XC"  value="-0.01E-3"/>
        <Term name="XTA" value="+0.01E-3"/>

        <Term name="YR"  value="-0.01E-3"/>
        <Term name="YS"  value="+0.02E-3"/>
        <Term name="YC"  value="+0.01E-3"/>
        <Term name="YTA" value="-0.01E-3"/>

        <Term name="ZR"  value="+9.00E-3"/>
        <Term name="ZS"  value="-2.00E-3"/>
        <Term name="ZC"  value="-1.00E-3"/>
        <Term name="ZTA" value="+1.00E-3"/>
    </Antenna>

    <BandOffset name="3">
        <Term name="ALPHA" value="+0.0027227136331111541"/>
        <Term name="BETA"  value="-0.015428710587629874"/>
    </BandOffset>

    <BandOffset name="6">
        <Term name="XR" value="+0.10E-3"/>
        <Term name="YR" value="+0.34E-3"/>
        <Term name="ZR" value="+0.03E-3"/>
        <Term name="ALPHA" value="+0.014433872913993104"/>
        <Term name="BETA"  value="+0.014433872913993104"/>
    </BandOffset>

    <BandOffset name="7">
        <Term name="XR" value="-0.31E-3"/>
        <Term name="YR" value="+0.11E-3"/>
        <Term name="ZR" value="-0.12E-3"/>
        <Term name="ALPHA" value="+0.0083252205320129523"/>
        <Term name="BETA"  value="+0.0"/>
    </BandOffset>
</FocusModel>
