<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * $Id$
 * $Source$
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
-->

<dtx:DTX xmlns="urn:schemas-cosylab-com:DTXBase:1.0"
    xsi:schemaLocation="urn:schemas-cosylab-com:DTX:1.0 http://www.nrao.edu/schemas/CONTROL/DTX.xsd"
      xmlns:dtx="urn:schemas-cosylab-com:DTX:1.0"
      xmlns:amb="urn:schemas-cosylab-com:AmbDevice:1.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

      USE_NEG_CLOCK_EDGE="false">

  <DG_3_3_V 

    archive_priority="15"
    archive_min_int="10"
    archive_max_int="10"

    default_value="0"

    graph_min="0"

    graph_max="10"

    format="%7.2f"
    description="3.3 Voltage reading from the DG"
    units="volt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="10"

    resolution="1"
    min_step="1"

    alarm_high_on="3.5"
    alarm_high_off="3.5"
    alarm_low_on="3.1"
    alarm_low_off="3.1"
    alarm_timer_trig="10"
    />

  <DG_5_V

    archive_priority="15"
    archive_min_int="10"
    archive_max_int="10"

    default_value="0"

    graph_min="0"

    graph_max="10"

    format="%7.2f"
    description="5.0 Voltage reading from the DG"
    units="volt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="10"

    resolution="1"
    min_step="1"

    alarm_high_on="5.2"
    alarm_high_off="5.2"
    alarm_low_on="4.8"
    alarm_low_off="4.8"
    alarm_timer_trig="10"
    />

  <DG_TEMP

    archive_priority="15"
    archive_min_int="10"
    archive_max_int="10"

    default_value="0"

    graph_min="0"

    graph_max="50"

    format="%7.2f"
    description="Temperature from DG"
    units="kelvin"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="10"

    resolution="1"
    min_step="1"

    alarm_high_on="313.15"
    alarm_high_off="313.15"
    alarm_low_on="283.15"
    alarm_low_off="283.15"
    alarm_timer_trig="10"
    />

  <DG_FW_VER
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="Firmware version"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <DG_VH1

    archive_priority="15"
    archive_min_int="600"
    archive_max_int="600"

    default_value="0"

    graph_min="0"

    graph_max="2.5"

    format="%7.2f"
    description="Phase 1 Reference Voltage High"
    units="volt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="600"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <DG_VL1

    archive_priority="15"
    archive_min_int="600"
    archive_max_int="600"

    default_value="0"

    graph_min="0"

    graph_max="2.5"

    format="%7.2f"
    description="Phase 1 Reference Voltage Low"
    units="volt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="600"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <DG_VH2

    archive_priority="15"
    archive_min_int="600"
    archive_max_int="600"

    default_value="0"

    graph_min="0"

    graph_max="2.5"

    format="%7.2f"
    description="Phase 2 Reference Voltage High"
    units="volt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="600"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <DG_VL2

    archive_priority="15"
    archive_min_int="600"
    archive_max_int="600"

    default_value="0"

    graph_min="0"

    graph_max="2.5"

    format="%7.2f"
    description="Phase 2 Reference Voltage Low"
    units="volt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="600"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_1_5_V

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    graph_min="0"

    graph_max="5"

    format="%7.2f"
    description="1.5 Voltage data for FPGAs 1,2,3."
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_1_8_V

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    format="%none"
    description="1.8 Voltage data for FPGAs 1,2,3."
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_BOARD_VOLTAGE

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    format="%none"
    description="Voltage data for board voltages"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_TMP

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    format="%none"
    description="Temperature data for FR and TTX"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_LASER_PWR

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    format="%none"
    description="none"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_LASER_BIAS

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    format="%none"
    description="none"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_STATUS
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="FR Status Register"
    units="none"
    min_timer_trig="0.048"

    bitDescription=""
    alarm_mask="0"
    alarm_trigger="0"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_TE_STATUS
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="FR TE Status Register"
    units="none"
    min_timer_trig="0.048"

    bitDescription=""
    alarm_mask="0"
    alarm_trigger="0"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_48_V

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    format="%none"
    description="none"
    units="none"
    min_timer_trig="0.048"

    bitDescription=""
    alarm_mask="0"
    alarm_trigger="0"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_FPGA_FW_VER_CH1
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="none"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_FPGA_FW_VER_CH2
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="none"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_FPGA_FW_VER_CH3
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="none"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_PHASE_OFFSET

    archive_priority="15"
    archive_min_int="1"
    archive_max_int="1"

    default_value="0"

    graph_min="0"

    graph_max="30"

    format="%none"
    description="Phase Switching Value"
    units="second"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="1"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <FR_LRU_CIN
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="Line Replaceable Units CIN"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_ALARM_STATUS
    archive_priority="15"
    archive_min_int="0"
    archive_max_int="0"
    default_value="0"

    format="%none"
    description="TTX Alarm Status data"
    units="none"
    min_timer_trig="0.048"

    bitDescription=""
    alarm_mask="0"
    alarm_trigger="0"

    default_timer_trig="0"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_BIAS_CH1

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH1 TTX laser bias current"
    units="ampere"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_BIAS_CH2

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH2 TTX laser bias current"
    units="ampere"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_BIAS_CH3

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH3 TTX laser bias current"
    units="ampere"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_PWR_CH1

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH1 TTX laser output power"
    units="watt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_PWR_CH2

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH2 TTX laser output power"
    units="watt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_PWR_CH3

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH3 TTX laser output power"
    units="watt"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_TMP_CH1

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH1 TTX laser temperature error"
    units="kelvin"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_TMP_CH2

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH2 TTX laser temperature error"
    units="kelvin"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_TMP_CH3

    archive_priority="15"
    archive_min_int="2"
    archive_max_int="2"

    default_value="0"

    format="%7.2f"
    description="CH3 TTX laser temperature error"
    units="kelvin"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="2"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <TTX_LASER_ENABLED

    archive_priority="15"
    archive_min_int="60"
    archive_max_int="60"

    default_value="0"

    format="%none"
    description="none"
    units="none"
    min_timer_trig="0.048"

    bitDescription=""
    alarm_mask="0"
    alarm_trigger="0"

    default_timer_trig="60"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <SERIAL_NUMBER

    archive_priority="15"
    archive_min_int="900"
    archive_max_int="900"

    default_value="0"

    format="%none"
    description="Generic Monitor Point. Return the Serial number of the device"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="900"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <PROTOCOL_REV_LEVEL

    archive_priority="15"
    archive_min_int="900"
    archive_max_int="900"

    default_value="0"

    format="%none"
    description="Generic Monitor Point. Return the Protocol Revision Level."
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="900"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <CAN_ERROR

    archive_priority="15"
    archive_min_int="1"
    archive_max_int="1"

    default_value="0"

    format="%none"
    description="Generic Monitor Point. Can Bus Errors since powered up and error code of last error"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="1"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <SW_REV_LEVEL

    archive_priority="15"
    archive_min_int="300"
    archive_max_int="300"

    default_value="0"

    format="%none"
    description="Generic Monitor Point.Revision level of embeded code. This is the software embedded in the AMBSI or the programmable device that is handling the AMB communications."
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="300"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

  <AMBIENT_TEMPERATURE

    archive_priority="15"
    archive_min_int="1"
    archive_max_int="1"

    default_value="0"

    format="%none"
    description="Generic Monitor Point. Temperature mesure by DS1820"
    units="none"
    min_timer_trig="0.048"

    min_delta_trig="1"

    default_timer_trig="1"

    resolution="1"
    min_step="1"
    alarm_timer_trig="0"
    />

    <Address
        ChannelNumber = "0"
        NodeNumber = "80"
    />
</dtx:DTX>
