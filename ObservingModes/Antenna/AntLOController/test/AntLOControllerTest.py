#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 - 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import shutil
shutil.rmtree("../lib/python/site-packages", True)
import sys
import unittest
import Acspy.Clients.SimpleClient
import Acspy.Common.ErrorTrace
import TMCDB_IDL
import asdmIDLTypes
import ModeControllerExceptions
import ControlExceptions
from PyDataModelEnumeration import PyReceiverBand
from PyDataModelEnumeration import PyCalibrationDevice
from PyDataModelEnumeration import PyNetSideband 
import Acspy.Common.TimeHelper
import time
from types import InstanceType

# **************************************************
# automated test class
# **************************************************
class AntLOControllerTest( unittest.TestCase ):
    def setUp(self):
        self.client = Acspy.Clients.SimpleClient.PySimpleClient()
        self.logger = self.client.getLogger()

    def initializeMasterForAntLOTesting(self, antennaName):
        floogConfig = TMCDB_IDL.AssemblyLocationIDL\
                      ("FLOOG", "FLOOG", 0x32, 0, 0)
        dgckConfig = TMCDB_IDL.AssemblyLocationIDL\
                     ("DGCK", "DGCK", 0x30, 0, 0)
        DTXBBpr0Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("DTXBBpr0", "DTXBBpr0", 0x50, 0, 0)
        DTXBBpr1Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("DTXBBpr1", "DTXBBpr1", 0x51, 0, 0)
        DTXBBpr2Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("DTXBBpr2", "DTXBBpr2", 0x52, 0, 0)
        DTXBBpr3Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("DTXBBpr3", "DTXBBpr3", 0x53, 0, 0)
        lo2BBpr0Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("LO2BBpr0", "LO2BBpr0", 0x40, 0, 0)
        lo2BBpr1Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("LO2BBpr1", "LO2BBpr1", 0x41, 0, 0)
        lo2BBpr2Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("LO2BBpr2", "LO2BBpr2", 0x42, 0, 0)
        lo2BBpr3Config = TMCDB_IDL.AssemblyLocationIDL\
                         ("LO2BBpr3", "LO2BBpr3", 0x43, 0, 0)
        IFProc0Config = TMCDB_IDL.AssemblyLocationIDL\
                        ("IFProc0", "IFProc0", 0x29, 0, 0)
        IFProc1Config = TMCDB_IDL.AssemblyLocationIDL\
                        ("IFProc1", "IFProc1", 0x2a, 0, 0)
        ACDConfig     =  TMCDB_IDL.AssemblyLocationIDL\
                        ("ACD", "ACD", 0x28, 0, 0)
        SASConfig     =  TMCDB_IDL.AssemblyLocationIDL\
                        ("SAS", "SAS", 0x300, 0, 0)
        feConfig = TMCDB_IDL.AssemblyLocationIDL\
                   ("FrontEnd", "FrontEnd", 0x25, 0, 0)
        antennaAssembly = [floogConfig, dgckConfig, \
                           DTXBBpr0Config, DTXBBpr1Config, \
                           DTXBBpr2Config, DTXBBpr3Config, \
                           lo2BBpr0Config, lo2BBpr1Config, \
                           lo2BBpr2Config, lo2BBpr3Config, \
                           IFProc0Config, IFProc1Config,\
                           ACDConfig, SASConfig, feConfig]
        
        wca3 = TMCDB_IDL.AssemblyLocationIDL\
               ("WCA3", "WCA3", 0, 0, 0)
        pd3 = TMCDB_IDL.AssemblyLocationIDL\
               ("PowerDist3", "PowerDist3", 0, 0, 0)
        cc3 = TMCDB_IDL.AssemblyLocationIDL\
               ("ColdCart3", "ColdCart3", 0, 0, 0)
        fec = TMCDB_IDL.AssemblyLocationIDL\
              ("Cryostat", "Cryostat", 0, 0, 0)
        lpr = TMCDB_IDL.AssemblyLocationIDL\
              ("LPR", "LPR", 0, 0, 0)
        ifsw = TMCDB_IDL.AssemblyLocationIDL\
               ("IFSwitch", "IFSwitch", 0, 0, 0)
        frontEndAssembly = [wca3, pd3, cc3, fec, lpr, ifsw];

        sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "TF1", "", 1,
                                          frontEndAssembly, antennaAssembly)
        tmcdb = self.client.getDefaultComponent(\
                "IDL:alma/TMCDB/TMCDBComponent:1.0");
        tmcdb.setStartupAntennasInfo([sai])

        ai = TMCDB_IDL.AntennaIDL(0, antennaName,  "", \
                                  asdmIDLTypes.IDLLength(12), \
                                  asdmIDLTypes.IDLArrayTime(0), \
                                  asdmIDLTypes.IDLLength(1.0), \
                                  asdmIDLTypes.IDLLength(2.0), \
                                  asdmIDLTypes.IDLLength(10.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), 0)
        tmcdb.setAntennaInfo(antennaName, ai)
        pi = TMCDB_IDL.PadIDL(0, "A001", asdmIDLTypes.IDLArrayTime(0), \
                              asdmIDLTypes.IDLLength(-1601361.555455), \
                              asdmIDLTypes.IDLLength(-5042191.805932), \
                              asdmIDLTypes.IDLLength(3554531.803007))
        tmcdb.setAntennaPadInfo(antennaName,pi)
        self.master.startupPass1()
        self.master.startupPass2()
        self.client.releaseComponent(tmcdb._get_name())

    def initializeArray(self, antennaName):
        self.master = self.client.getComponent('CONTROL/MASTER')
        self.failUnless(self.master != None,
                        "Unable to create the master component")
        self.shutdownMasterAtEnd = False;
        if (str(self.master.getMasterState()) == 'INACCESSIBLE'):
            self.initializeMasterForAntLOTesting(antennaName);
            self.shutdownMasterAtEnd = True;
        self.antennaName = self.master.getAvailableAntennas()[0]

    def destroyArray(self):
        if (str(self.master.getMasterState()) == 'OPERATIONAL' and
            self.shutdownMasterAtEnd):
            self.master.shutdownPass1()
            self.master.shutdownPass2()
        self.client.releaseComponent(self.master._get_name()) 

    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        loc = self.client.getDynamicComponent \
             ( None,
               'IDL:alma/Control/AntLOController:1.0',
               None,
               'CONTROL/DA41/cppContainer')
        self.failUnless(loc != None,
                            "Unable to create an Antenna LO Component")
        self.client.releaseComponent(loc._get_name())

    def test02( self ):
        try:
            antennaName = "DA41"
            self.initializeArray(antennaName)
            loc = None;
            try:   
                loc = self.client.getDynamicComponent \
                      ( None,
                        'IDL:alma/Control/AntLOController:1.0',
                        None,
                        'CONTROL/DA41/cppContainer')
                loc.allocate(antennaName)
                
                # Set Fringe Tracking to FLOOG and LO2 devices
                loc.setEnableFringeTracking(True)
                
                # Get Fringe Tracking status
                self.assertEqual(loc.getEnableFringeTracking(), True);
                
                # Set Frequencies for FLOOG and LO2 devices
                timeNow = Acspy.Common.TimeHelper.getTimeStamp()
                ep = timeNow.value + (480000 * 2)
                # This is the default setup when the ATF is tuned to 103.8GHz 
                loc.setFrequencies(PyReceiverBand.ALMA_RB_03,\
                                   100E9, 35.5E6, PyNetSideband.USB, 1, \
                                   10.9675E9, 11.0345E9, 11.0315E9,11.0305E9,\
                                   True, True, ep)
                
                # Set a frequency offset
                loc.setAntennaFrequencyOffsetFactor(1)

                time.sleep(0.5);
                # Get the nominal frequency of FLOOG and LO2 devices.
                (frequencyBand, floogFreq, lo1TuneHigh, 
                 lo2BBpr0Freq, lo2BBpr1Freq, lo2BBpr2Freq, lo2BBpr3Freq, 
                 ABHigh, CDHigh) = loc.getFrequencies()
            
                #self.logger.logDebug("lo1sb: " + str(lo1TuneHigh))
                self.assertEqual(frequencyBand, 'ALMA_RB_03');
                self.assertAlmostEqual(floogFreq, 35.5E6, 1);
                self.assertEqual(lo1TuneHigh, PyNetSideband.USB);
                self.assertAlmostEqual(lo2BBpr0Freq, 10.9675E9, 2);
                self.assertAlmostEqual(lo2BBpr1Freq, 11.0345E9, 2);
                self.assertAlmostEqual(lo2BBpr2Freq, 11.0315E9, 2);
                self.assertAlmostEqual(lo2BBpr3Freq, 11.0305E9, 2);
                self.assertTrue(ABHigh);
                self.assertTrue(CDHigh);

                # Get the current frequency offset
                offset = loc.getAntennaFrequencyOffsetFactor()
                self.assertEqual(offset, 1);
                
                # Get the receiver information
                (hwSerialNumber, dewarName, dewarTemp) = loc.getReceiverInfo()
                self.assertEqual(hwSerialNumber, '1234');
                self.assertEqual(dewarName, 'BillyBob');
                self.assertAlmostEqual(dewarTemp, 65.0, 1);
                
                # Get the signal paths setup
                (BaseBandAHigh, BaseBandBHigh, BaseBandCHigh, BaseBandDHigh, \
                 ABUpperSideBand, CDUpperSideBand) = loc.getSignalPaths()
                
                self.assertTrue(BaseBandAHigh);
                self.assertTrue(BaseBandBHigh);
                self.assertTrue(BaseBandCHigh);
                self.assertTrue(BaseBandDHigh);
                self.assertTrue(ABUpperSideBand);
                self.assertTrue(CDUpperSideBand);

            finally:
                # Test finished...destroy array and  release the component.
                if (loc != None): self.client.releaseComponent(loc._get_name())
                self.destroyArray();
        except (ControlExceptions.IllegalParameterErrorEx,
                ModeControllerExceptions.HardwareFaultEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).\
                log("test02 failed");
            raise

#     def testCheckSubdevices(self):
#         try:
#             antennaName = "DA41"
#             self.initializeArray(antennaName)
#             loc = None;
#             try:   
#                 loc = self.client.getDynamicComponent \
#                       ( None,
#                         'IDL:alma/Control/AntLOController:1.0',
#                         None,
#                         'CONTROL/DA41/cppContainer')
#                 loc.allocate(antennaName)
                
#                 # Check the components references were obtained
#                 prefix = "CONTROL/" + antennaName + "/"
#                 self.assertEqual(loc.getFLOOG(), prefix + 'FLOOG');
#                 self.assertEqual(loc.getDGCK(), prefix + 'DGCK');
#                 self.assertEqual(loc.getLO2BBpr0(), prefix + 'LO2BBpr0');
#                 self.assertEqual(loc.getLO2BBpr1(), prefix + 'LO2BBpr1');
#                 self.assertEqual(loc.getLO2BBpr2(), prefix + 'LO2BBpr2');
#                 self.assertEqual(loc.getLO2BBpr3(), prefix + 'LO2BBpr3');
#                 self.assertEqual(loc.getIFProcessorP0(), prefix + 'IFProc0');
#                 self.assertEqual(loc.getIFProcessorP1(), prefix + 'IFProc1');
#                 self.assertEqual(loc.getFrontEnd(), prefix + 'FrontEnd');
#                 self.assertEqual(loc.getSAS(), prefix + 'SAS');

#             finally:
#                 # Test finished...destroy array and  release the component.
#                 if (loc != None): self.client.releaseComponent(loc._get_name())
#                 self.destroyArray();
#         except (ControlExceptions.IllegalParameterErrorEx,
#                 ModeControllerExceptions.HardwareFaultEx), ex:
#             Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
#             raise

#     def testCalibrationDevice(self):
#         try:
#             antennaName = "DA41"
#             self.initializeArray(antennaName)
#             loc = None;
#             try:   
#                 loc = self.client.getDynamicComponent \
#                       ( None,
#                         'IDL:alma/Control/AntLOController:1.0',
#                         None,
#                         'CONTROL/DA41/cppContainer')
#                 loc.allocate(antennaName)

#                 fend = self.client.getComponent(loc.getFrontEnd())

#                 fend.powerUpBand(PyReceiverBand.ALMA_RB_03)
#                 fend.selectBand(PyReceiverBand.ALMA_RB_03)

#                 currentState = loc.getCurrentStateData()
                
#                 self.assertEqual(currentState.antennaName, antennaName)
#                 self.assertEqual(currentState.calDevice,
#                                  PyCalibrationDevice.NONE)
#                 self.assertTrue(currentState.signal)
#                 self.assertFalse(currentState.reference)
#                 self.assertTrue(currentState.onSky)

#                 loc.setCalibrationDevice(PyCalibrationDevice.HOT_LOAD)

#                 currentState = loc.getCurrentStateData()
#                 self.assertEqual(currentState.antennaName, antennaName)
#                 self.assertEqual(currentState.calDevice,
#                                  PyCalibrationDevice.HOT_LOAD)
#                 self.assertFalse(currentState.signal)
#                 self.assertTrue(currentState.reference)
#                 self.assertFalse(currentState.onSky)
                             
#             finally:
#                 # Test finished...destroy array and  release the component.
#                 if (loc != None): self.client.releaseComponent(loc._get_name())
#                 self.destroyArray();
#         except (ControlExceptions.IllegalParameterErrorEx,
#                 ModeControllerExceptions.HardwareFaultEx), ex:
#             Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
#             raise

#     def testSettingPhotonicReference(self):
#         try:
#             antennaName = "DA41"
#             self.initializeArray(antennaName)
#             loc = None;
#             try:   
#                 loc = self.client.getDynamicComponent \
#                       ( None,
#                         'IDL:alma/Control/AntLOController:1.0',
#                         None,
#                         'CONTROL/DA41/cppContainer')
#                 loc.allocate(antennaName)

#                 loc.selectPhotonicReference(1)
#                 self.assertEqual(1, loc.getSelectedPhotonicReference())

#                 loc.selectPhotonicReference(2)
#                 self.assertEqual(2, loc.getSelectedPhotonicReference())

#                 loc.selectPhotonicReference(3)
#                 self.assertEqual(3, loc.getSelectedPhotonicReference())

#             finally:
#                 # Test finished...destroy array and  release the component.
#                 if (loc != None): self.client.releaseComponent(loc._get_name())
#                 self.destroyArray();
#         except (ControlExceptions.IllegalParameterErrorEx,
#                 ModeControllerExceptions.HardwareFaultEx), ex:
#             Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
#             raise




# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    suite = unittest.makeSuite(AntLOControllerTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
