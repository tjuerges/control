// $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2011
//
// This library is free software; you can redistribute it and/oR
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA

#include <AntLOControllerImpl.h>

#include <cmath> // for std::floor and std::fabs
#include <loggingMACROS.h> // for LOG_TO_*
#include <simpleCallbackImpl.h>
#include <string> // for std::string
#include <sstream> // for std::ostringstream
#include <AntennaC.h> //  for Control::Antenna
#include <ACSErrTypeOK.h>
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <CReceiverBand.h>
#include <DGCKC.h>   //   for Control::DGCK
#include <FrontEndC.h> // for Control::FrontEnd
#include <FrontEndExceptions.h>
#include <IFProcC.h> // for Control::IFProc
#include <IFSwitchC.h> // for Control::IFSwitch
#include <FLOOGC.h> // for Control::FLOOG
#include <LO2C.h> // for Control::LO2
#include <DTXC.h> // for Control::DTX
#include <LO2Exceptions.h>
#include <ModeControllerExceptions.h>
#include <WCAExceptions.h>
#include <CNetSideband.h>
#include <SASC.h>   //   for Control::SAS
#include <ControlDataInterfacesC.h> // For the LOOffsettingMode
#include <TETimeUtil.h>
#include <Guard_T.h> 

using NetSidebandMod::NetSideband;
using std::string;
using std::ostringstream;
using std::floor;
using std::max;

using Control::AntLOControllerImpl;
using ControlExceptions::CAMBErrorEx;
using ControlExceptions::InvalidRequestEx;
using ControlExceptions::IllegalParameterErrorEx;
using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::IllegalParameterErrorCompletion;
using ControlExceptions::TimeoutExImpl;
using ControlExceptions::TimeoutEx;
using ControlExceptions::TimeoutCompletion;
using ControlExceptions::INACTErrorEx;
using ControlExceptions::INACTErrorCompletion;
using ControlExceptions::HardwareErrorEx;
using ModeControllerExceptions::BadConfigurationExImpl;
using ModeControllerExceptions::HardwareFaultExImpl;
using ModeControllerExceptions::HardwareFaultEx;
using ModeControllerExceptions::HardwareFaultCompletion;
using ModeControllerExceptions::UnallocatedEx;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::UnallocatedCompletion;
using FrontEndExceptions::UnknownFailureEx;
using FrontEndExceptions::NoBandSelectedEx;
using WCAExceptions::LockFailedEx;
using WCAExceptions::LockFailedCompletion;

/* Timeout for the optimization of the SAS */


AntLOControllerImpl::AntLOControllerImpl(const ACE_CString& name,
    maci::ContainerServices* cs):
    AntModeControllerImpl(name, cs)
{
}

AntLOControllerImpl::~AntLOControllerImpl()
{
}

/* ----------------------- ACS Lifecycle Methods -------------------- */

void AntLOControllerImpl::cleanUp() 
{
    abortFrequencyChanges();
    ACE_Write_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    status_m = Control::AntModeController::UNINITIALIZED;
    releaseReferences();
    AntModeControllerImpl::cleanUp();
}

/* ----------------------- AntModeControllerInterface --------------- */
void AntLOControllerImpl::allocate(const char* antennaName) {
    AntModeControllerImpl::allocate(antennaName);

    try {
        fe_m->enableCalDeviceDataReporting(true);
    } catch (const INACTErrorEx &ex) {
        BadConfigurationExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Additional","The Front End reported that"
        "it does not have control over the Calibration device."
        "It could not enable the Data Reporting.");
        nex.log();
    }
}

void AntLOControllerImpl::deallocate() {
    try {
        fe_m->enableCalDeviceDataReporting(false);
    } catch (const INACTErrorEx &ex) {
        //Do nothing.
    }

    AntModeControllerImpl::deallocate();
}


/* --------------------------- CORBA Methods ------------------------- */
void AntLOControllerImpl::queueFrequencies(const Control::AntLOController::SubscanInformationSeq& specs)
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    try {
        const int numSubscans = specs.length();
        // Setup the WCA in the front-end
        {
            Control::FrontEnd::SubscanInformationSeq_var f;
            f = new Control::FrontEnd::SubscanInformationSeq();
            f->length(numSubscans);
            for (int s = 0; s < numSubscans; s++) {
                ACS::Time epoch = specs[s].startTime;
                if (epoch == 0) {
                    epoch = ::getTimeStamp();
                    epoch = TETimeUtil::ceilTE(::getTimeStamp());
                    epoch += 5*TETimeUtil::TE_PERIOD_ACS;
                }
                f[s].startTime = epoch;
                
                f[s].band = specs[s].band;
                f[s].photoRefFreq = specs[s].photoRefFreq;
                f[s].addFLOOG = specs[s].addFLOOG;
                f[s].acdState = specs[s].acdState;
            }
            fe_m->queueFrequencies(f.in());
        }
        // Setup the FLOOG & LO2's and IFproc
        // TODO. 
        // Change FLOOG interface to queue cold multiplier and sideband changes
        // Change FLOOG interface to use booleans for sideband
        // Change FLOOG interface to use photonic reference frequency
        for (int s = 0; s < numSubscans; s++) {
            ACS::Time epoch = specs[s].startTime;
            if (epoch == 0) {
                epoch = ::getTimeStamp();
                epoch = TETimeUtil::ceilTE(::getTimeStamp());
                epoch += 5*480000ULL;
            }
            
            const bool addFLOOG = specs[s].addFLOOG;
            const double floogFreq =  specs[s].floogFreq;
            const int coldMultiplier = specs[s].coldMultiplier;
            double loFreq = specs[s].photoRefFreq;
            if (addFLOOG) {
                floog_m->SetFTSSideband(NetSidebandMod::USB);
                loFreq += floogFreq;
            } else {
                floog_m->SetFTSSideband(NetSidebandMod::LSB);
                loFreq -= floogFreq;
            }
            loFreq *= coldMultiplier;
            floog_m->SetUserFrequency(loFreq, floogFreq, epoch);
            floog_m->SetColdMultiplier(coldMultiplier);

            // Setup all the LO2's
            const bool ABUpperSideBand = specs[s].ABUpperSideBand;
            const bool CDUpperSideBand = specs[s].CDUpperSideBand;
            // TODO. 
            // Change LO2 interface to use booleans for sideband
            NetSidebandMod::NetSideband ABSideband;
            NetSidebandMod::NetSideband CDSideband;
            if (ABUpperSideBand) {
                ABSideband = NetSidebandMod::USB; 
            } else {
                ABSideband = NetSidebandMod::LSB;
            }
            if (CDUpperSideBand) {
                CDSideband = NetSidebandMod::USB; 
            } else {
                CDSideband = NetSidebandMod::LSB;
            }
            lo2BBpr0_m->SetUserFrequency(specs[s].lo2BBpr0Freq, ABSideband, epoch);
            lo2BBpr1_m->SetUserFrequency(specs[s].lo2BBpr1Freq, ABSideband, epoch);
            lo2BBpr2_m->SetUserFrequency(specs[s].lo2BBpr2Freq, CDSideband, epoch);
            lo2BBpr3_m->SetUserFrequency(specs[s].lo2BBpr3Freq, CDSideband, epoch);
            // Setup the IFProc
            const bool use12GHzFilterBBpr0 = specs[s].use12GHzFilterBBpr0;
            const bool use12GHzFilterBBpr1 = specs[s].use12GHzFilterBBpr1;
            const bool use12GHzFilterBBpr2 = specs[s].use12GHzFilterBBpr2;
            const bool use12GHzFilterBBpr3 = specs[s].use12GHzFilterBBpr3;

            ifProc0_m->setSignalPaths(use12GHzFilterBBpr0, use12GHzFilterBBpr1,
                    use12GHzFilterBBpr2, use12GHzFilterBBpr3, ABUpperSideBand,
                    CDUpperSideBand, epoch);

            ifProc1_m->setSignalPaths(use12GHzFilterBBpr0, use12GHzFilterBBpr1,
                    use12GHzFilterBBpr2, use12GHzFilterBBpr3, ABUpperSideBand,
                    CDUpperSideBand, epoch);

          }
    } catch (const CAMBErrorEx& ex) {
        ostringstream msg;
        msg << "Cannot tune the LO's on antenna " << getAntennaName()
            << " due to a communication problem (CAN Bus Error)";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        ostringstream msg;
        msg << "Cannot tune the LO's on antenna " << getAntennaName()
            << " as some LO components are not operational.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const InvalidRequestEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName();
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const WCAExceptions::InsufficientPowerEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName()
            << " as the WCA does not have enough LO power.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const LockFailedEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName()
            << " as the WCA is not locked.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const NoBandSelectedEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName()
            << " as no receiver band has been selected on the front end.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const FrontEndExceptions::BandNotSupportedEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName()
            << " as the specified band on the front-end is not supported.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch(const FrontEndExceptions::BandNotPoweredEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName()
            << " as the specified band is not powered.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch(const FrontEndExceptions::IfSwitchFailureEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName()
            << " as the IF switch is not working.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch(const FrontEndExceptions::LprFailureEx& ex) {
        ostringstream msg;
        msg << "Cannot set the first LO frequency on antenna "
            << getAntennaName()
            << " as the LPR is not working.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const LO2Exceptions::FreqNotAvailableEx& ex) {
        ostringstream msg;
        msg << "Cannot set the second LO frequency on antenna "
            << getAntennaName()
            << " as the requested frequency not available.";
        IllegalParameterErrorExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getIllegalParameterErrorEx();
    }

}

void AntLOControllerImpl::lockFrontEnd(Control::AntennaCallback* cb) {
    const int maxLockTime = 3; // seconds
    const ACS::Time startTime = ::getTimeStamp();
    ACSErr::CompletionImpl c = ACSErrTypeOK::ACSErrOKCompletion();
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    try {
        checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        fe_m->lockFrontEnd();
    } catch (const INACTErrorEx& ex) {
        INACTErrorCompletion hc(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        c = hc;
    } catch (const LockFailedEx& ex) {
        LockFailedCompletion hc(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        c = hc;
    } catch (const UnallocatedEx& ex) {
        UnallocatedCompletion hc(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        c = hc;
    }
    // After 5 seconds there is no guarantee the callback object still exists.
    // Removed this for testing purposes. In theory this should always work
    // Since the client proxy still exists and y the server has gone away,
    // the invocation on the callback should get a system exception.
    const ACS::Time now = ::getTimeStamp();
//    if ((now - startTime) < maxLockTime * TETimeUtil::ACS_ONE_SECOND) {
    try {
        ostringstream msg1;
        msg1<<"Trying to call callback at: "<<TETimeUtil::toTimeDateString(::getTimeStamp());
         LOG_TO_DEVELOPER(LM_DEBUG, msg1.str())

        cb->report(getAntennaName().c_str(), c);
         msg1.str("Call back OK");
        LOG_TO_DEVELOPER(LM_DEBUG, msg1.str());
    } catch(...) {
        ostringstream msg;
        msg << "Could not report that the front-end was locked as it took "
            << TETimeUtil::toSeconds(now-startTime) << " seconds"
            << " and the maximum allowed is " << maxLockTime << " seconds";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str())
    }
}


void AntLOControllerImpl::abortFrequencyChanges() {
    //ntroncos: Note to the future maintainer. Yes this is ugly,
    // but it make its best effor to shutdown stuff in the 
    // different parts. 
    //This is oneway, MUST not throw.
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    try {
        checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    } catch (...) {
      return;
    }
    try {
        fe_m->abortFrequencyChanges();    
    } catch (...) {
    }
    try {
        ifProc0_m->flush(0);
    } catch(...){
    }
    try {
        ifProc1_m->flush(0);
    } catch (...) {
    }
}


ACS::TimeIntervalSeq* AntLOControllerImpl::timeToTune(const Control::AntLOController::SubscanInformationSeq& specs)
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    Control::FrontEnd::SubscanInformationSeq_var f;
    f = new Control::FrontEnd::SubscanInformationSeq();
    f->length(specs.length());
    for (unsigned int s = 0; s <specs.length()  ; s++) {

        f[s].startTime = 0;
        f[s].band = specs[s].band;
        f[s].photoRefFreq = specs[s].photoRefFreq;
        f[s].addFLOOG = specs[s].addFLOOG;
        f[s].acdState = specs[s].acdState;
    }
    return fe_m->timeToTune(f);
}

ACS::TimeIntervalSeq* AntLOControllerImpl::timeToTuneACD(const Control::AntLOController::SubscanInformationSeq& specs)
{
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    Control::FrontEnd::SubscanInformationSeq_var f;
    f = new Control::FrontEnd::SubscanInformationSeq();
    f->length(specs.length());
    for (unsigned int s = 0; s <specs.length()  ; s++) {

        f[s].startTime = 0;
        f[s].band = specs[s].band;
        f[s].photoRefFreq = specs[s].photoRefFreq;
        f[s].addFLOOG = specs[s].addFLOOG;
        f[s].acdState = specs[s].acdState;
    }
    return fe_m->timeToTuneACD(f);
}

void AntLOControllerImpl::setEnableFringeTracking(CORBA::Boolean enable) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    //Set fringe tracking at FLOOG and LO2 devices.
    floog_m->EnableFringeTracking(enable);
    lo2BBpr0_m->EnableFringeTracking(enable);
    lo2BBpr1_m->EnableFringeTracking(enable);
    lo2BBpr2_m->EnableFringeTracking(enable);
    lo2BBpr3_m->EnableFringeTracking(enable);
}

CORBA::Boolean AntLOControllerImpl::getEnableFringeTracking() {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Get the current status of FLOOG and LO2 devices.
    bool floogFT = floog_m->FringeTrackingEnabled();
    bool lo2BBpr0FT = lo2BBpr0_m->FringeTrackingEnabled();
    bool lo2BBpr1FT = lo2BBpr1_m->FringeTrackingEnabled();
    bool lo2BBpr2FT = lo2BBpr2_m->FringeTrackingEnabled();
    bool lo2BBpr3FT = lo2BBpr3_m->FringeTrackingEnabled();

    return (floogFT && lo2BBpr0FT && lo2BBpr1FT && lo2BBpr2FT && lo2BBpr3FT);
}

void AntLOControllerImpl::setEnableDelayTracking(CORBA::Boolean enable) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Set Delay tracking in the DGCK
    dgck_m->enableDelayTracking(enable);
}

CORBA::Boolean AntLOControllerImpl::getEnableDelayTracking() {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Get the current status from the DGCK device.
    return dgck_m->delayTrackingEnabled();
}

void AntLOControllerImpl::setAntennaFrequencyOffsetFactor(CORBA::Long offset){
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Set Offset for FLOOG and LO2 devices
    try {
        floog_m->SetFrequencyOffsetFactor(offset);
        lo2BBpr0_m->SetFrequencyOffsetFactor(offset);
        lo2BBpr1_m->SetFrequencyOffsetFactor(offset);
        lo2BBpr2_m->SetFrequencyOffsetFactor(offset);
        lo2BBpr3_m->SetFrequencyOffsetFactor(offset);
    } catch (const CAMBErrorEx& ex) {
        const string msg("Cannot set frequency offset due to CANBus Error");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        const string msg("Cannot set frequency off due to device is inactive");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }
}

CORBA::Long AntLOControllerImpl::getAntennaFrequencyOffsetFactor() {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    long offset = false;

    try {
        offset = floog_m->GetFrequencyOffsetFactor();
    } catch (const CAMBErrorEx& ex) {
        const string msg("Cannot get frequency offset due to CANBus Error.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        const string
            msg("Cannot get frequency offset due to device is inactive.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }

    return offset;
}

void AntLOControllerImpl::setOffsetToRejectImageSideband(bool rejectImage) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    floog_m->SetOffsetToRejectImageSideband(rejectImage);
}

CORBA::Boolean AntLOControllerImpl::offsetRejectsImageSideband() {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    return floog_m->OffsetRejectsImageSideband();
}


void AntLOControllerImpl::setAntennaFrequencyOffsetMode(
    Control::LOOffsettingMode mode) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Set Offset for FLOOG and LO2 devices
    try {
        floog_m->setFrequencyOffsettingMode(mode);
        lo2BBpr0_m->setFrequencyOffsettingMode(mode);
        lo2BBpr1_m->setFrequencyOffsettingMode(mode);
        lo2BBpr2_m->setFrequencyOffsettingMode(mode);
        lo2BBpr3_m->setFrequencyOffsettingMode(mode);
    } catch(const CAMBErrorEx& ex) {
        ModeControllerExceptions::HardwareFaultExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Cannot set frequency mode due to CANBus "
            "Error");
        nex.log();
        throw nex.getHardwareFaultEx();
    } catch(const INACTErrorEx& ex) {
        ModeControllerExceptions::HardwareFaultExImpl nex(ex, __FILE__,
            __LINE__, __PRETTY_FUNCTION__);
        nex.addData("Detail", "Cannot set frequency off because the device "
            "is inactive");
        nex.log();
        throw nex.getHardwareFaultEx();
    }
}

void AntLOControllerImpl::
enable180DegreeWalshFunctionSwitching(CORBA::Boolean enable) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    floog_m->EnablePhaseSwitching180Deg(enable);
    dtxBBpr0_m->enableWalshFunctionSwitching(enable);
    dtxBBpr1_m->enableWalshFunctionSwitching(enable);
    dtxBBpr2_m->enableWalshFunctionSwitching(enable);
    dtxBBpr3_m->enableWalshFunctionSwitching(enable);
}

void AntLOControllerImpl::setFrequencies(
                    ReceiverBandMod::ReceiverBand band,
                    double      loFreq,
                    double      floogFreq,
                    NetSideband lo1TuneHigh,
                    int         coldMultiplier,
                    double      lo2BBpr0Freq,
                    double      lo2BBpr1Freq,
                    double      lo2BBpr2Freq,
                    double      lo2BBpr3Freq,
                    bool        ABUpperSideBand,
                    bool        CDUpperSideBand,
                    ACS::Time   epoch) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    Control::AntLOController::SubscanInformationSeq_var f;
    f = new Control::AntLOController::SubscanInformationSeq();
    f->length(1);
    f[0].startTime = epoch;
    f[0].band = band;
    f[0].floogFreq = floogFreq;
    f[0].addFLOOG = (lo1TuneHigh==NetSidebandMod::USB) ? true : false;
    f[0].photoRefFreq = loFreq/coldMultiplier - 
        floogFreq * ( f[0].addFLOOG ?  1: -1);
    f[0].coldMultiplier = coldMultiplier;
    f[0].lo2BBpr0Freq = lo2BBpr0Freq;
    f[0].lo2BBpr1Freq = lo2BBpr1Freq;
    f[0].lo2BBpr2Freq = lo2BBpr2Freq;
    f[0].lo2BBpr3Freq = lo2BBpr3Freq;
    f[0].ABUpperSideBand = ABUpperSideBand;
    f[0].CDUpperSideBand = CDUpperSideBand;
    const double threshold = 10.5E9;// Hz
    f[0].use12GHzFilterBBpr0 =  lo2BBpr0Freq > threshold ? true : false;       
    f[0].use12GHzFilterBBpr1 =  lo2BBpr1Freq > threshold ? true : false;       
    f[0].use12GHzFilterBBpr2 =  lo2BBpr2Freq > threshold ? true : false;       
    f[0].use12GHzFilterBBpr3 =  lo2BBpr3Freq > threshold ? true : false;       
    f[0].acdState = CalibrationDeviceMod::NONE;

    try {
        queueFrequencies(f.in());
        fe_m->lockFrontEnd();
    } catch (const INACTErrorEx& ex) {
        const string msg("Cannot set the frequency due to an INACT Error.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const HardwareErrorEx& ex) {
        const string msg("Cannot set the frequency due to a Hardware Error.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const LockFailedEx& ex) {
        const string msg("Cannot set the frequency due to a LockFailure Error.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }
}

void AntLOControllerImpl::setFrequenciesAsynch(
                    ReceiverBandMod::ReceiverBand band,
                    double      loFreq,
                    double      floogFreq,
                    NetSideband lo1TuneHigh,
                    int         coldMultiplier,
                    double      lo2BBpr0Freq,
                    double      lo2BBpr1Freq,
                    double      lo2BBpr2Freq,
                    double      lo2BBpr3Freq,
                    bool        ABUpperSideBand,
                    bool        CDUpperSideBand,
                    ACS::Time   epoch,
                    Control::AntennaCallback* cb) {
    try {
        setFrequencies(band, loFreq, floogFreq, lo1TuneHigh, coldMultiplier,
                       lo2BBpr0Freq, lo2BBpr1Freq, lo2BBpr2Freq, lo2BBpr3Freq,
                       ABUpperSideBand, CDUpperSideBand, epoch);
        cb->report(getAntennaName().c_str(), ACSErrTypeOK::ACSErrOKCompletion());
    } catch (const HardwareFaultEx& ex){
        HardwareFaultCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (const UnallocatedEx& ex) {
        UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (const IllegalParameterErrorEx& ex) {
        IllegalParameterErrorCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    }
}

void AntLOControllerImpl::getFrequencies(
                     CORBA::String_out frequencyBand,
                     double&           floogFreq,
                     NetSideband&      lo1TuneHigh,
                     double&           lo2BBpr0Freq,
                     double&           lo2BBpr1Freq,
                     double&           lo2BBpr2Freq,
                     double&           lo2BBpr3Freq,
                     CORBA::Boolean&   ABUpperSideBand,
                     CORBA::Boolean&   CDUpperSideBand) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Get frequency of the FLOOG and LO2
    try {
        floogFreq    = floog_m->GetNominalFrequency();
        lo2BBpr0Freq = lo2BBpr0_m->GetNominalFrequency();
        lo2BBpr1Freq = lo2BBpr1_m->GetNominalFrequency();
        lo2BBpr2Freq = lo2BBpr2_m->GetNominalFrequency();
        lo2BBpr3Freq = lo2BBpr3_m->GetNominalFrequency();

    } catch (const CAMBErrorEx& ex) {
        const string msg("Cannot get frequency due to CANBus Error.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        const string msg("Cannot get frequency because device is inactive.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }

    // Get the current receiver band
    try {
        ReceiverBandMod::ReceiverBand b = fe_m->getCurrentBand();
        frequencyBand = CORBA::string_dup(CReceiverBand::toString(b).c_str());
    } catch (const INACTErrorEx& ex) {
        string msg("Cannot get the current receiver band");
        msg += " as the front-end device is not operational.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const CAMBErrorEx& ex) {
        ostringstream msg;
        msg << "Cannot get the current receiver band"
            << " as there is a problem communicating with the front-end.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const NoBandSelectedEx& ex) {
        string msg("Cannot get the current receiver band");
        msg += " as no observing band has been selected.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const UnknownFailureEx& ex) {
        string msg("Cannot get the current receiver band");
        msg += " as there is a unknown failure in the front-end.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }

    // Get the current sideband used in the WCA. This is the sideband of the
    // FLOOG with respect to the laser synthesiser and is an enumeration.
    try {
        lo1TuneHigh = fe_m->getSideband();
        ostringstream msg;
        msg << "Get frontend sideband: " << CNetSideband::name(lo1TuneHigh)
            << "(" << hex << static_cast<int>(lo1TuneHigh) << ")";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    } catch (const INACTErrorEx& ex) {
        string msg("Cannot get the current 1st LO sideband");
        msg += " as the front-end device is not operational.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const CAMBErrorEx& ex) {
        ostringstream msg;
        msg << "Cannot get the current 1st LO sideband"
            << " as there is a problem communicating with the front-end.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const NoBandSelectedEx& ex) {
        string msg("Cannot get the current 1st LO sideband");
        msg += " as no observing band has been selected.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const UnknownFailureEx& ex) {
        string msg("Cannot get the current 1st LO sideband");
        msg += " as there is a unknown failure in the front-end.";
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }

    // Get IF Processor sideband select states
    Control::BooleanSeq_var dummy =
        new Control::BooleanSeq(4);
    dummy->length(4);
    getSignalPaths(dummy[0], dummy[1], dummy[2], dummy[3],
                   ABUpperSideBand, CDUpperSideBand);

}

void AntLOControllerImpl::getSignalPaths(CORBA::Boolean& BaseBandAHigh,
                                         CORBA::Boolean& BaseBandBHigh,
                                         CORBA::Boolean& BaseBandCHigh,
                                         CORBA::Boolean& BaseBandDHigh,
                                         CORBA::Boolean& ABUpperSideBand,
                                         CORBA::Boolean& CDUpperSideBand) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    Control::BooleanSeq_var ifProc0SP = new Control::BooleanSeq(6);
    ifProc0SP->length(6);
    Control::BooleanSeq_var ifProc1SP = new Control::BooleanSeq(6);
    ifProc1SP->length(6);

    ACS::Time timeStamp;
    // Retrieve signal path from IFProc0 and IFProc1
    try {
        ifProc0SP = ifProc0_m->GET_SIGNAL_PATHS(timeStamp);
        ifProc1SP = ifProc1_m->GET_SIGNAL_PATHS(timeStamp);
    } catch (const CAMBErrorEx& ex) {
        const string msg("Cannot get signal paths due to CANBus Error ");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        const string msg("Cannot set signal paths due to device is inactive ");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }

    // Compare both signal paths
    for(unsigned int sp = 0; sp < 6; sp++) {
        if (ifProc0SP[sp] != ifProc1SP[sp]) {
            // Not the same signal path for both IFProcs
            const string msg("Diferent signal paths for each polarisation!");
            HardwareFaultExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ex.addData("Detail", msg);
            ex.log();
            throw ex.getHardwareFaultEx();
            return;
        }
    }

    // Signal paths are OK, then return IFProc0 Signal Path
    BaseBandAHigh   = ifProc0SP[0];
    BaseBandBHigh   = ifProc0SP[1];
    BaseBandCHigh   = ifProc0SP[2];
    BaseBandDHigh   = ifProc0SP[3];
    ABUpperSideBand = ifProc0SP[4];
    CDUpperSideBand = ifProc0SP[5];
}

void AntLOControllerImpl::optimizeBBSignalLevels(float bbTargetLevel) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Send the command to IFProc0 and IFProc1
    try {
      ifProc0_m->setPowerLevel(bbTargetLevel);
      ifProc1_m->setPowerLevel(bbTargetLevel);
    } catch (const CAMBErrorEx& ex) {
        const string msg("Cannot set signal levels due to CANBus Error.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        const string msg("Cannot set power levels due to device is inactive.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }
}

void AntLOControllerImpl::
optimizeBBSignalLevelsAsynch(float bbTargetLevel,
                             Control::AntennaCallback* cb) {
    try {
      optimizeBBSignalLevels(bbTargetLevel);
      cb->report(getAntennaName().c_str(),ACSErrTypeOK::ACSErrOKCompletion());
    } catch (const HardwareFaultEx& ex){
      HardwareFaultCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
      cb->report(getAntennaName().c_str(), c);
      return;
    } catch (const UnallocatedEx& ex) {
      UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
      cb->report(getAntennaName().c_str(), c);
      return;
    }
}

void AntLOControllerImpl::optimizeIFSignalLevels(float level) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    try {
        //Maxout attenuation.
        ifs_m->SET_CHANNEL01_ATTENUATION(15);
        ifs_m->SET_CHANNEL02_ATTENUATION(15);
        ifs_m->SET_CHANNEL11_ATTENUATION(15);
        ifs_m->SET_CHANNEL12_ATTENUATION(15);
        
        usleep(static_cast<int>(1000E3));//1.00 secs
        //calculate initial attenuation.
        ACS::Time timeStamp;
        Control::FloatSeq_var power;
        // float power[2];
        double att=0;
        
        //SB 0 pol 0
        power = ifProc0_m->getSBPower(timeStamp);
        att = power[0] - level + 15.5;
        att = floor( max(0.0, att) );
        if (att > 15) {
            att = 15;
        }
        ifs_m->SET_CHANNEL01_ATTENUATION(static_cast<int>(att));
        
        //SB 0 pol 1
        att = power[1] - level + 15.5;
        att = floor( max(0.0, att) );
        if (att > 15) {
            att = 15;
        }
        ifs_m->SET_CHANNEL02_ATTENUATION(static_cast<int>(att));
        
        //SB 1 pol 0
        power = ifProc1_m->getSBPower(timeStamp);
        att = power[0] - level + 15.5;
        att = floor( max(0.0, att) );
        if (att > 15) {
            att = 15;
        }
        ostringstream msg;
        msg << "Att = " << att << " after IF for SET_CHANNEL11_ATTENUATION";
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
        ifs_m->SET_CHANNEL11_ATTENUATION(static_cast<int>(att));
        
        //SB 1 pol 
        att = power[1] - level + 15.5;
        att = floor( max(0.0, att) );
        if (att > 15) {
            att = 15;
        }
        ifs_m->SET_CHANNEL12_ATTENUATION(static_cast<int>(att));
    } catch (const CAMBErrorEx& ex) {
        const string msg("Cannot optimize the IFSwitch attenuation due to CANBus Error.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        const string msg("Cannot optimize the IFSwitch attenuation due to device is inactive.");
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }
}

void AntLOControllerImpl::
optimizeIFSignalLevelsAsynch(float ifTargetLevel,
                             Control::AntennaCallback* cb) {
    try {
        optimizeIFSignalLevels(ifTargetLevel);
        cb->report(getAntennaName().c_str(),ACSErrTypeOK::ACSErrOKCompletion());
    } catch (const HardwareFaultEx& ex){
        HardwareFaultCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (const UnallocatedEx& ex) {
        UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    }
}

void AntLOControllerImpl::optimizeSignalLevels(float ifTargetLevel,
                                               float bbTargetLevel) {
    optimizeIFSignalLevels(ifTargetLevel);
    optimizeBBSignalLevels(bbTargetLevel);
}

void AntLOControllerImpl::optimizeSignalLevelsAsynch(float ifTargetLevel,
                                                     float bbTargetLevel,
                                            Control::AntennaCallback* cb) {
   try {
       optimizeSignalLevels(ifTargetLevel, bbTargetLevel);
       cb->report(getAntennaName().c_str(),ACSErrTypeOK::ACSErrOKCompletion());
    } catch (const HardwareFaultEx& ex){
       HardwareFaultCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
       cb->report(getAntennaName().c_str(), c);
       return;
    } catch (const UnallocatedEx& ex) {
       UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
       cb->report(getAntennaName().c_str(), c);
       return;
   }
}

void AntLOControllerImpl::getReceiverInfo(CORBA::String_out hwSerialNumber,
                                          CORBA::String_out dewarName,
                                          CORBA::Double& dewarTemp) {
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
}

void AntLOControllerImpl::
setCalibrationDevice(CalibrationDeviceMod::CalibrationDevice cd){
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    try {
        fe_m->setCalibrationDevice(cd);
    } catch (const IllegalParameterErrorEx& ex) {
        IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    } catch (const HardwareErrorEx& ex) {
        HardwareFaultExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        HardwareFaultExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareFaultEx();
    } catch (const TimeoutEx& ex) {
        TimeoutExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getTimeoutEx();
    }
}

void AntLOControllerImpl::
setCalibrationDeviceAsynch(CalibrationDeviceMod::CalibrationDevice cd,
                           Control::AntennaCallback* cb) {
    try {
        setCalibrationDevice(cd);
    } catch (const HardwareFaultEx& ex){
        HardwareFaultCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = getAntennaName() + " reporting from set Cal Device";
        LOG_TO_DEVELOPER(LM_DEBUG, msg);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (const UnallocatedEx& ex) {
        UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = getAntennaName() + " reporting from set Cal Device";
        LOG_TO_DEVELOPER(LM_DEBUG, msg);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (const IllegalParameterErrorEx& ex) {
        IllegalParameterErrorCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = getAntennaName() + " reporting from set Cal Device";
        LOG_TO_DEVELOPER(LM_DEBUG, msg);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (const TimeoutEx& ex) {
        TimeoutCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = getAntennaName() + " reporting from set Cal Device";
        LOG_TO_DEVELOPER(LM_DEBUG, msg);
        cb->report(getAntennaName().c_str(), c);
        return;
    }

    string msg = getAntennaName() + " reporting from set Cal Device";
    LOG_TO_DEVELOPER(LM_DEBUG, msg);
    cb->report(getAntennaName().c_str(), ACSErrTypeOK::ACSErrOKCompletion());
}

offline::StateData* AntLOControllerImpl::getCurrentStateData() {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    CalibrationDeviceMod::CalibrationDevice cd;
    try {
        cd = fe_m->getCurrentCalibrationDevice();
    } catch (const IllegalParameterErrorEx& ex) {
        HardwareFaultExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareFaultEx();
    } catch (const HardwareErrorEx& ex) {
        HardwareFaultExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareFaultEx();
    } catch (const INACTErrorEx& ex) {
        HardwareFaultExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareFaultEx();
    }

    offline::StateData_var stateData = new offline::StateData();
    stateData->antennaName = CORBA::string_dup(getAntennaName().c_str());
    stateData->weight = 1.0;
    stateData->calDevice = cd;

    /* This is a simple first approximation,
       we will want to be smarter about this when we start
       doing more sophisticated observing modes
    */
    switch (cd) {
    case CalibrationDeviceMod::NONE:
    case CalibrationDeviceMod::SOLAR_FILTER:
    case CalibrationDeviceMod::QUARTER_WAVE_PLATE:
        stateData->signal    = true;
        stateData->reference = false;
        stateData->onSky     = true;
        break;
    case CalibrationDeviceMod::AMBIENT_LOAD:
    case CalibrationDeviceMod::HOT_LOAD:
        stateData->signal    = false;
        stateData->reference = true;
        stateData->onSky     = false;
        break;
    default:
        /* This should be COLD_LOAD and NOISE_TUBE_LOAD which we do not have
           so we should never get here, throw an exception if we do. */
        HardwareFaultExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg("An unrecognized Cal Device was returned from hardware");
        ex.addData("Error Message", msg);
        throw ex.getHardwareFaultEx();
    }

    return stateData._retn();
}

void AntLOControllerImpl::selectPhotonicReference(const char* photoRef) {
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    try {
        allocatedAntenna_m->selectPhotonicReference(photoRef);
    } catch (const HardwareErrorEx& ex){
        HardwareFaultExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareFaultEx();
    } catch (const InvalidRequestEx& ex) {
        UnallocatedExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getUnallocatedEx();
    } catch (const IllegalParameterErrorEx& ex){
        IllegalParameterErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getIllegalParameterErrorEx();
    }
}

void AntLOControllerImpl::selectPhotonicReferenceAsynch(const char*  photoRef,
                                                        Control::AntennaCallback* cb){
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    try {
        checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    } catch (const UnallocatedEx &ex) {
      UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
      cb->report(getAntennaName().c_str(), c);
      return;
    }
    allocatedAntenna_m->selectPhotonicReferenceAsynch(photoRef, cb);
}

char* AntLOControllerImpl::getSelectedPhotonicReference(){
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    try {
        return allocatedAntenna_m->getSelectedPhotonicReference();
    } catch (const InvalidRequestEx& ex){
        UnallocatedExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getUnallocatedEx();
    } catch (const HardwareErrorEx& ex){
        HardwareFaultExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw nex.getHardwareFaultEx();
    }
}

void AntLOControllerImpl::
optimizeSASPol1Asynch(bool forceCalibration,
                      Control::AntennaCallback* cb){
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    try {
        checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    } catch (const UnallocatedEx &ex) {
      UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
      cb->report(getAntennaName().c_str(), c);
      return;
    }
    allocatedAntenna_m->optimizeSASPol1Asynch(forceCalibration, cb);
}

void AntLOControllerImpl::
optimizeSASPol2Asynch(bool forceCalibration,
                      Control::AntennaCallback* cb){
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    try {
        checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    } catch (const UnallocatedEx &ex) {
      UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
      cb->report(getAntennaName().c_str(), c);
      return;
    }
    allocatedAntenna_m->optimizeSASPol2Asynch(forceCalibration, cb);
}

void AntLOControllerImpl::
optimizeSASPolarizationAsynch(bool forceCalibration,
                              Control::AntennaCallback* cb){
    ACE_Read_Guard< ACE_RW_Mutex > guard(cleanUpMutex_m);
    try {
        checkALOCIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    } catch (const UnallocatedEx &ex) {
      UnallocatedCompletion c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
      cb->report(getAntennaName().c_str(), c);
      return;
    }
    allocatedAntenna_m->optimizeSASPolarizationAsynch(forceCalibration, cb);
}

char* AntLOControllerImpl::getFLOOG() {
    if (!floog_m.isNil()) {
        return floog_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getLO2BBpr0() {
    if (!lo2BBpr0_m.isNil()) {
        return lo2BBpr0_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getLO2BBpr1() {
    if (!lo2BBpr1_m.isNil()) {
        return lo2BBpr1_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getLO2BBpr2() {
    if (!lo2BBpr2_m.isNil()) {
        return lo2BBpr2_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getLO2BBpr3() {
    if (!lo2BBpr3_m.isNil()) {
        return lo2BBpr3_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getIFProcessorP0() {
    if (!ifProc0_m.isNil()) {
        return ifProc0_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getIFProcessorP1() {
    if (!ifProc1_m.isNil()) {
        return ifProc1_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getDGCK() {
    if (!dgck_m.isNil()) {
        return dgck_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getFrontEnd() {
    if (!fe_m.isNil()) {
        return fe_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getIFSwitch() {
    if (!ifs_m.isNil()) {
        return ifs_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getSAS() {
    if (!sas_m.isNil()) {
        return sas_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getDTXBBpr0() {
    if (!dtxBBpr0_m.isNil()) {
        return dtxBBpr0_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getDTXBBpr1() {
    if (!dtxBBpr1_m.isNil()) {
        return dtxBBpr1_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getDTXBBpr2() {
    if (!dtxBBpr2_m.isNil()) {
        return dtxBBpr2_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

char* AntLOControllerImpl::getDTXBBpr3() {
    if (!dtxBBpr3_m.isNil()) {
        return dtxBBpr3_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

// ---------------- C++ Methods -------------
void AntLOControllerImpl::acquireReferences(const string& antennaName) {
    // Remove any old references (in case this function is executed twice).
    // But only do this if necessary.
    if (antennaName != getAntennaName()) releaseReferences();

    // If the antenna is already initialized it means we already have the
    // references for the correct antenna. So there is nothing left to do.
    if (getStatus() != Control::AntModeController::UNINITIALIZED) return;

    // This gets a reference to the antenna component
    AntModeControllerImpl::acquireReferences(antennaName);

    // Get references to the antenna-based LO components
    string currentComponent("FLOOG");
    try {
        CORBA::String_var deviceName;
        //FLOOG
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        floog_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::FLOOG>(deviceName);
        // LO2BBpr0
        currentComponent = "LO2BBpr0";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        lo2BBpr0_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::LO2>(deviceName);
        // LO2BBpr1
        currentComponent = "LO2BBpr1";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        lo2BBpr1_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::LO2>(deviceName);
        // LO2BBpr2
        currentComponent = "LO2BBpr2";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        lo2BBpr2_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::LO2>(deviceName);
        // LO2BBpr3
        currentComponent = "LO2BBpr3";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        lo2BBpr3_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::LO2>(deviceName);
        // IFProc0
        currentComponent = "IFProc0";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        ifProc0_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::IFProc>(deviceName);
        // IFProc1
        currentComponent = "IFProc1";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        ifProc1_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::IFProc>(deviceName);
        // FrontEnd
        currentComponent = "FrontEnd";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        fe_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::FrontEnd>(deviceName);
        // IFSwitch
        currentComponent = "IFSwitch";
        deviceName = fe_m->
            getSubdeviceName(currentComponent.c_str());
        ifs_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::IFSwitch>(deviceName);
        // DGCK
        currentComponent = "DGCK";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        dgck_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::DGCK>(deviceName);
       // SAS
        currentComponent = "SAS";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        sas_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::SAS>(deviceName);
        // DTXBBpr0
        currentComponent = "DTXBBpr0";
        deviceName = allocatedAntenna_m->
                getSubdeviceName(currentComponent.c_str());
        dtxBBpr0_m = getContainerServices()->
                getComponentNonStickySmartPtr<Control::DTX>(deviceName);
        // DTXBBpr1
        currentComponent = "DTXBBpr1";
        deviceName = allocatedAntenna_m->
                getSubdeviceName(currentComponent.c_str());
        dtxBBpr1_m = getContainerServices()->
                getComponentNonStickySmartPtr<Control::DTX>(deviceName);
        // DTXBBpr2
        currentComponent = "DTXBBpr2";
        deviceName = allocatedAntenna_m->
                getSubdeviceName(currentComponent.c_str());
        dtxBBpr2_m = getContainerServices()->
                getComponentNonStickySmartPtr<Control::DTX>(deviceName);
        // DTXBBpr3
        currentComponent = "DTXBBpr3";
        deviceName = allocatedAntenna_m->
                getSubdeviceName(currentComponent.c_str());
        dtxBBpr3_m = getContainerServices()->
                getComponentNonStickySmartPtr<Control::DTX>(deviceName);
    } catch (const IllegalParameterErrorEx ex) {
        releaseReferences();
        string msg = "Could not find a " + currentComponent + " component.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (const maciErrType::CannotGetComponentExImpl& ex) {
        releaseReferences();
        string msg = currentComponent + " reference is null.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__,__LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    }
}

void AntLOControllerImpl::releaseReferences() {
    // Release our references to the Antenna-based LO hardware devices (FLOOG,
    // 4*LO2's, 2*IFProc's & FE). The smart pointer will call releaseComponent
    // if necessary.
    floog_m.release();
    lo2BBpr0_m.release();
    lo2BBpr1_m.release();
    lo2BBpr2_m.release();
    lo2BBpr3_m.release();
    ifProc0_m.release();
    ifProc1_m.release();
    fe_m.release();
    ifs_m.release();
    dgck_m.release();
    sas_m.release();
    dtxBBpr0_m.release();
    dtxBBpr1_m.release();
    dtxBBpr2_m.release();
    dtxBBpr3_m.release();

    // Set the state to uninitialized, the antenna name to an empty string and
    // release the antenna reference.
    AntModeControllerImpl::releaseReferences();
}

void AntLOControllerImpl::
checkALOCIsOK(const char* fileName, const int lineNumber, const char* functionName) {
    if (getStatus() == Control::AntModeController::UNINITIALIZED) {
        const string msg = 
            "Antenna LO controller has not been allocated to an antenna.";
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getUnallocatedEx();
    }

    string msg;
    if (floog_m.isNil()) msg += " FLOOG reference is null.";
    if (lo2BBpr0_m.isNil()) msg += " LO2BBpr0 reference is null.";
    if (lo2BBpr1_m.isNil()) msg += " LO2BBpr1 reference is null.";
    if (lo2BBpr2_m.isNil()) msg += " LO2BBpr2 reference is null.";
    if (lo2BBpr3_m.isNil()) msg += " LO2BBpr3 reference is null.";
    if (ifProc0_m.isNil()) msg += " IFProc0 reference is null.";
    if (ifProc1_m.isNil()) msg += " IFProc1 reference is null.";
    if (fe_m.isNil()) msg += " Front-end reference is null.";
    if (ifs_m.isNil()) msg += " IF switch reference is null.";
    if (dgck_m.isNil()) msg += " DGCK reference is null.";
    if (sas_m.isNil()) msg += " SAS reference is null.";
    if (dtxBBpr0_m.isNil()) msg += " DTXBBpr0 reference is null.";
    if (dtxBBpr1_m.isNil()) msg += " DTXBBpr1 reference is null.";
    if (dtxBBpr2_m.isNil()) msg += " DTXBBpr2 reference is null.";
    if (dtxBBpr3_m.isNil()) msg += " DTXBBpr3 reference is null.";

    if (!msg.empty()) {
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getUnallocatedEx();
    }
}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(AntLOControllerImpl)
