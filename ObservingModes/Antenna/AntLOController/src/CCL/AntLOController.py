#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# who       when      what
# --------  --------  ----------------------------------------------
# rsoto   2007-07-30  created 
#

"""
This module is part of the Control Command Language.
Defines the AntLOController class.
"""

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import ModeControllerExceptions
from PyDataModelEnumeration import PyNetSideband
from PyDataModelEnumeration import PyCalibrationDevice
from CCL.EnumerationHelper import *

class AntLOController:
    '''
    The AntLOController object is used to manipulate local oscillators
    within a single antenna. Its manipulates the FLOOG and Second local
    oscillators of the antenna.
    
    EXAMPLE:
    # Load the necessary defintions
    import CCL.AntLOController
    # create the object
    loc = CCL.AntLOController.AntLOController("DV01")
    # Get the name of FLOOG
    loc.getFLOOG()
    # Enable Fringe Tracking
    loc.setEnableFringeTracking(True);
    # Set a frequency offset
    loc.setAntennaFrecuencyOffset(0.001)
    # destroy the component
    del(loc)
    '''
    def __init__(self, antennaName=None, componentName=None):
        '''
        The constructor creates a AntLOController object.

        The antennaName argument should be a string containing the antenna
        name. This constructor starts a dynamic component that
        implements the functions available in the AntLOController
        object. The constructor also establishes the connection
        between this dynamic component and the relevant hardware
        components  in the specified antenna. An exception
        is thrown if there is a problem creating this component or
        establishing the connection to the previously mentioned
        hardware components.

        If the componentName is not none, then the object uses that component
        to implement the functions available in Antenna Local Oscillator
        Controller.
        It is assumed that the component was allocated by other means and
        references to the relavent hardware already exist.  This method is
        intended for internal use, and should only be used by experts.

        Exceptions are thrown if there is a problem creating or connecting
        to this component, establishing the connection to the previously
        mentioned hardware components, or if either both or neither
        antennaName and componentName are specified.

        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # destroy the component
        del(loc)
        '''

        if not((antennaName == None) ^ (componentName == None)):
            raise;

        self.__client = Acspy.Clients.SimpleClient.PySimpleClient("CCL AntLOController Client")
        self.logger = self.__client.getLogger()
        if componentName != None:
            # Specified previously existing component, just get reference
            self.__lo = self.__client.getComponent(componentName)
            if (self.__lo == None):
                return None
            
        # The container name is specified in the MACI/Components area
        # of the CDB.
        if antennaName != None:
            self.__lo = self.__client.getDynamicComponent \
                        (None, 'IDL:alma/Control/AntLOController:1.0',
                         'AntLOController', None)
            if (self.__lo == None):
                return None
            try:
                self.__lo.allocate(antennaName);
            except (ModeControllerExceptions.CannotGetAntennaEx, \
                    ModeControllerExceptions.BadConfigurationEx), e:
                self.__lo.deallocate();
                raise;


    def __del__(self):
        '''
        The destructor shuts down the component and releases all the
        references it has to the FLOOG, LO2A, LO2B, LO2C, LO2D,
        IFProc0 & IFProc1 components.
     EXAMPLE:
     See the example for the constructor (__init__ function)
     '''
        self.logger.logDebug("Destructing CCL AntLOController object.")
        self.__client.releaseComponent(self.__lo._get_name())
        self.__client.disconnect()

    def setEnableFringeTracking(self, enable=True):
        '''
        This method enable/disable the fringe tracking at FLOOG and
        LO2 devices. By default it should be enable.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the current status of LO Controller component
        LOStatus = loc.getStatus()
        if LOStatus == "ALLOCATED":
           # Enable the fringe tracking.
           loc.setEnableFringeTracking(True);
           # Get the status of fringe tracking
           ftStatus =  loc.getEnableFringeTracking();
        # destroy the object
        del(loc)
        '''
        return self.__lo.setEnableFringeTracking(enable);

    def getEnableFringeTracking(self):
        '''
        This method returns the current status of fringe
        tracking.
        EXAMPLE:
        See the example for the setEnableFringeTracking function.
        '''
        return self.__lo.getEnableFringeTracking();

    def setEnableDelayTracking(self, enable=True):
        '''
        This method enable/disable the delay tracking at FLOOG and
        DGCK devices. By default it should be enabled.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the current status of LO Controller component
        LOStatus = loc.getStatus()
        if LOStatus == "ALLOCATED":
           # Enable the fringe tracking.
           loc.setEnableDelayTracking(True);
           # Get the status of fringe tracking
           ftStatus =  loc.getEnableDelayTracking();
           # destroy the object
           del(loc)
        '''
        return self.__lo.setEnableDelayTracking(enable);

    def getEnableDelayTracking(self):
        '''
        This method returns the current status of delay
        tracking.
        EXAMPLE:
        See the example for the setEnableDelayTracking function.
        '''
        return self.__lo.getEnableDelayTracking();

    def getStatus(self):
        '''
        This method returns the status of the LO Controller
        component.
        EXAMPLE:
        See the example for the setEnableFringeTracking function.
        '''
        return self.__lo.getStatus();

    def setAntennaFrequencyOffsetMode(self, mode):
        '''
        Set the frequency offset mode in all of the LO2s and the FLOOG.
        '''
        return self.__lo.setAntennaFrequencyOffsetMode(mode);

    def setAntennaFrequencyOffset(self, offset=0.0):
        '''
        In high spectral resolution modes the Walsh functions are
        desynchronized by the correlator and we must use a frecuency
        offset in the signal chain to reject spurious signals.
        This method allows the offset to be adjusted.
        EXAMPLE:
        See the example for the setFrequencies function.
        '''
        return self.__lo.setAntennaFrequencyOffset(offset);

    def getAntennaFrequencyOffset(self):
        '''
        This method returns the current offset the for the
        antenna.
        EXAMPLE:
        See the example for the setFrequencies function.
        '''
        return self.__lo.getAntennaFrequencyOffset();

    def setFrequencies(self, loFreq = 106.8375E9, floogFreq = 31.5E6,\
                       lo1TuneHigh = PyNetSideband.USB, \
                       lo2BBpr0Freq = 106E8, lo2BBpr1Freq = 106E8, \
                       lo2BBpr2Freq = 106E8, lo2BBpr3Freq = 106E8, \
                       ABUpperSideBand=True, CDUpperSideBand=True, \
                       epoch = 0):
        '''
        This method allows set the tunning parameter for
        the antenna. It will set the frequency for the
        FLOOG and LO2 devices.
        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        #Set the frequencies of FLOOG and LO2 devices
        loc.setFrequencies(106.8375E9, 31.5E6, USB, 106E8, 106E8,\
        106E8, 106E8, True, True, 0.0)
        #Get the frequencies previously set
        (frequencyBand, flFreq, lo1TuneHigh, lo2BBpr0Freq, lo2BBpr1Freq, \
         lo2BBpr2Freq, lo2BBprFreq, True, True) = loc.getFrequencies()
        #Set a frequency offset for FLOOG and LO2 devices.
        loc.setAntennaFrequencyOffset(6.0E4)
        #Get the frequency offset previously set
        antFreqOffset = loc.getAntennaFrequencyOffset()
        # destroy the object
        del(loc)
        '''
        return self.__lo.setFrequencies(loFreq, floogFreq, lo1TuneHigh,\
                                        lo2BBpr0Freq, lo2BBpr1Freq, \
                                        lo2BBpr2Freq, llo2BBpr3Freq, \
                                        ABUpperSideBand, CDUpperSideBand, \
                                        epoch);
    def getFrequencies(self):
        '''
        This method returns the current tunning
        of the antenna.
        EXAMPLE:
        See the example for the setFrequencies function.
        '''
        return self.__lo.getFrequencies();

    def setSignalPaths(self, BaseBandAHigh = True, BaseBandBHigh = True,\
                       BaseBandCHigh = True, BaseBandDHigh = True, \
                       ABUpperSideBand = True, CDUpperSideBand = True):
        '''
        This method configure the polarizer P0 and polarizer P1 of the
        IFDC. To do different configurations on each polarization use
        the IFProcessor device driver directly.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        #Set the signal paths for P0 and P1 polarizer
        loc.setSignalPaths(True, False, True, True, False, False)
        # destroy the object
        del(loc)
        '''
        return self.__lo.setSignalPaths(BaseBandAHigh, BaseBandBHigh, \
                                        BaseBandCHigh, BaseBandDHigh, \
                                        ABUpperSideBand, CDUpperSideBand);

    def getSignalPaths(self):
        '''
        This method returns the status of the six IFProc switches that
        define the signal paths.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        (BaseBandAHigh, BaseBandBHigh, BaseBandCHigh, BaseBandDHigh, \
         ABUpperSideBand, CDUpperSideBand) = loc.getSignalPaths()
        '''
        return self.__lo.getSignalPaths();
    
    def optimizeBBSignalLevels(self, level = 1.0):
        '''
        This method sets the attenuators in the IFProcessors so that
        the output level is what is specified (in Volts).  The default
        value should be used in most cases.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        loc.optimizeSignalLevels()
        '''
        
        return self.__lo.optimizeSignalLevel(level);

    def optimizeIFSignallevels(self, level = -23.0):
        '''
        This method sets the attenuators in the IFSwitch so that
        the output level is what is specified (in DBM).  The default
        value should be used in most cases.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        loc.optimizeIFSAttenuation()
        '''
        
        return self.__lo.optimizeIFSignalLevels(level);

    def optimizeSignallevels(self, ifTargetLevel = -23.0, bbTargetLevel = 1.0):
        '''
        This method sets the attenuators in the IFSwitch so that
        the output level is what is specified (in DBM).  The default
        value should be used in most cases.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        loc.optimizeIFSAttenuation()
        '''
        
        return self.__lo.optimizeSignalLevels(ifTargetLevel, bbTargetLevel);

    def getReceiverInfo(self):
        '''
        This method returns general information regarding the Receiver.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        (HwSerialNumber, DewarName, DewarTemp) = loc.getReceiverInfo()
        '''
        return self.__lo.getReceiverInfo();

    def setCalibrationDevice(self, calibrationDevice="NONE"):
        '''
        This method will put the specified calibration device in front
        of the currently selected band
        When None is selected the path is cleared and the receiver is looking
        at the sky.
        The device can either be passed in as a string or as an enumeration.
        Supported values are:
           AMBIENT_LOAD
           HOT_LOAD
           NONE

        If no band is selected for the front end this method will return an
        exception.
        '''
        enum = getEnumeration(calibrationDevice, PyCalibarationDevice)
        return self.__lo.setCalibrationDevice(enum)

    def getCurrentStateData(self):
        '''
        This method returns the the information on the current state of
        the reciever.   (This is the current calibration device state)
         
        If the state is not well defined for some reason (i.e. the 
        ACD is commanded to a different band) then an exception is 
        thrown.
        '''
        return self.__lo.getCurrentStateData()

    def setOffsetToRejectImageSideband(self, offsetImage = True):
        '''
        Determines if the LO offsetting is configured to reject
        the image sideband (true) or the signal sideband (false)
        '''
        self.__lo.setOffsetToRejectImageSideband(offsetImage)
        
    def offsetRejectsImageSideband(self):
        '''
        Returns the current state of the LO Offsetting Algorithm, if
        true then the Image sideband will be rejected, otherwise
        the Signal sideband will be rejected.
        '''
        return self.__lo.offsetRejectsImageSideband()

    def getFLOOG(self):
        '''
        Return the FLOOG object used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the FLOOG object
        floog = loc.getFLOOG();
        # destroy the object
        del(loc)
        '''
        componentName = self.__lo.getFLOOG()
        return CCL.FLOOG.FLOGG(None, componentName)
        

    def getLO2BBpr0(self):
        '''
        Return the Second Local Oscillator object corresponding to
        Base Band Pair A used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the LO2BBpr0 object
        lo2BBpr0 = loc.getLO2BBpr0();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getLO2BBpr0()
        return CCL.LO2.LO2(None, componentName)

    def getLO2BBpr1(self):
        '''
        Return the Second Local Oscillator object corresponding to
        Base Band Pair B used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the LO2BBpr1 object
        lo2B = loc.getLO2BBpr1();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getLO2BBpr1()
        return CCL.LO2.LO2(None, componentName)
    

    def getLO2BBpr2(self):
        '''
        Return the Second Local Oscillator object corresponding to
        Base Band Pair C used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the LO2BBpr2 object
        lo2C = loc.getLO2BBpr2();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getLO2BBpr2()
        return CCL.LO2.LO2(None, componentName)

    def getLO2BBpr3(self):
        '''
        Return the Second Local Oscillator object corresponding to
        Base Band Pair D used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the LO2BBpr3 object
        lo2D = loc.getLO2BBpr3();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getLO2BBpr3()
        return CCL.LO2.LO2(None, componentName)

    def getIFProcessorP0(self):
        '''
        Return the IF Processor object associated with the
        polarization 0 used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the IFProc0 object
        ifProc0 = loc.getIFProcessorP0();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getIFProcessorP0()
        return CCL.IFProc.IFProc(None, componentName)
    
    def getIFProcessorP1(self):
        '''
        Return the IF Processor object associated with the
        polarization 1 used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the IFProc1 object
        ifProc1 = loc.getIFProcessorP1();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getIFProcessorP1()
        return CCL.IFProc.IFProc(None, componentName)

    def getDGCK(self):
        '''
        Return the DGCK object used by this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the FLOOG object
        dgck = loc.getDGCK();
        # destroy the object
        del(loc)
        '''
        componentName = self.__lo.getDGCK()
        return CCL.DGCK.DGCK(None, componentName)

    def getFrontEnd(self):
        '''
        Return the front end object associated with this observing mode.
        This will allow you access to many detailed aspects of
        the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the FrontEnd object
        fe = loc.getFrontEnd();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getFrontEnd()
        return CCL.FrontEnd.FrontEnd(None, componentName)
    
    def getSAS(self):
        '''
        Return the sub array switch object associated with this
        observing mode. This will allow you access to many detailed
        aspects of the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. 

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the FrontEnd object
        fe = loc.getFrontEnd();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getSAS()
        return CCL.SAS.SAS(None, componentName)

    def getDTXBBpr0(self):
        '''
        Return the DTS transmitter object corresponding to baseband pair 0.
        This will allow you access to many detailed
        aspects of the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. 

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the DTS transmitter object
        fe = loc.getDTXBBpr0();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getDTXBBpr0()
        return CCL.DTX.DTX(None, None, componentName)
    
    def getDTXBBpr1(self):
        '''
        Return the DTS transmitter object corresponding to baseband pair 1.
        This will allow you access to many detailed
        aspects of the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. 

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the DTS transmitter object
        fe = loc.getDTXBBpr1();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getDTXBBpr1()
        return CCL.DTX.DTX(None, None, componentName)

    def getDTXBBpr2(self):
        '''
        Return the DTS transmitter object corresponding to baseband pair 2.
        This will allow you access to many detailed
        aspects of the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. 

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the DTS transmitter object
        fe = loc.getDTXBBpr2();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getDTXBBpr2()
        return CCL.DTX.DTX(None, None, componentName)

    def getDTXBBpr3(self):
        '''
        Return the DTS transmitter object corresponding to baseband pair 3.
        This will allow you access to many detailed
        aspects of the device. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Local
        Oscillator observations in ways not forseen in this mode
        controller. 

        EXAMPLE:
        # Load the necessary defintions
        import CCL.AntLOController
        # create the object
        loc = CCL.AntLOController.AntLOController("DV01")
        # Get the DTS transmitter object
        fe = loc.getDTXBBpr3();
        # destroy the object
        del(loc)
        '''
        componentName =  self.__lo.getDTXBBpr3()
        return CCL.DTX.DTX(None, None, componentName)

