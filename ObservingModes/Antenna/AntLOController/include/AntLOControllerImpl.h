// $Id$
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//

#ifndef ANTLOCONTROLLERIMPL_H
#define ANTLOCONTROLLERIMPL_H

// Base class(es)
#include <antModeControllerImpl.h>

//CORBA servant header
#include <AntLOControllerS.h>

//includes for data members
#include <acsComponentSmartPtr.h> // for maci::SmartPtr
#include <CNetSideband.h>
#include <acserr.h>
#include <ControlDataInterfacesC.h>

#include <RW_Mutex.h> 

// Forward declarations for classes that this component uses
namespace Control
{
    class IFProc;
    class FLOOG;
    class LO2;
    class FrontEnd;
    class DGCK;
    class SAS;
    class AntennaCallback;
    class IFSwitch;
    class DTX;
}

namespace Control
{
    class AntLOControllerImpl: public virtual POA_Control::AntLOController,
        public Control::AntModeControllerImpl
    {
        public:
        // ------------------- Constructor & Destructor -----------------

        /// The constructor for any ACS C++ component must have this signature.
        AntLOControllerImpl(const ACE_CString& name,
                            maci::ContainerServices* cs);

        /// The destructor ensures that the references to the hardware
        /// components are released.
        virtual ~AntLOControllerImpl();

        // ---------------- Component Lifecycle -------------------------
  
        virtual void cleanUp();

        // ---------------- AntModeController Interface -----------------
        /// See the AntModeController IDL for a description of this
        /// method.  This extends the functionality to turning the
        /// monitoring of the ACD temperatures on
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// \exception ModeControllerExceptions::BadConfigurationEx
        virtual void allocate(const char* antennaName);

        /// See the AntModeController IDL for a description of this
        /// method.  This extends the functionality to turning the
        /// monitoring of the ACD temperatures off
        virtual void deallocate();

        // ------------------- CORBA Interface --------------------------

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::INACTErrorEx 
        /// \exception IllegalParameterErrorEx 
        void queueFrequencies(const Control::AntLOController::SubscanInformationSeq& freqencies);

        /// See the IDL file for a description of this function.
        void lockFrontEnd(Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        void abortFrequencyChanges();

        /// See the IDL file for a description of this function.
        virtual ACS::TimeIntervalSeq* timeToTune(
            const Control::AntLOController::SubscanInformationSeq& freqencies);

        /// See the IDL file for a description of this function.
        ACS::TimeIntervalSeq* timeToTuneACD(
            const Control::AntLOController::SubscanInformationSeq& freqencies);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        virtual void setEnableFringeTracking(CORBA::Boolean enable);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        virtual CORBA::Boolean getEnableFringeTracking();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        virtual void setEnableDelayTracking(CORBA::Boolean enable);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        virtual CORBA::Boolean getEnableDelayTracking();

        /// Set the frequency offset mode in all of the LO2s and the FLOOG.
        /// \exception ModeControllerExceptions::HardwareFaultEx
        void setAntennaFrequencyOffsetMode(Control::LOOffsettingMode mode);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void setAntennaFrequencyOffsetFactor(CORBA::Long offset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual CORBA::Long getAntennaFrequencyOffsetFactor();

        /// See the IDL file for a description of this function.
        virtual void setOffsetToRejectImageSideband(bool);
    
        /// See the IDL file for a description of this function.
        virtual CORBA::Boolean offsetRejectsImageSideband();

        /// See the IDL file for a description of this function.
        virtual void enable180DegreeWalshFunctionSwitching(CORBA::Boolean);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual void setFrequencies(ReceiverBandMod::ReceiverBand band,
                                    double loFreq, double floogFreq,
                                    NetSidebandMod::NetSideband lo1TuneHigh,
                                    int    coldMultiplier,
                                    double lo2BBpr0Freq, double lo2BBpr1Freq,
                                    double lo2BBpr2Freq, double lo2BBpr3Freq,
                                    CORBA::Boolean ABUpperSideBand,
                                    CORBA::Boolean CDUpperSideBand,
                                    ACS::Time epoch);

        /// See the IDL file for a description of this function.
        virtual void setFrequenciesAsynch(ReceiverBandMod::ReceiverBand band,
                                          double loFreq, double floogFreq,
                                          NetSidebandMod::NetSideband lo1TuneHigh,
                                          int    coldMultiplier,
                                          double lo2BBpr0Freq,
                                          double lo2BBpr1Freq,
                                          double lo2BBpr2Freq,
                                          double lo2BBpr3Freq,
                                          CORBA::Boolean ABUpperSideBand,
                                          CORBA::Boolean CDUpperSideBand,
                                          ACS::Time epoch,
                                          Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void getFrequencies(CORBA::String_out frequencyBand,
                                    double& floogFreq,
                                    NetSidebandMod::NetSideband& lo1TuneHigh,
                                    double& lo2BBpr0Freq, double& lo2BBpr1Freq,
                                    double& lo2BBpr2Freq, double& lo2BBpr3Freq,
                                    CORBA::Boolean& ABUpperSideBand,
                                    CORBA::Boolean& CDUpperSideBand);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void getSignalPaths(CORBA::Boolean& BaseBandAHigh,
                                    CORBA::Boolean& BaseBandBHigh,
                                    CORBA::Boolean& BaseBandCHigh,
                                    CORBA::Boolean& BaseBandDHigh,
                                    CORBA::Boolean& ABUpperSideBand,
                                    CORBA::Boolean& CDUpperSideBand);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void optimizeSignalLevels(float ifTargetLevel,
                                          float bbTargetLevel);

        /// See the IDL file for a description of this function.
        virtual void optimizeSignalLevelsAsynch(float ifTargetLevel,
                                                float bbTargetLevel,
						Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void optimizeBBSignalLevels(float level);

        /// See the IDL file for a description of this function.
        virtual void optimizeBBSignalLevelsAsynch(float targetLevel,
						Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void optimizeIFSignalLevels(float level);


        /// See the IDL file for a description of this function.
        virtual void optimizeIFSignalLevelsAsynch(float targetLevel,
                                                Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        virtual void getReceiverInfo(CORBA::String_out hwSerialNumber,
                                     CORBA::String_out dewarName,
                                     CORBA::Double& dewarTemp);

        /// See the IDL file for a description of this function
        virtual void setCalibrationDevice
          (CalibrationDeviceMod::CalibrationDevice cd);

        /// See the IDL file for a description of this function
        virtual void setCalibrationDeviceAsynch
          (CalibrationDeviceMod::CalibrationDevice cd,
           Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function
        virtual offline::StateData* getCurrentStateData();

        /// See the IDL file for a description of this function.
        virtual void selectPhotonicReference(const char* photonicRefId);

        /// See the IDL file for a description of this function.
        virtual void selectPhotonicReferenceAsynch(const char* photonicRefId,
                                                   Control::AntennaCallback*);

        /// See the IDL file for a description of this function.
        virtual char* getSelectedPhotonicReference();

        /// See the IDL file for a description of this function.
        virtual void optimizeSASPolarizationAsynch(bool forceCalibration,
                                                   Control::AntennaCallback*);

        /// See the IDL file for a description of this function.
        virtual void optimizeSASPol1Asynch(bool forceCalibration,
                                           Control::AntennaCallback*);

        /// See the IDL file for a description of this function.
        virtual void optimizeSASPol2Asynch(bool forceCalibration,
                                           Control::AntennaCallback*);

        /// See the IDL file for a description of this function.
        virtual char* getFLOOG();

        /// See the IDL file for a description of this function.
        virtual char* getLO2BBpr0();

        /// See the IDL file for a description of this function.
        virtual char* getLO2BBpr1();

        /// See the IDL file for a description of this function.
        virtual char* getLO2BBpr2();

        /// See the IDL file for a description of this function.
        virtual char* getLO2BBpr3();

        /// See the IDL file for a description of this function.
        virtual char* getIFProcessorP0();

        /// See the IDL file for a description of this function.
        virtual char* getIFProcessorP1();

        /// See the IDL file for a description of this function.
        virtual char* getDGCK();

        /// See the IDL file for a description of this function.
        virtual char* getFrontEnd();

        /// See the IDL file for a description of this function.
        virtual char* getIFSwitch();

        /// See the IDL file for a description of this function.
        virtual char* getSAS();

        /// See the IDL file for a description of this function.
        virtual char* getDTXBBpr0();

        /// See the IDL file for a description of this function.
        virtual char* getDTXBBpr1();

        /// See the IDL file for a description of this function.
        virtual char* getDTXBBpr2();

        /// See the IDL file for a description of this function.
        virtual char* getDTXBBpr3();

        private:

        // ------------------- Mode Controller Methods --------------------
        ///
        /// Configure this component to use the equipment in the specified
        /// antenna. The supplied argument is an antenna name like "DV01" and
        /// the associated antenna component must already be started (by the
        /// master component). This function will then query the antenna
        /// component to get the references to the antenna based LO components
        /// (FLOOG, 4 LO2's and 2 IFProc's). This function can be called twice
        /// and if so it will release the equipment in the antenna it was
        /// previously using and acquire the equipment in the new antenna.
        /// \param antennaName
        /// Name of antenna component to use e.g., "DV01"
        /// \exception ModeControllerExceptions::BadConfigurationEx
        /// A BadConfiguration exception is generated if the antenna does not
        /// have a mount component
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// A CannotGetAntenna exception is generated if the the specified
        /// antenna component cannot be contacted
        virtual void acquireReferences(const std::string& antennaName);

        /// Restore this component to the state it was in after
        /// construction. In this state this component does not have a
        /// reference to any components and none of its functions, except
        /// allocate & getState should be used.
        virtual void releaseReferences();

        /// The copy constructor is made private to prevent a compiler
        /// generated one from being used. It is not implemented.
        AntLOControllerImpl(const AntLOControllerImpl& other);

        /// Methods for interacting (in C++) with the subdevices
        ACSErr::CompletionImpl waitForSASOptimization();

        /// Check that this AntLOContoller can be used
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// If this AntLOContoller has been allocated to an antenna
        void checkALOCIsOK(const char* fileName, int lineNumber, 
                           const char* functionName);

        /// References to the antenna-based LO components
        maci::SmartPtr<Control::FLOOG> floog_m;
        maci::SmartPtr<Control::LO2> lo2BBpr0_m;
        maci::SmartPtr<Control::LO2> lo2BBpr1_m;
        maci::SmartPtr<Control::LO2> lo2BBpr2_m;
        maci::SmartPtr<Control::LO2> lo2BBpr3_m;
        maci::SmartPtr<Control::IFProc> ifProc0_m;
        maci::SmartPtr<Control::IFProc> ifProc1_m;
        maci::SmartPtr<Control::FrontEnd> fe_m;
        maci::SmartPtr<Control::IFSwitch> ifs_m;
        maci::SmartPtr<Control::DGCK> dgck_m;
        maci::SmartPtr<Control::SAS> sas_m;
        maci::SmartPtr<Control::DTX> dtxBBpr0_m;
        maci::SmartPtr<Control::DTX> dtxBBpr1_m;
        maci::SmartPtr<Control::DTX> dtxBBpr2_m;
        maci::SmartPtr<Control::DTX> dtxBBpr3_m;

        const static ACS::Time SAS_OPTIMIZATION_TIMEOUT;

        ACE_RW_Mutex cleanUpMutex_m; 
    };
} // End of Control Namespace
#endif // ANTLOCONTROLLERIMPL_H
