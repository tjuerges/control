#!/usr/bin/env python
# Optical Pointing Test script.

from Acspy.Clients.SimpleClient import PySimpleClient

client = PySimpleClient()
antenna = client.getComponent("CONTROL/Antenna")
optPoint = client.getDynamicComponent("*",
                                      "IDL:alma/Control/OpticalPointing:1.0",
                                      "OpticalPointing",
                                      "CONTROL/DV01/cppContainer")
from Control import DeviceConfig


# Configure the Antenna. Ultimately this will be done by the master componentent

mountConfig = DeviceConfig("Mount",
                           "IDL:alma/Control/MountVA:1.0",
                           "CONTROL/DV01/cppContainer",
                           "MountVA",
                           [],
                           0,
                           "Generic")

mountCtrlConfig = DeviceConfig("MountController",
                               "IDL:alma/Control/MountController:1.0",
                               "CONTROL/DV01/cppContainer",
                               "MountController",
                               [mountConfig],
                               -1,
                               "Generic")

frameGrabberConfig = DeviceConfig("FrameGrabber",
                                  "IDL:alma/Control/FrameGrabber:1.0",
                                  "CONTROL/DV01/cppContainer",
                                  "FrameGrabber",
                                  [],
                                  -1,
                                  "Generic")

mountConfig = DeviceConfig("Mount",
                           "IDL:alma/Control/MountVA:1.0",
                           "CONTROL/DV01/cppContainer",
                           "MountVA",
                           [],
                           -1,
                           "Generic");

mountCtrlConfig = DeviceConfig("MountController",
                               "IDL:alma/Control/MountController:1.0",
                               "CONTROL/DV01/cppContainer",
                               "MountController",
                               [mountConfig],
                               -1,
                               "Generic");

opticalTelescopeConfig = DeviceConfig("PrototypeOpticalTelescope",
                                      "IDL:alma/Control/PrototypeOpticalTelescope:1.0",
                                      "CONTROL/DV01/cppContainer",
                                      "PrototypeOpticalTelescope",
                                      [],
                                      -1,
                                      "Generic")

antennaConfig =  DeviceConfig("CONTROL/Antenna",
                              "IDL:alma/Control/Antenna:1.0",
                              "CONTROL/DV01/cppContainer",
                              "antenna",
                              [mountCtrlConfig, frameGrabberConfig, opticalTelescopeConfig, ],
                              -1,
                              "")

antenna.configure(antennaConfig,"")

# Start a Optical Pointing component & configure it to work on the
# specified antenna
optPoint.configure(antenna._get_name())

# Do the pointing observation here

# This is an example of getting a dark exposure and a source exposure

optPoint.makeSourceExposure(10.0)
optPoint.getSourceExposure()
optPoint.makeDarkExposure(10.0)
optPoint.getDarkExposure()



# close down
client.releaseComponent(optPoint._get_name())
antenna.shutdown()
client.releaseComponent(antenna._get_name())
client.disconnect()








