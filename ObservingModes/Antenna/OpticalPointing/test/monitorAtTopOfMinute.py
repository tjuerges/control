#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This function sends a monitor request to the specified mointor point
at the start of each minute. This monitor request will identify a
particular time that, is the start of the minute (according the the
clock on teh ABM). This mark can be seen on an oscilloscope and
compared with the time on the GPS (the 1 pulse-per-minute
signal). This is used to check timing on the optical pointing
mini-rack.

"""

import time
import acstime
import Acspy.Common.EpochHelper
import Acspy.Common.TimeHelper
import TETimeUtil
import ambManager

mgr = ambManager.AmbManager('DV01')

timeNow = time.localtime()
# truncate the minutes field.
lastMin = (timeNow[0], timeNow[1], timeNow[2], timeNow[3], timeNow[4], \
           0, timeNow[6], timeNow[7], timeNow[8])

tu = Acspy.Common.TimeHelper.TimeUtil()
# ep = Acspy.Common.TimeHelper.getTimeStamp()
ep = tu.py2epoch(time.mktime(lastMin))
# There are 1250 TE's in a minute
ep = TETimeUtil.plusTE(ep, 1250)

while 1:
   print time.asctime(), \
         'Monitor at', Acspy.Common.EpochHelper.EpochHelper(ep).\
         toString(acstime.TSArray, "%H:%M:%S.%3q", 0, 0)
   # This blocks until the monitor request is done. 
   mgr.monitorTE(4, 0x1F, 0x01, ep.value)
   ep = TETimeUtil.plusTE(ep, 1250)
   # sleep for 30'sec in case the user want to interrupt with ^C 
   time.sleep(30)
