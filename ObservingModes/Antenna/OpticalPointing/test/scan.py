#!/usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

from Acspy.Clients.SimpleClient import PySimpleClient

client = PySimpleClient()
from Control import *
antenna = client.getComponent("CONTROL/ALMA001")

from Control import DeviceConfig

# Configure the Antenna

mountConfig =  DeviceConfig("Mount",
                            "IDL:alma/Control/MountVA:1.0",
                            "CONTROL/ABM001/cppContainer",
                            "MountVA",
                            [],
                            0,
                            "Generic")

mountCtrlConfig = DeviceConfig("MountController",
                               "IDL:alma/Control/MountController:1.0",
                               "CONTROL/ABM001/cppContainer1",
                               "MountController",
                               [mountConfig],
                               -1,
                               "Generic")

frameGrabberConfig = DeviceConfig("FrameGrabber",
                                  "IDL:alma/Control/FrameGrabber:1.0",
                                  "CONTROL/Optical/cppContainer",
                                  "FrameGrabber",
                                  [],
                                  -1,
                                  "Generic")

opticalTelescopeConfig = DeviceConfig("PrototypeOpticalTelescope",
                                      "IDL:alma/Control/PrototypeOpticalTelescope:1.0",
                                      "CONTROL/ABM001/cppContainer",
                                      "PrototypeOpticalTelescope",
                                      [],
                                      0x1f,
                                      "Generic")

# I do not include the framegrabber because it does not seem to work.
# TODO; Jeff to look into this
antennaConfig =  DeviceConfig("CONTROL/ALMA001",
                              "IDL:alma/Control/Antenna:1.0",
                              "CONTROL/ABM001/cppContainer",
                              "antenna",
                              [mountCtrlConfig, opticalTelescopeConfig],
                              -1,
                              "")

antenna.configure(antennaConfig,"")


# Start a Optical Pointing component & configure it to work on the
# specified antenna

# This is commented out because it also does not work. I never see the
# component being started. TODO: Jeff to look into this.
# optPoint = client.getDynamicComponent("*",
#                                       "IDL:alma/Control/OpticalPointing:1.0",
#                                       "OpticalPointing",
#                                       "CONTROL/ACC/cppContainer")

# optPoint.configure(antenna._get_name())


from ACS import acsTRUE, acsFALSE
from math import cos, pi
from time import sleep

# Set the antenna poitning model. The numbers used are teh last known good optical pointing model.
mc = client.getComponent("CONTROL/ALMA001/MountController/Generic")
mc.standby()
pm = PointingModelParameters(1, 0, -258.1, 450.1, -1.42, -4.26, -28.87, 28.09, 0, 0.77, 0, 0, 0, 0.44, 0.45, 0.9, -14.42, 191.7, -29.6, 15.5) 
mc.setPointingModelCoefficients(pm)
mc.enablePointingModel(acsTRUE)
mc.track()

# Move the telescope to the top of the Holography tower. This is to check that the optical telescope is working.
mc.sourceHorizon(2.583957775, 0.137182763333)

# While its slewing lets turn on the optical telescope
ot = client.getComponent("CONTROL/ALMA001/PrototypeOpticalTelescope/Node0x01f/Generic")
ot.initializeTelescope();
sleep(30)

#Define a function that does a raster scan around a central point.
def searchForStar(centralRA = 0.0, centralDec = 0.0, searchWidth=1.0, stepSize=5.0/60.0, dwellTime = 5.0   ):
    # Assume:
    # 0. The telscope is in track mode
    # 1. Telescope is near the field of view
    # 2. The optical telescope is on and focussed.
    # All angles are in degrees and times are in seconds
    nSteps = int(searchWidth/stepSize);
    if (2 * int(nSteps/2) != nSteps):
        nSteps -= 1;
    nSteps /= 2;
    print 'Doing', (nSteps * 2 + 1), \
    'steps on each side. Estimated completion time is', \
    (nSteps * 2 + 1)**2*dwellTime/60.0, 'minutes'
  
    # Convert to radians
    centralRA = centralRA * pi/ 180.0
    centralDec = centralDec * pi/ 180.0
    stepSize = stepSize * pi/ 180.0
  
    for idec in range(-nSteps, nSteps):
        dec = idec * stepSize + centralDec;
        raStepSize = stepSize/ cos(dec)
        for ira in range (-nSteps , nSteps ):
            ra = ira * raStepSize + centralRA
            print 'index (ra, dec)', ira, idec, 'RA, Dec (deg)', ra*180./pi, dec*180./pi
             mc.sourceEquatorial(ra, dec, 2000, 0, 0, 0, 0,
                                 Mean, NearestAz, UnderTheTop)
            sleep(dwellTime)

# set the position of the star and move the telescope to it.
ra = 0.78 * 180.0/pi; dec = 1.5598 * 180.0/pi # Polaris
#ra = 4.740 * 180.0/pi; dec = 1.5122 * 180.0/pi # delta UMi
#ra = 4.821 * 180.0/pi; dec = -0.6001 * 180.0/pi  # epsilon Sag
#ra = 6.0143 * 180.0/pi; dec = -0.5184 * 180.0/pi  #Fomalhaut
#ra = 4.8735 * 180.0/pi; dec = 0.6769 * 180.0/pi   #Vega
mc.sourceEquatorial(ra, dec, 2000, 0, 0, 0, 0, Mean, NearestAz, UnderTheTop)
sleep(30)

# Now start looking for the star. As the above position for Polaris is
# good I'll narrow the search area to a smaller value.
searchForStar(ra, dec, 0.5)

# This shoudl shut things down
#antenna.shutdown()
#client.releaseComponent(antenna._get_name())
#client.disconnect()
