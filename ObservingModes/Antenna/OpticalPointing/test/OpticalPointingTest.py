#!/usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2006 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import sys
import unittest
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import Acspy.Clients.SimpleClient
import Control
import CCL.OpticalPointing
import ModeControllerExceptions
import ControlExceptions
import Acspy.Common.ErrorTrace
import Acspy.Common.Log
import maci
from math import radians

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):

    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        op = client.getDynamicComponent \
             (None, \
              'IDL:alma/Control/OpticalPointing:1.0',
              'OpticalPointing', 'CONTROL/DA41/cppContainer')
        client.releaseComponent(op._get_name())

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        try:
            op = None;
            try:
                antName = master.getAvailableAntennas()[0]
                antennaComponentName = 'CONTROL/' + antName;
                compName = 'CONTROL/Array001/' + antName;
# The library name is specified in the CDB in Components.xml
                compSpec = maci.ComponentSpec\
                           (compName,
                            'IDL:alma/Control/OpticalPointing:1.0', '*', '*')
                op = client.\
                     getCollocatedComp(compSpec, False, antennaComponentName);
                self.failUnless(op != None,
                                "Unable to create a OpticalPointing component")
                op.allocate(antName)
                componentName = op.getOpticalTelescope();
                self.assertEqual(componentName,
                                 'CONTROL/' + antName + '/OpticalTelescope');
                componentName = op.getMountController();
                self.assertEqual(componentName,
                                 'CONTROL/' + antName + '/MountController');
                op.initializeHardware();
                op.setTolerance(radians(1./60));
                op.setDirection(radians(-10.0), radians(-89.5), 0.0, 0.0, 0.0);
                data = op.doExposure(5.0);
                data = op.doSourceExposure(3.0, radians(0.0), radians(-89.0), \
                                           0.0, 0.0, 0.0)
                self.assertEqual(data.antennaName, antName);
#             op.writeSourceFITS("source.fits")
                op.shutdownHardware();
            finally:
                if (op != None):
                    client.releaseComponent(op._get_name())
        except (ControlExceptions.HardwareErrorEx,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise
            
    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        try:
             op = None;
             antName = master.getAvailableAntennas()[0]
             op = CCL.OpticalPointing.OpticalPointing(antName);
             op.initializeHardware();

             op.setTolerance(radians(1./60));
             op.getMountController().setDirection("0:0:0", "-88.0.0");
             data = op.doExposure(5.0);
             data = op.doSourceExposure(3.0, radians(170), radians(-88));
             self.assertEqual(data.antennaName, antName);
#             op.writeSourceFITS("source.fits")
             op.getOpticalTelescope().dayTimeObserving()
#             self.failUnless(op.getOpticalTelescope().IRFilterUsed() == True, \
#                             "IR Filter should be used");
             op.getMountController().stop();
             op.shutdownHardware();
        except (ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':

# Bring the control subsystem operational
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    master = client.getComponent('CONTROL/MASTER')
    shutdownMasterAtEnd = False;
    if (master.getMasterState() == Control.INACCESSIBLE):
        master.startupPass1()
        master.startupPass2()
        shutdownMasterAtEnd = True;
    if (master.getMasterState() != Control.OPERATIONAL):
        print 'Could not bring the control subsystem to an operational state'
        exit(1)

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

# Shutdown the control system if we started it up
    if shutdownMasterAtEnd:
        master.shutdownPass1()
        master.shutdownPass2()
    client.releaseComponent(master._get_name()) 
