#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This utility will dump teh azimuth and elevation of the star
"""
import math
import time
import CCL.OpticalPointing
import ModeControllerExceptions
import acstime
import Acspy.Common.EpochHelper
import sys

from optparse import OptionParser
parser = OptionParser()

parser.add_option("--ra", type="float", dest="ra",
                  help="The right-ascension, in decimal degrees, of the star. If not specified the current antenna position is used.")

parser.add_option("--dec", type="float", dest="dec",
                  help="The declination, in decimal degrees, of the star. If not specified the current antenna position is used.")

parser.add_option("--antenna", "-a", dest="antennaName",  metavar='antenna',
                  help="The antenna to use e.g.DV01. No default")

parser.add_option("--time", "-t", default=1, type="float", dest="exposureTime",
                  help="The length of time for each exposure. Default: %default seconds.")

(options, args) = parser.parse_args()

def opticalPositionDump(antennaName, ra, dec, exposureTime):
    '''
    Searches for a star by offsetting the mount from its current
    position along a sprial pattern.

    TODO. Add more of a description.

    EXAMPLE:
    '''
    op = CCL.OpticalPointing.OpticalPointing(antennaName)
    op.initializeHardware();
    op._OpticalPointing__op.setSignalToNoiseThreshold(4)
    op.setTolerance(math.radians(15./60/60))
    if (ra != None and dec != None):
        op.setDirection(math.radians(ra), math.radians(dec))
    keepGoing = True
    print "             Hit ^C to stop."
    print "     Time       Azimuth    Elevation (degrees)"
    while (keepGoing):
        try:
            data = op.doExposure(exposureTime)
            print "%s %12.6f %10.6f"%( 
                Acspy.Common.EpochHelper.EpochHelper((data.endTime + data.startTime)/2).toString(acstime.TSArray, "%H:%M:%S.%3q", 0, 0),
                math.degrees(data.sourceAZ.value - data.offsetAZ.value),
                math.degrees(data.sourceEL.value - data.offsetEL.value))
            sys.stdout.flush()
        except (ModeControllerExceptions.BadDataEx), e:
            print ("Skipping this exposure as its parameters could not be determined")
        except (ModeControllerExceptions.MountFaultEx), e:
            print ("Skipping this exposure as there was a problem getting data from the mount.")
        except (ModeControllerExceptions.HardwareFaultEx), e:
            print("Skipping this star as the was a problem with the optical telescope or the framegrabber. Lets hope its intermittant.")
        except KeyboardInterrupt:
            keepGoing = False;
    del op

if (options.antennaName == None):
    print 'You must specify which antenna to use e.g., ALMA04'

if (options.ra != options.dec and (options.ra == None or options.dec == None)):
    print 'You must specify an right-ascension *and* a declination (or neither)'

opticalPositionDump(options.antennaName, options.ra, options.dec, \
                    options.exposureTime)

