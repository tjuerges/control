// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2006 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "OpticalPointingImpl.h"

#include <AntennaC.h>
#include <MountControllerC.h>
#include <OpticalTelescopeC.h>
#include <ControlExceptions.h>
#include <ModeControllerExceptions.h>
#include <ControlDeviceExceptions.h>
#include <maciErrType.h>
#include <PositionStreamConsumer.h> // for PositionConsumer
#include <LogToAudience.h> // for PositionConsumer
#include <TETimeUtil.h> // for TETimeUtil
#include <slalib.h> // for sla* functions
#include <loggingMACROS.h> // for AUTO_TRACE
#include <math.h> // for fabs and sqrt and M_PI
#include <iostream>
#include <iomanip> // for std::setprecision and std::fixed
#include <sstream> // for ostringstream
#include <vector>
#include <string> // for std::string
#include <unistd.h> // for usleep
#include <MountImpl.h> // for MountImpl::printAzEl
#include <fitsio.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using std::string;
using std::ostringstream;
using std::endl;
using std::vector;
using std::abs;
using std::sin;
using std::cos;
using Control::OpticalPointingImpl;
using acstime::TSArray;
using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::IllegalParameterErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::BadConfigurationExImpl;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::MountFaultEx;
using ModeControllerExceptions::MountFaultExImpl;
using Control::MountStatusData;

const double defaultOptFreq = 545E12;

//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------

OpticalPointingImpl::OpticalPointingImpl(const ACE_CString& name,
                                         maci::ContainerServices* cs) :
    Control::AntModeControllerImpl(name, cs),
    positionConsumer_m(0),
    magZeroExpTime_m(2.5), 
    snrThreshold_m(10.0), 
    rotation_m(0.0),
    xScale_m(1.0),
    yScale_m(1.0),
    xSize_m(1024),
    ySize_m(1024),
    azPos_m(100.0),
    elPos_m(100.0)
{
    AUTO_TRACE(__func__);
}

OpticalPointingImpl::~OpticalPointingImpl() {
    AUTO_TRACE(__func__);
    releaseReferences();
}

//-----------------------------------------------------------------------------
// Mode Controller Methods
//-----------------------------------------------------------------------------
void OpticalPointingImpl::acquireReferences(const string& antennaName) {
    AUTO_TRACE(__func__);
  
    // Remove any old references (in case this function is executed twice).
    // But only do this if necessary.
    if (antennaName != getAntennaName()) releaseReferences();

    // If the antenna is already initialized it means we already have the
    // references for the correct antenna. So there is nothing left to do.
    if (getStatus() != Control::AntModeController::UNINITIALIZED) return;
    
    {
        const string msg = "Configuring antenna " + antennaName + 
            " for optical pointing.";
        LOG_TO_OPERATOR(LM_INFO, msg);
    }

    // This gets a reference to the antenna component, sets the antenna name
    // and state.
    AntModeControllerImpl::acquireReferences(antennaName);

    // Get a reference to the MountController component (that may start the
    // component).
    try {
        // Store these strings in String_var objects to ensure the char* gets deleted.
        const CORBA::String_var antennaCompName = allocatedAntenna_m->name();
        const CORBA::String_var mountControllerCompName = 
            allocatedAntenna_m->getMountControllerName();
        maci::ComponentSpec mountSpec;
        mountSpec.component_name = mountControllerCompName;
        mountSpec.component_type = "IDL:alma/Control/MountController:1.0";
        mountSpec.component_code = "*";
        mountSpec.container_name = "*";
        mountController_m = getContainerServices()->
            getCollocatedComponentSmartPtr<Control::MountController>
            (mountSpec, false, antennaCompName);
        mountController_m->allocate(antennaName.c_str());
    } catch (ControlExceptions::INACTErrorEx& ex) {
        // We can end up here if the antenna name is wrong because ACS will
        // start an antenna component on the fly. However it will be
        // uninitialized.
        ostringstream message;
        message << "The antenna component for antenna called '" 
                << antennaName << "' is not initialized." << endl
                << "Possible causes are" << endl
                << "1. Is the antenna name is incorrect " << endl
                << "2. The ALMA software is not operational.";
        ModeControllerExceptions::CannotGetAntennaExImpl 
            newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", message.str());
        newEx.log();
        throw newEx.getCannotGetAntennaEx();
    } catch (maciErrType::maciErrTypeExImpl& ex) {
        // By catching the base exception I do not need to explicitly catch the
        // NoPermissionExImpl, IncompleteComponentSpecExImpl,
        // InvalidComponentSpecExImp,
        // ComponentSpecIncompatibleWithActiveComponentExImpl,
        // maciErrType::CannotGetComponentExImpl exceptions. It would be nice
        // if CORBA exceptions also had a base class.
        ostringstream message;
        message << "Cannot get a create a mount controller component for the '" 
                << antennaName << "' antenna." << endl
                << "Possible causes are" << endl
                << "1. Is the antenna name is incorrect " << endl
                << "2. The ALMA software is not operational.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", message.str());
        newEx.log();
        throw newEx.getBadConfigurationEx();
    }
    
    // Get a reference to the optical telescope
    CORBA::String_var deviceName;
    try {
        deviceName = allocatedAntenna_m->getSubdeviceName("OpticalTelescope");
        opticalTelescope_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::OpticalTelescope>(deviceName);
    } catch (IllegalParameterErrorEx& ex) {
        string errorMsg = "The configuration for antenna ";
        errorMsg += antennaName;
        errorMsg += " does not contain an optical telescope.";
        errorMsg += " Update the telescope monitor & control data base (TMCDB)";
        errorMsg += " or use a different antenna.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", errorMsg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        string errorMsg = 
            "Cannot get a reference to the optical telescope component.";
        errorMsg += " The component name was ";
        errorMsg += deviceName;
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", errorMsg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    }

    // Initialise the position consumer but do not let it start accumulating
    // data yet.
    positionConsumer_m = new PositionStreamConsumer(antennaName); 
    positionConsumer_m->setTimeRange(0, 0);
}

void OpticalPointingImpl::releaseReferences() {
    AUTO_TRACE(__func__);

    // Turn on the focus model and focusPointingModel (they were turned off
    // earlier).
    if (!mountController_m.isNil()) {
        CORBA::String_var mountName(mountController_m->getMount());
        maci::SmartPtr<Control::Mount> mount = getContainerServices()->
            getComponentNonStickySmartPtr<Control::Mount>(mountName.in());
        mount->enableFocusPointingModel(true);
        mount->enableFocusModel(true);
    }

    // Deactivate the PositionStreamConsumer
    // Note that it is not deleted. This will be done by the ORB (I hope).
    if (positionConsumer_m != 0) {
        positionConsumer_m->disconnect();
        positionConsumer_m = 0;
    }

    // release the optical telescope
    opticalTelescope_m.release();

    // release mount controller
    mountController_m.release();

    // The derived class must call the base class releaseReferences method to
    // change state and unset the antenna name.
    AntModeControllerImpl::releaseReferences();
}

//-----------------------------------------------------------------------------
// CORBA interface
//-----------------------------------------------------------------------------
void OpticalPointingImpl::initializeHardware() {
    AUTO_TRACE(__func__);
  
    try {
        // Initialize the optical telescope. This turns the power on and, if
        // necessary, initializes the focus & filters. It also does a bias and
        // blank exposure and then opens the shutter.
        opticalTelescope_m->initializeTelescope();

        // Get scaling parameters from the telescope.
        double xSizeInRad, ySizeInRad;
        opticalTelescope_m->fieldOfView(xSizeInRad, ySizeInRad, rotation_m); 
        opticalTelescope_m->detectorSize(xSize_m, ySize_m); 
        xScale_m = xSizeInRad/xSize_m;
        yScale_m = ySizeInRad/ySize_m;

	// Turn off the focus model and focusPointingModel. The subreflecuor is
	// not used for optical pointing so there should not be any pointing
	// corrections to compensate for its motion. All automatic subreflector
	// movement is disabled so that optical pointing will not be affected
	// by any problems moving the subreflector.
	CORBA::String_var mountName(mountController_m->getMount());
	maci::SmartPtr<Control::Mount> mount = getContainerServices()->
            getComponentNonStickySmartPtr<Control::Mount>(mountName.in());
	mount->enableFocusPointingModel(false);
	mount->enableFocusModel(false);

        // Set the refraction frequency to the correct frequency for the
        // optical work.
        const double f = opticalTelescope_m->observingFrequency();
        mountController_m->setObservingFrequency(f);

        // Put the mount in track mode (via standby mode).
        mountController_m->track();

        // Turn on the high rate position stream
        mountController_m->enableMountDataPublication();

    } catch (MountFaultEx& ex) {
        string msg = "Cannot initialize the optical pointing system";
        msg += " as there is a problem with the mount.";
        HardwareErrorExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareErrorEx();
    } catch (IllegalParameterErrorEx& ex) {
        string msg = "Cannot initialize the optical pointing system";
        msg += " as there is an coding or configuration error.";
        HardwareErrorExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareErrorEx();
    } catch (CORBA::SystemException& ex) {
        string msg = "Cannot initialize the optical pointing system.";
        msg += " Perhaps the container on the relevant antenna has crashed.";
        HardwareErrorExImpl newEx(__FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareErrorEx();
    }
}

void OpticalPointingImpl::shutdownHardware() {
    AUTO_TRACE(__func__);

    try {
        // Stop the antenna moving.
        mountController_m->stop(); 
        // Turn off the high rate position stream
        mountController_m->disableMountDataPublication();

    } catch (MountFaultEx& ex) {
        string msg = "There was a problem stopping the antenna mount.";
        msg += " Carrying on regardless.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
    }

    // Shutdown the optical telescope (close shutter etc.)
    opticalTelescope_m->shutdownTelescope();
}

void OpticalPointingImpl::doDarkExposure(double exposureTime) {
    AUTO_TRACE(__func__);

    if (status_m == Control::AntModeController::UNINITIALIZED) {
        UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
        ex.log();
        throw ex.getUnallocatedEx();
    } 
  
    {
        ostringstream msg;
        msg << "Taking a dark exposure of " << std::fixed
            << std::setprecision(1) << exposureTime << " seconds.";
        getLogger()->log(Logging::BaseLog::LM_INFO, msg.str(),
                         __FILE__, __LINE__, __func__);
    }

//     try { // close the aperture 
//         opticalTelescope_m->closeAperture();
//     } catch (PrototypeOpticalTelescopeExceptions::HardwareFaultEx& ex) {
//         // Translate the PrototypeOpticalTelescope exception to an OpticalPointing one
//         ModeControllerExceptions::HardwareFaultExImpl 
//             newEx(ex, __FILE__,__LINE__, __func__);
//         newEx.log();
//         throw newEx.getHardwareFaultEx();
//     }

//     try {
//         // Request the frame grabber to take an exposure (dark)
//         frameGrabber_m->doDarkExposure(exposureTime);
//     } catch (ControlDeviceExceptions::HwLifecycleEx& ex) {
//         // Translate the HwLifecycleEx exception to an HardwareFaultEx
//         ModeControllerExceptions::HardwareFaultExImpl 
//             newEx(ex, __FILE__,__LINE__, __func__);
//         newEx.log();
//         throw newEx.getHardwareFaultEx();
//     } catch (FrameGrabberExceptions::InvalidExposureEx& ex) {
//         // Transform into ModeControllerExceptions::BadDataEx for backward
//         // compatibilty
//         ModeControllerExceptions::BadDataExImpl
//             newEx(ex, __FILE__, __LINE__, __func__);
//         newEx.log();
//         throw newEx.getBadDataEx();
//     }
  
    // The ControlExceptions::DeviceBusyEx exception is not caught as it does
    // not need to be translated.
}

Control::SubScanOpticalPointingData* 
OpticalPointingImpl::doSourceExposure(double exposureTime, 
				      double ra, double dec,
				      double pmRA, double pmDec,
				      double parallax) {
    AUTO_TRACE(__func__);

    if (status_m == Control::AntModeController::UNINITIALIZED) {
        UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
        ex.log();
        throw ex.getUnallocatedEx();
    }

     try {
         setDirection(ra, dec, pmRA, pmDec, parallax);
         // open the aperture as it may have been closed after a dark exposure.
         //         opticalTelescope_m->openAperture();
         return doExposure(exposureTime);
     } catch (ControlExceptions::HardwareErrorEx& ex) {
         // Translate the PrototypeOpticalTelescope exception to an OpticalPointing one
         ModeControllerExceptions::HardwareFaultExImpl 
             newEx(ex, __FILE__,__LINE__, __func__);
         newEx.log();
         throw newEx.getHardwareFaultEx();
     }
}

Control::SubScanOpticalPointingData* 
OpticalPointingImpl::doExposure(double exposureTime) {
    AUTO_TRACE(__func__);

    if (status_m == Control::AntModeController::UNINITIALIZED) {
        UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
        ex.log();
        throw ex.getUnallocatedEx();
    }

    if (exposureTime < 0.0 || exposureTime > 5*60) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "exposureTime");
        ex.addData("Value", exposureTime);
        ex.addData("ValidRange", "0.0 to 300.0 seconds");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    {
        ostringstream msg;
        msg << "Taking an exposure of " << std::fixed
            << std::setprecision(3) << exposureTime << " seconds.";
        LOG_TO_OPERATOR(LM_INFO, msg.str());
    }
    // Accummulate positions from now to a few seconds after we expect the
    // exposure to end. This is just to limit the amount of memory consumed by
    // the buffers in the positionConsumer_m object.
    {
        ACS::Time startTime(::getTimeStamp());
        EpochHelper stopTime(startTime);
        // The 5 seconds here is arbitrary
        stopTime.add(DurationHelper(static_cast<long double>(exposureTime+5.0)).value());
        positionConsumer_m->setTimeRange(startTime, stopTime.value().value);
    }

    // Take an exposure and get the centroid
    Control::OpticalTelescope::StarPosition star = 
        opticalTelescope_m->doExposure(exposureTime);

    // Mark the position as bad in case there is an exception between now and
    // when we calculate the updated values.
    azPos_m = elPos_m = 100.0;

    // Now work out the actual start and stop time of the exposure.
    ACS::Time startTime(star.timestamp);
    ACS::Time stopTime;
    { // TODO. Move this into TETimeUtil
        const acstime::Duration expTime = 
            DurationHelper(static_cast<long double>(exposureTime)).value();
        EpochHelper endTime(startTime);
        endTime.add(expTime);
        stopTime = endTime.value().value;
    }

    vector<MountStatusData> positions = 
        positionConsumer_m->getDataRange(startTime, stopTime);
    positionConsumer_m->clear();

    // Check if the position data is perfect and if not bail out.
    MountStatusData avg;
    try {
        PositionStreamConsumer::checkPositions(positions);
        avg = PositionStreamConsumer::average(positions);
        if (!avg.onSource) {
            HardwareErrorExImpl ex(__FILE__, __LINE__, __func__);
            string msg = "Antenna was off-source during the exposure.";
            ex.addData("Detail", msg);
            ex.log();
            throw ex.getHardwareErrorEx();
        }
    } catch (HardwareErrorExImpl& ex) {
        HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
        ostringstream msg;
        msg << "There was a problem with the position data collected between "
            << TETimeUtil::toTimeString(startTime)
            << " and " << TETimeUtil::toTimeString(stopTime) 
            << ". Terminating the analysis of this exposure.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        nex.addData("Detail", msg.str());
        nex.log();
        throw nex.getHardwareErrorEx();
    }

    // Now we have good positions. Svae them in so they can be used in the
    // header of the FITS file.
    azPos_m = avg.azPosition;
    elPos_m = avg.elPosition;

    // Check if the exposure is OK.
    if (star.dataIsBad) { // bail out.
        HardwareErrorExImpl ex(__FILE__, __LINE__, __func__);
        ostringstream msg;
        msg << "The exposure that started at "
            << TETimeUtil::toTimeString(startTime) 
            << " (number " << star.number
            << ") was marked as invalid by the optical telescope."
            << " Check there the is a star in the field of view."
            << " Terminating the analysis of this exposure.";
        LOG_TO_OPERATOR(LM_WARNING, msg.str());
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getHardwareErrorEx();
    }

    // From here on its data analysis
    Control::SubScanOpticalPointingData_var
        returnData(new Control::SubScanOpticalPointingData());

    returnData->antennaName = CORBA::string_dup(getAntennaName().c_str());
    returnData->peakIntensity.value = star.brightness;
    returnData->SNR = star.brightness/star.noise;
    returnData->peakPositionX = star.xPosition;
    returnData->peakPositionY = star.yPosition;
    {
        ostringstream msg;
        msg << "Peak position returned from exposure (" 
            << returnData->peakPositionX << ", " 
            << returnData->peakPositionY << ") pixels." 
            << " SNR = " << returnData->SNR;
        LOG_TO_OPERATOR(LM_INFO, msg.str());
    }

    // Get the position of the telescope at the midpoint of the exposure. This
    // is where the telescope should be pointing if the telescope servo and
    // pointing model are perfect. Hence it should be the commanded position
    // and it should be the (Az,El) before the current pointing model is
    // applied.
    returnData->sourceAZ.value = avg.azCommanded;
    returnData->sourceEL.value = avg.elCommanded;
    {
        ostringstream msg;
        msg << std::setprecision(12) 
            << "For the exposure between " 
            << TETimeUtil::toTimeString(startTime)
            << " and " << TETimeUtil::toTimeString(stopTime)
            << " average commanded position was ";
        MountImpl::printAzEl(msg, avg.azCommanded, avg.elCommanded);
        msg << ", the average actual position was "; 
        MountImpl::printAzEl(msg, avg.azPosition, avg.elPosition);
        msg << " and the average pointing model correction was ";
        MountImpl::printAzEl(msg,  avg.azPointingModelCorrection, 
                             avg.elPointingModelCorrection);
        LOG_TO_OPERATOR(LM_INFO, msg.str());
    }

    // The offset returned should also take into account the offset that occurs
    // because of the current point model.
    if (avg.pointingModel) {
        returnData->pointingModelOffsetAz = avg.azPointingModelCorrection;
        returnData->pointingModelOffsetEl = avg.elPointingModelCorrection;
    } else {
        returnData->pointingModelOffsetAz = 0.0;
        returnData->pointingModelOffsetEl = 0.0;
    }
    if (avg.auxPointingModel) {
        returnData->pointingModelOffsetAz += avg.azAuxPointingModelCorrection;
        returnData->pointingModelOffsetEl += avg.elAuxPointingModelCorrection;
    }
    const double azOffset = star.longOffset/std::cos(avg.elPosition);
    const double elOffset = star.latOffset;
    returnData->CCDOffsetAz = azOffset;
    returnData->CCDOffsetEl = elOffset;
    returnData->offsetAZ.value = 
        avg.azCommanded - avg.azPosition - azOffset +
        returnData->pointingModelOffsetAz;
    returnData->offsetEL.value = 
        avg.elCommanded - avg.elPosition - elOffset +
        returnData->pointingModelOffsetEl;

    returnData->errorAZ.value = star.longWidth;
    returnData->errorEL.value = star.latWidth/std::cos(avg.elPosition);

    // Put the timestamp info into the data structure 
    // For some strange reason I return exposure time in seconds, so convert to
    // ACS Time.
    returnData->startTime = startTime;
    returnData->endTime = stopTime;
    
    // Here we want to get the plate scale of the optical telescope
    // TODO. Fix this up
    returnData->xBeamWidth.value = M_PI/180/6;
    returnData->yBeamWidth.value = M_PI/180/6;

    // Set the antenna name
    returnData->antennaName = getAntennaName().c_str();

    return returnData._retn();
}


// void OpticalPointingImpl::
// startRapidPosition(double exposureTime,
// 		   const char* outputFile) {

//     // Check if the consumer is already running
//     if (pthread_mutex_trylock(&reductionConsumerMtx_m) == 0) {
//         pthread_mutex_unlock(&reductionConsumerMtx_m);
//     } else {
//         OpticalPointingExceptions::LoopRunningExImpl 
//             ex(__FILE__, __LINE__, "OpticalPointingImpl::startRapidPosition");
//         ex.log();
//         throw ex.getLoopRunningEx();
//     }

//     try {
//         // Open the aperture of the optical telescope
//         opticalTelescope_m->openAperture();
//     } catch (PrototypeOpticalTelescopeExceptions::HardwareFaultEx& ex) {
//         status_m = Control::AntModeController::ERROR;
//         ModeControllerExceptions::HardwareFaultExImpl 
//             newEx(ex, __FILE__, __LINE__, __func__);
//         newEx.log();
//         throw newEx.getHardwareFaultEx();
//     }

//     try {
//         // Start the FrameGrabber's Rapid Position Loop
//         frameGrabber_m->startRapidPosition(exposureTime,
//                                            snrThreshold_m);
//     } catch (ControlExceptions::DeviceBusyEx& ex) {
//         OpticalPointingExceptions::FrameGrabberFaultExImpl 
//             newEx(ex, __FILE__, __LINE__, __func__);
//         newEx.log();
//         throw newEx.getFrameGrabberFaultEx(); 
//     } catch (ControlDeviceExceptions::HwLifecycleEx& ex) {
//         OpticalPointingExceptions::FrameGrabberFaultExImpl 
//             newEx(ex, __FILE__, __LINE__, __func__);
//         newEx.log();
//         throw newEx.getFrameGrabberFaultEx();
//     } catch (FrameGrabberExceptions::InvalidExposureEx& ex) {
//         OpticalPointingExceptions::FrameGrabberFaultExImpl 
//             newEx(ex, __FILE__, __LINE__, __func__);
//         newEx.log();
//         throw newEx.getFrameGrabberFaultEx();
//     } catch (...) {
//         OpticalPointingExceptions::FrameGrabberFaultExImpl
//             ex(__FILE__, __LINE__, __func__);
//         ex.log();
//         throw ex.getFrameGrabberFaultEx();
//     }

//     // Set the output file name
//     rapidPositionOutputFile_m = outputFile;
//     if (rapidPositionOutputFile_m != "") {
//         // Open the file and write the predefined header 
//         // (see method processData() below
//         ofstream of(rapidPositionOutputFile_m.c_str());
//         if (of.is_open()) {
//             // Write the file header
//             of << rapidPositionHeader_m;
//             of.close();
//         } else {
//             // Problems opening the file, so ignore it and log a warning
//             rapidPositionOutputFile_m = "";
//             string msg = "Could not open output file for rapid position.";
//             msg += " So it will be ignored.";
//             getLogger()->log(Logging::BaseLog::LM_WARNING, msg,
//                              __FILE__, __LINE__, __func__);
//         }
//     }

//     // Clear the ReductionData queue
//     while (reductionDataQueue_m.size() > 0) {
//         reductionDataQueue_m.pop();
//     }

//     // Set the number of initial events to be discarded
//     reductionDataEventCounter_m = 
//         OpticalPointingImpl::ReductionStreamConsumer::EVENTS_TO_BE_DISCARDED;

//     // Activate the ReductionStreamConsumer
//     if (reductionConsumer_m == NULL) {
//         // Consumer creation seems to start immediately to handle events,
//         // so we lock the mutex right here
//         pthread_mutex_lock(&reductionConsumerMtx_m);
//         // Create an instance of the consumer
//         reductionConsumer_m =
//             new OpticalPointingImpl::ReductionStreamConsumer(*this);
//     } else {
//         try { 
//             // Resume the event handling
//             pthread_mutex_lock(&reductionConsumerMtx_m);
//             reductionConsumer_m->resume();
//         } catch (...) {
//             OpticalPointingExceptions::ConsumerFaultExImpl
//                 ex(__FILE__, __LINE__, __func__);
//             ex.addData("Detail", "Reduction Data Consumer");
//             throw ex.getConsumerFaultEx();
//         }
//     }
// }

// void OpticalPointingImpl::stopRapidPosition() {

//     // Check if the consumer is currently running
//     if (pthread_mutex_trylock(&reductionConsumerMtx_m) == 0) {
//         pthread_mutex_unlock(&reductionConsumerMtx_m);
//         OpticalPointingExceptions::LoopNotRunningExImpl 
//             ex(__FILE__, __LINE__, __func__);
//         ex.log();
//         throw ex.getLoopNotRunningEx();
//     }

//     try {
//         // Stop the FrameGrabber's Rapid Position Loop
//         frameGrabber_m->stopRapidPosition();
//     } catch (ControlExceptions::DeviceBusyEx& ex) {
//         OpticalPointingExceptions::FrameGrabberFaultExImpl 
//             newEx(ex, __FILE__, __LINE__, __func__);
//         newEx.log();
//         throw newEx.getFrameGrabberFaultEx(); 
//     } catch (ControlDeviceExceptions::HwLifecycleEx& ex) {
//         OpticalPointingExceptions::FrameGrabberFaultExImpl 
//             newEx(ex, __FILE__, __LINE__, __func__);
//         newEx.log();
//         throw newEx.getFrameGrabberFaultEx();
//     } catch (...) {
//         OpticalPointingExceptions::FrameGrabberFaultExImpl
//             ex(__FILE__, __LINE__, __func__);
//         ex.log();
//         throw ex.getFrameGrabberFaultEx();
//     }

//     // Check if the ReductionStreamConsumer object exists
//     if (reductionConsumer_m != NULL) {
//         try { 
//             // Suspend the event handling
//             reductionConsumer_m->suspend();
//             pthread_mutex_unlock(&reductionConsumerMtx_m);

//         } catch (...) {
//             OpticalPointingExceptions::ConsumerFaultExImpl
//                 ex(__FILE__, __LINE__, __func__);
//             ex.addData("Detail", "Reduction Data Consumer");
//             throw ex.getConsumerFaultEx();
//         }
//     }
// }

void OpticalPointingImpl::
setDirection(double ra, double dec,
             double pmRA, double pmDec, double parallax) {
    AUTO_TRACE(__func__);
    mountController_m->setDirection(ra, dec, pmRA, pmDec, parallax);
}

void OpticalPointingImpl::setTolerance(double tolerance) {
    AUTO_TRACE(__func__);
    mountController_m->setTolerance(tolerance);
}

double OpticalPointingImpl::getTolerance() {
    AUTO_TRACE(__func__);
    return mountController_m->getTolerance();
}

void OpticalPointingImpl::setTimeout(double timeout) {
    AUTO_TRACE(__func__);
    mountController_m->setTimeout(timeout);
}

double OpticalPointingImpl::getTimeout() {
    AUTO_TRACE(__func__);
    return mountController_m->getTimeout();
}

// double OpticalPointingImpl::calcExposureTime(double stellarMagnitude) {
//   AUTO_TRACE("calcExposureTime");
//   return magZeroExpTime_m * pow(100.0, stellarMagnitude / 5.0);
// }

// double OpticalPointingImpl::zeroMagnitudeExposureTime() {
//   AUTO_TRACE("OpticalPointingImpl::zeroMagnitudeExposureTime");
//   return magZeroExpTime_m;
// }

// void OpticalPointingImpl::setZeroMagnitudeExposureTime(double expTime) {
//   AUTO_TRACE("OpticalPointingImpl::setZeroMagnitudeExposureTime");
//   magZeroExpTime_m = expTime;
// }

void OpticalPointingImpl::
reduceExposure(double&    posX,
	       double&    posY,
	       double&    posErrX,
	       double&    posErrY,
	       double&    SNR,
	       long&      width,
	       long&      height,
	       ACS::Time& timeStamp,
	       double&    exposureTime) {
    AUTO_TRACE(__func__);

//     try {
//         // Invoke the frame grabber reduceExposure
//         Control::ReductionData data = 
//             frameGrabber_m->reduceExposure(snrThreshold_m);
//         // Pass back the information to the method parameters
//         posX         = data.posX;
//         posY         = data.posY;
//         posErrX      = data.posErrX;
//         posErrY      = data.posErrY;
//         SNR          = data.SNR;
//         width        = data.width;
//         height       = data.height;
//         timeStamp    = data.timeStamp;
//         exposureTime = data.exposureTime;
				  
//     } catch (ControlDeviceExceptions::HwLifecycleEx& ex) {
//         // Translate the HwLifecycleEx exception to an HardwareFaultEx
//         ModeControllerExceptions::HardwareFaultExImpl 
//             newEx(ex, __FILE__,__LINE__, __func__);
//         newEx.log();
//         throw newEx;
//     } catch (FrameGrabberExceptions::BadDataEx& ex) {
//         FrameGrabberExceptions::BadDataExImpl
//             ex(__FILE__, __LINE__, __func__);
//         throw ex;
//     } catch (FrameGrabberExceptions::InvalidExposureEx& ex) {
//         FrameGrabberExceptions::InvalidExposureExImpl
//             ex(__FILE__, __LINE__, __func__);
//         throw ex;
//     }  
}

double OpticalPointingImpl::signalToNoiseThreshold() {
    AUTO_TRACE(__func__);
    return snrThreshold_m;
}

void OpticalPointingImpl::setSignalToNoiseThreshold(double threshold) {
    AUTO_TRACE(__func__);
    snrThreshold_m = threshold;
}

char* OpticalPointingImpl::getMountController() {
    AUTO_TRACE(__func__);
    if (!mountController_m.isNil()) {
        return CORBA::String_var(mountController_m->name())._retn();
    } else {
        return CORBA::String_var("")._retn();
    }
}

char* OpticalPointingImpl::getOpticalTelescope() {
    AUTO_TRACE(__func__);
    if (!opticalTelescope_m.isNil()) {
        return CORBA::String_var(opticalTelescope_m->name())._retn();
    } else {
        return CORBA::String_var("")._retn();
    }
}

void OpticalPointingImpl::
transformPosition(const double ccdPositionX,
		  const double ccdPositionY, 
		  const double elevation,
		  const double width,
		  const double height,
		  double& offsetAz,
		  double& offsetEl) 
{
    AUTO_TRACE(__func__);

    // Do the rotation
    offsetAz = 
        ccdPositionX * cos(rotation_m) - ccdPositionY * sin(rotation_m);
    offsetEl = 
        ccdPositionX * sin(rotation_m) + ccdPositionY * cos(rotation_m);
    // and scaling
    offsetAz *= xScale_m / cos(elevation);
    offsetEl *= yScale_m;
}

/* ----------------- Miscellaneous -------------------------- */
void OpticalPointingImpl::saveLastExposure(const char* filename) {
    AUTO_TRACE(__func__);
    // 1. Get the FITS file from teh OPT
    OctetSeq_var image = opticalTelescope_m->getLastImage();

    // 2. Create a fitsio "in-memory" representation of this data
    fitsfile* memPtr;
    string memFilename("");
    int status = 0;
    void* imageMem = image->get_buffer();
    size_t memSize = image->length();
    fits_open_memfile(&memPtr, memFilename.c_str(), READONLY, 
                      &imageMem, &memSize, static_cast<size_t>(2880), NULL, 
                      &status);

    // 3. Copy this "in-memory" FITS file to disk    
    fitsfile* diskPtr;
    fits_create_file(&diskPtr, filename, &status);
    fits_copy_hdu(memPtr, diskPtr, true, &status);
    fits_close_file(memPtr, &status);

    // 4. Add extra keywords, known only at this level, to more completely
    // describe the image.
    {
        string keyName("CRPIX1");
        int keyValue(xSize_m/2);
        string keyComment("Pixel coordinate of reference point");
        fits_write_key(diskPtr, TINT, keyName.c_str(), &keyValue, 
                       keyComment.c_str(), &status);
        keyName = "CRPIX2";
        keyValue = ySize_m/2;
        fits_write_key(diskPtr, TINT, keyName.c_str(), &keyValue,
                       keyComment.c_str(), &status);
    }
    if (abs(azPos_m) < 10.0 && abs(elPos_m) < 10.0) {
        string keyName("CRVAL1");
        double keyValue(azPos_m*180.0/M_PI);
        string keyComment("Az at reference point");
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue, 
                       keyComment.c_str(), &status);
        keyName = "CRVAL2";
        keyValue = elPos_m*180.0/M_PI;
        keyComment = "El at reference point";
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue,
                       keyComment.c_str(), &status);
    }
    {
        string keyName("CDELT1");
        double scaleInDeg = xScale_m*180.0/M_PI;
        double keyValue(scaleInDeg);
        ostringstream keyComment;
        keyComment << scaleInDeg*60*60 << " arcsec per pixel.";
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue,
                       keyComment.str().c_str(), &status);
    }
    {
        string keyName("CDELT2");
        double scaleInDeg = yScale_m*180.0/M_PI;
        double keyValue(scaleInDeg);
        ostringstream keyComment;
        keyComment << scaleInDeg*60*60 << " arcsec per pixel.";
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue,
                       keyComment.str().c_str(), &status);
    }
    {
        string keyName("PC1_1");
        double keyValue = cos(rotation_m);
        ostringstream keyComment;
        keyComment << "Detector is rotated by " << rotation_m*180.0/M_PI 
                   << " degrees.";
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue,
                       keyComment.str().c_str(), &status);
        keyName = "PC2_2";
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue,
                       keyComment.str().c_str(), &status);
        keyValue = sin(rotation_m);
        keyName = "PC1_2";
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue,
                       keyComment.str().c_str(), &status);
        keyValue = - sin(rotation_m);
        keyName = "PC2_1";
        fits_write_key(diskPtr, TDOUBLE, keyName.c_str(), &keyValue,
                       keyComment.str().c_str(), &status);
    }
    {
        string keyName("CUNIT1");
        string keyValue("deg");
        string keyComment("Angles are degrees.");
        fits_write_key(diskPtr, TSTRING, keyName.c_str(), keyValue.c_str(),
                       keyComment.c_str(), &status);
        keyName = "CUNIT2";
        fits_write_key(diskPtr, TSTRING, keyName.c_str(), keyValue.c_str(),
                       keyComment.c_str(), &status);
    }
    
    fits_close_file(diskPtr, &status);
 }

/* --------------- ReductionStreamConsumer ------------------ */

// // Delay time constant definition
// const ACS::Time
// OpticalPointingImpl::ReductionStreamConsumer::REDUCTION_CONSUMER_DELAY = 5000000;
// // Number of initial events to be discarded
// const int
// OpticalPointingImpl::ReductionStreamConsumer::EVENTS_TO_BE_DISCARDED   = 2;

// OpticalPointingImpl::ReductionStreamConsumer::ReductionStreamConsumer(OpticalPointingImpl& opticalPointingDevice) :
//     nc::SimpleConsumer<Control::ReductionData>(Control::CHANNELNAME_CONTROLREALTIME),
//     opticalPointingDevice_p(opticalPointingDevice)
// {
//     addSubscription<Control::ReductionData>
//         (&ReductionStreamConsumer_eventHandler, dynamic_cast<void*>(this));
//     consumerReady();
// }

// OpticalPointingImpl::ReductionStreamConsumer::~ReductionStreamConsumer()
// {
//     disconnect();
// }

//
// Rapid Position Output file header (see processData() below)
//
// const char* OpticalPointingImpl::rapidPositionHeader_m = 
//                        "timeStamp,exposureTime,posX,posY,posErrX,posErrY,SNR,actAz,actEl,skyFrameAz,skyFrameEl\n";

//
// The processData() method implements the needed processing of the
// available position data. It becomes triggered by each ReductionData
// event provided by the FrameGrabber and requests the corresponding
// antenna position to the PositionStream consumer. Afterwards the
// positions are transformed and written to an output file.
//
// A data queue has been incorporated in order to allow to handle
// the delay of the incoming antenna position data. The minimum
// elapsed time is set at the constant REDUCTION_CONSUMER_DELAY.
//
// void 
// OpticalPointingImpl::ReductionStreamConsumer::processData(Control::ReductionData eventData)
// {
//     // We have to work-around a feature of the notification
//     // channel: when a consumer is suspend()ed incoming events
//     // are queued and not discarded. When resume()d all queued
//     // events are processed at once. In our case there might be
//     // some events remaining due to a weak synchronization with
//     // the FrameGrabber supplier, so the best is to discard the
//     // first events as they might not correspond to the current
//     // run. For this we use a countdown counter:
//     if (opticalPointingDevice_p.reductionDataEventCounter_m > 0) {
//         opticalPointingDevice_p.reductionDataEventCounter_m--;
//         return;
//     } 

//     // Add the event to the queue
//     opticalPointingDevice_p.reductionDataQueue_m.push(eventData);

//     // Check if the first event in the queue lays already
//     // REDUCTION_CONSUMER_DELAY [100nsec] behind
//     if (::getTimeStamp() 
//         - opticalPointingDevice_p.reductionDataQueue_m.front().timeStamp
//         > REDUCTION_CONSUMER_DELAY)
//         {
//             // Retrieve a reference to the first element of the queue
//             Control::ReductionData data = 
//                 opticalPointingDevice_p.reductionDataQueue_m.front();

//             ostringstream outputString;

//             // Preparing output string
//             // Format: see OpticalPointingImpl::rapidPositionHeader_m
//             outputString << data.timeStamp << ","
//                          << data.exposureTime << ","
//                          << data.posX << ","
//                          << data.posY << ","
//                          << data.posErrX << ","
//                          << data.posErrY << ","
//                          << data.SNR;

//             // Check if the position consumer instance exists
//             if (opticalPointingDevice_p.positionConsumer_m == NULL) {
//                 string msg = "Position consumer not available.";
//                 msg += " Ignoring antenna position.";
//                 opticalPointingDevice_p.getLogger()->log(Logging::BaseLog::LM_ERROR, 
//                                                          msg, __FILE__, __LINE__, 
//                                                          "ReductionStreamConsumer::processData");
//                 // TBD: logging repeatGuard needed!
//             } else {
//                 // Obtain the respective position values from the position stream consumer
//                 double actAz = 0.0;
//                 double actEl = 0.0;
//                 double encAz = 0.0;
//                 double encEl = 0.0;
//                 try {
//                     opticalPointingDevice_p.positionConsumer_m->
//                         getActPosition(data.timeStamp, actAz, actEl);
//                     opticalPointingDevice_p.positionConsumer_m->
//                         getEncPosition(data.timeStamp, encAz, encEl);
//                 } catch (...) {
//                     string msg = "Exception caught from position stream consumer.";
//                     msg += " Ignoring antenna position and carrying on anyhow.";
//                     opticalPointingDevice_p.getLogger()->log(Logging::BaseLog::LM_ERROR, 
//                                                              msg, __FILE__, __LINE__, 
//                                                              "ReductionStreamConsumer::processData");
//                     // TBD: logging repeatGuard needed!  
//                 }

//                 // Add the antenna position data to the output string
//                 outputString << "," << actAz << "," << actEl;

//                 // Transform the CCD position into Az and El
//                 double skyFrameAz = 0.0;
//                 double skyFrameEl = 0.0;
//                 opticalPointingDevice_p.transformPosition(data.posX,
//                                                           data.posY, 
//                                                           encEl,
//                                                           data.width,
//                                                           data.height,
//                                                           skyFrameAz,
//                                                           skyFrameEl);
        
//                 // Add the sky frame positions to the output string
//                 outputString << "," << skyFrameAz << "," << skyFrameEl;
//             }

//             // The data event is not needed anymore, so remove it
//             opticalPointingDevice_p.reductionDataQueue_m.pop();

//             // Log the resulting string for debugging purposes
//             opticalPointingDevice_p.getLogger()->log(Logging::BaseLog::LM_DEBUG,
//                                                      outputString.str(),
//                                                      __FILE__, __LINE__,
//                                                      "ReductionStreamConsumer::processData");

//             // Add a new line for the file writing
//             outputString << endl;

//             // Check if we have a valid output file
//             if (opticalPointingDevice_p.rapidPositionOutputFile_m != "") {
//                 // Open file for append
//                 ofstream of;
//                 of.open (opticalPointingDevice_p.rapidPositionOutputFile_m.c_str(),
//                          ios::out | ios::app);
//                 if (of.is_open()) {
//                     // Add output string and close the file
//                     of << outputString.str();
//                     of.close();
//                 } else {
//                     // Log a message for debugging purpose
//                     opticalPointingDevice_p.getLogger()->log(Logging::BaseLog::LM_DEBUG,
//                                                              "Error when opening output file.",
//                                                              __FILE__, __LINE__,
//                                                              "ReductionStreamConsumer::processData");
//                 }

//             }

//         }
// }

/////////////////////////////////////////////////
// MACI DLL support functions
/////////////////////////////////////////////////
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(OpticalPointingImpl)
