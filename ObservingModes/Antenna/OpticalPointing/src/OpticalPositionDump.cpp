// "@(#) $Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <signal.h>
#include <unistd.h> // for sleep
#include <cmath> // for M_PI
#include <maciSimpleClient.h>
#include <OpticalPositionConsumer.h>
#include <PositionStreamConsumer.h>
#include <TETimeUtil.h>
#include <pthread.h>
#include <ace/Time_Value.h>
#include <OpticalTelescopeC.h> // for Control::OpticalTelescope
#include <MountC.h> // for Control::MountStatusData
#include <ControlDeviceC.h> // for Control::ControlDevice
#include <iostream> // for cerr, cout & endl;
#include <iomanip> // for fixed & setprecision
#include <TETimeUtil.h>
#include <fstream>
#include <string>
#include <vector>
#include <list>

using std::vector;
using std::list;
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::ostringstream;
using Control::OpticalTelescope;
using Control::MountStatusData;

bool runFlag = true;

void print(double data, bool isValid, int fieldWidth, ostream& file) {
    file << " " << std::setw(fieldWidth);
    if (isValid) {
        file << data*180./M_PI;
    } else {
        file << "invalid";
    }
}

void* runLoop(void* simpleClient) {
    ACE_Time_Value t;

    while(runFlag) {
        t.set(1L,0L);
        static_cast<maci::SimpleClient*>(simpleClient)->run(t);
    }
    return NULL;
}

void SigCatcher(int num) {
    runFlag = false;
}

class Writer {
public:
    Writer(const string& antennaName, double exposureTime) :
        centroids_m(),
        exposureTime_m(TETimeUtil::toTimeInterval(exposureTime)),
        opc_m(antennaName.c_str()),
        psc_m(antennaName.c_str()){};
    
    ~Writer() {
        opc_m.disconnect();
        psc_m.disconnect();
    }

    void write(ostream& file) {
        {
            vector<OpticalTelescope::StarPosition> newData(opc_m.getData());
            if (!newData.empty()) {
                opc_m.clear(newData.back().timestamp+1);
            }
            centroids_m.insert(centroids_m.end(), newData.begin(), newData.end());
        }
        
        for (list<OpticalTelescope::StarPosition>::iterator
                 star = centroids_m.begin(); star != centroids_m.end(); ++star) {

            ACS::Time startTime = star->timestamp;
            ACS::Time stopTime = startTime + exposureTime_m;
            vector<MountStatusData> psn = psc_m.getDataRange(startTime, stopTime);
            // TODO. Call a function, that needs to be writte, to clear all the
            // PSC data before stopTime. Otherwise it just keeps accumulating
            // ie., consuming memory.
                
            MountStatusData avg;
            bool gotData = false;
            if (!psn.empty()) {
                gotData = true;
                avg = PositionStreamConsumer::average(psn);
                // Now check that we have got all the requested PSC data
                // If this message is seen too often then a better job of
                // aligning the OPT and Mount data will need to be done.
                if (startTime < (psn.front().timestamp - TETimeUtil::TE_PERIOD_ACS)) {
                    ostringstream msg;
                    msg << "Could not get all the position data for the exposure that started at "
                        << TETimeUtil::toTimeString(startTime)
                        << ". The first available position was at "
                        << TETimeUtil::toTimeString(psn.front().timestamp)
                        << " so that "
                        << (psn.front().timestamp - startTime)/TETimeUtil::TE_PERIOD_ACS
                        << " positions were missing.";
                    LOG_TO_OPERATOR(LM_WARNING, msg.str());
                }
                if (stopTime > (psn.back().timestamp + TETimeUtil::TE_PERIOD_ACS)) {
                    ostringstream msg;
                    msg << "Could not get all the position data for the exposure that stopped at "
                        << TETimeUtil::toTimeString(stopTime)
                        << ". The last available position was at "
                        << TETimeUtil::toTimeString(psn.back().timestamp)
                        << " so that "
                        << (stopTime - psn.back().timestamp)/TETimeUtil::TE_PERIOD_ACS
                        << " positions were missing.";
                    LOG_TO_OPERATOR(LM_WARNING, msg.str());
                }
            }
            // TODO. The code below is very similar to that in the
            // OpticalPointingImpl::doExposure function. Move this to common
            // (static) functions.
            const double elOffset = star->latOffset;
            double azOffset = star->longOffset;
            if (avg.elPositionsValid) {
                azOffset /= std::cos(avg.elPosition);
            }

            double pointingModelOffsetAz = 0.0;
            double pointingModelOffsetEl = 0.0;
            if (avg.pointingModel) {
                pointingModelOffsetAz = avg.azPointingModelCorrection;
                pointingModelOffsetEl = avg.elPointingModelCorrection;
            }
            if (avg.auxPointingModel) {
                pointingModelOffsetAz += avg.azAuxPointingModelCorrection;
                pointingModelOffsetEl += avg.elAuxPointingModelCorrection;
            }

            file << "! " << TETimeUtil::toTimeString((startTime+stopTime)/2);
            file << std::fixed << std::setprecision(3) 
                 << " " << std::setw(9) << star->noise 
                 << " " << std::setw(11) << star->xPosition 
                 << " " << std::setw(9) << star->yPosition
                 << endl;
            file << "! " << std::fixed << std::setprecision(6);
            print(pointingModelOffsetAz, gotData, 11, file);
            print(pointingModelOffsetEl, gotData, 9, file);
            print(azOffset, gotData && avg.elPositionsValid, 11, file);
            print(elOffset, gotData, 9, file);
            file << endl;

            file << "  ";
            print(avg.azCommanded, gotData && avg.azCommandedValid, 11, file);
            print(avg.elCommanded, gotData && avg.elCommandedValid, 9, file);
            double offsetAz = 
                avg.azCommanded - avg.azPosition - azOffset + pointingModelOffsetAz;
            double offsetEl = 
                avg.elCommanded - avg.elPosition - elOffset + pointingModelOffsetEl;
            print(offsetAz, gotData && avg.azPositionsValid && avg.azCommandedValid, 11, file);
            print(offsetEl, gotData && avg.elPositionsValid && avg.elCommandedValid, 9, file);
            file << endl;
        }
        centroids_m.clear();
        file.flush();
    }
private:
    list<OpticalTelescope::StarPosition> centroids_m;
    ACS::TimeInterval exposureTime_m;
    OpticalPositionConsumer opc_m;
    PositionStreamConsumer psc_m;
};


int main(int argc, char *argv[]) {

    // Parse the input arguments and ensure they make sense
    if (argc < 2 || argc > 4) {
        cerr << "OpticalPositionDump <AntennaName> <exposure-time> <output-file> " << endl;
        cerr << "e.g., OpticalPositionDump PM01 0.5 star-positions.dat" << endl;
        cerr << "If no exposure time is set 0.1 seconds will be used."
             << endl;
        cerr << "If no output file is specified data will be sent to the terminal."
             << endl;
        cerr << "Use Ctrl-C to exit program" << endl;
        return -1;
    }

    //
    // The user has provided required arguments.  We may proceed.
    maci::SimpleClient sc;
    ostream* out = &cout;
    
    sc.init(argc, argv); // Just pass in the name of the program
    if (!sc.login()) {
        cerr << "Problem connecting to the central services."
             << " Is the system operational?" << endl;;
        return -1;
    }
    
    if (argc > 3) {
        ostream* fout = new std::ofstream(argv[3], ios_base::out);
        if (!(*fout)) {
            cerr << "Error opening output file: \"" << argv[3] << "\" exiting!"
                 << endl;
            sc.logout();
            return -1;
        }
        out = fout;
        cerr << "Position data sent to file: \"" << argv[3] << "\"" << endl;
    } else {
        cerr << "Position data sent to the terminal." << endl;
    }
    
    double exposureTime = 0.1;
    if (argc > 2) exposureTime = atof(argv[2]);
    if (exposureTime < 0.050) {
        exposureTime = 0.050;
        LOG_TO_OPERATOR(LM_INFO, "Resetting the exposure time to the minimum value of 0.050 seconds.");
    }
    
    // Enable the signal handler
    if (signal(SIGINT, SigCatcher) == SIG_ERR) {
        perror("Unable to catch interupt signals");
        sc.logout();
        return -1;
    }

    // argv[1] is the name of the antenna e.g., DV01. We need to use
    // this to find the name of the optical telescope & mount components.
    maci::SmartPtr<Control::OpticalTelescope> opt;
    maci::SmartPtr<Control::Mount> mount;
    string deviceName;
    try {
        string antennaName = "CONTROL/";
        antennaName += argv[1];
        maci::SmartPtr<Control::ControlDevice> antenna_p =
            sc.getComponentNonStickySmartPtr<Control::ControlDevice>
            (antennaName.c_str());
        deviceName = antenna_p->getSubdeviceName("OpticalTelescope");
        opt = sc.getComponentNonStickySmartPtr<Control::OpticalTelescope>(deviceName.c_str());
        deviceName = antenna_p->getSubdeviceName("Mount");
        mount = sc.getComponentNonStickySmartPtr<Control::Mount>(deviceName.c_str());
    } catch (ControlExceptions::IllegalParameterErrorEx) {
        cerr << "Unable to contact the " << deviceName << " component."
             << " Is the antenna name of " << argv[1] << " correct and"
             << " does the specified antenna contain an optical telescope and mount?" << endl;
        sc.logout();
        return -1;
    } catch (maciErrType::CannotGetComponentExImpl) {
        cerr << "Unable to contact the optical telescope component." 
             << " Is the antenna name correct and"
             << " does the specified antenna contain an optical telescope?"
             << " If so check if the relevant container has crashed?";
        sc.logout();
        return -1;
    } catch (...) {
        cerr <<"Caught an exception while trying to get the " + deviceName + " component."
             << " Returning prematurely." << endl;
        sc.logout();
        return -1;
    }

    // TODO. Catch the exceptions these functions might throw
    opt->startContinuousExposure(exposureTime);
    mount->enableMountStatusDataPublication(true);

    Writer writer(argv[1], exposureTime);
    pthread_t              tID;
    pthread_create(&tID, NULL, runLoop, dynamic_cast<void*>(&sc));

    cerr << "Use Ctrl-C to exit program" << endl;
    *out << "Star Position for antenna " << argv[1] << " on " 
         << TETimeUtil::toTimeDateString(::getTimeStamp()).substr(0, 10)
         << " Exposure time: " << std::fixed << std::setprecision(3) 
         << exposureTime << " seconds" << endl;

    while (runFlag) {
        writer.write(*out);
        sleep(1);
    }
    pthread_join(tID,NULL);

    writer.write(*out);
    // There is no need to close the file as its done in the destructor!
    opt->stopContinuousExposure();
    mount->enableMountStatusDataPublication(false);
    sc.logout();
}
