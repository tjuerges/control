#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It defines the OpticalPointing class.
"""

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import ModeControllerExceptions
import ModeControllerExceptionsImpl
import ControlExceptions
import ControlExceptionsImpl
import CCL.OpticalTelescope
import CCL.MountController
import ScriptExec.rm
import maciErrType
import maci
import CCL.SIConverter

class OpticalPointing:
    '''
    The OpticalPointing object is used to perform pointing
    measurements using an optical telescope fixed to the backup
    structure on an antenna. Its core functions move the antenna to
    point at a star, take an exposue of that star, an analyze the data
    to determine the apparent star position. The example below
    illustrates this. This object is the basis of an optical pointing
    observing script and an example of an observing script that uses
    this object can be found in CVS at
    SSR/ObservingModes/src/OpticalPointing.py
    
    EXAMPLE:
    # Load the necessary defintions
    import CCL.OpticalPointing
    # create the object
    op = CCL.OpticalPointing.OpticalPointing("DV01")
    # take a dark exposure and open the shutter.
    op.initializeHardware();
    # Observe a star
    data = op.doSourceSubscan(exposureTime, ra, dec)
    # write the exposure to a fits file.
    fullFileName = op.writeSourceFits('star.fits'):
    # stow the antenna
    op.shutdownHardware();
    # destroy the component
    del(op)
    '''

    def __init__(self, antennaName = None, componentName = None):
        '''
        The constructor creates a OpticalPointing object.

        If the antennaName is defined then this constructor starts a
        dynamic component that implements the functions available in
        the OpticalPointing object. The constructor also establishes
        the connection between this component and the relevant
        components (OpticalTelescope & MountController) in the
        specified antenna.

        If the componentName is not None, then the object uses that
        component to implement the functions available in Optical
        Pointing It is assumed that the component was allocated by
        other means and references to the relavent hardware already
        exist.  This method is intended for internal use, and should
        only be used by experts.

        This function throws a:
        * CannotGetAntenna exception if it cannot communicate with the
        specified antenna component. This may mean you have
        incorrectly specified the antenna name.
        * BadConfiguration exception if the specified antenna does not
        contain all the required hardware components ie., the
        MountController, PrototypeOpticalTelescope and FrameGrabber components.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("DV01")
        # destroy the component
        del(op)
        '''
        try:
            # Indicates whether a reference to the CORBA component was
            # obtained (and hence whether the destructor needs to
            # relase it). If things work its set to true at the end
            # of this constructor. There are probably better ways to
            # do this.
            self.releaseComponent = False; 


            # Only allow one of the antennaName or componentName
            # arguments to be set.
            if not((antennaName == None) ^ (componentName == None)):
                raise;

            # 1. Get a reference to a possibly newly created optical
            #  pointing mode controller object
            if antennaName != None:
                antennaComponentName = 'CONTROL/'+antennaName;
                # The library name is specified in the CDB in Components.xml
                compSpec = maci.ComponentSpec\
                           ('*',
                            'IDL:alma/Control/OpticalPointing:1.0',
                            '*', '*')
                self.__op = ScriptExec.rm.getCollocatedComp\
                            (compSpec, False, antennaComponentName);
                if (self.__op == None): # Should throw an exception here
                    return None
                self.releaseComponent = True;
                self.__op.allocate(antennaName);

            # 2. Specified previously existing component so just get the
            # reference. This option is for experts only.
            if componentName != None:
                # Specified previously existing component, just get reference
                self.__op = ScriptExec.rm.getComponent(componentName)
                if (self.__op == None):
                    return None
        except (maciErrType.CannotGetComponentEx), e:
            raise ModeControllerExceptionsImpl.CannotGetAntennaExImpl(exception=e);
        except (ControlExceptions.INACTErrorEx), e:
            raise ModeControllerExceptionsImpl.BadConfigurationExImpl(exception=e);
        except (ModeControllerExceptions.CannotGetAntennaEx, \
                ModeControllerExceptions.BadConfigurationEx), e:
            self.__op.deallocate();
            raise;

    def __del__(self):
        '''
        The destructor shuts down the underlying optical pointing
        component (assuming no other clients have references to
        it). This will release the references the optical pointing
        mode controller has to the optical telescope and mount
        components. It does not shutdown the hardware ie., close the
        shutter on the optical telescope or apply the brakes on the
        mount.

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        if (self.releaseComponent):
            ScriptExec.rm.releaseComponent(self.__op._get_name());

    def initializeHardware(self):
        '''
        Initialize the relevant equipment in the antenna for use in
        optical pointing. This means:
        1. Put the antenna mount into track mode.
        2. Turn on the camera in the optical telescope
        3. Open the shutter on the optical telescope.

        This function throws a:
        * MountFault exception if there is any problem communicating
        with the antenna mount. This includes hardware faults.
        * HardwareFault exception if there is any other problem
        communicating with the optical telescope or frame-grabber.
        This includes hardware faults and being unable to take a dark
        exposure.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("DA41")
        # Get the hardware ready
        op.initializeHardware()
        # Stow the antenna
        op.shutdownHardware()
        # destroy the component
        del(op)
        '''
        return self.__op.initializeHardware();

    def shutdownHardware(self):
        '''
        This function should be used at the end of an optical pointing
        observation. It tries to do the following:
        1. Moves the antenna to its survival stow position and then puts
        it in shutdown mode.
        2. Closes the shutter on the optical telescope and powers down
        the camera.

        This function throws a:
        * HardwareFault exception if there is any problem closing the
        shutter or powering down the camera.

        EXAMPLE:
        See the example for the initializeHardware function.
        '''
        return self.__op.shutdownHardware();
        
    def doSourceExposure(self, exposureTime, ra, dec,
                         pmRA=0, pmDec=0, parallax=0):
        '''
        Take a exposure of a star at the specified position
        integrating over the specified amount of time (in seconds) and
        return the parameters of the star in the field of view. The
        right ascension & declination are in radians, the proper
        motions in both axes is in radians/sec and the parallax is in
        radians.

        This method opens the shutter and waits until the antenna is
        at the specified position (within the tolerance specified
        using the setTolerance function). It then starts an exposure
        for the specified amount of time. Once the exposure is
        complete it subtracts the dark frame and analyzes the image to
        determine the parameters of the star position. These
        parameters are returned in a SubScanOpticalPointingData
        structure (described in the documentation for the doExposure
        function). This function block fopr the duration of all these
        tasks.

        This function throws a:
        * IllegalParameterError exception if any of the supplied
        parameters is out of range.
        * MountFault exception if there is any problem communicating
        with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna mount does not go "on-source" after
        a suitable amount of time (controlled using the setTimeout function).
        * HardwareFault exception if there is any other problem
        communicating with the optical telescope or frame-grabber.
        This includes hardware faults.
        * DeviceBusy exception if the frame-grabber is busy with
        another exposure.
        * BadData exception if there is any problem analyzing the data.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        from math import radians
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("DV01")
        # Get the hardware ready
        op.initializeHardware()
        # Take an exposure of a star
        data = op.doSourceExposure(10.0, '23:34:12.345', '-78.23.12.98')
        # write the source exposure to a FITS file.
        op.saveLastExposure("star.fits")
        # Close the camera shutter and stop the antenna moving
        op.shutdownHardware()
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        exposureTimeInSecs =  CCL.SIConverter.toSeconds(exposureTime)
        return self.__op.doSourceExposure\
               (exposureTimeInSecs, raInRad, decInRad,\
                pmRAInRadPerSec,  pmDecInRadPerSec, parallaxInRad);

#     def startRapidPosition(self, exposureTime, outputFile):
#         '''
#         This method starts the FrameGrabber Rapid Position Loop and
#         an internal event consumer that receives the incoming events.
#         For each event received from the frame grabber the respective
#         position data is obtained from the position stream consumer.
#         The antenna position is matched by using the timestamp of the
#         exposure (middle) and both the reduction data and the position
#         data is appended to the defined output file.

#         EXAMPLE:
#         # Load the necessary defintions
#         import CCL.OpticalPointing
#         from time import sleep
#         # create the object
#         op = CCL.OpticalPointing.OpticalPointing("DV01")
#         # Get the hardware ready
#         op.initializeHardware()
#         # Do a dark frame with the desired exposure time
#         op.doDarkExposure(0.07)
#         # Start the rapid position analysis
#         op.startRapidPosition(0.07, "/tmp/myoutputfile.csv")
#         # Wait for the time you want to record the data (e.g. 60 seconds)
#         sleep 60
#         # Stop the rapid position analysis
#         op.stopRapidPosition()
#         # Stow the antenna
#         op.shutdownHardware()
#         # destroy the component
#         del(op)
#         '''
#         return self.__op.startRapidPosition(exposureTime, outputFile);

#     def stopRapidPosition(self):
#         '''
#         This method stops the FrameGrabber Rapid Position Loop and
#         the internal event consumer that received the incoming events.

#         EXAMPLE:
#         See example of startRapidPosition()
#         '''
#         return self.__op.stopRapidPosition();

    def doExposure(self, exposureTime):
        '''
        Take an exposure. This method takes an exposure using the
        optical telescope. The exposure time is specified in
        seconds. Unlike the doSourceExposure function it does not
        command the antenna mount or open/close the shutter on the optical
        telescope. It does do all of the following:
        1. Commands the optical telescope to take an exposure.
        3. Waits for this exposure to complete
        4. Gets the parameters of the exposure from optical
           telescope. This includes the stars offset from the centre of
           the CCD.
        5. Get the positions of the antenna for the duration of the exposure
        6. Determines if the antenna was on-source for the duration of
           the exposure. If not a BadData exception is thrown.
        7. Determines the average position bfor the duration of the exposure.
        8. Adds the stars offset to the average position.  

        It returns in a SubScanOpticalPointingData structure,
        the parameters of the star in the field.

        The SubScanOpticalPointingData structure is defined in the
        ICD/CONTROL/idl/ControlInterfaces.midl file and has the
        following fields.
        antennaName - The antenna name as given in the constructor.
        startTime - When the sub-scan started in ACS time units.
        endTime - When the sub-scan stopped in ACS time units.
        ACS time units are in steps of 100ns since the start of the
        Julian calendar in 1582.
        sourceAZ.value - The azimuth of the centre of the camera
                         at the midpoint of the exposure (in radians).
        sourceEL.value - The elevation of the centre of the camera
                         at the midpoint of the exposure (in radians).
        offsetAZ.value - The azimuth of the star at the midpoint of the
                         exposure (in radians).
        offsetEL.value - The elevation of the star at the midpoint of the
                         exposure (in radians).
        errorAZ.value - The 1 sigma error in the azimuth value (in radians).
        errorEL.value - The 1 sigma error in the elevation value (in radians).
        xBeamWidth.value - The field of view in the X axes (in radians).
        yBeamWidth.value - The field of view in the Y axes (in radians).
        peakIntensity.value - The peak value of the star (arbitrary units)
        
        This function throws a:
        * DeviceBusy exception if the frame-grabber is busy with a
        previous exposure. 
        * HardwareFault exception if there is any other problem using
        the frame-grabber.  This includes hardware faults.
        * MountFault exception if the mount does not track the star
        correctly during the exposure (to within the tolerance
        specified using the setTolerance function).
        * BadData exception if there is any problem analyzing the
        data. This may happen if the star is too near the edge of
        the frame or the antenna is off-source during an exposure

        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        from math import radians
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("DV01")
        # Get the hardware ready
        op.initializeHardware()
        # move the antenna to the right position externally
        op.setDirection(radians(-114.493), radians(87.37))
        # Insert the IR filter on the optical telescope
        op.getOpticalTelescope().dayTimeObserving()
        # Take an exposure of a star
        data op.doExposure(10.0)
        # Stow the hardware
        op.shutdownHardware()
        # destroy the component
        del(op)
        '''
        exposureTimeInSecs =  CCL.SIConverter.toSeconds(exposureTime)
        return self.__op.doExposure(exposureTimeInSecs);

    def setDirection(self, ra, dec,
                     pmRA=0, pmDec=0, parallax=0):
        '''
        Move the antenna to the specified celestial position and track
        a star at that position. The right ascension & declination are
        in radians, the proper motion on both axes is in radians/sec
        and the parallax is in radians. This function will not return
        until the antenna is pointing at the specified position to
        within a specified tolerance. The angular tolerance is
        adjusted using the setTolerance function.
        
        This function throws a:
        * IllegalParameterError exception if any of the supplied
        parameters is out of range.
        * MountFault exception if there is any problem communicating
        with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
        a suitable amount of time (controlled using the setTimeout function).

        EXAMPLE:
        See the example for the doExposure function
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self.__op.setDirection(raInRad, decInRad, \
                                      pmRAInRadPerSec, pmDecInRadPerSec, \
                                      parallaxInRad);

    def setTolerance(self, angle):
        '''
        Set the angular separation that defines when an antenna is "on
        source". The angle is specified in radians. If the separation
        between the commanded and actual positions is less than this
        angle functions that wait for the antenna to start tracking a
        star, like the setDirection function, will complete.

        This function throws a:
        * IllegalParameterError exception if the angle is not between 0 and pi

        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        from math import radians
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("DA41")
        # Get the hardware ready
        op.initializeHardware()
        # Move the antenna using the default tolerance and timeout
        op.setDirection(radians(-114.493), radians(87.37))
        # Halve the tolerance
        defaultTolerance = op.getTolerance();
        op.setTolerance('1.5arcsec');
        # Increase the timeout by 20 seconds.
        defaultTimeout = op.getTimeout();
        op.setTimeOut(defaultTimeout+20.0);
        # Move the antenna again
        op.setDirection(radians(78), radians(34.0))
        # Stow the antenna
        op.shutdownHardware()
        # destroy the component
        del(op)
        '''
        angleInRad = CCL.SIConverter.toRadians(angle)
        return self.__op.setTolerance(angleInRad);

    def getTolerance(self):
        '''
        Get the angular separation that defines when an antenna is "on
        source". The returned angle is in radians and will always be
        greater than zero and less than pi. The default value is
        1.0/60/180*pi (1 arc-sec).

        EXAMPLE:
        See the example for the setTolerance function
        '''
        return self.__op.getTolerance();

    def setTimeout(self, timeout):
        '''
        Set the maximum time to wait for the antenna to move to the
        commanded position. The time is in seconds. If the antenna has
        not moved to the specified position, to within the required
        tolerance, within the specified amount of time then functions
        that move the antenna, like setDirection, will throw a Timeout
        exception. A negative value disables the timeout and should be
        used with caution.

        EXAMPLE:
        See the example for the setTolerance function
        '''
        timeoutInSecs =  CCL.SIConverter.toSeconds(timeout)
        return self.__op.setTimeout(timeoutInSecs);

    def getTimeout(self):
        '''
        Get the maxmimum time allowed for the antenna to move to the
        specified position. If the antenna has is not "on-source"
        after this amount of time then functions that move the
        antenna, like setDirection, will throw a Timeout exception.
        The returned time is in seconds and a negative value indicates
        that no timeout will be used. The default value is 160 seconds.

        EXAMPLE:
        See the example for the setTolerance function
        '''
        return self.__op.getTimeout();

#     def setSignalToNoiseThreshold(self, threshold):
#         '''
#         Sets the Signal-to-Noise Ratio (SNR) to be used at the
#         exposure reduction algorithm.
#         '''
#         return self.__op.setSignalToNoiseThreshold(threshold);

    def saveLastExposure(self, fileName):
        '''
        Writes the latest exposure to a FITS file with the specified
        filename. This function will silently overwrite any existing
        file with the same name. 

        EXAMPLE:
        See the example for the doSourceExposure function.
        '''
        return self.__op.saveLastExposure(fileName);

    def spiralSearch(self, maxDist='30 arcmin', exposure=2, step='10 arcmin'):
        '''
        Searches for a star by offsetting the mount from its current
        position in a spiral pattern (the pattern is not a true spiral
        but rather a "square spiral"). The origin of the spiral is
        wherever the antenna is initially pointing and can be adjusted
        using the setDirection function in this object. This function
        then offsets the mount in steps defined by the "step"
        argument. Once the search has completed one circuit of the
        origin at an angular distance of "step" it moves out to a
        radius of 2*step and does another circuit. The function
        completes when the distance from the origin exceeds the value
        of the "maxDist" argument.

        After each step the mount pauses for a period of time defined
        by the "exposure" argument. During this interval an exposure
        is taken and the results analyzed. This function reports if a
        star is detected along with what the current offset is.

        All angular arguments are in radians and the step argument
        default to .1 degrees and the maxDist argument defaults to .5
        degrees. The exposure argument is in seconds and defaults to 2
        seconds.
 
        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        from math import radians
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("ALMA04")
        # Get the hardware ready
        op.initializeHardware()
        # move the antenna to point at Sirius
        op.setDirection(radians(101.2871), radians(-16.7161))
        # Start a spiral search
        op.spiralSearch()
        # Stow the hardware
        op.shutdownHardware()
        # destroy the component
        del(op)
        '''
        def distance(x,y):
            return (x**2+y**2)**0.5

        def moveTo(x0, y0, x1, y1, step, exposure, mc):
            while (x0 != x1) or (y0 != y1): 
                if (x1 > x0):
                    x0 += 1
                elif (x1 < x0):
                    x0 -= 1
                if (y1 > y0):
                    y0 += 1
                elif (y1 < y0):
                    y0 -= 1
                mc.setHorizonOffset(x0*step, y0*step)
                try:
                    data = self.doExposure(exposure)
                    print "Offset by (%7.3f, %7.3f) degrees. Star detected!" % (math.degrees(x0*step), math.degrees(y0*step))
                except (ModeControllerExceptions.BadDataEx), e:
                    print "Offset by (%7.3f, %7.3f) degrees." % (math.degrees(x0*step), math.degrees(y0*step))
            return (x0, y0);

        x = 0
        y = 0
        turn = 1
        mystep = 1
        mc = self.getMountController();
        
        maxDistInRad = CCL.SIConverter.toRadians(maxDist)
        stepInRad = CCL.SIConverter.toRadians(step)
        while(distance(x * stepInRad, y * stepInRad) <= maxDistInRad):
            if (turn%4 == 1):
                (x,y) = moveTo(x, y, x + mystep, y, stepInRad, exposure, mc)
            elif (turn%4 == 2):
                (x, y) = moveTo(x, y, x, y + mystep, stepInRad, exposure, mc)
                mystep=mystep+1
            elif (turn%4 == 3):
                (x, y) = moveTo(x, y, x - mystep, y, stepInRad, exposure, mc)
            elif (turn%4 == 0):
                (x, y) = moveTo(x, y, x, y - mystep, stepInRad, exposure, mc)
                mystep = mystep+1
            turn += 1
        print "Returning to zero offset"
        mc.setHorizonOffset(0, 0)

    def getOpticalTelescope(self):
        '''
        Return the optical telescope object used by this observing
        mode.  This will allow you access to many detailed aspects of
        the telescope. However it should only be used by people who
        understand how this object interacts with this observing
        mode. It is made available so that experts can undertake
        optical pointing observations in ways not forseen in this
        object.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("DA41")
        # Get the optical telescope object
        ot = op.getOpticalTelescope();
        # change to daytime observing (inserts the IR filter)
        ot.daytimeObserving();
        # destroy the object
        del(op)
        '''
        compName = self.__op.getOpticalTelescope();
        return CCL.OpticalTelescope.OpticalTelescope(componentName=compName);

    def getMountController(self):
        '''
        Return the mount controller object used by this observing
        mode.  This will allow you access to many detailed aspects of
        the antenna position. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake
        optical pointing observations in ways not forseen in this mode
        controller.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.OpticalPointing
        # create the object
        op = CCL.OpticalPointing.OpticalPointing("DV01")
        # Get the mount controller object
        mc = op.getMountController();
        # put the antenna in track mode 
        print mc.trackMode();
        # put the antenna in the maintenance stow position (near the zenith)
        print mc.maintenanceStow();
        # put the antenna in shutdown mode (activates the brakes).
        print mc.shutdownMode();
        # destroy the object
        del(op)
        '''
        compName = self.__op.getMountController();
        return CCL.MountController.MountController(componentName=compName)
