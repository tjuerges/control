// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2007, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#ifndef OPTICALPOINTING_IDL
#define OPTICALPOINTING_IDL

#include <AntModeController.idl>
#include <ModeControllerExceptions.idl>
#include <ControlExceptions.idl>
#include <ControlDataInterfaces.idl>

#pragma prefix "alma"

module  Control {

    /// The OpticalPointing component provides the higher level functionality
    /// necessary for the Optical Pointing observing mode. It coordinates the
    /// optical telescope and the mount controller and implements functionality
    /// specific to this observing mode.
    interface OpticalPointing : Control:: AntModeController {

        /// See OpticalPointing.py for a description of this function
        void initializeHardware()
            raises (ControlExceptions::HardwareErrorEx,
                    ModeControllerExceptions::UnallocatedEx);

        /// See OpticalPointing.py for a description of this function
        void shutdownHardware()
            raises (ControlExceptions::HardwareErrorEx,
                    ModeControllerExceptions::UnallocatedEx);

        /// See OpticalPointing.py for a description of this function
        Control::SubScanOpticalPointingData
        doSourceExposure(in double exposureTime,
                         in double ra, in double Dec,
                         in double pmRA, in double pmDec,
                         in double parallax)
            raises(ModeControllerExceptions::UnallocatedEx,
                   ControlExceptions::IllegalParameterErrorEx,
                   ModeControllerExceptions::MountFaultEx,
                   ModeControllerExceptions::TimeoutEx,
                   ModeControllerExceptions::HardwareFaultEx,
                   ControlExceptions::DeviceBusyEx,
                   ModeControllerExceptions::BadDataEx);


        /// See OpticalPointing.py for a description of this function
        void doDarkExposure(in double exposureTime)
            raises(ModeControllerExceptions::UnallocatedEx,
                   ControlExceptions::DeviceBusyEx,
                   ModeControllerExceptions::HardwareFaultEx,
                   ModeControllerExceptions::BadDataEx);

        /// See OpticalPointing.py for a description of this function
        Control::SubScanOpticalPointingData doExposure(in double exposureTime)
            raises(ModeControllerExceptions::UnallocatedEx,
                   ControlExceptions::DeviceBusyEx,
                   ControlExceptions::HardwareErrorEx,
                   ControlExceptions::IllegalParameterErrorEx,
                   ModeControllerExceptions::HardwareFaultEx,
                   ModeControllerExceptions::MountFaultEx,
                   ModeControllerExceptions::BadDataEx);

//         /// See OpticalPointing.py for a description of this function
//         void startRapidPosition(in double exposureTime,
//                                 in string outputFile)
//             raises(OpticalPointingExceptions::LoopRunningEx,
//                    ModeControllerExceptions::HardwareFaultEx,
//                    OpticalPointingExceptions::FrameGrabberFaultEx,
//                    OpticalPointingExceptions::ConsumerFaultEx);	

//         /// See OpticalPointing.py for a description of this function
//         void stopRapidPosition()
//             raises(OpticalPointingExceptions::LoopNotRunningEx,
//                    OpticalPointingExceptions::FrameGrabberFaultEx,
//                    OpticalPointingExceptions::ConsumerFaultEx);	

        /// See OpticalPointing.py for a description of this function
        void setDirection(in double ra, in double dec,
                          in double pmRA, in double pmDec, in double parallax)
            raises(ModeControllerExceptions::UnallocatedEx,
                   ControlExceptions::IllegalParameterErrorEx,
                   ModeControllerExceptions::MountFaultEx,
                   ModeControllerExceptions::TimeoutEx);

        /// See OpticalPointing.py for a description of this function
        void setTolerance(in double angle)
            raises(ModeControllerExceptions::UnallocatedEx,
                   ControlExceptions::IllegalParameterErrorEx,
                   ModeControllerExceptions::MountFaultEx);

        /// See OpticalPointing.py for a description of this function
        double getTolerance()
            raises(ModeControllerExceptions::UnallocatedEx,
                   ModeControllerExceptions::MountFaultEx);

        /// See OpticalPointing.py for a description of this function
        void setTimeout(in double timeout);

        /// See OpticalPointing.py for a description of this function
        double getTimeout();

        /// See OpticalPointing.py for a description of this function
        void saveLastExposure(in string fileName);

        /// Return the name of the mount controller component for this
        /// observing mode. This will allow you access to many detailed
        /// aspects of the antenna.
        string getMountController();

        /// Return the name of the optical telescope component for this
        /// observing mode. This will allow you access to many detailed
        /// aspects of the telescope.
        string getOpticalTelescope();

    };
};


#endif // OPTICALPOINTING_IDL


