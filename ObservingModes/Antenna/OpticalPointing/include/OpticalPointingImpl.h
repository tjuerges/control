// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef OPTICALPOINTINGIMPL_H
#define OPTICALPOINTINGIMPL_H

// Base class(es)
#include <antModeControllerImpl.h>

//CORBA servant header
#include <OpticalPointingS.h>

// Forward declarations for classes that this component uses
class PositionStreamConsumer;

// Forward declarations for classes that this component uses
namespace Control {
    class OpticalTelescope;
    class MountController;
}

//includes for data members
#include <acsComponentSmartPtr.h> // for maci::SmartPtr

namespace Control {
    class OpticalPointingImpl: public Control::AntModeControllerImpl,
                               public virtual POA_Control::OpticalPointing
    {
    public:
        // ------------------- Class defintions ---------------------------

        // Reduction stream consumer class
//         class ReductionStreamConsumer :
//             public nc::SimpleConsumer<Control::ReductionData>
//         {
//         private:
//             static const ACS::Time REDUCTION_CONSUMER_DELAY;
//         public:
//             ReductionStreamConsumer(OpticalPointingImpl& 
//                                     opticalPointingDevice);
//             virtual ~ReductionStreamConsumer();
//             void processData(Control::ReductionData data);
//             static const int       EVENTS_TO_BE_DISCARDED;
//         protected:
//             OpticalPointingImpl& opticalPointingDevice_p;
//         };

//         // Friends
//         friend class OpticalPointingImpl::ReductionStreamConsumer;

        // ------------------- Constructor & Destructor -------------------
    
        /// The constructor for any ACS C++ component must have this signature
        OpticalPointingImpl(const ACE_CString& name,
                            maci::ContainerServices* containerServices);

        /// The destructor does nothing special. It must be virtual because
        /// this class contains virtual functions.
        virtual ~OpticalPointingImpl();   
    
        // --------------------- LifeCycle interface ----------------------

        // --------------------- CORBA interface --------------------------
    
        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        /// \exception ModeControllerExceptions::MountFaultEx
        virtual void initializeHardware();
    
        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        /// \exception ModeControllerExceptions::MountFaultEx
        virtual void shutdownHardware();
    
        // --------------- Methods to take exposures ---------------

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        ///	\exception ControlExceptions::DeviceBusyEx
        /// \exception ModeControllerExceptions::BadDataEx
        virtual Control::SubScanOpticalPointingData* 
        doSourceExposure(double exposureTime, 
                         double ra, double dec, 
                         double pmRA, double pmDec,
                         double parallax);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        ///	\exception ControlExceptions::DeviceBusyEx
        /// \exception ModeControllerExceptions::BadDataEx
        virtual void doDarkExposure(double exposureTime);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ModeControllerExceptions::BadDataEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        virtual Control::SubScanOpticalPointingData* 
        doExposure(double exposureTime);

//         /// See the IDL file for a description of this function.
//         /// \exception OpticalPointingExceptions::LoopRunningEx
//         /// \exception ModeControllerExceptions::HardwareFaultEx
// 	/// \exception OpticalPointingExceptions::FrameGrabberFaultEx
// 	/// \exception OpticalPointingExceptions::ConsumerFaultEx
//         virtual void startRapidPosition(double exposureTime,
//                                         const char *outputFile);

//         /// See the IDL file for a description of this function.
//         /// \exception OpticalPointingExceptions::LoopNotRunningEx
// 	/// \exception OpticalPointingExceptions::FrameGrabberFaultEx
// 	/// \exception OpticalPointingExceptions::ConsumerFaultEx
//         virtual void stopRapidPosition();
    
        /// See OpticalPointing.py for a description of this function
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setDirection(double ra, double dec,
                          double pmRA, double pmDec, double parallax);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setTolerance(double angle);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double getTolerance();

        /// See the IDL file for a description of this function.
        void setTimeout(double timeout);

        /// See the IDL file for a description of this function.
        double getTimeout();

        /// See the IDL file for a description of this function.
        char* getMountController();

        /// See the IDL file for a description of this function.
        char* getOpticalTelescope(); 

//         /// See the IDL file for a description of this function.
//         char* getFrameGrabber();

//     /// See the documentation in the IDL file for a description of this
//     /// function.
//     /// \exception ModeControllerExceptions::BadConfigurationEx
//     /// \exception ControlDeviceExceptions::HwLifecycleEx
//     virtual void abortExposure();
//     double calcExposureTime(double stellarMagnitude);
//     virtual double zeroMagnitudeExposureTime();
//     virtual void setZeroMagnitudeExposureTime(double);
    
        /* ----------------- Analysis Method -------------------- */
    

        /// \exception ModeControllerExceptions::HardwareFaultEx
        /// \exception FrameGrabberExceptions::BadDataExImpl
        /// \exception FrameGrabberExceptions::InvalidExposureExImpl
        virtual void reduceExposure(double& resultAz, double& resultEl,
                                    double& ccdErrorAz, double& ccdErrorEl, 
                                    double& SNR, long& width, long& height,
                                    ACS::Time& timeStamp, 
                                    double& exposureTime);
    
        // This method takes the CCD Position (in x and y pixels) rotates it
        // into the frame of the telescope.  It applies the appropriate scaling
        // factors and returns the result. It throws a hardwarefaultException
        // if it cannot get the relevant scaling parameters from the optical
        // telescope.
        virtual void transformPosition(const double ccdPositionAz, 
                                       const double ccdPositionEl,
                                       const double commandedElevation,
                                       const double width,
                                       const double height,
                                       double& resultAz, double& resultEl);

        virtual double signalToNoiseThreshold();
    
        virtual void setSignalToNoiseThreshold(double threshold);

        /// See the IDL file for a description of this function.
        virtual void saveLastExposure(const char* fileName);
    
    private:
        // ------------------- Mode Controller Methods --------------------    

        /// Configure this component to use the equipment in the specified
        /// antenna. The supplied argument is an antenna name like "DV01" and
        /// the associated antenna component must already be started (by the
        /// master component). This function will then query the antenna
        /// component to get the references to the optical telescope, frame
        /// grabber & mount controller components. This function can be called
        /// twice and if so it will release the equipment in the antenna it was
        /// previously using and acquire the equipment in the new antenna.
        /// \param antennaName 
        /// Name of antenna component to use e.g., "DV01"
        /// \exception ModeControllerExceptions::BadConfigurationEx
        /// A BadConfiguration exception is generated if the antenna does not
        /// have a mount component
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// A CannotGetAntenna exception is generated if the the specified
        /// antenna component cannot be contacted
        virtual void acquireReferences(const std::string& antennaName);
    
        /// Restore this component to the state it was in after
        /// construction. In this state this component does not have a
        /// reference to any components and none of its functions, except
        /// allocate & getState should be used.
        virtual void releaseReferences();
    
//     //  bool checkCompletion(CompletionImpl completion);
//     // Check that the optical telescope reference is non-nill. Otherwise
//     // throw an exception.

//     // \exception ModeControllerExceptions::BadConfigurationEx
//     void checkForOpticalTelescopeComponent();

//     // Check that the frame grabber reference is non-nill. Otherwise throw
//     // an exception.
//     // \exception ModeControllerExceptions::BadConfigurationEx
//     void checkForFrameGrabberComponent();

//     // Check that the mountController reference is non-nill. Otherwise throw
//     // an exception.
//     // \exception ModeControllerExceptions::BadConfigurationEx
//     // void checkForMountControllerComponent() 
    
        // References to the optical telescope and mountcontroller components.
        maci::SmartPtr<Control::OpticalTelescope> opticalTelescope_m;
        maci::SmartPtr<Control::MountController> mountController_m;

        // This object is used to get the antenna positions. I'd rather not use
        // a pointer but as the PositionStreamConsumer class does not have a
        // default constructor and an assign(antennaName) function. So I need
        // to use a pointer to the PositionStreamConsumer class (which I can
        // set to zero in the contructor of the AntModeController class).
        PositionStreamConsumer* positionConsumer_m;

        // This object is used to get the reduction data results that the
        // FrameGrabber produces when running its Rapid Position loop. It
        // subscribes to the notification channel and, once activated, just
        // starts to handle every incoming event.
        //        ReductionStreamConsumer* reductionConsumer_m;

        // ReductionData queue needed because there is a time delay between
        // these events arrive and the antenna position data is available.
        //        std::queue<Control::ReductionData> reductionDataQueue_m;

        // To work-around a notification channel feature (see more details at
        // processData() we use a counter:
        //         int reductionDataEventCounter_m;

        // Mutex used to check whether the consumer is running or not
        //         pthread_mutex_t          reductionConsumerMtx_m;
    
        // Rapid Position output file name
        //        std::string                   rapidPositionOutputFile_m;
        // Rapid Position output file header
        //         static const char*       rapidPositionHeader_m;
  
        CORBA::Double            magZeroExpTime_m;
        CORBA::Double            snrThreshold_m;

        // Constants obtained from the Optical Telescope
        double rotation_m;
        double xScale_m;
        double yScale_m;
        short xSize_m;
        short ySize_m;

        // The centre position, measured by the antenna, of the last
        // exposure. This goes in the header of the FITS file. Values are in
        // radians and any values greater than 10 are to be considered invalid.
        double azPos_m;
        double elPos_m;
    }; // end OpticalPointingImpl class

    // Because SimpleConsumer is not object oriented we need an
    // event handler method
//     void ReductionStreamConsumer_eventHandler(Control::ReductionData data,
//                                               void *reference)
//     {
//         static_cast<OpticalPointingImpl::ReductionStreamConsumer*>(reference)->
//             processData(data);
//     }
} // end Control namespace
#endif // OPTICALPOINTINGIMPL_H
