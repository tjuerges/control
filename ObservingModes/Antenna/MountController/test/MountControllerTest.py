#!/usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import sys
import unittest
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import Acspy.Clients.SimpleClient
import TMCDB_IDL;
import asdmIDLTypes;
import Control
import CCL.MountController
from math import radians, degrees
import time
import ModeControllerExceptions
import ControlExceptions
import Acspy.Common.ErrorTrace, Acspy.Common.Log
import exceptions
import maci
import Acspy.Common.TimeHelper
import PyDataModelEnumeration

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
    def doHorizonStrokes(self, mc, useQuanta=False):
        (longOff, latOff) = mc.getHorizonOffset();
        self.assertAlmostEqual(longOff, 0, 6)
        self.assertAlmostEqual(latOff, 0, 6)

        if (useQuanta):
            longOffset = '-2deg'; latOffset = '60arcmin'
        else:
            longOffset = radians(-2); latOffset = radians(1)
        mc.setHorizonOffset(longOffset, latOffset)
        (longOff, latOff) = mc.getHorizonOffset();
        self.assertAlmostEqual(longOff, radians(-2), 6)
        self.assertAlmostEqual(latOff, radians(1), 6)

        if (useQuanta):
            longOffset = '60arcmin'; latOffset = '-2deg'
        else:
            longOffset = radians(1); latOffset = radians(-2)
        mc.setHorizonOffsetAsync(longOffset, latOffset)
        mc.waitUntilOnSource()
        (longOff, latOff) = mc.getHorizonOffset();
        self.assertAlmostEqual(longOff,  radians(1), 6)
        self.assertAlmostEqual(latOff, radians(-2), 6)

        if (useQuanta):
            longOffset = '-120arcmin'; latOffset = '-4deg'
        else:
            longOffset = radians(-2); latOffset = radians(-4)
        timeNow = \
                Acspy.Common.TimeHelper.TimeUtil().py2epoch(time.time()).value;
        mc.setHorizonOffsetQueued(longOffset, latOffset, timeNow)
        mc.waitUntilOnSource()
        (longOff, latOff) = mc.getHorizonOffset();
        self.assertAlmostEqual(longOff,  radians(-2), 6)
        self.assertAlmostEqual(latOff, radians(-4), 6)

        if (useQuanta):
            longOffset = '-120arcmin'; latOffset = '-4deg'
            longVel = '12arcmin/s'; latVel = '0.4deg/s'
        else:
            longOffset = radians(-2); latOffset = radians(-4)
            longVel = radians(.2); latVel = radians(.4)
        mc.setHorizonLinearStroke(longVel, latVel,
                                  longOffset,  latOffset)
        time.sleep(15);
        (longOff, latOff) = mc.getHorizonOffset();
        self.assertTrue(latOff > 0)
        self.assertTrue(longOff > 0)
        mc.setHorizonOffset(0, 0)
        
    def doAzElStrokes(self, mc, useQuanta=False):
        (azOff, elOff) = mc.getHorizonOffset();
        self.assertAlmostEqual(azOff, 0, 6)
        self.assertAlmostEqual(elOff, 0, 6)

        if (useQuanta):
            azOffset = '-2deg'; elOffset = '60arcmin'
        else:
            azOffset = radians(-2); elOffset = radians(1)
        mc.setAzElOffset(azOffset, elOffset)
        (azOff, elOff) = mc.getHorizonOffset();
        self.assertAlmostEqual(azOff, radians(-2), 6)
        self.assertAlmostEqual(elOff, radians(1), 6)

        if (useQuanta):
            azOffset = '60arcmin'; elOffset = '-2deg'
        else:
            azOffset = radians(1); elOffset = radians(-2)
        mc.setAzElOffsetAsync(azOffset, elOffset)
        mc.waitUntilOnSource()
        (azOff, elOff) = mc.getHorizonOffset();
        self.assertAlmostEqual(azOff,  radians(1), 6)
        self.assertAlmostEqual(elOff, radians(-2), 6)

        if (useQuanta):
            azOffset = '-120arcmin'; elOffset = '-4deg'
        else:
            azOffset = radians(-2); elOffset = radians(-4)

        startTimeInSec = time.time() + 4.0;
        startTime=Acspy.Common.TimeHelper.TimeUtil().py2epoch(startTimeInSec);
        mc.setAzElOffsetQueued(azOffset, elOffset, startTime.value)
        t = 0;
        while (t < startTime.value):
            time.sleep(0.5)
            (az, el, t) = mc.actualAzEl();
        mc.waitUntilOnSource()

        (az, el, t) = mc.actualAzEl(); 
        self.assertAlmostEqual(az,  radians(98), 6)
        self.assertAlmostEqual(el, radians(16), 6)

        if (useQuanta):
            azOffset = '-120arcmin'; elOffset = '-4deg'
            azVel = '12arcmin/s'; elVel = '0.4deg/s'
        else:
            azOffset = radians(-2); elOffset = radians(-4)
            azVel = radians(.2); elVel = radians(.4)
        mc.setAzElLinearStroke(azVel, elVel, azOffset,  elOffset)
        time.sleep(15);
        (azOff, elOff) = mc.getHorizonOffset();
        self.assertTrue(elOff > 0)
        self.assertTrue(azOff > 0)
        mc.setAzElOffset(0, 0)
        
    def doEquatorialStrokes(self, mc, useQuanta=False):
        (longOff, latOff) = mc.getEquatorialOffset();
        self.assertAlmostEqual(longOff, 0, 6)
        self.assertAlmostEqual(latOff, 0, 6)

        if (useQuanta):
            longOffset = '-2deg'; latOffset = '60arcmin'
        else:
            longOffset = radians(-2); latOffset = radians(1)
        mc.setEquatorialOffset(longOffset, latOffset)
        (longOff, latOff) = mc.getEquatorialOffset()
        self.assertAlmostEqual(longOff, radians(-2), 6)
        self.assertAlmostEqual(latOff, radians(1), 6)

        if (useQuanta):
            longOffset = '3600arcsec'; latOffset = '-2deg'
        else:
            longOffset = radians(1); latOffset = radians(-2)
        mc.setEquatorialOffsetAsync(longOffset, latOffset)
        mc.waitUntilOnSource()
        (longOff, latOff) = mc.getEquatorialOffset();
        self.assertAlmostEqual(longOff,  radians(1), 6)
        self.assertAlmostEqual(latOff, radians(-2), 6)
        
        if (useQuanta):
            longOffset = '-120arcmin'; latOffset = '-4deg'
        else:
            longOffset = radians(-2); latOffset = radians(-4)
        timeNow = \
                Acspy.Common.TimeHelper.TimeUtil().py2epoch(time.time()).value;
        mc.setEquatorialOffsetQueued(longOffset, latOffset, timeNow)
        mc.waitUntilOnSource()
        (longOff, latOff) = mc.getEquatorialOffset();
        self.assertAlmostEqual(longOff,  radians(-2), 6)
        self.assertAlmostEqual(latOff, radians(-4), 6)

    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        mc = client.getDynamicComponent \
             (None, 'IDL:alma/Control/MountController:1.0',\
              None, 'CONTROL/DA41/cppContainer')
        client.releaseComponent(mc._get_name())

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        try:
            mc = None;
            try:
                tu = Acspy.Common.TimeHelper.TimeUtil()
                antennaName = master.getAvailableAntennas()[0]
                antennaComponentName = 'CONTROL/' + antennaName;
                antenna = client.getComponent(antennaComponentName)
                compName = antenna.getMountControllerName()
                client.releaseComponent(antenna._get_name())
# The library name is specified in the CDB in Components.xml
                compSpec = maci.ComponentSpec\
                           (compName,
                            'IDL:alma/Control/MountController:1.0', '*', '*')
                mc = client.\
                     getCollocatedComp(compSpec, False, antennaComponentName);
                self.failUnless(mc != None,
                                "Unable to create a MountController Component")
                mc.allocate(antennaName)
            
                componentName = mc.getMount();
                self.assertEqual(componentName,
                                 'CONTROL/' + antennaName + '/Mount');
            
                self.assertTrue(mc.isStopped())
                mc.track();
                self.assertFalse(mc.isStopped())
            
                defaultTolerance = mc.getTolerance();
                self.assertAlmostEquals(defaultTolerance, radians(10.0/60/60));
                mc.setTolerance(defaultTolerance*2.0);
                defaultTimeout = mc.getTimeout();
                self.assertAlmostEquals(defaultTimeout, 110.0);
                mc.setTimeout(defaultTimeout+20.0);

                self.assertAlmostEqual(degrees(mc.getElevationLimit()), 2.0);
                mc.setElevationLimit(radians(5.0));
                self.assertAlmostEqual(degrees(mc.getElevationLimit()), 5.0);
                mc.resetLimits();
                self.assertAlmostEqual(degrees(mc.getElevationLimit()), 2.0);
            
# Test the horizon source
                mc.setAzElAsync(radians(99), radians(19));
                mc.waitUntilOnSource();
                (az, el, t) = mc.commandedAzEl()
                self.assertAlmostEqual(degrees(az), 99, 4)
                self.assertAlmostEqual(degrees(el), 19, 4)
        
                mc.setAzEl(radians(100), radians(20));
                (az, el, t) = mc.actualAzEl()
                self.assertAlmostEqual(degrees(az), 100, 2)
                self.assertAlmostEqual(degrees(el), 20, 2)
                self.assertFalse(mc.timeToSet() < 0)
        
                (a, b) = mc.getRefractionCoefficients()
                self.assertAlmostEqual(a, 0, 6)
                self.assertAlmostEqual(b, 0, 6)
# Test the horizon offset commands (with a horizon source)
#               self.doHorizonStrokes(mc)
                self.doAzElStrokes(mc)
        
# Test the equatorial source
                (az, el) = mc.toAzElEquatorial(radians(359), radians(-80.0), \
                                               0, 0, 0)
                self.assertTrue(el > 0)
                (az, el) = mc.toAzElEquatorial(radians(1), radians(87.0), \
                                               0, 0, 0)
                self.assertTrue(el < 0)
                self.assertTrue(mc.timeToSetEquatorial\
                                (radians(359), radians(-80.0),0,0,0) > 0.0)
                self.assertFalse(mc.timeToRiseEquatorial\
                                 (radians(359), radians(-80.0),0,0,0) < 0.0)
                self.assertTrue(mc.isObservableEquatorial\
                                (radians(359), radians(-80.0), 10*60, 0, 0, 0));
                self.assertFalse(mc.isObservableEquatorial\
                                 (radians(270), radians(89.9), 0, 0, 0, 0));
                mc.setDirectionAsync(radians(10.0), radians(-80.0), 0, 0, 0);
                self.assertTrue(mc.timeToSource() > 0.0)
                mc.waitUntilOnSource();
                self.assertTrue(mc.timeToSource() < 0.001)
                (ra, dec, t) = mc.commandedRADec()
                self.assertAlmostEqual(degrees(ra), 10, 4)
                self.assertAlmostEqual(degrees(dec), -80, 4)
                self.assertFalse(mc.timeToSet() < 0)
      
                self.assertAlmostEqual(mc.getEpoch(), 2000.0, 1)
                mc.setEpoch(2005.5);
                mc.setDirection(radians(2.0), radians(-81), 0, 0, 0);
                (ra, dec, t) = mc.actualRADec()
                self.assertAlmostEqual(mc.getEpoch(), 2005.5, 1)
                self.assertAlmostEqual(degrees(ra), 1.9, 1)
                self.assertAlmostEqual(degrees(dec), -81.0, 1)
# Test the horizon and equatorial offset commands (with an equatorial source)
#               self.doHorizonStrokes(mc)
#               self.doEquatorialStrokes(mc)
      
# Test the collection of pointing data
                startTime = tu.py2epoch(time.time()).value;
                startTime += tu.py2duration(5).value;
                scanTime = 18;
                stopTime = startTime + tu.py2duration(scanTime).value;
                to = mc.startPointingDataCollection(startTime, stopTime);
                self.assertTrue(to > scanTime and to < scanTime + 15)
                data = mc.getPointingDataTable(to)
                self.assertTrue(len(data) == 375);
                data = mc.collectPointingData(scanTime)
                self.assertTrue(len(data) == 375);
      
# Test the planetary source
                mc.setObservingFrequency(40.12345E9)
                planets = ['mercury', 'venus', 'mars', 'jupiter', \
                           'saturn', 'uranus', 'neptune', 'pluto', 'sun', 'moon']
                i = 0
                while (i < len(planets)) and \
                          (mc.toAzElPlanet(planets[i])[1] <= radians(5)):
                    i = i + 1;
                if (i < len(planets)):
                    self.assertTrue(mc.toAzElPlanet(planets[i])[1] > radians(5))
                    self.assertFalse(mc.timeToRisePlanet(planets[i]) > 0.0)
                    self.assertTrue(mc.timeToSetPlanet(planets[i]) > 0.0)
                    self.assertTrue(mc.isObservablePlanet(planets[i], 10*60))
                    mc.setPlanet(planets[i]);
                    (ra, dec, t) = mc.commandedRADec()
                    self.assertTrue(degrees(abs(dec)) < 30.0);
                    self.assertFalse(mc.timeToSet() < 0)

                (a, b) = mc.getRefractionCoefficients()
                self.assertTrue(abs(a) > 1E-6 and abs(a) < 1E-2)
                self.assertTrue(abs(b) > 1E-9 and abs(b) < 1E-4)
# Test the horizon and equatorial offset commands (with an planetary source)
#               self.doHorizonStrokes(mc)
#               self.doEquatorialStrokes(mc)

# Test the ephemeris source
                mc.setObservingFrequency(400.12345E9)
                ep = CCL.MountController.Ephemeris();
                timeNow = tu.py2epoch(time.time()).value;
                ra = radians(-90);
                dec = radians(-80);
                dist = 4E12;# m
                i = 0;
                while (i < 10):
                    ep.addRow(timeNow, ra, dec, dist);
                    timeNow += tu.py2duration(60).value;
                    ra += radians(.5); dec -= radians(.1);
                    i += 1
                mc.setEphemeris(ep.getData());

# Test the horizon and equatorial offset commands (with an ephemeris source)
#               self.doHorizonStrokes(mc)
#               self.doEquatorialStrokes(mc)

#               mc.maintenanceStow();
#               mc.survivalStow();
                pol = PyDataModelEnumeration.PyPolarizationType.X
                band = PyDataModelEnumeration.PyReceiverBand.ALMA_RB_03
                pcr = Control.PointingCalResult(antennaName, pol, \
                                                0.1, 0.0, 0.2, 0.0, 0.3, 0.0, \
                                                0.4, 0.0)
                mc.applyPointingCalibrationResult(pcr)
                pcrbad = Control.PointingCalResult(antennaName, pol, \
                                                   10*60*60, 0., 0.2, 0., \
                                                   .3, 0., .4, 0.)
                self.assertRaises(ControlExceptions.IllegalParameterErrorEx, \
                                  mc.applyPointingCalibrationResult,\
                                  pcrbad);
                
                fcr = Control.FocusCalResult(antennaName, pol, band, \
                                             -0.0055, 0.0, 0.0, 0.0, 0.0, 0.0, \
                                             False, True, True)
                mc.applyFocusCalibrationResult(fcr)
            finally:
                if (mc != None):
                    if (str(mc.getStatus()) != 'UNINITIALIZED'):
                        mc.stop();
                    client.releaseComponent(mc._get_name())
        except (ModeControllerExceptions.MountFaultEx,
                ControlExceptions.IllegalParameterErrorEx,
                ModeControllerExceptions.TimeoutEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger())
            raise

    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        try:
            mc = None;
            try:
                tu = Acspy.Common.TimeHelper.TimeUtil()
                antennaName = master.getAvailableAntennas()[0]
                mc = CCL.MountController.MountController(antennaName);
                
                self.assertTrue(mc.isStopped())
                mc.track();
                self.assertFalse(mc.isStopped())
                
                defaultTolerance = mc.getTolerance();
                self.assertAlmostEquals(defaultTolerance, radians(10.0/60/60));
                mc.setTolerance('20arcsec');
                defaultTimeout = mc.getTimeout();
                self.assertAlmostEquals(defaultTimeout, 110.0);
                mc.setTimeout(defaultTimeout+20.0);
                
                self.assertAlmostEqual(degrees(mc.getElevationLimit()), 2.0);
                mc.setElevationLimit('5 deg');
                self.assertAlmostEqual(degrees(mc.getElevationLimit()), 5.0);
                mc.resetLimits();
                self.assertAlmostEqual(degrees(mc.getElevationLimit()), 2.0);
            
# Test the horizon source
                mc.setAzElAsync('99deg', '19deg');
                mc.waitUntilOnSource();
                (az, el, t) = mc.commandedAzEl()
                self.assertAlmostEqual(degrees(az), 99, 4)
                self.assertAlmostEqual(degrees(el), 19, 4)
                
                mc.setAzEl('100.0.0', '20.0.0');
                (az, el, t) = mc.actualAzEl()
                self.assertAlmostEqual(degrees(az), 100, 2)
                self.assertAlmostEqual(degrees(el), 20, 2)
                self.assertFalse(mc.timeToSet() < 0)
                
                (a, b) = mc.getRefractionCoefficients()
                self.assertAlmostEqual(a, 0, 6)
                self.assertAlmostEqual(b, 0, 6)
# Test the horizon offset commands (with a horizon source)
                self.doHorizonStrokes(mc, True)
                self.doAzElStrokes(mc, True)
                
# Test the equatorial source
                (az, el) = mc.toAzElEquatorial("359deg", "-80.0deg")
                self.assertTrue(el > 0)
                (az, el) = mc.toAzElEquatorial("1deg", "87.0deg")
                self.assertTrue(el < 0)
                self.assertTrue(mc.timeToSetEquatorial("359deg", "-80deg") \
                                > 0.0)
                self.assertFalse(mc.timeToRiseEquatorial("359deg", "-80deg") \
                                 < 0.0)
                self.assertTrue(mc.isObservableEquatorial\
                                ("359deg", "-80.0deg", "10min"));
                self.assertFalse(mc.isObservableEquatorial\
                                 ("270deg", "89.9deg"));
                
                mc.setDirectionAsync('10.0 deg', '-80.0 deg');
                self.assertTrue(mc.timeToSource() > 0.0)
                mc.waitUntilOnSource();
                self.assertTrue(mc.timeToSource() < 0.001)
                (ra, dec, t) = mc.commandedRADec()
                self.assertAlmostEqual(degrees(ra), 10, 4)
                self.assertAlmostEqual(degrees(dec), -80, 4)
                self.assertFalse(mc.timeToSet() < 0)
                
                self.assertAlmostEqual(mc.getEpoch(), 2000.0, 1)
                mc.setEpoch(2005.5);
                mc.setDirection('2.0.0', '-81.0.0');
                (ra, dec, t) = mc.actualRADec()
                self.assertAlmostEqual(mc.getEpoch(), 2005.5, 1)
                self.assertAlmostEqual(degrees(ra), 1.9, 1)
                self.assertAlmostEqual(degrees(dec), -81.0, 1)
# Test the horizon and equatorial offset commands (with an equatorial source)
                self.doHorizonStrokes(mc, True)
                self.doEquatorialStrokes(mc)
                
# Test the collection of pointing data
                startTime = tu.py2epoch(time.time()).value;
                startTime += tu.py2duration(5).value;
                scanTime = 18;
                stopTime = startTime + tu.py2duration(scanTime).value;
                to = mc.startPointingDataCollection(startTime, stopTime);
                self.assertTrue(to > scanTime and to < scanTime + 15)
                data = mc.getPointingDataTable(to)
                self.assertTrue(len(data) == 375);
                data = mc.collectPointingData(scanTime)
                self.assertTrue(len(data) == 375);
            
# Test the planetary source
                mc.setObservingFrequency("40.12345 GHz")
                planets = ['mercury', 'venus', 'mars', 'jupiter', \
                           'saturn', 'uranus', 'neptune', 'pluto', 'sun', 'moon']
                i = 0
                while (i < len(planets)) and \
                          (mc.toAzElPlanet(planets[i])[1] <= radians(5)):
                    i = i + 1;
                if (i < len(planets)):
                    self.assertTrue(mc.toAzElPlanet(planets[i])[1] > radians(5))
                    self.assertFalse(mc.timeToRisePlanet(planets[i]) > 0.0)
                    self.assertTrue(mc.timeToSetPlanet(planets[i]) > 0.0)
                    self.assertTrue(mc.isObservablePlanet(planets[i], 10*60))
                    mc.setPlanet(planets[i]);
                    (ra, dec, t) = mc.commandedRADec()
                    self.assertTrue(degrees(abs(dec)) < 30.0);
                    self.assertFalse(mc.timeToSet() < 0)

                (a, b) = mc.getRefractionCoefficients()
                self.assertTrue(abs(a) > 1E-6 and abs(a) < 1E-2)
                self.assertTrue(abs(b) > 1E-9 and abs(b) < 1E-4)
# Test the horizon and equatorial offset commands (with an planetary source)
                self.doHorizonStrokes(mc, True)
                self.doEquatorialStrokes(mc)
                
# Test the ephemeris source
                mc.setObservingFrequency("3 mm")
                ep = CCL.MountController.Ephemeris();
                timeNow = tu.py2epoch(time.time()).value;
                ra = radians(-90);
                dec = radians(-80);
                dist = 4E12;# m
                i = 0;
                while (i < 10):
                    ep.addRow(timeNow, ra, dec, dist);
                    timeNow += tu.py2duration(60).value;
                    ra += radians(.5); dec -= radians(.1);
                    i += 1
                mc.setEphemeris(ep);
# Test the horizon and equatorial offset commands (with an ephemeris source)
                self.doHorizonStrokes(mc, True)
                self.doEquatorialStrokes(mc)
#                mc.maintenanceStow();
#                mc.survivalStow();
            finally:
                if (mc != None):
                    mc.stop();
        except (ModeControllerExceptions.MountFaultEx,
                ControlExceptions.IllegalParameterErrorEx,
                ModeControllerExceptions.TimeoutEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger())
            raise
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':

# Bring the control subsystem operational
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    master = client.getComponent('CONTROL/MASTER')
    shutdownMasterAtEnd = False;
    if (master.getMasterState() == Control.INACCESSIBLE):
        master.startupPass1()
        master.startupPass2()
        shutdownMasterAtEnd = True;
    if (master.getMasterState() != Control.OPERATIONAL):
        print 'Could not bring the control subsystem to an operational state'
        exit(1)

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

# Shutdown the control system if we started it up
    if shutdownMasterAtEnd:
        master.shutdownPass1()
        master.shutdownPass2()
    client.releaseComponent(master._get_name()) 
