#include <iostream> 
#include <string>
#include <dlfcn.h>
#include <Time_Value.h>
#include <ACE.h>

using namespace std;
typedef bool (*DLLOpenFunc)(int argc, char *argv[]);

int main(int argc, char* argv[]) {
    if (argc <= 1) {
        exit(0);
    }
    string filename = argv[1];
    cerr << "file = " << filename << endl;
    ACE_Time_Value startTime = ACE_OS::gettimeofday();
    //    void* mylib = dlopen(filename.c_str(), RTLD_GLOBAL | RTLD_NOW);
    void* mylib = dlopen(filename.c_str(), RTLD_GLOBAL|RTLD_LAZY);
    ACE_Time_Value stopTime = ACE_OS::gettimeofday(); stopTime -= startTime;
    cerr << "Took " << stopTime.msec()/1000.0 << " seconds to call dlopen." 
         << endl;
    DLLOpenFunc dllOpen = (DLLOpenFunc) dlsym(mylib, "DLLOpen");
    if (dllOpen != 0) {
        cerr << "Calling dllOpen" << endl;
        bool retVal = dllOpen(argc, argv);
    }
    cerr << "Calling dlclose " << endl;
    int eret = dlclose(mylib);
    cerr << "eret = " << eret << endl;
}
