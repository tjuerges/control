// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include <iostream>
#include <ASDMAll.h>
#include <vector>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[]) {
    // Create an ASDM object attached to the specified (currently hardcoded)
    // file. 
    // TODO: make the filename a command line parameter.
    asdm::ASDM myASDM;
    myASDM.setFromFile("uid___X1e1_Xb66_X1");

    // Get a PointingTable object
    asdm::PointingTable& pointing = myASDM.getPointing();
    vector<asdm::PointingRow *> rows(pointing.get());
    cerr << "Pointing table contains " << rows.size() << " rows." << endl;

    // Print each row to standard out 
    // TODO print all cols and not just the ones I'm currently interested in.
    for (unsigned int r = 0; r < rows.size(); r++) {
        asdm::PointingRow* thisRow = rows[r];
        vector<vector<asdm::Angle> > target(thisRow->getTarget());
        vector<vector<asdm::Angle> > offset(thisRow->getOffset());
        vector<vector<asdm::Angle> > pointingDirection(thisRow->getPointingDirection());
        //        vector<asdm::Angle> enc(thisRow->getEncoder());
        vector<vector<asdm::Angle> > sourceOffset(thisRow->getSourceOffset());
        asdm::ArrayTime timeOrigin(thisRow->getTimeOrigin());
        cout << setprecision(14) << timeOrigin.getMJD() << setprecision(9)
             << " " << target[0][0] << " " << target[0][1] 
             << " " << offset[0][0] << " " << offset[0][1] 
             << " " << pointingDirection[0][0] << " " << pointingDirection[0][1] 
            //             << " " << enc[0] << " " << enc[1]
             << " " << sourceOffset[0][0] << " " << sourceOffset[0][1] 
             << endl;
    }
}
