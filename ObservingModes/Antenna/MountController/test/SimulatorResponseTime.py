# An ad-hoc script for examining the response time of the AMBSimulatr.
# Set the SNIFF flag before compiling the AMBLBsimulator to generate a
# log that can be used by this script.
import acstime
import Acspy.Common.EpochHelper
f = file('tmp/simulator.log')
d = f.readlines()
f.close();
ymd = '2007-01-01T'
ephb = Acspy.Common.EpochHelper.EpochHelper()
ephe = Acspy.Common.EpochHelper.EpochHelper()
mx = 0.0;
mn = 1E9;
sm = 0;
ct = 0;
for l in range(len(d)):
    db = d[l].split()
    if ( (l+1 < len(d)) and db[len(db)-1] == 'request'):
        ephb.fromString(acstime.TSArray, ymd+db[0])
        de = d[l+1].split()
        ephe.fromString(acstime.TSArray, ymd+de[0])
        rt = (ephe.value().value - ephb.value().value)/10.0
        sm = sm + rt
        if (rt > mx):
            mx = rt
        if (rt < mn):
            mn = rt
        ct = ct + 1;
        if (rt > 1000):
            print d[l], d[l+1]

print 'max', mx, 'min', mn, 'average', sm/ct, ' micro-seconds over ', ct, ' monitor points'
