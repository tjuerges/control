#include <acstimeEpochHelper.h> // for EpochHelper
#include <acstimeTimeUtil.h> // for TimeUtil
#include <TETimeUtil.h> // for TETimeUtil
#include <casacore/casa/aips.h> // basic casa definitions (like casa::Double)
#include <casacore/casa/Arrays/Vector.h> // for casa::Vector
#include <casacore/casa/Quanta/Euler.h> // for casa::Euler
#include <casacore/casa/Quanta/MVAngle.h> // for casa::MVAngle
#include <casacore/casa/Quanta/MVEpoch.h> // for casa::MVEpoch
#include <casacore/casa/Quanta/MVPosition.h> // for casa::MVPosition
#include <casacore/casa/Quanta/Quantum.h> // for casa::Quantity
#include <casacore/casa/Quanta/RotMatrix.h> // for casa::RotMatrix
#include <casacore/casa/OS/Path.h> // for casa::Path
#include <casacore/measures/Measures/MDirection.h> // for casa::MDirection
#include <casacore/measures/Measures/MEpoch.h> // for casa::MEpoch
#include <casacore/measures/Measures/MPosition.h> // for casa::MPosition
#include <casacore/measures/Measures/MeasConvert.h> // for casa::MeasConvert
#include <casacore/measures/Measures/MeasComet.h> // for casa::MeasComet
#include <casacore/measures/Measures/MeasIERS.h> // for casa::MeasIERS
#include <casacore/tables/Tables/ScaColDesc.h> // for casa::ScalaColumnDesc
#include <casacore/tables/Tables/ScalarColumn.h> // for casa::ScalarColumn
#include <casacore/tables/Tables/SetupNewTab.h> // for casa::SetupNewTable
#include <casacore/tables/Tables/Table.h> // for casa::Table
#include <casacore/tables/Tables/TableDesc.h> // for casa::TableDesc
#include <casacore/tables/Tables/TableRecord.h> // for casa::TableDesc
#include <casacore/measures/Measures/MCDirection.h> // Needed to work around a bug in CASA-3.0.0
#include <casacore/measures/Measures/MCPosition.h> // Needed to work around a bug in CASA-3.0.0
#include <casacore/measures/Measures/MCEpoch.h> // Needed to work around a bug in CASA-3.0.0
#include <iostream> // for std::cout, std::cerr & std::endl
#include <iomanip> // for std::setprecision
#include <unistd.h> // for usleep


using namespace std;

casa::Table createCometTable(const casa::Vector<casa::Double>& mjd, 
                             const casa::Vector<casa::Double>& ra,
                             const casa::Vector<casa::Double>& dec,
                             const casa::Vector<casa::Double>& rho) {
  // Check that all the input vectors have the same size.
  const unsigned int nRow = mjd.size();
  if (nRow < 1 || 
      ra.size() != nRow ||
      dec.size() != nRow ||
      rho.size() != nRow) {
    // GIGO
    return casa::Table();
  }

  // Now create a table containing this user defined Ephemeris 
  casa::TableDesc td("", "1", casa::TableDesc::Scratch);
  td.addColumn(casa::ScalarColumnDesc<casa::Double>("MJD"));
  td.addColumn(casa::ScalarColumnDesc<casa::Double>("RA"));
  td.addColumn(casa::ScalarColumnDesc<casa::Double>("DEC"));
  td.addColumn(casa::ScalarColumnDesc<casa::Double>("Rho"));
  td.addColumn(casa::ScalarColumnDesc<casa::Double>("RadVel"));
  td.addColumn(casa::ScalarColumnDesc<casa::Double>("DiskLong"));
  td.addColumn(casa::ScalarColumnDesc<casa::Double>("DiskLat"));
  casa::SetupNewTable newTab("UserEphemeris", td, casa::Table::Scratch);
  // eventually this will be a memory table but for now its a plain table
  casa::Table userEphTab(newTab, casa::Table::Plain, nRow);

  // Now fill the columns (and column keywords)
  casa::ScalarColumn<casa::Double> sc;
  {
    sc.attach(userEphTab, "MJD");
    sc.putColumn(mjd);
    sc.rwKeywordSet().define("UNIT", "d");
  }
  {
    sc.attach(userEphTab, "RA");
    sc.putColumn(ra);
    sc.rwKeywordSet().define("UNIT", "deg");
  }
  {
    sc.attach(userEphTab, "DEC");
    sc.putColumn(dec);
    sc.rwKeywordSet().define("UNIT", "deg");
  }
  {
    sc.attach(userEphTab, "Rho");
    sc.putColumn(rho);
    sc.rwKeywordSet().define("UNIT", "m");
  }
  // The remaining columsn are unused (so set them to zero)
  const casa::Vector<casa::Double> zero(nRow, 0.0);
  {
    sc.attach(userEphTab, "RadVel");
    sc.putColumn(zero);
    sc.rwKeywordSet().define("UNIT", "m/s");
  }
  {
    sc.attach(userEphTab, "DiskLat");
    sc.putColumn(zero);
    sc.rwKeywordSet().define("UNIT", "rad");
  }
  {
    sc.attach(userEphTab, "DiskLong");
    sc.putColumn(zero);
    sc.rwKeywordSet().define("UNIT", "rad");
  }
  { // Fill the table keywords
    casa::TableRecord& tkw = userEphTab.rwKeywordSet();
    if (nRow > 1) {
      const double tdelta = mjd(1) - mjd(0);
      tkw.define("MJD0", mjd(0) - tdelta);
      tkw.define("dMJD", tdelta);
    } else { // nRow == 1
      const double tdelta = mjd(0)/10.0;
      tkw.define("MJD0", mjd(0) - tdelta);
      tkw.define("dMJD", tdelta);
    }
    tkw.define("NAME", "User Specified Ephemeris");
    // Always use a geocentric reference
    tkw.define("GeoLong", 0.0);
    tkw.define("GeoLat", 0.0);
    tkw.define("GeoDist", 0.0);
    // These are arbitrary strings
    EpochHelper().toUTCdate(0, 0);

    const string today = TETimeUtil::toTimeDateString
      (TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
    tkw.define("VS_CREATE", today.c_str());
    tkw.define("VS_DATE", today.c_str());
    tkw.define("VS_VERSION", "1.0");
    tkw.define("VS_TYPE", "Table of comet/planetary positions");
  }
  { // Fill the table info
    casa::TableInfo& ti = userEphTab.tableInfo();
    ti.setType("IERS");
    ti.setSubType("Comet");
  }
  userEphTab.flush();
  return userEphTab;
}

int main(int argc, char *argv[]) {

  { // Test 1. Conversion a position to latitude/long 
    // (including earth's oblateness) and addition of pad and antenna positions

  // First define a position on the earth (the Vertex prototype antenna)
  const double padX = -1601361.555455;
  const double padY = -5042191.805932;
  const double padZ = 3554531.803007;
  cout << casa::Quantity(1, "rad/s").getValue("rad/yr") << endl;

  casa::MPosition 
    padLocation(casa::MVPosition(padX, padY, padZ), casa::MPosition::ITRF);
  cout << "Pad Location (ITRF)" << setprecision(15) << endl
       << "Latitude: " << padLocation.getValue().getLat("deg") << endl
       << "Longitude: " << padLocation.getValue().getLong("deg") << endl
       << "Altitude: " << padLocation.getValue().getLength("m") << endl;

  // Now convert it to WGS84 and get the latitude and longitude 
  casa::MVPosition padLocationWGS = 
    casa::MeasConvert<casa::MPosition>(padLocation,
                                       casa::MPosition::WGS84)().getValue();
  cout << "Pad Location (WGS84)" << setprecision(15) << endl
       << "Latitude: " << padLocationWGS.getLat("deg") << endl
       << "Longitude: " <<  padLocationWGS.getLong("deg") << endl
       << "Altitude: " <<  padLocationWGS.getLength("m") << endl;
  const double padLat = padLocationWGS.getLat("rad").getValue();
  const double padLong = padLocationWGS.getLong("rad").getValue();

  // Now define the position of the antenna with respect to the pad
  const double antEast = 1;
  const double antNorth = 2;
  const double antUp = 10;

  // Now rotate (using the pad latitude and longitude) this local position to
  // one in the (X,Y,Z) reference frame.

  // The minus signs and 90-angle terms were determined empirically
  casa::MVPosition antLocation(-1*antEast, -1*antNorth, antUp);
  antLocation *= 
    casa::RotMatrix(casa::Euler(M_PI_2-padLat, 0.0, M_PI_2-padLong));

  // Now that both the pad and the antenna are in the same frame we can add
  // them
  antLocation += padLocation.getValue();

  // Display the result in the WGS84 frame
  casa::MVPosition antLocationWGS = 
    casa::MeasConvert<casa::MPosition>
    (casa::MPosition(antLocation, casa::MPosition::ITRF),
     casa::MPosition::WGS84)().getValue();
  cout << "Antenna Location (WGS84)" << endl
       << "Latitude: " << casa::MVAngle(antLocationWGS.getLat()).string(casa::MVAngle::ANGLE, 9) << endl
       << "Longitude: " << casa::MVAngle(antLocationWGS.getLong()).string(casa::MVAngle::ANGLE, 9) << endl
       << "Altitude: " <<  antLocationWGS.getLength("m") << endl;
  }
  { // Test. Convert from TAI to UTC (difference should be exactly 33 seconds)
    const double mjd = 
      EpochHelper(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).toUTCdate(0, 0);
    casa::MEpoch mep(casa::MVEpoch(casa::Quantity(mjd, "d")), 
                     casa::MEpoch::TAI);
    const double inTAI = mep.getValue().getTime("s").getValue();
    mep = casa::MeasConvert<casa::MEpoch>(mep, casa::MEpoch::UTC)();
    const double inUTC = mep.getValue().getTime("s").getValue();
    cout << "TAI-UTC is " <<  inTAI - inUTC << " secs." << endl;
  }
  { // Test 3. Get earth orientation data 
    const double mjd = 
      EpochHelper(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).toUTCdate(0, 0);
    double dUT;
    casa::MeasIERS::get(dUT, casa::MeasIERS::PREDICTED,
                        casa::MeasIERS::dUT1, mjd);
    cout << "dUT1: " << dUT << endl;
    double polX, polY;
    casa::MeasIERS::get(polX, casa::MeasIERS::PREDICTED,
                        casa::MeasIERS::X, mjd);
    casa::MeasIERS::get(polY, casa::MeasIERS::PREDICTED,
                        casa::MeasIERS::Y, mjd);
    cout << "Polar motion (X,Y): " << polX << " " << polY << endl;
  }
  { // Test 4. Compute a planets position.
    // First define a position on the earth (the Vertex prototype antenna)
    const double x = -1601361.555455;
    const double y = -5042191.805932;
    const double z = 3554531.803007;
 
    // Next define a time (now on this computer)
    double mjd = 
      EpochHelper(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).toUTCdate(0, 0);
 
    // Put these together as a reference frame.
    casa::MeasFrame 
      mf(casa::MPosition(casa::MVPosition(x, y, z), casa::MPosition::ITRF), 
         casa::MEpoch(casa::MVEpoch(mjd), casa::MEpoch::UTC));
 
    // Now create a conversion object for the specified planet
    casa::MeasConvert<casa::MDirection> 
      mc(casa::MDirection::VENUS,
         casa::MeasRef<casa::MDirection>(casa::MDirection::J2000, mf));
 
    casa::MDirection md; // Should do an initial unnecessary conversion as the
                         // first one takes around 50 times longer

    EpochHelper eph;
    EpochHelper timeNow;
 
    for (int i = 0; i < 10; i++) {
      timeNow.value(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
      eph.value(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
      mf.resetEpoch(eph.toUTCdate(0, 0));
      md = mc();
      {
        EpochHelper timeEnd(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
        DurationHelper execTime(timeEnd.difference(timeNow.value()));
        ostringstream msg;
        msg << "Conversion took " << execTime.toSeconds()*1000
            << " milli-seconds.";
        cout << msg.str();
      }
   
      string ra = 
        casa::MVAngle(md.getValue().getLong()).string(casa::MVAngle::TIME, 9);
      string dec = 
        casa::MVAngle(md.getValue().getLat()).string(casa::MVAngle::ANGLE, 9);
      cout << " Venus (" << ra << ", " <<  dec << ")" << endl;
      usleep(1E6);
    }
  }
  { // Test 5. Compute using a user defined ephemeris.
    // First define a position on the earth (the Vertex prototype antenna)
    const double x = -1601361.555455;
    const double y = -5042191.805932;
    const double z = 3554531.803007;
    
    // Next define a time (now on this computer)
    double mjd = 
      EpochHelper(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).toUTCdate(0, 0);
    
    // Put these together as a reference frame.
    casa::MeasFrame 
      mf(casa::MPosition(casa::MVPosition(x, y, z), casa::MPosition::ITRF), 
         casa::MEpoch(casa::MVEpoch(mjd), casa::MEpoch::UTC));
    
    // Now create a user ephemeris
    const int nRow = 10;
    double cra = M_PI/180*15, cdec = 1, crho = 1E6, tdelta = 0.1;
    casa::Vector<casa::Double> time(nRow), ra(nRow), dec(nRow), rho(nRow);
    casa::Vector<casa::Double> radVel(nRow), diskLat(nRow), diskLong(nRow);
    for (int i = 0; i < nRow; i++) {
      time(i) = mjd; mjd += tdelta; 
      ra(i) = cra; cra += .1;
      dec(i) = cdec*180/M_PI; cdec -= .01;
      rho(i) = crho; crho += 1E4;
      radVel(i) = diskLat(i) = diskLong(i) = 0.0;
    }

    casa::Table userEphTab = createCometTable(time, ra, dec, rho);
    userEphTab.flush();

//     cerr << "Is a Table called " << userEphTab.tableName()
//          << " readable? " << 
//       (casa::Table::isReadable(userEphTab.tableName()) == true ? "Yes" : "No") 
//          << endl;

    // Eventually I will be able to just pass in the table (and not its name)
    casa::MeasComet comet(casa::Path(userEphTab.tableName()).baseName());

    mf.set(comet);
    // Now create a conversion object for the comet
    casa::MeasConvert<casa::MDirection> 
      mc(casa::MDirection::Ref(casa::MDirection::COMET, mf),
         casa::MDirection::J2000);
    
    casa::MDirection md; // Should do an initial unnecessary conversion as the
    // first one takes around 50 times longer
  
    EpochHelper eph;
    EpochHelper timeNow;
    
    for (int i = 0; i < 10; i++) {
      timeNow.value(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
      eph.value(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
      mf.resetEpoch(eph.toUTCdate(0, 0));
      md = mc();
      {
        EpochHelper timeEnd(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
        DurationHelper execTime(timeEnd.difference(timeNow.value()));
        ostringstream msg;
        msg << "Conversion took " << execTime.toSeconds()*1000
            << " milli-seconds.";
        cout << msg.str();
      }
      
      string ra = 
        casa::MVAngle(md.getValue().getLong()).string(casa::MVAngle::TIME, 9);
      string dec = 
        casa::MVAngle(md.getValue().getLat()).string(casa::MVAngle::ANGLE, 9);
      cout << " RA, Dec are (" << ra << ", " <<  dec << ")" << endl;
      usleep(1E6);
    }
  }
}
