#include <slalib.h>
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

// The values listed calculated below were carefully checked against MICA by
// Ralph Marson, Darrell Emerson and Jeff Mangum. Differences were less than
// 50mas. To do the comparison against MICA the refraction correction had to be
// turned off, by setting the presssure and releative humidity to zero, and the
// polar motion terms were set to zero. If this test ever fails it indicates a
// problem in the SLA functions we are relying on or the math units of the
// computer doing the calculations.

int main() {
  // 1. Convert mean position to apparent and print result
  const double rm = 0.0; // mean RA
  const double dm = 0.0; // mean Dec
  const double pr = 0.0; // proper motion RA
  const double pd = 0.0; // proper motion Dec
  const double px = 0.0; // parallax
  const double rv = 0.0; // radial velocity
  const double eq = 2000.0; // epoch of mean postion
  const double date = 54385.0; // 2007-10-12T00:00:00
  double ra, da; // apparent RA and Dec
  slaMap(rm, dm, pr, pd, px, rv, eq, date, &ra, &da);
  cout << "Apparent RA: " << setprecision(12) 
       << ra*12/M_PI << " (hrs) Dec: " << da*180/M_PI << " (deg)" << endl;

  // 1.5 Do this using the quick routines (as thats what we really use)
  vector<double> amprms(21);
  slaMappa(eq, date, &amprms[0]);
  slaMapqk(rm, dm, pr, pd, px, rv, &amprms[0], &ra, &da);
  cout << "   Quick RA: " << setprecision(12) 
       << ra*12/M_PI << " (hrs) Dec: " << da*180/M_PI << " (deg)" << endl;

  // 2. Convert apparent to observed and print result
  const double dut = -0.199; // UT1 - UTC
  const double deg2rad = M_PI/180.0; // conversion factor
  const double elongm = -107.619450000003*deg2rad; // telescope longitude (rad)
  const double phim = 34.0749445321981*deg2rad; // telescope latitude (rad)
  const double hm = 2135.4233443684; // telescope height (m)
  const double as2rad = deg2rad/60/60; // conversion factor
  const double xp = 0.099 * as2rad; // polar motion x (rad)
  const double yp = 0.196 * as2rad; // polar motion y (rad)
  const double tdk = 15.0 + 273.15; // Ambient temperature (deg K)
  const double pmb = 790.0; // Pressure (mBar). Set to 0 to turn off refraction
  const double rh = 0.2; // relative humidity. Set to 0 to turn off refraction
  const double wl = 299792458.0/104E9*1E6; // Observing wavelength (um)
  const double tlr = 0.0065; // lapse rate (Kelvins per meter)
  double aob, zob, hob, dob, rob; // observed Az, ZD, HA, Dec, RA.
  slaAop(ra, da, date, dut, elongm, phim, hm, xp, yp, tdk, pmb, rh, wl, tlr,
         &aob, &zob, &hob, &dob, &rob);
  cout << "Observed Az: " << setprecision(12) << aob*180/M_PI 
       << " (deg) El: " << (M_PI_2 - zob)*180/M_PI << " (deg)" << endl;

  // 2.5 Do this using the quick routines (as thats what we really use)
  vector<double> aoprms(14);
  slaAoppa(date, dut, elongm, phim, hm, xp, yp, tdk, pmb, rh, wl, tlr, 
           &aoprms[0]);
  slaAopqk(ra, da, &aoprms[0], &aob, &zob, &hob, &dob, &rob);
  cout << "   Quick Az: " << setprecision(12) << aob*180/M_PI 
       << " (deg) El: " << (M_PI_2 - zob)*180/M_PI << " (deg)" << endl;

}
