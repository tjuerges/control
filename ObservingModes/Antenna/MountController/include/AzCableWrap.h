// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef AZCABLEWRAP_H
#define AZCABLEWRAP_H
// Base class(es)
#include <loggingLoggable.h>

// Forward declarations for classes that this component uses

//includes for data members

namespace Control {
    class AzCableWrap : public Logging::Loggable { 
    public:
	// ------------------- Constructor & Destructor -------------------
	
	/// 
	AzCableWrap(Logging::Logger::LoggerSmartPtr logger);
	
	// Copy constructor (copy semantics).
	AzCableWrap(const AzCableWrap& other);
	
	/// Assignment (copy semantics).
	AzCableWrap& operator=(const AzCableWrap& other);
	
	// --------------------- C++ public functions ---------------------
	
        double unwrap(const double& az);
        
        void setCurrentPosition(const double& az, const double& el); 

    private:
        double az_m;

    }; // end AzCableWrap class
} // end Control namespace

#endif // AZCABLEWRAP_H
