// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef MOUNTCONTROLLERIMPL_H
#define MOUNTCONTROLLERIMPL_H

// Base class(es)
#include <antModeControllerImpl.h>

//CORBA servant header
#include <MountControllerS.h>

// Forward declarations for classes that this component uses
namespace Control {
    class TrackableObject;
    class AlarmHelper;
}
class PositionStreamConsumer;

//includes for data members
#include <MountC.h> // for Control::Mount
#include <WeatherStationControllerC.h> // for Control::WeatherStationController
#include <acsComponentSmartPtr.h> // for maci::SmartPtr
#include <acsThread.h>
#include <deque> // for std::deque
#include <acstimeEpochHelper.h> // for EpochHelper
#include <acscommonC.h> // for ACS::Time
#include <Pattern.h> // for Pattern
#include <AzCableWrap.h> // for AzCableWrap
#include <ace/Recursive_Thread_Mutex.h>
#include <casacore/measures/Measures/MPosition.h> // for casa::MPosition
#include <casacore/measures/Measures/MDirection.h> // for casa::MDirection
#include <boost/shared_ptr.hpp> // for boost::shared_ptr
#include <ControlCommonInterfacesC.h> // for Control::PointingCalResult

namespace Control {
    class MountControllerImpl:
        public virtual POA_Control::MountController,
        public Control::AntModeControllerImpl
    {
    public:
        // ------------------- Constructor & Destructor -------------------

        /// The constructor for any ACS C++ component must have this signature
        MountControllerImpl(const ACE_CString& name,
                            maci::ContainerServices* containerServices);

        /// The destructor shuts down the thread and releases the reference to
        /// the Mount component. It must be virtual because this class contains
        /// virtual functions.
        virtual ~MountControllerImpl();

        // --------------------- Component LifeCycle interface -----------

        /// This creates the trajectory planner thread. Its starts suspended
        /// and is activated by the track method.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void initialize();

        /// This terminates the trajectory planner thread (and in normal cases
        /// it should already have been suspended by the stop method).
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();

        // --------------------- CORBA interface --------------------------

        /// Returns true is shutter is open
        /// \exception ModeControllerExceptions::MountFaultEx in case the mount
        /// component threw ControlExceptions::CAMBErrorEx.
        /// \exception ModeControllerExceptions::UnallocatedEx in case the
        /// mount component is not in hwOperational state or if the mount is
        /// not allocated yet.
        virtual CORBA::Boolean isShutterOpen();

        /// Returns true is shutter is closed
        /// \exception ModeControllerExceptions::MountFaultEx in case the mount
        /// component threw ControlExceptions::CAMBErrorEx.
        /// \exception ModeControllerExceptions::UnallocatedEx in case the
        /// mount component is not in hwOperational state or if the mount is
        /// not allocated yet.
        virtual CORBA::Boolean isShutterClosed();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void track();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void trackAsync();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void stop();

        /// See the IDL file for a description of this function.
        bool isStopped();

        /// See the IDL file for a description of this function.
        char* azElToString(double az, double el);

        /// See the IDL file for a description of this function.
        char* raDecToString(double ra, double dec);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setAzEl(double commandedAz, double commandedEl);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAzElAsync(double commandedAz, double commandedEl);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setDirection(double commandedRA, double commandedDec,
                          double pmRA, double pmDec, double parallax);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setDirectionAsync(double commandedRA, double commandedDec,
                               double pmRA, double pmDec, double parallax);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void waitUntilOnSource();

        /// See the IDL file for a description of this function.
        void waitUntilOnSourceCB(Control::AntModeControllerCB* cb);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        double timeToSource();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        double timeToSlew(double fromAz, double fromEl,
                          double toAz, double toEl);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        double timeToSet();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void survivalStow();

        /// See the IDL file for a description of this function.
        void survivalStowCB(Control::AntModeControllerCB* cb);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void maintenanceStow();

        /// See the IDL file for a description of this function.
        void maintenanceStowCB(Control::AntModeControllerCB* cb);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void actualAzEl(double& az, double& el, ACS::Time& timestamp);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void commandedAzEl(double& az, double& el, ACS::Time& timestamp);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void actualRADec(double& ra, double& dec, ACS::Time& timestamp);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void commandedRADec(double& ra, double& dec, ACS::Time& timestamp);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void toAzElEquatorial(double ra, double dec,
                              double pmRA, double pmDec, double parallax,
                              double& az, double& el);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        bool isObservableEquatorial(double ra, double dec, double duration,
                                    double pmRA, double pmDec,
                                    double parallax);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double timeToRiseEquatorial(double ra, double dec,
                                    double pmRA, double pmDec,
                                    double parallax);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double timeToSetEquatorial(double ra, double dec,
                                   double pmRA, double pmDec, double parallax);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setEpoch(double epoch);

        /// See the IDL file for a description of this function.
        double getEpoch();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setTolerance(double angle);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double getTolerance();

        /// See the IDL file for a description of this function.
        void setTimeout(double timeOut);

        /// See the IDL file for a description of this function.
        double getTimeout();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setPlanet(const char* planetName);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setPlanetAsync(const char* planetName);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void toAzElPlanet(const char* planetName,
                          double& az, double& el);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void toRADecPlanet(const char* planetName,
                          double& ra, double& dec);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        bool isObservablePlanet(const char* planetName, double duration);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double timeToRisePlanet(const char* planetName);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double timeToSetPlanet(const char* planetName);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setEphemeris(const Control::Ephemeris& userEphemeris);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setEphemerisAsync(const Control::Ephemeris& userEphemeris);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void toAzElEphemeris(const Control::Ephemeris& userEphemeris,
                             double& az, double& el);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void toRADecEphemeris(const Control::Ephemeris& userEphemeris,
                             double& ra, double& dec);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        bool isObservableEphemeris(const Control::Ephemeris& userEphemeris,
                                   double duration);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double timeToRiseEphemeris(const Control::Ephemeris& userEphemeris);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double timeToSetEphemeris(const Control::Ephemeris& userEphemeris);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setHorizonOffset(double longOffset, double latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setHorizonOffsetAsync(double longOffset, double latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setHorizonOffsetQueued(double longOffset, double latOffset,
                                    ACS::Time startTime);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void incrementHorizonOffsetLongAsync(double offsetInc);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void incrementHorizonOffsetLatAsync(double offsetInc);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        void getHorizonOffset(double& longOffset, double& latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        ACS::Time setHorizonLinearStroke(double longVelocity,
                                         double latVelocity,
                                         double longOffset,
                                         double latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        ACS::Time setHorizonLinearStrokeQueued(double longVelocity,
                                               double latVelocity,
                                               double longOffset,
                                               double latOffset,
                                               ACS::Time startTime);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setEquatorialOffset(double longOffset, double latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setEquatorialOffsetAsync(double longOffset, double latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setEquatorialOffsetQueued(double longOffset, double latOffset,
                                       ACS::Time startTime);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void incrementEquatorialOffsetLongAsync(double offsetInc);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void incrementEquatorialOffsetLatAsync(double offsetInc);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        void getEquatorialOffset(double& longOffset, double& latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setEquatorialLinearStroke(double longVelocity,
                                       double latVelocity,
                                       double longOffset,
                                       double latOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setEquatorialLinearStrokeQueued(double longVelocity,
                                             double latVelocity,
                                             double longOffset,
                                             double latOffset,
                                             ACS::Time startTime);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setAzElOffset(double azOffset, double elOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAzElOffsetAsync(double azOffset, double elOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAzElOffsetQueued(double azOffset, double elOffset,
                                 ACS::Time startTime);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        void setAzElLinearStroke(double azVelocity, double elVelocity,
                                 double azOffset, double elOffset);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAzElLinearStrokeQueued(double azVelocity, double elVelocity,
                                       double azOffset, double elOffset,
                                       ACS::Time startTime);

        /// See the IDL file for a description of this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setObservingFrequency(double frequency);

        /// Convert an observing frequency to a band. The MountController is
        /// not the right place for this function.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        static int frequencyToBand(double frequency);

        /// See the IDL file for a description of this function.
        double getObservingFrequency();

        /// See the IDL file for a description of this function.
        void getRefractionCoefficients(double& a, double& b);

        /// See the IDL file for a description of this function.
        char* getMount();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        void enableMountDataPublication();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        void disableMountDataPublication();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        Control::MountController::PointingData getPointingData();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double startPointingDataCollection(ACS::Time startTime,
                                           ACS::Time stopTime);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        Control::MountController::PointingDataTable*
        getPointingDataTable(double timeout);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ModeControllerExceptions::TimeoutEx
        Control::MountController::PointingDataTable*
        getSelectedPointingDataTable(ACS::Time startTime, ACS::Time stopTime, double timeout);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        /// \exception ControlExceptions::DeviceBusyEx
        /// \exception ModeControllerExceptions::TimeoutEx
        Control::MountController::PointingDataTable*
        collectPointingData(double duration);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        bool collectingData(ACS::Time& startTime, ACS::Time& stopTime);

        /// See the IDL file for a description of this function.
        void abortDataCollection();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        TMCDB::PointingModel* getPointingModel();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        bool isPointingModelEnabled();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        TMCDB::PointingModel* getAuxPointingModel();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        double getAuxPointingModelCoefficient(const char* name);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterErrorEx
        void setAuxPointingModel(const TMCDB::PointingModel& model);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void zeroAuxPointingModel();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        bool isAuxPointingModelEnabled();

        /* --------- Mount Parameter Reporting Functions -------------*/
        /// See the IDL file for a description of these functions.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void reportPointingModel();

        /// See the IDL file for a description of these functions.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void reportFocusModel();

        /// See the IDL file for a description of these functions.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void reportFocusPosition();

        /// See the IDL file for a description of these functions.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterEx
        void applyPointingCalibrationResult(const PointingCalResult& result);

        /// See the IDL file for a description of these functions.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ControlExceptions::IllegalParameterEx
        void applyFocusCalibrationResult(const FocusCalResult& result);

        /// See the IDL file for a description of these functions.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        /// \exception ControlExceptions::IllegalParameterEx
        void setElevationLimit(double elevationLimit);

        /// See the IDL file for a description of these functions.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        double getElevationLimit();

        /// See the IDL file for a description of these functions.
        void resetLimits();

        // --------------------- C++ public functions ---------------------

        /// Make the mount moveable
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::MountFaultEx
        void track(bool async);

    private:
        // ------------------- Mode Controller Methods --------------------

        /// Configures this component to use the equipment in the
        /// specified antenna. The supplied argument is an antenna name like
        /// "DV01" and the associated antenna component must already be started
        /// (by the master component). This function will then query the
        /// antenna component to get the references to the mount
        /// component. This function can be called twice and if so it will
        /// release the equipment in the antenna it was previously using and
        /// acquire the equipment in the new antenna.
        /// \param antennaName
        /// Name of antenna component to use e.g., "DV01"
        /// \exception ModeControllerExceptions::BadConfigurationEx
        /// A BadConfiguration exception is generated if the antenna does not
        /// have a mount component
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// A CannotGetAntenna exception is generated if the the specified
        /// antenna component cannot be contacted
        virtual void acquireReferences(const std::string& antennaName);

        /// Restore this component to the state it was in after
        /// construction. In this state this component does not have a
        /// reference to any components and none of its functions, except
        /// allocate & getState should be used.
        virtual void releaseReferences();

        /// The copy constructor is made private to prevent a compiler
        /// generated one from being used. It is not implemented.
        MountControllerImpl(const MountControllerImpl& other);

        /// Check that we can use the Mount component.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// If the Mount component is null
        /// \exception ModeControllerExceptions::MountFaultEx
        /// If the communications to the mount has failed unexpectedly.
        void checkMountIsOK(const char* fileName, int lineNum,
                            const char* functionName);

        /// Add the pad and antenna position to produce an overall position for
        /// the antenna.
        static casa::MPosition addPositions(const TMCDB_IDL::PadIDL& pi,
                                            const TMCDB_IDL::AntennaIDL& ai,
                                            Logging::Logger::LoggerSmartPtr logger);

        /// This method will do all the work to convert a celestial position
        /// (e.g. RA/Dec) to azimuth and elevation coordinates and then send
        /// these positions to the mount component. It is run by the trajectory
        /// planner thread periodically (every second) and queues the commands
        /// for the next four seconds.
        void trajectoryPlanner(const ACS::TimeInterval& loopTime);

        /// Immediatly switch to the new source. This calls flushCommands and
        /// then starts scheduling the new source.
        /// \exception ControlExceptions::IllegalParameterErrorEx
        ///   If the temperature sent to the Mount is out of range (unlikely to happen)
        void switchSource(boost::shared_ptr<TrackableObject> src,
                          bool avoidZenith=true);

        /// Flush all queued commands. This clears all queues in this class and
        /// the mount controller, and sets the start time for the new source
        /// \exception ControlExceptions::IllegalParameterErrorEx
        ///   If the temperature sent to the Mount is out of range (unlikely to happen)
        void flushCommands();

        /// A reference to the Mount component. After construction this is a
        /// nill reference and will refer to an actual component after the
        /// acquireReferences function has successfully completed. It reverts
        /// to a nil reference when releasReferences is called.
        maci::SmartPtr<Control::Mount> mount_m;

        /// The Position of the Antenna. This is the sum of the pad location
        /// and any offsets specific to the antenna.
        casa::MPosition antennaLocation_m;

        /// A reference to the weather station component. After construction
        /// this is a nill reference and will refer to an actual component
        /// after the acquireReferences function has successfully completed. It
        /// reverts to a nil reference when releasReferences is called.
        maci::SmartPtr<Control::WeatherStationController> ws_m;

        /// The observing frequency is used only if a refraction correction
        /// needs to be done.
        double obsFreq_m;

        /// The assumed epoch for all subsequent setDirection commands.
        double epoch_m;

        /// Maximum length of time, in seconds, to wait for the mount to get to
        /// its commanded position.
        double timeout_m;

        /// The time when we we started to use the current source
        ACS::Time startOfSource_m;

        /// This thread wakes up every second and sends more positions to the
        /// mount component
        class TrajectoryPlannerThread : public ACS::Thread {
        public:
            TrajectoryPlannerThread(const ACE_CString& name,
                                    MountControllerImpl& mc,
                                    const ACS::TimeInterval& responseTime,
                                    const ACS::TimeInterval& sleepTime);
            virtual void runLoop();
        private:
            MountControllerImpl&  mc_m;
        };

        /// The instance of the thread object
        TrajectoryPlannerThread* tpThread_m;

        /// The remaining data members (sourceList_m and lastScheduledTime_m)
        /// are used by both this class and the trajectory planner thread. To
        /// prevent simultaneous access they are protected by the
        /// sourceListMutex_m mutex. Always acquire this mutex before using
        /// these variables.
        ACE_Recursive_Thread_Mutex sourceListMutex_m;

        /// A queue of the sources we are observing.
        std::deque<boost::shared_ptr<Control::TrackableObject> > sourceList_m;

        /// This the last time that has been scheduled by the trajectory
        /// planner thread.
        EpochHelper lastScheduledTime_m;

        /// These variables cache the last values returned from the
        /// weatherstation so we can check if the weather data is updating (and
        /// warn users if not). This heuristic is used because the python
        /// implementation of the weather station (which is a bridge component
        /// to the VLA weather station) does not return a cotrrect timestamp
        /// with the weather data. REMOVE THIS HACK WHEN THE WEATHER STATIONS
        /// ARE FIXED.
        double tempInC_m;
        double pressureInP_m;
        double humidity_m;
        EpochHelper wsTime_m;

        // The name of the pad this antenna is on. Its read from the TMCDB and
        // used to get the most relevant weather data.
        std::string padName_m;

        // An object to facilitate the sending of alarms
        boost::shared_ptr<AlarmHelper> alarms_m;

        /// This function checks if the weather parameters are likely to be OK.
        void checkWeatherIsOK();

        /// Define a structure for the commanded directions & offsets. These
        /// values are populated by the trajectory planner thread.
        struct Commands {
            Control::EquatorialDirection target;
            Control::Offset equatorial;
            Control::EquatorialDirection equatorialRADec;
            Control::Offset horizon;
            Control::HorizonDirection horizonAzEl;
            Control::HorizonDirection commanded;
            ACS::Time timestamp;
            boost::shared_ptr<Control::TrackableObject> source;
        };

        /// Define a container for accumulating commanded directions and
        /// offsets. This container is populated by the trajectory planner
        /// thread if we are accumulating pointing data. Because this
        /// container, and its associated data members, is accessed by both the
        /// thread and functions in the MountControllerImpl class there is a
        /// mutex to prevent simultaneous access.
        std::vector<Commands> commands_m;
        ACS::Time commandStartTime_m, commandStopTime_m;
        bool collectingData_m;
        ACE_Recursive_Thread_Mutex commandsMutex_m;

        /// Define a container for accumulating commanded directions and
        /// offsets. This container is populated by the trajectory planner
        /// whenever it is commanding the mount but its size is limited and it
        /// can never contain more than about six seconds of data. Because this
        /// container is accessed by both the thread and the getPointingData
        /// function in the MountControllerImpl class there is a mutex to
        /// prevent simultaneous access.
        std::deque<Commands> latestCommands_m;
        ACE_Recursive_Thread_Mutex latestCommandsMutex_m;

        /// The measured directions, if we are accumulating pointing data are
        /// stored in an instance of the PositionStreamConsumer object.
        PositionStreamConsumer* positionConsumer_m;

        /// This function is called when cleaning up. As I want to ensure the
        /// cleanup does not stop waiting for a mutex this version of the
        /// abortDataCollection function does not acquire the relevant mutexes.
        void unprotectedAbortDataCollection();

        /// This function merges the measured and commanded data putting the
        /// results into the PointingData structure. This factors out code
        /// common to the getPointingData and getPointingDataTable
        /// functions. If the timestamps on the commanded data do not match
        /// with the measured data then the commanded data is ignored.
        /// \exception ModeControllerExceptions::MountFaultEx
        /// If the measured data is invalid.
        void mergePointingData(const Control::MountStatusData& measuredData,
                               const Commands& commandData,
                               Control::MountController::PointingData& pd);

        /// This class knows where the antenna is currently pointing and works
        /// out which azimuth wrap to use.
        AzCableWrap cableWrap_m;

    }; // end  MountControllerImpl class
} // end Control namespace

#endif // MOUNTCONTROLLERIMPL_H
