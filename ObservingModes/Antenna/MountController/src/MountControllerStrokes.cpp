// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"

#include <loggingMACROS.h> // for AUTO_TRACE
#include <string> // for string
#include <sstream> // for ostringstream
#include <cmath> // for M_PI & M_PI_2
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <TETimeUtil.h> // for TETimeUtil
#include <HorizonObject.h> // HorizonObject & TrackableObject 
#include <OffsetStroke.h> // OffsetStroke
#include <LinearStroke.h> // LinearStroke
#include <Pattern.h> // Pattern
#include <casacore/casa/Quanta/Quantum.h> // for casa::Quantity
#include <casacore/casa/Quanta/MVDirection.h> // for casa::MVDirection

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::string;
using std::ostringstream;
using std::endl;
using Control::MountControllerImpl;
using ControlExceptions::IllegalParameterErrorExImpl;
using ACS::Time;
using log_audience::OPERATOR;

// This compilation unit contains the part of the MountController class that
// relates to strokes (this includes offsets). Its split out from
// MountControllerImpl.cpp file to speed compilation when working on just this
// aspect of the class.

void MountControllerImpl::
setHorizonOffset(double longOffset, double latOffset) {
    AUTO_TRACE(__func__);
  
    setHorizonOffsetAsync(longOffset, latOffset);
    waitUntilOnSource();
}

void MountControllerImpl::
setHorizonOffsetAsync(double longOffset, double latOffset) {
    AUTO_TRACE(__func__);

    { // clear any previous or future offsets/strokes
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        sourceList_m.front()->horizonPattern().clear();

        // Flush the queue in the Mount component (this will flush all commands
        // associated with the previous offset). 
        flushCommands();

        // Now command the new offset
        setHorizonOffsetQueued(longOffset, latOffset, startOfSource_m);
    }
    // Immediately get the trajectory planner to start using this new source
    trajectoryPlanner(tpThread_m->getSleepTime());
}

void MountControllerImpl::
setHorizonOffsetQueued(double longOffset, double latOffset, Time startTime) {
    AUTO_TRACE(__func__);

    if (fabs(longOffset) > 3*M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "longOffset");
        ex.addData("Value", longOffset);
        ex.addData("ValidRange", "-3*pi to 3*pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(latOffset) > M_PI_2) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "latOffset");
        ex.addData("Value", latOffset);
        ex.addData("ValidRange", "-pi/2 to pi/2");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    bool done = true;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        if (!sourceList_m.empty()) {
            if (startTime == 0) startTime = lastScheduledTime_m.value().value;
            boost::shared_ptr<Stroke> 
                stroke(new OffsetStroke(startTime, longOffset, latOffset));
            sourceList_m.front()->horizonPattern().insert(stroke);
        } else {
            done = false;
        }
    }

    // do the logging outside after the previous block as it does not need the
    // source-list mutex
    if (done) {
        ostringstream msg;
        msg << "Offsetting antenna " << getAntennaName() << " by (" 
            << casa::Quantity(longOffset, "rad").getValue("arcmin") << ", " 
            << casa::Quantity(latOffset, "rad").getValue("arcmin") 
            << ") arc-minutes in horizon coordinates at "
            << TETimeUtil::toTimeString(startTime);
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    } else {
        string msg = "Cannot apply a horizon offset on antenna " 
            + getAntennaName() + " as you are not tracking a source.";
        msg += " Offset command ignored.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}

void MountControllerImpl::incrementHorizonOffsetLongAsync(double offsetInc) {
    AUTO_TRACE(__func__);

    ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
    double longOffset, latOffset;
    // I do not acquire the sourceList_m mutex here so there is a small chance
    // the source has changed between calling the following two functions.
    // TODO. Correct this.
    getHorizonOffset(longOffset, latOffset);
    setHorizonOffsetAsync(longOffset + offsetInc, latOffset);
}

void MountControllerImpl::incrementHorizonOffsetLatAsync(double offsetInc) {
    AUTO_TRACE(__func__);

    ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
    double longOffset, latOffset;
    // I do not acquire the sourceList_m mutex here so there is a small chance
    // the source has changed between calling the following two functions.
    // TODO. Correct this.
    getHorizonOffset(longOffset, latOffset);
    setHorizonOffsetAsync(longOffset, latOffset + offsetInc);
}

void MountControllerImpl::
getHorizonOffset(double& longOffset, double& latOffset) {
    AUTO_TRACE(__func__);

    const Time timeNow(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value);
    ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
    if (!sourceList_m.empty()) {
        sourceList_m.front()->
            horizonPattern().getOffset(timeNow, longOffset, latOffset);
    } else {
        longOffset = 0.0;
        latOffset = 0.0;
    }
}

Time MountControllerImpl::
setHorizonLinearStroke(double longVelocity, double latVelocity,
                       double longOffset, double latOffset) {
    AUTO_TRACE(__func__);
  
    setHorizonOffset(longOffset, latOffset);
    return setHorizonLinearStrokeQueued(longVelocity, latVelocity, 
                                        longOffset, latOffset, 0);
}

Time MountControllerImpl::
setHorizonLinearStrokeQueued(double longVelocity, double latVelocity,
                             double longOffset, double latOffset,
                             Time startTime) {
    AUTO_TRACE(__func__);

    if (fabs(longVelocity) > 6*M_PI/180) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "longVelocity");
        ex.addData("Value", longVelocity);
        ex.addData("ValidRange", "-6*pi/180 to 6*pi/180");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(latVelocity) > 6*M_PI/180) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "latVelocity");
        ex.addData("Value", latVelocity);
        ex.addData("ValidRange", "-6*pi/180 to 6*pi/180");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(longOffset) > 3*M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "longOffset");
        ex.addData("Value", longOffset);
        ex.addData("ValidRange", "-3*pi to 3*pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(latOffset) > M_PI_2) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "latOffset");
        ex.addData("Value", latOffset);
        ex.addData("ValidRange", "-pi/2 to pi/2");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    bool done = true;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
        if (!sourceList_m.empty()) {
            if (startTime == 0) startTime = lastScheduledTime_m.value().value;
            boost::shared_ptr<Stroke> 
                stroke(new LinearStroke(startTime, longOffset, latOffset, 
                                        longVelocity, latVelocity));
            sourceList_m.front()->horizonPattern().insert(stroke);
        } else {
            done = false;
        }
    }

    // do the logging outside after the previous block as it does not need the
    // source-list mutex
    if (done) {
        ostringstream msg;
        msg << "Starting a linear stroke on antenna " << getAntennaName() 
            << " with an offset of ("
            << casa::Quantity(longOffset, "rad").getValue("arcmin") << ", " 
            << casa::Quantity(latOffset, "rad").getValue("arcmin") 
            << ") arc-minutes and a velocity of ("
            << casa::Quantity(longVelocity, "rad/s").getValue("arcmin/s") 
            << ", " 
            << casa::Quantity(latVelocity, "rad/s").getValue("arcmin/s") 
            << ") arc-minutes/sec at " << TETimeUtil::toTimeString(startTime);
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    } else {
        string msg = "Cannot do the linear stroke on antenna " 
            + getAntennaName() + " as you are not tracking a source.";
        msg += " Stroke command ignored.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    return startTime;
}

void MountControllerImpl::
setEquatorialOffset(double longOffset, double latOffset) {
    AUTO_TRACE(__func__);
  
    setEquatorialOffsetAsync(longOffset, latOffset);
    waitUntilOnSource();
}

void MountControllerImpl::
setEquatorialOffsetAsync(double longOffset, double latOffset) {
    AUTO_TRACE(__func__);

    { // clear any previous or future offsets/strokes
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        sourceList_m.front()->equatorialPattern().clear();

        // Flush the queue in the Mount component (this will flush all commands
        // associated with the previous offset). 
        flushCommands();

        // Now command the new offset
        setEquatorialOffsetQueued(longOffset, latOffset, startOfSource_m);
    }
    // Immediately get the trajectory planner to start using this new source
    trajectoryPlanner(tpThread_m->getSleepTime());
}

void MountControllerImpl::
setEquatorialOffsetQueued(double longOffset, double latOffset,Time startTime) {
    AUTO_TRACE(__func__);

    if (fabs(longOffset) > 2*M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "longOffset");
        ex.addData("Value", longOffset);
        ex.addData("ValidRange", "-2*pi to 2*pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(latOffset) > M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "latOffset");
        ex.addData("Value", latOffset);
        ex.addData("ValidRange", "-pi to pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    bool done = true;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        if (!sourceList_m.empty() &&
            (dynamic_cast<HorizonObject*>(sourceList_m.front().get()) == 0)) {
            if (startTime == 0) startTime = lastScheduledTime_m.value().value;
            boost::shared_ptr<Stroke> 
                stroke(new OffsetStroke(startTime, longOffset, latOffset));
            sourceList_m.front()->equatorialPattern().insert(stroke);
        } else {
            done = false;
        }
    }

    // do the logging outside after the previous block as it does not need the
    // source-list mutex
    if (done) {
        ostringstream msg;
        msg << "Offsetting antenna " << getAntennaName() << " by (" 
            << casa::Quantity(longOffset, "rad").getValue("arcmin") << ", " 
            << casa::Quantity(latOffset, "rad").getValue("arcmin") 
            << ") arc-minutes in equatorial coordinates at "
            << TETimeUtil::toTimeString(startTime);
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    } else {
        string msg = "Cannot apply an equatorial offset on antenna " 
            + getAntennaName() + " as you are not tracking a source.";
        msg += " Offset command ignored.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}

void MountControllerImpl::
incrementEquatorialOffsetLongAsync(double offsetInc) {
    AUTO_TRACE(__func__);

    ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
    double longOffset, latOffset;
    // I do not acquire the sourceList_m mutex here so there is a small chance
    // the source has changed between calling the following two functions.
    // TODO. Correct this.
    getEquatorialOffset(longOffset, latOffset);
    setEquatorialOffsetAsync(longOffset + offsetInc, latOffset);
}

void MountControllerImpl::
incrementEquatorialOffsetLatAsync(double offsetInc) {
    AUTO_TRACE(__func__);

    ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
    double longOffset, latOffset;
    // I do not acquire the sourceList_m mutex here so there is a small chance
    // the source has changed between calling the following two functions.
    // TODO. Correct this.
    getEquatorialOffset(longOffset, latOffset);
    setEquatorialOffsetAsync(longOffset, latOffset + offsetInc);
}

void MountControllerImpl::
getEquatorialOffset(double& longOffset, double& latOffset) {
    AUTO_TRACE(__func__);

    const Time timeNow(TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value);
    ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
    if (!sourceList_m.empty()) {
        sourceList_m.front()->
            equatorialPattern().getOffset(timeNow, longOffset, latOffset);
    } else {
        longOffset = 0.0;
        latOffset = 0.0;
    }
}

void MountControllerImpl::
setEquatorialLinearStroke(double longVelocity, double latVelocity,
                          double longOffset, double latOffset) {
    AUTO_TRACE(__func__);
  
    setEquatorialOffset(longOffset, latOffset);
    return setEquatorialLinearStrokeQueued(longVelocity, latVelocity, 
                                           longOffset, latOffset, 0);
}

void MountControllerImpl::
setEquatorialLinearStrokeQueued(double longVelocity, double latVelocity,
                                double longOffset, double latOffset,
                                Time startTime) {
    AUTO_TRACE(__func__);

    if (fabs(longVelocity) > 6*M_PI/180) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "longVelocity");
        ex.addData("Value", longVelocity);
        ex.addData("ValidRange", "-6*pi/180 to 6*pi/180");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(latVelocity) > 6*M_PI/180) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "latVelocity");
        ex.addData("Value", latVelocity);
        ex.addData("ValidRange", "-6*pi/180 to 6*pi/180");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(longOffset) > 2*M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "longOffset");
        ex.addData("Value", longOffset);
        ex.addData("ValidRange", "-2*pi to 2*pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(latOffset) > M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "latOffset");
        ex.addData("Value", latOffset);
        ex.addData("ValidRange", "-pi to pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    bool done = true;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
        if (!sourceList_m.empty() &&
            (dynamic_cast<HorizonObject*>(sourceList_m.front().get()) == 0)) {
            if (startTime == 0) startTime = lastScheduledTime_m.value().value;
            boost::shared_ptr<Stroke> 
                stroke(new LinearStroke(startTime, longOffset, latOffset, 
                                        longVelocity, latVelocity));
            sourceList_m.front()->equatorialPattern().insert(stroke);
        } else {
            done = false;
        }
    }

    // do the logging outside after the previous block as it does not need the
    // source-list mutex
    if (done) {
        ostringstream msg;
        msg << "Starting an equatorial linear stroke on antenna " 
            << getAntennaName() << " with an offset of ("
            << casa::Quantity(longOffset, "rad").getValue("arcmin") << ", " 
            << casa::Quantity(latOffset, "rad").getValue("arcmin") 
            << ") arc-minutes and a velocity of ("
            << casa::Quantity(longVelocity, "rad/s").getValue("arcmin/s") 
            << ", " 
            << casa::Quantity(latVelocity, "rad/s").getValue("arcmin/s") 
            << ") arc-minutes/sec at " << TETimeUtil::toTimeString(startTime);
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    } else {
        string msg = "Cannot do the equatorial linear stroke as you are not";
        msg += " tracking a celestial source. Stroke command ignored.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}

void MountControllerImpl::
setAzElOffset(double azOffset, double elOffset) {
    AUTO_TRACE(__func__);
    setAzElOffsetAsync(azOffset, elOffset);
    waitUntilOnSource();
}

void MountControllerImpl::
setAzElOffsetAsync(double azOffset, double elOffset) {
    AUTO_TRACE(__func__);

    { // clear any previous or future offsets/strokes
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        sourceList_m.front()->horizonPattern().clear();

        // Flush the queue in the Mount component (this will flush all commands
        // associated with the previous offset). 
        flushCommands();

        // Now command the new offset
        setAzElOffsetQueued(azOffset, elOffset, startOfSource_m);
    }
    // Immediately get the trajectory planner to start using this new source
    trajectoryPlanner(tpThread_m->getSleepTime());
}

void MountControllerImpl::
setAzElOffsetQueued(double azOffset, double elOffset, Time startTime) {
    AUTO_TRACE(__func__);

    if (fabs(azOffset) > 3*M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "azOffset");
        ex.addData("Value", azOffset);
        ex.addData("ValidRange", "-3*pi to 3*pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(elOffset) > M_PI_2) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "elOffset");
        ex.addData("Value", elOffset);
        ex.addData("ValidRange", "-pi/2 to pi/2");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    bool done = false;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        if (!sourceList_m.empty() &&
            (dynamic_cast<HorizonObject*>(sourceList_m.front().get()) != 0)) {
            boost::shared_ptr<Control::TrackableObject> src = 
                sourceList_m.front();
            if (startTime == 0) startTime = lastScheduledTime_m.value().value;
            Control::EquatorialDirection target;
            Control::Offset equatorial;
            Control::EquatorialDirection equatorialRADec;
            Control::Offset horizon;
            Control::HorizonDirection horizonAzEl;
            Control::HorizonDirection commanded;
            Control::HorizonDirection commandedRate;
            src->getPointing(startTime, target, equatorial, equatorialRADec, 
                             horizon, horizonAzEl, commanded, commandedRate);
            const double az = commanded.az - horizonAzEl.az;
            const double el = commanded.el - horizonAzEl.el;
            boost::shared_ptr<OffsetStroke> 
                stroke(new OffsetStroke(startTime, azOffset, elOffset + el));
	    stroke->setElRef(el);
            const casa::MVDirection refDirection(az, 0);
            src->horizonPattern().setRefDirection(refDirection);
            src->horizonPattern().insert(stroke);
            done = true;
        }
    }

    // do the logging outside after the previous block as it does not need the
    // source-list mutex
    if (done) {
        ostringstream msg;
        msg << "Offsetting antenna " << getAntennaName() << " by (" 
            << casa::Quantity(azOffset, "rad").getValue("arcmin") << ", " 
            << casa::Quantity(elOffset, "rad").getValue("arcmin") 
            << ") arc-minutes in azimuth and elevation at "
            << TETimeUtil::toTimeString(startTime);
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    } else {
        string msg = "Cannot apply a azimuth and elevation offset on antenna " 
            + getAntennaName() + " as you are not using a source with"
            + " a constant azimuth and elevation.";
        msg += " Offset command ignored.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}

void MountControllerImpl::
setAzElLinearStroke(double azVelocity, double elVelocity,
                    double azOffset, double elOffset) {
    AUTO_TRACE(__func__);
  
    setAzElOffset(azOffset, elOffset);
    return setAzElLinearStrokeQueued(azVelocity, elVelocity, 
                                     azOffset, elOffset, 0);
}

void MountControllerImpl::
setAzElLinearStrokeQueued(double azVelocity, double elVelocity,
                          double azOffset, double elOffset,
                          Time startTime) {
    AUTO_TRACE(__func__);

    if (fabs(azVelocity) > 6*M_PI/180) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "azVelocity");
        ex.addData("Value", azVelocity);
        ex.addData("ValidRange", "-6*pi/180 to 6*pi/180");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(elVelocity) > 3*M_PI/180) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "elVelocity");
        ex.addData("Value", elVelocity);
        ex.addData("ValidRange", "-3*pi/180 to 3*pi/180");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(azOffset) > 2*M_PI) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "azOffset");
        ex.addData("Value", azOffset);
        ex.addData("ValidRange", "-2*pi to 2*pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (fabs(elOffset) > M_PI/2) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "elOffset");
        ex.addData("Value", elOffset);
        ex.addData("ValidRange", "-pi/2 to pi/2");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    bool done = false;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
        if (!sourceList_m.empty() &&
            (dynamic_cast<HorizonObject*>(sourceList_m.front().get()) != 0)) {
            boost::shared_ptr<Control::TrackableObject> src = 
                sourceList_m.front();
            if (startTime == 0) startTime = lastScheduledTime_m.value().value;

            Control::EquatorialDirection target;
            Control::Offset equatorial;
            Control::EquatorialDirection equatorialRADec;
            Control::Offset horizon;
            Control::HorizonDirection horizonAzEl;
            Control::HorizonDirection commanded;
            Control::HorizonDirection commandedRate;
            src->getPointing(startTime, target, equatorial, equatorialRADec, 
                             horizon, horizonAzEl, commanded, commandedRate);
            const double az = commanded.az - horizonAzEl.az;
            const double el = commanded.el - horizonAzEl.el;
            boost::shared_ptr<LinearStroke> 
                stroke(new LinearStroke(startTime, azOffset, elOffset + el, 
                                        azVelocity, elVelocity));
	    stroke->setElRef(el);
            const casa::MVDirection refDirection(az, 0);
            src->horizonPattern().setRefDirection(refDirection);
            src->horizonPattern().insert(stroke);
            done = true;
        }
    }

    // do the logging outside after the previous block as it does not need the
    // source-list mutex
    if (done) {
        ostringstream msg;
        msg << "Starting an az/el linear stroke on antenna " 
            << getAntennaName() << " with an offset of ("
            << casa::Quantity(azOffset, "rad").getValue("arcmin") << ", " 
            << casa::Quantity(elOffset, "rad").getValue("arcmin") 
            << ") arc-minutes and a velocity of ("
            << casa::Quantity(azVelocity, "rad/s").getValue("arcmin/s") 
            << ", " 
            << casa::Quantity(elVelocity, "rad/s").getValue("arcmin/s") 
            << ") arc-minutes/sec at " << TETimeUtil::toTimeString(startTime);
        LOG_TO_AUDIENCE(LM_INFO, __func__, msg.str(), OPERATOR);
    } else {
        string msg = "Cannot do a azimuth/elevation linear stroke on antenna " 
            + getAntennaName() + " as you are not using a source with"
            + " a constant azimuth and elevation.";
        msg += " Stroke command ignored.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}

