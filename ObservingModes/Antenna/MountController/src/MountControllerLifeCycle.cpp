// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"
#include "SourceAlarmTypes.h"

#include <ModeControllerExceptions.h>
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <TmcdbErrType.h> // TMCDB error declarations
#include <ControlDeviceC.h> // for Control::ControlDevice
#include <TMCDBAccessIFC.h> // for TMCDB::Access
#include <TETimeUtil.h> // for TETimeUtil
#include <TrackableObject.h> // TrackableObject 
#include <PositionStreamConsumer.h> // For PositionConsumer
#include <MountImpl.h>
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <controlAlarmSender.h> // for Control::AlarmSender
#include <casacore/casa/Quanta/MVPosition.h> // for casa::MVPosition
#include <casacore/casa/Quanta/Euler.h> // for casa::Euler
#include <casacore/casa/Quanta/RotMatrix.h> // for casa::RotMatrix
#include <casacore/measures/Measures/MPosition.h> // for casa::MPosition
#include <casacore/measures/Measures/MeasConvert.h> // for casa::MeasConvert
#include <casacore/measures/Measures/MCPosition.h> // Needed to work around a bug in CASA-3.0.0
#include <string> // for string
#include <sstream> // for ostringstream
#include <iomanip> // for setw, setprecision etc.
#include <AntennaC.h>

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::string;
using std::ostringstream;
using std::endl;
using Control::MountControllerImpl;
using Control::AlarmSender;
using ModeControllerExceptions::CannotGetAntennaExImpl;
using ModeControllerExceptions::BadConfigurationExImpl;
using ControlExceptions::IllegalParameterErrorEx;
using std::setprecision;

// The default value for how long to wait for the mount to go to the commanded
// position setDirection and setAzEl functions timeout. The value is
// in seconds and is set to a value which should be sufficient for any
// plausible slew.
const double defaultTimeout = 110.0;

// This is how often the thread runs (once every 1.008 seconds).
const int loopTimeInTE = 21;
const ACS::TimeInterval loopTime = 
    loopTimeInTE*TETimeUtil::TE_PERIOD_DURATION.value;

// These default antenna position is for antenna PM03 at the OSF on TF6.
// These numbers come from 1.1.2.9 of
// ICD/CONTROL/TMCDBComponent/src/alma/TMCDBComponentImpl/TMCDBSimACAOptHoloComponentImpl.java
// Unless there are errors these values are replaced by values read from the
// TMCDB.

const double x =  2202229.615;
const double y = -5445184.762;
const double z = -2485382.116;

// The default observing frequency used for refraction correction and
// to adjust the focus and pointing models. This value is the standard
// band 3 observing frequency at the OSF.
const double defaultObsFreq = 86.234E9;

// The default epoch for all setDirection commands
const double defaultEpoch = 2000.0;

//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
MountControllerImpl::
MountControllerImpl(const ACE_CString& name, maci::ContainerServices* cs)
    :AntModeControllerImpl(name, cs),
     mount_m(),
     antennaLocation_m(casa::MVPosition(x, y, z), casa::MPosition::ITRF),
     ws_m(),
     obsFreq_m(defaultObsFreq),
     epoch_m(defaultEpoch),
     timeout_m(defaultTimeout),
     startOfSource_m(0),
     tpThread_m(NULL),
     sourceListMutex_m(),
     sourceList_m(),
     lastScheduledTime_m(ACS::Time(0)),
     tempInC_m(15.0),  // 15 deg C
     pressureInP_m(700E2), // 700hPa
     humidity_m(0.2), // 20%
     wsTime_m(static_cast<ACS::Time>(0)),
     padName_m("Unknown"),
     alarms_m(),
     commands_m(0),
     commandStartTime_m(0),
     commandStopTime_m(0),
     collectingData_m(false),
     commandsMutex_m(),
     latestCommands_m(0),
     latestCommandsMutex_m(),
     positionConsumer_m(0),
     cableWrap_m(getLogger())
{
}

MountControllerImpl::~MountControllerImpl() {
    cleanUp(); 
    // No need to call releaseReferences. Its done in
    // antModeController::cleanUp
}

//-----------------------------------------------------------------------------
// Lifecycle Methods
//-----------------------------------------------------------------------------

void MountControllerImpl::initialize() {
    // Do anything needed by the base class. Currently this is nothing
    AntModeControllerImpl::initialize();

    // Now create the thread. Its starts suspended.
    string threadName = CORBA::String_var(name()).in();
    threadName += "TrajectoryPlannerThread";
    tpThread_m = getContainerServices()->getThreadManager()->
        create<TrajectoryPlannerThread, MountControllerImpl>
        (threadName.c_str(), *this, loopTime, loopTime);
}

void MountControllerImpl::cleanUp() {
    // Shutdown the thread i.e., stop sending commands to the Mount
    tpThread_m->terminate();

    // Do anything needed by the base class. This includes releasing references
    AntModeControllerImpl::cleanUp();
}

//-----------------------------------------------------------------------------
// Mode Controller Methods
//-----------------------------------------------------------------------------

void MountControllerImpl::acquireReferences(const string& antennaName) {
    if (antennaName != getAntennaName()) releaseReferences();
    if (getStatus() != Control::AntModeController::UNINITIALIZED) return;
    
    // This gets a reference to the antenna component
    AntModeControllerImpl::acquireReferences(antennaName);

    try {
        CORBA::String_var deviceName;
        // get a reference to the mount component
        deviceName = allocatedAntenna_m->getSubdeviceName("Mount");
        mount_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::Mount>(deviceName);
    } catch (IllegalParameterErrorEx ex) {
        releaseReferences();
        string msg = "Could not find a mount component.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        releaseReferences();
        string msg = "Mount reference is null.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__,__LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    }

    // Get a reference to the TMCDB and extract the antenna location.
    try {
        maci::SmartPtr<TMCDB::Access> tmcdb =
            getContainerServices()->getDefaultComponentSmartPtr<TMCDB::Access>
            ("IDL:alma/TMCDB/Access:1.0");
        TMCDB_IDL::AntennaIDL_var ai = 
            tmcdb->getAntennaInfo(antennaName.c_str());
        TMCDB_IDL::PadIDL_var pi = 
            tmcdb->getCurrentAntennaPadInfo(antennaName.c_str());
        padName_m = pi->PadName;
        antennaLocation_m = addPositions(*pi, *ai, getLogger());
        {
            const casa::MVPosition& antLoc(antennaLocation_m.getValue());
            ostringstream msg;
            msg << "Antenna " << ai->AntennaName 
                << " is on pad " << padName_m << std::fixed
                << ". Latitude: " << setprecision(6) << antLoc.getLat("deg")
                << " Longitude: " << setprecision(6) << antLoc.getLong("deg")
                << " Altitude: " << setprecision(3) << antLoc.getLength("m");
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        }
    } catch (maciErrType::maciErrTypeExImpl& ex) {
        string msg = "Cannot retrieve the antenna location from the database.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (TmcdbErrType::TmcdbSqlEx& ex) {
        string msg = "Cannot retrieve the antenna location from the database.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (TmcdbErrType::TmcdbNoSuchRowEx& ex) {
        string msg = "Cannot retrieve the antenna location from the database.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (TmcdbErrType::TmcdbErrTypeEx& ex) {
        string msg = "Cannot retrieve the antenna location from the database.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (TmcdbErrType::TmcdbDuplicateKeyEx& ex) {
        string msg = "Cannot retrieve the antenna location from the database.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (TmcdbErrType::TmcdbRowAlreadyExistsEx& ex) {
        string msg = "Cannot retrieve the antenna location from the database.";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    }

    // Get a reference to the weather station component. The component name is
    // hard coded and, when COMP-3205 is resolved, should be changed to use a
    // default component.
    try {
        const string wsComponentName = "CONTROL/WeatherStationController";
        ws_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::WeatherStationController>
            (wsComponentName.c_str());

    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        string msg = "Cannot connect to the weather station";
        msg += " - using default values.";
        msg += " Detailed error trace follows.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        ex.log(LM_WARNING);
        ws_m.release();
    }

    // Initialise the position consumer
    positionConsumer_m = new PositionStreamConsumer(antennaName); 
    // set a time range so that we do not necessarily accumulate positions (as
    // someone else may turn on publication of the data)
    positionConsumer_m->setTimeRange(ACS::Time(1), ACS::Time(2));

    // Initialize the alarms sender
    {
        alarms_m.reset(new AlarmSender());

        vector<AlarmInformation> alarms(4);
        alarms[0].alarmCode = NO_COMMS_TO_WS; 
        alarms[0].alarmDescription = 
            "Cannot get a reference to the WeatherStationController component";
        alarms[1].alarmCode = WS_DISABLED;
        alarms[1].alarmDescription = 
            "All suitable weather stations are disabled.";
        alarms[2].alarmCode = OLD_WS_DATA;
        alarms[2].alarmDescription = "The weather data is too old.";
        alarms[3].alarmCode = NO_IERS_DATA;
        alarms[3].alarmDescription = 
            "Cannot get earth orientation (IERS) data.";
        alarms_m->initializeAlarms("MountController", antennaName, alarms);
        alarms_m->forceTerminateAllAlarms();
    }

    // Set these state variables to their default value. I cannot rely on the
    // constructor to do this as this component may be reused rather than being
    // destroyed & reconstructed.
    setTimeout(defaultTimeout);
    setEpoch(defaultEpoch);

    // I need to change the status to allocated here (a bit early) as
    // the setTolerance & setObservingFrequeny functions do not work
    // if we are uninitialized.
    status_m = Control::AntModeController::ALLOCATED;

    // Reset Mount component state parameters to their default value to prevent
    // coupling between SB's from different observers.  

    setObservingFrequency(defaultObsFreq);
    /// TODO. This should be done when the Array MountController starts as we
    /// cannot count on the MountControler component shutting down between
    /// SB's. e.g., a mount panel will keep the mount controller alive.
    setTolerance(MountImpl::DEFAULT_TOLERANCE);
    resetLimits();
    // TODO.
    // 1. Reload the pointing model
    // 2. Zero the auxiliary pointing model
    // 3. Reload the focus model
    // 4. Enable the focus model 
    // 5. Eventually metrology modes should also be reset to default values
}

void MountControllerImpl::releaseReferences() {
    // Its essential, to prevent container crashes, that the TrajectoryPlanner
    // thread is shutdown at this point.  Normally it would be done when the
    // user calls stop(), but, in case they forget to do this this I'll do this
    // here. Its done by hand, rather than calling stop() so I do not have to
    // deal with catching exceptions.
    if (!tpThread_m->isSuspended()) {
        string msg = "Suspending the TrajectoryPlannerThread";
        msg += " before releasing the component references.";
        msg += " Normally this would be done using the stop function.";
        LOG_TO_DEVELOPER(LM_WARNING, msg);
        tpThread_m->suspend();
    }

    // Clear the source list. This is not essential and I'm just being
    // defensive.  There is no need to acquire the mutex when accessing the
    // sourceList as the thread should be suspended.
    sourceList_m.clear();

    // Abort any data collection. Again I'm just being defensive
    unprotectedAbortDataCollection();

    // Deactivate the PositionStreamConsumer
    // Note that it is not deleted. This will be done by the ORB (I hope).
    if (positionConsumer_m != 0) {
        positionConsumer_m->disconnect();
        positionConsumer_m = 0;
    }

    // Release the mount component. The smart pointer will call
    // releaseComponent if necessary.
    mount_m.release();
 
    // release the weather station. The smart pointer will call
    // releaseComponent if necessary.
    ws_m.release();

    // clear all alarms as they are no longer relevant
    if (alarms_m.get() != 0) {
        alarms_m->forceTerminateAllAlarms();
        alarms_m.reset();
    }

    // Set the state to uninitialized, the antenna name to an empty string &
    // release the non-sticky reference to the antenna component.
    AntModeControllerImpl::releaseReferences();
}

casa::MPosition MountControllerImpl::
addPositions(const TMCDB_IDL::PadIDL& pi, 
             const TMCDB_IDL::AntennaIDL& ai,
             Logging::Logger::LoggerSmartPtr logger) {
    casa::MPosition 
        padLocation(casa::MVPosition(pi.XPosition.value, 
                                     pi.YPosition.value, 
                                     pi.ZPosition.value) 
                    , casa::MPosition::ITRF);
    // Now convert it to WGS84 and get the latitude and longitude 
    double padLat, padLong;
    {
        casa::MVPosition padLocationWGS = 
            casa::MeasConvert<casa::MPosition>
            (padLocation, casa::MPosition::WGS84)().getValue();
        padLat = padLocationWGS.getLat("rad").getValue();
        padLong = padLocationWGS.getLong("rad").getValue();
        ostringstream msg;
        msg << "Location of pad " << pi.PadName
            << ". Latitude: " << padLocationWGS.getLat("deg")
            << " Longitude: " <<  padLocationWGS.getLong("deg")
            << " Altitude: " <<  padLocationWGS.getLength("m");
        LOG_TO_OPERATOR_WITH_LOGGER(LM_DEBUG, msg.str(), logger);
    }

    // Now get the position of the antenna with respect to the pad
    const double antEast = ai.XPosition.value;
    const double antNorth = ai.YPosition.value;
    const double antUp = ai.ZPosition.value;

    // Now rotate (using the pad latitude and longitude) this local position to
    // one in the (X,Y,Z) reference frame.  The minus signs and 90-angle terms
    // were determined empirically
    casa::MVPosition antLocation(-1*antEast, -1*antNorth, antUp);
    antLocation *= 
        casa::RotMatrix(casa::Euler(M_PI_2-padLat, 0.0, M_PI_2-padLong));

    // Now that both the pad and the antenna are in the same frame we can add
    // them
    antLocation += padLocation.getValue();
    const casa::MPosition antLocationITRF(antLocation, casa::MPosition::ITRF);

    // Return the position in the WGS84 frame (as thats whats needed by SLALIB)
    return casa::MeasConvert<casa::MPosition>(antLocationITRF,
                                              casa::MPosition::WGS84)();
}
