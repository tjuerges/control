#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Mar 5, 2007  created
# tsh/rso   June20, 2007 re-implemented as a SpiralBase class.
# ntroncos  Sep 15, 2007 Refurbish to include new mount.


""" 
NAME
 CCL.MountController.SpiralBase

DESCRIPTION
 This class is part of MountController class. 
 It defines a spiral search pattern for moving a specific
 antenna.

CLASSES
 SimpleSpiral
"""

__version__ = "@(#) $Id$"
#$Source$

import CCL.MountController
import sys
import time
import traceback
import exceptions

class WrongModeException(exceptions.Exception):
    def __init__(self, msg = ""):
        self.errorMessage = msg
        return
    def __str__(self):
        return "The astronomical object %s cannot be seen at this time of day" \
               % self.errorMessage


class SpiralBase:
    """
    Base clase to do spiral search. This class provide the method and
    funtionality to be used by spiral search script. The scrips should 
    creat a new class sub classing SpiralBase, and overriding doDwell() 
    method. doDwell should provide the specific script functionality.
    """

    def __init__(self, antennaName=None, componentName=None):
        """
        The constructor creates a SpiralBase object.
    
        If the antennaName is defined then this constructor starts a
        dynamic component that implements the
        functions available in the SpiralBase object. The
        constructor also establishes the connection between this
        dynamic component and the relevant hardware components
        and the specified antenna.
    
        If the componentName is not none, then the object uses that component
        to implement the functions available in SpiralBase
        It is assumed that the component was allocated by other means and
        references to the relavent hardware already exist.  This method is
        intended for internal use, and should only be used by experts.
    
        An exception is thrown if there is a problem creating
        this component, establishing the connection to the
        previously mentioned hardware components, or if either
        both or neither antennaName and componentName are specified.
        Exceptions are documented in CCL.MountController.MountController
    
        EXAMPLE:
        # Load the necessary defintions
        import CCL.MountController
        # create the object
        sp = CCL.MountController.SpiralBase("DV01")
        # destroy the component
        del(sp)
        """ 
        self.mc = None
        self.mc = CCL.MountController.MountController(antennaName,
                                                      componentName)
        if (self.mc is None):
            return None

    def __del__(self):
        if (self.mc is not None):
            del(self.mc)

    def getCurrentOffsets(self):
        """
        Wraper for mc.getHorizonOffsets, returns azimuth and elevation 
        in radians.
        """
        return self.mc.getHorizonOffset()    

    def distance(self, x, y):
        """
        Calculates and returns the euclidean distance between 
        point (x,0) and (0,y).
        """
        return ((x ** 2) + (y ** 2)) ** 0.5

    def moveTo(self, x0, y0, x1, y1, step):
        """
        Calculate the offsets and apply them using mc.setHorizonOffset(x,y).

        """
        xOff = 0
        yOff = 0
        while (x0 != x1) or (y0 != y1):
            if (x1 > x0):
                x0 += 1
            elif (x1 < x0):
                x0 -= 1
            if (y1 > y0):
                y0 += 1
            elif (y1 < y0):
                y0 -= 1
            
            xOff = x0 * step
            yOff = y0 * step
            
            # Send offsets in Az/El, step is already in radians
            self.mc.setHorizonOffset(xOff, yOff)

            #generic hook method
            self.doDwell()
        return (x0,y0)

    # This method may be re-implemented in a child class. 
    # It is very useful for printing purposes.
    def doDwell(self):
        """
        This function should be overriden to do the inteligent 
        procesing of signals.
        """
        return True

    def doSpiral(self, maxDist, step, clear=False):
        """
        This function searches for a star by offsetting 
        the mc from its current position along a sprial pattern.
        The maxDist correxpond to the maximal distance
        (in arcmins) for searching in the spiral.
        The step parameter is the distance (in arcsec) to 
        be moved in each iteration of the spiral
        clear is a flag Boolean for deleting the current offsets of 
        the system, the default is False

        EXAMPLE:
        # Load the necessary definitions
        from CCL.MountController import SpiralBase
        #create the object
        sp = CCL.MountController.SpiralBase("DV01")
        #execute the spiral
        sp.doSpiral(2, 12.0, 55.0, True)
        #destroy the component
        del(sp)
        """
        if self.mc.isStopped():
            raise WrongModeException("Antenna is Stopped")
        if clear:
            # Reseting the offset
            self.mc.setHorizonOffset(0.0, 0.0)
        try:
            x = 0
            y = 0
            turn = 1
            mystep = 1
            dist = self.distance(x * step, y * step)
            while dist <= maxDist:
                if (turn % 4 == 1):
                    (x, y) = self.moveTo(x, y, x + mystep, y, step)
                elif (turn % 4 == 2):
                    (x, y) = self.moveTo(x, y, x, y + mystep, step)
                    mystep += 1
                elif (turn % 4 == 3):
                    (x, y) = self.moveTo(x, y, x - mystep, y, step)
                elif (turn % 4 == 0):
                    (x, y) = self.moveTo(x, y, x, y - mystep, step)
                    mystep += 1
                turn += 1
                dist = self.distance(x * step, y * step)
        except KeyboardInterrupt:
                pass
        except:
            ex = sys.exc_info()
            traceback.print_exception(ex[0], ex[1], ex[2])


#__o0o__
