#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------


""" 
NAME
 CCL.MountController.FivePointsBase

DESCRIPTION
 This class is part of MountController class. 
 It defines a five points search pattern for moving a specific
 antenna.

CLASSES
 FivePointsBase
"""

__version__ = "@(#) $Id$"
#$Source$

import CCL.MountController
import sys
import time
import traceback
import exceptions
from math import pi

class FivePointsBase:
    """
    DESCRIPTION
    """

    def __init__(self, antennaName=None, componentName=None):
        """
        Doc
        """ 
        self.mc = None
        self.mc = CCL.MountController.MountController(antennaName,
                                                      componentName)
        self.baseAzOffset = 0
        self.baseElOffset = 0
        self.offset = 0
        self.offSourceAz = 0 
        if (self.mc is None):
            return None

    def __del__(self):
        if (self.mc is not None):
            del(self.mc)

    def applyOffsets(self, Az, El):
        """
        Add the suplied offset to the actual offset of the anntena.
        units are arcseconds it internally transforms the into radians.
        """
        self.mc.setHorizonOffset(self.baseAzOffset + Az * pi / 180.0 / 3600.0, 
                           self.baseElOffset + El * pi / 180.0 / 3600.0)

    def clearOffsets(self):
        """
        Set horizon offsets to 0.0 and 0.0
        """
        self.mc.setHorizonOffset(0.0, 0.0)

    # This method must be re-implemented in a child class. 
    # It is very useful for printing purposes.
    def doDwell(self, position):
        """
        This function must be overriden to do the inteligent 
        procesing of signals.
        """
        raise exceptions.Exception("This method must be reimplemented in the child class")

    def doFivePoints(self):
        """
        Move the antenna in a 5 point patern with an offsource point 
        between each point. Method doDwell is invoked after every point. 
        """
        if self.mc.isStopped():
            raise WrongModeException("Antenna is Stopped")
        (self.baseAzOffset, self.baseElOffset) = self.mc.getHorizonOffset()

        if (self.options.quickForm == 0):
            #1(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(1)
            #2(1). High elevation
            self.mc.setHorizonOffset(self.baseAzOffset,
                                     self.baseElOffset + self.offset)
            self.doDwell(2)

            #3(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(3)
         
            #4(2). Low elevation
            self.mc.setHorizonOffset(self.baseAzOffset,
                                     self.baseElOffset - self.offset)
            self.doDwell(4)

            #5(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(5)
            
            #6(3). Center
            self.mc.setHorizonOffset(self.baseAzOffset,
                                     self.baseElOffset)
            self.doDwell(6)

            #7(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(7)
         
            #8(4). High azimuth
            self.mc.setHorizonOffset(self.baseAzOffset + self.offset,
                                     self.baseElOffset)
            self.doDwell(8)

            #9(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(9)
            
            #10(5). Low azimuth
            self.mc.setHorizonOffset(self.baseAzOffset - self.offset,
                                     self.baseElOffset)
            self.doDwell(10)

            #11(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(11)

        else:
            #1(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(1)
            #2(1). High elevation
            self.mc.setHorizonOffset(self.baseAzOffset,
                                     self.baseElOffset + self.offset)
            self.doDwell(2)
            #4(2). Low elevation
            self.mc.setHorizonOffset(self.baseAzOffset,
                                     self.baseElOffset - self.offset)
            self.doDwell(4)
            #6(3). Center
            self.mc.setHorizonOffset(self.baseAzOffset,
                                     self.baseElOffset)
            self.doDwell(6)
            #8(4). High azimuth
            self.mc.setHorizonOffset(self.baseAzOffset + self.offset,
                                     self.baseElOffset)
            self.doDwell(8)
            #10(5). Low azimuth
            self.mc.setHorizonOffset(self.baseAzOffset - self.offset,
                                     self.baseElOffset)
            self.doDwell(10)

            #11(-). offsource
            self.mc.setHorizonOffset(self.baseAzOffset + self.offSourceAz,
                                     self.baseElOffset)
            self.doDwell(11)

            
        #Return to center
        self.mc.setHorizonOffset(self.baseAzOffset,
                                 self.baseElOffset)
        self.doDwell(0)

    def initOffsets(self, Az, El):
         """
         Set offsets.
         Units are in arcseconds it internally transforms the into radians.
         """
         self.baseAzOffset = Az * pi / 180.0 / 3600.0
         self.baseElOffset = El * pi / 180.0 / 3600.0
         self.mc.setHorizonOffset(self.baseAzOffset, self.baseElOffset)

    def restoreOffsets(self):
         """
         Restore Base offsets.
         """
         self.mc.setHorizonOffset(self.baseAzOffset, self.baseElOffset)

    def setParams(self, offSource=0, offset=0):
        """
        Set the initial parameter for offSource and offset. 
        The unints must be arcsecs.
        """
        self.offset = offset * pi / 180.0 / 3600.0
        self.offSourceAz = offSource * pi / 180.0 / 3600.0

#__o0o__
