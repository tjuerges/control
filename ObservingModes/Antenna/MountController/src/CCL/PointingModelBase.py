'''
Load a new model from a file.
'''
from Acspy.Clients.SimpleClient import PySimpleClient
from string import split
import TMCDB

# schema for pointing database.
SCHEMA=[
    ('TS',"'%s'",'0000-00-00 00:00:00'),
    ('ABM','%d',1),
    ('Enabled','%d',1),
    ('BAND','%d',-1),
    ('IA','%.4f',0.),
    ('IE','%.4f',0.),
    ('HASA','%.4f',0.),
    ('HACA','%.4f',0.),
    ('HESE','%.4f',0.),
    ('HECE','%.4f',0.),
    ('HESA','%.4f',0.),
    ('HASA2','%.4f',0.),
    ('HASA3','%.4f',0.),
    ('HACA2','%.4f',0.),
    ('HESA2','%.4f',0.),
    ('HECA2','%.4f',0.),
    ('HACA3','%.4f',0.),
    ('HECA3','%.4f',0.),
    ('HESA3','%.4f',0.),
    ('NPAE','%.4f',0.),
    ('CA','%.4f',0.),
    ('AN','%.4f',0.),
    ('AW','%.4f',0.),
    ('COMMENT',"'%s'",'')
    ]

PT_TERMS = ['#', 'Enabled', 'BAND', 'IA', 'IE', 
    'HASA', 'HACA', 'HESE', 'HECE', 'HESA', 
    'HASA2', 'HASA3', 'HACA2', 'HESA2', 'HECA2', 
    'HACA3', 'HECA3', 'HESA3', 
    'NPAE', 'CA', 'AN', 'AW']

BAND_TO_ID={'OFF':-1, 'OPT':0, 'OPTICAL':0, 'BAND1':1, 'BAND2':2, 'BAND3':3,
    'BAND4':4, 'BAND5':5, 'BAND6':6, 'BAND7':7, 'BAND8':8, 'BAND9':9,
    'B100':100,'B240':240,
    None:None}



class PrintModel:
    def __init__(self, enabled, band, pm):
        self.pm = pm
        self.enabled = enabled
        self.band = band
    def __str__(self):
        msg = '%-16s%d\n' % ('Enabled',self.enabled)
        msg = msg + '%-16s%d\n' % ('BAND',self.band)

        for i in range(0,len(self.pm)):
	    data = self.pm[i]
            msg = msg + '%-15s%7.4f\n' % (data.name,data.value)       
        return msg

class PointingModel:
    '''
    Load and read the pointing models on the ABMs for a particular band.  The 
    PointingModel structure defined in Trajectory.idl is used to pass arguments 
    between PointingModel code and the user above.

    The bands are OFF, OPTICAL, BAND1, ..., BAND9
    '''
    def __init__(self, antenna):
        self.antenna = antenna
        self.client = PySimpleClient.getInstance()
        self._mount = self.client.getComponent("CONTROL/" + antenna + "/Mount")
        self.pmData = []
	self._pmDataIndex = [] 	
        self.enabled = False
        self.band = 3
    def __del__(self):
        # Release the components
        pass
    def getLoadedModel(self):
        '''
        Return the pointing model loaded into the software. 
        '''
        self.pmData = self._mount.getPointingModel()
	self.enabled = self.getPointingModelStatus()
        return self.pmData

    def getAuxLoadedModel(self):
        '''
        Return the pointing model loaded into the software. 
        '''
        self.pmData = self._mount.getAuxPointingModel()
        self.enabled = self.getAuxPointingModelStatus()
        return self.pmData


    def loadModel(self, pm):
        '''
        Put the pointing model on the mount.  

        '''
        self._mount.setPointingModel(pm)

    def loadAuxModel(self, pm):
        '''
        Put the aux pointing model on the mount.  

        '''
        self._mount.setAuxPointingModel(pm)
    
    def getModelFromFile(self, filename):
        '''
        Read file into PM structure

        Parameters:
            filename - required.  File contains NAME VALUE pairs, 
        '''
        fd=file(filename,'r')
        namevalues = fd.readlines()
        fd.close()

        i = 1
        for row in namevalues:
             line = split(row)
             #put this in a try/except
	     if( len(line) == 0):
		 pass # empty line
             elif( line[0] not in PT_TERMS):
                 pass #trow an exception
             elif( line[0] == '#'):
                 pass #commented line
             elif( line[0] == 'Enabled'):
                 self.enabled = int(line[1])
             elif( line[0] == 'BAND'):
                 self.band = int(line[1])
             else:
                 #poiting coefficients
		 pointingModelId = 0 #not used so far, waiting TMCDB database
		 coeffName = line[0]
		 coeffValue = float(line[1])
 
                 if(len(line) == 2):
                        coeffError = 0.0
                 elif (len(line) == 3):
                        coeffError = float(line[2])
                 else:
                        #raise an error due to wrong format of line
                        coeffError = 0.0

		 coef = TMCDB.ModelTerm(coeffName, coeffValue);

	         self.pmData.append(coef) 
		 self._pmDataIndex.append(coeffName) 
             i = i+1

    def addCoeffValue(self, coeffName=0 , deltaCoeffValue=0.0):
	if( coeffName == ""):
		return False

	try:
                for i in self.pmData:
                    if i.name == coeffName:
                        i.value += float(deltaCoeffValue)
		return True
		 
	except ValueError:
		#the coeffName does not exist in the file, just return
		return False

    def __band2id(self, band):
        try:
            return BAND_TO_ID[band]
        except:
            msg = 'Unknown BAND %s' % (band)
            raise msg

    def setPointingModelStatus(self, enabled):
	self._mount.enablePointingModel(enabled)

    def setAuxPointingModelStatus(self, enabled):
        self._mount.enableAuxPointingModel(enabled)

    def getPointingModelStatus(self):
        return self._mount.isPointingModelEnabled()

    def getAuxPointingModelStatus(self):
        return self._mount.isAuxPointingModelEnabled()


if __name__ == "__main__":

    filename = 'vertex.mod'
    antenna  = 'DV01'
    pt = PointingModel(antenna)
    pt.getModelFromFile(filename)
    print "read from file " + filename
    print PrintModel(pt.enabled, pt.band, pt.pmData)
    pt.loadModel(pt.pmData)
    test = pt.getLoadedModel()
    print "read from mount of " + antenna
    print PrintModel(pt.enabled, pt.band, test)
    print "setting Poiting Model Status..."
    pt.setPointingModelStatus(pt.enabled)
    print "isPoitingModelEnabled: " + str(pt.getPointingModelStatus())
