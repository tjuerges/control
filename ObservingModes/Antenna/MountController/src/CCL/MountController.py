#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
Defines the MountController class.
"""
import Acspy.Common.ErrorTrace
import ModeControllerExceptions
import ModeControllerExceptionsImpl
import ControlExceptions
import ControlExceptionsImpl
import math
import ScriptExec.rm
import maci
import CCL.Mount
import CCL.MountVertex
import CCL.MountACA
import CCL.MountAEM
import CCL.MountA7M
import maciErrType
import CCL.SIConverter

class MountController:
    '''
    The MountController object is used to manipulate the antenna mount
    in a general way but with an understanding of some astronomical
    concepts. Its core functions move an antenna to track a celestial
    source. This should be contrasted with the lower level Mount
    object that manipulates the antenna in azimuth and elevation or
    the higher level ArrayMountController object that moves groups of
    antennas.

    FUNCTIONALITY:
    The MountController object has over 50 functions that can be
    categorised as follows:
    * Mode changes
      track, stop, isStopped
    * Moving the antenna in horizon coordinates
      setAzEl, setAzElAsync
    * Moving the antenna in equatorial coordinates
      setDirection, setDirectionAsync, toAzElEquatorial,
      isObservableEquatorial, setEpoch, getEpoch,
      timeToRiseEquatorial, timeToSetEquatorial
    * Stowing the antenna
      survivalStow, maintenanceStow
    * Utility functions related to moving the antenna
      waitUntilOnSource, timeToSource, timeToSet, timeToSlew
      setTolerance, getTolerance, setTimeout, getTimeout
    * Getting the current position (see also Collection of pointing data)
      actualAzEl, commandedAzEl, actualRADec, commandedRADec
    * Collection of pointing data
      getPointingData, startPointingDataCollection,
      getPointingDataTable, getSelectedPointingDataTable,
      collectPointingData, collectingData, abortDataCollection
    * Refraction corrections
      setObservingFrequency, getObservingFrequency, getRefractionCoefficients
    * Tracking planets
      setPlanet, setPlanetAsync, toAzElPlanet, toRADecPlanet
      isObservablePlanet, timeToRisePlanet, timeToSetPlanet
    * Tracking other solar system objects
      setEphemeris, setEphemerisAsync, toAzElEphemeris, toRADecEphemeris
      isObservableEphemeris, timeToRiseEphemeris, timeToSetEphemeris
    * Offsets and Strokes in horizon coordinates
      setHorizonOffset, setHorizonOffsetAsync, setHorizonOffsetQueued
      incrementHorizonOffsetLat, incrementHorizonOffsetLong, 
      getHorizonOffset
      setHorizonLinearStroke, setHorizonLinearStrokeQueued
    * Offsets and Strokes in equatorial coordinates
      setEquatorialOffset, setEquatorialOffsetAsync, setEquatorialOffsetQueued
      incrementEquatorialOffsetLat, incrementEquatorialOffsetLong, 
      getEquatorialOffset, setEquatorialLinearStroke,
      setEquatorialLinearStrokeQueued
    * Offsets and Strokes in azimuth and elevation coordinates
      setAzElOffset, setAzElOffsetAsync, setAzElOffsetQueued,
      setAzElLinearStroke, setAzElLinearStrokeQueued
    * Pointing models
      getAuxPointingModel, setAuxPointingModel, zeroAuxPointingModel,
      isAuxPointingModelEnabled, getPointingModel, isPointingModelEnabled
    * Antenna Motion Limits
      setElevationLimit, getElevationLimit, resetLimits
    * Access to lower level functionality
      getMount
    * Scripting utilities
      azElToString, raDecToString

    EXAMPLE 1:
    # Move the antenna to track a calibrator
    # Load the necessary definitions
    import CCL.MountController
    from math import radians
    # create the object
    mc = CCL.MountController.MountController("DV01")
    # track the calibrator at RA = 19h34m, Dec = -63.8 degrees
    mc.setDirection("19:34", "-63.8deg")
    # stow the antenna
    mc.survivalStow()
    # destroy the component
    del(mc)

    UNITS:
    All functions in this class return values in SI
    units. Specifically angles are in radians, angular velocities are
    in radians/second, frequencies are in Hertz and time intervals are
    in seconds. Absolute times are in ACS time units which are the
    number of 100 nanosecond intervals since the start of Gregorian
    calendar.

    Parameters for all functions should also use SI units if a numeric
    value is given. Alternatively the user can specify the parameter
    using a string which contains a numeric value and a unit e.g., "10
    deg". If a string is used the units are checked to ensure the
    dimensionality is correct (although wavelength to frequency
    conversions are automatically done). See the documentation for the
    CCL.SIConverter class for a list of commonly used  unit strings.

    Absolute times must be in ACS time units which is a long long
    (64-bit) integer counting the number of 100 nanosecond intervals since
    the start of the Gregorian calendar on 1582-10-15T00:00:00.000.
    Shown below is an example of how to convert a calendar date
    to/from an ACS time.

    EXAMPLE 2
    # A. Convert a date/time to an ACS time unit
    # Load the necessary definitions
    import Acspy.Common.EpochHelper, acstime
    # Create an object for converting to an ACS time
    eph = Acspy.Common.EpochHelper.EpochHelper()
    # Load a time into this object
    eph.fromString(acstime.TSArray, "2008-03-27T18:45:00.000")
    startTime = eph.value().value

    # B. Convert an ACS time unit to a human readable string
    # Create a new object 
    eph = Acspy.Common.EpochHelper.EpochHelper(startTime)
    # convert this time back to a string
    print eph.toString(acstime.TSArray, "", 0, 0)
    '''

    def __init__(self, antennaName=None, componentName=None):
        '''
        The constructor creates a MountController object.

        The antennaName argument should be a string containing the
        antenna name. This constructor starts a dynamic component that
        implements the functions available in the MountController
        object. The constructor also establishes the connection
        between this dynamic component and the mount component in the
        specified antenna. An exception is thrown if there is a
        problem creating this component or establishing the connection
        to the mount component.

        If the componentName is not none, then the object uses that
        component to implement the functions available in this class.
        It is assumed that the component was allocated by other means
        and references to the relavent hardware already exist.  This
        method is intended for internal use, and should only be used
        by experts.

        Exceptions are thrown if there is a problem creating or
        connecting to this component, establishing the connection to
        the previously mentioned hardware components, or if either
        both or neither antennaName and componentName are specified.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.MountController
        # create the object
        mc = CCL.MountController.MountController("DV01")
        # destroy the component
        del(mc)
        '''
        try:
            # Indicates whether a reference to the CORBA component was
            # obtained (and hence whether the destructor needs to
            # relase it). If things work its set to true at the end
            # of this constructor. There are probably better ways to
            # do this.
            self.releaseComponent = False; 

            # Only allow one of the antennaName or componentName
            # arguments to be set.
            if not((antennaName == None) ^ (componentName == None)):
                raise;
            
            # 1. Get a reference to a, possible new created, mount
            # controller CORBA object.
            if antennaName != None:
                antennaComponentName = 'CONTROL/'+antennaName;
                antenna = ScriptExec.rm.getComponent(antennaComponentName)
                compName = antenna.getMountControllerName()
                ScriptExec.rm.releaseComponent(antennaComponentName)
                # The library name is specified in the CDB in Components.xml
                compSpec = maci.ComponentSpec\
                           (compName,
                            'IDL:alma/Control/MountController:1.0',
                            '*', '*')
                self.__mc = ScriptExec.rm.\
                     getCollocatedComp(compSpec, False, antennaComponentName);
                if (self.__mc == None): # Should throw an exception here
                    return None
                self.releaseComponent = True;
                self.__mc.allocate(antennaName);

            # 2. Specified previously existing component so just get the
            # reference. This option is for experts only.
            if componentName != None:
                self.__mc = ScriptExec.rm.getComponent(componentName)
                if (self.__mc == None): # Should throw an exception here
                    return None
                self.releaseComponent = True;

        except (maciErrType.CannotGetComponentEx), e:
            raise ModeControllerExceptionsImpl.CannotGetAntennaExImpl(exception=e);
        except (ControlExceptions.INACTErrorEx), e:
            raise ModeControllerExceptionsImpl.BadConfigurationExImpl(exception=e);
        except (ModeControllerExceptions.CannotGetAntennaEx, \
                ModeControllerExceptions.BadConfigurationEx), e:
            self.__mc.deallocate();
            raise;

    def __del__(self):
        '''
        The destructor shuts down the underlying mount controller
        component (assuming no other clients have references to
        it). This will releases the references the mount controller
        has to the mount component. It does not put the antenna into
        shutdown state or stow the antenna.

        EXAMPLE:
        See the example for the constructor (__init__ function)
        '''
        if (self.releaseComponent):
            ScriptExec.rm.releaseComponent(self.__mc._get_name());

    def track(self):
        '''
        Get the antenna ready to move. This puts the ACU in autonomous
        mode on both axes sequencing through standby mode, clearing
        faults and initializing encoders if necessary. It also resumes
        any threads that are necessary for commanding the antenna
        every 48ms. This function will implicitly be called by an
        command that will move the antenna.
        
        This function throws a:
        * MountFault exception if there is any problem communicating
        with the antenna mount. This includes hardware faults.

        EXAMPLE:
        See the example for the isStopped function
        '''
        return self.__mc.track()

    def stop(self):
        '''
        Stop the antenna at its current position. This stops all
        commands being sent to the Mount by clearing the current
        source and puts the ACU into standby mode on both axes. This
        function should work at all times unless the alma software
        cannot communicate with the ACU or if the ACU is is local
        access mode.
        
        This function throws a:
        * MountFault exception if there is any problem communicating
        with the antenna mount. This includes hardware faults and if
        the ACU is in local access mode.

        EXAMPLE:
        See the example for the isStopped function
        '''
        return self.__mc.stop()

    def isStopped(self):
        '''
        Returns True if the mount controller is stopped. When stopped
        the mount controller will not send commands to the ACU (via
        the mount component). Use the track command to enable commands
        to be sent the ACU and the stop command to stop the anternna
        motion.

        This function does not throw any exceptions.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.MountController
        from math import radians
        # create the object
        mc = CCL.MountController.MountController("DA41")
        # Move the antenna using the default tolerance and timeout
        if mc.isStopped(): mc.track()
        # Move the antenna
        mc.setDirection(0, 1);
        #stop the motion()
        mc.stop()
        # destroy the component
        del(mc)
        '''
        return self.__mc.isStopped()

    def setAzEl(self, az, el):
        '''
        Move the antenna to the specified azimuth and elevation and
        stop at that position. The azimuth & elevation are angles.
        This function will not return until the antenna is pointing at
        the specified position to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance function.
        
        This function throws a:

        * IllegalParameterError exception if:
          the specified azimuth and elevation are not angles;
          the azimuth is greater than +/- 3*PI/2 (270 degrees);
          the elevation  is less than 2*PI/180 (2 degrees) or greater
          than 88.9/90*PI/2 (88.9 degrees).
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        '''
        azInRad = CCL.SIConverter.toRadians(az)
        elInRad = CCL.SIConverter.toRadians(el)
        return self.__mc.setAzEl(azInRad, elInRad)

    def setAzElAsync(self, az, el):
        '''
        This is an asynchronous version of the setAzEl function. It
        commands the antenna to move but does not wait for the antenna
        to arrive at the specified position before returning. See the
        description of the setAzEl function for details of the
        arguments and exceptions this function can throw. Unlike the
        setAzEl function this function will not throw a Timeout
        exception. The waitUntilOnSource function can be used to
        determine when the antenna goes on source.
        '''
        azInRad = CCL.SIConverter.toRadians(az)
        elInRad = CCL.SIConverter.toRadians(el)
        return self.__mc.setAzElAsync(azInRad, elInRad)

    def setDirection(self, ra, dec, pmRA=0, pmDec=0, parallax=0):
        '''
        Move the antenna to the specified celestial position and track
        a star at that position. The right ascension & declination are
        angles, the proper motion on both axes are angular velocities
        and the parallax is an angle. This function will not return
        until the antenna is pointing at the specified position to
        within a specified tolerance. The angular tolerance is
        adjusted using the setTolerance function.

        The celestial positions *must* be in the FK5 coordinate system
        and, by default, should be in the 2000.0 epoch and
        equinox. The epoch and equinox can be changed using the
        setEpoch function.

        This function throws a:
        * IllegalParameterError exception if any of the supplied
          parameters have the wrong units or are out of range. Specifically if:
          commandedRA is greater than +/- 2*PI (360 degrees);
          commandedDec is greater than +/-PI/2;
          the source is below the horizon.
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self.__mc.setDirection(raInRad, decInRad, pmRAInRadPerSec, \
                                      pmDecInRadPerSec, parallaxInRad)

    def setDirectionAsync(self, ra, dec, pmRA=0, pmDec=0, parallax=0):
        '''
        This is an asynchronous version of the setDirection
        function. It commands the antenna to move but does not wait
        for the antenna to arrive at the specified position before
        returning. See the description of the setDirection function
        for details of the arguments and exceptions this function can
        throw. Unlike the setDirection function this function will not
        throw a Timeout exception. The waitUntilOnSource function can
        be used to determine when the antenna goes on source.
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self.__mc.setDirectionAsync(raInRad, decInRad, pmRAInRadPerSec,\
                                           pmDecInRadPerSec, parallaxInRad)

    def waitUntilOnSource(self):
        '''
        This function waits i.e., does not return, until the antenna
        is pointing at the last commanded position. This function can
        be used if a you need to wait for the antenna to be on-source
        before continuing with your script. Care should be taken when
        using this function with queued offsets. With queued offsets
        this function will indicate you are on-source and return but
        you need to check the offset is the expected one in the queue.
        
        This function throws a:
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        * Timeout exception (yes the same as the previous) if the
          mount controller is not tracking a source. In this case the
          function will returned immediately and not wait for the
          timeout interval to expire.
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults
        '''
        return self.__mc.waitUntilOnSource()

    def timeToSource(self):
        '''
        This function returns the estimated time, in seconds, it will take
        for this antenna to complete a slew i.e., for the
        "on-source". This function will return zero if the
        antenna already is on-source so its important to command the
        antenna to start moving to the new position before calling this
        
        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * IllegalParameter exception if the brakes are set or the
          antenna has not been commanded to track a source.
        '''
        return self.__mc.timeToSource()

    def timeToSlew(self, fromAz, fromEl, toAz, toEl):
        '''
        This function returns the estimated time, in seconds, it will
        take for this antenna to complete move between the two
        specified positions and go "on-source"
        
        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * IllegalParameter exception if any of teh specified positions
          its ouside the limits of the antenna
        '''
        fromAzInRad = CCL.SIConverter.toRadians(fromAz)
        fromElInRad = CCL.SIConverter.toRadians(fromEl)
        toAzInRad = CCL.SIConverter.toRadians(toAz)
        toElInRad = CCL.SIConverter.toRadians(toEl)
        return self.__mc.timeToSlew(fromAzInRad, fromElInRad, toAzInRad, toElInRad)

    def timeToSet(self):
        '''
        Returns the time, in seconds, before the source currently
        being tracked will go below the elevation limit of the
        antenna. Returns a very large number if the source never sets
        - perhaps because its circumpolar. The elevation limit can be
          set using the setElevationLimit function.

        This function throws a:
        * TIllegalParameter exception if the antenna is not currently
          tracking a source.
        '''
        return self.__mc.timeToSet();

    def survivalStow(self):
        '''
        Stow the antenna. This moves the antenna to the stow position,
        sets the mount to shutdown mode and inserts the stow
        pins. This is done using by putting the ACU in survival stow
        mode. The survival stow position offers the best protection
        against bad weather.

        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self.__mc.survivalStow()

    def maintenanceStow(self):
        '''
        Stow the antenna. This moves the antenna to the maintenance
        position, sets the mount to shutdown mode and inserts the stow
        pins. This is done using by putting the ACU in maintenance
        stow mode. The maintenance position allows access to the
        receiver cabin.

        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self.__mc.maintenanceStow()

    def actualAzEl(self):
        '''
        This function returns the current azimuth and elevation of the
        antenna. These returned values are in radians. In addition it
        returns, as a third argument, the time these values were
        meaasured. The time is an integer in ACS Time units (of
        hundreds nano-seconds).
        
        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self.__mc.actualAzEl()

    def commandedAzEl(self):
        '''
        This function returns the latest azimuth and elevation command
        sent to the antenna. These returned values are in radians.  In
        addition it returns, as a third argument, the time these
        values were meaasured. The time is an integer in ACS Time
        units (of hundreds nano-seconds).
        
        This function throws a:
        * MountFault exception if the mount controller is not
          commanding the mount
        '''
        return self.__mc.commandedAzEl()

    def actualRADec(self):
        '''
        This function returns the current right-ascension and
        declination of the antenna. These returned values are in
        radians.  In addition it returns, as a third argument, the
        time these values were meaasured. The time is an integer in
        ACS Time units (of hundreds nano-seconds).
        
        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self.__mc.actualRADec()

    def commandedRADec(self):
        '''
        This function returns the latest right-ascension and
        declination command sent to the antenna. These returned values
        are in radians.  In addition it returns, as a third argument,
        the time these values were meaasured. The time is an integer
        in ACS Time units (of hundreds nano-seconds).
        
        This function throws a:
        * MountFault exception if the mount controller is not
          commanding the mount
        '''
        return self.__mc.commandedRADec()

    def toAzElEquatorial(self, ra, dec, pmRA=0, pmDec=0, parallax=0):
        '''
        Returns the azimuth and elevation of the source at the
        specified right-ascension and declination (on this antenna at
        this instant in time). The returned azimuth an elevation
        includes effects due to precession, proper motion and
        refraction and is in the current epoch. The returned azimuth
        and elevation do not include the offsets introduced by the
        pointing model. See the setDirection function for a
        description of the input parameters.
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self.__mc.toAzElEquatorial(raInRad, decInRad, pmRAInRadPerSec, \
                                          pmDecInRadPerSec, parallaxInRad)

    def isObservableEquatorial(self, ra, dec, duration=0.0, \
                               pmRA=0, pmDec=0, parallax=0):
        '''
        Returns true if a source at the specified right-ascension and
        declination would be observable on this antenna at this
        instant in time. This really means "is the elevation of this
        source above the elevation limit of the antenna. Set the
        duration parameter to a positive value to determine if the
        source will be observable over an extended period of time
        (starting from now). The duration is in seconds. See the
        toAzElequatorial function for more details on what effects are
        included in the conversion to azimuth and elevation. See the
        setDirection function for a description of the input
        parameters. The elevation limit can be set using the
        setElevationLimit function.
        
        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        durationInSec = CCL.SIConverter.toSeconds(duration)
        return self.__mc.isObservableEquatorial\
               (raInRad, decInRad, durationInSec, \
                pmRAInRadPerSec, pmDecInRadPerSec, parallaxInRad)

    def timeToRiseEquatorial(self, ra, dec, pmRA=0, pmDec=0, parallax=0):
        '''
        Returns the time, in seconds, before the source at the
        specified right-ascension and declination will rise above the
        elevation limit of the antenna. This function will return zero
        if the source is already up and a negative number if the
        source never rises. See the toAzElEquatorial function for more
        details on what effects are included in determining the
        calculation. See the setDirection function for a description
        of the input parameters. The elevation limit can be set using
        the setElevationLimit function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self.__mc.timeToRiseEquatorial\
               (raInRad, decInRad,
                pmRAInRadPerSec, pmDecInRadPerSec, parallaxInRad)

    def timeToSetEquatorial(self, ra, dec, pmRA=0, pmDec=0, parallax=0):
        '''
        Returns the time, in seconds, before the source at the
        specified right-ascension and declination will go below the
        elevation limit of the antenna. This function will return a
        negative number of the source is already set and a very large
        positive number if the source never sets. See the
        toAzElEquatorial function for more details on what effects are
        included in determining the calculation. See the setDirection
        function for a description of the input parameters. The
        elevation limit can be set using the setElevationLimit
        function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self.__mc.timeToSetEquatorial\
               (raInRad, decInRad, \
                pmRAInRadPerSec, pmDecInRadPerSec, parallaxInRad)

    def setEpoch(self, epoch):
        '''
        Specify the epoch and equinox, as numeric value and in years,
        for all subsequent setDirection commands. This value, along
        with the parameters specified in the setDirection command,
        determine the correction due to both proper motion and
        precession. This is reset to the default value (2000.0) when
        the underlying component is shutdown and then restarted. This
        parameter has no effect for the setAzEl, setPlanet and
        setEphemeris functions.

        This function throws a:
        * IllegalParameterError exception if the epoch is less than 1800.0
          and more than 2200.0

        EXAMPLE:
        mc.setEpoch(2005.5)
        '''
        return self.__mc.setEpoch(epoch)

    def getEpoch(self):
        '''
        Get the epoch and equinox that is will be used to control the
        amount of proper motion and precession that is applied to all
        subsequent setDirection commands. Initially this function will
        return 2000.0 and, when set, remains in effect until changed
        (or the mount controller component is shutdown).

        EXAMPLE:
        '''
        return self.__mc.getEpoch()

    def setTolerance(self, tolerance):
        '''
        Set the angular separation that defines when an antenna is "on
        source". If the separation between the commanded and actual
        positions is less than this angle functions that wait for the
        antenna to start tracking a star, like the setDirection
        function, will complete.

        This function throws a:
        * IllegalParameterError exception if the specified value is
         not an angle between 0 and PI

        EXAMPLE:
        # Load the necessary defintions
        import CCL.MountController
        from math import radians
        # create the object
        mc = CCL.MountController.MountController("DA41")
        # Move the antenna using the default tolerance and timeout
        mc.setDirection(radians(-114.493), radians(87.37))
        # Halve the tolerance
        defaultTolerance = mc.getTolerance();
        mc.setTolerance(defaultTolerance/2.0);
        # Increase the timeout by 20 seconds.
        defaultTimeout = mc.getTimeout();
        mc.setTimeOut(defaultTimeout+20.0);
        # Move the antenna again
        mc.setDirection(radians(78), radians(34.0))
        # Stow the antenna
        mc.survivalStow()
        # destroy the component
        del(mc)
        '''
        toleranceInRad = CCL.SIConverter.toRadians(tolerance)
        return self.__mc.setTolerance(toleranceInRad);

    def getTolerance(self):
        '''
        Get the angular separation that defines when an antenna is "on
        source". The returned angle is in radians and will always be
        greater than zero and less than PI. The default value is
        10.0/60/60/180*PI (10 arc-sec).

        EXAMPLE:
        See the example for the setTolerance function
        '''
        return self.__mc.getTolerance();

    def setTimeout(self, timeout):
        '''
        Set the maximum time to wait for the antenna to move to the
        commanded position. The time is in seconds. If the antenna has
        not moved to the specified position, to within the required
        tolerance, within the specified amount of time then functions
        that move the antenna, like setDirection, will throw a Timeout
        exception. A negative value disables the timeout and should be
        used with caution.

        EXAMPLE:
        See the example for the setTolerance function
        '''
        timeoutInSec = CCL.SIConverter.toSeconds(timeout)
        return self.__mc.setTimeout(timeoutInSec);

    def getTimeout(self):
        '''
        Get the maxmimum time allowed for the antenna to move to the
        specified position. If the antenna has is not "on-source"
        after this amount of time then functions that move the
        antenna, like setDirection, will throw a Timeout exception.
        The returned time is in seconds and a negative value indicates
        that no timeout will be used. The default value is 110 seconds.

        EXAMPLE:
        See the example for the setTolerance function
        '''
        return self.__mc.getTimeout();

    def setPlanet(self, planetName):
        '''
        Move the antenna to point at the specified "planet". The
        planet name must be a string and one of the following:
        Mercury, Venus, Mars, Jupiter, Saturn, Neptune, Uranus,
        Neptune and Pluto, Sun & Moon. Note the last three are not
        officially planets and the Earth is not on the list.
        
        This function will not return until the antenna is pointing at
        the specified position to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance function.

        This function uses the JPL DE200 ephemeris to determine the
        planets position. The polynomial that describes the orbit is
        evaluated 21 times a second.

        This function throws a:
        * IllegalParameterError exception if you mistyped the planet
          name. The plane name is case-insensitive.  This exception will
          also be thrown if the specified planet is below the
          elevation limit of the antenna.
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout
          function).
        '''
        return self.__mc.setPlanet(planetName)

    def setPlanetAsync(self, planetName):
        '''
        This is an asynchronous version of the setPlanet function. It
        commands the antenna to move but does not wait for the antenna
        to arrive at the specified position before returning. See the
        description of the setPlanet function for details of the
        arguments and exceptions this function can throw. Unlike the
        setPlanet function this function will not throw a Timeout
        exception. The waitUntilOnSource function can be used to
        determine when the antenna goes on source.
        '''
        return self.__mc.setPlanetAsync(planetName)

    def toAzElPlanet(self, planetName):
        '''
        Returns the azimuth and elevation of the specified planet (on
        this antenna at this instant in time). The returned azimuth an
        elevation includes effects due to precession, proper motion
        and refraction and is in the J2000 epoch. The returned
        azimuth and elevation do not include the offsets introduced by
        the pointing model. See the setPlanet function for a
        description of the allowed planet names.
        '''
        return self.__mc.toAzElPlanet(planetName)

    def toRADecPlanet(self, planetName):
        '''
        Returns the right-ascension and declination of the specified
        planet (on this antenna at this instant in time). The returned
        position is in the J2000 epoch. See the setPlanet function for
        a description of the allowed planet names.
        '''
        return self.__mc.toRADecPlanet(planetName)

    def isObservablePlanet(self, planetName, duration=0.0):
        '''
        Returns true if the specified planet would be observable on
        this antenna at this instant in time. This really means "is
        the elevation of this planet above the elevation limit of the
        antenna. Set the duration parameter to a positive value to
        determine if the source will be observable over an extended
        period of time (starting from now). The duration is in
        seconds.  See the toAzElPlanet function for more details on
        what effects are included in the conversion to azimuth and
        elevation.  See the setPlanet function for a description of
        the allowed planet names. The elevation limit can be set using
        the setElevationLimit function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        durationInSec = CCL.SIConverter.toSeconds(duration)
        return self.__mc.isObservablePlanet(planetName, durationInSec)

    def timeToRisePlanet(self, planetName):
        '''
        Returns the time, in seconds, before the specified planet will
        rise above the elevation limit of the antenna. This function
        will return zero if the source is already up and a negative
        number if the source never rises. See the toAzElPlanet
        function for more details on what effects are included in
        determining the calculation. See the setPlanet function for a
        description of the input parameters. The elevation limit can
        be set using the setElevationLimit function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        return self.__mc.timeToRisePlanet(planetName)

    def timeToSetPlanet(self, planetName):
        '''
        Returns the time, in seconds, before the specified planet will
        go below the elevation limit of the antenna. This function
        will return a negative number of the source is already set and
        a very large positive number if the source never sets. See the
        toAzElPlanet function for more details on what effects are
        included in determining the calculation. See the setDirection
        function for a description of the input parameters. The
        elevation limit can be set using the setElevationLimit
        function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        return self.__mc.timeToSetPlanet(planetName)

    def setEphemeris(self, ephemeris):
        '''
        Move the antenna to point at an object defined using the
        specified ephemeris. See a description of the
        CCL.MountController.Ephemeris class for how to create and use
        an ephemeris object.
        
        This function will not return until the antenna is pointing at
        the specified object to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance function.

        This function throws a:
        * IllegalParameterError exception if there are some errors in
          the ephemeris. The CCL.MountController.Ephemeris class will
          define the constraints that are checked. This exception will
          also be thrown if the specified object is below the
          elevation limit of the antenna.
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout
          function).
        '''
        return self.__mc.setEphemeris(ephemeris.getData())

    def setEphemerisAsync(self, ephemeris):
        '''
        This is an asynchronous version of the setEphemeris
        function. It commands the antenna to move but does not wait
        for the antenna to arrive at the specified position before
        returning. See the description of the setEphemeris function
        for details of the arguments and exceptions this function can
        throw. Unlike the setEphemeris function this function will not
        throw a Timeout exception. The waitUntilOnSource function can
        be used to determine when the antenna goes on source.
        '''
        return self.__mc.setEphemerisAsync(ephemeris.getData())

    def toAzElEphemeris(self, ephemeris):
        '''
        Returns the azimuth and elevation of the object in the
        specified ephemeris (on this antenna at this instant in
        time). The returned azimuth an elevation includes effects due
        to precession, proper motion and refraction and is in the
        J2000 epoch. The returned azimuth and elevation do not include
        the offsets introduced by the pointing model. See the
        CCL.MountController.Ephemeris class for details of the input
        argument. 
        '''
        return self.__mc.toAzElEphemeris(ephemeris.getData())

    def toRADecEphemeris(self, ephemeris):
        '''
        Returns the right-ascension and declination of the object in
        the specified ephemeris (on this antenna at this instant in
        time). The returned position is in the J2000 epoch. See the
        CCL.MountController.Ephemeris class for details of the input
        argument.
        '''
        return self.__mc.toRADecEphemeris(ephemeris.getData())

    def isObservableEphemeris(self, ephemeris, duration=0.0):
        '''
        Returns true if the object in the specified ephemeris would be
        observable on this antenna at this instant in time. This
        really means "is the elevation of this planet above the
        elevation limit of the antenna. Set the duration parameter to
        a positive value to determine if the source will be observable
        over an extended period of time (starting from now). The
        duration is in seconds. See the toAzElEphemeris function for
        more details on what effects are included in the conversion to
        azimuth and elevation.  See the CCL.MountController.Ephemeris
        class for details of the input argument. The elevation limit
        can be set using the setElevationLimit function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        durationInSec = CCL.SIConverter.toSeconds(duration)
        return self.__mc.isObservableEphemeris(ephemeris.getData(), durationInSec)

    def timeToRiseEphemeris(self, ephemeris):
        '''
        Returns the time, in seconds, before the object in the
        specified ephemeris will rise above the elevation limit of the
        antenna. This function will return zero if the source is
        already up and a negative number if the source never
        rises. See the toAzElEphemeris function for more details on
        what effects are included in determining the calculation. See
        the setEphemeris function for a description of the input
        parameters. The elevation limit can be set using the
        setElevationLimit function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        return self.__mc.timeToRiseEphemeris(ephemeris.getData())

    def timeToSetEphemeris(self, ephemeris):
        '''
        Returns the time, in seconds, before the object in the
        specified ephemeris will go below the elevation limit of the
        antenna. This function will return a negative number if the
        source is already set and a very large positive number if the
        source never sets. See the toAzElEphemeris function for more
        details on what effects are included in determining the
        calculation. See the setEphemeris function for a description
        of the input parameters. The elevation limit can be set using
        the setElevationLimit function.

        This function throws a:
        * IllegalParameter exception if any of the input values are
          incorrect
        * MountFault exception if there is any problem determining the
          elevation limit of the antenna
        '''
        return self.__mc.timeToSetEphemeris(ephemeris.getData())

    def setHorizonOffset(self, longOffset, latOffset):
        '''
        Offset the commanded azimuth and elevation by the specified
        values. You must have previously commanded the antenna to move
        using functions like setAzEl, setDirection or setPlanet,
        before calling this function.

        The offsets are angles and in a spherical coordinate system
        with an origin that is centered about the position returned by
        the getHorizonReference function. Both offsets shift the
        position along a great circle. For example while a longitude
        offset will result in a change in azimuth it will usually also
        involve a smaller change in the elevation. A latitude offset
        will always involve just a change in elevation.

        These offsets are entirely independent of the equatorial
        offsets or the offsets that occur as part of the pointing
        model. Unlike the offsets that are part of the pointing model
        these offsets are cleared whenever you call the setAzEl or
        setDirection functions or any linear stroke becomes current.

        This function will not return until the antenna is pointing at
        the specified offset to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance function.
        This function will clear any queued offsets or strokes.
        
        This function throws a:
        * IllegalParameter exception if:
          either offset is not an angle;
          the longitude offset is bigger than +/-3*PI (540 degrees);
          the latitude offset is bigger than +/-PI/2 (90 degrees);
          if you are not tracking a source.
        * Timeout exception if the antenna does not go to the
          specified offset after a suitable amount of time (controlled
          using the setTimeout function).

        EXAMPLE:
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setHorizonOffset(longInRad, latInRad)

    def setHorizonOffsetAsync(self, longOffset, latOffset):
        '''
        This is an asynchronous version of the setHorizonOffset
        function. It immediately commands the antenna to move to the
        specified offset but does not wait for the antenna to arrive
        at the specified offset before returning. See the description
        of the setHorizonOffset function for details of the arguments
        and exceptions this function can throw. Unlike the
        setHorizonOffset function this function will not throw a
        Timeout exception.

        The waitUntilOnSource function can be used to determine when
        the antenna goes on source.

        EXAMPLE:
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setHorizonOffsetAsync(longInRad, latInRad)

    def setHorizonOffsetQueued(self, longOffset, latOffset, startTime):
        '''
        This is a queued version of the setHorizonOffsetAsync
        function. It commands the antenna to move to the specified
        offset, at the specified time. The start time is in ACS time
        units. Unlike the setHorizonOffsetAsync function this will not
        clear any queued offsets or strokes.

        The azimuth and elevation commands that are derived from this
        offset are queued and sent to the real-time software in
        advance of when they are needed. This means that the startTime
        must be more than about 4 seconds in the future to have the
        offset begin moving the antenna when expected. A start time of
        less than this will move the antenna as soon as possible which
        is currently about 3-4 seconds in the future. Use the
        setHorizonOffsetAsync command to move the antenna earlier
        (this command flushes the queues and can move it sooner).
        
        With this function the user could queue up a sequence of
        offsets to efficiently command the antenna to do, for example,
        a five-point observation. However the user is responsible for
        ensuring there is sufficient time between each offset in the
        queue as no check is made that the antenna has reached the
        specified offsets.

        EXAMPLE:
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setHorizonOffsetQueued(longInRad, latInRad, startTime)

    def incrementHorizonOffsetLongAsync(self, offsetInc):
        '''
        Increment (or decrement) the longitude component of the
        horizon offset by the specified value. The latitude offset is
        unchanged. This is an "incremental" version of the
        setHorizonOffsetAsync function. If the horizon offset is time
        varying, because for example you are doing a linear stroke,
        then the offset, in both latitude and longitude, will become a
        constant value with the longitude offset incremented from its
        instantaneous value. See description of the
        setHorizonOffsetAsync function for other details of this
        function.
        '''
        offsetInRad = CCL.SIConverter.toRadians(offsetInc)
        return self.__mc.incrementHorizonOffsetLongAsync(offsetInRad);

    def incrementHorizonOffsetLatAsync(self, offsetInc):
        '''
        Increment (or decrement) the latitude component of the horizon
        offset by the specified value. This is the same as the
        incrementHorizonOffsetLongAsync function except it increments
        the latitude component of the offset and leaves the longitude
        component unchanged.
        '''
        offsetInRad = CCL.SIConverter.toRadians(offsetInc)
        return self.__mc.incrementHorizonOffsetLatAsync(offsetInRad);

    def getHorizonOffset(self):
        '''
        Return the horizon offset that is currently being used to
        adjust the commanded azimuth and elevation.

        EXAMPLE:
        '''
        return self.__mc.getHorizonOffset();

    def setHorizonLinearStroke(self, longVelocity, latVelocity,
                               longOffset=0, latOffset=0):
        '''
        Command the antenna to move along a line at the specified rate
        from the specified offset. The offsets are angles and the
        velocities are angular velocities. This command will
        pre-position the antenna at the specified offset before
        starting the linear stroke. The motion will *not* stop until
        the antenna is given a new command. Once the motion has
        started there is no check that the antenna is tracking, to
        within the specified tolerance, the commanded positions. This
        function will return once the linear stroke has started i.e.,
        after the pre-positioning is complete and return the time the
        stroke actually started. This function will clear any queued
        offsets or strokes.

        The motion is along a great circle with its origin about the
        position returned by the getHorizonReference function. See the
        documentation for the function for setHorizonOffset additional
        details.

        This function throws a:
        * IllegalParameterError exception if:
          The velocity parameters are not angular velocities;
          The offset parameters are not angles;
          The latitude or longitude velocities are greater than
          +/-6*PI/180 (6 degrees/sec);
          The longitude offset is bigger than +/-3*PI (540 degrees);
          The latitude offset is bigger than +/-PI/2 (90 degrees);
          You are not tracking a source.
        * Timeout exception if the antenna does not go to the
          specified offset after a suitable amount of time (controlled
          using the setTimeout function).

        EXAMPLE:
        '''
        longVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(longVelocity)
        latVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(latVelocity)
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setHorizonLinearStroke\
               (longVelocityInRadPerSec, latVelocityInRadPerSec,
                longInRad, latInRad)
    
    def setHorizonLinearStrokeQueued(self, longVelocity, latVelocity,
                                     longOffset, latOffset, startTime):
        '''
        This is a queued version of the setHorizonLinearStroke
        function. It commands the antenna to start the linear stroke
        at the specified time (in ACS time units). If the specified
        time is in the past this function will immediately command the
        antenna to move but, as this start time is also the origin for
        the additional motion due to the velocity terms, a start time
        in the past will require the antenna to "catch up". A better
        option is to use a start time of zero which means "as soon as
        possible". This function will return the time the stroke
        started. This will be the same as the startTime argument
        unless the startTime argument is zero.  See the description
        of the setHorizonLinearStroke function for details of the
        other arguments and exceptions this function can throw. Unlike
        the setHorizonLinearStroke function this function:
        * does not clear any queued offsets or strokes
        * it does not wait for the antenna to arrive at the specified
          offset before beginning the linear stroke.
        * Does not throw a Timeout exception
       
        With this function the user could queue up a sequence of
        strokes and offsets to efficiently command the antenna to do,
        for example, a raster scan. However the user is responsible
        for ensuring there is sufficient time between each offset or
        stroke in the queue as no check is made that the antenna has
        reached the specified positions.

        EXAMPLE:
        '''
        longVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(longVelocity)
        latVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(latVelocity)
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setHorizonLinearStrokeQueued\
               (longVelocityInRadPerSec, latVelocityInRadPerSec, \
                longInRad, latInRad, startTime)


    def setEquatorialOffset(self, longOffset, latOffset):
        '''
        Offset the commanded right-ascension & declination by the
        specified values. You must have previously commanded the
        antenna to track a celestial source using functions like
        setDirection or setPlanet, before calling this function. You
        cannot offset in equatorial coordinates if the antenna is
        pointing at a fixed azimuth and elevation e.g, using setAzEl.

        The offsets are angles in a spherical coordinate system with
        an origin that is centered about the position returned by the
        getEquatorialReference function. Both offsets shift the
        position along a great circle. For example while a longitude
        offset will result in a change in right-ascension it will
        usually also involve a smaller change in the declination. A
        latitude offset will always involve just a change in
        declination.

        These offsets are entirely independent of the horizon offsets
        or the offsets that occur as part of the pointing
        model. Unlike the offsets that are part of the pointing model
        these offsets are cleared whenever you call the setAzEl or
        setDirection functions or any other linear equatorial stroke
        becomes current.

        This function will not return until the antenna is pointing at
        the specified offset to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance function.
        This function will clear any queued offsets or strokes.
        
        This function throws a:
        * IllegalParameter exception if
          the offsets are not angles
          the the longitude offset is bigger than +/-2*PI (360 degrees);
          the latitude offset is bigger than +/-PI (180 degrees);
          if you are not tracking a celestial source.
        * Timeout exception if the antenna does not go to the
          specified offset after a suitable amount of time (controlled
          using the setTimeout function).

        EXAMPLE:
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setEquatorialOffset(longInRad, latInRad)

    def setEquatorialOffsetAsync(self, longOffset, latOffset):
        '''
        This is an asynchronous version of the setEquatorialOffset
        function. It immediately commands the antenna to move to the
        specified offset but does not wait for the antenna to arrive
        at the specified offset before returning. See the description
        of the setEquatorialOffset function for details of the arguments
        and exceptions this function can throw. Unlike the
        setEquatorialOffset function this function will not throw a
        Timeout exception.

        The waitUntilOnSource function can be used to determine when
        the antenna goes on source.

        EXAMPLE:
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setEquatorialOffsetAsync(longInRad, latInRad)

    def setEquatorialOffsetQueued(self, longOffset, latOffset, startTime):
        '''
        This is a queued version of the setEquatorialOffsetAsync
        function. It commands the antenna to move to the specified
        offset, at the specified time, in ACS time units. Unlike the
        setEquatorialOffsetAsync function this will not clear any
        queued offsets or strokes.

        The azimuth and elevation commands that are derived from this
        offset are queued and sent to the real-time software in
        advance of when they are needed. This means that the startTime
        must be more than about 4 seconds in the future to have the
        offset begin moving the antenna when expected. A start time of
        zero means "as soon as possible" which is currently about 3-4
        seconds in the future. Use the setEquatorialOffsetAsync
        command to move the antenna earlier (this command flushes the
        queues and can move it sooner).
        
        With this function the user could queue up a sequence of
        offsets to efficiently command the antenna to do, for example,
        a five-point observation. However the user is responsible for
        ensuring there is sufficient time between each offset in the
        queue as no check is made that the antenna has reached the
        specified offsets.
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setEquatorialOffsetQueued(longInRad, latInRad, \
                                                   startTime)

    def incrementEquatorialOffsetLongAsync(self, offsetInc):
        '''
        Increment (or decrement) the longitude component of the
        equatorial offset by the specified value. The latitude offset is
        unchanged. This is an "incremental" version of the
        setEquatorialOffsetAsync function. If the equatorial offset is time
        varying, because for example you are doing a linear stroke,
        then the offset, in both latitude and longitude, will become a
        constant value with the longitude offset incremented from its
        instantaneous value. See description of the
        setEquatorialOffsetAsync function for other details of this
        function.
        '''
        offsetInRad = CCL.SIConverter.toRadians(offsetInc)
        return self.__mc.incrementEquatorialOffsetLongAsync(offsetInRad);

    def incrementEquatorialOffsetLatAsync(self, offsetInc):
        '''
        Increment (or decrement) the latitude component of the equatorial
        offset by the specified value. This is the same as the
        incrementEquatorialOffsetLongAsync function except it increments
        the latitude component of the offset and leaves the longitude
        component unchanged.
        '''
        offsetInRad = CCL.SIConverter.toRadians(offsetInc)
        return self.__mc.incrementEquatorialOffsetLatAsync(offsetInRad);

    def getEquatorialOffset(self):
        '''
        Return the horizon offset that is currently being used to
        adjust the commanded right-ascension and declination.

        EXAMPLE:
        '''
        return self.__mc.getEquatorialOffset();

    def setEquatorialLinearStroke(self, longVelocity, latVelocity,
                                  longOffset=0, latOffset=0):
        '''
        Command the antenna to move along a line at the specified rate
        from the specified offset. The offsets are angles and the
        velocities are angular velocities. This command will
        pre-position the antenna at the specified offset before
        starting the linear stroke. The motion will *not* stop until
        the antenna is given a new command. Once the motion has
        started there is no check that the antenna is tracking, to
        within the specified tolerance, the commanded positions. This
        function will return once the linear stroke has started i.e.,
        after the pre-positioning is complete. This function will
        clear any queued offsets or strokes.

        The motion is along a great circle with its origin about the
        position returned by the getEquatorialReference function. See the
        documentation for the function for setEquatorialOffset additional
        details.

        This function throws a:
        * IllegalParameterError exception if:
          the velocities are not anglular velocities;
          the offsets are not angles;
          The latitude or longitude velocities are greater than
          +/-6*PI/180 (6 degrees/sec);
          The longitude offset is bigger than +/-2*PI (360 degrees);
          The latitude offset is bigger than +/-PI (180 degrees);
          You are not tracking a celestial source.
        * Timeout exception if the antenna does not go to the
          specified offset after a suitable amount of time (controlled
          using the setTimeout function).

        EXAMPLE:
        '''
        longVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(longVelocity)
        latVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(latVelocity)
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setEquatorialLinearStroke\
               (longVelocityInRadPerSec, latVelocityInRadPerSec, \
                longInRad, latInRad)
    
    def setEquatorialLinearStrokeQueued(self, longVelocity, latVelocity,
                                        longOffset, latOffset, startTime):
        '''
        This is a queued version of the setEquatorialLinearStroke
        function. It commands the antenna to start the linear stroke
        at the specified time, in ACS time units. If the specified
        time is in the past this function will immediately command the
        antenna to move but, as this start time is also the origin for
        the additional motion due to the linear (non-zero velocity)
        terms, a start time in the past will require the antenna to
        "catch up". A better option is to use a start time of zero
        which means "as soon as possible". See the description of the
        setEquatorialLinearStroke function for details of the other
        arguments and exceptions this function can throw. Unlike the
        setEquatorialLinearStroke function this function: * does not
        clear any queued offsets or strokes * it does not wait for the
        antenna to arrive at the specified offset before beginning the
        linear stroke.  * Does not throw a Timeout exception
       
        With this function the user could queue up a sequence of
        strokes and offsets to efficiently command the antenna to do,
        for example, a raster scan. However the user is responsible
        for ensuring there is sufficient time between each offset or
        stroke in the queue as no check is made that the antenna has
        reached the specified positions.

        EXAMPLE:
        '''
        longVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(longVelocity)
        latVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(latVelocity)
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self.__mc.setEquatorialLinearStrokeQueued\
               (longVelocityInRadPerSec, latVelocityInRadPerSec,
                longInRad, latInRad, startTime)

    def setAzElOffset(self, azOffset, elOffset):
        '''
        Offset the commanded azimuth and elevation by the specified
        values. You must have previously commanded the antenna to move
        to a fixed posiition using functions like setAzEl before
        calling this function. These offsets cannot be used when
        tracking a a celestial source - for this you should use
        setHorizonOffset.

        Unlike the offsets in horizon or equatorial coordinates these
        offsets are *not* in a spherical coordinate system. Instead
        they individually manipulate the azimuth and elevation axes.

        These offsets are entirely independent of the equatorial
        offsets or the offsets that occur as part of the pointing
        model but they cannot be used in conjuntion with any horizon
        offsets. Unlike the offsets that are part of the pointing
        model these offsets are cleared whenever you call the setAzEl
        or setDirection functions or any linear stroke becomes
        current.

        This function will not return until the antenna is pointing at
        the specified offset to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance function.
        This function will clear any queued offsets or strokes.
      
        This function throws a:
        * IllegalParameter exception if the the azimuth offset is
          bigger than +/-3*PI (540 degrees) or the elevation offset is
          bigger than +/-PI/2 (90 degrees) or if you are not tracking
          a source.
        * Timeout exception if the antenna does not go to the
          specified offset after a suitable amount of time (controlled
          using the setTimeout function).

        EXAMPLE:
        '''
        azInRad = CCL.SIConverter.toRadians(azOffset)
        elInRad = CCL.SIConverter.toRadians(elOffset)
        return self.__mc.setAzElOffset(azInRad, elInRad)

    def setAzElOffsetAsync(self, azOffset, elOffset):
        '''
        This is an asynchronous version of the setAzElOffset
        function. It immediately commands the antenna to move to the
        specified offset but does not wait for the antenna to arrive
        at the specified offset before returning. See the description
        of the setAzElOffset function for details of the arguments
        and exceptions this function can throw. Unlike the
        setAzElOffset function this function will not throw a
        Timeout exception.

        The waitUntilOnSource function can be used to determine when
        the antenna goes on source.
        '''
        azInRad = CCL.SIConverter.toRadians(azOffset)
        elInRad = CCL.SIConverter.toRadians(elOffset)
        return self.__mc.setAzElOffsetAsync(azInRad, elInRad)

    def setAzElOffsetQueued(self, azOffset, elOffset, startTime):
        '''
        This is a queued version of the setAzElOffsetAsync
        function. It commands the antenna to move to the specified
        offset, at the specified time. Unlike the setAzElOffsetAsync
        function this will not clear any queued offsets or strokes.

        The azimuth and elevation commands that are derived from this
        offset are queued and sent to the real-time software in
        advance of when they are needed. This means that the startTime
        must be more than about 4 seconds in the future to have the
        offset begin moving the antenna when expected. A start time of
        zero means "as soon as possible" which is currently about 3-4
        seconds in the future. Use the setAzElOffsetAsync command to
        move the antenna earlier (this command flushes the queues and
        can move it sooner).
        
        With this function the user could queue up a sequence of
        offsets to efficiently command the antenna to a sequence of
        positions. However the user is responsible for ensuring there
        is sufficient time between each offset in the queue as no
        check is made that the antenna has reached the specified
        offsets.
        '''
        azInRad = CCL.SIConverter.toRadians(azOffset)
        elInRad = CCL.SIConverter.toRadians(elOffset)
        return self.__mc.setAzElOffsetQueued(azInRad, elInRad, startTime)

    def setAzElLinearStroke(self, azVelocity, elVelocity,
                            azOffset=0, elOffset=0):
        '''
        Command the antenna to move along a line at the specified rate
        from the specified offset. The offsets are angles and the
        velocities are angular velocities. This command will
        pre-position the antenna at the specified offset before
        starting the linear stroke. The motion will *not* stop until
        the antenna is given a new command. Once the motion has
        started there is no check that the antenna is tracking, to
        within the specified tolerance, the commanded positions. This
        function will return once the linear stroke has started i.e.,
        after the pre-positioning is complete and return the time the
        stroke actually started. This function will clear any queued
        offsets or strokes.

        This function throws a:
        * IllegalParameterError exception if:
          The velocity parameters are not angular velocities;
          The offset parameters are not angles;
          The azimuth velocity is greater than +/-6*PI/180 (6 degrees/sec);
          The elevation velocity is greater than +/-3*PI/180 (3 degrees/sec);
          The longitude offset is bigger than +/-3*PI (540 degrees);
          The latitude offset is bigger than +/-PI/2 (90 degrees);
          You are not offsetting about a fixed point in azimuth and elevation.
        * Timeout exception if the antenna does not go to the
          specified offset after a suitable amount of time (controlled
          using the setTimeout function).

        EXAMPLE:
        '''
        azVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(azVelocity)
        elVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(elVelocity)
        azInRad = CCL.SIConverter.toRadians(azOffset)
        elInRad = CCL.SIConverter.toRadians(elOffset)
        return self.__mc.setAzElLinearStroke\
               (azVelocityInRadPerSec, elVelocityInRadPerSec, azInRad, elInRad)
    
    def setAzElLinearStrokeQueued(self, azVelocity, elVelocity,
                                  azOffset, elOffset, startTime):
        '''
        This is a queued version of the setAzElLinearStroke
        function. It commands the antenna to start the linear stroke
        at the specified time (in ACS time units). If the specified
        time is in the past this function will immediately command the
        antenna to move but, as this start time is also the origin for
        the additional motion due for the velocity terms, a start time
        in the past will require the antenna to "catch up". A better
        option is to use a start time of zero which means "as soon as
        possible". This function will return the time the stroke
        started. This will be the same as the startTime argument
        unless the startTime arguement is zero.  See the description
        of the setAzElLinearStroke function for details of the
        other arguments and exceptions this function can throw. Unlike
        the setHorizonLinearStroke function this function:
        * does not clear any queued offsets or strokes
        * it does not wait for the antenna to arrive at the specified
          offset before beginning the linear stroke.
        * Does not throw a Timeout exception
       
        With this function the user could queue up a sequence of
        strokes and offsets to efficiently move the antenna. However
        the user is responsible for ensuring there is sufficient time
        between each offset or stroke in the queue as no check is made
        that the antenna has reached the specified positions.

        EXAMPLE:
        '''
        azVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(azVelocity)
        elVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(elVelocity)
        azInRad = CCL.SIConverter.toRadians(azOffset)
        elInRad = CCL.SIConverter.toRadians(elOffset)
        return self.__mc.setAzElLinearStrokeQueued\
               (azVelocityInRadPerSec, elVelocityInRadPerSec, \
                azInRad, elInRad, startTime)

    def setObservingFrequency(self, frequency):
        '''
        Specify the observing frequency/wavelength. This value is
        used, by the mount controller, to correct for atmospheric
        refraction and by the Mount to change pointing and focus
        models (if the observing band changes). It will *not* tune any
        receivers.

        The specified observing frequency will be used for all
        subsequent commands that move the antenna. It is reset to the
        default value (86.234GHz) when the underlying component is
        shutdown and then restarted.

        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * IllegalParameterError exception if the specified value is
          not a frequency or wavelength and is less than 100THz but
          not in an ALMA receiver band.
        '''
        frequencyInHz = CCL.SIConverter.toHertz(frequency)
        return self.__mc.setObservingFrequency(frequencyInHz)

    def getObservingFrequency(self):
        '''
        Get the observing frequency. See the setObservingFrequency
        function for a description of where this is used. Initially
        the observing frequency is set at a default value and, when
        set, remains in effect until changed (or the mount controller
        component is shutdown).

        EXAMPLE:
        '''
        return self.__mc.getObservingFrequency()

    def getRefractionCoefficients(self):
        '''
        Get the refraction coefficients currently being used. The
        refraction coefficients are in radians and are the A & B terms
        in the equation:
        ZD_vac = ZD_obs + A*tan(ZD_obs) + B*tan^3(ZD_obs)
        where:
        ZD_vac is the topocentric zenith distance in a vacuum
        ZD_obs is the observed topocentric zenith distance 

        If the antenna is at a fixed position i.e., its commanded
        using the setAzEl function or not been commanded at all, then
        no refraction correction is done and this function returns two
        zeros.
        
        EXAMPLE:
        '''
        return self.__mc.getRefractionCoefficients()

    def getMount(self):
        '''
        Return the mount object used by this observing mode.  This
        will allow you access to many detailed aspects of the antenna
        position. However it should only be used by people who
        understand how this object interacts with this hardware
        controller. It is made available so that experts can
        manipulate the antenna servo in ways beyond the scope of this
        object. Until the python facade class is written this will
        just return the component name. A zero length string is
        returned if this object does not have a reference to the mount
        object.
        '''
        componentName = self.__mc.getMount();
        antenna = componentName.split('/')[1]
        if antenna.startswith('DV'):
            return CCL.MountVertex.MountVertex(componentName=componentName)
        elif antenna.startswith('PM'):
            return CCL.MountACA.MountACA(componentName=componentName)
        elif antenna.startswith('DA'):
            return CCL.MountAEM.MountAEM(componentName=componentName)
        elif antenna.startswith('CM'):
            return CCL.MountA7M.MountA7M(componentName=componentName)
        return CCL.Mount.Mount(componentName=componentName)

    def getPointingData(self):
        '''
        Get the pointing data. This function returns a time
        synchronized snapshot of the current commanded and measured
        direction of the mount (including all offsets).

        The pointing data that is returned is a dictionary that
        contains information about where the antenna was pointing at
        the current time. It contains the fields:
        * target: This is the commanded direction of the source. It
          does not include offsets in the equatorial or horizon
          coordinate system.
        * equatorial: This is an offset, with axes aligned with the
          equatorial coordinate system, that is added to the
          above-mentioned target direction.
        * equatorialRADec: This is the resultaant change in
          right-ascension and declination that occurs because of the
          equatorial offset.
        * horizon: This is an offset, with axes aligned with the
          horizon coordinate system, that is added to the sum of the
          target direction and equatorial offset after the rotation to
          horizon coordinates.
        * horizonAzEl: This is the resultant change in azimuth and
          elevation that occurs because of the horizon offset.
        * pointing: This is the correction applied by the pointing
          model. This correction is added to the commands just before
          they sent to the antenna control unit (ACU).  This is the
          sum of both the main and auxiliary pointing models.
        * commanded: This is the commanded direction after the
          addition of both the horizon and equatorial offsets and
          after all pointing models are applied but before any
          metrology correction. It is, in radians, the value sent to
          the antenna control unit (ACU) and is in horizon
          coordinates.
        * measured: This is the measured direction as obtained from
          the ACU using the AZ_POSN_RSP & EL_POSN_RSP monitor points
          (the value aligned with the timing event). It includes
          corrections made by the metrology system but does not
          include any pointing model corrections made by the mount
          component. The measured and commanded values can be
          subtracted to obtain the pointing error in the antenna servo
          system.
        * measuredTarget: This is the measured direction converted to
          equatorial coordinates. The horizon and equatorial offsets
          have been removed prior to doing this conversion. It is in
          the same coordinate system as the target field.
        * onSource: True if the difference between the commanded and
          measured directions are less than the tolerance obtained by the
          getTolerance function.
        * timestamp: This is the time, in ACS Time units of hundreds
          nano-seconds, that is associated with this data.
        * stopped: Returns true if the mount controller is not sending
          commands to the ACU. In this case only the measured
          directions (measured and measuredTarget fields) of the
          returned dictionary are meaningful.
        
        The target, equatorialRADec and measuredTarget fields are a
        dictionary with two elements ra & dec.
        The commanded, horizonAzEl, pointing and measured fields are
        dictionaries with two elements az & el.
        The equatorial and horizon fields are dictionaries with two
        elements lat & lng.
        All angles are in radians.

        This function throws a:
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self.__mc.getPointingData()

    def startPointingDataCollection(self, startTime, stopTime):
        '''
        Start collecting pointing data. This will tell the mount
        controller to start accumulating pointing data that can be
        returned using the getPointingDataTable or
        getSelectedPointingDataTable functions. The start and stop
        times are integers in ACS Time units (of hundreds of
        nano-seconds).

        If the start time is in the past this function will begin
        collecting data as soon as possible. The start time cannot be
        more than 25hours in the future (as this is probably an
        indication of a software error elsewhere). The stop time must
        be in the future, greater than the start time, and but not
        more than 25 hours ahead of the start time (to limit the
        amount of memory used). This function will return immediatly
        and the return value indicates how long, in seconds, till the
        data collection completes.

        This function throws a:
        * IllegalParameterError exception if the start or stop times
          are incorrect (as described above).
        * DeviceBusy exception if the mount controller is already
          collecting data (even if the data collection starts in the
          future).
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self.__mc.startPointingDataCollection(startTime, stopTime)

    def getPointingDataTable(self, timeout = 300.0):
        '''
        Get the pointing data. This function returns all the pointing
        data that has been collected by the mount controller as a
        result of the last successful execution of the
        startPointingDataCollection function. A zero length sequence
        is returned if startPointingDataCollection has never been
        called. If the mount controller is currently collecting data
        this function will block until the data collection is
        completed. The timeout parameter is the time, in seconds, that
        specifies how long this function will wait for the data
        collection to complete. The timout must always be non-negative
        and defaults to 300 seconds (5 mins).

        The pointing data that is returned is a list with zero or more
        elements. Each element of this list is a dictionary that
        contains, for a different time, information about where the
        antenna was pointing. See the getPointingData function for a
        more detailed description of each field in this
        dictionary. The data will always be sorted by increasing time
        and typically there will be a new direction every 48ms.

        This function throws a:
        * IllegalParameterError exception if the timeout is negative.
        * Timeout exception is thrown if the data collection is not
          complete after the specified amount of time.
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        timeoutInSec = CCL.SIConverter.toSeconds(timeout)
        return self.__mc.getPointingDataTable(timeoutInSec)

    def getSelectedPointingDataTable(self, startTime, stopTime, timeout = 300.0):
        '''
        Get the pointing data between the specified times. This is
        identical to the getPointingDataTable function except that a
        subset of the data is returned. The start and stop times must
        be between the values specified in the
        startPointingDataCollection function. If the start and stop
        times are on timing event boundaries then data at the start
        time will be returned and the data at the stop time will not.
        '''
        timeoutInSec = CCL.SIConverter.toSeconds(timeout)
        return self.__mc.getSelectedPointingDataTable(startTime, stopTime, timeoutInSec)

    def collectPointingData(self, duration):
        '''
        Collect pointing data. This function starts data collection as
        soon as possible (typically a few seconds in the future) and
        collects pointing data for the specified amount of time (in
        seconds). It blocks until data collection is complete and then
        returns the pointing data. See the getPointingDataTable
        function for a description of the data returned by this
        function.

        This function throws a:
        * IllegalParameterError exception if the duration is less than
          0.048 seconds or greater than 25*60*60 seconds (25hrs)
        * Timeout exception is thrown if the data collection is not
          complete after the expected amount of time.
        * A DeviceBusy exception is thrown if the mount controller is
          already collecting data (even if the data collection starts
          in the future).
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        durationInSec = CCL.SIConverter.toSeconds(duration)
        return self.__mc.collectPointingData(durationInSec)

    def collectingData(self):
        '''
        Return whether we are collecting data and the data collection
        times. This function will return a tuple whose first element
        is true if we are currently collecting pointing data.

        The remaining two elements of the tuple are when the data
        collection has started (or will start) and when the data
        collection ended (or will end). If a start time that is in the
        past was specified the returned start time will be the actual
        start time. If data collection has completed the start and
        stop times will then reflect the start and stop times of the
        last successful call to startPointingDataCollection.

        This function does not throw any exceptions.
        '''
        return self.__mc.collectingData()

    def abortDataCollection(self):
        '''
        Abort the current pointing data collection. This will abort
        the current data collection process. Data currently collected
        will be available through the getPointingDataTable function
        and the actual times of the last data collection, including an
        adjustment of the stop time to reflect the call to this
        function, will be available through the collectingData
        function.

        This function does not throw any exceptions.
        '''
        return self.__mc.abortDataCollection()

    def getPointingModel(self):
        '''
        Get the current pointing model for this antenna.

        This pointing model is is used to correct large-scale
        imperfections in the antenna and should not normally be used
        by the observer to tweak the pointing for a specific
        observation. Instead the observer should use the auxiliary
        pointing model. Functions for changing the primary pointing
        model are in the CCL.Mount class and should only be used by
        knowledgeable users.

        See the description of the CCL.Mount class for a full
        description of this function as this function is identical
        except that it throws different exceptions.

        This function throws a:
        * MountFault exception if there is any problem getting the
          pointing model from the antenna mount.
        '''
        return self.__mc.getPointingModel()

    def isPointingModelEnabled(self):
        '''
        Returns True if the pointing model for this antenna is
        enabled. See the description of the CCL.Mount class for a full
        description of this function as this function is identical
        except that it throws different exceptions.

        This function throws a:
        * MountFault exception if there is any problem getting this
          information from the antenna mount.
        '''
        return self.__mc.isPointingModelEnabled()

    def getAuxPointingModel(self):
        '''
        Get the current auxiliary pointing model for this antenna. See the
        description of the CCL.Mount class for a full description of
        this function as this function is identical except that it
        throws different exceptions.

        This function throws a:
        * MountFault exception if there is any problem getting the
          pointing model from the antenna mount.
        '''
        return self.__mc.getAuxPointingModel()

    def setAuxPointingModel(self, coefficients):
        '''
        Set the current auxiliary pointing model for this
        antenna. Setting this pointing model also enables it. See the
        description of the CCL.Mount class for more information on
        this function as this function.

        This function throws a:
        * MountFault exception if there is any problem setting the
          pointing model in the antenna mount.
        * IllegalParameter exception if there is a problem with the
          specified coefficients.
        '''
        return self.__mc.setAuxPointingModel(coefficients)

    def zeroAuxPointingModel(self):
        '''
        Sets all coefficients to zero in the auxiliary pointing model
        for this antenna. Zeroing the pointing model also disables it.
        See the description of the CCL.Mount class for more
        information on this function as this function.
        This function throws a:

        * MountFault exception if there is any problem setting the
          pointing model in the antenna mount.
        '''
        return self.__mc.zeroAuxPointingModel()

    def isAuxPointingModelEnabled(self):
        '''
        Returns True if the auxiliary pointing model for this antenna is
        enabled. See the description of the CCL.Mount class for a full
        description of this function as this function is identical
        except that it throws different exceptions.

        This function throws a:
        * MountFault exception if there is any problem getting this
          information from the antenna mount.
        '''
        return self.__mc.isAuxPointingModelEnabled()

    def azElToString(self, az, el):
        '''
        Returns the specified position in a string of the form
        "(+DDD.MM.SS.sss, +DD.MM.SS.sss)". 
        '''
        azInRad = CCL.SIConverter.toRadians(az)
        elInRad = CCL.SIConverter.toRadians(el )
        return self.__mc.azElToString(azInRad, elInRad)

    def raDecToString(self, ra, dec):
        '''
        Returns the specified position in a string of the form
        "(+HH:MM:SS.sss, +DD.MM.SS.sss)".
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        return self.__mc.raDecToString(raInRad, decInRad)

    def setElevationLimit(self, elevationLimit):
        '''
        Set the minimum elevation for all commands sent to the
        antenna. This value must be greater than the hardware limit.
        Typically this is two degrees.

        This function will throw an
        * IllegalParameterError exception if the specified elevation
          limit is less than the hardware limit or if its bigger than
          the maximum elevation (88.9 degrees)
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        elevationLimitInRad = CCL.SIConverter.toRadians(elevationLimit)
        return self.__mc.setElevationLimit(elevationLimitInRad)
        
    def getElevationLimit(self):
        '''
        Get the minimum elevation for all commands sent to the
        antenna. The number returned is in radians and, by default,
        this is two degrees.

        This function will throw a
        * MountFault exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self.__mc.getElevationLimit()

    def resetLimits(self):
        '''
        Reset all antenna motion limits to the maximum value allowed
        by the antenna. Typically this is from -270 to +270 degres in
        azimuth and 2.0 to 88.9 dgerees in elevation.

        This function does not throw any exceptions. If the elevation
        limits cannot be reset a log message is generated
        '''
        return self.__mc.resetLimits()


class Ephemeris:
    '''
    The Ephemeris object is a helper class that is used with the
    MountController functions that take an ephemeris as an an argument
    (setEphemeris etc.). 

    EXAMPLE:
    # Load the necessary defintions
    import CCL.MountController
    # create an ephemeris object
    ep = CCL.MountController.Ephemeris()
    # Put some data into it
    ep.addRow(1, 2, 3, 4);
    ep.addRow(1.1, 2.1, 2.9, 4.01);
    ep.addRow(1.2, 2.2, 2.8, 4.02);
    # Create a MountController object and use the ephemeris.
    mc = CCL.MountController.MountController('DV01')
    mc.setEphemeris(ep)
    # stow the antenna
    mc.survivalStow()
    '''
    def __init__(self):
        self.__ephemeris = [];

    def addRow(self, time, ra, dec, rho):
        # It would be nice if I could just import
        # Control.MountController. However this does not work
        '''
        Add a row to the ephemeris. Each row contains 4 values, The
        time in ACS  Time units, the right ascension in radians, the
        declination in radians, and the distance to the object in
        meters.
        '''
        import Control
        self.__ephemeris.append(\
            Control.EphemerisRow(time, ra, dec, rho))

    def getData(self):
        return self.__ephemeris
