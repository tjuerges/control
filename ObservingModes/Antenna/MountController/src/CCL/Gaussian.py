#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "$Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# ntroncos  2007/10/09  Gaussian class to support gaussian average 
#                       for five point.

""" 
NAME
"""

__version__ = "$Id$"
#$Source$



import time
import sys
import exceptions
import CCL.MountController
from Numeric import zeros, array
from math import log, sqrt, exp, pi
import Acspy.Common.EpochHelper
import acstime

class Gaussian:
    """
    DESCRIPTION
    """

    def __init__(self,offset,initialOffsets,gausName,quickForm,antennaName=None,componentName=None):
        """
        Init the gaussian class calculator.
        """
        self.__initialAzOffset = eval(initialOffsets[0])
        self.__initialElOffset = eval(initialOffsets[1])
        self.__gausName = gausName
        self.__offset = offset
        self.__quickForm = quickForm
        self.clear()
        self.mc = None
        self.mc = CCL.MountController.MountController(antennaName,
                                                      componentName)

    def __del__(self):
        if (self.mc is not None):
            del(self.mc)
    
    def calculate(self, TPreads=None):
        """
	Calculate the gausian center using the reading of 5 points and 6
	offsource points. This does return no value. Only sets them in the
	gaussian object.
    
        GAUSSIAN CALCULATION:
	Each axis has 3 on source readings and 4 offsource reading. First we
	calculate 3 reading differences as the difference between the on source
	reading and the average of the previous and after offsource reading.
	ex To calculate the diference corresponding to the high elevation point
	we must do:
            ey1 = HE - (OS1 + OS2)/2
	Then we have the power diferences between being on source and offsource
	for point in elevation and azimuth. This difference must all be
	diferent between each other, and greater than zero.
    
        With the three point is posible to calculate the correction term as:
            - ec = X_2 + (X_3 - X_2)*((log(ey1)-log(ey3)) /
	          (2*(log(ey1)+log(ey3) - 2*log(ey2))))
        The error of such correction is calculated as:
              et12 = ((ec - X_2)^2 - (X_3 - ec)^2 / (2 * (log(ey1) - log(ey2)))
              et32 = ((ec - X_2)^2 - (X_3 - ec)^2 / (2 * (log(ey3) - log(ey2)))
              SIGMA = sqrt(et12|et32) select which is greater than 0
        
        This same expresion is used for elevation and azimuth readings.
        """
        x2 = 0
        x3 = self.__offset
    
        #elevation calculations
        if (self.__quickForm == 0): 
            ey1 = TPreads[2] - (TPreads[1] + TPreads[3])/2.0
            ey2 = TPreads[6] - (TPreads[5] + TPreads[7])/2.0
            ey3 = TPreads[4] - (TPreads[3] + TPreads[5])/2.0
        else:
            # interpolate between the first and final OFF
            ey1 = TPreads[2] - (TPreads[1]*5 + TPreads[11])/6.0
            ey2 = TPreads[6] - (TPreads[1] + TPreads[11])/2.0
            ey3 = TPreads[4] - (TPreads[1]*2 + TPreads[11])/3.0
        
#            print "ey1 TPreads: 2, 1, 3 = ", TPreads[2], TPreads[1], TPreads[3]
#            print "ey2 TPreads: 6, 5, 7 = ", TPreads[6], TPreads[5], TPreads[7]
#            print "ey3 TPreads: 4, 3, 5 = ", TPreads[4], TPreads[3], TPreads[5]

        if ey1 == ey2 and ey2 == ey3 and ey3 == ey1:
            raise GaussianEx("All ey values are equal", [ey1, ey2, ey3])
        if  ey1 <= 0 or ey2 <= 0 or ey3 <= 0:
            raise GaussianEx("One of the ey values is < 0", [ey1, ey2, ey3])
        if ( 2 * ( log(ey1) + log(ey3) - 2 * log(ey2)) == 0):
            raise GaussianEx("( 2 * ( log(ey1) + log(ey3) - 2 * log(ey2))" +
	                     " = 0, preventing devision by zero", 
			     [ey1, ey3, ey2])
        ec = x2 + ( x3 - x2 ) * (( log(ey1) - log(ey3) ) / ( 2 * ( log(ey1) + log(ey3) - 2 * log(ey2))))
    
        et12 = ((ec - x2)**2 - (x3 - ec )**2) / (2 * (log(ey1) - log(ey2)))
    
        et32 = ((ec - x2)**2 - (x3 - ec )**2) / (2 * (log(ey3) - log(ey2)))
    
        if et12 == 0 and et32 == 0:
            raise GaussianEx("Calculating Sigma both et values = 0",
	                      [et12, et32])
        if et12 < 0 and et32 < 0:
            raise GaussianEx("Calculating Sigma both et values <0",
	                      [et12, et32])
        sigmaEl = sqrt(max(et12, et32))
        ampEl = exp((( x2 - ec ) / sigmaEl )**2 / 2. ) * ey2    
        fwhpEl = 2 * sqrt( 2 * log(2)) * sigmaEl
    
        #azimuth calculations
        if (self.__quickForm == 0): 
            ay1 = TPreads[8] -  (TPreads[7] + TPreads[9])/2.0
            ay2 = TPreads[6] -  (TPreads[5] + TPreads[7])/2.0
            ay3 = TPreads[10] - (TPreads[9] + TPreads[11])/2.0
        else:
            # interpolate between the first and final OFF
            ay1 = TPreads[8] -  (TPreads[1] + TPreads[11]*2)/3.0
            ay2 = TPreads[6] -  (TPreads[1] + TPreads[11])/2.0
            ay3 = TPreads[10] - (TPreads[1] + TPreads[11]*5)/6.0
        
#            print "ay1 TPreads: 8, 7, 9 = ",TPreads[8],TPreads[7], TPreads[9]
#            print "ay2 TPreads: 6, 5, 7 = ",TPreads[6],TPreads[5], TPreads[7]
#            print "ay3 TPreads: 10,9,11 = ",TPreads[10],TPreads[9],TPreads[11]

        if ay1 == ay2 and ay2 == ay3 and ay3 == ay1:
            raise GaussianEx("All ay values are equal", [ay1, ay2, ay3])
        if  ay1 <= 0 or ay2 <= 0 or ay3 <= 0:
            raise GaussianEx("One of the ay values is < 0", [ay1, ay2, ay3])
        if ( 2 * ( log(ay1) + log(ay3) - 2 * log(ay2)) == 0):
            raise GaussianEx("( 2 * ( log(ay1) + log(ay3) - 2 * log(ay2))" +
	                     " = 0, preventing devision by zero", 
			     [ay1, ay3, ay2])
        print "      +Elev         |         %.4f" % (ey1,) 
        print "-Azim Center +Azim  |  %.4f %.4f  %.4f" % (ay3,0.5*(ey2+ay2),ay1)
        print "      -Elev         |         %.4f" % (ey3,)
        ac = x2 + ( x3 - x2 ) * (( log(ay1) - log(ay3) ) / ( 2 * ( log(ay1) + log(ay3) - 2 * log(ay2))))
        
        at12 = ((ac - x2)**2 - (x3 - ac )**2) / (2 * (log(ay1) - log(ay2)))
    
        at32 = ((ac - x2)**2 - (x3 - ac )**2) / (2 * (log(ay3) - log(ay2)))
    
        if at12 == 0 and at32 == 0:
            raise GaussianEx("Calculating Sigma both at values = 0",
	                      [at12, at32])
        if at12 < 0 and at32 < 0:
            raise GaussianEx("Calculating Sigma both at values <0",
	                      [at12, at32])
        sigmaAz = sqrt(max(at12, at32))
        ampAz = exp((( x2 - ac ) / sigmaAz )**2 / 2. ) * ay2    
        fwhpAz = 2 * sqrt( 2 * log(2)) * sigmaAz
    
        self.__gauAzOffset = -ac
        self.__gauElOffset = -ec
        self.__ampAz = ampAz
        self.__ampEl = ampEl
        self.__sigmaAz = sigmaAz
        self.__sigmaEl = sigmaEl
        self.__fwhpAz = fwhpAz
        self.__fwhpEz = fwhpEl
    
        self.__avgGauAzOffset += -ac
        self.__avgGauElOffset += -ec
        self.__avgAmpAz += ampAz
        self.__avgAmpEl += ampEl
        self.__avgSigmaAz += sigmaAz
        self.__avgSigmaEl += sigmaEl
        self.__avgFwhpAz += fwhpAz
        self.__avgFwhpEz += fwhpEl
#        print "Incrementing __dataIter from %d to %d" % (self.__dataIter,self.__dataIter+1)    
        self.__dataIter += 1
        
    def clear(self):
        """
        Clear all data in the gauss class holder.
        """
#        print "Running clear()"
        self.__gauAzOffset = 0.0
        self.__gauElOffset = 0.0
        self.__ampAz = 0.0
        self.__ampEl = 0.0
        self.__sigmaAz = 0.0
        self.__sigmaEl = 0.0
        self.__fwhpAz = 0.0
        self.__fwhpEz = 0.0
        self.__avgGauAzOffset = 0.0
        self.__avgGauElOffset = 0.0
        self.__avgAmpAz = 0.0
        self.__avgAmpEl = 0.0
        self.__avgSigmaAz = 0.0
        self.__avgSigmaEl = 0.0
        self.__avgFwhpAz = 0.0
        self.__avgFwhpEz = 0.0
        self.__dataIter = 0.0

    def getAvgGaussCorrection(self):
        """
	Return the average AZ EL correction. If either correction is bigger
        than 50 arcsec it will throw an GaussianLimitEx
        """
        avgAz = self.__avgGauAzOffset / self.__dataIter
        avgEl = self.__avgGauElOffset / self.__dataIter
        if avgAz > 50 or avgEl > 50:
            raise GaussianLimitEx("Correction is bigger than 50 threshold", 
                                  [avgAz, avgEl])
        return (avgAz, avgEl)


    def getCycles(self):
        """
        Return how many correct cycles has the gaussian made.
        """
        return self.__dataIter

    def printIntermidiate(self,initialAzOffset,initialElOffset):
        print "ITER: %d" % (self.__dataIter,)
        print "Timestamp    Azim   Elev   AzOff   ElOff  AzTotal  ElTotal ampAz ampEl sigmaAz sigmaEl fwhpAz fwhpEl"
        totalAz = self.__gauAzOffset + initialAzOffset
        totalEl = self.__gauElOffset + initialElOffset
        (currentAz, currentEl, timestamp) = self.mc.actualAzEl()
        currentAz *= 180.0/pi
        currentEl *= 180.0/pi
        timestring = Acspy.Common.EpochHelper.EpochHelper(timestamp).toString(acstime.TSArray,                                          
                                                                 "%H:%M:%S.%3q", 0, 0)
        print "%s %6.2f %.2f %+6.2f\" %+6.2f\" %+6.2f\" %+6.2f\"  %.4f %.4f %.3f %.3f %.1f\" %5.1f\"" % (timestring, currentAz, currentEl, self.__gauAzOffset, self.__gauElOffset, totalAz, totalEl, self.__ampAz, self.__ampEl, self.__sigmaAz, self.__sigmaEl, self.__fwhpAz, self.__fwhpEz)
        totalAz = self.__avgGauAzOffset/self.__dataIter + initialAzOffset
        totalEl = self.__avgGauElOffset/self.__dataIter + initialElOffset
        # put the label for arcseconds in the terminal display version
        if (self.__dataIter > 1):
            print "%s %6.2f %.2f %+6.2f\" %+6.2f\" %+6.2f\" %+6.2f\"  %.4f %.4f %.3f %.3f %.1f\" %5.1f\"" % (timestring, currentAz, currentEl, self.__avgGauAzOffset/self.__dataIter,
               self.__avgGauElOffset/self.__dataIter,
               totalAz,
               totalEl,
               self.__avgAmpAz/self.__dataIter,
               self.__avgAmpEl/ self.__dataIter,
               self.__avgSigmaAz/self.__dataIter ,
               self.__avgSigmaEl/self.__dataIter,
               self.__avgFwhpAz/self.__dataIter,
               self.__avgFwhpEz/self.__dataIter)

    def printFinal(self,initialAzOffset,initialElOffset):
        self.printIntermidiate(initialAzOffset,initialElOffset)
        if (len(self.__gausName) > 0): 
            f = open(self.__gausName,"a")
            totalAz = self.__avgGauAzOffset/self.__dataIter + initialAzOffset
            totalEl = self.__avgGauElOffset/self.__dataIter + initialElOffset
            (currentAz, currentEl, timestamp) = self.mc.actualAzEl()
            currentAz *= 180.0/pi
            currentEl *= 180.0/pi
            timestring = Acspy.Common.EpochHelper.EpochHelper(timestamp).toString(acstime.TSArray,                                          
                                                                 "%H:%M:%S.%3q", 0, 0)
            # do not put the label for arcseconds in the file version
            print >>f, "%s %6.2f %.2f %+6.2f %+6.2f %+6.2f %+6.2f %.4f %.4f %.3f %.3f %.1f %5.1f" % (timestring,currentAz,currentEl,self.__avgGauAzOffset/self.__dataIter,
               self.__avgGauElOffset/self.__dataIter,
               totalAz,
               totalEl,
               self.__avgAmpAz/self.__dataIter,
               self.__avgAmpEl/ self.__dataIter,
               self.__avgSigmaAz/self.__dataIter ,
               self.__avgSigmaEl/self.__dataIter,
               self.__avgFwhpAz/self.__dataIter,
               self.__avgFwhpEz/self.__dataIter)
            f.close()
            print "Output appended to file = %s" % (self.__gausName,)

class GaussianEx(exceptions.Exception):
    def __init__(self, error, values):
        self.error = error
	self.values = values
        return
    def __str__(self):
        return repr(self.error) + repr(self.values)

class GaussianLimitEx(exceptions.Exception):
    def __init__(self, error, values):
        self.error = error
	self.values = values
        return
    def __str__(self):
        return repr(self.error) + repr(self.values)
#__o0o__
