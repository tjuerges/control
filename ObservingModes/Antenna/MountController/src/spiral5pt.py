#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Mar 5, 2007  created
# eduvall   Jun 13, 2007 removed track, converted to CCL, added screen reports, converted to AzEL offsets
#                        added 5 point, added move to Gaussian center
# eduvall   Oct  5, 2007 Revised Guassian handling to improve robustness and --wait=4: Search on NEW CODE-10-04 


print """
IMPORTANT:

Separate system software sessions only occasionally "see" each other's offset values.

Because of this problem, having this routine zero the offsets usually does not generate expected results.

To avoid trouble it is best to zero offsets in mountPanel before starting this routine. 

Since stopping spiral routine and starting 5 point routine constitute two separate sessions, the two routines had to be combined to avoid this problem.


Typical call:
./spiral5point.py -a alma01 -f test21

"""

import optparse
parser = optparse.OptionParser()

parser.add_option("-d", "--doPrgs", dest = "doPrgs", default = 3, type = "int", 
  help = "Select programs to run: 1 = spiral only, 2 = 5 point only, 3 = both. Default: %default")

parser.add_option("-i", "--initial", type="str", dest="initialOffsets", default='', metavar='Az,El',
                  help="The initial AzEl Offsets to use e.g. 20,20 arcsecs. Leave empty to start at current position. Default: %default arcsecs.")

parser.add_option("-P", "--power", type="str", dest="getTP", default='YES', metavar='yes/no',
                  help="Get and show total power. Default: %default")

#Spiral options
parser.add_option("--\nSpiral options\n", metavar='****************************************************************************')

parser.add_option("-a", "--antenna", dest = "antenna", default = "", type = "str", 
  help = "Antenna to use for tracking.", 
  metavar = "ANTENNA")

parser.add_option("-x", "--clear", dest = "clear", default = "Yes", type = "str", 
  help = "Clear offsets when started? Default: %default.", 
  metavar = "y/n")

parser.add_option("-t", "--time", dest = "dwellTime", default = 1, type="float", 
  help = "How long to pause at each position in addition to collecting total power. Default: %default seconds.", 
  metavar = "SECONDS")

parser.add_option("--max", dest="maxOffset", default = 12.0, type="float", 
  help="How far out to search in arcmins. Default: %default arcmins")   # -m gets interpreted by ACS as manager reference.

parser.add_option("-p", "--step", dest="step", default = 55.0, type="float", 
  help="How far to step for each new position in arcsecs. Default %default arcsecs.")


# Five point options
parser.add_option("--\nFive point options\n", metavar='****************************************************************************')

parser.add_option("-z", "--offSourceAz", default=300.0, type="float", dest="offSourceAz",
                  help="Offset in Az when going 'offsource' arcsecs. Default: %default arcsecs.")

parser.add_option("-o", "--offset", default=30, type="float", dest="offset",
                  help="Offset from center in Az and El for the fivepoint in arcsecs. Default: %default arcsecs.")

# NEW CODE-10-04: Changed default from 3 to 4
parser.add_option("-w", "--wait", default=4, type="float", dest="wait",
                  help="How long to wait for antenna to reach destination. Default: %default seconds.")

parser.add_option("-c", "--cycles", default=1000, type="int", dest="cycles",
                  help="How many times to run entire 5 point cycle. Default: %default")

parser.add_option("-s", "--sleep", default=1, type="float", dest="sleep",
                  help="How long to pause between cycles. Default: %default seconds.")


# Total power options
parser.add_option("--\nTotal power options\n", metavar='****************************************************************************')

parser.add_option("-u", "--full", dest="full", default=True,
                  help="Move to off source between every move. \nTHIS PROGRAM is not ready for unfull operation. Default: %default")

parser.add_option("-b", "--baseband", default='A', type="str", dest="bb",
                  help="Use this baseband for 5 point calculations, range A,B,C,D. Default: %default")

parser.add_option("-r", "--repeat", default=10, type="int", dest="repeat",
                  help="Monitor total power how many times. Default: %default")

parser.add_option("-y", "--delay", default=0.1, type="float", dest="delay",
                  help="How long to delay between total power monitors. Default: %default seconds.")

parser.add_option("-f", "--file", default="", type="str", dest="fileName",
                  help="Enter file name to save five point data to file.\n"+\
                  "Name will be suffixed with '.csv'. Default: %default")

parser.add_option("-e", "--sum", default=0, type="int", dest="sumTPsets",
                  help="Sum across this many TP sets then do Gaussian fit - 0 for no summing. Default: %default")

parser.add_option("--displayTP", default='No', type="str", dest="displayTP", metavar='yes/no',
                  help="Display TP samples. Default: %default")


# Gaussian options
parser.add_option("--\nGaussian options\n", metavar='****************************************************************************')

parser.add_option("-g", "--gaus", default='No', type="str", dest="gaus",
                  help="Move antenna to Gaussian center. Default: %default.")

parser.add_option("-v", "--avg", default=4, type="int", dest="avg",
                  help="Average how many Gaussian results before moving. Default: %default.")

parser.add_option("-G", "--save", default="", type="str", dest="gausName",
                  help="Enter file name to save Gaussian results to file.\n"+\
                  "Name will be suffixed with '.csv'. Default: %default")

(options, args) = parser.parse_args()


print 'Importing libraries'
import struct, sys, time, types, traceback
from math import cos, degrees, radians, log, sqrt, exp

antenna = options.antenna.upper()

from ambManager import *
from ControlExceptions import *
import TETimeUtil
reload(Control) 
import CCL.MountController
from ScriptExec.rm import getComponent
mgr = AmbManager(antenna)

# Convert and check inputs
doPrgs = options.doPrgs
if doPrgs > 1:
  doPrgsi = 'Y'
else:
  doPrgsi = ''
clear = options.clear.upper()
dwellTime = options.dwellTime
maxOffset = options.maxOffset * 60.0
offSourceAz = options.offSourceAz
step = options.step
offSourceAz = options.offSourceAz        # seconds
offset = options.offset                  # seconds
bb = ord(options.bb.upper())-65  # convert lettered baseband to number
displayTP = options.displayTP.upper()
fileName = options.fileName
gausName = options.gausName
avg = float(options.avg)
repeat = options.repeat
sumTPsets = options.sumTPsets

mc = CCL.MountController.MountController(antenna)
mc.setTolerance(radians(5/3600.0))
mc.setTimeout(60)

az = el = curAzOffset = curElOffset = cnt = 0    # initialize variables here for global use
ecuAzOffset = gauAzOffset = gauElOffset = egaAzOffset = sigmaAz = ampAz = fwhpAz = sigmaEl = ampEl = fwhpEl = 0 
actData=[]
avgData = {'Spiral  ':[0,0,0,0],
           'Start   ':[0,0,0,0],
           'Initial ':[0,0,0,0],
           'os hi-el':[0,0,0,0],   # BB-A, BB-B, BB-C, BB-D, time
           'hi-el   ':[0,0,0,0],
           'os lo-el':[0,0,0,0],
           'lo-el   ':[0,0,0,0],
           'os cntr ':[0,0,0,0],
           'cntr    ':[0,0,0,0],
           'os hi-az':[0,0,0,0],
           'hi-az   ':[0,0,0,0],
           'os lo-az':[0,0,0,0],
           'lo-az   ':[0,0,0,0],
           'os end  ':[0,0,0,0],
           'Gaussian':[0,0,0,0],
           'Final   ':[0,0,0,0]}

def doSpiral():
  print 'Antenna:', antenna, '\n'
  printHeader('')
  try:
    if clear == 'YES':
      print 'Cleared offsets'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('Cleared offsets','Spiral  ',0,0,1,'')
      print ''
    else:
      print 'Incoming offsets'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('Incoming offsets','Spiral  ',0,0,0,'')
    if options.initialOffsets != '':
      iniOffsets = options.initialOffsets.split(',')
      incAzOffset = eval(iniOffsets[0])
      incElOffset = eval(iniOffsets[1])
      print 'Initial offsets'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('Initial offsets','Start   ', incAzOffset, incElOffset, 1, '')
#      az, el, curAzOffset, curElOffset = Offsets('Initial offsets','Spiral  ',0,0,0,'')
    x = 0
    y = 0
    turn = 1
    mystep = 1
    dist = distance(x * step, y * step)
    cnt = 0
    while dist <= maxOffset:
      cnt += 1
      if cnt % 18 ==  0:
        printHeader('')
      if (turn % 4 == 1):
        (x, y, az, el, curAzOffset, curElOffset) = moveTo(x, y, x + mystep, y, 'Moving Clockwise')
      elif (turn % 4 == 2):
        (x, y, az, el, curAzOffset, curElOffset) = moveTo(x, y, x, y + mystep, 'Moving Up')
        mystep += 1
      elif (turn % 4 == 3):
        (x, y, az, el, curAzOffset, curElOffset) = moveTo(x, y, x - mystep, y, 'Moving CntrClock')
      elif (turn % 4 == 0):
        (x, y, az, el, curAzOffset, curElOffset) = moveTo(x, y, x, y - mystep, 'Moving Down')
        mystep += 1
      turn += 1
      dist = distance(x * step, y * step)
      time.sleep(dwellTime)
    print "\nReached the maximum search distance. Stopping program.\n"
    sys.exist(0)
  except KeyboardInterrupt, ex:
    print "\nSpiral interrupted by user."
    """
    while 0:
      try:
        if doPrgs < 2:
          break  
        print "Spiral stopped by user."
        while str(doPrgsi).upper() not in ('Y','N','YES','NO'):
          doPrgsi = raw_input('Run five point y/n: ')
        if str(doPrgsi).upper() in ('Y','YES'):
          break
        elif str(doPrgsi).upper() in ('N','NO'):
          sys.exist(0)
      except KeyboardInterrupt, ex:
        continue
      except:
        ex = sys.exc_info()
        traceback.print_exception(ex[0], ex[1], ex[2])
    """
  except:
    ex = sys.exc_info()
    traceback.print_exception(ex[0], ex[1], ex[2])
  print 'Final values'.ljust(16),
  az, el, curAzOffset, curElOffset = Offsets('Final values','Spiral  ',0,0,0,'')
  print ''
  return az, el, curAzOffset, curElOffset
  # End of spiral


def fivePoint(az, el, curAzOffset, curElOffset):
  avgGaus=[]
  cntGaus=[0,0,0]        # total gaus calcs accepted, cycle cnt while approaching options.avg, gaus calcs accepted while approaching options.avg
  print '\nStarting Five Point routine'
  if fileName != '':
    fh = file(fileName+'.csv','a')
    printHeader(fh)
  else:
    fh = ''
    printHeader('')
  if gausName != '':
    fg = file(gausName+'.csv','a')
    GaussianFileHdr(fg)
  else:
    fg = ''
  if doPrgs == 3:
    print 'Incoming'.ljust(16),
    az, el, curAzOffset, curElOffset = Offsets('','Start   ', 0, 0, 0, fh)
  else:
    if options.initialOffsets != '':
      iniOffsets = options.initialOffsets.split(',')
      incAzOffset = eval(iniOffsets[0])
      incElOffset = eval(iniOffsets[1])
      print 'Initial offsets'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','Start   ', incAzOffset, incElOffset, 1, fh)
    else:
      print 'Initial offsets'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','Start   ', 0, 0, 0, fh)
  intAzOffset = curAzOffset
  intElOffset = curElOffset
  Gcycle = 0
  try:
    for cycle in range(1,options.cycles):
      printHeader('')

      print 'Initial offsets'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','Initial ', 0, 0, 0, fh)
  
      # 1(1). offsource
      print 'To offsource'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','os hi-el', intAzOffset + offSourceAz, intElOffset, 1, fh)
  
      # 2(2). N
      print 'To hi el'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','hi-el   ', intAzOffset, intElOffset + offset, 1, fh)

      # 3(-). offsource
      if (options.full):
        print 'To offsource'.ljust(16),
        az, el, curAzOffset, curElOffset = Offsets('','os lo-el', intAzOffset + offSourceAz, intElOffset, 1, fh)
  
      # 4(3). S
      print 'To lo el'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','lo-el   ', intAzOffset, intElOffset - offset, 1, fh)

      # 5(-). offsource
      if (options.full):
        print 'To offsource'.ljust(16),
        az, el, curAzOffset, curElOffset = Offsets('','os cntr ', intAzOffset + offSourceAz, intElOffset, 1, fh)

      # 6(4). Centre
      print 'To center'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','cntr    ', intAzOffset, intElOffset, 1, fh)

      # Get time stamp
      tm = time.time()
      epoch = TETimeUtil.unix2epoch(tm)
      (tm_year, tm_mon, tm_day, tm_hour, tm_min, tm_sec)= time.gmtime(tm)[:-3]

      # 7(-). offsource
      if (options.full):
        print 'To offsource'.ljust(16),
        az, el, curAzOffset, curElOffset = Offsets('','os hi-az', intAzOffset + offSourceAz, intElOffset, 1, fh)

      # 8(5). E (or maybe west)
      print 'To hi az'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','hi-az   ', intAzOffset + offset, intElOffset, 1, fh)

      # 9(-). offsource
      if (options.full):
        print 'To offsource'.ljust(16),
        az, el, curAzOffset, curElOffset = Offsets('','os lo-az', intAzOffset + offSourceAz, intElOffset, 1, fh)

      # 10(6). W (or maybe east)
      print 'To lo az'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','lo-az   ', intAzOffset - offset, intElOffset, 1, fh)

      # 11(7). offsource
      print 'To offsource'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('','os end  ', intAzOffset + offSourceAz, intElOffset, 1, fh)

      print 'Return to center'.ljust(16),
      az, el, curAzOffset, curElOffset = Offsets('Return to center','', intAzOffset, intElOffset, 1, fh)

      if sumTPsets == 0 or cycle % sumTPsets == 0:
        if sumTPsets > 0:
          for i in avgData:
            avgData[i][3] /= float(sumTPsets)
        gauAzOffset, gauElOffset, gausOk = Gaussian(az, el, curAzOffset, curElOffset, fg, cycle, epoch, cntGaus, avgData, avgGaus, tm_year, tm_mon, tm_day, tm_hour, tm_min, tm_sec)
        Gcycle += 1
        if gausOk and options.gaus.upper() in ('Y','YES') and Gcycle % int(avg) == 0:
          if abs(gauAzOffset) > 50 or abs(gauElOffset) > 50:
            print 'Average Gaussian Az or El offset too large: ', gauAzOffset, gauElOffset
          else:
            printHeader('')
            print 'Move to Gaussian'.ljust(16),
            az, el, curAzOffset, curElOffset = Offsets('','Gaussian', intAzOffset + gauAzOffset, intElOffset + gauElOffset, 1, fh)
            cntGaus[0] += 1
            cntGaus[1]  = 0
            cntGaus[2] += 1
            intAzOffset = curAzOffset
            intElOffset = curElOffset
        for i in avgData:
          avgData[i][3] = 0

      if options.sleep > 0:
        print 'Waiting '+str(options.sleep)+' seconds'
        time.sleep(options.sleep)

  except KeyboardInterrupt, ex:
    print "\n\nFive point stopped by user."
    print 'Return to center'.ljust(16),
    az, el, curAzOffset, curElOffset = Offsets('Return to center','', intAzOffset, intElOffset, 1, '')

  if fh != '':
    fh.close()
    fh = ''
  print 'Final values'.ljust(16),
  az, el, curAzOffset, curElOffset = Offsets('','Final   ', 0, 0, 0, fh)
  print ''
  # End of five point



#*****************************
# Subroutines

def moveTo(x0, y0, x1, y1, action):
  global cnt
  while (x0 != x1) or (y0 != y1):
    cnt += 1
    if cnt % 18 ==  0:
      printHeader('')
    if (x1 > x0):
      x0 += 1
    elif (x1 < x0):
      x0 -= 1
    if (y1 > y0):
      y0 += 1
    elif (y1 < y0):
      y0 -= 1
    comAzOffset = x0 * step
    comElOffset = y0 * step
    print action.ljust(16),
    az, el, curAzOffset, curElOffset = Offsets(action, 'Spiral  ',comAzOffset, comElOffset, 1,'')
    time.sleep(dwellTime)
  return (x0, y0, az, el, curAzOffset, curElOffset)


def Offsets(action, position, comAzOffset, comElOffset, move, fh):
  (azRad, elRad, tstamp) = mc.actualAzEl()
  if move:
    #elRad = getComponent(mc.getMount())._get_actualEl().get_sync()[0]
    mc.setHorizonOffset(sec2rad(comAzOffset),sec2rad(comElOffset))
    time.sleep(options.wait)
  #az = degrees(getComponent(mc.getMount)._get_actualAz().get_sync()[0])
  #el = degrees(getComponent(mc.getMount)._get_actualEl().get_sync()[0])
  az=degrees(azRad)
  el=degrees(elRad)
  curAzOffset, curElOffset = map(rad2sec, mc.getHorizonOffset())
  #curAzOffset *= cos(radians(el))
  if options.getTP and position != '':
    avgData[position][0] = 0
    avgData[position][1] = 0
    avgData[position][2] = 0
    # NEW CODE-10-04 add option
    if displayTP == 'YES':
      print '\n'
    for i in range(0,repeat):
      d,t = mgr.monitor(0,0x29,0x4)
      tm = time.time()
      d = struct.unpack('!8B',d)
      if position != '':
        actData.append((position, t, tm, d))
        collectedData = (bytes2int(d[bb*2:bb*2+2]) * 2.5 / 2**16)
        if displayTP == 'YES':
          print "Collected Data", collectedData
        avgData[position][2] += collectedData
      time.sleep(options.delay)
    if displayTP == 'YES':
      print '\n'.ljust(16)
    if position != '':
      avgData[position][0]  = t
      avgData[position][1]  = tm
      avgData[position][2] /= float(repeat)
      avgData[position][3] += avgData[position][2]
  print decAlign(comAzOffset,3).rjust(9)+'  '+decAlign(comElOffset,3).rjust(9)+' | '+sec2dms(comAzOffset/cos(radians(el)))+' '+sec2dms(comElOffset)+' || '+\
        decAlign(curAzOffset,3).rjust(9)+'  '+decAlign(curElOffset,3).rjust(9)+' | '+sec2dms(curAzOffset/cos(radians(el)))+' '+sec2dms(curElOffset)+' || '+\
        decAlign(az,6).rjust(11)+' '+decAlign(el,6).rjust(11)+' | '+deg2dms(az)+' '+deg2dms(el)+' || '+\
        decAlign(az - curAzOffset/cos(radians(el))/3600.0,6).rjust(11)+' '+decAlign(el - curElOffset/3600.0,6).rjust(11)+' | '+deg2dms(az - curAzOffset/cos(radians(el))/3600.0)+' '+deg2dms(el - curElOffset/3600.0),
  if options.getTP and position != '':
    print '|| '+decAlign(avgData[position][3],6)
  else:
    print ''
  if fh != '':
    tm = time.time()
    epoch = TETimeUtil.unix2epoch(tm)
    (tm_year, tm_mon, tm_day, tm_hour, tm_min, tm_sec)= time.gmtime(tm)[:-3]
    w = '"'+iif(action == '',position,action)+'",'+\
        str(epoch.value)+',"'+str(tm_year)+'-'+str(tm_mon)+'-'+str(tm_day).rjust(2,'0')+'","'+str(tm_hour).rjust(2,'0')+':'+str(tm_min).rjust(2,'0')+':'+str(tm_sec).rjust(2,'0')+'",'+\
        str(comAzOffset)+','+str(comElOffset)+','+sec2dms(comAzOffset)+','+sec2dms(comAzOffset/cos(radians(el)))+','+sec2dms(comElOffset)+','+\
        str(curAzOffset)+','+str(curElOffset)+','+sec2dms(curAzOffset)+','+sec2dms(curAzOffset/cos(radians(el)))+','+sec2dms(curElOffset)+','+\
        str(az)+','+str(el)+','+deg2dms(az)+','+deg2dms(el)+','+\
        str(az - curAzOffset/cos(radians(el))/3600.0)+','+str(el - curElOffset/3600.0)+','+deg2dms(az - curAzOffset/cos(radians(el))/3600.0)+','+deg2dms(el - curElOffset/3600.0)

    if options.getTP and position != '':
      w += ','+str(avgData[position][3])
    fh.write(w+'\r')
  return az, el, curAzOffset, curElOffset


def printHeader(fh):
  print '\n'
  print '                                Commanded offsets - Sky               ||              Reported offsets - Encoder              ||               Reported position - Encoder               ||           Base position = Reported less Offsets         ||  Ttl pwr\n'+\
        '                      Az   sec   El   |     Az/cos(el)       El       || Az*cos(el)      El   |       Az             El       ||     Az   deg    El      |       Az           El         ||     Az   deg    El      |       Az             El       ||    Avg  \n'
  if fh != '':
    fh.write(',"ALMA Time","UTC","UTC","Commanded offsets",,,,,"Reported Offsets",,,,,"Reported position",,,,"Base position",,,,"Total power",' + \
             '"Gaussian delta",,,,,"Azimuth",,,"Elevation",,,\r')
    fh.write('"Action","100ns","Date","Time","Az enc","El enc","Az enc","Az/cos(el)","El","Az enc","El enc","Az enc","Az/cos(el)","El","Az","El","Az","El","Az","El","Az","El","Avg",' +\
             '"Az enc","El enc","Az enc","Az/cos(el)","El","Sigma","Amp","FW@ 1/2P","Sigma","Amp","FW@ 1/2P"\r')
  return


def Gaussian(az, el, curAzOffset, curElOffset, fg, cycle, epoch, cntGaus, avgData, avgGaus, tm_year, tm_mon, tm_day, tm_hour, tm_min, tm_sec):
  print 'Calculate Gaussian center'
  try:
    cntGaus[2] += 1
    x2 = 0
    x3 = offset
#    print "avgData hi-el", avgData['hi-el   '][3], avgData['os hi-el'][3], avgData['os lo-el'][3]
    ey1 = avgData['hi-el   '][3] - (avgData['os hi-el'][3] + avgData['os lo-el'][3])/2.0
#    print "avgData cntr", avgData['cntr    '][3], avgData['os cntr '][3], avgData['os hi-az'][3]
    ey2 = avgData['cntr    '][3] - (avgData['os cntr '][3] + avgData['os hi-az'][3])/2.0
#    print "avgData lo-el", avgData['lo-el   '][3], avgData['os lo-el'][3], avgData['os cntr '][3]
    ey3 = avgData['lo-el   '][3] - (avgData['os lo-el'][3] + avgData['os cntr '][3])/2.0

    # NEW CODE-10-04: interdispersed here to improve robustness
    if ey1 <= 0 or ey2 <= 0 or ey3 <= 0:
      print "An El y <= 0 -- skipping moment calculation y1, y2, y3", ey1, ey2, ey3
      return 0, 0, 0
    if ey1 == ey2 and ey2 == ey3 and ey1 == ey3:
      print "El y values are identical -- skipping moment calculation y1, y2, y3", ey1, ey2, ey3
      return 0, 0, 0
    if log(ey1) + log(ey3) == 2 * log(ey2):
      print "El y cause x/0 in center calculation -- skipping moment calculation y1, y2, y3", ey1, ey2, ey3
      return 0, 0, 0
    ec = x2 + ( x3 - x2 ) * (( log(ey1) - log(ey3) ) / ( 2 * ( log(ey1) + log(ey3) - 2 * log(ey2))))
    et12 = (( ec - x2)**2 - ( x3 - ec )**2 ) / ( 2 * ( log(ey1) - log(ey2) ))
    if ey2 >= ey1 or et12 < 0:
      et32 = (( ec - x2)**2 - ( x3 - ec )**2 ) / ( 2 * ( log(ey3) - log(ey2) ))
      if ey2 >= ey3 or et32 < 0:
        print "El y cause sqrt(neg) or x/0 in sigma calc -- skipping moment calculation y1, y2, y3", ey1, ey2, ey3
        return 0, 0, 0
      sigmaEl = sqrt(et32)
    else:
      sigmaEl = sqrt(et12)
    if sigmaEl == 0:
      print "El sigma = 0 -- skipping moment calculation y1, y2, y3", ey1, ey2, ey3
      return 0, 0, 0
    ampEl = exp((( x2 - ec ) / sigmaEl )**2 / 2. ) * ey2 
    fwhpEl = 2 * sqrt( 2 * log(2)) * sigmaEl

    x2 = 0
    x3 = offset
    ay1 = avgData['hi-az   '][3] - (avgData['os hi-az'][3] + avgData['os lo-az'][3])/2.0
    ay2 = avgData['cntr    '][3] - (avgData['os cntr '][3] + avgData['os hi-az'][3])/2.0
    ay3 = avgData['lo-az   '][3] - (avgData['os lo-az'][3] + avgData['os end  '][3])/2.0
    if ay1 <= 0 or ay2 <= 0 or ay3 <= 0:
      print "An Az y <= 0 -- skipping moment calculation y1, y2, y3", ay1, ay2, ay3
      return 0, 0, 0
    if ay1 == ay2 and ay2 == ay3 and ay1 == ay3:
      print "Az y values are identical -- skipping moment calculation y1, y2, y3", ay1, ay2, ay3
      return 0, 0, 0
    if log(ay1) + log(ay3) == 2 * log(ay2):
      print "Az y cause x/0 in center calculation -- skipping moment calculation y1, y2, y3", ay1, ay2, ay3
      return 0, 0, 0
    ac = x2 + ( x3 - x2 ) * (( log(ay1) - log(ay3) ) / ( 2 * ( log(ay1) + log(ay3) - 2 * log(ay2))))
    at12 = (( ac - x2)**2 - ( x3 - ac )**2 ) / ( 2 * ( log(ay1) - log(ay2) ))
    if ay2 >= ay1 or at12 < 0:
      at32 = (( ac - x2)**2 - ( x3 - ac )**2 ) / ( 2 * ( log(ay3) - log(ay2) ))
      if ay2 >= ay3 or at32 < 0:
        print "Az y cause sqrt(neg) or x/0 in sigma calc -- skipping moment calculation y1, y2, y3", ay1, ay2, ay3
        return 0, 0, 0
      sigmaAz = sqrt(at32)
    else:
      sigmaAz = sqrt(at12)
    if sigmaAz == 0:
      print "Az sigma = 0 -- skipping moment calculation y1, y2, y3", ay1, ay2, ay3
      return 0, 0, 0
    ampAz = exp((( x2 - ac ) / sigmaAz )**2 / 2. ) * ay2 
    fwhpAz = 2 * sqrt( 2 * log(2)) * sigmaAz
  
    gauAzOffset = -ac
    gauElOffset = -ec
    egaAzOffset = gauAzOffset/cos(radians(el))
    ecuAzOffset = curAzOffset/cos(radians(el))
    acurAzOffset, acurElOffset, aecuAzOffset, agauAzOffset, agauElOffset, aegaAzOffset, asigmaAz, aampAz, afwhpAz, asigmaEl, aampEl, afwhpEl =\
     curAzOffset,  curElOffset,  ecuAzOffset,  gauAzOffset,  gauElOffset,  egaAzOffset,  sigmaAz,  ampAz,  fwhpAz,  sigmaEl,  ampEl,  fwhpEl
    if avg > 0.1: # float
      avgGaus.append([curAzOffset,curElOffset,ecuAzOffset,gauAzOffset,gauElOffset,egaAzOffset,sigmaAz,ampAz,fwhpAz,sigmaEl,ampEl,fwhpEl])
      avgLgth = min(int(avg),len(avgGaus))
      for i in avgGaus[-avgLgth:-1]:   # -1 skips last avg due to values are already in vars
        acurAzOffset     += i[0]
        acurElOffset     += i[1]
        aecuAzOffset     += i[2]
        agauAzOffset     += i[3]
        agauElOffset     += i[4]
        aegaAzOffset     += i[5]
        asigmaAz         += i[6]
        aampAz           += i[7]
        afwhpAz          += i[8]  
        asigmaEl         += i[9]
        aampEl           += i[10]
        afwhpEl          += i[11]
      acurAzOffset     /= avgLgth
      acurElOffset     /= avgLgth
      aecuAzOffset     /= avgLgth
      agauAzOffset     /= avgLgth
      agauElOffset     /= avgLgth
      aegaAzOffset     /= avgLgth
      asigmaAz         /= avgLgth
      aampAz           /= avgLgth
      afwhpAz          /= avgLgth
      asigmaEl         /= avgLgth
      aampEl           /= avgLgth
      afwhpEl          /= avgLgth

    print '\n'
    print '                            Reported offsets - Encoder                ||         Gaussian offset correction - Encoder         ||  Reported offsets plus Gaussian correction - encoder  ||                Base position -encoder                   \n'+\
          '                 Az*cos(el)      El   |        Az             El      || Az*cos(el)      El   |        Az             El      ||      Az         El   |        Az              El      ||     Az   deg    El      |       Az             El       \n'
#                             0.000     -0.000 |    0:00:00.000   -0:00:00.000 ||     1.871     -2.413 |    0:00:01.871   -0:00:02.413 ||     1.871     -2.413 |    0:00:01.871    -0:00:02.413 ||  150.410953   28.267449 |  150:24:39.431   28:16:02.818

    print 'Current data'.ljust(16)+' '+\
          decAlign(curAzOffset,3).rjust(9)+'  '+decAlign(curElOffset,3).rjust(9)+' | '+sec2dms(ecuAzOffset)+' '+sec2dms(curElOffset)+' || '+\
          decAlign(gauAzOffset,3).rjust(9)+'  '+decAlign(gauElOffset,3).rjust(9)+' | '+sec2dms(egaAzOffset)+' '+sec2dms(gauElOffset)+' || '+\
          decAlign(curAzOffset+gauAzOffset,3).rjust(9)+'  '+decAlign(curElOffset+gauElOffset,3).rjust(9)+' | '+sec2dms(ecuAzOffset+egaAzOffset)+'  '+sec2dms(curElOffset+gauElOffset)+' || '+\
          decAlign(az - ecuAzOffset/3600.0,6).rjust(11)+' '+decAlign(el - curElOffset/3600.0,6).rjust(11)+' | '+deg2dms(az - ecuAzOffset/3600.0)+' '+deg2dms(el - curElOffset/3600.0)+'\n'+\
          'Current avg'.ljust(16)+' '+\
          decAlign(acurAzOffset,3).rjust(9)+'  '+decAlign(acurElOffset,3).rjust(9)+' | '+sec2dms(acurAzOffset)+' '+sec2dms(acurElOffset)+' || '+\
          decAlign(agauAzOffset,3).rjust(9)+'  '+decAlign(agauElOffset,3).rjust(9)+' | '+sec2dms(agauAzOffset)+' '+sec2dms(agauElOffset)+' || '+\
          decAlign(acurAzOffset+agauAzOffset,3).rjust(9)+'  '+decAlign(acurElOffset+agauElOffset,3).rjust(9)+' | '+sec2dms(acurAzOffset+agauAzOffset)+'  '+sec2dms(acurElOffset+agauElOffset)+' || '+\
          decAlign(az - aecuAzOffset/3600.0,6).rjust(11)+' '+decAlign(el - acurElOffset/3600.0,6).rjust(11)+' | '+deg2dms(az - ecuAzOffset/3600.0)+' '+deg2dms(el - acurElOffset/3600.0)

    print '\n'
    print '                            Gaussian azimuth data        ||          Gaussian elevation data        || Total 5 pnt loops  Loops this move \n'+\
          '                      Sigma         Amp         FW@ 1/2P ||      Sigma         Amp         FW@ 1/2P || Gaussian Skipped   Gaussian Skipped\n'
    print 'Gaussian data'.ljust(16)+' '+\
          decAlign(sigmaAz,5).rjust(11)+'   '+decAlign(ampAz,5).rjust(11)+' | '+decAlign(fwhpAz,5).rjust(11)+' || '+\
          decAlign(sigmaEl,5).rjust(11)+'   '+decAlign(ampEl,5).rjust(11)+' | '+decAlign(fwhpEl,5).rjust(11)+' ||  '+\
          str(cycle).rjust(7)+' '+str(cycle - cntGaus[0]).rjust(7)+' '+str(cntGaus[1]).rjust(7)+' '+str(cntGaus[1] - cntGaus[2]).rjust(7)+'\n'+\
          'Gaussian avg'.ljust(16)+' '+\
          decAlign(asigmaAz,5).rjust(11)+'   '+decAlign(aampAz,5).rjust(11)+' | '+decAlign(afwhpAz,5).rjust(11)+' || '+\
          decAlign(asigmaEl,5).rjust(11)+'   '+decAlign(aampEl,5).rjust(11)+' | '+decAlign(afwhpEl,5).rjust(11)+'\n'
    
    if fg != '':
      fg.write('"Gaussian data",'+\
       str(epoch.value)+',"'+str(tm_year)+'-'+str(tm_mon)+'-'+str(tm_day).rjust(2,'0')+'","'+str(tm_hour).rjust(2,'0')+':'+str(tm_min).rjust(2,'0')+':'+str(tm_sec).rjust(2,'0')+'",'+\
 
       str(curAzOffset)+','+str(ecuAzOffset)+','+str(curElOffset)+','+sec2dms(curAzOffset)+','+sec2dms(ecuAzOffset)+','+sec2dms(curElOffset)+','+\
       str(az)+','+str(el)+','+deg2dms(az)+','+deg2dms(el)+','+\
       str(az - ecuAzOffset/3600.0)+','+str(el - curElOffset/3600.0)+','+deg2dms(az - ecuAzOffset/3600.0)+','+deg2dms(el - curElOffset/3600.0)+','+\

       str(gauAzOffset)+','+str(egaAzOffset)+','+sec2dms(gauElOffset)+','+sec2dms(gauAzOffset)+','+sec2dms(egaAzOffset)+','+sec2dms(gauElOffset)+','+\
       str(curAzOffset+gauAzOffset)+','+str(ecuAzOffset+egaAzOffset)+','+str(curElOffset+gauElOffset)+','+sec2dms(curAzOffset+gauAzOffset)+','+sec2dms(ecuAzOffset+egaAzOffset)+','+sec2dms(curElOffset+gauElOffset)+','+\
       str(az + egaAzOffset/3600.0)+','+str(el + gauElOffset/3600.0)+','+deg2dms(az + egaAzOffset/3600.0)+','+deg2dms(el + gauElOffset/3600.0)+','+\

       str(sigmaAz)+','+str(ampAz)+','+str(fwhpAz)+','+\
       str(sigmaEl)+','+str(ampEl)+','+str(fwhpEl)+','+\
 
       str(acurAzOffset)+','+str(aecuAzOffset)+','+str(acurElOffset)+','+sec2dms(acurAzOffset)+','+sec2dms(aecuAzOffset)+','+sec2dms(acurElOffset)+','+\
       str(az)+','+str(el)+','+deg2dms(az)+','+deg2dms(el)+','+\
       str(az - aecuAzOffset/3600.0)+','+str(el - acurElOffset/3600.0)+','+deg2dms(az - aecuAzOffset/3600.0)+','+deg2dms(el - acurElOffset/3600.0)+','+\

       str(agauAzOffset)+','+str(aegaAzOffset)+','+sec2dms(agauElOffset)+','+sec2dms(agauAzOffset)+','+sec2dms(aegaAzOffset)+','+sec2dms(agauElOffset)+','+\
       str(acurAzOffset+agauAzOffset)+','+str(aecuAzOffset+aegaAzOffset)+','+str(acurElOffset+agauElOffset)+','+sec2dms(acurAzOffset+agauAzOffset)+','+sec2dms(aecuAzOffset+aegaAzOffset)+','+sec2dms(acurElOffset+agauElOffset)+','+\
       str(az + aegaAzOffset/3600.0)+','+str(el + agauElOffset/3600.0)+','+deg2dms(az + aegaAzOffset/3600.0)+','+deg2dms(el + agauElOffset/3600.0)+','+\

       str(asigmaAz)+','+str(aampAz)+','+str(afwhpAz)+','+\
       str(asigmaEl)+','+str(aampEl)+','+str(afwhpEl)+'\r')
  except:
    print 'Error calculating or displaying Gaussian'
    ex = sys.exc_info()
    traceback.print_exception(ex[0], ex[1], ex[2])
  return agauAzOffset, agauElOffset, 1


def GaussianFileHdr(fg):
  fg.write(',"ALMA Time","UTC","UTC",'+\

           '"Reported Offsets enc",,,,,,'+\
           '"Reported position enc",,,,'+\
           '"Base position enc",,,,'+\

           '"Gaussian offset correction enc",,,,,,'+\
           '"Reported offsets plus Gaussian correction",,,,,,'+\
           '"Base position + Rep + Gaus offsets",,,,'+\

           '"Gaussian Azimuth data",,,'+\
           '"Gaussian Elevation data",,,'+\

           '"Avg Reported Offsets enc",,,,,,'+\
           '"Reported position enc",,,,'+\
           '"Avg Base position enc",,,,'+\

           '"Avg Gaussian offset correction enc",,,,,,'+\
           '"Avg Reported offsets plus Gaussian correction",,,,,,'+\
           '"Base position + Rep + Gaus offsets",,,,'+\

           '"Avg Gaussian Azimuth data",,,'+\
           '"Avg Gaussian Elevation data",,\r')


  fg.write('"Action","100ns","Date","Time",'+\

           '"Az","Az*cos(el)","El","Az","Az*cos(el)","El",'+\
           '"Az","El","Az","El",'+\
           '"Az","El","Az","El",'+\

           '"Az","Az*cos(el)","El","Az","Az*cos(el)","El",'+\
           '"Az","Az*cos(el)","El","Az","Az*cos(el)","El",'+\
           '"Az","El","Az","El",'+\

           '"Sigma","Amp","FW @ 1/2P",'+\
           '"Sigma","Amp","FW @ 1/2P",'+\

           '"Az","Az*cos(el)","El","Az","Az*cos(el)","El",'+\
           '"Az","El","Az","El",'+\
           '"Az","El","Az","El",'+\

           '"Az","Az*cos(el)","El","Az","Az*cos(el)","El",'+\
           '"Az","Az*cos(el)","El","Az","Az*cos(el)","El",'+\
           '"Az","El","Az","El",'+\

           '"Sigma","Amp","FW @ 1/2P",'+\
           '"Sigma","Amp","FW @ 1/2P"\r')




#******************************
# Utilities

def distance(x, y):
  return ((x ** 2) + (y ** 2)) ** 0.5


#******************************
# Utilities by Gene

def iif(c,t,f):
  " inline IF statement "
  if c:
    return t
  else:
    return f


def bytes2int(b):
  " Converts bytes returned by struct.unpack to value "
  if isinstance(b, types.IntType):
    return b
  v = 0
  l = len(b)
  for i in range(0,l):
    v += b[l-i-1]*256**i
  return v


def decAlign(n,j):
  " Right aligns decimal number n to number of digits j past decimal point"
  " Returns string"
  s = str(round(n,j))
  p = s.find('.')+iif(j<1,0,j+1)
  return (s+'0'*(p-len(s)))[:p]


def deg2dms(v):
  d=int(v)
  m=(v-d)*60
  s=(m-int(m))*60
  return (iif(v<0,'-','')+str(abs(d))).rjust(4)+':'+str(abs(int(m))).rjust(2,'0')+':'+decAlign(abs(s),3).rjust(6,'0')


def rad2dms(v):
  v = v*180/3.1415926535897931
  d=int(v)
  m=(v-d)*60
  s=(m-int(m))*60
  return (iif(v<0,'-','')+str(abs(d))).rjust(4)+':'+str(abs(int(m))).rjust(2,'0')+':'+decAlign(abs(s),3).rjust(6,'0')


def rad2sec(v):
  return v*180/3.1415926535897931*3600


def sec2dms(v):
  v = v/3600.0
  d=int(v)
  m=(v-d)*60
  s=(m-int(m))*60
  return (iif(v<0,'-','')+str(abs(d))).rjust(4)+':'+str(abs(int(m))).rjust(2,'0')+':'+decAlign(abs(s),3).rjust(6,'0')


def sec2rad(v):
  return v*3.1415926535897931/180.0/3600.0





#*****************************
# Run main program
if doPrgs == 1 or doPrgs == 3:
  az, el, curAzOffset, curElOffset = doSpiral()
if doPrgs > 1 and doPrgsi in ('Y','YES'):
  fivePoint(az, el, curAzOffset, curElOffset)



