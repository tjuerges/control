// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"

#include <ModeControllerExceptions.h>
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <PositionStreamConsumer.h> // For PositionConsumer
#include <TrackableObject.h> // TrackableObject 
#include <EquatorialObject.h> // EquatorialObject 
#include <TETimeUtil.h> // for TETimeUtil
#include <acstimeTimeUtil.h> // for TimeUtil
#include <LogToAudience.h> // for LOG_TO_*
#include <string> // for string
#include <sstream> // for ostringstream
#include <unistd.h> // for sleep
#include <algorithm> // for std::copy

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::string;
using std::ostringstream;
using Control::MountControllerImpl;
using ModeControllerExceptions::MountFaultExImpl;
using ModeControllerExceptions::TimeoutExImpl;
using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::DeviceBusyExImpl;

// This is how frequently to poll a property to see if it has reached its
// commanded value.  The time is in seconds. This should not be less than 0.048
// seconds. This parameter should probably be in the CDB.
const double pollingTime = 0.2;
const int pollingTimeInUs = static_cast<int>(pollingTime * 1E6);

void MountControllerImpl::enableMountDataPublication() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    mount_m->enableMountStatusDataPublication(true);
}

void MountControllerImpl::disableMountDataPublication() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ACS::Time t0, t1;
    if (!collectingData(t0, t1)) {
        mount_m->enableMountStatusDataPublication(false);
    }
}

Control::MountController::PointingData 
MountControllerImpl::getPointingData() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // 1. Get the latest measured position from the mount.
    const Control::MountStatusData_var currentPosition = 
        mount_m->getMountStatusData();

    // 2. Find the corresponding command data 
    const ACS::Time ts = currentPosition->timestamp;
    Commands command;
    ostringstream msg;
    {
        ACS::ThreadSyncGuard guard(&latestCommandsMutex_m);
        const int cmdSize = latestCommands_m.size();
        msg << "Looking for the commands at time " 
            << TETimeUtil::toTimeString(ts) 
            << ". Found "<< cmdSize << " commands. ";
        int cmdIdx = 0;
        ACS::Time lastTime = 0;
        if (cmdSize > 0) {
            lastTime = latestCommands_m.front().timestamp;
            msg << "Earliest command is at " 
                << TETimeUtil::toTimeString(lastTime);
            if (cmdSize > 1) {  
                lastTime = latestCommands_m[cmdSize-1].timestamp;
                msg << " and latest command is at " 
                    << TETimeUtil::toTimeString(lastTime);
            }
            msg << ".";
        }
        while (cmdIdx < cmdSize && latestCommands_m[cmdIdx].timestamp < ts) {
            cmdIdx++;
        }
        if (cmdIdx < cmdSize && latestCommands_m[cmdIdx].timestamp == ts) {
            // Found the commands for the right time. Copy it and release the
            // mutex
            command = latestCommands_m[cmdIdx];
            msg << " Found the matching command at index " << cmdIdx;
            acstime::Epoch tse; tse.value = ts;
            double delay = 
                DurationHelper(EpochHelper(lastTime).difference(tse)).toSeconds();
            msg << " and this is " << delay << " seconds in the past.";
        } else {
            // This timestamp will not match in mergePointingData
            command.timestamp = 0; 
        }
    }
    LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
  
    // Fill this structure in and return it
    Control::MountController::PointingData_var posnData = 
        new Control::MountController::PointingData;
    mergePointingData(currentPosition.in(), command, posnData.out());
  
    return posnData._retn();
}

double MountControllerImpl::
startPointingDataCollection(ACS::Time startTime, ACS::Time stopTime) {
    {
        ACS::Time t0, t1;
        if (collectingData(t0, t1)) {
            DeviceBusyExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ostringstream msg;
            msg << "Already collecting pointing data that started at "
                << TETimeUtil::toTimeString(t0) 
                << " and will conclude at " << TETimeUtil::toTimeString(t1)
                << ". You cannot start collecting more data until this has"
                << " concluded. Use the abortDataCollection function if you"
                << " wish to stop the current pointing data collection."; 
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getDeviceBusyEx();
        }
    }

    acstime::Epoch timeNow = TimeUtil::ace2epoch(ACE_OS::gettimeofday());
    {
        // The sourceList mutex prevents the trajectory planner thread
        // executing while we are running this function (hence the
        // lastScheduledTime_m does not change in the middle of this function).
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);

        ACS::Time oldestCommandTime;
        {
            ACS::ThreadSyncGuard guard2(&commandsMutex_m);
            if (latestCommands_m.size() > 0) {
                oldestCommandTime = latestCommands_m.front().timestamp;
            } else {
                oldestCommandTime = lastScheduledTime_m.value().value;
                // if we have not started the mount controller then lastScheduledTime_m
                // will be zero! We cannot leave it at zero as we will then try to
                // reserve far too much memory (below) for the Commands vector.
                if (oldestCommandTime < timeNow.value) {
                    oldestCommandTime = TETimeUtil::ceilTE(timeNow).value;
                }
            }
        }

        if (stopTime <= oldestCommandTime) {
            IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ostringstream msg;
            msg << "Cannot collect pointing data as the stop time of " 
                << TETimeUtil::toTimeString(stopTime) 
                << " is in the past or too close to the current time.";
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }

        // Ensure startTime & stoptime are on a TE boundries
        startTime = TETimeUtil::ceilTE(startTime);
        stopTime = TETimeUtil::ceilTE(stopTime);

        // Ensure that stoptime is less than 25hrs in the future. Its probably
        // a programming error elsewhere if its more than 25hrs in the future.
        if (DurationHelper(EpochHelper(stopTime).difference(timeNow)).toSeconds() 
            > 25*60*60+10.0) { // Add a 10 second grace period!
            IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ostringstream msg;
            msg << "Cannot collect pointing data as the scheduled stop time of " 
                << TETimeUtil::toTimeString(stopTime)
                << " is more than 25 hours in the future."
                << " Time now is " << TETimeUtil::toTimeString(timeNow);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }

        if (startTime >= stopTime) {
            IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ostringstream msg;
            msg << "Cannot collect pointing data as the start time of " 
                << TETimeUtil::toTimeString(startTime) 
                << " is not less than the stop time of " 
                << TETimeUtil::toTimeString(stopTime);
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }
        if (startTime < oldestCommandTime) {
            string msg = "Cannot collect all the requested pointing data.";
            msg += " The start time has been adjusted from "
                + TETimeUtil::toTimeString(startTime) + " to "
                + TETimeUtil::toTimeString(oldestCommandTime);
            LOG_TO_OPERATOR(LM_WARNING, msg);
            startTime = oldestCommandTime;
        } else {
            string msg = "Can collect all the requested pointing data.";
            msg += " The start time of " + TETimeUtil::toTimeString(startTime) 
                + " is OK as we have data from "
                + TETimeUtil::toTimeString(oldestCommandTime);
            LOG_TO_DEVELOPER(LM_DEBUG, msg);
        }
        int totalCommands = 
            (stopTime - startTime)/TETimeUtil::TE_PERIOD_DURATION.value + 1;
        ACS::ThreadSyncGuard guard2(&commandsMutex_m); 
        {
            ACS::ThreadSyncGuard guard3(&latestCommandsMutex_m);
            int curCommands = latestCommands_m.size();
            totalCommands += curCommands;
            // This clears the old commands and allocates enough memory for a
            // new one.  WARNING. If this is changed to a vector<Command*> then
            // you also have to delete each element of the vector. One
            // alternative is to use a vector<boost::shared_ptr<Command> >
            commands_m.reserve(totalCommands);
            commands_m.resize(curCommands);
            std::copy(latestCommands_m.begin(), latestCommands_m.end(), commands_m.begin());
        }
        commandStartTime_m = startTime;
        commandStopTime_m = stopTime;
        collectingData_m = true;
    } // release the Mutex so the thread can execute

    // turn on data publication and start the PositionStreamConsumer
    mount_m->enableMountStatusDataPublication(true);
    positionConsumer_m->setTimeRange(commandStartTime_m, commandStopTime_m); 

    { // log messages for the operator.
        ostringstream msg;
        msg << "Collecting pointing data from " 
            << TETimeUtil::toTimeString(commandStartTime_m) 
            << " to " << TETimeUtil::toTimeString(commandStopTime_m) 
            << " on antenna " << getAntennaName();
        LOG_TO_OPERATOR(LM_DEBUG, msg.str());
    }
    if (isStopped()) {
        string msg = 
            "The mount controller is not sending commands to the mount.";
        msg += " Commanded pointing data will not be collected.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
    }

    EpochHelper endTime(stopTime);
    DurationHelper timeToGo(endTime.difference(timeNow));
    // Add an extra offset of 5 seconds in the timeToGo as it takes a while to
    // get the data from the notification channel.
    return static_cast<double>(timeToGo.toSeconds() + 5.0);
}

Control::MountController::PointingDataTable* 
MountControllerImpl::getSelectedPointingDataTable(ACS::Time startTime, ACS::Time stopTime, double timeout) {
    // Wait until all the measured data is available. The commanded data must
    // be available before the measured data (unless causality is violated).
    double timeToWait = timeout;
    bool timedOut = false;
    while (!positionConsumer_m->pastStopTime(stopTime) && timeToWait >= 0.0) {
        usleep(pollingTimeInUs);
        timeToWait -= pollingTime;
        if ((timeToWait < 0) && !positionConsumer_m->pastStopTime()) {
            timedOut = true;
        }
    }

    // We timeout if we could not get all the measured data within the
    // specified time.
    if (timedOut) {// throw an exception
        ACS::Time overallStartTime, overallStopTime;
        collectingData(overallStartTime, overallStopTime);
        const ACS::Time timeNow = ::getTimeStamp();
        TimeoutExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ostringstream msg;
        msg << "Timed out while trying to get pointing data."
            << " Data collection was between "
            << TETimeUtil::toTimeString(overallStartTime) 
            << " & " 
            << TETimeUtil::toTimeString(overallStopTime)
            << " and pointing data was needed between  "
            << TETimeUtil::toTimeString(startTime) 
            << " & " 
            << TETimeUtil::toTimeString(stopTime)
            << ". Time now is " <<  TETimeUtil::toTimeString(timeNow)
            << " and the timeout was set at " << timeout << " seconds.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getTimeoutEx();
    }

    // Get the measured data.
    vector<Control::MountStatusData> measData;
    try {
        measData = positionConsumer_m->getDataRange(startTime, stopTime);
    } catch (IllegalParameterErrorExImpl& ex) {
        // Swallow this exception. It should never be thrown.
        ex.log(LM_WARNING); 
    }
    const int measSize = measData.size();

    // align & copy the commanded and measured positions and return it to the
    // user.
    Control::MountController::PointingDataTable_var pData = 
        new Control::MountController::PointingDataTable;
    pData->length(measSize);

    int pDataIdx = 0;
    Commands thisCommand;
    const int cmdSize = commands_m.size();
    for (int measIdx = 0, cmdIdx = 0; measIdx < measSize; measIdx++) {
        const Control::MountStatusData& thisMeasData = measData[measIdx];

        // If the measured data is invalid skip the whole row
        if (thisMeasData.azPositionsValid && thisMeasData.elPositionsValid) {
            // 1. Find the corresponding command data
            const ACS::Time ts = thisMeasData.timestamp;
            {
                ACS::ThreadSyncGuard guard(&commandsMutex_m);
                while (cmdIdx < cmdSize && commands_m[cmdIdx].timestamp < ts) {
                    cmdIdx++;
                }
                if (cmdIdx < cmdSize && commands_m[cmdIdx].timestamp == ts) {
                    // Got the right data. Now copy it and release the mutex
                    thisCommand = commands_m[cmdIdx];
                } else {
                    // This timestamp will not match in mergePointingData
                    thisCommand.timestamp = 0;
                }
            }
            // 2. Merge the data
            mergePointingData(thisMeasData, thisCommand, pData[pDataIdx]);
            pDataIdx++;
        }
    }

    // Trim the pointing data (in case we had invalid measured data).
    pData->length(pDataIdx);

    if (pDataIdx > 0) {
        ostringstream msg;
        msg << "Returning " << pDataIdx 
            << " rows of pointing data from antenna " << getAntennaName() 
            << ". Pointing data starts at " 
            << TETimeUtil::toTimeString(pData[0].timestamp)
            << " and stops at " 
            << TETimeUtil::toTimeString(pData[pDataIdx-1].timestamp);
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    } else {
        const string msg = "Not returning any pointing data from antenna " 
            + getAntennaName();
        LOG_TO_OPERATOR(LM_WARNING, msg);
    }
    return pData._retn();
}

Control::MountController::PointingDataTable* 
MountControllerImpl::getPointingDataTable(double timeout) {
    ACS::Time startTime, stopTime;
    collectingData(startTime, stopTime);
    Control::MountController::PointingDataTable* data;
    try {
        data = getSelectedPointingDataTable(startTime, stopTime, timeout);
        disableMountDataPublication();

        // Check we have completed collection of the commanded data. We should not
        // be collecting data here and, if we are, its probably because the
        // trajectory planner thread was suspended during of the data
        // collection.
        if (collectingData(startTime, stopTime)) {
            string msg;
            msg += "The collection of commanded pointing data did not complete.";
            msg += " Limited or no commanded pointing data will be available.";
            LOG_TO_OPERATOR(LM_WARNING, msg);
        }
    } catch (...) {
        disableMountDataPublication();
        throw;
    }
    return data;
}

Control::MountController::PointingDataTable* 
MountControllerImpl::collectPointingData(double duration) {
    if (duration < 0.048 || duration >= 25*60*60) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ostringstream msg;
        msg << "Cannot collect pointing data as the duration cannot be less"
            << " than 0.048 seconds or bigger than 25*60*60 seconds."
            << " Specified value was " << duration << " seconds.";
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    ACS::Time startTime;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        startTime = lastScheduledTime_m.value().value;
    }
    // if we have not started the mount controller then lastScheduledTime_m
    // will be zero!
    const acstime::Epoch timeNow = TimeUtil::ace2epoch(ACE_OS::gettimeofday());
    if (startTime < timeNow.value) { // mount controller is stopped
        startTime = TETimeUtil::ceilTE(timeNow).value;
    }
    EpochHelper stopTime(startTime);
    stopTime.add(DurationHelper(static_cast<long double>(duration)).value());

    const double timeout = 
        startPointingDataCollection(startTime, stopTime.value().value);
    return getPointingDataTable(timeout);
}

bool MountControllerImpl::collectingData(ACS::Time& startTime, 
                                         ACS::Time& stopTime) {
    ACS::ThreadSyncGuard guard(&commandsMutex_m); 
    startTime = commandStartTime_m;
    stopTime = commandStopTime_m;
    return collectingData_m;
}

void MountControllerImpl::abortDataCollection() {
    ACS::Time startTime, stopTime;
    { // Do not hold onto the mutex for too long as the thread needs it!
        ACS::ThreadSyncGuard guard(&commandsMutex_m);
        startTime = commandStartTime_m;
        stopTime = commandStopTime_m;
    }
    ostringstream msg;
    msg << "Aborting the data collection that started at "
        << TETimeUtil::toTimeString(startTime) 
        << " and was due to stop at " << TETimeUtil::toTimeString(stopTime)
        << ". Time now is " <<  TETimeUtil::toTimeString(::getTimeStamp());
    LOG_TO_OPERATOR(LM_WARNING, msg.str());
    ACS::ThreadSyncGuard guard2(&sourceListMutex_m);
    ACS::ThreadSyncGuard guard(&commandsMutex_m);
    unprotectedAbortDataCollection();
}

// This function is called when cleaning up. As I want to ensure the cleanup
// does not stop waiting for a mutex this version of the abortDataCollection
// function does not acquire the relevant mutexes.
void MountControllerImpl::unprotectedAbortDataCollection() {
    if (collectingData_m) {
        commandStopTime_m = lastScheduledTime_m.value().value;
        collectingData_m = false;
    }
}

void MountControllerImpl::
mergePointingData(const Control::MountStatusData& measuredData, 
                  const Commands& commandData,
                  Control::MountController::PointingData& pointingData) {
    ACS::Time ts; 
    double actualAz, actualEl;
    double azPointingCorrection = 0, elPointingCorrection = 0;

    // 1. Fill all the "measured" fields (except measuredTarget)
    // These come from the mount component.
    if (measuredData.azPositionsValid && measuredData.elPositionsValid) {
        // onSource
        pointingData.onSource = measuredData.onSource;
        // timestamp
        pointingData.timestamp = ts = measuredData.timestamp;
        // pointing. Add both pointing models together.
        if (measuredData.pointingModel) {
            azPointingCorrection += measuredData.azPointingModelCorrection;
            elPointingCorrection += measuredData.elPointingModelCorrection;
        }
        if (measuredData.auxPointingModel) {
            azPointingCorrection += measuredData.azAuxPointingModelCorrection;
            elPointingCorrection += measuredData.elAuxPointingModelCorrection;
        }
        pointingData.pointing.az = azPointingCorrection;
        pointingData.pointing.el = elPointingCorrection;
        // measured. This does *not* include the pointing model correction
        actualAz = measuredData.azPosition;
        actualEl = measuredData.elPosition;
        pointingData.measured.az = actualAz - azPointingCorrection;
        pointingData.measured.el = actualEl - elPointingCorrection;
        // measuredTarget
        // This can be filled with just the measured data but more accurately
        // if we are commanding the mount. I'll fill this field below once I
        // know which method to use.
    } else {
        MountFaultExImpl newEx(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        string errorMsg = "Cannot get valid positions from the mount.";
        newEx.addData("Detail", errorMsg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }

    // 2. Fill all the "commanded" fields (and measuredTarget)
    // These come from the trajectory planner thread in the mount controller.
    // Only fill the command data if the timestamps match.
    const bool stopped = (commandData.timestamp == ts) ? false : true;
    pointingData.stopped = stopped;

    if (!stopped) {
        pointingData.target = commandData.target;
        pointingData.equatorial = commandData.equatorial;
        pointingData.equatorialRADec = commandData.equatorialRADec;
        pointingData.horizon = commandData.horizon;
        pointingData.horizonAzEl = commandData.horizonAzEl;
        // commanded. This does *not* include the pointing model correction
        pointingData.commanded.az = 
            commandData.commanded.az-azPointingCorrection;
        pointingData.commanded.el = 
            commandData.commanded.el-elPointingCorrection;
        // measuredTarget
        // This is the more accurate version as it uses the same
        // TrackableObject with identical refraction and polar motion
        // corrections i.e., weather, observing frequency and IERS data
        commandData.source->getActualRADec(actualAz, actualEl, ts,
                                           pointingData.measuredTarget.ra, 
                                           pointingData.measuredTarget.dec);
    } else { // Fill dummy values.
        pointingData.target.ra = pointingData.target.dec = 0.0;
        pointingData.equatorial.lng = pointingData.equatorial.lat = 0.0;
        pointingData.equatorialRADec.ra=pointingData.equatorialRADec.dec = 0.0;
        pointingData.horizonAzEl.az = pointingData.horizonAzEl.el = 0.0;
        pointingData.horizon.lng = pointingData.horizon.lat = 0.0;
        pointingData.commanded.az = pointingData.commanded.el = 0.0;
        // measuredTarget
        // This is the less accurate version as it uses a fake TrackableObject
        // with different refraction and polar motion corrections i.e.,
        // weather, observing frequency and IERS data. If I use a HorizonObject
        // then I have to, separately, specify the antenna location and weather
        // station. But I cannot specify the observing frequency so for the
        // extra bit of accuracy I'll use an EquatorialObject.
        EquatorialObject dummySource(0.0, 0.0, 0.0, 0.0, 0.0,
                                     antennaLocation_m, getAntennaName(),
                                     padName_m, &*ws_m, obsFreq_m, 
                                     epoch_m, getLogger(), alarms_m, false);
        dummySource.getActualRADec(actualAz, actualEl, ts, 
                                   pointingData.measuredTarget.ra, 
                                   pointingData.measuredTarget.dec);

    }
    // subtract any equatorial offsets.
    pointingData.measuredTarget.ra -= pointingData.equatorialRADec.ra;
    pointingData.measuredTarget.dec -= pointingData.equatorialRADec.dec;
}
