#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tshen    2007-07-30 created.
#
"""
This utility sets the auxiliary pointing model from a file to an antenna
"""

__revision__ = "@(#) $Id$"
#$Source$

import optparse
import sys
import traceback
from   CCL.PointingModelBase import PointingModel, PrintModel



if __name__ == "__main__":
    parser = optparse.OptionParser(usage="%prog -a ANTENNA -f FILE [options]",
                                   version=__revision__)

    parser.add_option("-f", "--file", dest = "filename", default = None, 
                      type = "string", 
                      help = "aux poiting model file name.", 
                      metavar = "FILE")
    parser.add_option("-a", "--antenna", dest = "antenna", default = None, 
                      type = "string", 
                      help = "Antenna to use for setting the aux\
                              pointing model.", 
                      metavar = "ANTENNA")
    parser.add_option("--CAOffset", dest = "caoffset", default = "0.0",
                      type = "float",
                      help = "additional offset for CA term, optional",
                      metavar = "FLOAT")
    parser.add_option("--ELOffset", dest = "eloffset", default = "0.0",
                       type = "float",
                       help = "aditional offset for EL term (affects to IE\
                               term, IE'=IE-ELOffset), optional",
                       metavar = "FLOAT")
    (options, args) = parser.parse_args()
    if options.filename is None or options.antenna is None:
        parser.error("Incorrect number of arguments. Provide the pointing \
                     model file name and the antenna name .\
                     Try option --help or -h.")
    try:
        pt = PointingModel(options.antenna)
        pt.getModelFromFile(options.filename)
        print "Reading from file: " + options.filename
        print ""
        print PrintModel(pt.enabled, pt.band, pt.pmData)

        if options.caoffset != 0.0:
            print "Adding --CAOffset : %f" % options.caoffset
            pt.addCoeffValue('CA', float(options.caoffset))
       
        if options.eloffset != 0.0:
            print "Adding --ELOffset : %f" % options.eloffset
            #the EL term affects to IE term,  IE'=IE-ELOffset COMP-1334
            eloffset = float(-options.eloffset)
            pt.addCoeffValue('IE', float(eloffset))
            
        print "==================================="
        pt.loadAuxModel(pt.pmData)
        print "Setting Aux Poiting Model Status..."
        pt.setAuxPointingModelStatus(pt.enabled)
        print "isAuxPoitingModelEnabled: " + str(pt.getAuxPointingModelStatus())
        test = pt.getAuxLoadedModel()
        print "Reading from CONTROL/" + options.antenna + "/Mount:"
        print ""
        print PrintModel(pt.enabled, pt.band, test)
        print "==================================="
    except:
        ex = sys.exc_info()
        traceback.print_exception(ex[0], ex[1], ex[2])
