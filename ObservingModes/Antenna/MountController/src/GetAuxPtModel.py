#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tshen    2007-07-30 created.
#
"""
This utilty get the auxiliary pointing model from the specified antenna
"""
__revision__ = "@(#) $Id$"
#$Source$

import optparse
import sys
import traceback
from   CCL.PointingModelBase import PointingModel, PrintModel

if __name__ == "__main__":
    parser = optparse.OptionParser(usage="%prog -a ANTENNA", 
                                   version=__revision__)
    parser.add_option("-a", "--antenna", dest = "antenna",  
                    default = None,
                    type = "string", 
                    help = "Antenna to get the Aux. Pointing Model from.", 
                    metavar = "ANTENNA")
    (options, args) = parser.parse_args()
    if options.antenna is None:
       parser.error("An antenna name must be suplied.")
    try:
        pt = PointingModel(options.antenna)
        ptm = pt.getAuxLoadedModel()
        print PrintModel(pt.enabled, pt.band, ptm)
    except:
        ex = sys.exc_info()
        traceback.print_exception(ex[0], ex[1], ex[2])
