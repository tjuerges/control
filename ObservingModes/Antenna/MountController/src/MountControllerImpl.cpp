// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2007 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "MountControllerImpl.h"
#include "SourceAlarmTypes.h"

#include <ModeControllerExceptions.h>
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <TETimeUtil.h> // for TETimeUtil
#include <MountError.h>
#include <TrackableObject.h> // TrackableObject
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <controlAlarmHelper.h> // for Control::Alarmhelper
#include <acstimeTimeUtil.h> // for TimeUtil
#include <string> // for string
#include <sstream> // for ostringstream
#include <unistd.h> // for sleep
#include <cmath> // for M_PI & M_PI_2
#include <algorithm> // for max and min

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::string;
using std::ostringstream;
using std::endl;
using std::abs;
using Control::MountControllerImpl;
using ModeControllerExceptions::MountFaultEx;
using ModeControllerExceptions::MountFaultExImpl;
using ModeControllerExceptions::UnallocatedEx;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::TimeoutEx;
using ModeControllerExceptions::TimeoutExImpl;
using ControlExceptions::IllegalParameterErrorEx;
using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::INACTErrorEx;
using ControlExceptions::CAMBErrorEx;

// This is how frequently to poll a property to see if it has reached its
// commanded value.  The time is in seconds. This should not be less than 0.048
// seconds. This parameter should probably be in the CDB.
const double pollingTime = 0.1;
const int pollingTimeInUs = static_cast<int>(pollingTime * 1E6);

//-----------------------------------------------------------------------------

// There are many functions in this class and as this file was getting large
// the implementations have now been split over a number of files.  This file
// now only contains the "miscellaneous" functions. The other files that
// contain the implementation of functions in the MountControllerImpl class
// are:
// * MountControllerLifeCycle: Functions that manage the startup/shutdown of
//   this component. ie., constructor/destructor, initializer/cleanup &
//   acquireReferences/releaseReferences.
// * MountControllerThread: Functions related to the trajectory planner thread
// * MountControllerAzEl: Functions for moving in horizon coordinates
// * MountControllerRADec: Functions for moving in equatorial coordinates
// * MountControllerPlanet: Functions for tracking planets
// * MountControllerEphemeris: Functions for tracking other nearby objects
// * MountControllerPointingData: Functions related to collecting pointing data
// * MountControllerStrokes: Functions related to offsets and linear strokes
//-----------------------------------------------------------------------------
// CORBA interface
//-----------------------------------------------------------------------------


bool MountControllerImpl::isShutterOpen() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    try {
        return mount_m->isShutterOpen();
    } catch (CAMBErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx.getMountFaultEx();
    } catch (INACTErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx.getMountFaultEx();
    }
}

bool MountControllerImpl::isShutterClosed() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    try {
        return mount_m->isShutterClosed();
    } catch (CAMBErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx.getMountFaultEx();
    } catch (INACTErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx.getMountFaultEx();
    }
}

void MountControllerImpl::track() {
    track(false);
}

void MountControllerImpl::trackAsync() {
    track(true);
}

void MountControllerImpl::track(bool async) {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    try {
        // Do nothing if there is nothing to do!
        if (!isStopped() && mount_m->isMoveable()) return;

        // Release the brakes.
        if (async) {
            mount_m->autonomousAsync();
        } else {
            mount_m->autonomous();
        }
    } catch (INACTErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Mount component is not operational.";
        msg += " Possible causes include; ";
        msg += " the control subsystem is not operational,";
        msg += " the specified antenna is not operational.";
        msg += " Correct these faults and try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (CAMBErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Problem communicating with the ACU.";
        msg += " Possible causes include; ";
        msg += " a problem in the AMB wiring,";
        msg += " a problem in the ACU,";
        msg += " a problem in the software running on the ABM.";
        msg += " This function may not have completed correctly.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::LocalModeEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg =
            "ACU is in local mode and will not respond to commands.";
        msg += " You need to manually switch to remote mode and try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::TimeOutEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Timed out trying to switch to to autonomus mode.";
        msg += " Possible causes include; ";
        msg += " an interlock is set,";
        msg += " a hardware failure,";
        msg += " a timeout that is too short.";
        msg += " You may want to try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }
    {
        // Clear any queued sources (to prevent the antenna unexpectedly
        // moving) Under normal circumstances there should be no active sources
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        sourceList_m.clear();

        // Set the initial position of the mount in the AzCableWrap class
        double az, el;
        ACS::Time ts;
        actualAzEl(az, el, ts);
        cableWrap_m.setCurrentPosition(az, el);

        // Flushing any pending commands in the mount is now done when a new
        // source is specified.
    }

    // Resume the trajectory planner thread (that sends commands to the
    // Mount). It should just idle until a source has been set.
    tpThread_m->resume();
}

void MountControllerImpl::stop() {
    if (!tpThread_m->isSuspended()) {
        const string msg = "Stopping antenna " + getAntennaName();
        LOG_TO_OPERATOR(LM_INFO, msg);
    }

    tpThread_m->suspend();
    // There is no need to acquire the mutex when accessing the sourceList as
    // the thread should be suspended.
    sourceList_m.clear();

    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    try {
        // flush all pending commands in the mount
        mount_m->flushTrajectory(0, false);
        // Now put the mount into standby mode (perhaps we should not do this).
        mount_m->standby();
    } catch (CAMBErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Problem communicating with the ACU.";
        msg += " Possible causes include; ";
        msg += " a problem in the AMB wiring,";
        msg += " a problem in the ACU,";
        msg += " a problem in the software running on the ABM.";
        msg += " This function may not have completed correctly.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::LocalModeEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg =
            "ACU is in local mode and will not respond to commands.";
        msg += " You need to manually switch to remote mode and try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::TimeOutEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Timed out trying to switch to to standby mode.";
        msg += " Possible causes include; ";
        msg += " an interlock is set,";
        msg += " a hardware failure,";
        msg += " a timeout that is too short.";
        msg += " You may want to try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }
}

bool MountControllerImpl::isStopped() {
    return tpThread_m->isSuspended();
}

void MountControllerImpl::
switchSource(boost::shared_ptr<TrackableObject> src, bool avoidZenith) {
    {
        // get exclusive access to the sourceList_m & lastScheduledTime_m
        // data members (as they are also used by the trajectory planner
        // thread). The lock is released when the guard is destroyed.
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);

        // Remove all sources from the queue.
        // TODO. Allow the queue to have more than one source in it.
        sourceList_m.clear();

        // Put the new source at the back of the queue.
        sourceList_m.push_back(src);

        // Also do all the following when we know the trajectory planner is
        // *not* running (because we have the sourcelist mutex).

        // Allow the mount to go to the zenith (however the shutter will be
        // closed)
        mount_m->avoidCLOUDSATRegion(avoidZenith);

        // With a new source we will probably be adjusting the subreflector
        // position. Before this happens update the ambient temperature used by
        // the focus model.
        mount_m->setAmbientTemperature(tempInC_m);

        // Flush the queue in the Mount component (this will flush all commands
        // associated with the previous source).
        flushCommands();
    }
    // Immediately get the trajectory planner to start using this new
    // source
    trajectoryPlanner(tpThread_m->getSleepTime());
}

void MountControllerImpl::flushCommands() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    // Flush the queue in the Mount component (this will flush all commands
    // associated with the previous source). Commands from startOfSource_m
    // and onwards will be flushed.  As commands are sent two TE's before
    // they are applicable we cannot flush any sooner that two TE's from
    // now. But in practice two TE's is not enough so I've arbitrarily
    // changed this to five.
    startOfSource_m = TETimeUtil::ceilTE(getTimeStamp()) +
        5 * TETimeUtil::TE_PERIOD_ACS;
    mount_m->flushTrajectory(startOfSource_m, true);

    // Flush the commands that will no longer be sent. If we do not do this
    // then the data in this queue will not be in time order and the code
    // that uses this queue assumes it is.
    {
        ACS::ThreadSyncGuard guard(&latestCommandsMutex_m);
        while (latestCommands_m.size() > 0 &&
               latestCommands_m.back().timestamp >= startOfSource_m) {
            latestCommands_m.pop_back();
        }
    }
    // The following ensures the trajectory planner thread starts
    // scheduling from the start of this source
    lastScheduledTime_m.value(startOfSource_m);
}

void MountControllerImpl::waitUntilOnSource() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Check there is a source and that we are in track mode
    {
        bool noSource = true;
        {
            ACS::ThreadSyncGuard guard(&sourceListMutex_m);
            noSource = sourceList_m.empty();
        }

        if (noSource || isStopped()) {
            // Throw a timeout exception. Give the exact reason in the
            // details
            TimeoutExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            ostringstream msg;
            msg << "Timed out waiting to go on-source.";
            if (noSource) {
                msg << " No source has been specified.";
            } else {
                msg << " Mount controller is not commanding the mount."
                    << " Ensure the mount controller is not stopped.";
            }
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getTimeoutEx();
        }
    }

    try {
        Control::MountStatusData_var currentPosition;
        // Wait for the mount to arrive at the required position.
        bool onSource = false;
        double timeout = timeout_m;
        if (timeout <= 0) timeout = 1E38; // A long time
        int numRetries = static_cast<int>(timeout/pollingTime);
        do {
            usleep(pollingTimeInUs);
            currentPosition = mount_m->getMountStatusData();
            onSource = currentPosition->onSource;
            // If its for the previous source then we cannot be "onsource".
            // TODO. Check this comparison
            if (currentPosition->timestamp <= startOfSource_m) {
                onSource = false;
            } else {
                onSource = currentPosition->onSource;
            }
            if (numRetries%20 == 0) {
                const double actualAz = currentPosition->azPosition;
                const double actualEl = currentPosition->elPosition;
                const double commandedAz = currentPosition->azCommanded;
                const double commandedEl = currentPosition->elCommanded;
                ostringstream msg;
                msg << "Waiting for the mount to finish moving. "
                    << " (Az, El) position: measured ";
                if (currentPosition->azPositionsValid &&
                    currentPosition->elPositionsValid) {
                    msg << azElToString(actualAz, actualEl);
                } else {
                    msg << "-invalid-";
                }
                msg << ", commanded ";
                if (currentPosition->azCommandedValid &&
                    currentPosition->elCommandedValid) {
                    msg << azElToString(commandedAz, commandedEl);
                } else {
                    msg << "-invalid-";
                }
                msg << ". Tries left = " << numRetries;
                if (onSource == false) {
                    msg << ". off source";
                } else {
                    msg << ". on source";
                }
                LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
            }
            numRetries--;
        } while (onSource == false && numRetries > 0);

        const double actualAz = currentPosition->azPosition;
        const double actualEl = currentPosition->elPosition;
        if (onSource == false) {
            // Throw an exception if it did not get to the specified
            // position.
            TimeoutExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
            // Add details of the commanded position, tolerance and timeout
            // and the and current position
            const double commandedAz = currentPosition->azCommanded;
            const double commandedEl = currentPosition->elCommanded;
            double subrefActX, subrefActY, subrefActZ;
            mount_m->getSubreflectorPosition(subrefActX, subrefActY, subrefActZ);
            subrefActX *= 1000; subrefActY *= 1000; subrefActZ *= 1000;
            double subrefActTip, subrefActTilt;
            mount_m->getSubreflectorRotation(subrefActTip, subrefActTilt);
            subrefActTip *= 180/M_PI; subrefActTilt *= 180/M_PI;
            double subrefCmdX, subrefCmdY, subrefCmdZ;
            mount_m->getSubreflectorCommandedPosition(subrefCmdX, subrefCmdY, subrefCmdZ);
            subrefCmdX *= 1000; subrefCmdY *= 1000; subrefCmdZ *= 1000;
            double subrefCmdTip, subrefCmdTilt;
            mount_m->getSubreflectorCommandedRotation(subrefCmdTip, subrefCmdTilt);
            subrefCmdTip *= 180/M_PI; subrefCmdTilt *= 180/M_PI;
            ostringstream msg;
            msg << "Timeout moving the mount to the requested position."
                << endl
                << "Antenna position (Az, El): Commanded "
                << azElToString(commandedAz, commandedEl) << endl
                << " Measured " << azElToString(actualAz, actualEl) << endl
                << "Position tolerance "
                << mount_m->getTolerance()*180.0/M_PI*60*60
                << " arc-sec." << endl
                << " Subreflector position (x, y, z): Commanded ("
                << subrefCmdX << ", " << subrefCmdY << ", "
                << subrefCmdZ << ") Measured ("
                << subrefActX << ", " << subrefActY << ", "
                << subrefActZ << ") mm" << endl
                << " Subreflector tip/tilt Commanded ("
                << subrefCmdTip << ", " << subrefCmdTilt << ") Measured ("
                << subrefActTip << ", " << subrefActTilt << ") deg." << endl
                << "Allowed movement time " << timeout << " secs.";
            ex.addData("Detail", msg.str());
            ex.log();
            throw ex.getTimeoutEx();
        } else {
            ostringstream msg;
            msg << "Antenna " << getAntennaName()
                << " has finished moving. Current position (Az,El) is "
                << azElToString(actualAz, actualEl);
            LOG_TO_OPERATOR(LM_INFO, msg.str());
        }
    } catch (INACTErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg =  "Cannot determine if the antenna is on-source";
        msg += " as the mount component is not operational.";
        msg += " Possible causes include; ";
        msg += " the control subsystem is not operational,";
        msg += " the specified antenna is not operational.";
        msg += " Correct these faults and try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }
}

void MountControllerImpl::waitUntilOnSourceCB(AntModeControllerCB* cb) {
    try {
        waitUntilOnSource();
    } catch (UnallocatedEx& ex) {
        ModeControllerExceptions::UnallocatedCompletion
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (MountFaultEx& ex) {
        ModeControllerExceptions::MountFaultCompletion
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (TimeoutEx& ex) {
        ModeControllerExceptions::TimeoutCompletion
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    }
    cb->report(getAntennaName().c_str(),
               ACSErrTypeOK::ACSErrOKCompletion());
}

double MountControllerImpl::timeToSource() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    // Check there is a source and that we are in track mode
    {
        bool noSource = true;
        {
            ACS::ThreadSyncGuard guard(&sourceListMutex_m);
            noSource = sourceList_m.empty();
        }

        if (noSource || isStopped()) {
            // Throw a illegal parameter exception. Give the exact reason in
            // the details
            IllegalParameterErrorExImpl ex(__FILE__, __LINE__,
                                           __PRETTY_FUNCTION__);
            string msg="Cannot work out how long it will take to go on-source";
            if (noSource) {
                msg += " as no source has been specified.";
            } else {
                msg += " as the antenna is not being sent commands.";
            }
            ex.addData("Detail", msg);
            ex.log();
            throw ex.getIllegalParameterErrorEx();
        }
    }
    try {
        return mount_m->timeToSource(startOfSource_m);
    } catch (CAMBErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx.getMountFaultEx();
    } catch (INACTErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        throw newEx.getMountFaultEx();
    }
}

double MountControllerImpl::timeToSet() {
    double time = 0.0;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        if (!sourceList_m.empty()) {
            return sourceList_m.front()->timeToSet(getElevationLimit());
        }
    }
    return time;
}

void MountControllerImpl::survivalStow() {
    try {
        // I need to clear any sources so that the mount controller does not
        // command the mount while its trying to stow.
        stop();
        const string msg("Moving antenna " + getAntennaName()
                         + " to the survival stow position");
        LOG_TO_OPERATOR(LM_INFO, msg);
        // No needed to call checkMountIsOK as its done by the stop() function
        mount_m->survivalStow();
    } catch (CAMBErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Problem communicating with the ACU.";
        msg += " Possible causes include; ";
        msg += " a problem in the AMB wiring,";
        msg += " a problem in the ACU,";
        msg += " a problem in the software running on the ABM.";
        msg += " This function may not have completed correctly.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::LocalModeEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg =
            "ACU is in local mode and will not respond to commands.";
        msg += " You need to manually switch to remote mode and try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::TimeOutEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Timed out trying to switch to release the brakes.";
        msg += " Possible causes include; ";
        msg += " an interlock is set,";
        msg += " a hardware failure,";
        msg += " a timeout that is too short.";
        msg += " You may want to try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }
}

void MountControllerImpl::survivalStowCB(AntModeControllerCB* cb) {
    try {
        survivalStow();
    } catch (UnallocatedEx& ex) {
        ModeControllerExceptions::UnallocatedCompletion
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (MountFaultEx& ex) {
        ModeControllerExceptions::MountFaultCompletion
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    }
    cb->report(getAntennaName().c_str(),
               ACSErrTypeOK::ACSErrOKCompletion());
}

void MountControllerImpl::maintenanceStow() {
    try {
        // I need to clear any sources so that the mount controller does not
        // command the mount while its trying to stow.
        stop();
        const string msg("Moving antenna " + getAntennaName()
                         + " to the maintenance position");
        LOG_TO_OPERATOR(LM_INFO, msg);
        // No needed to call checkMountIsOK as its done by the stop() function
        mount_m->maintenanceStow();
    } catch (CAMBErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Problem communicating with the ACU.";
        msg += " Possible causes include; ";
        msg += " a problem in the AMB wiring,";
        msg += " a problem in the ACU,";
        msg += " a problem in the software running on the ABM.";
        msg += " This function may not have completed correctly.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::LocalModeEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg =
            "ACU is in local mode and will not respond to commands.";
        msg += " You need to manually switch to remote mode and try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    } catch (MountError::TimeOutEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg = "Timed out trying to switch to release the brakes.";
        msg += " Possible causes include; ";
        msg += " an interlock is set,";
        msg += " a hardware failure,";
        msg += " a timeout that is too short.";
        msg += " You may want to try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }
}

void MountControllerImpl::maintenanceStowCB(AntModeControllerCB* cb) {
    try {
        maintenanceStow();
    } catch (UnallocatedEx& ex) {
        ModeControllerExceptions::UnallocatedCompletion
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    } catch (MountFaultEx& ex) {
        ModeControllerExceptions::MountFaultCompletion
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    }
    cb->report(getAntennaName().c_str(),
               ACSErrTypeOK::ACSErrOKCompletion());
}

void MountControllerImpl::setTolerance(double tolerance) {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    mount_m->setTolerance(tolerance);
}

double MountControllerImpl::getTolerance() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    return mount_m->getTolerance();
}

void MountControllerImpl::setTimeout(double timeout) {
    timeout_m = timeout;
}

double MountControllerImpl::getTimeout() {
    return timeout_m;
}

char* MountControllerImpl::getMount() {
    if (!CORBA::is_nil(&*mount_m)) {
        return mount_m->name();
    } else {
        return CORBA::string_dup("");
    }
}

int MountControllerImpl::frequencyToBand(double frequency) {
    frequency /= 1E9;
    if (frequency > 31.3 && frequency < 45.0) {
        return 1;
        // Band 2 really goes up to 90GHz.
    } else if (frequency >= 67.0 && frequency < 84.0) {
        return 2;
    } else if (frequency >= 84.0 && frequency <= 116.0) {
        return 3;
    } else if (frequency >= 125.0 && frequency <= 163.0) {
        return 4;
    } else if (frequency > 163.0 && frequency < 211.0) {
        return 5;
    } else if (frequency >= 211.0 && frequency <= 275.0) {
        return 6;
    } else if (frequency > 275.0 && frequency <= 373.0) {
        return 7;
    } else if (frequency >= 385.0 && frequency <= 500.0) {
        return 8;
    } else if (frequency >= 602.0 && frequency <= 720.0) {
        return 9;
    } else if (frequency >= 787.0 && frequency <= 950.0) {
        return 10;
    } else {
        ostringstream msg;
        msg << "The observing frequency of " << frequency
            << "GHz is not possible with ALMA.";
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__,
                                        __PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
}

void MountControllerImpl::setObservingFrequency(double frequency) {
    if (frequency < 100E12) {
        // Do not call setBand if its optical.  Optical pointing is done using
        // their own pointing model and no focus model.
        const int band = frequencyToBand(frequency);
        checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        mount_m->setBand(band);
    }
    obsFreq_m = frequency;
}

double MountControllerImpl::getObservingFrequency() {
    return obsFreq_m;
}

void MountControllerImpl::getRefractionCoefficients(double& a, double& b) {
    a = 0; b = 0;
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m);
        if (!sourceList_m.empty()) {
            sourceList_m.front()->getRefractionCoefficients(a, b);
        }
    }
}

void MountControllerImpl::
checkMountIsOK(const char* fileName, int lineNumber,
               const char* functionName) {

    // Conceptually this should be in a separate function as it should be
    // tested for in more places. Add it here for now to see if this addresses
    // COMP-4021.
    if (getStatus() == Control::AntModeController::UNINITIALIZED) {
        string msg = "Mount controller has not been allocated to an antenna.";
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getUnallocatedEx();
    }

    if (mount_m.isNil()) {
        string msg = "Mount reference is null.";
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getUnallocatedEx();
    }
    // So we think we have a reference, but lets checks its really
    // there. Because we have a non-sticky reference it could be released
    // by the master.
    const string antName = getAntennaName();
    try {
        Control::HardwareDevice::HwState state = mount_m->getHwState();
        if (state != Control::HardwareDevice::Operational &&
            state != Control::HardwareDevice::Simulation) {
            string msg = "The mount component is not operational.";
            msg += " Check that antenna " + antName + " is operational";
            msg += " and does not have any errors related to the mount.";
            MountFaultExImpl ex(fileName, lineNumber, functionName);
            ex.addData("Detail", msg);
            ex.log();
            throw ex.getMountFaultEx();
        }
    } catch (CORBA::SystemException& ex) {
        string msg = "Cannot communicate with the mount component.";
        msg += " Perhaps the mount component is shutdown";
        msg += " or its container has crashed.";
        msg += " Check that antenna " + antName + " is operational";
        msg += " and does not have any errors related to the mount.";
        MountFaultExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getMountFaultEx();
    }
}

void MountControllerImpl::checkWeatherIsOK() {
    const string antennaName = getAntennaName();
    if (ws_m.isNil()) {
        alarms_m->activateAlarm(NO_COMMS_TO_WS);
        const string msg("There is no connection to the weather station");
        LOG_TO_OPERATOR(LM_ERROR, msg);
        return;
    }
    // Check the weather station component is really there (and has not
    // been shutdown or crashed
    try {
        Control::HardwareController::ControllerState wsState = ws_m->getState();
        if (wsState != Control::HardwareController::Operational) {
            alarms_m->activateAlarm(WS_DISABLED);
            string msg("The weather station is not operational");
            LOG_TO_OPERATOR(LM_ERROR, msg);
            msg = "The weather station state is ";
            switch (wsState) {
            case Control::HardwareController::Degraded:
                msg += "degraded"; break;
            case Control::HardwareController::Shutdown:
                msg += "shutdown"; break;
            case Control::HardwareController::Operational:
            default:
                msg += "incorrect";
            }
            LOG_TO_DEVELOPER(LM_ERROR, msg);
            return;
        }
    } catch (CORBA::SystemException& ex) {
        alarms_m->activateAlarm(NO_COMMS_TO_WS);
        string msg("Cannot communicate with the weather station.");
        msg += " Perhaps the weather station component is shutdown";
        msg += " or its container has crashed.";
        msg += " Check that the control subsystem operational.";
        msg += " Will use hard-coded weather parameters.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
        ws_m.release();
        return;
    }
    try {
        // 1. get the temperature
        ACS::Time startTime = ::getTimeStamp();
        const Control::CurrentWeather::Temperature temp =
            ws_m->getTemperatureAtPad(padName_m.c_str());
        if (!temp.valid) {
            alarms_m->activateAlarm(WS_DISABLED);
            ostringstream msg;
            msg << "Cannot get the air temperature from the weather station."
                << " Using a simulated value of " << temp.value << " deg. C.";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
            return;
        }

        // 2. get the pressure
        const Control::CurrentWeather::Pressure pressure =
            ws_m->getPressureAtPad(padName_m.c_str());
        if (!pressure.valid) {
            alarms_m->activateAlarm(WS_DISABLED);
            ostringstream msg;
            msg << "Cannot get the pressure from the weather station."
                << " Using a simulated value of "
                << pressure.value/100 << " hPa.";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
            return;
        }

        // 3. get the humidity
        const Control::CurrentWeather::Humidity humidity =
            ws_m->getHumidityAtPad(padName_m.c_str());
        if (!humidity.valid) {
            alarms_m->activateAlarm(WS_DISABLED);
            ostringstream msg;
            msg << "Cannot get the humidity from the weather station."
                << " Using a simulated value of "
                << humidity.value*100 << "%.";
            LOG_TO_OPERATOR(LM_ERROR, msg.str());
            return;
        }

        const ACS::Time timeNow = ::getTimeStamp();
        {
            string msg = "Took " + TETimeUtil::toSeconds(timeNow-startTime)
                + " seconds to get the weather data";
            double elapsed = static_cast<double>(timeNow-startTime)*100E-9;
            ACE_Log_Priority priority = LM_DEBUG;
            if (elapsed > .2) priority = LM_INFO;
            if (elapsed > 1) priority = LM_NOTICE;
            if (elapsed > 5) priority = LM_WARNING;
            if (elapsed > 20) priority = LM_ERROR;
            LOG_TO_DEVELOPER(priority, msg);
        }

        // Now if this data is the same as the cached values and more than
        // 30 minutes old warn the user. The tolerances are somewhat arbitrary.
        if ((abs(temp.value - tempInC_m) < 1E-1) &&
            (abs(pressure.value - pressureInP_m) < 10) &&
            (abs(humidity.value - humidity_m) < 1E-3)) {
            acstime::Epoch tn; tn.value = timeNow;
            if ((abs(DurationHelper(wsTime_m.difference(tn)).toSeconds())
                 > 30*60)) { // Its the same and too old

                alarms_m->activateAlarm(OLD_WS_DATA);
                ostringstream msg;
                msg << "Weather parameters have not changed in over 30 mins. "
                    << "Check the weather station is working";
                LOG_TO_OPERATOR(LM_ERROR, msg.str());
                return;
            } else {
                // the weather is the same but its not too old. Do not reset
                // the timestamp
            }
        } else { // weather values have changed. Reset the timestamp
            wsTime_m.value(timeNow);
        }
        // If we get here then things are OK so
        // 1. Update the cached values
        tempInC_m = temp.value;
        pressureInP_m = pressure.value;
        humidity_m = humidity.value;
        // 2. Clear the weather station alarms (as they may have previously
        // been set)
        alarms_m->deactivateAlarm(NO_COMMS_TO_WS);
        alarms_m->deactivateAlarm(WS_DISABLED);
        alarms_m->deactivateAlarm(OLD_WS_DATA);
    } catch (IllegalParameterErrorEx& ex) {
        string msg("Pad name supplied by the TMCDB is not correct.");
        msg += " Please ask computing support to correct this.";
        LOG_TO_OPERATOR(LM_ERROR, msg);
        IllegalParameterErrorExImpl newEx(ex);
        newEx.log();
        alarms_m->activateAlarm(WS_DISABLED);
        return;
    }
}

void MountControllerImpl::setElevationLimit(double elevationLimit) {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    try {
        mount_m->setMinimumElevation(elevationLimit);
    } catch (INACTErrorEx& ex) {
        throw MountFaultExImpl(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__)
            .getMountFaultEx();
    } catch (IllegalParameterErrorEx& ex) {
        throw MountFaultExImpl(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__)
            .getMountFaultEx();
    }
}

double MountControllerImpl::getElevationLimit() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    try {
        double azMin, azMax, elMin, elMax;
        mount_m->getMotionLimits(azMin, azMax, elMin, elMax);
        return elMin;
    } catch (INACTErrorEx& ex) {
        throw MountFaultExImpl(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__)
            .getMountFaultEx();
    } catch (IllegalParameterErrorEx& ex) {
        throw MountFaultExImpl(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__)
            .getMountFaultEx();
    }
}

void MountControllerImpl::resetLimits() {
    try {
        checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        mount_m->resetLimits();
    } catch (UnallocatedEx& ex) {
        // do nothing (the exception is already logged)
    }
}

//-------------------------------------------------------------------------
// MACI DLL support functions
//-------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(MountControllerImpl)
