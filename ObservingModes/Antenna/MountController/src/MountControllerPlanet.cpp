// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <ModeControllerExceptions.h>
#include <PlanetaryObject.h> // PlanetaryObject 
#include <acstimeTimeUtil.h> // for TimeUtil
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <casacore/measures/Measures/MDirection.h> // for casa::MDirection
#include <string> // for string
#include <sstream> // for ostringstream
#include <cmath> // for M_PI & M_PI_2

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::string;
using std::ostringstream;
using Control::MountControllerImpl;
using ControlExceptions::IllegalParameterErrorExImpl;
using Control::PlanetaryObject;

void MountControllerImpl::setPlanet(const char* planetName) {
    // Put the mount in track here even though trackAsync is called in
    // setAzElAsync to ensure that any exceptions that may be thrown when
    // putting the mount in autonomous mode get propogated to the caller.
    track();
    setPlanetAsync(planetName);
    waitUntilOnSource();
}

void MountControllerImpl::setPlanetAsync(const char* planetName) {
    const casa::MDirection::Types planet(PlanetaryObject::planetNameToEnum(planetName));
  
    if (!isObservablePlanet(planetName, 0)) {
        double az, el;
        toAzElPlanet(planetName, az, el);
        ostringstream msg;
        msg << "Cannot move antenna " << getAntennaName() << " to point at " 
            << planetName 
            << " as its currently below the elevation limit of the antenna."
            << " Corresponding (Az, El) is " << azElToString(az, el);
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    {
        string msg("Moving antenna ");
        msg += getAntennaName() + " to point at " + planetName;
        LOG_TO_OPERATOR(LM_INFO, msg);
    }
  
    trackAsync();

    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    // Create the new source (it must be on the heap as it will be "deleted")
    boost::shared_ptr<TrackableObject> 
        newSource(new PlanetaryObject(planet, antennaLocation_m, 
                                      getAntennaName(), padName_m,
                                      &*ws_m, obsFreq_m,
                                      getLogger(), alarms_m, true));

    // move the mount to the new source ASAP
    switchSource(newSource);
}

void MountControllerImpl::toAzElPlanet(const char* planetName, 
                                       double& az, double& el) {
    const casa::MDirection::Types planet(PlanetaryObject::planetNameToEnum(planetName));
    PlanetaryObject proposedTarget(planet, antennaLocation_m,
                                   getAntennaName(), padName_m,
                                   &*ws_m, obsFreq_m,
                                   getLogger(), alarms_m, false);
    const ACS::Time timeNow = 
        TimeUtil::ace2epoch(ACE_OS::gettimeofday()).value;
    proposedTarget.getAzEl(timeNow, az, el);
}

void MountControllerImpl::toRADecPlanet(const char* planetName, 
                                       double& ra, double& dec) {
    const casa::MDirection::Types planet(PlanetaryObject::planetNameToEnum(planetName));
    PlanetaryObject proposedTarget(planet, antennaLocation_m,
                                   getAntennaName(), padName_m,
                                   &*ws_m, obsFreq_m,
                                   getLogger(), alarms_m, false);
    proposedTarget.getCommandedRADec(0.0, 0.0, 0LL, ra, dec);
}

bool MountControllerImpl::isObservablePlanet(const char* planetName, 
                                             double duration) {
    double az, el;
    toAzElPlanet(planetName, az, el);
    if (el < getElevationLimit()) return false;
    if (timeToSetPlanet(planetName) < duration) return false;
    return true;
}

double MountControllerImpl::timeToRisePlanet(const char* planetName) {
    const casa::MDirection::Types planet(PlanetaryObject::planetNameToEnum(planetName));
    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    PlanetaryObject proposedTarget(planet,
                                   antennaLocation_m, getAntennaName(),
                                   padName_m, &*ws_m, obsFreq_m,
                                   getLogger(), alarms_m, false);
    return proposedTarget.timeToRise(getElevationLimit());
}

double MountControllerImpl::timeToSetPlanet(const char* planetName) {
    const casa::MDirection::Types planet(PlanetaryObject::planetNameToEnum(planetName));
    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    PlanetaryObject proposedTarget(planet,
                                   antennaLocation_m, getAntennaName(),
                                   padName_m, &*ws_m, obsFreq_m,
                                   getLogger(), alarms_m, false);
    return proposedTarget.timeToSet(getElevationLimit());
}
