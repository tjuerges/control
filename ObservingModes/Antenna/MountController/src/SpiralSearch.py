#!/usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# tjuerges  Mar 5, 2007  created
# tsh/rso   Jun 20, 2007 re-implemented as a client of SpiralBase class.
# ntroncos  Sep 11, 2007 fix COMP-1439
# ntroncos  Sep 11, 2007 fix coding style to comply with SE
# ntroncos  Sep 15, 2007 Added functionality to record maximum total power 
#                        readings.

"""
This client will move the specified antenna in a spiral search pattern.
This is useful for looking for celestial objects that are outside the
field of view.
This client implements a child class called AntennaSpiral from the
SpiralBase client. The method doDwell is re-implemented for printing the
offsets computed and sent by the spiral.
"""

__revision__ = "@(#) $Id$"
#$Source$

import CCL.SpiralBase
import ModeControllerExceptions
import ControlExceptions 
import math
import optparse
import sys
import time

class AntennaSpiral(CCL.SpiralBase.SpiralBase):
    """
    Child class of CCL.SpiralBase. This class overrides the doDwell 
    method from the parrent class.
    """
    def __init__(self, antenna, dwellTime):
        CCL.SpiralBase.SpiralBase.__init__(self, antenna)
        self.dwellTime = dwellTime
        self.azOffset=0
        self.elOffset=0
        self.maxPower=0

    def getPowerReading(self):
        """
        Get the Total power and return it.
        """
        return 0.0
      
    def doDwell(self):
        """
        Apply offsets and take reading of total power.
        """
        (azOffset, elOffset) = self.mc.getHorizonOffset() 
        time.sleep(self.dwellTime)
        #
	# Read total power. when a higher value is read, save the value and off
	# sets.
        tpower = self.getPowerReading()
        if tpower > self.maxPower:
            self.maxPower=tpower
            self.azOffset=azOffset
            self.elOffset=elOffset
            print "New max total power reading at offsets: power az el", \
                  self.maxPower, self.azOffset, self.elOffset
        return True

if __name__ == "__main__":
    parser = optparse.OptionParser(usage="%prog -a ANTENNA [options]",
                                   version=__revision__)
    parser.add_option("-a", "--antenna", dest = "antenna", default = None, 
                    type = "string", 
                    help = "Antenna to use for tracking.", 
                    metavar = "ANTENNA")
    parser.add_option("-c", "--clear", action="store_true", dest = "clear",
                     default = False, 
                     help = "Clear offsets when started? Default: %default.")
    parser.add_option("-w", "--wait", dest = "dwellTime", default = 1, 
                    type="float", 
                    help = "How long to dwell at each position. \
                            Default: %default seconds.", 
                    metavar = "DWELLTIME")
    parser.add_option("-m", "--max", dest="maxDist", default = 12.0, 
                    type="float", 
                    help="How far out to search [arcmin].\
                          Default: %default arcmin.")   
    parser.add_option("-s", "--step", dest="step", default = 55.0, 
                      type="float", 
                      help="How far to step for each new position [arcsec].\
                            Default %default arcsec.")
    (options, args) = parser.parse_args()
    if options.antenna is None :
        parser.error("An antenna must be suplied")
    maxDist = math.radians(options.maxDist / 60.0)
    step = math.radians(options.step / 3600.0)
    aSpiral = None
    try:
        aSpiral = AntennaSpiral(options.antenna, options.dwellTime)
        aSpiral.doSpiral(maxDist, step, options.clear)
        print "Max total power found ar offsets: power az el", \
               aSpiral.maxPower, aSpiral.azOffset, aSpiral.elOffset
    except (ModeControllerExceptions.CannotGetAntennaEx), e:
        print options.antenna, "is not a valid antenna name."
    except (ModeControllerExceptions.BadConfigurationEx), e:
        print "The ALMA software is not 'Operational'", \
              "or the connection to the specified antenna has failed."
    except (ControlExceptions.IllegalParameterErrorEx), e:
        print "longitude offset is bigger than +/-3*PI (540 degrees)", \
              "or the latitude offset is bigger than +/-PI/2", \
              "(90 degrees)."
    except (CCL.SpiralBase.WrongModeException), e:
        print "The Antenna is Stopped"


    if aSpiral is not None:
        del(aSpiral)

#__o0o__
