// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <EphemerisObject.h> // EphemerisObject 
#include <sstream> // for ostringstream
#include <cmath> // for M_PI & M_PI_2
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::ostringstream;
using Control::MountControllerImpl;
using ControlExceptions::IllegalParameterErrorExImpl;

void MountControllerImpl::
setEphemeris(const Control::Ephemeris& userEphemeris) {
    // Put the mount in track here even though trackAsync is called in
    // setEphemerisAsync to ensure that any exceptions that may be thrown when
    // putting the mount in autonomous mode get propogated to the caller.
    track();
    setEphemerisAsync(userEphemeris);
    waitUntilOnSource();
}

void MountControllerImpl::
setEphemerisAsync(const Control::Ephemeris& userEphemeris) {
    if (!isObservableEphemeris(userEphemeris, 0)) {
        double az, el;
        toAzElEphemeris(userEphemeris, az, el);
        ostringstream msg;
        msg << "Cannot move antenna " << getAntennaName() << " to point at"
            << " the user defined object as its currently below the elevation"
            << " limit of the antenna. Corresponding (Az, El) is "
            << azElToString(az, el);
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    trackAsync();

    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    // Create the new source (it must be on the heap as it will be "deleted")
    boost::shared_ptr<TrackableObject> 
        newSource(new EphemerisObject(userEphemeris, 
                                      antennaLocation_m, getAntennaName(),
                                      padName_m, &*ws_m, obsFreq_m, 
                                      getLogger(), alarms_m));
    {
        Control::EquatorialDirection target;
        Control::Offset equatorial;
        Control::EquatorialDirection equatorialRADec;
        Control::Offset horizon;
        Control::HorizonDirection horizonAzEl;
        Control::HorizonDirection commanded;
        Control::HorizonDirection commandedRate;
        newSource->getPointing(::getTimeStamp(), target, equatorial, equatorialRADec, 
                               horizon, horizonAzEl, commanded, commandedRate);

        ostringstream msg; 
        msg << "Moving antenna " << getAntennaName() 
            << " to point at a position specified with an ephemeris."
            << " The initial (RA, Dec) = " 
            << raDecToString(target.ra, target.dec);
        LOG_TO_OPERATOR(LM_INFO, msg.str());
    }
        
    // move the mount to the new source ASAP
    switchSource(newSource);
}

void MountControllerImpl::
toAzElEphemeris(const Control::Ephemeris& userEphemeris, 
                double& az, double& el) {
    EphemerisObject proposedTarget(userEphemeris, antennaLocation_m, 
                                   getAntennaName(), padName_m, &*ws_m, 
                                   obsFreq_m, getLogger(), alarms_m);
    proposedTarget.getAzEl(::getTimeStamp(), az, el);
}

void MountControllerImpl::
toRADecEphemeris(const Control::Ephemeris& userEphemeris, 
                double& ra, double& dec) {
    EphemerisObject proposedTarget(userEphemeris, antennaLocation_m, 
                                   getAntennaName(), padName_m, &*ws_m, 
                                   obsFreq_m, getLogger(), alarms_m);
    proposedTarget.getCommandedRADec(0.0, 0.0, ::getTimeStamp(), ra, dec);
}

bool MountControllerImpl::
isObservableEphemeris(const Control::Ephemeris& 
                      userEphemeris, double duration) {
    double az, el;
    toAzElEphemeris(userEphemeris, az, el);
    if (el < getElevationLimit()) return false;
    if (timeToSetEphemeris(userEphemeris) < duration) return false;
    return true;
}

double MountControllerImpl::
timeToRiseEphemeris(const Control::Ephemeris& userEphemeris) {
    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    EphemerisObject proposedTarget(userEphemeris,
                                   antennaLocation_m, getAntennaName(),
                                   padName_m, &*ws_m, obsFreq_m,
                                   getLogger(), alarms_m);
    return proposedTarget.timeToRise(getElevationLimit());
}

double MountControllerImpl::
    timeToSetEphemeris(const Control::Ephemeris& userEphemeris) {
    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    EphemerisObject proposedTarget(userEphemeris,
                                   antennaLocation_m, getAntennaName(),
                                   padName_m, &*ws_m, obsFreq_m,
                                   getLogger(), alarms_m);
    return proposedTarget.timeToSet(getElevationLimit());
}

