// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008 - 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"

#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <TrackableObject.h> // TrackableObject 
#include <HorizonObject.h> // HorizonObject 
#include <TETimeUtil.h> // for TETimeUtil
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <acstimeTimeUtil.h> // for TimeUtil
#include <slalib.h> // for sla* functions
#include <string> // for string
#include <sstream> // for ostringstream
#include <cmath> // for M_PI & M_PI_2
#include <iomanip> // for std::setprecision

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::string;
using std::ostringstream;
using std::endl;
using Control::MountControllerImpl;
using ControlExceptions::IllegalParameterErrorEx;
using ControlExceptions::IllegalParameterErrorExImpl;

// how many loopTimes in the future the trajectory planner thread schedules
// (currently 4.032 seconds as the loopTime is 1.008 seconds)
const int lookaheadFactor = 4;

void MountControllerImpl::
trajectoryPlanner(const ACS::TimeInterval& loopTime) {

    // This locks the mutex for for the duration of the execution of this
    // function. The alternative is to lock it each we need to access the
    // sourceList. However tests on my laptop indicate this function takes less
    // than 10 milliseconds to execute so locking it for the duration should
    // not be a problem.
    ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
  
    if (sourceList_m.empty()) {
        string msg = "No source given. Not commanding the antenna to move.";
        LOG_TO_DEVELOPER(LM_DEBUG, msg);
        return;
    } 

    // Do not send commands if the brakes are engaged.
    try {
        if (!mount_m->isMoveable()) {
            // I must clear the source to prevent unexpected movements when the
            // brakes are manually released (as described in COMP-3079)
            sourceList_m.clear();
            unprotectedAbortDataCollection();
            string msg = "Brakes are unexpectedly engaged on antenna ";
            msg += getAntennaName();
            msg += ". No more commands will be sent.";
            LOG_TO_OPERATOR(LM_CRITICAL, msg);
            return;
        }
        // Threads should not throw exceptions. So I'll just catch these
        // exceptions, shut down the thread and log the exception.
    } catch (ControlExceptions::INACTErrorEx& ex) {
        tpThread_m->suspend();
        sourceList_m.clear();
        unprotectedAbortDataCollection();
        ControlExceptions::INACTErrorExImpl 
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg("Mount component is not operational.");
        msg += " No more commands will be sent.";
        newEx.addData("Detail", msg);
        newEx.log();
        return;
    } catch (ControlExceptions::CAMBErrorEx& ex) {
        tpThread_m->suspend();
        sourceList_m.clear();
        unprotectedAbortDataCollection();
        ControlExceptions::CAMBErrorExImpl 
            newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg("Cannot communicate with the ACU.");
        msg += " No more commands will be sent.";
        newEx.addData("Detail", msg);
        newEx.log();
        return;
    }

    EpochHelper timeNow(::getTimeStamp());
    EpochHelper nextTE(TETimeUtil::ceilTE(timeNow.value()));
    bool newSource = false;
    if (timeNow > lastScheduledTime_m.value()) {
        lastScheduledTime_m.value(nextTE.value());
        newSource = true;
    }
    EpochHelper horizon(nextTE.value());
    {
        DurationHelper lookAhead(loopTime);
        lookAhead *= lookaheadFactor;
        horizon += lookAhead.value();
    }
    {
        ostringstream msg;
        const string startTime = TETimeUtil::toTimeString(lastScheduledTime_m);
        const string endTime =  TETimeUtil::toTimeString(horizon);
        const string timeNowString = TETimeUtil::toTimeString(timeNow);
        msg << "Time now: " << timeNowString
            << " Scheduling from " << startTime << " to " << endTime;
        LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
    }
  
    const int numTrajs =  
        (horizon.value().value - lastScheduledTime_m.value().value)/
        TETimeUtil::TE_PERIOD_DURATION.value;
    Control::TrajectorySeq_var commands = new Control::TrajectorySeq(numTrajs);
    commands->length(numTrajs);

    ACS::Time t = lastScheduledTime_m.value().value;
    Commands c;
    Control::HorizonDirection commandedRate;
    ostringstream sentCmds;
    sentCmds << "Sending " << commands->length() 
             << " trajectory commands to the mount component.";
  
    for (int i = 0; i < numTrajs; 
         i++, t+=TETimeUtil::TE_PERIOD_DURATION.value) {
        // Decide if we need to move to the next source.
        while ((sourceList_m.size() > 1) && 
               (sourceList_m[1]->getStartTime() <= t)) {
            sourceList_m.pop_front();
            // TODO. Tell the source to update its weather parameters at this
            // point as the source may have been in the queue for many minutes
            // and the weather may have changed.
            newSource = true;
        }
    
        sourceList_m.front()->getPointing(t, c.target, 
                                          c.equatorial, c.equatorialRADec,
                                          c.horizon, c.horizonAzEl, 
                                          c.commanded, commandedRate);
        // RA is always between 0 and 2pi (or 0 and 24 hrs)
        c.target.ra = slaDranrm(c.target.ra);

        // Do not unwrap when using a Horizon object as I assume if a user
        // spoecified a specific azimuth they really mean it.
        // TODO: Add code to prevent an unwrap when doing offsets!
        if (dynamic_cast<Control::HorizonObject*>(sourceList_m.front().get()) == 0) {
            c.commanded.az = cableWrap_m.unwrap(c.commanded.az);
        } else {
            // The CableWrap class needs to keep track of the last commanded
            // azimuth
            cableWrap_m.setCurrentPosition(c.commanded.az, c.commanded.el);
        }
        commands[i].az = c.commanded.az;
        commands[i].el = c.commanded.el;
        commands[i].azRate = commandedRate.az;
        commands[i].elRate = commandedRate.el;
        commands[i].time = t;

        { // Accumulate the messages here but log it at the end of the to
          // prevent the logging taking tens of milliseconds.
            const string timeString = 
                TETimeUtil::toTimeString(commands[i].time);
            sentCmds << endl 
                     << "At " << timeString << " the commanded (Az, El) is "
                     << azElToString(commands[i].az, commands[i].el)
                     << " & rates are (" << std::setprecision(4)
                     << commands[i].azRate*180/M_PI << ", " 
                     << commands[i].elRate*180/M_PI << ") deg/sec";
        }
        
        // Handle the accumulation of commanded positions
        c.timestamp = t;
        c.source = sourceList_m.front();
        {   // Always store the last few seconds of commands so they are
            // available if someone, like the mount GUI, calls the
            // getPointingData function. I estimate the latency through the
            // mount is less than three seconds.
            const unsigned int maxBufferSize = 3*21 +
                loopTime/TETimeUtil::TE_PERIOD_DURATION.value*lookaheadFactor;
            ACS::ThreadSyncGuard guard(&latestCommandsMutex_m);
            while (latestCommands_m.size() > maxBufferSize) {
                latestCommands_m.pop_front();
            }
            latestCommands_m.push_back(c);
        }
        {   // If collecting data then accumulate commands for the requested
            // interval
            ACS::ThreadSyncGuard guard(&commandsMutex_m);
            if (collectingData_m && t >= commandStopTime_m) {
                collectingData_m = false;
            }
            if (collectingData_m && t >= commandStartTime_m) {
                commands_m.push_back(c);
            }
        }
    }

    lastScheduledTime_m.value(horizon.value());

    try {
// Commented out the following log message as it was producing too many
// logs. This was at the request of Jeff Kern. This will inhibit our ability to
// deduce the antenna position if it is not behaving as expected. This log
// message can be reenabled if problems are seen.
// Ralph Marson 2009-12-16.
        //        LOG_TO_DEVELOPER(LM_DEBUG, sentCmds.str());
        mount_m->setTrajectory(commands);
    } catch (CORBA::SystemException& ex) {
        // This probably indicates the mount component has disappeared. 
        tpThread_m->suspend();
        sourceList_m.clear();
        unprotectedAbortDataCollection();
        // Do not call stop() here as we then have to catch another bunch of
        // exceptions.
    } catch (IllegalParameterErrorEx& ex) {
        // just catch this exception and swallow it (after shutting down the
        // thread and logging it).  I've never seen this bit of code executed
        // in the field.
        tpThread_m->suspend();
        sourceList_m.clear();
        unprotectedAbortDataCollection();
        IllegalParameterErrorExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.log();
    }

// Commented out the following log message as it was producing too many
// logs. This was at the request of Jeff Kern. This will inhibit our ability to
// diagnose timing problems in the MountConttoller component. This log message
// can be reenabled if problems are seen.
// Ralph Marson 2009-12-16.
//     {
//         EpochHelper timeEnd(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
//         DurationHelper execTime(timeEnd.difference(timeNow.value()));
//         ostringstream msg;
//         msg << "Thread took " << static_cast<int>(execTime.toSeconds()*1000+.5)
//             << " milli-seconds to complete."
//             << " Started at " << TETimeUtil::toTimeString(timeNow)
//             << " and completed at " << TETimeUtil::toTimeString(timeEnd);
//         LOG_TO_DEVELOPER(LM_DEBUG, msg.str());
//     }
}

MountControllerImpl::TrajectoryPlannerThread::
TrajectoryPlannerThread(const ACE_CString& name, MountControllerImpl& mc, 
                        const ACS::TimeInterval& responseTime,
                        const ACS::TimeInterval& sleepTime)
    :ACS::Thread(name, responseTime, sleepTime),
     mc_m(mc)
{}

void MountControllerImpl::TrajectoryPlannerThread::runLoop() {
    try {
        mc_m.trajectoryPlanner(getSleepTime());
    } catch (ACSErr::ACSbaseExImpl& ex) {
        ex.log();
        ostringstream msg;
        msg << "Caught an unexpected ACS exception in the " 
            << __PRETTY_FUNCTION__ << " function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    } catch (CORBA::Exception& ex) {
        ostringstream msg;
        msg << "Caught an unexpected CORBA exception named " << ex._name();
        msg << " in the " << __PRETTY_FUNCTION__ << " function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    } catch (...) {
        ostringstream msg;
        msg << "Caught an unexpected exception in the "
            << __PRETTY_FUNCTION__ << " function.";
        msg << " Ignoring it!";
        LOG_TO_DEVELOPER(LM_ERROR, msg.str());
    }
}
