#! /usr/bin/env python
# @(#) $Id$
# $Source$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This utility function will move the specified antenna to the near the
zenith and put it in shutdown mode. It is intended for people who are
less familiar with the ALMA computing system. The ALMA software must
be started and in the operational state for this function to work.
"""

__revision__ = "@(#) $Id$"

import CCL.MountController
import ModeControllerExceptions
import Acspy.Common.ErrorTrace
from optparse import OptionParser

if __name__ == "__main__":
    parser = OptionParser(usage="%prog -a ANTENNA", version=__revision__)
    parser.add_option("-a", "--antenna", dest="antenna",  metavar='antenna',
                      default=None,
                      help="The antenna to use e.g. DV01. No default")
    (options, args) = parser.parse_args()
    
    if (options.antenna is None):
        parser.error("An antenna name must be supplied.")

    try:
        m = CCL.MountController.MountController(options.antenna)
        m.maintenanceStow();
    except (ModeControllerExceptions.CannotGetAntennaEx), e:
        print options.antenna, "is not a valid antenna name."
        print "The following error trace may indicate the underlying problem."
        Acspy.Common.ErrorTrace.ErrorTraceHelper(e.errorTrace).log();
    except (ModeControllerExceptions.BadConfigurationEx), e:
        print "The ALMA software is not 'Operational'",\
              "or antenna",  options.antenna, "is not configured with a mount."
        print "The following error trace may indicate the underlying problem."
        Acspy.Common.ErrorTrace.ErrorTraceHelper(e.errorTrace).log();
    except (ModeControllerExceptions.MountFaultEx), e:
        print "Cannot move the antenna. Common reasons include:"
        print "1. The ACU is in local mode,"
        print "2. An interlock e.g., a door is open, is active,"
        print "3. Timed out waiting for the antenna to release the brakes,"
        print "4. There is a problem communicating with the ACU."
        print "The following error trace may indicate the underlying problem."
        Acspy.Common.ErrorTrace.ErrorTraceHelper(e.errorTrace).log();
