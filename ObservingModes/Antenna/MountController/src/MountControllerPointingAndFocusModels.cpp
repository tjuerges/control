// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "MountControllerImpl.h"
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <sstream> // for ostringstream
#include <ModeControllerExceptions.h>

using Control::MountControllerImpl;
using TMCDB::PointingModel;
using ControlExceptions::IllegalParameterErrorExImpl;
using ControlExceptions::IllegalParameterErrorEx;
using std::ostringstream;
using ModeControllerExceptions::MountFaultEx;
using ModeControllerExceptions::MountFaultExImpl;
using ModeControllerExceptions::UnallocatedEx;
using ModeControllerExceptions::UnallocatedExImpl;

PointingModel* MountControllerImpl::getPointingModel() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    return mount_m->getPointingModel();
}

CORBA::Boolean MountControllerImpl::isPointingModelEnabled() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    return mount_m->isPointingModelEnabled();
}

PointingModel* MountControllerImpl::getAuxPointingModel() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    return mount_m->getAuxPointingModel();
}

void MountControllerImpl::
setAuxPointingModel(const PointingModel& model) {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    mount_m->setAuxPointingModel(model);
    mount_m->enableAuxPointingModel(true);
}

void MountControllerImpl::zeroAuxPointingModel() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    mount_m->zeroAuxPointingModel();
    return mount_m->enableAuxPointingModel(false);
}

bool MountControllerImpl::isAuxPointingModelEnabled() {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    return mount_m->isAuxPointingModelEnabled();
}

void MountControllerImpl::reportPointingModel(){
    try {
        checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        mount_m->reportPointingModel();
    } catch (UnallocatedEx& ex) {
        string msg = "Cannot put the current pointing model into the ASDM.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        UnallocatedExImpl newEx(ex, __FILE__,__LINE__,__PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
    } catch (MountFaultEx& ex) {
        string msg = "Cannot put the current pointing model into the ASDM.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        MountFaultExImpl newEx(ex, __FILE__,__LINE__,__PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
    }
}

void MountControllerImpl::reportFocusModel(){
    try {
        checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        mount_m->reportFocusModel();
    } catch (UnallocatedEx& ex) {
        string msg = "Cannot put the current focus model into the ASDM.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        UnallocatedExImpl newEx(ex, __FILE__,__LINE__,__PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
    } catch (MountFaultEx& ex) {
        string msg = "Cannot put the current focus model into the ASDM.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        MountFaultExImpl newEx(ex, __FILE__,__LINE__,__PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
    }
}

void MountControllerImpl::reportFocusPosition(){
    try {
        checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        mount_m->reportFocusPosition();
    } catch (UnallocatedEx& ex) {
        string msg = "Cannot put the current subreflector position into the ASDM.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        UnallocatedExImpl newEx(ex, __FILE__,__LINE__,__PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
    } catch (MountFaultEx& ex) {
        string msg = "Cannot put the current subreflector position into the ASDM.";
        LOG_TO_OPERATOR(LM_WARNING, msg);
        MountFaultExImpl newEx(ex, __FILE__,__LINE__,__PRETTY_FUNCTION__);
        newEx.addData("Detail", msg);
        newEx.log();
    }
}

void MountControllerImpl::
applyPointingCalibrationResult(const PointingCalResult& result) {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    const string antName(result.antennaName);
    if (getAntennaName() != antName) {
        IllegalParameterErrorExImpl ex( __FILE__,__LINE__,__PRETTY_FUNCTION__);
        string msg = "The pointing calibration for antenna " + antName;
        msg += " cannot be applied to antenna " + getAntennaName();
        LOG_TO_OPERATOR(LM_ERROR, msg);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    double oldCa = mount_m->getAuxPointingModelCoefficient("CA");
    double oldIe = mount_m->getAuxPointingModelCoefficient("IE");

    mount_m->setAuxPointingModelCoefficient("CA", oldCa + result.corrAz);
    mount_m->setAuxPointingModelCoefficient("IE", oldIe - result.corrEl);
    mount_m->enableAuxPointingModel(true);
}

void MountControllerImpl::
applyFocusCalibrationResult(const FocusCalResult& result){
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    const string antName(result.antennaName);
    if (getAntennaName() != antName) {
        IllegalParameterErrorExImpl ex( __FILE__,__LINE__,__PRETTY_FUNCTION__);
        string msg = "The focus calibration for antenna " + antName;
        msg += " cannot be applied to antenna " + getAntennaName();
        LOG_TO_OPERATOR(LM_ERROR, msg);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    mount_m->enableFocusModel(false);
    if (!result.xFixed) {
        double xr = mount_m->getFocusModelCoefficient("XR");
        xr += result.X;
        mount_m->setFocusModelCoefficient("XR", xr);
    }
    if (!result.yFixed) {
        double yr = mount_m->getFocusModelCoefficient("YR");
        yr += result.Y;
        mount_m->setFocusModelCoefficient("YR", yr);
    }
    if (!result.zFixed) {
        double zr = mount_m->getFocusModelCoefficient("ZR");
        zr += result.Z;
        mount_m->setFocusModelCoefficient("ZR", zr);
    }
    mount_m->enableFocusModel(true);
}
