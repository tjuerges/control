# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2007 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#
# additional include and library search paths
USER_INC = -I$(CASA_ROOT)/include 
USER_INC += -I$(CASA_ROOT)/include/casacore # Needed to work around a bug in CASA-3

#
# Libraries (public and local)
# ----------------------------
LIBRARIES       = MountController

#
# Dependencies of MountController{,Stubs} libraries
MountController_OBJECTS = AzCableWrap \
                          MountControllerImpl MountControllerLifeCycle \
                          MountControllerThread \
                          MountControllerAzEl MountControllerRADec \
                          MountControllerEphemeris MountControllerPlanet \
                          MountControllerPointingData MountControllerStrokes \
                          MountControllerPointingAndFocusModels
MountController_LIBS	= antModeControllerImpl MountControllerStubs \
                          TETimeUtil PositionStreamConsumer sla \
                          maciErrType MountError \
                          MountStubs ControlDeviceStubs \
                          WeatherStationControllerStubs TMCDBAccessIFStubs \
                          TmcdbErrType ControlDataInterfacesStubs \
                          TMCDBDataStructuresStubs controlAlarmHelper Source 

MountControllerStubs_LIBS = AntModeControllerStubs acscommonStubs \
                            ModeControllerExceptions ControlExceptions \
                            ControlCommonInterfacesStubs SourceStubs

#
# Python stuff (public and local)
# ----------------------------
PY_SCRIPTS         = 	SpiralSearch FivePoint \
			GetPtModel SetPtModel GetAuxPtModel SetAuxPtModel \
                        spiral5pt \
                        SurvivalStow MaintenanceStow
PY_PACKAGES        = CCL

# 
# IDL Files and flags
# 
IDL_FILES = MountController

#
#>>>>> END OF standard rules

#
# INCLUDE STANDARDS
# -----------------

MAKEDIRTMP := $(shell searchFile include/acsMakefile)
ifneq ($(MAKEDIRTMP),\#error\#)
   MAKEDIR := $(MAKEDIRTMP)/include
   include $(MAKEDIR)/acsMakefile
endif

#
# TARGETS
# -------
all:	do_all
	@echo " . . . 'all' done" 

clean : clean_all
	$(RM) *~ ../include/*~ ../idl/*~ ../*~ core* a.out ../rtai
	@echo " . . . clean done"

clean_dist : clean clean_dist_all 
# The following directory is created by 'make all' and contains
# temporary files. make clean should delete it but, at the
# moment does not. There is an accepted SPR on this topic. Until
# then do it by hand.
	$(RM) ../lib ../bin 
	@echo " . . . clean_dist done"

man   : do_man 
	@echo " . . . man page(s) done"

install : install_all
	@echo " . . . installation done"

