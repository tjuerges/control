// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2009 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

#include "AzCableWrap.h"
#include <slalib.h> // for sla* functions
#include <cmath> // for std::abs

// shortcuts to avoid having to clutter the code with namespace qualifiers
using Control::AzCableWrap;
using Logging::Logger;
using std::abs;

AzCableWrap::AzCableWrap(Logger::LoggerSmartPtr logger)
    :Logging::Loggable(logger),
     az_m(0)
{  
}

AzCableWrap::AzCableWrap(const AzCableWrap& other)
    :Logging::Loggable(other),
     az_m(other.az_m)
{
}

AzCableWrap& AzCableWrap::operator=(const AzCableWrap& other) {
    if (this != &other) {
        // The Logger does not have an operator= function
        //        Logger::operator=(other);
        az_m = other.az_m;
    }
    return *this;
}

double AzCableWrap::unwrap(const double& az) {
    const double nAzpm = slaDrange(az); // -180 < nAzpm < +180
    if (abs(nAzpm) > M_PI_2) { // we have two options  
        const double nAzp = slaDranrm(az); // 0 < nAzp < 360
        if (az_m > M_PI_2) { // Always go to az > 90
            az_m = nAzp;
        } else if (az_m < - M_PI_2) { // Always go to az < -90
            az_m = nAzp - 2 * M_PI;
        } else { // Pick nearest az
            if (slaDranrm(slaDranrm(az_m) - nAzp) <= M_PI) {
                az_m = nAzp - 2 * M_PI;
            } else {
                az_m = nAzp;
            }
        }
    } else { // az is from -90 to 90 -> only one choice
        az_m = nAzpm;
    }
    // Do not go within five degrees of the azimuth limits. See AIV-3137 for
    // why.  This is a stop gap solution. The real solution is for this class
    // to choose a wrap that will allow us to track a source for as long as
    // required (or possible).
    const double azLimit =  M_PI/180.*265.; // 265 degrees
    if (az_m > azLimit) {
        az_m -= 2*M_PI;
    } else if (az_m < - azLimit) {
        az_m += 2*M_PI;
    }
    return az_m;
}

void AzCableWrap::setCurrentPosition(const double& az, const double& el) {
    // TODO. Range checking.
    az_m = az;
}
