// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"
#include <ModeControllerExceptions.h>
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <ModeControllerExceptions.h> // for MountFaultExImpl
#include <EquatorialObject.h> // EquatorialObject 
#include <casacore/casa/Quanta/MVAngle.h> // for casa::MVAngle
#include <string> // for string
#include <sstream> // for ostringstream
#include <cmath> // for M_PI & M_PI_2
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberatly left the casa code off this list so it can more
// easily be identified.
using std::string;
using std::ostringstream;
using Control::MountControllerImpl;
using ModeControllerExceptions::MountFaultExImpl;
using ControlExceptions::IllegalParameterErrorExImpl;

char* MountControllerImpl::raDecToString(double ra, double dec) {
    ostringstream msg; 
    msg << "(" << casa::MVAngle(ra).string(casa::MVAngle::TIME, 9)
        << ", " << casa::MVAngle(dec).string(casa::MVAngle::ANGLE, 9) << ")";
    CORBA::String_var retVal = msg.str().c_str();
    return retVal._retn();
}

void MountControllerImpl::setDirection(double commandedRA, 
                                       double commandedDec,
                                       double pmRA,
                                       double pmDec,
                                       double parallax) {
    // Put the mount in track here even though trackAsync is called in
    // setDirectionAsync to ensure that any exceptions that may be thrown when
    // putting the mount in autonomous mode get propogated to the caller.
    track();
    setDirectionAsync(commandedRA, commandedDec, pmRA, pmDec, parallax);
    waitUntilOnSource();
}

void MountControllerImpl::setDirectionAsync(double commandedRA, 
                                            double commandedDec,
                                            double pmRA,
                                            double pmDec,
                                            double parallax) {
    if (fabs(commandedRA) > 2*M_PI) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ParameterName", "commandedRA");
        ex.addData("Value", commandedRA);
        ex.addData("ValidRange", "-2*pi to 2*pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    if (fabs(commandedDec) > M_PI_2) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ParameterName", "commandedDec");
        ex.addData("Value", commandedDec);
        ex.addData("ValidRange", "-pi/2 to pi/2");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    // Set the limit at 30 arc-sec/year as Barnard's star moves at 10.3
    // arc-sec/year. Note the value must be in rad/sec.
    if (fabs(pmRA) > 30/(60*60*180/M_PI)/(60*60*24*365.25)) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ParameterName", "pmRA");
        ex.addData("Value", pmRA);
        ex.addData("ValidRange", 
                   "-4.6e-12 rad/sec to +4.6e-12rad/sec (+/-30 arc-sec/yr)");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    if (fabs(pmDec) > 30/(60*60*180/M_PI)/(60*60*24*365.25)) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("ParameterName", "pmDec");
        ex.addData("Value", pmDec);
        ex.addData("ValidRange", 
                   "-4.6e-12 rad/sec to +4.6e-12rad/sec (+/-30 arc-sec/yr)");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    // TODO. Check for a valid value in the parallax parameter

    if (!isObservableEquatorial(commandedRA, commandedDec, 0,
                                pmRA, pmDec, parallax)) {
        double az, el;
        toAzElEquatorial(commandedRA, commandedDec, pmRA, pmDec, parallax, 
                         az, el);
        ostringstream msg;
        msg << "Cannot move antenna " << getAntennaName() 
            << " to an (RA, Dec) of "
            << raDecToString(commandedRA, commandedDec)
            << " as its below the elevation limit."
            << " Corresponding (Az, El) is " << azElToString(az, el);
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__,__PRETTY_FUNCTION__);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);

    {
        ostringstream msg; 
        msg << "Moving the antenna " << getAntennaName() << " to (RA, Dec) = " 
            << raDecToString(commandedRA, commandedDec);
        LOG_TO_OPERATOR(LM_INFO, msg.str());
    }
  
    trackAsync();

    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    // Create the new source (it must be on the heap as it will be "deleted")
    boost::shared_ptr<TrackableObject> 
        newSource(new EquatorialObject(commandedRA, commandedDec,
                                       pmRA, pmDec, parallax,
                                       antennaLocation_m, getAntennaName(),
                                       padName_m,
                                       &*ws_m, obsFreq_m, epoch_m, 
                                       getLogger(), alarms_m, true));

    // move the mount to the new source ASAP
    switchSource(newSource);
}

void MountControllerImpl::toAzElEquatorial(double ra, double dec, double pmRA, 
                                           double pmDec, double parallax, 
                                           double& az, double& el) {
    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    EquatorialObject proposedTarget(ra, dec, pmRA, pmDec, parallax,
                                    antennaLocation_m, getAntennaName(),
                                    padName_m, &*ws_m, obsFreq_m, epoch_m,
                                    getLogger(), alarms_m, false);
    proposedTarget.getAzEl(getTimeStamp(), az, el);
}

void MountControllerImpl::
actualRADec(double& ra, double& dec, ACS::Time& timestamp) {
    double az, el;
    actualAzEl(az, el, timestamp);
  
    // TODO. Improve this by using the timestamp to find the right source (in
    // the latestCommands_m deque) and using that to convert to RA/Dec.
    {
        ACS::ThreadSyncGuard guard(&sourceListMutex_m); 
        if (!sourceList_m.empty()) {
            sourceList_m.front()->getActualRADec(az, el, timestamp, ra, dec);
            return;
        }
    }
    // Handle the case where we are not tracking anything by creating a fake
    // source. If I use a HorizonObject then I have to, separately, specify the
    // antenna location and weather station. But I cannot specify the observing
    // frequency so for the extra bit of accuracy I'll use an EquatorialObject.
    EquatorialObject dummySource(0.0, 0.0, 0.0, 0.0, 0.0,
                                 antennaLocation_m, getAntennaName(),
                                 padName_m, &*ws_m, obsFreq_m, epoch_m, 
                                 getLogger(), alarms_m, 
                                 false);
    dummySource.getActualRADec(az, el, timestamp, ra, dec);
}

void MountControllerImpl::
commandedRADec(double& ra, double& dec, ACS::Time& timestamp) {
    bool gotData = false;
    {
        ACS::ThreadSyncGuard guard(&latestCommandsMutex_m);
        const int cmdSize = latestCommands_m.size();
        if (cmdSize > 0) {
            const Commands& lastCmd =  latestCommands_m[cmdSize-1];
            ra = lastCmd.target.ra;
            dec = lastCmd.target.dec;
            timestamp = lastCmd.timestamp;
            gotData = true;
        }
    }

    if (!gotData) {
        MountFaultExImpl newEx( __FILE__, __LINE__, __PRETTY_FUNCTION__);
        string msg =  
            "Cannot get the commanded right ascension  or declination";
        msg += " as we are not sending any commands to the mount.";
        newEx.log();
        throw newEx.getMountFaultEx();
    }
}

bool MountControllerImpl::
isObservableEquatorial(double ra, double dec, double duration,
                       double pmRA, double pmDec, double parallax) {
    // TODO Check duration is not negative.

    double az, el;
    toAzElEquatorial(ra, dec, pmRA, pmDec, parallax, az, el);
    if (el < getElevationLimit()) return false;
    if (timeToSetEquatorial(ra, dec, pmRA, pmDec, parallax) < duration) {
        return false;
    }
    
    return true;
}

double MountControllerImpl::
timeToRiseEquatorial(double ra, double dec,
                     double pmRA, double pmDec, double parallax) {
    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    EquatorialObject proposedTarget(ra, dec, pmRA, pmDec, parallax,
                                    antennaLocation_m, getAntennaName(),
                                    padName_m, &*ws_m, obsFreq_m, epoch_m,
                                    getLogger(), alarms_m, false);
    return proposedTarget.timeToRise(getElevationLimit());
}

double MountControllerImpl::
timeToSetEquatorial(double ra, double dec,
                    double pmRA, double pmDec, double parallax) {
    // Warn the user if the weather parameters are bad. This also checks that
    // the component reference is OK and is done before we try to use it.
    checkWeatherIsOK();

    EquatorialObject proposedTarget(ra, dec, pmRA, pmDec, parallax,
                                    antennaLocation_m, getAntennaName(),
                                    padName_m, &*ws_m, obsFreq_m, epoch_m,
                                    getLogger(), alarms_m, false);
    return proposedTarget.timeToSet(getElevationLimit());
}

void MountControllerImpl::setEpoch(double epoch) {
    if (epoch < 1900.0 || epoch > 2200.5) {
        IllegalParameterErrorExImpl ex( __FILE__,__LINE__,__PRETTY_FUNCTION__);
        ex.addData("ParameterName", "epoch");
        ex.addData("Value", epoch);
        ex.addData("ValidRange", "1900.00 to 2200.5 years");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    epoch_m = epoch;
}

double MountControllerImpl::getEpoch() {
    return epoch_m;
}
