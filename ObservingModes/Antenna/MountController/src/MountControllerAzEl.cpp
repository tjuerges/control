// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "MountControllerImpl.h"

#include <ModeControllerExceptions.h>
#include <ControlExceptions.h> // for IllegalParameterErrorExImpl
#include <HorizonObject.h> // HorizonObject 
#include <LogToAudience.h> // for LOG_TO_{DEVELOPER,OPERATOR}
#include <loggingMACROS.h> // for AUTO_TRACE
#include <casacore/casa/Quanta/MVAngle.h> // for casa::MVAngle
#include <string> // for string
#include <sstream> // for ostringstream
#include <cmath> // for M_PI & M_PI_2
#include <TETimeUtil.h> // for TETimeUtil

// shortcuts to avoid having to clutter the code with namespace
// qualifiers. I've deliberately left the casa code off this list so it can
// more easily be identified.
using std::string;
using std::ostringstream;
using Control::MountControllerImpl;
using ModeControllerExceptions::MountFaultExImpl;
using ControlExceptions::IllegalParameterErrorExImpl;

void MountControllerImpl::setAzEl(double commandedAz, double commandedEl) {
    AUTO_TRACE(__func__);
    // Put the mount in track here even though trackAsync is called in
    // setAzElAsync to ensure that any exceptions that may be thrown when
    // putting the mount in autonomous mode get propogated to the caller.
    track(); 
    setAzElAsync(commandedAz, commandedEl);
    waitUntilOnSource();
}

void MountControllerImpl::setAzElAsync(double commandedAz, double commandedEl){
    AUTO_TRACE(__func__);

    if (fabs(commandedAz) > 3.0*M_PI_2) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "commandedAz");
        ex.addData("Value", commandedAz);
        ex.addData("ValidRange", "-3*pi/2 to 3*pi/2");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if ((commandedEl < 2*M_PI/180) || (commandedEl > 88.9*M_PI/180)) {
        IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __func__);
        ex.addData("ParameterName", "commandedEl");
        ex.addData("Value", commandedEl);
        ex.addData("ValidRange", "2*pi/180 to 88.9*pi/180");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }
    
    checkMountIsOK(__FILE__, __LINE__, __func__);

    {
        // use a String_var intermediary to ensure the char* returned by the
        // CORBA azElToString function is deleted.
        const string posn = CORBA::String_var(azElToString(commandedAz, commandedEl))._retn();
        ostringstream msg; 
        msg << "Moving antenna " << getAntennaName() 
            << " to (Az, El) = " << posn;
        LOG_TO_OPERATOR(LM_INFO, msg.str());
    }
  
    trackAsync();

    // Do *not* setup a weather station as there is no need to bother with
    // refraction corrections when commanding in Horizon coordinates.

    // Create the new source (it must be on the heap as it will be "deleted")
    boost::shared_ptr<TrackableObject> 
        newSource(new HorizonObject(commandedAz, commandedEl, 
                                    getLogger(), alarms_m));
    // Set the antenna location as its needed by the get{Actual,Commanded}RADec
    // function.
    newSource->setAntenna(antennaLocation_m, padName_m);

    // move the mount to the new source ASAP
    switchSource(newSource, false);
}

void MountControllerImpl::
actualAzEl(double& az, double& el, ACS::Time& timestamp) {
    AUTO_TRACE(__func__);

    checkMountIsOK(__FILE__, __LINE__, __func__);
    try {
        const Control::MountStatusData_var currentPosition = 
            mount_m->getMountStatusData();
        if (currentPosition->azPositionsValid && 
            currentPosition->elPositionsValid) {
            az = currentPosition->azPosition;
            el = currentPosition->elPosition;
            timestamp = currentPosition->timestamp;
        } else {
            MountFaultExImpl ex(__FILE__, __LINE__, __func__);
            string msg = "Cannot get a valid position from the mount.";
            ex.addData("Detail", msg);
            const string d = 
                currentPosition->azPositionsValid ? "true": "false";
            ex.addData("Azimuth valid flag", 
                       currentPosition->azPositionsValid ? "true": "false");
            ex.addData("Measured azimuth", currentPosition->azPosition);
            ex.addData("Elevation valid flag", 
                       currentPosition->elPositionsValid ? "true": "false");
            ex.addData("Measured elevation", currentPosition->elPosition);
            ex.log();
            throw ex.getMountFaultEx();
        }
    } catch (ControlExceptions::INACTErrorEx& ex) {
        MountFaultExImpl newEx(ex, __FILE__, __LINE__, __func__);
        string msg =  "Cannot determined the actual azimuth or elevation";
        msg += " as the mount component is not operational.";
        msg += " Possible causes include; ";
        msg += " the control subsystem is not operational,";
        msg += " the specified antenna is not operational.";
        msg += " Correct these faults and try again.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }
    {
        // use a String_var intermediary to ensure the char* returned by the
        // CORBA azElToString function is deleted.
        const string posn = CORBA::String_var(azElToString(az, el))._retn();
        const string msg = "Measured (az, el) is " + posn + 
            " at " + TETimeUtil::toTimeString(timestamp);
        LOG_TO_OPERATOR(LM_DEBUG, msg);
    }
}

void MountControllerImpl::
commandedAzEl(double& az, double& el, ACS::Time& timestamp) {
    AUTO_TRACE(__func__);

    bool gotData = false;
    {
        ACS::ThreadSyncGuard guard(&latestCommandsMutex_m);
        const int cmdSize = latestCommands_m.size();
        if (cmdSize > 0) {
            const Commands& lastCmd = latestCommands_m[cmdSize-1];
            az = lastCmd.commanded.az;
            el = lastCmd.commanded.el;
            timestamp = lastCmd.timestamp;
            gotData = true;
        }
    }

    if (!gotData) {
        MountFaultExImpl newEx( __FILE__, __LINE__, __func__);
        string msg =  "Cannot get the commanded azimuth or elevation";
        msg += " as we are not sending any commands to the mount.";
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getMountFaultEx();
    }
    {
        // use a String_var intermediary to ensure the char* returned by the
        // CORBA azElToString function is deleted.
        const string posn = CORBA::String_var(azElToString(az, el))._retn();
        const string msg = "Commanded (az, el) is " + posn + 
            " at " + TETimeUtil::toTimeString(timestamp);
        LOG_TO_OPERATOR(LM_DEBUG, msg);
    }
}

char* MountControllerImpl::azElToString(double az, double el) {
    // Commented out the following line because this function is called by the
    // trajectory planner thread numerous times and the tracing was having a
    // significant impact (10's of ms) on the thread execution time.
    //   AUTO_TRACE(__func__);
    ostringstream msg; 
    msg << "(" << casa::MVAngle(az).string(casa::MVAngle::ANGLE, 9)
        << ", " << casa::MVAngle(el).string(casa::MVAngle::ANGLE, 9) << ")";
    CORBA::String_var retVal = msg.str().c_str();
    return retVal._retn();
}

double MountControllerImpl::timeToSlew(double fromAz, double fromEl, double toAz, double toEl) {
    checkMountIsOK(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    return mount_m->timeToSlew(fromAz, fromEl, toAz, toEl);
}

