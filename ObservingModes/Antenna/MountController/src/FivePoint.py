#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This utility will move the specified antenna in a five point pattern
while tracking a celestial source. This script works units in arcsec.
FivePointBase will internally transformthem to the requirements of the
MountController.
"""

__revision__ = "@(#) $Id$"
#$Source$

import math
import time
import sys
from Numeric import zeros, array
from optparse import OptionParser, OptionGroup
import ModeControllerExceptions
import omniORB
import maciErrType
import CCL.FivePointsBase
import CCL.Gaussian
import CCL.TrackBase
import traceback
from math import pi


#HACK
from ambManager import *
import types
import struct

class FivePoints(CCL.FivePointsBase.FivePointsBase):
    
    def __init__(self, options):
        CCL.FivePointsBase.FivePointsBase.__init__(self, options.antenna)
        self.TPreads = zeros(12, 'd')
        self.options = options
        iniOffsets = options.initialOffsets.split(',')
        self.initOffsets(eval(iniOffsets[0]),eval(iniOffsets[1]))
        self.initialAzOff = eval(iniOffsets[0])
        self.initialElOff = eval(iniOffsets[1])
        self.gauss = CCL.Gaussian.Gaussian(options.offset,iniOffsets,options.gausName,options.quickForm,options.antenna)
        self.setParams(options.offSourceAz, options.offset)

        self.mgr = AmbManager(options.antenna)

    def bytes2int(self,b):
        "Converts bytes returned by struct.unpack to value "
        if isinstance(b, types.IntType):
            return b
        v = 0
        l = len(b)
        for i in range(0,l):
            v += b[l-i-1]*256**i
        return v


    def doDwell(self, position):
        """
        Overriding doDwell. This method is invoked afeter every movement of the
        anetnna. Is in this method where total power reading should be taken.
        Position is the place of the sky where the antenna has been displaced.
        The range goes from 0 to 11.
        SEE CCL.FivePointBase.doFivePoints for further details.
        """
        powerRead = 0.0
        time.sleep(self.options.wait)
        if options.verbose > 1:
            print "start integrating on position=%2d" % (position,)
        for i in range(options.repeat):
            #powerRead = CCL.TOTALPOWER.GETPOWER

            #HACK BEGIN remove when CCL for TP
            d,t = self.mgr.monitor(0,0x29,0x4)#HACK
            d = struct.unpack('!8B',d)#HACK
            bb = ord(options.bb.upper())-65#HACK
            collectedData = (self.bytes2int(d[bb*2:bb*2+2]) * 2.5 / 2**16)#HACK
            # change to +=  (Gene & Todd)
            powerRead += collectedData #HACK
            #HACK END

            time.sleep(options.delay)
        # change to =  (Gene & Todd)
        self.TPreads[position] += powerRead / float(options.repeat)
        if options.verbose > 0:
            print " done integrating on position=%2d, powerRead=%.4f, TPreads=%.4f" % (position, powerRead/float(options.repeat), self.TPreads[position])

    def doFivePoints(self):
        """
        Overriding doFivepoints method from the parrent class. The parrent
        class method only moves the antenna to the five points and the
        offsources in between. Since more inteligence is required, like
        averaging power readings, this must be done here.
        """
        for cycle in range(1, 1+(options.cycles*options.sumTPsets)):
            if ((cycle-1) % options.sumTPsets) == 0:
                print "Starting cycle %d of %d" % ((cycle-1)/options.sumTPsets+1, options.cycles)
            time.sleep(options.sleep)
            CCL.FivePointsBase.FivePointsBase.doFivePoints(self)
            if cycle % options.sumTPsets != 0:
                continue
            #Start gaussian stuff
            #average the power readings across multiple reading.
            for k in range(1, 12):
                self.TPreads[k] /= float(options.sumTPsets)
            try:
                self.gauss.calculate(self.TPreads)
            except (CCL.Gaussian.GaussianEx), e:
                print e
                print "skipping calculation"
                pass
            else:
                if self.gauss.getCycles() % int(options.avg) != 0:
                    if options.verbose > 1:
                        self.gauss.printIntermidiate(self.initialAzOff,self.initialElOff)
                    continue
                try:
                    (avgGaussAz, avgGaussEl) = self.gauss.getAvgGaussCorrection()
                    self.gauss.printFinal(self.initialAzOff,self.initialElOff)
                except (CCL.Gaussian.GaussianLimitEx), e:
                    print e
                    pass
                else:
                    if options.gaus.upper() in ('Y','YES'):
                        #try for ControlExceptions.IllegalParameterErrorEx
                        self.applyOffsets(avgGaussAz, avgGaussEl)
                        self.initialAzOff += avgGaussAz
                        self.initialElOff += avgGaussEl
            self.gauss.clear()
            self.TPreads = zeros(12, 'd')

     
if __name__ == "__main__":
    parser = OptionParser(usage="%prog -a ANTENNA -p PLANET [options] [TotalPower options] [Gaussian options]", version=__revision__)
    
    parser.add_option("-a", "--antenna", dest = "antenna", type = "str",
                      default = None,
                      help = "Antenna to use for tracking.",
                      metavar = "ANTENNA")

    parser.add_option("-c", "--cycles", default=1500, type="int", 
                      dest="cycles",
                      help="How many times to run entire 5 point cycle. Default: %default")
    
    parser.add_option("-i", "--initial", type="str", dest="initialOffsets", 
                      default='0,0', metavar='Az,El',
                      help="The initial AzEl Offsets to use e.g. 20,20 arcsecs. Leave empty to start at current position. Default: %default arcsecs.")
    
    parser.add_option("-F", "--freq", dest = "freq", type = "float",
                      default = 104E9,
                      help = "Observing Frequency",
                      metavar = "FREQUENCY")

    parser.add_option("-o", "--offset", default=30, type="float", 
                      dest="offset",
                      help="Offset from center in Az and El for the fivepoint in arcsecs. Default: %default arcsecs.")
    
    parser.add_option("-P", "--power", type="str", dest="getTP", 
                      default='yes', metavar='yes/no',
                      help="Get and show total power. Default: %default")
    
    parser.add_option("-p", "--planet", dest = "planet", type = "str",
                      default = None,
                      help = "Planet to track.",
                      metavar = "PLANET")
    
    parser.add_option("-q", "--quick", default=0, type="int", dest="quickForm",
                      help="Run the quick format: off/+el/-el/cen/+az/-az/off.")
    
    parser.add_option("-s", "--sleep", default=1, type="float", dest="sleep",
                      help="How long to pause between cycles. Default: %default seconds.")
    
    parser.add_option("-t", "--tolerance", default=5.0, type="float", dest="tolerance",
                      help="Set the acquisition tolerance in arcsec. Default: %default.")
    
    parser.add_option("-w", "--wait", default=4, type="float", dest="wait",
                      help="How long to wait for antenna to reach destination. Default: %default seconds.")
    
    parser.add_option("-z", "--offSourceAz", default=300.0, type="float", 
                      dest="offSourceAz",
                      help="Offset in Az when going 'offsource' arcsecs. Default: %default arcsecs.")
    
    parser.add_option("", "--verbose", dest = "verbose", type = "int",
                      default = 1,
                      help = "Verbose level from the least 1 to the maximum 5",
                      metavar = "LEVEL")

    #Total Power options
    tpGroup = OptionGroup(parser, "Total Power options")
    
    tpGroup.add_option("-b", "--baseband", default='A', type="str", dest="bb",
                       help="Use this baseband for 5 point calculations, range A,B,C,D. Default: %default")
    
    tpGroup.add_option("-r", "--repeat", default=10, type="int", dest="repeat",
                       help="Monitor total power how many times. Default: %default")
    
    tpGroup.add_option("-y", "--delay", default=0.1, type="float", dest="delay",
                       help="How long to delay between total power monitors. Default: %default seconds.")
    
    tpGroup.add_option("-f", "--file", default="", type="str", dest="fileName",
                       help="Enter file name to save five point data to file.\n"+\
                      "Name will be suffixed with '.csv'. Default: %default")
    
    tpGroup.add_option("-e", "--sum", default=1, type="int", dest="sumTPsets",
                       help="Sum across this many TP sets then do Gaussian fit - 0 for no summing. Default: %default")
    parser.add_option_group(tpGroup)
    
    
    #Gaussian options
    gGroup = OptionGroup(parser, "Gaussian options")
    
    gGroup.add_option("-g", "--gaus", default='No', type="str", dest="gaus",
                      help="Move antenna to Gaussian center. Default: %default.")
    
    gGroup.add_option("-v", "--avg", default=4, type="int", dest="avg",
                      help="Average how many Gaussian results before moving. Default: %default.")
    
    gGroup.add_option("-G", "--save", default="", type="str", dest="gausName",
                      help="Enter file name to save Gaussian results to file.\n"+\
                      "Name will be suffixed with '.csv'. Default: %default")
    parser.add_option_group(gGroup)
    
    
    (options, args) = parser.parse_args()
    myFivePoints = None
    myTrack = None
    if (options.antenna is None):
        parser.error("An ANTENNA must be suplied")
    if (options.planet is None):
        parser.error("A PLANET must be suplied")
    try:
        myTrack = CCL.TrackBase.TrackBase(options.antenna)
        print "Trying to track planet:", options.planet
        print "Setting timeout to 150 seconds"
        myTrack.mc.setTimeout(150)
        print "Setting tolerance to %.1f arc seconds" % (options.tolerance,)
        myTrack.mc.setTolerance(options.tolerance*pi/(180.0*3600.0))
        myTrack.trackPlanet(options.planet)
        obsFreq = myTrack.mc.getObservingFrequency()
        if (options.freq != obsFreq):
            print "Changing observing frequency from ", obsFreq, " to ", options.freq
            myTrack.changeObservingFrequency(options.freq)
        myFivePoints = FivePoints(options)
        myFivePoints.doFivePoints()
    except (ModeControllerExceptions.CannotGetAntennaEx), e:
        print options.antenna, "is not a valid antenna name."
    except (ModeControllerExceptions.BadConfigurationEx), e:
        print "The ALMA software is not 'Operational'", \
              "or the connection to the specified antenna has failed."
    except (ModeControllerExceptions.CannotGetAntennaEx), e:
        print options.antenna, "is not a valid antenna name."
    except (ModeControllerExceptions.TimeoutEx), e:
        print "Recieved a Timeout when trying to move."
    except (ModeControllerExceptions.MountFaultEx), e:
        print "Cannot comunicate with the mount", \
              "check there are no faults."
    except (omniORB.CORBA.BAD_PARAM), e:
        print "The ALMA software has not been started."
    except (maciErrType.CannotGetComponentEx), e:
        print "The ALMA software is groovy."
    except (CCL.TrackBase.UnreachableObjectException), e:
        print e
    except KeyboardInterrupt, ex:
        if(myFivePoints is not None):
            myFivePoints.restoreOffsets()
        print "Shutdown invoked by user"
    except:
        ex = sys.exc_info()
        traceback.print_exception(ex[0], ex[1], ex[2])
    del(myFivePoints) 
    del(myTrack) 

