#!/usr/bin/env python

import pylab as pl
from math import log
from math import sqrt
from math import degrees
#from numpy import std
#from numpy import mean


def mean(a):
    if len(a) == 0:
        return 0
    return sum(a)/len(a)

def std(a):
    if len(a) <= 1:
        return 1.0
    xBar = mean(a)
    b = []
    for x in a:
        b.append(pow(x-xBar,2))
    return sqrt(sum(b))/(len(b)-1)

    
class HoloCrossScan:
    def __init__(self, fd = None, scanType = None):
        self.horizStroke = None
        self.vertStroke  = None
        self.horizFit    = None
        self.vertFit     = None
        if fd != None:
            self.loadFromFile(fd, scanType)
        
    def loadFromFile(self, fd, scanType):
        fd.readline() #Get rid of Scan header
        if scanType != 'Vert':
            fd.readline() # Get rid of Az Position
        if scanType != 'Horiz':
            fd.readline() # Get rid of El Position
        if scanType != 'Vert':
            # Read the Horizontal Stroke
            self.setHorizStroke(HoloCrossStroke(fd = fd))
        if scanType != 'Horiz':
            # Read the Vertical Stroke
            self.setVertStroke(HoloCrossStroke(fd = fd))

    def saveToFile(self, fd, scanNumber):
        fd.write("Scan %d\n" % scanNumber)
        if self.horizStroke != None:
            fd.write("\tAzimuth Position: %f\n"%self.horizFit.getCenter())
        if self.vertStroke != None:
            fd.write("\tElevation Position: %f\n"%self.vertFit.getCenter())
        if self.horizStroke != None:
            self.horizStroke.saveToFile(fd)
        if self.vertStroke != None:
            self.vertStroke.saveToFile(fd)

    def printResults(self, scanNumber):
        print "Scan %d" % scanNumber
        if self.horizStroke != None:
            print "\tAzimuth Position: %f" % self.horizFit.getCenter()
        if self.vertStroke != None:
            print "\tElevation Position: %f" % self.vertFit.getCenter()

    def setHorizStroke(self, stroke):
        self.horizStroke = stroke
        self.horizFit = ParabolicFit(self.horizStroke)
        self.horizFit.fitAz()
        
    def setVertStroke(self, stroke):
        self.vertStroke = stroke
        self.vertFit = ParabolicFit(self.vertStroke)
        self.vertFit.fitEl()

    def horizPlot(self):
        if self.horizStroke != None:
            self.horizStroke.azSignalPlot(self.color)
            self.horizStroke.referenceLine(self.color)
        if self.horizFit != None:
            self.horizFit.plot(self.color)    

    def vertPlot(self):
        if self.vertStroke != None:
            self.vertStroke.elSignalPlot(self.color)
            self.vertStroke.referenceLine(self.color)
        if self.vertFit != None:
            self.vertFit.plot(self.color)

    def directionPlot(self):
        if self.horizStroke != None:
            self.horizStroke.directionPlot(self.color);
            if self.horizFit != None:
                horizCenter = float(self.horizFit.getCenter())
            else:
                horizCenter = mean(self.horizStroke.azVector)
        else:
            horizCenter = mean(self.vertStroke.azVector)

        if self.vertStroke != None:
            self.vertStroke.directionPlot(self.color);
            if self.vertFit != None:
                vertCenter = float(self.vertFit.getCenter())
            else:
                vertCenter = mean(self.vertStroke.azVector)
        else:
            vertCenter = mean(self.horizStroke.azVector)
            
        line, = pl.plot([horizCenter],[vertCenter])
        line.set_linestyle('')
        line.set_markeredgecolor(self.color)
        line.set_markerfacecolor(self.color)
        line.set_marker('+')
        line.set_markersize(12)

class HoloCrossStroke:
    def __init__(self,fd = None, dataTuple = None):
        if fd != None:
            dataTuple = self.loadFromFile(fd)
        self.azVector = dataTuple[0]
        self.elVector = dataTuple[1]
        self.ssVector = dataTuple[2]
        self.rrVector = dataTuple[3]    
        self.meanReference = mean(self.rrVector)
        self.peakSignal    = max(self.ssVector)

    def loadFromFile(self, fd):
        az = []
        el = []
        ss = []
        rr = []
        fd.readline() # Get rid of the Az / El / SS /RR header line
        
        dataline = fd.readline()
        while dataline != 'EndStroke\n':
            splitline = dataline.split()
            az.append(float(splitline[0]))
            el.append(float(splitline[1]))
            ss.append(float(splitline[2]))
            rr.append(float(splitline[3]))
            dataline = fd.readline()
        return (az,el,ss,rr)

    def saveToFile(self, fd):
        fd.write("Az\t\tEl\t\t\tSS\t\t\tRR\n")
        for i in range(len(self.azVector)):
            fd.write("%f\t%f\t%f\t%f\n" % (self.azVector[i],self.elVector[i],
                                           self.ssVector[i],self.rrVector[i]))
        fd.write('EndStroke\n')
                     
    def directionPlot(self, color):
        line, = pl.plot(self.azVector, self.elVector)
        line.set_color(color)
                
    def azSignalPlot(self, color):
        line, = pl.plot(self.azVector, self.ssVector)
        line.set_linestyle('')
        line.set_markeredgecolor(color)
        line.set_markerfacecolor(color)
        line.set_marker('o')
        line.set_markersize(2)
                    
    def elSignalPlot(self, color):
        line, = pl.plot(self.elVector, self.ssVector)
        line.set_linestyle('')
        line.set_markeredgecolor(color)
        line.set_markerfacecolor(color)
        line.set_marker('o')
        line.set_markersize(2)
                    
    def referenceLine(self, color):
        pl.plot(pl.gca().get_xlim(),[self.meanReference,self.meanReference])
    

class ParabolicFit:
    def __init__(self,stroke):
        self.fitMin = stroke.ssVector.index(stroke.peakSignal)
        self.fitMax = stroke.ssVector.index(stroke.peakSignal)
        self.peakSignal = stroke.peakSignal
        while self.fitMin > 0 and \
                  stroke.ssVector[self.fitMin] > 0.5* stroke.peakSignal :
            self.fitMin -= 1
            
        while self.fitMax < len(stroke.ssVector) and \
              stroke.ssVector[self.fitMax] > 0.5* stroke.peakSignal :
            self.fitMax += 1

        self.azData   = stroke.azVector[self.fitMin:self.fitMax]
        self.elData   = stroke.elVector[self.fitMin:self.fitMax]
        self.scaledData =[]
        for power in stroke.ssVector[self.fitMin:self.fitMax]:
            self.scaledData.append(power/self.peakSignal)

    def fitAz(self):
        self.abscissaMean = mean(self.azData)
        self.plotAxis = self.azData
        self.fitAxis = [ (x - self.abscissaMean) for x in self.plotAxis]
        self.fit = pl.polyfit(self.fitAxis,self.scaledData,2)

    def fitEl(self):
        self.abscissaMean = mean(self.elData)
        self.plotAxis = self.elData
        self.fitAxis = [ (x - self.abscissaMean) for x in self.plotAxis]
        self.fit = pl.polyfit(self.fitAxis,self.scaledData,2)

    def plot(self,color):
        plotData = pl.polyval(self.fit, self.fitAxis)
        line, = pl.plot(self.plotAxis,
                     [ (x * self.peakSignal) for x in plotData])
        line.set_color(color)
        line.set_linestyle('-')

    def getCenter(self):
        return (-self.fit[1]/(2*self.fit[0]))+self.abscissaMean

    def getFWHM(self):
        peakVal, = pl.polyval(self.fit,[self.getCenter()-self.abscissaMean])
        a = self.fit[0]
        b = self.fit[1]
        c = self.fit[2]-(peakVal/2.0)
        if pow(b,2) - (4*a*c) < 0 :
            return "N/A"
        return abs(sqrt(pow(b,2) - (4*a*c))/a)


class HoloCrossScanPlot:
    def __init__(self,filename=None):
        self.colorList = ['r','b','g','c','m','k']
        self.scanList = []
        self.date        = None
        self.antenna     = None
        self.scanType    = None
        self.scanVelocity= None
        self.scanLength  = None
        self.repeatCount = None
        self.frequency   = None
        self.nominalAz   = None
        self.nominalEl   = None
        
        if filename != None:
            self.loadFromFile(filename)
            

    def configurePlot(self,
                      date         = None,
                      antenna      = None,
                      scanType     = None,
                      scanVelocity = None,
                      scanLength   = None,
                      repeatCount  = None,
                      frequency    = None,
                      nominalAz    = None,
                      nominalEl    = None):
        self.date = date
        self.antenna = antenna
        self.scanType = scanType
        self.scanVelocity= scanVelocity
        self.scanLength = scanLength
        self.repeatCount = repeatCount
        self.frequency = frequency
        self.nominalAz  = nominalAz  
        self.nominalEl  = nominalEl

    def loadFromFile(self, filename):
        self.filename = filename
        fd = open(filename,'r')
        self.date        = fd.readline().split(':',1)[1].split()[0]
        self.antenna     = fd.readline().split(':')[1].split()[0]
        self.scanType    = fd.readline().split(':')[1].split()[0]
        self.scanVelocity= float(fd.readline().split(':')[1].split()[0])
        self.scanLength  = float(fd.readline().split(':')[1].split()[0])
        self.repeatCount = int(fd.readline().split(':')[1].split()[0])
        self.frequency   = float(fd.readline().split(':')[1].split()[0])
        fd.readline()
        self.nominalAz   = float(fd.readline().split('=')[1])
        self.nominalEl   = float(fd.readline().split('=')[1])

        for i in range(self.repeatCount):
            self.addScan(HoloCrossScan(fd,self.scanType))
        fd.close()

    def saveToFile(self, filename):
        fd = open(filename,'w')
        fd.write("Holography7m Cross Scan: " + self.date + "\n")
        fd.write("Antenna: " + self.antenna + "\n")
        fd.write("Scan Type: " + self.scanType + "\n")
        fd.write("Scan Velocity: %f deg/sec" % self.scanVelocity + "\n")
        fd.write("Scan Length: %f degrees" % self.scanLength + "\n")
        fd.write("Repeat Count: %d" % self.repeatCount + "\n")
        fd.write("Frequency: %e Hz" % self.frequency + "\n")
        fd.write("Nominal Tower Postion (deg):\n")
        fd.write("\tAzimuth   = %f\n" % self.nominalAz)
        fd.write("\tElevation = %f\n" % self.nominalEl)

        for scan in self.scanList:
            scan.saveToFile(fd,self.scanList.index(scan))
        fd.close()
    
    def addScan(self, scan):
        self.scanList.append(scan)
        scan.color = self.colorList[self.scanList.index(scan)]

    def printExecInfo(self):
        print "Holography7m Cross Scan: " + self.date
        print "Antenna: " + self.antenna
        print "Scan Type: " + self.scanType
        print "Scan Velocity: %f deg/sec" % self.scanVelocity
        print "Scan Length: %f degrees" % self.scanLength
        print "Repeat Count: %d" % self.repeatCount
        print "Frequency: %e Hz" % self.frequency 
        print "Nominal Tower Postion (deg):"
        print "\tAzimuth   = %f" % self.nominalAz
        print "\tElevation = %f" % self.nominalEl

    def printResults(self):
        azCenter = []
        elCenter = []
        azCenterMean = None
        elCenterMean = None
        for scan in self.scanList:
            scan.printResults(self.scanList.index(scan))
            if scan.horizFit != None:
                azCenter.append(scan.horizFit.getCenter())
            if scan.vertFit != None:
                elCenter.append(scan.vertFit.getCenter())
        if len(azCenter) > 0:
            azCenterMean = mean(azCenter)
            print ("Azimuth Center %3.5f deg (%5.3f\")" %
                   (azCenterMean, std(azCenter) * 3600))
        if len(elCenter) > 0:
            elCenterMean = mean(elCenter)
            print ("Elevation Center: %3.5f (%5.3f\")" %
                   (elCenterMean, std(elCenter) * 3600))
        return (azCenterMean, elCenterMean)

    def initDisplay(self):
        pl.figure(figsize=[11,11])
        pl.ioff()

    def show(self):
        pl.ion()
        pl.show()
        
    def draw(self):
        pl.clf()

        # Data Plots
        self.annotatePlot()
    
        # The actual plot of the scan
        pl.subplot(2,2,2)
        for scan in self.scanList:
            scan.directionPlot()
        pl.axis('equal')
        pl.xlabel("Azimuth (degrees)")
        pl.ylabel("Elevation (degrees)")

        # Azimuth power plots w/ fits
        pl.subplot(2,2,3)
        for scan in self.scanList:
            scan.horizPlot()
        pl.xlabel("Azimuth (degrees)")
        pl.ylabel("Signal Power")

        # Elevation power plots w/ fits
        pl.subplot(2,2,4)
        for scan in self.scanList:
            scan.vertPlot()
        pl.xlabel("Elevation (degrees)")
        pl.ylabel("Signal Power")

        pl.get_current_fig_manager().show()
        
    def annotatePlot(self):
        fig = pl.gcf()
        t = fig.text(0.3,0.9, 'Holography7m Beam Scan Results',
                     horizontalalignment='center',
                     fontsize='20')
        if self.date != None:
            fig.text(0.3, 0.883, '%s' % self.date,
                     horizontalalignment='center')
            
        if self.antenna != None:
            fig.text(0.09, 0.85, 'Antenna:      %s' % self.antenna)
        if self.frequency != None:
            fig.text(0.09, 0.835,'Frequency:    %3.3f GHz' %
                     (self.frequency/1E9))
        if self.repeatCount != None:
            fig.text(0.09, 0.82, 'Repeat Count: %d' % self.repeatCount)

        if self.scanType != None:
            fig.text(0.31, 0.85, 'Scan Type:       %s' % self.scanType)
        if self.frequency != None:
            fig.text(0.31, 0.835,
                     r'$\rm{Scan Length:\ \ \ \ \ \ }%3.3f^\prime$' %
                     (self.scanLength*60.))
        if self.repeatCount != None:
            fig.text(0.31, 0.82,
                    r'$\rm{Scan Velocity:\ \ \ }%3.2f^{\prime\prime}/\rm{sec}$' %
                     (self.scanVelocity*3600.))



        headerText = []
        headerText.append(fig.text(0.125, 0.785, 'Scan'))
        headerText.append(fig.text(0.125, 0.768, 'Number'))
        headerText.append(fig.text(0.22,0.785, 'Center'))
        headerText.append(fig.text(0.22,0.768, '(deg)'))
        headerText.append(fig.text(0.285,0.785, 'FWHM'))
        headerText.append(fig.text(0.285,0.768, '(arcmin)'))
        headerText.append(fig.text(0.36,0.785, 'Center'))
        headerText.append(fig.text(0.36,0.768, '(deg)'))
        headerText.append(fig.text(0.42,0.785, 'FWHM'))
        headerText.append(fig.text(0.42,0.768, '(arcmin)'))
#        headerText.append(fig.text(0.255,0.805, 'AZIMUTH',
#                                   fontweight='Bold'))
#        headerText.append(fig.text(0.39,0.805, 'ELEVATION',
#                                   fontweight='Bold'))
        headerText.append(fig.text(0.255,0.805, 'AZIMUTH'))
        headerText.append(fig.text(0.39,0.805, 'ELEVATION'))
        
        for t in headerText:
            t.set_horizontalalignment('center')

        azCenter = []
        elCenter = []
        for scan in self.scanList:
            scanText = []
            vertPos = 0.75 - (0.025 * self.scanList.index(scan))
            scanText.append(fig.text(0.125, vertPos ,'Scan %d' %
                                     self.scanList.index(scan),
                                     horizontalalignment='center'))
            if scan.horizFit != None:
                scanText.append(fig.text(0.245, vertPos, '%3.5f' %
                                         scan.horizFit.getCenter(),
                                         horizontalalignment='right'))
                azCenter.append(scan.horizFit.getCenter())
                if scan.horizFit.getFWHM() == 'N/A':
                    scanText.append(fig.text(0.285, vertPos,'N/A',
                                             horizontalalignment='center'))
                else:
                    scanText.append(fig.text(0.305, vertPos, '%3.2f' %
                                             (scan.horizFit.getFWHM()*60.),
                                             horizontalalignment='right'))

            else:
                scanText.append(fig.text(0.225, vertPos,'-',
                                         horizontalalignment='center'))
                scanText.append(fig.text(0.285, vertPos,'-',
                                         horizontalalignment='center'))

            if scan.vertFit != None:
                scanText.append(fig.text(0.38, vertPos, '%3.5f' %
                                         scan.vertFit.getCenter(),
                                         horizontalalignment='right'))
                elCenter.append(scan.vertFit.getCenter())
                if scan.vertFit.getFWHM() == 'N/A':
                    scanText.append(fig.text(0.42, vertPos,'N/A',
                                             horizontalalignment='center'))
                else:
                    scanText.append(fig.text(0.44, vertPos, '%3.2f' %
                                             (scan.vertFit.getFWHM()*60.0),
                                             horizontalalignment='right'))

            else:
                scanText.append(fig.text(0.36, vertPos,'-',
                                         horizontalalignment='center'))
                scanText.append(fig.text(0.42, vertPos,'-',
                                         horizontalalignment='center'))
        
            for t in scanText:
                t.set_color(scan.color)

        vertPos = 0.75 - (0.025 * (len(self.scanList)+0.5))
        fig.text(0.125, vertPos-0.01, 'Mean', horizontalalignment='center')
        if len(azCenter) > 0:
            fig.text(0.255, vertPos, r'$%3.5f^\circ$' % mean(azCenter),
                     horizontalalignment='center')
            fig.text(0.255, vertPos-0.02,
                     '$\sigma = %5.3f^{\prime\prime}$' %
                     (std(azCenter) * 3600),
                     horizontalalignment='center')
    
        if len(elCenter) > 0:
            fig.text(0.39, vertPos, r'$%3.5f^\circ$' % mean(elCenter),
                     horizontalalignment='center')
            fig.text(0.39, vertPos-0.02,
                     '$\sigma = %5.3f^{\prime\prime}$' %
                     (std(elCenter) * 3600),
                     horizontalalignment='center')

        vertPos = 0.75 - (0.025 * (len(self.scanList)+3.5))
        fig.text(0.125, vertPos, 'Nominal', horizontalalignment='center')
        fig.text(0.255, vertPos, r'$%3.5f^\circ$' % degrees(self.nominalAz),
                  horizontalalignment='center')
        fig.text(0.39, vertPos,  r'$%3.5f^\circ$' % degrees(self.nominalEl),
                  horizontalalignment='center')
