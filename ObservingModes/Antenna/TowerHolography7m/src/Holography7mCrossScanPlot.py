#!/usr/bin/env python

from Holography7mCrossScanPlotter  import *
from optparse import OptionParser 
parser = OptionParser()
parser.add_option("-f", "--file", dest = "filename",
                  help="Filename to plot (required)")
parser.add_option('-q', '--quiet', action="store_false",default = True,
                  dest = "guiDisplay", help='Disable GUI display')

(options, args) = parser.parse_args()

if options.filename == None:
    print "A filename must be specified, use the \"--file\" option"
    exit(-1)
    
plotter = HoloCrossScanPlot(filename=options.filename)
if options.guiDisplay:
    plotter.initDisplay()
    plotter.draw()
    plotter.show()
else:
    plotter.printExecInfo()
    plotter.printResults()
