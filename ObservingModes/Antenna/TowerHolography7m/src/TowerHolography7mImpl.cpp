// @(#) $Id$
// 
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2007, 2008
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "TowerHolography7mImpl.h"

#include <loggingMACROS.h> // for AUTO_TRACE
#include <string> // for string
#include <sstream> // for ostringstream
#include <iomanip> // for std::setprecision and std::fixed
#include <ModeControllerExceptions.h>
#include <HolographyExceptions.h>
#include <ControlExceptions.h>
#include <EthernetDeviceExceptions.h>
#include <AntennaC.h> // for Control::Antenna_var
#include <acstime.h> // for acstime::Epoch
#include <acstimeTimeUtil.h> // for TimeUtil
#include <TETimeUtil.h> // for TETimeUtil
#include <acstimeDurationHelper.h> // for DurationHelper
#include <acstimeEpochHelper.h> // for EpochHelper
#include <cmath> // for M_PI, fabs & MAXFLOAT
#include <unistd.h> // for usleep
#include <PositionStreamConsumer.h> // For PositionConsumer
#include <Angle.h> // for Angle
#include <asdmIDLTypesC.h> // for IDLAngle
#include <PositionStreamConsumer.h> // For PositionConsumer
#include <slalib.h> // for sla* functions
#include <HoloRx7mC.h>
#include <MountControllerC.h>
#include <MountC.h>

#include <TMCDBAccessIFC.h>

// shortcuts to avoid having to clutter the code with namespace qualifiers
using std::string;
using std::ostringstream;
using std::endl;
using Control::TowerHolography7mImpl;
using Control::TowerHolography7m;

using ModeControllerExceptions::MountFaultEx;
using ModeControllerExceptions::MountFaultExImpl;
using ModeControllerExceptions::CannotGetAntennaEx;
using ModeControllerExceptions::CannotGetAntennaExImpl;
using ModeControllerExceptions::BadConfigurationEx;
using ModeControllerExceptions::BadConfigurationExImpl;
using ModeControllerExceptions::UnallocatedEx;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::TimeoutEx;
using ModeControllerExceptions::TimeoutExImpl;
using ControlExceptions::IllegalParameterErrorEx;
using ControlExceptions::IllegalParameterErrorExImpl;
using ModeControllerExceptions::HardwareFaultEx;
using ModeControllerExceptions::HardwareFaultExImpl;
using ControlExceptions::DeviceBusyEx;
using ControlExceptions::DeviceBusyExImpl;
using ModeControllerExceptions::BadDataEx;
using ModeControllerExceptions::BadDataExImpl;
using ControlExceptions::HardwareErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using EthernetDeviceExceptions::SocketOperationFailedEx;
using EthernetDeviceExceptions::SocketOperationFailedExImpl;

// The zero time should always be exactly zero (as an ACS::Time object) 
const ACS::Time zeroTime = 0;
// The undefined time value may be anything.
const ACS::Time undefinedTime = zeroTime;

//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
TowerHolography7mImpl::
TowerHolography7mImpl(const ACE_CString& name, maci::ContainerServices* cs)
  :AntModeControllerImpl(name, cs),
   positionConsumer_m(0),
   startTime_m(undefinedTime),
   stopTime_m(undefinedTime)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

towerAz_m = 0.0/180.0*M_PI;
towerEl_m = 5.0/180.0*M_PI;
}

TowerHolography7mImpl::~TowerHolography7mImpl()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);
  cleanUp(); 
  // No need to call releaseReferences. Its done in antModeController::cleanUp
}       

//-----------------------------------------------------------------------------
// Lifecycle Methods
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Mode Controller Methods
//-----------------------------------------------------------------------------

void TowerHolography7mImpl::acquireReferences(const string& antennaName)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  // Remove any old references (in case this function is executed twice).
  // But only do this if necessary.
  if (antennaName != getAntennaName()) releaseReferences();

  // If the antenna is already initialized it means we already have the
  // references for the correct antenna. So there is nothing left to do.
  if (getStatus() != Control::AntModeController::UNINITIALIZED) return;
    
  {
    const string msg = "Configuring antenna " + antennaName + " for tower holography.";
    LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, msg, log_audience::OPERATOR);
  }

  // get reference to the Antenna
  maci::SmartPtr<Control::Antenna> antenna;
  const string antennaComponentName = "CONTROL/" + antennaName;
  try {
    antenna = getContainerServices()->
      getComponentNonStickySmartPtr<Control::Antenna>(antennaComponentName.c_str());
    antenna->reportPointingModel();
  } catch (maciErrType::CannotGetComponentExImpl& ex) {
    ostringstream msg;
    msg << "Cannot get a reference to the '" << antennaName << "' antenna component." << endl
        << "Possible causes include:" << endl
        << "1. You specified an incorrect name" << endl
        << "2. The antenna is not operational" << endl
        << "3. The CDB is incorrect" << endl
        << "4. The connection to the ABM is faulty";
    CannotGetAntennaExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getCannotGetAntennaEx();
  } catch (ControlExceptions::INACTErrorEx& ex) {
    ostringstream msg;
    msg << "Unable to report pointing model for '" << antennaName
	<< "' Antenna is not operational";
    LOG_TO_AUDIENCE(LM_DEBUG, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
  }

  string currentComponent("MountController");
  CORBA::String_var deviceName;
  try {
    { 
      deviceName = antenna->getMountControllerName();
      // Get a reference to the MountController component (that may start the component).
      maci::ComponentSpec mountSpec;
      mountSpec.component_name = deviceName;
      mountSpec.component_type = "IDL:alma/Control/MountController:1.0";
      mountSpec.component_code = "*";
      mountSpec.container_name = "*";

      mountController_m = getContainerServices()->getCollocatedComponentSmartPtr<Control::MountController>
        (mountSpec, false, antennaComponentName.c_str());
      mountController_m->allocate(antennaName.c_str());
    }

    // Cache a reference to the Mount component
    currentComponent = "Mount";
    deviceName = mountController_m->getMount();
    mount_m = getContainerServices()->getComponentNonStickySmartPtr<Control::Mount>(deviceName);

    // Get a reference to the holography receiver 
    currentComponent = "HoloRx7m";
    deviceName = antenna->getSubdeviceName(currentComponent.c_str());
    hrx7m_m = getContainerServices()->getComponentNonStickySmartPtr<Control::HoloRx7m>(deviceName);

    // Setup connection with holography 7m receiver
    hrx7m_m->connectRX();

  } catch (maciErrType::CannotGetComponentExImpl& ex) {
    ostringstream msg;
    msg << "Cannot get a reference to the " << deviceName << " component.  Possible causes are" << endl
        << "1. The antenna name is incorrect " << endl
        << "2. The antenna is not operational";
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch (IllegalParameterErrorEx ex) {
    string msg = "The configuration for antenna " + antennaName + " does not contain a ";
    msg += currentComponent + " device.  Update the telescope monitor & control data base (TMCDB)";
    msg += " or use a different antenna.";
    BadConfigurationExImpl newEx(ex,  __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg);
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch (maciErrType::NoPermissionExImpl& ex) {
    ostringstream msg;
    msg << "Cannot get a mount controller component for the '" << antennaName << "' antenna."
        << " Possible causes are" << endl
        << "1. The antenna name is incorrect " << endl
        << "2. The ALMA software is not operational.";
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch (maciErrType::IncompleteComponentSpecExImpl& ex) {
    ostringstream msg;
    msg << "Cannot get a mount controller component for the '" << antennaName << "' antenna."
        << " Possible causes are" << endl
        << "1. A software problem.";
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch (maciErrType::InvalidComponentSpecExImpl& ex) {
    ostringstream msg;
    msg << "Cannot get a mount controller component for the '" << antennaName << "' antenna."
        << " Possible causes are" << endl
        << "1. A software problem.";
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch (maciErrType::ComponentSpecIncompatibleWithActiveComponentExImpl& ex) {
    ostringstream msg;
    msg << "Cannot get a mount controller component for the '" << antennaName << "' antenna."
        << " Possible causes are" << endl
        << "1. The system was not shutdown completely before restarting";
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch (SocketOperationFailedExImpl& ex) {
    ostringstream msg;
    msg << "Cannot make a connection with Holography Receiver (ACA 7m)"
        << " Possible causes are" << endl
        << "1. Receiver refused connection. (release other connection)." << endl
        << "2. Receiver network stack freezed. (reset the receiver)" << endl
        << "3. A software problem." << endl
        << "please check container log for detailed message.";
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch (HardwareErrorExImpl& ex) {
    ostringstream msg;
    msg << "Holography Receiver (ACA 7m) seems not working correctly."
        << " Possible causes are" << endl
        << "1. Receiver 5MHz input stopped." << endl
        << "2. A software problem." << endl
        << "please check container log for detailed message.";
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  }

  // Initialize the position consumer
  positionConsumer_m = new PositionStreamConsumer(antennaName); 

  // Set the antenna name and state in the base class
  AntModeControllerImpl::acquireReferences(antennaName);

  // Get reference holography tower position according to antenna pad
  CORBA::String_var pad;
  TMCDB::HolographyTowerRelativePadDirectionSeq padDirections;
  maci::SmartPtr<TMCDB::Access> tmcdb;
  try {
    tmcdb = getContainerServices()->getDefaultComponentSmartPtr<TMCDB::Access>("IDL:alma/TMCDB/Access:1.0");
    pad = tmcdb->getCurrentAntennaPadInfo(antennaName.c_str())->PadName;
    padDirections = *(tmcdb->getHolographyTowerRelativePadDirection(pad.in()));
  } catch(maciErrType::NoDefaultComponentExImpl& ex) {
    ostringstream msg;
    msg << "Error accessing TMCDB Access component. No default component." << endl;
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch(maciErrType::CannotGetComponentExImpl& ex) {
    ostringstream msg;
    msg << "Error accessing TMCDB Access component." << endl;
    BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  } catch(...) {
    ostringstream msg;
    msg << "Error getting TMCDB configuration for pad "<< pad <<"." << endl;
    BadConfigurationExImpl newEx(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  }

  if (padDirections.length() != 1) {
    ostringstream msg;
    msg << "Multiple reference tower positions for pad " << pad << " are defined, please fix your TMCDB configuration." << endl;
    BadConfigurationExImpl newEx(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", msg.str());
    newEx.log();
    throw newEx.getBadConfigurationEx();
  }

  double az = padDirections[0].azimuth;
  double el = padDirections[0].elevation;

  ACS_LOG(LM_SOURCE_INFO, __PRETTY_FUNCTION__, (LM_INFO, "Reference tower position for pad %s: %f, %f.", pad.in(), az, el));
  towerAz_m = az/180.0*M_PI;
  towerEl_m = el/180.0*M_PI;
}

//---------------------------------------------------------------------
void TowerHolography7mImpl::releaseReferences()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  // Deactivate the PositionStreamConsumer
  // Note that it is not deleted. This will be done by the ORB (I hope).
  if (positionConsumer_m != 0) {
      positionConsumer_m->disconnect();
      positionConsumer_m = 0;
  }

  // Do not turn on the focus model and focusPointingModel even though they
  // were turned off earlier. The subreflector can only be used once the
  // holography receiver is removed from the top of the quadruped. When that
  // happens the focus model will be reenabled as part of the Mount component
  // startup. This ensures that any manually set set subreflector position is
  // not altered when the focus model is re-enabled.
  //  mount_m->enableFocusPointingModel(true);
  //  mount_m->enableFocusModel(true);

  // Release our references to the mount controller, holography receiver
  // and Mount components. The smart pointer will call releaseComponent if
  // necessary.
  mountController_m.release();
  mount_m.release();
  hrx7m_m.release();

  // The derived class must call the base class releaseReferences method to
  // change state and unset the antenna name.
  AntModeControllerImpl::releaseReferences();
}

//-----------------------------------------------------------------------------
// CORBA interface
//-----------------------------------------------------------------------------
void TowerHolography7mImpl::initializeHardware(bool lowBand)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  } 
  try {
    // Turn off the focus model and focus pointing model. The subreflector is
    // not used for tower holography so there should not be any pointing
    // corrections to compensate for its motion. All automatic subreflector
    // movement is disabled so that holography will not be affected by any
    // problems moving the subreflector.
    mount_m->enableFocusPointingModel(false);
    mount_m->enableFocusModel(false);
    // Send the antenna towards the tower
    status_m = Control::AntModeController::SLEWING;
    mountController_m->setAzElAsync(towerAz_m, towerEl_m);
  } catch (IllegalParameterErrorEx& ex) {
    status_m = Control::AntModeController::ALLOCATED;
    MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getMountFaultEx();
  } catch (TimeoutEx& ex) {
    status_m = Control::AntModeController::ALLOCATED;
    MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getMountFaultEx();
  } // No need to catch MountFault's as they are in the exception declaration
    // for this function and be be passed up without conversion to a different
    // type of exception
  status_m = Control::AntModeController::ALLOCATED;

  try {
    // This blocks until the mount is pointing at the tower. 
    mountController_m->waitUntilOnSource();
  } catch (TimeoutEx& ex) {
    MountFaultExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.addData("Detail", "converted from TimeoutEx");
    newEx.log();
    throw newEx.getMountFaultEx();
  } catch (...) {
  }

  // Set the state (ready to observe)
  status_m = Control::AntModeController::READY;
}

void TowerHolography7mImpl::shutdownHardware()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  } 

  // Stow the antenna and activate the brakes. This may throw a mountfault
  try {
    mountController_m->stop();
  } catch (MountFaultEx& ex) {
    status_m = Control::AntModeController::ALLOCATED;
    throw;
  }

  // The hardware should now be uninitialized
  status_m = Control::AntModeController::ALLOCATED;
}

void TowerHolography7mImpl::setTowerPosition(CORBA::Double az, CORBA::Double el)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (fabs(az) > 3.0*M_PI_2) {
    IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.addData("ParameterName", "az");
    ex.addData("Value", az);
    ex.addData("ValidRange", "-3*pi/2 to 3*pi/2");
    ex.log();
    throw ex.getIllegalParameterErrorEx();
  }
  if ((el < 0) || (el > M_PI_2)) {
    IllegalParameterErrorExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.addData("ParameterName", "el");
    ex.addData("Value", el);
    ex.addData("ValidRange", "0 to pi/2");
    ex.log();
    throw ex.getIllegalParameterErrorEx();
  }
  towerAz_m = az;
  towerEl_m = el;
}

void TowerHolography7mImpl::
getTowerPosition(CORBA::Double& az, CORBA::Double& el)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  az = towerAz_m;
  el = towerEl_m;
}

void TowerHolography7mImpl::getTowerXYZPosition(CORBA::Double& X, 
                                              CORBA::Double& Y,
                                              CORBA::Double& Z)
{
  /* Someday someone smart (RALPH) should figure out how to get these
     from the TMCDB, and keep the az,el,range and XYZ coordinates together
     for now I just hard code them.
  */

  AUTO_TRACE(__PRETTY_FUNCTION__);

  X = -1601267.611872;
  Y = -5042415.614327;
  Z = 3554338.023112;
}

CORBA::Double TowerHolography7mImpl::getFrequency()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  } 
  double freq = 104.02E9;
  return freq;
}

CORBA::Double TowerHolography7mImpl::
startHorizontalSubscan(CORBA::Double offset, CORBA::Boolean forward,
                       CORBA::Double width, CORBA::Double velocity)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  const double maxVelocity = 6.0/180*M_PI;
  prepareForMotionSubscan(offset, forward, width, velocity, maxVelocity);
  

  // Send mount to move to the specified offset. Do not wait for it to get
  // there but assume it will be there after a fixed amount of time. 
  mountController_m->setHorizonOffsetAsync(-width/2, offset);

  // This sets the st{art,op}Time_m and incorporates an assumption on how long
  // it will take for the antenna to move to the initial offset.
  defineSubscanBoundries(fabs(width/velocity));

  // Queue the strokes
  mountController_m->
      setHorizonLinearStrokeQueued(velocity, 0.0, 
                                   -width/2, offset, startTime_m);
  mountController_m->
    setHorizonOffsetQueued(width/2, offset, stopTime_m);

  // Start the data collection in the receiver
  return concludeSubscan("horizontal");
}

CORBA::Double TowerHolography7mImpl::
startVerticalSubscan(CORBA::Double offset, CORBA::Boolean forward,
                     CORBA::Double width, CORBA::Double velocity)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  const CORBA::Double maxVelocity = 3.0/180*M_PI;
  prepareForMotionSubscan(offset, forward, width, velocity, maxVelocity); 

  // Send mount to move to the specified offset. Do not wait for it to get
  // there but assume it will be there after a fixed amount of time. 
  mountController_m->setHorizonOffsetAsync(offset, -width/2);

  // This sets the st{art,op}Time_m and incorporates an assumption on how long
  // it will take for the antenna to move to the initial offset.
  defineSubscanBoundries(fabs(width/velocity));

  // Queue the strokes
  mountController_m->setHorizonLinearStrokeQueued(0, velocity, 
                                                  offset, -width/2, 
                                                  startTime_m);
  mountController_m->
      setHorizonOffsetQueued(offset, width/2, stopTime_m);
  
  // Start the data collection in the receiver
  return concludeSubscan("vertical");
}

CORBA::Double TowerHolography7mImpl::startPhaseCal(CORBA::Double duration)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (duration <= 0.0) {
    IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.addData("ParameterName", "duration");
    ex.addData("Value", duration);
    ex.addData("ValidRange", "0+ to infinity");
    ex.log();
    throw ex.getIllegalParameterErrorEx();
  }

  prepareForSubscan();

  // Move the mount to point at the tower.
  mountController_m->setAzEl(towerAz_m, towerEl_m);

  // This sets the st{art,op}Time_m
  defineSubscanBoundries(duration);
  
  // Start the data collection in the receiver
  return concludeSubscan("phase calibration");
}

void TowerHolography7mImpl::
defineSubscanBoundries(double subscanDuration)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  // Work out when the subscan should start.

  EpochHelper startTime(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
  startTime.value(TETimeUtil::ceilTE(startTime.value()));
  acstime::Duration leadTime = TETimeUtil::TE_PERIOD_DURATION;
  // This is 4 a second delay and is dominated by the 4 second max lookahead
  // time in the MountController. To eliminate this delay:
  // * This script will need to know the parameters for the next sub-scan. This
  //   will allow a queue of strokes to be established and largely eliminate this
  //   4 second penalty between each subscan.
  // * This script will need to know how long it takes for the Mount to move to
  //   the initial offset (this should come from the Mount).
  leadTime.value *= 85;
  startTime += leadTime;

  // Work out when the subscan should stop
  EpochHelper stopTime(startTime.value());
  acstime::Duration
      strokeDuration = DurationHelper(static_cast<long double>(subscanDuration)).value();
  strokeDuration = TETimeUtil::ceilDuration(strokeDuration);
  stopTime += strokeDuration;

  // Now set the data members
  startTime_m = startTime.value().value;
  stopTime_m = stopTime.value().value;
}

char* TowerHolography7mImpl::getMountController()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (!mountController_m.isNil()) {
    return mountController_m->name();
  } else {
    return CORBA::string_dup("");
  }
}

char* TowerHolography7mImpl::getHolographyReceiver()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (!hrx7m_m.isNil()) {
    return hrx7m_m->name();
  } else {
    return CORBA::string_dup("");
  }
}

Control::HolographySubScanData*
TowerHolography7mImpl::getSubscanData(CORBA::Double timeout)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  } 

  if (timeout < 0) timeout = 1e38;// a long time

  // wait for the stroke to end or timeout immediately if it will take longer
  // than the specified timeout value. 
  // TODO: The initial value of the sleepTime variable is the amount of extra
  // time to wait to allow for the antenna position data to get from the
  // antenna to the ACC via the notification channel. This propagation time is
  // current set to 1 second but should eventually be configurable via the CDB.
  double sleepTime(1.0);
  {
    const acstime::Epoch timeNow = 
      EpochHelper(TimeUtil::ace2epoch(ACE_OS::gettimeofday())).value();
    EpochHelper timeToComplete(timeNow);
    timeToComplete += 
      DurationHelper(static_cast<long double>(timeout)).value();
    EpochHelper stopTime(stopTime_m);
    if (timeToComplete.value().value < stopTime_m) {
      TimeoutExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
      ex.addData("Specified timeout", timeout);
      ex.addData("Latest completion time", 
                 TETimeUtil::toTimeString(timeToComplete));
      ex.addData("Expected completion time",  
                 TETimeUtil::toTimeString(stopTime));
      ex.log();
      throw ex.getTimeoutEx();
    } else {
      sleepTime += DurationHelper(stopTime.difference(timeNow)).toSeconds();
    }
  }
  if (sleepTime > 0) {
    ostringstream msg;
    msg << "Waiting " << std::fixed << std::setprecision(1) << sleepTime 
        << " seconds for the sub-scan to complete.";
    LOG_TO_AUDIENCE(LM_DEBUG, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
    usleep(static_cast<unsigned long>(sleepTime*1E6));
  }

  status_m = Control::AntModeController::ANALYZING;

  Control::HolographySubScanData_var subscanData = 
    new Control::HolographySubScanData;

  subscanData->antennaName = CORBA::string_dup(getAntennaName().c_str());
  subscanData->startTime = startTime_m;
  subscanData->endTime = stopTime_m;

  { // Get the Holography data
    HoloRx7m::HoloDeviceData_var rx7mData;
    try {
      rx7mData = hrx7m_m->getSubscanData(0);
    } catch(HolographyExceptions::SubscanAbortedEx& ex) {
      TimeoutExImpl newEx(ex,  __FILE__, __LINE__, __PRETTY_FUNCTION__);
      newEx.log();
      throw newEx.getTimeoutEx();
    } catch(HolographyExceptions::NoSubscanStopEx& ex) {
      // TODO. A better job of handling these exceptions
      TimeoutExImpl newEx(ex,  __FILE__, __LINE__, __PRETTY_FUNCTION__);
      newEx.log();
      throw newEx.getTimeoutEx();
    } catch(HolographyExceptions::NoSubscanEx& ex) {
      // TODO. A better job of handling these exceptions
      TimeoutExImpl newEx(ex,  __FILE__, __LINE__, __PRETTY_FUNCTION__);
      newEx.log();
      throw newEx.getTimeoutEx();
    }

    const ACS::TimeInterval exposureDuration = rx7mData->exposureDuration;
    subscanData->exposureDuration = exposureDuration;
    const int numSamples = rx7mData->holoData.length();
    {
      ostringstream msg;
      msg << "Retrieved " << numSamples << " data samples from the holography receiver (ACA7m).";
      LOG_TO_AUDIENCE(LM_DEBUG, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
    }
    if (rx7mData->startTime != startTime_m) {
      ostringstream msg;
      msg << "The start time on the data returned from "
          << "the holography receiver is wrong." << endl
          << "The expected start time is "
          << TETimeUtil::toTimeString(startTime_m)
          << " and the actual start time is "
          << TETimeUtil::toTimeString(rx7mData->startTime);
      BadDataExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
      ex.addData("Detail", msg.str());
      ex.log();
      throw ex.getBadDataEx();
    }
    subscanData->holoData.length(numSamples);

    for (int i = 0; i < numSamples; i++) {
      subscanData->holoData[i] = rx7mData->holoData[i];
    }

  }

  // This stops the flow of data. Its started in the startHorizontalStroke method
  mount_m->enableMountStatusDataPublication(false);
  
  // get the position Data
  const ACS::Time startPosTime = startTime_m;
  const ACS::Time stopPosTime = stopTime_m;
  
  // Get the time stamps
  vector<ACS::Time> times;
  try {
    positionConsumer_m->getTimes(startPosTime, stopPosTime, times);
  } catch (IllegalParameterErrorExImpl& ex) {
    BadDataExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getBadDataEx();
  }
  const unsigned int timeSize = times.size();
  { 
    ostringstream msg;
    msg << "Retrieved " << timeSize << " time stamps from the mount.";
    LOG_TO_AUDIENCE(LM_DEBUG, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
  }
  
  // Get the commanded positions
  vector<double> cmdAz, cmdEl;
  try {
    positionConsumer_m->getCmdPosition(startPosTime, stopPosTime, 
                                       cmdAz, cmdEl);
  } catch (IllegalParameterErrorExImpl& ex) {
    BadDataExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getBadDataEx();
  }
  const unsigned int cmdSize = cmdAz.size();
  {
    ostringstream msg;
    msg << "Retrieved " << cmdSize << " commanded positions from the mount.";
    LOG_TO_AUDIENCE(LM_DEBUG, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
  }
  
  // Get the actual positions
  vector<double> actAz, actEl;
  try {
    positionConsumer_m->getActPosition(startPosTime, stopPosTime, 
                                       actAz, actEl);
  } catch (IllegalParameterErrorExImpl& ex) {
    BadDataExImpl newEx(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getBadDataEx();
  }
  const unsigned int actSize = actAz.size();
  { 
    ostringstream msg;
    msg << "Retrieved " << actSize << " measured positions from the mount.";
    LOG_TO_AUDIENCE(LM_DEBUG, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
  }
    
  // Now check that all the position data is a consistent size
  const unsigned int actualNumTEs = timeSize; 
  if (actualNumTEs != cmdSize &&
      cmdSize != cmdEl.size() &&
      (2*actualNumTEs) != actSize &&
      actSize != actEl.size()) {
    ostringstream msg;
    msg << "Inconsistant amount of data returned from the mount.";
    BadDataExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.addData("Detail", msg.str());
    ex.log();
    throw ex.getBadDataEx();
  }
    
  // Now check that all the data is the expected size
  const unsigned int expectedNumTEs = 
    (stopTime_m - startTime_m)/
    TETimeUtil::TE_PERIOD_DURATION.value;
  if (expectedNumTEs != timeSize) {
    ostringstream msg;
    if (timeSize == 0) {
      msg << "Unable to get any position data. Zeroing the positions for the entire sub-scan.";
      LOG_TO_AUDIENCE(LM_ERROR, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
    } else {
      msg << "Problem getting positions from the Mount." << endl
          << "Zeroing " << expectedNumTEs - times.size() << " positions out of " << expectedNumTEs 
          << " in this sub-scan.";
      LOG_TO_AUDIENCE(LM_WARNING, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
    }
  }

  // Add a check that the commanded and actual positions are close
  // here.
  {
    const double tolerance = 1./60*M_PI/180.0;
    unsigned int numOffSource = 0;
    double maxSeparation = 0.0;
    for (unsigned int i = 0; i < timeSize; i++) {
      const double az1 = cmdAz[i];
      const double el1 = cmdEl[i];
      const double az2 = actAz[2*i];
      const double el2 = actEl[2*i];
      const double thisSeparation = slaDsep(az1, el1, az2, el2);
      if (thisSeparation > tolerance) numOffSource++;
      if (thisSeparation > maxSeparation) maxSeparation = thisSeparation;
    }
    if (numOffSource > 0) {
      ACE_Log_Priority priority = LM_WARNING;
      ostringstream msg;
      msg << "The antenna did not point at the commanded position ";
      if (numOffSource == timeSize) {
        msg << "for any of the measured positions in this sub-scan.";
        priority = LM_ERROR;
      } else {
        msg << "for " << numOffSource << " out of the " 
            << timeSize << " measured positions in this sub-scan.";
      }
      msg << " The maximum disrepency is " << maxSeparation*180/M_PI << " degrees.";
      msg << " The position tolerance is " << tolerance*180/M_PI << " degrees.";
      // TODO. This should be an alarm.
      LOG_TO_AUDIENCE(priority, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
    }
  }
  // Now copy the positions to the HolographySubScanData structure.  Do
  // flagging ie., adding null data, if necessary.
  subscanData->pointingDirection.length(expectedNumTEs);
  subscanData->encoderData.length(expectedNumTEs);
  asdmIDLTypes::IDLAngle cAz;
  asdmIDLTypes::IDLAngle cEl;
  asdmIDLTypes::IDLAngle aAz;
  asdmIDLTypes::IDLAngle aEl;
  ACS::Time expectedTime = startPosTime;
  for (unsigned int i = 0, j = 0, k = 0; i < expectedNumTEs; i++) {
    // We need to check the times are right because if a monitor did not work
    // it will not be published on the notification channel
    if (times.size() > j && times[j] == expectedTime) {
      cAz = asdm::Angle(cmdAz[j]).toIDLAngle();
      cEl = asdm::Angle(cmdEl[j]).toIDLAngle();
      j++; 
      // We only save the measured positions that are on TE boundries. The
      // inter-TE positions are discarded here.
      aAz = asdm::Angle(actAz[k]).toIDLAngle();
      aEl = asdm::Angle(actEl[k]).toIDLAngle();
      k += 2; 
    } else {
      cAz = cEl = aAz = aEl = asdm::Angle(0.0).toIDLAngle();
    }
    PositionData& cmdPosData = subscanData->pointingDirection[i];
    cmdPosData.az = cAz;
    cmdPosData.el = cEl;
    PositionData& actPosData = subscanData->encoderData[i];
    actPosData.az = aAz;
    actPosData.el = aEl;
    expectedTime += TETimeUtil::TE_PERIOD_DURATION.value; 
  }
  {
    ostringstream msg;
    msg << "Returning the data from the sub-scan that started at " << TETimeUtil::toTimeString(startTime_m)
        << " and finished at "<< TETimeUtil::toTimeString(stopTime_m);
    LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
  }

  status_m = Control::AntModeController::READY;
  return subscanData._retn();
}

//--------------------------------------------------------------------
void TowerHolography7mImpl::
prepareForMotionSubscan(const CORBA::Double offset, 
                        const CORBA::Boolean forward,
                        CORBA::Double& width,
                        CORBA::Double& velocity,
                        const CORBA::Double maxVelocity)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);

    if (fabs(offset) > M_PI_2) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ParameterName", "offset");
        ex.addData("Value", offset);
        ex.addData("ValidRange", "-pi/2 to pi/2");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (width <= 0 || width > M_PI) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ParameterName", "width");
        ex.addData("Value", width);
        ex.addData("ValidRange", "0 to pi");
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    if (velocity <= 0 || velocity > maxVelocity) {
        IllegalParameterErrorExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        ex.addData("ParameterName", "velocity");
        ex.addData("Value", velocity);
        ostringstream msg; msg << "0 to " << maxVelocity;
        ex.addData("ValidRange", msg.str().c_str());
        ex.log();
        throw ex.getIllegalParameterErrorEx();
    }

    // Adjust the stroke parameters for a reverse scan
    if (!forward) {
        // Start at the most positive azimuth value and go towards lower values
        width *= -1;
        velocity *= -1;
    }

    // This may throw some exceptions which are not handled here (but they are in
    // the function declaration).
    prepareForSubscan();
}

void TowerHolography7mImpl::prepareForSubscan()
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  if (status_m != Control::AntModeController::READY) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  }
  
  // Check that we do not already have a stroke in progress. This is done by
  // comparing the current time with the last scan stop time. Throw a device
  // busy exception if the user is too hasty.
  const acstime::Epoch timeNow(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
  
  // Adust the current time to be on a TE boundry (if only we could adjust time
  // in real-life). This helps ensure that all the following times are at TE
  // boundries.
  {
    EpochHelper stopTime(stopTime_m);
    if ((stopTime > timeNow) && 
        !(stopTime.value().value == undefinedTime)) {
      DeviceBusyExImpl ex( __FILE__, __LINE__, __PRETTY_FUNCTION__);
      ex.addData("Scan stop time", TETimeUtil::toTimeString(stopTime));
      ex.addData("Time now", TETimeUtil::toTimeString(timeNow));
      ex.log();
      throw ex.getDeviceBusyEx();
    }
  }
  // This starts the flow of data. Its stopped in the getSubscanData method
  // Turn on the high rate position stream
  mount_m->enableMountStatusDataPublication(true);
}

double TowerHolography7mImpl::concludeSubscan(const string& scanDescription)
{
  AUTO_TRACE(__PRETTY_FUNCTION__);

  status_m = Control::AntModeController::OBSERVING;
  // Now tell the Holography receiver when to collect data. This uses the
  // absolute times.
  try {
    hrx7m_m->startSubscan(startTime_m);
    hrx7m_m->stopSubscan(stopTime_m);
    // It would be nice to have all these exceptions handled by one bit of code
    // but, unlike python, this is not possible in C++ (which assumes you will
    // have a common base class for exceptions you want to group together). The
    // alternative is to put these few lines of code into a separate function. I
    // may yet do this.
  } catch (DeviceBusyEx ex) {
    status_m = Control::AntModeController::ERROR;
    HardwareFaultExImpl newEx(ex,  __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getHardwareFaultEx();
  } catch ( ControlDeviceExceptions::HwLifecycleEx ex) {
    status_m = Control::AntModeController::ERROR;
    HardwareFaultExImpl newEx(ex,  __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getHardwareFaultEx();
  } catch (HolographyExceptions::NoSubscanEx ex) {
    status_m = Control::AntModeController::ERROR;
    HardwareFaultExImpl newEx(ex,  __FILE__, __LINE__, __PRETTY_FUNCTION__);
    newEx.log();
    throw newEx.getHardwareFaultEx();
  }
  {
    ostringstream msg;
    msg << "Started a holography " << scanDescription << " sub-scan at "
        << TETimeUtil::toTimeString(startTime_m)
        << " that should complete at " << TETimeUtil::toTimeString(stopTime_m);
    LOG_TO_AUDIENCE(LM_INFO, __PRETTY_FUNCTION__, msg.str(), log_audience::OPERATOR);
  }
  const acstime::Epoch timeNow(TimeUtil::ace2epoch(ACE_OS::gettimeofday()));
  return DurationHelper(EpochHelper(stopTime_m).difference(timeNow)).toSeconds();
}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(TowerHolography7mImpl)
