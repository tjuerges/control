#!/usr/bin/env python

# Either this is a temporary script or it needs to be much
# improved. Its currently needed to move the antenna to the tower and
# tune the receiver. Then the transmitter can then be manually tuned
# and put into the bandpass of the reciever. This needs to be done
# prior to a scheduling block being executed.
#

from optparse import OptionParser
from sys import exit
from math import degrees
from math import radians
parser = OptionParser()

parser.add_option("--az", "--azimuth", type="float", dest="az",
                  metavar='Azimuth',                 
                  help="The azimuth of the holography tower in degrees")

parser.add_option("--el", "--elevation", type="float", dest="el",
                  metavar='Elevation',
                  help="The elevation of the holography tower in degrees")

parser.add_option("-a", "--antenna", dest="antennaName",  metavar='Antenna',
                  help="The antenna to use e.g. DV01. No default")

parser.add_option("-b", "--band", dest="tuningBand",  metavar='TuningBand',
                  default = "High",
                  help="No effect on 7m Holography (left for compatibility)")

parser.add_option('--hwClockOffset', dest='hwClockOffset', metavar="hwClockOffset",
                  type='int',
                  help='hardware clock offset (msec)')

(options, args) = parser.parse_args()

if options.antennaName == None:
    print "An antenna name must be specified, use the \"--antenna\" option"
    exit(-1)

from CCL.TowerHolography7m import TowerHolography7m
try:
    th = TowerHolography7m(options.antennaName)
except:
    print "Error unable to access TowerHolography7m CCL object for " + \
          options.antennaName + "."
    exit(-1)

(az, el) = th.getTowerPosition()
if options.az != None :
    az = radians(options.az)
if options.el != None :
    el = radians(options.el)

if (options.az != None or options.el != None):
    th.setTowerPosition(az,el)
    print "Using specified tower position az = %f el = %f" % \
          (degrees(az), degrees(el))
else:
    print "Using standard tower position az = %f el = %f" % \
          (degrees(az), degrees(el))

th.initializeHardware()

# hardware clock offset (msec)
rx7m = th.getHolographyReceiver()
if options.hwClockOffset != None:
    rx7m.setHwClockOffset(options.hwClockOffset)
print "hwClockOffset =", rx7m.getHwClockOffset()

del(th)
