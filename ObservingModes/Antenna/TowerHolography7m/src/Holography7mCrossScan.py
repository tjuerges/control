#!/usr/bin/env python
# This script is a user interface to doing cross scans using the holography
# receiver.  

import os, time

obs_timestamp = time.strftime("%Y%m%dt%H%M%Sz", time.gmtime())

def renameRawdata():
    log_dir = "Holo7mRawData"
    log_file = "rawdata.txt"

    f_src = os.environ['HOME'] + '/' + log_dir + '/' + log_file
    f_dst = f_src + '.' + obs_timestamp
    if os.access(f_src, os.F_OK):
        os.rename(f_src, f_dst)

def AverageData(data):
    az = []
    el = []
    ss = []
    rr = []

    for i in range(1, len(data.encoderData)):
        az.append(degrees(data.encoderData[i].az.value))
        el.append(degrees(data.encoderData[i].el.value))

        sAvg = data.holoData[(4*i) - 1].ss
        sAvg += data.holoData[(4*i)].ss
        sAvg += data.holoData[(4*i) + 1].ss
        sAvg = data.holoData[(4*i) + 2].ss
        sAvg /= 4.0
        ss.append(sAvg)
        
        rAvg = data.holoData[(4*i) - 1].rr
        rAvg += data.holoData[(4*i)].rr
        rAvg += data.holoData[(4*i) + 1].rr
        rAvg = data.holoData[(4*i) + 2].rr
        rAvg /= 4.0
        rr.append(rAvg)
    return(az, el, ss, rr)


from optparse import OptionParser
from datetime import datetime
from sys import exit
from math import degrees
from math import radians
from Holography7mCrossScanPlotter import *
import pylab as pl

parser = OptionParser()

parser.add_option("--az", "--azimuth", type="float", dest="az",
                  metavar='Azimuth',                 
                  help="The azimuth of the holography tower in degrees")

parser.add_option("--el", "--elevation", type="float", dest="el",
                  metavar='Elevation',
                  help="The elevation of the holography tower in degrees")

parser.add_option("-a", "--antenna", dest="antennaName",  metavar='Antenna',
                  help="The antenna to use e.g. DV01. No default")

parser.add_option("-b", "--band", dest="tuningBand",  metavar='TuningBand',
                  default = "High",
                  help="No effect on 7m Holography (left for compatibility)")

parser.add_option("-s", "--scanType", dest='scanType', metavar='Scan Type',
                  default = "Cross",
                  help='The type of scan to do ("Horiz", "Vert", or "Cross")')
 
parser.add_option("-f", "--file", dest='filename', metavar="Filename",
                  help='The file name to save scans in')

parser.add_option("-c", "--count", dest='repeatCount', metavar="RepeatCount",
                  type="int", default=1,
                  help='The number of scans to do')

parser.add_option("-v", "--velocity", dest='velocity', type='float',
                  default = 100.0,
                  help='The scan velocity (arcsec/sec), default is 100"/sec')

parser.add_option('-l', '--length', type ='float', default = 0.1,
                  dest = 'scanLength', metavar="ScanLength",
                  help='The scan length, default is 0.1 degrees')

parser.add_option('-r', '--reverse', action="store_true", dest = "reverseScan")
parser.add_option('-u', '--unidir', action="store_true", dest = "uniDirection",
                  default = False, help='Unidirectional scan (all same dir)')
parser.add_option('-q', '--quiet', action="store_false", dest = "guiDisplay",
                  default = True, help='Disable GUI display')

parser.add_option('--hwClockOffset', dest='hwClockOffset', metavar="hwClockOffset",
                  type='int',
                  help='hardware clock offset (msec)')

(options, args) = parser.parse_args()

# Get the Tower Holography Mode Controller
if options.antennaName == None:
    print "An antenna name must be specified, use the \"--antenna\" option"
    exit(-1)
from CCL.TowerHolography7m import TowerHolography7m
try:
    th = TowerHolography7m(options.antennaName)
except:
    print "Error unable to access TowerHolography7m CCL object for " + \
          options.antennaName + "."
    exit(-1)

# Modify the velocity paramenter to be in degrees/sec
options.velocity /= 3600.0

# Correctly set the Tower Position
(az, el) = th.getTowerPosition()
if options.az != None :
    az = radians(options.az)
if options.el != None :
    el = radians(options.el)
if (options.az != None or options.el != None):
    th.setTowerPosition(az,el)

th.initializeHardware()

# hardware clock offset (msec)
rx7m = th.getHolographyReceiver()
if options.hwClockOffset != None:
    rx7m.setHwClockOffset(options.hwClockOffset)
print "hwClockOffset =", rx7m.getHwClockOffset()

frequency = th.getFrequency()

plotter = HoloCrossScanPlot()
plotter.configurePlot(datetime.utcnow().isoformat(),
                      options.antennaName,
                      options.scanType,
                      options.velocity,
                      options.scanLength,
                      options.repeatCount,
                      frequency,
                      az,
                      el)
if options.guiDisplay:
    plotter.initDisplay()
    plotter.draw()
else:
    plotter.printExecInfo()

# Now iterate over the repeat count
for scanNumber in range(options.repeatCount):
    scan = HoloCrossScan()

    if options.uniDirection:
        forwardDirection = True
    else:
        forwardDirection = scanNumber % 2 == 0
    if options.reverseScan:
        forwardDirection = not forwardDirection

    if options.scanType.lower() != "vert":
        timeout = th.startHorizontalSubscan(0.0, forward=forwardDirection,
                                            width=radians(options.scanLength),
                                            velocity=radians(options.velocity))
        azData = th.getSubscanData(timeout+2)
        scan.setHorizStroke(HoloCrossStroke(dataTuple=AverageData(azData)))
        

    if options.scanType.lower() != "horiz":
        timeout = th.startVerticalSubscan(0.0, forward=forwardDirection,
                                          width=radians(options.scanLength),
                                          velocity=radians(options.velocity))
        elData = th.getSubscanData(timeout+2)
        scan.setVertStroke(HoloCrossStroke(dataTuple=AverageData(elData)))

    plotter.addScan(scan)
    if options.guiDisplay:
        plotter.draw()
    else:
        scan.printResults(scanNumber-1)

del(th)

renameRawdata()

if options.filename != None:
    plotter.saveToFile(options.filename)

plotter.printResults()

if options.guiDisplay:
    plotter.show()

