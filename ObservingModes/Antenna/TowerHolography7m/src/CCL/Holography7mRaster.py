#!/usr/bin/env python
# This class produces a model of the raster scan used to do Tower Holography
# Note here I follow Robert in defining resoultion as:
#    effective pixel size on the map; the actual resolution will be
#    worse due to tapering in data reduction, and the map is also
#    calculated with more pixels in the data reduction (for display
#    and panel fitting purposes).

from math import pi
from math import ceil

class Holography7mRaster:

    ## Holography Raster Constructor
    #
    # \param resolution: The effective pixel size on the map (in meters).
    #                    The actual resolution will be worse due to tapering
    #                    in data reduction, and the map is also calculated
    #                    with more pixels in the data reduction (for display
    #                    and panel fitting purposes).
    #     default: 0.066667 m
    # \param dishDiameter: The size of the dish (in meters)
    #     default: 12 m
    # \param frequency: The operating frequency of the holography system (Hz)
    #     default: 96.155E9
    def __init__(self, resolution=0.066667, dishDiameter = 12.0,
                 frequency = 96.155E9):
        self.__resolution = resolution
        self.__dishDiameter = dishDiameter
        self.__frequency = frequency

        self.nRow       = ceil(dishDiameter / resolution)
        self.rowSpacing = 25.0*(pi/648000.0) * (104.02E9/frequency)
        self.strokeLength = (self.nRow-1) * self.rowSpacing


    ## printRasterInfo
    #
    #  Method will print to stdout information about this particular raster
    #  map
    def printRasterInfo(self):
        print "Raster Map Info:"
        print "\tResolution:     %5.3e m" % self.__resolution
        print "\tDish Diameter:  %2.1f m" % self.__dishDiameter
        print "\tFrequency:      %3.3e Hz" % self.__frequency
        print "\tNumber of Rows: %d" % self.nRow
        print "\tRow Spacing:    %3.3e rad (%4.2f\")" % (self.rowSpacing,
                                               self.rowSpacing*(648000.0/pi))
        print "\tStroke Length:  %3.3e rad (%f4.2\')" % (self.strokeLength,
                                               self.strokeLength*(10800.0/pi))

    ## getStrokeInfo
    #
    # Return the information for a particular stroke.
    # 
    # \param strokeNumber: strokes are numbered from 0 through nRow-1 starting     #                      at the lowest elevation.  All strokes are constant
    #                      El.
    # \return ((azOffset, elOffset), (azLength, elLength)),
    #          az and el offset are the starting point of the stroke
    #          az and el length and the length of the stroke in the respective
    #          direction
    def getStrokeInfo(self, strokeNumber):
        elOffset = (strokeNumber - self.nRow/2.0 + 0.5)*self.rowSpacing
        if (strokeNumber % 2):
            azOffset = 0.5 * self.strokeLength
            direction = False
        else:
            azOffset =  -0.5 * self.strokeLength
            direction = True
        return ((azOffset,elOffset),(self.strokeLength,0),direction)
    


if __name__ == "__main__":

    def printStrokeInfo(strokeNumber,strokeInfo):
        myStr = ("Stroke Number %3d starts at (%4.2f\',%4.2f\')" + \
        " and has length (%4.2f\',%4.2f\') and moves") % \
        (strokeNumber,
         strokeInfo[0][0]*(10800.0/pi), strokeInfo[0][1]*(10800.0/pi),
         strokeInfo[1][0]*(10800.0/pi),strokeInfo[1][1]*(10800.0/pi))
        if (strokeInfo[2]):
            myStr += " Forward"
        else:
            myStr += " Backward"
        print myStr
        
    holoRaster = Holography7mRaster()
    holoRaster.printRasterInfo()
    
    printStrokeInfo(0,holoRaster.getStrokeInfo(0))
    printStrokeInfo(89,holoRaster.getStrokeInfo(89))
    printStrokeInfo(90,holoRaster.getStrokeInfo(90))
    printStrokeInfo(179,holoRaster.getStrokeInfo(179))
