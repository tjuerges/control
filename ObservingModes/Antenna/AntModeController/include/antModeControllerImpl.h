#ifndef ANTMODECONTROLLERIMPL_H
#define ANTMODECONTROLLERIMPL_H
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2005, 2006, 2007, 2008, 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Base class(es)
#include <acscomponentImpl.h>

//CORBA servant header
#include <AntModeControllerS.h>

// Forward declarations for classes that this component uses
class maci::ContainerServices;

//includes for data members
#include <string>
#include <ace/Thread_Mutex.h>
#include <acsComponentSmartPtr.h> // for maci::SmartPtr

// Forward declarations for classes that this component uses
namespace Control {
    class Antenna;
}

namespace Control { 
    class AntModeControllerImpl: 
        public virtual POA_Control::AntModeController,
        public acscomponent::ACSComponentImpl
    {
    public:
        ///
        /// The constructor for any ACS C++ component must have this signature.
        ///
        AntModeControllerImpl(const ACE_CString& name,
                              maci::ContainerServices* cs);

        ///
        /// The destructor ensures that the references to the hardware
        /// components are released.
        ///
        virtual ~AntModeControllerImpl();
    
        ///
        /// This component lifecycle method ensures that the references to the
        /// hardware components have been released.
        ///
        virtual void cleanUp();
    
        ///
        /// See the IDL file for a description of this function.
        ///
        virtual Control::AntModeController::Status getStatus();

        ///
        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// \exception ModeControllerExceptions::BadConfigurationEx
        ///
        virtual void allocate(const char* antennaName);

        ///
        /// See the IDL file for a description of this function.
        ///
        virtual void allocateAsynch(const char* antennaName,
                                    Control::AntModeControllerCB* cb);

        virtual void deallocate();

        ///
        /// See the IDL file for a description of this function.
        ///
        virtual void deallocateAsynch(Control::AntModeControllerCB* cb);

        //
        // See the IDL file for a description of this function.
        //
        char* getAllocatedAntennaName();

    protected:
        /// @fn void acquireReferences 
        /// This method should configure this component to use the equipment in
        /// the specified antenna. The supplied argument is an antenna name
        /// like "DV01" and the associated antenna component must already be
        /// started (by the master component). This function should then start
        /// any controllers it needs and query the antenna component to get the
        /// references the hardware components it needs. It This function can
        /// be called twice and if so it should release the equipment in the
        /// antenna it was previously using and acquire the equipment in the
        /// new antenna.  Although this is a pure virtual function there is an
        /// implementation in this base class that should be called at the
        /// *end* of the corresponding function in the derived class. It just
        /// sets the state (to initialized) and the antenna name (to the
        /// specificed value). This base class function should not be called if
        /// there were any problems in aquiring all the necesssary references.
        /// @param AntennaName Name of antenna component to use e.g., "DV01" A
        /// CannotGetAntenna exception should be generated if the the specified
        /// antenna component cannot be contacted A BadConfiguration exception
        /// should generated if the antenna does not have references to the
        /// necessary hardware.
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// \exception ModeControllerExceptions::BadConfigurationEx
        virtual void acquireReferences(const std::string& antennaName) = 0;

        /// @fn void releaseReferences 

        /// Restore this component to the state it was in after
        /// construction. In this state this component does not have a
        /// reference to the mount component, the positionConsumer is shutdown
        /// and none of its functions, except allocate & getState should be
        /// used.  Although this is a pure virtual function there is an
        /// implementation in this base class that should be called at the
        /// *end* of the corresponding function in the derived class. It just
        /// sets the state (to uninitialized) and the antenna name (to an empty
        /// string).
        virtual void releaseReferences() = 0;
 
        // The antenna status is used by specific mode controllers i.e.,
        // derived classes, to more easily determine where it is in its
        // lifecycle and what it is doing.
        Control::AntModeController::Status status_m;
    
        // This returns the name of the currently allocated antenna e.g.,
        // DV01. It is zero length if no antenna has been allocated i.e., the
        // status is UNINITIALIZED.
        std::string getAntennaName();

        // This is a non-sticky reference to the antenna that the antenna
        // component the device is currently assigned to.  If the antenna 
        // is unassigned this should be a nil reference.
        maci::SmartPtr<Control::Antenna> allocatedAntenna_m;


    private:
        // The copy constructor is made private to prevent a compiler generated
        // one from being used. It is not implemented.
        AntModeControllerImpl(const AntModeControllerImpl& other);

        // The name of the currently allocated antenna e.g., DV01. It is zero
        // length if no antenna has been allocated.
        std::string antennaName_m;

        // This mutex serialises access to the allocate and deallocate
        // functions. It ensures that these functions are run to completion
        // before any other client can run them.
        ACE_Thread_Mutex resourceMutex_m;

    };
} // end Control namespace
#endif // AntModeControllerImpl_H
