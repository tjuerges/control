#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#

class AntModeController:
    """Base class for CCL Mode Controllers.
    """

    def __init__(self):
       """Constructor.
       """
       self.interrupted = False

    def interrupt(self):
       """Interrupts the mode controller.
       This function should be overriden by an asynchronous operation in
       child classes.
       """
       self.interrupted = True

    def isInterrupted(self):
       """Has this object been interrupted?
       If needed, this method can be overriden by child classes.
       """
       return self.interrupted

#
# __oOo__
