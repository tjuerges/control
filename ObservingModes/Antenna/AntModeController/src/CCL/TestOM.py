from ScriptExec.rm import getComponent
from ScriptExec.rm import getDynamicComponent
from ScriptExec.rm import getLogger
from CCL.ObservingMode import ObservingMode

class TestOM(ObservingMode):

    def __init__(self):
        ObservingMode.__init__(self)
        self.logger = getLogger()

    def interrupt(self):
        self.logger.logInfo("Don't interrupt me when I'm interrupting!")

#
# __oOo__
