from ScriptExec.rm import getComponent
from ScriptExec.rm import getDynamicComponent
from ScriptExec.rm import getLogger
from CCL.AntModeController import AntModeController

class TestMC(AntModeController):

    def __init__(self):
        AntModeController.__init__(self)
        self.logger = getLogger()

    def interrupt(self):
        self.logger.logInfo("Oh my God, I have been interrupted!")

#
# __oOo__
