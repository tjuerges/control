/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.ObservingModes;

import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import alma.ACSErr.Completion;
import alma.Control.AntModeControllerCB;
import alma.Control.AntModeControllerCBHelper;
import alma.Control.AntModeControllerCBPOA;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.container.ContainerServices;

/**
 * Insert a Class/Interface comment.
 * 
 */
public class AntModeControllerCBImpl extends AntModeControllerCBPOA {
	
	final private int MAX_EXPECTED_RESPONSES = 128;

	private ContainerServices containerServices;

	private AntModeControllerCB externalInterface = null;

	private Semaphore semaphore;

	private int pendingResponses;

	private HashMap<String, Completion> completionStatus;
	
	protected Logger logger;

	public AntModeControllerCBImpl(ContainerServices cs)
			throws AcsJContainerServicesEx {
		containerServices = cs;

		completionStatus = new HashMap<String, Completion>();
		semaphore = new Semaphore(MAX_EXPECTED_RESPONSES);
		semaphore.drainPermits();
		pendingResponses = 0;
		externalInterface = 
			AntModeControllerCBHelper.narrow(containerServices.activateOffShoot(this));
		logger = cs.getLogger();
	}

	/* -------------- CORBA Interface ------------------ */
	/* This is a one-way void call so we cannot throw exceptions*/
	public void report(String antennaId, Completion status) {

		if (completionStatus.containsKey(antennaId)) {
			completionStatus.put(antennaId, status);
			semaphore.release();
		} else {
			// Value Reported that was not expected
			// Log the error
		}
	}

	/* --------------- Java Interface ----------------- */
    // TODO.
    // 0. Move this to the AntModeController module
    // 1. Fix up the indentation
    // 2. Fix up the error handling
    // 3. Understand why we have a java interface instead of making 
    // the addExpectedResponse & waitForCompletion functions CORBA ones.

	public AntModeControllerCB getExternalInterface() {
		return this.externalInterface;
	}

	public void addExpectedResponse(String antennaId) {
		pendingResponses++;
		// TODO. Perhaps just check if this antennaId is already in the map.
		if (pendingResponses > MAX_EXPECTED_RESPONSES) {
			// Too many requests, throw an exception
		}
		completionStatus.put(antennaId, null);
	}

	// Note that the wait function deactivates the offshoot so this cannot
	// be used again.
	public HashMap<String, Completion> waitForCompletion(long timeOut) {
            try {
		boolean status;
		status = semaphore.tryAcquire(pendingResponses, timeOut, TimeUnit.SECONDS);
		
		if (!status)
                    logger.fine("Mode controller operation timeout.");
		
                containerServices.deactivateOffShoot(this);
            } catch (InterruptedException ex) {
                // TODO. 
                // 1. Log that we were interrupted while waiting for
                // semaphores 
                // 2. Set all the semaphores that have not
                // returned to something
            } catch  (AcsJContainerServicesEx ex) {
                // Log the fact that we had trouble deactivating the offshoot
            }
            return completionStatus;
	}
}
