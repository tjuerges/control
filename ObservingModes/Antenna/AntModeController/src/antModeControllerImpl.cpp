// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006, 2007, 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include "antModeControllerImpl.h"

#include <ModeControllerExceptions.h>
#include <string> // for string
#include <ACSErrTypeOK.h>
#include <Time_Value.h> // for ACE_Time_Value
#include <AntennaC.h>
#include <sstream> // for std::ostringstream

using std::string;
using std::ostringstream;
using Control::AntModeControllerImpl;
using Logging::BaseLog;
using ModeControllerExceptions::CannotGetAntennaEx;
using ModeControllerExceptions::CannotGetAntennaExImpl;
using ModeControllerExceptions::CannotGetAntennaCompletion;
using ModeControllerExceptions::BadConfigurationEx;
using ModeControllerExceptions::BadConfigurationExImpl;
using ModeControllerExceptions::BadConfigurationCompletion;

AntModeControllerImpl::
AntModeControllerImpl(const ACE_CString& name,
		      maci::ContainerServices* containerServices):
    ACSComponentImpl(name, containerServices),
    status_m(Control::AntModeController::UNINITIALIZED),
    allocatedAntenna_m(),
    antennaName_m(""),
    resourceMutex_m()
{
    const string fnName = "AntModeControllerImpl::AntModeControllerImpl";
    AUTO_TRACE(fnName);
}

AntModeControllerImpl::~AntModeControllerImpl() {
    const string fnName = "AntModeControllerImpl::~AntModeControllerImpl";
    AUTO_TRACE(fnName);
    // We cannot call the virtual function because, at this point, the derived
    // class has already been destroyed.
    // Normally references should have already been released by the cleanUp
    // function but the cleanup function may not be called if an exception,
    // that results in the destruction of this obect, is being handled.
    //  This is here as documntation. remove after a the release.
    //  2011-03-30
    //    AntModeControllerImpl::releaseReferences();
}

void AntModeControllerImpl::cleanUp() {
    const string fnName = "AntModeControllerImpl::cleanUp";
    AUTO_TRACE(fnName);
    releaseReferences();
    acscomponent::ACSComponentImpl::cleanUp();
}

void AntModeControllerImpl::acquireReferences(const string& antennaName) {
    const string fnName = "AntModeControllerImpl::acquireReferences";
    AUTO_TRACE(fnName);

    // Remove any old references (in case this function is executed twice).
    // But only do this if necessary.
    if (antennaName != getAntennaName() && getAntennaName()!="") {
        releaseReferences();
    }

    // If the antenna is already initialized it means we already have the
    // references for the correct antenna. So there is nothing left to do.
    if (getStatus() != Control::AntModeController::UNINITIALIZED) return;
    
    const string antCompName = "CONTROL/" + antennaName;
    try {
        allocatedAntenna_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::Antenna>
            (antCompName.c_str());
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        ostringstream msg;
        msg << "Cannot get a reference to the '" << antennaName
            << "' antenna component." << endl
            << "Possible causes include:" << endl
            << "1. You specified an incorrect name" << endl
            << "2. The CDB is incorrect" << endl
            << "3. The connection to the ABM is faulty";
        ModeControllerExceptions::CannotGetAntennaExImpl
            newEx(ex, __FILE__, __LINE__, fnName.c_str());
        newEx.addData("Detail", msg.str());
        newEx.log();
        throw newEx.getCannotGetAntennaEx();
    }

    {  // This is only for debugging.
        Control::DeviceNameList_var subDevices = 
            allocatedAntenna_m->getSubdeviceList();
        ostringstream msg;
        msg << "Configuring the mode controller. ";
        msg << "Available devices in antenna "<< antennaName << " are:";
        LOG_TO_AUDIENCE(LM_DEBUG, fnName, msg.str(), log_audience::DEVELOPER);
        for (unsigned int i = 0; i < subDevices->length(); i++) {
            ostringstream msg1;
            msg1 << "Reference name: " << (*subDevices)[i].ReferenceName
                 << " Full name: " << (*subDevices)[i].FullName;
            LOG_TO_AUDIENCE(LM_DEBUG, fnName, msg1.str(), 
                            log_audience::DEVELOPER); 
        }
    } 

    // Set the antenna name and state.
    antennaName_m = antennaName;
}

void AntModeControllerImpl::releaseReferences() {
    const string fnName = "AntModeControllerImpl::releaseReferences";
    AUTO_TRACE(fnName);
    antennaName_m = "";

    allocatedAntenna_m.release();
    status_m = Control::AntModeController::UNINITIALIZED;
}

Control::AntModeController::Status AntModeControllerImpl::getStatus() {
    const string fnName = "AntModeControllerImpl::getStatus";
    AUTO_TRACE(fnName);
    return status_m;
}

void AntModeControllerImpl::allocate(const char* antennaName) {
    const string fnName("AntModeControllerImpl::allocate");
    AUTO_TRACE(fnName);
    // Use a mutex so that this function or the deallocate function can only be
    // run by one client at a time. The second client blocks until the first is
    // completed.
    {
        // To avoid deadlocks I use a timed acquire. If it fails an exception
        // is thrown and if it works the guard takes over the mutex. The mutex
        // is not released, even momentarily, until the guard is destroyed.
        ACE_Time_Value timeoutEndPoint;
        timeoutEndPoint.set(60.0); // 60 seconds.
        timeoutEndPoint += ACE_OS::gettimeofday();

        if (resourceMutex_m.acquire(timeoutEndPoint) == -1) {
            string msg("Cannot acquire the mutex that prevents");
            msg += " simultaneous access to the " + fnName + " function.";
            msg += " Perhaps there is a hung GUI or CCL (python) interpreter.";
            msg += " Please shutdown unused clients and try again.";
            CannotGetAntennaExImpl newEx(__FILE__, __LINE__, fnName.c_str());
            newEx.addData("Detail", msg);
            newEx.log();
            throw newEx.getCannotGetAntennaEx();
        }
    }
    ACE_Guard<ACE_Thread_Mutex> guard(resourceMutex_m, true, true);
    acquireReferences(string(antennaName));
    status_m = Control::AntModeController::ALLOCATED;  
}

void AntModeControllerImpl::allocateAsynch(const char* antennaName,
					   Control::AntModeControllerCB* cb) {
    const string fnName = "AntModeControllerImpl::allocateAsynch";
    AUTO_TRACE(fnName);
    try {
        allocate(antennaName);
    } catch (CannotGetAntennaEx& ex) {

        CannotGetAntennaCompletion completion(ex, __FILE__, __LINE__, 
                                              fnName.c_str());
        cb->report(getAntennaName().c_str(), completion);
        return;
    } catch (BadConfigurationEx& ex) {
        BadConfigurationCompletion completion(ex, __FILE__, __LINE__, 
                                              fnName.c_str());
        cb->report(getAntennaName().c_str(), completion);
        return;
    }

    // Success: Report success through callback
    ACSErrTypeOK::ACSErrOKCompletion okComplete;
    cb->report(getAntennaName().c_str(), okComplete);
}

void AntModeControllerImpl::deallocate() {
    const string fnName("AntModeControllerImpl::deallocate");
    AUTO_TRACE(fnName);
    // Use a mutex so that this function or the allocate function can only be
    // run by one client at a time. The second client blocks until the first is
    // completed.
    {
        // To avoid deadlocks I use a timed acquire. If it fails an warning is
        // logged and the function terminates without releasing any references
        // and if it works the guard takes over the mutex. The mutex is not
        // release, even momentarily, until the guard is destroyed.
        ACE_Time_Value timeoutEndPoint;
        timeoutEndPoint.set(60.0); // 60 seconds.
        timeoutEndPoint += ACE_OS::gettimeofday();

        if (resourceMutex_m.acquire(timeoutEndPoint) == -1) {
            string msg("Cannot acquire the mutex that prevents");
            msg += " simultaneous access to the " + fnName + " function.";
            msg += " Perhaps there is a hung GUI or CCL (python) interpreter.";
            msg += " Ignoring this error (and not doing anything).";
            getLogger()->log(BaseLog::LM_WARNING, msg, __FILE__, __LINE__, 
                             fnName);
            return;
        }
    }
    ACE_Guard<ACE_Thread_Mutex> guard(resourceMutex_m, true, true);
    releaseReferences();
}

void AntModeControllerImpl::deallocateAsynch(Control::AntModeControllerCB* cb){
    const string fnName("AntModeControllerImpl::deallocateAsync");
    AUTO_TRACE(fnName);

    deallocate();
    //Report completion through callback.
    ACSErrTypeOK::ACSErrOKCompletion okComplete;
    cb->report(getAntennaName().c_str(), okComplete);
}

string AntModeControllerImpl::getAntennaName() {
    return antennaName_m;
}

char* AntModeControllerImpl::getAllocatedAntennaName() {
    return CORBA::string_dup(antennaName_m.c_str());
}
