#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
Defines the TotalPower class.
"""

#import AcsutilPy.ACSImport
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import ModeControllerExceptions

class TotalPower:
    '''
    The TotalPower object is used to perfom total power
    observations of astronomical sources.  This object 
    is the Antenna level object providing precise control
    of the behavior of a single antenna.

    Often this object will be recieved from the TotalPowerObservingMode
    and will not need to be instanciated from the CCL directly,
    although a constructor is provided to allow creation
    (for whatever reason)
    
    EXAMPLE:
    # Load the necessary defintions
    from TotalPower import *
    # create the object
    tp = TotalPower("DV01")
   
    # stow the antenna
    tp.shutdownHardware();
    # destroy the component
    del(tp)
    '''

    def __init__(self, antennaName=None, componentName=None):
        '''
        The constructor creates a TotalPower object.

        If the antennaName is defined then a dynamic component that implements
        the functions available in the TotalPower object is created.
        Theconstructor also establishes the connection between this
        dynamic component and the relevant hardware components(MountController
        and AntLOController) in the specified antenna.

        If the componentName is not none, then the object uses that component
        to implement the functions available in TotalPower.
        It is assumed that the component was allocated by other means and
        references to the relavent hardware already exist.  This method is
        intended for internal use, and should only be used by experts.
        
        Exceptions are thrown if there is a problem creating or connecting
        to this component, establishing the connection to the previously
        mentioned hardware components, or if either both or neither
        antennaName and componentName are specified.

        EXAMPLE:
        # Load the necessary defintions
        from TotalPower import *
        # create the object
        tp = TotalPower("DV01")
        # destroy the component
        del(tp)
        '''
        if not((antennaName == None) ^ (componentName == None)):
            raise;
        
        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
	self.__client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
        if componentName != None:
            # Specified previously existing component, just get reference
            self.__tp = self.__client.getComponent(componentName)
            if (self.__tp == None):
                return None
            
        if antennaName != None:
            # Specified Antenna name, generate and allocate a new
            # dynamic component
            self.__tp = self.__client.getDynamicComponent \
                     (None, 'IDL:alma/Control/TotalPower:1.0',
                      None, None)
            if (self.__tp == None):
                return None
            try:
                self.__tp.allocate(antennaName);
            except (ModeControllerExceptions.CannotGetAntennaEx, \
                    ModeControllerExceptions.BadConfigurationEx), e:
                self.__tp.deallocate();
                raise;

    def __del__(self):
        '''
	The destructor releases the component, if the component is shutdown
        all references are released.

	EXAMPLE:
	See the example for the constructor (__init__ function)
	'''
        self.__client.releaseComponent(self.__tp._get_name())
        self.__client.disconnect()


    def initializeHardware(self):
        '''
        Initialize the relevant equipment in the antenna for use in
        TotalPower. This means:
        1. Put the mount into track mode


        A MountFault exception is generated if there is any problem
        initializing or using the mount or mount controller. This
        includes hardware faults.  A HardwareFault exception is
        generated if there is any problem initializing the other hardware.

        EXAMPLE:
        # Load the necessary defintions
        import TotalPower
        # create the object
        tp = TotalPower.TotalPower("DA41")
        # Get the hardware ready
        tp.initializeHardware()
        # Stow the antenna
        tp.shutdownHardware()
        # destroy the component
        del(tp)
        '''
        return self.__tp.initializeHardware();

    def shutdownHardware(self):
        '''
        This function should be used at the end of an observation.
        It puts the mount in its survival stow position and then
        puts it in shutdown mode. A MountFault exception is
        generated if there is any problem using the mount or mount
        controller components and this includes hardware faults.

	EXAMPLE:
	See the example for the initializeHardware function.
        '''
        return self.__tp.shutdownHardware();

    def getMountController(self):
        '''
        Return the mount controller object used by this observing
        mode.  This will allow you access to many detailed aspects of
        the antenna position. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake Total
        Power  observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name. A zero lenth string
        is returned if this object does not have a reference to the
        mount controller object.
        '''
        componentName = self.__tp.getMountController();
        return componentName;
    # Just return the name until I can get the python facade class written 
    #        return MountController(componentName);

    def getAntennaLOController(self):
        '''
        Return the Antenna LO Controller used by this antenna controller
        mode.  This will allow you access to many detailed aspects of
        the LO Chain. However it should only be used by people who
        understand how this object interacts with this observing
        mode. It is made available so that experts have complete control
        over the system.

        EXAMPLE:
        # Load the necessary defintions
        import TotalPower
        # create the object
        tp = TotalPower.TotalPower("DA41")
        # Get the hardware ready
        tp.initializeHardware()
        # Get the LO Controller
        LoController = tp.getAntennaLOController()
        # destroy the component
        del(tp)
        '''
        cn = self.__tp.getAntennaLOController();
        return AntennaLOController.AntennaLOController(componentName = cn);

