// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <TotalPowerImpl.h>
#include <ACSErrTypeOK.h>

#include <ControlExceptions.h>
#include <ControlDeviceExceptions.h>
#include <ModeControllerExceptions.h>
#include <loggingMACROS.h> // for AUTO_TRACE & LOG_TO_AUDIENCE
#include <AntennaC.h> // for Control::Antenna
#include <IFProcC.h> // for Control::IFProc
#include <sstream> // for std::ostringstream
#include <vector> // for std::vector

using Control::TotalPowerImpl;
using std::string; 
using std::vector; 
using std::ostringstream;
using std::endl;
using ControlExceptions::IllegalParameterErrorEx;
using ModeControllerExceptions::BadConfigurationExImpl;
using ModeControllerExceptions::UnallocatedEx;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::UnallocatedCompletion;
using ModeControllerExceptions::HardwareFaultExImpl;
using ModeControllerExceptions::HardwareFaultEx;
using ModeControllerExceptions::HardwareFaultCompletion;
using ControlDeviceExceptions::IllegalConfigurationEx;
using ControlDeviceExceptions::IllegalConfigurationExImpl;
using log_audience::OPERATOR;
using log_audience::DEVELOPER;

//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
TotalPowerImpl::TotalPowerImpl(const ACE_CString& name,
                               maci::ContainerServices* containerServices) :
    AntModeControllerImpl(name, containerServices)
{
    AUTO_TRACE(__func__);
}

TotalPowerImpl::~TotalPowerImpl() {
    AUTO_TRACE(__func__);
    releaseReferences();
}

//-----------------------------------------------------------------------------
// Lifecycle Methods
//-----------------------------------------------------------------------------

void TotalPowerImpl::cleanUp() {
    AUTO_TRACE(__func__);
    // Do anything needed by the base class. This includes releasing references
    AntModeControllerImpl::cleanUp();
}

//-----------------------------------------------------------------------------
// Mode Controller Methods
//-----------------------------------------------------------------------------

void TotalPowerImpl::acquireReferences(const string& antennaName) {
    AUTO_TRACE(__func__);

    // Remove any old references (in case this function is executed twice).
    // But only do this if necessary.
    if (antennaName != getAntennaName()) releaseReferences();

    // If the antenna is already initialized it means we already have the
    // references for the correct antenna. So there is nothing left to do.
    if (getStatus() != Control::AntModeController::UNINITIALIZED) return;
    
    // This gets a reference to the antenna component
    AntModeControllerImpl::acquireReferences(antennaName);
  
    // Get references to the antenna-based LO components
    string currentComponent("IFProc0");
    try {
        CORBA::String_var deviceName;  
        // IFproc0
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        ifproc0_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::IFProc>(deviceName);
        // IFProc1
        currentComponent = "IFProc1";
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        ifproc1_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::IFProc>(deviceName);
    } catch (IllegalParameterErrorEx ex) {
        releaseReferences();
        string msg = "Could not find a " + currentComponent + " component.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        releaseReferences();
        string msg = currentComponent + " reference is null.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__,__LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    }
}

void TotalPowerImpl::releaseReferences() {
    AUTO_TRACE(__func__);

    // Shutdown any data transmission (in case it has not already been done)
    if (getStatus() != Control::AntModeController::UNINITIALIZED) {
        endDataAcquisition();
        setDataConsumer(NULL);
    }
    // Release our references to the IFProcessors
    ifproc0_m.release();
    ifproc1_m.release();

    // The derived class must call the base class releaseReferences method to
    // change state and unset the antenna name.
    AntModeControllerImpl::releaseReferences();
}

void TotalPowerImpl::setDataConsumer(const char* tppName) {
    AUTO_TRACE(__func__);
    bool allocating(true);
    try {
        // tell both the IFProcessors about the new data consumer
        checkRefsAreOK(__FILE__, __LINE__, __func__);
        if (tppName==NULL) {
            allocating = false;
            ifproc0_m->deallocate();
            ifproc1_m->deallocate();
        } else {
            ifproc0_m->allocate(tppName);
            ifproc1_m->allocate(tppName);
        }
    } catch (IllegalConfigurationEx& ex) {
        string msg("Cannot");
        msg += allocating ? " specify": " undefine";
        msg += " the total-power processor used to consume the data for";
        msg += " antenna " + getAntennaName();
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        if (allocating) throw newEx.getHardwareFaultEx();
    }
}

void TotalPowerImpl::setDataConsumerAsynch(const char* tppName, Control::AntennaCallback* cb)
{
    ACSErr::CompletionImpl c = ACSErrTypeOK::ACSErrOKCompletion();
    try {
        setDataConsumer(tppName);
    } catch (const UnallocatedEx &ex) {
        UnallocatedCompletion hc(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        c = hc;
    } catch (const HardwareFaultEx &ex) {
        HardwareFaultCompletion hc(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        c =hc;
    } catch (...) {
        HardwareFaultCompletion hc(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        hc.addData("Detail", "Unkown error");
        c =hc;
    }

    try {

        cb->report(getAntennaName().c_str(), c);

    } catch(...) {
        ostringstream msg;
        msg << "Could not report on the callback ";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str())
    }
}

void TotalPowerImpl::beginDataAcquisition() {
    AUTO_TRACE(__func__);
    // tell both the IFProcessors to start sending data
    try {
        checkRefsAreOK(__FILE__, __LINE__, __func__);
        ifproc0_m->beginDataAcquisition(0);
        ifproc1_m->beginDataAcquisition(0);
    } catch (IllegalConfigurationEx& ex) {
        string msg = "Cannot start the transmission of total-power data from";
        msg += " antenna " + getAntennaName();
        msg += getAntennaName();
        HardwareFaultExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getHardwareFaultEx();
    }
}

void TotalPowerImpl::beginDataAcquisitionAsynch(Control::AntennaCallback* cb)
{
    ACSErr::CompletionImpl c = ACSErrTypeOK::ACSErrOKCompletion();
    try {
        beginDataAcquisition();
    } catch (const UnallocatedEx &ex) {
        UnallocatedCompletion hc(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        c = hc;
    } catch (const HardwareFaultEx &ex) {
        HardwareFaultCompletion hc(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        c =hc;
    } catch (...) {
        HardwareFaultCompletion hc(__FILE__, __LINE__, __PRETTY_FUNCTION__);
        hc.addData("Detail", "Unkown error");
        c =hc;
    }

    try {

        cb->report(getAntennaName().c_str(), c);

    } catch(...) {
        ostringstream msg;
        msg << "Could not report on the callback ";
        LOG_TO_DEVELOPER(LM_WARNING, msg.str())
    }

}

void TotalPowerImpl::endDataAcquisition() {
    AUTO_TRACE(__func__);
    checkRefsAreOK(__FILE__, __LINE__, __func__);
    // TODO. The IFProc should not throw an exception if we ask it to
    // abortDataAcquisition when it is already aborted.
    try {
        ifproc0_m->abortDataAcquisition();
    } catch (IllegalConfigurationEx& ex) {
        string msg("Problem shutting down the flow of total-power data from IFProc 0 on");
        msg += " antenna " + getAntennaName() + ". Ignoring this problem.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.log();
    } catch (UnallocatedEx& ex){ 
        string msg("Problem shutting down the flow of total-power data from");
        msg += " antenna " + getAntennaName() + ". Ignoring this problem.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
    }
    try {
        ifproc1_m->abortDataAcquisition();
    } catch (IllegalConfigurationEx& ex) {
        string msg("Problem shutting down the flow of total-power data from IFProc 1 on");
        msg += " antenna " + getAntennaName() + ". Ignoring this problem.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
        IllegalConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.log();
    } catch (UnallocatedEx& ex){ 
        string msg("Problem shutting down the flow of total-power data from");
        msg += " antenna " + getAntennaName() + ". Ignoring this problem.";
        LOG_TO_AUDIENCE(LM_WARNING, __func__, msg, OPERATOR);
    }
}

void TotalPowerImpl::
checkRefsAreOK(const char* fileName, int lineNumber, const char* functionName){
    if (status_m == Control::AntModeController::UNINITIALIZED) {
        string msg = "Total Power Mode controller has not been allocated";
        msg += " to an antenna";
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getUnallocatedEx();
    } 

    if (ifproc0_m.isNil()) {
        string msg = "IF Processor (pol 0) reference is null. Has this";
        msg += " mode controller been allocated to an antenna?";
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getUnallocatedEx();
    }

    if (ifproc1_m.isNil()) {
        string msg = "IF Processor (pol 1) reference is null. Has this";
        msg += " mode controller been allocated to an antenna?";
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg);
        ex.log();
        throw ex.getUnallocatedEx();
    }

    // So we think we have a reference to the IF processors, but lets checks
    // its really there. Because we have a non-sticky reference they could be
    // released by the master.
    int p = 0;
    try {
        vector<Control::HardwareDevice::HwState> state(2);
        state[p] = ifproc0_m->getHwState();
        p++;
        state[p] = ifproc1_m->getHwState();
        while (p >= 0) {
            if (state[p] == Control::HardwareDevice::Operational ||
                state[p] == Control::HardwareDevice::Simulation) {
                p--;
            } else {
                ostringstream msg;
                msg << "The IF processor (pol " << p << ") component is not "
                    << " operational. Perhaps this component is being "
                    << " shutdown or started. Check that antenna " 
                    << getAntennaName() << " is operational and does not "
                    << " have any errors related to the IF processor.";
                UnallocatedExImpl ex(fileName, lineNumber, functionName);
                ex.addData("Detail", msg.str());
                ex.log();
                throw ex.getUnallocatedEx();
            }
        }
    } catch (CORBA::SystemException& ex) {
        ostringstream msg;
        msg << "Cannot communicate with the IF Processor (pol " << p 
            << ") component. Perhaps this component has been shutdown"
            << " (or crashed). Check that antenna " << getAntennaName() 
            << " is operational and does not have any errors related to the"
            << " IF processor.";
        UnallocatedExImpl ex(fileName, lineNumber, functionName);
        ex.addData("Detail", msg.str());
        ex.log();
        throw ex.getUnallocatedEx();
    }
}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(TotalPowerImpl)
