#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import Acspy.Clients.SimpleClient
import TMCDB_IDL;
import asdmIDLTypes;
#import CCL.TotalPower
import ModeControllerExceptions
import Acspy.Common.ErrorTrace
import exceptions
import maci

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
    def setUp(self):
	self.client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()

    def tearDown(self):
        del(self.client)
     
    def initializeMasterForTP(self, antennaName):
        IFProc0Config = TMCDB_IDL.AssemblyLocationIDL\
                        ("IFProc0", "IFProc0", 0x29, 0, 0)
        IFProc1Config = TMCDB_IDL.AssemblyLocationIDL\
                        ("IFProc1", "IFProc1", 0x2a, 0, 0)
        antennaAssembly = [IFProc0Config, IFProc1Config]
        frontEndAssembly = []
        sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "A001", "", 1,
                                          frontEndAssembly, antennaAssembly)
        tmcdb = self.client.getDefaultComponent(\
                "IDL:alma/TMCDB/TMCDBComponent:1.0");
        tmcdb.setStartupAntennasInfo([sai])
        ai = TMCDB_IDL.AntennaIDL(0, antennaName,  "", \
                                  asdmIDLTypes.IDLLength(12), \
                                  asdmIDLTypes.IDLArrayTime(0), \
                                  asdmIDLTypes.IDLLength(1.0), \
                                  asdmIDLTypes.IDLLength(2.0), \
                                  asdmIDLTypes.IDLLength(10.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), 0)
        tmcdb.setAntennaInfo(antennaName,ai)
        pi = TMCDB_IDL.PadIDL(0, "A001", asdmIDLTypes.IDLArrayTime(0), \
                              asdmIDLTypes.IDLLength(-1601361.555455), \
                              asdmIDLTypes.IDLLength(-5042191.805932), \
                              asdmIDLTypes.IDLLength(3554531.803007))
        tmcdb.setAntennaPadInfo(antennaName,pi)
        self.master.startupPass1()
        self.master.startupPass2()
        self.client.releaseComponent(tmcdb._get_name());
        
    def initializeMaster(self, antennaName):
        self.master = self.client.getComponent('CONTROL/MASTER')
        self.failUnless(self.master != None,
                        "Unable to create the master component")
        if (str(self.master.getMasterState()) == 'INACCESSIBLE'):
            self.initializeMasterForTP(antennaName);
        self.antennaName = self.master.getAvailableAntennas()[0];

    def shutdownMaster(self):
        if (str(self.master.getMasterState()) == 'OPERATIONAL'):
            self.master.shutdownPass1()
            self.master.shutdownPass2()
        self.client.releaseComponent(self.master._get_name()) 

    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        tp = self.client.getDynamicComponent (None, \
              'IDL:alma/Control/TotalPower:1.0',
              None, 'CONTROL/DA41/cppContainer')
        self.client.releaseComponent(tp._get_name())

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        self.initializeMaster('DA41');
            
        try:
            antennaComponentName = 'CONTROL/' + self.antennaName
            compName = 'CONTROL/' + self.antennaName + "/TotalPower"
            compSpec = maci.ComponentSpec\
                       (compName,
                        'IDL:alma/Control/TotalPower:1.0', '*', '*')
            tp = self.client.\
                 getCollocatedComp(compSpec, False, antennaComponentName)
            self.failUnless(tp != None,
                            "Unable to create a TotalPower component")
            tp.allocate(self.antennaName)
            tp.deallocate();
            self.client.releaseComponent(tp._get_name())
        except (ModeControllerExceptions.HardwareFaultEx,
                ModeControllerExceptions.UnallocatedEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
            tp.deallocate();
            self.client.releaseComponent(tp._get_name())
            self.shutdownMaster();
            raise
        except (exceptions.AssertionError), ex:
            tp.deallocate();
            self.client.releaseComponent(tp._get_name())
            self.shutdownMaster();
            raise

        self.shutdownMaster();
        
    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        self.initializeMaster('DA41');
 
        try:
            pass
#             tp = CCL.TotalPower.TotalPower(self.antennaName);
#             del(tp);
        except (ModeControllerExceptions.HardwareFaultEx,
                ModeControllerExceptions.UnallocatedEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log();
            del(tp)
            self.shutdownMaster();
            raise
        except (exceptions.AssertionError), ex:
            del(tp)
            self.shutdownMaster();
            raise

        self.shutdownMaster();
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()
    
