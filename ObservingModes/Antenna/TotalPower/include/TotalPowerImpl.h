// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef TOTALPOWERIMPL_H
#define TOTALPOWERIMPL_H

// Base class(es)
#include <antModeControllerImpl.h>

//CORBA servant header
#include <TotalPowerS.h>

// Forward declarations for classes that this component uses
namespace Control {
    class IFProc;
}

//includes for data members
#include <acsComponentSmartPtr.h> // for maci::SmartPtr

namespace Control {
    class TotalPowerImpl :
        public virtual POA_Control::TotalPower,
        public Control::AntModeControllerImpl
    {
    public:
        
        // ------------------- Constructor & Destructor -------------------

        /// The constructor for any ACS C++ component must have this signature
        TotalPowerImpl(const ACE_CString& name,
                       maci::ContainerServices* cs);
      
        /// The destructor does nothing special. It must be virtual because
        /// this class contains virtual functions.
        virtual ~TotalPowerImpl();

        // --------------------- Component LifeCycle interface -----------

        /// This turns off the production of total power data in the IF
        /// processors and deallocates the references this component has to the
        /// IF processors.
        /// \exception acsErrTypeLifeCycle::LifeCycleExImpl
        virtual void cleanUp();

        // --------------------- CORBA interface --------------------------
              
        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void setDataConsumer(const char* tppName);

        virtual void setDataConsumerAsynch(const char* tppName, Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void beginDataAcquisition();

        virtual void beginDataAcquisitionAsynch(Control::AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        virtual void endDataAcquisition();

    private:
        // ------------------- Mode Controller Methods --------------------    

        ///
        /// Configure this component to use the equipment in the specified
        /// antenna. The supplied argument is an antenna name like "DV01" and
        /// the associated antenna component must already be started (by the
        /// master component). This function will then query the antenna
        /// component to get the references to the antenna based LO components
        /// (FLOOG, 4 LO2's and 2 IFProc's). This function can be called twice
        /// and if so it will release the equipment in the antenna it was
        /// previously using and acquire the equipment in the new antenna.
        /// \param antennaName 
        /// Name of antenna component to use e.g., "DV01" 
        /// \exception ModeControllerExceptions::BadConfigurationEx
        /// A BadConfiguration exception is generated if the antenna does not
        /// have a mount component
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// A CannotGetAntenna exception is generated if the the specified
        /// antenna component cannot be contacted
        virtual void acquireReferences(const std::string& antennaName);
    
        /// Restore this component to the state it was in after
        /// construction. In this state this component does not have a
        /// reference to any components and none of its functions, except
        /// allocate & getState should be used.
        virtual void releaseReferences();

        // ------------------- C++ Private Methods  --------------------    

        /// Check that we can use the IF processor components.
        /// \exception ModeControllerExceptions::UnallocatedEx
        /// If there is a problem with either IF processor 
        void checkRefsAreOK(const char* fileName, int lineNumber, 
                            const char* functionName);

        // The copy constructor is made private to prevent a compiler generated
        // one from being used. It is not implemented.
        TotalPowerImpl(const TotalPowerImpl&);

        // ------------------- Data Members  --------------------    

        // Pointer references to the IFProcessors
        maci::SmartPtr<Control::IFProc> ifproc0_m;
        maci::SmartPtr<Control::IFProc> ifproc1_m;
    };
} // End Control namespace
#endif // TOTALPOWERIMPL_H
