antName = 'DV01'
width = 30 #degrees.
rowStep = 1 #degrees.
velocity = 1 #degree/sec

from TowerHolography import *
from math import radians

th = TowerHolography(antName)
th.setTowerPosition(0.0, radians(width/2+10.0))
th.initializeHardware()
elOffset = -width/2.;
for i in range(width/rowStep/2):
    to = th.startHorizontalStroke(radians(elOffset), True, \
                                  radians(width), radians(velocity))
    data = th.getSubscanData(to)
    elOffset = elOffset + rowStep;
    to = th.startHorizontalStroke(radians(elOffset), False, \
                                  radians(width), radians(velocity))
    data = th.getSubscanData(to)
    elOffset = elOffset + rowStep;
#del(th)
