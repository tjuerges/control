#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import TMCDB_IDL;
import asdmIDLTypes;

client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
master = client.getComponent('CONTROL/MASTER')
Acspy.Common.QoS.setObjectTimeout(master, 60000)
if (str(master.getMasterState()) == 'INACCESSIBLE'):
    antennaName = 'DA41'
    hrxConfig = TMCDB_IDL.AssemblyLocationIDL("Holography Reciever", \
                                              "HoloRx", 0x1c, 0, 0)
    hdspConfig = TMCDB_IDL.AssemblyLocationIDL("Holography DSP", \
                                               "HoloDSP", 0x1d, 0, 0)
    mountConfig = TMCDB_IDL.AssemblyLocationIDL("Mount", "Mount", 0x0, 0, 0)
    sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "PAD1", "", 1, [], \
                                      [hrxConfig, hdspConfig, mountConfig])
    tmcdb = client.getDefaultComponent("IDL:alma/TMCDB/TMCDBComponent:1.0");
    tmcdb.setStartupAntennasInfo([sai])
    ai = TMCDB_IDL.AntennaIDL(0, antennaName,  "", \
                              asdmIDLTypes.IDLLength(12), \
                              asdmIDLTypes.IDLArrayTime(0), \
                              asdmIDLTypes.IDLLength(1.0), \
                              asdmIDLTypes.IDLLength(2.0), \
                              asdmIDLTypes.IDLLength(10.0), \
                              asdmIDLTypes.IDLLength(0.0), \
                              asdmIDLTypes.IDLLength(0.0), \
                              asdmIDLTypes.IDLLength(0.0), 0)
    tmcdb.setAntennaInfo(antennaName, ai)
    pi = TMCDB_IDL.PadIDL(0, "A001", asdmIDLTypes.IDLArrayTime(0), \
                          asdmIDLTypes.IDLLength(-1601361.555455), \
                          asdmIDLTypes.IDLLength(-5042191.805932), \
                          asdmIDLTypes.IDLLength(3554531.803007))
    tmcdb.setAntennaPadInfo(antennaName, pi)
    master.startupPass1()
    master.startupPass2()
    client.releaseComponent(tmcdb._get_name());
