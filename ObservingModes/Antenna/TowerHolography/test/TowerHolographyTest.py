#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006, 2007
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
import Acspy.Clients.SimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import TMCDB_IDL;
import asdmIDLTypes;
from math import radians
import CCL.TowerHolography;
import ModeControllerExceptions
import Acspy.Common.ErrorTrace
import exceptions

# Add this here as this interface is not always imported by
# $MODROOT/lib/python/site-packages/Control/__init__.py (depending on
# how things are built).  It needs to be imported to get a reference
# to the CONTROL/MASTER component.
import ControlMasterIF_idl

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
    def setUp(self):
        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()

    def tearDown(self):
        del(self.client)
     
    def initializeMasterForHolography(self, antennaName):
        hrxConfig = TMCDB_IDL.AssemblyLocationIDL("Holography Reciever", \
                                                  "HoloRx", 0x1c, 0, 0)
        hdspConfig = TMCDB_IDL.AssemblyLocationIDL("Holography DSP", \
                                                   "HoloDSP", 0x1d, 0, 0)
        mountConfig = TMCDB_IDL.AssemblyLocationIDL("Mount", "Mount", 0, 0, 0)
        sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "PAD1", "", 1, [], \
                                          [hrxConfig, hdspConfig, mountConfig])
        tmcdb = self.client.getDefaultComponent(\
            "IDL:alma/TMCDB/TMCDBComponent:1.0");
        tmcdb.setStartupAntennasInfo([sai])
        ai = TMCDB_IDL.AntennaIDL(0, antennaName,  "", \
                                  asdmIDLTypes.IDLLength(12), \
                                  asdmIDLTypes.IDLArrayTime(0), \
                                  asdmIDLTypes.IDLLength(1.0), \
                                  asdmIDLTypes.IDLLength(2.0), \
                                  asdmIDLTypes.IDLLength(10.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), 0)
        tmcdb.setAntennaInfo(antennaName, ai)
        pi = TMCDB_IDL.PadIDL(0, "A001", asdmIDLTypes.IDLArrayTime(0), \
                              asdmIDLTypes.IDLLength(-1601361.555455), \
                              asdmIDLTypes.IDLLength(-5042191.805932), \
                              asdmIDLTypes.IDLLength(3554531.803007))
        tmcdb.setAntennaPadInfo(antennaName,pi)
        self.master.startupPass1()
        self.master.startupPass2()
        self.client.releaseComponent(tmcdb._get_name());
        
    def initializeMaster(self):
        self.master = self.client.getComponent('CONTROL/MASTER')
        self.failUnless(self.master != None,
                        "Unable to create the master component")
        Acspy.Common.QoS.setObjectTimeout(self.master, 60000)
        self.shutdownMasterAtEnd = False;
        if (str(self.master.getMasterState()) == 'INACCESSIBLE'):
            self.initializeMasterForHolography("DA41");
            self.shutdownMasterAtEnd = True;
        self.antennaName = self.master.getAvailableAntennas()[0];

    def shutdownMaster(self):
        if (str(self.master.getMasterState()) == 'OPERATIONAL' and
            self.shutdownMasterAtEnd):
            self.master.shutdownPass1()
            self.master.shutdownPass2()
        self.client.releaseComponent(self.master._get_name()) 

    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        th = self.client.getDynamicComponent \
             (None, \
              'IDL:alma/Control/TowerHolography:1.0',
              'TowerHolography', 'CONTROL/DA41/cppContainer')
        self.client.releaseComponent(th._get_name())

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        self.initializeMaster();
        try:
            antName = self.antennaName
            towerHolographyComponentName = 'CONTROL/' + antName + \
                                           '/TowerHolography'
            th = self.client.getDynamicComponent \
                 (towerHolographyComponentName, \
                  'IDL:alma/Control/TowerHolography:1.0',
                  'TowerHolography', 'CONTROL/DA41/cppContainer')
            self.failUnless(th != None,
                            "Unable to create a TowerHolography Component")
            # Some functions (like initializing the antenna) can take
            # quite some time so set the timeout to 3 mins
            Acspy.Common.QoS.setObjectTimeout(th, 180000)
      
            th.allocate(antName);

            # Check we can get references to hardware components
            componentName = th.getHolographyReceiver();
            self.assertEqual(componentName, 'CONTROL/' + antName + '/HoloRx');

            componentName = th.getHolographyDSP();
            self.assertEqual(componentName, 'CONTROL/' + antName + '/HoloDSP');

            # set the tolerance to 1 arc-minute for the simulator. The
            # real antenna does better.
            componentName = th.getMountController();
            self.assertEqual(componentName,
                             'CONTROL/' + antName + '/MountController');
            mc = self.client.getComponent(componentName)
            mc.setTolerance(radians(1./60)); 
            self.client.releaseComponent(componentName);
      
            # HoloRx functions. 
            self.assertTrue(th.tuneLow() < 100E9);
            self.assertTrue(th.tuneHigh() > 100E9);
            self.assertTrue(th.isLocked() == True);
            self.assertTrue(th.getFrequency() > 100E9);

            # Check the tower position functions
            th.setTowerPosition(radians(100), radians(18));
            (az, el) = th.getTowerPosition()
            self.assertAlmostEquals(az, radians(100));
            self.assertAlmostEquals(el, radians(18));
      
            # tune the receiver to the high band and move the mount to
            # the tower
            th.initializeHardware(True);

            # Check a horizontal sub-scan
            timeToComplete = th.startHorizontalSubscan(0.0, True, \
                                                       radians(1.0), \
                                                       radians(0.1));
            self.assertTrue((timeToComplete > 0) and (timeToComplete < 300));
            a = th.getSubscanData(timeToComplete+1);
            self.assertEqual(a.antennaName, self.antennaName);
            self.assertTrue(a.startTime > 100000000000000000 & a.startTime < 200000000000000000)
            self.assertTrue(a.endTime > 100000000000000000 & a.endTime < 200000000000000000)
            self.assertTrue(a.endTime > a.startTime)
            self.assertEqual(a.exposureDuration, 120000);
            dataLen = len(a.holoData)
            self.assertEqual(len(a.encoderData)*4, dataLen);
            self.assertEqual(len(a.pointingDirection)*4, dataLen);
            self.assertTrue(dataLen > 500 and dataLen < 1000);
            lastElem = dataLen - 1;
            # The returned data is in this weird order because of the
            # scrambling in the non-real-time simulator. The order
            # will be different (and correct) when using the real-time
            # simulator. I still check the values as they are always
            # the same (until we change STL librarires or COMP-2239 is
            # fixed).
            self.assertEqual(a.holoData[lastElem].flag, False);
            self.assertAlmostEquals(a.holoData[lastElem].qq, 5011.0);
            self.assertAlmostEquals(a.holoData[lastElem].qr, 5012.0);
            self.assertAlmostEquals(a.holoData[lastElem].qs, 5013.0);
            self.assertAlmostEquals(a.holoData[lastElem].rr, 5014.0);
            self.assertAlmostEquals(a.holoData[lastElem].rs, 5015.0);
            self.assertAlmostEquals(a.holoData[lastElem].ss, 5016.0);

            # Check a phase cal sub-scan.
            timeToComplete = th.startPhaseCal(10.0);
            self.assertTrue((timeToComplete > 0) & (timeToComplete < 30));
            b = th.getSubscanData(timeToComplete+1);
            self.assertEqual(b.antennaName, self.antennaName);
            self.assertTrue(b.startTime > 100000000000000000 & b.startTime < 200000000000000000)
            self.assertTrue(b.endTime > 100000000000000000 & b.endTime < 200000000000000000)
            self.assertTrue(b.endTime > a.startTime)
            self.assertEqual(b.exposureDuration, 120000);
            dataLen = len(b.holoData)
            self.assertEqual(len(a.encoderData)*4, dataLen);
            self.assertEqual(len(a.pointingDirection)*4, dataLen);
            self.assertTrue(dataLen > 500 and dataLen < 1000);
            lastElem = dataLen - 1;
            self.assertEqual(b.holoData[lastElem].flag, False);
            self.assertAlmostEquals(b.holoData[lastElem].qq, 10027.0);
            self.assertAlmostEquals(b.holoData[lastElem].qr, 10028.0);
            self.assertAlmostEquals(b.holoData[lastElem].qs, 10029.0);
            self.assertAlmostEquals(b.holoData[lastElem].rr, 10030.0);
            self.assertAlmostEquals(b.holoData[lastElem].rs, 10031.0);
            self.assertAlmostEquals(b.holoData[lastElem].ss, 10032.0);

            # Check a vertical sub-scan
            timeToComplete = th.startVerticalSubscan(radians(-1), False, \
                                                     radians(1.0), \
                                                     radians(0.1));
            self.assertTrue((timeToComplete > 0) & (timeToComplete < 30));
            a = th.getSubscanData(timeToComplete+1);

            self.assertEqual(a.antennaName, self.antennaName);
            self.assertTrue(a.startTime > 100000000000000000 & a.startTime < 200000000000000000)
            self.assertTrue(a.endTime > 100000000000000000 & a.endTime < 200000000000000000)
            self.assertTrue(a.endTime > a.startTime)
            self.assertEqual(a.exposureDuration, 120000);
            dataLen = len(a.holoData)
            self.assertEqual(len(a.encoderData)*4, dataLen);
            self.assertEqual(len(a.pointingDirection)*4, dataLen);
            self.assertTrue(dataLen > 500 and dataLen < 1000);
            lastElem = dataLen - 1;
            self.assertEqual(a.holoData[lastElem].flag, False);
            self.assertAlmostEquals(a.holoData[lastElem].qq, 15043.0);
            self.assertAlmostEquals(a.holoData[lastElem].qr, 15044.0);
            self.assertAlmostEquals(a.holoData[lastElem].qs, 15045.0);
            self.assertAlmostEquals(a.holoData[lastElem].rr, 15046.0);
            self.assertAlmostEquals(a.holoData[lastElem].rs, 15047.0);
            self.assertAlmostEquals(a.holoData[lastElem].ss, 15048.0);

            # shutdown and deallocate
            th.shutdownHardware();
            th.deallocate();
            self.client.releaseComponent(th._get_name())
        except (ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log()
            th.shutdownHardware();
            th.deallocate();
            self.client.releaseComponent(th._get_name())
            self.shutdownMaster();
            raise
        except (exceptions.AssertionError), ex:
            th.shutdownHardware();
            th.deallocate();
            self.client.releaseComponent(th._get_name())
            self.shutdownMaster();
            raise

        self.shutdownMaster();

    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        self.initializeMaster();
        try:
            th = CCL.TowerHolography.TowerHolography(self.antennaName);

            # Check we can get references to hardware components
            hrx = th.getHolographyReceiver();
            del hrx
            hdsp = th.getHolographyDSP();
            del hdsp
          
            # Set the tolerance to 1 arc-minute for the simulator. The
            # real antenna does better.
            mc = th.getMountController();
            mc.setTolerance(radians(1./60));
            del mc

            # HoloRx functions
            self.assertTrue(th.tuneLow() < 100E9);
            self.assertTrue(th.tuneHigh() > 100E9);
            self.assertTrue(th.isLocked() == True);
            self.assertTrue(th.getFrequency() > 100E9);

            # Check the tower position functions
            th.setTowerPosition(radians(100), radians(18));
            (az, el) = th.getTowerPosition()
            self.assertAlmostEquals(az, radians(100));
            self.assertAlmostEquals(el, radians(18));

            # tune the receiver and move the mount to the tower
            th.initializeHardware();

            # Check a horizontal sub-scan
            timeToComplete = th.startHorizontalSubscan(0.0, True, \
                                                       radians(1.0), \
                                                       radians(0.1));
            self.assertTrue((timeToComplete > 0) and (timeToComplete < 300));
            a = th.getSubscanData(timeToComplete+1);
            self.assertEqual(a.antennaName, self.antennaName);
            self.assertTrue(a.startTime > 100000000000000000 & a.startTime < 200000000000000000)
            self.assertTrue(a.endTime > 100000000000000000 & a.endTime < 200000000000000000)
            self.assertTrue(a.endTime > a.startTime)
            self.assertEqual(a.exposureDuration, 120000);
            dataLen = len(a.holoData)
            self.assertEqual(len(a.encoderData)*4, dataLen);
            self.assertEqual(len(a.pointingDirection)*4, dataLen);
            self.assertTrue(dataLen > 500 and dataLen < 1000);
            lastElem = dataLen - 1;
            self.assertEqual(a.holoData[lastElem].flag, False);
            self.assertAlmostEquals(a.holoData[lastElem].qq, 5011.0);
            self.assertAlmostEquals(a.holoData[lastElem].qr, 5012.0);
            self.assertAlmostEquals(a.holoData[lastElem].qs, 5013.0);
            self.assertAlmostEquals(a.holoData[lastElem].rr, 5014.0);
            self.assertAlmostEquals(a.holoData[lastElem].rs, 5015.0);
            self.assertAlmostEquals(a.holoData[lastElem].ss, 5016.0);

            # Check a phase cal sub-scan.
            timeToComplete = th.startPhaseCal(10.0);
            self.assertTrue((timeToComplete > 0) & (timeToComplete < 30));
            b = th.getSubscanData(timeToComplete+1);
            self.assertEqual(b.antennaName, self.antennaName);
            self.assertTrue(b.startTime > 100000000000000000 & b.startTime < 200000000000000000)
            self.assertTrue(b.endTime > 100000000000000000 & b.endTime < 200000000000000000)
            self.assertTrue(b.endTime > a.startTime)
            self.assertEqual(b.exposureDuration, 120000);
            dataLen = len(b.holoData)
            self.assertEqual(len(a.encoderData)*4, dataLen);
            self.assertEqual(len(a.pointingDirection)*4, dataLen);
            self.assertTrue(dataLen > 500 and dataLen < 1000);
            lastElem = dataLen - 1;
            self.assertEqual(b.holoData[lastElem].flag, False);
            self.assertAlmostEquals(b.holoData[lastElem].qq, 10027.0);
            self.assertAlmostEquals(b.holoData[lastElem].qr, 10028.0);
            self.assertAlmostEquals(b.holoData[lastElem].qs, 10029.0);
            self.assertAlmostEquals(b.holoData[lastElem].rr, 10030.0);
            self.assertAlmostEquals(b.holoData[lastElem].rs, 10031.0);
            self.assertAlmostEquals(b.holoData[lastElem].ss, 10032.0);

            # Check a vertical sub-scan
            timeToComplete = th.startVerticalSubscan(radians(-1), False, \
                                                     radians(1.0), \
                                                     radians(0.1));
            self.assertTrue((timeToComplete > 0) & (timeToComplete < 30));
            a = th.getSubscanData(timeToComplete+1);

            self.assertEqual(a.antennaName, self.antennaName);
            self.assertTrue(a.startTime > 100000000000000000 & a.startTime < 200000000000000000)
            self.assertTrue(a.endTime > 100000000000000000 & a.endTime < 200000000000000000)
            self.assertTrue(a.endTime > a.startTime)
            self.assertEqual(a.exposureDuration, 120000);
            dataLen = len(a.holoData)
            self.assertEqual(len(a.encoderData)*4, dataLen);
            self.assertEqual(len(a.pointingDirection)*4, dataLen);
            self.assertTrue(dataLen > 500 and dataLen < 1000);
            lastElem = dataLen - 1;
            self.assertEqual(a.holoData[lastElem].flag, False);
            self.assertAlmostEquals(a.holoData[lastElem].qq, 15043.0);
            self.assertAlmostEquals(a.holoData[lastElem].qr, 15044.0);
            self.assertAlmostEquals(a.holoData[lastElem].qs, 15045.0);
            self.assertAlmostEquals(a.holoData[lastElem].rr, 15046.0);
            self.assertAlmostEquals(a.holoData[lastElem].rs, 15047.0);
            self.assertAlmostEquals(a.holoData[lastElem].ss, 15048.0);

            # stow the antenna
            th.shutdownHardware();
            del(th);
        except (ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace).log()
            th.shutdownHardware();
            del(th)
            self.shutdownMaster();
            raise
        except (exceptions.AssertionError), ex:
            th.shutdownHardware();
            del(th)
            self.shutdownMaster();
            raise
            
        
        self.shutdownMaster();
    
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()
