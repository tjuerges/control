#ifndef TOWERHOLOGRAPHYIMPL_H
#define TOWERHOLOGRAPHYIMPL_H
// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2006m 2007, 2008
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

// Base class(es)
#include <antModeControllerImpl.h>

//CORBA servant header
#include <TowerHolographyS.h>

// Forward declarations for classes that this component uses
class PositionStreamConsumer;
namespace Control {
  class HOLORXImpl;
  class HOLODSPImpl;
  class Mount;
  class MountController;
}

//includes for data members
#include <acstimeC.h> // for acstime::Epoch
#include <ControlExceptions.h>
#include <ModeControllerExceptions.h>
#include <acsComponentSmartPtr.h> // for maci::SmartPtr

namespace Control {
  class TowerHolographyImpl: 
    public virtual POA_Control::TowerHolography,
    public Control::AntModeControllerImpl
  { 
  public:
    
    // ------------------- Constructor & Destructor -------------------
    
    ///
    /// The constructor for any ACS C++ component must have this signature
    ///
    TowerHolographyImpl(const ACE_CString& name,
                        maci::ContainerServices* containerServices);
    
    ///
    /// The destructor does nothing special. It must be virtual because this
    /// class contains virtual functions.
    ///
    virtual ~TowerHolographyImpl();
    
    // --------------------- LifeCycle interface ----------------------
    
    // --------------------- CORBA interface --------------------------
    
    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException,
    ///        ModeControllerExceptions::MountFaultEx,
    ///        ModeControllerExceptions::HardwareFaultEx,
    ///        ModeControllerExceptions::UnallocatedEx);
    virtual void initializeHardware(bool lowBand);
    
    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException,
    ///        ModeControllerExceptions::MountFaultEx,
    ///        ModeControllerExceptions::UnallocatedEx);
    void shutdownHardware();

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw (CORBA::SystemException,
    ///         ControlExceptions::IllegalParameterErrorEx);
    void setTowerPosition(CORBA::Double az, CORBA::Double el);

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw (CORBA::SystemException);
    void getTowerPosition(CORBA::Double& az, CORBA::Double& el);

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw (CORBA::SystemException);
    void getTowerXYZPosition(CORBA::Double& X, CORBA::Double& Y,
                             CORBA::Double& Z);

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException,
    ///        ModeControllerExceptions::HardwareFaultEx,
    ///        ModeControllerExceptions::UnallocatedEx);
    CORBA::Double tuneLow();

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException,
    ///        ModeControllerExceptions::HardwareFaultEx,
    ///        ModeControllerExceptions::UnallocatedEx);
    CORBA::Double tuneHigh();

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException,
    ///        ModeControllerExceptions::HardwareFaultEx,
    ///        ModeControllerExceptions::UnallocatedEx);
    CORBA::Double getFrequency();

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException,
    ///        ModeControllerExceptions::HardwareFaultEx,
    ///        ModeControllerExceptions::UnallocatedEx);
    bool isLocked();

    ///
    /// See the IDL file for a description of this function.
    /// 
    ///  throw (ModeControllerExceptions::MountFaultEx,
    ///         ModeControllerExceptions::HardwareFaultEx,
    ///         ControlExceptions::IllegalParameterErrorEx,
    ///         ControlExceptions::DeviceBusyEx,
    ///         ModeControllerExceptions::UnallocatedEx);
    CORBA::Double startHorizontalSubscan(CORBA::Double offset, 
                                        CORBA::Boolean forward,
                                        CORBA::Double width, 
                                        CORBA::Double velocity);

    ///
    /// See the IDL file for a description of this function.
    /// 
    ///  throw (ModeControllerExceptions::MountFaultEx,
    ///         ModeControllerExceptions::HardwareFaultEx,
    ///         ControlExceptions::IllegalParameterErrorEx,
    ///         ControlExceptions::DeviceBusyEx,
    ///         ModeControllerExceptions::UnallocatedEx);
    CORBA::Double startVerticalSubscan(CORBA::Double offset, 
                                      CORBA::Boolean forward,
                                      CORBA::Double width, 
                                      CORBA::Double velocity);

    ///
    /// See the IDL file for a description of this function.
    /// 
    ///  throw (ModeControllerExceptions::MountFaultEx,
    ///         ModeControllerExceptions::HardwareFaultEx,
    ///         ControlExceptions::IllegalParameterErrorEx,
    ///         ControlExceptions::DeviceBusyEx,
    ///         ModeControllerExceptions::UnallocatedEx);
    CORBA::Double startPhaseCal(CORBA::Double duration);

    ///
    /// See the IDL file for a description of this function.
    /// 
    ///  throw(CORBA::SystemException,
    ///        ModeControllerExceptions::TimeoutEx,
    ///        ModeControllerExceptions::BadDataEx,
    ///        ModeControllerExceptions::UnallocatedEx);
    virtual Control::HolographySubScanData* 
    getSubscanData(CORBA::Double timeout);

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException);
    char* getMountController() ;

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException);
    char* getHolographyReceiver() ;

    ///
    /// See the IDL file for a description of this function.
    ///
    ///  throw(CORBA::SystemException);
    char* getHolographyDSP() ;

    // --------------------- C++ public functions ---------------------
    
  private:
    // ------------------- Mode Controller Methods --------------------    
    /// @fn void acquireReferences 
    /// This method configures this component to use the equipment in the
    /// specified antenna. The supplied argument is an antenna name like
    /// "DV01" and the associated antenna component must already be started
    /// (by the master component). This function will then query the antenna
    /// component to get the references to the holography receiver, holography
    /// digital signal processor & mount controller components. This function
    /// can be called twice and if so it will release the equipment in the
    /// antenna it was previously using and acquire the equipment in the new
    /// antenna.
    /// @param AntennaName Name of antenna component to use e.g., "DV01"
    /// A CannotGetAntenna exception is generated if the the specified
    /// antenna component cannot be contacted
    /// A BadConfiguration exception is generated if the antenna does
    /// not have references to a mount controller or either holography
    /// receiver components.
    ///  throw(ModeControllerExceptions::CannotGetAntennaEx,
    ///        ModeControllerExceptions::BadConfigurationEx);
    virtual void acquireReferences(const std::string& antennaName);
    
    /// @fn void releaseReferences 
    /// Restore this component to the state it was in after construction. In
    /// this state this component does not have a reference to any components
    /// and none of its functions, except allocate & getState should be used.
    virtual void releaseReferences();
    
    // The copy constructor is made private to prevent a compiler generated one
    // from being used. It is not implemented.
    TowerHolographyImpl(const TowerHolographyImpl& other);
    
    // This function just contains code that is common to the
    // startHorizontalSubscan and startVerticalSubscan functions.  It throws an
    // illegal parameter exception one of the supplied parameters is out of
    // range, a device busy exception if a sub-scan is already in progress and
    // an unallocated exception if this mode controller is not associated with
    // any antenna.
    //  throw(ControlExceptions::IllegalParameterErrorEx,
    //        ControlExceptions::DeviceBusyEx,
    //        ModeControllerExceptions::MountFaultEx,
    //        ModeControllerExceptions::UnallocatedEx);
      void prepareForMotionSubscan(const CORBA::Double offset, 
                                   const CORBA::Boolean forward,
                                   CORBA::Double& width,
                                   CORBA::Double& velocity,
                                   const CORBA::Double maxVelocity);

    // This function just contains code that is common to the start*Subscan and
    // startPhaseCal function. It throws a Device busy exception if a sub-scan
    // is already in progress and an Unallocated exception if this mode
    // controller is not associated with any antenna.
    //  throw(ControlExceptions::DeviceBusyEx,
    //        ModeControllerExceptions::MountFaultEx,
    //        ModeControllerExceptions::UnallocatedEx);
    void prepareForSubscan();

    // This function just contains code that is common to the start*Subscan and
    // startPhaseCal functions. It throws a harwareFault exception if there is
    // any commanding the holography receiver.
    //  throw (ModeControllerExceptions::HardwareFaultEx);
    double concludeSubscan(const std::string& scanDescription);

    // This function is used by the start* functions. It defines the start and
    // end of the subscan based on the specified subscan duration and any other
    // constraints that need to be considered.
    void  defineSubscanBoundries(double subscanDuration);
    
    // References to the holography receiver, DSP and mount controller
    // components. After construction this is a nill reference and will refer
    // to an actual component after the acquireReferences function has
    // successfully completed. It reverts to a nil reference when
    // releasReferences is called.
    maci::SmartPtr<Control::HOLORXImpl> hrx_m;
    maci::SmartPtr<Control::HOLODSPImpl> hdsp_m;
    maci::SmartPtr<Control::MountController> mountController_m;

    // In principle we do not need to make the reference to the mount component
    // a member variable. If we didn't we would need to call
    // {get,release}Component everytime doExposure was executed. This might be
    // a few times every minute in a typical optical pointing script. I'll save
    // a bit of overhead and cache the reference here.
    maci::SmartPtr<Control::Mount> mount_m;

    // This object is used to get the antenna positions. I'd rather not use a
    // pointer but as the PositionStreamConsumer class does not have a default
    // constructor and an assign(antennaName) function. So I need to use a
    // pointer to the PositionStreamConsumer class (which I can set to zero in
    // the contructor of the AntModeController class). When this object is
    // created it will cause the position data to be sent, from the antenna,
    // over the notification channel. Hence this object is only created when a
    // client requests this to be done (with the initializePositionConsumer)
    // function. 
    PositionStreamConsumer* positionConsumer_m;

    // This is the tower position. 
    double towerAz_m;
    double towerEl_m;

    // This is the start and stop time of the last (or current) sub-scan. Zero
    // means we have never done a sub-scan.
    ACS::Time startTime_m;
      ACS::Time stopTime_m;

  }; // end  TowerHolographyImpl class
} // end Control namespace

#endif // TOWERHOLOGRAPHYIMPL_H
