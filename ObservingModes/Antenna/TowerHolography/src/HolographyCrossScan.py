#!/usr/bin/env python
# This script is a user interface to doing cross scans using the holography
# receiver.  

def AverageData(data):
    az = []
    el = []
    ss = []
    rr = []

    for i in range(1, len(data.encoderData)):
        az.append(degrees(data.encoderData[i].az.value))
        el.append(degrees(data.encoderData[i].el.value))

        sAvg = data.holoData[(4*i) - 1].ss
        sAvg += data.holoData[(4*i)].ss
        sAvg += data.holoData[(4*i) + 1].ss
        sAvg = data.holoData[(4*i) + 2].ss
        sAvg /= 4.0
        ss.append(sAvg)
        
        rAvg = data.holoData[(4*i) - 1].rr
        rAvg += data.holoData[(4*i)].rr
        rAvg += data.holoData[(4*i) + 1].rr
        rAvg = data.holoData[(4*i) + 2].rr
        rAvg /= 4.0
        rr.append(rAvg)
    return(az, el, ss, rr)


from optparse import OptionParser
from datetime import datetime
from sys import exit
from math import degrees
from math import radians
import pylab as pl
from HolographyCrossScanPlotter import *
from CCL.TowerHolography import TowerHolography
from CCL.PointingModelBase import PointingModel

parser = OptionParser()

parser.add_option("--az", "--azimuth", type="float", dest="az", metavar='Azimuth',
                  help="The azimuth of the holography tower in degrees")

parser.add_option("--el", "--elevation", type="float", dest="el", metavar='Elevation',
                  help="The elevation of the holography tower in degrees")

parser.add_option("-a", "--antenna", dest="antennaName",  metavar='Antenna',
                  help="The antenna to use e.g. DV01. No default")

parser.add_option("-b", "--band", dest="tuningBand",  metavar='TuningBand',
                  default = "High",
                  help="The tuning band to use (High or Low), default is High")

parser.add_option("-s", "--scanType", dest='scanType', metavar='Scan Type',
                  default = "Cross",
                  help='The type of scan to do ("Horiz", "Vert", or "Cross")')
 
parser.add_option("-f", "--file", dest='filename', metavar="Filename",
                  help='The file name to save scans in')

parser.add_option("-c", "--count", dest='repeatCount', metavar="RepeatCount",
                  type="int", default=1,
                  help='The number of scans to do')

parser.add_option("-v", "--velocity", dest='velocity', type='float',
                  default = 100.0,
                  help='The scan velocity (arcsec/sec), default is 100"/sec')

parser.add_option('-l', '--length', type ='float', default = 0.1,
                  dest = 'scanLength', metavar="ScanLength",
                  help='The scan length, default is 0.1 degrees')

parser.add_option('-r', '--reverse', action="store_true", dest = "reverseScan")
parser.add_option('-u', '--unidir', action="store_true", dest = "uniDirection",
                  default = False, help='Unidirectional scan (all same dir)')
parser.add_option('-q', '--quiet', action="store_false", dest = "guiDisplay",
                  default = True, help='Disable GUI display')

(options, args) = parser.parse_args()

# Get the Tower Holography Mode Controller
if options.antennaName == None:
    print "An antenna name must be specified, use the \"--antenna\" option"
    exit(-1)
try:
    th = TowerHolography(options.antennaName)
except:
    print "Error unable to access TowerHolography CCL object for " + \
          options.antennaName + "."
    exit(-1)

# Modify the velocity paramenter to be in degrees/sec
options.velocity /= 3600.0

# Get the Tower Position
(az, el) = th.getTowerPosition()
if options.az != None :
    az = radians(options.az)
if options.el != None :
    el = radians(options.el)
if (options.az != None or options.el != None):
    th.setTowerPosition(az,el)


# Tune the receiver
if options.tuningBand.lower() == 'high':
    th.initializeHardware(False)
elif options.tuningBand.lower() == 'low':
    th.initializeHardware(True)
else:
    print ("Unrecognized tuning requested, aborting");
    del(th)
    exit(-1)

frequency = th.getFrequency()

plotter = HoloCrossScanPlot()
plotter.configurePlot(datetime.utcnow().isoformat(),
                      options.antennaName,
                      options.scanType,
                      options.velocity,
                      options.scanLength,
                      options.repeatCount,
                      frequency,
                      az,
                      el)
if options.guiDisplay:
    plotter.initDisplay()
    plotter.draw()
else:
    plotter.printExecInfo()
    
# Now iterate over the repeat count
for scanNumber in range(options.repeatCount):
    scan = HoloCrossScan()

    if options.uniDirection:
        forwardDirection = True
    else:
        forwardDirection = scanNumber % 2 == 0
    if options.reverseScan:
        forwardDirection = not forwardDirection

    if options.scanType.lower() != "vert":
        timeout = th.startHorizontalSubscan(0.0, forward=forwardDirection,
                                            width=radians(options.scanLength),
                                            velocity=radians(options.velocity))
        azData = th.getSubscanData(timeout+2)
        scan.setHorizStroke(HoloCrossStroke(dataTuple=AverageData(azData)))
        

    if options.scanType.lower() != "horiz":
        timeout = th.startVerticalSubscan(0.0, forward=forwardDirection,
                                          width=radians(options.scanLength),
                                          velocity=radians(options.velocity))
        elData = th.getSubscanData(timeout+2)
        scan.setVertStroke(HoloCrossStroke(dataTuple=AverageData(elData)))

    plotter.addScan(scan)
    if options.guiDisplay:
        plotter.draw()
    else:
        scan.printResults(scanNumber-1)

if options.filename != None:
    plotter.saveToFile(options.filename)

# Show reults
(azCenter, elCenter) = plotter.printResults()
if options.guiDisplay:
    plotter.show()

# Ask user if new position should be applyed.
print "\nApply tower position correction?"
confirm = str(raw_input('(please type yes or no) '))
confirm = confirm.lower()
while( not(confirm == 'yes' or confirm == 'no')):
    confirm = str(raw_input('(please type yes or no) '))
    confirm = confirm.lower()

if confirm == 'yes':
    # Get AuxPtModel
    try:
        pt = PointingModel(options.antennaName)
        pt.getAuxLoadedModel()
    except:
        print "Error unable to access PointingModel CCL object for " + \
              options.antennaName + "."
        exit(-1)

    # Get default tower position
    (azTower, elTower) = th.getTowerPosition()

    #Caculate offset
    azOffset = azCenter - degrees(azTower)
    elOffset = elCenter - degrees(elTower)

    # Set pointing model
    print "Nominal tower position is: %f, %f" % (degrees(azTower), degrees(elTower))
    print "Applying offsets to AuxPtModel: azOffset=%f, elOffset=%f" % (azOffset, elOffset)
    try:
        pt.addCoeffValue('CA', azOffset*3600.0)
        pt.addCoeffValue('IE', -elOffset*3600.0)
        pt.loadAuxModel(pt.pmData)
        pt.setAuxPointingModelStatus(True)
    except:
        print "Error unable to set auxiliary pointing model for " + \
              options.antennaName + "."
        exit(-1)
    print "Done!"

del(th)
