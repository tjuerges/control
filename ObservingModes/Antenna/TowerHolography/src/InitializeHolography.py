#!/usr/bin/env python

# Either this is a temporary script or it needs to be much
# improved. Its currently needed to move the antenna to the tower and
# tune the receiver. Then the transmitter can then be manually tuned
# and put into the bandpass of the reciever. This needs to be done
# prior to a scheduling block being executed.
#

from optparse import OptionParser
from sys import exit
from math import degrees
from math import radians
parser = OptionParser()

parser.add_option("--az", "--azimuth", type="float", dest="az",
                  metavar='Azimuth',                 
                  help="The azimuth of the holography tower in degrees")

parser.add_option("--el", "--elevation", type="float", dest="el",
                  metavar='Elevation',
                  help="The elevation of the holography tower in degrees")

parser.add_option("-a", "--antenna", dest="antennaName",  metavar='Antenna',
                  help="The antenna to use e.g. DV01. No default")

parser.add_option("-b", "--band", dest="tuningBand",  metavar='TuningBand',
                  default = "High",
                  help="The tuning band to use (High or Low), default is High")

(options, args) = parser.parse_args()

if options.antennaName == None:
    print "An antenna name must be specified, use the \"--antenna\" option"
    exit(-1)

import CCL.TowerHolography
try:
    th = CCL.TowerHolography.TowerHolography(options.antennaName)
except:
    print "Error unable to access TowerHolography CCL object for " + \
          options.antennaName + "."
    exit(-1)

(az, el) = th.getTowerPosition()

if options.az != None :
    az = radians(options.az)
if options.el != None :
    el = radians(options.el)

if (options.az != None or options.el != None):
    th.setTowerPosition(az,el)
    print "Using specified tower position az = %f el = %f" % \
          (degrees(az), degrees(el))
else:
    print "Using standard tower position az = %f el = %f" % \
          (degrees(az), degrees(el))

if options.tuningBand.lower() == 'high':
    print ("Tuning Holography Receiver to High band")
    th.initializeHardware(False)
elif options.tuningBand.lower() == 'low':
    print ("Tuning Holography Receiver to Low band")
    th.initializeHardware(True)
else:
    print ("Unrecognized tuning requested, not initializing hardware");
    del(th)
    exit(-1)

del(th)
