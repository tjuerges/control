#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
Defines the TowerHolography class.
"""

import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import math
import CCL.HoloRx
import CCL.HoloDSP
import ModeControllerExceptions
import CCL.MountController
import ScriptExec.rm
import maci

class TowerHolography:
    '''
    The TowerHolography object is used to perform holography with the
    transmitter at a fixed location (on a tower) and the ALMA
    holography receiver. Its core functions move the antenna while
    collecting data from the holography receiver and the example below
    illustrates this. This object is the basis of an holography
    observing script and an example of an observing script that uses
    this object can be found in CVS at
    SSR/ObservingModes/src/TowerHolographyScript.py
    
    EXAMPLE:
    # Load the necessary defintions
    import CCL.TowerHolography
    # create the object
    th = CCL.TowerHolography.TowerHolography("DV01")
    # Set a tower position
    th.setTowerPosition(1.5, 0.157);
    # tune the receiver and move the mount to the tower
    th.initializeHardware();
    # start a phase calibration sub-scan
    timeToComplete = th.startPhaseCal(10.0);
    # get the receiver and mount data from the last (phasecal) sub-scan
    data = th.getSubscanData(timeToComplete*1.2);
    # start a horizontal sub-scan
    timeToComplete = th.startHorizontalSubscan(0, True, .1, .01);
    # get the receiver and mount data from the last (horizontal) sub-scan
    data = th.getSubscanData(timeToComplete*1.2);
    # start a vertical sub-scan
    timeToComplete = th.startVerticalSubscan(0, True, .1, .01);
    # get the receiver and mount data from the last (vertical) sub-scan
    data = th.getSubscanData(timeToComplete*1.2);
     # stow the antenna
    th.shutdownHardware();
    # destroy the component
    del(th)

    Many of the functions in this class can throw exceptions. Unless
    specified otherwise all exceptions are in the
    ModeControllerExceptions namespace. To write robust scripts it is
    essential to catch and handle these exceptions. The following
    example shows how to do this.

    EXAMPLE 2:
    # Load the necessary defintions
    import CCL.TowerHolography
    import ModeControllerExceptions
    import Acspy.Common.ErrorTrace
    # Create the object but handle the exceptions
    try:
        th = CCL.TowerHolography.TowerHolography("DA41")
    except (ModeControllerExceptions.CannotGetAntennaEx), e:
        print("Have you specified the correct antenna.")
        print("Underlying error message is:")
        Acspy.Common.ErrorTrace.ErrorTraceHelper(e.errorTrace).log()
    except (ModeControllerExceptions.BadConfigurationEx), e:
        print("The antenna you specified cannot do holography.")
        print("Underlying error message is:")
        Acspy.Common.ErrorTrace.ErrorTraceHelper(e.errorTrace).log()
    '''

    def __init__(self, antennaName=None, componentName=None):
        '''
        The constructor creates a TowerHolography object.

        If the antennaName is defined then this constructor starts a
        dynamic component that implements the
        functions available in the TowerHolography object. The
        constructor also establishes the connection between this
        dynamic component and the relevant hardware components
        (HoloRx, HoloDSP & MountController) in the specified
        antenna.

        If the componentName is not none, then the object uses that component
        to implement the functions available in Tower Holography.
        It is assumed that the component was allocated by other means and
        references to the relavent hardware already exist.  This method is
        intended for internal use, and should only be used by experts.
    
        An exception is thrown if there is a problem creating
        this component, establishing the connection to the
        previously mentioned hardware components, or if either
        both or neither antennaName and componentName are specified.
	
        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DV01")
        # destroy the component
        del(th)
        '''
        if not((antennaName == None) ^ (componentName == None)):
            raise;

        self.logger = ScriptExec.rm.getLogger()

        if componentName != None:
            # Specified previously existing component, just get reference
            self.__th = ScriptExec.rm.getComponent(componentName)
            if (self.__th == None):
                return None
            
        # The container name is specified in the MACI/Components area
        # of the CDB.
        if antennaName != None:
            antennaComponentName = 'CONTROL/'+antennaName;
            compSpec = maci.ComponentSpec\
                       ('*',
                        'IDL:alma/Control/TowerHolography:1.0',
                        '*', '*')
            self.__th = ScriptExec.rm.\
                        getCollocatedComp(compSpec, False, antennaComponentName);
#            self.__th = ScriptExec.rm.getDynamicComponent \
#                        (None, 'IDL:alma/Control/TowerHolography:1.0',
#                         'TowerHolography', None)
            if (self.__th == None):
                return None
            try:
                self.__th.allocate(antennaName);
            except (ModeControllerExceptions.CannotGetAntennaEx, \
                    ModeControllerExceptions.BadConfigurationEx), e:
                self.__th.deallocate();
                raise;

    def initializeHardware(self, tuneLow = False):
        '''
        Initialize the relevant equipment in the antenna for use in
        tower holography.  This means:
        1. Put the mount into track mode and drive it to the tower position.
        2. Tune the holography receiver for use in the specified band.  If
        tune low is False the high band (104GHz) is used, if True the low
        band (74 GHz).
        3. Reset the holography digital signal processor
        4. Adjust the attenuators to optimize the signal levels (not
        currently implemented).
        A MountFault exception is generated if there is any problem
        initializing or using the mount or mount controller. This
        includes hardware faults.  A HardwareFault exception is
        generated if there is any problem initializing the holography
        receiver and this includes being unable to lock the reciever.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DA41")
        # Get the hardware ready
        th.initializeHardware()
        # Stow the antenna
        th.shutdownHardware()
        # destroy the component
        del(th)
        '''
        return self.__th.initializeHardware(tuneLow);

    def shutdownHardware(self):
        '''
        This function should be used at the end of a holography
        observation. It puts the mount in its survival stow position
        and then puts it in shutdown mode. A MountFault exception is
        generated if there is any problem using the mount or mount
        controller components and this includes hardware faults.

	EXAMPLE:
	See the example for the initializeHardware function.
        '''
        return self.__th.shutdownHardware();
        

    def setTowerPosition(self, az, el):
        '''
        Set the position of the holography tower. If this function is
        not called prior to observations then a default value is used.
        The default value is correct, as of 2006-10-20, for the Vertex
        antenna at the ATF. This function will throw an
        IllegalParameterErrorEx exception if the azimuth is outside
        the range -3*pi/2 to +3*pi/2 (-270 to +270 degrees) or the
        elevation is outside the range 0 to +pi/2 (0 to +90
        degrees). The azimuth and elevation must be given in radians.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DV01")
        # load he math module as it will be used in the following function
        import math
        # set the tower position. values are in degrees and converted
        # to radians by the math.radians function.
        th.setTowerPosition(math.radians(148.45), math.radians(8.143))
        # get current tower position (in radians)
        (az, el) = th.getTowerPosition();
        # destroy the component
        del(th)
        '''
        return self.__th.setTowerPosition(az, el);

    def getTowerPosition(self):
        '''
        Get the position of the holography tower. This function will
        return the default value if a tower position has not
        explicitly been set using the setTowerPosition function. The
        azimuth and elevation are returned in radians as seen from the
        antenna.

	EXAMPLE:
	See the example for the setTowerPosition function.
        '''
        return self.__th.getTowerPosition();

    def tuneLow(self):
        '''
        See the documentation of the HoloRx object for a description
        of this function.
        '''
        return self.__th.tuneLow();

    def tuneHigh(self):
        '''
        See the documentation of the HoloRx object for a description
        of this function.
        '''
        return self.__th.tuneHigh();

    def getFrequency(self):
        '''
        See the documentation of the HoloRx object for a description
        of this function.
        '''
        return self.__th.getFrequency();

    def isLocked(self):
        '''
        See the documentation of the HoloRx object for a description
        of this function.
        '''
        return self.__th.isLocked();

    def startHorizontalStroke(self, offset, forward = True,
                              width=math.radians(1.64),
                              velocity=math.radians(5.0/60.0)):
        '''
        This is a deprecated function that is identical to startHorizontalSubscan.
        '''
        return self.__th.startHorizontalSubscan(offset, \
                                                forward, width, velocity);


    def startHorizontalSubscan(self, offset, forward = True,
                              width=math.radians(1.64),
                              velocity=math.radians(5.0/60.0)):
        '''
        Start a horizontal sub-scan ie., one where the antenna moves
        in azimuth. The sub-scan will not be exactly horizontal as the
        path taken by the mount will be a great circle. Hence the
        elevation will change slightly but by a lot less than the
        azimuth. The width is the angle, in radians the antenna
        moves. It defaults to 1.64 degrees, a value often used for
        measurements at the ATF. The width should always be a positive
        value. The sub-scan will be centered about the tower position
        but offset in elevation (latitude) by an angle given by the
        offset parameter. The offset is also given in radians and can
        be positive or negative. There is no default for the offset
        (but zero is a suitable value for testing). The velocity
        specifies how fast the mount will move during the sub-scan and
        is specified in radians/sec. The default value is 5
        arc-min/sec a value often used for measurements at the ATF. By
        default the mount will move from a low azimuth to a larger
        value but, if the forward parameter is false, the mount will
        move the other way i.e., from a larger azimuth to a lower
        value. This is used to interlace the sub-scans when doing a
        raster. This function will return the time, in seconds, that
        the sunscan should take to complete. This function will check
        that the supplied parameters are plausible and throw an
        illegal parameter exception if the absolute value of the
        offset is greater than pi/2 (90 degrees), if the width is
        greater than pi (180 degrees) or less than or equal to zero,
        or if the velocity is not between zero and 6 deg/sec. This
        function will throw an unallocated exception if no antenna has
        been specified. It will throw a mount fault exception if there
        is any problem moving the mount. It will throw a hardware
        fault exception if there is any problem with the holography
        receiver. It will throw a device busy exception if a sun-scan
        is currently in progress.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DA41")
        # Get the hardware ready
        th.initializeHardware()
        # Start a sub-scan.
        timeout = th.startHorizontalSubscan(0.0, True, 0.03, 0.003)
        # Get the data from the last sub-scan
        data = th.getSubscanData(timeout+2)
        # plot the encoder azimuth.
        from pylab import *
        plot([data.encoderData[i].az.value for i in range(len(data.encoderData))])
        # Stow the antenna
        th.shutdownHardware()
        # destroy the component
        del(th)
        '''
        return self.__th.startHorizontalSubscan(offset, \
                                                forward, width, velocity);

    def startVerticalSubscan(self, offset, forward = True,
                              width=math.radians(1.64),
                              velocity=math.radians(5.0/60.0)):
        '''
        Start a vertical sub-scan ie., one where the antenna moves
        in elevation. The sub-scan will not be exactly vertical as the
        path taken by the mount will be a great circle. Hence the
        azimuth will change slightly but by a lot less than the
        azimuth. The width is the angle, in radians the antenna
        moves. It defaults to 1.64 degrees, a value often used for
        measurements at the ATF. The width should always be a positive
        value. The sub-scan will be centered about the tower position
        but offset in azimuth (longitude) by an angle given by the
        offset parameter. The offset is also given in radians and can
        be positive or negative. There is no default for the offset
        (but zero is a suitable value for testing). The velocity
        specifies how fast the mount will move during the sub-scan and
        is specified in radians/sec. The default value is 5
        arc-min/sec a value often used for measurements at the ATF. By
        default the mount will move from a low elevation to a larger
        value but, if the forward parameter is false, the mount will
        move the other way i.e., from a larger elevation to a lower
        value. This is used to interlace the sub-scans when doing a
        raster. This function will return the time, in seconds, that
        the sunscan should take to complete. This function will check
        that the supplied parameters are plausible and throw an
        illegal parameter exception if the absolute value of the
        offset is greater than pi/2 (90 degrees), if the width is
        greater than pi (180 degrees) or less than or equal to zero,
        or if the velocity is not between zero and 3 deg/sec. This
        function will throw an unallocated exception if no antenna has
        been specified. It will throw a mount fault exception if there
        is any problem moving the mount. It will throw a hardware
        fault exception if there is any problem with the holography
        receiver. It will throw a device busy exception if a sun-scan
        is currently in progress.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DV01")
        # Get the hardware ready
        th.initializeHardware()
        # Start a sub-scan.
        timeout = th.startVerticalSubscan(0.0, False, 0.01, 0.001)
        # Get the data from the last sub-scan
        data = th.getSubscanData(timeout+2)
        # plot the encoder elevation.
        from pylab import *
        plot([data.encoderData[i].el.value for i in range(len(data.encoderData))])
        # Stow the antenna
        th.shutdownHardware()
        # destroy the component
        del(th)
        '''
        return self.__th.startVerticalSubscan(offset, \
                                              forward, width, velocity);

    def startPhaseCal(self, duration=60.0):
        '''
        Do a phase calibration sub-scan. A phase calibration involves
        moving the antenna to the tower position and collecting data
        from the holography receiver for the specified period of
        time. This function will return the time, in seconds, that the
        sub-scan should take to complete. This function will check
        that the supplied parameters are plausible and throw an
        illegal parameter exception if the duration is not a positive
        value. The duration does not include any slew time. This
        function will throw an unallocated exception if no antenna has
        been specified. It will throw a mount fault exception if there
        is any problem moving the mount. It will throw a hardware
        fault exception if there is any problem with the holography
        receiver. It will throw a device busy exception if a sub-scan
        is currently in progress.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DV01")
        # Get the hardware ready
        th.initializeHardware()
        # Start a sub-scan.
        timeout = th.startPhaseCal(20.0)
        # Get the data from the last sub-scan
        data = th.getSubscanData(timeout+2)
        # plot the ss data.
        from pylab import *
        plot([data.holoData[i].ss for i in range(len(data.holoData))])
        show()
        # Stow the antenna
        th.shutdownHardware()
        # destroy the component
        del(th)
        '''
        return self.__th.startPhaseCal(duration);

    def getSubscanData(self, timeout=300.0):
        '''
        Return the data from the last sub-scan. If there has been no
        sub-scan i.e, a call to either startPhaseCal or the
        startHorizontalSubscan functions, then this will return a
        structure with all sequences of zero length (and a normal
        intent). This function will block if a sub-scan is in progress
        and then return the data after the sub-scan completes. It will
        throw a timeout exception if the scan is not expected to
        complete within the specified timeout period. A timeout of
        less than zero means wait forever and should be used with
        caution. This function will throw an unallocated exception if
        no antenna has been specified. It will throw a BadData
        exception if the timestamps on the position data are not not
        the expected values.

	EXAMPLE:
	See the examples for the startPhaseCal and
	startHorizontalSubscan functions.
        '''
        return self.__th.getSubscanData(timeout);

    def getMountController(self):
        '''
        Return the mount controller object used by this observing
        mode.  This will allow you access to many detailed aspects of
        the antenna position. However it should only be used by people
        who understand how this object interacts with this observing
        mode. It is made available so that experts can undertake tower
        holography observations in ways not forseen in this mode
        controller. Until the python facade class is written this will
        just return the component name. A zero lenth string
        is returned if this object does not have a reference to the
        mount controller object.
        '''
        componentName = self.__th.getMountController()
        return CCL.MountController.MountController(None, componentName)


    def getHolographyReceiver(self):
        '''
        Return the holography receiver object used by this observing
        mode.  This will allow you access to many detailed aspects of
        the receiver. However it should only be used by people who
        understand how this object interacts with this observing
        mode. It is made available so that experts can undertake tower
        holography observations in ways not forseen in this object.

        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DA41")
        # set the attenuator on the signal channel 
        th.getHolographyReciever().SET_SIG_ATTENUATION(2)
        # destroy the component
        del(th)
        '''
        componentName = self.__th.getHolographyReceiver();
        return CCL.HoloRx.HoloRx(componentName);

    def getHolographyDSP(self):
        '''
        Return the holography digital signal processor object used by
        this observing mode.  This will allow you access to many
        detailed aspects of the processor. However it should only be
        used by people who understand how this object interacts with
        this observing mode. It is made available so that experts can
        undertake tower holography observations in ways not forseen in
        this object.
        
        EXAMPLE:
        # Load the necessary defintions
        import CCL.TowerHolography
        # create the object
        th = CCL.TowerHolography.TowerHolography("DV01")
        # see if the HoloDSP component is operational
        hdsp = th.getHolographyDSP();
        print str(hdsp.getHwState());
        # destroy the component
        del(th)
        '''
        componentName = self.__th.getHolographyDSP();
        return CCL.HoloDSP.HoloDSP(componentName);
