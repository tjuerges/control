#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2006
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import sys
import unittest
from Acspy.Clients.SimpleClient import PySimpleClient
import Acspy.Common.QoS
import Acspy.Util.ACSCorba
import TMCDB_IDL
import asdmIDLTypes
import Control
from math import radians

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
    def setUp(self):
        Acspy.Common.QoS.setObjectTimeout(Acspy.Util.ACSCorba.getManager(), \
                                          20000)
        self.client = PySimpleClient.getInstance()

    def tearDown(self):
        self.client.disconnect()
        del(self.client)

        
    def initializeMasterForAntLOTesting(self, antennaName):
        mountConfig = TMCDB_IDL.AssemblyLocationIDL("Mount", "Mount", 0, 0, 0)
        sai = TMCDB_IDL.StartupAntennaIDL(antennaName, "PAD1", "", 1, [], \
                                          [mountConfig])
        tmcdb = self.client.getDefaultComponent(\
            "IDL:alma/TMCDB/TMCDBComponent:1.0")
        tmcdb.setStartupAntennasInfo([sai])
        ai = TMCDB_IDL.AntennaIDL(0, antennaName,  "", \
                                  asdmIDLTypes.IDLLength(12), \
                                  asdmIDLTypes.IDLArrayTime(0), \
                                  asdmIDLTypes.IDLLength(1.0), \
                                  asdmIDLTypes.IDLLength(2.0), \
                                  asdmIDLTypes.IDLLength(10.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), \
                                  asdmIDLTypes.IDLLength(0.0), 0)
        tmcdb.setAntennaInfo(antennaName, ai)
        pi = TMCDB_IDL.PadIDL(0, "A001", asdmIDLTypes.IDLArrayTime(0), \
                              asdmIDLTypes.IDLLength(-1601361.555455), \
                              asdmIDLTypes.IDLLength(-5042191.805932), \
                              asdmIDLTypes.IDLLength(3554531.803007))
        tmcdb.setAntennaPadInfo(antennaName, pi)
        self.master.startupPass1()
        self.master.startupPass2()
        self.client.releaseComponent(tmcdb._get_name())

    def initializeArray(self):
        self.master = self.client.getComponent('CONTROL/MASTER')
        self.failUnless(self.master != None,
                        "Unable to create the master component")
        Acspy.Common.QoS.setObjectTimeout(self.master, 60000)
        if (str(self.master.getMasterState()) == 'INACCESSIBLE'):
            self.initializeMasterForAntLOTesting("DV01");
            self.shutdownMasterAtEnd = True;
        self.antennaName = self.master.getAvailableAntennas()[0]

    def destroyArray(self):
        if (str(self.master.getMasterState()) == 'OPERATIONAL' and
            self.shutdownMasterAtEnd):
            self.master.shutdownPass1()
            self.master.shutdownPass2()
        self.client.releaseComponent(self.master._get_name())

    def test01( self ):
        '''
        This simple test just loads and unloads the component. It finds
        errors due to missing symbols.
        '''
        sfi = self.client.getDynamicComponent \
             (None,
              'IDL:alma/Control/SingleFieldInterferometry:1.0',
              None,
              'CONTROL/DA41/cppContainer')
        self.client.releaseComponent(sfi._get_name())

    def test02( self ):
        '''
        Test that I can allocate and deallocate the mode controller
        to an antenna
        '''
        pass
#         self.initializeArray()
#         antName = self.antennaName
#         SFIComponentName = 'CONTROL/' + antName + '/SingleFieldInterferometry'
#         sfi = self.client.getDynamicComponent \
#               (SFIComponentName, \
#                'IDL:alma/Control/SingleFieldInterferometry:1.0',
#                'SingleFieldInterferometry', 'CONTROL/DV01/cppContainer')
#         self.failUnless(sfi != None,
#                       "Unable to create a SingleFieldInterferometry Component")
#         sfi.allocate(self.antennaName)
#         loc = sfi.getAntennaLOController()
#         self.failIf(loc=="","Empty String returned for Antenna LO Controller")
#         mc = sfi.getMountController()
#         self.failIf(mc=="","Empty String returned for Mount Controller")
#         sfi.deallocate()
#         self.destroyArray()
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
    if len( sys.argv ) == 1:
        sys.argv.append( "automated" )
        
    unittest.main()
