// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <SingleFieldInterferometryImpl.h>
#include <simpleCallbackImpl.h>
#include <LogToAudience.h>
#include <LLCC.h> // for Control::LLC
#include <WVRC.h> // for Control::WVR
#include <AntennaC.h> //  for Control::Antenna
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <ModeControllerExceptions.h>
#include <loggingMACROS.h> // for AUTO_TRACE
#include <ACSErrTypeOK.h>
#include <string> // for string

using Control::SingleFieldInterferometryImpl;
using std::string;              
using ModeControllerExceptions::BadConfigurationExImpl;
using ModeControllerExceptions::UnallocatedEx;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::UnallocatedCompletion;
using ControlExceptions::HardwareErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::HardwareErrorCompletion;

//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
SingleFieldInterferometryImpl::
SingleFieldInterferometryImpl(const ACE_CString& name,
                              maci::ContainerServices* cs) :
  AntModeControllerImpl(name, cs)
{
    AUTO_TRACE(__func__);
}

SingleFieldInterferometryImpl::~SingleFieldInterferometryImpl()
{
    AUTO_TRACE(__func__);
    releaseReferences();
}

//-----------------------------------------------------------------------------
//  CORBA interface 
//-----------------------------------------------------------------------------
           
void SingleFieldInterferometryImpl::enableLLCCorrection(bool enable) {
 ACS_TRACE(__func__);

 if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    llc_m->enableLengthCorrection(enable);
  } catch (ControlExceptions::CAMBErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  } catch (ControlExceptions::INACTErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  }
}

void SingleFieldInterferometryImpl::
enableLLCCorrectionAsynch(bool enable, AntennaCallback* cb) {
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), c);
  }
  
  try {
    enableLLCCorrection(enable);
    cb->report(getAntennaName().c_str(), ACSErrTypeOK::ACSErrOKCompletion());
  } catch (UnallocatedEx& ex) {
    UnallocatedCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), c);
  } catch (HardwareErrorEx& ex) {
    HardwareErrorCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), c);
  }
}

bool SingleFieldInterferometryImpl::isLLCCorrectionEnabled() {
  ACS_TRACE(__func__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    return llc_m->lengthCorrectionEnabled();
  } catch (ControlExceptions::CAMBErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  } catch (ControlExceptions::INACTErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  }
}

void SingleFieldInterferometryImpl::
isLLCCorrectionEnabledAsynch(BooleanCallback* cb) {
  ACS_TRACE(__func__);

  try {
    cb->report(getAntennaName().c_str(), isLLCCorrectionEnabled(),
               ACSErrTypeOK::ACSErrOKCompletion());
  } catch (UnallocatedEx& ex) {
    UnallocatedCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), 0, c);
  } catch (HardwareErrorEx& ex) {
    HardwareErrorCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), 0, c);
  }
}

float SingleFieldInterferometryImpl::getLLCTimeToReset(){
  ACS_TRACE(__func__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    return llc_m->getTimeToLimit();
  } catch (ControlExceptions::CAMBErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  } catch (ControlExceptions::INACTErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  }
}

void SingleFieldInterferometryImpl::
getLLCTimeToResetAsynch(DurationCallback* cb){
  ACS_TRACE(__func__);
  try {
    ACS::Time timeToReset = static_cast<ACS::Time>(1E7 * getLLCTimeToReset());
    cb->report(getAntennaName().c_str(), timeToReset,
               ACSErrTypeOK::ACSErrOKCompletion());
  } catch (UnallocatedEx& ex) {
    UnallocatedCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), 0, c);
  } catch (HardwareErrorEx& ex) {
    HardwareErrorCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), 0, c);
  }
}

Control::LLC::LLCPositionResetData_t 
SingleFieldInterferometryImpl::resetLLCStretcher(){
  ACS_TRACE(__func__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    return llc_m->setStretcherToOptimumPosition();
  } catch (ControlExceptions::CAMBErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  } catch (ControlExceptions::INACTErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  }
}

void SingleFieldInterferometryImpl::  
resetLLCStretcherAsynch(Control::LLCPositionResetCallback* cb) {
  ACS_TRACE(__func__);
  Control::LLC::LLCPositionResetData_t resetData; 
  try {
    resetData = resetLLCStretcher();
    cb->report(getAntennaName().c_str(), resetData,
               ACSErrTypeOK::ACSErrOKCompletion());
  } catch (UnallocatedEx& ex) {
    UnallocatedCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), resetData, c);
  } catch (HardwareErrorEx& ex) {
    HardwareErrorCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), resetData, c);
  }
}

Control::LLC::LLCPositionResetData_t 
SingleFieldInterferometryImpl::setLLCStretcherPosition(float position){
  ACS_TRACE(__func__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    return llc_m->setStretcherToPosition(position);
  } catch (ControlExceptions::CAMBErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  } catch (ControlExceptions::INACTErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  }
}

void SingleFieldInterferometryImpl::  
setLLCStretcherPositionAsynch(float position,
                              Control::LLCPositionResetCallback* cb) {
  ACS_TRACE(__func__);
  Control::LLC::LLCPositionResetData_t resetData; 
  try {
    resetData = setLLCStretcherPosition(position);
    cb->report(getAntennaName().c_str(), resetData,
               ACSErrTypeOK::ACSErrOKCompletion());
  } catch (UnallocatedEx& ex) {
    UnallocatedCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), resetData, c);
  } catch (HardwareErrorEx& ex) {
    HardwareErrorCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), resetData, c);
  }
}

/* See the IDL file for a description of this function. */
bool SingleFieldInterferometryImpl::getLLCPolarizationCalRequired() {
  ACS_TRACE(__func__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __func__);
    ex.log();
    throw ex.getUnallocatedEx();
  }
  
  try {
    return llc_m->polarizationCalRequired();
  }  catch (ControlExceptions::CAMBErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  } catch (ControlExceptions::INACTErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __func__);
    throw nex.getHardwareErrorEx();
  }
}
        
void SingleFieldInterferometryImpl::
getLLCPolarizationCalRequiredAsynch(BooleanCallback* cb){
  ACS_TRACE(__func__);
  try {
    bool required = getLLCPolarizationCalRequired();
    cb->report(getAntennaName().c_str(), required,
               ACSErrTypeOK::ACSErrOKCompletion());
  } catch (UnallocatedEx& ex) {
    UnallocatedCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), 0, c);
  } catch (HardwareErrorEx& ex) {
    HardwareErrorCompletion c(ex,__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), 0, c);
  }
}

void SingleFieldInterferometryImpl::
doLLCPolarizationCalibration(ACS::Time timeout,
                             bool forceCalibration,
                             AntennaCallback* cb) {
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __func__);
    c.log();
    cb->report(getAntennaName().c_str(), c);
  }
  
  try {
    if (llc_m->startPolarizationOptimization(forceCalibration)){
      SimpleCallbackImpl localCb(getContainerServices());
      llc_m->waitForPolarizationCalibration(timeout, 
                                            localCb.getExternalReference());
      localCb.waitForCompletion(timeout + 1000000000); // +10 s
    }
 }  catch (ControlExceptions::CAMBErrorEx& ex) {
    HardwareErrorCompletion c(ex, __FILE__, __LINE__, __func__);
    cb->report(getAntennaName().c_str(), c);
    return;
  } catch (ControlExceptions::INACTErrorEx& ex) {
    HardwareErrorCompletion c(ex, __FILE__, __LINE__, __func__);
    cb->report(getAntennaName().c_str(), c);
    return;
  } catch (ControlExceptions::TimeoutExImpl& ex) {
    ControlExceptions::TimeoutCompletion c(ex, __FILE__, __LINE__, __func__);
    cb->report(getAntennaName().c_str(), c);
    return;
  } catch (ControlCommonExceptions::OSErrorExImpl& ex) {
    ControlCommonExceptions::OSErrorCompletion c(ex, __FILE__, __LINE__, __func__);
    cb->report(getAntennaName().c_str(), c);
    return;
  } catch (ControlCommonExceptions::AsynchronousFailureExImpl& ex){
    ControlCommonExceptions::AsynchronousFailureCompletion c(ex,__FILE__, __LINE__, __func__);
    cb->report(getAntennaName().c_str(), c);
    return;
  }
  cb->report(getAntennaName().c_str(), ACSErrTypeOK::ACSErrOKCompletion());
}

void SingleFieldInterferometryImpl::setWVRIntegrationTime(ACS::TimeInterval interval) {
    AUTO_TRACE(__func__);

    // Do not do anything if we do not have a WVR;
    if (!checkWVRIsOK()) return;

    // TODO. Do the actual work here.

}
#include <simpleCallbackImpl.h>
#include <LogToAudience.h>

void SingleFieldInterferometryImpl::
setWVRIntegrationTimeCB(ACS::TimeInterval interval, AntModeControllerCB* cb) {
    AUTO_TRACE(__func__);

    try {
        setWVRIntegrationTime(interval);
    } catch (ModeControllerExceptions::HardwareFaultEx& ex) {
        ModeControllerExceptions::HardwareFaultCompletion 
            c(ex, __FILE__, __LINE__, __func__);
        cb->report(getAntennaName().c_str(), c);
        return;
    }

    // Notify callback of success
    ACSErrTypeOK::ACSErrOKCompletion okComplete;
    cb->report(getAntennaName().c_str(), okComplete);
}

ACS::TimeInterval SingleFieldInterferometryImpl::
getWVRIntegrationTime() {
    AUTO_TRACE(__func__);

    // Do not do anything if we do not have a WVR;
    if (!checkWVRIsOK()) return 0;

    // TODO. Do the actual work here.
    return 480000ULL*24;
}

bool SingleFieldInterferometryImpl::isWVRSendingData() {
    return checkWVRIsOK();
}

template <typename T> maci::SmartPtr<T> 
SingleFieldInterferometryImpl::getOptionalComponent(const std::string& name) {
    try {
        CORBA::String_var componentName;  
        componentName = allocatedAntenna_m->
            getSubdeviceName(name.c_str());
        return getContainerServices()->
          getComponentNonStickySmartPtr<T>(componentName);
    } catch (ControlExceptions::IllegalParameterErrorEx ex) {
        ControlExceptions::IllegalParameterErrorExImpl newEx(ex);
        newEx.log(LM_DEBUG);
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        ex.log(LM_DEBUG);
    }
    string msg = "Could not find a " + name + " component on antenna ";
    msg += getAntennaName() + ". Will proceed without one.";
    msg += " Some capabilities will not be available.";
    LOG_TO_OPERATOR(LM_WARNING, msg);
    maci::SmartPtr<T> nilReturn(getContainerServices(), 
                                T::_nil(), false);

    return nilReturn;
}

bool SingleFieldInterferometryImpl::checkWVRIsOK() {
    const string antName = getAntennaName();
    if (antName.length() == 0) {
        string msg = "Antenna name is empty.";
        msg += " Has the mode controller been allocated to an antenna?";
        LOG_TO_DEVELOPER(LM_ERROR, msg); 
        return false;
    }

    if (wvr_m.isNil()) {
        string msg = "WVR reference is null.";
        msg += " Does antenna " + antName + " have a WVR in its TMCDB configuration?";
        LOG_TO_DEVELOPER(LM_ERROR, msg); 
        return false;
    }
    // So we think we have a reference, but lets checks its really
    // there. Because we have a non-sticky reference it could be released
    // by the master.
    try {
        Control::HardwareDevice::HwState state = wvr_m->getHwState();
        if (state != Control::HardwareDevice::Operational &&
            state != Control::HardwareDevice::Simulation) {
            string msg = "The WVR component is not operational.";
            msg += " Check that antenna " + antName + " is operational";
            msg += " and does not have any errors related to the WVR.";
            LOG_TO_DEVELOPER(LM_ERROR, msg); 
            return false;
        }
    } catch (CORBA::SystemException& ex) {
        string msg = "Cannot communicate with the WVR component.";
        msg += " Perhaps the WVR component is shutdown";
        msg += " or its container has crashed.";
        msg += " Check that antenna " + antName + " is operational";
        msg += " and does not have any errors related to the WVR.";
        LOG_TO_DEVELOPER(LM_ERROR, msg); 
        return false;
    }
    return true;
}

//-----------------------------------------------------------------------------
// Lifecycle Methods
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Mode Controller Methods
//-----------------------------------------------------------------------------

void SingleFieldInterferometryImpl::
acquireReferences(const string& antennaName) {
    AUTO_TRACE(__func__);

    // Remove any old references (in case this function is executed twice).
    // But only do this if necessary.
    if (antennaName != getAntennaName()) releaseReferences();

    // If the antenna is already initialized it means we already have the
    // references for the correct antenna. So there is nothing left to do.
    if (getStatus() != Control::AntModeController::UNINITIALIZED) return;
    
    // This gets a reference to the antenna component
    AntModeControllerImpl::acquireReferences(antennaName);
  
    // Get references to the antenna-based components specific to SFI.
    string currentComponent("LLC");
    try {
        CORBA::String_var deviceName;  
        //LLC
        deviceName = allocatedAntenna_m->
            getSubdeviceName(currentComponent.c_str());
        llc_m = getContainerServices()->
            getComponentNonStickySmartPtr<Control::LLC>(deviceName);
    } catch (ControlExceptions::IllegalParameterErrorEx ex) {
        releaseReferences();
        string msg = "Could not find a " + currentComponent + " component.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__, __LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        releaseReferences();
        string msg = currentComponent + " reference is null.";
        msg += " Is your configuration correct?";
        BadConfigurationExImpl newEx(ex, __FILE__,__LINE__, __func__);
        newEx.addData("Detail", msg);
        newEx.log();
        throw newEx.getBadConfigurationEx();
    }

    wvr_m = getOptionalComponent<Control::WVR>("WVR");
}

void SingleFieldInterferometryImpl::releaseReferences() {
    AUTO_TRACE(__func__);

    // release the managed hardware components. The smart pointer will call
    // releaseComponent if necessary.
    llc_m.release();
    wvr_m.release();

    // Set the state to uninitialized, the antenna name to an empty string and
    // release the antenna reference.
    AntModeControllerImpl::releaseReferences();
}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(SingleFieldInterferometryImpl)
