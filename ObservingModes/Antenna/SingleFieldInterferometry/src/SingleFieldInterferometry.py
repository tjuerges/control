#! /usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2007 - 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
It defines the SingleFieldInterferometry mode controller.
"""

import Acspy.Clients.SimpleClient
import ModeControllerExceptions

class SingleFieldInterferometry:
    '''
    The SingleFieldInterferometry object is used to perfom simple
    interferometric observations of astronomical sources.  This
    object is the Antenna level object providing precise control
    of the behavior of a single antenna.

    Often this object will be recieved from the SFIObservingMode
    and will not need to be instanciated from the CCL directly,
    although a constructor is provided to allow creation
    (for whatever reason)
    
    EXAMPLE:
    # Load the necessary defintions
    from SingleFieldInterferometry import *
    # create the object
    sfi = SingleFieldInterferometry("DV01")
    # reset the LLC
    sfi.resetAntennaLLCStretcher()
    # destroy the component
    del(th)
    '''

    def __init__(self, antennaName=None, componentName=None):
        '''
        The constructor creates a SingleFieldInterferometry object.

        If the antennaName is defined then a dynamic component that implements
        the functions available in the SingleFieldInterferometry object is
        created. Theconstructor also establishes the connection between this
        dynamic component and the relevant hardware components(MountController
        and AntLOController) in the specified antenna.

        If the componentName is not none, then the object uses that component
        to implement the functions available in SingleFieldInterferometry.
        It is assumed that the component was allocated by other means and
        references to the relavent hardware already exist.  This method is
        intended for internal use, and should only be used by experts.
        
        Exceptions are thrown if there is a problem creating or connecting
        to this component, establishing the connection to the previously
        mentioned hardware components, or if either both or neither
        antennaName and componentName are specified.

        EXAMPLE:
        # Load the necessary defintions
        from SingleFieldInterferometry import *
        # create the object
        sfi = SingleFieldInterferometry("DV01")
        # destroy the component
        del(sfi)
        '''
        if not((antennaName == None) ^ (componentName == None)):
            raise;
        
	self.__client = Acspy.Clients.SimpleClient.PySimpleClient.getInstance()
        if componentName != None:
            # Specified previously existing component, just get reference
            self.__sfi = self.__client.getComponent(componentName)
            if (self.__sfi == None):
                return None
            
        if antennaName != None:
            # Specified Antenna name, generate and allocate a new
            # dynamic component
            self.__sfi = self.__client.getDynamicComponent \
                     (None, 'IDL:alma/Control/SingleFieldInterferometry:1.0',
                      None, None)
            if (self.__sfi == None):
                return None
            try:
                self.__sfi.allocate(antennaName);
            except (ModeControllerExceptions.CannotGetAntennaEx, \
                    ModeControllerExceptions.BadConfigurationEx), e:
                self.__sfi.deallocate();
                raise;

    def __del__(self):
        '''
	The destructor releases the component, if the component is shutdown
        all references are released.

	EXAMPLE:
	See the example for the constructor (__init__ function)
	'''
        self.__client.releaseComponent(self.__sfi._get_name())
        self.__client.disconnect()


    def resetAntennaLLCStretcher(self):
        '''
        Reset the LLC associated with this mode controller
        '''
        return self.__sfi.resetAntennaLLCStretcher();

