// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

static char *rcsId="@(#) $Id$"; 
static void *use_rcsId = ((void)&use_rcsId,(void *) &rcsId);

#include <AntInterferometryControllerImpl.h>
#include <simpleCallbackImpl.h>
#include <LogToAudience.h>
#include <LLCC.h> // for Control::LLC
#include <WVRC.h> // for Control::WVR
#include <AntennaC.h> //  for Control::Antenna
#include <ControlExceptions.h>
#include <ControlCommonExceptions.h>
#include <ModeControllerExceptions.h>
#include <loggingMACROS.h> // for AUTO_TRACE
#include <ACSErrTypeOK.h>
#include <string> // for string

using Control::AntInterferometryControllerImpl;
using std::string;              
using ModeControllerExceptions::BadConfigurationExImpl;
using ModeControllerExceptions::UnallocatedEx;
using ModeControllerExceptions::UnallocatedExImpl;
using ModeControllerExceptions::UnallocatedCompletion;
using ControlExceptions::HardwareErrorEx;
using ControlExceptions::HardwareErrorExImpl;
using ControlExceptions::HardwareErrorCompletion;
using ControlExceptions::InvalidRequestEx;
using ControlExceptions::InvalidRequestExImpl;
using ControlExceptions::TimeoutEx;
using ControlExceptions::TimeoutExImpl;
using ControlExceptions::IllegalParameterErrorEx;
using ControlExceptions::IllegalParameterErrorExImpl;

//-----------------------------------------------------------------------------
// Constructor & Destructor
//-----------------------------------------------------------------------------
AntInterferometryControllerImpl::
AntInterferometryControllerImpl(const ACE_CString& name,
                              maci::ContainerServices* cs) :
  AntModeControllerImpl(name, cs)
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
}

AntInterferometryControllerImpl::~AntInterferometryControllerImpl()
{
    AUTO_TRACE(__PRETTY_FUNCTION__);
    releaseReferences();
}

//-----------------------------------------------------------------------------
//  CORBA interface 
//-----------------------------------------------------------------------------
           
void AntInterferometryControllerImpl::enableLLCCorrection(bool enable) {
 ACS_TRACE(__PRETTY_FUNCTION__);

 if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    allocatedAntenna_m->enableLLCCorrection(enable);
  } catch (HardwareErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getHardwareErrorEx();
  } catch (InvalidRequestEx& ex) {
    InvalidRequestExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getInvalidRequestEx();
  }
}

void AntInterferometryControllerImpl::
enableLLCCorrectionAsynch(bool enable, AntennaCallback* cb) {
 ACS_TRACE(__PRETTY_FUNCTION__);
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    c.log();
    cb->report(getAntennaName().c_str(), c);
  }
    allocatedAntenna_m->enableLLCCorrectionAsynch(enable, cb);
}

bool AntInterferometryControllerImpl::isLLCCorrectionEnabled() {
  ACS_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  
  try {
    return allocatedAntenna_m->isLLCCorrectionEnabled();
  } catch (HardwareErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getHardwareErrorEx();
  } catch (InvalidRequestEx& ex) {
    InvalidRequestExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getInvalidRequestEx();
  }
}

void AntInterferometryControllerImpl::
isLLCCorrectionEnabledAsynch(BooleanCallback* cb) {
  ACS_TRACE(__PRETTY_FUNCTION__);
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    c.log();
    cb->report(getAntennaName().c_str(), false, c);
  }
  allocatedAntenna_m->isLLCCorrectionEnabledAsynch(cb);
}

float AntInterferometryControllerImpl::getLLCTimeToReset(){
  ACS_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  }
  try {
    return allocatedAntenna_m->getLLCTimeToReset();
  } catch (HardwareErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getHardwareErrorEx();
  } catch (InvalidRequestEx& ex) {
    InvalidRequestExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getInvalidRequestEx();
  }
}

void AntInterferometryControllerImpl::
getLLCTimeToResetAsynch(DurationCallback* cb){
  ACS_TRACE(__PRETTY_FUNCTION__);
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    c.log();
    cb->report(getAntennaName().c_str(), 0, c);
  }
  allocatedAntenna_m->getLLCTimeToResetAsynch(cb);
}

Control::LLC::LLCPositionResetData_t 
AntInterferometryControllerImpl::resetLLCStretcher(){
  ACS_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    return allocatedAntenna_m->resetLLCStretcher();
  } catch (HardwareErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getHardwareErrorEx();
  } catch (InvalidRequestEx& ex) {
    InvalidRequestExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getInvalidRequestEx();
  } catch (TimeoutEx& ex) {
     TimeoutExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
     throw nex.getTimeoutEx();
  } catch (IllegalParameterErrorEx& ex) {
     IllegalParameterErrorExImpl nex (ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
     throw nex.getIllegalParameterErrorEx();
  }
}

void AntInterferometryControllerImpl::  
resetLLCStretcherAsynch(Control::LLCPositionResetCallback* cb) {
  ACS_TRACE(__PRETTY_FUNCTION__);
  Control::LLC::LLCPositionResetData_t resetData; 
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    c.log();
    cb->report(getAntennaName().c_str(), resetData, c);
  }
  allocatedAntenna_m->resetLLCStretcherAsynch(cb);
}

Control::LLC::LLCPositionResetData_t 
AntInterferometryControllerImpl::setLLCStretcherPosition(float position){
  ACS_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  }

  try {
    return allocatedAntenna_m->setLLCStretcherPosition(position);
  } catch (TimeoutEx &ex){
    TimeoutExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getTimeoutEx();
  } catch (HardwareErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getHardwareErrorEx();
  } catch (IllegalParameterErrorEx& ex) {
    IllegalParameterErrorExImpl nex ( __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getIllegalParameterErrorEx();
  } catch (InvalidRequestEx& ex) {
    InvalidRequestExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getInvalidRequestEx();
  }
}

void AntInterferometryControllerImpl::  
setLLCStretcherPositionAsynch(float position,
                              Control::LLCPositionResetCallback* cb) {
  ACS_TRACE(__PRETTY_FUNCTION__);
  Control::LLC::LLCPositionResetData_t resetData; 
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    c.log();
    cb->report(getAntennaName().c_str(), resetData, c);
  }
  allocatedAntenna_m->setLLCStretcherPositionAsynch(position, cb);
}

/* See the IDL file for a description of this function. */
bool AntInterferometryControllerImpl::getLLCPolarizationCalRequired() {
  ACS_TRACE(__PRETTY_FUNCTION__);

  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedExImpl ex(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    ex.log();
    throw ex.getUnallocatedEx();
  }
  
  try {
   return allocatedAntenna_m->getLLCPolarizationCalRequired();
  } catch (TimeoutEx &ex){
     TimeoutExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
     throw nex.getTimeoutEx();
  } catch (HardwareErrorEx& ex) {
    HardwareErrorExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getHardwareErrorEx();
  } catch (InvalidRequestEx& ex) {
    InvalidRequestExImpl nex(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
    throw nex.getInvalidRequestEx();
  }
}
        
void AntInterferometryControllerImpl::
getLLCPolarizationCalRequiredAsynch(BooleanCallback* cb){
  ACS_TRACE(__PRETTY_FUNCTION__);
  bool required = false;
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    c.log();
    cb->report(getAntennaName().c_str(), required, c);
  }
  allocatedAntenna_m->getLLCPolarizationCalRequiredAsynch(cb);

}

void AntInterferometryControllerImpl::
doLLCPolarizationCalibration(ACS::Time timeout,
                             bool forceCalibration,
                             AntennaCallback* cb) {
  if (status_m == Control::AntModeController::UNINITIALIZED) {
    UnallocatedCompletion c(__FILE__, __LINE__, __PRETTY_FUNCTION__);
    c.log();
    cb->report(getAntennaName().c_str(), c);
  }
  allocatedAntenna_m->doLLCPolarizationCalibration(timeout, forceCalibration, cb);
  
}

void AntInterferometryControllerImpl::setWVRIntegrationTime(ACS::TimeInterval interval) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Do not do anything if we do not have a WVR;
    if (!checkWVRIsOK()) return;

    // TODO. Do the actual work here.

}
#include <simpleCallbackImpl.h>
#include <LogToAudience.h>

void AntInterferometryControllerImpl::
setWVRIntegrationTimeCB(ACS::TimeInterval interval, AntModeControllerCB* cb) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    try {
        setWVRIntegrationTime(interval);
    } catch (ModeControllerExceptions::HardwareFaultEx& ex) {
        ModeControllerExceptions::HardwareFaultCompletion 
            c(ex, __FILE__, __LINE__, __PRETTY_FUNCTION__);
        cb->report(getAntennaName().c_str(), c);
        return;
    }

    // Notify callback of success
    ACSErrTypeOK::ACSErrOKCompletion okComplete;
    cb->report(getAntennaName().c_str(), okComplete);
}

ACS::TimeInterval AntInterferometryControllerImpl::
getWVRIntegrationTime() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Do not do anything if we do not have a WVR;
    if (!checkWVRIsOK()) return 0;

    // TODO. Do the actual work here.
    return 480000ULL*24;
}

bool AntInterferometryControllerImpl::isWVRSendingData() {
    return checkWVRIsOK();
}

template <typename T> maci::SmartPtr<T> 
AntInterferometryControllerImpl::getOptionalComponent(const std::string& name) {
    try {
        CORBA::String_var componentName;  
        componentName = allocatedAntenna_m->
            getSubdeviceName(name.c_str());
        return getContainerServices()->
          getComponentNonStickySmartPtr<T>(componentName);
    } catch (IllegalParameterErrorEx ex) {
        IllegalParameterErrorExImpl newEx(ex);
        newEx.log(LM_DEBUG);
    } catch (maciErrType::CannotGetComponentExImpl& ex) {
        ex.log(LM_DEBUG);
    }
    string msg = "Could not find a " + name + " component on antenna ";
    msg += getAntennaName() + ". Will proceed without one.";
    msg += " Some capabilities will not be available.";
    LOG_TO_OPERATOR(LM_WARNING, msg);
    maci::SmartPtr<T> nilReturn(getContainerServices(), 
                                T::_nil(), false);

    return nilReturn;
}

bool AntInterferometryControllerImpl::checkWVRIsOK() {
    const string antName = getAntennaName();
    if (antName.length() == 0) {
        string msg = "Antenna name is empty.";
        msg += " Has the mode controller been allocated to an antenna?";
        LOG_TO_DEVELOPER(LM_ERROR, msg); 
        return false;
    }

    if (wvr_m.isNil()) {
        string msg = "WVR reference is null.";
        msg += " Does antenna " + antName + " have a WVR in its TMCDB configuration?";
        LOG_TO_DEVELOPER(LM_ERROR, msg); 
        return false;
    }
    // So we think we have a reference, but lets checks its really
    // there. Because we have a non-sticky reference it could be released
    // by the master.
    try {
        Control::HardwareDevice::HwState state = wvr_m->getHwState();
        if (state != Control::HardwareDevice::Operational &&
            state != Control::HardwareDevice::Simulation) {
            string msg = "The WVR component is not operational.";
            msg += " Check that antenna " + antName + " is operational";
            msg += " and does not have any errors related to the WVR.";
            LOG_TO_DEVELOPER(LM_ERROR, msg); 
            return false;
        }
    } catch (CORBA::SystemException& ex) {
        string msg = "Cannot communicate with the WVR component.";
        msg += " Perhaps the WVR component is shutdown";
        msg += " or its container has crashed.";
        msg += " Check that antenna " + antName + " is operational";
        msg += " and does not have any errors related to the WVR.";
        LOG_TO_DEVELOPER(LM_ERROR, msg); 
        return false;
    }
    return true;
}

//-----------------------------------------------------------------------------
// Lifecycle Methods
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Mode Controller Methods
//-----------------------------------------------------------------------------

void AntInterferometryControllerImpl::
acquireReferences(const string& antennaName) {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // Remove any old references (in case this function is executed twice).
    // But only do this if necessary.
    if (antennaName != getAntennaName()) releaseReferences();

    // If the antenna is already initialized it means we already have the
    // references for the correct antenna. So there is nothing left to do.
    if (getStatus() != Control::AntModeController::UNINITIALIZED) return;
    
    // This gets a reference to the antenna component
    AntModeControllerImpl::acquireReferences(antennaName);
  
    wvr_m = getOptionalComponent<Control::WVR>("WVR");
}

void AntInterferometryControllerImpl::releaseReferences() {
    AUTO_TRACE(__PRETTY_FUNCTION__);

    // release the managed hardware components. The smart pointer will call
    // releaseComponent if necessary.
    wvr_m.release();

    // Set the state to uninitialized, the antenna name to an empty string and
    // release the antenna reference.
    AntModeControllerImpl::releaseReferences();
}

//-----------------------------------------------------------------------------
// MACI DLL support functions
//-----------------------------------------------------------------------------
#include <maciACSComponentDefines.h>
MACI_DLL_SUPPORT_FUNCTIONS(AntInterferometryControllerImpl)
