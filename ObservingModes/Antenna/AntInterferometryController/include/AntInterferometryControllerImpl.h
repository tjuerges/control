// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2007 - 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//
#ifndef ANTINTERFEROMETRYCONTROLLERIMPL_H
#define ANTINTERFEROMETRYCONTROLLERIMPL_H

// Base class(es)
#include <antModeControllerImpl.h>

//CORBA servant header
#include <AntInterferometryControllerS.h>

//includes for data members
#include <acsComponentSmartPtr.h> // for maci::SmartPtr

// Forward declarations for classes that this component uses
namespace Control {
    class WVR;
}

namespace Control {
    class AntInterferometryControllerImpl :
        public virtual POA_Control::AntInterferometryController,
        public Control::AntModeControllerImpl
    {
    public:
        
        /// The constructor for any ACS C++ component must have this signature
        AntInterferometryControllerImpl(const ACE_CString& name,
                                      maci::ContainerServices* cs);
    
        /// The destructor does nothing special. It must be virtual because
        /// this class contains virtual functions.
        virtual ~AntInterferometryControllerImpl();
    
        // --------------------- CORBA interface --------------------------
    
        /* See the IDL file for a description of this function. */
        virtual void enableLLCCorrection(bool);

        /* See the IDL file for a description of this function. */
        virtual void enableLLCCorrectionAsynch(bool enable, 
                                               AntennaCallback* cb);

        /* See the IDL file for a description of this function. */
        virtual bool isLLCCorrectionEnabled();

        /* See the IDL file for a description of this function. */
        virtual void isLLCCorrectionEnabledAsynch(BooleanCallback* cb);

        /* See the IDL file for a description of this function. */
        virtual float getLLCTimeToReset();

        /* See the IDL file for a description of this function. */
        virtual void getLLCTimeToResetAsynch(DurationCallback* cb);

        /* See the IDL file for a description of this function. */
        virtual Control::LLC::LLCPositionResetData_t resetLLCStretcher();

        /* See the IDL file for a description of this function. */
        virtual void 
          resetLLCStretcherAsynch(Control::LLCPositionResetCallback* cb);

        /* See the IDL file for a description of this function. */
        virtual Control::LLC::LLCPositionResetData_t 
          setLLCStretcherPosition(float position);

        /* See the IDL file for a description of this function. */
        virtual void setLLCStretcherPositionAsynch(float position,
                                      Control::LLCPositionResetCallback* cb);

        /* See the IDL file for a description of this function. */
        virtual bool getLLCPolarizationCalRequired();

        /* See the IDL file for a description of this function. */
        virtual void getLLCPolarizationCalRequiredAsynch(BooleanCallback* cb);

        /* See the IDL file for a description of this function. */
        virtual void doLLCPolarizationCalibration(ACS::Time timeout,
                                                  bool forceCalibration,
                                                  AntennaCallback* cb);

        /// See the IDL file for a description of this function.
        virtual ACS::TimeInterval getWVRIntegrationTime();

        /// See the IDL file for a description of this function.
        /// \exception ModeControllerExceptions::HardwareFaultEx
        virtual void setWVRIntegrationTime(ACS::TimeInterval interval);

        /// See the IDL file for a description of this function.
        virtual void setWVRIntegrationTimeCB(ACS::TimeInterval interval,
                                             Control::AntModeControllerCB* cb);
        /// See the IDL file for a description of this function.
        bool isWVRSendingData();

    private:
        // ------------------- Mode Controller Methods --------------------    

        /// Get a reference to a component with the specified type e.g., "LORR"
        /// on the currently allocated antenna. Return a nill reference if this
        /// cannot be done.
        template <typename T>
        maci::SmartPtr<T> getOptionalComponent(const std::string& name);

        /// Returns false unless we can communicate with the WVR and determine
        /// its operational.
        bool checkWVRIsOK();

        /// Configure this component to use the equipment in the specified
        /// antenna. The supplied argument is an antenna name like "DV01" and
        /// the associated antenna component must already be started (by the
        /// master component). This function will then query the antenna
        /// component to get the references to the line-length corrector
        /// (anipulation of teh Antenna based LO components is done through the
        /// LocxalOscillato/AntLOController). This function can be called twice
        /// and if so it will release the equipment in the antenna it was
        /// previously using and acquire the equipment in the new antenna.
        /// \param antennaName 
        /// Name of antenna component to use e.g., "DV01" 
        /// \exception ModeControllerExceptions::BadConfigurationEx
        /// A BadConfiguration exception is generated if the antenna does not
        /// have a mount component
        /// \exception ModeControllerExceptions::CannotGetAntennaEx
        /// A CannotGetAntenna exception is generated if the the specified
        /// antenna component cannot be contacted
        virtual void acquireReferences(const std::string& antennaName);
    
        /// Restore this component to the state it was in after
        /// construction. In this state this component does not have a
        /// reference to any components and none of its functions, except
        /// allocate & getState should be used.
        virtual void releaseReferences();
    
        // The copy constructor is made private to prevent a compiler generated
        // one from being used. It is not implemented.
        AntInterferometryControllerImpl
          (const AntInterferometryControllerImpl&);
    
        /// References to the antenna-based hardware devices components
        maci::SmartPtr<Control::WVR> wvr_m;

    };
} // End Control namespace
#endif // ANTINTERFEROMETRYCONTROLLERIMPL_H
