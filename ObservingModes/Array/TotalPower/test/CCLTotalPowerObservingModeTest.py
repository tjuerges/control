#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

# TODO: Fix the releaseComponent in tearDown

from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Util.XmlObjectifier import XmlObject
from CCL.TotalPowerObservingMode import TotalPowerObservingMode
from CCL.logging import getLogger
from CCL.ScanIntent import ScanIntent
import Control
import ScanIntentMod
import ObservingModeTester_idl
import unittest

class CCLTotalPowerObservingModeTest(unittest.TestCase):
    
    def setUp(self):
        self.logger = getLogger()
        self.client = PySimpleClient()
        try:
          # Insert methods into python simulator
          testutilCompName = "CONTROL/ObservingModeTester"
          simidl           = "IDL:alma/ACSSim/Simulator:1.0"
          self.testutil       = self.client.getComponent(testutilCompName)
          self.simulator = self.client.getDefaultComponent(simidl)
          self.testutil.modifySimulationCode(self.simulator) 
        except Exception, e:
          print "Exception modifying simulator code:\n" + str(e) 
          self.logger.severe("Exception:" + str(e)) 
          raise
        # Not strictly correct. The Total Power Observing Mode offshoot is not
        # a component, but it works.
        self.tpOffshoot = self.client.getDynamicComponent("*",
                                 "IDL:alma/Control/TotalPowerObservingMode:1.0",
                                 "*",
                                 "*")
        self.tp = TotalPowerObservingMode(self.tpOffshoot, self.logger)
        
    def tearDown(self):
        self.client.releaseComponent(self.simulator._get_name())
        self.client.releaseComponent(self.testutil._get_name())

    def testTotalPowerObservingMode(self):
        self.tp.beginScan(ScanIntent(ScanIntentMod.OBSERVE_TARGET))
        self.tp.endScan()
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(CCLTotalPowerObservingModeTest("testTotalPowerObservingMode"))
    return suite


if __name__ == '__main__':
    unittest.main(defaultTest='suite')
