/*
 * "@(#) $Id$"
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2008
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 */

package alma.Control.ObservingModes;

import java.io.File;
import java.io.FileOutputStream;
import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.BasebandNameMod.BasebandName;
import alma.Control.AntennaCharacteristics;
import alma.Control.BDDStreamInfo;
import alma.Control.Common.Util;
import alma.Control.ResourceId;
import alma.Control.ScanEndedEvent;
import alma.Control.ScanPurpose;
import alma.Control.ScanStartedEvent;
import alma.Control.SubscanEndedEvent;
import alma.Control.SubscanStartedEvent;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.StaticResource;
import alma.Control.Common.ACSTestEnvironment;
import alma.Control.Common.ContainerSpec;
import alma.ControlSB.Source;
import alma.ControlSB.SpectralSpec;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ScanIntentMod.ScanIntent;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.component.client.ComponentClient;
import alma.acs.container.ContainerServices;
import alma.acs.nc.CorbaNotificationChannel;
import alma.acs.nc.SimpleSupplier;
import alma.acs.nc.Receiver;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.offline.DataCapturer;
import alma.xmlentity.XmlEntityStruct;
import alma.xmlstore.Identifier;

public class TotalPowerObservingModeTest extends TestCase {
    /** Number of tests to run before shutting down ACS */
    static final int NTESTS = 1;

    /** Counter for the test number. Necessary to shutdown ACS. */
    static int testCount = NTESTS;

    /** Whether ACS was started by the test suite */
    static boolean startedACS = false;

    /** ACS Component client */
    static ComponentClient compClient = null;

    /** Log file streams */
    static FileOutputStream[] logs;

    private ACSTestEnvironment acsenv = null;

    private ContainerServices container = null;

    private Logger logger;
    private ResourceManager resMng;
    private Simulator sim;
    private TotalPowerObservingModeImpl obsMode;
    private ObservingModeArray array;
    private boolean scanStartedEventReceived;
    private ScanStartedEvent scanStartedEvent;
    private boolean scanEndedEventReceived;
    private ScanEndedEvent scanEndedEvent;
    private boolean subScanStartedEventReceived;
    private SubscanStartedEvent subScanStartedEvent;
    private boolean subScanEndedEventReceived;
    private SubscanEndedEvent subScanEndedEvent;

    // //////////////////////////////////////////////////////////////////
    // Constructors
    // //////////////////////////////////////////////////////////////////

    public TotalPowerObservingModeTest(String test) {
        super(test);
    }

    // //////////////////////////////////////////////////////////////////
    // Test fixture methods
    // //////////////////////////////////////////////////////////////////

    protected void setUp() throws Exception {

        ContainerSpec[] cont = new ContainerSpec[0];
        String[] env = { "ACS_CDB=." };
        acsenv = new ACSTestEnvironment(cont, env);

        File tempDir = new File("./tmp");
        if (tempDir.mkdir()) {
            System.out.println("--- ./tmp directory created");
        } else {
            System.out.println("--- ./tmp directory already exists");
        }
        if (!acsenv.isACSRunning(0)) {
            logs = new FileOutputStream[5];
            logs[0] = new FileOutputStream("./tmp/CONTROL_ACC_pythonContainer.log");
            logs[1] = new FileOutputStream("./tmp/CONTROL_ACC_javaContainer.log");
            logs[2] = new FileOutputStream("./tmp/ACC_javaContainer.log");
            logs[3] = new FileOutputStream("./tmp/ACS.log");
            logs[4] = new FileOutputStream("./tmp/ARCHIVE.log");
            acsenv.startACS(logs[3]);
            acsenv.startContainers(System.out);
            acsenv.startContainer("CONTROL/ACC/pythonContainer", "py", logs[0]);
            acsenv.startContainer("CONTROL/ACC/javaContainer", "java", logs[1]);
            acsenv.startContainer("ACC/javaContainer", "java", logs[2]);
            acsenv.startArchive(logs[4]);
            Thread.sleep(5000); // TODO I should find a way to synch. with the
            // containers
            startedACS = true;
        } else {
            System.out.println("--- ACS is already running.");
            if (testCount == NTESTS)
                startedACS = false;
        }

        String managerLoc = System.getenv("MANAGER_REFERENCE");
        System.out.println("--- managerLoc = " + managerLoc);
        if (compClient == null) {
            compClient = new ComponentClient(null, managerLoc, "TotalPowerTest");
        }
        container = compClient.getContainerServices();
        logger = container.getLogger();
        this.sim = ObservingModeTesterImpl.modifyPythonSimCode(container);

        this.array  = ObservingModeTesterImpl.getFakeArray();
        this.resMng = ResourceManager.getInstance(container);
        Resource<Identifier> idres = new StaticResource<Identifier>(container, 
            "ARCHIVE_IDENTIFIER", "alma.xmlstore.IdentifierHelper", true);
        resMng.addResource(idres);
        Resource<DataCapturer> dcres = new DynamicResource<DataCapturer>(container, 
            array.getDataCapturerName(), "IDL:alma/offline/DataCapturer:1.0");
        resMng.addResource(dcres);
        Resource<SimpleSupplier> pubres = new PublisherResource(container, "CONTROL_SYSTEM", true);
        resMng.addResource(pubres);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            logger.severe(msg
                    + ":\n"
                    + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            fail(msg);
        }

        obsMode = null;

        scanStartedEventReceived = false;
        scanStartedEvent = null;

        scanEndedEventReceived = false;
        scanEndedEvent = null;

        subScanStartedEventReceived = false;
        subScanStartedEvent = null;

        subScanEndedEventReceived = false;
        subScanEndedEvent = null;

        super.setUp();
    }

    protected void tearDown() throws Exception {
        testCount--;

        container.releaseComponent(sim.name());
        resMng.releaseResources();

        // Doing this will kill any CCL test that follows... So don't do it!
        /*
        if (startedACS && testCount == 0) {
            acsenv.stopArchive(logs[4]);
            acsenv.shutdownContainers(null);
            acsenv.shutdownACS(logs[3]);
            for (int i = 0; i < logs.length; i++)
                logs[i].close();
        }
        */

        super.tearDown();
    }

    // //////////////////////////////////////////////////////////////////
    // Test cases
    // //////////////////////////////////////////////////////////////////

    public void testBasic() throws Exception {
        TotalPowerObservingModeImpl obsMode = 
            new TotalPowerObservingModeImpl(container, array, "Array001/TP");
        assertEquals("yes", "yes");
    }

    public void testInitializeHardware() throws Exception {
    //    	
    // // Configure the total power mode controllers simulators
    // AntennaCharacteristics[] ac = array.getAntennaConfigurations();
    // for(int i=0; i<ac.length; i++) {
    // String mcn = String.format("%s%03d", "Array001/TP", i+1);
    //
    // String code = "LOGGER.logInfo('allocate() called')\n";
    // code += "from Acssim.Goodies import setGlobalData\n";
    // code += "antennaName = parameters[0]\n";
    // code += "setGlobalData('"+mcn+":antennaName', antennaName)\n";
    // code += "None";
    // simulator.setMethod(mcn, "allocate", code, 0.0);
    //
    // code = "LOGGER.logInfo('initializeHardwareAsynch called')\n";
    // code += "callback = parameters[0]\n";
    // code += "import ACSErr\n";
    // code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
    // code += "from Acssim.Goodies import getGlobalData\n";
    // code += "antennaName = getGlobalData('"+mcn+":antennaName')\n";
    // code += "callback.report(antennaName, compl)\n";
    // code += "None";
    // simulator.setMethod(mcn, "initializeHardwareAsynch", code, 0.0);
    //            
    // }
    //    	        
    // // Create the total power observing mode
    // TotalPowerObservingModeImpl obsMode =
    // new TotalPowerObservingModeImpl(container,
    // array,
    // "Array001/TP");
    // obsMode.initializeHardware();
    //        
    //    	
    }
        
    public void testShutdownHardware() throws Exception {
    //
    // // Configure the total power mode controllers simulators
    // AntennaCharacteristics[] ac = array.getAntennaConfigurations();
    // for(int i=0; i<ac.length; i++) {
    // String mcn = String.format("%s%03d", "Array001/TP", i+1);
    //
    // String code = "LOGGER.logInfo('allocate() called')\n";
    // code += "from Acssim.Goodies import setGlobalData\n";
    // code += "antennaName = parameters[0]\n";
    // code += "setGlobalData('"+mcn+":antennaName', antennaName)\n";
    // code += "None";
    // simulator.setMethod(mcn, "allocate", code, 0.0);
    //
    // code = "LOGGER.logInfo('shutdownHardwareAsynch called')\n";
    // code += "callback = parameters[0]\n";
    // code += "import ACSErr\n";
    // code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
    // code += "from Acssim.Goodies import getGlobalData\n";
    // code += "antennaName = getGlobalData('"+mcn+":antennaName')\n";
    // code += "callback.report(antennaName, compl)\n";
    // code += "None";
    // simulator.setMethod(mcn, "shutdownHardwareAsynch", code, 0.0);
    //            
    // }
    //    	        
    // // Create the total power observing mode
    // TotalPowerObservingModeImpl obsMode =
    // new TotalPowerObservingModeImpl(container,
    // array,
    // "Array001/TP");
    // obsMode.shutdownHardware();
    }

    public synchronized void testBeginScan() throws Exception {
        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver("CONTROL_SYSTEM", container);
        receiver.attach("alma.Control.ScanStartedEvent", this);
        receiver.begin();

        // Install some behavior in the DataCapturer simulator
        String code = "LOGGER.logInfo('startScan() called')\n";
        code += "LOGGER.logInfo('arrayId: ' + parameters[0])\n";
        code += "LOGGER.logInfo('startTime: ' + str(parameters[1]))\n";
        code += "LOGGER.logInfo('scanNumber: ' + str(parameters[2]))\n";
        code += "LOGGER.logInfo('intent: ' + str(parameters[3]))\n";
        code += "None";
        sim.setMethod(array.getDataCapturerName(), "startScan", code, 0.0);

        // // Construct the obs. mode and call beginScan
        obsMode = new TotalPowerObservingModeImpl(container, array, "Array001/TP");
        ScanIntent[] intents = {ScanIntent.OBSERVE_TARGET};
        //obsMode.beginScan(intents, createDummySource());

        // // Wait for the ScanStartedEvent
        // while (!scanStartedEventReceived) {
        // try {
        // wait(5000);
        // } catch (InterruptedException ex) {
        // String msg = "Interrupted when waiting for event.";
        // logger.severe(msg);
        // fail(msg);
        // }
        // }

        // assertEquals(1, array.getScanNumber());
        // assertEquals(0, array.getSubScanNumber());
    }

    public synchronized void testEndScan() throws Exception {

        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver("CONTROL_SYSTEM", 
            container);
        receiver.attach("alma.Control.ScanEndedEvent", this);
        receiver.begin();

        // Install some behavior in the DataCapturer simulator
        String code = "LOGGER.logInfo('endScan() called')\n";
        code += "LOGGER.logInfo('arrayId: ' + parameters[0])\n";
        code += "LOGGER.logInfo('startTime: ' + str(parameters[1]))\n";
        code += "LOGGER.logInfo('scanNumber: ' + str(parameters[2]))\n";
        code += "LOGGER.logInfo('numberOfSubScans: ' + str(parameters[3]))\n";
        code += "None";
        sim.setMethod(array.getDataCapturerName(), "endScan", code, 0.0);

        // Construct the obs. mode and call beginScan
        obsMode = new TotalPowerObservingModeImpl(container, array, 
            "Array001/TP");
        //obsMode.endScan();

        // Wait for the ScanEndedEvent
        //while (!scanEndedEventReceived) {
        //    try {
        //        wait(5000);
        //    } catch (InterruptedException ex) {
        //        String msg = "Thread interrupted when waiting for event.";
        //        logger.severe(msg);
        //        fail(msg);
        //    }
        //}
    }

    public synchronized void testBeginSubScan() throws Exception {

        // // Create a consumer for the ExecBlockEndedEvent.
        // Receiver receiver;
        // logger.info("Creating NC Receiver and subscribing to events...");
        // receiver =
        // AbstractNotificationChannel.getReceiver(AbstractNotificationChannel.CORBA,
        // "CONTROL_SYSTEM", container);
        // receiver.attach("alma.Control.ScanStartedEvent", this);
        // receiver.attach("alma.Control.SubscanStartedEvent", this);
        // receiver.begin();

        // // Install some behavior in the DataCapturer simulator
        // String code = "LOGGER.logInfo('startTotalPowerSubscan() called')\n";
        // code += "None";
        // simulator.setMethod(array.getDataCapturerName(),
        // "startTotalPowerSubscan", code, 0.0);

        // // Configure the total power mode controllers simulators
        // AntennaCharacteristics[] ac = array.getAntennaConfigurations();
        // for(int i=0; i<ac.length; i++) {
        // String mcn = String.format("%s%03d", "Array001/TP", i+1);

        // code = "LOGGER.logInfo('allocate() called')\n";
        // code += "from Acssim.Goodies import setGlobalData\n";
        // code += "antennaName = parameters[0]\n";
        // code += "setGlobalData('"+mcn+":antennaName', antennaName)\n";
        // code += "None";
        // simulator.setMethod(mcn, "allocate", code, 0.0);

        // code = "LOGGER.logInfo('"+mcn+": recordPositionDataAsynch
        // called')\n";
        // code += "LOGGER.logInfo('"+mcn+": startTime: ' +
        // str(parameters[0]))\n";
        // code += "LOGGER.logInfo('"+mcn+": endTime: ' +
        // str(parameters[1]))\n";
        // code += "LOGGER.logInfo('"+mcn+": callback: ' +
        // str(parameters[2]))\n";
        // code += "callback = parameters[2]\n";
        // code += "import ACSErr\n";
        // code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        // code += "from Acssim.Goodies import getGlobalData\n";
        // code += "antennaName = getGlobalData('"+mcn+":antennaName')\n";
        // code += "LOGGER.logInfo('"+mcn+": reporting callback for antenna ' +
        // antennaName)\n";
        // code += "callback.report(antennaName, compl)\n";
        // code += "None";
        // simulator.setMethod(mcn, "recordPositionDataAsynch", code, 0.0);
        // }

        // // Construct the obs. mode
        // obsMode = new TotalPowerObservingModeImpl(container,
        // array,
        // "Array001/TP");

        // // In order to call beginSubscan(), it is needed to call
        // // beginScan() first. The Source object that is passed in the second
        // // paramenter is sent to DataCapturer in beginSubscan().
        // ScanIntent[] intents = { ScanIntent.TARGET };
        // obsMode.beginScan(intents, createDummySource());

        // // Wait for the ScanStartedEvent
        // while (!scanStartedEventReceived) {
        // try {
        // wait(5000);
        // } catch (InterruptedException ex) {
        // String msg = "Thread interrupted when waiting for event.";
        // logger.severe(msg);
        // fail(msg);
        // }
        // }

        // // TODO: here we force the LOObsMode reference to be taken
        // obsMode.getLOObservingMode();

        // Field fld = createDummyField();
        // SpectralSpec spec = createDummySpectralSpec();
        // alma.Control.TotalPowerConfiguration config =
        // createDummyTotalPowerConfiguration();
        // SubscanIntent[] subIntents = { SubscanIntent.ON_SOURCE };

        // obsMode.beginSubscan(fld, spec, config, subIntents);

        // // Wait for the ScanEndedEvent
        // while (!subScanStartedEventReceived) {
        // try {
        // wait(5000);
        // } catch (InterruptedException ex) {
        // String msg = "Thread interrupted when waiting for event.";
        // logger.severe(msg);
        // fail(msg);
        // }
        // }

    }

    public synchronized void testEndSubScan() throws Exception {

        // // Create a consumer for the ExecBlockEndedEvent.
        // Receiver receiver;
        // logger.info("Creating NC Receiver and subscribing to events...");
        // receiver =
        // AbstractNotificationChannel.getReceiver(AbstractNotificationChannel.CORBA,
        // "CONTROL_SYSTEM", container);
        // receiver.attach("alma.Control.SubscanEndedEvent", this);
        // receiver.attach("alma.Control.ScanStartedEvent", this);
        // receiver.attach("alma.Control.SubscanStartedEvent", this);
        // receiver.begin();

        // ScanPurpose[] purposes = new ScanPurpose[1];
        // purposes[0] = ScanPurpose.TARGET;

        // // Install some behavior in the DataCapturer simulator
        // String code = "LOGGER.logInfo('startTotalPowerSubscan() called')\n";
        // code += "None";
        // simulator.setMethod(array.getDataCapturerName(),
        // "startTotalPowerSubscan", code, 0.0);

        // code = "LOGGER.logInfo('endSubscan called')\n";
        // code += "None";
        // simulator.setMethod(array.getDataCapturerName(), "endSubscan", code,
        // 0.0);

        // // Configure the total power mode controllers simulators
        // AntennaCharacteristics[] ac = array.getAntennaConfigurations();
        // for(int i=0; i<ac.length; i++) {
        // String mcn = String.format("%s%03d", "Array001/TP", i+1);

        // code = "LOGGER.logInfo('allocate() called')\n";
        // code += "from Acssim.Goodies import setGlobalData\n";
        // code += "antennaName = parameters[0]\n";
        // code += "setGlobalData('"+mcn+":antennaName', antennaName)\n";
        // code += "None";
        // simulator.setMethod(mcn, "allocate", code, 0.0);

        // code = "LOGGER.logInfo('"+mcn+": recordPositionDataAsynch
        // called')\n";
        // code += "LOGGER.logInfo('"+mcn+": startTime: ' +
        // str(parameters[0]))\n";
        // code += "LOGGER.logInfo('"+mcn+": endTime: ' +
        // str(parameters[1]))\n";
        // code += "LOGGER.logInfo('"+mcn+": callback: ' +
        // str(parameters[2]))\n";
        // code += "callback = parameters[2]\n";
        // code += "import ACSErr\n";
        // code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
        // code += "from Acssim.Goodies import getGlobalData\n";
        // code += "antennaName = getGlobalData('"+mcn+":antennaName')\n";
        // code += "LOGGER.logInfo('"+mcn+": reporting callback for antenna ' +
        // antennaName)\n";
        // code += "callback.report(antennaName, compl)\n";
        // code += "None";
        // simulator.setMethod(mcn, "recordPositionDataAsynch", code, 0.0);
        // }

        // // Construct the obs. mode
        // obsMode = new TotalPowerObservingModeImpl(container,
        // array,
        // "Array001/TP");

        // // In order to call beginSubscan(), it is needed to call
        // // beginScan() first. The Source object that is passed in the second
        // // parameter is sent to DataCapturer in beginSubscan().
        // ScanIntent[] intents = { ScanIntent.TARGET };
        // obsMode.beginScan(intents, createDummySource());

        // // Wait for the ScanStartedEvent
        // while (!scanStartedEventReceived) {
        // try {
        // wait(5000);
        // } catch (InterruptedException ex) {
        // String msg = "Timeout when waiting for event.";
        // logger.severe(msg);
        // fail(msg);
        // }
        // }

        // // Call beginSubscan. In order to call endSubScan() it is needed to
        // call
        // // beginSubscan() first because the IntegrationDetails structure,
        // which
        // // endSubscan() uses, is setup in beginSubscan().
        // Field fld = createDummyField();
        // SpectralSpec spec = createDummySpectralSpec();
        // alma.Control.TotalPowerConfiguration config =
        // createDummyTotalPowerConfiguration();
        // SubscanIntent[] subIntents = { SubscanIntent.ON_SOURCE };

        // obsMode.beginSubscan(fld, spec, config, subIntents);

        // // Wait for the ScanEndedEvent
        // while (!subScanStartedEventReceived) {
        // try {
        // wait(5000);
        // } catch (InterruptedException ex) {
        // String msg = "Thread interrupted when waiting for event.";
        // logger.severe(msg);
        // fail(msg);
        // }
        // }

        // logger.info("Calling endSubscan()");

        // obsMode.endSubscan();

        // // Wait for the ScanEndedEvent
        // logger.info("Waiting to receive SubscanEndedEvent.");
        // while (!subScanEndedEventReceived) {
        // try {
        // wait(5000);
        // } catch (InterruptedException ex) {
        // String msg = "Thread interrupted when waiting for event.";
        // logger.severe(msg);
        // fail(msg);
        // }
        // }
    }

    // //////////////////////////////////////////////////////////////////
    // Event reception methods
    // //////////////////////////////////////////////////////////////////

    public synchronized void receive(ScanStartedEvent event) {
        logger.info("Received ScanStartedEvent.");
        scanStartedEvent = event;
        scanStartedEventReceived = true;
        notify();
    }

    public synchronized void receive(ScanEndedEvent event) {
        logger.info("Received ScanEndedEvent.");
        scanEndedEvent = event;
        scanEndedEventReceived = true;
        notify();
    }

    public synchronized void receive(SubscanStartedEvent event) {
        logger.info("Received SubscanStartedEvent.");
        subScanStartedEvent = event;
        subScanStartedEventReceived = true;
        notify();
    }

    public synchronized void receive(SubscanEndedEvent event) {
        logger.info("Received SubscanEndedEvent.");
        subScanEndedEvent = event;
        subScanEndedEventReceived = true;
        notify();
    }

    // //////////////////////////////////////////////////////////////////
    // Suite
    // //////////////////////////////////////////////////////////////////

    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "1"; // Default test suite.
        try {
            if (testSuite.equals("1")) {
                suite.addTest(new TotalPowerObservingModeTest("testBasic"));
            } else if (testSuite.equals("2")) {
                suite.addTest(new TotalPowerObservingModeTest("testInitializeHardware"));
            } else if (testSuite.equals("3")) {
                suite.addTest(new TotalPowerObservingModeTest("testShutdownHardware"));
            } else if (testSuite.equals("4")) {
                suite.addTest(new TotalPowerObservingModeTest("testBeginScan"));
            } else if (testSuite.equals("5")) {
                suite.addTest(new TotalPowerObservingModeTest("testEndScan"));
            } else if (testSuite.equals("6")) {
                suite.addTest(new TotalPowerObservingModeTest("testBeginSubScan"));
            } else if (testSuite.equals("7")) {
                suite.addTest(new TotalPowerObservingModeTest("testEndSubScan"));
            }
        } catch (Exception ex) {
            System.err.println("Error when creating TotalPowerObservingModeTest: "
                    + ex.toString());
        }
        return suite;
    }

    // //////////////////////////////////////////////////////////////////
    // Private methods
    // //////////////////////////////////////////////////////////////////

    private SpectralSpec createDummySpectralSpec() {
        SpectralSpec spec = new SpectralSpec(0.0, 0.0, 0.0, "TransitionName",
            alma.ControlSB.ReceiverBandType.NotUsed, 0.0);
        return spec;
    }

    private alma.Control.TotalPowerConfiguration createDummyTotalPowerConfiguration() {

        alma.Control.TotalPowerBaseBandConfig bbc = new alma.Control.TotalPowerBaseBandConfig();
        bbc.baseBandIndex = 1;
        bbc.baseBandName = BasebandName.BB_1;
        bbc.bin0Duration = 1;
        bbc.bin1Duration = 1;
        bbc.numSwitchCycles = 1;
        bbc.windowFunction = alma.Control.eWindows.BARTLETT;
        bbc.polarizations = new alma.Control.TotalPowerPolarization[0];

        alma.Control.TotalPowerBaseBandConfig[] bbcs = { bbc };
        alma.Control.TotalPowerConfiguration config = new alma.Control.TotalPowerConfiguration(1, 1, bbcs);
        return config;
    }

    private XmlEntityStruct createDummySource() {

        String xml = "<?xml version='1.0' encoding='UTF-8'?>"
                + "<FieldSource almatype='APDM::FieldSource' entityPartId=''>"
                + "<sourceCoordinates system='J2000'>"
                + "<ns6:longitude xmlns:ns6='Alma/ValueTypes' unit='rad'>0.0</ns6:longitude>"
                + "<ns7:latitude xmlns:ns7='Alma/ValueTypes' unit='rad'>0.0</ns7:latitude>"
                + "</sourceCoordinates>"
                + "<sourceName>Unknown</sourceName>"
                + "<sourceVelocity referenceSystem=''>"
                + "<ns8:centerVelocity xmlns:ns8='Alma/ValueTypes' unit='km/s'>0.0</ns8:centerVelocity>"
                + "</sourceVelocity>"
                + "<sourceEphemeris>None</sourceEphemeris>"
                + "<pMRA unit='mas/yr'>0.0</pMRA>"
                + "<pMDec unit='mas/yr'>0.0</pMDec>"
                + "<nonSiderealMotion>false</nonSiderealMotion>"
                + "<parallax unit='arcsec'>0.0</parallax>" + "<name>''</name>"
                + "<SourceProperty almatype='APDM::SourceProperty'>"
                + "<sourceFrequency unit='GHz'>0.0</sourceFrequency>"
                + "<sourceFluxI unit='Jy'>0.0</sourceFluxI>"
                + "<sourceDiameter unit='arcsec'>0.0</sourceDiameter>"
                + "</SourceProperty>"
                + "<solarSystemObject>''</solarSystemObject>"
                + "</FieldSource>";

        XmlEntityStruct src = new XmlEntityStruct(xml, "", "FieldSource", "1.0", "");

        return src;
    }
}

// __oOo__
