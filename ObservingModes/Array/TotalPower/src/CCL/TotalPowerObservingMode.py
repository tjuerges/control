#! /usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2011
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

"""
This module is part of the Control Command Language.
Defines the TotalPowerObservingMode class.
"""

from CCL.LocalOscillatorInterferometricInterface import \
     LocalOscillatorInterferometricInterface
from CCL.ObservingModeBase import ObservingModeBase
from CCL.ArrayMountControllerInterface import ArrayMountControllerInterface
from CCL.InterferometryControllerInterface import \
     InterferometryControllerInterface
import Control
import CCL.utils

class TotalPowerObservingMode(ObservingModeBase,
                              LocalOscillatorInterferometricInterface,
                              ArrayMountControllerInterface,
                              InterferometryControllerInterface):
    """
    Total Power Observing Mode CCL Class.
    """

    def __init__(self, tp_offshoot, logger):
        """
        Constructor.
        """
        ObservingModeBase.__init__(self)
        LocalOscillatorInterferometricInterface.__init__(self, tp_offshoot, logger)
        ArrayMountControllerInterface.__init__(self, tp_offshoot, logger)
        InterferometryControllerInterface.__init__(self, tp_offshoot, logger)
        self._offshoot = tp_offshoot
        self.field = None
    
    def getAntennas(self):
        """
        Returns a list containing the antenna names allocated in the array.
        """
        return self._offshoot.getAntennas()

    
    def waitForTarget(self):
        """
        Wait until the system is ready to observe the previously
        specified target. This includes waiting for the antennas to go
        on source and the receiver to be tuned.
        """
        return self._offshoot.waitForTarget();


    def setSamplingFrequency(self, sampleRate):
        '''
        Set the time constant for the total power data.  A "Boxcar"
        weighting function is used so the observer should ensure that
        sufficient oversampling of the source is done.
        
        The averaging is done on integer 2kHz samples, so only durations 
        of an integer number of should be specified.  If a non-integer 
        value is requested the next highest integer sampling frequency is 
        used.  The SampleRate is specified in Hz.
        '''
        self._offshoot.setSamplingFrequency(sampleRate)

    def getSamplingFrequency(self):
        '''
        Returns the current value of the sampling frequency (in Hz)
        '''
        return self._offshoot.getSamplingFrequency()

    def beginScan(self, purposes):
        purposes = CCL.utils.aggregate(purposes)
        scanIntentData = []
        for purpose in purposes:
            scanIntentData.append(purpose.getScanIntentData())
        self._offshoot.beginScan(scanIntentData)
        
    def endScan(self):
        self._offshoot.endScan()

    def setTarget(self, SubscanFieldSource, SubscanSpectralSpec):
        self._offshoot.setTarget(SubscanFieldSource.getXMLString(),
                                   SubscanSpectralSpec.getXMLString())

    def doSubscan(self, subscanDuration, subScanIntents):
        self._offshoot.doSubscan(long(subscanDuration/100E-9), subScanIntents)
 
    def doSubscanSequence(self, subscanSeqSpec):
        '''
        Do a sequence of subscans using the specified parameters. This
        functions does some simple conversions.
        '''
        # TODO. Investigate if this can this be put into the base class
        
        # We need to convert some fields of the SubscanSequenceSpec to
        # strings in order to send the sources and spectral specs over
        # CORBA
        import copy
        sourceEntities = []
        for source in subscanSeqSpec.sources:
            sourceEntities.append(source.getXMLString())

        spectraEntities = []
        for spectra in subscanSeqSpec.spectra:
            spectraEntities.append(spectra.getXMLString())

        # Convert durations from seconds to units of 100ns
        acsSubscans = []
        for subscan in subscanSeqSpec.subscans:
            acsSubscan = copy.copy(subscan)
            acsSubscan.duration = long(acsSubscan.duration/100E-9)
            acsSubscans.append(acsSubscan)

        corbaSpecification = Control.SubscanSequenceSpec(sourceEntities,
                                                         spectraEntities,
                                                         acsSubscans)
        
        self._offshoot.doSubscanSequence(corbaSpecification)
