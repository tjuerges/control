// "@(#) $Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2011
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


package alma.Control.ObservingModes;

import java.util.logging.Logger;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.BasebandNameMod.BasebandName;
import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.Control.AbortionException;
import alma.Control.ArrayMountController;
import alma.Control.ArrayOffset;
import alma.Control.Band2Band3Overlap;
import alma.Control.CorrelatorCalibrationSequenceSpec;
import alma.Control.DSBbasebandSpec;
import alma.Control.FocusCalResult;
import alma.Control.InterferometryController;
import alma.Control.LocalOscillator;
import alma.Control.PointingCalResult;
import alma.Control.SSBbasebandSpec;
import alma.Control.SimpleCallback;
import alma.Control.SourceOffset;
import alma.Control.SourceOffsetFrame;
import alma.Control.SpectralSetup;
import alma.Control.SubscanBoundries;
import alma.Control.SubscanSequenceSpec;
import alma.Control.SubscanSpec;
import alma.Control.TotalPowerBaseBandConfig;
import alma.Control.TotalPowerConfiguration;
import alma.Control.TotalPowerObservingModeOperations;
import alma.Control.TotalPowerPolarization;
import alma.Control.TowerHolography;
import alma.Control.Common.Util;
import alma.Control.NewTPPPackage.SubscanInfo;
import alma.ControlCommonExceptions.AsynchronousFailureEx;
import alma.ControlExceptions.DeviceBusyEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.Correlator.SubscanTiming;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.NetSidebandMod.NetSideband;
import alma.NewTotPwrProcErr.AlreadyShutDownEx;
import alma.NewTotPwrProcErr.NoAntennasEx;
import alma.NewTotPwrProcErr.NoBasebandEx;
import alma.ObservingModeExceptions.AntennasDisagreeEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.PolarizationTypeMod.PolarizationType;
import alma.StokesParameterMod.StokesParameter;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.container.ContainerServices;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.xmlentity.XmlEntityStruct;

/**
 * Total power observing mode implementation. Total power has to sides, Square Law and Autocorrelation:
 * <ul>
 * <li>Square Law: This mode uses the detectors located in the IFProcs. It is safe to assume that this 
 * detectors are always available since they are integrated within the IF Down Converters. This mode makes use
 * of the Square Law Controller({@link SquareLawControllerImpl}) to collect
 * and store the detector data into the ASDMS</li>
 * 
 * <li>Autocorrelation: This mode of observation has no been implemented yet. This mode will configure the
 * Correlator so the autocorrelation data is acquired and stored into the ASDM.</li>
 * </ul>
 * In the future it will be possible to do either or both observation modes at the same time.
 *
 * @author rmarson
 * @author ntroncos (SquareLaw refactoring)
 */
public class TotalPowerObservingModeImpl 
    extends ObservingModeBaseImpl<TowerHolography> 
    implements TotalPowerObservingModeOperations {

    // Controllers we need to use in this mode
    private LocalOscillatorImpl localOscillator = null;
    private ArrayMountControllerImpl amc = null;
    private SquareLawControllerImpl slc = null;
    private InterferometryControllerImpl intController = null;

    // The current source we should use. Its set using setTarget or setSource.
    private FieldSourceT currentSrc = null;

    // The current spectrum specification we should use. Its set using
    // setTarget or setSpectrum.
    private SpectralSpecT currentSpec = null;

    // The offsets of the last subscan in a sequence.
    private SourceOffset curOffset = new SourceOffset(0, 0, 0, 0, SourceOffsetFrame.HORIZON);

    /**
     * @param cs - Container service instance
     * @param array - Observing mode array
     * @param bulkDataDistributorName - String with the bulkDataDistributor name
     * @throws ObsModeInitErrorEx in case any resource cannot be allocated
     */
    public TotalPowerObservingModeImpl(ContainerServices cs,
            ObservingModeArray array, String bulkDataDistributorName)
            throws ObsModeInitErrorEx {
        super(cs, array);

        opLog.info("Total-power observing mode starting up.");

        amc = new ArrayMountControllerImpl(cs, array);
        localOscillator = new LocalOscillatorImpl(cs, array);
        slc = new SquareLawControllerImpl(cs, array, bulkDataDistributorName);
        // TODO. This should not start a delay server.
        try {
            // The following line of code will throw a AcsJBadParameterEx if the
            // array does not have a correlator. It would be nice to have a
            // better interface.            
            array.getCorrelatorObservationControlComponentName();
            // If we get here we have a correlator in the array
            intController = new InterferometryControllerImpl(cs, array);
        } catch (AcsJBadParameterEx e) {
        }
    }

    @Override
    public void cleanUp() {
        opLog.info("Total-power observing mode shutting down.");

        if (localOscillator != null) {
            localOscillator.cleanUp();
            localOscillator = null;
            devLog.debug("Completed deactivating the local oscillator.");
        }

        if (amc != null) {
            amc.cleanUp();
            amc = null;
            devLog.debug("Completed deactivating array mount controller.");
        }

        if (slc != null) {
            slc.cleanUp();
            slc = null;
            devLog.debug("Completed deactivating squarelaw controller.");
        }
        
        // Cleanup the configurations and the array used in by correlator
        if (intController != null) {
            intController.cleanUp();
            intController = null;
            devLog.debug("Completed deactivating the interferometery controller.");
        }

        super.cleanUp();
        devLog.debug("Completed shutting down the total-power observing mode.");
    }
    
    @Override
    public void doSubscan(long subscanDuration, SubscanIntent[] intents)
            throws ObservingModeErrorEx {
        // @TODO. Move this into a base class
        try {
            if (currentSrc == null || currentSpec == null) {
                String msg = "Cannot start a subscan as no target has been specified. Please ensure setTarget has been called.";
                throwObservingModeErrorEx(msg);
            }
            SubscanSequenceSpec subscanSeqSpec = new SubscanSequenceSpec();
            subscanSeqSpec.sources = new String[] { ObservingModeUtilities.serializeFieldSource(
                    currentSrc, logger).xmlString };
            subscanSeqSpec.spectra = new String[] { ObservingModeUtilities.serializeSpectralSpec(
                    currentSpec, logger).xmlString };
            SubscanSpec subscanSpec = new SubscanSpec();
            subscanSpec.intent = intents;
            subscanSpec.sourceId = 0;
            subscanSpec.spectralId = 0;
            subscanSpec.calibrationId = 0; // @TODO. Fix this up
                                           // currentSpec.getCorrelatorConfigurationID();
            subscanSpec.duration = Util.roundTE(subscanDuration);
            SourceOffset zeroOffset = new SourceOffset(0, 0, 0, 0, SourceOffsetFrame.HORIZON);
            subscanSpec.pointingOffset = new ArrayOffset[] { new ArrayOffset(zeroOffset, "Default") };
            subscanSpec.delayCenterOffset = zeroOffset;
            subscanSpec.subreflectorOffset = new double[0];
            subscanSpec.calLoad = CalibrationDevice.NONE;
            subscanSeqSpec.subscans = new SubscanSpec[] { subscanSpec };
            doSubscanSequence(subscanSeqSpec);
        } catch (IllegalParameterErrorEx ex) {
            String msg = "Cannot complete the subscan.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }
   
    @Override
    public void doSubscanSequence(SubscanSequenceSpec spec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // Setup as much as possible before we work out when the sub-scans
        // actually start
        try {
            final int numSubscans = spec.subscans.length;
            FieldSourceT[] sources = new FieldSourceT[numSubscans];
            SpectralSpecT[] spectra = new SpectralSpecT[numSubscans];
            boolean[] sourceChanged = new boolean[numSubscans];
            boolean[] spectrumChanged = new boolean[numSubscans];
            ObservingModeUtilities.extractTargetSequence(spec, currentSrc,
                    currentSpec, logger, sources, sourceChanged, spectra,
                    spectrumChanged);
            boolean usesCorrelator = (intController != null ? true : false)
                    && ObservingModeUtilities.hasCorrelatorConfig(spectra[0],
                            logger);
            int[] calIds = null;
            if (usesCorrelator) {
                calIds = ObservingModeUtilities.extractCalIds(spec.subscans, logger);
             }
            
            if (currentSrc == null || currentSpec == null) {
                // This is the first sub-scan in this SB and the
                // caller has not run setTarget (or setSource & setSpectrum).
                // So setup the hardware before doing any sub-scans, or even
                // estimating the time between sub-scans, to:
                // 1. Check that we have control of the hardware.
                // 2. Allow us to get better estimate of the
                // time between subsequent sub-scans.
                setTarget(sources[0], spectra[0]);
            }
        
            final int firstSubscanNumber = array.getSubScanNumber() + 1;
            int[] subscanNumbers = new int[numSubscans];

            SubscanIntent[][] intents = new SubscanIntent[numSubscans][];
            SourceOffset[] offsets = new SourceOffset[numSubscans];
            double[] antSlewTimes = new double[numSubscans];

            long[] subscanDurations = new long[numSubscans];
            final String[] uids = getUIDs((short) numSubscans);
            SubscanInfo[] ssi = new SubscanInfo[numSubscans];
            final int scanNumber = array.getScanNumber();
            final String asdmId = array.getASDMEntRef().entityId;
            int[] numberOfIntegrations = new int[numSubscans];
            CalibrationDevice[] acdPositions = new CalibrationDevice[numSubscans];
            
            TotalPowerConfiguration[] allTPConfig = new TotalPowerConfiguration[numSubscans];
            
            for (int s = 0; s < numSubscans; s++) {
                final SubscanSpec subscan = spec.subscans[s];
                subscanNumbers[s] = firstSubscanNumber + s;
                intents[s] = subscan.intent;
                acdPositions[s] = subscan.calLoad;
                SourceOffset newOffset = null;
                if (subscan.pointingOffset.length == 0) {
                    newOffset = curOffset;
                } else if (subscan.pointingOffset.length == 1) {
                    newOffset = subscan.pointingOffset[0].offset;
                } else {
                    msg = "Cannot have pointing subarrays in total power mode.";
                    throwIllegalParameterErrorEx(msg);
                }
                offsets[s] = newOffset;
                if (sourceChanged[s]) {
                    antSlewTimes[s] = amc.timeToSlew(currentSrc, curOffset,
                            sources[s], offsets[s]);
                } else {
                    antSlewTimes[s] = 0.0;
                }
                msg = "Estimated antenna slew time before we can start subscan "
                        + subscanNumbers[s] + " is "
                        + String.format("%.3f", antSlewTimes[s]) + " seconds.";
                devLog.debug(msg);
                curOffset = newOffset;
                currentSrc = sources[s];
                // TODO. Find out if Doppler correction should be done *after*
                // we have computed the subscan boundries so that a better time
                // can be used for this correction.                
                long now = ObservingModeUtilities.getCurrentACSTime();
                spectra[s] = localOscillator.dopplerShift(spectra[s], sources[s], now);
                spectra[s] = localOscillator.alignSpectralSpec(spectra[s]);
                subscanDurations[s] = Util.ceilTE(spec.subscans[s].duration);
                allTPConfig[s] = extractTotalPowerConfiguration(
                        spectra[s], subscanDurations[s], logger);
                numberOfIntegrations[s] = (int) (subscanDurations[s]/allTPConfig[s].integrationDuration);
                // Setup as much of the info for the total-power processor as
                // possible leaving just the subscan start and stop times for
                // later

                SubscanInfo thisInfo = new SubscanInfo();
                thisInfo.dataOID = uids[s];
                thisInfo.baseband = ObservingModeUtilities.baseBandIDs(spectra[s]);
                thisInfo.execID = asdmId;
                thisInfo.polarization = new int[] { 0, 1 };
                thisInfo.scanNumber = scanNumber;
                thisInfo.subscanNumber = subscanNumbers[s];
                ssi[s] = thisInfo;
            }
            // Do this *after* we have done the Doppler correction.
            long[] tuningTimes = localOscillator.timeToTune(spectra, acdPositions);
            for (int s = 0; s < numSubscans; s++) {
                msg = "Estimated tuning time before we can start subscan "
                        + subscanNumbers[s] + " is "
                        + String.format("%.3f", tuningTimes[s] * 100E-9)
                        + " seconds.";
                devLog.debug(msg);
            }

            final String arrayName = array.getArrayName();
            dataCapturer.startSubscanSequence(arrayName, intents);
            {
                SourceOffset zeroOffset = new SourceOffset(0.0, 0.0, 0.0, 0.0, 
                        SourceOffsetFrame.HORIZON);
                // The delay center offsets are always zero for TP
                // TODO. Define a better interface to DC for conveying this.
                SourceOffset[] delayOffsets = new SourceOffset[numSubscans];
                // TODO. Replace XmlEntityStruct with a string
                XmlEntityStruct[] asXML = new XmlEntityStruct[numSubscans];
                for (int s = 0; s < numSubscans; s++) {
                    delayOffsets[s] = zeroOffset;
                    amc.fillRADec(sources[s]);
                    asXML[s] = ObservingModeUtilities.serializeFieldSource(
                            sources[s], logger);
                }
                dataCapturer.setSubscanSequenceSource(arrayName, asXML,
                        delayOffsets);
            }
            {
                XmlEntityStruct[] asXML = new XmlEntityStruct[numSubscans];
                for (int s = 0; s < numSubscans; s++) {
                    asXML[s] = ObservingModeUtilities.serializeSpectralSpec(
                            spectra[s], logger);
                }
                dataCapturer.setSubscanSequenceSpectrum(arrayName, asXML);
            }

            dataCapturer.setSubscanSequenceTotalPowerConfig(arrayName,
                    allTPConfig);
            dataCapturer.setSubscanSequenceDataRangeUID(arrayName, uids);

            // Compute the sub-scan start and stop times. The section of code
            // between here and the starting of the first subscan should be
            // lightweight.
            SubscanTiming[] sst = null;
            if (usesCorrelator) {
                sst = intController.getReconfigurationTimes(calIds,
                        subscanDurations);
            }
            alma.Correlator.SubscanInformation[] ssic = new alma.Correlator.SubscanInformation[numSubscans];

            long endOfLastSubscan = ObservingModeUtilities.getCurrentACSTime();
            endOfLastSubscan = Util.ceilTE(endOfLastSubscan);
	    endOfLastSubscan += Util.roundTE(Util.toTimeInterval(0.196 * 3));
            SubscanBoundries[] subscanBoundries = new SubscanBoundries[numSubscans];
            for (int s = 0; s < numSubscans; s++) {
                SubscanInfo thisInfo = ssi[s];
                // Set the minimum time between sub-scans to 0.5
                // seconds. The first subscan needs a longer leadTime
                // of 4 seconds to accommodate the pipeline latencies
                // in the Mount & MountController. This minimum was
                // determined empirically using the
                // SimpleContinuumTest at the OSF on 2010-11-07.
                long minLeadTime = Util.toTimeInterval(0.5);
                if (s == 0)  minLeadTime = Util.toTimeInterval(4.0);
                long leadTime = Math.max(minLeadTime,
                        Util.toTimeInterval(antSlewTimes[s]));
                leadTime = Math.max(leadTime, tuningTimes[s]);
                if (usesCorrelator) leadTime = Math.max(leadTime, sst[s].setupTime);
                leadTime = Util.ceilTE(leadTime);
                msg = "Setup time for subscan " + subscanNumbers[s] + " is "
                        + String.format("%.3f", leadTime * 100E-9)
                        + " seconds.";
                devLog.debug(msg);
                if (usesCorrelator) {
                    sst[s].setupTime = leadTime; 
                    sst[s].duration = subscanDurations[s];
                    ssic[s] = new alma.Correlator.SubscanInformation(scanNumber, subscanNumbers[s], spec.subscans[s].calibrationId, sst[s]);
                }
                thisInfo.startTime = endOfLastSubscan + leadTime;
                thisInfo.endTime = thisInfo.startTime + subscanDurations[s];
                subscanBoundries[s] = new SubscanBoundries(thisInfo.startTime, thisInfo.endTime);
                endOfLastSubscan = thisInfo.endTime;
            }
            // Queue the offsets/strokes and start the collection of
            // pointing data.
            amc.beginSubscanSequence(subscanBoundries, offsets);
            // Queue all the frequency changes
            localOscillator.startSubscanSequence(subscanBoundries, spectra, acdPositions);
            {
                long[] subscanStartTimes = new long[numSubscans];
                long[] subscanEndTimes = new long[numSubscans];
                for (int s = 0; s < numSubscans; s++) {
                    subscanStartTimes[s] = subscanBoundries[s].startTime;
                    subscanEndTimes[s] = subscanBoundries[s].endTime;
                }
                // TODO. Pass the susbcan boundries instead of start and stop times
                dataCapturer.setSubscanSequenceTiming(arrayName,
                        subscanStartTimes, subscanEndTimes);
            }
            
            if (usesCorrelator) {
                long corrStartTime = subscanBoundries[0].startTime - sst[0].setupTime;         
                intController.startSubscanSequence(corrStartTime, ssic,
                        currentSrc, offsets);
            }
            //Configure all total-power sequences
            slc.configureSubscanSequence(allTPConfig, subscanBoundries, ssi);

            for (int s = 0; s < numSubscans; s++) {
                // Kick off the total-power processor
                slc.waitForSubscanStart();
                // Wait for the correlator to start the next subscan
                if (usesCorrelator) intController.waitForSubscanStart();

                final long startTime = ssi[s].startTime;
                final long endTime = ssi[s].endTime;
                // Send the weather data to DC and publish the subscan started
                // event.
                super.beginSubscan(intents[s], startTime, endTime - startTime);
                // For each sub-scan send the sub-reflector position to data
                // capture
                amc.waitForSubscanStart();
                // I expect the LO to be locked within 5.0 seconds of
                // the sub-scan start and if not we bail. Flagging should be used to
                // mark any data collected when the LO's are not locked.
                long timeout = Util.toTimeInterval(5.0) + 
                    (startTime - ObservingModeUtilities.getCurrentACSTime()); 
                localOscillator.waitForSubscanStart(timeout);
                // TODO. These frequency changes need to be queued in the mount
                // controller.
                // TODO If there is a band change this will also change the
                // pointing and focus model. Sometimes we do not want to do this
                // so we need a way to prevent this.
                final double freq = localOscillator.averageSkyFrequency();
                amc.setObservingFrequency(freq);
                // Send the state table to data capture
                localOscillator.beginSubscan();
                
                // Now wait for the sub-scan to end and send some correlator
                // data to DC.
                if (usesCorrelator) intController.waitForSubscanEnd();

                // wait for the square law data
                int actualIntegrations = slc.waitForSubscanEnd();
                // TODO. Check if the TPP correctly returns the actual number of
                // integrations. If so enable the following line of code. In a
                // simulated environment it returns zero!
                actualIntegrations = numberOfIntegrations[s];
                
                // send the pointing data to data capture.
                amc.waitForSubscanEnd();
                
                dataCapturer.setSubscanSequenceIntegrations(arrayName,
                        subscanNumbers[s], actualIntegrations);
                currentSrc = sources[s];
                curOffset = offsets[s];               
                super.endSubscan(endTime, alma.Control.Completion.SUCCESS);
            }
            dataCapturer.endSubscanSequence(arrayName, subscanNumbers);
        } catch (TableUpdateErrorEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataErrorEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (AbortionException ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataCapturerErrorEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DeviceBusyEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (AlreadyShutDownEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (NoBasebandEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (NoAntennasEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (TimeoutEx ex) {
            if (intController != null) intController.stopSubscanSequence();
            msg = "Cannot complete the subscan sequence.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }

   // TODO Move this into a base class
    @Override
    public void setTarget(String src, String spec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx, TimeoutEx {
        setSourceAsync(src);
        setSpectrumAsync(spec);
        waitForTarget();
    }

    private void setTarget(FieldSourceT src, SpectralSpecT spec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx, TimeoutEx {
        setSourceAsync(src);
        setSpectrumAsync(spec);
        waitForTarget();
    }

    // TODO Move this into a base class
    @Override
    public void setTargetAsync(String src, String spec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        setSourceAsync(src);
        setSpectrumAsync(spec);
    }

    // TODO Move this into a base class
    @Override
    public void waitForTarget() throws ObservingModeErrorEx, TimeoutEx {
        waitForSpectrum();
        waitUntilOnSource();
    }

    // Setting the Spectrum needs to be cleaned up
    // TODO Move this into a base class
    public void setSpectrum(String spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        setSpectrum(ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger));
    }

    // TODO Move this into a base class
    private void setSpectrum(SpectralSpecT spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        setSpectrumAsync(spectralSpec);
        waitForSpectrum();
    }

    @Override
    // TODO Move this into a base class
    public void setSpectrumAsync(String spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        setSpectrumAsync(ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger));
    }
  
    // keep this function here!
    private void setSpectrumAsync(SpectralSpecT spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        currentSpec = null;
        checkSpectralSpecIsOK(spectralSpec);
        // Tune the receiver i.e., set the LO's
        localOscillator.setSpectrumAsync(spectralSpec);

        slc.setIntegrationDuration(SquareLawControllerImpl.extractSLIntegrationDuration(spectralSpec,
                logger));
        // Tell all the mounts about the new frequency. This is used
        // to compute the refraction correction. It may also change
        // the pointing model and move the subreflector.
        final double freq = localOscillator.averageSkyFrequency();
        amc.setObservingFrequency(freq);

        currentSpec = spectralSpec;
    }

    private void waitForSpectrum() throws ObservingModeErrorEx {
        // @TODO. Implement this.
        // try {
        // LOChain.waitUntilLocked();
        // } catch (HardwareFaultEx ex) {
        // String msg = "Cannot tune the receiver ie., set the LO's.";
        // throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        // }
    }
    
    private void checkSpectralSpecIsOK(SpectralSpecT spectralSpec)
            throws IllegalParameterErrorEx {
        // ObservingModeUtilities.serializeSpectralSpec(spectralSpec, logger);
        // @TODO. Check
        // 1. That the frequency is OK
        // * That the frequency is possible with the current configuration.
        // * Issue operator log messages if the receiver band, LO1 or LO2
        // parameters used differ from the specified ones.
        // * That the specified receiver band is the same as the
        // one actually used.
        // String rb = currentSpec.getSpectralSpec().getFrequencySetup().
        // getReceiverBand().toString();
        // short receiverBand = 0;
        // try {
        // receiverBand = (short) JReceiverBand.newReceiverBand(rb).value();
        // } catch (AcsJBadParameterEx ex) {
        // String msg = "Wrong value in SpectralSpec FrequencySetup
        // ReceiverBand";
        // }
        // 2. Check that the square law sampling rate is OK.
    }

    private void checkSourceIsOK(FieldSourceT fieldSource)
            throws IllegalParameterErrorEx {
        // @TODO. Check the fieldSource is OK. This should really be done by a
        // different
        // function in the array mount controller that checks if the source is
        // observable.
    }

    @Override
    public void setIntegrationDuration(double duration) {
        slc.setIntegrationDuration(duration);
    }
    
    @Override
    public double getIntegrationDuration() {
       return slc.getIntegrationDuration();
    }
 
    ////////////////////////////////////////////////////////////////////
    // Functions that primarily use the ArrayMountController
    ////////////////////////////////////////////////////////////////////
 
    @Override
    public void setSource(String fieldSource) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, TimeoutEx {
        setSourceAsync(fieldSource);
        waitUntilOnSource();
    }
    
    @Override
    public void setSourceAsync(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        FieldSourceT src = ObservingModeUtilities.deserializeFieldSource(
                fieldSource, logger);
        setSourceAsync(src);
    }

    private void setSourceAsync(FieldSourceT src)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        checkSourceIsOK(src);
        amc.setSourceAsync(src);
        currentSrc = src;
    }
    
    @Override
    public void waitUntilOnSource() throws ObservingModeErrorEx, TimeoutEx {
        amc.waitUntilOnSource();
    }
    
    @Override
    public ArrayMountController getArrayMountController()
            throws ContainerServicesEx {
        return amc.getArrayMountController();
    }

    @Override
    public boolean isObservable(String fieldSource, double duration)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        return amc.isObservable(fieldSource, duration);
    }

    @Override
    public void setElevationLimit(double elevationLimit)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        amc.setElevationLimit(elevationLimit);
    }

    @Override
    public double getElevationLimit() throws ObservingModeErrorEx {
        return amc.getElevationLimit();
    }

    @Override
    public void resetLimits() {
        amc.resetLimits();
    }

    @Override
    public void applyPointingCalibrationResult(PointingCalResult[] result,
            PolarizationType polarization, String[] antennas)
            throws IllegalParameterErrorEx {
        amc.applyPointingCalibrationResult(result, polarization, antennas);
    }

    @Override
    public void applyFocusCalibrationResult(FocusCalResult[] result,
                                            PolarizationType polarization,
                                            String[] antennas)
        throws IllegalParameterErrorEx {
        amc.applyFocusCalibrationResult(result, polarization, antennas);
    }
    
    ////////////////////////////////////////////////////////////////////
    // Functions that primarily use the  Interferometry Controller
    ////////////////////////////////////////////////////////////////////
    
    @Override
    public void doLLCPolarizationCalibrationAsynch(int timeout,
            boolean forceCalibration, SimpleCallback cb) {
        intController.doLLCPolarizationCalibrationAsynch(timeout,
                forceCalibration, cb);
    }

    public String getDelayServerName() {
        return intController.getDelayServerName();
    }

    public void disableWVRCalibration(boolean disable) {
        intController.disableWVRCalibration(disable);
    }

    public void setPhaseDirection(String fieldSource)
            throws IllegalParameterErrorEx {
        intController.setPhaseDirection(fieldSource);
    }

    public void applyWVRCalibration(ASDMDataSetIDL wvrCalib)
            throws ObservingModeErrorEx {
        intController.applyWVRCalibration(wvrCalib);
    }

    public long getWVRintegrationTime() throws ObservingModeErrorEx {
        return intController.getWVRintegrationTime();
    }

    public InterferometryController getInterferometryController()
            throws ContainerServicesEx {
        return intController.getInterferometryController();
    }

    public void enableLLCCorrection(boolean enable)
            throws AsynchronousFailureEx {
        intController.enableLLCCorrection(enable);
    }

    public boolean isLLCCorrectionEnabled() throws AsynchronousFailureEx {
        return intController.isLLCCorrectionEnabled();
    }

    public long getTimeToLLCReset() throws AsynchronousFailureEx {
        return intController.getTimeToLLCReset();
    }

     public void resetLLCStretchers() throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        intController.resetLLCStretchers();
    }

    public void setLLCStretcherPosition(float position)
            throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        intController.setLLCStretcherPosition(position);
    }

    public boolean getLLCPolarizationCalRequired() throws AsynchronousFailureEx {
        return intController.getLLCPolarizationCalRequired();
    }

    public int[] loadConfigurations(String[] spectralSpecs)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        boolean useCorrelator = (intController != null);
        if (useCorrelator) {
            final int numSpecs = spectralSpecs.length;
            SpectralSpecT[] specs = new SpectralSpecT[numSpecs];
            for (int i = 0; i < numSpecs; i++){
                specs[i] = ObservingModeUtilities.deserializeSpectralSpec(spectralSpecs[i], logger);
            }
            useCorrelator = ObservingModeUtilities.checkForUniformCorrelatorConfigUse(specs, logger);
        }
        if (!useCorrelator) {
            int numIds = spectralSpecs.length;
            int[] invalidIds = new int[numIds];
            for (int s = 0; s < numIds; s++) {
                invalidIds[s] = -1;
            }
            return invalidIds;
        }
        return intController.loadConfigurations(spectralSpecs);
    }

    public int[] loadCalibrations(CorrelatorCalibrationSequenceSpec calSpecs)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
 
        boolean useCorrelator = (intController != null);
        if (useCorrelator) {
            final int numCals = calSpecs.configIds.length;
            for (int i = 0; i < numCals; i++){
                if (calSpecs.configIds[i] < 0) {
                    useCorrelator = false;
                }
            }
        }
        if (!useCorrelator) {
            int numIds = calSpecs.calibrationList.length;
            int[] invalidIds = new int[numIds];
            for (int s = 0; s < numIds; s++) {
                invalidIds[s] = -1;
            }
            return invalidIds;
        }
        // This only works because we are, for now, requiring all sub-scans in a
        // sequence to use the same source.
        // TODO. Remove this restriction.
        try {
            if (calSpecs.FieldSourceList.length > 0) {
                setSource(calSpecs.FieldSourceList[0]);
            } else {
                setSourceAsync(currentSrc);
                waitUntilOnSource();
            }
        } catch (TimeoutEx ex) {
            String msg = "Cannot move the antennas to the specified calibration source.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        return intController.loadCalibrations(calSpecs);
    }

    public void clearConfigurations(int[] configIDs) {
        intController.clearConfigurations(configIDs);
    }

    public void clearCalibrations(int[] calIDs) {
        intController.clearCalibrations(calIDs);
    }

    /* ------------ Local Oscillator Methods ----------- */
    @Override
    public short setFrequency(SSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, HardwareFaultEx {

        short nSols = localOscillator.setFrequency(bbspec, overlap);
        /*
         * Set the frequency of the observing mode, used for refraction, to the
         * mean of the four baseband frequencies.
         */
        amc.setObservingFrequency(localOscillator.averageSkyFrequency());

        return nSols;
    }

    @Override
    public short setFrequencyDSB(DSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, HardwareFaultEx {

        short nSols = localOscillator.setFrequencyDSB(bbspec, overlap);
        /*
         * Set the frequency of the observing mode, used for refraction, to the
         * mean of the four baseband frequencies.
         */
        double mean = localOscillator.averageSkyFrequency();
        amc.setObservingFrequency(mean);

        return nSols;
    }
    
    @Override
    public double[] getFrequency() throws HardwareFaultEx, AntennasDisagreeEx {
        return localOscillator.getFrequency();
    }

    @Override
    public void setCalibrationDevice(CalibrationDevice device)
            throws ObservingModeErrorEx, TimeoutEx {
        localOscillator.setCalibrationDevice(device);
    }

    @Override
    public CalibrationDevice getCalibrationDevice() throws AntennasDisagreeEx {
        return localOscillator.getCalibrationDevice();
    }

    @Override
    public void optimizeSignalLevels(float ifTargetLevel, float bbTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeSignalLevels(ifTargetLevel, bbTargetLevel);
    }
    
    @Override
    public void optimizeIFSignalLevels(float ifTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeIFSignalLevels(ifTargetLevel);
    }

    @Override
    public void optimizeBBSignalLevels(float bbTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeBBSignalLevels(bbTargetLevel);
    }

    @Override
    public LocalOscillator getLocalOscillator() throws ContainerServicesEx {
        return localOscillator.getLocalOscillator();
    }

    @Override
    public void optimizeSASPol1Asynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPol1Asynch(forceCalibration, cb);
    }

    @Override
    public double averageSkyFrequency() {
        return localOscillator.averageSkyFrequency();
    }

    public String dopplerShift(String spectralSpec, 
                String dopplerSource, long epoch)
                    throws ObservingModeErrorEx, IllegalParameterErrorEx {
        return localOscillator.dopplerShift(spectralSpec, dopplerSource, epoch);
    }
    public void enableDelayTracking(boolean enable) throws HardwareFaultEx {
        localOscillator.enableDelayTracking(enable);
    }

    public boolean delayTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        return localOscillator.delayTrackingEnabled();
    }

    public void enableFringeTracking(boolean enable) throws HardwareFaultEx {
        localOscillator.enableFringeTracking(enable);
    }

    public boolean fringeTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        return localOscillator.fringeTrackingEnabled();
    }

    public void optimizeSASPolarizationAsynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPolarizationAsynch(forceCalibration, cb);
    }

    public void optimizeSASPol2Asynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPol2Asynch(forceCalibration, cb);
    }

    public void setOffsetToRejectImageSideband(boolean rejectImage)
            throws HardwareFaultEx {
        localOscillator.setOffsetToRejectImageSideband(rejectImage);
    }

    ////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////

    public static TotalPowerConfiguration extractTotalPowerConfiguration(
            SpectralSpecT spec, long subscanDuration, Logger logger)
            throws IllegalParameterErrorEx {
        long integrationDuration = Math.round(SquareLawControllerImpl.extractSLIntegrationDuration(
                spec, logger) * 10E6);
        // TODO. There are obviously incorrect numbers in here. Discuss with
        // Nuria how much of this is needed by data capture.
        SpectralSetup spectSetup = new SpectralSetup();
        spectSetup.centerFrequency = 111;
        spectSetup.refFrequency = 0.0;
        spectSetup.totalBandwidth = 2.0E9;
        spectSetup.effectiveBandwidth = 0.0;
        spectSetup.netSideband = NetSideband.LSB;

        TotalPowerPolarization pol = new TotalPowerPolarization();
        pol.polarization = 0;
        pol.numCorr = 2;
        pol.corrType = new StokesParameter[2];
        pol.corrType[0] = StokesParameter.XX;
        pol.corrType[1] = StokesParameter.YY;

        pol.corrProduct = new PolarizationType[2][2];
        pol.corrProduct[0][0] = PolarizationType.X;
        pol.corrProduct[0][1] = PolarizationType.X;
        pol.corrProduct[1][0] = PolarizationType.Y;
        pol.corrProduct[1][1] = PolarizationType.Y;
        pol.spectSetup = new SpectralSetup[1];
        pol.spectSetup[0] = spectSetup;

        TotalPowerBaseBandConfig bbConfig = new TotalPowerBaseBandConfig();
        bbConfig.bin0Duration = integrationDuration;
        bbConfig.bin1Duration = 0;
        bbConfig.numSwitchCycles = 1;
        bbConfig.windowFunction = alma.Control.eWindows.UNIFORM;
        bbConfig.polarizations = new TotalPowerPolarization[1];
        bbConfig.polarizations[0] = pol;

        TotalPowerConfiguration tpConfig = new TotalPowerConfiguration();
        tpConfig.integrationDuration = integrationDuration;
        tpConfig.subScanDuration = (int) (subscanDuration / integrationDuration);
        int[] bbIDs = ObservingModeUtilities.baseBandIDs(spec);
        tpConfig.baseBands = new TotalPowerBaseBandConfig[bbIDs.length];
        for (int i = 0; i < bbIDs.length; i++) {
            bbConfig.baseBandIndex = bbIDs[i];
            bbConfig.baseBandName = BasebandName.from_int(bbConfig.baseBandIndex);
            tpConfig.baseBands[i] = new TotalPowerBaseBandConfig(bbConfig.baseBandIndex, bbConfig.baseBandName, bbConfig.bin0Duration, bbConfig.bin1Duration, bbConfig.numSwitchCycles, bbConfig.windowFunction, bbConfig.polarizations);
        }
        return tpConfig;
    }
 }
