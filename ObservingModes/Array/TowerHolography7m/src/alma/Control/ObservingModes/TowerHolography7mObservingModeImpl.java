/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2007
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.ObservingModes;

import java.util.Iterator;
import java.util.logging.Level;

import org.omg.CORBA.DoubleHolder;

import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJDataCapturerErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.Control.Completion;
import alma.Control.HolographyScanData;
import alma.Control.HolographySubScanData;
import alma.Control.ObservingModes.ObservingModeArray;
import alma.Control.ScanEndedEvent;
import alma.Control.ScanIntentData;
import alma.Control.ScanStartedEvent;
import alma.Control.SubscanEndedEvent;
import alma.Control.SubscanStartedEvent;
import alma.Control.TowerHolography7m;
import alma.Control.TowerHolography7mObservingModeOperations;
import alma.Control.AbortionException;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJException;
import alma.asdmIDLTypes.IDLFrequency;
import alma.asdmIDLTypes.IDLLength;
import alma.log_audience.OPERATOR;
import alma.offline.DataCapturer;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;

/**
 * Insert a Class/Interface comment.
 * 
 */
public class TowerHolography7mObservingModeImpl extends
        ObservingModeBaseImpl<TowerHolography7m> implements
        TowerHolography7mObservingModeOperations {


    public TowerHolography7mObservingModeImpl(ContainerServices cs,
            ObservingModeArray array) 
        throws ObsModeInitErrorEx {
        
        super(cs, array);
        createAntModeControllers("alma.Control.TowerHolography7mHelper",
                                 "TowerHolography7m");
        
        String msg;
        msg = "Tower holography observing mode starting up.";
        logger.logToAudience(Level.INFO, msg, OPERATOR.value);

        // This observing mode is only valid for arrays with a single antenna
        if (antennaModeController.size() != 1) {
            msg = "Tower Holography is only valid on arrays with a"
                + " single antenna.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx());
        }
    }

    public String getTowerHolographyModeController() {
        Iterator<String> iter = antennaModeController.keySet().iterator();
        String an = iter.next();
        return new String(antennaModeController.get(an).name());
    }

    public void beginScan(ScanIntentData[] intentData, 
                          String scanName, int nRow)
        throws DataCapturerErrorEx {
        
        //logBeginScan(scanIntents);
        HolographyScanData holographyScanData = new HolographyScanData();
        // Initialize the Holography Scan Data
        {
            holographyScanData.name = scanName;
            holographyScanData.mapRowSize = nRow;
            holographyScanData.mapColSize = nRow;

            holographyScanData.numberCorr = 6;
            String[] dataProducts = { "Q2", "QR", "QS", "R2", "RS", "S2" };
            holographyScanData.holographyType = dataProducts;

            Iterator<String> iter = antennaModeController.keySet().iterator();
            String an = iter.next();
            TowerHolography7m thMC = antennaModeController.get(an);

            // Get the tower XYZ position from the Holography Mode Controller
            {
                DoubleHolder X = new DoubleHolder(0);
                DoubleHolder Y = new DoubleHolder(0);
                DoubleHolder Z = new DoubleHolder(0);
                thMC.getTowerXYZPosition(X, Y, Z);

                // Get the tower position from mode controller
                holographyScanData.towerXPosition = new IDLLength(X.value);
                holographyScanData.towerYPosition = new IDLLength(Y.value);
                holographyScanData.towerZPosition = new IDLLength(Z.value);
            }

            // Get the focus from the mount controller (eventually)
            {
                holographyScanData.focusOffset = new IDLLength(0.091);
            }

            // Get the frequency from the Holography Receiver
            {
                double frequency;
                try {
                    frequency = thMC.getFrequency();
                    holographyScanData.frequency = new IDLFrequency(frequency);
                } catch (HardwareFaultEx ex) {

                } catch (UnallocatedEx ex) {

                }
            }
        }

        long now = ObservingModeUtilities.getCurrentACSTime();

        // Do these belong in the Array.beginScanEvent method?
        array.incrScanNumber();
        array.resetSubScanNumber();
        
        ScanStartedEvent event = new ScanStartedEvent();
        event.execId = array.getASDMEntRef();
        event.arrayName = array.getArrayName();
        event.scanNumber = array.getScanNumber();
        event.scanType = intentData;
        event.startTime = now;
        try {
            publisher.publishEvent(event);
        } catch (AcsJException e) {
            e.log(logger);
        }

        try {
            final String arrayName = array.getArrayName();
            dataCapturer.startScan(arrayName,
                    ObservingModeUtilities.getCurrentACSTime(), intentData);
            dataCapturer.setScanHolographyConfig(arrayName, holographyScanData);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Error starting holography scan in OFFLINE/DataCapturer.";
            logger.logToAudience(Level.INFO, msg, OPERATOR.value);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        } catch (DataErrorEx ex) {
            String msg = "Error starting holography scan in OFFLINE/DataCapturer.";
            logger.logToAudience(Level.INFO, msg, OPERATOR.value);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        }
    }

//    public void endScan() throws DataCapturerErrorEx {
//        logger.info("endScan called.");

//        long now = ObservingModeUtilities.getCurrentACSTime();
        
//        try {
//            dataCapturer.endScan(array.getArrayName(),
//                                 now,
//                                 array.getScanNumber(), 
//                                 array.getSubScanNumber());
//        } catch (TableUpdateErrorEx ex) {
//            String msg = "Error ending scan in OFFLINE/DataCapturer.";
//            logger.fine(msg);
//            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
//            exHlp.setProperty("Message", msg);
//            throw exHlp.toDataCapturerErrorEx();
//        } catch (DataErrorEx ex) {
//            String msg = "Error ending scan in OFFLINE/DataCapturer.";
//            logger.fine(msg);
//            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
//            exHlp.setProperty("Message", msg);
//            throw exHlp.toDataCapturerErrorEx();
//        }

//        ScanEndedEvent event = new ScanEndedEvent();
//        event.execId = array.getASDMEntRef();
//        event.arrayName = array.getArrayName();
//        event.scanNumber = array.getScanNumber();
//        event.status = Completion.SUCCESS;
//        event.endTime = now;
//        try {
//            publisher.publishEvent(event);
//        } catch (AcsJException e) {
//            e.log(logger);
//        }
            
//    }

    public void doHorizontalSubscan(double offset, boolean forward,
            double width, double velocity)
            throws alma.ModeControllerExceptions.MountFaultEx,
            alma.ModeControllerExceptions.HardwareFaultEx,
            alma.ControlExceptions.IllegalParameterErrorEx,
            alma.ControlExceptions.DeviceBusyEx,
            alma.ModeControllerExceptions.UnallocatedEx {

    }

    public void doVerticalSubscan(double offset, boolean forward, double width,
            double velocity) throws alma.ModeControllerExceptions.MountFaultEx,
            alma.ModeControllerExceptions.HardwareFaultEx,
            alma.ControlExceptions.IllegalParameterErrorEx,
            alma.ControlExceptions.DeviceBusyEx,
            alma.ModeControllerExceptions.UnallocatedEx {

    }

    public void doPhaseCal(double duration) {

    }

    public void beginSubscan(SubscanIntent[] purpose) 
        throws AbortionException, DataCapturerErrorEx {
        logger.logToAudience(Level.INFO, "Starting a subscan.", OPERATOR.value);
        
        long now = ObservingModeUtilities.getCurrentACSTime();
        array.incrSubScanNumber();

        // Send SubscanStartedEvent
        SubscanStartedEvent event = new SubscanStartedEvent();
        event.execId = array.getASDMEntRef();
        event.arrayName = array.getArrayName();
        event.scanNumber = array.getScanNumber();
        event.subscanNumber = array.getSubScanNumber();
        event.startTime = now;
        try {
            publisher.publishEvent(event);
        } catch (AcsJException e) {
            e.log(logger);
        }
        
        try {
            final String arrayName = array.getArrayName();
            SubscanIntent[][] intents = new SubscanIntent[][]{purpose};
            dataCapturer.startSubscanSequence(arrayName, intents);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Error starting holography sub-scan in OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        } catch (DataErrorEx ex) {
            String msg = "Error starting holography sub-scan in OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        }
    }

    public void sendSubscanData(HolographySubScanData data) throws DataCapturerErrorEx {
        try {
            final String arrayName = array.getArrayName();
            final long[] startTime = new long[]{data.startTime};
            final long[] endTime = new long[]{data.endTime};
            dataCapturer.setSubscanSequenceTiming(arrayName, startTime, endTime);
            dataCapturer.sendSubScanHolographyData(arrayName, data);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Error sending holography sub-scan data to OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        } catch (DataErrorEx ex) {
            String msg = "Error sending holography sub-scan data to OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        }
    }

    public void endSubscan() throws ObservingModeErrorEx, DataCapturerErrorEx {
        logger.fine("endSubscan called.");
        int[] successfulSubscans = new int[] { array.getSubScanNumber() };
        try {
            dataCapturer.endSubscanSequence(array.getArrayName(),
                    successfulSubscans);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot end the subscan";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        } catch (DataErrorEx e) {
            String msg = "Cannot end the subscan";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        super.endSubscan(ObservingModeUtilities.getCurrentACSTime(),
                Completion.SUCCESS);
    }
}
