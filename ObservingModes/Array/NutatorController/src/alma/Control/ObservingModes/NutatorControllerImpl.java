// "$Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010 
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU LesSkyser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


package alma.Control.ObservingModes;

/**
 * @author Nicolas Troncoso <ntroncos@alma.cl>
 * @version $Id$
 *
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;
import java.util.logging.Level;

import alma.Control.NUTATOR;
import alma.Control.NUTATORHelper;
import alma.Control.NutatorControllerOperations;
import alma.Control.SubscanBoundries;
import alma.Control.HardwareDevicePackage.HwState;
import alma.ControlDeviceExceptions.IllegalConfigurationEx;
import alma.ControlDeviceExceptions.wrappers.AcsJIllegalConfigurationEx;
import alma.ControlExceptions.CAMBErrorEx;
import alma.ControlExceptions.INACTErrorEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.wrappers.AcsJHardwareFaultEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogger;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.SwitchingCycleT;

/**
 * NutatorController implementation that deals with coordinating the available
 * NUTATORs. I'm not sure if this should be an ACS::OffShoot or just a native
 * class that gets used by the observingModes. I will ponder over the issue
 * later.
 * 
 * Implementation details:
 * <ul>
 * <li>This class is not intended to be sub-classed so it will be labeled final.
 * </li>
 * <li>It is possible that a new object created by this class is invalid, the
 * solution to address this is to throw an exception in the constructor. Since
 * God kills a cute little kitten every time an exception is thrown in a
 * constructor, this class provides a static factory method. ( Effective Java™,
 * Second Edition, page 5). Hence the constructor will be private, and the only
 * way to get an object of this class its to do so through the factory method,
 * which can throw an exception.</li>
 * <li>I here by ban any use of arrays in this class. You should use classes
 * that implement the List interface to operate over "arrays". Any array that is
 * obtain from outside by this class will be converted into a convenient object
 * that has an implementation of List. Every time your code throws a
 * IndexOutOfBoundsException, your name appear in Santa's bad boy list. If you
 * break this rule I <b>WILL</b> hunt you down.</li>
 * </ul>
 * 
 * 
 * <pre>
 * NUTATOR ICD: ALMA-36.20.00.00-70.35.20.00-E-ICD
 * </pre>
 * 
 * The intended usage for this class is:
 * <ul>
 * <li>Configure setup the NUTATOR</li>
 * <li>Setup the standby program</li>
 * <li>Start the program on all configured NUTATORs</li>
 * <li>Stop the program on all configured NUTATORs</li>
 * </ul>
 */
/**
 * @author ntroncos
 *
 */
public final class NutatorControllerImpl implements NutatorControllerOperations {



    private ContainerServices cs_m = null;
    private ObservingModeArray array_m = null;
    private SpectralSpecT currentSpec_m = null;
    private List<NUTATOR> nutatorList_m = null;
    private AcsLogger logger_m = null;
    private static int MAX_NUTATORS = 5;
    private List<SubscanBoundries> subscanBoundries = null;
    private ListIterator<SubscanBoundries> sequence = null;



    /**
     * Factory method for the NutatorControllerImpl class. Calling this class is the 
     * only valid way of getting a NutatorControllerImpl object.
     * 
     * @param cs Container service instance
     * @param array Observing mode array
     * @param spectralSpec SpectralSpec configuration
     * @throws ObsModeInitErrorEx in case any resource cannot be allocated.
     */
    public static NutatorControllerImpl getNutatorControllerImpl(ContainerServices cs, ObservingModeArray array, SpectralSpecT spectralSpec) throws ObsModeInitErrorEx
    {
        NutatorControllerImpl thisObject = new NutatorControllerImpl(cs, array, spectralSpec);
        try {
            thisObject.getNutatorList();
        } catch (IllegalConfigurationEx ex){
            throw new AcsJObsModeInitErrorEx(ex).toObsModeInitErrorEx();
        }   
        return thisObject;
    }


    /**
     * Very simple constructor, just initialize some class variables.
     * 
     * @param cs - Container service instance
     * @param array - Observing mode array
     * @param spectralSpec - SpectralSpec configuration
     */
    private NutatorControllerImpl(ContainerServices cs,
            ObservingModeArray array, SpectralSpecT spectralSpec)
    {
        this.cs_m = cs;
        this.array_m = array;
        this.currentSpec_m = spectralSpec;
        this.logger_m = cs.getLogger();
    }   

    /**
     * Creates a reference to each of the available and operating NUTATORs.
     * 
     *
     * @throws IllegalConfigurationEx in case no NUTATORs are available.
     */ 
    private void getNutatorList() throws IllegalConfigurationEx 
    {
        List<String> antennaList = new ArrayList<String>(Arrays.asList(array_m.getAntennas()));
        nutatorList_m = new Vector<NUTATOR>(MAX_NUTATORS); 
        for (String antenna: antennaList) {
            NUTATOR tmpref = null;
            org.omg.CORBA.Object obj = null;
            String nutatorName = "CONTORL/" + antenna + "/NUTATOR";
            try {
                obj = cs_m.getComponentNonSticky(nutatorName);
            } catch (AcsJContainerServicesEx ex) {
                logger_m.log(Level.FINE, "Could no get nuatator: "+nutatorName, ex);
            }

            tmpref = NUTATORHelper.narrow(obj);
            if (HwState.Operational == tmpref.getHwState()) {
                nutatorList_m.add(tmpref);
                //@TODO maybe remove this log later.
                logger_m.log(Level.INFO, "Antenna: "+antenna+" Has a working NUTATOR");
            } 
        }

        if (nutatorList_m.size() == 0) {
            // This is an issue. This class was created to make use of NUTATORs, but there are
            // none available. Except right away.
            AcsJIllegalConfigurationEx ex = new AcsJIllegalConfigurationEx();
            ex.setProperty("Mesage", "No available Nutators");
            throw ex.toIllegalConfigurationEx();
        }
        if (nutatorList_m.size() > MAX_NUTATORS) {
            logger_m.logToAudience(Level.WARNING, "More than "+MAX_NUTATORS+" have been configure. This is likely to be an error.", "Operator");  
        }
    }




    /**
     * Configure and validate the NUTATOR program in every available NUATOR.
     * 
     * 
     * @throws IllegalConfigurationEx
     *             in case the nutator program is invalid.
     * @throws HardwareFaultEx
     *             in case the is some sort of hardware error.
     * 
     */
    public void setupStandbyProgram() throws IllegalConfigurationEx, HardwareFaultEx
    {
        SwitchingCycleT cycle = currentSpec_m.getSwitchingCycle();
        cycle.hashCode(); //dummy op
        //Here goes the code that sets up the available nutators. 

    }

    /**
     * Stub method, that will leave the nutators in some X state, when before
     * this class is destroyes.
     */
    void cleanUp()
    {

    }
    
    /**
     * Start the standby program in the nutator.
     * 
     * This method need a start time as a parameter. It should be the same start
     * time for the subscan. Bare in mind that the start command on the nutator
     * will start the program on the second Timing event following the start
     * command.
     * 
     * Probably this should receive a completion as an argument. So it may
     * accumulate the errors.
     * 
     * @throws HardwareFaultEx
     *             in case the is some sort of hardware error.
     * 
     */
    public void startProgram(long starttime) throws HardwareFaultEx
    {
        for(NUTATOR nut:nutatorList_m) {
            try {
                nut.startProgamAt(starttime);
            } catch (INACTErrorEx e) {
                  throw new AcsJHardwareFaultEx(e).toHardwareFaultEx();
            } catch (CAMBErrorEx e) {
                throw new AcsJHardwareFaultEx(e).toHardwareFaultEx();
            } catch (IllegalParameterErrorEx e) {
                throw new AcsJHardwareFaultEx(e).toHardwareFaultEx();
            } catch (IllegalConfigurationEx e) {
                throw new AcsJHardwareFaultEx(e).toHardwareFaultEx();
            }
        }

    }


    /**
     * Stop the active program in the nutator.
     * 
     * 
     * @throws HardwareFaultEx
     *             in case the is some sort of hardware error.
     * 
     */ 
    public void stopProgram(long stoptime) throws HardwareFaultEx
    {
        for(NUTATOR nut:nutatorList_m) {
            try {
                nut.stopProgamAt(stoptime);
            } catch (INACTErrorEx e) {
                  throw new AcsJHardwareFaultEx(e).toHardwareFaultEx();
            } catch (CAMBErrorEx e) {
                throw new AcsJHardwareFaultEx(e).toHardwareFaultEx();
            }
        }
    }

    
    /**
     * Configure the SubscanSequence for the observation.
     * 
     * @param ssb
     *            array of SubscanBoundries
     * @throws IllegalConfigurationEx
     *             if the SubscanBoundries array is empty.
     */
    public void configureSubscanSequence(SubscanBoundries[] ssb) throws IllegalConfigurationEx 
    {
        subscanBoundries = new ArrayList<SubscanBoundries>(Arrays.asList(ssb));
        sequence = subscanBoundries.listIterator();
        if (!sequence.hasNext()){
            AcsJIllegalConfigurationEx ex = new AcsJIllegalConfigurationEx();
            ex.setProperty("Mesage", "Sub scan boundry list is empty");
            throw ex.toIllegalConfigurationEx();
        }
    }
    
    
    /**
     * Start a subscan. This method only effectively does something on the first
     * subscan. It will start the NUTATOR program that is loaded
     * 
     * @throws HardwareFaultEx
     *             pass through from startProgram()
     */
    public void waitForSubscanStart() throws HardwareFaultEx 
    {
        if (! sequence.hasPrevious()) {
            SubscanBoundries ssb = sequence.next();
            startProgram(ssb.startTime);
        }
    }
    
    /**
     * Stop a subscan. This method only effectively does something on the last
     * subscan. It will stop the NUTATOR program that is loaded
     * 
     * @throws HardwareFaultEx
     *             pass through from stopProgram()
     */
    public void waitForSubscanEnd()  throws HardwareFaultEx 
    {
        if (! sequence.hasNext()) {
            SubscanBoundries ssb = sequence.previous();
            stopProgram(ssb.endTime);
        }
    }
    
    
}
