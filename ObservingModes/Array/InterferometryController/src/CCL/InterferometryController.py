#! /usr/bin/env python
# "@(#) $Id$"
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2010
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

from CCL.InterferometryControllerInterface import \
     InterferometryControllerInterface
    
class InterferometryController(InterferometryControllerInterface):
    """
    Python Binding to the Interferometry Controller
    """
    
    def __init__(self, offshoot, logger):
        InterferometryControllerInterface.__init__(self, offshoot, logger)

    def clearConfigurationIDs(self, corrConfIDs):
        self._offshoot.clearConfigurationIDs(corrConfIDs)

    def isWVRSendingData(self):
        """
        Returns true if any water vapour radiometer in this array is
        producing data.
        """
        return self._offshoot.isWVRSendingData()

