#! /usr/bin/env python
# "@(#) $Id$"
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import time
import copy
from CCL.utils import aggregate
import Control
import CCL.SIConverter
from CCL.APDMSchedBlock import getCheckSum
from Control import CorrelatorCalibrationSequenceSpec
    
from ControlCommonCallbacks.SimpleCallbackImpl import SimpleCallbackImpl
from ControlExceptionsImpl import InvalidRequestExImpl

#
class CorrIdListElement:
    def __init__(self, CorrId, LastTimeUsed, CheckSum):
        self.CorrId = CorrId
        self.LastTimeUsed = LastTimeUsed
        self.CheckSum = CheckSum

class CalIdListElement:
    def __init__(self, CalId, CalTime):
        self.CalId = CalId
        self.CalTime = CalTime

class InterferometryControllerInterface:
    """
    Python Binding to the Interferometry Controller Interface
    """
    
    def __init__(self, offshoot, logger):
        self._offshoot = offshoot
        self._logger   = logger

        # These are the maximum available slots in the correlator
        # for the Configurations and the Calibrations.
        self._MAX_CONFIGURATIONS = 16
        self._MAX_CALIBRATIONS = 16

        # The _corrIdList is a dictionary of Spectral Specs, to
        # CorrIdListElements
        self._corrIdList = {}

        # The _calIdList is a dictionary of (SpectralSpec, FieldSource)
        # which gives a CalIdListElement
        self._calIdList = {}
        
        self._calibrationValidityTime = 1800

    def getDelayServer(self):
        """
        This method returns the CCL Delay server object that was created
        by the SFI observing mode.
        """
        from CCL.SkyDelayServer import SkyDelayServer
        return SkyDelayServer(componentName=self._offshoot.
                              getDelayServerName())
        
    def setPhaseDirection(self, fieldSource):
        """
        This sets the direction of the phase center to the specified source.
        """
        self._offshoot.setPhaseDirection(fieldSource.getXMLString())

    def getCalibrationIds(self, subscanSeqSpec):
        '''
        This method should be called before every call to dosubscansequece
        in which implements the interferometry controller interface.
        This method will:
        1) Make sure that all spectral specs in the subscan spec are
           loaded into the correlator.  Unloading previous versions as
           necessary.
        2) Ensure that calibrations are available for all of the pairs
           in the spectral spec.  Performing them as needed.
        3) Place the appropriate Cal ID into each subscan spec in the
           the subscan sequece.
        '''
        if not isinstance(subscanSeqSpec, Control.SubscanSequenceSpec):
            msg = "Method getCalibrationIds only accepts " + \
                  "Control.SubscanSequenceSpec objects."
            raise IllegalParamterErrorExImpl(msg)

        self.prepareConfigurations(subscanSeqSpec.spectra)
        self.prepareCorrelatorCalibrations(subscanSeqSpec)
        for subscan in subscanSeqSpec.subscans:
            src = subscanSeqSpec.sources[subscan.sourceId]
            spect = subscanSeqSpec.spectra[subscan.spectralId]
            subscan.calibrationId = self._calIdList[(spect, src)].CalId
        
    def getCorrelatorCalibrationValidityInterval(self):
        '''
        Returns the currently specified Correlator Calibration Validity
        Interval (in seconds).  Correlator calibrations older than this
        will be automatically updated when they are attempted to be used.
        '''
        return self._calibrationValidityTime
    
    def setCorrelatorCalibrationValidityInterval(self, validityTime):
        '''
        Set the Correlator Calibration Validity Interval to the specificed
        time.  Correlator calibrations older than this
        will be automatically updated when they are attempted to be used.

        If this number is supplied as an integer it is interpreted in seconds,
        this number may also be entered as a string with units
        Example:
           setCorrelatorCalibrationValidityInterval("30 min")
        '''
        self._calibrationValidityTime = CCL.SIConverter.toSeconds(validityTime)


    def prepareConfigurations(self, SpectralSpecList):
        '''
        This method causes the passed in SubscanSpectralSpecs to
        be loaded into the correlator.

        These are only loaded if:
          a) They have not previously been loaded
          b) The checksum for the spectral spec is modified

        Previous versions are automatically cleared on to make space if all
        of the slots in the correlator are occupied.  
        '''
        # Make a local copy of the list
        spectSpecList = copy.copy(aggregate(SpectralSpecList))

        listToClear = []
        listToConfigure = []
        # Remove any elements from the list which are already loaded
        # Iterate over the original and edit the copy
        for spec in aggregate(SpectralSpecList):
            if spec in self._corrIdList:
                if spec.getCheckSum() != self._corrIdList[spec].CheckSum:
                    listToClear.append(spec)
                    listToConfigure.append(spec)
            else:
                listToConfigure.append(spec)

        # Now figure out if we need to remove any because there are
        # too many defined already
        numberToClear =  len(self._corrIdList) + len(listToConfigure) - \
                        len(listToClear) - self._MAX_CONFIGURATIONS

        if numberToClear > 0:
            timeOrderedList = self._getTimeOrderedConfigList()
            for spec in timeOrderedList:
                if spec in spectSpecList:
                    timeOrderedList.remove(spec)

            if len(timeOrderedList) < numberToClear:
                msg = "Unable to create enough free configuration slots in"+\
                      (" the corelator, (number needed: %d, available: %d)." %\
                       (numberToClear, len(timeOrderedList)))
                raise InvalidRequestExImpl(msg)
            
            listToClear += timeOrderedList[:numberToClear]

        # Now remove the ones we have selected
        if len(listToClear) > 0:
            self.clearConfigurations(listToClear)
            
        # Now we need to load the remaining list
        if len(listToConfigure) > 0:
            self.loadConfigurations(listToConfigure)

        # Mark all of the spectral specs as being used now
        now = time.time()
        for spect in spectSpecList:
            self._corrIdList[spect].LastTimeUsed = now
        
    def loadConfigurations(self, SpectralSpecList):
        '''
        This method does all of the bookkeeping associated with configuring
        a new set of Spectral Specs, the inputs are assumed to be spectral
        spec objects.
        '''
        # We need to convert the objects to xml strings to send to the offshoot
        xmlSpectralSpecList = map(lambda s: s.getXMLString(), SpectralSpecList)
        ids = self._loadConfigurations(xmlSpectralSpecList)

        for idx in range(len(SpectralSpecList)):
            self._corrIdList[SpectralSpecList[idx]] =\
                     CorrIdListElement(ids[idx], 0,
                              getCheckSum(xmlSpectralSpecList[idx]))

    def clearConfigurations(self, spectralSpecList):
        '''
        This method causes the specified configurations and any calibrations
        associated with them to be cleared.
        '''
        spectralSpecList = aggregate(spectralSpecList)
        idsToClear = []
        for spec in spectralSpecList :
            idsToClear.append(self._corrIdList[spec].CorrId)
            self._corrIdList.pop(spec)

        # Need to remove the Calibrations that are associated with this
        # configuration as the correlator removes them automatically
        removeList = []
        for key in self._calIdList:
            if key[0] in spectralSpecList:
                removeList.append(key)

        for key in removeList:
            self._calIdList.pop(key)

        self._clearConfigurations(idsToClear)

    def prepareCorrelatorCalibrations(self, subscanSeqSpec):
        '''
        This method checks to see if the given (spectralSpec, fieldSource)
        combinations have a valid calibrationID.  If the calibration
        does not exist or is expired a new calibration is performed.
        '''
        if not isinstance(subscanSeqSpec, Control.SubscanSequenceSpec):
            raise ControlExceptions.IllegalParameterErrorExImpl()

        calibrationsToRelease = []
        for subscan in subscanSeqSpec.subscans:
            spectSpec = subscanSeqSpec.spectra[subscan.spectralId]
            fieldSrc =  subscanSeqSpec.sources[subscan.sourceId]
            
            if not self._calIdList.has_key((spectSpec, fieldSrc)):
                # New Field Source
                self._calIdList[(spectSpec, fieldSrc)] = None
            elif self._calIdList[(spectSpec,fieldSrc)] != None and \
                 time.time() - self._calibrationValidityTime > \
                 self._calIdList[(spectSpec,fieldSrc)].CalTime :
                # Expired Calibration
                calibrationsToRelease.append(self._calIdList\
                                             [(spectSpec,fieldSrc)].CalId)
                self._calIdList[(spectSpec,fieldSrc)] = None

        if None not in self._calIdList.values():
            # Nothing to do all calibrations are current
            return

        # Now we need to check to see how many calibrations slots we have
        # already used.  The method getTimeOrderedCal list ignores those
        # entries flagged by a None.
        numberToClear = len(self._calIdList) - self._MAX_CALIBRATIONS

        if numberToClear > 0:
            timeOrderedList = self._getTimeOrderedCalList()
            if len(timeOrderedList) < numberToClear:
                msg = "Unable to create enough free calibration slots in"+\
                      (" the corelator, (number needed: %d, available: %d)." %\
                       (numberToClear, len(timeOrderedList)))
                raise InvalidRequestExImpl(msg)
            
            for calKey in timeOrderedList[:numberToClear]:
                calibrationsToRelease.append(self._calIdList[calKey].CalId)

        if len(calibrationsToRelease) > 0:
            self.clearCalibrations(calibrationsToRelease)

        # Now go through and build the CalibrationSequenceSpec
        calSpectSpecs    = []
        calFieldSources = []
        calPairList     = []

        for key in self._calIdList.keys():
            if self._calIdList[key] is not None:
                continue
            if key[0] not in calSpectSpecs:
                calSpectSpecs.append(key[0])
            spectIndex = calSpectSpecs.index(key[0])
                
            if key[1] not in calFieldSources:
                calFieldSources.append(key[1])
            srcIndex = calFieldSources.index(key[1])
                
            if (spectIndex, srcIndex) not in calPairList:
                calPairList.append(Control.CorrCalPair(srcIndex, spectIndex))
        
        if len(calPairList) > 0:
            corrCalSequence = CorrelatorCalibrationSequenceSpec\
                              (map(lambda f: f.getXMLString(),calFieldSources),
                               map(lambda s: self._corrIdList[s].CorrId, calSpectSpecs),
                               calPairList)
            calIds = self._loadCalibrations(corrCalSequence)

            for idx in range(len(calIds)):
                key = (calSpectSpecs[calPairList[idx].spectralId],
                       calFieldSources[calPairList[idx].sourceId])
                self._calIdList[key] = CalIdListElement(calIds[idx],
                                                        time.time())

    def clearCalibrations(self, calIdList):
        '''
        This method causes the specified calibrations to be cleared.
        '''
        calIdList = aggregate(calIdList)
        keysToClear = []
        for key in self._calIdList:
            if self._calIdList[key] is None:
                continue
            if self._calIdList[key].CalId in calIdList:
                keysToClear.append(key)

        for key in keysToClear:
            self._calIdList.pop(key)

        self._clearCalibrations(calIdList)
                

                          
    def disableWVRCalibration(self, disable):
        """
        This function allows you to disable (or enable) the automatic
        addition of the CALIBRATE_WVR intent to all scans. The
        CALIBRATE_WVR scan intent is used to initiate, at the end of
        the scan, the calculation of coefficients. These coefficients
        are used to convert the temperatures measured by the
        water-vapour radiometer (WVR) to a path-length.

        By default the CALIBRATE_WVR scan intent is added if at least
        one antenna in this array has a working WVR and the
        CALIBRATE_WVR scan intent has not already been specified by the
        user. If this function is called with disable=true, this
        behaviour is turned off and the CALIBRATE_WVR intent is not
        automatically added. The default behaviour is restored with
        disable=false.

        If there are no antennas in the array with WVR's then this
        observing mode will always automatically remove the
        CALIBRATE_WVR intents as no calculation of coefficients is
        possible.
        """
        return self._offshoot.disableWVRCalibration(disable)

    def applyWVRCalibration(self, wvrCalibration):
        """
        Update the WVR coefficients used by the online system. The
        coefficients are used by the correlator software to convert the
        temperatures measured by the water-vapour radiometer (WVR) to a
        path-length and by the delay server to adjust the calculated
        delays for both the wet and dry components of the atmosphere.   

        These coefficients are computed after the end of a scan with
        the CALIBRATE_WVR intent. They can be obtained using the
        getRawWVRCalResult function of the CalResults class.

        When this function is called the coefficients are applied as
        soon as possible. This function should not be called while a
        subscan is running.

        """
        return self._offshoot.applyWVRCalibration(wvrCalibration)

    def getWVRintegrationTime(self):
        """
        Returns the current integration time of all the water vapour
        radiometers in this array. The integration time of all WVR's in
        this array must be the same if its to be correctly collected
        and used in the online and offline software systems
        """
        return self._offshoot.getWVRintegrationTime(self)

    def getInterferometryController(self):
        """
        Get the Array Mount Controller offshoot.  This allows more
        access to the methods within the controller.
        """
        import CCL.InterferometryController
        return CCL.InterferometryController.InterferometryController\
               (self._offshoot.getInterferometryController, self._logger)

    def enableLLCCorrection(self, enable = True):
        '''
        This method will enable (or disble if the parameter is false) the
        LLC Length correction on all antenna in the Array.

        On error an AsynchronousFailureExcption is thrown
        '''
        self._offshoot.enableLLCCorrection(enable)        

    def isLLCCorrectionEnabled(self):
        '''
        This method checks to see if the LLCs on all antennas are enabled
        or disabled.  If the result is a mix, or other issues are encountered
        an AsynchronousFailureExcption is thrown
        '''

        return self._offshoot.isLLCCorrectionEnabled()        
        
        
    def getTimeToLLCReset(self):
        '''
        This method determines the maximum (estimated) time before one
        of the LLCs associated with this array needs to be reset.
        
        The value is returned in seconds
        
        On error an AsynchronousFailureExcption is thrown
        '''
        return (self._offshoot.getTimeToLLCReset()/1.0E7)

    def resetLLCStretchers(self):
        '''
        This method will cause all LLCs in the Array to be reset
        to the optimum value of the stretcher.

        On error an AsynchronousFailureExcption is thrown
        '''
        self._offshoot.resetLLCStretchers()

    def setLLCStretcherPosition(self, position):
        '''
        This method will set the LLC to the specified position (expressed
        as a fraction of the total range) 0.0 - 1.0.  To prevent setting it
        to a saturated position valid values are 0.05- 0.95.
        '''
        self._offshoot.setLLCStretcherPosition(position)
        
    def getLLCPolarizationCalRequired(self):
        '''
        This method checks if a polarization calibration is required
        on any LLC associated with this array.
        
        On error an AsynchronousFailureExcption is thrown
        '''
        return self._offshoot.getLLCPolarizationCalRequired()


    def doLLCPolarizationCalibration(self, timeout = 900,
                                     forceCalibration=True):
        '''
        This method will cause all LLCs associated with this array
        to perform a polarization calibration.  If the force calibration
        flag is true then the calibration is always performed, if it is
        false then the calibartion is performed only if required on a LLC
        by LLC basis.
        
        The timeout parameter is the client side timeout (in seconds)
        '''
        cb = SimpleCallbackImpl()
        self._offshoot.doLLCPolarizationCalibrationAsynch(timeout,
                           forceCalibration, cb.getExternalInterface())
        cb.waitForCompletion(timeout+1)


    # These methods actually load and unload the configurations and
    # calibrations from the correlator.  They are overridden in the
    # simulator, and need to be actually implemented here.
    def _loadConfigurations(self, SpectralSpecList):
        '''
        This is the method which actually would send this to the hardware
        for the real case in simulation it is easier
        '''
        return self._offshoot.loadConfigurations(SpectralSpecList)
                

    def _loadCalibrations(self, corrCalSequence):
        '''
        This is the method which actually would send this to the hardware
        for the real case in simulation it is easier
        '''
        return self._offshoot.loadCalibrations(corrCalSequence)

    def _clearConfigurations(self, CorrConfigIdList):
        '''
        This is the method which actually would cause the correlator to
        release the configuration slots.

        Note this also releases all Calibrations associated with this
        configuration.
        '''
        return self._offshoot.clearConfigurations(CorrConfigIdList)

    def _clearCalibrations(self, calibrationsToRelease):
        '''
        This is the method which actually would cause the correlator to
        release the calibration slots.
        '''
        return self._offshoot.clearCalibrations(calibrationsToRelease)

    def _getTimeOrderedConfigList(self):
        '''
        This helper method returns all configurations with the oldest
        configuration first.
        '''
        timeMap = {}
        for key in self._corrIdList:
            if self._corrIdList[key].LastTimeUsed not in timeMap:
                timeMap[self._corrIdList[key].LastTimeUsed] = []
            timeMap[self._corrIdList[key].LastTimeUsed].append(key)
            
        timeKeys = timeMap.keys()
        timeKeys.sort()

        returnList = []
        for key in timeKeys:
            returnList += timeMap[key]
        return returnList
        
    def _getTimeOrderedCalList(self):
        '''
        This helper method returns all calibrations with the oldest
        calibration first.  Calibrations marked with None are omitted.
        '''
        timeMap = {}
        for key in self._calIdList:
            if self._calIdList[key] is None:
                continue
            if self._calIdList[key].CalTime not in timeMap:
                timeMap[self._calIdList[key].CalTime] = []
            timeMap[self._calIdList[key].CalTime].append(key)

        timeKeys = timeMap.keys()
        timeKeys.sort()

        returnList = []
        for key in timeKeys:
            returnList += timeMap[key]
        return returnList
