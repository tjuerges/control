// "$Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010 
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.ArrayList;
import java.util.Iterator;
//Move this to ObservingModeBaseUtil
import java.util.logging.Level;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.ACSErrTypeOK.wrappers.ACSErrOKAcsJCompletion;
import alma.AntennaMotionPatternMod.AntennaMotionPattern;
import alma.CalDataOriginMod.CalDataOrigin;
import alma.CalibrationFunctionMod.CalibrationFunction;
import alma.CalibrationSetMod.CalibrationSet;
import alma.Control.AntInterferometryController;
import alma.Control.CorrelatorCalibrationSequenceSpec;
import alma.Control.DelaySource;
import alma.Control.EphemerisRow;
import alma.Control.InterferometryController;
import alma.Control.InterferometryControllerHelper;
import alma.Control.InterferometryControllerOperations;
import alma.Control.InterferometryControllerPOATie;
import alma.Control.LLCStretcherResetData;
import alma.Control.ScanIntentData;
import alma.Control.SimpleCallback;
import alma.Control.SkyDelayServer;
import alma.Control.SkyDelayServerHelper;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.StaticResource;
import alma.Control.Common.Util;
import alma.Control.CommonCallbacks.AntennaCallbackImpl;
import alma.Control.CommonCallbacks.BooleanCallbackImpl;
import alma.Control.CommonCallbacks.DurationCallbackImpl;
import alma.Control.LLCPackage.LLCPositionResetCallbackImpl;
import alma.ControlCommonExceptions.AsynchronousFailureEx;
import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
import alma.ControlCommonExceptions.wrappers.AsynchronousFailureAcsJCompletion;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ControlExceptions.wrappers.AcsJTimeoutEx;
import alma.ControlExceptions.wrappers.TimeoutAcsJCompletion;
import alma.CorrErr.ArrayInUseEx;
import alma.CorrErr.CalibrationIdNotExistEx;
import alma.CorrErr.ConfigurationIdNotExistEx;
import alma.CorrErr.CorrHardwareEx;
import alma.CorrErr.CorrResourceConflictEx;
import alma.CorrErr.FailedToConnectBDDEx;
import alma.CorrErr.InvalidAntennaIdEx;
import alma.CorrErr.InvalidArrayEx;
import alma.CorrErr.InvalidCalibrationIdEx;
import alma.CorrErr.InvalidConfigEx;
import alma.CorrErr.InvalidConfigurationIdEx;
import alma.CorrErr.InvalidFlowNumberEx;
import alma.CorrErr.InvalidSubScanTimingEx;
import alma.CorrErr.SubarrayInUseEx;
import alma.CorrErr.TooManyConfigsEx;
import alma.Correlator.CalibrationInformation;
import alma.Correlator.ObservationControl;
import alma.Correlator.SubScanCorrelatorData;
import alma.Correlator.SubscanInformation;
import alma.Correlator.SubscanTiming;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.GetUVWErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.DelayServerExceptions.InitializeServerErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.ScanIntentMod.ScanIntent;
import alma.TmcdbErrType.TmcdbConnectionFailureEx;
import alma.TmcdbErrType.TmcdbErrorEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogLevel;
import alma.acs.time.TimeHelper;
import alma.acs.util.UTCUtility;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.asdmIDL.CalWVRTableIDL;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.types.FieldSourceTSolarSystemObjectType;
import alma.entity.xmlbinding.valuetypes.SkyCoordinatesT;
import alma.entity.xmlbinding.valuetypes.types.SkyCoordinatesTSystemType;
import alma.log_audience.DEVELOPER;
import alma.log_audience.OPERATOR;
import alma.Control.SourceOffsetFrame;
import alma.Control.SourceOffset;

// This class encapsulates the use of the Correlator by an observing mode this
// includes the functionality of the Delay Server and the WVRs.

public class InterferometryControllerImpl extends
        ObservingModeCore<AntInterferometryController> implements
        InterferometryControllerOperations {
 
    // These are the objects for making this class an offshoot
    private InterferometryControllerPOATie tieBindingClass = null;
    private InterferometryController corbaOffshoot = null;

    // Reference to the delay server which is currently serving this array in
    // the correlator
    private SkyDelayServer delayServer = null;

    // A reference to the component we use to communicate with the correlator
    private ObservationControl correlator = null;

    // Indicates if any WVR's in the array are publishing data.
    boolean workingWVRs = false;

    // Callback Objects used in communication with Correlator
    private CalibrationSeqCBImpl calibrationSeqCB = null;
    private SubscanSeqCBImpl subscanSeqCB = null;

    // Expected end time of the calibration sequence
    long calibrationSequenceEndTime = 0;

    // The data member control whether WVR intents are automatically added to
    // all scans.
    boolean automaticallyAddWVRIntent = true;

    public InterferometryControllerImpl(ContainerServices cs,
            ObservingModeArray array) throws ObsModeInitErrorEx {
        super(cs, array);
        this.array = array;

        createAntModeControllers(
                "alma.Control.AntInterferometryControllerHelper",
                "AntInterferometryController");

        try {
            setupCorrelator();
            createDelayServer();
            configureWVRs();
        } catch (ObsModeInitErrorEx ex) {
            cleanUp();
            throw (ex);
        }

        try {
            devLog.fine("Trying to enable the LLC corrections");
            enableLLCCorrection(true);
        } catch (AsynchronousFailureEx ex) {
            String msg = "There was a problem enabling the LLC corrections";
            //Move this to ObservingModeBaseUtil
            logger.logToAudience(Level.WARNING, msg + " Underlying error is: "
                + Util.getNiceErrorTraceString(ex.errorTrace), OPERATOR.value);
        }
    }

    @Override
    public void cleanUp() {
        devLog.fine("Interferometry controller shutting down.");
        if (delayServer != null) {
            cs.releaseComponent(delayServer.name());
            devLog.finer("Completed deactivating the delay server.");
        }
        if (correlator != null) {
            // Normally there should not be a subscan sequence running. But just
            // in case there is now would be a good time to shut it down.
            // Correlator should ignore this call if a subscan sequence is not
            // running.
            this.stopSubscanSequence();

            String msg;
            try {
                correlator.clearAllIds(array.getArrayName());
                devLog.finer("Completed clearing all correlator id's.");
            } catch (InvalidArrayEx ex) {
                msg = "The correlator cannot clear its configuration and calibration id's.";
                msg += " This may prevent subsequent use of the correlator.";
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
            } catch (ArrayInUseEx ex) {
                msg = "The correlator cannot clear its configuration and calibration id's.";
                msg += " This may prevent subsequent use of the correlator.";
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
            }
            try {
                correlator.destroySubArray(array.getArrayName());
                devLog.finer("Completed destroying the correlator array");
            } catch (InvalidArrayEx ex) {
                msg = "The correlator cannot destroy its array.";
                msg += " This may prevent subsequent use of the correlator.";
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
            } catch (ArrayInUseEx ex) {
                msg = "The correlator cannot destroy its array.";
                msg += " This may prevent subsequent use of the correlator.";
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
            }
            correlator = null;
        }

        try {
            devLog.fine("Trying to disable the LLC corrections");
            enableLLCCorrection(false);
        } catch (AsynchronousFailureEx ex) {
            String msg = "There was a problem disabling the LLC corrections";
            //Move this to ObservingModeBaseUtil
            logger.logToAudience(Level.WARNING, msg + " Underlying error is: "
                + Util.getNiceErrorTraceString(ex.errorTrace), OPERATOR.value);
        }

        devLog.fine("Completed shutting down the interferometry controller.");
    }

    /* ------------------------- Setup Methods ------------------ */
    public void createDelayServer() throws ObsModeInitErrorEx {
        if (delayServer != null) {
            devLog.warning("Delay server already exists. This is unexpected.");
            return;
        }

        try {
            // Create a DelayServer component dynamically.
            ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
            spec.setComponentType("IDL:alma/Control/SkyDelayServer:1.0");
            spec.setComponentName(array.getArrayComponentName()
                    + "/DELAYSERVER");
            logger.fine("Creating delay server with name '"
                    + spec.getComponentName() + "'.");
            org.omg.CORBA.Object obj = cs.getDynamicComponent(spec, false);
            delayServer = SkyDelayServerHelper.narrow(obj);
            // Initialize the DelayServer.
            delayServer.initializeServer(array.getArrayName());

            // Tell the delay server about all the antennas in this array.
            // TODO. This will halt at the first exception. Find if there is
            // any point in trying the remaining antennas if an
            // AddAntennaError exception is thrown
            for (String antenna : array.getAntennas()) {
                delayServer.addAntenna(antenna);
            }
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Cannot create a delay server";
            ObservingModeUtilities.throwObsModeInitErrorEx(msg,
                    new AcsJObsModeInitErrorEx(ex), logger);
        } catch (InitializeServerErrorEx ex) {
            String msg = "Cannot initialize the delay server";
            ObservingModeUtilities.throwObsModeInitErrorEx(msg,
                    new AcsJObsModeInitErrorEx(ex), logger);
        } catch (TmcdbErrorEx ex) {
            String msg = "Error adding antenna(s) to the delay server. ";
            msg += "TMCDB error.";
            ObservingModeUtilities.throwObsModeInitErrorEx(msg,
                    new AcsJObsModeInitErrorEx(ex), logger);
        } catch (TmcdbConnectionFailureEx ex) {
            String msg = "Error adding antenna(s) to the delay server. ";
            msg += "TMCDB connection error.";
            ObservingModeUtilities.throwObsModeInitErrorEx(msg,
                    new AcsJObsModeInitErrorEx(ex), logger);
        }
    }

    protected void setupCorrelator() throws ObsModeInitErrorEx {
        try {
            if (correlator == null) {
                // We need to get the correct observation Control
                ResourceManager resMng = ResourceManager.getInstance(cs);
                String corrCompName = null;
                corrCompName = array
                        .getCorrelatorObservationControlComponentName();

                // Define the Correlator observation control component
                Resource<ObservationControl> resource = new StaticResource<ObservationControl>(
                        cs, corrCompName,
                        "alma.Correlator.ObservationControlHelper", false,
                        false, logger);
                resMng.addResource(resource);
                resMng.acquireResources();

                // Get a reference to the component
                correlator = resMng.getComponent(corrCompName);
            }

            // Defining this array in the correlator
            String msg = "Defining, in the correlator, an array called "
                    + array.getArrayName() + " with antennas [";
            String[] antennaList = antennaModeController.keySet().toArray(
                    new String[0]);

            for (String antenna : antennaList) {
                msg += antenna + ", ";
            }
            msg = msg.substring(0, msg.length() - 2) + "]";
            devLog.info(msg);
            correlator.setSubArray(array.getArrayName(), antennaList, array
                    .getCorrelatorBDDStreamInfo());

        } catch (InvalidArrayEx ex) {
            String msg = "Invalid array error when defining the array in the correlator.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (AcsJBadParameterEx ex) {
            String msg = "Unknown correlator type";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Cannot get a reference to a correlator observation control component";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (InvalidAntennaIdEx ex) {
            String msg = "Invalid antena when defining the array in the correlator.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (FailedToConnectBDDEx ex) {
            String msg = "Failed to connect to bulkdata distributor";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (InvalidFlowNumberEx ex) {
            String msg = "Invalid flow number";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        }
    }

    protected void configureWVRs() throws ObsModeInitErrorEx {
        try {
            workingWVRs = isWVRSendingData();
            final String arrayName = array.getArrayName();
            correlator.expectWVRData(arrayName, workingWVRs);
            dataCapturer.expectWvrData(workingWVRs);
            if (workingWVRs) {
                correlator.setWVRIntegrationTime(arrayName,
                        getWVRintegrationTime());
            }
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot configure the correlator to use WVR data";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (SubarrayInUseEx ex) {
            String msg = "Cannot configure the correlator to use WVR data";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (InvalidConfigEx ex) {
            String msg = "Cannot configure the correlator to use WVR data";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (ObservingModeErrorEx ex) {
            String msg = "Cannot configure the correlator to use WVR data";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        }
    }

    // --------------- External CORBA Interface ---------------------
    @Override
    public InterferometryController getInterferometryController()
            throws ContainerServicesEx {
        // This method returns the Offshoot reference to this object it will
        // create the POA object if that has not already been created.
        try {
            if (tieBindingClass == null) {
                tieBindingClass = new InterferometryControllerPOATie(this);
            }

            if (corbaOffshoot == null) {
                corbaOffshoot = InterferometryControllerHelper.narrow(cs.activateOffShoot(tieBindingClass));
            }
        } catch (AcsJContainerServicesEx jex) {
            jex.log(logger);
            throw jex.toContainerServicesEx();
        }
        return corbaOffshoot;
    }

    @Override
    public void setPhaseDirection(String fieldSource)
            throws IllegalParameterErrorEx {
        FieldSourceT src = ObservingModeUtilities.deserializeFieldSource(
                fieldSource, logger);
        setPhaseDirection(src);
    }

    public void setPhaseDirection(FieldSourceT fieldSource) throws IllegalParameterErrorEx {
        SourceOffset zeroOffset = new SourceOffset(0, 0, 0, 0, SourceOffsetFrame.HORIZON); 
        setPhaseDirection(fieldSource, zeroOffset);
    }

    public void setPhaseDirection(FieldSourceT fieldSource, SourceOffset offset)
            throws IllegalParameterErrorEx {
        if (delayServer == null) {
            logger.severe("Unable to set phase direction no delay server!");
            return;
        } 
        delayServer.addSource(fieldToDelaySource(fieldSource, offset, 
                UTCUtility.utcJavaToOmg(System.currentTimeMillis())));
    }

    @Override
    public String getDelayServerName() {
        if (delayServer == null) {
            return "";
        }
        return delayServer.name();
    }

    @Override
    public void applyWVRCalibration(ASDMDataSetIDL wvrCalib)
            throws ObservingModeErrorEx {
        final long now = ObservingModeUtilities.getCurrentACSTime();
        final CalWVRTableIDL calWVRTable = wvrCalib.calWVR;
        final String arrayName = array.getArrayName();
        try {
            delayServer.setAtmosphericDelayModelASDM(now, calWVRTable);
            correlator.setWVRCoefficients(arrayName, calWVRTable);
            dataCapturer.sendWVRCoefficients(arrayName, calWVRTable);
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot set the WVR coefficients in the correlator";
            msg += " as the array name is incorrect.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (InvalidConfigEx ex) {
            String msg = "Cannot set the WVR coefficients in the correlator";
            msg += " as the coefficients are incorrect.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot set the WVR coefficients as data capture";
            msg += " cannot record the values.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataErrorEx ex) {
            String msg = "Cannot set the WVR coefficients as data capture";
            msg += " cannot record the values.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }

    }

    public void disableWVRCalibration(boolean disable) {
        /**
         * This function allows you to disable (or enable) the functions in this
         * class which automatically add the CALIBRATE_WVR intent to all scans.
         * 
         * @param disable
         *            set to true to disable the computation of WVR Calibrations
         *            unless explicitly specified by the user.
         */
        automaticallyAddWVRIntent = !disable;
    }

    public ScanIntentData[] addWVRIntent(ScanIntentData[] allScanIntents) {
        /**
         * This function adds a CALIBRATE_WVR intent if one is not specified and
         * we have working WVR's. It also removes this intent if none of the
         * WVR's, on this array, are working.
         * 
         * @param allScanIntents
         *            The input set of scan intents
         * @return the mondified scan intents
         */
        boolean needToAddIntent = automaticallyAddWVRIntent && workingWVRs;
        ArrayList<ScanIntentData> newIntents = new ArrayList<ScanIntentData>();
        for (ScanIntentData sid : allScanIntents) {
            if (sid.scanIntent == ScanIntent.CALIBRATE_WVR) {
                if (workingWVRs) {
                    needToAddIntent = false;
                    newIntents.add(sid);
                } else {
                    // as we do not have a WVR remove this intent by not copying
                }
            } else {
                newIntents.add(sid);
            }
        }
        if (needToAddIntent) {
            ScanIntentData sid = new ScanIntentData();
            sid.scanIntent = ScanIntent.CALIBRATE_WVR;
            sid.calDataOrig = CalDataOrigin.WVR;
            sid.calSet = CalibrationSet.UNSPECIFIED;
            sid.onlineCalibration = true;
            sid.antennaMotionPattern = AntennaMotionPattern.UNSPECIFIED;
            sid.calFunction = CalibrationFunction.UNSPECIFIED;
            newIntents.add(sid);
        }
        return newIntents.toArray(new ScanIntentData[0]);
    }

    public int[] loadConfigurations(SpectralSpecT[] specs)
            throws ObservingModeErrorEx {
        int numSpecs = specs.length;
        String[] spectralSpecsAsXML = new String[numSpecs];
        for (int idx = 0; idx < numSpecs; idx++) {
            spectralSpecsAsXML[idx] = ObservingModeUtilities.serializeSpectralSpec(
                    specs[idx], logger).xmlString;
        }
        return loadConfigurations(spectralSpecsAsXML);
    }
    
    @Override
    public int[] loadConfigurations(String[] specs) throws ObservingModeErrorEx {
        try {
            return correlator.loadConfigurations(array.getArrayName(), specs);
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot load a configuration into the correlator";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (InvalidConfigEx ex) {
            String msg = "Cannot load a configuration into the correlator";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (TooManyConfigsEx ex) {
            String msg = "Cannot load a configuration into the correlator";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        return null; // should never get here
    }

    /**
     * Until I figure out how best to do this, just pass this along to the
     * Observation Control
     */
    public SubscanTiming[] getReconfigurationTimes(int[] calibIds, long[] subscanDurations)
            throws ObservingModeErrorEx {
        SubscanTiming[] sst = null;
        try {
            sst = correlator.getReconfigurationTimes(array.getArrayName(),
                    calibIds, subscanDurations);
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot start the subscan.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (CalibrationIdNotExistEx ex) {
            String msg = "Cannot start the subscan.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (InvalidSubScanTimingEx ex) {
            String msg = "Cannot start the subscan.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        return sst;
    }

    public SubscanTiming[] getCalibrationReconfigurationTimes(int[] configIds)
            throws ObservingModeErrorEx {
        try {
            return correlator.getCalibrationReconfigurationTimes(array
                    .getArrayName(), configIds);
        } catch (InvalidArrayEx ex) {
            String msg = "Unable to Calibrate the Correlator.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (ConfigurationIdNotExistEx ex) {
            String msg = "Unable to Calibrate the Correlator.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        return null; // Should never get here
    }

    /* This method returns a set of calibration IDs */
    public void startCalibrationSequence(long startTime,
            CalibrationInformation[] calInfo) throws ObservingModeErrorEx {
        if (calibrationSeqCB != null) {
            /*
             * Log some sort of error here, this means we never waited for the
             * previous calibration
             */

        }

        calibrationSeqCB = new CalibrationSeqCBImpl(cs);
        correlator.startCalibrationSequence(array.getArrayName(), startTime,
                calInfo, calibrationSeqCB.getCORBAObject());

        /* Calculate the endTime for the calibration here */
        long totalCalibTime = 0;
        for (CalibrationInformation ci : calInfo) {
            totalCalibTime += ci.subscanTime.setupTime
                    + ci.subscanTime.duration;
        }
        /* Only the processing time for the final subscan should be included */
        totalCalibTime += calInfo[calInfo.length - 1].subscanTime.processingTime;

        totalCalibTime += Util.toTimeInterval(5.0);
        calibrationSequenceEndTime = totalCalibTime + startTime;
    }

    /* This method waits until the expected end of the Calibration Sequence */
    public int[] waitForCalibration() throws ObservingModeErrorEx {
        if (calibrationSeqCB == null) {
            /* Nothing to do here */
            String msg = "No Calibration Sequence Active.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }

        /* Find the time remaining */
        long timeRemaining = calibrationSequenceEndTime
                - ObservingModeUtilities.getCurrentACSTime();
        if (timeRemaining < 0)
            timeRemaining = 0;

        /* Wait for the calibration scans to end */
        if (!calibrationSeqCB.waitForResult(timeRemaining)) {
            // If there are errors log them and throw an exception
            String msg = "Cannot calibrate the correlator.";
            alma.ACSErr.Completion[] c = calibrationSeqCB.getCompletions();
            if (c.length == 0) {
                msg += " No response was received within " + timeRemaining
                        + " seconds";
            } else {
                msg += ".The underlying error is has type=" + c[0].type
                        + " code=" + c[0].code;
                if (c[0].previousError.length > 0) {
                    msg += " and error trace";
                    for (alma.ACSErr.ErrorTrace et : c[0].previousError) {
                        msg += " " + Util.getNiceErrorTraceString(et);
                    }
                }
            }
            /* Probably should be a timeout */
            calibrationSeqCB = null;
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        int[] calibrationIds = calibrationSeqCB.getCalIds();
        calibrationSeqCB = null;
        calibrationSequenceEndTime = 0;
        return calibrationIds;
    }

    public void startSubscanSequence(long startTime,
            SubscanInformation[] subscanInfo) throws ObservingModeErrorEx {
        if (subscanSeqCB != null) {
            /*
             * Log some sort of error here, this means we never waited for the
             * previous calibration
             */
        }
        subscanSeqCB = new SubscanSeqCBImpl(cs, startTime, subscanInfo);
        correlator.startSubscanSequence(array.getArrayName(), startTime,
                array.getASDMEntRef(), 1,
                subscanInfo, subscanSeqCB.getCORBAObject());
        long now = ObservingModeUtilities.getCurrentACSTime();
        if (now + Util.toTimeInterval(.096) > startTime) {
            String msg = "Insufficient lead time for the subscan.";
            msg += " Subscan start time is " + Util.acsTimeToString(startTime);
            msg += " & time now is " + Util.acsTimeToString(now);
            logger.severe(msg);
        }
    }

    public void startSubscanSequence(long startTime,
            SubscanInformation[] subscanInfo, FieldSourceT src,
            SourceOffset[] offsets) throws ObservingModeErrorEx {

        // Convert all the field sources & offsets to delay sources. Do this
        // before starting the correlator in case there are problems with any of
        // them.
        final int numSubscans = offsets.length;
        long subscanStartTime = startTime;
        DelaySource[] delaySources = new DelaySource[numSubscans];
        for (int s = 0; s < numSubscans; s++) {
            try {
                delaySources[s] = fieldToDelaySource(src, offsets[s],
                        subscanStartTime);
            } catch (IllegalParameterErrorEx ex) {
                String msg = "Cannot execute the subscan sequence";
                msg += "as the source cannot be tracked by the delay server.";
                throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
            }
            SubscanTiming t = subscanInfo[s].subscanTime;
            subscanStartTime += t.setupTime + t.duration;
        }

        // Now kick off the correlator
        startSubscanSequence(startTime, subscanInfo);

        // let the delay server also know about these sources
        delayServer.addSources(delaySources);
    }
    
    public void stopSubscanSequence() {
        if (correlator == null) return;
        try {
            devLog.info("Stopping the subscan sequence");
            correlator.stopSubscanSequence(array.getArrayName());
        } catch (InvalidArrayEx ex) {
            String msg = "The correlator software cannot stop the subscan sequence";
            msg += " as the array called " + array.getArrayName();
            msg += " is incorrect. Ignoring this problem.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        } catch (CorrResourceConflictEx ex) {
            String msg = "The correlator software cannot stop the subscan sequence";
            msg += " as there is a problem in the correlator data processor.";
            msg += " Ignoring this problem.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        } catch (CorrHardwareEx ex) {
            String msg = "The correlator software cannot stop the subscan sequence";
            msg += " as there is a problem in the correlator hardware.";
            msg += " Ignoring this problem.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        }
    }
   
    // This function performs all the specified calibrations without moving the
    // antenna. This is only correct if:
    // 1. The antenna is at the same place for all sub-scans in the sequence
    // 2. You have previously moved the antenna to point at that place
    @Override
    public int[] loadCalibrations(CorrelatorCalibrationSequenceSpec calSpecs)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
 
        final int numCals = calSpecs.calibrationList.length;
        int[] calList = new int[numCals];
        for (int c = 0; c < numCals; c++) {
            int specId = calSpecs.calibrationList[c].spectralId;
            calList[c] = calSpecs.configIds[specId];
        }
        SubscanTiming[] sst = getCalibrationReconfigurationTimes(calList);
        CalibrationInformation[] calInfo = new CalibrationInformation[numCals];
        for (int c = 0; c < numCals; c++) {
            calInfo[c] = new CalibrationInformation(calList[c], sst[c]);
        }
        final long startTime = ObservingModeUtilities.getCurrentACSTime()
                + Util.toTimeInterval(0.2);
        startCalibrationSequence(Util.roundTE(startTime), calInfo);
        return waitForCalibration();
    }

    // This method waits until the next subscan starts. It throws an
    // exception if the subscan did not start within the expected
    // amount of time.
    public void waitForSubscanStart() throws ObservingModeErrorEx {
        if (subscanSeqCB == null) {
            String msg = "Cannot wait for the subscan to start";
            msg += " as the startSubscanSequence function has not been called.";
            ObservingModeUtilities.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(), logger);
        }
        subscanSeqCB.waitForSubscanStart();
    }

    // This method waits until the next subscan to end. It throws an
    // exception if the subscan did not end within the expected
    // amount of time.
    public void waitForSubscanEnd() throws ObservingModeErrorEx {
        String msg;
        if (subscanSeqCB == null) {
            msg = "Cannot wait for the subscan to end";
            msg += " as the startSubscanSequence function has not been called.";
            ObservingModeUtilities.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(), logger);
        }
        subscanSeqCB.waitForSubscanEnd();
        final int last = subscanSeqCB.numberCompleted() - 1;
        SubScanCorrelatorData data = subscanSeqCB.getSubscanCorrelatorDatum(last);
        final int subscanNum = subscanSeqCB.subscan(last);
        msg = "Subscan " + subscanNum + " contains " + data.totalIntegrations
                + " integration(s) (" + data.binaryDataSizeOfFullResolution
                + " total bytes) and "
                + data.numberSubIntegrationsPerIntegration
                + " subintegration(s)/integration ("
                + data.binaryDataSizeOfChannelAverage + " total bytes).";
        devLog.debug(msg);

        try {
            dataCapturer.sendSubScanCorrelatorData(array.getArrayName(), subscanNum, data);
        } catch (TableUpdateErrorEx ex) {
            msg = "Unable to send some correlator data to data capture";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataErrorEx ex) {
            msg = "Unable to send some correlator data to data capture";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (GetUVWErrorEx ex) {
            msg = "Unable to send some correlator data to data capture";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }

    public void clearAllIds() throws InvalidArrayEx {
        try {
            correlator.clearAllIds(array.getArrayName());
        } catch (ArrayInUseEx ex) {
            // TODO. Fix this up
            String msg = "The correlator cannot clear its configuration and calibration id's.";
            msg += " This may prevent subsequent use of the correlator.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        }
    }

    @Deprecated
    @Override
    public void clearConfigurationIDs(int[] corrConfIDs)
            throws ObservingModeErrorEx {
        try {
            if (corrConfIDs != null && corrConfIDs.length > 0) {
                // TODO. A log that handles arrays with more than one element!
                logger.fine("Clearing the correlator configuration ID: "
                        + corrConfIDs[0]);
                correlator.clearConfigurationIds(array.getArrayName(),
                        corrConfIDs);
            }
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " array name of " + array.getArrayName()
                    + " is incorrect.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (ConfigurationIdNotExistEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + corrConfIDs
                    + " could not be cleared";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (InvalidConfigurationIdEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + corrConfIDs
                    + " is in use.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }
    
    // This throws no exceptions to ensure we can shutdown without having to
    // catch exceptions.
    @Override
    public void clearConfigurations(int[] configIDs) {
        try {
            if (configIDs != null && configIDs.length > 0) {
                devLog.fine("Clearing correlator configuration id's: "
                        + configIDs);
                correlator.clearConfigurationIds(array.getArrayName(),
                        configIDs);
            }
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " array name of " + array.getArrayName()
                    + " is incorrect.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        } catch (ConfigurationIdNotExistEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + configIDs
                    + " could not be cleared";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        } catch (InvalidConfigurationIdEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + configIDs
                    + " is in use.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        }
    }

    @Deprecated
    public void clearCalibrationIDs(int[] corrCalIDs)
            throws ObservingModeErrorEx {
        try {
            if (corrCalIDs != null && corrCalIDs.length > 0) {
                // TODO. A log that handles arrays with more than one element!
                logger.fine("Clearing the correlator calibration ID: "
                        + corrCalIDs[0]);
                correlator
                        .clearCalibrationIds(array.getArrayName(), corrCalIDs);
            }
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " array name of " + array.getArrayName()
                    + " is incorrect.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (CalibrationIdNotExistEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + corrCalIDs
                    + " could not be cleared";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (InvalidCalibrationIdEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + corrCalIDs
                    + " is in use.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }

    // This throws no exceptions to ensure we can shutdown without having to
    // catch exceptions.
    @Override
    public void clearCalibrations(int[] calIDs) {
        try {
            if (calIDs != null && calIDs.length > 0) {
                devLog.fine("Clearing the correlator calibration id's: "
                        + calIDs);
                correlator.clearCalibrationIds(array.getArrayName(), calIDs);
            }
        } catch (InvalidArrayEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " array name of " + array.getArrayName()
                    + " is incorrect.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        } catch (CalibrationIdNotExistEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + calIDs
                    + " could not be cleared";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        } catch (InvalidCalibrationIdEx ex) {
            String msg = "Cannot clear the correlator configuration as the"
                    + " previous correlator configuration of " + calIDs
                    + " is in use.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        }
    }

   /**
     * @return True if any WVR in this array is publishing data
     */
    @Override
    public boolean isWVRSendingData() {
        Iterator<String> keyIter = antennaModeController.keySet().iterator();
        boolean isPublishingData = false;
        while (keyIter.hasNext() && !isPublishingData) {
            String key = keyIter.next();
            isPublishingData = antennaModeController.get(key)
                    .isWVRSendingData();
        }
        return isPublishingData;
    }

    /**
     * Get the integration time of all the WVR's that are publishing data in
     * this array.
     * 
     * @return The integration time in units of 100ns.
     * @throws ObservingModeErrorEx
     *             If there is any difference in the integration times or if the
     *             integration time on any operational WVR could not be
     *             obtained.
     */
    @Override
    public long getWVRintegrationTime() throws ObservingModeErrorEx {
        Iterator<String> keyIter = antennaModeController.keySet().iterator();
        long intTime = -1;
        while (keyIter.hasNext()) {
            String key = keyIter.next();
            AntInterferometryController antController = antennaModeController
                    .get(key);
            if (antController.isWVRSendingData()) {
                long currentTime = 0;
                try {
                    currentTime = antController.getWVRIntegrationTime();
                } catch (UnallocatedEx ex) {
                    String msg = "Cannot get the integration time for the WVR's";
                    msg += " as antenna " + key + "is not assigned to array ";
                    msg += array.getArrayName();
                    throwObservingModeErrorEx(msg,
                            new AcsJObservingModeErrorEx(ex));
                } catch (HardwareFaultEx ex) {
                    String msg = "Cannot get the integration time for the WVR's";
                    msg += " as the WVR on antenna " + key;
                    msg += " is not responding.";
                    throwObservingModeErrorEx(msg,
                            new AcsJObservingModeErrorEx(ex));
                }
                if (intTime < 0) {
                    intTime = currentTime;
                }
                if (intTime != currentTime) {
                    String msg = "All the WVR's in array ";
                    msg += array.getArrayName();
                    msg += " must have the same integration time.";
                    msg += " One has an integration time of ";
                    msg += intTime * 100E-9;
                    msg += " secs. and another has an integration time of ";
                    msg += currentTime * 100E-9 + " secs.";
                    throwObservingModeErrorEx(msg,
                            new AcsJObservingModeErrorEx());
                }
            }
        }
        return intTime;
    }

    /* ------------- LLC Observing Mode Operations ------------- */
    /* See LLCObservingModeInterface.idl for a description of this method */
    @Override
    public void enableLLCCorrection(boolean enable)
            throws AsynchronousFailureEx {
        final long timeout = 3;
        AntennaCallbackImpl cbObj;

        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Failed to create callback object "
                    + "for asynchronous execution");
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }

        Iterator<String> keyIter = antennaModeController.keySet().iterator();

        while (keyIter.hasNext()) {
            String key = keyIter.next();
            try {
                cbObj.addExpectedResponse(key);
            } catch (AcsJInvalidRequestEx e) {
                /* Something is really wrong. but go on */
                continue;
            }
            antennaModeController.get(key).enableLLCCorrectionAsynch(enable,
                    cbObj.getExternalInterface());
        }

        try {
            cbObj.waitForCompletion(timeout);
        } catch (AcsJAsynchronousFailureEx jex) {
            throw jex.toAsynchronousFailureEx();
        } catch (AcsJTimeoutEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Timed out waiting for responses");
            throw jex.toAsynchronousFailureEx();
        }

    }

    /* See LLCObservingModeInterface.idl for a description of this method */
    @Override
    public boolean isLLCCorrectionEnabled() throws AsynchronousFailureEx {
        final long timeout = 7;
        BooleanCallbackImpl cbObj;

        try {
            cbObj = new BooleanCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Failed to create callback object "
                    + "for asynchronous execution");
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }

        Iterator<String> keyIter = antennaModeController.keySet().iterator();

        while (keyIter.hasNext()) {
            String key = keyIter.next();
            try {
                cbObj.addExpectedResponse(key);
            } catch (AcsJInvalidRequestEx e) {
                /* Something is really wrong. but go on */
                continue;
            }
            antennaModeController.get(key).isLLCCorrectionEnabledAsynch(
                    cbObj.getExternalInterface());
        }

        try {
            cbObj.waitForCompletion(timeout);
        } catch (AcsJAsynchronousFailureEx jex) {
            throw jex.toAsynchronousFailureEx();
        } catch (AcsJTimeoutEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Timed out waiting for responses");
            throw jex.toAsynchronousFailureEx();
        }

        if (cbObj.allTrue() || cbObj.allFalse()) {
            return cbObj.allTrue();
        }

        // Antennas Disagree
        AcsJAsynchronousFailureEx jEx = new AcsJAsynchronousFailureEx();
        String msg = "LLC Correction is not consistently enabled"
                + " for all antennas in the array.";
        jEx.setProperty("Message", msg);
        throw jEx.toAsynchronousFailureEx();
    }

    /* See LLCObservingModeInterface.idl for a description of this method */
    @Override
    public long getTimeToLLCReset() throws AsynchronousFailureEx {
        final long timeout = 7; // 7 seconds ought to be enough
        DurationCallbackImpl cbObj;
        try {
            cbObj = new DurationCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Failed to create callback object "
                    + "for asynchronous execution");
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }

        Iterator<String> keyIter = antennaModeController.keySet().iterator();

        while (keyIter.hasNext()) {
            String key = keyIter.next();
            try {
                cbObj.addExpectedResponse(key);
                antennaModeController.get(key).getLLCTimeToResetAsynch(
                        cbObj.getExternalInterface());
            } catch (AcsJInvalidRequestEx e) {
                /* Something is really wrong. but go on */
                continue;
            }
        }

        try {
            cbObj.waitForCompletion(timeout);
        } catch (AcsJAsynchronousFailureEx jex) {
            throw jex.toAsynchronousFailureEx();
        } catch (AcsJTimeoutEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Timed out waiting for responses");
            throw jex.toAsynchronousFailureEx();
        }

        return cbObj.getMinimumTime();
    }

    /* See LLCObservingModeInterface.idl for a description of this method */
    @Override
    public void resetLLCStretchers() throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        final long timeout = 7;
        LLCPositionResetCallbackImpl cbObj;
        try {
            cbObj = new LLCPositionResetCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Failed to create callback object "
                    + "for asynchronous execution");
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }

        Iterator<String> keyIter = antennaModeController.keySet().iterator();

        /* Get the start of the reset operation */
        long startTime = TimeHelper.getTimeStamp().value;

        while (keyIter.hasNext()) {
            String key = keyIter.next();
            try {
                cbObj.addExpectedResponse(key);
            } catch (AcsJInvalidRequestEx e) {
                /* Something is really wrong. but go on */
                continue;
            }
            antennaModeController.get(key).resetLLCStretcherAsynch(
                    cbObj.getExternalInterface());
        }

        AcsJAsynchronousFailureEx jex = null;
        try {
            cbObj.waitForCompletion(timeout);
        } catch (AcsJAsynchronousFailureEx e) {
            jex = new AcsJAsynchronousFailureEx(e);
        } catch (AcsJTimeoutEx e) {
            jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Timed out waiting for responses");
        }
        // Now get the stop time of the duratoin
        long endTime = TimeHelper.getTimeStamp().value;
        LLCStretcherResetData[] resetData = cbObj.getLLCStretcherResetData();

        /*
         * Store whatever information we got back from the reset in data capture
         */
        alma.ControlCommonExceptions.wrappers.AcsJDataCapturerErrorEx exHlp = null;
        try {
            dataCapturer.sendLLCStretcherReset(resetData, startTime, endTime);
        } catch (TableUpdateErrorEx ex) {
            exHlp = new alma.ControlCommonExceptions.wrappers.AcsJDataCapturerErrorEx(
                    ex);
        } catch (DataErrorEx ex) {
            exHlp = new alma.ControlCommonExceptions.wrappers.AcsJDataCapturerErrorEx(
                    ex);
        }

        /*
         * If we've had a problem throw the exception if there is a problem with
         * the reset, send that type. Otherwise send the data capture error.
         */
        if (exHlp != null) {
            String msg = "Error calling DataCapturer sendLLCStretcherReset()"
                    + " function.";
            exHlp.setProperty("Message", msg);
            if (jex == null) {
                throw exHlp.toDataCapturerErrorEx();
            } else {
                logger.warning(msg);
            }
        }

        if (jex != null) {
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }
    }

    /* See LLCObservingModeInterface.idl for a description of this method */
    @Override
    public void setLLCStretcherPosition(float position)
            throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        final long timeout = 7;
        LLCPositionResetCallbackImpl cbObj;
        try {
            cbObj = new LLCPositionResetCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Failed to create callback object "
                    + "for asynchronous execution");
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }

        Iterator<String> keyIter = antennaModeController.keySet().iterator();

        /* Get the start of the reset operation */
        long startTime = TimeHelper.getTimeStamp().value;

        while (keyIter.hasNext()) {
            String key = keyIter.next();
            try {
                cbObj.addExpectedResponse(key);
            } catch (AcsJInvalidRequestEx e) {
                /* Something is really wrong. but go on */
                continue;
            }
            antennaModeController.get(key).setLLCStretcherPositionAsynch(
                    position, cbObj.getExternalInterface());
        }

        AcsJAsynchronousFailureEx jex = null;
        try {
            cbObj.waitForCompletion(timeout);
        } catch (AcsJAsynchronousFailureEx e) {
            jex = new AcsJAsynchronousFailureEx(e);
        } catch (AcsJTimeoutEx e) {
            jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Timed out waiting for responses");
        }
        // Now get the stop time of the duratoin
        long endTime = TimeHelper.getTimeStamp().value;
        LLCStretcherResetData[] resetData = cbObj.getLLCStretcherResetData();

        /*
         * Store whatever information we got back from the reset in data capture
         */
        alma.ControlCommonExceptions.wrappers.AcsJDataCapturerErrorEx exHlp = null;
        try {
            dataCapturer.sendLLCStretcherReset(resetData, startTime, endTime);
        } catch (TableUpdateErrorEx ex) {
            exHlp = new alma.ControlCommonExceptions.wrappers.AcsJDataCapturerErrorEx(
                    ex);
        } catch (DataErrorEx ex) {
            exHlp = new alma.ControlCommonExceptions.wrappers.AcsJDataCapturerErrorEx(
                    ex);
        }

        /*
         * If we've had a problem throw the exception if there is a problem with
         * the reset, send that type. Otherwise send the data capture error.
         */
        if (exHlp != null) {
            String msg = "Error calling DataCapturer sendLLCStretcherReset()"
                    + " function.";
            exHlp.setProperty("Message", msg);
            if (jex == null) {
                throw exHlp.toDataCapturerErrorEx();
            } else {
                logger.warning(msg);
            }
        }

        if (jex != null) {
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }
    }

    /* See LLCObservingModeInterface.idl for a description of this method */
    @Override
    public boolean getLLCPolarizationCalRequired() throws AsynchronousFailureEx {
        final long timeout = 7;
        BooleanCallbackImpl cbObj;

        try {
            cbObj = new BooleanCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Failed to create callback object "
                    + "for asynchronous execution");
            jex.log(logger);
            throw jex.toAsynchronousFailureEx();
        }

        Iterator<String> keyIter = antennaModeController.keySet().iterator();

        while (keyIter.hasNext()) {
            String key = keyIter.next();
            try {
                cbObj.addExpectedResponse(key);
            } catch (AcsJInvalidRequestEx e) {
                /* Something is really wrong. but go on */
                continue;
            }
            antennaModeController.get(key).getLLCPolarizationCalRequiredAsynch(
                    cbObj.getExternalInterface());
        }

        try {
            cbObj.waitForCompletion(timeout);
        } catch (AcsJAsynchronousFailureEx jex) {
            throw jex.toAsynchronousFailureEx();
        } catch (AcsJTimeoutEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Timed out waiting for responses");
            throw jex.toAsynchronousFailureEx();
        }

        return !cbObj.allFalse();
    }

    /* See LLCObservingModeInterface.idl for a description of this method */
    @Override
    public void doLLCPolarizationCalibrationAsynch(int timeout,
            boolean forceCalibration, SimpleCallback cb) {
        try {
            doLLCPolarizationCalibration(timeout, forceCalibration);
            cb.report(new ACSErrOKAcsJCompletion().toCorbaCompletion());
        } catch (AcsJAsynchronousFailureEx e) {
            cb.report(new AsynchronousFailureAcsJCompletion(e)
                    .toCorbaCompletion());
        } catch (AcsJTimeoutEx e) {
            cb.report(new TimeoutAcsJCompletion(e).toCorbaCompletion());
        }
    }

    /*
     * This is the pure java implementation which can be used from within this
     * class without the problem of the CORBA timeout.
     */
    protected void doLLCPolarizationCalibration(int timeout,
            boolean forceCalibration) throws AcsJAsynchronousFailureEx,
            AcsJTimeoutEx {
        AntennaCallbackImpl cbObj;
        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            AcsJAsynchronousFailureEx jex = new AcsJAsynchronousFailureEx(e);
            jex.setProperty("Message", "Failed to create callback object "
                    + "for asynchronous execution");
            jex.log(logger);
            throw jex;
        }

        Iterator<String> keyIter = antennaModeController.keySet().iterator();

        while (keyIter.hasNext()) {
            String key = keyIter.next();
            try {
                cbObj.addExpectedResponse(key);
            } catch (AcsJInvalidRequestEx e) {
                /* Something is really wrong. but go on */
                continue;
            }
            antennaModeController.get(key).doLLCPolarizationCalibration(
                    (long) (timeout * 1E7), forceCalibration,
                    cbObj.getExternalInterface());
        }
        cbObj.waitForCompletion(timeout + 1);
    }

    /* This method converts a Subscan Field Source to a delay source */
    private DelaySource fieldToDelaySource(FieldSourceT fs, SourceOffset offset, long startTime)
            throws IllegalParameterErrorEx {
        DelaySource delaySource = new DelaySource();

        if (fs.hasNonSiderealMotion() && fs.getNonSiderealMotion() == true) {
            // Its a solar system object
            // Initialize default values for fields that should not be used.
            delaySource.RA = 0.0;
            delaySource.Dec = 0.0;
            delaySource.DRA = 0.0;
            delaySource.DDec = 0.0;
            delaySource.Parallax = 0.0;
            delaySource.Epoch = 0.0;
            // These fields are used
            delaySource.PlanetaryObject = true;
            FieldSourceTSolarSystemObjectType planet = fs
                    .getSolarSystemObject();
            delaySource.ObjectName = planet.toString();
            if (planet == FieldSourceTSolarSystemObjectType.EPHEMERIS) {
                delaySource.positionList = ObservingModeUtilities
                        .ephemerisToIDL(fs.getSourceEphemeris(), logger);
            } else {
                delaySource.positionList = new EphemerisRow[0];
            }
        } else { // Its far away
            SkyCoordinatesT coords = fs.getSourceCoordinates();
            SkyCoordinatesTSystemType system = coords.getSystem();
            if (system != SkyCoordinatesTSystemType.J2000) {
                String msg = "The '" + system
                        + "' coordinate system is not supported.";
                logger.logToAudience(AcsLogLevel.ERROR, msg, OPERATOR.value);
                ObservingModeUtilities.throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            // These fields are used
            delaySource.RA = ObservingModeUtilities
                    .convertAngleToRadians(coords.getLongitude());
            delaySource.Dec = ObservingModeUtilities
                    .convertAngleToRadians(coords.getLatitude());
            delaySource.DRA = ObservingModeUtilities
                    .convertAngularVelocityToRadiansPerSecond(fs.getPMRA());
            delaySource.DDec = ObservingModeUtilities
                    .convertAngularVelocityToRadiansPerSecond(fs.getPMDec());
            delaySource.Parallax = ObservingModeUtilities
                    .convertAngleToRadians(fs.getParallax());
            delaySource.Epoch = 2451545.0 - 2400000.5; // J2000
            delaySource.PlanetaryObject = false;
            // Initialize default values for fields that should not be used.
            delaySource.ObjectName = "";
            delaySource.positionList = new EphemerisRow[0];
        }
        delaySource.sourceStartTime = startTime;
        delaySource.offset = offset;
        return delaySource;
    }
}
