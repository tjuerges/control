#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#


from CCL.utils import aggregate
from CCL.InterferometryControllerInterface import \
     InterferometryControllerInterface

from Control import CorrelatorCalibrationSequenceSpec
    
class InterferometryControllerSimulator(InterferometryControllerInterface):    
    def __init__(self, array):
        InterferometryControllerInterface.__init__(self, None, None)
        self._correlatorConfigSlots = range(self._MAX_CONFIGURATIONS)
        self._correlatorCalSlots = range(self._MAX_CALIBRATIONS)
        self._array = array      
            
    def setPhaseDirection(self, subscanFieldSource):
        """
        This sets the direction of the phase center to the specified source.
        """
        self._addExecutionLine("+ Phase Direction Set to:")
        pass

    def disableWVRCalibration(self, disable):
        self._addExecutionLine("+ WVR Calibration Disabled")

    def applyWVRCalibration(self, wvrCalibration):
        self._addExecutionLine("+ WCR Calibration Applied")

    def getWVRintegrationTime(self):
        """
        Returns the current integration time of all the water vapour
        radiometers in this array. The integration time of all WVR's in
        this array must be the same if its to be correctly collected
        and used in the online and offline software systems
        """
        return 1.0

    def enableLLCCorrection(self, enable = True):
        if enable:
            self._addExecutionLine("+ LLC Correction Enabled")
        else:
            self._addExecutionLine("+ LLC Correction Disabled")

    def isLLCCorrectionEnabled(self):
        '''
        This method checks to see if the LLCs on all antennas are enabled
        or disabled.  If the result is a mix, or other issues are encountered
        an AsynchronousFailureExcption is thrown
        '''
        return false       
        
    def getTimeToLLCReset(self):
        return 30

    def resetLLCStretchers(self):
        self._addExecutionLine("+ LLC Stretchers Reset")

    def setLLCStretcherPosition(self, position):
        self._addExecutionLine("+ LLC Stretchers Set to Position %f" %
                               position)
        
    def getLLCPolarizationCalRequired(self):
        return true

    def doLLCPolarizationCalibration(self, timeout = 300,
                                     forceCalibration=True):
        self._addExecutionLine("+ LLC Polaization Cal Performed")
        self._array.incrementElapsedTime(300)

    # --------- These methods are not normally exposed
    def executeSubscan(self, subscanDuration):
        self._array.incrementElapsedTime(subscanDuration)

    def _loadConfigurations(self, SpectralSpecList):
        '''
        This is the method which actually would send this to the hardware
        for the real case in simulation it is easier
        '''
        try:
            idList = []
            for spec in aggregate(SpectralSpecList):
                idList.append(self._correlatorConfigSlots.pop())
            return idList
        except IndexError:
            print "Error unable to load configuration all slots full"
            raise IndexError
        
    def _clearConfigurations(self, CorrConfigIdList):
        '''
        This is the method which actually would cause the correlator to
        release the configuration slots.

        Note this also releases all Calibrations associated with this
        configuration.
        '''
        # Need to recycle these config slots somehow.
        #self._correlatorCalSlots.append(self._calIdList[key][0])
        self._correlatorConfigSlots.extend(aggregate(CorrConfigIdList))


    def _loadCalibrations(self, CorrCalSeqSpec):
        '''
        This is the method which actually would send this to the hardware
        for the real case in simulation it is easier
        '''
        if not isinstance(CorrCalSeqSpec, CorrelatorCalibrationSequenceSpec):
            raise ControlIllegalParameterErrorExImpl()

        rtnCal=self._correlatorCalSlots[0:len(CorrCalSeqSpec.calibrationList)]
        self._correlatorCalSlots = \
            self._correlatorCalSlots[len(CorrCalSeqSpec.calibrationList):]
        return rtnCal
 
    def _clearCalibrations(self, calibrationsToRelease):
        '''
        This is the method which actually would cause the correlator to
        release the calibration slots.
        '''
        self._correlatorCalSlots.extend(aggregate(calibrationsToRelease))


