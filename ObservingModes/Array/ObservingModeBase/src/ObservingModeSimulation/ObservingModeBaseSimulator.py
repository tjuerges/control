#!/usr/env/python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
#

from CCL.ObservingModeBase import ObservingModeBase
from ObservingModeSimulation.ArrayMountControllerSimulator import *

from CCL.utils import aggregate

class ObservingModeBaseSimulator(ObservingModeBase):
    def __init__(self, arraySimulator):
        ObservingModeBase.__init__(self)
        self._array = arraySimulator
         
        self._currentScanIndex = -1;
        self._currentSubscanIndex = -1;
        self._scanIntents      = [];
        self._subscanIntents   = [];
        self._subscanInterval = []
        self._scanInterval    = []

        self._executionSummary = {}

        self.numSubscans = 0
        self.numScans    = 0
        self.scanStarted = False
        self.log         = []
        self._source = None
        self._spectralSpec  = None

    def getElapsedTime(self):
        '''
        This method returns the amount of time (seconds) since the begin
        execution method was called.

        For the purposes of simulation we simulate these times.
        '''
        return self._array.getElapsedTime()
    
    def __generateTimestamp(self):
        min = int(self._array.getElapsedTime() / 60)
        sec = (self._array.getElapsedTime() % 60)
        timestamp = "%02d:%02d.%03d" % (min, int(sec), int((sec%1)*1000))
        return timestamp

        
    #def isObservable(self, source, duration=0):
    #    '''
    #    All observing modes must implement this
    #    '''
    #    return True



    def beginScan(self, intent):
        self._array.beginScan(intent)
                                      
    def endScan(self):
        self._array.endScan()        

    def doSubscanSequence(self, subscanSequenceSpec):
        self._array.doSubscanSequence(subscanSequenceSpec)
            
##     def printReport(self):
##         print "Is Array Mount Controller: ", isinstance(self, ArrayMountControllerSimulator)


##         print "Total Elapsed Time: ", \
##               self.__elapsedTimeToString(self._array.getElapsedTime())
##         print "Total Scans: ", len(self._scanInterval)

##         self._generateScanLines()
##         self._generateSubscanLines()

##         # Sort the list to get it in correct order
##         keys = self._executionSummary.keys()
##         keys.sort()
##         for key in keys:
##             print self._executionSummary[key]


##     def _generateScanLines(self):
##         for scanIndex in range(len(self._scanInterval)):
##             line = "Scan %3d: "% scanIndex  
##             line += self.__elapsedTimeToString(self._scanInterval[scanIndex][0])
##             line += " to "
##             line += self.__elapsedTimeToString(self._scanInterval[scanIndex][1])
##             for intent in self._scanIntents[scanIndex]:
##                 line += "\t" + str(intent.getScanIntent()) + " "
                
##             if isinstance(self, ArrayMountControllerSimulator):
##                 line += self._getAMCSource(self._scanInterval[scanIndex][0])
                    
##             self._addExecutionLine(line, self._scanInterval[scanIndex][0])
            
##     def _generateSubscanLines(self):
##         for subscanIndex in range(len(self._subscanInterval)):
##             line = "\tSubscan "
##             line += self.__elapsedTimeToString(self._subscanInterval[subscanIndex][0])
##             line += " to "
##             line += self.__elapsedTimeToString(self._subscanInterval[subscanIndex][1])

##             for intent in self._subscanIntents[subscanIndex]:
##                 line += "\t" + str(intent) + " "

            
##             if isinstance(self, ArrayMountControllerSimulator):
##                 stroke = self._getAMCStroke(self._subscanInterval[subscanIndex][0])
##                 if  stroke != (0,0,0,0):
##                     print stroke
                
##             self._addExecutionLine(line, self._subscanInterval[subscanIndex][0])
                    

##     def _addExecutionLine(self, message, timestamp = None):
##         if timestamp is None:
##             timestamp = self._array.getElapsedTime()
            
##         while self._executionSummary.has_key(timestamp):
##             timestamp += 0.000001
            
##         self._executionSummary[timestamp] = message


##     def __elapsedTimeToString(self, time):
##         # Elapsed times are kept in seconds convert to a mm:ss
##         return "%d:%02d" % (time/60, time%60)
