#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when      what
# --------  --------  ----------------------------------------------
# jkern  2009-11-16  created
#

#************************************************************************
#  This is a base class for all observing mode CCL bindings
# 
# 
#------------------------------------------------------------------------
#


import CCL.SIConverter
import time


class SBOutOfTime(Exception):
    def __init__(self):
        Exception.__init__(self)
        
class ObservingModeBase:
    def __init__(self):
        self.startTime = time.time()
        self._maximumTime = None
        self._cleanupListTime = 0.0
        self._cleanupList = []
        self._setSignalLevelsRequired = False

    def getElapsedTime(self):
        '''
        This method returns the amount of time (seconds) since the
        observing mode was created.
        '''
        return time.time() - self.startTime

    def setMaximumSchedBlockTime(self, time):
        '''
        This method sets the maximum allowable time for a scheduling
        block execution.

        A value of either 0 or None is interpreted to mean that the
        scheduling block should run until the script completes.
        Use this carefully as it may lead to an infinite execution.
        '''
        if time is None:
            self._maximumTime = None
            return
        
        self._maximumTime = CCL.SIConverter.toSeconds(time)
        if self._maximumTime == 0:
            self._maximumTime = None

    def setSignalLevelsNextTarget(self):
        '''
        This method will cause the levels to be set before the next
        target is observed.
        '''
        self._setSignalLevelsRequired = True

    def getCleanupStartTime(self):
        '''
        This method returns the elapsed time at which we must start the
        cleanup routines.  This is the Maximum Time permitted minus
        the time required for the cleanup list.

        If no maximum time is specified, "None" is returned
        '''
        if self._maximumTime is None:
            return None

        return self._maximumTime - self._cleanupListTime

    def getTimeBeforeCleanup(self):
        '''
        This method returns the amount of time before we must start
        the cleanup routines.

        If no maximum time is specified, "None" is returned
        '''
        if self._maximumTime is None:
            return None

        return self.getCleanupStartTime() - self.getElapsedTime()
        
    def checkElapsedTime(self, scanDuration = 0):
        '''
        This method checks to see if we have time to run a scan of specified
        druratoin before we need to run the cleanup methods.  An exception
        is thrown if we do not have sufficient time.
        '''
        if self._maximumTime is not None:
            if self._cleanupListTime + self.getElapsedTime() + scanDuration>= \
               self._maximumTime:
                raise SBOutOfTime
            
    def addTargetToCleanupList(self, target):
        '''
        This method adds to the list of targets which time must be reserved
        for.  It ensures that the script does not end before these final
        targets are observed.
        '''
        self._cleanupList.append(target)
        self._cleanupListTime += target.getScanExecutionTime()

    def clearCleanupList(self):
        '''
        This method removed all targets from the cleanup list.
        '''
        self._cleanupList = []
        self._cleanupListTime = 0
        
    def executeCleanupList(self):
        '''
        This method causes all of the targets in the cleanup list to be
        executed and clears the list.
        '''
        # Set the _cleanupListTime to zero to make the time available
        # for the calibrators
        self._cleanupListTime = 0
        for target in self._cleanupList:
            target.execute(self)
            
        self.clearCleanupList()

    def executeFromTargetList(self, targetList, minimumNumber = 1):
        '''
        This method will check that at least minimumNumber of the
        targets in the targetList are valid (e.g. do not need to be
        executed).

        The method will return after minimumNumber targets have found
        to be currently valid (or observed) or all targets in the list
        have been checked.
        '''
        numberExecuted = 0
        for target in targetList:
            if target is not None and target.isObservable(self):
                if self._setSignalLevelsRequired:
                    target.setSignalLevels(self)
                    self._setSignalLevelsRequired = False
                target.executeIfNeeded(self)
                numberExecuted += 1
            if numberExecuted >= minimumNumber:
                return

    def executeGroupCalibrators(self, group,
                                minAmplitudeCalTarget = 1,
                                minAtmCalTarget = 1,
                                minBandpassCalTarget = 1,
                                minDelayCalTarget = 1,
                                minFocusCalTarget = 1,
                                minPhaseCalTarget = 1,
                                minPointingCalTarget = 1):
        '''
        This method will ensure that at at least minimumNumber of
        targets of each calibrator specified in the group are executed.

        Execution order is:
           * PointingCalTarget
           * FocusCalTarget
           * PhaseCalTarget
           * DelayCalTarget.py       
           * AmplitudeCalTarget
           * AtmCalTarget
           * BandpassCalTarget
        '''
        self.executeFromTargetList(group.getPointingCalTargetList(),
                                   minPointingCalTarget)
        self.executeFromTargetList(group.getFocusCalTargetList(),
                                   minFocusCalTarget)
        self.executeFromTargetList(group.getPhaseCalTargetList(),
                                   minPhaseCalTarget)
        self.executeFromTargetList(group.getDelayCalTargetList(),
                                   minDelayCalTarget)
        self.executeFromTargetList(group.getAmplitudeCalTargetList(),
                                   minAmplitudeCalTarget)
        self.executeFromTargetList(group.getAtmCalTargetList(),
                                   minAtmCalTarget)
        self.executeFromTargetList(group.getBandpassCalTargetList(),
                                   minBandpassCalTarget)


#
# ___oOo___
