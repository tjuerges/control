/*
 * "@(#) $Id$"
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2006, 2008, 2010
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */

package alma.Control.ObservingModes;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.ValidationException;

import alma.ACSErr.ErrorTrace;
import alma.Control.ArrayOffset;
import alma.Control.EphemerisRow;
import alma.Control.SourceOffset;
import alma.Control.SubscanSequenceSpec;
import alma.Control.SubscanSpec;
import alma.Control.Common.Util;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJDataCapturerErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.acs.entityutil.EntityException;
import alma.acs.entityutil.EntitySerializer;
import alma.acs.logging.AcsLogger;
import alma.entity.xmlbinding.schedblock.BaseBandSpecificationT;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.SpectralSpecTChoice;
import alma.entity.xmlbinding.schedblock.types.BaseBandSpecificationTBaseBandNameType;
import alma.entity.xmlbinding.valuetypes.AngularVelocityT;
import alma.entity.xmlbinding.valuetypes.DoubleWithUnitT;
import alma.entity.xmlbinding.valuetypes.FrequencyT;
import alma.entity.xmlbinding.valuetypes.TimeT;
import alma.xmlentity.XmlEntityStruct;

public class ObservingModeUtilities {

    /* Static method to convert xml Ephemeris to an Ephemeris row */
    public static EphemerisRow[] ephemerisToIDL(String xmlEphemeris,
                                                AcsLogger logger)
        throws IllegalParameterErrorEx {
        String msg;
        // TODO Parse the Ephemeris string. Each row is of the form
        // "time (ACS::Time),RA (rad), Dec (rad), distance (m);"
        // Fields are delimited by commas and rows by semicolons.
        // An example of one row is:
        // "134790786000000000,5.651023,-0.268622,786140503830.000000;"
        String[] rows = xmlEphemeris.split(";");
        if (rows.length < 2) {
            msg = "Supplied ephemeris must contain at least two rows.";
            msg += " It has " + rows.length + " rows.";
           msg += " Rows are delimited by semicolons.";
           msg += " Incorrect ephemeris is " + xmlEphemeris;
           throwIllegalParameterErrorEx(msg,
                   new AcsJIllegalParameterErrorEx(), logger);
       }
       EphemerisRow[] idlEphemeris = new EphemerisRow[rows.length];
       for (int r = 0; r < rows.length; r++) {
           String fields[] = rows[r].split(",");
           if (fields.length != 4) {
               msg = "Each row of the supplied ephemeris must contain 4 fields.";
               msg += " Row " + r + " has " + fields.length + " fields.";
               msg += " Fields are delimited by commas.";
               msg += " Incorrect row is " + rows[r];
               throwIllegalParameterErrorEx(msg,
                       new AcsJIllegalParameterErrorEx(), logger);
           }
           long ts = Long.valueOf(fields[0]).longValue();
           if (ts < 131659776000000000L || ts > 2194774112000000000L) {
               msg = "The timestamps must be between the years";
               msg += " 2000 (131659776000000000) and 2200 (2194774112000000000)";
               msg += " Row " + r + " has a timestamp of ";
               msg += Util.acsTimeToString(ts) + " (" + ts + ")";
               throwIllegalParameterErrorEx(msg, rows[r], logger);
           }
           double ra = Double.valueOf(fields[1]).doubleValue();
           if (Math.abs(ra) > 2 * Math.PI) {
               msg = "The RA must be a number between -2*PI and 2*PI radians.";
               msg += " Row " + r + " has an RA of " + ra + " radians";
               msg += " Check the format and units are correct.";
               msg += " Incorrect row is " + rows[r];
               throwIllegalParameterErrorEx(msg,
                       new AcsJIllegalParameterErrorEx(), logger);
           }
           double dec = Double.valueOf(fields[2]).doubleValue();
           if (Math.abs(dec) > Math.PI / 2.0) {
               msg = "The declination must be a number between -PI/2 and PI/2 radians.";
               msg += " Row " + r + " has a declination of " + dec
                       + " radians";
               throwIllegalParameterErrorEx(msg, rows[r], logger);
           }
           double rho = Double.valueOf(fields[3]).doubleValue();
           if (rho < 6000E3) {
               msg = "The distance must be greater than 6000km";
               msg += " (as we cannot observe subterranian objects).";
               msg += " Row " + r + " has a distance of " + rho + " meters";
               throwIllegalParameterErrorEx(msg, rows[r], logger);
           }
           idlEphemeris[r] = new EphemerisRow(ts, ra, dec, rho);
       }
       return idlEphemeris;
   }
    
   @Deprecated
    public static FieldSourceT deserializeFieldSource(
            XmlEntityStruct fieldSource, Logger logger)
            throws IllegalParameterErrorEx {
        return deserializeFieldSource(fieldSource.xmlString, logger);
    }

    public static FieldSourceT deserializeFieldSource(String xmlString,
            Logger logger) throws IllegalParameterErrorEx {
        try {
            return FieldSourceT.unmarshalFieldSourceT(new StringReader(xmlString));
        } catch (MarshalException ex) {
            String msg = "The specified source parameters are not in the expected format";
            msg += " (of type FieldSourceT). Perhaps you are using an old project or script?";
            msg += " XML is: " + xmlString;
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(ex), logger);
        } catch (ValidationException ex) {
            String msg = "The specified source parameters are not in the expected format";
            msg += " (of type FieldSourceT). Perhaps you are using an old project or script?";
            msg += " XML is: " + xmlString;
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(ex), logger);
        }
        return null; // We should never get here
    }

    public static SpectralSpecT deserializeSpectralSpec(String xmlString,
            Logger logger) throws IllegalParameterErrorEx {
        try {
            return SpectralSpecT.unmarshalSpectralSpecT(new StringReader(xmlString));
        } catch (MarshalException ex) {
            String msg = "The specified spectral specification not in the expected format";
            msg += " (of type SpectralSpecT). Perhaps you are using an old project or script?";
            msg += " Spectral specification is: " + xmlString;
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(ex), logger);
        } catch (ValidationException ex) {
            String msg = "The specified spectral specification not in the expected format";
            msg += " (of type SpectralSpecT). Perhaps you are using an old project or script?";
            msg += " Spectral specification is: " + xmlString;
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(ex), logger);
        }
        return null; // We should never get here
    }
    
     public static XmlEntityStruct serializeFieldSource(
            FieldSourceT fieldSource, Logger logger)
            throws ObservingModeErrorEx {
        
        StringWriter writer = new StringWriter();
        try {
            fieldSource.marshal(writer);
        } catch (MarshalException me) {
            String msg = "Could not serialize the FieldSource.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(me), logger);
        } catch (ValidationException ve) {
            String msg = "Could not serialize the FieldSource.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ve), logger);
        }

        XmlEntityStruct xes = new XmlEntityStruct();
        xes.xmlString = writer.toString();
        return xes;
    }
    
    public static XmlEntityStruct serializeSpectralSpec(
            SpectralSpecT spectralSpec, Logger logger)
            throws ObservingModeErrorEx {
        
        StringWriter writer = new StringWriter();
        try {
            spectralSpec.marshal(writer);
        } catch (MarshalException me) {
            String msg = "Could not serialize the SpectralSpec. ";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(me), logger);
        } catch (ValidationException ve) {
            String msg = "Could not serialize the SpectralSpec. ";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ve), logger);
        }

        XmlEntityStruct xes = new XmlEntityStruct();
        xes.xmlString = writer.toString();
        return xes;
    }

    private static XmlEntityStruct serializeObject(Object object,
            Logger logger, String type) throws ObservingModeErrorEx {
        EntitySerializer serializer = EntitySerializer.getEntitySerializer(logger);
        XmlEntityStruct entity = new XmlEntityStruct();
        try {
            entity = serializer.serializeEntity(object);
            // For reasons I do not understand the entityId field is sometimes
            // null. This makes it impossible to pass the XmlEntityStruct
            // to other objects using CORBA. So its reset when necessary.
            if (entity.entityId == null) {
                entity.entityId = new String("");
            }
        } catch (EntityException ex) {
            String msg = "Could not serialize the " + type
                    + ". This is an internal error and should be reported.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex),
                    logger);
        }
        return entity;
    }
    
     /* ---------- Exception Handler Methods ---------------------- */
    // This private exception is used by the ephemerisToIDL method 
    private static void throwIllegalParameterErrorEx(String msg, 
                                                     String row,
                                                     AcsLogger logger) 
        throws IllegalParameterErrorEx {
        msg += " Check the format and units are correct.";
        msg += " Incorrect row is " + row;
        throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx(),
                                     logger);
    }

    static void throwObsModeInitErrorEx(String msg, AcsJObsModeInitErrorEx jex,
            Logger logger) throws ObsModeInitErrorEx {
        jex.setProperty("Message", msg);
        if (logger != null) {
            jex.log(logger);
        }
        throw jex.toObsModeInitErrorEx();
    }

    static void throwIllegalParameterErrorEx(String msg,
            AcsJIllegalParameterErrorEx jex, Logger logger)
            throws IllegalParameterErrorEx {
        jex.setProperty("Message", msg);
        if (logger != null) {
            jex.log(logger);
        }
        throw jex.toIllegalParameterErrorEx();
    }

    static void throwObservingModeErrorEx(String msg,
            AcsJObservingModeErrorEx jex, Logger logger)
            throws ObservingModeErrorEx {
        jex.setProperty("Message", msg);
        if (logger != null) {
            jex.log(logger);
        }
        throw jex.toObservingModeErrorEx();
    }

    static void throwDataCapturerErrorEx(String msg,
            AcsJDataCapturerErrorEx jex, Logger logger)
            throws DataCapturerErrorEx {
        jex.setProperty("Message", msg);
        if (logger != null) {
            jex.log(logger);
        }
        throw jex.toDataCapturerErrorEx();
    }

    static void logSevereError(String msg, ErrorTrace et, String audience,
                               AcsLogger logger) {
        logger.logToAudience(Level.SEVERE, msg + " Underlying error is: "
                + Util.getNiceErrorTraceString(et), audience);
    }

    /* ------------------- Conversion Routines --------------------*/
    protected static long getCurrentACSTime() {
        return Util.arrayTimeToACSTime(Util.getArrayTime().get());
    }

    /**
     * A utility function to convert TimeT to seconds.
     * 
     * @param time
     *            Time with units
     * @return Time in seconds
     */
    public static double convertTimeToSeconds(TimeT time) {
        String unit = time.getUnit();
        double value = time.getContent();
        double convFactor = 1.0;
        if (unit.equals("ns"))
            convFactor = 1.0e-9;
        else if (unit.equals("us"))
            convFactor = 1.0e-6;
        else if (unit.equals("ms"))
            convFactor = 1.0e-3;
        else if (unit.equals("min"))
            convFactor = 60.0;
        else if (unit.equals("h"))
            convFactor = 3600.0;
        else if (unit.equals("yr"))
            convFactor = 31557600.0; // 24*60*60*365.25
        return value * convFactor;
    }

    /**
     * A utility function to convert an angle to radians.
     * 
     * @param angle
     *            Angle with units
     * @return Angle in seconds
     */
    public static double convertAngleToRadians(DoubleWithUnitT angle) {
        String unit = angle.getUnit();
        double value = angle.getContent();
        double convFactor = 1.0;
        if (unit.equals("deg"))
            convFactor = Math.PI / 180.0; // 2*PI/(360)
        else if (unit.equals("arcmin"))
            convFactor = Math.PI / 10800.0; // 2*PI/(360*60)
        else if (unit.equals("arcsec"))
            convFactor = Math.PI / 648000.0; // 2*PI/(360*60*60)
        else if (unit.equals("mas"))
            convFactor = Math.PI / 648000000.0; // 2*PI/(360*60*60*1000)
        return value * convFactor;
    }

    /**
     * A utility function to convert an angular velocity to radians per second.
     * 
     * @param angvel
     *            Angular velocity with units
     * @return Angular velocity in radians per second
     */
    public static double convertAngularVelocityToRadiansPerSecond(
            AngularVelocityT angvel) {
        String unit = angvel.getUnit();
        double value = angvel.getContent();
        double convFactor = 1.0;
        if (unit.equals("arcsec/s"))
            convFactor = Math.PI / 648000.0; // 2*PI/(360*60*60);
        else if (unit.equals("arcmin/s"))
            convFactor = Math.PI / 10800.0; // 2*PI/(360*60)
        else if (unit.equals("deg/s"))
            convFactor = Math.PI / 180.0; // 2*PI/(360)
        else if (unit.equals("mas/yr"))
            convFactor = Math.PI / 20449324800000000.0; // 2*PI/(360*60*60*1000*365.25*24*60*60)
        return value * convFactor;
    }

    /**
     * A utility function to convert a frequency to Hertz
     * 
     * @param freq
     *            Frequency with units
     * @return Frequency in Hz
     */
    public static double convertFrequencyToHertz(FrequencyT freq) {
        String unit = freq.getUnit();
        double value = freq.getContent();
        double convFactor = 1.0;
        if (unit.equals("kHz"))
            convFactor = 1E3;
        else if (unit.equals("MHz"))
            convFactor = 1E6;
        else if (unit.equals("GHz"))
            convFactor = 1E9;
        else if (unit.equals("THz"))
            convFactor = 1E12;
        return value * convFactor;
    }    

    static public int[] baseBandIDs(SpectralSpecT spectralSpec) {
        BaseBandSpecificationT bbs[] = spectralSpec.getFrequencySetup().getBaseBandSpecification();
        int ids[] = new int[bbs.length];
        ids[0] = 1;
        for (int b = 0; b < bbs.length; b++) {
            BaseBandSpecificationTBaseBandNameType bbName = bbs[b].getBaseBandName();
            if (bbName == BaseBandSpecificationTBaseBandNameType.BB_1) {
                ids[b] = 1;
            } else if (bbName == BaseBandSpecificationTBaseBandNameType.BB_2) {
                ids[b] = 2;
            } else if (bbName == BaseBandSpecificationTBaseBandNameType.BB_3) {
                ids[b] = 3;
            } else if (bbName == BaseBandSpecificationTBaseBandNameType.BB_4) {
                ids[b] = 4;
            }
        }
        return ids;
    }

    static public void checkSubscanSequence(SubscanSequenceSpec subscans,
            FieldSourceT latestSource,
            SpectralSpecT latestSpectrum, boolean usingCorrelator,
            Logger logger) throws IllegalParameterErrorEx {
        Set<String> sa = new HashSet<String>();
        sa.add("Default");
        ObservingModeUtilities.checkSubscanSequence(subscans, latestSource,
                latestSpectrum, sa, usingCorrelator, logger);
    }
    
    static public boolean checkForUniformCorrelatorConfigUse(
            SpectralSpecT[] spectra, Logger logger)
            throws IllegalParameterErrorEx {
        // Check that each spectra
        if (spectra.length == 0) {
            return false;
        }
        // Found out if the first spectral spec uses the square-law detectors
        final boolean usingCorr = hasCorrelatorConfig(spectra[0], logger);
        for (int s = 1; s < spectra.length; s++) {
            final boolean hasConfig = hasCorrelatorConfig(spectra[s], logger);
            if ((usingCorr && !hasConfig) || (!usingCorr && hasConfig)) {
                String msg = "All spectral specs, or none of them,";
                msg += " in the subscan sequence must have a correlator configuration.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
        }
        return usingCorr;
    }
    
    static public boolean hasCorrelatorConfig(
            SpectralSpecT spectralSpec, Logger logger) {
        // Found out if the first spectral spec uses the square-law detectors
        boolean usingCorr = false;
        SpectralSpecTChoice sstc = spectralSpec.getSpectralSpecTChoice();
        if (sstc != null) {
            if (sstc.getBLCorrelatorConfiguration() != null
                    || sstc.getACACorrelatorConfiguration() != null) {
                usingCorr = true;
            }
        }
        return usingCorr;
    }

    static public boolean checkForUniformSquareLawUse(SpectralSpecT[] spectra,
            Logger logger) throws IllegalParameterErrorEx {
        if (spectra.length == 0) {
            return false;
        }
        // Found out if the first spectral spec uses the square-law detectors
        final boolean usingSQD = 
            (spectra[0].getSquareLawSetup() != null) ? true : false;
        // Now check the remaining spectral specs have the same setup.
        for (int s = 1; s < spectra.length; s++) {
            final boolean hasSQD = (spectra[s].getSquareLawSetup() != null);
            if ((usingSQD && !hasSQD) || (!usingSQD && hasSQD)) {
                String msg = "All spectral specs, or none of them,";
                msg += " in the subscan sequence must have a square law setup.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
        }
        return usingSQD;
    }
    
    static public int[] extractCalIds(SubscanSpec[] subscans, Logger logger)
            throws IllegalParameterErrorEx {
        final int numSubscans = subscans.length;
        int[] calIds = new int[numSubscans];
        for (int s = 0; s < numSubscans; s++) {
            final int id = subscans[s].calibrationId;
            if (id < 0) {
                String msg = "Invalid correlator calibration identifier of "
                        + id;
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            calIds[s] = id;
        }
        return calIds;
    }
    
    static public void extractTargetSequence(SubscanSequenceSpec subscans,
            FieldSourceT latestSource, SpectralSpecT latestSpectrum,
            Logger logger, FieldSourceT[] sourceList,
            boolean[] sourceChanged, SpectralSpecT[] spectrumList,
            boolean[] spectrumChanged) throws IllegalParameterErrorEx {
        Set<String> sa = new HashSet<String>();
        sa.add("Default");
        ObservingModeUtilities.extractTargetSequence(subscans, latestSource,
                latestSpectrum, sa, logger, sourceList,
                sourceChanged, spectrumList, spectrumChanged);
    }

    static public void extractTargetSequence(SubscanSequenceSpec subscans,
            FieldSourceT latestSource, SpectralSpecT latestSpectrum,
            Set<String> currentSubarrays,
            Logger logger, FieldSourceT[] sourceList, boolean[] sourceChanged,
            SpectralSpecT[] spectrumList, boolean[] spectrumChanged)
            throws IllegalParameterErrorEx {
        String msg;

        // Check source list is not empty
        final int numSources = subscans.sources.length;
        if (numSources == 0 && latestSource == null) {
            msg = "No sources have been specified";
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }

        // Check spectral spec list is not empty
        final int numSpectra = subscans.spectra.length;
        if (numSpectra == 0 && latestSpectrum == null) {
            msg = "No spectra have been specified.";
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }

        // Deserialize each field source
        FieldSourceT[] sources = new FieldSourceT[numSources];
        for (int s = 0; s < numSources; s++) {
            sources[s] = ObservingModeUtilities.deserializeFieldSource(
                    subscans.sources[s], logger);
        }

        SpectralSpecT[] spectra = new SpectralSpecT[numSpectra];
        for (int s = 0; s < numSpectra; s++) {
            spectra[s] = ObservingModeUtilities.deserializeSpectralSpec(
                    subscans.spectra[s], logger);
            // TODO Check that we can, in theory, tune to each spectrum. It is
            // difficult to check all fields as a Doppler shift might move you
            // into or out of a band.
        }

        checkForUniformSquareLawUse(spectra, logger);
        checkForUniformCorrelatorConfigUse(spectra, logger);
        
        // Check the sub-scan list does not have zero length
        final int numSubscans = subscans.subscans.length;
        if (numSubscans == 0) {
            msg = "No subscans have been specified";
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }
        if (sourceList.length != numSubscans
                || sourceChanged.length != numSubscans
                || spectrumList.length != numSubscans
                || spectrumChanged.length != numSubscans) {
            msg = "Arrays are not the correct length";
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }

        // Now check the fields in each sub-scan
        for (int s = 0; s < numSubscans; s++) {
            SubscanSpec subscan = subscans.subscans[s];
            // Check each source id
            if (subscan.sourceId >= numSources) {
                msg = "Source index is too big";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            if (subscan.sourceId < 0 && latestSource == null) {
                msg = "No source has been specified";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            boolean antennaMoves = false;
            if (subscan.sourceId >= 0) {
                latestSource = sources[subscan.sourceId];
                antennaMoves = true;
            }
            sourceList[s] = latestSource;
            // Check the offsets for each pointing subarray
            for (ArrayOffset a : subscan.pointingOffset) {
                // check all the specified sub-arrays exist
                // TODO. make this check case-insensitive
                if (!currentSubarrays.contains(a.name)) {
                    msg = "The pointing subarray called " + a.name
                            + " does not exist.";
                    msg += " Available subarrays are ";
                    for (String sa : currentSubarrays) {
                        msg += sa + " ";
                    }
                    throwIllegalParameterErrorEx(msg,
                            new AcsJIllegalParameterErrorEx(), logger);
                }
                // Do not check, for now, if the offset for this subscan is the
                // same as for the previous subscan. Just assume they are
                // different. TODO. Fix this up.
                antennaMoves = true;
                // Check the offsets are OK
                SourceOffset offset = a.offset;
                if (Math.abs(offset.latitudeOffset) > Math.PI / 2) {
                    msg = "Latitude offset is too big";
                    throwIllegalParameterErrorEx(msg,
                            new AcsJIllegalParameterErrorEx(), logger);
                }
                if (Math.abs(offset.longitudeOffset) > Math.PI * 3 / 2) {
                    msg = "Longitude offset is too big";
                    throwIllegalParameterErrorEx(msg,
                            new AcsJIllegalParameterErrorEx(), logger);
                }
                // TODO Check the velocities.
                // TODO Check each source + offset isObservable
            }
            sourceChanged[s] = antennaMoves;

            // Check the each spectral id
            if (subscan.spectralId >= numSpectra) {
                msg = "Spectral index is too big";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            if (subscan.spectralId < 0 && latestSpectrum == null) {
                msg = "No specrum has been specified";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            boolean spectrumChange = false;
            if (subscan.spectralId >= 0) {
                latestSpectrum = spectra[subscan.spectralId];
                spectrumChange = true;
            }
            spectrumList[s] = latestSpectrum;
            spectrumChanged[s] = spectrumChange;

            // check each duration
            if (subscan.duration <= 0) {
                msg = "Subscan duration is too small";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            // Check the intents
            if (subscan.intent.length == 0) {
                msg = "The subscan does not have an intent";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
        }
    }
   
    static public void checkSubscanSequence(SubscanSequenceSpec subscans,
            FieldSourceT latestSource,
            SpectralSpecT latestSpectrum,
            Set<String> currentSubarrays, boolean usingCorrelator, Logger logger)
            throws IllegalParameterErrorEx {
        String msg;
        // Check source list is OK
        final int numSources = subscans.sources.length;
        if (numSources == 0 && latestSource == null) {
            msg = "No sources have been specified";
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }
        for (int s = 0; s < numSources; s++) {
            ObservingModeUtilities.deserializeFieldSource(
                    subscans.sources[s], logger);
        }
        // Check Spectra list is OK
        final int numSpectra = subscans.spectra.length;
        if (numSpectra == 0 && latestSpectrum == null) {
            msg = "No spectra have been specified.";
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }
        for (int s = 0; s < numSpectra; s++) {
            ObservingModeUtilities.deserializeSpectralSpec(
                    subscans.spectra[s], logger);
            // TODO Check that we can, in theory, tune to each spectrum
        }

        // Check the sub-scan list
        // This list cannot have zero length
        if (subscans.subscans.length == 0) {
            msg = "No subscans have been specified";
            throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }
        // Now check the fields in each sub-scan
        boolean sourceSpecified = (latestSource != null);
        boolean spectraSpecified = (latestSpectrum != null);
        for (SubscanSpec s : subscans.subscans) {
            // Check each source id
            if (s.sourceId >= numSources) {
                msg = "Source index is too big";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            if (s.sourceId < 0 && !sourceSpecified) {
                msg = "No source has been specified";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            sourceSpecified = true;
            // Check the each spectral id
            if (s.spectralId >= numSpectra) {
                msg = "Spectral index is too big";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            if (s.spectralId < 0 && !spectraSpecified) {
                msg = "No specrum has been specified";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            spectraSpecified = true;
            // Check the offsets for each pointing subarray
            for (ArrayOffset a : s.pointingOffset) {
                // check all the specified sub-array exists
                // TODO. make this check case-insensitive
                if (!currentSubarrays.contains(a.name)) {
                    msg = "The pointing subarray called " + a.name
                            + " does not exist.";
                    msg += " Available subarrays are ";
                    for (String sa : currentSubarrays) {
                        msg += sa + " ";
                    }
                    throwIllegalParameterErrorEx(msg,
                            new AcsJIllegalParameterErrorEx(), logger);
                }
                // Check the offsets are OK
                SourceOffset offset = a.offset;
                if (Math.abs(offset.latitudeOffset) > Math.PI / 2) {
                    msg = "Latitude offset is too big";
                    throwIllegalParameterErrorEx(msg,
                            new AcsJIllegalParameterErrorEx(), logger);
                }
                if (Math.abs(offset.longitudeOffset) > Math.PI * 3 / 2) {
                    msg = "Longitude offset is too big";
                    throwIllegalParameterErrorEx(msg,
                            new AcsJIllegalParameterErrorEx(), logger);
                }
                // TODO Check the velocities.
                // TODO Check each source + offset isObservable
            }
            // check each calibration id
            if (usingCorrelator && (s.calibrationId < 0)) {
                msg = "Illegal correlator calibration index";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            // check each duration
            if (s.duration <= 0) {
                msg = "Subscan duration is too small";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
            // Check the intents
            if (s.intent.length == 0) {
                msg = "The subscan does not have an intent";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(), logger);
            }
        }
    }
 }