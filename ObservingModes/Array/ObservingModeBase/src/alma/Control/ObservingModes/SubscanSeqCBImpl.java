// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2009, 2010
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import alma.ACSErr.Completion;
import alma.Correlator.SubScanCorrelatorData;
import alma.Correlator.SubscanInformation;
import alma.Correlator.SubscanTiming;
import alma.Correlator.SubscanSeqCB;
import alma.Correlator.SubscanSeqCBHelper;
import alma.Correlator.SubscanSeqCBPOA;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJCompletion;
import alma.Control.Common.DeveloperLogger;
import alma.Control.Common.Util;

public class SubscanSeqCBImpl extends SubscanSeqCBPOA {
    private final double extraWaitTime = 2.0; // The extra time, in seconds, to
                                              // wait before timing out waiting
                                              // for a subscan to start or end.
    private ContainerServices cs = null;
    private DeveloperLogger logger = null;
    private SubscanSeqCB corbaObject = null;
    private long[] expectedStartTimes = null;
    private long[] expectedEndTimes = null;
    private int[] expectedScans = null;
    private int[] expectedSubscans = null;
    private int numSubscans = 0;
    private int startedSubscans = 0;
    private int endedSubscans = 0;
    private Semaphore subscanStartedSem = null;
    private Semaphore subscanEndedSem = null;
    private ArrayList<Integer> scans = new ArrayList<Integer>();
    private ArrayList<Integer> subscans = new ArrayList<Integer>();
    private ArrayList<SubScanCorrelatorData> data = new ArrayList<SubScanCorrelatorData>();
    private ArrayList<Completion> completions = new ArrayList<Completion>();
    private int currentSubscan = 0;

    public SubscanSeqCBImpl(ContainerServices cs, long startTime,
            SubscanInformation[] subscanInfo) throws ObservingModeErrorEx {
        this.cs = cs;
        logger = new DeveloperLogger(cs.getLogger());
        try {
            corbaObject = SubscanSeqCBHelper.narrow(cs.activateOffShoot(this));
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Cannot create a SubscanSeqCB CORBA object.";
            ObservingModeUtilities.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(ex), logger.getLogger());
        }
        
        // Compute the expected start and end times for each subscan
        numSubscans = subscanInfo.length;
        expectedStartTimes = new long[numSubscans];
        expectedEndTimes = new long[numSubscans];
        expectedScans = new int[numSubscans];
        expectedSubscans = new int[numSubscans];
        
        for (int s = 0; s < numSubscans; s++) {
            SubscanInformation ssi = subscanInfo[s];
            expectedScans[s] = ssi.scanNumber;
            expectedSubscans[s] = ssi.subScanNumber;
            SubscanTiming sst = ssi.subscanTime;
            expectedStartTimes[s] = startTime + sst.setupTime;
            expectedEndTimes[s] = expectedStartTimes[s] + sst.duration
                    + sst.processingTime;
            startTime += sst.setupTime + sst.duration + sst.processingTime;
            String msg = "Scan " + expectedScans[s] 
                    + " subscan " + expectedSubscans[s] 
                    + " is expected to start at "
                    + Util.acsTimeToString(expectedStartTimes[s])
                    + " and end at "
                    + Util.acsTimeToString(expectedEndTimes[s] - sst.duration);
            logger.fine(msg);
        }
        
        subscanStartedSem = new Semaphore(numSubscans);
        subscanStartedSem.drainPermits();
        subscanEndedSem = new Semaphore(numSubscans);
        subscanEndedSem.drainPermits();
    }

    // -------------- CORBA Functions ------------------
    // This is a one-way void call so we cannot throw exceptions
    public void subscanStarted(int scanNumber, int subscanNumber) {
        subscanStartedSem.release();
        startedSubscans++;
        if (startedSubscans > numSubscans) {
            String msg = "The subscanStarted function was called too many times.";
            msg += " The subscan sequence has " + numSubscans;
            msg += " subscans but the subscanStarted function has been called ";
            msg += startedSubscans + " times.";
            msg += " Ignoring this function call.";
            logger.severe(msg);
            return;
        }
        final int expectedScan = expectedScans[startedSubscans-1];
        final int expectedSubscan = expectedSubscans[startedSubscans-1];
        if (scanNumber != expectedScan
                || subscanNumber != expectedSubscan) {
            String msg;
            msg = "Unexpected scan and/or subscan numbers.";
            msg += " Expected scan " + expectedScan;
            msg += " & subscan " + expectedSubscan;
            msg += " but got scan " + scanNumber;
            msg += " & subscan " + subscanNumber;
            msg += ". Ignoring this inconsistancy.";
            logger.warning(msg);
        }
    }

    // This is a one-way void call so we cannot throw exceptions
    public void subscanEnded(int completedScan, int completedSubscan,
            SubScanCorrelatorData subscanData, Completion status) { 
        endedSubscans++;
        if (endedSubscans > numSubscans) {
            String msg = "The subscanEnded function was called too many times.";
            msg += " The subscan sequence has " + numSubscans;
            msg += " subscans but the subscanEnded function has been called ";
            msg += endedSubscans + " times.";
            msg += " Ignoring this function call.";
            logger.severe(msg);
            subscanEndedSem.release();
            return;
        }
        scans.add(completedScan);
        subscans.add(completedSubscan);
        data.add(subscanData);
        completions.add(status);
        subscanEndedSem.release();

        final int expectedScan = expectedScans[endedSubscans-1];
        final int expectedSubscan = expectedSubscans[endedSubscans-1];
        if (completedScan != expectedScan
                || completedSubscan != expectedSubscan) {
            String msg;
            msg = "Unexpected scan and/or subscan numbers.";
            msg += " Expected scan " + expectedScan;
            msg += " & subscan " + expectedSubscan;
            msg += " but got scan " + completedScan;
            msg += " & subscan " + completedSubscan;
            msg += ". Assuming the new values are correct.";
            logger.warning(msg);
        }
        
        // We expect more calls to this callback if the completion is error free
        // and we have not got all the expected completions
        boolean isOK = !AcsJCompletion.fromCorbaCompletion(status).isError();
        if (isOK && endedSubscans < numSubscans) {
            return;
        }

        // Turn off the CORBA object. Once this is done this object can only
        // be accessed as a java object by the observing mode that created the
        // callback.
        try {
            cs.deactivateOffShoot(this);
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Error deactivating offshoot. Carrying on regardless.";
            logger.warning(msg);
        }
        corbaObject = null;
    }

    // --------------- Java only functions -----------------
    public SubscanSeqCB getCORBAObject() {
        return corbaObject;
    }
    
    public void waitForSubscanStart() throws ObservingModeErrorEx {
        boolean ok = true;
        String msg = "";
        long timeOut = 0;
        try {
            // TODO. Check that currentSubscan < expectedStartTimes.length;
            final long expectedStartTime = expectedStartTimes[currentSubscan];
            final long now = ObservingModeUtilities.getCurrentACSTime();
            if (now < expectedStartTime) {
                timeOut = expectedStartTime - now;
            }
            timeOut += Util.toTimeInterval(extraWaitTime);
            msg = "Waiting up to " + String.format("%.3f", timeOut * 100E-9)
                    + " seconds for sub-scan "
                    + expectedSubscans[currentSubscan] + " (#"
                    + (currentSubscan + 1) + " in a sequence of " + numSubscans
                    + ")" + " to start. Its expected to start at "
                    + Util.acsTimeToString(expectedStartTime) + " and its now "
                    + Util.acsTimeToString(now);
            logger.fine(msg);
            ok = subscanStartedSem.tryAcquire(1, timeOut / 10,
                    TimeUnit.MICROSECONDS);
            if (!ok) {
                msg = "Timed out after";
            } else {
                msg = "The correlator indicated that subscan ";
                msg += expectedSubscans[currentSubscan] + " started ";
                long delay = ObservingModeUtilities.getCurrentACSTime() - expectedStartTime;
                msg += String.format("%.3f", Math.abs(delay*100E-9)) + " seconds ";
                msg += (delay < 0) ? "before" : "after";
                msg += " it was expected to start.";
                logger.fine(msg);
            }
        } catch (InterruptedException ex) {
            msg = "Interrupted while";
            ok = false;
        }
        if (!ok) {
            msg += " waiting for " + timeOut * 100E-9;
            msg += " seconds for the correlator to start the sub-scan.";
            ObservingModeUtilities.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(), logger.getLogger());
        }
    }
    
    public void waitForSubscanEnd() throws ObservingModeErrorEx {
        // Wait for the subscanEnded function to be called (or timeout)
        boolean ok = false;
        String msg = "";
        long timeOut = 0;
        try {
            // TODO. Check that currentSubscan < expectedStartTimes.length;
            final long expectedEndTime = expectedEndTimes[currentSubscan];
            final long now = ObservingModeUtilities.getCurrentACSTime();
            if (now < expectedEndTime) {
                timeOut = expectedEndTime - now;
            }
            timeOut += Util.toTimeInterval(extraWaitTime);
            msg = "Waiting up to " + String.format("%.3f", timeOut * 100E-9)
                    + " seconds for sub-scan "
                    + expectedSubscans[currentSubscan] + " (#"
                    + (currentSubscan + 1) + " in a sequence of " + numSubscans
                    + ")" + " to complete. Its expected to end at "
                    + Util.acsTimeToString(expectedEndTime) + " and its now "
                    + Util.acsTimeToString(now);
            logger.fine(msg);
            ok = subscanEndedSem.tryAcquire(1, timeOut / 10,
                    TimeUnit.MICROSECONDS);
            if (!ok) {
                msg = "Timed out after";
            } else {
                msg = "The correlator indicated that subscan ";
                msg += expectedSubscans[currentSubscan] + " ended ";
                long delay = ObservingModeUtilities.getCurrentACSTime() - expectedEndTime;
                msg += String.format("%.3f", Math.abs(delay*100E-9)) + " seconds ";
                msg += (delay < 0) ? "before" : "after";
                msg += " it was expected to end.";
                logger.fine(msg);
                currentSubscan++;
            }
        } catch (InterruptedException ex) {
            msg = "Interrupted while";
            ok = false;
        }
        if (!ok) {
            msg += " waiting for " + timeOut*100E-9
                    + " seconds for the correlator to complete the sub-scan.";
            ObservingModeUtilities.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(), logger.getLogger());
        }

        // Now see if the last completion contains an error
        AcsJCompletion lastCompletion = AcsJCompletion.fromCorbaCompletion(completions.get(completions.size() - 1));
        if (lastCompletion.isError()) {
            AcsJObservingModeErrorEx ex = new AcsJObservingModeErrorEx(lastCompletion.getAcsJException().getErrorTrace());
            msg = "Problem completing the subscan.";
            ObservingModeUtilities.throwObservingModeErrorEx(msg, ex,
                    logger.getLogger());
        }
    }
    
    public void waitForSubscanSequence() throws ObservingModeErrorEx {
        for (int s = 0; s < numSubscans; s++) {
            waitForSubscanStart();
            waitForSubscanEnd();
        }
    }
           
    public int numberCompleted() {
        return scans.size();
    }

    public int[] scans() {
        int[] intArray = new int[scans.size()];
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = scans.get(i).intValue();
        }
        return intArray;
    }

    public int scan(int i) {
        return scans.get(i).intValue();
    }

    public int[] subscans() {
        int[] intArray = new int[subscans.size()];
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = scans.get(i).intValue();
        }
        return intArray;
    }

    public int subscan(int i) {
        return subscans.get(i).intValue();
    }

    public SubScanCorrelatorData[] getSubscanData() {
        return data.toArray(new SubScanCorrelatorData[0]);
    }

    public SubScanCorrelatorData getSubscanCorrelatorDatum(int i) {
        return data.get(i);
    }

    public Completion[] getCompletions() {
        return completions.toArray(new Completion[0]);
    }

    public Completion getCompletion(int i) {
        return completions.get(i);
    }
}
