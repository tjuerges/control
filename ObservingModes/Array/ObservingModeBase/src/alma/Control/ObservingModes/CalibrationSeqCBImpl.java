// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import alma.ACSErr.Completion;
import alma.Correlator.CalibrationSeqCB;
import alma.Correlator.CalibrationSeqCBHelper;
import alma.Correlator.CalibrationSeqCBPOA;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJCompletion;
import alma.acs.logging.AcsLogger;

public class CalibrationSeqCBImpl extends CalibrationSeqCBPOA {

    private ContainerServices cs;
    private AcsLogger logger;
    private CalibrationSeqCB corbaObject;
    private Semaphore semaphore;
    private int[] calIds;
    private Completion[] completions;

    public CalibrationSeqCBImpl(ContainerServices cs)
            throws ObservingModeErrorEx {
        this.cs = cs;
        logger = cs.getLogger();
        semaphore = new Semaphore(1);
        semaphore.drainPermits();
        try {
            corbaObject = CalibrationSeqCBHelper.narrow(cs
                    .activateOffShoot(this));
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Cannot create a CalibrationSeqCB CORBA object.";
            ObservingModeUtilities.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(ex), logger);
        }
    }

    // -------------- CORBA Functions ------------------
    // This is a one-way void call so we cannot throw exceptions
    public void report(int[] calibrations, Completion[] status) {
        completions = status;
        calIds = calibrations;
        semaphore.release();

        // Turn off the CORBA object. Once this is done this object can only
        // be accessed as a java object.
        try {
            cs.deactivateOffShoot(this);
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Error deactivating offshoot. Carrying on regardless.";
            logger.warning(msg);
        }
        corbaObject = null;
    }

    // --------------- Java only functions -----------------
    public CalibrationSeqCB getCORBAObject() {
        return corbaObject;
    }

    // The timeout is in seconds
    public boolean waitForResult(double timeOut) {
        return waitForResult((long) (timeOut/100E-9));
    }
    
    // The timeout is in 100's of ns (ACS duration units).
    public boolean waitForResult(long timeOut) {
        // Wait for the report function to be called (or timeout)
        boolean ok = false;
        timeOut = (timeOut+5)/10;
       try {
            ok = semaphore.tryAcquire(1, timeOut, TimeUnit.MICROSECONDS);
        } catch (InterruptedException ex) {
            String msg = "Interrupted while waiting for up to " + timeOut/1E6
                    + " seconds for the correlator calibration sub-scans.";
            logger.warning(msg);
            return false;
        }
        if (!ok) {
            String msg = "Timed out after waiting " + timeOut/1E6
                    + " seconds for the correlator calibration sub-scans.";
            logger.warning(msg);
            return false;
        }

        // There is certainly an error if the lengths of the arrays is
        // inconsistent
        if (completions.length != calIds.length) {
            return false;
        }
        // Now iterate through the completions to see if any of them contain an
        // error
        for (int i = 0; i < completions.length; i++) {
            AcsJCompletion c = AcsJCompletion
                    .fromCorbaCompletion(completions[i]);
            if (c.isError()) {
                return false;
            }
        }
        return true;
    }

    public Completion[] getCompletions() {
        return completions;
    }

    public int[] getCalIds() {
        return calIds;
    }
}
