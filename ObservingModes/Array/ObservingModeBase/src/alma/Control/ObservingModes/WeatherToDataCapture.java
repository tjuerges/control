// "@(#) $Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2009
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.logging.Level;

import alma.Control.CurrentWeather;
import alma.Control.WeatherData;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.StaticResource;
import alma.Control.CurrentWeatherPackage.Humidity;
import alma.Control.CurrentWeatherPackage.Location;
import alma.Control.CurrentWeatherPackage.Pressure;
import alma.Control.CurrentWeatherPackage.Temperature;
import alma.Control.CurrentWeatherPackage.WindDirection;
import alma.Control.CurrentWeatherPackage.WindSpeed;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogger;
import alma.acs.util.UTCUtility;
import alma.log_audience.DEVELOPER;
import alma.log_audience.OPERATOR;
import alma.offline.DataCapturer;

public class WeatherToDataCapture {

    // The ubiquitous ACS container services
    private ContainerServices cs;

    // The ubiquitous ACS Logger
    private AcsLogger logger = null;

    // The reference to data capture
    private DataCapturer dc = null;

    // The reference to the weather station controller
    private CurrentWeather wsc = null;

    // A list of all the weather stations
    private Location[] wsList = null;

    public WeatherToDataCapture(DataCapturer dataCaptureComponent,
            ContainerServices cs) throws ObsModeInitErrorEx {

        // 1. Setup container services and the logger
        if (cs == null) {
            AcsJObsModeInitErrorEx jEx = new AcsJObsModeInitErrorEx();
            String msg;
            msg = "Container services must not be null.";
            jEx.setProperty("Message", msg);
            // Cannot log it as we do not have a logger yet!
            throw jEx.toObsModeInitErrorEx();
        }

        this.cs = cs;
        this.logger = this.cs.getLogger();
        logger.logToAudience(Level.FINEST, getClass().getName()
                + ".WeatherToDataCapture", DEVELOPER.value);

        // 2. Setup the data capture reference
        if (dataCaptureComponent == null) {
            String msg = "Data capture reference must not be null.";
            ObservingModeUtilities.throwObsModeInitErrorEx(msg,
                    new AcsJObsModeInitErrorEx(), logger);
        }

        this.dc = dataCaptureComponent;

        // 3. Setup the WeatherStationController reference. The
        // component name is hard-coded.
        try {
                String wscName = "IDL:alma/Control/WeatherStationController:1.0";
                StaticResource<CurrentWeather> wscRes = 
                new StaticResource<CurrentWeather>(cs, 
                        wscName, 
                        "alma.Control.CurrentWeatherHelper", 
                        false, true, logger);
            ResourceManager.getInstance(this.cs).acquireResource(wscRes);
            this.wsc = ResourceManager.getInstance(this.cs).getComponent(wscName);
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = 
                "Cannot get a reference to a the weather station controller";
            ObservingModeUtilities.throwObsModeInitErrorEx(msg,
                    new AcsJObsModeInitErrorEx(ex), logger);
        }

        this.wsList = wsc.weatherStationLocations();
        if (wsList.length == 0) {
            String msg = "There are no weather stations available.";
            msg += " No weather data will be put into the ASDM!";
            logger.logToAudience(Level.SEVERE, msg, OPERATOR.value);
        }
    }

    public void sendWeatherData() {
        if (wsList.length == 0) {
            String msg = "There are no weather stations available.";
            msg += " No weather data will be put into the ASDM!";
            logger.logToAudience(Level.SEVERE, msg, OPERATOR.value);
            return;
        }
        int i = 0;
        try {
            WeatherData[] wd = new WeatherData[wsList.length];
            for (i = 0; i < wsList.length; i++) {
                wd[i] = new WeatherData();
                long maxTimestamp = 0;
                long minTimestamp = 9000000000000000000L;
                // name
                wd[i].name = wsList[i].name;
                { // Pressure
                    Pressure pressure = wsc.getPressure(wsList[i].name);
                    wd[i].pressure = pressure.value;
                    if (pressure.valid) {
                        if (pressure.timestamp < minTimestamp) {
                            minTimestamp = pressure.timestamp;
                        }
                        if (pressure.timestamp > maxTimestamp) {
                            maxTimestamp = pressure.timestamp;
                        }
                        wd[i].pressureFlag = false;
                    } else {
                        wd[i].pressureFlag = true;
                    }
                }
                {// Humidity
                    Humidity humidity = wsc.getHumidity(wsList[i].name);
                    wd[i].humidity = humidity.value;
                    if (humidity.valid) {
                        if (humidity.timestamp < minTimestamp) {
                            minTimestamp = humidity.timestamp;
                        }
                        if (humidity.timestamp > maxTimestamp) {
                            maxTimestamp = humidity.timestamp;
                        }
                        wd[i].humidityFlag = false;
                    } else {
                        wd[i].humidityFlag = true;
                    }
                }
                {// Temperature
                    Temperature temperature = wsc
                            .getTemperature(wsList[i].name);
                    wd[i].temperature = temperature.value;
                    if (temperature.valid) {
                        if (temperature.timestamp < minTimestamp) {
                            minTimestamp = temperature.timestamp;
                        }
                        if (temperature.timestamp > maxTimestamp) {
                            maxTimestamp = temperature.timestamp;
                        }
                        wd[i].temperatureFlag = false;
                    } else {
                        wd[i].temperatureFlag = true;
                    }
                }
                { // Wind Direction
                    WindDirection windDirection = wsc
                            .getWindDirection(wsList[i].name);
                    wd[i].windDirection = windDirection.value;
                    if (windDirection.valid) {
                        if (windDirection.timestamp < minTimestamp) {
                            minTimestamp = windDirection.timestamp;
                        }
                        if (windDirection.timestamp > maxTimestamp) {
                            maxTimestamp = windDirection.timestamp;
                        }
                        wd[i].windDirectionFlag = false;
                    } else {
                        wd[i].windDirectionFlag = true;
                    }
                }
                { // Wind Speed (and max wind speed)
                    WindSpeed windSpeed = wsc.getWindSpeed(wsList[i].name);
                    wd[i].windSpeed = wd[i].windMax = windSpeed.value;
                    if (windSpeed.valid) {
                        if (windSpeed.timestamp < minTimestamp)
                            minTimestamp = windSpeed.timestamp;
                        if (windSpeed.timestamp > maxTimestamp)
                            maxTimestamp = windSpeed.timestamp;
                        wd[i].windSpeedFlag = wd[i].windMaxFlag = false;
                    } else {
                        wd[i].windSpeedFlag = wd[i].windMaxFlag = true;
                    }
                }
                // Times
                wd[i].timestamp = maxTimestamp / 2 + minTimestamp / 2;
                wd[i].timeInterval = maxTimestamp - minTimestamp;
                if (wd[i].timeInterval == 0) {
                    wd[i].timeInterval = 480000;
                }
            } // end of loop over each weather station
            for (i = 0; i < wsList.length; i++) {
                String msg = "Weather station data sent to data capture for " 
                    +  wd[i].name;
                msg += "\n Mean time " 
                    + UTCUtility.getUTCDate(UTCUtility.utcOmgToJava(wd[i].timestamp));
                msg += ". Interval: " +  wd[i].timeInterval*100E-9 + " seconds.";
                msg += "\n Temperature: " + wd[i].temperature + " deg. C ("
                + (wd[i].temperatureFlag? "invalid": "OK") + ")";
                msg += "\n Pressure: " + wd[i].pressure/100 + " hPa (" 
                + (wd[i].pressureFlag? "invalid": "OK") + ")";
                msg += "\n Humidity: " + wd[i].humidity*100 + " % (" 
                + (wd[i].humidityFlag? "invalid": "OK") + ")";
                msg += "\n Wind speed: " + wd[i].windSpeed + " m/s (" 
                + (wd[i].windSpeedFlag? "invalid": "OK") + ")";
                msg += " max speed: " + wd[i].windMax + " m/s (" 
                + (wd[i].windMaxFlag? "invalid": "OK") + ")";
                msg += " and direction: " + wd[i].windDirection*180/Math.PI + " deg (" 
                + (wd[i].windDirectionFlag? "invalid": "OK") + ")";
                logger.logToAudience(Level.FINE, msg, OPERATOR.value);
            }
            dc.sendWeatherData(wd);
        } catch (IllegalParameterErrorEx ex) {
            String msg = "The name of the weather station called '";
            msg += wsList[i].name + "' is incorrect.";
            msg += " Not all weather data will be put into the ASDM!";
            ObservingModeUtilities.logSevereError(msg, ex.errorTrace,
                    OPERATOR.value, logger);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot update the weather station table.";
            msg += " No weather data will be put into the ASDM!";
            ObservingModeUtilities.logSevereError(msg, ex.errorTrace,
                    OPERATOR.value, logger);
        } catch (DataErrorEx ex) {
            String msg = "Problem with the weather data.";
            msg += " No weather data will be put into the ASDM!";
            ObservingModeUtilities.logSevereError(msg, ex.errorTrace,
                    OPERATOR.value, logger);
        }
    }
    // public void sendWeatherStationCharacteristics() {
    // }

    // public void cleanUp() {
    // }
}
