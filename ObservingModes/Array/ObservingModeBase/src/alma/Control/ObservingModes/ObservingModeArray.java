/*
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 * "@(#) $Id$"
 */

package alma.Control.ObservingModes;

import alma.ACSErrTypeCommon.wrappers.AcsJBadParameterEx;
import alma.Control.AntennaCharacteristics;
import alma.Control.BDDStreamInfo;
import alma.Control.ResourceId;
import alma.Control.ScanIntentData;
import alma.ScanIntentMod.ScanIntent;
import alma.asdmIDLTypes.IDLEntityRef;

/**
 * The observing modes maintain a reference to the array that created them. This interface
 * is used as the type of this reference, thereby restricting the relationship between the 
 * array component implementation class and the observing modes. The ObservingModeArray 
 * interface is, in other words, the view that the observing modes have about the array 
 * that created them.
 * 
 * @version $Id$
 * @author Rafael Hiriart
 *
 */
public interface ObservingModeArray {

    /**
     * Returns the array name.
     * 
     * @return Array name.
     */
    String getArrayName();

    /**
     * Get array component name.
     * 
     * Equivalent to "CONTROL/" + getArrayName(). 
     * 
     * @return Array component name.
     */
    String getArrayComponentName();
    
    /**
     * Returns the list of antennas allocated to this array
     *
     * @return List of antenna names for this array
     */
    String[] getAntennas();

    /**
     * Returns the configuration of the antennas allocated in this array.
     * 
     * @return Antenna characteristics for the array antennas.
     */
    AntennaCharacteristics[] getAntennaConfigurations();
    
    /**
     * Gets the scan number.
     * 
     * @return Scan number
     */
    int getScanNumber();
    
    /**
     * Increment the scan number.
     */
    void incrScanNumber();
    
    /**
     * Reset the scan number.
     */
    void resetScanNumber();
    
    /**
     * Gets the subscan number.
     * 
     * @return Subscan number
     */
    int getSubScanNumber();
    
    /**
     * Increment the scan number.
     */
    void incrSubScanNumber();
    
    /**
     * Reset the subscan number.
     */
    void resetSubScanNumber();
    
    /**
     * Gets the ASDM entity reference. (Also known as the ExecBlock ID.)
     * 
     * @return ASDM entity reference
     */
    IDLEntityRef getASDMEntRef();
    
    /**
     * Gets the DataCapturer name.
     * 
     * @return DataCapturer name
     */
    String getDataCapturerName();
    
    int getCorrelatorConfigID();
    void incrCorrelatorConfigID();

    /**
     * Gets the configuration name.
     * The configuration name comes from the TMCDB and is set in the array
     * by CONTROL/MASTER. It possibilitates to setup special cases in the
     * observing modes. For example, it is used to decide what type of LO
     * controller class to use. This was needed to support the ATF,
     * where a special class, different from the production LO controller,
     * was used.
     * 
     * @return Configuration name
     */
    String getConfigurationName();
    
    boolean sbHasBeenAborted();
    
    boolean sbHasBeenStopped();
    
    boolean sbHasToStop();
    
    /**
     * Returns an array with the Resource identifiers for the Photonic
     * References assigned to the Array. Each Resource identifier contains
     * both the Resource reference name and the Resource component name for
     * the PhotonicReference.
     * 
     * @return Photonic References Resource Identifiers
     */
    ResourceId[] getPhotonicReferences();
    
    /**
     * Get the Correlator Bulk Data Distributor Streaming Information.
     * <P>
     * The BDDStreamInfo defines which Bulk Data Distributor component to use, and
     * the flow numbers for streaming the full spectral data, the channel average
     * data and the WVR data.
     * 
     * @return BDDStreamInfo IDL structure
     * @see ICD/CONTROL/idl/ControlBasicInterfaces.midl
     */
    BDDStreamInfo getCorrelatorBDDStreamInfo();
    
    /**
     * Get the Correlator ObservationControl component name.
     * 
     * Control interacts with Correlator through a component that implements the
     * 'IDL:alma/Correlator/ObservationControl:1.0' interface.
     * Depending on which type of correlator the array has been configured to use, the
     * component that needs to be accessed will differ, being either the BL or
     * the ACA version.
     * 
     * @return Observation control component name
     * @throws AcsJBadParameterEx If the Array has not been configured properly, so the
     * correlator type is unknown.
     */
    String getCorrelatorObservationControlComponentName() throws AcsJBadParameterEx;
    
    // Methods to set status variables
    
    void setCorrConfigID(int corrConfigID);

    void setScan(int scan);

    void setSubScan(int subScan);

    void setScanIntentData(ScanIntentData[] scanIntentData);

    void setScanStartTime(long scanStartTime);

    void setSubScanDuration(long subScanDuration);

    void setUsesCorr(boolean usesCorr);

    void setApState(String apState);

    void setCorrMode(short corrMode);

    void setExecBlockAllowedTime(long execBlockAllowedTime);

    void setExecBlockUid(String execBlockUid);

    void setSchedBlockUid(String schedBlockUid);

    void setPiName(String piName);

    void setProjectName(String projectName);

    void setScript(String script);

    void setExecBlockStartTime(long execBlockStartTime);
}

// __oOo__
