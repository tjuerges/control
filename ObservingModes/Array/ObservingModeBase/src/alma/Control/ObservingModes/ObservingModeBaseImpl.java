// "@(#) $Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import alma.Control.AbortionException;
import alma.Control.AntModeController;
import alma.Control.Completion;
import alma.Control.ObservingModeBaseOperations;
import alma.Control.ScanEndedEvent;
import alma.Control.ScanIntentData;
import alma.Control.ScanStartedEvent;
import alma.Control.SubscanEndedEvent;
import alma.Control.SubscanStartedEvent;
import alma.Control.Common.DeveloperLogger;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.Util;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJDataCapturerErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJException;
import alma.acs.nc.SimpleSupplier;
import alma.log_audience.DEVELOPER;
import alma.log_audience.OPERATOR;

/** 
 * This class is the base class for all of our True observing modes
 * as opposed to the ObservingModeHelpers.
 *
 * It should have all common functionality (that is things that all
 * observing modes should do).
 *
 * Currently this class inherits from the ObservingModeCore which means
 * it has the infrastructure to deal with antenna Mode Controllers.  I am
 * not sure we need this long term and we may want to get rid of it.
 */

public class ObservingModeBaseImpl<MC extends AntModeController> 
    extends ObservingModeCore<MC>
    implements ObservingModeBaseOperations {

    // Indicates if the SB should be aborted.
    private boolean sbExecAborted = false;

    // A class that sends weather data to data capture;
    protected WeatherToDataCapture weather;
    
    // This is used to publish the events defining the scan/sub-scan boundaries
    protected SimpleSupplier publisher = null;


    // //////////////////////////////////////////////////////////////////
    // Constructors
    // //////////////////////////////////////////////////////////////////
    
    public ObservingModeBaseImpl(ContainerServices cs,
                                 ObservingModeArray array)
        throws ObsModeInitErrorEx {
        super(cs, array);

        /* Initialize the class to report the weather data */
        weather = new WeatherToDataCapture(getDataCapture(), cs);
        
        // Used to publish events indicating the begin and end of scan and
        // sub-scans
        publisher = getControlSystemNC();
    }

    /* --------- OBSERVING MODE INTERFACE IMPLEMENTATION ----------- */

    /* Method for handling everything that needs to be done at the 
       beginning of every scan */
    public void beginScan(ScanIntentData[] scanIntents)
            throws DataCapturerErrorEx, ObservingModeErrorEx {

        /* Modify the counters in the array class */
        array.incrScanNumber();
        array.resetSubScanNumber();
        array.setScanIntentData(scanIntents);

        /* Notify the user that we are starting a new scan */
        logBeginScan(scanIntents);

        /* This is the startTime for the Scan */
        final long now = ObservingModeUtilities.getCurrentACSTime();

        ScanStartedEvent event = new ScanStartedEvent();
        event.execId = array.getASDMEntRef();
        event.arrayName = array.getArrayName();
        event.scanNumber = array.getScanNumber();
        event.scanType = scanIntents;
        event.startTime = now;

        try {
            publisher.publishEvent(event);
        } catch (AcsJException ex) {
            // This is fatal this event is used by Telcal
            String msg = "Cannot publish a scan started event.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }

        try {
            dataCapturer.startScan(array.getArrayName(), now, scanIntents);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Error starting scan in OFFLINE/DataCapturer.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        } catch (DataErrorEx ex) {
            String msg = "Problem telling data capture the scan has started.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        }
    }

    /* Method for handling everything that needs to be done at the 
       end of every scan */
    // TODO. Send the scan status
    public void endScan() throws ObservingModeErrorEx, DataCapturerErrorEx {

        try {
            long now = ObservingModeUtilities.getCurrentACSTime();
            final String arrayName = array.getArrayName();
            final int scanNumber = array.getScanNumber();
            // TODO Why does the subscan number need to be sent?
            // What about sending a scan status?
            dataCapturer.endScan(arrayName, now, scanNumber,
                    array.getSubScanNumber());

            ScanEndedEvent event = new ScanEndedEvent();
            event.execId = array.getASDMEntRef();
            event.arrayName = arrayName;
            event.scanNumber = scanNumber;
            event.status = Completion.SUCCESS;
            event.endTime = now;
            publisher.publishEvent(event);
            array.setScanIntentData(null);
        } catch (AcsJException ex) {
            // This is fatal this event is used by Telcal
            String msg = "Cannot publish a scan ended event.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (TableUpdateErrorEx ex) {
            String msg = "Problem telling data capture the scan has ended.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        } catch (DataErrorEx ex) {
            String msg = "Problem telling data capture the scan has ended.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        }
    }

    @Deprecated
    public void beginSubscan(SubscanIntent[] subscanIntents) 
        throws ObservingModeErrorEx, AbortionException, DataCapturerErrorEx
    {
        /* Check to see if we need to abort this scan */
        if (getAbortScanFlag()) {
            throw new AbortionException(
                    "SB execution has been aborted by the user.");
        }
        
        /* Increment the subscan number in the array */
        array.incrSubScanNumber();

        /* Send the weather data to data capture */
        weather.sendWeatherData();
    }
    
    public void beginSubscan(SubscanIntent[] subscanIntents, long startTime,
            long duration) throws ObservingModeErrorEx, AbortionException,
            DataCapturerErrorEx {
        // Check to see if we need to abort this scan
        // TODO Replace this with an ACS exception
        if (getAbortScanFlag()) {
            throw new AbortionException("Execution has been aborted by the observer.");
        }

        // Increment the subscan number in the array
        array.incrSubScanNumber();
        String msg;
        { // produce a hopefully useful log for the observer
            msg = "Subscan " + array.getSubScanNumber();
            if (subscanIntents.length > 0) {
                if (subscanIntents.length > 1) {
                    msg += " has intens of [";
                    for (SubscanIntent i : subscanIntents) {
                        msg += i + " ";
                    }
                    msg += "]";
                } else {
                    msg += " has an intent of " + subscanIntents[0];
                }
            } else {
                msg += " has no intent";
            }

            msg += ", takes " + String.format("%.3f", duration * 100E-9)
                    + " seconds";
            msg += " & starts at " + Util.acsTimeToString(startTime);
            opLog.info(msg);
        }
        /* Send the weather data to data capture */
        weather.sendWeatherData();

        // Send the sub-scan started event. This is just for users
        // benefit and is only used by the data flow panel in the OMC
        {
            SubscanStartedEvent event = new SubscanStartedEvent();
            event.execId = array.getASDMEntRef();
            event.arrayName = array.getArrayName();
            event.scanNumber = array.getScanNumber();
            event.subscanNumber = array.getSubScanNumber();
            event.startTime = startTime;
            try {
                publisher.publishEvent(event);
            } catch (AcsJException ex) {
                // This is not fatal as the only consumer of these
                // events is the dataflow GUI.
                msg = "Cannot publish a subscan started event.";
                logSevereError(msg, ex.getErrorTrace(), OPERATOR.value);
            }
        }
    }

    @Deprecated
    public void endSubscan(int numberOfIntegrations)
            throws ObservingModeErrorEx, DataCapturerErrorEx {
        final long now = ObservingModeUtilities.getCurrentACSTime();

       // Send weather data to data capture
        weather.sendWeatherData();

        String msg = "Subscan " + array.getSubScanNumber()
                + " has completed at " + Util.acsTimeToString(now);
        opLog.fine(msg);

        SubscanEndedEvent event = new SubscanEndedEvent();
        event.execId = array.getASDMEntRef();
        event.arrayName = array.getArrayName();
        event.scanNumber = array.getScanNumber();
        event.subscanNumber = array.getSubScanNumber();
        event.status = Completion.SUCCESS;
        event.endTime = now;

        try {
           // The check for the event.execId has been introduced to allow
            // test cases to call this function directly without having to
            // call beginSubscan() first.
            if (event.execId != null) {
                publisher.publishEvent(event);
            }
        } catch (AcsJException ex) {
            // This is not fatal as the only consumer of these events is
            // the dataflow GUI
            msg = "Cannot send the sub-scan ended event.";
            logSevereError(msg, ex.getErrorTrace(), OPERATOR.value);
        }
    }
    
    public void endSubscan(long endTime, Completion status)
            throws ObservingModeErrorEx, DataCapturerErrorEx {
        // Send weather data to data capture
        weather.sendWeatherData();

        final int subscanNumber = array.getSubScanNumber();
        final String arrayName = array.getArrayName();
        String msg = "Sub-scan " + subscanNumber + " was completed at "
                + Util.acsTimeToString(endTime);
        opLog.fine(msg);

        try {
            SubscanEndedEvent event = new SubscanEndedEvent();
            event.execId = array.getASDMEntRef();
            event.arrayName = arrayName;
            event.scanNumber = array.getScanNumber();
            event.subscanNumber = subscanNumber;
            event.status = status;
            event.endTime = endTime;
            publisher.publishEvent(event);
        } catch (AcsJException ex) {
            // This is not fatal as the only consumer of these
            // events is the dataflow GUI in the OMC
            msg = "Cannot publish a sub-scan ended event.";
            logSevereError(msg, ex.getErrorTrace(), DEVELOPER.value);
        }
      }

    /* ------------ Functions to be moved ------------------ */
    protected void logBeginScan(ScanIntentData[] scanIntentData) {
        String msg = "Starting scan number " + array.getScanNumber()
                + " on array " + array.getArrayName();

        if (scanIntentData == null) {
            msg += ".";
        } else {
            if (scanIntentData.length == 1) {
                msg += " with scan intent " + scanIntentData[0].scanIntent
                        + " using " + scanIntentData[0].calDataOrig + ".";
            } else {
                msg += " with scan intents of ";
                for (int i = 0; i < scanIntentData.length - 1; i++) {
                    msg += scanIntentData[i].scanIntent + " using ";
                    msg += scanIntentData[i].calDataOrig + "), ";
                }
                msg += scanIntentData[scanIntentData.length - 1].scanIntent
                        + " using ";
                msg += scanIntentData[scanIntentData.length - 1].calDataOrig
                        + ".";
            }
        }
        opLog.info(msg);
    } 

     ////////////////////////////////////////////////////////////////////
     // Private and protected operations
     ////////////////////////////////////////////////////////////////////

     ////////////////////////////////////////////////////////////////////
     // ObservingModeBase operations
     ////////////////////////////////////////////////////////////////////

    public void abortScan() {
        sbExecAborted = true;
    }

    public void resetAbortScanFlag() {
        sbExecAborted = false;
    }

    public boolean getAbortScanFlag() {
        return sbExecAborted;
    }
    
    private SimpleSupplier getControlSystemNC() throws ObsModeInitErrorEx {
        ResourceManager resMng = ResourceManager.getInstance(cs);
        SimpleSupplier publisher = null;
        try {
            Resource<SimpleSupplier> res = resMng.getResource("CONTROL_SYSTEM");
            publisher = res.getComponent();
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Cannot access to the CONTROL_SYSTEM notification channel.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex.getErrorTrace()));
        }
        return publisher;
    }
    
    static public Boolean shutdownObservingMode(alma.Control.ObservingModeBase offshoot,
            org.omg.PortableServer.Servant poaTie, ContainerServices cs) {
        Boolean retVal = true;
        DeveloperLogger logger = new DeveloperLogger(cs.getLogger());
        if (offshoot != null) {
            String msg = "Calling the cleanup function on the offshoot";
            logger.finer(msg);
            try {
                offshoot.cleanUp();
            } catch (Exception ex) {
                logger.warning("Error calling Offshoot cleanup method.");
                ex.printStackTrace();
            }
        }
        if (poaTie != null) {
            try {
                String msg = "Deactivating the offshoot.";
                logger.finer(msg);
                cs.deactivateOffShoot(poaTie);
            } catch (AcsJContainerServicesEx ex) {
                String msg = "Problem deactivating the offshoot.";
                logger.warning(msg);
                ex.log(logger.getLogger());
                retVal = false;
            } catch (Exception ex) {
                ex.printStackTrace();
                String msg = "Problem deactivating the offshoot.";
                logger.warning(msg);
                retVal = false;                
            }
        }
        return retVal;
    }
}
