/*
 * "@(#) $Id$"
 * ALMA - Atacama Large Millimeter Array
 * (c) Associated Universities Inc., 2010 - 2011
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */

package alma.Control.ObservingModes;

// import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.Map;
import java.util.TreeMap;

// import java.util.logging.Logger;

import alma.ACSErr.Completion;
import alma.ACSErr.ErrorTrace;
import alma.Control.AntModeController;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.Util;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.BadConfigurationEx;
import alma.ModeControllerExceptions.CannotGetAntennaEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.ModeControllerExceptions.wrappers.AcsJTimeoutEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJDataCapturerErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceExceptionsEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJCompletion;
import alma.acs.logging.AcsLogger;
import alma.Control.Common.OperatorLogger;
import alma.Control.Common.DeveloperLogger;


import alma.log_audience.DEVELOPER;
import alma.offline.DataCapturer;
import alma.xmlstore.Identifier;
import alma.xmlstore.IdentifierPackage.NotAvailable;

/**
 * The ObservingModeCore is a base class for any Observing Mode Objects
 * which need to handle having many antenna based controllers 
 */
public class ObservingModeCore<MC extends AntModeController> {

    // The ubiquitous ACS ContainerServices
    protected ContainerServices cs;

    // Loggers, the generic as well as specialized Operator and Developer
    protected AcsLogger logger;
    protected OperatorLogger opLog = null;
    protected DeveloperLogger devLog = null;

    // A Map with the antenna mode controllers, indexed by antenna name
    protected Map<String, MC> antennaModeController = 
        new TreeMap<String, MC>();

    // An interface to the array that created this observing mode
    protected ObservingModeArray array;

    // A reference to Data Capture which can be used
    protected DataCapturer dataCapturer = null;

     // //////////////////////////////////////////////////////////////////
     // Constructors
     // //////////////////////////////////////////////////////////////////
    
    public ObservingModeCore(ContainerServices cs,
                             ObservingModeArray array)
        throws ObsModeInitErrorEx {
        if (cs == null) {
            ObservingModeUtilities.throwObsModeInitErrorEx
                ("ContainerServices must not be null.",
                 new AcsJObsModeInitErrorEx(),
                 null);
        }

        this.cs = cs;
        logger = this.cs.getLogger();
        opLog = new OperatorLogger(logger);
        devLog = new DeveloperLogger(logger);
        devLog.finest(getClass().getName() + ".ObservingModeBase");

        this.array = array;
        this.dataCapturer = getDataCapture();
    }

    /**
     * This method returns a list of all the antennas which this
     * Object is managing
     */
    public String[] getAntennas() {
        return antennaModeController.keySet().toArray(new String[0]);
    }

    /**
     * Populates the antennaModeController map, creating one antenna mode
     * controller for each member in the array
     * 
     * ObservingModeBase concrete children are expected to call this method to
     * create their antenna mode controllers.
     * 
     * @param helperClassName
     *            Antenna mode controller helper class name. Note: May be it
     *            should be more appropriate to pass the IDL ID.
     * @param suffix
     *           Suffix to use when forming the mode controller name. The mode
     *            controller name will be formed concatenating the antenna
     *            component name and the suffix.
     */

    protected void createAntModeControllers(String helperClassName,
                                            String suffix) 
        
        throws ObsModeInitErrorEx {
        createAntModeControllers(array.getAntennas(), helperClassName, suffix);
    }


    /**
     * Populates the antennaModeController map, creating one antenna mode
     * controller for each member in the antenna list.
     * 
     * ObservingModeBase concrete children are expected to call this method to
     * create their antenna mode controllers.
     * 
     * @param helperClassName
     *            Antenna mode controller helper class name. Note: May be it
     *            should be more appropriate to pass the IDL ID.
     * @param suffix
     *           Suffix to use when forming the mode controller name. The mode
     *            controller name will be formed concatenating the antenna
     *            component name and the suffix.
     */
    protected void createAntModeControllers(String[] antennaList,
                                            String helperClassName,
                                            String suffix) 
        throws ObsModeInitErrorEx {
        createAntModeControllers(antennaList, helperClassName, suffix, true);
    }

    /**
     * Populates the antennaModeController map, creating one antenna mode
     * controller for each member in the antennaCharacteristics.
     * 
     * ObservingModeBase concrete children are expected to call this method to
     * create their antenna mode controllers.
     * 
     * @param antennaList
     *            List of the antenna names (i.e. DV01) which this object
     *            should be managing.
     * @param helperClassName
     *            Antenna mode controller helper class name. Note: May be it
     *            should be more appropriate to pass the IDL ID.
     * @param suffix
     *            Suffix to use when forming the mode controller name. The mode
     *            controller name will be formed concatenating the antenna
     *            component name and the suffix.
     * @param allocate
     *            Whether to allocate or not the mode controller to the antenna.
     *            This option is provided to allow the application of any
     *            operation over the mode controller that need to be performed
     *            before allocate. If False, then the allocation needs to be
     *            done explicitly in the derived class.
     */
    protected void createAntModeControllers(String[] antennaList,
                                            String helperClassName,
                                            String suffix, boolean allocate)
        throws ObsModeInitErrorEx {
         logger.logToAudience(Level.FINEST, getClass().getName()
                              + ".createAntModeControllers", DEVELOPER.value);

         /* To Do: This should be done in parallel */
         for (int idx =0; idx < antennaList.length; idx++) {
             addAntModeController(antennaList[idx], helperClassName,
                                  suffix, allocate);
         }
    }
    
    protected void addAntModeController(String antennaName,
                                        String helperClassName,
                                        String suffix, boolean allocate)
        throws ObsModeInitErrorEx {


        ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
        Method narrowMethod = null;

        try {
            Class helper = null;
            Method idMethod = null;
            helper = Class.forName(helperClassName);
            idMethod = helper.getMethod("id", (Class[]) null);
            narrowMethod = helper.getMethod("narrow", Object.class);
            spec.setComponentType((String) idMethod.invoke(null));
        } catch (ClassNotFoundException ex) {
            String msg = "Error finding the helper class called "
                + helperClassName;
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (NoSuchMethodException ex) {
            String msg = "Error finding methods on helper class called "
                + helperClassName;
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (InvocationTargetException ex) {
            String msg = "Cannot determine the IDL type of class "
                + helperClassName + 
                " as this class does not contain an id function.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (IllegalAccessException ex) {
            String msg = "Cannot determine the IDL type of class " +
                helperClassName + " as the id method cannot be executed.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        }

        MC narrowedObj = null;
        String antCompName = ""; // eg., "CONTROL/DV01"
        String mcName = ""; // eg., "CONTROL/DV01/SingleFieldInterferometry"

        try {
            antCompName = "CONTROL/" + antennaName;
            mcName = antCompName + "/" + suffix;

            spec.setComponentName(mcName);

            logger.logToAudience
                (Level.FINE, "Trying to get a reference to a component called "
                 + mcName + " thats collocated with the component called "
                 + antCompName, DEVELOPER.value);
            org.omg.CORBA.Object obj = 
                cs.getCollocatedComponent(spec, false, antCompName);

            narrowedObj = (MC) narrowMethod.invoke(null, obj);
            if (allocate) {
                logger.logToAudience
                    (Level.FINE, "Got a narrowed the reference to the"+
                     " component called " + mcName + 
                     ". Allocating it to antenna " + antennaName, 
                     DEVELOPER.value);
                narrowedObj.allocate(antennaName);
            }
            addModeController(antennaName, narrowedObj);
        } catch (AcsJContainerServicesEx ex) {
            String msg = "Error getting a reference to the component called "
                + mcName;
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (IllegalAccessException ex) {
            String msg = "Error narrowing the component called " + mcName;
            throwObsModeInitErrorEx(msg,  new AcsJObsModeInitErrorEx(ex));
        } catch (InvocationTargetException ex) {
            String msg = "Error finding component type for the " + 
                "component called " + mcName;
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (CannotGetAntennaEx ex) {
            String msg = "Cannot get a reference to the antenna " + 
                "component called " + antCompName;
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (BadConfigurationEx ex) {
            String msg = "Antenna " + antennaName
                + " does not contain all the required devices.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        }
    }

            
    /**
     * Adds a mode controller to the controller map.
     * 
     * @param modeController Mode controller.
     */
    protected void addModeController(String antennaName,
                                  MC modeController) {
         logger.logToAudience(Level.FINEST, getClass().getName()
                 + ".addModeController", DEVELOPER.value);
         if (modeController == null)
             throw new NullPointerException("AntModeController is null");
         logger.finer("Adding mode controller in map with key " + antennaName);
         antennaModeController.put(antennaName, modeController);
    }
                                  
    /**
     * Removes the mode controller assigned to a given antenna from the mode
     * controller map.
     * 
     * @param antennaName
     *            Antenna name.
     * @return Mode controller removed, or null if there is no mode controller
     *         in the map associated with the antenna.
     */
    protected MC removeModeController(String antennaName) {
        logger.logToAudience(Level.FINEST, getClass().getName()
                             + ".removeModeController", DEVELOPER.value);
        return antennaModeController.remove(antennaName);
    }


    protected DataCapturer getDataCapture() throws ObsModeInitErrorEx {
        ResourceManager resMng = ResourceManager.getInstance(cs);
        DataCapturer dc = null;
        try {
            Resource<DataCapturer> dcRes = 
                resMng.getResource(array.getDataCapturerName());
            dc = dcRes.getComponent();
        } catch (AcsJResourceExceptionsEx ex1) {
            try {
                // DataCapturer is normally created when array.beginExecution
                // is called.
                // TODO. Get rid of this. It was put in to help with testing and
                // we should instead ensure that our tests call
                // array.beginExecution (which then creates a data capture
                // component).
                logger.warning("A data capture component hasn't been created"+
                               " yet i.e, beginExection has not been called."+
                               " Creating one on the fly.");
                String dcIFName = "IDL:alma/offline/DataCapturer:1.0";
                String dcName = array.getDataCapturerName();
                DynamicResource<DataCapturer> resource = 
                    new DynamicResource<DataCapturer>(cs, dcName, dcIFName,
                                                      true);
                resMng.acquireResource(resource);
                dc = resMng.getComponent(dcName);
            } catch (AcsJResourceExceptionsEx ex) {
                String msg = "Cannot create a data capture component";
                throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
            }
        }
        return dc;
    }
    
    public String[] getUIDs(short numIDs) throws ObservingModeErrorEx {
        String[] uids = null;
        try {
            ResourceManager resMng = ResourceManager.getInstance(cs);
            Resource<Identifier> idRes = resMng.getResource("ARCHIVE_IDENTIFIER");
            Identifier identifier = idRes.getComponent();
            // Get a range from the ARCHIVE Identifier. The range can
            // be used to assign unique entity ids. It is not locked.
            uids = identifier.getUIDs(numIDs);
        } catch (AcsJResourceExceptionsEx ex) {
            String msg = "Cannot get any identifiers from the archive.";
            msg += " No data can be saved.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (NotAvailable ex) {
            String msg = "Cannot get any identifiers from the archive.";
            msg += " No data can be saved.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        return uids;
    }

    /**
     * Releases the antenna mode controllers and clear the 
     * antennaModeController map.
     */
    public void cleanUp() {
        logger.logToAudience(Level.FINEST, getClass().getName() + ".cleanUp",
                             DEVELOPER.value);
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            String ant = iter.next();
            String mcName = antennaModeController.get(ant).name();
            // Do not deallocate the mode controller here!
            // It should normally happen when the component is being shutdown
            // i.e., its in the AntModeController::cleanUp function. We cannot
            // explicitly release the mode controller as the MountController 
            // may be in use by a MountPanel.
            logger.logToAudience(Level.FINE, "Releasing mode controller '"
                                 + mcName + "'.", DEVELOPER.value);
            cs.releaseComponent(mcName);
        }
        antennaModeController.clear();
    }



    /* ------------------------ Callback Interface -------------- */
    // This indicates how many antennas, in this array, can throw
    // exceptions and still have functions that operate on the entire
    // array execute without throwing an exception. TODO. A real
    // implementation of this function.
    public int maxNumBadAntennas() {
        logger.logToAudience(Level.FINEST, getClass().getName()
                             + ".maxNumBadAntennas", DEVELOPER.value);
        return 0;
    }

    protected AntModeControllerCBImpl createCallback() 
        throws ObservingModeErrorEx {
        logger.finest("alma.ObservingModeBaseImpl.createCallback");
        AntModeControllerCBImpl cbObj = null;
         try {
             cbObj = new AntModeControllerCBImpl(cs);
         } catch (AcsJContainerServicesEx ex) {
             String msg = "Cannot create an AntModeControllerCB offshoot";
             throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
         }
         return cbObj;
    }
    
    protected void checkCompletions(HashMap<String, Completion> completions,
                                    String commandName) 
        throws ObservingModeErrorEx, TimeoutEx {
        logger.finest("alma.ObservingModeBaseImpl.checkCompletions");
        
        // Check the completions
        ArrayList<String> noCompletion = new ArrayList<String>();
        HashMap<String, Completion> errorCompletion =
            new HashMap<String, Completion>();
        for (Iterator<String> iter = completions.keySet().iterator(); 
             iter.hasNext();) {
            final String antName = iter.next();
            Completion compl = completions.get(antName);
            if (compl == null) {
                noCompletion.add(antName);
            } else if (AcsJCompletion.fromCorbaCompletion(compl).isError()) {
                errorCompletion.put(antName, compl);
            }
        }
        // Log all bad or missing completions
        final int numBadAntennas = noCompletion.size()+errorCompletion.size();
        
        if (numBadAntennas > 0) {
            AcsJTimeoutEx jTimeEx = new AcsJTimeoutEx();
            {
                String msg = "Problem determining if antenna(s) ";
                for (Iterator<String> iter = noCompletion.iterator();
                     iter.hasNext();) {
                    String antName = iter.next();
                    msg += antName + " ";
                }
                msg += "have completed the "
                    + commandName
                    + " command (as their completion is null)."
                    + " Perhaps the timeout is too short. Assuming there"
                    + " is a problem.";
                jTimeEx.setProperty("Message", msg);
                jTimeEx.log(logger);
            }
            
            AcsJObservingModeErrorEx jObservingModeErrorEx = 
                new AcsJObservingModeErrorEx();
            {
                String msg = "Errors while waiting for antenna(s) ";
                for (Iterator<String> iter = 
                         errorCompletion.keySet().iterator(); iter.hasNext();){
                    final String antName = iter.next();
                    msg += antName + " ";
                    jObservingModeErrorEx.setProperty(antName,
                                                      Util.getNiceErrorTraceString(errorCompletion.get(antName).previousError[0]));
                }
                msg += "to do the " + commandName + " command.";
                jObservingModeErrorEx.setProperty("Message", msg);
                jObservingModeErrorEx.log(logger);
            }
            
            // If enough antennas are bad, throw an exception.
            if (numBadAntennas > maxNumBadAntennas()) {
                // throw the exception that affects more antennas.
                if (noCompletion.size() > errorCompletion.size()) {
                    throw jTimeEx.toTimeoutEx();
                } else {
                    throw jObservingModeErrorEx.toObservingModeErrorEx();
                }
            }
        }
    }

    /* --------- Exception Helper Methods ------------------ */
    protected void throwObsModeInitErrorEx(String msg,
                                           AcsJObsModeInitErrorEx jex)
        throws ObsModeInitErrorEx {
        jex.setProperty("Array", array.getArrayName());
        ObservingModeUtilities.throwObsModeInitErrorEx(msg, jex, logger);
    }

    protected void throwIllegalParameterErrorEx(String msg)
            throws IllegalParameterErrorEx {
        throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
    }

    protected void throwIllegalParameterErrorEx(String msg,
            AcsJIllegalParameterErrorEx jex) throws IllegalParameterErrorEx {
        jex.setProperty("Array", array.getArrayName());
        ObservingModeUtilities.throwIllegalParameterErrorEx(msg, jex, logger);
    }

    protected void throwObservingModeErrorEx(String msg)
            throws ObservingModeErrorEx {
        throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
    }

    protected void throwObservingModeErrorEx(String msg,
            AcsJObservingModeErrorEx jex) throws ObservingModeErrorEx {
        jex.setProperty("Array", array.getArrayName());
        ObservingModeUtilities.throwObservingModeErrorEx(msg, jex, logger);
    }

    protected void throwDataCapturerErrorEx(String msg,
            AcsJDataCapturerErrorEx jex) throws DataCapturerErrorEx {
        jex.setProperty("Array", array.getArrayName());
        ObservingModeUtilities.throwDataCapturerErrorEx(msg, jex, logger);
    }

   /* ------------- Logging Helper Methods ------------------ */
    protected void logSevereError(String msg, ErrorTrace et, String audience) {
        ObservingModeUtilities.logSevereError(msg, et, audience, logger);
    }
}