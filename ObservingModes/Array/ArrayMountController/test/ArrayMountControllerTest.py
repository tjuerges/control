#!/usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import sys
import unittest
import time
import math
import shutil
shutil.rmtree("../lib/python/site-packages", True)
import Acspy.Clients.SimpleClient
import ObservingModeExceptions
import maciErrTypeImpl
import Acspy.Common.ErrorTrace
import ControlExceptions
import ModeControllerExceptions
import ObservingModeExceptions
import Control
import TETimeUtil
import Acspy.Common.TimeHelper
import Acspy.Common.Log
import CCL.Array
import CCL.FieldSource

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
        
    def test01( self ):
        '''
        This simply starts the array mount controller offshoot and
        shuts it down again. It uses the real control system
        components (components from other subsystems are
        simulated). It checks the basic lifecycle of the array mount
        controller offshoot.
        '''
        try:
            arrayId = None
            array = None
            try:
                arrayId = master.createAutomaticArray(\
                   master.getAvailableAntennas(),
                   master.getAvailablePhotonicReferences(),
                   Control.BL);
                array = client.getComponent(arrayId.arrayComponentName)
                self.failUnless(array != None,
                                "Unable to create an automatic array")
                sfi = array.getSFIOffshoot()
                self.failUnless(sfi != None,
                                "Unable to create a single-field interferometry observing mode")
                amc = sfi.getArrayMountController()
                self.failUnless(amc != None,
                                "Unable to create an array mount controller")
            finally:
                if (array != None): client.releaseComponent(arrayId.arrayComponentName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ObservingModeExceptions.ObservingModeErrorEx,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx,
                ControlExceptions.IllegalParameterErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        try:
            arrayId = None
            array = None
            try:
                amc = None
                arrayId = master.createAutomaticArray(\
                   master.getAvailableAntennas(),
                   master.getAvailablePhotonicReferences(),
                   Control.BL);
                array = client.getComponent(arrayId.arrayComponentName)
                self.failUnless(array != None,
                                "Unable to create a manual array")
                sfi = array.getSFIOffshoot()
                self.failUnless(sfi != None,
                                "Unable to create a single-field interferometry observing mode")
                amc = sfi.getArrayMountController()
                self.failUnless(amc != None,
                                "Unable to create an array mount controller")
                # Start testing the IDL interface here.

                # test getMountController
                self.assertEqual(amc.getMountController("DA41"), \
                                 "CONTROL/DA41/MountController");

                # test setAzEl, setAzElAsync and waitUntilOnSource
                amc.setAzElAsync(math.radians(20), math.radians(40))
                amc.waitUntilOnSource()
                amc.setAzEl(math.radians(10), math.radians(20))

                # test setDirection, setDirectionAsync,
                # isObservableEquatorial & setObservingFrequency
                amc.setDirectionAsync(math.radians(0), math.radians(-80), \
                                      0.0, 0.0, 0.0)
                amc.waitUntilOnSource()
                self.assertTrue(amc.isObservableEquatorial(math.radians(10),
                                                   math.radians(-80), 0.0, \
                                                   0.0, 0.0, 0.0))
                amc.setObservingFrequency(100E9);
                amc.setDirection(math.radians(10), math.radians(-80), \
                                 0.0, 0.0, 0.0)
        
                # test startPointingDataCollection, getPointingDataTable &
                # abortDataCollection
                timeNow = \
                        Acspy.Common.TimeHelper.TimeUtil().py2epoch(time.time());
                timePlus5Sec = TETimeUtil.plusTE(timeNow, 21*5).value;
                timePlus15Sec = TETimeUtil.plusTE(timeNow, 21*15).value;
                to = amc.startPointingDataCollection(timePlus5Sec, timePlus15Sec)
                pd = amc.getPointingDataTable(to);
                numAnt = len(array.getAntennas());
                self.assertEqual(len(pd), numAnt)
                for i in range(numAnt):
                    self.assertEqual(len(pd[i].data), 210)
                amc.abortDataCollection();

                # test setPlanet, setPlanetAsync, isObservablePlanet
                planets = ['mercury', 'venus', 'mars', 'jupiter', 'saturn', \
                           'uranus', 'neptune', 'pluto', 'sun', 'moon']
                i = 0
                while (i < len(planets) and
                       not amc.isObservablePlanet(planets[i], 60.0)):
                    i = i + 1;
                if (i < len(planets)):
                    amc.setPlanetAsync(planets[i]);
                    amc.waitUntilOnSource()
                    amc.setPlanet(planets[i]);

                # tests setHorizonOffset and setHorizonOffsetAsync
                amc.setHorizonOffsetAsync(math.radians(-1), math.radians(2));
                amc.waitUntilOnSource()
                amc.setHorizonOffset(math.radians(2), math.radians(-1));

                # test maintenanceStow, survivalStow
#                 amc.maintenanceStow();
#                 amc.survivalStow();
            finally:
                # test stop
                if (amc != None):
                    amc.stop();
                if (array != None): client.releaseComponent(arrayId.arrayComponentName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ObservingModeExceptions.ObservingModeErrorEx,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx,
                ControlExceptions.IllegalParameterErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise
        
    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        pass
        try:
            arrayId = None
            array = None
            sfi = None
            amc = None
            try:
                arrayId = master.createAutomaticArray(\
                   master.getAvailableAntennas(),
                   master.getAvailablePhotonicReferences(),
                   Control.BL);
                array = CCL.Array.Array(arrayId.arrayName)
                self.failUnless(array != None,
                                "Unable to create a manual array")
                sfi = array.getSingleFieldInterferometryObservingMode()
                self.failUnless(sfi != None,
                                "Unable to create a single-field interferometry observing mode")
                amc = sfi.getArrayMountController()
                self.failUnless(amc != None,
                                "Unable to create an array mount controller")

                # Start testing the CCL interface here.

                # test setAzEl, setAzElAsync, setSourceAsync(Az,El)
                # and waitUntilOnSource
                amc.setAzElAsync("20deg", "40deg")
                amc.waitUntilOnSource()
                amc.setAzEl("10deg", "20deg")
                ssfs = CCL.FieldSource.HorizonSource("-20deg", "10deg");
                amc.setSourceAsync(ssfs)
                amc.waitUntilOnSource()
        
                # test setDirection, setDirectionAsync, isObservableEq &
                # setObservingFrequency and setSource(RA,Dec)
                amc.setDirectionAsync("0deg", "-80deg");
                amc.waitUntilOnSource()
                self.assertTrue(amc.isObservableEquatorial("10deg", "-80deg"))
                amc.setObservingFrequency("100GHz")
                amc.setDirection("10deg", "-80deg");
                ssfs = CCL.FieldSource.EquatorialSource("11:0:0", "-80.0.0");
                amc.setSource(ssfs)
  
                # test startPointingDataCollection, getPointingDataTable &
                # abortDataCollection
                timeNow =  \
                        Acspy.Common.TimeHelper.TimeUtil().py2epoch(time.time());
                timePlus5Sec = TETimeUtil.plusTE(timeNow, 21*5).value;
                timePlus15Sec = TETimeUtil.plusTE(timeNow, 21*15).value;
                to = amc.startPointingDataCollection(timePlus5Sec, timePlus15Sec)
                pd = amc.getPointingDataTable(to);
                numAnt = len(array.antennas());
                self.assertEqual(len(pd), numAnt)
                for i in range(numAnt):
                    self.assertEqual(len(pd[i].data), 210)
                amc.abortDataCollection();

                # test setPlanet, setPlanetAsync, setSource(planet) &
                # isObservablePlanet
                planets = ['jupiter', 'mercury', 'venus', 'mars', 'saturn', \
                           'uranus', 'neptune', 'pluto', 'sun', 'moon']
                i = 0
                while (i < len(planets)) and \
                       not amc.isObservablePlanet(planets[i], "1min"):
                    i = i + 1;
                if (i < len(planets)):
                    amc.setPlanetAsync(planets[i]);
                    amc.setPlanet(planets[i]);
                    ssfs = CCL.FieldSource.PlanetSource(planets[i]);
                    amc.setSource(ssfs);
                # tests setHorizonOffset and setHorizonOffsetAsync
                amc.setHorizonOffsetAsync("-1deg", "2deg");
                amc.waitUntilOnSource()
                amc.setHorizonOffset("2deg", "-1deg");

                # test setSource{,Async)(Ephemeris) &
                # isObservable(Ephemeris)
                # TODO. test setEphemeris, setEphemerisAsync &
                # isObservableEphemeris
                moons = ['io', 'europa', 'ganymede', 'callisto',
                         'pallas', 'ceres', 'titan', 'juno', 'vesta']
                i = 0; foundSource = False;
                while (i < len(moons) and not foundSource):
                    mySource = CCL.FieldSource.PlanetSource(moons[i])
                    if amc.isObservable(mySource, '10min'):
                        foundSource = True;
                    else:
                        i += 1;
                if (foundSource):
                    amc.setSourceAsync(mySource);
                    amc.setSource(mySource);
                    # TODO Write a getEphemeris function to extract an
                    # ephemeris from a SubscanFieldSource
#                     eph = getEphemeris(moons[i])
#                     amc.setEphemerisAsync(eph);
#                     amc.setEphemeris(eph);
#                     self.assertTrue(amc.isObservableEphemeris(eph))
                    # tests setHorizonOffset and setHorizonOffsetAsync
                    amc.setHorizonOffsetAsync("-1deg", "2deg");
                    amc.waitUntilOnSource()
                    amc.setHorizonOffset("2deg", "-1deg");
                    
                    
                # Test getMountController getPointingModels, zeroAuxPointingModel, 
                # updateAuxPointingModel & setAuxPointingModel
                amc.getMountController("DA41").getMount().enablePointingModel(True);
                amc.getMountController("DV02").getMount().enablePointingModel(True);
                amc.getMountController("PM01").getMount().enablePointingModel(True);
                amc.zeroAuxPointingModel();
                pms = amc.getPointingModels();
                self.assertEqual(len(pms), 3)
                self.assertTrue(pms[0].antennaName != '')
                self.assertTrue(pms[1].antennaName != '')
                self.assertTrue(pms[2].antennaName != '')
                self.assertTrue(len(pms[0].auxModel) == 0)
                self.assertTrue(len(pms[1].auxModel) == 0)
                self.assertTrue(len(pms[2].auxModel) == 0)
                pm = pms[0].primaryModel
                pm1 = Control.ArrayMountController.AntennaPointingModel('DA41', pm)
                pm2 = Control.ArrayMountController.AntennaPointingModel('DV02', pm)
                pm3 = Control.ArrayMountController.AntennaPointingModel('PM01', pm)
                amc.updateAuxPointingModel([pm1, pm2, pm3]);
                pms = amc.getPointingModels();
                self.assertEqual(len(pms), 3)
                self.assertTrue(len(pms[0].auxModel) != 0)
                self.assertTrue(len(pms[1].auxModel) != 0)
                self.assertTrue(len(pms[2].auxModel) != 0)
                amc.setAuxPointingModel([pm2]);
                pms = amc.getPointingModels();
                self.assertEqual(len(pms), 3)
                self.assertTrue((len(pms[0].auxModel) == 0) or \
                                (len(pms[1].auxModel) == 0) or \
                                (len(pms[2].auxModel) == 0))

                # test survivalStow maintenanceStow
#                amc.maintenanceStow();
#                amc.survivalStow();
            finally:
                # test stop
                if (amc != None):
                    amc.stop();
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ObservingModeExceptions.ObservingModeErrorEx,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx,
                ControlExceptions.IllegalParameterErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
        
# Bring the control subsystem operational
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    master = client.getComponent('CONTROL/MASTER')
    shutdownMasterAtEnd = False;
    if (master.getMasterState() == Control.INACCESSIBLE):
        master.startupPass1()
        master.startupPass2()
        shutdownMasterAtEnd = True
    if (master.getMasterState() != Control.OPERATIONAL):
        print 'Could not bring the control subsystem to an operational state'
        exit(1)

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

# Shutdown the control system is we started it up
    if shutdownMasterAtEnd:
        master.shutdownPass1()
        master.shutdownPass2()
    client.releaseComponent(master._get_name()) 
    
