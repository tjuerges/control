#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#


#import Acspy.Util.XmlObjectifier
import CCL.FieldSource
import ControlExceptionsImpl
## import CCL.SIConverter

class ArrayMountControllerSimulator:
    def __init__(self, array):
        self._array = array
        self._sourceList = []
        self._elevationLimit =  0.034906585039886591 # 2 Degrees
        
    def setSource(self, source):
        self.setSourceAsync(source)
        
    def setSourceAsync(self, source):
##         if (isinstance(source, Acspy.Util.XmlObjectifier.XmlObject) and
##             len(source.getElementsByTagName('FieldSource')) == 1):
##             source = CCL.Source.SubscanFieldSource(source)
        # Store the source Name and any offsets
        if not isinstance(source, CCL.APDMSchedBlock.FieldSource):
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl();
            msg = 'Source parameter must be a CCL.Source object';
            ex.addData('Detail', msg)
            ex.log(self._logger)
            raise ex

 ##        try:
##             stroke = source.getOffset()
##         except AttributeError:
##            stroke = (0,0,0,0)
        self._array.addEvent("Source has been set to: %s." %
                             source.sourceName )

        #self._sourceList.append([self._array.getElapsedTime(),
        #                         source.sourceName, stroke])
                                  
    def isObservable(self, source, duration=0.0):
        # For simulation purposes just return True
        return True

    def waitUntilOnSource(self):
        #TODO: Make this more sophisticated
        pass

    def _getAMCSource(self, startTime):
        # This method finds the name of the source that was observed at
        # the specified start time.
        targetName = "None"
        for idx in range(len(self._sourceList)):
            if self._sourceList[idx][0] <= startTime:
                targetName = self._sourceList[idx][1]
        return targetName
                
    def _getAMCStroke(self, startTime):
        # This method finds the name of the source that was observed at
        # the specified start time.
        stroke = (0.0, 0.0, 0.0, 0.0)
        for idx in range(len(self._sourceList)):
            if self._sourceList[idx][0] <= startTime:
               stroke = self._sourceList[idx][2]
        return stroke

    def applyPointingCalibrationResult(self, result, polarization,
                                        antennas=[]):
         self._addExecutionLine("+ Pointing Calibration Applied")

    def applyFocusCalibrationResult(self, result, polarization, antennas=[]):
        self._addExecutionLine("+ Focus Calibration Applied")
         
    def setElevationLimit(self, elevationLimit):
        self._elevationLimit = elevationLimit

    def getElevationLimit(self):
        return self._elevationLimit

    def resetLimits(self):
        self._elevationLimit = 0.034906585039886591


