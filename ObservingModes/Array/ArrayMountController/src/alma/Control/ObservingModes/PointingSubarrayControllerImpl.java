// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2010 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import alma.Control.AntennaPointingData;
import alma.Control.ArrayMountController;
import alma.Control.ArrayOffset;
import alma.Control.FocusCalResult;
import alma.Control.SourceOffset;
import alma.Control.PointingCalResult;
import alma.Control.PointingSubarrayControllerInterfaceOperations;
import alma.Control.SubscanBoundries;
import alma.ControlExceptions.DeviceBusyEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.PolarizationTypeMod.PolarizationType;
import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogger;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.Control.SourceOffsetFrame;

public class PointingSubarrayControllerImpl implements
    PointingSubarrayControllerInterfaceOperations {

    /* This is a map containing the Array Mount Controllers */
    private HashMap<String, ArrayMountControllerImpl>  amcMap;

    // This is a cached version of the container services and the
    // array which we will need to create ArrayMountControllers

    private ContainerServices   cs;
    private ObservingModeArray  array;
    private AcsLogger           logger;

    public PointingSubarrayControllerImpl(ContainerServices cs,
                                          ObservingModeArray array)
        throws ObsModeInitErrorEx {
        if (cs == null) {
            ObservingModeUtilities.throwObsModeInitErrorEx
                ("ContainerServices must not be null.",
                 new AcsJObsModeInitErrorEx(),
                 null);
        }

        this.cs     = cs;
        this.array  = array;
        this.logger = this.cs.getLogger();

        /* Start off with all of the antennas in the Default Array */
        amcMap = new HashMap<String, ArrayMountControllerImpl>();
        amcMap.put("Default", new ArrayMountControllerImpl(cs, array));

    }

    @Override
    public void createSubarray(String subarrayName,
                               String[] antennaList)
        throws ObsModeInitErrorEx, IllegalParameterErrorEx {
        /* First check the subarrayName */
        if (amcMap.containsKey(subarrayName)) {
            String msg = "The subarray name " + subarrayName + 
                " is alread in use";
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        
        if (antennaList.length == 0) {
            String msg = "You must specify at least one antenna to be in " + 
                "the new subarray";
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        
        Iterator<String> iter;

        /* Now make sure all of the antennas in antenna list are in the
           Default Array */
        Set<String> applicationSet = new HashSet<String>();
        for (String a : antennaList) applicationSet.add(a);
  
        Set<String> defaultSet = new HashSet<String>();
        for (String a : amcMap.get("Default").getAntennas()) defaultSet.add(a);

        if (!defaultSet.containsAll(applicationSet)) {
            String msg = "Unable to create pointing subarray, the " + 
                "following antennas are unavailable:";
            for (iter = applicationSet.iterator(); iter.hasNext();) {
                String antenna = iter.next();
                if (!defaultSet.contains(antenna)) {
                    msg += " " + antenna;
                }
            }
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }

        /* Ok now we know were good to go, create an empty 
           ArrayMountController */
        ArrayMountControllerImpl amc =
            new ArrayMountControllerImpl(cs, array, new String[0]);
        for (iter = applicationSet.iterator(); iter.hasNext();) {
            String antenna = iter.next();
            amc.addModeController(antenna, amcMap.get("Default").
                                  removeModeController(antenna));
        }
        amcMap.put(subarrayName, amc);
    }
    
    @Override
    public void destroySubarray(String subarrayName) 
        throws IllegalParameterErrorEx {

        if (subarrayName.equals("Default")) {
            String msg = "You are not allowed to destroy the default subarray";
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            jEx.log(logger);
            throw jEx.toIllegalParameterErrorEx();
        }
        
        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "The subarray " + subarrayName + "does not exist!";
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            jEx.log(logger);
            throw jEx.toIllegalParameterErrorEx();
        }
           
        /* Return the antennas to the default array */
        for (String a : amc.getAntennas())
            amcMap.get("Default").
                addModeController(a, amc.removeModeController(a));
        
        amcMap.remove(subarrayName);
    }
    

    public boolean subarrayExists(String subarrayName) {
        return amcMap.containsKey(subarrayName);
    }
    
    @Override
    public void setSource(String fieldSource)
        throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {
        FieldSourceT fs = ObservingModeUtilities.deserializeFieldSource(fieldSource, logger);
        setSource(fs);
    }

    public void setSource(FieldSourceT fieldSource)
            throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {
        setSourceAsync(fieldSource);
        waitUntilOnSource();
    }

    @Override
    public void setSubarraySource(String subarrayName,
            String fieldSource) throws ObservingModeErrorEx,
            TimeoutEx, IllegalParameterErrorEx {
        setSubarraySourceAsync(subarrayName, fieldSource);
        waitUntilSubarrayOnSource(subarrayName);
    }

    @Override
    public void setSourceAsync(String fieldSource)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {

        FieldSourceT fs = ObservingModeUtilities
                .deserializeFieldSource(fieldSource, logger);
        setSourceAsync(fs);
    }

    public void setSourceAsync(FieldSourceT fs)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {

        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().setSourceAsync(fs);
        }
    }

    @Override
    public void setSubarraySourceAsync(String subarrayName,
                                       String fieldSource) 
        throws ObservingModeErrorEx, IllegalParameterErrorEx {
   
        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "Unknown subarray name supplied: " + subarrayName;
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        
        amc.setSourceAsync(fieldSource);
    }

    @Override
    public boolean isObservable(String fieldSource, double duration)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {

        FieldSourceT fs =
            ObservingModeUtilities.deserializeFieldSource(fieldSource, logger);

        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            if (!iter.next().isObservable(fs, duration)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isObservableBySubarray(String subarrayName,
                                          String fieldSource, 
                                          double duration)
        throws IllegalParameterErrorEx, ObservingModeErrorEx {

        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "Unknown subarray name supplied: " + subarrayName;
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        return amc.isObservable(fieldSource, duration);
    }

    @Override
    public void waitUntilOnSource() throws ObservingModeErrorEx, TimeoutEx {
        /* To Do: Need to modify the timeout behavior here */
        
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().waitUntilOnSource();
        }
    }

    @Override
    public void waitUntilSubarrayOnSource(String subarrayName)
        throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {
        /* To Do: Need to modify the timeout behavior here */
        
        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "Unknown subarray name supplied: " + subarrayName;
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        amc.waitUntilOnSource();
    }

    // These are convience methods which just distribute the method over
    // all of the subarrays
    public void reportPointingModels() {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().reportPointingModels();
        }
    }

     public void reportFocusModels() {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().reportFocusModels();
        }
     }

     public void reportFocusPositions() {
         Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
         while (iter.hasNext()) {
            iter.next().reportFocusPositions();
        }
     }

    public void setObservingFrequency(double frequency)
        throws IllegalParameterErrorEx, ObservingModeErrorEx {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().setObservingFrequency(frequency);
        }
     }   

    public double timeToSource() 
        throws ObservingModeErrorEx, IllegalParameterErrorEx {
        double timeToSource = 0.0;

        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            timeToSource = Math.max(timeToSource, iter.next().timeToSource());
        }
        return timeToSource;
    }
    
    static public Map<String, SourceOffset> mergeOffsets(
            Map<String, SourceOffset> curOffset, ArrayOffset[] newOffset) {
        Map<String, SourceOffset> mergedOffset = new HashMap<String, SourceOffset>();
        for (String sa : curOffset.keySet()) {
            mergedOffset.put(sa, curOffset.get(sa));
        }
        for (ArrayOffset ao : newOffset) {
            mergedOffset.put(ao.name, ao.offset);
        }
        return mergedOffset;
    }

    public double timeToSlew(
            FieldSourceT fromSrc, Map<String, SourceOffset> fromOffset, 
            FieldSourceT toSrc, Map<String, SourceOffset> toOffset)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        double allSlewTime = 0.0;

        for (String sa : amcMap.keySet()) {
            SourceOffset saFromOffset;
            SourceOffset saToOffset;
            if (fromOffset.containsKey(sa)) {
                saFromOffset = fromOffset.get(sa);
            } else {
                saFromOffset = new SourceOffset(0, 0, 0, 0, SourceOffsetFrame.HORIZON);
            }
            if (toOffset.containsKey(sa)) {
                saToOffset = toOffset.get(sa);
            } else {
                saToOffset = saFromOffset;
            }
            final double saSlewTime = amcMap.get(sa).timeToSlew(fromSrc,
                    saFromOffset, toSrc, saToOffset);
            allSlewTime = Math.max(allSlewTime, saSlewTime);
        }
        return allSlewTime;
    }

    public double startPointingDataCollection(long startTime, long stopTime)
        throws ObservingModeErrorEx, DeviceBusyEx, IllegalParameterErrorEx {
        
        double maxDuration = 0.0;
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            maxDuration = Math.max(maxDuration, iter.next().
                     startPointingDataCollection(startTime, stopTime));
        }
        return maxDuration;
    }

    public AntennaPointingData[] getPointingDataTable(double timeout) {
        /* TODO: The timeout is not handled correctly here */
        ArrayList<AntennaPointingData> allPD = 
            new ArrayList<AntennaPointingData>();

        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            try {
                AntennaPointingData[] pdList =
                    iter.next().getPointingDataTable(timeout);
                for (AntennaPointingData pd : pdList ) {
                    allPD.add(pd);
                }
            } catch (ObservingModeErrorEx ex) {
                logger.warning("Error retreiving pointing data table from " + 
                               "subarray");
            } catch (TimeoutEx ex) {
                logger.warning("Timeout retreiving pointing data table " + 
                               "from subarray");
            } catch (IllegalParameterErrorEx ex) {
                logger.warning("Illegal paramter retreiving pointing data " +
                               "table from subarray");
            }
        }
        return allPD.toArray(new AntennaPointingData[0]);
    }

    /**
     * This method returns the "Default" Array Mount Controller 
     */
    @Override
    public ArrayMountController getArrayMountController() 
        throws ContainerServicesEx 
    {
        try {
            return getSubarrayMountController("Default");
        } catch (IllegalParameterErrorEx ex) {
            String msg = "Error Finding the Default Array Mode Controller.";
            AcsJContainerServicesEx jEx = new AcsJContainerServicesEx();
            jEx.setProperty("Message", msg);
            throw jEx.toContainerServicesEx();
        }
    }

    public ArrayMountControllerImpl getArrayMountControllerImpl()
            throws IllegalParameterErrorEx {
        return getSubarray("Default");
    }

    @Override
    public ArrayMountController getSubarrayMountController(String subarray) 
        throws ContainerServicesEx, IllegalParameterErrorEx {
        ArrayMountControllerImpl amc = getSubarray(subarray);
        return amc.getArrayMountController();
    } 
    
    public Set<String> getCurrentSubarrays() {
        return amcMap.keySet();
    }

    @Override
    public String[] getSubarrays() {
        Set<String> subarrays = getCurrentSubarrays();
        return subarrays.toArray(new String[0]);
    }
    
    public ArrayMountControllerImpl getSubarray(String subarrayName)
            throws IllegalParameterErrorEx {
        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "Unknown subarray called " + subarrayName;
            AcsJIllegalParameterErrorEx jex = new AcsJIllegalParameterErrorEx();
            ObservingModeUtilities.throwIllegalParameterErrorEx(msg, jex,
                    logger);
        }
        return amc;
    }

    public void cleanUp() {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().cleanUp();
        }
    }

    @Override
    public void applyPointingCalibrationResult(PointingCalResult[] result,
                PolarizationType polarization, String[] antennas)
        throws IllegalParameterErrorEx {

        Map<String, String[] > partition = partitionAntennas(antennas);

        Iterator<String> iter = partition.keySet().iterator();
        while (iter.hasNext()) {
            String subarrayName = iter.next();
            amcMap.get(subarrayName).applyPointingCalibrationResult
                (result, polarization, partition.get(subarrayName));
        }
    }


    @Override
        public void applyFocusCalibrationResult(FocusCalResult[] result,
                                                PolarizationType polarization,
                                                String[] antennas)
        throws IllegalParameterErrorEx {
        Map<String, String[] > partition = partitionAntennas(antennas);

        Iterator<String> iter = partition.keySet().iterator();
        while (iter.hasNext()) {
            String subarrayName = iter.next();
            amcMap.get(subarrayName).applyFocusCalibrationResult
                (result, polarization, partition.get(subarrayName));
        }
    }
    
    @Override
    public void setElevationLimit(double limit) 
        throws ObservingModeErrorEx, IllegalParameterErrorEx {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().setElevationLimit(limit);
        }
    }

    @Override
    public void setSubarrayElevationLimit(String subarrayName, double limit) 
        throws ObservingModeErrorEx, IllegalParameterErrorEx {
        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "Unknown subarray name supplied: " + subarrayName;
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        amc.setElevationLimit(limit);
    }

    @Override
    public double getElevationLimit() 
        throws ObservingModeErrorEx {
        // Main loop that sends the command to each antenna
        double maxLimit = 0.0;
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            maxLimit = Math.max(maxLimit, iter.next().getElevationLimit());
        }
        return maxLimit;
    }
    
    @Override
    public double getSubarrayElevationLimit(String subarrayName)
        throws ObservingModeErrorEx, IllegalParameterErrorEx {
        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "Unknown subarray name supplied: " + subarrayName;
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        return amc.getElevationLimit();
    }

    @Override
    public void resetLimits() {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().resetLimits();
        }
    }

    @Override
    public void resetSubarrayLimits(String subarrayName) 
    throws IllegalParameterErrorEx {
        ArrayMountControllerImpl amc = amcMap.get(subarrayName);
        if (amc == null) {
            String msg = "Unknown subarray name supplied: " + subarrayName;
            AcsJIllegalParameterErrorEx jEx =new AcsJIllegalParameterErrorEx();
            jEx.setProperty("Message", msg);
            throw jEx.toIllegalParameterErrorEx();
        }
        amc.resetLimits();
    }
    /* This class takes an array of antennas and splits them so that
       they are listed in the returned Map based on the pointing subarray
       that they are part of. 
       
       If any antenna in the array is not part of an pointing subarray an
       IllegalParameterErrorEx is thrown.  If the list of antennas is
       of zero length, then this returns all antennas associated with each
       subarray
    */
    private Map<String, String[] > partitionAntennas(String[] antennas)
        throws IllegalParameterErrorEx {
        // Put the antennas parameter in a more confortable container,
        // consider the case of zero antennas, in which case all antennas
        // should be used
        
        Set<String> applicationSet = null;
        if (antennas.length > 0) {
            applicationSet = new HashSet<String>();
            for (String a : antennas)
                applicationSet.add(a);
        }

        // Get the list of antennas from each antenna mode controller 
        HashMap<String, String[] > antennaSets = new HashMap<String, String[]>();
        Iterator<String> iter;
        for (iter = amcMap.keySet().iterator(); iter.hasNext();) {
            String subarrayName = iter.next();
            Set<String> arrayAnts = new HashSet<String>();
            for (String antenna : amcMap.get(subarrayName).getAntennas()) {
                arrayAnts.add(antenna);
            }

            if (applicationSet != null) {
                /* Ok now keep in arrayAnts the list of those antennas which
                   are in the application list
                */
                arrayAnts.retainAll(applicationSet);

                /* And remove from the application set those that are in the
                   array
                */
                applicationSet.removeAll(arrayAnts);
            }

            if (arrayAnts.size() > 0) {
                antennaSets.put(subarrayName, 
                                applicationSet.toArray(new String[0]));
            }
        }
    
        /* All of the antennas should have been removed from the list */
        if (applicationSet != null && applicationSet.size() > 0) {
            AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
            String msg = "Failed to partition antennas, the following "
                + "antennas do not belong to any subarray:";
            for (iter = applicationSet.iterator(); iter.hasNext();) {
                msg += " " + iter.next();
            }
            ex.setProperty("Message", msg);
            throw ex.toIllegalParameterErrorEx();
        }
        return antennaSets;
    }


    /*   ---------------- Observing Cycle Methods ----------------- */

    /**
     * This method just delegates to the underlying Array Mount Controller see
     * ArrayMountControllerImpl.java for a description.
     * @param boundries
     * @throws ObservingModeErrorEx
     */
    void startSubscanSequence(SubscanBoundries[] boundries)
            throws ObservingModeErrorEx {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().startSubscanSequence(boundries);
        }
    }
    
    /**
     * @param boundries
     * @param offsets
     * @throws ObservingModeErrorEx
     * @throws IllegalParameterErrorEx
     */
    void beginSubscanSequence(SubscanBoundries[] boundries,
            ArrayList<Map<String, SourceOffset>> offsets)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        for (String sa : amcMap.keySet()) {
            SourceOffset[] offsetList = new SourceOffset[offsets.size()];
            SourceOffset curOffset = new SourceOffset(0.0, 0.0, 0.0, 0.0, SourceOffsetFrame.HORIZON);
            for (int ss = 0; ss < offsets.size(); ss++) {
                if (offsets.get(ss).containsKey(sa)) {
                    curOffset = offsets.get(ss).get(sa);
                }
                offsetList[ss] = curOffset;
            }
            amcMap.get(sa).beginSubscanSequence(boundries, offsetList);
        }
    }

    /**
     * This method just delegates to the underlying Array Mount Controller see
     * ArrayMountControllerImpl.java for a description.
     * @throws ObservingModeErrorEx
     */
    void waitForSubscanStart() throws ObservingModeErrorEx {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().waitForSubscanStart();
        }
    }

    /**
     * This method just delegates to the underlying Array Mount Controller see
     * ArrayMountControllerImpl.java for a description.
     * @throws ObservingModeErrorEx
     * @throws DataCapturerErrorEx
     */
    void waitForSubscanEnd() throws ObservingModeErrorEx, DataCapturerErrorEx {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().waitForSubscanEnd();
        }
    }

    /**
     * This method just delegates to the underlying Array Mount Controller
     * see ArrayMountControllerImpl.java for a description.
     * @param startTime long
     * @param stopTime long
     * @throws ObservingModeErrorEx 
     */
    void beginSubscan(long startTime, long stopTime) 
        throws ObservingModeErrorEx {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().beginSubscan(startTime, stopTime);
        }
    }

    /**
     * This method just delegates to the underlying Array Mount Controller
     * see ArrayMountControllerImpl.java for a description.
     * @throws ObservingModeErrorEx
     * @throws DataCapturerErrorEx
     */
    void endSubscan() 
        throws ObservingModeErrorEx, DataCapturerErrorEx {
        Iterator<ArrayMountControllerImpl> iter = amcMap.values().iterator();
        while (iter.hasNext()) {
            iter.next().endSubscan();
        }
    }
}
