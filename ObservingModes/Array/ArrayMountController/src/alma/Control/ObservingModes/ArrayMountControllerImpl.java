// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.omg.CORBA.DoubleHolder;
import org.omg.CORBA.LongHolder;

import alma.ACSErr.Completion;
import alma.ACSErr.ErrorTrace;
import alma.Control.AntennaPointingData;
import alma.Control.ArrayMountController;
import alma.Control.ArrayMountControllerHelper;
import alma.Control.ArrayMountControllerOperations;
import alma.Control.ArrayMountControllerPOATie;
import alma.Control.EphemerisRow;
import alma.Control.EquatorialDirection;
import alma.Control.FocusCalResult;
import alma.Control.HorizonDirection;
import alma.Control.MountController;
import alma.Control.NamedHorizonDirection;
import alma.Control.PointingCalResult;
import alma.Control.SourceOffset;
import alma.Control.SourceOffsetFrame;
import alma.Control.SubscanBoundries;
import alma.Control.ArrayMountControllerPackage.AntennaPointingModel;
import alma.Control.ArrayMountControllerPackage.BothAntennaPointingModels;
import alma.Control.Common.Util;
import alma.Control.MountControllerPackage.PointingData;
import alma.ControlExceptions.DeviceBusyEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJDeviceBusyEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.MountFaultEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJDataCapturerErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.PolarizationTypeMod.PolarizationType;
import alma.ReceiverBandMod.ReceiverBand;
import alma.TMCDB.ModelTerm;
import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogLevel;
import alma.acs.logging.AcsLogger;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.types.FieldSourceTSolarSystemObjectType;
import alma.entity.xmlbinding.valuetypes.LatitudeT;
import alma.entity.xmlbinding.valuetypes.LongitudeT;
import alma.entity.xmlbinding.valuetypes.SkyCoordinatesT;
import alma.entity.xmlbinding.valuetypes.types.SkyCoordinatesTSystemType;
import alma.log_audience.DEVELOPER;
import alma.log_audience.OPERATOR;
import alma.offline.DataCapturer;

public class ArrayMountControllerImpl extends
        ObservingModeCore<MountController> implements
        ArrayMountControllerOperations {

    // These are the objects for making this class an offshoot
    private ArrayMountControllerPOATie tieBindingClass= null;
    private ArrayMountController       corbaOffshoot = null;

    // How long to wait for all the pointing data to be collected.
    // A value < 0 indicates that startPointingData has never been called
    private double pointingDataTimeout = -1.0;

    // A list of antennas that threw an exception when we told them to move.
    // These antennas will be skipped, to prevent long timeouts, when waiting
    // for the antennas to go on-source
    private ArrayList<String> badAntennas_m = new ArrayList<String>();
    
    // The following two fields are used when doing sequences of sub-scans.
    // These are the times of each sub-scan in a sequence. There are used for computing timeouts.
    SubscanBoundries[] subscanBoundries = null;
    // This where we are in a sub-scan sequence. Its reset to zero in
    // startSubcanSequence and incremented, or unset, in waitForSubscanEnded. A
    // negative value means we have not started or completed the sequence;
    int currentSubscanSequence = -1;

    public ArrayMountControllerImpl(ContainerServices cs,
                                    ObservingModeArray array)
        throws ObsModeInitErrorEx {
        super(cs, array);
        createAntModeControllers("alma.Control.MountControllerHelper",
                                 "MountController");
        reportPointingModels();
        reportFocusModels();
    }

    // A constructor that allows to specify the AntennaList,
    // so an ArrayMountController can be created only with a subset of
    // the array antennas.
    public ArrayMountControllerImpl(ContainerServices cs,
                                    ObservingModeArray array,
                                    String[]           antennaList)
        throws ObsModeInitErrorEx {
        super(cs, array);
        createAntModeControllers(antennaList,
                                 "alma.Control.MountControllerHelper",
                                 "MountController");
        reportPointingModels();
        reportFocusModels();
    }
    
    @Override
    public void setSource(String fieldSource) throws ObservingModeErrorEx,
            TimeoutEx, IllegalParameterErrorEx {
        setSourceAsync(fieldSource);
        waitUntilOnSource();
    }

    @Override
    public void setSourceAsync(String fieldSource)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        FieldSourceT fs = 
            ObservingModeUtilities.deserializeFieldSource(fieldSource, logger);
        setSourceAsync(fs);
    }

    @Override
    public boolean isObservable(String fieldSource, double duration)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        FieldSourceT fs =
            ObservingModeUtilities.deserializeFieldSource(fieldSource, logger);
        return isObservable(fs, duration);
    }

    protected void setSource(FieldSourceT fieldSource)
            throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {
        setSourceAsync(fieldSource);
        waitUntilOnSource();
    }
    
    protected void setSourceAsync(FieldSourceT fs)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        String msg;
        if (fs.getNonSiderealMotion() == true) {
            FieldSourceTSolarSystemObjectType planet = fs.getSolarSystemObject();
            if (planet == FieldSourceTSolarSystemObjectType.EPHEMERIS) {
                EphemerisRow[] ephemeris = ObservingModeUtilities.ephemerisToIDL(fs.getSourceEphemeris(), logger);
                setEphemerisAsync(ephemeris);
            } else {
                setPlanetAsync(planet.toString());
            }
        } else {
            final SkyCoordinatesT coords = fs.getSourceCoordinates();
            final SkyCoordinatesTSystemType system = coords.getSystem();
            if (system == SkyCoordinatesTSystemType.J2000) {
                final double ra = ObservingModeUtilities.convertAngleToRadians(coords.getLongitude());
                final double dec = ObservingModeUtilities.convertAngleToRadians(coords.getLatitude());
                // TODO. Handle proper-motion etc.
                setDirectionAsync(ra, dec, 0.0, 0.0, 0.0);
            } else if (system == SkyCoordinatesTSystemType.AZEL) {
                final double az = coords.getLongitude().getContent();
                final double el = coords.getLatitude().getContent();
                setAzElAsync(az, el);
            } else {
                msg = "The '" + system
                        + "' coordinate system is not supported.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
            }
        }
    }

    protected boolean isObservable(FieldSourceT fs,
            double duration) throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        if (fs.getNonSiderealMotion() == true) {
            FieldSourceTSolarSystemObjectType planet = fs.getSolarSystemObject();
            if (planet == FieldSourceTSolarSystemObjectType.EPHEMERIS) {
                EphemerisRow[] ephemeris = ObservingModeUtilities.ephemerisToIDL(fs.getSourceEphemeris(), logger);
                return isObservableEphemeris(ephemeris, duration);
            } else {
                return isObservablePlanet(planet.toString(), duration);
            }
        } else {
            final SkyCoordinatesT coords = fs.getSourceCoordinates();
            final SkyCoordinatesTSystemType system = coords.getSystem();
            if (system == SkyCoordinatesTSystemType.J2000) {
                final double ra = ObservingModeUtilities.convertAngleToRadians(coords.getLongitude());
                final double dec = ObservingModeUtilities.convertAngleToRadians(coords.getLatitude());
                // TODO. Handle proper-motion etc.
                return isObservableEquatorial(ra, dec, duration, 0, 0, 0);
            } else if (system == SkyCoordinatesTSystemType.AZEL) {
                final double az = coords.getLongitude().getContent();
                final double el = coords.getLatitude().getContent();
                // TODO: This is still a bit clugely, we should be able to do
                // better
                return ((Math.abs(az) < Math.PI * 270 / 180)
                        && (el >= Math.PI * 2 / 180) && (el <= Math.PI * 88.9 / 180));
            } else {
                msg = "The '" + system
                        + "' coordinate system is not supported.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
            }
        }
        return false; // Just to make the compiler happy
    }

    @Override
    public void setAzEl(double az, double el) throws ObservingModeErrorEx, TimeoutEx,
            IllegalParameterErrorEx {
        setAzElAsync(az, el);
        waitUntilOnSource();
    }

    @Override
    public void setAzElAsync(double az, double el) throws ObservingModeErrorEx,
            IllegalParameterErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that sends the command to each antenna
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            final String antName = iter.next();
            try {
                antennaModeController.get(antName).setAzElAsync(az, el);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    @Override
    public void waitUntilOnSource() throws ObservingModeErrorEx, TimeoutEx {
        AntModeControllerCBImpl cbObj = createCallback();
        
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            // Only consider antennas that did not throw any
            // exceptions the last time we commanded them
            if (!badAntennas_m.contains(antName)) {
                MountController mc = antennaModeController.get(antName);
                cbObj.addExpectedResponse(antName);
                // This returns immediately (as its a one-way function).
                // Its below that we wait for all the call-backs to
                //complete.
                mc.waitUntilOnSourceCB(cbObj.getExternalInterface());
                    }
            }   
        
        // Now wait for the antennas to get on source
        // TODO. Make the timeout a floating point number.
        HashMap<String, Completion> completions = 
            cbObj.waitForCompletion((long) timeout());
        // If there are enough bad completions this may throw an exception.
        checkCompletions(completions, "waitUntilOnSource");
    }
    
    @Override
    public double timeToSource() throws ObservingModeErrorEx, IllegalParameterErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that gets the time for each antenna.
        double maxSlewTime = 0.0;
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            final String antName = iter.next();
            try {
                maxSlewTime = Math.max(maxSlewTime, antennaModeController.get(antName).timeToSource());
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
        return maxSlewTime;
    }
    
    @Override
    public double timeToSlew(String fromEnt, String toEnt)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        FieldSourceT from = ObservingModeUtilities
                .deserializeFieldSource(fromEnt, logger);
        FieldSourceT to = ObservingModeUtilities
                .deserializeFieldSource(toEnt, logger);
        return timeToSlew(from, to);
    }

    public double timeToSlew(FieldSourceT from, FieldSourceT to)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // First work out the azimuth and elevation for each antenna. These
        // numbers should differ only slightly because all the antennas are
        // fairly close together. A quicker approximation is to do this
        // computation for just one antenna.
        NamedHorizonDirection[] fromDir = toAzEl(from);
        NamedHorizonDirection[] toDir = toAzEl(to);

        // Main loop that gets the time for each antenna. This must be done for
        // each antenna as different antennas may have different slew speeds.
        double maxSlewTime = 0.0;
        Iterator<String> iter = antennaModeController.keySet().iterator();
        final int numAnts = antennaModeController.keySet().size();
        for (int a = 0; a < numAnts; a++) {
            final String antName = iter.next();
            try {
                MountController mc = antennaModeController.get(antName);
                // TODO. Check that antName & toDir[a].antenna &
                // fromDir[a].antenna all match
                HorizonDirection thisToDir = toDir[a].value;
                HorizonDirection thisFromDir = fromDir[a].value;
                final double thisSlewTime = mc.timeToSlew(thisFromDir.az,
                        thisFromDir.el, thisToDir.az, thisToDir.el);
                maxSlewTime = Math.max(maxSlewTime, thisSlewTime);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
        return maxSlewTime;
    }
    
    public double timeToSlew(FieldSourceT fromSrc, SourceOffset fromOffset, 
                FieldSourceT toSrc, SourceOffset toOffset)
    throws ObservingModeErrorEx, IllegalParameterErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // First work out the azimuth and elevation for each antenna. These
        // numbers should differ only slightly because all the antennas are
        // fairly close together. A quicker approximation is to do this
        // computation for just one antenna.
        NamedHorizonDirection[] fromDir = toAzEl(fromSrc);
        NamedHorizonDirection[] toDir = toAzEl(toSrc);

        // Now add the offsets for the to and from directions. This
        // implementation makes a couple of approximations.
        // 1. Thats the offsets are in horizon coordinates 
        // 2. That they are small.
        // TODO. Remove these approximations.
        for (int a = 0; a < fromDir.length; a++) {
            fromDir[a].value.el += fromOffset.latitudeOffset;
            fromDir[a].value.az += fromOffset.longitudeOffset/Math.cos(fromDir[a].value.el);
        }
        for (int a = 0; a < toDir.length; a++) {
            toDir[a].value.el += toOffset.latitudeOffset;
            toDir[a].value.az += toOffset.longitudeOffset/Math.cos(toDir[a].value.el);
        }

        // Main loop that gets computed for each antenna. This must be done for
        // each antenna as different antennas may have different slew speeds.
        double maxSlewTime = 0.0;
        Iterator<String> iter = antennaModeController.keySet().iterator();
        final int numAnts = antennaModeController.keySet().size();
        for (int a = 0; a < numAnts; a++) {
            final String antName = iter.next();
            try {
                MountController mc = antennaModeController.get(antName);
                // TODO. Check that antName & toDir[a].antenna &
                // fromDir[a].antenna all match
                HorizonDirection thisToDir = toDir[a].value;
                HorizonDirection thisFromDir = fromDir[a].value;
                final double thisSlewTime = mc.timeToSlew(thisFromDir.az,
                        thisFromDir.el, thisToDir.az, thisToDir.el);
                maxSlewTime = Math.max(maxSlewTime, thisSlewTime);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
        return maxSlewTime;
    }

    @Override
    public NamedHorizonDirection[] toAzEl(String src)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        FieldSourceT fs = ObservingModeUtilities
                .deserializeFieldSource(src, logger);
        return toAzEl(fs);
    }
    
    public NamedHorizonDirection[] toAzEl(FieldSourceT fs)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        if (fs.getNonSiderealMotion() == true) {
            FieldSourceTSolarSystemObjectType planet = fs
                    .getSolarSystemObject();
            if (planet == FieldSourceTSolarSystemObjectType.EPHEMERIS) {
                EphemerisRow[] ephemeris = ObservingModeUtilities
                        .ephemerisToIDL(fs.getSourceEphemeris(), logger);
                return toAzElEphemeris(ephemeris);
            } else {
                return toAzElPlanet(planet.toString());
            }
        } else {
            final SkyCoordinatesT coords = fs.getSourceCoordinates();
            final SkyCoordinatesTSystemType system = coords.getSystem();
            if (system == SkyCoordinatesTSystemType.J2000) {
                final double ra = ObservingModeUtilities
                        .convertAngleToRadians(coords.getLongitude());
                final double dec = ObservingModeUtilities
                        .convertAngleToRadians(coords.getLatitude());
                // TODO. Handle proper-motion etc.
                return toAzElEquatorial(ra, dec, 0, 0, 0);
            } else if (system == SkyCoordinatesTSystemType.AZEL) {
                final double az = coords.getLongitude().getContent();
                final double el = coords.getLatitude().getContent();
                final HorizonDirection thisAzEl = new HorizonDirection(az, el);
                Iterator<String> iter = antennaModeController.keySet()
                        .iterator();
                final int numAnts = antennaModeController.keySet().size();
                NamedHorizonDirection[] azEl = new NamedHorizonDirection[numAnts];
                for (int a = 0; a < numAnts; a++) {
                    final String antName = iter.next();
                    azEl[a] = new NamedHorizonDirection(antName, thisAzEl);
                }
                return azEl;
            } else {
                msg = "The '" + system
                        + "' coordinate system is not supported.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
                // We should never get here. So this is just to keep the
                // compiler happy.
                return new NamedHorizonDirection[0];
            }
        }
    }
    
    @Override
    public EquatorialDirection toRADec(String src)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        FieldSourceT fs = ObservingModeUtilities.deserializeFieldSource(src,
                logger);
        return toRADec(fs);
    }

    public void fillRADec(FieldSourceT src) throws IllegalParameterErrorEx,
            ObservingModeErrorEx {
        if (src.getNonSiderealMotion() == true) {
            EquatorialDirection radec = toRADec(src);
            SkyCoordinatesT coords = src.getSourceCoordinates();
            LongitudeT ra = new LongitudeT();
            ra.setUnit("rad");
            ra.setContent(radec.ra);
            coords.setLongitude(ra);
            LatitudeT dec = new LatitudeT();
            dec.setUnit("rad");
            dec.setContent(radec.dec);
            coords.setLatitude(dec);
        }
    }

    public EquatorialDirection toRADec(FieldSourceT fs)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        if (fs.getNonSiderealMotion() == true) {
            FieldSourceTSolarSystemObjectType planet = fs.getSolarSystemObject();
            if (planet == FieldSourceTSolarSystemObjectType.EPHEMERIS) {
                EphemerisRow[] ephemeris = ObservingModeUtilities.ephemerisToIDL(
                        fs.getSourceEphemeris(), logger);
                return toRADecEphemeris(ephemeris);
            } else {
                return toRADecPlanet(planet.toString());
            }
        } else {
            final SkyCoordinatesT coords = fs.getSourceCoordinates();
            final SkyCoordinatesTSystemType system = coords.getSystem();
            if (system == SkyCoordinatesTSystemType.J2000) {
                final double ra = ObservingModeUtilities.convertAngleToRadians(coords.getLongitude());
                final double dec = ObservingModeUtilities.convertAngleToRadians(coords.getLatitude());
                return new EquatorialDirection(ra, dec);
            } else {
                msg = "Conversions from the '" + system
                        + "' coordinate system to RA/Dec are not supported.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
                // We should never get here. So this is just to keep the
                // compiler happy.
                return new EquatorialDirection();
            }
        }
    }

    @Override
    public void stop() throws ObservingModeErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                antennaModeController.get(antName).stop();
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    @Override
    public void survivalStow() throws ObservingModeErrorEx, TimeoutEx {
        badAntennas_m.clear();

        AntModeControllerCBImpl cbObj = createCallback();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            antennaModeController.get(antName).survivalStowCB(
                    cbObj.getExternalInterface());
            cbObj.addExpectedResponse(antName);
        }

        // Now wait for the antennas to stow
        HashMap<String, Completion> completions = cbObj.waitForCompletion((long) timeout());
        // If there are enough bad completions this may throw an exception.
        checkCompletions(completions, "survivalStow");
    }

    @Override
    public void maintenanceStow() throws ObservingModeErrorEx, TimeoutEx {
        badAntennas_m.clear();
        AntModeControllerCBImpl cbObj = createCallback();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            antennaModeController.get(antName).maintenanceStowCB(
                    cbObj.getExternalInterface());
            cbObj.addExpectedResponse(antName);
        }

        // Now wait for the antennas to stow
        HashMap<String, Completion> completions = cbObj.waitForCompletion((long) timeout());
        // If there are enough bad completions this may throw an exception.
        checkCompletions(completions, "maintenanceStow");
    }

    @Override
    public void setDirection(double ra, double dec, double pmRA, double pmDec,
            double parallax) throws ObservingModeErrorEx, TimeoutEx,
            IllegalParameterErrorEx {
        setDirectionAsync(ra, dec, pmRA, pmDec, parallax);
        waitUntilOnSource();
    }

    @Override
    public void setDirectionAsync(double ra, double dec, double pmRA,
            double pmDec, double parallax) throws ObservingModeErrorEx,
            IllegalParameterErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                antennaModeController.get(antName).setDirectionAsync(ra, dec,
                        pmRA, pmDec, parallax);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    @Override
    public boolean isObservableEquatorial(double ra, double dec,
            double duration, double pmRA, double pmDec, double parallax)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // Main loop that sends the command to each antenna
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();
        
        Iterator<String> iter = antennaModeController.keySet().iterator();
        int numObservable = 0;
        String antName = "";
        while (iter.hasNext()) {
            antName = iter.next();
            try {// TODO. Do this all in parallel, with callbacks, as each
                // antenna takes a few tenth's of a second
                boolean observable = antennaModeController.get(antName).isObservableEquatorial(
                        ra, dec, duration, pmRA, pmDec, parallax);
                if (observable) {
                    numObservable++;
                }
            } catch (UnallocatedEx ex) { // skip this antenna
                unallocatedEx.put(antName, ex);
            } catch (MountFaultEx ex) {// skip this antenna
                mountFaultEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot observe the specified object as"
                        + " the parameters are incorrect. Assuming this is true"
                        + " for all antennas in this array.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(ex));
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);

        // Finally check if enough antennas can observe the source
        final int numAntennas = antennaModeController.size();
        if ((numAntennas - numObservable) <= maxNumBadAntennas()) {
            return true;
        } else {
            return false;
        }
    }
    
    public NamedHorizonDirection[] toAzElEquatorial(double ra, double dec,
            double pmRA, double pmDec, double parallax)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // Main loop that sends the command to each antenna
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();

        Iterator<String> iter = antennaModeController.keySet().iterator();
        final int numAnts = antennaModeController.size();

        NamedHorizonDirection[] azEl = new NamedHorizonDirection[numAnts];
        String antName = "";
        DoubleHolder az = new DoubleHolder();
        DoubleHolder el = new DoubleHolder();
        for (int a = 0; a < numAnts; a++) {
            antName = iter.next();
            try {// TODO. Do this all in parallel, with callbacks, as each
                // antenna takes a few tenth's of a second
                MountController mc = antennaModeController.get(antName);
                mc.toAzElEquatorial(ra, dec, pmRA, pmDec, parallax, az, el);
                HorizonDirection thisAzEl = new HorizonDirection(az.value,
                        el.value);
                azEl[a] = new NamedHorizonDirection(antName, thisAzEl);
            } catch (UnallocatedEx ex) { // skip this antenna
                unallocatedEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot observe the specified object as"
                        + " the parameters are incorrect. Assuming this is true"
                        + " for all antennas in this array.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(ex));
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        throwIfUnallocated(unallocatedEx, unallocatedEx.size());
        return azEl;
    }

    @Override
    public void setObservingFrequency(double frequency)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        String msg;

        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();
        badAntennas_m.clear();

        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            String antName = iter.next();
            try {// TODO. Do this all in parallel, with callbacks, as each
                // antenna takes a few tenth's of a second
                antennaModeController.get(antName).setObservingFrequency(
                        frequency);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot set the observing frequency"
                        + " (for the refraction correction and pointing/focus models)"
                        + " as the specified frequency is incorrect. Assuming"
                        + " this error is true for all antennas in this"
                        + " array and skipping any attempts to set the"
                        + " observing frequency on the remaining antennas.";
                throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx(ex));
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    @Override
    public void setPlanet(String planetName) throws ObservingModeErrorEx, TimeoutEx,
            IllegalParameterErrorEx {
        setPlanetAsync(planetName);
        waitUntilOnSource();
    }

    @Override
    public void setPlanetAsync(String planetName) throws ObservingModeErrorEx,
            IllegalParameterErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                antennaModeController.get(antName).setPlanetAsync(planetName);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    @Override
    public boolean isObservablePlanet(String planetName, double duration)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // TODO. factor out code that is common with the isObservable function

        // Main loop that sends the command to each antenna
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        Iterator<String> iter = antennaModeController.keySet().iterator();
        int numObservable = 0;
        String antName = "";
        while (iter.hasNext()) {
            antName = iter.next();
            try {// TODO. Do this all in parallel, with callbacks, as each
                // antenna takes a few tenth's of a second
                boolean observable = antennaModeController.get(antName).isObservablePlanet(
                        planetName, duration);
                if (observable) {
                    numObservable++;
                }
            } catch (UnallocatedEx ex) { // skip this antenna
                unallocatedEx.put(antName, ex);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot observe the specified object as"
                        + " the parameters are incorrect. Assuming this is true"
                        + " for all antennas in this array.";
                throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx(ex));
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);

        // Finally check if enough antennas can observe the source
        final int numAntennas = antennaModeController.size();
        if ((numAntennas - numObservable) <= maxNumBadAntennas()) {
            return true;
        } else {
            return false;
        }
    }

    public NamedHorizonDirection[] toAzElPlanet(String planetName)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // Main loop that sends the command to each antenna
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();

        Iterator<String> iter = antennaModeController.keySet().iterator();
        final int numAnts = antennaModeController.size();

        NamedHorizonDirection[] azEl = new NamedHorizonDirection[numAnts];
        String antName = "";
        DoubleHolder az = new DoubleHolder();
        DoubleHolder el = new DoubleHolder();
        for (int a = 0; a < numAnts; a++) {
            antName = iter.next();
            try {// TODO. Do this all in parallel, with callbacks, as each
                // antenna takes a few tenth's of a second
                MountController mc = antennaModeController.get(antName);
                mc.toAzElPlanet(planetName, az, el);
                HorizonDirection thisAzEl = new HorizonDirection(az.value,
                        el.value);
                azEl[a] = new NamedHorizonDirection(antName, thisAzEl);
            } catch (UnallocatedEx ex) { // skip this antenna
                unallocatedEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot observe the specified object as"
                        + " the parameters are incorrect. Assuming this is true"
                        + " for all antennas in this array.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(ex));
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        throwIfUnallocated(unallocatedEx, unallocatedEx.size());
        return azEl;
    }
    
    public EquatorialDirection toRADecPlanet(String planetName)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // Check we have at least one antenna
        Iterator<MountController> iter = antennaModeController.values().iterator();
        if (!iter.hasNext()) {
            msg = "The array does not have any antennas.";
            msg += " One is needed to compute the RA/Dec of the specified planet";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        MountController mc = iter.next();
        DoubleHolder ra = new DoubleHolder();
        DoubleHolder dec = new DoubleHolder();
        try {
            mc.toRADecPlanet(planetName, ra, dec);
        } catch (UnallocatedEx e) {
            msg = "The array does not have any operational antennas.";
            msg += " One is needed to compute the RA/Dec of the specified planet";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        EquatorialDirection raDec = new EquatorialDirection(ra.value, dec.value);
        return raDec;
    }

    @Override
    public void setEphemeris(EphemerisRow[] ephemeris) throws ObservingModeErrorEx,
            TimeoutEx, IllegalParameterErrorEx {
        setEphemerisAsync(ephemeris);
        waitUntilOnSource();
    }

    @Override
    public void setEphemerisAsync(EphemerisRow[] ephemeris)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that sends the command to each antenna
       for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                antennaModeController.get(antName).setEphemerisAsync(ephemeris);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }
    
    @Override
    public boolean isObservableEphemeris(EphemerisRow[] ephemeris,
            double duration) throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // TODO. factor out code that is common with the isObservable function

        // Main loop that sends the command to each antenna
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        Iterator<String> iter = antennaModeController.keySet().iterator();
        int numObservable = 0;
        String antName = "";
        while (iter.hasNext()) {
            antName = iter.next();
            try {// TODO. Do this all in parallel, with callbacks, as each
                // antenna takes a few tenth's of a second
                boolean observable = antennaModeController.get(antName).isObservableEphemeris(
                        ephemeris, duration);
                if (observable) {
                    numObservable++;
                }
            } catch (UnallocatedEx ex) {// skip this antenna
                unallocatedEx.put(antName, ex);
            } catch (MountFaultEx ex) {// skip this antenna
                mountFaultEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot observe the specified object as"
                        + " the parameters are incorrect. Assuming this is true"
                        + " for all antennas in this array.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(ex));
            }
        }
        
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);

        // Finally check if enough antennas can observe the source
        final int numAntennas = antennaModeController.size();
        if ((numAntennas - numObservable) <= maxNumBadAntennas()) {
            return true;
        } else {
            return false;
        }
    }

    public NamedHorizonDirection[] toAzElEphemeris(EphemerisRow[] ephemeris)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // Main loop that sends the command to each antenna
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();

        Iterator<String> iter = antennaModeController.keySet().iterator();
        final int numAnts = antennaModeController.size();

        NamedHorizonDirection[] azEl = new NamedHorizonDirection[numAnts];
        String antName = "";
        DoubleHolder az = new DoubleHolder();
        DoubleHolder el = new DoubleHolder();
        for (int a = 0; a < numAnts; a++) {
            antName = iter.next();
            try {// TODO. Do this all in parallel, with callbacks, as each
                // antenna takes a few tenth's of a second
                MountController mc = antennaModeController.get(antName);
                mc.toAzElEphemeris(ephemeris, az, el);
                HorizonDirection thisAzEl = new HorizonDirection(az.value,
                        el.value);
                azEl[a] = new NamedHorizonDirection(antName, thisAzEl);
            } catch (UnallocatedEx ex) { // skip this antenna
                unallocatedEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot observe the specified object as"
                        + " the parameters are incorrect. Assuming this is true"
                        + " for all antennas in this array.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx(ex));
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        throwIfUnallocated(unallocatedEx, unallocatedEx.size());
        return azEl;
    }
    
    public EquatorialDirection toRADecEphemeris(EphemerisRow[] ephemeris)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        String msg;
        // Check we have at least one antenna
        Iterator<MountController> iter = antennaModeController.values().iterator();
        if (!iter.hasNext()) {
            msg = "The array does not have any antennas.";
            msg += " One is needed to compute the RA/Dec of the supplied ephemeris";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        MountController mc = iter.next();
        DoubleHolder ra = new DoubleHolder();
        DoubleHolder dec = new DoubleHolder();
        try {
            mc.toRADecEphemeris(ephemeris, ra, dec);
        } catch (UnallocatedEx e) {
            msg = "The array does not have any operational antennas.";
            msg += " One is needed to compute the RA/Dec of the supplied ephemeris";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        EquatorialDirection raDec = new EquatorialDirection(ra.value, dec.value);
        return raDec;
    }

   @Override
   public double startPointingDataCollection(long startTime, long stopTime)
            throws ObservingModeErrorEx, DeviceBusyEx, IllegalParameterErrorEx {
        String msg;
        // Main loop that sends the command to each antenna
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();
        HashMap<String, DeviceBusyEx> deviceBusyEx = new HashMap<String, DeviceBusyEx>();
        Iterator<String> iter = antennaModeController.keySet().iterator();
        double maxDuration = 0.0;
        final int maxRetries = 1; // Only retry once
        int retries = maxRetries;
        String antName = "";
        while (iter.hasNext() || retries < maxRetries) {
            if (retries == maxRetries) {
                antName = iter.next();
            }
            try {
                double thisDuration = antennaModeController.get(antName).startPointingDataCollection(
                        startTime, stopTime);
                // reset the retry counter
                retries = maxRetries;
                // keep the maximum duration
                if (thisDuration > maxDuration) {
                    maxDuration = thisDuration;
                }
            } catch (DeviceBusyEx ex) { // Try again
                msg = "Already collecting pointing data on antenna "
                        + antName
                        + ". Trying to abort the previous data collection ";
                if (retries > 0) {
                    msg += "and restart it with the latest parameters.";
                    msg += " Number of tries left " + retries;
                    logSevereError(msg, ex.errorTrace, DEVELOPER.value);
                    retries--;
                } else {
                    msg += "and skipping data collection on this antenna.";
                    logSevereError(msg, ex.errorTrace, OPERATOR.value);
                    retries = maxRetries;
                    deviceBusyEx.put(antName, ex);
                }
                antennaModeController.get(antName).abortDataCollection();
            } catch (UnallocatedEx ex) { // skip this antenna
                msg = "Mount controller is not allocated."
                        + " Skipping the collection of pointing data on"
                        + " antenna " + antName;
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
                unallocatedEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) { // skip all antennas
                msg = "Cannot collect pointing data as the "
                        + " specified subscan boundries are incorrect."
                        + " Skipping the collection of all pointing data.";
                logSevereError(msg, ex.errorTrace, OPERATOR.value);
                while (iter.hasNext()) {
                    iter.next();
                }
            } catch (MountFaultEx ex) { // skip this antenna
                msg = "Problem communicating with the mount."
                        + " Skipping the collection of pointing data on"
                        + " antenna " + antName;
                logSevereError(msg, ex.errorTrace, OPERATOR.value);
                mountFaultEx.put(antName, ex);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = deviceBusyEx.size() + unallocatedEx.size()
                + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
        throwIfDeviceBusy(deviceBusyEx, numBadAntennas);
        return maxDuration;
    }
    
    @Override
    public AntennaPointingData[] getPointingDataTable(double timeout)
            throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {

        Iterator<String> iter = antennaModeController.keySet().iterator();
        if (!iter.hasNext()) {
            return new AntennaPointingData[0];
        }
        
        String antName = iter.next();
        LongHolder startTime = new LongHolder();
        LongHolder stopTime = new LongHolder();
        try {
            antennaModeController.get(antName).collectingData(startTime, stopTime);
        } catch (UnallocatedEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       return  getSelectedPointingDataTable(startTime.value, stopTime.value, timeout);
  }

    public AntennaPointingData[] getSelectedPointingDataTable(long startTime,
            long stopTime, double timeout) throws ObservingModeErrorEx,
            TimeoutEx, IllegalParameterErrorEx {
        String msg;
        ArrayList<AntennaPointingData> allPD = new ArrayList<AntennaPointingData>();

        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            String antName = iter.next();
            try {
                // 1. Get the data from the mount controller.
                alma.Control.MountControllerPackage.PointingData[] mountData = antennaModeController.get(
                        antName).getSelectedPointingDataTable(startTime, stopTime, timeout);

                // 2. Copy the relevant bits it to the
                // AntennaPointingData struct
                AntennaPointingData currentAPD = new AntennaPointingData();
                currentAPD.antennaName = antName;
                final int dataLen = mountData.length;
                currentAPD.data = new alma.Control.PointingData2[dataLen];

                for (int i = 0; i < dataLen; i++) {
                    alma.Control.MountControllerPackage.PointingData md = mountData[i];
                    alma.Control.PointingData2 thisPD = new alma.Control.PointingData2();

                    thisPD.timestamp = md.timestamp;

                    // The target, in horizon coordinates, is not
                    // directly available. Its computed from three
                    // of the available angles.
                    thisPD.target = new alma.Control.HorizonDirection();
                    thisPD.target.az = md.commanded.az + md.pointing.az
                            - md.horizonAzEl.az;
                    thisPD.target.el = md.commanded.el + md.pointing.el
                            - md.horizonAzEl.el;

                    thisPD.sourceOffset = new alma.Control.EquatorialDirection();
                    thisPD.sourceOffset.ra = md.equatorial.lng;
                    thisPD.sourceOffset.dec = md.equatorial.lat;
                    thisPD.offset = new alma.Control.HorizonDirection();
                    thisPD.offset.az = md.horizon.lng;
                    thisPD.offset.el = md.horizon.lat;
                    thisPD.pointingDirection = md.commanded;
                    thisPD.encoder = md.measured;

                    currentAPD.data[i] = thisPD;
                }
                allPD.add(currentAPD);

            } catch (UnallocatedEx ex) { // skip this antenna
                msg = "Mount controller is not allocated."
                        + " Skipping the collection of pointing data on"
                        + " antenna " + antName;
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
            } catch (TimeoutEx ex) { // abort scan & skip this antenna.
                msg = "Timed out collecting pointing data."
                        + " Skipping the collection of pointing data on"
                        + " antenna " + antName;
                logSevereError(msg, ex.errorTrace, OPERATOR.value);
                antennaModeController.get(antName).abortDataCollection();
            } catch (MountFaultEx ex) { // abort scan & skip this antenna
                msg = "Problem communicating with the mount."
                        + " Skipping the collection of pointing data on"
                        + " antenna " + antName;
                logSevereError(msg, ex.errorTrace, OPERATOR.value);
                antennaModeController.get(antName).abortDataCollection();
            } catch (IllegalParameterErrorEx ex) {
                // abort scans and skip remaining antennas. All
                // antennas get the same parameters so, if one
                // throws an exception, they all should.
                msg = "Cannot collect pointing data as the "
                        + " specified timeout is incorrect."
                        + " This is a programming error."
                        + " Skipping the collection of pointing data on"
                        + " all antennas";
                logSevereError(msg, ex.errorTrace, OPERATOR.value);
                antennaModeController.get(antName).abortDataCollection();
                while (iter.hasNext()) {
                    antName = iter.next();
                    antennaModeController.get(antName).abortDataCollection();
                }
            }
        }
        AntennaPointingData[] pointingData = new AntennaPointingData[allPD.size()];
        allPD.toArray(pointingData);
        {
            int nAnt = pointingData.length;
            msg = "Returning data for " + nAnt + " antennas.";
            logger.logToAudience(AcsLogLevel.DEBUG, msg, DEVELOPER.value);
            for (int i = 0; i < nAnt; i++) {
                int nRows = pointingData[i].data.length;
                msg = "Got " + nRows;
                msg += " rows of data from antenna "
                        + pointingData[i].antennaName;
                if (nRows > 0) {
                    msg += ". Timestamp of first row is "
                            + Util.acsTimeToString(pointingData[i].data[0].timestamp);
                    msg += ". Timestamp of last row is "
                            + Util.acsTimeToString(pointingData[i].data[nRows - 1].timestamp);
                }
                logger.logToAudience(AcsLogLevel.DEBUG, msg, DEVELOPER.value);
            }
        }
        return pointingData;
    }
    
    @Override
    public void abortDataCollection() {
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            String antName = iter.next();
            // TODO. Do this all in parallel, with callbacks, as each
            // antenna takes a few tenth's of a second
            antennaModeController.get(antName).abortDataCollection();
        }
    }

    @Override
    public void setHorizonOffset(double longOffset, double latOffset)
            throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {
        setHorizonOffsetAsync(longOffset, latOffset);
        waitUntilOnSource();
    }

    @Override
    public void setHorizonOffsetAsync(double longOffset, double latOffset)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        setHorizonOffsetQueued(longOffset, latOffset, 0);
    }

    @Override
    public void setHorizonOffsetQueued(double longOffset, double latOffset,
            long startTime) throws ObservingModeErrorEx, IllegalParameterErrorEx {
        setOffsetQueued(longOffset, latOffset, startTime, SourceOffsetFrame.HORIZON);
    }

    @Override
    public void setEquatorialOffset(double longOffset, double latOffset)
            throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {
        setEquatorialOffsetAsync(longOffset, latOffset);
        waitUntilOnSource();
    }

    @Override
    public void setEquatorialOffsetAsync(double longOffset, double latOffset)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
       setEquatorialOffsetQueued(longOffset, latOffset, 0);
    }

    @Override
    public void setEquatorialOffsetQueued(double longOffset, double latOffset,
            long startTime) throws ObservingModeErrorEx, IllegalParameterErrorEx {
        setOffsetQueued(longOffset, latOffset, startTime, SourceOffsetFrame.EQUATORIAL);
    }
    

    public void setStrokeQueued(SourceOffset stroke, long startTime)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {

        final double longOffset = stroke.longitudeOffset;
        final double latOffset = stroke.latitudeOffset;
        final double longVelocity = stroke.longitudeVelocity;
        final double latVelocity = stroke.latitudeVelocity;
        final SourceOffsetFrame frame = stroke.frame;
        final double smallVelocity = 1E-9; // thats less than 1mas/sec.
        if (Math.abs(longVelocity) < smallVelocity
                && Math.abs(latVelocity) < smallVelocity) {
            setOffsetQueued(longOffset, latOffset, startTime, frame);
        }

        if (startTime == 0) {
            String msg = "The stroke cannot have a zero start time unless all the velocities are zero.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        if (frame == SourceOffsetFrame.GALACTIC) {
            String msg = "Offsets in galactic coordinates are not yet supported";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }

        badAntennas_m.clear();
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                MountController mc = antennaModeController.get(antName);
                if (frame == SourceOffsetFrame.HORIZON) {
                    mc.setHorizonLinearStrokeQueued(longVelocity, latVelocity,
                            longOffset, latOffset, startTime);
                } else {
                    mc.setEquatorialLinearStrokeQueued(longVelocity,
                            latVelocity, longOffset, latOffset, startTime);
                }
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
    }

    private void setOffsetQueued(double longOffset, double latOffset,
            long startTime, SourceOffsetFrame frame)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        if (frame == SourceOffsetFrame.GALACTIC) {
            String msg = "Offsets in galactic coordinates are not yet supported.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        // We have added a convention to the startTime parameter. If the
        // startTime is 0 then we make an Async call, otherwise we used the
        // queued version Jeff and Ralph 2009-04-20
        badAntennas_m.clear();
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                MountController mc = antennaModeController.get(antName);
                if (startTime == 0) {
                    if (frame == SourceOffsetFrame.HORIZON) {
                        mc.setHorizonOffsetAsync(longOffset, latOffset);
                    } else {
                        mc.setEquatorialOffsetAsync(longOffset, latOffset);
                    }
                } else {
                    if (frame == SourceOffsetFrame.HORIZON) {
                        mc.setHorizonOffsetQueued(longOffset, latOffset,
                                startTime);
                    } else {
                        mc.setEquatorialOffsetQueued(longOffset, latOffset,
                                startTime);
                    }
                }
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
                badAntennas_m.add(antName);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
                badAntennas_m.add(antName);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
    }

    @Override
    public void setHorizonLinearStrokeQueued(double longVelocity,
            double latVelocity, double longOffset, double latOffset,
            long startTime) throws ObservingModeErrorEx, IllegalParameterErrorEx {
        SourceOffset offset = new SourceOffset();
        offset.frame = SourceOffsetFrame.HORIZON;
        offset.latitudeOffset = latOffset;
        offset.longitudeOffset = longOffset;
        offset.latitudeVelocity = latVelocity;
        offset.longitudeVelocity = longVelocity;
        setStrokeQueued(offset, startTime);
     }

    @Override
    public String getMountController(String antName)
            throws IllegalParameterErrorEx {
        String msg;
        if (!antennaModeController.keySet().contains(antName)) {
            msg = "Cannot return a mount controller for antenna " + antName;
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        return antennaModeController.get(antName).name();
    }

    // TODO. A real implementation of this function
    public double timeout() {
        return 180.0;
    }

    @Override
    public BothAntennaPointingModels[] getPointingModels() throws ObservingModeErrorEx {
        ArrayList<BothAntennaPointingModels> allModels = new ArrayList<BothAntennaPointingModels>();

        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Get the model from each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                MountController mc = antennaModeController.get(antName);
                if (mc.isPointingModelEnabled()) {
                    BothAntennaPointingModels curModel = new BothAntennaPointingModels();
                    if (mc.isPointingModelEnabled()) {
                        curModel.primaryModel = mc.getPointingModel();
                    } else {
                        curModel.primaryModel = new ModelTerm[0];
                    }
                    if (mc.isAuxPointingModelEnabled()) {
                        curModel.auxModel = mc.getAuxPointingModel();
                    } else {
                        curModel.auxModel = new ModelTerm[0];
                    }
                    curModel.antennaName = antName;
                    // TODO. Fix this up
                    curModel.band = ReceiverBand.ALMA_RB_03;
                    allModels.add(curModel);
                }
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);

        return allModels.toArray(new BothAntennaPointingModels[0]);
    }

    @Override
    public void setAuxPointingModel(AntennaPointingModel[] models)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        // Check that all the antennas names are OK.
        throwIfAntennaNotInArray(models);

        // Zero the auxiliary pointing model on all antennas.
        zeroAuxPointingModel();

        // Set the pointing models on the specified antennas.
        setAuxPointingModelInternal(models);
    }

    @Override
    public void updateAuxPointingModel(AntennaPointingModel[] models)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        // Check that all the antennas names are OK.
        throwIfAntennaNotInArray(models);

        // Set the pointing models on the specified antennas.
        setAuxPointingModelInternal(models);
    }

    @Override
    public void zeroAuxPointingModel() throws ObservingModeErrorEx {
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Zero the model in each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                MountController mc = antennaModeController.get(antName);
                mc.zeroAuxPointingModel();
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
            }
        }
        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception. The order in which they are checked is important
        // and the first ones checked have a higher priority of being thrown.
        final int numBadAntennas = unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    // TODO. Should only throw an exception if enough antenna names are
    // incorrect
    private void throwIfAntennaNotInArray(AntennaPointingModel[] models)
            throws IllegalParameterErrorEx {
        String msg;
        HashSet<String> antennas = 
            new HashSet<String>(antennaModeController.size());

        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            antennas.add(iter.next().toUpperCase());
        } 
        for (AntennaPointingModel thisModel : models) {
            String antName = thisModel.antennaName.toUpperCase();
            if (antName.isEmpty()) {
                msg = "Antenna name cannot be empty.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
            }
            if (!antennas.contains(antName)) {
                msg = "Antenna '" + antName + "' is not in the array.";
                msg += " Array contains antennas " + antennas;
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
            }
        }
    }

    private void setAuxPointingModelInternal(AntennaPointingModel[] models)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        for (AntennaPointingModel thisModel : models) {
            final String antName = thisModel.antennaName;
            try {
                MountController mc = antennaModeController.get(antName);
                mc.setAuxPointingModel(thisModel.pointingModel);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
            }
        }
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    // private AntModeControllerCBImpl createCallback() throws MountFaultEx {
    // AntModeControllerCBImpl cbObj;
    // try {
    // cbObj = new AntModeControllerCBImpl(cs);
    // } catch (AcsJContainerServicesEx ex) {
    // AcsJMountFaultEx jEx = new AcsJMountFaultEx();
    // String msg = "Cannot create an AntModeControllerCB offshoot";
    // jEx.setProperty("Message", msg);
    // jEx.log(logger);
    // throw jEx.toMountFaultEx();
    // }
    // return cbObj;
    // }

    // private void checkCompletions(HashMap<String, Completion> completions,
    // String commandName) throws MountFaultEx, TimeoutEx {
    // // Check the completions
    // ArrayList<String> noCompletion = new ArrayList<String>();
    // HashMap<String, Completion> errorCompletion = new HashMap<String,
    // Completion>();
    // for (Iterator<String> iter = completions.keySet().iterator();
    // iter.hasNext();) {
    // final String antName = iter.next();
    // Completion compl = completions.get(antName);
    // if (compl == null) {
    // noCompletion.add(antName);
    // } else if (AcsJCompletion.fromCorbaCompletion(compl).isError()) {
    // errorCompletion.put(antName, compl);
    // }
    // }
    // // Log all bad or missing completions
    // final int numBadAntennas = noCompletion.size() + errorCompletion.size();
    // if (numBadAntennas > 0) {
    // AcsJTimeoutEx jTimeEx = new AcsJTimeoutEx();
    // {
    // String msg = "Problem determining if antenna(s) ";
    // for (Iterator<String> iter = noCompletion.iterator(); iter.hasNext();) {
    // String antName = iter.next();
    // msg += antName + " ";
    // }
    // msg += "have completed the "
    // + commandName
    // + " command (as their completion is null)."
    // + " Perhaps the timeout is too short. Assuming there is a problem.";
    // jTimeEx.setProperty("Message", msg);
    // jTimeEx.log(logger);
    // }

    // AcsJMountFaultEx jMountEx = new AcsJMountFaultEx();
    // {
    // String msg = "Errors while waiting for antenna(s) ";
    // for (Iterator<String> iter = errorCompletion.keySet().iterator();
    // iter.hasNext();) {
    // final String antName = iter.next();
    // msg += antName + " ";
    // jMountEx.setProperty(
    // antName,
    // Util.getNiceErrorTraceString(errorCompletion.get(antName).previousError[0]));
    // }
    // msg += "to do the " + commandName + " command.";
    // jMountEx.setProperty("Message", msg);
    // jMountEx.log(logger);
    // }

    // // If enough antennas are bad, throw an exception.
    // if (numBadAntennas > maxNumBadAntennas()) {
    // // throw the exception that affects more antennas.
    // if (noCompletion.size() > errorCompletion.size()) {
    // throw jTimeEx.toTimeoutEx();
    // } else {
    // throw jMountEx.toMountFaultEx();
    // }
    // }
    // }
    // }

    private void throwIfDeviceBusy(HashMap<String, DeviceBusyEx> ex,
            int numBadAntennas) throws DeviceBusyEx {
        String msg;
        final int numEx = ex.size();
        final int numAntennas = antennaModeController.size();
        if (numEx > 0) {
            AcsJDeviceBusyEx jEx = new AcsJDeviceBusyEx();
            msg = "Cannot command " + numBadAntennas + " antennas out of "
                    + numAntennas + " as " + numEx + " of them are busy.";
            jEx.setProperty("Message", msg);
            for (Iterator<String> iter = ex.keySet().iterator(); iter.hasNext();) {
                String antName = iter.next();
                jEx.setProperty(
                        antName,
                        Util.getNiceErrorTraceString(ex.get(antName).errorTrace));
            }
            jEx.log(logger);
            if (numBadAntennas > maxNumBadAntennas()) {
                throw jEx.toDeviceBusyEx();
            }
        }
    }

    private void throwIfIllegalParameterError(
            HashMap<String, IllegalParameterErrorEx> ex, int numBadAntennas)
            throws IllegalParameterErrorEx {
        String msg;
        final int numEx = ex.size();
        final int numAntennas = antennaModeController.size();
        if (numEx > 0) {
            AcsJIllegalParameterErrorEx jEx = new AcsJIllegalParameterErrorEx();
            msg = "Cannot command " + numBadAntennas + " antennas out of "
                    + numAntennas
                    + " as the specified parameters are incorrect for " + numEx
                    + " of them.";
            jEx.setProperty("Message", msg);
            for (Iterator<String> iter = ex.keySet().iterator(); iter.hasNext();) {
                String antName = iter.next();
                jEx.setProperty(
                        antName,
                        Util.getNiceErrorTraceString(ex.get(antName).errorTrace));
            }
            jEx.log(logger);
            if (numBadAntennas > maxNumBadAntennas()) {
                throw jEx.toIllegalParameterErrorEx();
            }
        }
    }

    private void throwIfUnallocated(HashMap<String, UnallocatedEx> ex,
            int numBadAntennas) throws ObservingModeErrorEx {
        String msg;
        final int numEx = ex.size();
        final int numAntennas = antennaModeController.size();
        if (numEx > 0) {
            AcsJObservingModeErrorEx jEx = new AcsJObservingModeErrorEx();
            msg = "Cannot command " + numBadAntennas + " antennas out of "
                    + numAntennas + " as " + numEx
                    + " of them are not attached to a mount component.";
            jEx.setProperty("Message", msg);
            for (Iterator<String> iter = ex.keySet().iterator(); iter.hasNext();) {
                String antName = iter.next();
                jEx.setProperty(
                        antName,
                        Util.getNiceErrorTraceString(ex.get(antName).errorTrace));
            }
            jEx.log(logger);
            if (numBadAntennas > maxNumBadAntennas()) {
                throw jEx.toObservingModeErrorEx();
            }
        }
    }

    private void throwIfMountFault(HashMap<String, MountFaultEx> ex,
            int numBadAntennas) throws ObservingModeErrorEx {
        String msg;
        final int numEx = ex.size();
        final int numAntennas = antennaModeController.size();
        if (numEx > 0) {
            AcsJObservingModeErrorEx jEx = new AcsJObservingModeErrorEx();
            msg = "Cannot command " + numBadAntennas + " antennas out of "
                    + numAntennas
                    + " as there is a problem with the mount component on "
                    + numEx + " of them.";
            jEx.setProperty("Message", msg);
            for (Iterator<String> iter = ex.keySet().iterator(); iter.hasNext();) {
                String antName = iter.next();
                jEx.setProperty(
                        antName,
                        Util.getNiceErrorTraceString(ex.get(antName).errorTrace));
            }
            jEx.log(logger);
            if (numBadAntennas > maxNumBadAntennas()) {
                throw jEx.toObservingModeErrorEx();
            }
        }
    }

    // This method reports the pointing model in use to the array
    @Override
    public void reportPointingModels() {
        // Get the model from each antenna
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            final String antName = iter.next();
            MountController mc = antennaModeController.get(antName);
            mc.reportPointingModel();
        }
    }

    @Override
    public void reportFocusModels() {
        // Get the model from each antenna
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            final String antName = iter.next();
            MountController mc = antennaModeController.get(antName);
            mc.reportFocusModel();
        }
    }

    @Override
    public void reportFocusPositions() {
        // Get the model from each antenna
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            final String antName = iter.next();
            MountController mc = antennaModeController.get(antName);
            mc.reportFocusPosition();
        }
    }

    // This is a static function because I did not want to put it in the IDL
    // interface (which would have made it available in CCL).
    // But it needs to be callable by the observing modes.
     public static void sendFocusAndPointingModelsToDC(ArrayMountController amc,
            DataCapturer dc, AcsLogger logger) {
        try {
            BothAntennaPointingModels[] allPMs = amc.getPointingModels();
            for (BothAntennaPointingModels currentPM : allPMs) {
                dc.sendAntennaPointingModel(currentPM.antennaName, currentPM.band,
                        currentPM.primaryModel, currentPM.auxModel);
            }
        } catch (ObservingModeErrorEx ex) {
            logModelError(ex.errorTrace, logger);
        } catch (TableUpdateErrorEx ex) {
            logModelError(ex.errorTrace, logger);
        } catch (DataErrorEx ex) {
            logModelError(ex.errorTrace, logger);
        }
    }

    private static void logModelError(ErrorTrace et, AcsLogger logger) {
        String msg = "Cannot put all pointing and/or focus models into the ASDM.";
        ObservingModeUtilities.logSevereError(msg, et, OPERATOR.value, logger);
    }

    /** This method returns the Offshoot reference to this object
       it will create the POA object if that has not already been 
       created.
    */
    @Override
    public ArrayMountController getArrayMountController() 
        throws ContainerServicesEx 
    {
        try {
            if (tieBindingClass == null) {
                tieBindingClass = new ArrayMountControllerPOATie(this);
            }

            if (corbaOffshoot == null) {
                corbaOffshoot = ArrayMountControllerHelper.
                    narrow(cs.activateOffShoot(tieBindingClass));
            }
        } catch (AcsJContainerServicesEx jex) {
            jex.log(logger);
            throw jex.toContainerServicesEx();
        }
        return corbaOffshoot;
    }

    @Override
    public void cleanUp() {
        // Comment out the following lines as it was really bothering the
        // commissioning scientists that the antennas went to standby after each
        // SB/script execution. They should have been pestering the
        // antenna IPT to make the brakes release faster.
//        try {
//            stop();
//        } catch (ObservingModeErrorEx ex) {
//            String msg = "Cannot stop the antennas.";
//            logSevereError(msg, ex.errorTrace, OPERATOR.value);
//        }

        corbaOffshoot = null;
        if (tieBindingClass != null) {
            try {
                cs.deactivateOffShoot(tieBindingClass);
            } catch (AcsJContainerServicesEx ex) {
                String msg ="Cannot deactivate the array mount controller offshoot.";
                logSevereError(msg, ex.getErrorTrace(), DEVELOPER.value);
            }
            tieBindingClass = null;
        }
        super.cleanUp();
    }




//     static ArrayMountControllerPOATie createAMCTie(ContainerServices cs,
//             ObservingModeArray array) throws ObsModeInitErrorEx {
//         return new ArrayMountControllerPOATie(new ArrayMountControllerImpl(cs, array));
//     }

//     static ArrayMountControllerPOATie createAMCTie(ContainerServices cs,
//             ArrayMountControllerImpl amcImpl) throws ObsModeInitErrorEx {
//         return new ArrayMountControllerPOATie(amcImpl);
//     }

//     static ArrayMountControllerPOATie createAMCTie(ContainerServices cs,
//             ObservingModeArray array, AntennaCharacteristics[] antConfigs)
//             throws ObsModeInitErrorEx {
//         return new ArrayMountControllerPOATie(new ArrayMountControllerImpl(cs, array, antConfigs));
//     }

//     static ArrayMountController createAMC(ArrayMountControllerPOATie amcTie,
//             ContainerServices cs) throws ObsModeInitErrorEx {
//         String msg;
//         ArrayMountController amc = null;
//         try {
//             amc = ArrayMountControllerHelper.narrow(cs.activateOffShoot(amcTie));
//         } catch (AcsJContainerServicesEx ex) {
//             msg = "Cannot create an array mount controller object.";
//             ObservingModeUtilities.throwObsModeInitErrorEx
//                 (msg, new AcsJObsModeInitErrorEx(ex), cs.getLogger());
//         }
//         return amc;
//     }
    @Override
     public void applyPointingCalibrationResult(PointingCalResult[] result,
            PolarizationType polarization, String[] antennas)
            throws IllegalParameterErrorEx {
        String msg;

        // Put the antennas parameter in a more confortable container,
        // consider the case of zero antennas, in which case all antennas
        // should be used
        Set<String> ants = new HashSet<String>();
        if (antennas.length == 0) {
            ants = antennaModeController.keySet();
        } else {
            for (String a : antennas) {
                ants.add(a);
            }
        }
        // All the antennas in the parameters should belong to the
        // antennaModeController key set.
        if (!antennaModeController.keySet().containsAll(ants)) {
            msg = "Not all requested antennas are controlled by this observing mode";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }

        // Select from results all the needed combinations of antenna and
        // polarization. If any is missing, an exception is thrown.
        Map<String, PointingCalResult> resultsToApply = new HashMap<String, PointingCalResult>();
        for (Iterator<String> iter = ants.iterator(); iter.hasNext();) {
            String targetAntenna = iter.next();
            // Search for this (antenna, polarization) index in results
            PointingCalResult resultToApply = null;
            for (int i = 0; i < result.length; i++) {
                if ((result[i].antennaName.equals(targetAntenna))
                        && (result[i].polarization.equals(polarization))) {
                    resultToApply = result[i];
                }
            }
            if (resultToApply == null) {
                AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
                msg = "Missing (antenna, polarization) pair";
                ex.setProperty("MissingAntenna", targetAntenna);
                ex.setProperty("MissingPolarization", polarization.toString());
                throwIllegalParameterErrorEx(msg, ex);
            }
            resultsToApply.put(targetAntenna, resultToApply);
        }

        // Now finally apply them
        for (Iterator<PointingCalResult> iter = resultsToApply.values().iterator(); iter.hasNext();) {
            PointingCalResult resultToApply = iter.next();
            MountController mc = antennaModeController.get(resultToApply.antennaName);
            try {
                mc.applyPointingCalibrationResult(resultToApply);
            } catch (UnallocatedEx ex) { // skip this antenna
                msg = "Mount controller is not allocated."
                        + " Hence antenna " + resultToApply.antennaName
                        + " cannot apply the specified pointing result.";
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
                logger.logToAudience(AcsLogLevel.SEVERE, msg, DEVELOPER.value);
            }
        }
    }

    @Override
    public void applyFocusCalibrationResult(FocusCalResult[] result,
            PolarizationType polarization, String[] antennas)
            throws IllegalParameterErrorEx {
        String msg;
        // Put the antennas parameter in a more confortable container,
        // consider the case of zero antennas, in which case all antennas
        // should be used
        Set<String> ants = new HashSet<String>();
        if (antennas.length == 0)
            ants = antennaModeController.keySet();
        else {
            for (String a : antennas)
                ants.add(a);
        }
        // All the antennas in the parameters should belong to the
        // antennaModeController key set.
        if (!antennaModeController.keySet().containsAll(ants)) {
            msg = "Not all requested antennas are controlled by this observing mode";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }

        // Select from results all the needed combinations of antenna and
        // polarization. If any is missing, an exception is thrown.
        Map<String, FocusCalResult> resultsToApply = new HashMap<String, FocusCalResult>();
        for (Iterator<String> iter = ants.iterator(); iter.hasNext();) {
            String targetAntenna = iter.next();
            // Search for this (antenna, polarization) index in results
            FocusCalResult resultToApply = null;
            for (int i = 0; i < result.length; i++) {
                if ((result[i].antennaName.equals(targetAntenna))
                        && (result[i].polarization.equals(polarization))) {
                    resultToApply = result[i];
                }
            }
            if (resultToApply == null) {
                AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
                ex.setProperty("MissingAntenna", targetAntenna);
                ex.setProperty("MissingPolarization", polarization.toString());
                msg = "Missing (antenna, polarization) pair";
                throwIllegalParameterErrorEx(msg, ex);
            }
            resultsToApply.put(targetAntenna, resultToApply);
        }

        // Now finally apply them
        for (Iterator<FocusCalResult> iter = resultsToApply.values().iterator(); iter.hasNext();) {
            FocusCalResult resultToApply = iter.next();
            MountController mc = antennaModeController.get(resultToApply.antennaName);
            try {
                mc.applyFocusCalibrationResult(resultToApply);
            } catch (UnallocatedEx ex) { // skip this antenna
                msg = "Mount controller is not allocated."
                        + " Hence antenna " + resultToApply.antennaName
                        + " cannot apply the specified pointing result.";
                logSevereError(msg, ex.errorTrace, DEVELOPER.value);
            }
        }
    }
    
    @Override
    public void setElevationLimit(double limit) throws ObservingModeErrorEx,
            IllegalParameterErrorEx {
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, IllegalParameterErrorEx> illegalParameterErrorEx = new HashMap<String, IllegalParameterErrorEx>();
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that sends the command to each antenna
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                antennaModeController.get(antName).setElevationLimit(limit);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
            } catch (IllegalParameterErrorEx ex) {
                illegalParameterErrorEx.put(antName, ex);
            }
        }
        final int numBadAntennas = illegalParameterErrorEx.size()
                + unallocatedEx.size() + mountFaultEx.size();
        throwIfIllegalParameterError(illegalParameterErrorEx, numBadAntennas);
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
    }

    @Override
    public double getElevationLimit() throws ObservingModeErrorEx {
        // These maps collect all the different types of exceptions that may be
        // thrown by the MountControllers
        HashMap<String, UnallocatedEx> unallocatedEx = new HashMap<String, UnallocatedEx>();
        HashMap<String, MountFaultEx> mountFaultEx = new HashMap<String, MountFaultEx>();

        // Main loop that sends the command to each antenna
        double maxLimit = 0.0;
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            try {
                maxLimit = Math.max(maxLimit,antennaModeController.get(antName).getElevationLimit());
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(antName, ex);
            } catch (MountFaultEx ex) {
                mountFaultEx.put(antName, ex);
            }
        }
        final int numBadAntennas = unallocatedEx.size() + mountFaultEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas);
        throwIfMountFault(mountFaultEx, numBadAntennas);
        return maxLimit;
    }
    
    @Override
    public void resetLimits() {
        for (Iterator<String> iter = antennaModeController.keySet().iterator(); iter.hasNext();) {
            final String antName = iter.next();
            antennaModeController.get(antName).resetLimits();
        }
    }
    
    /*   ---------------- Observing Cycle Methods ----------------- */
    
    // Start pointing data collection for a sequence of sub-scans
    void startSubscanSequence(SubscanBoundries[] boundries)
            throws ObservingModeErrorEx {
        String msg = "Cannot start a subscan sequence ";
        int numSubscans = boundries.length;
        if (numSubscans == 0) {
            msg += "as no subscans have been specified.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }

        // There is a overhead of a few seconds when starting pointing data
        // collection. To ensure we do not loose data, between sub-scans in a
        // sequence, we collect data for the entire duration of the sequence.
        // The waitForSubscanEnded function then selects the data for each
        // sub-scan and sends it to data capture.
        // Here we add a +1 to the stop time to ensure we get the pointing data
        // on the TE that ends the sub-scan
        try {
            final long startOfFirstSubscan = boundries[0].startTime;
            final long endOfLastSubscan = boundries[numSubscans - 1].endTime;
            pointingDataTimeout = startPointingDataCollection(
                    startOfFirstSubscan, endOfLastSubscan + 1);
        } catch (DeviceBusyEx ex) {
            msg += "as there is a problem collecting pointing data.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (IllegalParameterErrorEx ex) {
            msg += "as there is a problem collecting pointing data.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        currentSubscanSequence = 0;
        subscanBoundries = boundries;
    }
    
    // Start pointing data collection and queue the offsets for a sequence of sub-scans
    void beginSubscanSequence(SubscanBoundries[] boundries, SourceOffset[] offsets)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        String msg = "Cannot start a subscan sequence ";
        final int numSubscans = boundries.length;
        if (numSubscans != offsets.length) {
            msg += "as an incorrect number of offsets is provided.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        // Start the collection of pointing data
        startSubscanSequence(boundries);
        for (int ss = 0; ss < numSubscans; ss++) {
            // This queues each offset/stroke to start as soon as the previous
            // sub-scan has finished. The first stroke is queued to start now!
            // TODO. Because of pipeline latencies in the MountController/Mount
            // "now" really means about four seconds from now (as the *Queued
            // functions do not flush the command queue). This is probably
            // too late and I'll need to add, in the MountController and
            // ArrayMountController, functions to flush the commands queued
            // before "now".
            long strokeStartTime;
            if (ss == 0) {
                strokeStartTime = ObservingModeUtilities.getCurrentACSTime();
            } else {
                strokeStartTime = boundries[ss - 1].endTime;
            }
            long subscanStartTime = boundries[ss].startTime;
            final double deltaTime = (subscanStartTime - strokeStartTime)*100E-9;
            // We need to adjust the offsets as we are starting the stroke
            // early. This is implements the "backwards extrapolation" technique
            // for ensuring the antenna is "on-source" and moving at the right
            // velocity when the sub-scan starts. Backwards extrapolation is not
            // done for the first sub-scan in a sequence.
            // First make a deep copy of the SourceOffset. Java makes this painful.
            SourceOffset stroke = new SourceOffset();
            stroke.frame = offsets[ss].frame;
            stroke.latitudeVelocity = offsets[ss].latitudeVelocity;
            stroke.longitudeVelocity = offsets[ss].longitudeVelocity;
            stroke.latitudeOffset = offsets[ss].latitudeOffset;
            stroke.longitudeOffset = offsets[ss].longitudeOffset;
            // Now adjust the starting position.
            stroke.longitudeOffset -= stroke.longitudeVelocity * deltaTime;
            stroke.latitudeOffset -= stroke.latitudeVelocity * deltaTime;
            // Now queue the stroke
            setStrokeQueued(stroke, strokeStartTime);
        }
        // Add an offset to stop the antenna motion at the end of the last sub-scan
        SourceOffset lastOff = offsets[numSubscans-1];
        SubscanBoundries lastTimes = boundries[numSubscans-1];
        double longOffset = lastOff.longitudeOffset;
        double subscanDuration = (lastTimes.endTime - lastTimes.startTime)*100e-9;
        longOffset += lastOff.longitudeVelocity*subscanDuration;
        double latOffset = lastOff.latitudeOffset;
        latOffset += lastOff.latitudeVelocity*subscanDuration;
        setOffsetQueued(longOffset, latOffset, lastTimes.endTime, lastOff.frame);
    }
   
     void waitForSubscanStart() 
        throws ObservingModeErrorEx {
        // long now = ObservingModeUtilities.getCurrentACSTime();
         // TODO Actually wait and then report the focus positions!
        reportFocusPositions();
    }
     
     void waitForSubscanEnd() throws ObservingModeErrorEx, DataCapturerErrorEx {
        if (currentSubscanSequence < 0 || subscanBoundries == null
                || pointingDataTimeout < 0) {
            String msg = "Unable to wait for a subscan to end."
                    + " Did you forget to call startSubscanSequence?";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }

        try {
            final long thisStartTime = subscanBoundries[currentSubscanSequence].startTime;
            final long thisEndTime = subscanBoundries[currentSubscanSequence].endTime;
            AntennaPointingData[] pd = getSelectedPointingDataTable(
                    thisStartTime, thisEndTime + 1, pointingDataTimeout);
            pointingDataTimeout -= 1;
            dataCapturer.sendPositionData(pd);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        } catch (DataErrorEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        } catch (TimeoutEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (IllegalParameterErrorEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        currentSubscanSequence++;
        if (currentSubscanSequence == subscanBoundries.length) {
            currentSubscanSequence = -1;
            subscanBoundries = null;
        }
    } 

    
    /**
     * This method will take care of all the things that the Array Mount
     * controller needs to do at the beginning of a subscan.
     * This is so far:
     *   Cause the focus positions to be reported
     *   Cause the pointing data to be collected.
     */
    void beginSubscan(long startTime, long stopTime) 
        throws ObservingModeErrorEx {
        reportFocusPositions();
        SubscanBoundries boundries = new SubscanBoundries(startTime, stopTime);
        startSubscanSequence(new SubscanBoundries[]{boundries});
    }

    /**
     * This method takes care of all ofthe the things the Array Mount
     * controller needs to do at the end of a subscan.
     * This is (so far):
     *   Send the position data to DC
     *
     *  Note if there is not a corresponding call to beginSubscan then
     *       this method will throw an ObservingModeErrorEx.
     */
    void endSubscan() 
        throws ObservingModeErrorEx, DataCapturerErrorEx {
        if (pointingDataTimeout < 0) {
            String msg = "Unable to complete subscan in ArrayMountController" +
                ".  Did you forget to call beginSubscan?";
            throwObservingModeErrorEx(msg,new AcsJObservingModeErrorEx());
        }
        
        try {
            AntennaPointingData[] pd = 
                getPointingDataTable(pointingDataTimeout);
            pointingDataTimeout = -1;
            dataCapturer.sendPositionData(pd);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        } catch (DataErrorEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        } catch (TimeoutEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (IllegalParameterErrorEx ex) {
            String msg = "Cannot put pointing data into the ASDM.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
       }

    }

    @Override
    public PointingData[] getPointingData() {
        List<PointingData> retVal = new ArrayList<PointingData>();
        for (String mcn : antennaModeController.keySet()) {
            try {
                MountController mc = antennaModeController.get(mcn);
                PointingData pd = mc.getPointingData(); 
                retVal.add(pd);
            } catch (UnallocatedEx ex) {
                ex.printStackTrace();
            } catch (MountFaultEx ex) {
                ex.printStackTrace();
            }
        }
        return retVal.toArray(new PointingData[0]);
    }

    @Override
    public short getShutterOpenCount() {
        short retVal = 0;
        for (String mcn : antennaModeController.keySet()) {
            try {
                MountController mc = antennaModeController.get(mcn);
                if (mc.isShutterOpen()) retVal++;
            } catch (UnallocatedEx ex) {
                ex.printStackTrace();
            } catch (MountFaultEx ex) {
                ex.printStackTrace();
            }
        }
        return retVal;
    }

    @Override
    public short getShutterClosedCount() {
        short retVal = 0;
        for (String mcn : antennaModeController.keySet()) {
            try {
                MountController mc = antennaModeController.get(mcn);
                if (mc.isShutterClosed()) retVal++;
            } catch (UnallocatedEx ex) {
                ex.printStackTrace();
            } catch (MountFaultEx ex) {
                ex.printStackTrace();
            }
        }
        return retVal;
    }
}
