/*ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

/** 
 * @author  jkern
 * @version $Id$
 * @since    
 */

package alma.Control.ObservingModes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import alma.ACSErr.Completion;
import alma.Control.IsObservableCB;
import alma.Control.IsObservableCBHelper;
import alma.Control.IsObservableCBPOA;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.acs.container.ContainerServices;



public class IsObservableCBImpl extends IsObservableCBPOA {
    final private int MAX_EXPECTED_RESPONSES = 128;

    private ContainerServices containerServices;

    private IsObservableCB externalInterface = null;

    private Semaphore semaphore;

    private int pendingResponses;

    private HashMap<String, Completion> completionStatus;
    private HashMap<String, Boolean>    observableStatus;

    protected Logger logger;


    public IsObservableCBImpl(ContainerServices cs)
        throws AcsJContainerServicesEx {
        containerServices = cs;

        completionStatus = new HashMap<String, Completion>();
        observableStatus = new HashMap<String, Boolean>();

        semaphore = new Semaphore(MAX_EXPECTED_RESPONSES);
        semaphore.drainPermits();
        pendingResponses = 0;
        externalInterface = 
            IsObservableCBHelper.narrow(containerServices.activateOffShoot(this));
        logger = cs.getLogger();
    }

    /* -------------- CORBA Interface ------------------ */
    /* This is a one-way void call so we cannot throw exceptions*/
    @Override
    public void report(String antennaId, boolean observable,
                       Completion status) {
        if (observableStatus.containsKey(antennaId)) {
            observableStatus.put(antennaId, observable);
        }

        if (completionStatus.containsKey(antennaId)) {
            completionStatus.put(antennaId, status);
            semaphore.release();
        }
    }

    /* --------------- Java Interface ----------------- */
    public IsObservableCB getExternalInterface() {
        return this.externalInterface;
    }

    public void addExpectedResponse(String antennaId) {
        pendingResponses++;
        if (pendingResponses > MAX_EXPECTED_RESPONSES) {
            // Too many requests, throw an exception
        }
        completionStatus.put(antennaId, null);
        observableStatus.put(antennaId, null);
    }

    public int getResponse(long timeOut)
        throws InterruptedException, IllegalParameterErrorEx {

        /* Wait until we are done getting getting responses */
        //        HashMap<String, alma.ACSErr.Completion> completions;
        // completions = waitForCompletion(timeOut);
        
        boolean status = semaphore.tryAcquire(pendingResponses, timeOut, 
                                              TimeUnit.SECONDS);
		
        if (!status)
            logger.fine("Mode controller operation timeout.");
        
        try {
            containerServices.deactivateOffShoot(this);
        } catch (AcsJContainerServicesEx ex) {
            logger.warning("Error Attempting to deactivate offshoot");
        } 

        /* First iterate through the completions to see if we need
           to throw an exception or if anyone failed to respond */


        /* There are two error cases here:
           IllegalParameterErrorEx and
           NotAllocatedErrorEx 
        */

        int numAntennaObservable = 0;

        Iterator<String> cit = completionStatus.keySet().iterator();
        while(cit.hasNext()) {
            String ant = cit.next();
            alma.ACSErr.Completion compl = completionStatus.get(ant);
                
            if (compl == null) {
                String msg = "Error in isObservable(). " +
                    "Completion for antenna '" + ant + "' is null.";
                logger.severe(msg);
            
            } else if (compl.type == 10006 && compl.code == 6){
                /* This is the unallocatedError ex, log it but go on */
                String msg = "Antenna '"+ ant +"' claims not to be allocated!";
                logger.warning(msg);
            } else if (compl.type == 10000 && compl.code == 2){
                // Turn this into a parameter exception
                AcsJIllegalParameterErrorEx ex 
                    = new AcsJIllegalParameterErrorEx();
                throw ex.toIllegalParameterErrorEx();
            } else if (compl.code != 0 && compl.type != 0) {
                // Unknown Error Condition: This really should never happen
                String msg = "Unknown error condition reported by '" +
                    ant + "' attempting to continue";
                logger.severe(msg);
            } else {
                // Successful Response
                if (observableStatus.get(ant)==true){
                    numAntennaObservable++;
                }
            }
        }
        
        return numAntennaObservable;
    }
        
}
