#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

"""
This module is part of the Control Command Language.
Defines the ArrayMountController class.
"""

import CCL.SIConverter

class ArrayMountControllerInterface:
    def __init__(self, amc_offshoot, logger):
        self._offshoot = amc_offshoot;
        self._logger   = logger

    def setSource(self, source):
        '''
        Move all antennas to the specified source and track that
        position. The source should be a CCL.FieldSource object such
        as a CCL.FieldSource.EquatorialSource or
        CCL.FieldSource.PlanetSource. This function will not return
        until the antenna is pointing at the specified position to
        within a specified tolerance. The angular tolerance is
        adjusted using the setTolerance function.

        This function throws a:
        * IllegalParameterError exception if any of the values in the
          source is incorrect.
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        '''
        return self._offshoot.setSource(source.getXMLString());

    def setSourceAsync(self, source):
        '''
        This is an asynchronous version of the setSource function. It
        commands all antennas to move but does not wait for them to
        arrive at the specified position before returning. See the
        description of the setSource function for details of the
        arguments and exceptions this function can throw. Unlike the
        setSource function this function will not throw a Timeout
        exception. The waitUntilOnSource function can be used to
        determine when all antennas are on source.
        '''
        return self._offshoot.setSource(source.getXMLString());

    def isObservable(self, source, duration=0.0):
        '''
        Returns True if the source is above the elevation limit of the
        antenna for the specified duration.
        '''
        durationInSec = CCL.SIConverter.toSeconds(duration)
        return self._offshoot.isObservable(source.getXMLString(), durationInSec)

    def waitUntilOnSource(self):
        '''
        This function waits ie., does not return, until all antennas
        are pointing at the last commanded position. This function can
        be used if a you need to wait for the antenna to be on-source
        before continuing with your script.
        
        This function throws a:
        * Timeout exception if all antennas do not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        * Timeout exception (yes the same as the previous) if any
          mount controller is not tracking a source. In this case the
          function will returned immediately and not wait for the
          timeout interval to expire.
        * ObservingModeError exception if there is any problem communicating
          with any antenna mount. This includes hardware faults
        '''
        return self._offshoot.waitUntilOnSource();

    def applyPointingCalibrationResult(self, result, polarization, antennas=[]):
        '''
        Applies the pointing calibration result in all or a subset of the
        antennas selecting the given polarization from the result.
        '''
        self._offshoot.applyPointingCalibrationResult(result, polarization,
                                                      antennas)

    def applyFocusCalibrationResult(self, result, polarization, antennas=[]):
        '''
        Applies the focus calibration result in all or a subset of the
        antennas selecting the given polarization from the result.
        '''
        self._offshoot.applyFocusCalibrationResult(result, polarization,
                                                   antennas)
        
    def setElevationLimit(self, elevationLimit):
        '''
        Set the minimum elevation for all commands sent to the
        antennas in thsi array. This value must be greater than the
        hardware limits of all antennas in teh array.  Typically this
        is two degrees.

        This function will throw an
        * IllegalParameterError exception if the specified elevation
          limit is less than the hardware limit or if its bigger than
          the maximum elevation (88.9 degrees)
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        elevationLimitInRad = CCL.SIConverter.toRadians(elevationLimit)
        return self._offshoot.setElevationLimit(elevationLimitInRad)
        
    def getElevationLimit(self):
        '''
        Get the largest elevation limit teh antenna in this array. The
        number returned is in radians.

        This function will throw a
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self._offshoot.getElevationLimit()

    def resetLimits(self):
        '''
        Reset the antenna motion limits, to the maximum value allowed,
        for all antennas in this array.

        This function does not throw any exceptions. If the elevation
        limits cannot be reset a log message is generated
        '''
        return self._offshoot.resetLimits()

    def getArrayMountController(self):
        '''
        Get the Array Mount Controller offshoot.  This allows much higher
        access to the methods within the observing mode.
        '''
        import CCL.ArrayMountController
        return CCL.ArrayMountController.ArrayMountController\
               (self._offshoot.getArrayMountController(), self._logger)

