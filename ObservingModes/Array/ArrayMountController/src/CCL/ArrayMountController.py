#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
Defines the ArrayMountController class.
"""

import Acspy.Util.XmlObjectifier;
import ControlExceptionsImpl;
import CCL.ArrayMountControllerInterface
import CCL.SIConverter

class ArrayMountController(CCL.ArrayMountControllerInterface.ArrayMountControllerInterface):
    '''
    The ArrayMountController object is used to manipulate groups (or
    an array) of MountController objects. Its core functions move all
    antennas to track a celestial source. This should be contrasted
    with the lower level MountController object that manipulates just
    one antenna and the higher level Observing modes (like the
    TotalPowerObservingMode or SingleFieldInterferometeryObservingMode
    objects) that use an ArrayMountController object to perform
    observations of a specific type.

    Access to the individual underlying MountController objects is
    provided using the getMountController function. This allows you to
    separately manipulate one antenna in the array

    The ArrayMountController object has over 21 functions. These are
    almost exclusively "array" versions of a corresponding function in
    the MountController object. Its expected that additional future
    versions of this object will contain additional functions that
    provide "array" versions of all the MountController functions.

    UNITS:
    All functions in this class return values in SI
    units. Specifically angles are in radians, angular velocities are
    in radians/second, frequencies are in Hertz and time intervals are
    in seconds. Absolute times are in ACS time units which are the
    number of 100 nanosecond intervals since the start of Gregorian
    calendar.

    Parameters for all functions should also use SI units if a numeric
    value is given. Alternatively the user can specify the parameter
    using a string which contains a numeric value and a unit e.g., "10
    deg". If a string is used the units are checked to ensure the
    dimensionality is correct (although wavelength to frequency
    conversions are automatically done). See the documentation for the
    CCL.SIConverter class for a list of commonly used  unit strings.

    Absolute times must be in ACS time units which is a long long
    (64-bit) integer counting the number of 100 nanosecond intervals
    since the start of the Gregorian calendar on
    1582-10-15T00:00:00.000.  See the documentation for the
    CCL.MountController class for an example of how to convert a
    calendar date to/from an ACS time.

    '''

    def __init__(self, amc_offshoot, logger):
        '''
        This object shoud not be directly constructed by the user. It
        is always a part of an observing mode and will be constructed
        by this observing mode when necessary. The
        getArrayMountController function in the observing mode should
        be used to create this object.
        '''
        CCL.ArrayMountControllerInterface.ArrayMountControllerInterface.__init__(self, amc_offshoot, logger)
        
    def setAzEl(self, az, el):
        '''
        Move all antennas to the specified azimuth and elevation and
        stop at that position. The azimuth & elevation are angles.
        This function will not return until the antenna is pointing at
        the specified position to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance function.
        
        This function throws a:
        * IllegalParameterError exception if:
          the specified azimuth and elevation are not angles;
          the azimuth is greater than +/- 3*PI/2 (270 degrees);
          the elevation  is less than 2*PI/180 (2 degrees) or greater
          than PI/2 (90 degrees).
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        '''
        azInRad = CCL.SIConverter.toRadians(az)
        elInRad = CCL.SIConverter.toRadians(el)
        return self._offshoot.setAzEl(azInRad, elInRad);

    def setAzElAsync(self, az, el):
        '''
        This is an asynchronous version of the setAzEl function. It
        commands all antennas to move but does not wait for them to
        arrive at the specified position before returning. See the
        description of the setAzEl function for details of the
        arguments and exceptions this function can throw. Unlike the
        setAzEl function this function will not throw a Timeout
        exception. The waitUntilOnSource function can be used to
        determine when all antennas are on source.
        '''
        azInRad = CCL.SIConverter.toRadians(az)
        elInRad = CCL.SIConverter.toRadians(el)
        return self._offshoot.setAzElAsync(azInRad, elInRad);

    def stop(self):
        '''
        Stop all antennas at their current position. This stops all
        commands being sent to the Mount by clearing the current
        source and puts all ACUs into standby mode on both axes. This
        function should work at all times unless the ALMA software
        cannot communicate with the ACU or if the ACU is is local
        access mode.
        
        This function throws a:
        * ObservingModeError exception if there is any problem communicating
        with the antenna mount. This includes hardware faults and if
        the ACU is in local access mode.
        '''
        return self._offshoot.stop();

    def survivalStow(self):
        '''
        Stow the antenna. This just moves all antennas to their
        survival stow position and sets the ACU to shutdown mode. It
        does not insert the stow pins. A better implementation is to
        put the ACU into the survival stow mode.

        This function throws a:
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self._offshoot.survivalStow();

    def maintenanceStow(self):
        '''
        Stow the antenna. This just moves the antenna to point near
        the zenith and sets the mount to shutdown mode. It does not
        insert the stow pins. An alternative implementation is to put
        the antenna into the maintenance stow mode. However, for the
        prototype antennas, this would point the antenna at the zenith
        and risk damage to the receiver by cloudsat.

        This function throws a:
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self._offshoot.maintenanceStow();

    def setDirection(self, ra, dec, pmRA=0., pmDec=0., parallax=0.):
        '''
        Move all antennas to the specified celestial position and track
        a star at that position. The right ascension & declination are
        angles, the proper motion on both axes are angular velocities
        and the parallax is an angle. This function will not return
        until the antenna is pointing at the specified position to
        within a specified tolerance. The angular tolerance is
        adjusted using the setTolerance function.

        The celestial positions *must* be in the FK5 coordinate system
        and, by default, should be in the 2000.0 epoch and
        equinox. The epoch and equinox can be changed using the
        setEpoch function.

        This function throws a:
        * IllegalParameterError exception if any of the supplied
          parameters have the wrong units or are out of range. Specifically if:
          commandedRA is greater than +/- 2*PI (360 degrees);
          commandedDec is greater than +/-PI/2;
          the source is below the horizon.
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self._offshoot.setDirection(raInRad, decInRad, \
                                       pmRAInRadPerSec, pmDecInRadPerSec, \
                                       parallaxInRad);

    def setDirectionAsync(self, ra, dec, pmRA=0., pmDec=0., parallax=0.):
        '''
        This is an asynchronous version of the setDirection
        function. It commands all antennas to move but does not wait
        for the antenna to arrive at the specified position before
        returning. See the description of the setDirection function
        for details of the arguments and exceptions this function can
        throw. Unlike the setDirection function this function will not
        throw a Timeout exception. The waitUntilOnSource function can
        be used to determine when the antenna goes on source.
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self._offshoot.setDirectionAsync(raInRad, decInRad, \
                                            pmRAInRadPerSec, pmDecInRadPerSec, \
                                            parallaxInRad);

    def isObservableEquatorial(self, ra, dec, duration=0., pmRA=0., pmDec=0., parallax=0.):
        '''
        Returns true if a source at the specified right-ascension and
        declination would be observable on this antenna at this
        instant in time. This really means "is the elevation of this
        source above the elevation limit of the antenna. . Set the
        duration parameter to a positive value to determine if the
        source will be observable over an extended period of time
        (starting from now). The duration is in seconds. See the
        setDirection function for a description of the input
        parameters.
        '''
        raInRad = CCL.SIConverter.toRadians(ra)
        decInRad = CCL.SIConverter.toRadians(dec)
        durationInSec = CCL.SIConverter.toSeconds(duration)
        pmRAInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmRA)
        pmDecInRadPerSec = CCL.SIConverter.toRadiansPerSecond(pmDec)
        parallaxInRad = CCL.SIConverter.toRadians(parallax)
        return self._offshoot.isObservableEquatorial\
               (raInRad, decInRad, durationInSec, \
                pmRAInRadPerSec, pmDecInRadPerSec, parallaxInRad);
    
    def setPlanet(self, planetName):
        '''
        Move the antenna to point at the specified "planet". The
        planet name must be a string and one of the following:
        Mercury, Venus, Mars, Jupiter, Saturn, Neptune, Uranus,
        Neptune and Pluto, Sun & Moon. Note the last three are not
        officially planets and the Earth is not on the list.
        
        This function will not return until all antennas are pointing
        at the specified position to within a specified tolerance. The
        angular tolerance is adjusted using the setTolerance functions
        in the MountController class.

        This function uses the JPL DE200 ephemeris to determine the
        planets position. The polynomial that describes the orbit is
        evaluated 21 times a second.

        This function throws a:
        * IllegalParameterError exception if you mistyped the planet
          name. The planet name is case-insensitive.  This exception will
          also be thrown if the specified planet is below the
          elevation limit of the antenna.
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout
          function).
        '''
        return self._offshoot.setPlanet(planetName);

    def setPlanetAsync(self, planetName):
        '''
        This is an asynchronous version of the setPlanet function. It
        commands all antennas to move but does not wait for them to
        arrive at the specified position before returning. See the
        description of the setPlanet function for details of the
        arguments and exceptions this function can throw. Unlike the
        setPlanet function this function will not throw a Timeout
        exception. The waitUntilOnSource function can be used to
        determine when the antenna goes on source.
        '''
        return self._offshoot.setPlanetAsync(planetName);

    def isObservablePlanet(self, planetName, duration):
        '''
        Returns true if the specified planet would be observable on
        all antennas at this instant in time. This really means "is
        the elevation of this planet above the elevation limit of the
        antenna. See the setPlanet function for a description of the
        allowed planet names.
        '''
        durationInSec = CCL.SIConverter.toSeconds(duration)
        return self._offshoot.isObservablePlanet(planetName, durationInSec);

    def setObservingFrequency(self, frequency):
        '''
        Specify the observing frequency/wavelength. This value is only
        used to correct for atmospheric refraction and will *not* tune
        any receivers. This function does not check if the specified
        value corresponds to any ALMA receiving band. The specified
        observing frequency will be used for all subsequent commands
        that apply a refraction correction when pointing the antenna
        i.e., setDirection, setPlanet, setEphemeris. It is reset to
        the default value when the underlying component is shutdown
        and then restarted.

        This function throws a:
        * IllegalParameterError exception if
          The specified value is not a frequency or wavelength;
          the frequency is not positive and less than 1000THz.
        '''
        frequencyInHz = CCL.SIConverter.toHertz(frequency)
        return self._offshoot.setObservingFrequency(frequencyInHz);

    def startPointingDataCollection(self, startTime, stopTime):
        '''
        Start collecting pointing data. This will tell all mount
        controllers to start accumulating pointing data that can be
        returned using the getPointingDataTable function. The start
        and stop times are integers in ACS Time units.

        If the start time is in the past this function will begin
        collecting data as soon as possible. The start time cannot be
        more than 25hours in the future (as this is probably an
        indication of a software error elsewhere). The stop time must
        be in the future, greater than the start time, and but not
        more than 25 hours ahead of the start time (to limit the
        amount of memory used). This function will return immediatly
        and the return value indicates how long, in seconds, till the
        data collection completes.

        This function throws a:
        * IllegalParameterError exception if the start or stop times
          are incorrect (as described above).
        * DeviceBusy exception if the mount controller is already
          collecting data (even if the data collection starts in the
          future).
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        return self._offshoot.startPointingDataCollection(startTime, stopTime);
    
    def getPointingDataTable(self, timeout=300.0):
        '''
        Get the pointing data. This function returns all the pointing
        data that has been collected by all mount controllers as a
        result of the last successful execution of the
        startPointingDataCollection function. A zero length sequence
        is returned if startPointingDataCollection has never been
        called. If the mount controller is currently collecting data
        this function will block until the data collection is
        completed. The timeout parameter is the time, in seconds, that
        specifies how long this function will wait for the data
        collection to complete. The timout must always be non-negative
        and defaults to 300 seconds (5 mins).

        The pointing data that is returned is a list with zero or more
        elements. Each element of this list is the pointing data for
        each antenna and contains two elements, the antenna name and a
        dictionary that contains the pointing data for that
        antenna. See the getPointingData function for a more detailed
        description of each field in this dictionary. The data for
        each antenna will always be sorted by increasing time and
        typically there will be a new direction every 48ms.

        This function throws a:
        * IllegalParameterError exception if the timeout is negative.
        * Timeout exception is thrown if the data collection is not
          complete after the specified amount of time.
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        '''
        timeoutInSec = CCL.SIConverter.toSeconds(timeout)
        return self._offshoot.getPointingDataTable(timeoutInSec);

    def abortDataCollection(self):
        '''
        Abort the current pointing data collection. This will abort
        the current data collection process. Data currently collected
        will be available through the getPointingDataTable function
        and the actual times of the last data collection, including an
        adjustment of the stop time to reflect the call to this
        function, will be available through the collectingData
        function.

        This function does not throw any exceptions.
        '''
        return self._offshoot.abortDataCollection();

    def setHorizonOffset(self, longOffset, latOffset):
        '''
        Offset the commanded azimuth and elevation by the specified
        values. You must have previously commanded all antennas to
        move using functions like setAzEl, setDirection or setPlanet,
        before calling this function.

        The offsets are angles and in a spherical coordinate system
        with an origin that is centered about the last commanded
        position. Both offsets shift the position along a great
        circle. For example while a longitude offset will result in a
        change in azimuth it will usually also involve a smaller
        change in the elevation. A latitude offset will always involve
        just a change in elevation.

        These offsets are entirely independent of the equatorial
        offsets or the offsets that occur as part of the pointing
        model. Unlike the offsets that are part of the pointing model
        these offsets are cleared whenever you call the setAzEl or
        setDirection functions or any linear stroke becomes current.

        This function will not return until all antennas are pointing
        at the specified offset to within a specified tolerance.  This
        function will clear any queued offsets or strokes.
        
        This function throws a:
        * IllegalParameter exception if:
          either offset is not an angle;
          the longitude offset is bigger than +/-3*PI (540 degrees);
          the latitude offset is bigger than +/-PI/2 (90 degrees);
          if you are not tracking a source.
        * Timeout exception if the antenna does not go to the
          specified offset after a suitable amount of time (controlled
          using the setTimeout function).
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self._offshoot.setHorizonOffset(longInRad, latInRad);

    def setHorizonOffsetAsync(self, longOffset, latOffset):
        '''
        This is an asynchronous version of the setHorizonOffset
        function. It immediately commands all antennas to move to the
        specified offset but does not wait for the antenna to arrive
        at the specified offset before returning. See the description
        of the setHorizonOffset function for details of the arguments
        and exceptions this function can throw. Unlike the
        setHorizonOffset function this function will not throw a
        Timeout exception.

        The waitUntilOnSource function can be used to determine when
        the antenna goes on source.
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self._offshoot.setHorizonOffsetAsync(longInRad, latInRad);

    def setHorizonOffsetQueued(self, longOffset, latOffset, startTime):
        '''
        This is a queued version of the setHorizonOffsetAsync
        function. It commands all antennas to move to the specified
        offset, at the specified time. The start time is in ACS time
        units. If the specified time is in the past this function will
        behave like setHorizonOffsetAsync except that it will not
        clear any queued offsets or strokes.  A start time of zero
        means "as soon as possible".

        With this function the user could queue up a sequence of
        offsets to efficiently command the antenna to do, for example,
        a five-point observation. However the user is responsible for
        ensuring there is sufficient time between each offset in the
        queue as no check is made that the antenna has reached the
        specified offsets.
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self._offshoot.setHorizonOffsetQueued(longInRad, latInRad, startTime);
    
    def setHorizonLinearStrokeQueued(self, longVelocity, latVelocity, longOffset, latOffset, startTime):
        '''
        This is a queued version of the setHorizonLinearStroke
        function. It commands all antennas to start the linear stroke
        at the specified time (in ACS time units). If the specified
        time is in the past this function will immediately command the
        antenna to move but, as this start time is also the origin for
        the additional motion due to the linear (non-zero velocity)
        terms, a start time in the past will require the antenna to
        "catch up". A better option is to use a start time of zero
        which means "as soon as possible". See the description of the
        setHorizonLinearStroke function for details of the other
        arguments and exceptions this function can throw. Unlike the
        setHorizonLinearStroke function this function:
        * does not clear any queued offsets or strokes
        * it does not wait for the antenna to arrive at the specified
          offset before beginning the linear stroke.
        * Does not throw a Timeout exception
       
        With this function the user could queue up a sequence of
        strokes and offsets to efficiently command all antennas to do,
        for example, a raster scan. However the user is responsible
        for ensuring there is sufficient time between each offset or
        stroke in the queue as no check is made that any antenna has
        reached the specified positions.
        '''
        longVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(longVelocity)
        latVelocityInRadPerSec = CCL.SIConverter.toRadiansPerSecond(latVelocity)
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self._offshoot.setHorizonLinearStrokeQueued\
               (longVelocityInRadPerSec, latVelocityInRadPerSec, \
                longInRad, latInRad, startTime);

    def setEquatorialOffset(self, longOffset, latOffset):
        '''
        Offset the commanded right ascension and declination by the
        specified values. This function is identical to the
        setHorizonOffset command except that the latitude offsets will
        shift the position in declination and the longitude offset
        will primarily shift the position in right
        ascension. Equatorial offsets cannot be used unless you are
        tracking a celestial source.
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self._offshoot.setEquatorialOffset(longInRad, latInRad);

    def setEquatorialOffsetAsync(self, longOffset, latOffset):
        '''
        This is an asynchronous version of the setEquatorialOffset
        function. See the setHorizonOffsetAsync function for a
        description of how this function differs from set
        setEquatorialOffset.
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self._offshoot.setEquatorialOffsetAsync(longInRad, latInRad);

    def setEquatorialOffsetQueued(self, longOffset, latOffset, startTime):
        '''
        This is a queued version of the setEquatorialOffsetAsync
        function. See the setHorizonOffsetQueued function for a
        description of how this function differs from the
        setEquatorialOffsetAsync function.
        '''
        longInRad = CCL.SIConverter.toRadians(longOffset)
        latInRad = CCL.SIConverter.toRadians(latOffset)
        return self._offshoot.setEquatorialOffsetQueued(longInRad, latInRad, startTime);
    
    def getPointingModels(self):
        '''
        Returns all the pointing models for all antennas in this
        array. The data that is returned is a list that normally has
        many elements as there are antennas in your array. Each
        element of this list is a dictionary that contains the
        following fields:
        * antennaName: A string that contains the antenna name.
        * pointingModel: This is a list of coefficients that are the
          current values for the pointing model on the antenna.
        * auxPointingModel: This is a list of coefficients that
          are the current values for the auxiliary pointing model on
          the antenna. If the auxiliary pointing model is not enabled
          this list will be zero length.
        If the pointing model is not enabled, or if there is any
        problem getting the data, then the entry for an individual
        antenna will be missing.
        '''
        return self._offshoot.getPointingModels();
        
    def setAuxPointingModel(self, model):
        '''
        Sets and enable the auxiliary pointing model on the specified
        antennas. Zeros and disables the auxiliary pointing model on
        all other antennas in this array.  The models parameter is a
        list that has one entry for each antenna. Each entry is a
        dictionary with two fields
        * antennaName: A string that contains the antenna name.
        * pointingModel: This is a list of coefficients that are the
          new values for the pointing model on the antenna.

        This function throws a:
        * IllegalParameter exception if:
          an antenna name is incorrect,
          the pointing model coefficients are not correctly specified.
        * ObservingModeError exception if there is any problem loading the
          pointing model in the antenna mount.

        '''
        return self._offshoot.setAuxPointingModel(model);
        
    def updateAuxPointingModel(self, model):
        '''
        Sets and enables the auxiliary pointing model on the specified
        antennas. Does not affect the pointing models on all other
        antennas. See the setAuxPointingModel for a description of the
        input parameter and the exceptions that may be thrown.
        '''
        return self._offshoot.updateAuxPointingModel(model);

    def zeroAuxPointingModel(self):
        '''
        Zeros and disables the auxiliary pointing model on all
        antennas in this array.
        This function throws a:
        * ObservingModeError exception if there is any problem zeroing the
          pointing model in the antenna mount.
        '''
        return self._offshoot.zeroAuxPointingModel();

    def getMountController(self, antennaName):
        '''
        Return a reference to the underlying MountController for the
        specified antenna. The specified antenna must be a string like
        "DV01" and be part of the array.
        '''
        # You might think that
        #       return CCL.MountController.MountController(antennaName)
        # is a simpler way to do this. However this does not check that the
        # specified antenna is in this array.
        import CCL.MountController
        return CCL.MountController.MountController(componentName=self._offshoot.getMountController(antennaName));
