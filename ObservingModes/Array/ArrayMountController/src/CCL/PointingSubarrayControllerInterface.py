#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

"""
This module is part of the Control Command Language.
Defines the PointingSubarrayControllerInterface class.
"""

import Acspy.Util.XmlObjectifier;
import ControlExceptionsImpl;
from CCL.ArrayMountControllerInterface import ArrayMountControllerInterface
import CCL.SIConverter

class PointingSubarrayControllerInterface(ArrayMountControllerInterface):
    def __init__(self, offshoot, logger):
        ArrayMountControllerInterface.__init__(self, offshoot, logger)
 
 
    def createSubarray(self, subarrayName, antennaList):
        '''
        Create a pointing subarray consisting of the antennas listed in
        antennaList.  This subarray can be accessed until such time as it
        is destroyed by the name given as the subarrayName.  All antennas
        in antennaList must be in the "Default" pointing subarray or the
        creation will fail.  Initially all antennas within an array are
        in the "Default" subarray.  At least one antenna must be specified
        in the antenna list.
        '''
        self._offshoot.createSubarray(subarrayName, antennaList)


    def destroySubarray(self, subarrayName):
        '''
        Destroy a pointing subarray, returning all antennas associated with
        it to the "Default" subarray.

        An exception is thrown if the subarray does not exist.
        '''
        self._offshoot.destroySubarray(subarrayName)
        

    def setSubarraySource(self, subarrayName, source):
        '''
        Move all antennas in the specified subarry to the specified
        source and track that position. The source should be a
        "CCL.FieldSource" object such as a
        CCL.FieldSource.EquatorialSource or
        CCL.FieldSource.PlanetSource. This function will not return
        until the antenna is pointing at the specified position to
        within a specified tolerance. The angular tolerance is
        adjusted using the setTolerance function.

        This function throws a:
        * IllegalParameterError exception if any of the values in the
          source is incorrect or the specified subarray does not exist.
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * Timeout exception if the antenna does not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        '''
        return self._offshoot.setSubarraySource(subarrayName,
                                                source.getXMLString());

    def setSubarraySourceAsync(self, subarrayName, source):
        '''
        This is an asynchronous version of the setSubarraySource function. It
        commands all antennas to move but does not wait for them to
        arrive at the specified position before returning. See the
        description of the setSource function for details of the
        arguments and exceptions this function can throw. Unlike the
        setSource function this function will not throw a Timeout
        exception. The waitUntilSubarrayOnSource function can be used to
        determine when all antennas are on source.
        '''
        return self._offshoot.setSubarraySourceAsync(subarrayName,
                                                     source.getXMLString());

    def isObservableBySubarray(self, subarrayName, source, duration=0.0):
        '''
        Returns True if the source is above the elevation limit of the
        antenna for the specified duration.

        This function throws a:
        * IllegalParemeterErrorEx if teh specified subarray does not exist.
        '''
        durationInSec = CCL.SIConverter.toSeconds(duration)

        return self._offshoot.isObservableBySubarray(subarrayName,
                                                     source.getXMLString(),
                                                     durationInSec)

    def waitUntilSubarrayOnSource(self, subarrayName):
        '''
        This function waits ie., does not return, until all antennas
        in the specified subarray are pointing at the last commanded
        position. This function can be used if a you need to wait for
        the antenna to be on-source before continuing with your script.
        
        This function throws a:
        * Timeout exception if all antennas do not go "on-source" after
          a suitable amount of time (controlled using the setTimeout function).
        * Timeout exception (yes the same as the previous) if any
          mount controller is not tracking a source. In this case the
          function will returned immediately and not wait for the
          timeout interval to expire.
        * ObservingModeError exception if there is any problem communicating
          with any antenna mount. This includes hardware faults
        * IllegalParemeterErrorEx if the specified subarray does not exist.
        '''
        return self._offshoot.waitUntilSubrarryOnSource(subarrayName);
        
    def setSubarrayElevationLimit(self, subarrayName, elevationLimit):
        '''
        Set the minimum elevation for all commands sent to the
        antennas in thsi array. This value must be greater than the
        hardware limits of all antennas in teh array.  Typically this
        is two degrees.

        This function will throw an
        * IllegalParameterError exception if the specified elevation
          limit is less than the hardware limit or if its bigger than
          the maximum elevation (88.9 degrees)
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * IllegalParemeterErrorEx if the specified subarray does not exist.
        '''
        elevationLimitInRad = CCL.SIConverter.toRadians(elevationLimit)
        return self._offshoot.setSubarrayElevationLimit(subarrayName,
                                                        elevationLimitInRad)
        
    def getSubarrayElevationLimit(self, subarrayName):
        '''
        Get the largest elevation limit of antenna in the specified subarray.
        The number returned is in radians.

        This function will throw a
        * ObservingModeError exception if there is any problem communicating
          with the antenna mount. This includes hardware faults.
        * IllegalParemeterErrorEx if the specified subarray does not exist.
        '''
        return self._offshoot.getSubarrayElevationLimit(subarrayName)

    def resetSubarrayLimits(self, subarrayName):
        '''
        Reset the antenna motion limits, to the maximum value allowed,
        for all antennas in this array.

        This function will throw a
        * IllegalParemeterErrorEx if the specified subarray does not exist.

        If for some reason the elevation limits cannot be reset a log
        message is generated
        '''
        return self._offshoot.resetSubarrayLimits(subarrayName)


    # The getArrayMountController method is already defined but we need
    # to modify it's doc string
    def getArrayMountController(self):
        '''
        This returns the "Default" Array Mount Controller offshoot.
        This allows much higher access to the methods within the
        observing mode.
        '''
        import CCL.ArrayMountController
        return CCL.ArrayMountController.ArrayMountController\
               (self._offshoot.getArrayMountController(), self._logger)

    def getSubarrayMountController(self, subarrayName):
        '''
        This returns the specified Array Mount Controller offshoot.
        This allows much higher access to the methods within the
        observing mode.
        '''
        import CCL.ArrayMountController
        return CCL.ArrayMountController.ArrayMountController\
               (self._offshoot.getSubarrayMountController(subarrayName),
                self._logger)

