// "$Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2011 
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU LesSkyser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA


package alma.Control.ObservingModes;

/**
 * @author Nicolas Troncoso <ntroncos@alma.cl>
 * @version $Id$
 *
 */

import java.util.Iterator;
import java.util.logging.Logger;
import java.util.Map.Entry;

import org.omg.CORBA.DoubleHolder;
import org.omg.CORBA.IntHolder;

import alma.Control.NewTPP;
import alma.Control.NewTPPHelper;
import alma.Control.SquareLawControllerOperations;
import alma.Control.SubscanBoundries;
import alma.Control.TotalPower;
import alma.Control.TotalPowerConfiguration;
import alma.Control.CommonCallbacks.AntennaCallbackImpl;
import alma.Control.NewTPPPackage.SubscanInfo;
import alma.ControlDeviceExceptions.IllegalConfigurationEx;
import alma.ControlExceptions.DeviceBusyEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.NewTotPwrProcErr.AlreadyShutDownEx;
import alma.NewTotPwrProcErr.DistributerNAEx;
import alma.NewTotPwrProcErr.NoAntennasEx;
import alma.NewTotPwrProcErr.NoBasebandEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.container.ContainerServices;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.SquareLawSetupT;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
import alma.ControlExceptions.wrappers.AcsJTimeoutEx;

/**
 * This is the controller of the Square Law mode. The responsibility of this class 
 * is to control the allocation for IFPROC Total Power detectors and TotalPowerProcesor.
 * This class is also responsible for correctly configuring the NUTATORs and the
 * BINs for such switching mode.
 * This class allows to control this behavior from any observing mode.
 * <p>
 * The intended usage is:
 * <ul>
 *    <li>If the observing mode requires SquareLaw get a SquareLawControllerImpl instance.</li>
 *    <li>Configure the instance with the proper array.</li>
 *    <li>@TODO Check if the switching mode requires NUTATORs, configure them.</li>
 *    <li>@TODO Configure any BINING configured in the switching mode.</li>
 *    <li>Execute the subsequence.</li>
 * </ul>
 *    For an example @see {@link alma.Control.ObservingModes.TotalPowerObservingModeImpl}
 * </p>
 *
 *
 */
public class SquareLawControllerImpl extends
ObservingModeCore<TotalPower> implements
SquareLawControllerOperations {

    // Total Power Processor
    private final NewTPP tppProc;
    
    // The name of the TotalPowerProcessor
    private final String tppName;
	private TotalPowerConfiguration[] allTp = null;
	private SubscanBoundries[] subscanBoundries = null;
	private SubscanInfo[] ssi = null;
	private int sequence = -2;

	/**
	 * @param cs - Container service instance
	 * @param array - Observing mode array
	 * @param bulkDataDistributorName - String with the bulkDataDistributor name
	 * @throws ObsModeInitErrorEx in case any resource cannot be allocated
	 */
	public SquareLawControllerImpl(ContainerServices cs,
			ObservingModeArray array, String bulkDataDistributorName) throws ObsModeInitErrorEx {
		super(cs, array);

		this.array = array;
		String msg;

        createAntModeControllers("alma.Control.TotalPowerHelper", "TotalPower");

        // Create the TotalPowerProcessor (NewTPP)
        org.omg.CORBA.Object obj = null;
        String tppComponentName = "CONTROL/" + array.getArrayName()
            + "/TotalPowerProcessor";
        String tppComponentType = "IDL:alma/Control/NewTPP:1.0";
        ComponentQueryDescriptor cqDesc = 
            new ComponentQueryDescriptor(tppComponentName, tppComponentType);
        try {
            obj=cs.getDynamicComponent(cqDesc, false);
        } catch (AcsJContainerServicesEx ex) {
            msg = "Cannot create a total-power processor object.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        }

        tppProc=NewTPPHelper.narrow(obj);
        tppName=tppProc.name();

        // Set the distributer in the TPP
        try {
                tppProc.setDistributer(bulkDataDistributorName);
        } catch (DistributerNAEx ex) {
                msg = "Distributer " +bulkDataDistributorName+" not available";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (AlreadyShutDownEx ex) {
                msg = tppComponentName+" already shut down";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        }

        {

            try {
                tppProc.configureArray(array.getAntennas());
            } catch (AlreadyShutDownEx ex) {
                msg = "Cannot start total-power data production from array " + array.getArrayName();
                throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
            }

        }

        
		
		try {
			
			AntennaCallbackImpl antennaCallbackConsumer = new AntennaCallbackImpl(
					cs);
			for (Entry<String, TotalPower> entry : antennaModeController
					.entrySet()) {

				antennaCallbackConsumer.addExpectedResponse(entry.getKey());

				entry.getValue().setDataConsumerAsynch(tppComponentName,
						antennaCallbackConsumer.getExternalInterface());
			}
			//FIXME: Put a real timeout.
			antennaCallbackConsumer.waitForCompletion(60);
			
			AntennaCallbackImpl antennaCallbackAcquisition;
			antennaCallbackAcquisition = new AntennaCallbackImpl(cs);
			for (Entry<String, TotalPower> entry : antennaModeController
					.entrySet()) {

				antennaCallbackAcquisition.addExpectedResponse(entry.getKey());

				entry.getValue().beginDataAcquisitionAsynch(
						antennaCallbackAcquisition.getExternalInterface());
			}
			
			//FIXME: Put a real timeout.
			antennaCallbackAcquisition.waitForCompletion(60);
			
		} catch (AcsJContainerServicesEx ex) {
			msg = "Could not create the callback";
			throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
		} catch (AcsJInvalidRequestEx ex) {
			msg = "Problem in the callback";
		    throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
		} catch (AcsJAsynchronousFailureEx ex) {
			msg = "Problem in the callback";
			throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
		} catch (AcsJTimeoutEx ex){
			msg = "Timeout waiting for the callback";
			throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
		}
		
		
		
/*		Iterator<String> iter = antennaModeController.keySet().iterator();
		while (iter.hasNext()) {
			String an = iter.next();
			TotalPower tpMC = antennaModeController.get(an);
			try {
				devLog.finer("Starting total-power data production from antenna "
						+ an);
				tpMC.setDataConsumer(tppComponentName);
				tpMC.beginDataAcquisition();
			} catch (UnallocatedEx ex) {
				msg = "Cannot start total-power data production from antenna "
					+ an;
				throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
			} catch (HardwareFaultEx ex) {
				msg = "Cannot start total-power data production from antenna "
					+ an;
				throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
			}
		}*/
	}

	/**
	 * Destroy the total power processor offshoot.
	 */
    @Override
	public void cleanUp() {
        opLog.info("Square-law controller shutting down.");
        Iterator<String> iter = antennaModeController.keySet().iterator();
        while (iter.hasNext()) {
            String an = iter.next();
            devLog.info("Stopping total-power data production from antenna "
                        + an);
            antennaModeController.get(an).endDataAcquisition();
            devLog.info("Stopped total-power data production from antenna "
                        + an);
        }
        // Clean and release the total power processor
        devLog.info("Shutting down the total-power processor");
        tppProc.shutdown();
        devLog.info("Releasing the  total-power processor component");
        cs.releaseComponent(tppName);

        allTp = null;
        subscanBoundries = null;
        ssi = null;
        sequence = 0;
        opLog.info("Square-law controller shut down.");
    }

    /**
     * Set the integration duration in the total power processor.
     *
     * If the total power processor is shutdown an exception is caught and logged.
     * No other indication that the method failed will be available.
     *
     * @param duration a double with the duration in seconds? @TODO check if this is correct.
     */
    public void setIntegrationDuration(double duration) {
        devLog.fine("setIntegrationDuration: " + duration);
        try {
                tppProc.setIntegrationDuration(duration);
        } catch (AlreadyShutDownEx ex) {
                logger.severe("Error setting integration duration");
        }
    }

    /**
     * Gets the integration duration that has been configured in 
     * the total power.
     *
     * If the total power processor is shutdown an exception is caught and logged.
     * In this case this method will return the default value 1.
     *
     * @return the duration in seconds? @TODO Check if this is correct.
     *
     */
    public double getIntegrationDuration() {
        try {
                return tppProc.getIntegrationDuration();
        } catch (AlreadyShutDownEx ex) {
                logger.severe("Error getting integration duration");
        }
        return 1;
    }
	/**
	 * Configure the square law controller with the timing boundaries and the scan durations.
	 * This member does not actively do anything, it just stores a copy parameters and 
	 * set the subscan sequence number count to -1. 
	 * @param allTp
	 * @param subscanBoundries
	 * @param ssi
	 */
	public void configureSubscanSequence(TotalPowerConfiguration[] allTp, SubscanBoundries[] subscanBoundries, SubscanInfo[] ssi){
		this.allTp = allTp;
		this.subscanBoundries = subscanBoundries;
		this.ssi = ssi;
		this.sequence = -1;
	}
	
 	/**
	 * Command the Total power processor to begin a subscan and return. 
	 * @throws DeviceBusyEx thrown by tppOffshoot.beginSubscan
	 * @throws IllegalConfigurationEx thrown by tppOffshoot.beginSubscan
	 */
	public void waitForSubscanStart() throws AlreadyShutDownEx, NoBasebandEx, DeviceBusyEx, NoAntennasEx{
		if (allTp == null || subscanBoundries == null ||ssi == null || sequence == -2){
			//@TODO throw an IllegalConfigurationEx exception.
	        
		}
		    sequence++;
			tppProc.setIntegrationDuration(allTp[sequence].integrationDuration*100E-9);
			tppProc.beginSubscan(ssi[sequence]);
	}

	/**
	 * Wait for the subscan to finish by commanding the Total power processor to wrap up the current subscan.
	 * @throws IFProcClientTimeOutEx thrown by tppOffshoot.subscanComplete
	 * @throws BulkSenderEx thrown by tppOffshoot.subscanComplete
	 * @throws ProcessingThreadInterruptedEx thrown by tppOffshoot.subscanComplete
	 */
	public int waitForSubscanEnd() throws NoBasebandEx, AlreadyShutDownEx{
		final long endTime = ssi[sequence].endTime;
        final long now = ObservingModeUtilities.getCurrentACSTime();
        tppProc.subscanComplete((int) ((endTime - now)*100E-9 + 2.0));
        devLog.fine("Time out is: " + ((endTime - now)*100E-9 + 2.0));
        IntHolder dataProduced = new IntHolder();
        DoubleHolder integrations = new DoubleHolder();
        IntHolder flaggedData = new IntHolder();
        tppProc.getDataInfo(dataProduced, integrations, flaggedData);
        String msg = "Number of integrations: " + integrations.value;
        msg += " Number of data: " + dataProduced.value;
        msg += " Number of flagged data: " + flaggedData.value;
        devLog.debug(msg);
        // TODO. Understand why integrations is a double.
        return (int) integrations.value;
	}
	
    static public double extractSLIntegrationDuration(
            SpectralSpecT spectralSpec, Logger logger)
            throws IllegalParameterErrorEx {
        SquareLawSetupT sq = spectralSpec.getSquareLawSetup();
        if (sq == null) {
            String msg = "Spectral specification does not contain a square law detector setup.";
            ObservingModeUtilities.throwIllegalParameterErrorEx(msg,
                    new AcsJIllegalParameterErrorEx(), logger);
        }
        return ObservingModeUtilities.convertTimeToSeconds(sq.getIntegrationDuration());
    }

}
