// @(#) $Id$
//
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2009 - 2011
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

package alma.Control.ObservingModes;

import alma.AntennaMotionPatternMod.AntennaMotionPattern;
import alma.CalDataOriginMod.CalDataOrigin;
import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.CalibrationFunctionMod.CalibrationFunction;
import alma.CalibrationSetMod.CalibrationSet;
import alma.Control.AbortionException;
import alma.Control.ArrayMountController;
import alma.Control.AstronomicalHolographyOperations;
import alma.Control.Band2Band3Overlap;
import alma.Control.Completion;
import alma.Control.CorrelatorCalibrationSequenceSpec;
import alma.Control.DSBbasebandSpec;
import alma.Control.FocusCalResult;
import alma.Control.InterferometryController;
import alma.Control.LocalOscillator;
import alma.Control.PointingCalResult;
import alma.Control.SSBbasebandSpec;
import alma.Control.ScanIntentData;
import alma.Control.SimpleCallback;
import alma.Control.SourceOffset;
import alma.Control.SourceOffsetFrame;
import alma.Control.SubscanBoundries;
import alma.Control.TowerHolography;
import alma.Control.Common.OperatorLogger;
import alma.Control.Common.Util;
import alma.ControlCommonExceptions.AsynchronousFailureEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.CorrErr.InvalidArrayEx;
import alma.Correlator.CalibrationInformation;
import alma.Correlator.SubscanInformation;
import alma.Correlator.SubscanTiming;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.ObservingModeExceptions.AntennasDisagreeEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.PolarizationTypeMod.PolarizationType;
import alma.ScanIntentMod.ScanIntent;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.container.ContainerServices;
import alma.acs.logging.AcsLogger;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.entity.xmlbinding.schedblock.ACACorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.BLCorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.SpectralSpecTChoice;
import alma.xmlentity.XmlEntityStruct;

public class AstronomicalHolographyImpl extends
        ObservingModeBaseImpl<TowerHolography> implements
        AstronomicalHolographyOperations {

    private InterferometryControllerImpl intController = null;
    private LocalOscillatorImpl localOscillator = null;
    private PointingSubarrayControllerImpl amcs = null;

    // The correlator configuration ID. This observing mode uses only one ID but
    // as all the function calls to the observation control interface need an
    // array I'll use an array.
    int[] corrConfId = new int[]{-1}; // A negative value means unset
    int corrCalID = -1;
    
    // These variable contain the "state" of the antenna motion
    private double subscanDuration = -1; // Must be set by the user
    private double subscanSpeed = 5.0 / 60 / 180 * Math.PI; // 5 arcmin/sec
    private boolean azimuthSubscans = true;
    private int nextSubscanDirection = 1; // increasing angle
    private boolean bidirectionalSubscans = false;

    // The current field source and spectral spec. These are set by the
    // setSource and setSpectrum functions and sent to data capture at the start
    // of each sub-scan sequence.
    // Initialize these fields to null and this means they have not been set.
    // This will prevent sub-scans from running.
    FieldSourceT currentSrc = null; 
    SpectralSpecT currentSpec = null;
    double integrationDuration = -1; // A negative value means unset

    public AstronomicalHolographyImpl(ContainerServices cs,
            ObservingModeArray array) throws ObsModeInitErrorEx {
        super(cs, array);
        String msg;
        try { // This big try block catches all exceptions and cleans up before
            // re-throwing the exception. This ensures all references are
            // released.
            opLog.info("Astronomical holography observing mode starting up.");

            // Check we have at least two antennas
            if (array.getAntennas().length < 2) {
                msg = "Need an array with at least two antennas for astronomical holography.";
                throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx());
            }

            // Create the controllers used by this observing mode
            amcs = new PointingSubarrayControllerImpl(cs, array);
            localOscillator = new LocalOscillatorImpl(cs, array);
            intController = new InterferometryControllerImpl(cs, array);

            // Enable fringe and delay tracking
            opLog.fine("Enabling delay compensation and fringe tracking.");
            try {
                localOscillator.enableDelayTracking(true);
                localOscillator.enableFringeTracking(true);
            } catch (HardwareFaultEx ex) {
                msg = "Error enabling delay compensation and fringe tracking.";
                throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
            }
            
            devLog.fine("Completed starting the astronomical holography observing mode.");
        } catch (ObsModeInitErrorEx ex) {
            cleanUp();
            throw ex;
        }
    }

    @Override
    public void cleanUp() {
        opLog.info("Astronomical holography observing mode shutting down.");
        // clean up the correlator resources
        if (intController != null) {
            intController.cleanUp();
            intController = null;
            devLog.finer("Completed deactivating the interferometery controller.");
        }

        if (localOscillator != null) {
            localOscillator.cleanUp();
            localOscillator = null;
            devLog.finer("Completed deactivating the local oscillator.");
        }

        if (amcs != null) {
            amcs.cleanUp();
            amcs = null;
            devLog.finer("Completed shutting down the pointing subarray controller.");
        }

        super.cleanUp();
        devLog.fine("Completed shutting down the astronomical holography observing mode.");
    }

    private ScanIntentData[] astroHolographyScanType() {
        ScanIntentData[] intent = new ScanIntentData[1];
        intent[0] = new ScanIntentData();
        intent[0].scanIntent = ScanIntent.MAP_ANTENNA_SURFACE;
        intent[0].calDataOrig = CalDataOrigin.CHANNEL_AVERAGE_CROSS;
        intent[0].calSet = CalibrationSet.UNSPECIFIED;
        intent[0].calFunction = CalibrationFunction.UNSPECIFIED;
        intent[0].antennaMotionPattern = AntennaMotionPattern.UNSPECIFIED;
        intent[0].onlineCalibration = false;
        return intent;
    }

    public void beginScan() throws ObservingModeErrorEx, DataCapturerErrorEx {
        super.beginScan(intController.addWVRIntent(astroHolographyScanType()));
    }

    public void setReferenceAntennas(String[] antennaList)
            throws ObsModeInitErrorEx, IllegalParameterErrorEx {
        String msg;

        amcs.createSubarray("Reference", antennaList);

        // Check that there is at least one antenna being measured
        if (amcs.getSubarray("Default").getAntennas().length == 0) {
            msg = "Must have at least one antenna being measured.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }

        msg = "Reference antennas are: ";
        for (String ant : amcs.getSubarray("Reference").getAntennas()) {
            msg += ant + " ";
        }

        msg += "Antennas being measured are: ";
        for (String ant : amcs.getSubarray("Default").getAntennas()) {
            msg += ant + " ";
        }
        opLog.info(msg);
    }

    public void setTarget(String source, String spectrum)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        try {
            setSource(source);
        } catch (TimeoutEx ex) {
            String msg = "Timout setting the source.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        setSpectrum(spectrum);
    }

    public void setSpectrum(String spectrum)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        currentSpec = null; // Set this to non-null if no exceptions are thrown
        integrationDuration = -1;
        String msg;
        // Tune the receiver
        SpectralSpecT spectralSpec = ObservingModeUtilities.deserializeSpectralSpec(
                spectrum, logger);
        spectralSpec = localOscillator.alignSpectralSpec(spectralSpec);
        localOscillator.setSpectrumAsync(spectralSpec);
        // TODO. Wait until locked
        // Tell all the mounts about the new frequency. This may change the
        // pointing and move the subreflector.
        final double freq = localOscillator.averageSkyFrequency();
        amcs.setObservingFrequency(freq);

         try {
            // Clear all configurations
            intController.clearAllIds();
            corrConfId[0] = -1;

            // Load our spectral spec into the correlator and get the
            // configuration ID
            corrConfId = intController.loadConfigurations(new SpectralSpecT[]{spectralSpec});
            if (corrConfId.length != 1 || corrConfId[0] < 0) {
                msg = "Could not get a configuration ID from the correlator.";
                msg += " loadConfigurations returned an ID of [";
                for (int i : corrConfId) {
                    msg += i + " ";
                }
                msg += "]";
                throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
            }
        } catch (InvalidArrayEx ex) {
            msg = "Cannot configure the correlator.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }

        // Calibrate the correlator if we are "on-source" i.e., if
        // setSource has been executed.
        if (currentSrc != null) {
            calibrateCorrelator();
        }
 
        // Get the integration duration out of the spectral spec. Its used to
        // adjust the subscan duration (if necessary).
        SpectralSpecTChoice ssc = spectralSpec.getSpectralSpecTChoice();
        if (ssc.getBLCorrelatorConfiguration() != null) {
            BLCorrelatorConfigurationT blcc = ssc.getBLCorrelatorConfiguration();
            integrationDuration = ObservingModeUtilities.convertTimeToSeconds(blcc.getIntegrationDuration());
        } else if (ssc.getACACorrelatorConfiguration() != null) {
            ACACorrelatorConfigurationT acacc = ssc.getACACorrelatorConfiguration();
            integrationDuration = ObservingModeUtilities.convertTimeToSeconds(acacc.getIntegrationDuration());
        }
        // TODO. Check the integration duration is a multiple of 48ms (and that
        // its not -1)

        // Only keep the new values if there were no exceptions thrown
        subscanDuration = alignSubscanDuration(subscanDuration,
                integrationDuration, logger);
        currentSpec = spectralSpec;
    }

    public void setSubscanDuration(double lengthInRad, double speedInRadPerSec)
            throws IllegalParameterErrorEx {
        String msg;
        if (lengthInRad < 1. / 60 / 60 / 180 * Math.PI
                || lengthInRad > 10. / 180 * Math.PI) {
            msg = "The subscan length must be between 1 arc-sec";
            msg += " and 10 degrees.";
            msg += " You specified a length of " + lengthInRad * 180 / Math.PI
                    + " degrees.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        if (speedInRadPerSec < 1. / 60 / 60 / 180 * Math.PI
                || speedInRadPerSec > 1. / 180 * Math.PI) {
            msg = "The scanning speed must be between 1 arcsec/sec and 1 deg/sec";
            msg += " You specified a speed of " + speedInRadPerSec * 60 * 60
                    * 180 / Math.PI + " arcsec/sec.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        double timeInSec = lengthInRad / speedInRadPerSec;
        subscanDuration = alignSubscanDuration(timeInSec, integrationDuration,
                logger);
        subscanSpeed = speedInRadPerSec;
    }

    public void doSubscans(double[] perpendicularOffsetsInRadians)
            throws IllegalParameterErrorEx, ObservingModeErrorEx,
            AbortionException {
        String msg;

        final int numOffsets = perpendicularOffsetsInRadians.length;
        if (numOffsets == 0) {
            msg = "You have not specified the perpendicular offset.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        if (numOffsets > 1000) {
            msg = "You have specified too many perpendicular offsets.";
            msg += " The limit is 1000 and you specified " + numOffsets;
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        for (int i = 0; i < numOffsets; i++) {
            double offset = perpendicularOffsetsInRadians[i];
            if (Math.abs(offset) > 10.0 / 180 * Math.PI) {
                msg = "The magnitude of the perpendicular offset is too big.";
                msg += " The limit is 10 degrees and you specified " + offset
                        * 180 / Math.PI + " degrees.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
            }
        }
        // Check that the setSubscanDuration function has been called
        if (subscanDuration < 0) {
            msg = "You have not specified the sub-scan duration.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        // Now check there is at least one antenna being measured.
        if (!amcs.subarrayExists("Reference")) {
            msg = "You have not specified the reference antennas.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        // Now check if the user has called setSource and setSpectrum
        if (currentSrc == null) {
            msg = "You have not specified which source to use.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        if (currentSpec == null) {
            msg = "You have not specified which frequency and correlator configuration to use.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }

        try {
            // Setup as much of data capture as possible before we work out when
            // the sub-scans actually start
            int[] subscanNumbers = new int[numOffsets];
            SubscanIntent[] scanningIntent = new SubscanIntent[1];
            scanningIntent[0] = SubscanIntent.SCANNING;
            SubscanIntent[][] allIntents = new SubscanIntent[numOffsets][];
            XmlEntityStruct[] allSrcs = new XmlEntityStruct[numOffsets];
            SourceOffset zeroOffset = new SourceOffset(0.0, 0.0, 0.0, 0.0, SourceOffsetFrame.HORIZON);
            SourceOffset[] allOffsets = new SourceOffset[numOffsets];

            XmlEntityStruct thisSrc = ObservingModeUtilities.serializeFieldSource(currentSrc, devLog.getLogger());
            XmlEntityStruct[] allSpecs = new XmlEntityStruct[numOffsets];
            XmlEntityStruct thisSpec = ObservingModeUtilities.serializeSpectralSpec(currentSpec, devLog.getLogger());
            final int firstSubscanNumber = array.getSubScanNumber() + 1;
            for (int s = 0; s < numOffsets; s++) {
                subscanNumbers[s] = firstSubscanNumber + s;
                allIntents[s] = scanningIntent;
                allSrcs[s] = thisSrc;
                allOffsets[s] = zeroOffset;
                allSpecs[s] = thisSpec;
            }
            final String arrayName = array.getArrayName();
            dataCapturer.startSubscanSequence(arrayName, allIntents);
            dataCapturer.setSubscanSequenceSource(arrayName, allSrcs, allOffsets);
            dataCapturer.setSubscanSequenceSpectrum(arrayName, allSpecs);

            // Work out the antenna movement (strokes) for all sub-scans
            double[] longOffsets = new double[numOffsets];
            double[] latOffsets = new double[numOffsets];
            double[] longVelocity = new double[numOffsets];
            double[] latVelocity = new double[numOffsets];
            double[] subscanDirections = new double[numOffsets];
            ArrayMountControllerImpl movingAnts = amcs.getSubarray("Default");
            for (int s = 0; s < numOffsets; s++) {
                double offset = perpendicularOffsetsInRadians[s];
                longOffsets[s] = offset;
                latOffsets[s] = offset;
                longVelocity[s] = 0;
                latVelocity[s] = 0;
                subscanDirections[s] = nextSubscanDirection;
                double subscanOffset = subscanSpeed * subscanDuration / 2
                        * nextSubscanDirection;
                double subscanVel = subscanSpeed * nextSubscanDirection;
                if (azimuthSubscans) {
                    longOffsets[s] = -subscanOffset;
                    longVelocity[s] = subscanVel;
                } else {
                    latOffsets[s] = -subscanOffset;
                    latVelocity[s] = subscanVel;
                }
                if (bidirectionalSubscans) {
                    nextSubscanDirection *= -1;
                }
                // Get the antennas moving to the starting offset. The queuing
                // of the strokes will happen later once we know how much time
                // the correlator needs between each sub-scan.
                // TODO. Decide if we really need to do this.
                movingAnts.setHorizonOffsetAsync(longOffsets[0], latOffsets[0]);
            }

            // Now Work out the sub-scan boundaries
            // 1. Get the correlator leadTimes for all subscans
            int[] calIDs = new int[numOffsets];
            long[] subscanDurations = new long[numOffsets];
            final long subscanDurationInACS = Util.roundTE(Util.toTimeInterval(subscanDuration));

            for (int s = 0; s < numOffsets; s++) {
                calIDs[s] = corrCalID;
                subscanDurations[s] = subscanDurationInACS;
            }
            SubscanTiming[] sst = intController.getReconfigurationTimes(calIDs, subscanDurations);
            for (int s = 0; s < sst.length; s++){
                msg = "Initial sst[" + s 
                    + "] setUptime:" + sst[s].setupTime*100E-9
                    + " duration:" + sst[s].duration*100E-9
                    + " processing time:" + sst[s].processingTime*100E-9;
                devLog.fine(msg);
            }
            if (sst.length != numOffsets) {
                // TODO. Throw an exception
            }
            
            // 2. Find out how long it will take the antenna to get "on-source".
            // This is just for the first sub-scan in the sequence.
            long antSlewTime = Util.toTimeInterval(amcs.timeToSource());
            // Compute the sub-scan start and stop times relative to an, as yet
            // unknown, start time for the sequence.
            SubscanInformation[] ssi = new SubscanInformation[numOffsets];
            long endOfLastSubscan = 0;
            final int scanNumber = array.getScanNumber();
            long[] strokeStartTimes = new long[numOffsets];
            SubscanBoundries[] subscanBoundries = new SubscanBoundries[numOffsets];
            for (int s = 0; s < numOffsets; s++) {
                long leadTime = Util.ceilTE(Math.max(antSlewTime,
                        sst[s].setupTime));
                msg = "Ant slew time: " + antSlewTime*100E-9
                    + " corr setup time: "  + sst[s].setupTime*100E-9
                    + " lead Time: "  + leadTime * 100E-9;
                devLog.fine(msg);
                long thisStartTime = endOfLastSubscan + leadTime;
                long thisStopTime = thisStartTime + subscanDurationInACS;
                subscanBoundries[s] = new SubscanBoundries(thisStartTime,
                        thisStopTime);
                // No need to add code to put the ACU into the right mode
                // as Nick Emerson tells me this is automatically done by the ACU
                strokeStartTimes[s] = thisStartTime - leadTime;
                msg = " Subscan boundries [ " + thisStartTime*100E-9
                    + ", "  + thisStopTime*100E-9
                    + " ] stroke start time: "  +  strokeStartTimes[s] * 100E-9;
                devLog.fine(msg);
                if (azimuthSubscans) {
                    longOffsets[s] -= subscanSpeed * leadTime * 100E-9
                            * subscanDirections[s];
                } else {
                    latOffsets[s] -= subscanSpeed * leadTime * 100E-9
                            * subscanDirections[s];
                }
                sst[s].setupTime = leadTime;
                sst[s].duration = subscanDurationInACS;
                ssi[s] = new SubscanInformation(scanNumber, subscanNumbers[s],
                        corrCalID, sst[s]);
                // For each subsequent stroke assume it takes 1.5 seconds to get
                // on-source.
                // TODO. This *will* need refinement as its assumes:
                // 1. Bidirectional sub-scans
                // 2. The antennas meet spec!
                antSlewTime = Util.toTimeInterval(1.536);
                endOfLastSubscan = thisStopTime;
                msg = " Final sst[" + s 
                    + "] setUptime:" + sst[s].setupTime*100E-9
                    + " duration:" + sst[s].duration*100E-9
                    + " processing time:" + sst[s].processingTime*100E-9;
                devLog.fine(msg);
            }

            // TODO. Reduce this number as much as possible. The requires
            // testing with real hardware.
            final long commsOverhead = Util.toTimeInterval(0.192*2 + 0.002*numOffsets);

            // From here until the startSubscanSequence function call is
            // received by the correlator we only have less than the
            // commsOverhead-96ms i.e, a fraction of a second
            // So keep the code in this time critical section lightweight
            {
                long startOfTimeCriticalSection = ObservingModeUtilities.getCurrentACSTime();
                final long corrStartTime = Util.ceilTE(startOfTimeCriticalSection + commsOverhead);
                
                long[] subscanStartTime = new long[numOffsets];
                long[] subscanEndTime = new long[numOffsets];
                for (int s = 0; s < numOffsets; s++) {
                    subscanBoundries[s].startTime += corrStartTime;
                    subscanBoundries[s].endTime += corrStartTime;
                    strokeStartTimes[s] += corrStartTime;
                    movingAnts.setHorizonLinearStrokeQueued(longVelocity[s],
                            latVelocity[s], longOffsets[s], latOffsets[s],
                            strokeStartTimes[s]);
                    subscanStartTime[s] = subscanBoundries[s].startTime;
                    subscanEndTime[s] = subscanBoundries[s].endTime;
                    msg = " Subscan boundries [ " + Util.acsTimeToString(subscanBoundries[s].startTime)
                        + ", "  + Util.acsTimeToString(subscanBoundries[s].endTime)
                        + " ]. Stroke start time: " +  Util.acsTimeToString(strokeStartTimes[s]);
                    devLog.fine(msg);
                }
                // Tell DC when the sub-scan starts and stops
                dataCapturer.setSubscanSequenceTiming(arrayName, subscanStartTime, subscanEndTime);
                // Kick off the correlator
                msg = "Corr start time: " + Util.acsTimeToString(corrStartTime);
                devLog.fine(msg);
                intController.startSubscanSequence(corrStartTime, ssi);
                // Start the collection of pointing data.
                amcs.startSubscanSequence(subscanBoundries);
                // Queue a command to stop at the boresight after the end of the
                // last stroke
                movingAnts.setHorizonLinearStrokeQueued(0, 0, 0, 0,
                        subscanBoundries[numOffsets - 1].endTime);
                long timeCriticalDuration = ObservingModeUtilities.getCurrentACSTime() - startOfTimeCriticalSection;
                msg = "Time critical section took "
                        + String.format("%.3f", timeCriticalDuration*100E-9)
                        + " seconds.";
                if (timeCriticalDuration > commsOverhead - Util.toTimeInterval(0.096)) {
                    devLog.severe(msg + " This is too long.");
                } else {
                    devLog.fine(msg + " This is OK.");
                }
            }
            // Now its all setup and we just have to wait for each subscan to execute
            for (int s = 0; s < numOffsets; s++) {
                // Wait for the correlator to start the next subscan
                intController.waitForSubscanStart();
                // Send the weather data to DC and publish the subscan started
                // event.
                final long thisSubscanStartTime = subscanBoundries[s].startTime;
                super.beginSubscan(scanningIntent, thisSubscanStartTime,
                        subscanDurationInACS);
                // For each sub-scan send the sub-reflector position to data
                // capture
                amcs.waitForSubscanStart();
                // Send the state table (and eventually the receiver table) to
                // data capture
                localOscillator.beginSubscan();

                // Now wait for the sub-scan to end
                intController.waitForSubscanEnd();
                // send the pointing data to data capture.
                amcs.waitForSubscanEnd();
                // Publish the sub-scan ended event and send weather data to
                // data capture
                super.endSubscan(subscanBoundries[s].endTime, Completion.SUCCESS);
            }
            // Tell Data capture the sequence of sub-scans has ended
            // TODO handle cases where the sequence fails or is aborted
            dataCapturer.endSubscanSequence(arrayName, subscanNumbers);
        } catch (TableUpdateErrorEx ex) {
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataErrorEx ex) {
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataCapturerErrorEx ex) {
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }

    public void setAzimuthSubscans() {
        azimuthSubscans = true;
    }

    public void setElevationSubscans() {
        azimuthSubscans = false;
    }

    public void setUniDirectionalSubscans(boolean increasing) {
        nextSubscanDirection = increasing ? 1 : -1;
        bidirectionalSubscans = false;
    }

    public void setBiDirectionalSubscans() {
        bidirectionalSubscans = true;
    }

    public void doPhaseCal(double timeInSec) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, AbortionException {
        String msg;

        if (timeInSec < 0.048 || timeInSec > 60.0) {
            msg = "The subscan duration must be between 48ms and 60 seconds.";
            msg += " You specified a duration of " + timeInSec + " seconds.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }

        // Now check if the user has called setSource and setSpectrum
        if (currentSrc == null) {
            msg = "You have not specified which source to use.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }
        if (currentSpec == null) {
            msg = "You have not specified which frequency and correlator configuration to use.";
            throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        }

        try { // Now do the sub-scan
            final SubscanIntent[] intents = new SubscanIntent[] { SubscanIntent.REFERENCE };
            final long duration = Util.toTimeInterval(alignSubscanDuration(
                    timeInSec, integrationDuration, logger));

            // Start moving the antennas toward the boresight
            amcs.getSubarray("Default").setHorizonOffsetAsync(0, 0);

            // let data capture know the subscan has started
            final String arrayName = array.getArrayName();
            final int subscanNumber = array.getSubScanNumber() + 1;
            {
                SubscanIntent[][] allIntents = new SubscanIntent[][] { intents };
                dataCapturer.startSubscanSequence(arrayName, allIntents);
                XmlEntityStruct[] allSrcs = new XmlEntityStruct[1];
                SourceOffset zeroOffset = new SourceOffset(0.0, 0.0, 0.0, 0.0, SourceOffsetFrame.HORIZON);
                SourceOffset[] allOffsets = new SourceOffset[]{zeroOffset};
                allSrcs[0] = ObservingModeUtilities.serializeFieldSource(
                        currentSrc, devLog.getLogger());
                dataCapturer.setSubscanSequenceSource(arrayName, allSrcs, allOffsets);
                XmlEntityStruct[] allSpecs = new XmlEntityStruct[1];
                allSpecs[0] = ObservingModeUtilities.serializeSpectralSpec(
                        currentSpec, devLog.getLogger());
                dataCapturer.setSubscanSequenceSpectrum(arrayName, allSpecs);
             }

            // Work out the sub-scan lead time and the sub-scan boundaries
            // Get the correlator leadTime.
            SubscanTiming[] sst = intController.getReconfigurationTimes(
                    new int[] { corrCalID }, new long[] { duration });
            final long corrLeadTime = Util.ceilTE(sst[0].setupTime);
            sst[0].setupTime = corrLeadTime;
            sst[0].duration = duration;
            SubscanInformation[] ssi = new SubscanInformation[1];
            ssi[0] = new SubscanInformation(array.getScanNumber(), subscanNumber, corrCalID, sst[0]);

            // Find out how long it will take the antenna to get "on-source"
            final long antSlewTime = Util.toTimeInterval(amcs.timeToSource());
            // Allow half a second for this observing mode to get the
            // startSubscanSequence function called in the correlator
            final long leadTime = Math.max(antSlewTime, corrLeadTime)
                    + Util.toTimeInterval(0.5);
            // From here until we call startSubscanSequence we only have less
            // than half a second.
            final long subscanStartTime = Util.ceilTE(ObservingModeUtilities.getCurrentACSTime()
                    + leadTime);
            final long subscanStopTime = subscanStartTime + duration;
            final long corrStartTime = subscanStartTime - corrLeadTime;
 
            // Now tell the correlator to start the subscan
            intController.startSubscanSequence(corrStartTime, ssi);

            // Start the collection of pointing data
            amcs.beginSubscan(subscanStartTime, subscanStopTime);

            { // tell data capture about the subscan timings
                long[] allStartTimes = new long[] { subscanStartTime };
                long[] allStopTimes = new long[] { subscanStopTime };
                dataCapturer.setSubscanSequenceTiming(arrayName, allStartTimes,
                        allStopTimes);
            }      
            
            // Wait for the subscan to start
            intController.waitForSubscanStart();
            
            // Calling the base class will check for the abort flag,
            // increment the subscan number, and send the weather data to
            // data capture
            super.beginSubscan(intents, subscanStartTime, duration);

            // Send the state table to data capture
            localOscillator.beginSubscan();

            // Now wait for the sub-scan to end
            intController.waitForSubscanEnd();

            // send the pointing data to data capture.
            amcs.waitForSubscanEnd();

            // Publish the sub-scan ended event and send weather data to
            // data capture
            super.endSubscan(subscanStopTime, Completion.SUCCESS);

            // Tell Data capture the sequence of sub-scans has ended
            // TODO handle cases where the sequence fails or is aborted
            int[] successfulSubscans = new int[] { subscanNumber };
            dataCapturer.endSubscanSequence(arrayName, successfulSubscans);
        } catch (TableUpdateErrorEx ex) {
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataErrorEx ex) {
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataCapturerErrorEx ex) {
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }

    static double alignSubscanDuration(double proposed, double integration,
            AcsLogger logger) {
        double actual = proposed;
        String msg;
        if (proposed < 0) {
            return proposed;
        }
        if (integration > 0) {
            actual = Math.ceil(proposed / integration) * integration;
        }
        actual = Util.ceilTE(Util.toTimeInterval(actual)) * 100E-9;
        if ((Math.abs(actual - proposed) > .01) && (integration > 0)) {
            msg = "Duration of each sub-scan adjusted from "
                    + String.format("%.3f", proposed) + " to "
                    + String.format("%.3f", actual)
                    + " seconds as the sub-scan must be a multiple"
                    + " of the correlator integration time of "
                    + String.format("%.3f", integration) + " seconds";
            OperatorLogger.warning(msg, logger);
        }
        return actual;
    }

    private void calibrateCorrelator() throws ObservingModeErrorEx {
        String msg;
        SubscanTiming[] times = intController.getCalibrationReconfigurationTimes(corrConfId);
        CalibrationInformation[] ci = new CalibrationInformation[1];
        ci[0] = new CalibrationInformation(corrConfId[0], times[0]);

        final long leadTime = Util.toTimeInterval(0.5);
        final long startTime = Util.roundTE(ObservingModeUtilities.getCurrentACSTime()
                + leadTime);

        intController.startCalibrationSequence(startTime, ci);

        int[] allCalIDs = intController.waitForCalibration();
        if (allCalIDs.length != 1 || allCalIDs[0] < 0) {
            msg = "Could not get a calibration ID from the correlator.";
            msg += " The returned ID was [";
            for (int i : allCalIDs) {
                msg += i + " ";
            }
            msg += "]";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        corrCalID = allCalIDs[0];
    }

    // ---------- Interferometry Controller Methods -------------
    /// See the InterferometryControllerInterface.py for a description of this
    /// function
    public int[] loadConfigurations(String[] spectralSpecs)
        throws IllegalParameterErrorEx, ObservingModeErrorEx {
        return intController.loadConfigurations(spectralSpecs);
    }
    
    public int[] loadCalibrations(CorrelatorCalibrationSequenceSpec calSpecs)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        // This only works because we are, for now, requiring all sub-scans in a
        // sequence to use the same source.
        // TODO. Remove this restriction.
        try {
            if (calSpecs.FieldSourceList.length > 0) {
                setSource(calSpecs.FieldSourceList[0]);
            } else {
                amcs.setSource(currentSrc);
            }
        } catch (TimeoutEx ex) {
            String msg = "Cannot move the antennas to the specified calibration source.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        return intController.loadCalibrations(calSpecs);
    }

    /// See the InterferometryControllerInterface.py for a description of this
    /// function
    public void clearConfigurations(int[] configIDs) {
        intController.clearConfigurations(configIDs);
    }
 
    /// See the InterferometryControllerInterface.py for a description of this
    /// function
    public void clearCalibrations(int[] calIDs) {
        intController.clearCalibrations(calIDs);
    }
 
    // See the documentation for the InterferomeryControllerInterface for
    // description of these methods.
    public void setPhaseDirection(String fieldSource)
            throws IllegalParameterErrorEx {
        intController.setPhaseDirection(fieldSource);
    }

    public String getDelayServerName() {
        return intController.getDelayServerName();
    }

    public void disableWVRCalibration(boolean disable) {
        intController.disableWVRCalibration(disable);
    }

    public void applyWVRCalibration(ASDMDataSetIDL wvrCalib)
            throws ObservingModeErrorEx {
        intController.applyWVRCalibration(wvrCalib);
    }

    public void clearConfigurationIDs(int[] corrConfIDs)
            throws ObservingModeErrorEx {
        intController.clearConfigurationIDs(corrConfIDs);
    }

    public boolean isWVRSendingData() {
        return intController.isWVRSendingData();
    }

    public long getWVRintegrationTime() throws ObservingModeErrorEx {
        return intController.getWVRintegrationTime();
    }

    public InterferometryController getInterferometryController()
            throws ContainerServicesEx {
        return intController.getInterferometryController();
    }

    public void enableLLCCorrection(boolean enable)
            throws AsynchronousFailureEx {
        intController.enableLLCCorrection(enable);
    }

    public boolean isLLCCorrectionEnabled() throws AsynchronousFailureEx {
        return intController.isLLCCorrectionEnabled();
    }

    public long getTimeToLLCReset() throws AsynchronousFailureEx {
        return intController.getTimeToLLCReset();
    }

    public void resetLLCStretchers() throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        intController.resetLLCStretchers();
    }

    public void setLLCStretcherPosition(float position)
            throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        intController.setLLCStretcherPosition(position);
    }

    public boolean getLLCPolarizationCalRequired() throws AsynchronousFailureEx {
        return intController.getLLCPolarizationCalRequired();
    }

    public void doLLCPolarizationCalibrationAsynch(int timeout,
            boolean forceCalibration, SimpleCallback cb) {
        intController.doLLCPolarizationCalibrationAsynch(timeout,
                forceCalibration, cb);
    }

    // -------------- Array Mount Controller Interface Methods ----------------
    // See the documentation for the ArrayMountControllerInterface for
    // description of these methods. Except as noted below.

    // See the setSourceAsync function (below) for a description of this
    // function. Once the antenna is on-source the correlator is calibrated if
    // possible.
    public void setSource(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx, TimeoutEx {
        setSourceAsync(fieldSource);
        waitUntilOnSource();

        // TODO: Find a better solution for this
        // Now that we are on-source calibrate the correlator
        // (if a configuration is loaded)
        if (currentSpec != null && corrConfId[0] >= 0) {
            calibrateCorrelator();
        }
    }

    // For astronomical holography this function needs to control both the phase
    // center and the pointing center of the array. And the phase center is, for
    // both pointing subarrays, always set to the boresight.
    public void setSourceAsync(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        currentSrc = null; // Set this to non-null if no exceptions are thrown
        amcs.setSourceAsync(fieldSource);

        // Now tell the delay server about the new source
        intController.setPhaseDirection(fieldSource);

        // If we got here then the source is OK.
        currentSrc = ObservingModeUtilities.deserializeFieldSource(
                fieldSource, logger);
        
        // Add the current RA/Dec to the field source to give an approximate
        // value for solar system objects. This is used by DC
        amcs.getArrayMountControllerImpl().fillRADec(currentSrc);
    }

    public void waitUntilOnSource() throws ObservingModeErrorEx, TimeoutEx {
        amcs.waitUntilOnSource();
    }

    public ArrayMountController getArrayMountController()
            throws ContainerServicesEx {
        return amcs.getArrayMountController();
    }

    public boolean isObservable(String fieldSource, double duration)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        return amcs.isObservable(fieldSource, duration);
    }

    public void setElevationLimit(double elevationLimit)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        amcs.setElevationLimit(elevationLimit);
    }

    public double getElevationLimit() throws ObservingModeErrorEx {
        return amcs.getElevationLimit();
    }

    public void resetLimits() {
        amcs.resetLimits();
    }

    public void applyPointingCalibrationResult(PointingCalResult[] result,
            PolarizationType polarization, String[] antennas)
            throws IllegalParameterErrorEx {
        amcs.applyPointingCalibrationResult(result, polarization, antennas);
    }

    public void applyFocusCalibrationResult(FocusCalResult[] result,
            PolarizationType polarization, String[] antennas)
            throws IllegalParameterErrorEx {
        amcs.applyFocusCalibrationResult(result, polarization, antennas);
    }

    // ------------ Local Oscillator Interfaces -------------------
    // See the documentation for the InterferomericLocalOscillatorInterface for
    // description of these methods. Except as noted below.
    public void enableDelayTracking(boolean enable) throws HardwareFaultEx {
        localOscillator.enableDelayTracking(enable);
    }

    public boolean delayTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        return localOscillator.delayTrackingEnabled();
    }

    public void enableFringeTracking(boolean enable) throws HardwareFaultEx {
        localOscillator.enableFringeTracking(enable);
    }

    public boolean fringeTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        return localOscillator.fringeTrackingEnabled();
    }

    public void optimizeSignalLevels(float ifTargetLevel, float bbTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeSignalLevels(ifTargetLevel, bbTargetLevel);
    }

    public void optimizeIFSignalLevels(float ifTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeIFSignalLevels(ifTargetLevel);
    }

    public void optimizeBBSignalLevels(float bbTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeBBSignalLevels(bbTargetLevel);
    }

    public short setFrequency(SSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, HardwareFaultEx {
        short nSols = localOscillator.setFrequency(bbspec, overlap);
        // The observing frequency is used by the mount to adjust the
        // subreflector & pointing (refraction correction and pointing model).
        double mean = localOscillator.averageSkyFrequency();
        amcs.setObservingFrequency(mean);
        return nSols;
    }

    public short setFrequencyDSB(DSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            ObservingModeErrorEx,HardwareFaultEx {
        short nSols = localOscillator.setFrequencyDSB(bbspec, overlap);
        // The observing frequency is used by the mount to adjust the
        // subreflector & pointing (refraction correction and pointing model).
        double mean = localOscillator.averageSkyFrequency();
        amcs.setObservingFrequency(mean);
        return nSols;
    }

    public double[] getFrequency() throws HardwareFaultEx, AntennasDisagreeEx {
        return localOscillator.getFrequency();
    }

    public void setCalibrationDevice(CalibrationDevice device)
            throws ObservingModeErrorEx, TimeoutEx {
        localOscillator.setCalibrationDevice(device);
    }

    public CalibrationDevice getCalibrationDevice() throws AntennasDisagreeEx {
        return localOscillator.getCalibrationDevice();
    }

    // TODO This needs work
    public void setSpectrumAsync(String spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        localOscillator.setSpectrumAsync(spectralSpec);
    }

    public double averageSkyFrequency() {
        return localOscillator.averageSkyFrequency();
    }

    public void optimizeSASPolarizationAsynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPolarizationAsynch(forceCalibration, cb);
    }

    public void optimizeSASPol1Asynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPol1Asynch(forceCalibration, cb);
    }

    public void optimizeSASPol2Asynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPol2Asynch(forceCalibration, cb);
    }

    public void setOffsetToRejectImageSideband(boolean rejectImage) 
	throws HardwareFaultEx
    {
	localOscillator.setOffsetToRejectImageSideband(rejectImage);
    }

    public LocalOscillator getLocalOscillator() throws ContainerServicesEx {
        return localOscillator.getLocalOscillator();
    }

    public String dopplerShift(String spectralSpec, 
                String dopplerSource, long epoch)
                    throws ObservingModeErrorEx, IllegalParameterErrorEx {
        return localOscillator.dopplerShift(spectralSpec, dopplerSource, epoch);
    }
}
