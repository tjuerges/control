#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2009, 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import CCL.SIConverter
import CCL.LocalOscillatorInterferometricInterface
import CCL.ArrayMountControllerInterface
import CCL.InterferometryControllerInterface
import CCL.ObservingModeBase

class AstronomicalHolography\
          (CCL.ObservingModeBase.ObservingModeBase,
           CCL.LocalOscillatorInterferometricInterface.LocalOscillatorInterferometricInterface,
           CCL.ArrayMountControllerInterface.ArrayMountControllerInterface,
           CCL.InterferometryControllerInterface.InterferometryControllerInterface):
    """
    This class implements the CCL interface to the astronomical
    holography observing mode.
    """
    
    def __init__(self, offshoot, logger):
        CCL.ObservingModeBase.ObservingModeBase.__init__(self)
        CCL.LocalOscillatorInterferometricInterface.LocalOscillatorInterferometricInterface.__init__(self, offshoot, logger)
        CCL.ArrayMountControllerInterface.ArrayMountControllerInterface.__init__(self, offshoot, logger)
        CCL.InterferometryControllerInterface.InterferometryControllerInterface.__init__(self, offshoot, logger)
        self._logger = logger
        self._offshoot = offshoot

    def setReferenceAntennas(self, antennaList):
        """
        Specify the antennas that will not move with each
        sub-scan. These are the antennas that will *not* have their
        surface measured and all other antennas in the array will have
        their surface measured. All antennas in this list must be part
        of the array, there must be at least one antenna in this list,
        at least one antenna in the array that is not in this list. If
        not an illegal parameter error exception is thrown
        """
        # Convert to a list if necessary.
        if (type(antennaList) != type([])):
            antennaList = antennaList.split()
        return self._offshoot.setReferenceAntennas(antennaList);

    def setSource(self, source):
        """
        Specify the source to be used for all subsequent
        subscans. This function moves all antennas in the array to
        point at this source before returning. If there is any problem
        with the specified configuration an illegal parameter error
        exception is thrown. If there is any problem moving the
        antennas an observing mode error exception is thrown.
        """
        return self._offshoot.setSource(source.getXMLString());

    def isObservable(self, source, duration=0.0):
        """
        Returns true if the specified source would be observable on
        all antennas at this instant in time. This really means 'is
        the elevation of this planet above the elevation limit of the
        antenna'. See the setPlanet function for a description of the
        allowed planet names.
        """
        durationInSec = CCL.SIConverter.toSeconds(duration)

        return self._offshoot.isObservable(source.getXMLString(), durationInSec)

    def setSpectrum(self, spectrum):
        """
        Specify the receiver and correlator configuration to be used
        for all subsequent subscans. This function tunes the LO's,
        receiver and configures and calibrates the correlator before
        returning. To do this correctly the antennas should first be
        tracking the source. If there is any problem with the
        specified configuration an illegal parameter error exception
        is thrown. If there us any problem configuring the hardware an
        observing mode error exception is thrown.
        """
        return self._offshoot.setSpectrum(spectrum.getXMLString());

    def setTarget(self, source, spectrum):
        """
        Specify the source and spectrum to be used for all subsequent
        sub-scans. This function is prefered to separately calling
        setSource and setSpectrum as it will, eventually, lock the
        LO's and tune the receivers while the antennas are slewing. At
        the moment it just serially calls setSource and
        setSpectrum. It throws the same exceptions as the setSource
        and setSpectrum functions.
        """
        return self._offshoot.setTarget(source.getXMLString(),
                                        spectrum.getXMLString());
    
    def setSubscanDuration(self, length, speed):
        """
        Specify the length of each subscan (row or column) in the
        raster. The subscan will always be centered about the current
        source and the perpendicular offset is specified in the
        doSubscan function. An illegal parameter exception is thrown
        if the length is less than 1 arc-second or greater than 10
        degrees.  An illegal parameter exception if the speed is less
        than 1 arc-sec/min or greater than 1 deg/sec.
        """
        lengthInRad = CCL.SIConverter.toRadians(length)
        speedInRadPerSec = CCL.SIConverter.toRadiansPerSecond(speed)
        return self._offshoot.setSubscanDuration(lengthInRad, speedInRadPerSec);
    
    def doSubscans(self, perpendicularOffsets):
        """
        Do one or more subscans. The parameters of the subscan must
        previously have been specified using the setSubscanDuration
        function.  These parameters along with each perpendicular
        offset specified in this function are used to fully define a
        subscan. This function will do one one subscan for each
        perpendicular offset.
        * An illegal parameter exception is thrown if this sequence is
        zero length or has more than 1000 offsets. Each element in the
        sequence must be an angle, whose magnitude is less than 10
        degrees. This exception is also thrown if this function is
        called before the setAntennasToMeasure function has been
        executed.
        * An observing mode error exception is thrown if there is any
        problem collecting the data. This includes all hardware
        errors.
        * An abortion exception is thrown, and the subscan aborted, if
        the user has pressed the stop button.
        This function waits for all subscans to complete.
        """
        # Convert to a list if necessary.
        if (type(perpendicularOffsets) != type([])):
            perpendicularOffsets = [perpendicularOffsets]
        perpendicularOffsetsInRad = []
        for offset in perpendicularOffsets:
            perpendicularOffsetsInRad.append(CCL.SIConverter.toRadians(offset))
        return self._offshoot.doSubscans(perpendicularOffsetsInRad);
    
    def setAzimuthSubscans(self):
        """
        Specify that all subsequant subscans are to move the antenna in
        azimuth. The elevation will be largely constant. Azimuth subscans
        are the default. 
        """
        return self._offshoot.setAzimuthSubscans()
    
    def setElevationSubscans(self):
        """
        Specify that all subsequant subscans are to move the antenna
        in elevation. The azimuth will be largely constant.
        """
        return self._offshoot.setElevationSubscans()

    def setUniDirectionalSubscans(self, increasing=True):
        """
        Specify that all subscans are to move the antenna in the same
        direction. The relevant angle will increase, as the subscan
        progresses, if the increasing parameter is
        True. Unidirectional subscans with increasing angle are the
        default.
        """
        return self._offshoot.setUniDirectionalSubscans(increasing)
    
    def setBiDirectionalSubscans(self):
        """
        Specify that consecutive subscans are to move the antenna in
        opposite directions.
        """
        return self._offshoot.setBiDirectionalSubscans()
    
    def doPhaseCal(self, time):
        """
        Move all antennas to the boresight and collect data for the
        specified amount of time.
        * An illegal parameter exception is thrown if time its less
        than 48ms or greater than 1 minute. This exception is also
        thrown if this function is called before the
        setAntennasToMeasure function has been executed.
        * An observing mode error exception is thorwn if there is any
        problem collecting the data. This includes all hardware
        errors.
        * An abortion exception is thrown, and the subscan aborted, if
        the user has pressed the stop button.
        This function waits for all subscans to complete.
        """
        timeInSec = CCL.SIConverter.toSeconds(time)
        return self._offshoot.doPhaseCal(timeInSec)

    def beginScan(self):
        """
        This function marks the beginning of a set of
        subscans. Normally all subscans needed to produce a map of the
        antenna surface are grouped within one scan so that the data
        reduction software can process them together. By grouping
        subscans one data set (ASDM) can contain the data needed to
        produce a number of maps. The beginScan function should be
        called before either the doSubscans or doPhaseCal functions
        are called. The complementary endSubscan function should be
        called when all subscans have been completed.  This function
        will throw an observing mode error exception if a scan boundry
        cannot be established.
        """
        return self._offshoot.beginScan()

    def endScan(self):
        """
        This function marks the end of of a set of subscans. See the
        beginScan function for more details. Unlike the beginscan
        function this function will not throw any exceptions.
        """
        return self._offshoot.endScan()
