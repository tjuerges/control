#!/usr/bin/env python
# @(#) $Id$
#******************************************************************************
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#

import Acspy.Clients.SimpleClient
import xml.dom.ext.reader.PyExpat
import glob
import xmlentity
import Control
import CCL.Array


def setupProject():
    """
    You cannot call the begin/end execution functions without having a
    project (APDM). This function sets one up and configures the IDL
    simulator to implement the behaviour, in schduling and data
    capture thats essential for these functions to work.

    Returns: A SB UID
    """
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    archconn =  client.getComponent("ARCHIVE_CONNECTION")
    archop = archconn.getOperational("ManualModeTest")
    archid =  client.getComponent("ARCHIVE_IDENTIFIER")
    reader =  xml.dom.ext.reader.PyExpat.Reader()
    ids = {}

    files = glob.glob('Project/*.prj')
    for fname in files:
        xmlfile = open(fname)
        xmlstring = unicode(xmlfile.read())
        xmlfile.close()
        uids = archid.getUIDs(1)

        # Get the entity name and remove the prefix
        dom = reader.fromString(xmlstring)
        entName = str(dom._get_childNodes()[0]._get_name()).split(':')
        entName = entName[len(entName)-1]
#        print "Storing ", entName, "; UID: ", uids[0]
        ids[entName] = uids[0]

        # Store the XML
        archop.store(xmlentity.XmlEntityStruct\
                     (xmlstring, uids[0], entName, '1.0', '0L'))

    # Configure data capture to send an ASDM archived event when the
    # execution ends. Otherwise the array waits for many (10) minutes
    # before timing out.
    simulator = client.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0")
    code =  "import offline, asdmIDLTypes, Control\n";
    code += "dcName = SELF.getName()\n";
    code += "arrayName = dcName.split('/')[1]\n"
    code += "sessionID = asdmIDLTypes.IDLEntityRef('uid://X0/X0/X0', 'X0', 'Session', '1')\n";
    code += "schedBlockID = asdmIDLTypes.IDLEntityRef('" + ids['SchedBlock'] + "', 'X0', 'SchedBlock', '1')\n";
    code += "bddsi = Control.BDDStreamInfo('', 0, 0, 0)\n";
    code += "DCID = offline.DataCapturerId(dcName, arrayName , sessionID, schedBlockID, bddsi, bddsi)\n";
    code += "ASDMID = asdmIDLTypes.IDLEntityRef('uid://X0/X0/X0', 'X0', 'ASDM', '1')\n";
    code += "event = offline.ASDMArchivedEvent(DCID, 'complete', ASDMID, 0L)\n";
    code += "supplyEventByInstance(dcName, 'CONTROL_SYSTEM', event)\n";
    code += "None";
    simulator.setMethodIF("IDL:alma/offline/DataCapturer:1.0", \
                          "endSB", code, 0)

    return ids["SchedBlock"]

def oldsetupProject():
    """
    You cannot call the begin/end execution functions without having a
    project (APDM). This function sets one up and configures the IDL
    simulator to implement the behaviour, in schduling and data
    capture thats essential for these functions to work.

    Returns: A SB UID
    """
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    archconn =  client.getComponent("ARCHIVE_CONNECTION")
    archop = archconn.getOperational("ManualModeTest")
    archid =  client.getComponent("ARCHIVE_IDENTIFIER")
    reader =  xml.dom.ext.reader.PyExpat.Reader()
    ids = {}
    
    files = glob.glob('Project/*.prj')
    for fname in files:
        xmlfile = open(fname)
        xmlstring = xmlfile.read()
        xmlfile.close()
        uids = archid.getUIDs(1)
        
        # Get the entity name and remove the prefix
        dom = reader.fromString(xmlstring)
        entName = str(dom._get_childNodes()[0]._get_name()).split(':')
        entName = entName[len(entName)-1]
#        print "Storing ", entName, "; UID: ", uids[0]
        ids[entName] = uids[0]
        
        # Store the XML
        archop.store(xmlentity.XmlEntityStruct\
                     (xmlstring, uids[0], entName, '1.0', '0L'))

    # Configure scheduling to return an entity reference (session ID)
    # when the startManualModeSession function is called as part of
    # the array.beginExecution function.
    simulator = client.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0")
    code =  "import asdmIDLTypes\n"
    code += "asdmIDLTypes.IDLEntityRef('uid://X0/X0/X0', '', 'Session', '1')"
    simulator.setMethod("SCHEDULING_MASTERSCHEDULER", "startManualModeSession",
                        code, 0)

    # Configure data capture to send an ASDM archived event when the
    # execution ends. Otherwise the array waits for many (10) minutes
    # before timing out.
    code =  "import offline, asdmIDLTypes, Control\n";
    code += "dcName = SELF.getName()\n";
    code += "arrayName = dcName.split('/')[1]\n"
    code += "sessionID = asdmIDLTypes.IDLEntityRef('uid://X0/X0/X0', 'X0', 'Session', '1')\n";
    code += "schedBlockID = asdmIDLTypes.IDLEntityRef('" + ids['SchedBlock'] + "', 'X0', 'SchedBlock', '1')\n";
    code += "bddsi = Control.BDDStreamInfo('', 0, 0, 0)\n";
    code += "DCID = offline.DataCapturerId(dcName, arrayName , sessionID, schedBlockID, bddsi, bddsi)\n";
    code += "ASDMID = asdmIDLTypes.IDLEntityRef('uid://X0/X0/X0', 'X0', 'ASDM', '1')\n";
    code += "event = offline.ASDMArchivedEvent(DCID, 'complete', ASDMID, 0L)\n";
    code += "supplyEventByInstance(dcName, 'CONTROL_SYSTEM', event)\n";
    code += "None";
    simulator.setMethodIF("IDL:alma/offline/DataCapturer:1.0", \
                          "endSB", code, 0)

    return ids["SchedBlock"]

def configureObservationControl():
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    simulator = client.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0")
    
    code  = "LOGGER.logInfo('loadConfigurations called')\n";
    code += "numConfigs = len(parameters[1])\n"
    code += "allConfigs = range(10, 10 + numConfigs)\n"
    code += "LOGGER.logInfo('loadConfigurations returns ' + str(allConfigs))\n"
    code += "allConfigs"
    simulator.setMethod("CORR/OBSERVATION_CONTROL", \
                        "loadConfigurations", code, 0)

    code  = "LOGGER.logInfo('getCalibrationReconfigurationTimes called')\n"
    code += "import Correlator\n"
    code += "sst = Correlator.SubscanTiming(0L, 0L, 0L)\n"
    code += "allTimes = []\n"
    code += "for i in range(len(parameters[1])):\n"
    code += "  allTimes.append(sst)\n"
    code += "LOGGER.logInfo('getCalibrationReconfigurationTimes returns ' + str(allTimes))\n"
    code += "allTimes"
    simulator.setMethod("CORR/OBSERVATION_CONTROL", \
                        "getCalibrationReconfigurationTimes", code, 0)

    code  = "LOGGER.logInfo('startCalibrationSequence called')\n"
    code += "import Correlator, ACSErr\n"
    code += "cb = parameters[3]\n" 
    code += "numCals = len(parameters[2])\n"
    code += "calIds = []\n"
    code += "completions = []\n"
    code += "c = ACSErr.Completion(0, 0, 0, [])\n"    
    code += "for i in range(numCals):\n"
    code += "  calIds.append(i + 100)\n"
    code += "  completions.append(c)\n"
    code += "LOGGER.logInfo('startCalibrationSequence is calling cb.report(' + str(calIds) + ', ' + str(completions) +')')\n"
    code += "cb.report(calIds, completions)\n"
    code += "None"
    simulator.setMethod("CORR/OBSERVATION_CONTROL", "startCalibrationSequence", code, 0)

    code  = "LOGGER.logInfo('getReconfigurationTimes called')\n"
    code += "import Correlator\n"
    code += "sst0= Correlator.SubscanTiming(65280000L, 0L, 10080000L)\n"
    code += "sst1= Correlator.SubscanTiming(25440000L, 0L, 10080000L)\n"
    code += "allTimes = []\n"
    code += "for i in range(len(parameters[1])):\n"
    code += "  if i == 0:\n"
    code += "    allTimes.append(sst0)\n"
    code += "  else:\n"
    code += "    allTimes.append(sst1)\n"
    code += "LOGGER.logInfo('getReconfigurationTimes returns ' + str(allTimes))\n"
    code += "allTimes"
    simulator.setMethod("CORR/OBSERVATION_CONTROL", \
                        "getReconfigurationTimes", code, 0)

    code  = "LOGGER.logInfo('startSubscanSequence called')\n"
    code += "import Correlator, ACSErr, time, Acspy.Common.TimeHelper, Acspy.Common.EpochHelper, acstime, Acspy.Clients.SimpleClient\n"
    code += "startTime = parameters[1]\n"
    code += "timeNow = Acspy.Common.TimeHelper.getTimeStamp().value\n"
    code += "if (startTime < timeNow):\n"
    code += "  msg = 'Subscan start time of '\n"
    code += "  eph = Acspy.Common.EpochHelper.EpochHelper(startTime)\n"
    code += "  msg += eph.toString(acstime.TSArray, '%H:%M:%S.%3q', 0, 0)\n"
    code += "  msg += ' is in the past. Time now is '\n"
    code += "  eph = Acspy.Common.EpochHelper.EpochHelper(timeNow)\n"
    code += "  msg += eph.toString(acstime.TSArray, '%H:%M:%S.%3q', 0, 0)\n"
    code += "  LOGGER.logWarning(msg)\n"
    code += "cb = parameters[5]\n" 
    code += "numSubscans = len(parameters[4])\n"
    code += "c = ACSErr.Completion(0, 0, 0, [])\n"    
    code += "nextEventTime = startTime\n"
    code += "arrayName = parameters[0]\n"
    code += "client = Acspy.Clients.SimpleClient.PySimpleClient()\n"
    code += "sscd = Correlator.SubScanCorrelatorData('uid://X0/X1/X1', 'uid://X0/X1/X2', 'uid://X0/X1/X3', 1, 1, False, False, 128, 256, False, 64, 10, 1L, 1L)\n"
    code += "for i in range(numSubscans):\n"
    code += "  scanNum = parameters[4][i].scanNumber\n"
    code += "  subscanNum = parameters[4][i].subScanNumber\n"
    code += "  setupTime = parameters[4][i].subscanTime.setupTime\n"
    code += "  nextEventTime += setupTime\n"
    code += "  timeNow = Acspy.Common.TimeHelper.getTimeStamp().value\n"
    code += "  if (nextEventTime > timeNow):\n"
    code += "    timeToSleep = (nextEventTime - timeNow)*100E-9\n"
    code += "    LOGGER.logInfo('Sleeping for ' + str(timeToSleep) + ' seconds')\n"
    code += "    time.sleep(timeToSleep)\n"
    code += "  LOGGER.logInfo('startSubscanSequence is calling cb.subscanStarted(' + str(scanNum) + ', ' + str(subscanNum) + ')')\n"
    code += "  cb.subscanStarted(scanNum, subscanNum)\n"
    code += "  duration = parameters[4][i].subscanTime.duration\n"
    code += "  nextEventTime += duration\n"
    code += "  if (i+1 == numSubscans):\n"
    code += "    processingTime = parameters[4][i].subscanTime.processingTime\n"
    code += "    nextEventTime += processingTime\n"
    code += "  timeNow = Acspy.Common.TimeHelper.getTimeStamp().value\n"
    code += "  if (nextEventTime > timeNow):\n"
    code += "    timeToSleep = (nextEventTime - timeNow)*100E-9\n"
    code += "    LOGGER.logInfo('Sleeping for ' + str(timeToSleep) + ' seconds')\n"
    code += "    time.sleep(timeToSleep)\n"
    code += "  LOGGER.logInfo('startSubscanSequence is calling cb.subscanEnded(' + str(scanNum) + ', ' + str(subscanNum) + ', ' + str(sscd) + ', ' + str(c) +')')\n"
    code += "  cb.subscanEnded(scanNum, subscanNum, sscd, c)\n"
    code += "None"
    simulator.setMethod("CORR/OBSERVATION_CONTROL", "startSubscanSequence", code, 0)

def configureArray():
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    ms = client.getComponent('SCHEDULING_MASTERSCHEDULER')
    master = client.getComponent('CONTROL/MASTER')
    arrayId = master.createManualArray(master.getAvailableAntennas(),master.getAvailablePhotonicReferences(),Control.BL);
    array = CCL.Array.Array(arrayId.arrayName)
    TestingUtils.configureObservationControl();
    sbId = TestingUtils.setupProject()
    array._array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))
