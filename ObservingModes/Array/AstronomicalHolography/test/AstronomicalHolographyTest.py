#!/usr/bin/env python
# @(#) $Id$
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2009 - 2011
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

import shutil
shutil.rmtree("../lib/python/site-packages/Control__POA", True)
import sys
import unittest
import Acspy.Clients.SimpleClient
import ObservingModeExceptions
import maciErrTypeImpl
import Acspy.Common.ErrorTrace
import ControlExceptions
import ModeControllerExceptions
import TMCDB_IDL;
import asdmIDLTypes;
import Control
import Acspy.Common.Log
import CCL.Array
import math
import TestingUtils
import CCL.FieldSource, CCL.SpectralSpec

# **************************************************
# automated test class
# **************************************************
class automated( unittest.TestCase ):
        
    def test01( self ):
        '''
        This simply starts the astronomical holography offshoot and
        shuts it down again. It uses the real control system
        components (components from other subsystems are
        simulated). It checks the basic lifecycle of the offshoot.
        '''
        try:
            arrayId = None
            array = None
            try:
                arrayId = master.createAutomaticArray(\
                   master.getAvailableAntennas(),
                   master.getAvailablePhotonicReferences(),
                   Control.BL);
                array = client.getComponent(arrayId.arrayComponentName)
                self.failUnless(array != None,
                                "Unable to create an automatic array")
                array.beginExecution()
                ah = array.getAstronomicalHolographyOffshoot()
                self.failUnless(ah != None,
                                "Unable to create an astronomical holography observing mode")
            finally:
                if (array != None): client.releaseComponent(arrayId.arrayComponentName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx,
                ControlExceptions.IllegalParameterErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise

    def test02( self ):
        '''
        This tests the IDL interface.
        '''
        pass
        try:
            arrayId = None
            scheduling = None
            array = None
            try:
                sbId = TestingUtils.setupProject()
                TestingUtils.configureObservationControl();
                result = Control.FAIL
                amc = None
                arrayId = master.createManualArray(\
                   master.getAvailableAntennas(),
                   master.getAvailablePhotonicReferences(),
                   Control.BL);
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                array = client.getComponent(arrayId.arrayComponentName)
                self.failUnless(array != None,
                                "Unable to create a manual array")
                array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))
                array.beginExecution()
                ah = array.getAstronomicalHolographyOffshoot()
                self.failUnless(ah != None,
                                "Unable to create an astronomical holography observing mode")
                ah.beginScan()
                ah.setReferenceAntennas(['DA41'])
                mySource = CCL.FieldSource.EquatorialSource('1:2:3', '-85.6.7')
                mySpectrum = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()
                ah.setTarget(mySource.getXMLString(), mySpectrum.getXMLString())
                ah.setSource(mySource.getXMLString())
                ah.setSpectrum(mySpectrum.getXMLString())
                ah.setSubscanDuration(math.radians(1.24), math.radians(300./60/60))
                ah.setBiDirectionalSubscans()
                ah.doSubscans([math.radians(-.1), 0, math.radians(1)])
                ah.setAzimuthSubscans()
                ah.setElevationSubscans()
                ah.setUniDirectionalSubscans(True)
                ah.doPhaseCal(5)
                ah.endScan()
                result = Control.SUCCESS
            finally:
                if (array != None):
                    array.endExecution(result, "")
                    client.releaseComponent(arrayId.arrayComponentName)
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName)
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx,
                ControlExceptions.IllegalParameterErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise
        
    def test03( self ):
        '''
        This tests the CCL interface.
        '''
        pass
        try:
            arrayId = None
            scheduling = None
            array = None
            ah = None
            try:
                result = Control.FAIL
                arrayId = master.createManualArray(\
                   master.getAvailableAntennas(),
                   master.getAvailablePhotonicReferences(),
                   Control.BL);
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                array = CCL.Array.Array(arrayId.arrayName)
                self.failUnless(array != None,
                                "Unable to create a manual array")
                sbId = TestingUtils.setupProject()
                TestingUtils.configureObservationControl();
                array._array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))
                array.beginExecution()
                ah = array.getAstronomicalHolographyObservingMode()
                ah.beginScan()
                self.failUnless(ah != None,
                   "Unable to create an astronomical holography observing mode")
                ah.setReferenceAntennas('DA41')
                ah.setSubscanDuration('1.24deg', '300arcsec/s')

                mySpectrum = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()
                ah.setSpectrum(mySpectrum)

                moons = ['Io', 'Ganymede', 'Callisto',
                         'Pallas', 'Ceres', 'Juno', 'Vesta']
                i = 0; foundSource = False;
                while (i < len(moons) and not foundSource):
                    mySource = CCL.FieldSource.PlanetSource(moons[i])
                    if ah.isObservable(mySource, '10min'):
                        foundSource = True;
                    else:
                        i += 1;
                if (not foundSource):
                    mySource = CCL.FieldSource.EquatorialSource('1:2:3', '-85.6.7')
                    
                ah.setSource(mySource)
                ah.setTarget(mySource, mySpectrum)
                ah.doSubscans(0)
                ah.setAzimuthSubscans()
                ah.setElevationSubscans()
                ah.setUniDirectionalSubscans()
                ah.setBiDirectionalSubscans()
                ah.doSubscans(['-10arcmin', 0, '10arcmin'])
                ah.doPhaseCal('5s')
                ah.endScan()
                result = Control.SUCCESS
            finally:
                if (array != None): array.endExecution(result, "")
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx,
                ControlExceptions.IllegalParameterErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
                .log(Acspy.Common.Log.getLogger());
            raise
        
    def test04( self ):
        '''
        This executes the core of the standard astronomical hography
        observing mode script.
        '''
        try:
            arrayId = None
            scheduling = None
            array = None
            ah = None
            try:
                result = Control.FAIL
                arrayId = master.createManualArray(\
                   master.getAvailableAntennas(),
                   master.getAvailablePhotonicReferences(),
                   Control.BL);
                schedulingName = 'SCHEDULING/'+arrayId.arrayName
                scheduling = client.getComponent(schedulingName,
                                                 comp_idl_type="IDL:alma/scheduling/Array:1.0",
                                                 is_dynamic=True)
                array = CCL.Array.Array(arrayId.arrayName)
                self.failUnless(array != None,
                                "Unable to create a manual array")
                sbId = TestingUtils.setupProject()
                TestingUtils.configureObservationControl();
                array._array.configure(asdmIDLTypes.IDLEntityRef(sbId, "", "", ""))
                # various parameters that come from the SB
                channelAverageDuration = 0.24
                numSubscansPerCal = 3 # Typically 1
                subscanDuration = 30.0
                calDuration = 10.08
                minEl = 10.0
                subscanSize = math.radians(538.5/60./60.)
                referenceAntennas = array.antennas()[0] 
                oneWay = False # Typically True
                numSubscans = 5 # Typically 18
                integrationDuration = 10.08 
                horizonScan = True 

                array.beginExecution()
                spectralSpec = CCL.SpectralSpec.SpectralSpec().CreateBLCorrelatorSpec()
                src = CCL.FieldSource.EquatorialSource("1:2:3", "-80deg")
                ah = array.getAstronomicalHolographyObservingMode()
                ah.setElevationLimit(math.radians(minEl))
                if ah.isObservable(src, 600):
                    ah.beginScan()
                    
                    ah.setTarget(src, spectralSpec)

                    # Setup the overall parameters of the holography scan.
                    ah.setReferenceAntennas(referenceAntennas.split(','))

                    # in radians/s
                    subscanSpeed = subscanSize / subscanDuration
                    ah.setSubscanDuration(subscanSize, subscanSpeed)

                    if horizonScan:
                        ah.setAzimuthSubscans()
                    else:
                        ah.setElevationSubscans()

                    if oneWay:
                        ah.setUniDirectionalSubscans()
                    else:
                        ah.setBiDirectionalSubscans()

                    # Now do the subscans
                    subscansDone = 0
                    while subscansDone < numSubscans:
                        ah.doPhaseCal(calDuration)
                        numSubscansToDo = min(numSubscansPerCal, numSubscans-subscansDone);
                        perpOffsets =  [subscanSize * ((subscansDone + s + 0.5)/numSubscans - 0.5) for s in range(numSubscansToDo)]
                        ah.doSubscans(perpOffsets)
                        subscansDone += numSubscansToDo;

                    # Finish up with a phaseCal
                    ah.doPhaseCal(calDuration)
                    ah.endScan()

                # Reset the elevation limit
                ah.resetLimits()

                result = Control.SUCCESS
            finally:
                if (array != None): array.endExecution(result, "")
                if (scheduling != None): client.releaseComponent(schedulingName)
                if (arrayId != None): master.destroyArray(arrayId.arrayName);
        except (ObservingModeExceptions.ObsModeInitErrorEx,
                maciErrTypeImpl.CannotGetComponentExImpl,
                ModeControllerExceptions.MountFaultEx,
                ModeControllerExceptions.TimeoutEx,
                ControlExceptions.IllegalParameterErrorEx), ex:
            Acspy.Common.ErrorTrace.ErrorTraceHelper(ex.errorTrace)\
            .log(Acspy.Common.Log.getLogger());
            raise
        
# **************************************************
# MAIN Program
# **************************************************
if __name__ == '__main__':
        
# Bring the control subsystem operational
    client = Acspy.Clients.SimpleClient.PySimpleClient()
    master = client.getComponent('CONTROL/MASTER')
    shutdownMasterAtEnd = False;
    if (master.getMasterState() == Control.INACCESSIBLE):
        master.startupPass1()
        master.startupPass2()
        shutdownMasterAtEnd = True
    if (master.getMasterState() != Control.OPERATIONAL):
        print 'Could not bring the control subsystem to an operational state'
        exit(1)

# Now run the tests
    runner = unittest.TextTestRunner()
    if len( sys.argv ) > 1:
        suite = unittest.makeSuite(automated, sys.argv[1])
    else:
        suite = unittest.makeSuite(automated)
    runner.run(suite)

# Shutdown the control system if we started it up
    if shutdownMasterAtEnd:
        master.shutdownPass1()
        master.shutdownPass2()
    client.releaseComponent(master._get_name()) 
