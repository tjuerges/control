#! /usr/bin/env python
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# $Id$
#

# HOWTO use this module:
#
# > tat makeEnv
# ipython
# In [1]: import StandAloneTest as t
# In [2]: l=t.lo # l is now a ref to the CCL interface for the LO 
# In [3]: l.setFrequency(100e9, 101e9) # Example of what is available...
#
# NOTES
# Handy utilities here for SpectralSpec and its associates.
# See $INTROOT/idl/Schedblock.xsd for entity definitions.

import sys
import CCL
import CCL.DopplerServer

from Acspy.Clients.SimpleClient import PySimpleClient
from  Acspy.Common.EpochHelper import EpochHelper
import acstime
import random
from CCL.logging import getLogger
from CCL.APDMValueTypes import Frequency, Speed
from CCL.APDMSchedBlock import FrequencySetup
from CCL.BaseBandSpecification import BaseBandSpecification
from CCL.FieldSource import EphemerisSource
from CCL.FieldSource import EquatorialSource
from CCL.FieldSource import HorizonSource
from CCL.FieldSource import PlanetSource
from CCL.HorizonResultParser import HorizonResultParser
from CCL.LocalOscillator import LocalOscillator
from CCL.SpectralSpec import SpectralSpec
from ControlCommonCallbacks.SimpleCallbackImpl import SimpleCallbackImpl
import ObservingModeTester_idl
import TMCDBAccessIF_idl
import xmlentity

import pyxb
import APDMEntities.SchedBlock
import CCL.SchedBlock
import CCL.APDMSchedBlock
#from CCL.APDMSchedBlock.BLBaseBandConfig import BLBaseBandConfig
#from CCL.APDMSchedBlock.BLSpectralWindow import BLSpectralWindow

logger           = None
dopCompName      = "IDL:alma/Control/DopplerServer:1.0"
testutilCompName = "CONTROL/ObservingModeTester"
client           = None
dopserver        = None
testutil         = None
loimpl           = None
lo               = None
sim              = None
setupDone        = False

def startupString():
    t  ="import StandaloneTest as t; l=t.setUp(); ss,fs=t.mk(); "
    t += "align=t.align; ts=t.toString"
    return t

def setUp():
    global client, dopserver, testutil, loimpl, lo, logger, sim, setupDone
    if setupDone:
        print "Sorry, setup has already been done!"
        return
    client    = PySimpleClient()
    logger    = getLogger()
    dopserver = client.getDynamicComponent("myDopServer", dopCompName,
                None, None)
    testutil  = client.getComponent(testutilCompName)
    pyxb.namespace.builtin.XMLSchema_instance.ProcessTypeAttribute(
        pyxb.namespace.builtin.XMLSchema_instance.PT_skip)    
    pyxb.RequireValidWhenParsing(False)
    pyxb.RequireValidWhenGenerating(False)
    try:
      print "Modifying sim code..."
      # Insert methods into python simulator
      simidl = "IDL:alma/ACSSim/Simulator:1.0"
      sim = client.getDefaultComponent(simidl)
      testutil.modifySimulationCode(sim) 
      print "Done modifying sim code"
    except Exception, e:
      print "Exception modifying simulator code:\n" + str(e) 
      logger.info("Exception:" + str(e)) 
      raise
    loimpl    = testutil.getTestLocalOscillator()
    lo        = CCL.LocalOscillator.LocalOscillator(loimpl, logger)
    setupDone = True
    return lo

def cleanUp():
    global dopserver, testutil, sim
    client.releaseComponent(dopCompName)
    dopserver = None
    client.releaseComponent(testutilCompName)
    testutil = None
    client.releaseComponent(sim._get_name())
    sim = None

def align(ss):
    "Calls LO.alignSpectralSpec(ss), returning modified ss"
    ssstr = testutil.alignSpectralSpec(ss.getXMLString())
    sx    = spectralSpecStringToStruct(ssstr)
    if True:
        print toString(ss)
        print toString(sx)
    return sx
  
def test1() :
    s = dopserver.helloDoppler()
    match = (s == "I'm a shifty fellow!")
    print s, match
    return match

def makeAzelSource(sname="azelSource") :
    source  = HorizonSource('10.01.02', '60.30.02')
    source.sourceName = sname
    return source

def makeEquatSource(sname="equatSource", ra='01:02:03', dec='-22.50.7',
            v=123) :
    source  = EquatorialSource(ra, dec)
    source.sourceName = sname
    spd = Speed(v)
    spd.unit = 'km/s'
    vobj = source.sourceVelocity
    vobj.centerVelocity = spd
    vobj.dopplerCalcType = vobj.dopplerCalcType.RADIO
    vobj.referenceSystem = vobj.referenceSystem.lsrk
    return source

def makePlanetSource(sname="MARS") :
    source  = PlanetSource(sname)
    source.sourceName = sname
    return source

def makeEphemerisSource(fname="./IoEphem.txt", sname="IOephem") :
    fs  = EphemerisSource(sname, fname)
    fs.sourceName = sname
    fs.solarSystemObject = 'Ephemeris'
    fs.nonSiderealMotion = True
    fs.sourceEphemeris = readEphemFile(fname)
    return fs

def makeSpectralSpec(freq, sb="", reverse=False, numSpw=3) :
    """Input is a single frequency or list of frequencies in GHz.
    The reverse parameter controls reversing the order of baseband names
    to make sure it is handled correctly.
    The numSpw is the number of correlator spectral windows per baseband."""
    if list != type(freq) : freq = [freq]
    sspec = SpectralSpec()
    blcorconf = CCL.APDMSchedBlock.BLCorrelatorConfiguration()
    sspec.BLCorrelatorConfiguration = blcorconf
    blcorconf.BLBaseBandConfig = []
    sspec.FrequencySetup.hasHardwareSetup = False
    sspec.FrequencySetup.receiverBand  = "ALMA_RB_0%d" %(_bandNo(freq[0]))
    # Yes, baseband numbers start at 1 for the SpectralSpec and 0 for hardware
    i = 0
    for f in freq:
        i += 1
        bbi = i
        if reverse: bbi = len(freq)-i+1
        if f < 10: continue
        bs = BaseBandSpecification()
        bs.weighting = 100.0
        bs.baseBandName = 'BB_%d' %bbi
        bs.centerFrequency = Frequency(f*1e9)
        sbchar = 'N'
        sbpref = bs.sideBandPreference.NONE
        try:
            sbchar = sb[i-1]
        except:
            pass
        if sbchar == 'U':   sbpref = bs.sideBandPreference.USB
        elif sbchar == 'L': sbpref = bs.sideBandPreference.LSB
        bs.sideBandPreference = sbpref
        bs.frequencySwitching = random.choice([True,False])
        sspec.FrequencySetup.BaseBandSpecification.append(bs)
        blcorbb =  CCL.APDMSchedBlock.BLBaseBandConfig()
        blcorconf.BLBaseBandConfig.append(blcorbb)
        blcorbb.linkBaseBandSpecification(bs)
        spw = []
        for j in range(numSpw):
            spw.append(CCL.APDMSchedBlock.BLSpectralWindow())
        blcorbb.BLSpectralWindow = spw
    return sspec

def _bandNo(f):
    "Returns receiver band number as an integer, based on input freq (GHz)"
    tops = [45, 85,116,163,211,275,373,500,720,950]
    b = 1
    for t in tops:
        if f < t: return b
        b += 1
    return 10

def makeSpectralSpecHw(lo1=104, floog=31, tunehigh=True, lo2=8, 
        sb="ULUL", reverse=False) :
    """Make a hardware SpectralSpec.
    Parameters:
     lo1: in GHz
     floog: in MHz
     tunehigh: boolean for LO1 sideband
     lo2list: list of lo2 frequencies (up to 4) in GHz"""
    if list != type(lo2) : lo2 = [lo2]
    sspec = SpectralSpec()
    sspec.FrequencySetup.hasHardwareSetup = True
    sspec.FrequencySetup.receiverBand  = "ALMA_RB_0%d" %(_bandNo(lo1))
    sspec.FrequencySetup.lO1Frequency  = Frequency(lo1*1e9)
    sspec.FrequencySetup.floog         = Frequency(floog*1e6)
    sspec.FrequencySetup.tuneHigh      = tunehigh
    i = 0
    for f in lo2:
        i += 1
        bbi = i
        if reverse: bbi = len(lo2)-i+1
        if f < 0.00001: continue
        bs = BaseBandSpecification()
        sbp = sb[i-1] 
        if sbp == 'U':   bs.sideBandPreference = bs.sideBandPreference.USB
        elif sbp == 'L': bs.sideBandPreference = bs.sideBandPreference.LSB
        else :           bs.sideBandPreference = bs.sideBandPreference.NONE
        bs.weighting = 100.0
        bs.baseBandName = 'BB_%d' %bbi
        bs.lO2Frequency = Frequency(f*1e9)
        bs.use12GHzFilter = (f > 10.5)
        bs.frequencySwitching = random.choice([True,False])
        sspec.addBaseband(bs)
    #print sspec.toxml()
    #lo.setSpectrum(sspec.toxml())
    return sspec

def formatSpectralSpec(ss, check=True) :
    """Produce a nice string for a SpectralSpec"""
    probs = ""
    fs = ss.FrequencySetup
    eng = fs.hasHardwareSetup
    s  = "Frest:%7.3fGHz" %(fs.restFrequency.value())
    s += "  Tran:'%s'" %(fs.transitionName)
    s += "   HasHW:%s   dopplerRef:%s" %(eng, fs.dopplerReference)
    s += "\nRxBand:%s" %fs.receiverBand
    s += "  LO1:%7.3fGHz" %(1e-9*fs.lO1Frequency.value())
    if fs.floog != None: flg = "%6.3fMHz" %(1e-6*fs.floog.value())
    else : flg = "None"
    s += "  FLOOG:%s tuneHigh:%s" %(flg, fs.tuneHigh)
    probnl = "\n!!"
    if fs.receiverBand == "UNDEFINED":
        probs += probnl + "Receiver band is UNDEFINED!!!"
    if fs.restFrequency.unit != "Hz": 
        probs += probnl + "No units for restFrequency!!!"
    if fs.floog != None and fs.floog.unit != "Hz": 
        probs += probnl + "No units for floog!!!"
    # Now do each baseband
    nBasebands = len(fs.BaseBandSpecification)
    for i in range(nBasebands):
        b = fs.BaseBandSpecification[i]
        blcorconf = ss.BLCorrelatorConfiguration.BLBaseBandConfig[i]
        bbn = b.baseBandName
        s += "\n %s " %bbn
        f = b.centerFrequency
        if fs.dopplerReference == "topo": skyref = "sky"
        else : skyref = "rest"
        if f == None: fstr = "None"
        else : fstr = "%7.3fGHz" %(1e-9*f.value())
        s += " %s:%s" %(skyref, fstr)
        if b.useUSB == None: sbstr = "None"
        elif b.useUSB: sbstr = "USB"
        else :         sbstr = "LSB"
        s += " %-4s" %sbstr
        s += " %-4s" %b.sideBandPreference
        s += " wgt:%3.0f" %(b.weighting)
        spwstr = " spw:"
        for spw in blcorconf.BLSpectralWindow:
            if spw.sideBand == "NOSB":  spwstr += "N"
            elif spw.sideBand == "USB": spwstr += "U"
            elif spw.sideBand == "LSB": spwstr += "L"
            else:                       spwstr += "X"
        s += spwstr
        flo2 = b.lO2Frequency
        if flo2 == None: flo2str = "None"
        else : flo2str = "%6.3fGHz" %(1e-9*flo2.value())
        s += " LO2:" + flo2str
        s += " 12GHzFilter:%-5s" %(str(b.use12GHzFilter))
        s += " fSwitch:%s" %b.frequencySwitching
        if f != None and f.unit != "Hz":
            probs += probnl + "No units for %s centerFrequency!!!" %bbn
        if flo2 != None and flo2.unit != "Hz":
            probs += probnl + "No units for %s lo2Frequency!!!" %bbn
    if check: s += probs
    return s

def formatFieldSource(f):
    "Produce a nicely formatted string describing a FieldSource"
    def vu(valunit,p, w=0):
        total = w
        if w == 0: total = p+3
        return "%*.*f %s" %(total, p, valunit.value(), valunit.unit)
    s = "FieldSource "
    s = ""
    s += "Name: " + f.sourceName
    c = f.sourceCoordinates
    lon = c.longitude
    lat =  c.latitude
    v = f.sourceVelocity
    sp = v.centerVelocity
    s += "   dopCalcMode: %-11s" %v.dopplerCalcType
    if f.nonSiderealMotion:
        s += "\n Planetary Object"
    else:
        s += "\n coords: %-6s  %s  %s" %(c.system, vu(lon,5), vu(lat,5))
    s += "\n vel: %8.2f %s  %s" %(sp.value(), sp.unit, v.referenceSystem)
    return s

def toString(x) :
    """
    Turns objects into pretty strings.
    Types supported:
        SpectralSpec
        FieldSource
    """ 
    if isinstance(x, (CCL.SpectralSpec.SpectralSpec, 
                      CCL.APDMSchedBlock.SpectralSpec)) :
        return formatSpectralSpec(x)
    if isinstance(x, (CCL.FieldSource.EquatorialSource,
                      CCL.FieldSource.HorizonSource,
                      CCL.FieldSource.PlanetSource,
                      CCL.FieldSource.EphemerisSource)) :
        return formatFieldSource(x)
    if isinstance(x, str):
        sb = APDMEntities.SchedBlock.CreateFromDocument(x)
        return formatSpectalSpec(sb.SpectralSpec) 
    return str(x)

def handyCallback() :
    cb = SimpleCallbackImpl()
    return cb.getExternalInterface()    

def mk(callback=False) :
    """Create and return some handy objects.
    Usage example:
     import Standalone as t
     l = t.setUp()
     ss,fs,cb = t.mk(callback=True)
     l.setSpectrumAsync(ss,fs, cb)
     ss,fs = t.mk()
     l.setSpectrum(ss)
     l.dopplerShift(ss,fs,0)
    """
    if not callback:
        return makeSpectralSpec([650,651]), makeEquatSource()
    return makeSpectralSpec([650,651]), makeEquatSource(), handyCallback()

def test2():
    'Test enums and velocity'
    ss,fs = mk()
    print(toString(fs))
    vobj = fs.sourceVelocity
    vobj.dopplerCalcType = vobj.dopplerCalcType.OPTICAL
    print(toString(fs))
    vobj.dopplerCalcType = vobj.dopplerCalcType.RELATIVISTIC
    print(toString(fs))
    vobj.dopplerCalcType = vobj.dopplerCalcType.RADIO
    sp= vobj.centerVelocity
    sp.set(10000)
    sp.unit = 'm/s'
    vobj.centerVelocity = sp
    print("============================================================")
    ds(ss,fs,v=True)
    print(toString(fs))
    print("============================================================")
    vobj.referenceSystem = vobj.referenceSystem.bar
    sp.set(200)
    sp.unit = 'km/s'
    vobj.centerVelocity = sp
    ds(ss,fs,v=True)
    print(toString(fs))
    vobj.referenceSystem = vobj.referenceSystem.topo
    ds(ss,fs,v=True)
    vobj.referenceSystem = vobj.referenceSystem.hel
    try :
        ds(ss,fs,v=True)
    except Exception, x:
        if True:
            print("Exception caught in heli\n" + str(x))
            print("========================================================")
    vobj.referenceSystem = vobj.referenceSystem.lsr
    ds(ss,fs,v=True)
    vobj.referenceSystem = vobj.referenceSystem.lsrk
    ds(ss,fs,v=True)

def test3():
    'Test enums and velocity'
    ss,fs = mk()
    vobj = fs.sourceVelocity
    vobj.dopplerCalcType = vobj.dopplerCalcType.RADIO
    sp= vobj.centerVelocity
    sp.set(1)
    sp.unit = 'km/s'
    vobj.centerVelocity = sp
    print("============================================================")
    ds(ss,fs,v=True)
    print(toString(fs))
    print("============================================================")
    sp.set(300)
    sp.unit = 'km/s'
    vobj.centerVelocity = sp
    ds(ss,fs,v=True)

def test4(ra="00:00:00", dec='00.1.1'):
    'Test as function of time'
    ss,fs = mk()
    fs = makeEquatSource(ra=ra, dec=dec)
    print("Using ra=%s dec=%s" %(ra,dec))
    vobj = fs.sourceVelocity
    vobj.dopplerCalcType = vobj.dopplerCalcType.RADIO
    sp= vobj.centerVelocity
    sp.set(0)
    sp.unit = 'km/s'
    vobj.centerVelocity = sp
    print("=======================================================")
    print(toString(fs))
    print(toString(ss))
    print("=======================================================")
    eday0 = 55540.0 - acstime.EpochOriginInMJD
    import math
    longitude = 22.9 # deg
    diurnal = math.cos(longitude*math.pi/180)*(40000+12)/(86400.0-240)
    annual = 29.88*2*math.pi/365.25
    print("Max Topo change in day: %.3f km/sec" %(2*diurnal))
    print("Max Geo per day: %.3f km/sec" %(annual))
    m  = "The Geo & Topo vectors are only aligned on the soltice, "
    m += "when they point at 0h/12h RA and 0 DEC"
    print(m)
    acstimeUnitsPerDay = 86400*1e7
    for d in range(0,366,3):  
        vels = []
        eday = eday0 + d
        eh = EpochHelper(long(eday*acstimeUnitsPerDay))
        tString = eh.toString(acstime.TSUTC, "",0,0)[:10]
        for i in range(0,25,2):
            edayfrac = eday + i/24.0
            e = edayfrac*acstimeUnitsPerDay
            sx = ds(ss,fs,e, v=False)
            vels.append(extractShift(ss,sx))
        ave = sum(vels)/len(vels)
        r   = max(vels)-min(vels)
        print("%3d %s %7.3f %5.3f" %(d+1,tString, ave, r))

def _timeloop(ss, fs, incDays=10.0, numInc=4, mjd0=55540.0, offset=0,
        cmpdata=None) :
    eday0 = mjd0 - acstime.EpochOriginInMJD
    acstimeUnitsPerDay = 86400*1e7
    difflist = []
    if cmpdata != None: 
        print "         DATE         VEL(dop) VEL(ref)  DIFF(m/s)"
        dates = [c[0] for c in cmpdata]
    for x in range(numInc):
        d = x*incDays  
        eday = eday0 + d
        eh = EpochHelper(long(eday*acstimeUnitsPerDay))
        tString = eh.toString(acstime.TSUTC, "",0,0)[:16]
        toff = 0
        e = (eday+offset/86400.0)*acstimeUnitsPerDay 
        sx = ds(ss, fs, e, v=False)
        shift = extractShift(ss,sx)
        if cmpdata == None: 
            print("%3d %s %8.4f" %(d+1, tString, shift))
        else:
            try:
                i = dates.index(tString)
            except:
                print "Did not find:", tString, " in:", dates
                return
            cd = cmpdata[i]
            diff = shift-cd[4]
            difflist.append(diff)
            print("%3d %s %8.3f %8.3f    %5d" \
                %(d+1, tString, shift, cd[4], diff*1000)) 
    if cmpdata != None:
        maxdiff = max(difflist)*1000
        mindiff = min(difflist)*1000 
        print ("Diff range: %d  Max/Min: [%d, %d]" \
            %((maxdiff-mindiff), mindiff, maxdiff)) 

def test5(ra="00:00:00", dec='00.1.1'):
    'Sample different times, velocities, frames and calc modes'
    ss,fs = mk()
    fs = makeEquatSource(ra=ra, dec=dec)
    print("Using ra=%s dec=%s" %(ra,dec))
    vobj = fs.sourceVelocity
    vobj.dopplerCalcType = vobj.dopplerCalcType.RADIO
    sp= vobj.centerVelocity
    sp.set(0)
    sp.unit = 'km/s'
    vobj.centerVelocity = sp
    print("=======================================================")
    print(toString(fs))
    print(toString(ss))
    _timeloop(ss,fs, numInc=30, incDays=0.1)
    print("=======================================================")
    sp.set(200)
    sp.unit = 'km/s'
    vobj.centerVelocity = sp
    print(toString(fs))
    print(toString(ss))
    _timeloop(ss,fs)
    print("=======================================================")

def test6(planetName=['Mars', 'Mercury'], numInc=365, incDays=1, compare=True):
    """Computes doppler shift for planets at different times and optionally
    compares ALMA inferred velocity from the doppler calcs 
    to JPL Horizons file in Ephem directory. The daily differences for Mercury
    are [-9, 0] m/sec and for Mars are [-2,1] m/sec. The source of the larger
    differences for Mercury is unknown, expect that it is not the temporal
    resolution of the underlying casa ephemeris."""
    ss = makeSpectralSpec(230)
    for pn in planetName:
        fs = makePlanetSource(pn)
        data = None
        if compare:
            data = readEphemDataFile(pn + "Topocentric.txt")
        print("=======================================================")
        print(toString(fs))
        print(toString(ss))
        _timeloop(ss,fs,numInc=numInc, incDays=incDays, cmpdata=data)
        print("=======================================================")

def test7(planetName='Io', numInc=24*4, offset=0, compare=True):
    """Use ephemeris object (defined in IoEphem.txt) for test. 
    This ephemeris must have fairly good time resolution to give
    accurate results. With 6 minute spacing in time, the difference between
    the JPL Horizons velocities and the ALMA velocities every hour over an 
    interval of 4 days is [-2, +1] m/sec. With 1 hour ephemeris sampling 
    the differences increase to 50m/sec."""
    ss = makeSpectralSpec(230)
    fseph = makeEphemerisSource()
    data = None
    if compare :
        data = readEphemDataFile(planetName + "Topocentric.txt")
    print("=======================================================")
    #print(toString(fseph))
    print(toString(ss))
    _timeloop(ss,fseph, 1.000001/24, numInc=numInc, mjd0=55124, 
        offset=offset, cmpdata=data)
    print("=======================================================")

def test8():
    'Test a specific set of frequencies'
    ss = makeSpectralSpec(230)
    ss = makeSpectralSpec([9.049956702819205E1, 9.149956224397318E1,
        1.0249950961756558E2,1.0349950483334671E2])
    sx = align(ss)
    print(toString(ss))
    print(toString(sx))
    Fin = ss.FrequencySetup
    F   = sx.FrequencySetup
    i = 0
    for b in F.BaseBandSpecification:
        bbsign = 2*b.useUSB - 1  # Turns boolean into +/-1
        f= b.centerFrequency.value()
        fhw = F.lO1Frequency.value() + bbsign*(b.lO2Frequency.value()-3e9)
        df1 = f-Fin.BaseBandSpecification[i].centerFrequency.value()
        df2 = f-fhw
        print "Output-Input:%.1f   Output-SSLO1/LO2::%.1f" %(df1, df2)
        i += 1       
    return sx

def test9(numInc=40, compare=True):
    """Detailed check of Mercury. Referencing to JPL topocentric velocities,
    look at differences between internal casa planet ephemeris, and JPL 
    generated epherides with time resolution of 1 hour and 10 minutes. 
    The results show no difference (daily ephems showed much larger
    differences). Conclude the source of the ~10m/sec differences is not in
    the precision of the tables (and that underlying casa table must have 
    a sample interval of approximately one hour)."""
    ss = makeSpectralSpec(230)
    mjd0 = 55793
    print ("It takes a while to load the fine ephemeris...")
    fsephfine   = makeEphemerisSource("./Mercury10minEphem.txt", "Mercury")
    print ("Now loading coarse ephemeris...")
    fsephcoarse = makeEphemerisSource("./MercuryHourlyEphem.txt", "Mercury")
    print ("Ephemeris loading done")
    fsmerc = makePlanetSource("Mercury")
    data = None
    if compare :
        data = readEphemDataFile("MercuryTopocentric.txt")
    print(toString(ss))
    print("===================== Planet ==========================")
    _timeloop(ss,fsmerc, 1.0, numInc=numInc, mjd0=mjd0, cmpdata=data)
    print("==================== 10min Ephem ======================")
    _timeloop(ss,fsephfine, 1.0, numInc=numInc, mjd0=mjd0, cmpdata=data)
    print("=================== Hourly Ephem ======================")
    _timeloop(ss,fsephcoarse, 1.0, numInc=numInc, mjd0=mjd0, cmpdata=data)
    print("=======================================================")

def readEphemFile(f="./IoEphem.txt"):
        # Test File 1 uses UT HR:MN and Sexagesimal Positions
        # Delta is expressed in AU
        parser = HorizonResultParser(f)
        return parser.getEphemerisString()

def extractShift(ss, sx) :
    """Take the 1st bb freq of the before and after SpectralSpecs and calculate
    the apparent velocity"""
    def getf(s, b=0):
        f=s.FrequencySetup.BaseBandSpecification[b].centerFrequency.value()
        return f
    Frest  = getf(ss)
    Fsky   = getf(sx)
    #print Frest, Fsky
    v0= 299792*(Frest - Fsky)/Frest
    return 299792*(Frest*Frest - Fsky*Fsky)/(Frest*Frest + Fsky*Fsky)

def ds(ss, fs, epoch=0, v=False):
    epochLong = long(epoch)
    #sx = spectralSpecStringToStruct(lo.dopplerShift(ss, fs, epochLong))
    sx = lo.dopplerShift(ss, fs, epochLong)
    if v:
        print(toString(fs))
        print(toString(ss))
        print(toString(sx))
        print("V = %9.3f km/sec" %(extractShift(ss, sx)))
        print("==========================================================")
    return sx

def spectralSpecStringToStruct(ss) :
    """Takes a SpectralSpec xml string and converts it to a 
    SpectralSpecT (structure).
    The only way to do this is to wrap it inside a SchedBlock
    and then convert the SchedBlock xml to an entity and then
    take out the SpectalSpec Entity. Painful, but the converstion from
    XML string to entity apparently only exists for a SchedBlock."""
    s = """<?xml version="1.0" encoding="UTF-8"?>
        <sbl:SchedBlock xmlns:ent="Alma/CommonEntity"
           xmlns:sbl="Alma/ObsPrep/SchedBlock"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:type="SchedBlock">"""
    if isinstance(ss, str):                         ssxml = ss
    elif isinstance(ss, xmlentity.XmlEntityStruct): ssxml = ss.xmlString
    else :                                          ssxml = ss.toxml()
    if ssxml.startswith("<?xml"):   ssxml = ssxml[ssxml.find('>')+1:]
    #if ssxml.find("SpectralSpecT"): ssxml = ssxml[ssxml.find('>')+1:]
    #st = "SpectralSpecT>"
    #if ssxml.endswith(st): ssxml = ssxml[:ssxml.find(st)]
    ssxml = ssxml.replace("ns1:", "sbl:")
    ssxml = ssxml.replace("SpectralSpecT", "SpectralSpec")
    s += "\n" + ssxml
    s += "\n</sbl:SchedBlock>"
    sb = CCL.SchedBlock.CreateFromDocument(s)
    return sb.SpectralSpec[0]

def readEphemDataFile(fname='MarsTopocentric.txt'):
    """Read the Horizons data file and return results in a tuple of
    TBD"""
    mn=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    fn = 'EphemData/' + fname
    f = open(fn)
    # Form a list where each element is a line
    alltext = f.readlines()
    f.close()
    data = []
    isDataline = False
    for l in alltext:
        if l.find('$$EOE') != -1: 
            break  # Stop processing when we find this
        if isDataline:
            l = l.strip('\r\n')
            l = l[1:] 
            date = l[:11] + 'T' + l[12:17]
            ms= l[5:8]
            for i in range(12):
                if ms == mn[i]: date=date.replace(ms, "%02d" %(i+1))
            dl = [date, l[22:32], l[34:45]]
            dl += [float(l[46:62]), float(l[62:74])]
            #dl.append([l[46:62], l[62:74]])
            #data.append(l)
            data.append(dl)
        if l.find('$$SOE') != -1: isDataline = True
    return data


