// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.logging.Logger;
import java.util.Vector;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.AntennaCharacteristics;
import alma.Control.Band2Band3Overlap;
import alma.Control.BDDStreamInfo;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.DSBbasebandSpec;
import alma.Control.PhotonicReference;
import alma.Control.ResourceId;
import alma.Control.SSBbasebandSpec;
import alma.Control.SidebandPreference;
import alma.Control.TuningParameters;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.acs.nc.SimpleSupplier;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.common.LOsolutions.LOsolutions;
import alma.entity.xmlbinding.schedblock.BaseBandSpecificationT;
import alma.entity.xmlbinding.schedblock.FrequencySetupT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.types.
            BaseBandSpecificationTBaseBandNameType;
import alma.entity.xmlbinding.valuetypes.FrequencyT;
import alma.offline.StateData;

public class LocalOscillatorTest extends ComponentClientTestCase {

    private Logger logger;
    private Simulator sim;
    private LocalOscillatorImpl loObsMode;
    private ContainerServices container;
    private ResourceManager resMng;

    // //////////////////////////////////////////////////////////////////
    // Constructors
    // //////////////////////////////////////////////////////////////////

    public LocalOscillatorTest(String test) throws Exception {
        super(test);
    }

    public LocalOscillatorTest() throws Exception {
        super(LocalOscillatorTest.class.getName());
    }
    
    // Several tests need a valid solution set first; use this to do it
    private short setFreqSSB(double f) throws Exception {
        // These freqs must be within 2GHz (to fit in the IF)
        // or an Exception will be thrown 
        double f0 = 0.0;
        if (f < 1) f0 = 111.6e9;
        else f0 = f*1e9;       
        double[] BBFreq   = { f0, f0-0.6e9, f0-0.9E9, f0-1.6E9 };
        double[] BBWeight = { 100.0, 0.0, 0.0, 0.0 };
        int nBasebands = BBFreq.length;
        logger.info("Setting the frequencies (SSB)...");
        SSBbasebandSpec[] ssbspec = new SSBbasebandSpec[nBasebands];
        for (int i=0; i<nBasebands; i++) {
            ssbspec[i] = LOsolutions.ssbBasebandSpec(BBFreq[i], BBWeight[i]);
        }
        Band2Band3Overlap overlap = Band2Band3Overlap.AutoChoice;
        return loObsMode.setFrequency(ssbspec, overlap);
    }
    
    private String toString(TuningParameters t) {
        String s = "\n=== TUNING PARAMETERS ===\n";
        s += "Band: " + t.receiverBand + "\n";
        s += "LS: " + t.LSFrequency*1e-9 + " GHz\n";
        s += "FLOOG: " + t.FLOOGFrequency*1e-6 + " MHz\n";
        s += "FLOOG tuneHigh: " + t.tuneHigh + "\n";
        s += "ColdMultiplier: " + t.coldMultiplier + "\n";
        s += "IFprocUSB BB0&1: " + t.band0band1selectUSB + "\n";
        s += "IFprocUSB BB2&3: " + t.band2band3selectUSB + "\n";
        for (int i=0; i<4; i++) {
            s += "  BB" + i + "\n";
            s += "    " + "LO2: " + t.LO2[i].DYTOFrequency*1e-9 + " GHz\n";
            s += "    " + "FTS: " + t.LO2[i].FTSFrequency*1e-6 + " MHz\n";
            s += "    " + "tuneHigh: " + t.LO2[i].tuneHigh + "\n";
            s += "    " + "skySideband: " + t.LO2[i].skyFreqSideband + "\n";
            s += "    " + "isUsed: " + t.LO2[i].isUsed + "\n";
        }
        return s;
    }

    // //////////////////////////////////////////////////////////////////
    // Test fixture methods
    // //////////////////////////////////////////////////////////////////
    protected void setUp() throws Exception {
        super.setUp();
        container   = getContainerServices();
        this.logger = container.getLogger();
        
        sim = ObservingModeTesterImpl.modifyPythonSimCode(container);

        resMng = ResourceManager.getInstance(container);
        Resource<SimpleSupplier> pubres = new PublisherResource(
                container, "CONTROL_SYSTEM", true);
        resMng.addResource(pubres);
        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource.";
            fail(msg);
        }
        
        ObservingModeArray farr = ObservingModeTesterImpl.getFakeArray();
        loObsMode = new LocalOscillatorImpl(container, farr);
    }

    protected void tearDown() throws Exception {
        container.releaseComponent(sim.name());
        resMng.releaseResources();
        super.tearDown();
    }

    // //////////////////////////////////////////////////////////////////
    // Test cases
    // //////////////////////////////////////////////////////////////////
    
    public void testSetGetCentralLO() throws Exception {
        double magicCVRfreq = 14.5e9;
        double lsSetFreq    = magicCVRfreq*5-125e6;
        loObsMode.setCentralLOFreq(lsSetFreq);
       //assertEquals(magicCVRfreq, loObsMode.getCVRFrequency(), 0.01);
        double lsFreq    = loObsMode.getLSFrequency();
        logger.info("LS:" + lsFreq);
        assertEquals(lsSetFreq, lsFreq, 0.01);
    }
    
    public void testGetLO1Frequency() throws Exception {
        setFreqSSB(200);
        double floogFreq = loObsMode.getFLOOGFrequency();
        assertEquals(-32E6, floogFreq);
        double lsFreq = loObsMode.getLSFrequency();
        int coldMultiplier = loObsMode.getColdMultiplier();
        logger.info("Cold multiplier:" + coldMultiplier);
        assertEquals(coldMultiplier, 6);
        double expLO1Freq = coldMultiplier * (floogFreq + lsFreq);
        double LO1Freq = loObsMode.getLO1Frequency();
        logger.info("LO1:" + LO1Freq*1e-9 + " GHz");
        assertEquals(expLO1Freq, LO1Freq, 0.1);
    }

    public void testGetStateTable() throws Exception {
        StateData[] stateTable = loObsMode.getStateTable();
        for (StateData row : stateTable) {
            logger.info("StateData.antennaName = " + row.antennaName);
            logger.info("StateData.calDevice = " + row.calDevice);
            logger.info("StateData.signal = " + row.signal);
            logger.info("StateData.reference = " + row.reference);
            logger.info("StateData.onSky = " + row.onSky);
            logger.info("StateData.weight = " + row.weight);
        }
    }

    public void testBasicSSB() throws Exception {
        short nSols = setFreqSSB(0);
        logger.info("Tuning solutions: " + nSols);

        // Test numberOfSolutions()
        logger.info("Getting number of solutions...");
        short nSols2 = loObsMode.numberOfSolutions();
        logger.info("Solutions = " + nSols2);
        assertEquals(nSols, nSols2);

        // Test currentTuningSolution()
        logger.info("Getting the current tuning solution...");
        short current = loObsMode.currentTuningSolution();
        logger.info("Current tuning solution = #" + current);

        // Test getTuningSolution()
        logger.info("Getting tuning solution #" + current + "...");
        TuningParameters tp = loObsMode.getTuningSolution(current);
        logger.info("TuningParameters.receiverBand = "
                + tp.receiverBand.toString());
        logger.info("TuningParameters.FLOOGFrequency = " + tp.FLOOGFrequency);
        logger.info("TuningParameters.tuneHigh = " + tp.tuneHigh);
        for (int i = 0; i < tp.LO2.length; i++) {
            logger.info("TuningParameters.LO2[" + i + "].FTSFrequency = "
                    + tp.LO2[i].FTSFrequency);
            logger.info("TuningParameters.LO2[" + i + "].DYTOFrequency = "
                    + tp.LO2[i].DYTOFrequency);
        }

        // Test getFrequency()
        logger.info("Getting the frequencies...");
        double[] freqs = loObsMode.getFrequency();
        for (int i = 0; i < freqs.length; i++) {
            logger.info("Baseband " + i + " observing frequency = " + freqs[i]);
            // TODO: assertEquals(BBFreq[i], freqs[i], 0.1E9)?
        }

        // Test useTuningSolution()
        int tuningIndex = current;
        if (tuningIndex == 0) tuningIndex = 1;
        else tuningIndex--;
        logger.info("Using tuning solution #" + tuningIndex);
        loObsMode.useTuningSolution((short) tuningIndex);
    }
    
    public void testBasicDSB() throws Exception {    
        // Test setFrequency()
        logger.info("Setting the frequencies (DSB)...");
        int nBasebands = 3;
        DSBbasebandSpec[] dsbspec = new DSBbasebandSpec[nBasebands];
        // BB0, double sideband, weight=100
        double w = 100.0;
        dsbspec[0] = LOsolutions.dsbBasebandSpec(110.7e9,w,99.0e9,w);
        // BB1, not used
        w = 0.0;
        dsbspec[1] = LOsolutions.dsbBasebandSpec(0.0,w,0.09,w);
        // BB2, SSB, auto chose which sideband, weight=50
        w = 50.0;
        dsbspec[2] = LOsolutions.dsbBasebandSpec(99.1e9,w,99.1e9,w);

        Band2Band3Overlap overlap = Band2Band3Overlap.AutoChoice;
        short nSols = loObsMode.setFrequencyDSB(dsbspec, overlap);
        logger.info("Tuning solutions: " + nSols);

        // Test numberOfSolutions()
        logger.info("Getting number of solutions...");
        short nSols2 = loObsMode.numberOfSolutions();
        logger.info("Solutions = " + nSols2);
        assertEquals(nSols, nSols2);

        // Test currentTuningSolution()
        logger.info("Getting the current tuning solution...");
        short current = loObsMode.currentTuningSolution();
        logger.info("Current tuning solution = #" + current);

        // Test getTuningSolution()
        logger.info("Getting tuning solution #" + current + "...");
        TuningParameters tp = loObsMode.getTuningSolution(current);
        logger.info("TuningParameters.receiverBand = "
                + tp.receiverBand.toString());
        logger.info("TuningParameters.FLOOGFrequency = " + tp.FLOOGFrequency);
        logger.info("TuningParameters.tuneHigh = " + tp.tuneHigh);
        for (int i = 0; i < tp.LO2.length; i++) {
            logger.info("TuningParameters.LO2[" + i + "].FTSFrequency = "
                    + tp.LO2[i].FTSFrequency);
            logger.info("TuningParameters.LO2[" + i + "].DYTOFrequency = "
                    + tp.LO2[i].DYTOFrequency);
        }

        // Test getFrequency()
        logger.info("Getting the frequencies...");
        double[] freqs = loObsMode.getFrequency();
        for (int i = 0; i < freqs.length; i++) {
            logger.info("Baseband " + i + " observing frequency = " + freqs[i]);
            // TODO: assertEquals(BBFreq[i], freqs[i], 0.1E9)?
        }

    }
    private void spew(String o) {
        System.out.println("SETTP:"+o);
    }
    public void testSetTuningParams() throws Exception {    
        short cur =  loObsMode.currentTuningSolution();
        logger.info("solutionID:" + cur);
        assertEquals(cur, -1);
        boolean exCaught = false;
        try {
            TuningParameters tp = loObsMode.getTuningParameters();
        } catch (Exception e) {
            exCaught = true;
        }
        logger.info("Exception caught when expected: " + exCaught);
        assert(exCaught);        
        short nSols = setFreqSSB(0);
        spew("Num SSB tuning solutions: " + nSols);
        cur =  loObsMode.currentTuningSolution();
        spew("solutionID:" + cur);
        boolean legitSol = (cur > -1);
        assert(legitSol);
        TuningParameters tp = loObsMode.getTuningParameters();
        logger.info(toString(tp));
        // Now let's change something before we set it back to the hw
        tp.LO2[0].DYTOFrequency -= 1.5e9;
        loObsMode.setTuningParameters(tp);
        cur =  loObsMode.currentTuningSolution();
        logger.info("solutionID:" + cur);
        assertEquals(cur, -1);
        tp = loObsMode.getTuningParameters();
        logger.info(toString(tp));
    }
    private BaseBandSpecificationT createBaseBandSpec(double freq, double w, 
            String bbname) {
        BaseBandSpecificationT b = new BaseBandSpecificationT();
        b.setBaseBandName(
                BaseBandSpecificationTBaseBandNameType.valueOf(bbname));
        FrequencyT f = new FrequencyT();
        f.setContent(freq);
        f.setUnit("GHz");
        b.setCenterFrequency(f);
        b.setWeighting(w);
        return b;
    }
    private SpectralSpecT createSpectralSpec() {
        FrequencySetupT fs = new FrequencySetupT();
        SpectralSpecT   ss = new SpectralSpecT();
        BaseBandSpecificationT bs1 = createBaseBandSpec(100.00, 100, "BB_1");
        BaseBandSpecificationT bs2 = createBaseBandSpec(100.01, 100, "BB_2");
        BaseBandSpecificationT bs3 = createBaseBandSpec(100.02, 100, "BB_3");
        BaseBandSpecificationT bs4 = createBaseBandSpec(100.03, 100, "BB_4");
        fs.addBaseBandSpecification(bs4);
        fs.addBaseBandSpecification(bs2);
        fs.addBaseBandSpecification(bs3);
        ss.setFrequencySetup(fs);
        return ss;
    }
    public void testSetSpectralSpec() {
        SpectralSpecT ss = createSpectralSpec();
        boolean noException = true;
        try {
            loObsMode.setSpectrumAsync(
                    ObservingModeUtilities.serializeSpectralSpec(ss, logger).
                        xmlString);
        } catch(Exception e) {
            logger.info("Exception on setSpectrumAsync");
            logger.info("" + e);
            noException = false;
        }
        assert(noException);
        short current = loObsMode.currentTuningSolution();
        logger.info("Current tuning solution = #" + current);
        assert (current >= 0);

        // Get the solution and check it
        TuningParameters tp = null;
        try {
            tp = loObsMode.getTuningSolution(current);
        } catch (Exception e){
            logger.info("Couldn't get TuningSolution for #"+current);
            noException = false;
        }
        assert(noException);
        // Log the current tuning parameters
        logger.info(LOsolutions.tuningHeaderString(true));
        Vector<String> lines = LOsolutions.tuningStringsPhysical(tp);
        for (String l: lines) logger.info(l);
        // Check tuning
        assertEquals(tp.LO2[3].isUsed, true);
        assertEquals(tp.LO2[1].isUsed, true);
        assertEquals(tp.LO2[2].isUsed, true);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "1"; // Default test suite.
        try {
            if (testSuite.equals("1")) {
                suite.addTest(new LocalOscillatorTest("testSetGetCentralLO"));
            } else if (testSuite.equals("2")) {
                suite.addTest(new LocalOscillatorTest("testGetLO1Frequency"));
            } else if (testSuite.equals("3")) {
                suite.addTest(new LocalOscillatorTest("testBasicSSB"));
            } else if (testSuite.equals("4")) {
                suite.addTest(new LocalOscillatorTest("testBasicDSB"));
            } else if (testSuite.equals("5")) {
                suite.addTest(new LocalOscillatorTest("testGetStateTable"));
            } else if (testSuite.equals("6")) {
                suite.addTest(new LocalOscillatorTest("testSetTuningParams"));
            } else if (testSuite.equals("7")) {
                suite.addTest(new LocalOscillatorTest("testSetSpectralSpec"));
            }
        } catch (Exception ex) {
            System.err.println("Error when creating LOObservingModeTest: "
                    + ex.toString());
        }
        return suite;
    }
    
    // To set up system and exit
    

}
