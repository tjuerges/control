/* 
 *
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ObservingModes;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;


import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.Control.SubscanBoundries;
import alma.Control.TuningParameters;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;

/**
 * @author ntroncos
 *
 */
public final class BoundrieSpectralSpecPairer {
	

	public static List<BoundriesSpectralSpecPair> getBoundriesSpectalSpecPairs(
			SubscanBoundries[] boundaries, SpectralSpecT[] spectralSpec)
			throws IllegalParameterErrorEx {
		
		List<BoundriesSpectralSpecPair> list = new ArrayList<BoundriesSpectralSpecPair>();
		List<SpectralSpecT> spectralSpecList = new ArrayList<SpectralSpecT>(
				Arrays.asList(spectralSpec));
		List<SubscanBoundries> boundariesList = new ArrayList<SubscanBoundries>(
				Arrays.asList(boundaries));

		if (spectralSpecList.size() != boundariesList.size()) {
			AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
			ex.setProperty("Additional",
					"Length of spectralspecs sequence does not match broundires sequence");
			throw ex.toIllegalParameterErrorEx();
		}
	
		for(int i=0; i< spectralSpecList.size(); i++){
			BoundriesSpectralSpecPair element =  new BoundriesSpectralSpecPair(boundariesList.get(i), spectralSpecList.get(i) );
			list.add(element);
		}
			return list;
	}

	public static List<SubscanInfoPack> getSubscanInfoSetup(
            SubscanBoundries[] boundaries, SpectralSpecT[] spectralSpec, 
            List<TuningParameters> tunningSetup, 
            CalibrationDevice[] acdPositions, 
            List<Double> avgFreq) 
                throws IllegalParameterErrorEx {
		List<SubscanInfoPack> setup = new ArrayList<SubscanInfoPack>();
		List<SubscanBoundries> boundariesList = new ArrayList<SubscanBoundries>(
				Arrays.asList(boundaries));
		List<SpectralSpecT> spectralSpecList = new ArrayList<SpectralSpecT>(
				Arrays.asList(spectralSpec));
		List<CalibrationDevice> acdList = new ArrayList<CalibrationDevice>(
				Arrays.asList(acdPositions));
		
		if(tunningSetup.size() != boundariesList.size() || 
           tunningSetup.size() != spectralSpecList.size() ||
           tunningSetup.size() != acdList.size()) {
			AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
			ex.setProperty("Additional",
					"Length of tuningSetup sequence does not match broundaries sequence, or acdPositions");
			ex.setProperty("Additional", "tuningSetup size: " + tunningSetup.size());
			ex.setProperty("Additional", "boundaries sequence size: " + boundariesList.size());
			ex.setProperty("Additional", "spectralspec size: " + spectralSpecList.size());
			ex.setProperty("Additional", "acd positions size: " + acdList.size());
			throw ex.toIllegalParameterErrorEx();
		}
		String msg;
		for(int i=0; i < tunningSetup.size(); i++){
			SubscanInfoPack element = null;
			if (i==0) {
				element = new SubscanInfoPack(boundariesList.get(i), spectralSpecList.get(i), tunningSetup.get(i), 
                    acdList.get(i), avgFreq.get(i).doubleValue() ,i+1);
			    msg = "Subscan starts at: " + alma.acs.util.UTCUtility.getUTCDate(alma.acs.util.UTCUtility.utcOmgToJava(boundariesList.get(i).startTime));
			    msg = msg + " Photoref start locking at: NOW";
			    Logger.getAnonymousLogger().info(msg);
			}else {
            	element = new SubscanInfoPack(boundariesList.get(i-1), spectralSpecList.get(i), tunningSetup.get(i), 
                    acdList.get(i), avgFreq.get(i).doubleValue(), i+1);
			    msg = "Subscan starts at: " + alma.acs.util.UTCUtility.getUTCDate(alma.acs.util.UTCUtility.utcOmgToJava(boundariesList.get(i).startTime));
			    msg = msg + " Photoref start locking at: " + alma.acs.util.UTCUtility.getUTCDate(alma.acs.util.UTCUtility.utcOmgToJava(boundariesList.get(i-1).endTime));
			    Logger.getAnonymousLogger().info(msg);
			}
			setup.add(element);			
		}
		
		return setup;
	}
	
	public static alma.Control.PhotonicReferencePackage.SubscanInformation[] getPhotonicRefeceSubScanArray(List<SubscanInfoPack> setup)
	{
		List<alma.Control.PhotonicReferencePackage.SubscanInformation> photoreflist= new ArrayList<alma.Control.PhotonicReferencePackage.SubscanInformation>();
		for(SubscanInfoPack element:setup){
			photoreflist.add(element.getPhotoRefSubscanInfo());
		}
		return photoreflist.toArray(new alma.Control.PhotonicReferencePackage.SubscanInformation[photoreflist.size()]);
	}
	
//	public static double[] getReferenceFrequency(List<SubscanInfoPack> setup)
//	{
//		double[] retval = new double[setup.size()];
//		int i =0;
//		for(SubscanInfoPack element: setup){
//			retval[i] = element.getPhotoRefSubscanInfo().frequency;
//			i++;
//		}
//		return retval;
//	}
	
	public static alma.Control.AntLOControllerPackage.SubscanInformation[] getAntLOSubscanArray(List<SubscanInfoPack> setup) 
	{
		List<alma.Control.AntLOControllerPackage.SubscanInformation> antLOList = new ArrayList<alma.Control.AntLOControllerPackage.SubscanInformation>();
		for (SubscanInfoPack element:setup){
			antLOList.add(element.getAntLOSubscanInfo());
		}
		return antLOList.toArray(new alma.Control.AntLOControllerPackage.SubscanInformation[antLOList.size()]);
	}
}



