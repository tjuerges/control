/* 
 *
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ObservingModes;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.Control.AntLOController;
import alma.Control.CommonCallbacks.AntennaCallbackImpl;
import alma.Control.CommonCallbacks.ProgressCallbackImpl;
import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
import alma.ControlCommonExceptions.wrappers.AsynchronousFailureAcsJCompletion;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ControlExceptions.wrappers.AcsJTimeoutEx;
import alma.ControlExceptions.wrappers.InvalidRequestAcsJCompletion;
import alma.ControlExceptions.wrappers.TimeoutAcsJCompletion;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.JavaContainerError.wrappers.ContainerServicesAcsJCompletion;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.ObservingModeErrorAcsJCompletion;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJCompletion;
import alma.log_audience.DEVELOPER;

/**
 * @author ntroncos
 *
 */
public class LocalOscilatorThread extends Thread {
	ProgressCallbackImpl photoRefCallback = null;
	AntennaCallbackImpl acdCallback = null;
	AcsJCompletion completion = null;
	Map<String, AntLOController> antennaModeController = null;
	CountDownLatch doneSignal = null;
	ContainerServices containerServices =null;
	List<SubscanInfoPack> setupInfo;
    SubscanInfoPack currentSetup = null;
	boolean abort;

	public LocalOscilatorThread(ProgressCallbackImpl photoRefCallback, Map<String, AntLOController> antennaModeController, ContainerServices containerServices, List<SubscanInfoPack> setupInfo) {
		super();
		this.photoRefCallback = photoRefCallback;
		this.antennaModeController = antennaModeController;
		this.doneSignal = new CountDownLatch(1);
		this.containerServices = containerServices;
		this.setupInfo = setupInfo;
		this.abort = false;
	}

	@Override
	public void run() {
		containerServices.getLogger().logToAudience(Level.INFO,"Running Thread for "+setupInfo.size()+" number of sequences.","");
		for(SubscanInfoPack subscan:setupInfo) {
			if (abort == true)
				break;
			try {

				acdCallback = new AntennaCallbackImpl(containerServices);
				
				long now = alma.acs.util.UTCUtility.utcJavaToOmg(System.currentTimeMillis());


				double millisecondsToStartTime =  ((subscan.getPhotoRefSubscanInfo().startTime - now)* 100.0 *1.0E-9 *1.0E3);
				if (millisecondsToStartTime > 0) {
					containerServices.getLogger().logToAudience(Level.INFO, "Got Early to sequence "+subscan.getSubscanNumber()+ " . Waiting for:"+millisecondsToStartTime*1.0E-3 + " Seconds.", DEVELOPER.value);
					containerServices.getLogger().logToAudience(Level.INFO, "Start time is "+alma.acs.util.UTCUtility.getUTCDate(alma.acs.util.UTCUtility.utcOmgToJava(subscan.getPhotoRefSubscanInfo().startTime))+" Time now is "+alma.acs.util.UTCUtility.getUTCDate(System.currentTimeMillis()),DEVELOPER.value); 
					sleep((long) millisecondsToStartTime);
				} else {
					if(subscan.getSubscanNumber() > 1)
					    containerServices.getLogger().logToAudience(Level.INFO, "Got Late to sequence "+subscan.getSubscanNumber()+ " . Thread is late by: "+millisecondsToStartTime*1.0E-3 + " Seconds.", DEVELOPER.value);
				}

				containerServices.getLogger().logToAudience(Level.INFO, "LocalOscilator procesing sequence: "+subscan.getSubscanNumber(), DEVELOPER.value);

				containerServices.getLogger().logToAudience(Level.INFO, "LocalOscilator commanding ACDs into new position. Sequence : "+subscan.getSubscanNumber(), DEVELOPER.value);

                currentSetup = subscan;
                
				moveACD(subscan.getAntLOSubscanInfo().acdState);

				waitForPhotonicReferece(subscan.getPhotoRefSubscanInfo().startTime);
				
				if (completion.isError()) {
					containerServices.getLogger().logToAudience(Level.SEVERE, "LocalOscilator could not lock the LS, skiping sequence: " +subscan.getSubscanNumber(),"");
					break;
				}

				containerServices.getLogger().logToAudience(Level.INFO, "LocalOscilator photonic reference Locked. sequence: "+subscan.getSubscanNumber(), DEVELOPER.value);

				lockFrontEnd(subscan.getAntLOSubscanInfo().startTime);

				containerServices.getLogger().logToAudience(Level.INFO, "LocalOscilator FrontEnds Locked. sequence: "+subscan.getSubscanNumber(), DEVELOPER.value);

				waitForACD(subscan.getAntLOSubscanInfo().startTime);
				containerServices.getLogger().logToAudience(Level.INFO, "LocalOscilator ACDs at commanded position: "+subscan.getSubscanNumber(), DEVELOPER.value);

			} catch (AcsJTimeoutEx e) {
				completion = new TimeoutAcsJCompletion(e);
			} catch (AcsJContainerServicesEx e) {
				completion = new ContainerServicesAcsJCompletion(e);
			} catch (AcsJInvalidRequestEx e) {
				completion = new InvalidRequestAcsJCompletion(e);
			} catch (AcsJAsynchronousFailureEx e) {
				completion = new AsynchronousFailureAcsJCompletion(e);
			} catch (Exception e) {
	    		AcsJObservingModeErrorEx nex = new AcsJObservingModeErrorEx(e);
	    		completion = new ObservingModeErrorAcsJCompletion(nex);
			} finally {
				doneSignal.countDown();
			}
		}

	}
    	
	private void waitForACD(long startTime) throws AcsJTimeoutEx,
			AcsJAsynchronousFailureEx {
		try {

			// FIXME: magic number 25, is 25 seconds. Put this timeout in a
			// sensible place.
			long timeout = 300;
			acdCallback.waitForCompletion(timeout);
			containerServices.getLogger().logToAudience(Level.INFO,
					"Completion on atennas returned", DEVELOPER.value);

		} catch (AcsJTimeoutEx e) {
			AcsJTimeoutEx nex = new AcsJTimeoutEx(e);
			nex.setProperty("Additional",
					"Timeout when moving ACDs into position");
			throw nex;
		}

	}

	private void moveACD(CalibrationDevice acdState)
			throws AcsJContainerServicesEx, AcsJInvalidRequestEx {
		if (abort == true) {
			return;
		}

		AntennaCallbackImpl antennaCallback = new AntennaCallbackImpl(
				containerServices);
		for (Entry<String, AntLOController> entry : antennaModeController
				.entrySet()) {

			antennaCallback.addExpectedResponse(entry.getKey());
			containerServices.getLogger().logToAudience(Level.INFO,
					"Command ACD on antenna: " + entry.getKey() + "to position: " + acdState,
					DEVELOPER.value);

			entry.getValue().setCalibrationDeviceAsynch(acdState,
					antennaCallback.getExternalInterface());

		}

	}

	private void waitForPhotonicReferece(long startTime) throws AcsJTimeoutEx {
		if (abort == true) {
			return;
		}

		try {
			// FIXME: magic number 25, is 25 seconds. Put this timeout in a
			// sensible place.
			long timeout = 300;
			completion = photoRefCallback.waitForCompletion(timeout);
		} catch (AcsJTimeoutEx e) {
			AcsJTimeoutEx nex = new AcsJTimeoutEx(e);
			nex.setProperty("Additional",
					"Timeout when setting the Photonic Reference");
			throw nex;
		}

	}

	private void lockFrontEnd(long startTime) throws AcsJContainerServicesEx,
			AcsJInvalidRequestEx, AcsJAsynchronousFailureEx, AcsJTimeoutEx {
		
		if (abort == true) {
			return;
		}
		
		AntennaCallbackImpl antennaCallback = new AntennaCallbackImpl(containerServices);
		for (Entry<String, AntLOController> entry : antennaModeController
				.entrySet()) {

			antennaCallback.addExpectedResponse(entry.getKey());

			containerServices.getLogger().logToAudience(Level.INFO,
					"Command lock on antenna: " + entry.getKey(),
					DEVELOPER.value);
			
			entry.getValue().lockFrontEnd(
					antennaCallback.getExternalInterface());

		}

		try {

			// FIXME: magic number 25, is 25 seconds. Put this timeout in a
			// sensible place.
			long timeout = 300;
			antennaCallback.waitForCompletion(timeout);
			containerServices.getLogger().logToAudience(Level.INFO,
					"Completion on atennas returned",
					DEVELOPER.value);

		} catch (AcsJTimeoutEx e) {
			AcsJTimeoutEx nex = new AcsJTimeoutEx(e);
			nex.setProperty("Additional", "Timeout when loking the FrontEnds");
			throw nex;
		}
	}


    public double getAverageSkyFrequency() 
    {
        double retval = 0.0;
        if (currentSetup != null) {
            retval = currentSetup.getAverageSkyFrequency();
        }
        return retval;

    }

	public AcsJCompletion waitForSubscanStart(long timeout)
			throws InterruptedException {
		if (!isAlive()) {
			// throw
			containerServices.getLogger().logToAudience(Level.SEVERE,
					"Waiting for subscans, but the thread is not running.",
					DEVELOPER.value);
		}

		doneSignal.await(timeout, TimeUnit.SECONDS);

		return completion;
	}

	public void abortSubscanSequence() {
		containerServices.getLogger().logToAudience(Level.INFO, "Aborting subsequences", DEVELOPER.value);
		abort=true;
		completion = new ObservingModeErrorAcsJCompletion();
		completion.setProperty("Additional", "Subscan was aborted");
		tellAntennasToAbort();
		interrupt();
	}
	
	private void tellAntennasToAbort() {
		for (Entry<String, AntLOController> entry : antennaModeController
				.entrySet()) {
			containerServices.getLogger().logToAudience(Level.INFO,
					"Command abort frequency Changes on: " + entry.getKey(),
					DEVELOPER.value);
			entry.getValue().abortFrequencyChanges();
		}
	}
}

