package alma.Control.ObservingModes;

import alma.Control.SubscanBoundries;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;

class BoundriesSpectralSpecPair {
	SubscanBoundries boundries = null;
	SpectralSpecT spectralSpec = null;
	public BoundriesSpectralSpecPair(SubscanBoundries boundries,SpectralSpecT spectralSpec) {
		this.boundries = boundries;
		this.spectralSpec = spectralSpec;
	}

	public SubscanBoundries getBoundries() 
	{
		return boundries;
	}
	
	public SpectralSpecT getSpectralSpec() 
	{
		return spectralSpec;
	}
}