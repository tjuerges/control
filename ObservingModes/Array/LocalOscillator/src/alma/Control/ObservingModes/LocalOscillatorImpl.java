/* 
 * $Id$
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ObservingModes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Vector;
import java.util.logging.Logger;

import org.omg.CORBA.BooleanHolder;
import org.omg.CORBA.DoubleHolder;
import org.omg.CORBA.StringHolder;

import alma.ACSErr.CompletionHolder;
import alma.ACSErrTypeOK.wrappers.ACSErrOKAcsJCompletion;
import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.Control.AntLOController;
import alma.Control.Band2Band3Overlap;
import alma.Control.CVR;
import alma.Control.DSBbasebandSpec;
import alma.Control.DopplerServer;
import alma.Control.DopplerServerHelper;
import alma.Control.EphemerisRow;
import alma.Control.LO2Parameters;
import alma.Control.LOOffsettingMode;
import alma.Control.LSCommon;
import alma.Control.LocalOscillator;
import alma.Control.LocalOscillatorHelper;
import alma.Control.LocalOscillatorOperations;
import alma.Control.LocalOscillatorPOATie;
import alma.Control.PhotonicReference;
import alma.Control.ResourceId;
import alma.Control.SSBbasebandSpec;
import alma.Control.SidebandPreference;
import alma.Control.SimpleCallback;
import alma.Control.SkyFreqSideband;
import alma.Control.SubscanBoundries;
import alma.Control.TuningParameters;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.StaticResource;
import alma.Control.Common.Util;
import alma.Control.CommonCallbacks.AntennaCallbackImpl;
import alma.Control.CommonCallbacks.ProgressCallbackImpl;
import alma.Control.CommonCallbacks.SimpleCallbackImpl;
import alma.ControlCommonExceptions.wrappers.AcsJAsynchronousFailureEx;
import alma.ControlCommonExceptions.wrappers.AsynchronousFailureAcsJCompletion;
import alma.ControlExceptions.INACTErrorEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJInvalidRequestEx;
import alma.ControlExceptions.wrappers.TimeoutAcsJCompletion;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.DopplerReferenceCodeMod.DopplerReferenceCode;
import alma.JavaContainerError.ContainerServicesEx;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.ModeControllerExceptions.wrappers.AcsJHardwareFaultEx;
import alma.ModeControllerExceptions.wrappers.AcsJTimeoutEx;
import alma.NetSidebandMod.NetSideband;
import alma.NetSidebandMod.NetSidebandHolder;
import alma.ObservingModeExceptions.AntennasDisagreeEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJAntennasDisagreeEx;
import alma.ObservingModeExceptions.wrappers.AcsJDataCapturerErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.ObservingModeErrorAcsJCompletion;
import alma.RadialVelocityReferenceCodeMod.RadialVelocityReferenceCode;
import alma.ReceiverBandMod.ReceiverBand;
import alma.ResourceExceptions.wrappers.AcsJNotYetAcquiredEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.ComponentQueryDescriptor;
import alma.acs.container.ContainerServices;
import alma.acs.exceptions.AcsJCompletion;
import alma.acs.exceptions.AcsJException;
import alma.common.LOsolutions.LOsolutions;
import alma.common.LOsolutions.SidebandSelect;
import alma.entity.xmlbinding.schedblock.ACACorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.BLBaseBandConfigT;
import alma.entity.xmlbinding.schedblock.BLCorrelatorConfigurationT;
import alma.entity.xmlbinding.schedblock.BLSpectralWindowT;
import alma.entity.xmlbinding.schedblock.BaseBandSpecificationT;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.FrequencySetupT;
import alma.entity.xmlbinding.schedblock.SchedBlockRefT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.entity.xmlbinding.schedblock.SpectralSpecTChoice;
import alma.entity.xmlbinding.schedblock.types.AbstractCorrelatorConfigurationTLOOffsettingModeType;
import alma.entity.xmlbinding.schedblock.types.AbstractSpectralWindowTSideBandType;
import alma.entity.xmlbinding.schedblock.types.BaseBandSpecificationTBaseBandNameType;
import alma.entity.xmlbinding.schedblock.types.BaseBandSpecificationTSideBandPreferenceType;
import alma.entity.xmlbinding.schedblock.types.FieldSourceTSolarSystemObjectType;
import alma.entity.xmlbinding.schedblock.types.FrequencySetupTDopplerReferenceType;
import alma.entity.xmlbinding.schedblock.types.FrequencySetupTReceiverBandType;
import alma.entity.xmlbinding.valuetypes.FrequencyT;
import alma.entity.xmlbinding.valuetypes.SkyCoordinatesT;
import alma.entity.xmlbinding.valuetypes.SpeedT;
import alma.entity.xmlbinding.valuetypes.VelocityT;
import alma.entity.xmlbinding.valuetypes.types.SkyCoordinatesTSystemType;
import alma.entity.xmlbinding.valuetypes.types.VelocityTDopplerCalcTypeType;
import alma.entity.xmlbinding.valuetypes.types.VelocityTReferenceSystemType;
import alma.hla.datamodel.enumeration.JReceiverBand;
import alma.log_audience.DEVELOPER;
import alma.offline.StateData;
import alma.xmlentity.XmlEntityStruct;

/**
 * 
 */
public class LocalOscillatorImpl extends ObservingModeCore<AntLOController>
        implements LocalOscillatorOperations {

    // An index into tuningSolutions vector indicating which solution is
    // currently being used.
    short currentSolution = -1;

    // The tuning parameters currently being used by the hardware
    protected TuningParameters currentTuningParameters = null;

    // The computational engine for the tuning solutions, and the store
    // of the set of tuning solutions
    protected LOsolutions loSolutions;

    // These are the objects for making this class an offshoot
    private LocalOscillatorPOATie tieBindingClass= null;
    private LocalOscillator       corbaOffshoot = null;

    // Properties
    private ResourceManager resMng;

    // CORBA References to hardware components
    private LSCommon          lscommon = null;
    private CVR               cvr      = null;
    private PhotonicReference photoRef = null;
    
    private ProgressCallbackImpl photoRefCallback = null;
    private LocalOscilatorThread localOscilatorThread = null;

    // CORBA Reference to calculation component
    private DopplerServer     dopplerServer = null;

    public LocalOscillatorImpl(ContainerServices cs, ObservingModeArray array)
            throws ObsModeInitErrorEx {
        super(cs, array);
        logger.finest(getClass().getName() + ".LocalOscillatorImpl");
        
        loSolutions = new LOsolutions(logger);

        try {
            ComponentQueryDescriptor spec = new ComponentQueryDescriptor();
            spec.setComponentType("IDL:alma/Control/DopplerServer:1.0");
            spec.setComponentName(array.getArrayComponentName()
                    + "/DOPPLERSERVER");
            dopplerServer = DopplerServerHelper.narrow(
                    cs.getDynamicComponent(spec, false));
        } catch (AcsJContainerServicesEx jex) {
            jex.log(logger);
            AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(jex);
            throw exhlp.toObsModeInitErrorEx();
        }

        // Start all the Antenna based LO components ie., all the
        // AntLOController components
        createAntModeControllers("alma.Control.AntLOControllerHelper",
                                 "AntLOController");

        ResourceId[] photonicReferences = array.getPhotonicReferences();
        if (photonicReferences.length < 1) {
            AcsJObsModeInitErrorEx ex = new AcsJObsModeInitErrorEx();
            ex.setProperty("Details", 
                "At least one PhotonicReference is required");
            throw ex.toObsModeInitErrorEx();
        }
 
        // Arbitrarily choose the first one
        ResourceId photonicRef = photonicReferences[0];
        if (photonicRef == null) {
            String msg = "PhotonicReference is null";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx());
        }
        if (photonicRef.ResourceName == null) {
            String msg = "PhotonicReference name is null";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx());
        }
        try {
            selectPhotonicReference(photonicRef.ResourceName);
        } catch (ObservingModeErrorEx e) {
            String msg = "Failed to select specific photonic reference: " +
                photonicRef.ResourceName;
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(e));
        } catch (TimeoutEx e) {
            String msg = "Timed out selecting photonic reference: " +
                photonicRef.ResourceName;
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(e));
        }
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public void selectPhotonicReference(String photonicRef) 
        throws ObservingModeErrorEx, TimeoutEx
    {
        logger.finest(getClass().getName() + ".selectPhotonicReference");

        String photoRefCompName = null;
        ResourceId[] photonicReferences = array.getPhotonicReferences();
        if (photonicReferences.length < 1) {
            AcsJObservingModeErrorEx ex = new AcsJObservingModeErrorEx();
            ex.setProperty("Details", 
                "At least one PhotonicReference is required");
            throw ex.toObservingModeErrorEx();
        }
        for ( ResourceId resId : photonicReferences ) {
            if (resId.ResourceName.equals(photonicRef)){
                photoRefCompName = resId.ComponentName;
            }
        }
        if (photoRefCompName == null) {
            String msg = "PhotonicReference has not been assigned" +
                " to this Array: " + photonicRef;
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
        }
        
        /* Release any old references */
        resMng = ResourceManager.getInstance(cs);
        if (cvr != null) {
            String name = cvr.name();
            resMng.releaseResource(name);
            resMng.freeResource(name);
        }
        if (lscommon != null) {
            String name = lscommon.name();
            resMng.releaseResource(name);
            resMng.freeResource(name);
        }
        if (photoRef != null) {
            String name = photoRef.name();
            resMng.releaseResource(name);
            resMng.freeResource(name);            
        }

        /* Now get the new reference */
        try {
            StaticResource<PhotonicReference> resPhotoRef = 
                new StaticResource<PhotonicReference>(cs, photoRefCompName,
                                 "alma.Control.PhotonicReferenceHelper");
            resMng.acquireResource(resPhotoRef);
            photoRef = resPhotoRef.getComponent();

            // Get a reference to the CVR
            final String CVRDeviceName = photoRef.getSubdeviceName("CVR");
            StaticResource<CVR> resCVR =
                new StaticResource<CVR>(cs, CVRDeviceName, 
                                        "alma.Control.CVRHelper");
            resMng.acquireResource(resCVR);
            cvr = resCVR.getComponent();
            
            // Get a reference to the LSCommon
            // This is a base class; the CDB will define the specific device
            // that is used.
            final String LSCommonDeviceName = photoRef.getSubdeviceName("LS");
            StaticResource<LSCommon> resLSCommon = 
                new StaticResource<LSCommon>(cs, LSCommonDeviceName, 
                                             "alma.Control.LSCommonHelper");
            resMng.acquireResource(resLSCommon);
            lscommon = resLSCommon.getComponent();
        } catch (AcsJResourceErrorEx e) {
            String msg = "Cannot get access to all the central LO components.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        } catch (AcsJNotYetAcquiredEx e) {
            String msg = "Cannot get access to all the central LO components.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        } catch (IllegalParameterErrorEx e) {
            String msg = "Cannot get access to all the central LO components.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        }

        /* Now send a command to every antenna to switch the 
           SAS to the new Photonic Reference
        */
        AntennaCallbackImpl cbObj = null;
        
        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object " 
                + "for asynchronous execution";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        }
           
        // Main loop that sends the command to each antenna
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            entry.getValue().
                selectPhotonicReferenceAsynch(photonicRef,
                                              cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                ex.log(logger);
            }
        }
        
        final long PhotonicReferenceSelectionTimeout = 45;
        try {
            cbObj.waitForCompletion(PhotonicReferenceSelectionTimeout);
        } catch (AcsJAsynchronousFailureEx e) {
            String msg = "Error detected in asynchronous execution of " +
                " selection photonic reference";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) { 
            /* Translate to mode controller exception for now */
            AcsJTimeoutEx nex = new AcsJTimeoutEx(e);
            throw nex.toTimeoutEx();
        }
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public void optimizeSASPolarizationAsynch(boolean forceCalibration,
                                              SimpleCallback cb) 
    {
        logger.finest("alma.LocalOscillatorImpl.optimizePhotonicReference");
        
        final long PhotonicReferenceOptimizationTimeout = 90;
        AntennaCallbackImpl cbObj = null;
        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            ObservingModeErrorAcsJCompletion c = 
                new ObservingModeErrorAcsJCompletion(e);
            c.setProperty("ErrorMessage", "Failed to create callback object " 
                          + "for asynchronous execution");
            cb.report(c.toCorbaCompletion());
            return;
        }
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            entry.getValue().
                optimizeSASPolarizationAsynch(forceCalibration,
                                              cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                ex.log(logger);
            }
        }
        
        try {
            cbObj.waitForCompletion(PhotonicReferenceOptimizationTimeout);
            cb.report(new ACSErrOKAcsJCompletion().toCorbaCompletion());
        } catch (AcsJAsynchronousFailureEx e) {
            AsynchronousFailureAcsJCompletion c =
                new AsynchronousFailureAcsJCompletion(e);
            c.setProperty("Error Message","Error detected in asynchronous"
                          + " execution of optimize photonic reference");
            cb.report(c.toCorbaCompletion());
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
            TimeoutAcsJCompletion c = new TimeoutAcsJCompletion(e);
            cb.report(c.toCorbaCompletion());
        }
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public void optimizeSASPol1Asynch(boolean forceCalibration,
                                      SimpleCallback cb) 
    {
        logger.finest("alma.LocalOscillatorImpl.optimizePhotonicReference");
        
        final long PhotonicReferenceOptimizationTimeout = 45;
        AntennaCallbackImpl cbObj = null;
        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            ObservingModeErrorAcsJCompletion c = 
                new ObservingModeErrorAcsJCompletion(e);
            c.setProperty("ErrorMessage", "Failed to create callback object " 
                          + "for asynchronous execution");
            cb.report(c.toCorbaCompletion());
            return;
        }
        
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            entry.getValue().
                optimizeSASPol1Asynch(forceCalibration,
                                      cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                ex.log(logger);
            }
        }
        
        try {
            cbObj.waitForCompletion(PhotonicReferenceOptimizationTimeout);
            cb.report(new ACSErrOKAcsJCompletion().toCorbaCompletion());
        } catch (AcsJAsynchronousFailureEx e) {
            AsynchronousFailureAcsJCompletion c =
                new AsynchronousFailureAcsJCompletion(e);
            c.setProperty("Error Message","Error detected in asynchronous"
                          + " execution of optimize photonic reference");
            cb.report(c.toCorbaCompletion());
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
            TimeoutAcsJCompletion c = new TimeoutAcsJCompletion(e);
            cb.report(c.toCorbaCompletion());
        }
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public void optimizeSASPol2Asynch(boolean forceCalibration,
                                      SimpleCallback cb) 
    {
        logger.finest("alma.LocalOscillatorImpl.optimizePhotonicReference");
        
        final long PhotonicReferenceOptimizationTimeout = 45;
        AntennaCallbackImpl cbObj = null;
        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            ObservingModeErrorAcsJCompletion c = 
                new ObservingModeErrorAcsJCompletion(e);
            c.setProperty("ErrorMessage", "Failed to create callback object " 
                          + "for asynchronous execution");
            cb.report(c.toCorbaCompletion());
            return;
        }
        
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            entry.getValue().
                optimizeSASPol2Asynch(forceCalibration,
                                      cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                ex.log(logger);
            }
        }
        
        try {
            cbObj.waitForCompletion(PhotonicReferenceOptimizationTimeout);
            cb.report(new ACSErrOKAcsJCompletion().toCorbaCompletion());
        } catch (AcsJAsynchronousFailureEx e) {
            AsynchronousFailureAcsJCompletion c =
                new AsynchronousFailureAcsJCompletion(e);
            c.setProperty("Error Message","Error detected in asynchronous"
                          + " execution of optimize photonic reference");
            cb.report(c.toCorbaCompletion());
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
            TimeoutAcsJCompletion c = new TimeoutAcsJCompletion(e);
            cb.report(c.toCorbaCompletion());
        }
    }

    private void throwIfUnallocated(Map<String, UnallocatedEx> ex,
            int numBadAntennas, String action) throws HardwareFaultEx {
        logger.finest(getClass().getName() + ".throwIfUnallocated");
        final int numEx = ex.size();
        final int numAntennas = antennaModeController.size();
        if (numEx > 0) {
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx();
            String msg;
            msg = "Cannot " + action + " on " + numBadAntennas;
            msg += " antennas out of " + numAntennas + " as " + numEx;
            msg += " of them are not attached to the LO components";
            msg += " in an antenna.";
            jEx.setProperty("Message", msg);
            for (Entry<String, UnallocatedEx> entry:ex.entrySet()){
                jEx.setProperty(
                    entry.getKey(),
                    Util.getNiceErrorTraceString(entry.getValue().errorTrace));
            }
            jEx.log(logger);
            if (numBadAntennas > maxNumBadAntennas()) {
                throw jEx.toHardwareFaultEx();
            }
        }
    }

    // --------------------- CORBA interface --------------------------

    /**
     * See the IDL file for a description of this function.
     * 
     * @throws HardwareFaultEx
     */
    @Override
    public void enableDelayTracking(boolean enable) throws HardwareFaultEx {
        logger.finest(getClass().getName() + ".enableDelayTracking");

        Map<String, UnallocatedEx> unallocatedEx = 
                new TreeMap<String, UnallocatedEx>();
        
		for (Entry<String, AntLOController> entry : antennaModeController.entrySet()) {
			try {
				entry.getValue().setEnableDelayTracking(enable);
			} catch (UnallocatedEx ex) {
				unallocatedEx.put(entry.getKey(), ex);
			}
		}

        // Now check the exceptions we caught and if there are enough of them,
        // throw an exception.
        final int numBadAntennas = unallocatedEx.size();
        final String cmd = enable ? "enable" : "disable";
        throwIfUnallocated(unallocatedEx, numBadAntennas,
                           cmd + " delay tracking");
    }

    /**
     * See the IDL file for a description of this function.
     * 
     * @throws HardwareFaultEx
     */
    @Override
    public boolean delayTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        logger.finest(getClass().getName() + ".delayTrackingEnabled");

        // Main loop that sends the command to each antenna
        Map<String, UnallocatedEx> unallocatedEx = 
                new TreeMap<String, UnallocatedEx>();

        Map<String, Boolean> results = new TreeMap<String, Boolean>();
        {
        	for (Entry<String, AntLOController> entry : antennaModeController.entrySet()) {
                try {// TODO. Do this all in parallel, with callbacks, as
                    // each antenna takes a few tenth's of a second
                    Boolean enabled = entry.getValue().
                                           getEnableDelayTracking();
                    results.put(entry.getKey(), enabled);
                } catch (UnallocatedEx ex) { // skip this antenna
                    unallocatedEx.put(entry.getKey(), ex);
                }
        	}
        }

        // Now check the exceptions we caught and, if there are enough of them,
        // throw an exception.
        final int numBadAntennas = unallocatedEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas,
                "determine if delay tracking is enabled");

        // Return just one result or an exception if there are disrepencies
        return uniqueResult(results, "Delay tracking");
    }

    /**
     * See the IDL file for a description of this function.
     * 
     * @throws HardwareFaultEx
     */
    @Override
    public void enableFringeTracking(boolean enable) throws HardwareFaultEx {
        logger.finest(getClass().getName() + ".enableFringeTracking");

        Map<String, UnallocatedEx> unallocatedEx = 
                    new TreeMap<String, UnallocatedEx>();
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
        	try{
                entry.getValue().setEnableFringeTracking(enable);
            } catch (UnallocatedEx ex) {
                unallocatedEx.put(entry.getKey(), ex);
            }
        }
        final int numBadAntennas = unallocatedEx.size();
        final String cmd = enable ? "enable" : "disable";
        throwIfUnallocated(unallocatedEx, numBadAntennas,
               cmd + " fringe tracking");
    }

    /**
     * See the IDL file for a description of this function.
     * 
     * @throws HardwareFaultEx
     */
    @Override
    public boolean fringeTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        logger.finest(getClass().getName() + ".fringeTrackingEnabled");
        // Main loop that sends the command to each antenna
        Map<String, UnallocatedEx> unallocatedEx = 
                    new TreeMap<String, UnallocatedEx>();
        Map<String, Boolean> results = new TreeMap<String, Boolean>();
        {

            for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
                
                try {// TODO. Do this all in parallel, with callbacks, as
                    // each antenna takes a few tenth's of a second
                    Boolean enabled = entry.getValue().
                                              getEnableFringeTracking();
                    results.put(entry.getKey(), enabled);
                } catch (UnallocatedEx ex) { // skip this antenna
                    unallocatedEx.put(entry.getKey(), ex);
                }
            }
        }
        // Now check the exceptions we caught and if there are enough of them,
        // throw an exception.
        final int numBadAntennas = unallocatedEx.size();
        throwIfUnallocated(unallocatedEx, numBadAntennas,
                "determine if fringe tracking is enabled");

        // Return just one result or an exception if there are discrepancies
        return uniqueResult(results, "Fringe tracking");
    }

    private boolean uniqueResult(Map<String, Boolean> results, String cmd)
            throws AntennasDisagreeEx {
        int numEnabled = 0;
        int numDisabled = 0;
        
        {// TODO There is probably a one line way to do this!
        	for(Entry<String, Boolean> entry:results.entrySet()){
        		if (entry.getValue() == true){
        			numEnabled++;
        		}else{
        			numDisabled++;
        		}
        	}
        
        }

        if (numEnabled == 0) {
            return false;
        } else if (numDisabled == 0) {
            return true;
        } else { // Antennas Disagree
            AcsJAntennasDisagreeEx jEx = new AcsJAntennasDisagreeEx();
            String msg = cmd + " is not consistently enabled"
                    + " for all antennas in the array.";
            jEx.setProperty("Message", msg);
            
            for(Entry<String, Boolean> entry:results.entrySet()){
            	String res = entry.getValue() ? "enabled" : "disabled";
                jEx.setProperty(entry.getKey(), res);
            }
            
            jEx.log(logger);
            throw jEx.toAntennasDisagreeEx();
        }
    }

    @Override
    public void optimizeSignalLevels(float ifTargetLevel, float bbTargetLevel)
            throws HardwareFaultEx {
        final long OptimizationTimeout = 30;

        logger.finest(getClass().getName() + ".optimizeSignalLevels");
        AntennaCallbackImpl cbObj = null;

        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object "
                + "for asynchronous execution";
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(e);
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        }

        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            entry.getValue().optimizeSignalLevelsAsynch(
                ifTargetLevel, bbTargetLevel, cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                ex.log(logger);
            }
        }

        try {
            cbObj.waitForCompletion(OptimizationTimeout);
        } catch (AcsJAsynchronousFailureEx e) {
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx();
            String msg = "Error detected in asynchronous execution of " +
                " optimizing signal levels";
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx();
            String msg = "Timeout while setting calibration devices";
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        }
    }

    @Override
     public void optimizeBBSignalLevels(float bbTargetLevel)
        throws HardwareFaultEx {
        final long OptimizationTimeout = 30;

        logger.finest(getClass().getName() + ".optimizeSignalLevels");
        AntennaCallbackImpl cbObj = null;

        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object "
                + "for asynchronous execution";
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(e);
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        }


        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            entry.getValue().
                optimizeBBSignalLevelsAsynch(bbTargetLevel,
                                             cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                String m = "Cannot optimizeBBSignalLevel for " 
                    + entry.getKey() + ";" + ex ;
                logger.warning(m);
                ex.log(logger);    
            }
        }

        try {
            cbObj.waitForCompletion(OptimizationTimeout);
        } catch (AcsJAsynchronousFailureEx e) {
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(e);
            String msg = "Error detected in asynchronous execution of " +
                " optimizing signal levels for antennas";
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(e);
            String msg = "Timeout while setting calibration devices";
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        }
    }

    @Override
    public void optimizeIFSignalLevels(float ifTargetLevel)
        throws HardwareFaultEx {
        final long OptimizationTimeout = 30;

        logger.finest(getClass().getName() + ".optimizeSignalLevels");
        AntennaCallbackImpl cbObj = null;

        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object "
                + "for asynchronous execution";
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(e);
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        }

 
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            entry.getValue().
                optimizeIFSignalLevelsAsynch(ifTargetLevel,
                                             cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                ex.log(logger);
            }
        }

        try {
            cbObj.waitForCompletion(OptimizationTimeout);
        } catch (AcsJAsynchronousFailureEx e) {
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx();
            String msg = "Error detected in asynchronous execution of " +
                " optimizing signal levels";
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx();
            String msg = "Timeout while setting calibration devices";
            jEx.setProperty("Message", msg);
            throw jEx.toHardwareFaultEx();
        }
    }

   /**
     * See the IDL file for a description of this function.
     * 
     * @throws IllegalParameterErrorEx
     * @throws HardwareFaultEx
     */
    @Override
    public short setFrequencyDSB(DSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            HardwareFaultEx {
        logger.finest(getClass().getName() + ".setFrequencyDSB");

        short numSolutions;
        Vector<DSBbasebandSpec> dsbspec = new Vector<DSBbasebandSpec>();
        for (int i = 0; i < bbspec.length; i++) dsbspec.add(bbspec[i]);

        currentSolution = -1;
        short sol;
        TuningParameters t;
        try {
            numSolutions = loSolutions.computeSolutionsDSB(dsbspec, overlap);
            sol          = loSolutions.preferredTuningSolution();
            // Get the tuning solution
            try {
                t = loSolutions.getTuningSolution(sol);
            } catch (Exception e) {
                AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx(e);
                throw ex.toIllegalParameterErrorEx();
            }

            // Setup the hardware
            if (array != null) configureHardware(t);
        } catch (AcsJIllegalParameterErrorEx ex) {
            final String errMsg = "Cannot set the frequency using the"
                    + " specified parameters.";
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            AcsJIllegalParameterErrorEx jEx = 
                    new AcsJIllegalParameterErrorEx(ex);
            jEx.setProperty("Detail", errMsg);
            throw jEx.toIllegalParameterErrorEx();
        } catch (Exception e) {
            final String errMsg = "Cannot set the frequency using the"
                    + " specified parameters.";
            logger.severe(errMsg + " Underlying error is:\n" + e);
            AcsJIllegalParameterErrorEx jex = 
                    new AcsJIllegalParameterErrorEx(e);
            throw jex.toIllegalParameterErrorEx();
        }
        // Setup the hardware
        currentTuningParameters = null;
        if (array != null) configureHardware(t);
        currentSolution         = sol;
        currentTuningParameters = t;
        return numSolutions;
    }

    /**
     * See the IDL file for a description of this function.
     * 
     * @throws IllegalParameterErrorEx
     * @throws HardwareFaultEx
     */
    @Override
    public short setFrequency(SSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            HardwareFaultEx {
        logger.finest(getClass().getName() + ".setFrequencySSB");

        short numSolutions;
        Vector<SSBbasebandSpec> dsbspec = new Vector<SSBbasebandSpec>();
        for (int i = 0; i < bbspec.length; i++) dsbspec.add(bbspec[i]);

        currentSolution = -1;
        short sol = 0;
        TuningParameters t;
        try {
            numSolutions = loSolutions.computeSolutions(dsbspec, overlap);
            sol = loSolutions.preferredTuningSolution();
            // Get the tuning solution
            try {
                t = loSolutions.getTuningSolution(sol);
            } catch (Exception e) {
                AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx(e);
                throw ex.toIllegalParameterErrorEx();
            }
        } catch (AcsJIllegalParameterErrorEx ex) {
            final String errMsg = "Cannot set the frequency using the"
                    + " specified  parameters.";
            AcsJIllegalParameterErrorEx jex = 
                    new AcsJIllegalParameterErrorEx(ex);
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            jex.setProperty("Detail", errMsg);
            throw jex.toIllegalParameterErrorEx();
        } catch (Exception e) {
            final String errMsg = "Cannot set the frequency using the"
                    + " specified  parameters, because:\n" + e;
            logger.severe(errMsg);
            AcsJIllegalParameterErrorEx jex = 
                    new AcsJIllegalParameterErrorEx(e);
            jex.setProperty("Detail", errMsg);
            throw jex.toIllegalParameterErrorEx();
        }
        // Setup the hardware
        currentTuningParameters = null;
        if (array != null) configureHardware(t);
        currentSolution = sol;
        currentTuningParameters = t;
        return numSolutions;
    }

    /**
     * See the IDL file for a description of this function.
     */ 
    @Override
    public void setSpectrum(String spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        logger.finest("alma.LocalOscillatorImpl.setSpectrum");
        SpectralSpecT ss = ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger);
        SimpleCallbackImpl cb = null;
        try {
            cb = new SimpleCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object "
                + "for setSpectrum asynchronous execution";
            logger.severe(msg);
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        }
        setSpectrumAsync(ss, cb.getExternalInterface());
        // waitUntilLocked();
        //} catch (TimeoutEx ex) {
        //String msg = "Cannot tune the LO's or receiver.";
        //throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
    }

    /**
     * Other observing modes use this, but should be converted to use
     * the (String, Callback) interface. Then this can be removed.
     */ 
    public void setSpectrumAsync(SpectralSpecT spectralSpec) 
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        SimpleCallbackImpl cb = null;
        try {
            cb = new SimpleCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object "
                + "for setSpectrum asynchronous execution";
            logger.severe(msg);
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        }
        setSpectrumAsync(spectralSpec, cb.getExternalInterface());
    }

    /**
     * See the IDL file for a description of this function.
     * TODO: Add callback
     */ 
    @Override
    public void setSpectrumAsync(String spectralSpec) // SimpleCallback cb)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        SpectralSpecT ss = ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger);
        SimpleCallbackImpl cb = null;
        try {
            cb = new SimpleCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object "
                + "for setSpectrum asynchronous execution";
            logger.severe(msg);
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        }
        setSpectrumAsync(ss, cb.getExternalInterface());
    }

    // Use the input frequencies in the SpectralSpec to compute solutions and
    // to set up the hardware. In R8.0 the rest frequencies will be used for
    // this (assuming they are really sky frequencies); in R8.1 the sky 
    // frequencies will be added to the SpectralSpec and these will be used.
    // If hasHardwareSetup is true then the specified hardware parameters are 
    // used to set the hardware, with no checking.
    public void setSpectrumAsync(SpectralSpecT spectralSpec, SimpleCallback cb)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        logger.finest("alma.LocalOscillatorImpl.setSpectrumAsync");

        TuningParameters tp = spectralSpecToTuningParameters(spectralSpec);

        // Setup the hardware, first nullifying the current solution
        currentTuningParameters = null;
        currentSolution         = -1;
        try {
	        /* Before we set the frequency, enable Walsh Functions and LO
	         offsetting as specified in the spectralSpec */
            enableWalshFunctionSwitching(spectralSpec);
            setAntennaFrequencyOffsetMode(spectralSpec);
            if (array != null) configureHardware(tp);
        } catch (HardwareFaultEx ex) {
            String msg = "Error setting frequency";
            AcsJObservingModeErrorEx omodeEx = 
                        new AcsJObservingModeErrorEx(ex);
            cb.report(new AsynchronousFailureAcsJCompletion(omodeEx).
                        toCorbaCompletion());
            throwObservingModeErrorEx(msg, omodeEx);
        } catch (IllegalParameterErrorEx ex) {
            AcsJObservingModeErrorEx omodeEx = 
                        new AcsJObservingModeErrorEx(ex);
            cb.report(new AsynchronousFailureAcsJCompletion(omodeEx).
                        toCorbaCompletion());
            throw(ex);
        }
        currentTuningParameters = tp;
        currentSolution         = tp.index;
        cb.report(new ACSErrOKAcsJCompletion().toCorbaCompletion());
        return;
    }

    /**
     * See the IDL file for a description of this function.
     */ 
    @Override
    public String dopplerShift(String spectralSpec,  String dopplerSource, 
               long epoch)
                    throws ObservingModeErrorEx, IllegalParameterErrorEx {      
        SpectralSpecT ss = ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger);
        FieldSourceT ds = null;
        // A null dopplerSource is allowed as a signal to do no doppler shift.
        // The null won't make it through CORBA call type-checking, but
        // could be used in a direct Java call.
        if (dopplerSource != null) {
            ds = ObservingModeUtilities.deserializeFieldSource(dopplerSource,
                    logger); 
        }       
        SpectralSpecT ssOut = dopplerShift(ss, ds, epoch);
        return ObservingModeUtilities.
                    serializeSpectralSpec(ssOut, logger).xmlString;
    }  
    /** 
     * The version of dopplerShift does all the work and uses the 
     * non-serialized inputs.
     */
    public SpectralSpecT dopplerShift(SpectralSpecT ss, FieldSourceT ds, 
                long epoch)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {

        FrequencySetupT fs = ss.getFrequencySetup();
        if (fs == null) {
            String msg = "The frequency setup is null";
            throwIllegalParameterErrorEx(msg,new AcsJIllegalParameterErrorEx());
        }
        int nBB = fs.getBaseBandSpecificationCount();
        if (nBB < 1 || nBB > 4) {
            String msg = 
                "Incorrect number of basebands in the frequency setup.\n"
                 + " A frequency setup can specify from 1 to 4 basebands.\n"
                 + " This setup contains " + nBB + " basebands.";
            throwIllegalParameterErrorEx(msg,new AcsJIllegalParameterErrorEx());
        }

        if (fs.getHasHardwareSetup()) {
            String m = "Doppler correction not done because "
                    + "'hasHardwareSetup' is set to true";
            opLog.info(m);
            return applyDoppler(ss, 1.0);
        }
        if (ds == null) {
            String m = "Doppler correction not done because ";
            m += "the doppler source is null";
            opLog.info(m);
            return applyDoppler(ss, 1.0);
        }
        if (fs.getDopplerReference() == 
                    FrequencySetupTDopplerReferenceType.TOPO) {
            String m = "Doppler correction not done because ";
            m += "the doppler mode is already 'TOPO'";
            opLog.info(m);
            return applyDoppler(ss, 1.0);
        }
        if (ds.getNonSiderealMotion() == false) { //Yep, false for equat & azel
            final SkyCoordinatesT coords = ds.getSourceCoordinates();
            final SkyCoordinatesTSystemType system = coords.getSystem();
            if (system == SkyCoordinatesTSystemType.AZEL) {
                String m = "Doppler correction not done because ";
                m += "the doppler source is of type AZEL";
                opLog.info(m);
                return applyDoppler(ss, 1.0);
            }
        }

        // OK, we need to do the Doppler calcs.
        // If epoch is near zero then substitute the current time
        if (epoch < 1e10) {
            epoch = Util.arrayTimeToACSTime(Util.getArrayTime().get());
        }

        VelocityT velT = ds.getSourceVelocity();    
        VelocityTDopplerCalcTypeType dc = velT.getDopplerCalcType();
        DopplerReferenceCode calcMode   = translateDopplerCalcEnum(dc);
        VelocityTReferenceSystemType vrT = velT.getReferenceSystem();
        RadialVelocityReferenceCode vmode = translateVelocityReferenceEnum(vrT);
        SpeedT speed    = velT.getCenterVelocity();
        double vel      = speed.getContent();
        String velUnit  = speed.getUnit();
        if (velUnit.equals("m/s")) {
            vel *= 0.001;
        }
        else if (velUnit.equals("km/h")) {
            vel /= 3600.0;
        }
        else if (!velUnit.equals("km/s")) {
            String msg = "The velocity unit of '" + velUnit
                        + "' is not supported.";
            opLog.severe(msg);
            throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
        }
        velUnit = "km/s";
        speed.setUnit(velUnit);
        // If the freq dop ref is LSRK, make sure that 0 VLSR is used for source
        if (fs.getDopplerReference() == 
                    FrequencySetupTDopplerReferenceType.LSRK) {
            boolean adjust = false;
            if (Math.abs(vel) > 0.001) adjust = true;
            if (vmode != RadialVelocityReferenceCode.LSRK) adjust = true;
            vel   = 0.0;
            vmode = RadialVelocityReferenceCode.LSRK;
            if (adjust) {
                String m = "A frequency dopplerReference of LSRK has been ";
                m += "requested; adjusting doppler source to 0.0 VLSR";
                opLog.info(m);
            }
        }

        if (ds.getNonSiderealMotion() == true) {
            FieldSourceTSolarSystemObjectType planet = ds.getSolarSystemObject();
            if (planet == FieldSourceTSolarSystemObjectType.EPHEMERIS) {
                // Ephemeris source
                EphemerisRow[] ephemeris = 
                    ObservingModeUtilities.ephemerisToIDL(
                        ds.getSourceEphemeris(), logger);
                double dopFactor = 
                            dopplerServer.dopFactorEphemeris(epoch, ephemeris);
                devLog.info("Dopfactor:" + dopFactor);
                return applyDoppler(ss, dopFactor);
            } 
            else {
                // Built-in planet 
                double dopFactor = 
                        dopplerServer.dopFactorPlanet(epoch, planet.toString());
                devLog.info("Dopfactor:" + dopFactor);
                return applyDoppler(ss, dopFactor);
            }
        } 
        else {
            // Standard equatorial source
            final SkyCoordinatesT coords = ds.getSourceCoordinates();
            final SkyCoordinatesTSystemType system = coords.getSystem();
            if (system == SkyCoordinatesTSystemType.J2000) {
                final double ra = ObservingModeUtilities.convertAngleToRadians(
                        coords.getLongitude());
                final double dec = ObservingModeUtilities.convertAngleToRadians(
                        coords.getLatitude());
                // TODO: Handle proper-motion and parallax
                double pmra     = 0.0;
                double pmdec    = 0.0;
                double parallax = 0.0;
                double dopFactor = dopplerServer.dopFactorEquatorial(epoch,
                         vel, vmode, calcMode, ra, dec, pmra, pmdec, parallax);
                devLog.info("Dopfactor: " + dopFactor);
                return applyDoppler(ss, dopFactor);
            } 
            else if (system == SkyCoordinatesTSystemType.AZEL) {
                String msg = "Any AZEL source should have already been handled";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
            } 
            else {
                String msg = "The '" + system
                        + "' coordinate system is not supported.";
                throwIllegalParameterErrorEx(msg,
                        new AcsJIllegalParameterErrorEx());
            }
        }
        String msg = "The dopplerShift method cannot figure out how to "
                        + "execute given the input dopplerSource information";
        throwIllegalParameterErrorEx(msg, new AcsJIllegalParameterErrorEx());
        return clone(ss); // Just to make the compiler happy...
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public String getHardwareParameters() throws IllegalParameterErrorEx {
        String s = "";
        try {
            s = loSolutions.getHardwareParameters();
        } catch (Exception e) {
            AcsJIllegalParameterErrorEx jex = new AcsJIllegalParameterErrorEx(e);
            throw jex.toIllegalParameterErrorEx();
        }
        return s;
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public short numberOfSolutions() {
        logger.finest(getClass().getName() + ".numberOfSolutions");
        return (short)loSolutions.numberOfSolutions();
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public short currentTuningSolution() {
        logger.finest(getClass().getName() + ".currentTuningSolution");
        return (short)currentSolution;
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public short preferredTuningSolution() {
        logger.finest(getClass().getName() + ".preferredTuningSolution");
        return loSolutions.preferredTuningSolution();
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public TuningParameters getTuningSolution(short solution)
            throws IllegalParameterErrorEx {
        logger.finest(getClass().getName() + ".getTuningSolution");

        try {
            return loSolutions.getTuningSolution(solution);
        } catch (Exception e) {
            AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx(e);
            throw ex.toIllegalParameterErrorEx();
        }
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public TuningParameters[] getAllTuningSolutions()
            throws IllegalParameterErrorEx {
        logger.finest(getClass().getName() + ".getAllTuningSolutions");

        try {
            Vector<TuningParameters> tps = loSolutions.getAllTuningSolutions();
            int len = tps.size();
            TuningParameters tparray[] = new TuningParameters[len];
            int i = 0;
            for (TuningParameters tp : tps)
                tparray[i++] = tp;
            return tparray;
        } catch (Exception e) {
            AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx(e);
            throw ex.toIllegalParameterErrorEx();
        }
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public void useTuningSolution(short solution)
            throws IllegalParameterErrorEx, HardwareFaultEx {
        logger.finest(getClass().getName() + ".useTuningSolution");

        final int nSols = loSolutions.numberOfSolutions();
        if (nSols <= 0) {
            final String errMsg = "There are no tuning solutions!"
                    + " Have you run setFrequency()?";

            logger.severe(errMsg);
            AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
            ex.setProperty("Detail", errMsg);
            throw ex.toIllegalParameterErrorEx();
        }
        if (solution < 0 || solution >= nSols) {
            String errMsg = "Only solutions between 0 - " + (nSols - 1) +
                " are available and you have requested solution " + solution;
            logger.severe(errMsg);

            AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
            ex.setProperty("Detail", errMsg);

            throw ex.toIllegalParameterErrorEx();
        }
        TuningParameters t;
        // Get the tuning solution
        try {
            t = loSolutions.getTuningSolution(solution);
        } catch (Exception e) {
            AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx(e);
            throw ex.toIllegalParameterErrorEx();
        }
        // Setup the hardware
        currentSolution = -1;
        currentTuningParameters = null;
        if (array != null) configureHardware(t);
        currentSolution = solution;
        currentTuningParameters = t;
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public double averageSkyFrequency() {
        double retval = 0.0;
        if (localOscilatorThread != null) {
            retval = localOscilatorThread.getAverageSkyFrequency();
        }
        if (retval == 0.0) {
            retval = loSolutions.averageSkyFrequency();
        }
        return retval;
    }     

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public TuningParameters getTuningParameters() 
            throws HardwareFaultEx  {
        if (currentTuningParameters == null) {
            String emsg = "No valid Tuning Parameters; ";
            emsg += "hardware not set to valid tuning solution/parameters";
            logger.severe(emsg);
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx();
            jEx.setProperty("Message", emsg);
            throw jEx.toHardwareFaultEx();
        }
        return currentTuningParameters;
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public void setTuningParameters(TuningParameters t) 
            throws IllegalParameterErrorEx, HardwareFaultEx  {
        // Setup the hardware
        currentSolution = -1;
        currentTuningParameters = null;
        if (array != null) configureHardware(t);
        currentTuningParameters = t;
    }


    /// See the IDL file for a description of this function
    @Override
    public StateData[] getStateTable() {
        logger.finest(getClass().getName() + ".getStateTable");

        // Obtain details on the antennas in this array
        
        ArrayList<StateData> stateTable = new ArrayList<StateData>();
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            try {
            	
                stateTable.add(entry.getValue().getCurrentStateData());  
                
            } catch (UnallocatedEx ex) {
                final String errMsg = 
                    "Unable to create state entry for antenna "  + 
                    entry.getKey() + ".  It claims to be unallocated";
                logger.severe(errMsg + " Underlying error is:\n"
                              + Util.getNiceErrorTraceString(ex.errorTrace));
            } catch (HardwareFaultEx ex) {
                final String errMsg = 
                    "Unable to create state entry for antenna "
                    + entry.getKey();
                logger.severe(errMsg + " Underlying error is:\n"
                              + Util.getNiceErrorTraceString(ex.errorTrace));
            }
        }
        return stateTable.toArray(new StateData[0]);
    }

    /// See the IDL file for a description of this function.
    @Override
    public CalibrationDevice getCalibrationDevice()        
        throws AntennasDisagreeEx {
        logger.finest(getClass().getName() + ".getCalibrationDevice");
        
        StateData[] stateTable = getStateTable();
        
        if (stateTable.length != antennaModeController.size()) {
            AcsJAntennasDisagreeEx jEx = new AcsJAntennasDisagreeEx();
            String msg = "Only " + stateTable.length + " antennas out of " +
                antennaModeController.size() + " responded without error.";
            jEx.setProperty("Message", msg);
            jEx.log(logger);
            throw jEx.toAntennasDisagreeEx();
        }

        CalibrationDevice candidateDevice = stateTable[0].calDevice;

        for (int i=1; i < stateTable.length; i++) {
            if (stateTable[i].calDevice != candidateDevice) {
                AcsJAntennasDisagreeEx jEx = new AcsJAntennasDisagreeEx();
                String msg = "At least two different devices were in use";
                jEx.setProperty("Message", msg);
                jEx.log(logger);
                throw jEx.toAntennasDisagreeEx();
            }
        }
        return candidateDevice;
    }

    /**
     * See the IDL file for a description of this function.
     */
    @Override
    public void setCalibrationDevice(CalibrationDevice cd) 
        throws ObservingModeErrorEx, TimeoutEx {

        logger.finest(getClass().getName() + ".setCalibrationDevice");
        
        final long CalibrationDeviceTimeout = 7;
        AntennaCallbackImpl cbObj = null;
        try {
            cbObj = new AntennaCallbackImpl(cs);
        } catch (AcsJContainerServicesEx e) {
            String msg = "Failed to create callback object " 
                + "for asynchronous execution";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        }
        
        // Main loop that sends the command to each antenna
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
             entry.getValue().
                setCalibrationDeviceAsynch(cd, cbObj.getExternalInterface());
            try {
                cbObj.addExpectedResponse(entry.getKey());
            } catch (AcsJInvalidRequestEx ex) {
                /* Something went wrong, log the error but continue */
                ex.log(logger);
            }
        }
        
        try {
            cbObj.waitForCompletion(CalibrationDeviceTimeout);
        } catch (AcsJAsynchronousFailureEx e) {
            String msg = "Failure in asynchronous execution of method" 
                + " setCalibrationDevice";            
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
        } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
            /* Translate to mode controller exception for now */
            AcsJTimeoutEx nex = new AcsJTimeoutEx(e);
            throw nex.toTimeoutEx();
        }
    }
    @Override
    public double[] getFrequency() throws HardwareFaultEx, AntennasDisagreeEx {
        logger.finest(getClass().getName() + ".getFrequency");

        double[] freqs = getLO2Frequencies();
        double lo1Freq;
        try {
            lo1Freq = getLO1Frequency();
        } catch (Exception e) {
            return freqs;
        }
        for (int i = 0; i < freqs.length; i++) {
            freqs[i] += lo1Freq;
            // 3E9 comes from:
            // 1. 4GHz baseband sampler (effectively a third LO)
            // 2. Half the baseband bandwidth i.e., 2GHz/2.
            // We use half the bandwidth to get the frequency at the center
            // of the band.
            freqs[i] -= 3E9;
        }
        return freqs;
    }

    @Override
    public double[] getFrequencyError() {
        double[] freqs = new double[4];
        for (int i = 0; i < 4; i++)
            freqs[i] = 0;
        return freqs;
    }

    @Override
    public double getLO1Frequency() throws HardwareFaultEx, AntennasDisagreeEx {
        logger.finest(getClass().getName() + ".getLO1Frequency");
        return getColdMultiplier() * (getLSFrequency() + getFLOOGFrequency());
    }

    @Override
    public double[] getLO2Frequencies() throws HardwareFaultEx,
            AntennasDisagreeEx {
        logger.finest(getClass().getName() + ".getLO2Frequencies");
        double[] lo2Freqs = new double[4];
        boolean  firstModeController = true;
        try {
            StringHolder thisBandName = new StringHolder();
            DoubleHolder thisFLOOGFreq = new DoubleHolder();
            NetSidebandHolder thisSB = new NetSidebandHolder();
            DoubleHolder thisLO20Freq = new DoubleHolder();
            DoubleHolder thisLO21Freq = new DoubleHolder();
            DoubleHolder thisLO22Freq = new DoubleHolder();
            DoubleHolder thisLO23Freq = new DoubleHolder();
            BooleanHolder thisband0band1IFPusb = new BooleanHolder();
            BooleanHolder thisband2band3IFPusb = new BooleanHolder();

            for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
                entry.getValue().getFrequencies
                (thisBandName, thisFLOOGFreq, thisSB, thisLO20Freq, 
                 thisLO21Freq, thisLO22Freq, thisLO23Freq,
                 thisband0band1IFPusb, thisband2band3IFPusb);
            if (!firstModeController) {
                // For now just compare the frequencies to within 1kHz.
                // TODO. Find out how accurate the comparison
                // really needs to be.
                // TODO. Compare all *four* LO2s
                final double freqdiff = lo2Freqs[0] - thisLO20Freq.value;
                if (Math.abs(freqdiff) > 1000) {
                    AcsJAntennasDisagreeEx jEx = new AcsJAntennasDisagreeEx();
                    String msg = "Cannot return an unambigious second LO"
                        + " frequency as its value differs across the "
                        + " array.";
                    jEx.setProperty("Detail", msg);
                    msg = "On antenna " + entry.getKey() + " its " 
                        + lo2Freqs[0] / 1E9 + " GHz.";
                        jEx.setProperty("Detail1", msg);
                        msg = "Use the setFrequency function to"
                                + " reset the second LO on all antennas.";
                        jEx.setProperty("Detail2", msg);
                        logger.severe(
                            Util.getNiceErrorTraceString(jEx.getErrorTrace()));
                        throw jEx.toAntennasDisagreeEx();
                    }
                } else {
                    lo2Freqs[0] = thisLO20Freq.value;
                    lo2Freqs[1] = thisLO21Freq.value;
                    lo2Freqs[2] = thisLO22Freq.value;
                    lo2Freqs[3] = thisLO23Freq.value;
                    firstModeController = false;
                }
            }
        } catch (UnallocatedEx ex) {
            final String errMsg = "Cannot get the second LO frequency as an"
                    + " AntLOController is not initialized.";
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.errorTrace));
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(ex.errorTrace);
            jEx.setProperty("Detail", errMsg);
            throw jEx.toHardwareFaultEx();
        }
        return lo2Freqs;
    }

    @Override
    public double getLSFrequency() throws HardwareFaultEx {
        logger.finest(getClass().getName() + ".getLSFrequency");
        double LSfreq = 0.0;
        try {
            LSfreq = photoRef.getFrequency();
        } catch (Exception e) {
            String emsg = "Trouble getting LS freq: " + e;
            logger.severe(emsg);
            AcsJHardwareFaultEx newEx = new AcsJHardwareFaultEx(e);
            newEx.setProperty("Detail", emsg);
            throw newEx.toHardwareFaultEx();
        }
        return LSfreq;    
    }

    @Override
    public double getCVRFrequency() throws HardwareFaultEx {
        logger.finest(getClass().getName() + ".getCVRFrequency");

        // Read the CVR frequency
        double freq = 0.0;
        AcsJCompletion completion;
        {
            CompletionHolder compHolder = new CompletionHolder();
            // The CVR is set/get in GHz, but we return Hz
            freq = cvr.FREQUENCY().get_sync(compHolder);
            completion = AcsJCompletion.fromCorbaCompletion(compHolder.value);
        }
        // If the completion contains an error then throw an exception.
        if (completion.isError()) {
            final AcsJException ex = completion.getAcsJException();
            final String errMsg = "Problem getting the frequency from the CVR."
                    + " Possible causes include:\n"
                    + "1. There is a problem communicating with the software "
                    + " on the ARTM\n"
                    + "2. The software on the ARTM cannot communicate "
                    + " with the CVR";
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            AcsJHardwareFaultEx newEx = 
                    new AcsJHardwareFaultEx(ex.getErrorTrace());
            newEx.setProperty("Detail", errMsg);
            throw newEx.toHardwareFaultEx();
        }
        // If the CVR frequency is zero then throw an exception.
        if (freq < 0.1) {
            final String errMsg = "The CVR frequency (" + freq
                    + "Hz) seems to be incorrect."
                    + " Possible causes include:\n"
                    + "1. The CVR hardware is faulty or misconfigured.\n"
                    + "2. The CVR software on the ARTM is faulty.";
            logger.severe(errMsg);
            AcsJHardwareFaultEx newEx = new AcsJHardwareFaultEx();
            newEx.setProperty("Detail", errMsg);
            throw newEx.toHardwareFaultEx();
        }
        return freq;
    }


    @Override
    public double getFLOOGFrequency() throws HardwareFaultEx,
            AntennasDisagreeEx {
        logger.finest(getClass().getName() + ".getFLOOGFrequency");

        double floogFreq = 0;
        NetSideband sideband = NetSideband.USB;
        boolean firstModeController = true;

        try {
            StringHolder thisBandName = new StringHolder();
            DoubleHolder thisFLOOGFreq = new DoubleHolder();
            NetSidebandHolder thisSB = new NetSidebandHolder();
            DoubleHolder thisLO20Freq = new DoubleHolder();
            DoubleHolder thisLO21Freq = new DoubleHolder();
            DoubleHolder thisLO22Freq = new DoubleHolder();
            DoubleHolder thisLO23Freq = new DoubleHolder();
            BooleanHolder thisband0band1IFPusb = new BooleanHolder();
            BooleanHolder thisband2band3IFPusb = new BooleanHolder();

            for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            	entry.getValue().getFrequencies
                    (thisBandName, thisFLOOGFreq, thisSB, thisLO20Freq, 
                     thisLO21Freq, thisLO22Freq, thisLO23Freq, 
                     thisband0band1IFPusb, thisband2band3IFPusb);
                if (!firstModeController) {
                    // For now just compare the frequencies to within 1kHz.
                    // TODO. Find out how accurate the comparison
                    // really needs to be.
                    double floogDiff = floogFreq - thisFLOOGFreq.value;
                    if (Math.abs(floogDiff) > 1000) {
                        AcsJAntennasDisagreeEx jEx = new AcsJAntennasDisagreeEx();
                        String msg = "Cannot return an unambigious FLOOG"
                                + " frequency as its value differs across the "
                                + " array.";
                        jEx.setProperty("Detail", msg);
                        msg = "On antenna " + entry.getKey() + " its " +
                            thisFLOOGFreq.value / 1E6 + " MHz.";
                        jEx.setProperty("Detail1", msg);
                        msg = "Use the setFrequency function to"
                                + " reset the second LO on all antennas.";
                        jEx.setProperty("Detail2", msg);
                        logger.severe(
                            Util.getNiceErrorTraceString(jEx.getErrorTrace()));
                        throw jEx.toAntennasDisagreeEx();
                    }
                } else {
                    floogFreq = thisFLOOGFreq.value;
                    sideband = thisSB.value;
                    firstModeController = false;
                }
            }
        } catch (UnallocatedEx ex) {
            final String errMsg = "Cannot get the second LO frequency as an"
                    + " AntLOController is not initialized.";
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.errorTrace));
            AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(ex);
            jEx.setProperty("Detail", errMsg);
            throw jEx.toHardwareFaultEx();
        }
        if (sideband == NetSideband.LSB)
            floogFreq *= -1;
        return floogFreq;
    }

    @Override
    public short getLSMultiplier() throws HardwareFaultEx {
        logger.finest(getClass().getName() + ".getLSMultiplier");

        // TODO. For ALMA this is a function of the observing
        // band. Its unclear if there will be functions in the photonic ref
        // that allow us to directly read this (but there should be).
        // For now, just return 5
        return 5;
    }

    @Override
    public short getColdMultiplier() throws HardwareFaultEx {
        logger.finest(getClass().getName() + ".getColdMultiplier");

        // TODO. This is a function of the observing band. So:
        // 1. Ask the front-end (which front-end?) what band we are using
        // 2. use that to lookup the cold multiplier

        // For now, get it from current solution
        if (currentTuningParameters == null) return 0;
        return currentTuningParameters.coldMultiplier;
    }
    
    @Override
    public String getPhotonicReference() {
        logger.finest(getClass().getName() + ".getPhotonicReference");
        return photoRef.name();
    }

    @Override
    public void setAntennaFrequencyOffsetMode(LOOffsettingMode mode)
            throws HardwareFaultEx {

        if (mode == LOOffsettingMode.LOOffsettingMode_NONE) {
            logger.info("Disabling LO Offsetting");
        } else if (mode == LOOffsettingMode.LOOffsettingMode_TWO_LOS) {
            logger.info("Enabling Two LO Offsetting Mode");
        } else if (mode == LOOffsettingMode.LOOffsettingMode_THREE_LOS) {
            logger.info("Enabling Three LO Offsetting Mode");
        } else {
            logger.info("Unknown LO Offsetting Mode Requested");
        }

        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            try {
				entry.getValue().setAntennaFrequencyOffsetMode(mode);
			} catch (UnallocatedEx ex) {
	            final String errMsg = "Cannot set the antenna frecuency offset mode in" + entry.getKey() +
                    " since the AntLOController is not initialized.";
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.errorTrace));
			}
        }
    }
    
    // Set the CVR and LaserSynthesizer
    @Override
    public void setCentralLOFreq(double LSfreq)
            throws IllegalParameterErrorEx, HardwareFaultEx {
        logger.fine(getClass().getName() + ".setCentralLOFreq");
        opLog.info( "Setting the photonic reference frequency"
                + " (CVR & laser synthesizer) to " 
                + String.format("%.6f", 1e-9 * LSfreq) + "GHz");
        try {
            photoRef.setFrequency(LSfreq);
        } catch (Exception e) {
            AcsJHardwareFaultEx ex = new AcsJHardwareFaultEx(e);
            String emsg = "Problem setting the laser synthesizer frequency";
            ex.setProperty("Detail", emsg);
            throw ex.toHardwareFaultEx();
        }
    }

    @Override
    public double getCVRAmplitude() throws HardwareFaultEx {
        logger.fine(getClass().getName() + ".getCVRFrequency");

        // Read the CVR amplitude
        double amp = 0.0;
        AcsJCompletion completion;
        {
            CompletionHolder compHolder = new CompletionHolder();
            amp = cvr.AMPLITUDE().get_sync(compHolder);
            completion = AcsJCompletion.fromCorbaCompletion(compHolder.value);
        }
        // If the completion contains an error then throw an exception.
        if (completion.isError()) {
            final AcsJException ex = completion.getAcsJException();
            final String errMsg = "Problem getting the signal"
                    + " amplitude from the CVR."
                    + " Possible causes include:\n"
                    + "1. There is a problem communicating with the software "
                    + " on the ARTM\n"
                    + "2. The software on the ARTM cannot communicate "
                    + " with the CVR";
            logger.severe(errMsg + "\nUnderlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            AcsJHardwareFaultEx newEx = new AcsJHardwareFaultEx(ex);
            newEx.setProperty("Detail", errMsg);
            throw newEx.toHardwareFaultEx();
        }
        logger.fine("amp = " + amp);
        return amp;
    }

    @Override 
    public void setCVRAmplitude(double amp) throws HardwareFaultEx,
            IllegalParameterErrorEx {
        logger.fine(getClass().getName() + ".setCVRAmplitude");

        // Check the amplitude is plausible
        if (amp < 0 || amp > 20) {
            // throw an exception
            final String errMsg = 
                  "Cannot set the CVR amplitude to " + amp
                  + "dBm as this equipment"
                  + " can only generate signals between 0 and 20dBm";
            logger.severe(errMsg);
            AcsJIllegalParameterErrorEx ex = new AcsJIllegalParameterErrorEx();
            ex.setProperty("Detail", errMsg);
            throw ex.toIllegalParameterErrorEx();
        }

        // Send it to the CVR
        try {
            cvr.SET_AMPLITUDE(amp);
        } catch(alma.ControlExceptions.INACTErrorEx ex) {
            final String errMsg = "Problem setting the amplitude in the CVR."
                + " Please check that the control subsystem is operational.";
	        logger.severe(errMsg + " Underlying error is:\n"
	                + Util.getNiceErrorTraceString(ex.errorTrace));
	        AcsJHardwareFaultEx newEx = new AcsJHardwareFaultEx(ex.errorTrace);
	        newEx.setProperty("Detail", errMsg);
	        throw newEx.toHardwareFaultEx();
        } catch (alma.EthernetDeviceExceptions.SocketOperationFailedEx ex) {
            final String errMsg = "Problem setting the amplitude in the CVR."
                    + " Possible causes include:\n"
                    + "1. There is a problem communicating with the CVR"
                    + " component.\n"
                    + "2. There is an Ethernet communication problem between"
                    + " the CVR component and the CVR hardware\n"
                    + "3. The CVR hardware is not functioning properly.\n";
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.errorTrace));
            AcsJHardwareFaultEx newEx = new AcsJHardwareFaultEx(ex.errorTrace);
            newEx.setProperty("Detail", errMsg);
            throw newEx.toHardwareFaultEx();
        } catch (alma.EthernetDeviceExceptions.IllegalParameterEx ex) {
            final String errMsg = "Problem setting the amplitude in the CVR."
                    + " Please check the value you specified is correct.";
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.errorTrace));
            AcsJHardwareFaultEx newEx = new AcsJHardwareFaultEx(ex.errorTrace);
            newEx.setProperty("Detail", errMsg);
            throw newEx.toHardwareFaultEx();
        }
    }

    /* This method returns the Offshoot reference to this object.
     * It will create the POA object if it has not already been 
     * created.
     */
    @Override
    public LocalOscillator getLocalOscillator() 
        throws ContainerServicesEx
    {
        LocalOscillator lo = null;
        try {
            if (tieBindingClass == null) {
                logger.fine("Creating LocalOscillatorPOATie");
                tieBindingClass = new LocalOscillatorPOATie(this);
                if (tieBindingClass == null) 
                    logger.warning("tieBindingClass is null");
            }

            if (corbaOffshoot == null) {
                logger.fine("Creating LocalOscillator corbaOffshoot");
                corbaOffshoot = LocalOscillatorHelper.
                    narrow(cs.activateOffShoot(tieBindingClass));
                if (corbaOffshoot == null) 
                    logger.severe("corbaOffshoot is null");
            }
        } catch (AcsJContainerServicesEx jex) {
            jex.log(logger);
            throw jex.toContainerServicesEx();
        }
        catch (Exception e) {
            logger.severe("exception:"+e);
        }
        return corbaOffshoot;
    }

    @Override
    public void cleanUp() {
        /* Turn off fringe and delay tracking */
        try {
            logger.fine("Shutting down delay compensation and fringe tracking.");
            enableDelayTracking(false);
            enableFringeTracking(false);
        } catch (HardwareFaultEx ex) {
            String msg = "Problem disabling delay compensation.";
            logSevereError(msg, ex.errorTrace, DEVELOPER.value);
        }
        
        abortSubscanSequence();
        
		try {
			//wait for 1/2 second.
			if (localOscilatorThread !=null) {
			    localOscilatorThread.join(500);
			    localOscilatorThread = null;
			}
			//Thread.sleep(10000);
		} catch (InterruptedException e) {
			// Nothing to do we are on our way out.
		}  
		
        corbaOffshoot = null;
        if (tieBindingClass != null) {
            try {
                cs.deactivateOffShoot(tieBindingClass);
            } catch (AcsJContainerServicesEx ex) {
                String msg ="Cannot deactivate the local oscillator offshoot.";
                logger.severe(msg);
            }
            tieBindingClass = null;
        }
        
        super.cleanUp();
    }

    @Override
    public void enable180DegreeWalshFunctionSwitching(boolean enable) 
        throws HardwareFaultEx {

        if (enable) {
            logger.info("Enabling 180 Degree Walsh Function Switching");
        } else {
            logger.info("Disabling 180 Degree Walsh Function Switching");
        }

        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
        	try {
				entry.getValue().enable180DegreeWalshFunctionSwitching(enable);
			} catch (UnallocatedEx ex) {
	            final String errMsg = 
                    "Cannot enable 180 degree walsh function in " 
                    + entry.getKey() +
                    " since the AntLOController is not initialized.";
                 logger.severe(errMsg + " Underlying error is:\n"
                               + Util.getNiceErrorTraceString(ex.errorTrace));
                 AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(ex);
                 jEx.setProperty("Detail", errMsg);
                 throw jEx.toHardwareFaultEx();
			}
        }
    }

    @Override
    public void setOffsetToRejectImageSideband(boolean rejectImage) 
	        throws HardwareFaultEx {
        if (rejectImage) {
            logger.info("LO Offsetting configured to reject image sideband");
        } 
        else {
            logger.info("LO Offsetting configured to reject signal sideband");
        }   
        for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            try {
                entry.getValue().setOffsetToRejectImageSideband(rejectImage);
	        } catch (UnallocatedEx ex) {
                final String errMsg = "Error in antenna" + entry.getKey()+
                "setting sideband rejections "+
                    "state AntLOController is not initialized.";
                logger.severe(errMsg + " Underlying error is:\n"
			        + Util.getNiceErrorTraceString(ex.errorTrace));
                AcsJHardwareFaultEx jEx = new AcsJHardwareFaultEx(ex);
                jEx.setProperty("Detail", errMsg);
                throw jEx.toHardwareFaultEx();
            }
        }
    }

    // ------------------------ End IDL methods -----------------------

    // ======================= HARDWARE CONFIGURATION ========================
    // All of the hardware setup is done here.
    // This method is synchronous and does not return until all the 
    private void configureHardware(TuningParameters t) 
        throws IllegalParameterErrorEx, HardwareFaultEx
    {
        logger.finest(getClass().getName() + ".configureHardware");

        for (int i=0; i<4; i++) {
            if (t.LO2[i].isUsed) {
                SidebandSelect sbp = SidebandSelect.Upper;
                if (t.LO2[i].skyFreqSideband == SkyFreqSideband.LSB)
                    sbp = SidebandSelect.Lower;
                double skyFreq = 0;
                try {
                    skyFreq = LOsolutions.actualSkyFreq(t, i, sbp);
                } catch (Exception e) {
                    AcsJIllegalParameterErrorEx ex =
                        new AcsJIllegalParameterErrorEx(e);
                    throw ex.toIllegalParameterErrorEx();
                }
               String msg = "Setting hardware Baseband" + i + 
                        ")  to frequency " +
                        String.format("%.3f", skyFreq*1E-9) + "GHz";
               opLog.info(msg);
            }
        }

  
        // To preserve backwards compatability make this method not change
        // the exceptions it throws.  This should be cleaned up as part of
        // the exception refactoring.
        try {
            // 1. Configure the central LO equipment
            setCentralLOFreq(t.LSFrequency);       

            // 2. Configure the LO equipment in each antenna
            final long TuningTimeout = 45;
            AntennaCallbackImpl cbObj = null;
            try {
                cbObj = new AntennaCallbackImpl(cs);
            } catch (AcsJContainerServicesEx e) {
                String msg = "Failed to create callback object " 
                    + "for asynchronous execution";
                throwObservingModeErrorEx(msg,
                                          new AcsJObservingModeErrorEx(e));
            }
            
            NetSideband lo1TuneHigh = NetSideband.USB;
            if (t.tuneHigh == false) lo1TuneHigh = NetSideband.LSB;
            ReceiverBand recBand = t.receiverBand;
            int coldMultiplier   = t.coldMultiplier;
        
            for (Entry<String, AntLOController> entry:antennaModeController.entrySet()) {
            	entry.getValue().setFrequenciesAsynch
                    (recBand, LO1Freq(t), t.FLOOGFrequency,
                     lo1TuneHigh,
                     coldMultiplier,
                     LOsolutions.getLO2Freq(t.LO2[0]),
                     LOsolutions.getLO2Freq(t.LO2[1]),
                     LOsolutions.getLO2Freq(t.LO2[2]),
                     LOsolutions.getLO2Freq(t.LO2[3]),
                     t.band0band1selectUSB,
                     t.band2band3selectUSB,
                     0,cbObj.getExternalInterface());
       
                try {
                    cbObj.addExpectedResponse(entry.getKey());
                } catch (AcsJInvalidRequestEx ex) {
                    /* Something went wrong, log the error but continue */
                    ex.log(logger);
                }
            }
        
            try {
                cbObj.waitForCompletion(TuningTimeout);
            } catch (AcsJAsynchronousFailureEx e) {
                String msg = "Failure in asynchronous execution of method" 
                    + " setFrequencies";            
                throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(e));
            } catch (alma.ControlExceptions.wrappers.AcsJTimeoutEx e) {
                /* Translate to mode controller exception for now */
                AcsJTimeoutEx nex = new AcsJTimeoutEx(e);
                throw nex.toTimeoutEx();
            } 
      
        } catch (ObservingModeErrorEx e) {
            /* Translate to hardware fault for now */
            AcsJHardwareFaultEx ex = new AcsJHardwareFaultEx(e);
            throw ex.toHardwareFaultEx();
        } catch (TimeoutEx e) {
            /* Translate to hardware fault for now */
            AcsJHardwareFaultEx ex = new AcsJHardwareFaultEx(e);
            throw ex.toHardwareFaultEx();
        }

    }
    // ===================== End Hardware Configuration ======================

    /** Extracts values from the FrequencySetup in the input SpectralSpec
     * and produces a completely populated and consistent FrequencySetup
     * in the FrequencySetup of the output SpectralSpec. If hasHardwareSetup
     * is false then an LO solution will be computed and values set for the
     * hardware parameters. If hasHardwareSetup is true then values such as
     * sky frequency will be computed based on the input hardware values. 
     * In both cases the output will have consistent values of sky frequency, 
     * etc and hardware parameters. The hasHardwareSetup field will not be
     * changed. The sideband field in the BLCorrelatorConfiguration 
     * SpectralWindows will also be set to match the selected sideband of the
     * first LO.
     */
    public SpectralSpecT alignSpectralSpec(SpectralSpecT spec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        TuningParameters tp = spectralSpecToTuningParameters(spec);
        SpectralSpecT    ss = tuningParametersToSpectralSpec(tp, spec, logger);
        if (true) {
            logSpectralSpec(spec, "Input:",  logger);
            logSpectralSpec(ss,   "Output:", logger);
        }
        return ss;
    }
    static public String alignSpectralSpec(String specString, Logger logger)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        SpectralSpecT spec = ObservingModeUtilities.deserializeSpectralSpec(
                specString, logger);
        LOsolutions los = new LOsolutions(logger);
        TuningParameters tp = spectralSpecToTuningParameters(spec, los, logger);
        SpectralSpecT ss    = tuningParametersToSpectralSpec(tp,  spec, logger);
        if (true) {
            logSpectralSpec(spec, "Input:",  logger);
            logSpectralSpec(ss,   "Output:", logger);
        }
        return ObservingModeUtilities.serializeSpectralSpec(ss, logger).
            xmlString;
    }

    /**
     * Calculate the actual first LO frequency given the specified parameters.
     */
    static private double LO1Freq(TuningParameters t) {
        double signFLOOG = 1;
        if (t.tuneHigh == false) signFLOOG = -1;
        double LOdriver = t.LSFrequency + signFLOOG*t.FLOOGFrequency;
        return t.coldMultiplier * LOdriver;
    }

    // -------------------- Observing Cycle Methods -----------------
    /**
     * This method will report to data capture all of the information
     * which is needed at the beginning of a subscan.
     *
     * This method is in Package scope so that the ObservingModes can use
     * it.
     *
     * These are:
     *   The subscanStateTable
     *   The Receiver Table (eventually)
     */
    void beginSubscan() 
        throws DataCapturerErrorEx  {
        try{
            dataCapturer.sendSubScanStateData(getStateTable());
        } catch (TableUpdateErrorEx ex) {
            String msg;
            msg = "Problem sending information to the ASDM (Data capture)";
             throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        } catch (DataErrorEx ex) {
            String msg;
            msg  = "Problem sending information to the ASDM (Data capture)";
            throwDataCapturerErrorEx(msg, new AcsJDataCapturerErrorEx(ex));
        }
    }

    public void setAntennaFrequencyOffsetMode(SpectralSpecT spectralSpec) 
            throws HardwareFaultEx {
        /*
         * This method sets the state of Lo Offsetting based on the values found
         * in the spectralSpec
         */
        AbstractCorrelatorConfigurationTLOOffsettingModeType mode = null;
        if (spectralSpec.getSpectralSpecTChoice() != null) {
            if (spectralSpec.getSpectralSpecTChoice().getBLCorrelatorConfiguration() != null) {
                BLCorrelatorConfigurationT cc =
                    spectralSpec.getSpectralSpecTChoice().getBLCorrelatorConfiguration();
                mode = cc.getLOOffsettingMode();

            } else if (spectralSpec.getSpectralSpecTChoice().getACACorrelatorConfiguration() != null) {
                ACACorrelatorConfigurationT cc =
                    spectralSpec.getSpectralSpecTChoice().getACACorrelatorConfiguration();
                mode = cc.getLOOffsettingMode();
            } else {
                mode = AbstractCorrelatorConfigurationTLOOffsettingModeType.NONE;
            }

            if (mode == AbstractCorrelatorConfigurationTLOOffsettingModeType.TWO_LOS) {
                setAntennaFrequencyOffsetMode(LOOffsettingMode.LOOffsettingMode_TWO_LOS);

            } else if (mode == AbstractCorrelatorConfigurationTLOOffsettingModeType.THREE_LOS) {
                setAntennaFrequencyOffsetMode(LOOffsettingMode.LOOffsettingMode_THREE_LOS);
            } else {
                setAntennaFrequencyOffsetMode(LOOffsettingMode.LOOffsettingMode_NONE);
            }
        } else {
            setAntennaFrequencyOffsetMode(LOOffsettingMode.LOOffsettingMode_NONE);
        }
    }

    public void enableWalshFunctionSwitching(SpectralSpecT spectralSpec) 
        throws HardwareFaultEx {
	    /* This method sets the state of Walsh function switching 
	     * based on the values found in the spectralSpec 
         * Notes: Only 180 Degree Walsh Functions are supported so far
	     */
    	if (spectralSpec.getSpectralSpecTChoice() != null) {
			if (spectralSpec.getSpectralSpecTChoice().getBLCorrelatorConfiguration() != null) {
			    BLCorrelatorConfigurationT cc =
			    	spectralSpec.getSpectralSpecTChoice().getBLCorrelatorConfiguration();
			    if (cc.hasEnable180DegreeWalshFunction()) {
			    	enable180DegreeWalshFunctionSwitching(cc.getEnable180DegreeWalshFunction());
			    } else {
			    	enable180DegreeWalshFunctionSwitching(false);
			    }
			} else if (spectralSpec.getSpectralSpecTChoice().getACACorrelatorConfiguration() != null) {
			    ACACorrelatorConfigurationT cc =
			    	spectralSpec.getSpectralSpecTChoice().getACACorrelatorConfiguration();
			    if (cc.hasEnable180DegreeWalshFunction()) {
			    	enable180DegreeWalshFunctionSwitching(cc.getEnable180DegreeWalshFunction());
			    } else {
			    	enable180DegreeWalshFunctionSwitching(false);
			    }
			} else {
			    enable180DegreeWalshFunctionSwitching(false);
			}
    	} else {
    	    enable180DegreeWalshFunctionSwitching(false);
    	}
    }
    
    /**
     * Queue a sequence of frequency changes. The function will queue
     * a series of frequency changes. The first frequency change will
     * be started as soon as possible and subsequent frequency changes
     * will be started as soon as the previous subscan has
     * completed. This function does not wait for any frequency
     * changes to complete (or even start). The waitForSubscanStart
     * function should be called to determine when a frequency change
     * has completed and if the change was successful. Normally the
     * waitForSubscanStart function should be called as many times as
     * their are subscans in the sequence. The waitForSubscanStart
     * returns a completion indicating if the frequency change was
     * successful. If their is any error the sequence of frequency
     * changes is aborted and the waitForSubscanStart should no longer
     * be called.
     * 
     * @param boundries The start and end time of each subscan in the sequence.  
     * @param spectralSpec. The spectral spec for each subscan in the sequence.
     * This array must be the same length as the boundaries array  
     * @throws ObservingModeErrorEx. If the hardware/software is not ready for these frequency changes.
     * @throws IllegalParameterErrorEx. If there is a problem with any of the parameters
     */
    public void startSubscanSequence(SubscanBoundries[] boundariesIn, 
            SpectralSpecT[] spectralSpecIn, CalibrationDevice[] acdPositions)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
    	 
    	 /*
    	  * This was supposed to be elegant. But the spectralSpecToTuningParameters is not static, so i cannot call it
    	  * from BoundrieSpectralSpecPairer. I dont want to touch that now so I will do multiple steps to get 
    	  * all the SubScanInfo I need to start this. Once spectralSpecToTuningParameters is static we can hide
    	  * all of this code in BoundrieSpectralSpecPairer
    	  */ 
    	//abortSubscanSequence();
    	if (localOscilatorThread !=null) {
    		localOscilatorThread = null;
    	}
    	
    	 List<BoundriesSpectralSpecPair> setup = BoundrieSpectralSpecPairer.getBoundriesSpectalSpecPairs(boundariesIn,spectralSpecIn);
   
    	 List<TuningParameters> tunningSetup = new ArrayList<TuningParameters>();

    	 List<Double> avgFreq =  new ArrayList<Double>();
          
    	 for(BoundriesSpectralSpecPair element:setup){
    		 tunningSetup.add(spectralSpecToTuningParameters(element.getSpectralSpec()));
             avgFreq.add(new Double(loSolutions.averageSkyFrequency()));

    	 }
    	 
    	 List<SubscanInfoPack> subscanInfoSetup = BoundrieSpectralSpecPairer.getSubscanInfoSetup(boundariesIn, spectralSpecIn, tunningSetup, acdPositions, avgFreq);

  //  	 for(SubscanInfoPack element:subscanInfoSetup){
    		 //logger.info(element.debug());
    		 //logger.logToAudience(Level.SEVERE, element.debug(), "");
    //		 
    //		 
    //	 }

		try {
			if (photoRefCallback == null) {
			    photoRefCallback = new ProgressCallbackImpl(cs);
			}
			//photoRefCallback.reset();
						
			photoRef.queueFrequencies(BoundrieSpectralSpecPairer
					.getPhotonicRefeceSubScanArray(subscanInfoSetup), photoRefCallback.getExternalInterface());
			
		} catch (INACTErrorEx e) {
			AcsJObservingModeErrorEx nex = new AcsJObservingModeErrorEx(e);
			nex.setProperty("Additional",
					"Could not queue frequencies in the LS");
			throw nex.toObservingModeErrorEx();
		} catch (AcsJContainerServicesEx e) {
			AcsJObservingModeErrorEx nex = new AcsJObservingModeErrorEx(e);
			nex.setProperty("Additional",
					"Could not create callback");
			throw nex.toObservingModeErrorEx();
		}
    		 
		for (Entry<String, AntLOController> entry : antennaModeController.entrySet()) {
			
		    try {   	
		    	java.util.Date datea = new java.util.Date();
				entry.getValue().queueFrequencies(BoundrieSpectralSpecPairer.getAntLOSubscanArray(subscanInfoSetup));
				java.util.Date dateb = new java.util.Date();
				logger.info("Antenna "+entry.getKey()+" took " + (dateb.getTime() - datea.getTime())/1000.0 + "seconds to queue frequency.");
		    } catch (IllegalParameterErrorEx ex){
	            final String errMsg = "Cannot queue frequencies in " + entry.getKey() +
                " since the AntLOController is reporting one of the as Illegal.";
                logger.severe(errMsg + " Underlying error is:\n"
                + Util.getNiceErrorTraceString(ex.errorTrace));
			} catch (UnallocatedEx ex) {
	            final String errMsg = "Cannot queue frequencies in " + entry.getKey() +
                " since the AntLOController is not initialized.";
                logger.severe(errMsg + " Underlying error is:\n"
                + Util.getNiceErrorTraceString(ex.errorTrace));
			} catch (HardwareFaultEx ex) {
	            final String errMsg = "Cannot queue frequencies in " + entry.getKey() +
                " since the AntLOController is reporting a hardware error.";
                logger.severe(errMsg + " Underlying error is:\n"
                + Util.getNiceErrorTraceString(ex.errorTrace));
			}
		}

		localOscilatorThread = new LocalOscilatorThread(photoRefCallback, antennaModeController, cs, subscanInfoSetup);
		
		localOscilatorThread.start();
        
    }
    
    /**
     * Wait for the next frequency change for the next subscan to
     * complete. See the startSubscanSequence for more details.
     * @param timout. How long to wait, in seconds before giving up.
     */ 
    public void waitForSubscanStart(long timeout) throws ObservingModeErrorEx {
        try {
            AcsJCompletion status = localOscilatorThread.waitForSubscanStart(timeout);
            if (status.isError()) {
                AcsJObservingModeErrorEx ex = new AcsJObservingModeErrorEx(status.getAcsJException());
                throw ex.toObservingModeErrorEx();
            }
        } catch (InterruptedException ex) {
            AcsJObservingModeErrorEx nex = new AcsJObservingModeErrorEx(ex);
            throw nex.toObservingModeErrorEx();
        }
    }
   
    /**
     * Stop a sequence of frequency changes. This function will stop
     * the sequence at the end of the current subscan.
     */
	public void abortSubscanSequence() {
		photoRef.abortFrequencyChanges();
		for (Entry<String, AntLOController> entry : antennaModeController
				.entrySet()) {
			entry.getValue().abortFrequencyChanges();
		}
		if (localOscilatorThread != null ) {
		    localOscilatorThread.abortSubscanSequence();
		}
	}
    
	
	public long[] timeToTune(SpectralSpecT[] specs, CalibrationDevice[] acdPositions)
			throws IllegalParameterErrorEx, ObservingModeErrorEx {
		try {
			double[] freqs = new double[specs.length];
			alma.Control.AntLOControllerPackage.SubscanInformation[] antennaSubscans = new alma.Control.AntLOControllerPackage.SubscanInformation[specs.length];

			long[] photrefTimes = null;

			Map<String, long[]> antennaTimes = new TreeMap<String, long[]>();

			for (int i = 0; i < specs.length; i++) {
				TuningParameters tunning = spectralSpecToTuningParameters(specs[i]);
				SubscanInfoPack subscanInfoSpecs = new SubscanInfoPack(
						new SubscanBoundries(), specs[i], tunning, acdPositions[i], 0.0, 1);

				antennaSubscans[i] = subscanInfoSpecs.getAntLOSubscanInfo();
				freqs[i] = tunning.LSFrequency;
			}

			// Process how long it takes for the Photonic Reference to look.
			photrefTimes = photoRef.timeToTune(freqs);

			// Process how long it takes for the antennas to lock the FrontEnd.
			for (Entry<String, AntLOController> entry : antennaModeController
					.entrySet()) {
				antennaTimes.put(entry.getKey(),
						entry.getValue().timeToTune(antennaSubscans));
			}

			long[] times = new long[specs.length];
			for (int i = 0; i < freqs.length; i++) {
				times[i] = 0;
				for (Entry<String, long[]> entry : antennaTimes.entrySet()) {
					times[i] = Math.max(times[i], entry.getValue()[i]);
				}
			}

			// Add antenna + photonic reference time.
			double onesecond = 1E7;
			times[0] = times[0] + (long) (3.5 * 1E7);
			for (int i = 0; i < times.length; i++) {
				logger.info("Sequence " + i + "--> FrontEnd takes: " + times[i]
						/ onesecond + " to lock. Photoref takes: "
						+ photrefTimes[i] / onesecond + " to lock.");
				times[i] = times[i] + photrefTimes[i];

			}

			// Process how long it takes for the ACD to move. This is done in
			// parallel to locking.
			antennaTimes.clear();
			for (Entry<String, AntLOController> entry : antennaModeController
					.entrySet()) {
				antennaTimes.put(entry.getKey(), entry.getValue()
						.timeToTuneACD(antennaSubscans));
			}

			for (int i = 0; i < freqs.length; i++) {
				long acdTime = 0;
				for (Entry<String, long[]> entry : antennaTimes.entrySet()) {
					acdTime = Math.max(acdTime, entry.getValue()[i]);
				}
				times[i] = Math.max(times[i], acdTime);
				logger.info("Sequence " + i + "--> FrontEnd+Photoref take: "
						+ times[i] / onesecond + " to lock. ACD takes: "
						+ acdTime / onesecond + " to move.");
			}
			return times;
		} catch (INACTErrorEx ex) {
			AcsJObservingModeErrorEx nex = new AcsJObservingModeErrorEx(ex);
			throw nex.toObservingModeErrorEx();
		}

	}
    
    // ---------------------------Internal methods ---------------------------
    // Convert a FrequencySetupTReceiverBandType enum to a ReceiverBand enum
    static private
    ReceiverBand convertReceiverBand(FrequencySetupTReceiverBandType rb) 
            throws IllegalParameterErrorEx {
        try {       
            return JReceiverBand.literal(rb.toString());
        } catch (Exception e) {
            AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx(e);
            throw ex.toIllegalParameterErrorEx();
        }
    }
    // Translate a VelocityTDopplerCalcType enum to a DopplerReferenceCode
    // enum, throwing a useful exception.
    static private DopplerReferenceCode
        translateDopplerCalcEnum(VelocityTDopplerCalcTypeType dc, Logger logger)
            throws IllegalParameterErrorEx {
        if (dc == VelocityTDopplerCalcTypeType.OPTICAL) {
            return DopplerReferenceCode.OPTICAL;
        }
        else if (dc == VelocityTDopplerCalcTypeType.RADIO) {
            return DopplerReferenceCode.RADIO;
        }
        else if (dc == VelocityTDopplerCalcTypeType.RELATIVISTIC) { 
            return DopplerReferenceCode.RELATIVISTIC;
        }
        else {
            String m = dc.toString() +" is not a recognized dopplerCalcType"; 
            logger.severe(m); 
            AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx();
            ex.setProperty("Additional", m);
            throw ex.toIllegalParameterErrorEx();
        }
    }
    private DopplerReferenceCode
        translateDopplerCalcEnum(VelocityTDopplerCalcTypeType dc)
            throws IllegalParameterErrorEx {
        return translateDopplerCalcEnum(dc, logger);
    }
    // Translate a VelocityTReferenceSystemType enum to a
    // RadialVelocityReferenceCode enum, throwing a useful exception.
    private RadialVelocityReferenceCode
        translateVelocityReferenceEnum(VelocityTReferenceSystemType rs)
            throws IllegalParameterErrorEx {
        if (rs == VelocityTReferenceSystemType.BAR) {
            return RadialVelocityReferenceCode.BARY;
        }
        else if (rs == VelocityTReferenceSystemType.LSRK) {
            return RadialVelocityReferenceCode.LSRK;
        }
        else if (rs == VelocityTReferenceSystemType.TOPO) {
            return RadialVelocityReferenceCode.TOPO;
        }
        else if (rs == VelocityTReferenceSystemType.HEL) {
            String m = "Using BAR for HEL (HEL is not supported)"; 
            opLog.info(m);
            return RadialVelocityReferenceCode.BARY; 
        }
        else if (rs == VelocityTReferenceSystemType.LSR) {
            return RadialVelocityReferenceCode.LSRD;
        }
        else {
            String m = rs.toString() +" is not a recognized "
                        + "VelocityTReferenceSystemType"; 
            logger.severe(m); 
            AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx();
            ex.setProperty("Additional", m);
            throw ex.toIllegalParameterErrorEx();
        }
    }
    // Translate a SideBandPreferenceType enum to a SidebandPreference enum,
    // throwing a useful exception.
    static private SidebandPreference translateSidebandPreferenceEnum(
                BaseBandSpecificationTSideBandPreferenceType sb, Logger logger)
            throws IllegalParameterErrorEx {
        if (sb == BaseBandSpecificationTSideBandPreferenceType.USB) {
            return SidebandPreference.Upper;
        }
        else if (sb == BaseBandSpecificationTSideBandPreferenceType.LSB) {
            return SidebandPreference.Lower;
        }
        else if (sb == BaseBandSpecificationTSideBandPreferenceType.NONE) { 
            return SidebandPreference.NoPreference;
        }
        else {
            String m = sb.toString() + " is not a recognized" +
                    " BaseBandSpecificationSideBandPreferenceType"; 
            logger.severe(m); 
            AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx();
            ex.setProperty("Additional", m);
            throw ex.toIllegalParameterErrorEx();
        }
    }
    private SidebandPreference translateSidebandPreferenceEnum(
                BaseBandSpecificationTSideBandPreferenceType sb)
            throws IllegalParameterErrorEx {
        return translateSidebandPreferenceEnum(sb, logger);
    }
    
    // Make a copy of a SpectralSpecT
    static public SpectralSpecT clone(SpectralSpecT ss, Logger logger)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        String s = 
            ObservingModeUtilities.serializeSpectralSpec(ss, logger).xmlString;
        return 
            ObservingModeUtilities.deserializeSpectralSpec(s, logger);
    }
    // Make a copy of a SpectralSpecT
    public SpectralSpecT clone(SpectralSpecT ss)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        return clone(ss, logger);
    }
  
    // Helper to dump out SpectralSpecs
    static private void logSpectralSpec(SpectralSpecT ss, String leader, 
                Logger logger) 
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        XmlEntityStruct s = 
            ObservingModeUtilities.serializeSpectralSpec(ss, logger);
        logger.fine(leader + s.xmlString);
    }

    // Helper to apply doppler factor to all bands; returns a copy of SpectSpec
    private SpectralSpecT applyDoppler(SpectralSpecT ssin, double dopFactor) 
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        SpectralSpecT ss = clone(ssin);
        FrequencySetupT fs = ss.getFrequencySetup();
        if (fs == null) {
            String msg = "The frequency setup is null";
            throwIllegalParameterErrorEx(msg,new AcsJIllegalParameterErrorEx());
        }
        int nBB = fs.getBaseBandSpecificationCount();
        for (int i=0; i < nBB; i++) {
            // Copy restFreq into skyFreq
            BaseBandSpecificationT b =fs.getBaseBandSpecification(i);
            FrequencyT f = b.getCenterFrequency();
            FrequencyT fout = new FrequencyT();
            fout.setContent(dopFactor*f.getContent());
            fout.setUnit(f.getUnit());
            b.setCenterFrequency(fout);
            fs.setBaseBandSpecification(i, b);
        }
        // Set ref to TOPO to show doppler has been done
        fs.setDopplerReference(FrequencySetupTDopplerReferenceType.TOPO);
        ss.setFrequencySetup(fs);
        return ss;
    }
    
    private TuningParameters spectralSpecToTuningParameters(SpectralSpecT ss) 
            throws IllegalParameterErrorEx {
        return spectralSpecToTuningParameters(ss, loSolutions, logger);
    }
    
    static private TuningParameters spectralSpecToTuningParameters(
                SpectralSpecT ss, LOsolutions loSolutions, Logger logger) 
            throws IllegalParameterErrorEx {
        FrequencySetupT fs = ss.getFrequencySetup();
        if (fs == null) {
            String msg = "The frequency setup is null in the spectralSpec";
            ObservingModeUtilities.throwIllegalParameterErrorEx(
                msg,new AcsJIllegalParameterErrorEx(), logger);
        }
        int nBB = fs.getBaseBandSpecificationCount();
        if (nBB < 1 || nBB > 4) {
            String msg = 
                "Incorrect number of basebands in the frequency setup."
                + " A frequency setup can specify from 1 to 4 basebands."
                + " This setup contains " + nBB + " basebands.";
            ObservingModeUtilities.throwIllegalParameterErrorEx(
                msg, new AcsJIllegalParameterErrorEx(), logger);
        }

        // Direct hardware setup
        if (fs.getHasHardwareSetup()) {
            return hasHardwareSetupToTuningParameters(fs, logger);
        }

        // Compute solutions for specified sky frequencies
        SSBbasebandSpec bbSpec[] = new SSBbasebandSpec[4];
        // We setup all 4 basebands, setting those not passed in as 'unused'
        for (int b = 0; b < 4; b++) {
            bbSpec[b] = new SSBbasebandSpec();
            bbSpec[b].sidebandPref = SidebandPreference.NoPreference;
            bbSpec[b].ifFreqHz  = 0.0;  // This means "center of the IF band"
            bbSpec[b].skyFreqHz = 0.0;  // This means unused.
            bbSpec[b].weight    = 0.0;
            // Loop over all input bb to see if we have specified this one
            for (int i = 0; i < nBB; i++) {
                int t; // The baseband name Type corresponds to our bb index
                BaseBandSpecificationTBaseBandNameType bn;
                BaseBandSpecificationT bs = fs.getBaseBandSpecification(i);
                bn = bs.getBaseBandName();
                t  = bn.getType();
                String msg = "";
                if (t == b) {
                    // Match; set the info that was input for this band
                    bbSpec[b].weight = bs.getWeighting();
                    FrequencyT f = bs.getCenterFrequency();
                    bbSpec[b].skyFreqHz = 
                        ObservingModeUtilities.convertFrequencyToHertz(f);                    
                    msg = "Setting " + bn + "(hardware Baseband" + b + 
                        ") to a frequency of " +
                        String.format("%.3f", bbSpec[b].skyFreqHz*1E-9) + "GHz";
                    BaseBandSpecificationTSideBandPreferenceType sb =
                            bs.getSideBandPreference();
                    if (sb != null) {
                        bbSpec[b].sidebandPref = 
                             translateSidebandPreferenceEnum(sb, logger);
                        msg += " with a SideBandPreference of " + sb;
                    }
                    else {
                        msg += ", but note that the SideBandPreference was";
                        msg += " not specified in the SpectralSpec.";
                    }                  
                    //logger.fine(msg);
                }
            }
        }
        Vector<SSBbasebandSpec> ssbspec = new Vector<SSBbasebandSpec>();
        for (int i = 0; i < bbSpec.length; i++) ssbspec.add(bbSpec[i]);
        try {
            // There is no way to specify this in the spectralSpec, 
            // so we make the choice
            Band2Band3Overlap overlap = Band2Band3Overlap.AutoChoice;
            // There is no way in the SpectralSpec to indicate a dsb type
            // solution, so we choose an ssb type here
            short numSolutions = loSolutions.computeSolutions(ssbspec, overlap);
            short sol = loSolutions.preferredTuningSolution();
            // Get and return the tuning solution parameters
            try {
                return loSolutions.getTuningSolution(sol);
            } catch (Exception e) {
                AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx(e);
                throw ex.toIllegalParameterErrorEx();
            }
        } catch (AcsJIllegalParameterErrorEx ex) {
            final String errMsg = "Cannot set the tuning parameters using the"
                    + " specified input parameters.";
            AcsJIllegalParameterErrorEx jex = 
                    new AcsJIllegalParameterErrorEx(ex);
            logger.severe(errMsg + " Underlying error is:\n"
                    + Util.getNiceErrorTraceString(ex.getErrorTrace()));
            jex.setProperty("Detail", errMsg);
            throw jex.toIllegalParameterErrorEx();
        } catch (Exception e) {
            final String errMsg = "Cannot set the tuning parameters using the"
                    + " specified input parameters because:\n" + e;
            logger.severe(errMsg);
            AcsJIllegalParameterErrorEx jex = 
                    new AcsJIllegalParameterErrorEx(e);
            jex.setProperty("Detail", errMsg);
            throw jex.toIllegalParameterErrorEx();
        }
    }

    // Hardware setup => TuningParameters
    static private TuningParameters hasHardwareSetupToTuningParameters(
            FrequencySetupT fs, Logger logger) throws  IllegalParameterErrorEx { 
        TuningParameters tp = new TuningParameters();
        tp.LO2 = new LO2Parameters[4];
        for (int lo2=0; lo2<4; lo2++) {
            tp.LO2[lo2] = new LO2Parameters();
            // This field cannot be null or the TuningParameters are 
            // invalid, so we fill it with a default for now
            tp.LO2[lo2].skyFreqSideband = SkyFreqSideband.USB;
        } 
        // Specify that this is a hardware spec, not a solution
        tp.index = -1; 
   
        FrequencySetupTReceiverBandType rbT = fs.getReceiverBand();
        ReceiverBand rb     = convertReceiverBand(rbT);
        FrequencyT F        = fs.getFloog();
        double floog        = ObservingModeUtilities.convertFrequencyToHertz(F);
        F                   = fs.getLO1Frequency();
        double lo1          = ObservingModeUtilities.convertFrequencyToHertz(F);
        boolean lo1TuneHigh = fs.getTuneHigh();
        tp.receiverBand     = rb;
        tp.FLOOGFrequency   = floog;
        tp.tuneHigh         = lo1TuneHigh;
        try {
            tp.LSFrequency = 
                    LOsolutions.LSfreqFromLO1(rb, lo1, floog, lo1TuneHigh);
            tp.coldMultiplier = LOsolutions.getColdMultiplier(rb);
        } catch (Exception e) {
            AcsJIllegalParameterErrorEx ex = 
                    new AcsJIllegalParameterErrorEx(e);
            throw ex.toIllegalParameterErrorEx();
        }
        // Loop over tuning parameter basebands      
        int nBB = fs.getBaseBandSpecificationCount();
        for (int tpb = 0; tpb < 4; tpb++) {
            int b = 0; // fs index to use for this tp band 
            boolean used = false;
            BaseBandSpecificationT bs = null;
            String tpbbname = "BB_" + (tpb+1);
            for (int i=0; i <nBB; i++) {
                bs = fs.getBaseBandSpecification(i);
                // The baseband name type corresponds to our bb index
                if (tpbbname.equals(bs.getBaseBandName().toString())) {
                    b = i;
                    used = true;
                    break;
                }
            }
            bs = fs.getBaseBandSpecification(b);
            if (!bs.hasUseUSB()) {
                String msg = "Baseband " + bs.getBaseBandName()  ;
                msg += " must have a value for useUSB";
                ObservingModeUtilities.throwIllegalParameterErrorEx(
                    msg, new AcsJIllegalParameterErrorEx(), logger);
            }
            if (!bs.hasUse12GHzFilter()) {
                String msg = "Baseband " + bs.getBaseBandName()  ;
                msg += " must have a value for useUSB";
                ObservingModeUtilities.throwIllegalParameterErrorEx(
                    msg, new AcsJIllegalParameterErrorEx(), logger);
            }
            boolean useUSB = bs.getUseUSB();
            tp.LO2[tpb].isUsed = used;
            F = bs.getLO2Frequency();
            double lo2 = ObservingModeUtilities.convertFrequencyToHertz(F);
            boolean tuneHigh = false;
            try {
                tuneHigh = LOsolutions.tuneHighFromLO2(rb, lo2);
            } catch (Exception e) {
                AcsJIllegalParameterErrorEx ex =
                        new AcsJIllegalParameterErrorEx(e);
                throw ex.toIllegalParameterErrorEx();
            }
            int sign = tuneHigh? 1:-1;
            tp.LO2[tpb].isUsed         = used;
            tp.LO2[tpb].tuneHigh       = tuneHigh;
            tp.LO2[tpb].DYTOFrequency  = lo2;
            tp.LO2[tpb].use12GHzFilter = bs.getUse12GHzFilter();
            // The FTS2 freq is not input, so we must infer it
            long harm = Math.round(lo2/125e6);
            double fts2 = lo2 - harm*125e6;
            tp.LO2[tpb].FTSFrequency  = Math.abs(fts2);
            // TODO: check if fts2 agrees with tuneHigh
            
            SidebandPreference sbp = translateSidebandPreferenceEnum(
                    bs.getSideBandPreference(), logger);
            if (useUSB) {   
                tp.LO2[tpb].skyFreqSideband = SkyFreqSideband.USB;
            }
            else {   
                tp.LO2[tpb].skyFreqSideband = SkyFreqSideband.LSB;
            }

            // Input may have conflicts, but we ignore and take last value
            boolean selectUSB = bs.getUseUSB(); 
            if (b < 2) tp.band0band1selectUSB = selectUSB;
            else       tp.band2band3selectUSB = selectUSB;                         
        }    
        return tp;
    }
    private TuningParameters hasHardwareSetupToTuningParameters(
            FrequencySetupT fs) throws  IllegalParameterErrorEx {
        return hasHardwareSetupToTuningParameters(fs, logger);
    } 
    // Take an input SpectralSpec and fill in all parameters from a tuning
    static private SpectralSpecT tuningParametersToSpectralSpec(
            TuningParameters tp, SpectralSpecT ssinput, Logger logger)
                 throws   ObservingModeErrorEx, IllegalParameterErrorEx { 
        SpectralSpecT   ss = clone(ssinput, logger);
        FrequencySetupT fs = ss.getFrequencySetup();
        if (fs == null) {
            String msg = "The frequency setup is null in the spectralSpec";
            ObservingModeUtilities.throwIllegalParameterErrorEx(
                msg, new AcsJIllegalParameterErrorEx(), logger);
        }
        // Check the receiverBand
        FrequencySetupTReceiverBandType band = fs.getReceiverBand();
        if (band == FrequencySetupTReceiverBandType.UNDEFINED) {
            String msg = "A receiverBand value of UNDEFINED " ;
            msg += "in the SpectalSpec is not allowed." ;
            ObservingModeUtilities.throwIllegalParameterErrorEx(
                msg, new AcsJIllegalParameterErrorEx(), logger);                
        }

        FrequencyT f = null;
        // Hardware specs were input (tp.index=-1), so fill in the sky stuff
        if (tp.index == -1) {
            double lo1 = LO1Freq(tp);
            f = new FrequencyT();
            f.setContent(lo1);
            f.setUnit("Hz");
            fs.setLO1Frequency(f);
            for (int i=0; i<4; i++) {
                String bbname = "BB_" + (i+1);
                // Start with a copy of input BB if it exists, otherwise start
                // with a new empty one.
                BaseBandSpecificationT b = 
                        getBaseBandSpecification(ssinput, bbname, logger);
                b.setBaseBandName(
                    BaseBandSpecificationTBaseBandNameType.valueOf(bbname));
                f = new FrequencyT();
                f.setContent(tp.LO2[i].DYTOFrequency);
                f.setUnit("Hz");
                b.setLO2Frequency(f);
                SidebandSelect sbp = SidebandSelect.Upper;
                boolean useUSB = true;
                if (tp.LO2[i].skyFreqSideband == SkyFreqSideband.LSB) {
                    sbp = SidebandSelect.Lower;
                    useUSB = false;
                }
                double skyFreq = 0;
                try {
                    skyFreq = LOsolutions.actualSkyFreq(tp, i, sbp);
                } catch (Exception e) {
                    AcsJIllegalParameterErrorEx ex =
                        new AcsJIllegalParameterErrorEx(e);
                    throw ex.toIllegalParameterErrorEx();
                }
                f = new FrequencyT();
                f.setContent(skyFreq);
                f.setUnit("Hz");
                b.setCenterFrequency(f);
                b.setUseUSB(useUSB);
                b.setUse12GHzFilter(tp.LO2[i].use12GHzFilter);
                b.setWeighting(100.0);
                BaseBandSpecificationTSideBandPreferenceType sbpt =
                    BaseBandSpecificationTSideBandPreferenceType.USB;
                if (sbp == SidebandSelect.Lower) sbpt = 
                    BaseBandSpecificationTSideBandPreferenceType.LSB;
                b.setSideBandPreference(sbpt); 
                replaceBaseBandSpecification(fs, b);
            }               
            ss.setFrequencySetup(fs);
        }
        // Sky freq input, fill in the hardware part of the spectralSpec
        else {
            // Check the receiverBand
            if (tp.receiverBand != convertReceiverBand(band)) {
                String msg = "The receiver band in the SpectralSpec (" ;
                msg += band + ") must be the same as the receiver band ";
                msg += " selected by the solution (" + tp.receiverBand + ")";
                ObservingModeUtilities.throwIllegalParameterErrorEx(
                    msg, new AcsJIllegalParameterErrorEx(), logger);                
            }
            // LO1
            double lo1 = LO1Freq(tp);
            f = new FrequencyT();
            f.setContent(lo1);
            f.setUnit("Hz");
            fs.setLO1Frequency(f);
            // FLOOG
            f = new FrequencyT();
            f.setContent(tp.FLOOGFrequency);
            f.setUnit("Hz");
            fs.setFloog(f);
            // FLOOG sideband
            fs.setTuneHigh(tp.tuneHigh);
            // Basebands
            int nBB = fs.getBaseBandSpecificationCount();
            for (int tpb = 0; tpb < 4; tpb++) {
                if (!tp.LO2[tpb].isUsed) continue;
                int b = 0; // fs index to use for this tp band 
                boolean used = false;
                BaseBandSpecificationT bs = null;
                for (int i=0; i <nBB; i++) {
                    bs = fs.getBaseBandSpecification(i);
                    // The baseband name type corresponds to our bb index
                    int fsb = bs.getBaseBandName().getType();
                    if (tpb == fsb) {
                        b = i;
                        break;
                    }
                }
                bs = fs.getBaseBandSpecification(b);
                // Center frequencies
                SidebandSelect sbp = SidebandSelect.Upper;
                boolean useUSB = true;
                if (tp.LO2[tpb].skyFreqSideband == SkyFreqSideband.LSB) {
                    sbp    = SidebandSelect.Lower;
                    useUSB = false;
                }
                double skyFreq = 0;
                try {
                    skyFreq = LOsolutions.actualSkyFreq(tp, tpb, sbp);
                } catch (Exception e) {
                    AcsJIllegalParameterErrorEx ex =
                         new AcsJIllegalParameterErrorEx(e);
                    throw ex.toIllegalParameterErrorEx();
                }
                f = new FrequencyT();
                f.setContent(skyFreq);
                f.setUnit("Hz");
                bs.setCenterFrequency(f);
                // LO2 frequencies
                f = new FrequencyT();
                f.setContent(tp.LO2[tpb].DYTOFrequency);
                f.setUnit("Hz");
                bs.setLO2Frequency(f);
                if (tpb < 2) {
                    bs.setUseUSB(tp.band0band1selectUSB);
                }
                else {
                    bs.setUseUSB(tp.band2band3selectUSB);
                }
                bs.setUse12GHzFilter(tp.LO2[tpb].use12GHzFilter);
            }
        }
        // This is the choice of correlator
        SpectralSpecTChoice ssc = ss.getSpectralSpecTChoice(); 
        if (ssc != null) {
            ACACorrelatorConfigurationT a = ssc.getACACorrelatorConfiguration();
            BLCorrelatorConfigurationT  b = ssc.getBLCorrelatorConfiguration();
            if (b != null) {
                // The BLcorrel BB config is linked to the BaseBandSpec by a ref
                BLBaseBandConfigT[] bbc = b.getBLBaseBandConfig();
                for (BLBaseBandConfigT bb: bbc) {
                    SchedBlockRefT bbsref = bb.getBaseBandSpecificationRef();
                    if (bbsref == null) {
                        String m = "There is a BLBaseBandConfig that does";
                        m += " not have a reference to a BaseBandSpecification";
                         throw new IllegalArgumentException(m);                                                   
                    }
                   // The "partId" of the reference is equal to the
                    // "entityPartId" of the object pointed by the reference. 
                    // We iterate over the baseband specifications until
                    // we find the object pointed to.
                    BaseBandSpecificationT theBaseBandSpec = null;
                    for (BaseBandSpecificationT bbspec : fs.getBaseBandSpecification()) {
                        if (bbspec.getEntityPartId().equals(bbsref.getPartId())) {
                            theBaseBandSpec = bbspec;
                        }
                    }
                    if (theBaseBandSpec == null) {
                        // The baseband spec reference is dangling
                        String m =  "dangled baseband spec ref, partId = "
                                + bbsref.getPartId();
                         throw new IllegalArgumentException(m);                           
                   }

                    // Now theBaseBandSpec refers to the specification referred
                    // from the BLBaseBandConfig. Get its SB and use it 
                    // to set all spectralWindows of the BLBaseBandConfig
                    AbstractSpectralWindowTSideBandType sb = null;
                    if (theBaseBandSpec.getUseUSB()) {
                        sb = AbstractSpectralWindowTSideBandType.USB;
                    }
                    else {
                        sb = AbstractSpectralWindowTSideBandType.LSB;
                    }
                    BLSpectralWindowT[] spws = bb.getBLSpectralWindow();
                    for (BLSpectralWindowT spw: spws) {
                        spw.setSideBand(sb);
                    }
                }
            }
        }
        return ss;
    }
    // If SS contains a BB with requested name, return a fresh copy;
    // if it doesn't have one, return a new empty BB
    static BaseBandSpecificationT getBaseBandSpecification(
            SpectralSpecT ss, String bbname, Logger logger) 
                throws ObservingModeErrorEx, IllegalParameterErrorEx {
        FrequencySetupT fs = ss.getFrequencySetup();
        boolean found = false;
        int nBB = fs.getBaseBandSpecificationCount();
        int i;
        String bbn = null;
        for (i=0; i<nBB; i++) {
            bbn = fs.getBaseBandSpecification(i).getBaseBandName().toString(); 
            if (bbname.equals(bbn)) {
                found = true;
                break;
            }
        }
        if (!found) return new BaseBandSpecificationT();

        SpectralSpecT sscopy = clone(ss, logger);
        return sscopy.getFrequencySetup().getBaseBandSpecification(i);
    }
    
    // If a BB exists with the same BB name as the new, replace it w/new   
    static void replaceBaseBandSpecification(FrequencySetupT fs, 
            BaseBandSpecificationT bbspec) {
        String bbname = bbspec.getBaseBandName().toString();
        boolean found = false;
        int nBB = fs.getBaseBandSpecificationCount();
        String nm;
        for (int i=0; i<nBB; i++) {
            nm =  fs.getBaseBandSpecification(i).getBaseBandName().toString();
            if (bbname.equals(nm)) {
                fs.setBaseBandSpecification(i, bbspec);
                found = true;
                break;
            }
        }
    }
       
   

    // ---------------------------------------------------------------------
    // Testing and debugging routines

    void dumpFreqs() {
        double[] freq = null;
        try {
            freq = getFrequency();
        } catch (Exception e) {
            spew("dumpFreqs says Bummer: " + e);
            e.printStackTrace();
            return;
        }
        String s = "Frequencies(GHz): ";
        int i = 1;
        for (double f : freq) {
            s += String.format("BB%1d=%2$8.3f ", i++, 1e-9 * f);
        }
        s += "\n";
        s += "Deviations(Hz):   ";

        spew(s);
    }

    static void spew(String s) {
        System.out.println(s);
    }

	
// -------------------------------- MAIN ---------------------------------
    // Used for running tests. To run these tests.
    // 1. make all
    // 2. cd CONTROL/Array/object/ControlArray
    // 3. acsStartJava -endorsed alma.Control.Array.LocalOscillatorImpl
    public static void main(String[] args) {
        ContainerServices cs = null;
        ObservingModeArray a = null;
        try {
            LocalOscillatorImpl lom = new LocalOscillatorImpl(cs, a);
            double f = 99e9;
            SSBbasebandSpec ssbSpec[] = new SSBbasebandSpec[1];
            ssbSpec[0] = LOsolutions.ssbBasebandSpec(f);
            lom.setFrequency(ssbSpec, Band2Band3Overlap.AutoChoice);
            spew("Num solutions: " + lom.numberOfSolutions());
            short current = lom.currentTuningSolution();
            spew("Current solution: " + current);
            TuningParameters t = lom.getTuningSolution(current);
            lom.loSolutions.tuningHeaders();
            lom.loSolutions.dumpTuning(t);
            // String s = "IFfreqs: ";
            // for (int i=0; i<4; i++) {
            // double ff=lom.loSolutions.getIFfrequency(t.LO2[i]);
            // s += String.format("%.1f ", 1e-9*ff);
            // }
            // spew (s);
            spew(lom.getHardwareParameters());

        } catch (Exception e) {
            spew(e.toString());
            e.printStackTrace();
        }
    }
}
