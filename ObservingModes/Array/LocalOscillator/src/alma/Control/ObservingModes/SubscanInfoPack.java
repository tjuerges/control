/* 
 *
 * ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006, 2008 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ObservingModes;

import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.Control.SubscanBoundries;
import alma.Control.TuningParameters;
import alma.common.LOsolutions.LOsolutions;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;

/**
 * @author ntroncos
 *
 */

public final class SubscanInfoPack {
	private alma.Control.AntLOControllerPackage.SubscanInformation antLOSubscanInfo = null;
	private alma.Control.PhotonicReferencePackage.SubscanInformation photoRefSubscanInfo = null;
    private TuningParameters tuningParameters = null;
	private long subscanNumber;
    private double averageSkyFrequency;


	public SubscanInfoPack(SubscanBoundries boundries, SpectralSpecT spectralspec, TuningParameters tunning, CalibrationDevice acd, double avg,long subscanNumber) {

/*        ACS::Time startTime; //! When to start switching. 
        // !It may take a few seconds for the tuning to complete
        ReceiverBandMod::ReceiverBand band; //! The front-end recever band to use
        double photoRefFreq; //! The frequency of the photonic reference
        double floogFreq;    //! The frequency of the FLOOG
        boolean addFLOOG;    //! True if the FLOOG is added to the photonic reference frequency
        long coldMultiplier; //! The cold multiplier factor. This depends on the band. 
        // TODO. Perhaps this can be obtained using a static function in the FrontEnd
        double lo2BBpr0Freq; //! The overall frequency of the second LO for baseband pair 0
        double lo2BBpr1Freq; //! The overall frequency of the second LO for baseband pair 1
        double lo2BBpr2Freq; //! The overall frequency of the second LO for baseband pair 2
        double lo2BBpr3Freq; //! The overall frequency of the second LO for baseband pair 3
        boolean ABUpperSideBand; //! If true then basebands 0 & 1 use the upper sideband
        boolean CDUpperSideBand; //! If true then basebands 2 & 3 use the upper sideband
        boolean use12GHzFilterBBpr0; //! If true use the 12GHz low-pass filter on baseband pair 0
        boolean use12GHzFilterBBpr1; //! On baseband pair 1
        boolean use12GHzFilterBBpr2; //! On baseband pair 2
        boolean use12GHzFilterBBpr3; //! On baseband pair 3
        CalibrationDeviceMod::CalibrationDevice acdState;
        //! The position of the ALMA calibration device (ACD). Only a subset of
        //! the enumeration values are allowed.
         
*/
		this.subscanNumber = subscanNumber;
		
		antLOSubscanInfo = new alma.Control.AntLOControllerPackage.SubscanInformation();
		
		photoRefSubscanInfo = new alma.Control.PhotonicReferencePackage.SubscanInformation();
        
        tuningParameters = tunning;
        
        averageSkyFrequency = avg;


		
		if (subscanNumber == 1) {
		    antLOSubscanInfo.startTime = 0;
		    photoRefSubscanInfo.startTime = 0;
		} else {
		    antLOSubscanInfo.startTime = boundries.endTime;
		    photoRefSubscanInfo.startTime = boundries.endTime;
		}
		antLOSubscanInfo.band = tunning.receiverBand;
		antLOSubscanInfo.photoRefFreq = tunning.LSFrequency;
		antLOSubscanInfo.floogFreq = tunning.FLOOGFrequency;
		antLOSubscanInfo.addFLOOG = tunning.tuneHigh;
		antLOSubscanInfo.coldMultiplier = tunning.coldMultiplier;
		antLOSubscanInfo.lo2BBpr0Freq = LOsolutions.getLO2Freq(tunning.LO2[0]);
		antLOSubscanInfo.lo2BBpr1Freq = LOsolutions.getLO2Freq(tunning.LO2[1]);
		antLOSubscanInfo.lo2BBpr2Freq = LOsolutions.getLO2Freq(tunning.LO2[2]);
		antLOSubscanInfo.lo2BBpr3Freq = LOsolutions.getLO2Freq(tunning.LO2[3]);
		antLOSubscanInfo.ABUpperSideBand = tunning.band0band1selectUSB;
		antLOSubscanInfo.CDUpperSideBand = tunning.band2band3selectUSB;
		antLOSubscanInfo.use12GHzFilterBBpr0 = false;
		antLOSubscanInfo.use12GHzFilterBBpr1 = false;
		antLOSubscanInfo.use12GHzFilterBBpr2 = false;
		antLOSubscanInfo.use12GHzFilterBBpr3 = false;
		try {
			antLOSubscanInfo.use12GHzFilterBBpr0 = spectralspec
					.getFrequencySetup().getBaseBandSpecification()[0]
					.getUse12GHzFilter();
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
		}
		try {
			antLOSubscanInfo.use12GHzFilterBBpr1 = spectralspec
					.getFrequencySetup().getBaseBandSpecification()[1]
					.getUse12GHzFilter();
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
		}
		try {
			antLOSubscanInfo.use12GHzFilterBBpr2 = spectralspec
					.getFrequencySetup().getBaseBandSpecification()[2]
					.getUse12GHzFilter();
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
		}
		try {
			antLOSubscanInfo.use12GHzFilterBBpr3 = spectralspec
					.getFrequencySetup().getBaseBandSpecification()[3]
					.getUse12GHzFilter();
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
		}
		antLOSubscanInfo.acdState = acd;
		photoRefSubscanInfo.frequency = tunning.LSFrequency;
		
	}

//	private CalibrationDevice getACDState(SpectralSpecT spectralspec) 
//	{
//		//TODO: Stubbed.
//		return CalibrationDevice.NONE;
//	}
	
	public  alma.Control.AntLOControllerPackage.SubscanInformation getAntLOSubscanInfo() {
		return antLOSubscanInfo;
	}

	public alma.Control.PhotonicReferencePackage.SubscanInformation getPhotoRefSubscanInfo() {
		return photoRefSubscanInfo;
	}
	
	public long getSubscanNumber() 
	{
		return subscanNumber;
	}
	

    public double getAverageSkyFrequency()
    {
        return averageSkyFrequency;
    }


	public String debug() 
	{
		String bla = new String("AntLO:\nStartTime: "+ antLOSubscanInfo.startTime +"\n" +
		"Band: "+antLOSubscanInfo.band +"\n" +
		"PhotRef: "+antLOSubscanInfo.photoRefFreq +"\n" +
		"Floog Freq: "+antLOSubscanInfo.floogFreq +"\n" +
		"Add Floog: "+antLOSubscanInfo.addFLOOG+"\n" +
		"ColdMultiplier: "+antLOSubscanInfo.coldMultiplier +"\n" +
		"LO2 0: "+antLOSubscanInfo.lo2BBpr0Freq +"\n" +
		"LO2 1: "+antLOSubscanInfo.lo2BBpr1Freq +"\n" +
		"LO2 2: "+antLOSubscanInfo.lo2BBpr2Freq +"\n" +
		"LO2 3: "+antLOSubscanInfo.lo2BBpr3Freq +"\n" +
		"ABUpper: "+antLOSubscanInfo.ABUpperSideBand+"\n" +
		"CDUpper: "+antLOSubscanInfo.CDUpperSideBand+"\n" +
		"Use filter 0: "+antLOSubscanInfo.use12GHzFilterBBpr0 +"\n" +
		"Use filter 1: "+antLOSubscanInfo.use12GHzFilterBBpr1 +"\n" +
		"Use filter 2: "+antLOSubscanInfo.use12GHzFilterBBpr2 +"\n" +
		"Use filter 3: "+antLOSubscanInfo.use12GHzFilterBBpr3 +"\n" +
		"ACD: "+antLOSubscanInfo.acdState +"\n" +
		"Photo Ref:\nFreq: "+photoRefSubscanInfo.frequency +"\n" +
		"StartTime: "+photoRefSubscanInfo.startTime+" Now: "+alma.acs.util.UTCUtility.utcJavaToOmg(System.currentTimeMillis()));
		return bla;
	}

}
