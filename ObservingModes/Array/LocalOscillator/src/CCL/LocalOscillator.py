#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id:"
#
#


from log_audience import OPERATOR
import ACSLog
import Control
import ControlExceptionsImpl;
import CCL.SpectralSpec
from PyDataModelEnumeration import PyCalibrationDevice
from CCL.EnumerationHelper import *
from CCL.LocalOscillatorInterferometricInterface import LocalOscillatorInterferometricInterface


class LocalOscillator(LocalOscillatorInterferometricInterface):
    '''
    This is the binding class to the Local Oscillator offshoot, all
    exprorted methods are implemented through this interface.
    '''
    
    def __init__(self, offshoot, logger):
        LocalOscillatorInterferometricInterface.__init__(self, offshoot,
                                                         logger) 

    def numberOfSolutions(self):
        """Returns the number of tuning solutions. This function will
        return zero if there are no tuning solutions (perhaps because
        setFrequency has not been run successfully)."""
        return self._offshoot.numberOfSolutions()
    
    def currentTuningSolution(self):
        """Returns the index of the tuning solution currently being
        used. This index indicates which of the TuningSolutions, as
        returned by the getAllTuningSolutions function, is currently
        being used. Normally it will return a number between zero and
        one less than the value returned by the
        numberOfTuningSolutions function. It will return a negative
        number if there are no tuning solutions (perhaps because
        setFrequency has not been run successfully)."""            
        return self._offshoot.currentTuningSolution()
    
    def preferredTuningSolution(self):
        """Returns the index of the tuning solution with the best score.
        This is an index of the TuningSolution set, as returned by
        the getAllTuningSolutions function. Normally it will return 
        a number between zero and one less than the value returned by the
        numberOfTuningSolutions function. It will return a negative
        number if there are no tuning solutions (perhaps because
        setFrequency has not been run successfully)."""            
        return self._offshoot.preferredTuningSolution()

    def getTuningSolution(self, solutionNumber):
        """ Retrieve the parameters of a specified tuning solution. This
        function must be supplied with an index between zero and one
        less than the value returned by the numberOfSolutions
        function otherwise it will throw an IllegalParameterErrorEx
        exception. It will return the parameters of the requested
        tuning solution."""
        return self._offshoot.getTuningSolution(solutionNumber)
                
    def useTuningSolution(self, solutionNumber):
        """ Specify which tuning solution to use. The input parameter must
        be between 0 and one less than the value returned by the
        numberOfSolutions function (inclusive) otherwise an
        IllegalParameterErrorEx exception will be thrown. The LO hardware
        is set by this method and if there is any problem configuring
        the hardware a HardwareFault exception is generated."""
        return self._offshoot.useTuningSolution(solutionNumber)
             
    def getHardwareParameters(self):
        """ Get a string representation for the underlying hardware parameters 
        used by the setFrequency method. The string contains multiple lines,
        including two header lines and one line for each band. 
        See printHardwareParameters."""
        return self._offshoot.getHardwareParameters()
             
    def printHardwareParameters(self):
        """Print the underlying hardware parameters used by the setFrequency
        method. The string contains multiple lines, including two header lines 
        and one line for each band. Useful for debugging."""
        print self._offshoot.getHardwareParameters()

    def setTuningParameters(self, parameters):
        """Explicitly specify a tuning solution to use. This function
        will check that all the supplied parameters are acceptable and
        if not it will throw an IllegalParameterErrorEx exception. It
        will then append this supplied tuning solution to the end of the
        current list of tuning solutions and return an index that can
        be used as the input parameter for the {get,use}TuningSolution
        functions. This function will also set the LO hardware to
        use the newly supplied tuning solution so there is no need to
        call useTuningSolution immediately after this function."""
        raise Exception, "setTuningParameters is not yet implemented"
        
    def tuningString(self, tuningSolutions, header=True, verbose=True):
        """Make a human readable string for a single tuning solution or a list 
        of tuning solutions.
        Parameters:
         tuningSolutions: single or list of tuning solutions, as are 
           returned by getAllTuningSolutions or getTuningSolution
         header: boolean indicating that a header be added before the table,
           default = True  
         verbose: boolean to control output of derived parameters 
            (Fsky, Fwca, coldX, Flo1) in addition to the TuningParameters;
            default=True 
        """
        def band2str(r):
            i = str(r).rfind('_')
            return str(r)[i+1:i+3]
        sols = tuningSolutions
        if type(tuningSolutions) != list:
            sols = [tuningSolutions]
        m = "" 
        if header :
            m += "Soln Bnd BB Use"
            if verbose: m += "     Sky"
            m +=  "      LS  FLG SB"
            if verbose: m += "     WCA coldX"
            if verbose: m += "     LO1"
            m += "    LO2   FTS2 SB   IF  Err Score\n" 
        f1 = "%4d %3s %2d" 
        for t in sols:
            ls      = t.LSFrequency
            coldX = t.coldMultiplier
            floog   = t.FLOOGFrequency
            floogsb = 1
            floogsbstr = "U"
            if not t.tuneHigh : 
                floogsb    = -1
                floogsbstr = "L"
            wca = ls + floogsb*floog
            band = t.receiverBand
            lo1 = coldX*wca
            bb = 0
            for lo2 in t.LO2:
                bb0 = (bb == 0)
                used = lo2.isUsed
                if used: usedC = 'Y'
                else   : usedC = 'n'
                if bb0: m += "%4d %3s" %(t.index, band2str(band))
                else:       m += "%8s" %""
                m += " %2s %3s" %(bb, usedC)
                flo2 = lo2.DYTOFrequency
                fts2 = lo2.FTSFrequency
                fts2sbstr = "U"
                if not lo2.tuneHigh: fts2sbstr = "L"
                sb = 1
                if lo2.skyFreqSideband == Control.LSB: sb = -1
                fif  = flo2-3e9
                fsky = lo1 + sb*fif
                if verbose:  
                    if used : m += " %7.3f" %(fsky*1e-9)
                    else    : m += "%8s" %""
                if bb0: m += " %7.3f %4.1f  %s" %(ls*1e-9,floog*1e-6,floogsbstr)
                else  : m += "%16s" %""
                if verbose: 
                    if bb0 : m += " %7.3f %5d" %(wca*1e-9,coldX)
                    else   : m += "%14s" %""
                if verbose :
                    if bb0 : m += " %7.3f" %(lo1*1e-9)
                    else   : m += "%8s" %""
                m += " %6.3f" %(flo2*1e-9)   
                m += " %6.3f  %s %4.1f"  %(fts2*1e-6,fts2sbstr,fif*1e-9)  
                if bb0: m += " %4.1f %5.2f"  %(t.weightedError*1e-6, t.score) 
                bb += 1
                m += "\n"                    
        return m 
               
    def printTunings(self, tuningSolutions, header=True, verbose=True):
        """Print a single tuning solution or a list of tuning solutions.
        Parameters:
         tuningSolutions: single or list of tuning solutions, as are 
           returned by getAllTuningSolutions or getTuningSolution
         header: boolean indicating that a header be added before the table,
           default = True  
         verbose: boolean to control out of derived parameters 
            (Fsky, Fwca, coldX, Flo1) in addition to the TuningParameters;
            default=True 
        """
        print self.tuningString(tuningSolutions, header, verbose)

    def getAllTuningSolutions(self, sort='SCORE'):
        """Retrieve the parameters of all the tuning solutions. The
        returned sequence will be zero length if there are no tuning
        solutions.
        Parameter:
          sort:    string indicating which item to sort on. Choices:
            SCORE: score, high to low (default)
            SOL:   solution index
            LO1:   first lo frequency
            ERR:   frequency error (average weighted across all used bands)"""
        def lo1(t):    
            coldx = t.coldMultiplier   
            floog = t.FLOOGFrequency
            floogsb = 1
            if not t.tuneHigh : floogsb = -1
            return coldx*(t.LSFrequency - 125e6+floogsb*floog)
        sol = self._offshoot.getAllTuningSolutions()
        if sort == 'SOL': 
            return sol
        elif sort == 'LO1':
            def sortfnc(t1, t2): return cmp(lo1(t1), lo1(t2))
            sol.sort(sortfnc)    
        elif sort == 'SCORE':
            def sortfnc(t1, t2): return cmp(t2.score, t1.score)
            sol.sort(sortfnc)    
        elif sort == 'ERR':
            def sortfnc(t1, t2): return cmp(t1.weightedError, t2.weightedError)
            sol.sort(sortfnc) 
        else:
            print "Unknown sort criterion, defaulting to 'SCORE'"
            print " Try 'SOL', 'LO1', 'SCORE', or 'ERR'"       
        return sol     
        
    def printTuningSolution(self, solutionNumber, header=True, verbose=True):
        """Print a single tuning solution.
        Parameters:
         solutionNumber: number of solution to print, starting at zero
         header: boolean indicating that a header be added before the table,
           default = True  
         verbose: boolean to control output of derived parameters 
            (Fsky, Fls, Fwca, Flo1) in addition to the TuningParameters;
            default=True 
        """        
        self.printTunings(self.getTuningSolution(solutionNumber=solutionNumber),
                header=header, verbose=verbose)
        
    def printCurrentTuningSolution(self, header=True, verbose=True):
        """Print the current tuning solution.
        Parameters:
         header: boolean indicating that a header be added before the table,
           default = True  
         verbose: boolean to control output of derived parameters 
            (Fsky, Fwca, coldX, Flo1) in addition to the TuningParameters;
            default=True 
        """        
        self.printTuningSolution(self.currentTuningSolution(),
                header=header, verbose=verbose)
        
    def printAllTuningSolutions(self, sort="SCORE", header=True, verbose=True):
        """Retrieve the parameters of all the tuning solutions. The
        returned sequence will be zero length if there are no tuning
        solutions.
         Parameter:
          sort:    string indicating which item to sort on. Choices:
            SCORE: score, high to low (default)
            SOL:   solution index
            LO1:   first lo frequency
            ERR:   frequency error
          header: boolean indicating that a header be added before the table
           default = True  
          verbose: boolean to control out of derived parameters 
            (Fsky, Fwca, coldX, Flo1) in addition to the TuningParameters;
            default=True""" 
        self.printTunings(self.getAllTuningSolutions(sort=sort), header=header,
                verbose=verbose)

    def getReceiverTable(self):
        """ Returns a table containing the current HW values of the
        receivers of all the antennas contained in the array.
        The returned values are entirely obtained by queries
        to the involved devices. The aim of the method is to
        provide the receiver table structure that is required
        by the DataCapturer."""
        return self._offshoot.getReceiverTable()

    def getStateTable(self):
        '''
        This method returns a table containing the current State value
        for each antenna in the array.  The return value of this method
        is suitable for passing to the data capture sendSubScanStateData 
        method. 

        This method works on a "best effort" basis.  Populating the 
        table with results from all antennas which responeded correctly
        and producing an error log for any antennas which throw an exception.
        '''
        return self._offshoot.getStateTable();
   
    def getFrequencyError(self):
        """Get the difference between the actual observing frequency
        and the requested. Constraints in the 2nd LO may make the 
        actual and requested baseband frequencies differ.
        @return Actual minus requested observing frequency for
        all four basebands"""
        return self._offshoot.getFrequencyError()
    
    def getLO1Frequency(self):
        """Returns the 1st LO frequency.
        The 1st LO frequency (f_LO1) is determined by the equation:
        f_LO1 = coldX * (f_LS +/- f_FLOOG)
        f_LS is the laser synthesizer frequency
        f_FLOOG is the FLOOG frequency
        coldX is the cold multiplication factor in the front-end"""
        return self._offshoot.getLO1Frequency()

    def getLO2Frequencies(self):
        """Returns the second LO frequencies."""
        return self._offshoot.getLO2Frequencies()


    def getLSFrequency(self):
        """Returns the difference between the two optical frequencies
        produced by the laser synthesier (in Hz). This is a
        frequency that is produced centrally and distributed to all
        antennas in the array."""
        return self._offshoot.getLSFrequency()

    def getCVRFrequency(self):
        """Returns the frequency, in Hz, of the central variable
        reference. The CVR frequency is usually in the range
        10-20GHz. This retrieves the value from the hardware, not from a 
        computed solution."""
        return self._offshoot.getCVRFrequency()

    def getFLOOGFrequency(self):
        """Returns the frequency, in Hz, of the 1st LO offset generator
        (FLOOG). The FLOOG frequency is usually in the range 20-45MHz."""
        return self._offshoot.getFLOOGFrequency()

    def getLSMultiplier(self):
        """Returns the frequency multiplication factor inside the laser
        synthesizer. Its a small integer (usually odd) that depends on
        the observing band."""
        return self._offshoot.getLSMultiplier()
    
    def getColdMultiplier(self):
        """Returns the "cold" frequency multiplication factor inside the
        front-end. Its small integer that depends on the observing
        band."""
        return self._offshoot.getColdMultiplier()

    def getCVRAmplitude(self):
        """Returns the amplitude, in dBm, of the central variable
        reference. The CVR amplitude and is usually in the range
        0-20dBm."""
        return self._offshoot.getCVRAmplitude()

    def getTuningParameters(self):
        """Returns the TuningParameter structure used to currently control
        the hardware. If none have been successfully used then an exception
        is thrown."""
        return self._offshoot.getTuningParameters()

    def setCVRAmplitude(self, amp):
        """Sets the amplitude, in dBm, of the signal produced by the
        central variable reference. The CVR amplitude and is usually
        in the range 0-20dBm.""" 
        return self._offshoot.setCVRAmplitude(amp)

    def setCentralLOFreq(self, CVRfreq, LSfreq): 
        return self._offshoot.setCentralLOFreq(CVRfreq, LSfreq)
   
    def setAntennaFrequencyOffsetMode(self,
                              offsettingMode = Control.LOOffsettingMode_NONE):
        '''
        This methos configures the LO Offsetting method for the array.
        Either enumerated values or strings are allowed.

        Be aware that this will be overridden by the value set in the
        Spectral Spec in most cases.
        '''
        if isinstance(offsettingMode, str):
            if offsettingMode.upper() == "LOOFFSETTINGMODE_NONE" or\
               offsettingMode.upper() == "NONE":
                offsettingMode = LOOffsettingMode_NONE
            elif offsettingMode.upper() == "LOOFFSETTINGMODE_TWO_LOS" or\
               offsettingMode.upper() == "TWO_LOS":
                offsettingMode = LOOffsettingMode_TWO_LOS
            elif offsettingMode.upper() == "LOOFFSETTINGMODE_THREE_LOS" or\
               offsettingMode.upper() == "THREE_LOS":
                offsettingMode = LOOffsettingMode_THREE_LOS
            else:
                msg = "The string \'%s\' is not a valid mode" % offsettingMode
                raise ControlExceptionsImpl.IllegalArgumentExImpl(msg)
        self._offshoot.setAntennaFrequencyOffsetMode(offsettingMode)

    def setEnable180DegreeWalshFunctionSwitching(self, enable):
        '''
        This method will enable/ disable the 180 Walsh function switching
        for this array.
        '''
        self._offset.setEnable180DegreeWalshFunctionSwitching(enable)
     
#   
# ___oOo___
