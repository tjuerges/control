#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id:"
#
#


from PyDataModelEnumeration import PyCalibrationDevice
from CCL.EnumerationHelper import *
from CCL.LocalOscillatorInterface import LocalOscillatorInterface
import ACSLog
from Control import LOOffsettingMode_NONE
from Control import LOOffsettingMode_TWO_LOS
from Control import LOOffsettingMode_THREE_LOS
from log_audience import OPERATOR

class LocalOscillatorInterferometricInterface(LocalOscillatorInterface):
    '''
    A slightly exteded set of local osciallator methods which all
    observing modes using the local osciallator and a correlator
    should export
    '''
    def __init__(self, offshoot, logger):
        LocalOscillatorInterface.__init__(self,offshoot, logger)

    def enableDelayTracking(self, enable):
        '''
        Enable / Disable Delay Tracking on all Antennas in the Array
        This will turn on the Delay tracking in the DGCK and 
        in the FLOOG, but has no effect of the Correlator (delay
        tracking is always active there).
        '''
        return self._offshoot.enableDelayTracking(enable)

    def delayTrackingEnabled(self):
        '''
        Return the current state of the delay tracking on all of the
        antennas in the array.  For this to return true, all
        antennas must be delay tracking.
        '''
        return self._offshoot.delayTrackingEnabled()

    def enableFringeTracking(self, enable):
        '''
        Enable/disable fringe tracking on all antennas in the array.
        '''
        return self._offshoot.enableFringeTracking(enable)

    def fringeTrackingEnabled(self):
        '''
        Returns the current state of of fringe tracking on all antennas
        in the array. Returns true only if all antennas are fringe tracking.
        '''
        return self._offshoot.fringeTrackingEnabled()

    def enableDelayCompensation(self, enable = True):
        """
        This method turns on (off) fringe and delay tracking across the array
        it is equivelent to calling enableDelayTracking and
        enableFringeTracking.
        """
        self._offshoot.enableDelayTracking(enable);
        self._offshoot.enableFringeTracking(enable);
        if (enable):
            msg = "Delay Compensation enabled for Array."
        else:
            msg = "Delay Compensation disabled for Array."
        self.logToOperator(ACSLog.ACS_LOG_INFO, msg)

    def delayCompensationEnabled(self):
        """
        This method returns True is fringe and delay tracking are active
        on all antennas in the array.
        """
        return (self._offshoot.delayTrackingEnabled() and
                self._offshoot.fringeTrackingEnabled())
 
    def optimizeSASPolarization(self, forceCalibration = False):
        '''
        This method optimizes both polarization controllers in the SAS.
        By default the optimization is performed on all SASs in the Array
        if any of them requires optimization.  If forceCalibration is True
        then the calibration is done regardless.
        '''
        print "Not Yet Implemented"

    def optimizeSASPol2(self,  forceCalibration = False):
        '''
        This method optimizes the second polarization controller in all
        SAS modules associated with this observing mode.  By default the
        optimization is performed on all SASs in the Array if any of them
        requires optimization.  If forceCalibration is True then the
        calibration is done regardless.
        '''
        print "Not Yet Implemented"     

    def logToOperator(self, priority, logMessage):
        '''
        Method which logs a report to with the OPERATOR flag set.
        '''
        self._logger.logNotSoTypeSafe(priority, "SFI Obs Mode: %s" %
                                     logMessage,
                                     OPERATOR)


    def setOffsetToRejectImageSideband(self, rejectImage):
        '''
        Configure the LO offsetting to reject the image sideband (true)
        or the signal sideband (false)
        '''
        self._offshoot.setOffsetToRejectImageSideband(rejectImage)




#   
# ___oOo___
