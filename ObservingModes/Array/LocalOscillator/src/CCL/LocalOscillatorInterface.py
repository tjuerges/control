#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id:"
#
#

import re
from CCL.EnumerationHelper import *
import CCL.FieldSource
import CCL.SchedBlock
import CCL.SpectralSpec
import Control
import ControlExceptionsImpl
from PyDataModelEnumeration import PyCalibrationDevice

class LocalOscillatorInterface:
    """
    A restricted set of methods which all observing modes which have
    a LocalOscillator method associated with them should export.
    """
    def __init__(self, offshoot, logger):
        self._logger   = logger
        self._offshoot = offshoot

    def setFrequency(self, *input):
        """Set the observing frequencies for all four basebands. For simple use
        the parameters are just the baseband frequencies in Hz, with None used
        for an unused baseband. Examples:
          setFrequency(345e9)   # Only use Baseband0
          setFrequency(100e9, None, 101.2e9, 99.9e9) # Don't use Baseband1     
        
        The LO chain hardware constrains the sky frequencies when more than 
        one baseband is used and this function will, if necessary, tune to a 
        nearby frequency. The baseband IF frequency is an optional input 
        parameter and weights can be used to specify the tradeoff of errors 
        between basebands. There are two use cases that are supported; 
        single sideband and double sideband. In the single sidebase use case 
        a preference for first LO sideband may be given. If NoPreference is 
        chosen then both sidebands will be tried if they are supported by the 
        hardware. 
        
        This method will run an algorithm that generates all the
        possible "tuning solutions" and then uses a heuristic to choose the 
        preferred solution. This function returns the number of tuning
        solutions with zero returned when there are no solutions for the
        specified frequencies. An exception is not thrown in the case of not
        being able to tune to one or more of the requested basebands. Other
        solutions are accessible using the function getAllTuningSolutions.
        All frequencies are specified in Hz and the preferred solution will
        be used to setup the hardware for the central LO and all the antennas
        in the array.
        """
        # More help will be added to this later in the file...
        return LocalOscillatorInterface._setFrequencyHelper(input,
                                                            self._offshoot)
    
    # Static method, no self argument        
    def _setFrequencyHelper(input, offshoot) : 
        """ Static helper method used by all observing modes"""   
        isDSB, bbspec, overlap = \
               LocalOscillatorInterface.parseSetFrequencyInput(input)
        if isDSB: return offshoot.setFrequencyDSB(bbspec, overlap)
        else:     return offshoot.setFrequency(bbspec, overlap)
    _setFrequencyHelper = staticmethod(_setFrequencyHelper)

    def getFrequency(self):
        """Get the observing frequency. This returns the actual observing
        frequency, at the center of the band, for all four basebands.
        This does not work correctly for DSB basebands.
        This function will throw an:
        * AntennasDisagree exception unless these frequencies are the
        same on all antennas in the array.
        * HardwareFault exception if there is any problem reading the
        current frequencies from the relevant hardware."""
        return self._offshoot.getFrequency()

    def optimizeSignalLevels(self, ifTargetLevel= -23.0, bbTargetLevel = 1.0):
        '''
        Set the antenuators in the Antenna to optimize the output
        levels.  The input parameter is the desired output (in dBm) at
        the Sideband Detector of the IFProc and at the baseband.
        '''
        return self._offshoot.optimizeSignalLevels(ifTargetLevel,
                                                   bbTargetLevel)

    def optimizeBBSignalLevels(self, bbTargetLevel = 1.0):
        '''
        Set the antenuators in the Antenna to optimize the output
        levels.  The input parameter is the desired output (in dBm) at
        the baseband detector.
        '''
        return self._offshoot.optimizeBBSignalLevels(bbTargetLevel)


    def optimizeIFSignalLevels(self, ifTargetLevel = -23.0):
        '''
        Set the antenuators in the Antenna to optimize the output
        levels.  The input parameter is the desired output (in dBm) at
        the Sideband Detector of the IFProc.
        '''
        return self._offshoot.optimizeIFSignalLevels(ifTargetLevel)


    def optimizeSASPol1Async(self, forceCalibration, callback):
        """
        This method optimizes the first polarization controller in all
        SAS modules associated with this observing mode.  By default the
        optimization is performed on all SASs in the Array if any of them
        requires optimization.  If forceCalibration is True then the
        calibration is done regardless.
        Parameters:
         forceCalibration: force calibration
         callback:         callback method to be done on completion
        """
        return self._offshoot.optimizeSASPol1Async(ifTargetLevel, callback)

    def setSpectrum(self, spectralSpec):
        """
        Set the observing frequencies given a spectral
        specification.  Only the parts of a spectral specification
        that pertain to tuning local oscillator and selecting a
        reciver band are used (and need exist).
        Parameters:
            spectralSpec: of type CCL.SpectralSpec
        """
        if isinstance(spectralSpec, CCL.APDMSchedBlock.SpectralSpec):
            xmlstr = spectralSpec.getXMLString()
            return self._offshoot.setSpectrum(xmlstr);
        else: 
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl();
            msg = 'spectralSpec parameter must be a CCL.SpectralSpec object';
            ex.addData('Detail', msg)
            ex.log(self._logger)
            raise ex

    def setSpectrumAsync(self, spectralSpec):
        """
        Set the observing frequencies given a spectral
        specification.  Only the parts of a spectral specification
        that pertain to tuning local oscillator and selecting a
        reciver band are used (and need exist).
        Parameters:
            spectralSpec: of type CCL.SpectralSpec
        TODO: Add callback
        """
        if isinstance(spectralSpec, CCL.APDMSchedBlock.SpectralSpec):
            xmlstr = spectralSpec.getXMLString()
            return self._offshoot.setSpectrumAsync(xmlstr);
        else: 
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl();
            msg = 'spectralSpec parameter must be a CCL.SpectralSpec object';
            ex.addData('Detail', msg)
            ex.log(self._logger)
            raise ex

    def _spectralSpecStringToStruct(self, ss) :
        """Takes a SpectralSpec xml string and converts it to a 
        SpectralSpec entity (structure).
        The only way to do this is to wrap it inside a SchedBlock
        and then convert the SchedBlock xml to an entity and then
        take out the SpectalSpec Entity. Painful, but the converstion from
        XML string to entity apparently only exists for a SchedBlock."""
        s = """<?xml version="1.0" encoding="UTF-8"?>
            <sbl:SchedBlock xmlns:ent="Alma/CommonEntity"
            xmlns:sbl="Alma/ObsPrep/SchedBlock"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:type="SchedBlock">"""
        if isinstance(ss, str):                         ssxml = ss
        elif isinstance(ss, xmlentity.XmlEntityStruct): ssxml = ss.xmlString
        else :                                          ssxml = ss.toxml()
        if ssxml.startswith("<?xml"):   ssxml = ssxml[ssxml.find('>')+1:]
        ssxml = ssxml.replace("ns1:", "sbl:")
        ssxml = ssxml.replace("SpectralSpecT", "SpectralSpec")
        s += "\n" + ssxml
        s += "\n</sbl:SchedBlock>"
        sb = CCL.SchedBlock.CreateFromDocument(s)
        return sb.SpectralSpec[0]

    def dopplerShift(self, spectralSpec, dopplerSource, epoch):
        """Takes the spectralSpec and uses the dopplerSource to Doppler shift
        its frequencies at the requested epoch. Returns sky frequencies in 
        a SpectralSpec.
        Parameters:
         spectralSpec: a CCL.SpectralSpec, containing rest frequencies
         dopplerSource: a CCL.FieldSource (ra/dec, etc) with velocity 
            to use for the Doppler calculations. A value of None will
            result in no Doppler shift.
         epoch: epoch for the position of the dopplerSource as an ACStime
        return: a CCL.SpectralSpec containing the input parameters and
          the computed sky frequencies.""" 
        if dopplerSource == None: return spectralSpec
        if not isinstance(spectralSpec, CCL.APDMSchedBlock.SpectralSpec):
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl();
            msg = 'spectralSpec parameter must be a CCL.APDMSchedBlock.SpectralSpec object';
            ex.addData('Detail', msg)
            ex.log(self._logger)
            raise ex
        if not isinstance(dopplerSource, CCL.APDMSchedBlock.FieldSource):
            ex = ControlExceptionsImpl.IllegalParameterErrorExImpl();
            msg = 'dopplerSource parameter must be a CCL.APDMSchedBlock.FieldSource object';
            ex.addData('Detail', msg)
            ex.log(self._logger)
            raise ex
        # This call returns a SpectralSpec as a string
        ss = self._offshoot.dopplerShift(spectralSpec.getXMLString(),
                    dopplerSource.getXMLString(), epoch);
        return self._spectralSpecStringToStruct(ss) 

    def setCalibrationDevice(self, calibrationDevice = "NONE"):
        """
        This method attempts to place the specified calibration device
        in front of the current band, for all antennas in the array.

        Available Calibration Devices are:
        AMBIENT_LOAD
        HOT_LOAD
        NONE

        If no band is selected for the front end this method will return an
        exception.
        """
        enum = getEnumeration(calibrationDevice, PyCalibrationDevice)
        return self._offshoot.setCalibrationDevice(enum)

    def getCalibrationDevice(self):
        '''
        This method returns the calibration device which is in front of the
        current cartridge.  If any antenna does not respond, or there is not
        a single uniform answer an exception is thrown.
        '''
        return self._offshoot.getCalibrationDevice()

    def averageSkyFrequency(self):
        '''
        This method returns the currently set mean sky frequency for the
        four basebands.
        '''
        return self._offshoot.averageSkyFrequency()

    def getLOObservingMode(self):
        '''
        Get the local oscillator offshoot.  This allows much higher
        access to the methods within the observing mode.
        '''
        import CCL.LocalOscillator
        return CCL.LocalOscillator.LocalOscillator\
               (self._offshoot.getLocalOscillator(), self._logger)

    # This is a static method so it doesn't have the self argument
    def parseSetFrequencyInput(args):
        """A helper routine used to parse the inputs of a setFrequency
        command. This is a static method that can be called from other
        modules for use in their setFrequency methods. 
        The return value is a set containing three values:
         1) A boolean that is true if the input is an SSB use case and
            false if it is a DSB use case
         2) A list of either SSBbasebandSpecs or DSBbasebandSpecs, depending
            on the use case, that can be passed to the CORBA setFrequency or
            setFrequencyDSB methods.
         3) An enumeration for the Band (frontend) choice when the desired
            frequencies fall within the overlap of Bands 2 and 3 (the only
            overlapping bands in ALMA).   

        """ 
             
        # ---------------- Begin internal functions ------------------       
        def isEnumItem(a) :
            return a.__class__.__name__ == 'EnumItem'
        
        def isBand2Band3OverlapEnum(a) :
            if not isEnumItem(a): return False 
            return a._parent_id.find("Band2Band3Overlap") <> -1
        
        def isSidebandPreferenceEnum(a) :
            if not isEnumItem(a): return False 
            return a._parent_id.find("SidebandPreference") <> -1
        
        def hasaList(alist) :
            "Takes a list and returns true if any of its members are lists"
            if type(alist) <> list: 
                raise Exception, "hasaList not passed a list"
            for i in alist:
                if type(i) == list: return True
            return False  
        
        def makeBasebandSpec(ssbSpec) :
            return Control.BasebandSpec(ssbSpec.skyFreqHz, ssbSpec.weight, 
                                        ssbSpec.ifFreqHz)
                                    
        def makeDSBspec(ssbspec1, ssbspec2) :
            if (ssbspec1.sidebandPref == Control.NoPreference and
                ssbspec2.sidebandPref == Control.NoPreference):
                if ssbspec1 > ssbspec2:
                    su = ssbspec1
                    sl = ssbspec2
                else:    
                    su = ssbspec2
                    sl = ssbspec1
            elif ssbspec1.sidebandPref == Control.Upper:
                su = ssbspec1
                sl = ssbspec2
            elif ssbspec1.sidebandPref == Control.Lower:
                su = ssbspec2
                sl = ssbspec1
            elif ssbspec2.sidebandPref == Control.Upper:
                su = ssbspec2
                sl = ssbspec1
            elif ssbspec2.sidebandPref == Control.Lower:
                su = ssbspec1
                sl = ssbspec2                
            return Control.DSBbasebandSpec(makeBasebandSpec(su),
                                           makeBasebandSpec(sl))
    
        def sbpToString(sbp):
            if sbp == Control.NoPreference: return "Either"
            if sbp == Control.Lower:        return " Lower"        
            if sbp == Control.Upper:        return " Upper" 
               
        def printSSB(bb):
            print "SSB     Freq  Weight    IF     SB"
            bbi = 0
            for s in bb:
                sbp = sbpToString(s.sidebandPref)
                m = " BB%d  %7.3f  %5.1f  %5.2f  %s" \
                        %(bbi, 1e-9*s.skyFreqHz, s.weight, 1e-9*s.ifFreqHz,sbp)
                if s.skyFreqHz < 100: m = " BB%d   Unused" %bbi
                print m
                bbi += 1    
                               
        def printDSB(bb):
            print "DSB     Freq  Weight    IF     SB"
            bbi = 0
            for dsb in bb:
                bbs = "BB%d" %bbi
                if dsb.USB.skyFreqHz < 100 and dsb.LSB.skyFreqHz < 100:
                    print " %s   Unused" %bbs
                    bbi += 1
                    continue
                s = dsb.USB
                m = " %3s  %7.3f  %5.1f  %5.2f   Upper" \
                        %(bbs, 1e-9*s.skyFreqHz, s.weight, 1e-9*s.ifFreqHz)
                if s.skyFreqHz > 100:
                    print m
                    bbs = ""
                if abs(s.skyFreqHz-dsb.LSB.skyFreqHz) > 100:
                    s = dsb.LSB
                    m = "  %s    %7.3f  %5.1f  %5.2f   Lower" \
                        %(bbs, 1e-9*s.skyFreqHz, s.weight, 1e-9*s.ifFreqHz)
                    print m
                bbi += 1    
            
        def parseSSB(b) :
            s = Control.SSBbasebandSpec(0, 100, 0, Control. NoPreference)
            tempb = list()
            for i in b:
                if isSidebandPreferenceEnum(i): s.sidebandPref = i
                else:                           tempb.append(i)    
            b = tempb
            if len(b) == 0:
                raise Exception, "Baseband must contain a frequency or None"
            if b[0] == None: b[0] = 0.0    
            s.skyFreqHz = b[0]
            if len(b) > 1: s.weight   = b[1]    
            if len(b) > 2: s.ifFreqHz = b[2]
            return s    
        # -------------- Finished with internal functions ----------------       
    
        # ------------------- Begin function body ----------------------       
        overlap = Control.AutoChoice
        nArgs   = len(args)
        #print "nArgs:", nArgs, "args:", args
        inputBasebands = list()
        lastBaseband = None
        for a in args:
            if type(a) == list:
                inputBasebands.append(a)
                lastBaseband = a
            elif type(a).__name__ == 'instance': 
                typeName = a.__class__.__name__
                if not isEnumItem(a):
                    msg  = "Input must be numbers and enumerations " 
                    msg += "of SidebandPreference or Band2Band3Overlap type,"
                    msg += " not " + typeName + " type"
                    raise Exception, msg
                if isBand2Band3OverlapEnum(a):
                    overlap = a    
                if isSidebandPreferenceEnum(a):
                    if lastBaseband == None:
                        msg = "Sideband preference must follow baseband freq"
                        raise Exception, msg
                    else: lastBaseband.append(a)
            else: 
                n = [a]
                lastBaseband = n        
                inputBasebands.append(n)
        #print "Input basebands:", inputBasebands
        if False: print "overlap:", overlap
        # Check that all lists have two items and see if we are DSB
        isDSB = False
        for b in inputBasebands:
            if hasaList(b) :
                allAreLists = True
                for i in b: 
                    if type(i) <> list: allAreLists = False
                if len(b) <> 2 or not allAreLists: 
                    msg  = "A baseband containing a list must contain exactly"
                    msg += " two lists. Baseband: " + b
                    raise Exception, msg
                else :
                    isDSB = True
        bb = list() # Place for the final baseband specs 
        if isDSB: 
            for b in inputBasebands:
                if hasaList(b):
                    s = makeDSBspec(parseSSB(b[0]), parseSSB(b[1]))
                else : 
                    ssb = parseSSB(b)   
                    s = makeDSBspec(ssb, ssb)
                bb.append(s)
            if False: printDSB(bb)    
        else: 
            for b in inputBasebands:
                s = parseSSB(b)
                bb.append(s)
            if False: printSSB(bb)
        return isDSB, bb, overlap  
    
    # This part of the doc is used for two different functions
    _parseSetFrequencyDetailDoc = \
        """
        Input to this method can be fairly complex as it needs to handle
        both single and double sideband basebands, weights and specified IF
        frequencies. However if it is desired to just set the frequencies for 
        a few basebands the syntax is very simple.
        Each sideband of a baseband (an SSBbaseband) can be specified
        as a list:
         [ skyFreq, weight, ifFreq, sidebandPref ]
        where the parameters are:
          skyFreq: desired sky frequency in Hertz.
                   None or 0.0 will make this SSBbaseband unused.
          weight:  weight of this SSBbaseband relative to others in setting
                   the first LO frequency (can't make everyone happy).
                   The maximum is 100, which is the default.
          ifFreq:  the desired IF frequency (solutions closer to this
                   will get a higher score), defaulting to zero, which
                   means the IF band center.
          sbPref:  sideband preference; an enumeration with values
                     Control.NoPreference (default)
                     Control.Upper
                     Control.Lower
        For the single sideband use case the sbPref should only be used when
        there is an ambiguity in the sideband of a target frequency that must
        be resolved. Only one sbPref is allowed because if both Upper and Lower
        are specified for different basebands then a conflict may occur.
        A double sideband baseband is a list composed of exactly two
        SSBbaseband specifications. When the sideband preference is
        NoPreference (the default) the sidebands will automatically be assigned
        based on the frequency.
        The complete input contains a comma separated set of baseband
        specifications for as many basebands as desired. The baseband
        specs may be either DSB or SSB (a mix is fine) and are applied
        to the hardware basebands in order, starting with Baseband0.
        The list format for an SSBbaseband is not required if only a frequency
        or a frequency and sideband preference are specified.
        The Band2/Band3 overlap choice is an additional parameter that 
        can be inserted anywhere in the input (delimited by a comma). 
        The possible values are:
          Control.NoPreference    (the default)
          Control.Band2
          Control.Band3
        
        The LO chain hardware imposes an additional constraint in the
        requirement that the first two basebands (0 & 1) receive signals from
        the same sideband when a sideband separating receiver is used. The
        same requirement is also placed on the second two basebands (2 & 3).
        When NoPreference is given (the default) this constraint will be taken
        into account in the sidebands present in the solution. If a conflict is
        requested in the input an exception will be thrown.   
          
        Examples:
          x
            SSB use case, overlap=AutoChoice
            BB0: f=x, w=100, IF=0, NoPreference 
          x,y
            SSB use case, overlap=AutoChoice
            BB0: f=x, w=100, IF=0, NoPreference  
            BB1: f=y, w=100, IF=0, NoPreference 
          x,None,Control.Band2,[y,20,Control.Upper] 
            SSB use case, overlap=Band2
            BB0: f=x, w=100, IF=0, NoPreference  
            BB1: Unused  
            BB2: f=y, w=20,  IF=0, Upper
          [x,y],z   
            SSB use case, overlap=AutoChoice
            BB0: f=x, w=y,    IF=0, NoPreference  
            BB1: f=z, w=100, IF=0, NoPreference 
          [[x],[y]],z   
            DSB use case, overlap=AutoChoice
            BB0: f=x, w=100  IF=0, Upper  
                 f=y, w=100, IF=0, Lower  
            BB1: f=z, w=100, IF=0, NoPreference 
          x,[[y],[z,20,5.2e9]],a,Control.Upper   
            DSB use case, overlap=AutoChoice
            BB0: f=x, w=100, IF=0,    NoPreference  
            BB1: f=y, w=100, IF=0,    Upper  
                 f=z, w=20,  IF=5.2G, Lower  
            BB2: f=a, w=100, IF=0,    Upper """
                              
    # Add common parsing docs        
    parseSetFrequencyInput.__doc__ += _parseSetFrequencyDetailDoc
    
    # Make parseSetFrequencyInput a static method
    parseSetFrequencyInput = staticmethod(parseSetFrequencyInput)

    # Add parsing docs to setFrequency
    setFrequency.__doc__ += _parseSetFrequencyDetailDoc

    def __getRidOfNamespaces(self, xml):
        xml = re.sub('ns[0-9]+:', '', xml)
        xml = re.sub('xmlns:ns[0-9]+="[^"]*"', '', xml)
        return xml
     
#   
# ___oOo___
