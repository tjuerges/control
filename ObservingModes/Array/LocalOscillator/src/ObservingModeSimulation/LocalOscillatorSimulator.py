#! /usr/bin/env python
# @(#) $Id$
#
# ALMA - Atacama Large Millimeter Array
# (c) Associated Universities Inc., 2008 - 2010
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#


import Acspy.Util.XmlObjectifier;
import CCL.Source;
## import ControlExceptionsImpl;
## import CCL.SIConverter
from CCL.LocalOscillator import LocalOscillator

class LocalOscillatorSimulator(LocalOscillator):
    def __init__(self, array):
        self._array = array
        pass
    
    
    def setSpectralSpec(self, spectralSpec):
        pass

    def optimizeSignalLevels(self, ifLevel, bbLevel):
        self._array.addEvent(("Signal Levels Optimized to %3.1f dB "+
                              "in the IF and %3.1f dB in the Baseband") %
                             (ifLevel,  bbLevel))
