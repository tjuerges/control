// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2010
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;

import alma.AntennaMotionPatternMod.AntennaMotionPattern;
import alma.CalDataOriginMod.CalDataOrigin;
import alma.CalibrationFunctionMod.CalibrationFunction;
import alma.CalibrationSetMod.CalibrationSet;
import alma.Control.AbortionException;
import alma.Control.OpticalPointing;
import alma.Control.OpticalPointingObservingModeOperations;
import alma.Control.ScanIntentData;
import alma.Control.SourceOffset;
import alma.Control.SourceOffsetFrame;
import alma.Control.SubScanOpticalPointingData;
import alma.Control.SubscanStartedEvent;
import alma.Control.Common.Util;
import alma.ControlExceptions.DeviceBusyEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.ControlExceptions.wrappers.AcsJIllegalParameterErrorEx;
import alma.ControlSB.OpticalCameraConfiguration;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.ModeControllerExceptions.BadDataEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.MountFaultEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.ModeControllerExceptions.UnallocatedEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJDataCapturerErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.ScanIntentMod.ScanIntent;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.container.ContainerServices;
import alma.acs.entityutil.EntityDeserializer;
import alma.acs.entityutil.EntityException;
import alma.acs.exceptions.AcsJException;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.valuetypes.SkyCoordinatesT;
import alma.log_audience.OPERATOR;
import alma.xmlentity.XmlEntityStruct;

public class OpticalPointingObservingModeImpl extends
        ObservingModeBaseImpl<OpticalPointing> implements
        OpticalPointingObservingModeOperations {

    public OpticalPointingObservingModeImpl(ContainerServices cs, 
                                            ObservingModeArray array) 
        throws ObsModeInitErrorEx {
        
        super(cs, array);

        String msg;
        msg = "Optical pointing observing mode starting up";
	logger.logToAudience(Level.INFO, msg, OPERATOR.value);

        createAntModeControllers("alma.Control.OpticalPointingHelper",
                                 "OpticalPointing");

        // This observing mode is only valid for arrays with a single antenna
        if (antennaModeController.size() != 1) {
            msg = "Optical pointing is only valid on arrays with a"
                + " single antenna.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx());
        }
        
    }

    public String getOpticalPointingModeController() {
        Iterator<String> iter = antennaModeController.keySet().iterator();
        String an = iter.next();
        return new String(antennaModeController.get(an).name());
    }

    public void sendOpticalPointingData(SubScanOpticalPointingData data) 
        throws DataCapturerErrorEx {

        try {
            final String arrayName = array.getArrayName();
            final long[] startTimes = new long[] { data.startTime };
            final long[] stopTimes = new long[] { data.endTime };
            dataCapturer.setSubscanSequenceTiming(arrayName, startTimes,
                    stopTimes);
            dataCapturer.sendSubScanOpticalPointingData(arrayName, data);
        } catch (TableUpdateErrorEx ex) {
            String msg = "Error sending optical pointing data to OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        } catch (DataErrorEx ex) {
            String msg = "Error sending optical pointing data to OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        }
    }

    /* Note that in Optical pointing there is always exactly one subscan
       per scan!  So change the begin scan method to also begin a subscan
     */
    public void beginScan(XmlEntityStruct src) 
        throws DataCapturerErrorEx, ObservingModeErrorEx, AbortionException {

        if (getAbortScanFlag()) {
            throw new AbortionException(
                    "SB execution has been aborted by the user.");
        }

        ScanIntentData[] purpose = new ScanIntentData[1];
        purpose[0] = new ScanIntentData();
        purpose[0].scanIntent = ScanIntent.CALIBRATE_POINTING;
        purpose[0].calDataOrig = CalDataOrigin.OPTICAL_POINTING;
        purpose[0].calSet = CalibrationSet.POINTING_MODEL;
        purpose[0].calFunction = CalibrationFunction.UNSPECIFIED;
        purpose[0].antennaMotionPattern = AntennaMotionPattern.NONE;
        purpose[0].onlineCalibration = false;

        super.beginScan(purpose);

        /* Now start the subscan */
        array.incrSubScanNumber();
        long now = ObservingModeUtilities.getCurrentACSTime();
        SubscanStartedEvent subScanStartedEvent = new SubscanStartedEvent();
        subScanStartedEvent.execId = array.getASDMEntRef();
        subScanStartedEvent.arrayName = array.getArrayName();
        subScanStartedEvent.scanNumber = array.getScanNumber();
        subScanStartedEvent.subscanNumber = array.getSubScanNumber();
        subScanStartedEvent.startTime = now;
        try {
        	publisher.publishEvent(subScanStartedEvent);
		} catch (AcsJException e) {
			e.log(logger);
		}

        OpticalCameraConfiguration occ = 
            new OpticalCameraConfiguration(alma.ControlSB.OpticalCameraFilter.Night, 5.0);

        // Call DataCapure
        try {
            final String arrayName = array.getArrayName();
            SubscanIntent[] opIntent = new SubscanIntent[]{SubscanIntent.ON_SOURCE}; 
            SubscanIntent[][] subscanIntents = new SubscanIntent[][]{opIntent}; 
            dataCapturer.setScanOpticalConfig(arrayName, occ);
            dataCapturer.startSubscanSequence(arrayName, subscanIntents);
            XmlEntityStruct[] srcs = new XmlEntityStruct[]{src};
            SourceOffset zeroOffset = new SourceOffset(0.0, 0.0, 0.0, 0.0, SourceOffsetFrame.HORIZON);
            SourceOffset[] allOffsets = new SourceOffset[]{zeroOffset};
            dataCapturer.setSubscanSequenceSource(arrayName, srcs, allOffsets);
         } catch (TableUpdateErrorEx ex) {
            String msg = "Error starting optical pointing sub-scan in OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        } catch (DataErrorEx ex) {
            String msg = "Error starting optical pointing sub-scan in OFFLINE/DataCapturer.";
            logger.fine(msg);
            AcsJDataCapturerErrorEx exHlp = new AcsJDataCapturerErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toDataCapturerErrorEx();
        }
        logger.finest("Leaving beginScan.");
    }

    public void doSourceScan(double exposureTime, XmlEntityStruct src)
        throws DeviceBusyEx, UnallocatedEx, TimeoutEx, HardwareFaultEx,
               IllegalParameterErrorEx, BadDataEx, MountFaultEx,
               AbortionException, DataCapturerErrorEx, ObservingModeErrorEx 
    {
    	logger.finest("Entering doSourceScan.");

    	// Transform the XML entity structure 'src' into a Java object
    	FieldSourceT fieldSource = null;
    	EntityDeserializer deserializer = EntityDeserializer.getEntityDeserializer(logger);
    	try {
    		fieldSource = (FieldSourceT) 
    		deserializer.deserializeEntity(src, FieldSourceT.class);
    	} catch (EntityException ex) {
	    String msg = "Error when deserializing FieldSource: " +
		ex.toString();
	    logger.severe(msg);
            AcsJIllegalParameterErrorEx exHlp = 
		new AcsJIllegalParameterErrorEx(ex);
            exHlp.setProperty("Message", msg);
            throw exHlp.toIllegalParameterErrorEx();
    	}

    	OpticalPointing opMC = null;
    	Set<String> antNames = antennaModeController.keySet();
    	if (antNames.size() == 1)    
    		opMC = antennaModeController.get(antNames.iterator().next());
    	else
    		logger.severe("Observing mode should contain one and only one antenna!");

    	// TODO: should values of RA, Dec, etc. be verified here before
    	// going on?

    	beginScan(src);
    	SubScanOpticalPointingData data;
    	try {
    		logger.finest("Calling OpticalPointing.doSourceExposure.");
    		SkyCoordinatesT sourceCoordinates = fieldSource.getSourceCoordinates();
            // TODO Check units!!!
    		data = 
    			opMC.doSourceExposure(exposureTime,
    					sourceCoordinates.getLongitude().getContent(),
    					sourceCoordinates.getLatitude().getContent(),
    					fieldSource.getPMRA().getContent(),
    					fieldSource.getPMDec().getContent(),
    					fieldSource.getParallax().getContent());
    		logger.finest("Returning from OpticalPointing.doSourceExposure.");
    	} catch (DeviceBusyEx ex) {
    		endScan();
    		throw ex;
    	} catch (UnallocatedEx ex) {
    		endScan();
    		throw ex;
    	} catch (TimeoutEx ex) {
    		endScan();
    		throw ex;
    	} catch (HardwareFaultEx ex) {
    		endScan();
    		throw ex;
    	} catch (IllegalParameterErrorEx ex) {
    		endScan();
    		throw ex;
    	} catch (BadDataEx ex) {
    		endScan();
    		throw ex;
    	} catch (MountFaultEx ex) {
    		endScan();
    		throw ex;
    	}
    	sendOpticalPointingData(data);
    	endScan();

    	logger.finest("Leaving doSourceScan.");
    }

    public void endScan() throws ObservingModeErrorEx, DataCapturerErrorEx {
        logger.finest("Entering endScan.");

        // Call DataCapture
        try {
            dataCapturer.endSubscanSequence(array.getArrayName(),
                    new int[] { array.getSubScanNumber() });
        } catch (TableUpdateErrorEx ex) {
            String msg = "Problem ending the subscan.";
            this.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(ex));
        } catch (DataErrorEx ex) {
            String msg = "Problem ending the subscan.";
            this.throwObservingModeErrorEx(msg,
                    new AcsJObservingModeErrorEx(ex));
        }
        super.endScan();
    }

}
