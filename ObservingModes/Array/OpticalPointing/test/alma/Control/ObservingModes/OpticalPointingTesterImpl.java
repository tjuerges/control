/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ObservingModes;

import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.Control.AntennaCharacteristics;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.OpticalPointingObservingMode;
import alma.Control.OpticalPointingObservingModeHelper;
import alma.Control.OpticalPointingObservingModePOATie;
import alma.Control.OpticalPointingObservingModeTesterOperations;
import alma.Control.ResourceId;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ContainerServices;
import alma.acs.nc.CorbaPublisher;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.offline.DataCapturer;

public class OpticalPointingTesterImpl implements ObservingModeArray, 
    ComponentLifecycle, OpticalPointingObservingModeTesterOperations {

    private ContainerServices container;
    private Logger logger;    
    
    private int corrConfigID = 0;
    private int scan = 0;
    private int subScan = 0;
    
    private OpticalPointingObservingModeImpl opObsMode;
    private OpticalPointingObservingModePOATie opObsModeTie;
    
    // -----------------------------------------------------------
    // Implementation of ComponentLifecycle
    // -----------------------------------------------------------

    public void initialize(ContainerServices containerServices) {
        container = containerServices;
        logger = containerServices.getLogger();
        logger.info("initialize() called...");
        
        Resource<CorbaPublisher> pubres = 
            new PublisherResource(container, 
                    "CONTROL_SYSTEM", true);
        ResourceManager.getInstance(container).addResource(pubres);
        Resource<DataCapturer> dcres =
            new DynamicResource(container, "Array001/DATACAPTURER", "IDL:alma/offline/DataCapturer:1.0");        
        try {
            ResourceManager.getInstance(container).acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource: ";
            logger.severe(msg);
        }
    }

    public void execute() {
        logger.info("execute() called...");
    }

    public void cleanUp() {
        logger.info("cleanUp() called");
        if (opObsMode != null) {
            logger.fine("Deactivating SFI Offshoot.");            
            opObsMode.cleanUp();
            if (opObsModeTie != null) {
                try {
                    container.deactivateOffShoot(opObsModeTie);
                } catch (AcsJContainerServicesEx ex) {
                    String msg = "Error deactivating SFI Offshoot.";
                    logger.fine(msg);
                    ex.log(logger);
                }
                opObsModeTie = null;
            }
            opObsMode = null;
        }
        ResourceManager.getInstance(container).releaseResources();
    }

    public void aboutToAbort() {
        cleanUp();
        logger.info("managed to abort...");
        System.out.println("Foo component managed to abort...");
    }

    // -----------------------------------------------------------
    // Implementation of ACSComponent
    // -----------------------------------------------------------

    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }

    public String name() {
        return container.getName();
    }

    // -----------------------------------------------------------
    // Implementation of ObservingModeArray
    // -----------------------------------------------------------
    
    public IDLEntityRef getASDMEntRef() {
        IDLEntityRef entRef = new IDLEntityRef();
        return entRef;
    }

    public AntennaCharacteristics[] getAntennaConfigurations() {
        
        String[] antennaNames = {"ALMA01"};
        AntennaCharacteristics[] ac = 
            new AntennaCharacteristics[antennaNames.length];
        for(int i = 0; i < antennaNames.length; i++) {
            ac[i] = new AntennaCharacteristics();
            ac[i].antennaName  = antennaNames[i];
            ac[i].dishDiameter = new IDLLength(0.0);
            ac[i].xPosition = new IDLLength(0.0);
            ac[i].yPosition = new IDLLength(0.0);
            ac[i].zPosition = new IDLLength(0.0);
            ac[i].xOffset = new IDLLength(0.0);
            ac[i].yOffset = new IDLLength(0.0);
            ac[i].zOffset = new IDLLength(0.0);
            ac[i].dateOfCommission = 0;
            ac[i].padId = "PadID";
            ac[i].padxPosition = new IDLLength(0.0);
            ac[i].padyPosition = new IDLLength(0.0);
            ac[i].padzPosition = new IDLLength(0.0);
            ac[i].cableDelay = 0;
        }
        return ac;
    }

    public int getCorrelatorConfigID() {
        return corrConfigID;
    }

    public void incrCorrelatorConfigID() {
        corrConfigID++;
    }
    
    public String getDataCapturerName() {
        return "Array001/DATACAPTURER";
    }

    public int getScanNumber() {
        return scan;
    }

    public int getSubScanNumber() {
        return subScan;
    }

    public void incrScanNumber() {
        scan++;
    }

    public void incrSubScanNumber() {
        subScan++;
    }

    public void resetScanNumber() {
        scan = 0;
    }

    public void resetSubScanNumber() {
        subScan = 0;
    }
    
    public String getConfigurationName() {
        return "ALMA";
    }
    
    @Override
    public String getArrayComponentName() {
        return "CONTROL/Array001";
    }

    @Override
    public String getArrayName() {
        return "Array001";
    }

	@Override
	public ResourceId[] getPhotonicReferences() {
		ResourceId[] photRefs = new ResourceId[1];
		photRefs[0] = new ResourceId();
		photRefs[0].ResourceName  = "PhotonicReference2";
		photRefs[0].ComponentName = "CONTROL/CentralLO/PhotonicReference2";
		return photRefs;
	}        

	@Override
	public boolean sbHasBeenAborted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sbHasBeenStopped() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sbHasToStop() {
		// TODO Auto-generated method stub
		return false;
	}    
	
	// -----------------------------------------------------------
    // Implementation of SingleFieldInterferometryTester
    // -----------------------------------------------------------

    public OpticalPointingObservingMode getOPOffshoot() throws ObsModeInitErrorEx {
        
        try {
            opObsMode = new OpticalPointingObservingModeImpl(container, this);
            opObsModeTie = new OpticalPointingObservingModePOATie(opObsMode);
            OpticalPointingObservingMode sfiOffshoot = 
                OpticalPointingObservingModeHelper.narrow(container.activateOffShoot(opObsModeTie));
            return sfiOffshoot;
        } catch (ObsModeInitErrorEx ex) {
            logger.fine("Error creating Optical Pointing Offshot: " + ex.toString());
            AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
            throw exhlp.toObsModeInitErrorEx();
        } catch (AcsJContainerServicesEx ex) {
            logger.fine("Error creating Optical Pointing Offshot: " + ex.toString());
            AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
            throw exhlp.toObsModeInitErrorEx();
        }        
    }
}
