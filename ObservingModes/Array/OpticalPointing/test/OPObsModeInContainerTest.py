#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Util.XmlObjectifier import XmlObject
from CCL.OpticalPointingObservingMode import OpticalPointingObservingMode
from CCL.logging import getLogger
import Control
import unittest

class OPObsModeInContainerTest(unittest.TestCase):
    
    def setUp(self):
        self.logger = getLogger()
        self.client = PySimpleClient()
        self.simulator = self.client.getDefaultComponent("IDL:alma/ACSSim/Simulator:1.0")
        self.tester = self.client.getComponent('OP_TESTER')
        
    def tearDown(self):
        self.client.releaseComponent(self.simulator._get_name())
        self.client.releaseComponent('OP_TESTER')

    def testInitializeShutdownHardware(self):
        op = OpticalPointingObservingMode(self.tester.getOPOffshoot(), self.logger)
        op.initializeHardware()
        op.shutdownHardware()

    def testBeginEndScan(self):
        srcxml = """
        <FieldSource xmlns:avt='Alma/ValueTypes'
            solarSystemObject='Pluto'>
            <sourceCoordinates system='J2000'>
                <avt:longitude unit='rad'>0.0</avt:longitude>
                <avt:latitude unit='rad'>0.0</avt:latitude>
            </sourceCoordinates>
            <sourceName>Unknown</sourceName>
            <pMRA unit='rad/s'>0.0</pMRA>
            <pMDec unit='rad/s'>0.0</pMDec>
            <nonSiderealMotion>false</nonSiderealMotion>
            <parallax unit='rad'>0.0</parallax>
            <name/>
        </FieldSource>"""
        srcobj = XmlObject(srcxml)
        opoff = self.tester.getOPOffshoot()                
        op = OpticalPointingObservingMode(opoff, self.logger)
        op.beginScan(srcobj)
        op.endScan()
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(OPObsModeInContainerTest("testInitializeShutdownHardware"))
    suite.addTest(OPObsModeInContainerTest("testBeginEndScan"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')
