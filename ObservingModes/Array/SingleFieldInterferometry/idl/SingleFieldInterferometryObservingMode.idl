// @(#) $Id$
//
// ALMA - Atacama Large Millimiter Array
// (c) Associated Universities Inc., 2008, 2009
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
//

#ifndef SFIOBSERVINGMODE_IDL
#define SFIOBSERVINGMODE_IDL

#include <PointingSubarrayController.idl>
#include <ControlCommonInterfaces.idl>
#include <ControlDataInterfaces.idl> // For enums for LO chain calc
#include <ControlExceptions.idl>
#include <LocalOscillator.idl>
#include <ModeControllerExceptions.idl>
#include <ObservingModeBase.idl>
#include <ObservingModeExceptions.idl>
#include <asdmIDL.idl>
#include <ObservingModeInterface.idl>
#include <InterferometryController.idl>
#include <JavaContainerError.idl>

#pragma prefix "alma"

module Control {

  //typedef sequence<long> corrConfigIDSeq;

  interface SingleFieldInterferometryObservingMode :
    ObservingModeBase,
    ObservingModeInterface,
    InterferometricLocalOscillatorInterface,
    PointingSubarrayControllerInterface,
    InterferometryControllerInterface
  {      

      /// Set the Pointing Direction for the Array.  All antenna's will be set to
      /// point in this direction. 
      void setPointingDirection(in FieldSource source)
          raises(ControlExceptions::IllegalParameterErrorEx,
                 ObservingModeExceptions::ObservingModeErrorEx,
                 ModeControllerExceptions::TimeoutEx);
      
      
      /// Specify the target that is to be used for all subsequent
      /// subscans. This also moves the antennas to point at the specified
      /// source, setsup the correlator and tunes the LOs. Eventually this may
      /// be a APDM Target object. The spectralspec is always loaded into teh
      /// correlartor and the subsequant configuration ID is returned. The
      /// caller is responsible for ensuring the configuration is unloaded,
      /// using the clearConfigurations function, when thsi configuration is no
      /// longer needed.
      long setTarget(in FieldSource source, in SpectralSpec spec)
          raises (ControlExceptions::IllegalParameterErrorEx,
                  ObservingModeExceptions::ObservingModeErrorEx);
      
      long setSpectralSpec(in SpectralSpec spec)
          raises (ControlExceptions::IllegalParameterErrorEx,
                  ObservingModeExceptions::ObservingModeErrorEx);
      
      /* Deprecated Method: use set source instead */
      void setFieldSource(in FieldSource fs)
          raises (ControlExceptions::IllegalParameterErrorEx,
                  ObservingModeExceptions::ObservingModeErrorEx,
                  ModeControllerExceptions::TimeoutEx);
    
  };
};

#endif // SFIOBSERVINGMODE_IDL
