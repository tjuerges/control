// "$Id$"
// ALMA - Atacama Large Millimeter Array
// (c) Associated Universities Inc., 2008 - 2011
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU LesSkyser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

package alma.Control.ObservingModes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import alma.CalibrationDeviceMod.CalibrationDevice;
import alma.Control.AbortionException;
import alma.Control.ArrayMountController;
import alma.Control.ArrayOffset;
import alma.Control.Band2Band3Overlap;
import alma.Control.Completion;
import alma.Control.CorrelatorCalibrationSequenceSpec;
import alma.Control.DSBbasebandSpec;
import alma.Control.FocusCalResult;
import alma.Control.InterferometryController;
import alma.Control.LocalOscillator;
import alma.Control.PointingCalResult;
import alma.Control.SSBbasebandSpec;
import alma.Control.ScanIntentData;
import alma.Control.SimpleCallback;
import alma.Control.SingleFieldInterferometryObservingModeOperations;
import alma.Control.SourceOffset;
import alma.Control.SourceOffsetFrame;
import alma.Control.SubscanBoundries;
import alma.Control.SubscanSequenceSpec;
import alma.Control.SubscanSpec;
import alma.Control.TotalPowerConfiguration;
import alma.Control.TowerHolography;
import alma.Control.Common.Util;
import alma.Control.NewTPPPackage.SubscanInfo;
import alma.ControlCommonExceptions.AsynchronousFailureEx;
import alma.ControlExceptions.DeviceBusyEx;
import alma.ControlExceptions.IllegalParameterErrorEx;
import alma.Correlator.SubscanInformation;
import alma.Correlator.SubscanTiming;
import alma.DataCaptureExceptions.DataErrorEx;
import alma.DataCaptureExceptions.TableUpdateErrorEx;
import alma.JavaContainerError.ContainerServicesEx;
import alma.ModeControllerExceptions.HardwareFaultEx;
import alma.ModeControllerExceptions.TimeoutEx;
import alma.NewTotPwrProcErr.AlreadyShutDownEx;
import alma.NewTotPwrProcErr.NoAntennasEx;
import alma.NewTotPwrProcErr.NoBasebandEx;
import alma.ObservingModeExceptions.AntennasDisagreeEx;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObservingModeErrorEx;
import alma.PolarizationTypeMod.PolarizationType;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.container.ContainerServices;
import alma.asdmIDL.ASDMDataSetIDL;
import alma.entity.xmlbinding.schedblock.FieldSourceT;
import alma.entity.xmlbinding.schedblock.SpectralSpecT;
import alma.xmlentity.XmlEntityStruct;

public class SingleFieldInterferometryObservingModeImpl extends
        ObservingModeBaseImpl<TowerHolography> implements
        SingleFieldInterferometryObservingModeOperations {

    // Controllers we need to use in this mode
    private InterferometryControllerImpl intController = null;
    private SquareLawControllerImpl slc = null;
    private LocalOscillatorImpl localOscillator = null;
    private PointingSubarrayControllerImpl psc = null;

    // The current source we should use. Its set using setTarget.
    private FieldSourceT curSource = null;

    // The current spectral specification we should use. Its set using
    // setTarget.
    private SpectralSpecT curSpectrum = null;

    // The offsets of the last subscan in a sequence.
    private Map<String, SourceOffset> curOffset = new HashMap<String, SourceOffset>();

    public SingleFieldInterferometryObservingModeImpl(ContainerServices cs,
            ObservingModeArray array) throws ObsModeInitErrorEx {
        super(cs, array);

        String msg;
        opLog.info("Single-field interferometery observing mode starting up");

        try {
            // Create the necessary parts of this observing mode
            psc             = new PointingSubarrayControllerImpl(cs, array);
            localOscillator = new LocalOscillatorImpl(cs, array);
            intController   = new InterferometryControllerImpl(cs, array);
            slc = new SquareLawControllerImpl(cs, array, "TOTALPOWER_BULKDATA_DISTRIBUTOR_2");
            //slc = new SquareLawControllerImpl(cs, array, array.getCorrelatorBDDStreamInfo().distributor);

            // Now enable fringe and delay tracking
            opLog.fine("Enabling delay compensation and fringe tracking.");
            localOscillator.enableDelayTracking(true);
            localOscillator.enableFringeTracking(true);
        } catch (HardwareFaultEx ex) {
            msg = "Error enabling delay compensation and fringe tracking.";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        } catch (ObsModeInitErrorEx ex) {
            cleanUp();
            msg = "Failed to cleanly activate the Observing Mode";
            throwObsModeInitErrorEx(msg, new AcsJObsModeInitErrorEx(ex));
        }
        devLog.fine("Completed starting the single-field interferometery observing mode.");
    }

    @Override
    public void cleanUp() {
        String msg = "Single-field interferometry observing mode shutting down.";
        opLog.info(msg);

        // Disable fringe and delay tracking and destroy the delay server
        if (intController != null) {
            intController.cleanUp();
            intController = null;
            devLog.debug("Completed deactivating the interferometery controller.");
        }

        if (slc != null) {
            slc.cleanUp();
            slc = null;
            devLog.debug("Completed deactivating squarelaw controller.");
        }
        
        if (localOscillator != null) {
            localOscillator.cleanUp();
            localOscillator = null;
            devLog.debug("Completed deactivating the local oscillator.");
        }

        if (psc != null) {
            psc.cleanUp();
            psc = null;
            devLog.debug("Completed deactivating array mount controller offshoot.");
        }

        super.cleanUp();
        devLog.fine("Completed shutting down the single-field interferometry observing mode.");
    }

    @Override
    public String[] getAntennas() {
        // Because this class does not have any mode controllers the antennas
        // come from the array.
        return array.getAntennas();
    }

    public int setTarget(String fieldSource, String spectralSpec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        FieldSourceT src = ObservingModeUtilities.deserializeFieldSource(
                fieldSource, logger);
        SpectralSpecT spec = ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger);
        return setTarget(src, spec, new SourceOffset(0,0,0,0,SourceOffsetFrame.HORIZON));
    }
    
    private int setTarget(FieldSourceT src, SpectralSpecT spec, SourceOffset delayOffset)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        setSourceAsync(src, delayOffset);
        int corrId = setSpectralSpec(spec);
        try {
            waitForTarget();
        } catch (TimeoutEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return corrId;
    }

    public void doSubscan(long subscanDuration, SubscanIntent[] intents) throws ObservingModeErrorEx {
        // TODO. Move this into a base class
        try {
            if (curSource == null || curSpectrum == null) {
                String msg = "Cannot start a subscan as no target has been specified. Please ensure setTarget has been called.";
                throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx());
            }
            SubscanSequenceSpec subscanSeqSpec = new SubscanSequenceSpec();
            subscanSeqSpec.sources = new String[0]; // The current source is used
            subscanSeqSpec.spectra = new String[0]; // The current spectra is used
            SubscanSpec subscanSpec = new SubscanSpec();
            subscanSpec.intent = intents;
            subscanSpec.sourceId = -1;
            subscanSpec.spectralId = -1;
            // TODO. FIX thsi up
            subscanSpec.calibrationId = 0;// curSpectrum.getCorrelatorConfigurationID();
            subscanSpec.duration = Util.roundTE(subscanDuration);
            Set<String> subarrays = psc.getCurrentSubarrays();
            int numSubarrays = subarrays.size();
            SourceOffset zeroOffset = new SourceOffset(0,0,0,0,SourceOffsetFrame.HORIZON);
            subscanSpec.pointingOffset = new ArrayOffset[numSubarrays];
            int i = 0;
            for (String sa: subarrays) {
                subscanSpec.pointingOffset[i] = new ArrayOffset(zeroOffset, sa);
                i++;
            }            
            subscanSpec.delayCenterOffset = zeroOffset;
            subscanSpec.subreflectorOffset = new double[0];
            subscanSpec.calLoad = CalibrationDevice.NONE;
            subscanSeqSpec.subscans = new SubscanSpec[] { subscanSpec };
            doSubscanSequence(subscanSeqSpec);
        } catch (IllegalParameterErrorEx ex) {
            String msg = "Cannot complete the subscan.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
    }

    public void doSubscanSequence(SubscanSequenceSpec spec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {

        String msg;
        // Setup as much of data capture as possible before we work out when
        // the sub-scans actually start
        try {
            final int numSubscans = spec.subscans.length;
            int[] subscanNumbers = new int[numSubscans];
            SubscanIntent[][] intents = new SubscanIntent[numSubscans][];
            CalibrationDevice[] acdPositions = new CalibrationDevice[numSubscans];
            FieldSourceT[] sources = new FieldSourceT[numSubscans];
            SpectralSpecT[] spectra = new SpectralSpecT[numSubscans];
            boolean[] sourceChanged = new boolean[numSubscans];
            boolean[] spectrumChanged = new boolean[numSubscans];
            
            ObservingModeUtilities.extractTargetSequence(spec, curSource, curSpectrum,
                    psc.getCurrentSubarrays(), logger, sources, sourceChanged,
                    spectra, spectrumChanged);
            int[] calIDs = ObservingModeUtilities.extractCalIds(spec.subscans, logger);
            
            if (curSource == null || curSpectrum == null) {
                // This is the first sub-scan in this SB and the
                // caller has not run setTarget (or setSource & setSpectrum).
                // So setup the hardware before doing any sub-scans, or even
                // estimating the time between sub-scans, to:
                // 1. Check that we have control of the hardware.
                // 2. Allow us to get better estimate of the
                // time between subsequent sub-scans.
                setTarget(sources[0], spectra[0], spec.subscans[0].delayCenterOffset);
            }
            
            final int firstSubscanNumber = array.getSubScanNumber() + 1;
            long subscanDurations[] = new long[numSubscans];
            SourceOffset[] delayOffsets = new SourceOffset[numSubscans];
            ArrayList<Map<String, SourceOffset>> offsets = new ArrayList<Map<String, SourceOffset>>();
            double[] antSlewTimes = new double[numSubscans];
            TotalPowerConfiguration[] allTPConfig = null;
            int[] numberOfIntegrations = null;
            String[] uids = null;
            SubscanInfo[] ssitp = null;
            final String asdmId = array.getASDMEntRef().entityId;
            final int scanNumber = array.getScanNumber();
            if (spectra[0].getSquareLawSetup() != null) {
                allTPConfig = new TotalPowerConfiguration[numSubscans];
                numberOfIntegrations = new int[numSubscans];
                uids = getUIDs((short) numSubscans);
                ssitp = new SubscanInfo[numSubscans];
           }
            for (int s = 0; s < numSubscans; s++) {
                final SubscanSpec subscan = spec.subscans[s];
                subscanNumbers[s] = firstSubscanNumber + s;
                subscanDurations[s] = Util.ceilTE(spec.subscans[s].duration);
                if (allTPConfig != null) {
                    allTPConfig[s] = TotalPowerObservingModeImpl.extractTotalPowerConfiguration(
                            spectra[s], subscanDurations[s], logger);
                    numberOfIntegrations[s] = (int) Math.round(subscanDurations[s]/allTPConfig[s].integrationDuration);
                }
                intents[s] = subscan.intent;
                acdPositions[s] = subscan.calLoad;
                delayOffsets[s] = subscan.delayCenterOffset;
                Map<String, SourceOffset> newOffset = curOffset;
                if (subscan.pointingOffset.length > 0) {
                    newOffset = PointingSubarrayControllerImpl.mergeOffsets(
                            curOffset, subscan.pointingOffset);
                    sourceChanged[s] = true;
                }
                offsets.add(newOffset);
                if (sourceChanged[s]) {
                    antSlewTimes[s] = psc.timeToSlew(curSource, curOffset,
                            sources[s], offsets.get(s));
                } else {
                    antSlewTimes[s] = 0.0;
                }
                msg = "Estimated antenna slew time before we can start subscan "
                        + subscanNumbers[s] + " is "
                        + String.format("%.3f", antSlewTimes[s]) + " seconds.";
                devLog.fine(msg);
                curSource = sources[s];
                curOffset = offsets.get(s);
                // TODO. Find out if Doppler correction should be done *after*
                // we have computed the subscan boundries so that a better time
                // can be used for this correction.                
                long now = ObservingModeUtilities.getCurrentACSTime();
                spectra[s] = localOscillator.dopplerShift(spectra[s], sources[s], now);
                spectra[s] = localOscillator.alignSpectralSpec(spectra[s]);
                curSpectrum = spectra[s];
                if (ssitp != null) {
                    SubscanInfo thisInfo = new SubscanInfo();
                    thisInfo.dataOID = uids[s];
                    thisInfo.baseband = ObservingModeUtilities.baseBandIDs(spectra[s]);
                    thisInfo.execID = asdmId;
                    thisInfo.polarization = new int[] { 0, 1 };
                    thisInfo.scanNumber = scanNumber;
                    thisInfo.subscanNumber = subscanNumbers[s];
                    ssitp[s] = thisInfo;
                }
           }

            // Do this *after* we have done the Doppler correction.
            long[] tuningTimes = localOscillator.timeToTune(spectra, acdPositions);
            for (int s = 0; s < numSubscans; s++) {
                msg = "Estimated tuning time before we can start subscan "
                        + subscanNumbers[s] + " is "
                        + String.format("%.3f", tuningTimes[s] * 100E-9)
                        + " seconds.";
                devLog.debug(msg);
            }

            final String arrayName = array.getArrayName();
            dataCapturer.startSubscanSequence(arrayName, intents);
            { // TODO. Replace XmlEntityStruct with a string
                XmlEntityStruct[] asXML = new XmlEntityStruct[numSubscans];
                for (int s = 0; s < numSubscans; s++) {
                    psc.getArrayMountControllerImpl().fillRADec(sources[s]);
                    asXML[s] = ObservingModeUtilities.serializeFieldSource(
                            sources[s], logger);
                }
                dataCapturer.setSubscanSequenceSource(arrayName, asXML,
                        delayOffsets);
            }
            {
                XmlEntityStruct[] asXML = new XmlEntityStruct[numSubscans];
                for (int s = 0; s < numSubscans; s++) {
                    asXML[s] = ObservingModeUtilities.serializeSpectralSpec(
                            spectra[s], logger);
                }
                dataCapturer.setSubscanSequenceSpectrum(arrayName, asXML);
            }
            if (allTPConfig != null) {
                dataCapturer.setSubscanSequenceTotalPowerConfig(arrayName,
                        allTPConfig);
                dataCapturer.setSubscanSequenceDataRangeUID(arrayName, uids);
            }
            // Compute the sub-scan start and stop times relative to an, as yet
            // unknown, start time for the sequence.
            SubscanTiming[] sst = intController.getReconfigurationTimes(calIDs,
                    subscanDurations);
            SubscanInformation[] ssi = new SubscanInformation[numSubscans];
            long endOfLastSubscan = 0;
            SubscanBoundries[] subscanBoundries = new SubscanBoundries[numSubscans];
            for (int s = 0; s < numSubscans; s++) {
                msg = "For subscan " + subscanNumbers[s]
                        + " the correlator setup time is "
                        + String.format("%.3f", sst[s].setupTime * 100e-9)
                        + " seconds & processing time time is "
                        + String.format("%.3f", sst[s].processingTime * 100e-9)
                        + " seconds.";
                devLog.fine(msg);

                long leadTime = Math.max(sst[s].setupTime,
                        Util.toTimeInterval(antSlewTimes[s]));
                leadTime = Math.max(leadTime, tuningTimes[s]);
                leadTime = Util.ceilTE(leadTime);
                msg = "Setup time for subscan " + subscanNumbers[s] + " is "
                        + String.format("%.3f", leadTime * 100E-9)
                        + " seconds.";
                devLog.debug(msg);
                long thisStartTime = endOfLastSubscan + leadTime;
                long subscanDuration = subscanDurations[s];
                long thisStopTime = thisStartTime + subscanDuration;
                subscanBoundries[s] = new SubscanBoundries(thisStartTime, thisStopTime);
                sst[s].setupTime = leadTime;
                sst[s].duration = subscanDuration;
                ssi[s] = new SubscanInformation(scanNumber, subscanNumbers[s], spec.subscans[s].calibrationId, sst[s]);
                endOfLastSubscan = thisStopTime;
            }

            // TODO. Reduce this number as much as possible. The requires
            // testing with real hardware.
            final long commsOverhead = Util.roundTE(Util.toTimeInterval(0.196 * 3));

            // From here until the startSubscanSequence function call is
            // received by the correlator we only have less than the
            // commsOverhead-96ms i.e, a fraction of a second
            // So keep the code in this time critical section lightweight
            {
                long startOfTimeCriticalSection = ObservingModeUtilities.getCurrentACSTime();
                final long corrStartTime = Util.roundTE(startOfTimeCriticalSection)
                        + commsOverhead;
                long[] subscanStartTime = new long[numSubscans];
                long[] subscanEndTime = new long[numSubscans];
                for (int s = 0; s < numSubscans; s++) {
                    subscanBoundries[s].startTime += corrStartTime;
                    subscanBoundries[s].endTime += corrStartTime;
                    subscanStartTime[s] = subscanBoundries[s].startTime;
                    subscanEndTime[s] = subscanBoundries[s].endTime;
                    if (ssitp != null) {
                        SubscanInfo thisInfo = ssitp[s];
                        thisInfo.startTime = subscanBoundries[s].startTime;
                        thisInfo.endTime = subscanBoundries[s].endTime;
                    }
                }
                // Tell DC when the sub-scan starts and stops
                dataCapturer.setSubscanSequenceTiming(arrayName,
                        subscanStartTime, subscanEndTime);
                // Kick off the correlator
                // TODO. This function needs to be updated when we support
                // source changes within a subscan sequence
                intController.startSubscanSequence(corrStartTime, ssi,
                        curSource, delayOffsets);

                //Configure all total-power sequences
                if (allTPConfig != null) {
                    slc.configureSubscanSequence(allTPConfig, subscanBoundries, ssitp);
                }

                // Queue the offsets/strokes and start the collection of
                // pointing data.
                psc.beginSubscanSequence(subscanBoundries, offsets);
                // Queue all the frequency changes
                localOscillator.startSubscanSequence(subscanBoundries, spectra, acdPositions);                
                double timeCriticalDuration = (ObservingModeUtilities.getCurrentACSTime() - startOfTimeCriticalSection) * 100E-9;
                msg = "Time critical section took "
                        + String.format("%.3f", timeCriticalDuration)
                        + " seconds.";
                if (timeCriticalDuration > commsOverhead
                        - Util.toTimeInterval(0.096)) {
                    devLog.severe(msg + " This is too long.");
                } else {
                    devLog.fine(msg + " This is OK.");
                }
            }

            // Now its all setup and we just have to wait for each subscan to
            // execute
            for (int s = 0; s < numSubscans; s++) {
                // Kick off the total-power processor
                if (allTPConfig != null) {
                    slc.waitForSubscanStart();
                }   
                
                // Wait for the correlator to start the next subscan
                intController.waitForSubscanStart();
                // Send the weather data to DC and publish the subscan started
                // event.
                final long thisSubscanStartTime = subscanBoundries[s].startTime;
                super.beginSubscan(intents[s], thisSubscanStartTime,
                        spec.subscans[s].duration);
                // For each sub-scan send the sub-reflector position to data
                // capture
                psc.waitForSubscanStart();
                // I expect the LO to be locked within 5.0 seconds of
                // the sub-scan start and if not we bail. Flagging should be used to
                // mark any data collected when the LO's are not locked.
                long timeout = Util.toTimeInterval(5.0) + 
                    (subscanBoundries[s].startTime - ObservingModeUtilities.getCurrentACSTime()); 
                localOscillator.waitForSubscanStart(timeout);

                // TODO. These frequency changes need to be queued in the mount
                // controller.
                // TODO If there is a band change this will also change the
                // pointing and focus model. Sometimes we do not want to do this
                // so we need a way to prevent this.
                final double freq = localOscillator.averageSkyFrequency();
                psc.setObservingFrequency(freq);

                // Send the state table to data capture
                localOscillator.beginSubscan();

                // Now wait for the sub-scan to end and send some correlator
                // data to DC.
                intController.waitForSubscanEnd();

                if (allTPConfig != null) {
                    // wait for the square law data
                    int actualIntegrations = slc.waitForSubscanEnd();
                    // TODO. Check if the TPP correctly returns the actual number of
                    //  integrations. If so enable the following line of code. In a
                    // simulated environment it returns zero!
                    actualIntegrations = numberOfIntegrations[s];
                    dataCapturer.setSubscanSequenceIntegrations(arrayName,
                            subscanNumbers[s], actualIntegrations);
                }
                
                // send the pointing data to data capture.
                psc.waitForSubscanEnd();
                
                // Publish the sub-scan ended event and send weather data to
                // data capture
                super.endSubscan(subscanBoundries[s].endTime,
                        Completion.SUCCESS);
            }
            // Tell Data capture the sequence of sub-scans has ended
            // TODO handle cases where the sequence fails or is aborted
            dataCapturer.endSubscanSequence(arrayName, subscanNumbers);
        } catch (TableUpdateErrorEx ex) {
            intController.stopSubscanSequence();
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataErrorEx ex) {
            intController.stopSubscanSequence();
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (DataCapturerErrorEx ex) {
            intController.stopSubscanSequence();
            msg = "Problem sending information to the ASDM (Data capture)";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        } catch (AbortionException ex) {
            intController.stopSubscanSequence();
            msg = "Got an abort expection";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
       } catch (AlreadyShutDownEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoBasebandEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DeviceBusyEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoAntennasEx e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void beginScan(ScanIntentData[] scanIntentData)
            throws ObservingModeErrorEx, DataCapturerErrorEx {
        super.beginScan(intController.addWVRIntent(scanIntentData));
    }

    /// ---------- Interferometry Controller Methods -------------
    /// See the InterferometryControllerInterface.py for a description of this
    /// function
    public int[] loadConfigurations(String[] spectralSpecs)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        return intController.loadConfigurations(spectralSpecs);
    }

    public int[] loadCalibrations(CorrelatorCalibrationSequenceSpec calSpecs)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        // This only works because we are, for now, requiring all sub-scans in a
        // sequence to use the same source.
        // TODO. Remove this restriction.
        try {
            if (calSpecs.FieldSourceList.length > 0) {
                setSource(calSpecs.FieldSourceList[0]);
            } else {
                setSource(curSource);
            }
        } catch (TimeoutEx ex) {
            String msg = "Cannot move the antennas to the specified calibration source.";
            throwObservingModeErrorEx(msg, new AcsJObservingModeErrorEx(ex));
        }
        return intController.loadCalibrations(calSpecs);
    }

    // / See the InterferometryControllerInterface.py for a description of this
    // / function
    public void clearConfigurations(int[] configIDs) {
        intController.clearConfigurations(configIDs);
    }

    // / See the InterferometryControllerInterface.py for a description of this
    // / function
    public void clearCalibrations(int[] calIDs) {
        intController.clearCalibrations(calIDs);
    }

    // / See the InterferometryController.py for a description of this
    // / function
    public void setPhaseDirection(String fieldSource)
            throws IllegalParameterErrorEx {
        intController.setPhaseDirection(fieldSource);
    }

    // / See the InterferometryController.py for a description of this
    // / function
    public String getDelayServerName() {
        return intController.getDelayServerName();
    }

    // / See the InterferometryController.py for a description of this
    // / function
    public void disableWVRCalibration(boolean disable) {
        intController.disableWVRCalibration(disable);
    }

    // / See the InterferometryController.py for a description of this
    // / function
    public void applyWVRCalibration(ASDMDataSetIDL wvrCalib)
            throws ObservingModeErrorEx {
        intController.applyWVRCalibration(wvrCalib);
    }

    // / See the InterferometryController.py for a description of this
    // / function
    public void clearConfigurationIDs(int[] corrConfIDs)
            throws ObservingModeErrorEx {
        intController.clearConfigurationIDs(corrConfIDs);
    }

    // / See the InterferometryController.py for a description of this
    // / function
    public boolean isWVRSendingData() {
        return intController.isWVRSendingData();
    }

    // / See the InterferometryController.py for a description of this
    // / function
    public long getWVRintegrationTime() throws ObservingModeErrorEx {
        return intController.getWVRintegrationTime();
    }

    public InterferometryController getInterferometryController()
            throws ContainerServicesEx {
        return intController.getInterferometryController();
    }

    /*
     * See InterferometryControllerInterface.py for a description of this method
     */
    public void enableLLCCorrection(boolean enable)
            throws AsynchronousFailureEx {
        intController.enableLLCCorrection(enable);
    }

    /*
     * See InterferometryControllerInterface.py for a description of this method
     */
    public boolean isLLCCorrectionEnabled() throws AsynchronousFailureEx {
        return intController.isLLCCorrectionEnabled();
    }

    /*
     * See InterferometryControllerInterface.py for a description of this method
     */
    public long getTimeToLLCReset() throws AsynchronousFailureEx {
        return intController.getTimeToLLCReset();
    }

    /*
     * See InterferometryControllerInterface.py for a description of this method
     */
    public void resetLLCStretchers() throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        intController.resetLLCStretchers();
    }

    /*
     * See InterferometryControllerInterface.py for a description of this method
     */
    public void setLLCStretcherPosition(float position)
            throws AsynchronousFailureEx,
            alma.ControlCommonExceptions.DataCapturerErrorEx {
        intController.setLLCStretcherPosition(position);
    }

    /*
     * See InterferometryControllerInterface.py for a description of this method
     */
    public boolean getLLCPolarizationCalRequired() throws AsynchronousFailureEx {
        return intController.getLLCPolarizationCalRequired();
    }

    /*
     * See InterferometryControllerInterface.py for a description of this method
     */
    public void doLLCPolarizationCalibrationAsynch(int timeout,
            boolean forceCalibration, SimpleCallback cb) {
        intController.doLLCPolarizationCalibrationAsynch(timeout,
                forceCalibration, cb);
    }

    /*
     * -------------- Array Mount Controller Methods ---------------- See the
     * documentation for the Array Mount Controller for description of these
     * methods. Except as noted below.
     */

    /**
     * For interferometry this function needs to control both the phase center
     * and the pointing center of the array. To adjust only the pointing center
     * use the setPointingDirection method.
     */
    public void setSource(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx, TimeoutEx {
        setSourceAsync(fieldSource);
        waitUntilOnSource();
    }

    public void setSource(FieldSourceT fieldSource) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, TimeoutEx {
        SourceOffset zeroOffset = new SourceOffset(0, 0, 0, 0, SourceOffsetFrame.HORIZON);
        setSourceAsync(fieldSource, zeroOffset);
        waitUntilOnSource();
    }
  
    @Deprecated
    public void setFieldSource(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx, TimeoutEx {
        setSource(fieldSource);
    }

    public void setSourceAsync(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        setPointingDirectionAsync(fieldSource);
        setPhaseDirection(fieldSource);
    }

    private void setSourceAsync(FieldSourceT fieldSource, SourceOffset delayOffset)
    throws IllegalParameterErrorEx, ObservingModeErrorEx {
        setPointingDirectionAsync(fieldSource);
        intController.setPhaseDirection(fieldSource, delayOffset);
    }
    
    public void setPointingDirectionAsync(FieldSourceT fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        psc.setSourceAsync(fieldSource);
        curSource = fieldSource;
    }

    public void setPointingDirectionAsync(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        FieldSourceT newSource = ObservingModeUtilities.deserializeFieldSource(
                fieldSource, logger);
        setPointingDirectionAsync(newSource);
    }

    public void setPointingDirection(String fieldSource)
            throws IllegalParameterErrorEx, ObservingModeErrorEx, TimeoutEx {
        setPointingDirectionAsync(fieldSource);
        waitUntilOnSource();
    }

    public void waitForTarget() throws ObservingModeErrorEx, TimeoutEx {
        waitUntilOnSource();
    }

    public void waitUntilOnSource() throws ObservingModeErrorEx, TimeoutEx {
        psc.waitUntilOnSource();
    }

    public ArrayMountController getArrayMountController()
            throws ContainerServicesEx {
        return psc.getArrayMountController();
    }

    public boolean isObservable(String fieldSource, double duration)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        return psc.isObservable(fieldSource, duration);
    }

    public void setElevationLimit(double elevationLimit)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        psc.setElevationLimit(elevationLimit);
    }

    public double getElevationLimit() throws ObservingModeErrorEx {
        return psc.getElevationLimit();
    }

    public void resetLimits() {
        psc.resetLimits();
    }

    public void applyPointingCalibrationResult(PointingCalResult[] result,
            PolarizationType polarization, String[] antennas)
            throws IllegalParameterErrorEx {
        psc.applyPointingCalibrationResult(result, polarization, antennas);
    }

    public void applyFocusCalibrationResult(FocusCalResult[] result,
            PolarizationType polarization, String[] antennas)
            throws IllegalParameterErrorEx {
        psc.applyFocusCalibrationResult(result, polarization, antennas);
    }

    /* ------- Pointing Subarray Controller Interface ------------------- */
    
    public String[] getSubarrays() {
        return psc.getSubarrays();
    }

    public void createSubarray(String subarrayName, String[] antennaList)
            throws ObsModeInitErrorEx, IllegalParameterErrorEx {
        psc.createSubarray(subarrayName, antennaList);
    }

    public void destroySubarray(String subarrayName)
            throws IllegalParameterErrorEx {
        psc.destroySubarray(subarrayName);
    }

    public void setSubarraySource(String subarrayName,
            String fieldSource) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, TimeoutEx {
        setSubarraySourceAsync(subarrayName, fieldSource);
        waitUntilSubarrayOnSource(subarrayName);
    }

    public void setSubarraySourceAsync(String subarrayName,
            String fieldSource) throws IllegalParameterErrorEx,
            ObservingModeErrorEx {
        psc.setSubarraySourceAsync(subarrayName, fieldSource);
    }

    public void waitUntilSubarrayOnSource(String subarrayName)
            throws ObservingModeErrorEx, TimeoutEx, IllegalParameterErrorEx {
        psc.waitUntilSubarrayOnSource(subarrayName);
    }

    public boolean isObservableBySubarray(String subarrayName,
            String fieldSource, double duration)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        return psc.isObservableBySubarray(subarrayName, fieldSource,
                duration);
    }

    public void setSubarrayElevationLimit(String subarrayName,
            double elevationLimit) throws ObservingModeErrorEx,
            IllegalParameterErrorEx {
        psc.setSubarrayElevationLimit(subarrayName, elevationLimit);
    }

    public double getSubarrayElevationLimit(String subarrayName)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        return psc.getSubarrayElevationLimit(subarrayName);
    }

    public void resetSubarrayLimits(String subarrayName)
            throws IllegalParameterErrorEx {
        psc.resetSubarrayLimits(subarrayName);
    }

    public ArrayMountController getSubarrayMountController(String subarrayName)
            throws ContainerServicesEx, IllegalParameterErrorEx {
        return psc.getSubarrayMountController(subarrayName);
    }

    /* -------- Methods from the Local Oscillator Interface -------------- */
    public void enableDelayTracking(boolean enable) throws HardwareFaultEx {
        localOscillator.enableDelayTracking(enable);
    }

    public boolean delayTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        return localOscillator.delayTrackingEnabled();
    }

    public void enableFringeTracking(boolean enable) throws HardwareFaultEx {
        localOscillator.enableFringeTracking(enable);
    }

    public boolean fringeTrackingEnabled() throws HardwareFaultEx,
            AntennasDisagreeEx {
        return localOscillator.fringeTrackingEnabled();
    }

    public void optimizeSignalLevels(float ifTargetLevel, float bbTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeSignalLevels(ifTargetLevel, bbTargetLevel);
    }

    public void optimizeIFSignalLevels(float ifTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeIFSignalLevels(ifTargetLevel);
    }

    public void optimizeBBSignalLevels(float bbTargetLevel)
            throws HardwareFaultEx {
        localOscillator.optimizeBBSignalLevels(bbTargetLevel);
    }

    public short setFrequency(SSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, HardwareFaultEx {

        short nSols = localOscillator.setFrequency(bbspec, overlap);
        // Tell all the mounts about the new frequency. This is used
        // to compute the refraction correction. It may also change
        // the pointing model and move the subreflector.
        psc.setObservingFrequency(localOscillator.averageSkyFrequency());

        return nSols;
    }

    public short setFrequencyDSB(DSBbasebandSpec[] bbspec,
            Band2Band3Overlap overlap) throws IllegalParameterErrorEx,
            ObservingModeErrorEx, HardwareFaultEx {

        short nSols = localOscillator.setFrequencyDSB(bbspec, overlap);
        // Tell all the mounts about the new frequency. This is used
        // to compute the refraction correction. It may also change
        // the pointing model and move the subreflector.
        double mean = localOscillator.averageSkyFrequency();
        psc.setObservingFrequency(mean);

        return nSols;
    }

    public double[] getFrequency() throws HardwareFaultEx, AntennasDisagreeEx {
        return localOscillator.getFrequency();
    }

    public void setCalibrationDevice(CalibrationDevice device)
            throws ObservingModeErrorEx, TimeoutEx {
        localOscillator.setCalibrationDevice(device);
    }

    public CalibrationDevice getCalibrationDevice() throws AntennasDisagreeEx {
        return localOscillator.getCalibrationDevice();
    }

    public void setSpectrum(String spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        setSpectrumAsync(spectralSpec);
        // TODO.Implement the following function
        // localOscillator.waitUntilTuned();
    }

    public void setSpectrumAsync(String spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        SpectralSpecT ss = ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger);
        setSpectrumAsync(ss);
    }
    
    private void setSpectrumAsync(SpectralSpecT spectralSpec)
            throws ObservingModeErrorEx, IllegalParameterErrorEx {
        // TODO. Also configure the correlator (ideally asynchronously)
        localOscillator.setSpectrumAsync(spectralSpec);
        
        if (spectralSpec.getSquareLawSetup() != null) {
            slc.setIntegrationDuration(SquareLawControllerImpl.extractSLIntegrationDuration(spectralSpec,
                    logger));   
        }
  
        // Tell all the mounts about the new frequency. This is used
        // to compute the refraction correction. It may also change
        // the pointing model and move the subreflector.
        final double freq = localOscillator.averageSkyFrequency();
        psc.setObservingFrequency(freq);
    }

    public double averageSkyFrequency() {
        return localOscillator.averageSkyFrequency();
    }

    public void optimizeSASPolarizationAsynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPolarizationAsynch(forceCalibration, cb);
    }

    public void optimizeSASPol1Asynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPol1Asynch(forceCalibration, cb);
    }

    public void optimizeSASPol2Asynch(boolean forceCalibration,
            SimpleCallback cb) {
        localOscillator.optimizeSASPol2Asynch(forceCalibration, cb);
    }

    public void setOffsetToRejectImageSideband(boolean rejectImage) 
	throws HardwareFaultEx
    {
	localOscillator.setOffsetToRejectImageSideband(rejectImage);
    }

    public LocalOscillator getLocalOscillator() throws ContainerServicesEx {
        return localOscillator.getLocalOscillator();
    }

    public int setSpectralSpec(String spectralSpec)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        SpectralSpecT ss = ObservingModeUtilities.deserializeSpectralSpec(
                spectralSpec, logger);
        return setSpectralSpec(ss);
    }

    private int setSpectralSpec(SpectralSpecT ss)
            throws IllegalParameterErrorEx, ObservingModeErrorEx {
        curSpectrum = null;

        // Tuning the receiver i.e., set the LO's. We wait for this to complete
        // in doSubscan.
        ss = localOscillator.alignSpectralSpec(ss);
        localOscillator.setSpectrumAsync(ss);

        int configId = intController.loadConfigurations(new SpectralSpecT[] { ss })[0];
        curSpectrum = ss;
        return configId;
    }

    public String dopplerShift(String spectralSpec, 
                String dopplerSource, long epoch)
                    throws ObservingModeErrorEx, IllegalParameterErrorEx {
        return localOscillator.dopplerShift(spectralSpec, dopplerSource, epoch);
    }
}
