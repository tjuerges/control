#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
# who       when        what
# --------  ----------  ----------------------------------------------
# rhiriart  2006-11-13  created
#

from Acspy.Util.XmlObjectifier import XmlObject
from CCL.EnumerationHelper import getEnumeration
from CCL.SpectralSpec import SpectralSpec
from CCL.logging import getLogger
from PyDataModelEnumeration import PyPolarizationType
from log_audience import OPERATOR
from math import degrees
import ACSLog
import CorrelatorCalibrationMod
import re
import Control

from CCL.LocalOscillatorInterferometricInterface import \
     LocalOscillatorInterferometricInterface
from CCL.PointingSubarrayControllerInterface import \
     PointingSubarrayControllerInterface
from CCL.InterferometryControllerInterface import \
     InterferometryControllerInterface
from CCL.ObservingModeBase import ObservingModeBase
    
class SFIObservingMode(ObservingModeBase,
                       LocalOscillatorInterferometricInterface,
                       PointingSubarrayControllerInterface,
                       InterferometryControllerInterface):

    """Single Field Interferometry Array Observing Mode.
    """
    
    def __init__(self, sfi_offshoot, logger):
        ObservingModeBase.__init__(self)
        LocalOscillatorInterferometricInterface.__init__(self, sfi_offshoot,
                                                         logger)
        PointingSubarrayControllerInterface.__init__(self, sfi_offshoot, logger)
        InterferometryControllerInterface.__init__(self, sfi_offshoot, logger)

    def getAntennas(self):
        """
        Returns a list containing the antenna names allocated in the array.
        """
        return self._offshoot.getAntennas()
        
    def setFieldSource(self, fieldSource):
        msg = "Deprecated method setFieldSource(fieldSource) in SingleFieldInterferometryObservingMode.py. Use setSource(fieldSource) instead"
        self.logToOperator(ACSLog.ACS_LOG_INFO, msg)
        self.setSource(fieldSource)
        
    def setPointingDirection(self, fieldSource):
        """
        This sets the pointing direction of all antennas in the array.
        to point as the specified source.
        """
        self._offshoot.setPointingDirection(fieldSource.getXMLString())
        
    def setDirection(self, fieldSource):
        """
        DEPRECATED

        The setDirection method will point all antennas in the array
        at the specified source and will set the phase center to be
        at the same source.
        """
        self.setPointingDirection(fieldSource)
        self.setPhaseDirection(fieldSource)

    def beginScan(self, purposes):
        scanIntentData = []
        if not isinstance(purposes, list):
            purposes = [purposes]
        for purpose in purposes:
            scanIntentData.append(purpose.getScanIntentData())
        self._offshoot.beginScan(scanIntentData)

    def endScan(self):
        self._offshoot.endScan()

    def setTarget(self, fieldSource, spectralSpec):
        ss = self._offshoot.setTarget(fieldSource.getXMLString(),
                                      spectralSpec.getXMLString())
        
    def doSubscanSequence(self, subscanSeqSpec):
        '''
        We need to convert the fields of the SubscanSequenceSpec to
        XML strings in order to send this coomand across CORBA
        '''
        import copy
        sourceXML = []
        for source in subscanSeqSpec.sources:
            sourceXML.append(source.getXMLString())

        spectraXML = []
        for spectra in subscanSeqSpec.spectra:
            spectraXML.append(spectra.getXMLString())
            
        acsSubscans = []
        for subscan in subscanSeqSpec.subscans:
            acsSubscan = copy.copy(subscan)
            acsSubscan.duration = int(acsSubscan.duration/100E-9)
            acsSubscans.append(acsSubscan)

        corbaSpecification = Control.SubscanSequenceSpec(sourceXML,
                                                         spectraXML,
                                                         acsSubscans)
        self._offshoot.doSubscanSequence(corbaSpecification)

    def doSubscan(self, subscanDuration, subScanIntents):
        self._offshoot.doSubscan(long(subscanDuration/100E-9), subScanIntents)
 
    def logToOperator(self, priority, logMessage):
        '''
        Method which logs a report to with the OPERATOR flag set.
        '''
        self._logger.logNotSoTypeSafe(priority, "SFI Obs Mode: %s" %
                                     logMessage,
                                     OPERATOR)
