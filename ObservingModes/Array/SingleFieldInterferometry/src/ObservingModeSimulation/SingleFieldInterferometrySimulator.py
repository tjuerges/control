#!/usr/env/python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#
#

from ObservingModeSimulation.ObservingModeBaseSimulator import *
from ObservingModeSimulation.PointingSubarrayControllerSimulator import *
from ObservingModeSimulation.InterferometryControllerSimulator import *
from ObservingModeSimulation.LocalOscillatorSimulator import *

from CCL.utils import aggregate

class SingleFieldInterferometrySimulator(ObservingModeBaseSimulator,
                                         PointingSubarrayControllerSimulator,
                                         LocalOscillatorSimulator,
                                         InterferometryControllerSimulator):
    def __init__(self, arraySimulator):
        ObservingModeBaseSimulator.__init__(self, arraySimulator)
        PointingSubarrayControllerSimulator.__init__(self, arraySimulator)
        InterferometryControllerSimulator.__init__(self, arraySimulator)
        
    def getAntennas(self):
        return self._array.getAntennas()
                
    def setPointingDirection(self, subscanFieldSource):
        # Delegate this directly to the AMC
        ArrayMountControllerSimulator.setSource(subscanFieldSource)

    def setTarget(self, subScanFieldSource, subScanSpectralSpec):
        self.setSource(subScanFieldSource)
        self.setSpectralSpec(subScanSpectralSpec)
        
    def doSubscan(self, subScanIntents):
        self.beginSubscan(subScanIntents)
        self.executeSubscan(30)
        self.endSubscan()
 
    # This method actually belongs in an LO simulator Class
    def setCalibrationDevice(self, state):
        pass
