/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ObservingModes;

import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.Control.AntennaCharacteristics;
import alma.Control.BDDStreamInfo;
import alma.Control.ResourceId;
import alma.Control.SingleFieldInterferometryObservingMode;
import alma.Control.SingleFieldInterferometryObservingModeHelper;
import alma.Control.SingleFieldInterferometryObservingModePOATie;
import alma.Control.SingleFieldInterferometryObservingModeTester;
import alma.Control.SingleFieldInterferometryObservingModeTesterOperations;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ContainerServices;
import alma.acs.nc.SimpleSupplier;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.offline.DataCapturer;
import alma.xmlentity.XmlEntityStruct;


public class SingleFieldInterferometryTesterImpl implements ComponentLifecycle, 
    SingleFieldInterferometryObservingModeTesterOperations {

    private ContainerServices container;
    private Logger logger;    
    private Simulator simulator;
    
    private int corrConfigID = 0;
    private int scan = 0;
    private int subScan = 0;
    
    private SingleFieldInterferometryObservingModeImpl sfiObsMode;
    private SingleFieldInterferometryObservingModePOATie sfiObsModeTie;
    
    // -----------------------------------------------------------
    // Implementation of ComponentLifecycle
    // -----------------------------------------------------------
    @Override
    public void initialize(ContainerServices containerServices) {
        container = containerServices;
        logger = containerServices.getLogger();
        logger.info("initialize() called...");
        
        Resource<SimpleSupplier> pubres = 
            new PublisherResource(container, "CONTROL_SYSTEM", true);
        ResourceManager.getInstance(container).addResource(pubres);
        Resource<DataCapturer> dcres =
            new DynamicResource(container, "Array001/DATACAPTURER", 
                "IDL:alma/offline/DataCapturer:1.0");        
        try {
            ResourceManager.getInstance(container).acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource: ";
            logger.severe(msg);
        }
                
        String simIF = "IDL:alma/ACSSim/Simulator:1.0";
        try {
            this.simulator = 
                SimulatorHelper.narrow(container.getDefaultComponent(simIF));
        } catch(Exception e) {}
            
        String code = "";

        // The simulator will return random numbers for the integ time and
        // the comparison for all integ times the same will barf.
        // This code returns the same integ time and fixes this problem.
        code  = "LOGGER.logInfo('AntInterferometry.getWVRIntegrationTime called...')\n";
        code += "1000000000";
        simulator.setMethodIF(
            "IDL:alma/Control/AntInterferometryController:1.0",
             "getWVRIntegrationTime", code, 0);
        
    }

    @Override
    public void execute() {
        logger.info("execute() called...");
    }

    @Override
    public void cleanUp() {
        logger.info("cleanUp() called");
        if (sfiObsMode != null) {
            logger.fine("Deactivating SFI Offshoot.");            
            sfiObsMode.cleanUp();
            if (sfiObsModeTie != null) {
                try {
                    container.deactivateOffShoot(sfiObsModeTie);
                } catch (AcsJContainerServicesEx ex) {
                    String msg = "Error deactivating SFI Offshoot.";
                    logger.fine(msg);
                    ex.log(logger);
                }
                sfiObsModeTie = null;
            }
            sfiObsMode = null;
        }
        ResourceManager.getInstance(container).releaseResources();
        ResourceManager.getInstance(container).freeAllResources();
    }

    @Override
    public void aboutToAbort() {
        cleanUp();
        logger.info("managed to abort...");
        System.out.println("Foo component managed to abort...");
    }

    // -----------------------------------------------------------
    // Implementation of ACSComponent
    // -----------------------------------------------------------

    @Override
    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }

    @Override
    public String name() {
        return container.getName();
    }

        
    // -----------------------------------------------------------
    // Implementation of SingleFieldInterferometryTester
    // -----------------------------------------------------------

    public SingleFieldInterferometryObservingMode getSFIOffshoot() 
        throws ObsModeInitErrorEx {
        
        try {
            ObservingModeArray array = ObservingModeTesterImpl.getFakeArray();
            sfiObsMode = 
              new SingleFieldInterferometryObservingModeImpl(container, array);
            sfiObsModeTie = new SingleFieldInterferometryObservingModePOATie(
                sfiObsMode);
            SingleFieldInterferometryObservingMode sfiOffshoot = 
                SingleFieldInterferometryObservingModeHelper.narrow(
                    container.activateOffShoot(sfiObsModeTie));
            return sfiOffshoot;
        } catch (ObsModeInitErrorEx ex) {
            logger.fine("Error creating SFI Offshot: " + ex.toString());
            AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
            throw exhlp.toObsModeInitErrorEx();
        } catch (AcsJContainerServicesEx ex) {
            logger.fine("Error creating SFI Offshot: " + ex.toString());
            AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
            throw exhlp.toObsModeInitErrorEx();
        }        
    }

	public void deactivateSFIOffshoot() {
		sfiObsMode.cleanUp();
		try {
			container.deactivateOffShoot(sfiObsModeTie);
		} catch (AcsJContainerServicesEx ex) {
			ex.printStackTrace();
		}
		sfiObsMode = null;
		sfiObsModeTie = null;
	}

}
