/*
 * ALMA - Atacama Large Millimeter Array
 * (c) European Southern Observatory, 2002
 * (c) Associated Universities Inc., 2002
 * Copyright by ESO (in the framework of the ALMA collaboration),
 * Copyright by AUI (in the framework of the ALMA collaboration),
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307  USA
 *
 * "@(#) $Id$"
 */

package alma.Control.ObservingModes;

import java.util.logging.Logger;

import junit.framework.Test;
import junit.framework.TestSuite;
import alma.ACSSim.Simulator;
import alma.ACSSim.SimulatorHelper;
import alma.AntennaMotionPatternMod.AntennaMotionPattern;
import alma.CalDataOriginMod.CalDataOrigin;
import alma.CalibrationFunctionMod.CalibrationFunction;
import alma.CalibrationSetMod.CalibrationSet;
import alma.Control.AbortionException;
import alma.Control.AntennaCharacteristics;
import alma.Control.BDDStreamInfo;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.Control.Common.StaticResource;import alma.Control.ResourceId;
import alma.Control.ScanEndedEvent;
import alma.Control.ScanIntentData;
import alma.Control.ScanPurpose;
import alma.Control.ScanStartedEvent;
import alma.Control.SubscanEndedEvent;
import alma.Control.SubscanStartedEvent;
import alma.Correlator.BaseBandConfig;
import alma.Correlator.CorrelatorConfiguration;
import alma.Correlator.ObservationControl;
import alma.Correlator.SpectralWindow;
import alma.ObservingModeExceptions.DataCapturerErrorEx;
import alma.ObservingModeExceptions.ObservingModeErrorEx;
import alma.ObservingModeExceptions.SubScanDataFlowErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.ScanIntentMod.ScanIntent;
import alma.SubscanIntentMod.SubscanIntent;
import alma.acs.component.client.ComponentClientTestCase;
import alma.acs.container.ContainerServices;
import alma.acs.nc.CorbaNotificationChannel;
import alma.acs.nc.SimpleSupplier;
import alma.acs.nc.Receiver;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.offline.DataCapturer;
import alma.xmlstore.Identifier;

public class SFIObservingModeTest extends ComponentClientTestCase {

    private Logger logger;
    private ResourceManager resMng;
    private Simulator sim;
    private SingleFieldInterferometryObservingModeImpl obsMode;
    private ObservingModeArray array;
    private boolean scanStartedEventReceived;
    private ScanStartedEvent scanStartedEvent;
    private boolean scanEndedEventReceived;
    private ScanEndedEvent scanEndedEvent;
    private boolean subScanStartedEventReceived;
    private SubscanStartedEvent subScanStartedEvent;
    private boolean subScanEndedEventReceived;
    private SubscanEndedEvent subScanEndedEvent;
    private String code;

    ////////////////////////////////////////////////////////////////////
    // Constructors
    ////////////////////////////////////////////////////////////////////
    
    public SFIObservingModeTest(String test) throws Exception {
        super(test);
    }
    
    public SFIObservingModeTest() throws Exception {
        super(SFIObservingModeTest.class.getName());
    }

    ////////////////////////////////////////////////////////////////////
    // Test fixture methods
    ////////////////////////////////////////////////////////////////////
    
    protected void setUp() throws Exception {
        
        super.setUp();
        ContainerServices cont = getContainerServices();
        this.logger = cont.getLogger();
        
        sim = ObservingModeTesterImpl.modifyPythonSimCode(cont);

        code =
            "import Control\n" +
            "from Control import MountController\n" + 
            "eq = Control.EquatorialDirection(0, 0)\n" + 
            "hz = Control.HorizonDirection(0, 0)\n" +
            "of = MountController.Offset(0.0, 0.0)\n" +
            "pd = MountController.PointingData(" +
                  "eq, of, eq, of, hz, hz, hz, hz, True, eq, 0, False)\n" + 
            "[pd]";
        sim.setMethodIF("IDL:alma/Control/SingleFieldInterferometry:1.0",
                "getPointingDataTable", code, 0);
        
        this.array = ObservingModeTesterImpl.getFakeArray();
        
        AntennaCharacteristics[] ac = array.getAntennaConfigurations();
        for (AntennaCharacteristics a : ac) {
            String antName = a.antennaName;
            String mcn = "CONTROL/" + antName + "/MountController";
            logger.info("Setting behaviour of mount controller for antenna '" +
                        antName + "'");
            code = "antennaName = parameters[0]\n";
            code += "LOGGER.logInfo('allocate(' + antennaName + ') called on " +
                        mcn + "')\n";
            code += "from Acssim.Goodies import setGlobalData\n";
            code += "setGlobalData('"+mcn+":antennaName', antennaName)\n";
            code += "None";
            sim.setMethod(mcn, "allocate", code, 0.0);
            
            code = "LOGGER.logInfo('waitUntilOnSourceCB called on " + mcn + 
                        "')\n";
            code += "callback = parameters[0]\n";
            code += "import ACSErr\n";
            code += "compl = ACSErr.Completion(0, 0, 0, [])\n";
            code += "from Acssim.Goodies import getGlobalData\n";
            code += "antennaName = getGlobalData('" + mcn + ":antennaName')\n";
            code += "LOGGER.logInfo('" + mcn + 
                        " reporting callback for antenna ' + antennaName)\n";
            code += "callback.report(antennaName, compl)\n";
            code += "None";
            sim.setMethod(mcn, "waitUntilOnSourceCB", code, 0.0);
            
            code = "LOGGER.logInfo('getPointingDataTable called on " + mcn + 
                        "')\n";
            code +=
                "import Control\n" +
                "from Control import MountController\n" +
                "eq = Control.EquatorialDirection(0, 0)\n" +
                "hz = Control.HorizonDirection(0, 0)\n" +
                "of = MountController.Offset(0.0, 0.0)\n" +
                "pd = MountController.PointingData" +
                    "(eq, of, eq, of, hz, hz, hz, hz, True, eq, 0, False)\n" +
                    "[pd]";
            
            sim.setMethod(mcn, "getPointingDataTable", code, 0.0);
        }
        
        this.resMng = ResourceManager.getInstance(cont);
        Resource<Identifier> idres = new StaticResource<Identifier>(cont, 
            "ARCHIVE_IDENTIFIER", "alma.xmlstore.IdentifierHelper", true);
        resMng.addResource(idres);        
        Resource<DataCapturer> dcres = 
            new DynamicResource<DataCapturer>(cont,
                                          array.getDataCapturerName(),
                                          "IDL:alma/offline/DataCapturer:1.0");
        resMng.addResource(dcres);
                
        Resource<ObservationControl> obsres = 
                new StaticResource<ObservationControl>(cont,
                    "IDL:alma/Correlator/ObservationControl:1.0",
                    "alma.Correlator.ObservationControlHelper", 
                    false, true, logger);                
		resMng.addResource(obsres);
        
        Resource<SimpleSupplier> pubres = 
            new PublisherResource(getContainerServices(), 
                "CONTROL_SYSTEM", true);
        resMng.addResource(pubres);

        try {
            resMng.acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource: ";
            fail(msg + ex);
        }

        obsMode = null;
        
        scanStartedEventReceived = false;
        scanStartedEvent = null;
        
        scanEndedEventReceived = false;
        scanEndedEvent = null;
        
        subScanStartedEventReceived = false;
        subScanStartedEvent = null;
        
        subScanEndedEventReceived = false;
        subScanEndedEvent = null;
    }
    
    protected void tearDown() throws Exception {
        getContainerServices().releaseComponent(sim.name());
        resMng.releaseResources();
        super.tearDown();
    }

    ////////////////////////////////////////////////////////////////////
    // Test cases
    ////////////////////////////////////////////////////////////////////
    
    public void testBasic() throws Exception {
        logger.info("Starting testBasic");        
        SingleFieldInterferometryObservingModeImpl sfi = 
            new SingleFieldInterferometryObservingModeImpl
            (getContainerServices(), array);
        logger.info("Calling to getLOObsMode");
        //alma.Control.LocalOscillator lo = sfi.getLOObservingMode();
        logger.info("Done a call to getLOObsMode");
        logger.info("Number of solutions " + sfi.getLocalOscillator().numberOfSolutions());
        logger.info("Calling to getArrayMountController");
        alma.Control.ArrayMountController amc = sfi.getArrayMountController();
        logger.info("Done a call to getArrayMountController");
        logger.info("Is observable " + amc.isObservableEquatorial(0, 0, 0, 0, 0, 0));
        logger.info("Calling amc.setDirection");
        amc.setDirection(0.0, 0.0, 0.0, 0.0, 0.0);
        //        logger.info("Calling sfi.setpointingDirection");
        // sfi.setPointingDirection(0.0, 0.0, 0.0, 0.0, 0.0);
        logger.info("Completed testBasic");
    }
    
    public synchronized void testBeginScan() throws Exception {
        logger.info("Starting testBeginScan");
        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
            "CONTROL_SYSTEM", getContainerServices());
        receiver.attach("alma.Control.ScanStartedEvent", this);
        receiver.begin();

        // Install some behavior in the DataCapturer simulator
        String code = "LOGGER.logInfo('startScan() called')\n";
        code +=       "LOGGER.logInfo('arrayId: ' + parameters[0])\n";
        code +=       "LOGGER.logInfo('startTime: ' + str(parameters[1]))\n";
        code +=       "LOGGER.logInfo('scanNumber: ' + str(parameters[2]))\n";
        code +=       "LOGGER.logInfo('intent: ' + str(parameters[3]))\n";
        code +=       "None";
        sim.setMethod(array.getDataCapturerName(), "startScan", code, 0.0);
        
        // Construct the obs. mode and call beginScan
        obsMode = new SingleFieldInterferometryObservingModeImpl
            (getContainerServices(), array);
        
        ScanIntentData intent = new ScanIntentData();
        intent.scanIntent = ScanIntent.OBSERVE_TARGET;
        intent.calDataOrig = CalDataOrigin.NONE;
        intent.calSet = CalibrationSet.NONE;
        intent.calFunction = CalibrationFunction.UNSPECIFIED;
        intent.antennaMotionPattern = AntennaMotionPattern.NONE;
        intent.onlineCalibration = false;
        ScanIntentData[] intents = { intent };
        
        
        obsMode.beginScan(intents);
        
        
        // Wait for the ScanStartedEvent
        while (!scanStartedEventReceived) {
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Timeout when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }
        }
        
        assertEquals(1, array.getScanNumber());
        assertEquals(0, array.getSubScanNumber());
        logger.info("Completed testBeginScan");
    }

    public synchronized void testEndScan() throws Exception {
        
        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
            "CONTROL_SYSTEM", getContainerServices());
        receiver.attach("alma.Control.ScanEndedEvent", this);
        receiver.begin();

        // Install some behavior in the DataCapturer simulator
        String code = "LOGGER.logInfo('endScan() called')\n";
        code +=       "LOGGER.logInfo('arrayId: ' + parameters[0])\n";
        code +=       "LOGGER.logInfo('startTime: ' + str(parameters[1]))\n";
        code +=       "LOGGER.logInfo('scanNumber: ' + str(parameters[2]))\n";
        code +=       "LOGGER.logInfo('numberOfSubScans: ' + str(parameters[3]))\n";
        code +=       "None";
        sim.setMethod(array.getDataCapturerName(), "endScan", code, 0.0);
        
        // Construct the obs. mode and call beginScan
        obsMode = new SingleFieldInterferometryObservingModeImpl
            (getContainerServices(), array);
        obsMode.endScan();
        
        // Wait for the ScanEndedEvent
        while (!scanEndedEventReceived) {
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Thread interrupted when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }                
        }
    }
    
    public synchronized void testBeginSubScan() throws Exception {
        
        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver
            ("CONTROL_SYSTEM", getContainerServices());
        receiver.attach("alma.Control.ScanStartedEvent", this);
        receiver.attach("alma.Control.SubscanStartedEvent", this);
        receiver.begin();

        // Install some behavior in the DataCapturer simulator
        String code = "LOGGER.logInfo('startCorrelatorSubscan() called')\n";
        code +=       "None";
        sim.setMethod(array.getDataCapturerName(), "startCorrelatorSubscan", code, 0.0);
        
        // Construct the obs. mode
        obsMode = new SingleFieldInterferometryObservingModeImpl
            (getContainerServices(), array);
        
        ScanPurpose[] purposes = new ScanPurpose[1];
        purposes[0] = ScanPurpose.TARGET;
        
        // In order to call beginSubscan(), it is needed to call
        // beginScan() first. The Source object that is passed in the second
        // paramenter is sent to DataCapturer in beginSubscan().
        ScanIntentData intent = new ScanIntentData();
        intent.scanIntent = ScanIntent.OBSERVE_TARGET;
        intent.calDataOrig = CalDataOrigin.NONE;
        intent.calSet = CalibrationSet.NONE;
        intent.calFunction = CalibrationFunction.UNSPECIFIED;
        intent.antennaMotionPattern = AntennaMotionPattern.NONE;
        intent.onlineCalibration = false;
        ScanIntentData[] intents = { intent };
        obsMode.beginScan(intents);
        
        // Wait for the ScanStartedEvent
        while (!scanStartedEventReceived) {
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Timeout when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }
        }
                
        SubscanIntent[] subScanIntents = { SubscanIntent.ON_SOURCE };
        obsMode.beginSubscan(subScanIntents);
        
        // Wait for the ScanEndedEvent
        while (!subScanStartedEventReceived) {
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Thread interrupted when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }                
        }
        
    }

    /**
     * FIXME
     * This test is currently broken. It would not compile so I have 
     * commented out the lines that would not compile.
     * S. Scott, 23 Sept, 2010.
     */
    public synchronized void testEndSubScan() throws Exception {
        
        // Create a consumer for the ExecBlockEndedEvent.
        Receiver receiver;
        logger.info("Creating NC Receiver and subscribing to events...");
        receiver = CorbaNotificationChannel.getCorbaReceiver(
            "CONTROL_SYSTEM", getContainerServices());
        receiver.attach("alma.Control.SubscanEndedEvent", this);
        receiver.attach("alma.Control.ScanStartedEvent", this);
        receiver.attach("alma.Control.SubscanStartedEvent", this);
        receiver.begin();

        ScanPurpose[] purposes = new ScanPurpose[1];
        purposes[0] = ScanPurpose.TARGET;

        // Construct the obs. mode
        obsMode = new SingleFieldInterferometryObservingModeImpl
            (getContainerServices(), array);
        
        // In order to call beginSubscan(), it is needed to call
        // beginScan() first. The Source object that is passed in the second
        // parameter is sent to DataCapturer in beginSubscan().
        ScanIntentData intent = new ScanIntentData();
        intent.scanIntent = ScanIntent.OBSERVE_TARGET;
        intent.calDataOrig = CalDataOrigin.NONE;
        intent.calSet = CalibrationSet.NONE;
        intent.calFunction = CalibrationFunction.UNSPECIFIED;
        intent.antennaMotionPattern = AntennaMotionPattern.NONE;
        intent.onlineCalibration = false;
        ScanIntentData[] intents = { intent };
        obsMode.beginScan(intents);
        
        // Wait for the ScanStartedEvent
        while (!scanStartedEventReceived) {
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Timeout when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }
        }
                
        // Call beginSubscan. In order to call endSubScan() it is needed to call
        // beginSubscan() first because the IntegrationDetails structure, which
        // endSubscan() uses, is setup in beginSubscan().
        
        SubscanIntent[] subScanIntents = { SubscanIntent.ON_SOURCE };
        obsMode.beginSubscan(subScanIntents);
        
        // Wait for the ScanEndedEvent
        while (!subScanStartedEventReceived) {
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Thread interrupted when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }                
        }        
                
        // The dummy CorrelatorConfiguration specified a 5 sec. subscan, so
        // the test must wait for
        // (subscan duration) + (5 sec. ahead time)
        long sleepTime = 10000;
        Thread.sleep(sleepTime);
        logger.info("Calling endSubscan()");
        
        // Need to call endSubscan() in a different thread, so we get the
        // opportunity to call subScanDataReady().
        Thread thread = new Thread() {
            public void run() {
                // FIXME: will not compile. S. Scott, 23 Sept, 2010
                /*
                try {
                    obsMode.endSubScan();
                } catch (ObservingModeErrorEx e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                */
            }
        };
        thread.start();
        
        // Notify that CORRELATOR has sent the subscan data to DataCapturer.
        logger.info("Notifying that subscan data is ready.");
        // ** FIXME **
        // The following line would not compile, but I don't know what the 
        // correct call should be. S. Scott 22 Aug, 2010
        //obsMode.subScanDataReady();
        
        // Wait for the ScanEndedEvent
        logger.info("Waiting to receive SubscanEndedEvent.");
        while (!subScanEndedEventReceived) {
            try {
                wait(5000);
            } catch (InterruptedException ex) {
                String msg = "Thread interrupted when waiting for event.";
                logger.severe(msg);
                fail(msg);
            }                
        }
    }

    public void testModifyCorrConfigForCalibration() throws Exception {
        
        // Initial spectral window configuration
        // swu1.centerFrequencyMHz = 50.0
        // swu1.effectiveBandwidthMHz = 100.0
        // swu1.effectiveNumberOfChannels = 2048
        SpectralWindow swu1 = new SpectralWindow(50.0,
                                                 100.0,
                                                 2048, 
                                                 1, 
                                                 alma.NetSidebandMod.NetSideband.LSB,
                                                 0, 
                                                 false, 
                                                 alma.WindowFunctionMod.WindowFunction.HANNING, 
                                                 new alma.Correlator.ChannelAverageRegion[] {new alma.Correlator.ChannelAverageRegion(10, 0)},
                                                 false,
                                                 alma.CorrelationBitMod.CorrelationBit.BITS_2x2,
                                                 false,
                                                 new alma.StokesParameterMod.StokesParameter[] {alma.StokesParameterMod.StokesParameter.XX}, 
                                                 false);
        
        BaseBandConfig bbc = 
            new BaseBandConfig(alma.BasebandNameMod.BasebandName.BB_1,
                               true, true,
                               alma.AccumModeMod.AccumMode.NORMAL,
                               alma.CorrelationModeMod.CorrelationMode.CROSS_AND_AUTO,
                               alma.SidebandProcessingModeMod.SidebandProcessingMode.NONE,
                               new alma.Correlator.SpectralWindow[] {swu1},
                               new alma.Correlator.BinSwitching_t(alma.SwitchingModeMod.SwitchingMode.NO_SWITCHING,
                                       1, new long[] {0L}, new long[] {10080000L}),
                               8500.0,
                               alma.ACAPolarizationMod.ACAPolarization.ACA_STANDARD,
                               0.0);
        BaseBandConfig[] bbcs = {bbc};
        
        /*
         * FIXME: will not compile. S. Scott, 23 Sept, 2010
        CorrelatorConfiguration corrConf = 
            new CorrelatorConfiguration(160000L,
                                        10080000L,
                                        5120000L,
                                        10080000L,
                                        alma.ReceiverSidebandMod.ReceiverSideband.DSB,
                                        bbcs,
                                        new alma.AtmPhaseCorrectionMod.AtmPhaseCorrection[]
                                        {alma.AtmPhaseCorrectionMod.AtmPhaseCorrection.AP_CORRECTED},
                                        100000.0,
                                        new alma.Correlator.ACAPhaseSwitchingConfigurations(true, true, 0L));        
        */
        obsMode = new SingleFieldInterferometryObservingModeImpl
            (getContainerServices(), array);
        //        obsMode.modifyCorrConfigForCalibration(corrConf);
        
        // Verify the modified CorrelatorConfiguration...
        /* 
         * FIXME: will not compile. S. Scott, 23 Sept, 2010
        assertEquals(2, corrConf.baseBands[0].spectralWindows.length);
        
        SpectralWindow swc0 = corrConf.baseBands[0].spectralWindows[0];
        assertEquals(4096, swc0.effectiveNumberOfChannels);
        assertEquals(2000.0, swc0.effectiveBandwidthMHz);
        
        SpectralWindow swc1 = corrConf.baseBands[0].spectralWindows[1];
        assertEquals(swu1.effectiveBandwidthMHz, swc1.effectiveBandwidthMHz);
        assertEquals(swu1.effectiveNumberOfChannels / 2, swc1.effectiveNumberOfChannels);
        */
    }
    
    ////////////////////////////////////////////////////////////////////
    // Event reception methods
    ////////////////////////////////////////////////////////////////////
    
    public synchronized void receive(ScanStartedEvent event) {
        logger.info("Received ScanStartedEvent.");
        scanStartedEvent = event;
        scanStartedEventReceived = true;
        notify();
    }

    public synchronized void receive(ScanEndedEvent event) {
        logger.info("Received ScanEndedEvent.");
        scanEndedEvent = event;
        scanEndedEventReceived = true;
        notify();
    }

    public synchronized void receive(SubscanStartedEvent event) {
        logger.info("Received SubscanStartedEvent.");
        subScanStartedEvent = event;
        subScanStartedEventReceived = true;
        notify();
    }
    
    public synchronized void receive(SubscanEndedEvent event) {
        logger.info("Received SubscanEndedEvent.");
        subScanEndedEvent = event;
        subScanEndedEventReceived = true;
        notify();
    }
    
    ////////////////////////////////////////////////////////////////////
    // Suite
    ////////////////////////////////////////////////////////////////////

    public static Test suite() {
        TestSuite suite = new TestSuite();
        String testSuite = System.getProperty("suite");
        if (testSuite == null)
            testSuite = "1"; // Default test suite.
        try {
            if (testSuite.equals("1")) {
                suite.addTest(new SFIObservingModeTest("testBasic"));
            } else if (testSuite.equals("2")) {
                suite.addTest(new SFIObservingModeTest("testBeginScan"));
            } else if (testSuite.equals("3")) {
                suite.addTest(new SFIObservingModeTest("testEndScan"));
            } else if (testSuite.equals("4")) {
                suite.addTest(new SFIObservingModeTest("testBeginSubScan"));
            } else if (testSuite.equals("5")) {
                suite.addTest(new SFIObservingModeTest("testEndSubScan"));
            } else if (testSuite.equals("6")) {
                suite.addTest(new SFIObservingModeTest("testModifyCorrConfigForCalibration"));                
            }
        } catch (Exception ex) {
            System.err.println("Error when creating AutomaticArrayStartingUpStateTest: "
                    + ex.toString());
        }
        return suite;
    }
}

// __oOo__
