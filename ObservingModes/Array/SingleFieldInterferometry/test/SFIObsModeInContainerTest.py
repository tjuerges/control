#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

import unittest
import Control
import ControlExceptions

from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Util.XmlObjectifier import XmlObject
from CCL.logging import getLogger
from CCL.APDMValueTypes import Frequency
from CCL.BaseBandSpecification import BaseBandSpecification
from CCL.CorrelatorConfig import CorrelatorConfig, BLCorrelatorConfig
from CCL.FieldSource import EquatorialSource
from CCL.SingleFieldInterferometryObservingMode import SFIObservingMode
from CCL.SpectralSpec import SpectralSpec
import ObservingModeTester_idl

class SFIObsModeInContainerTest(unittest.TestCase):
    
    def setUp(self):
        self.client = PySimpleClient()
        self.logger = getLogger()
        try:
            # Insert methods into python simulators
            ObsModetesterCompName = "CONTROL/ObservingModeTester"
            self.testutil  = self.client.getComponent(ObsModetesterCompName)
            if self.testutil == None:
                m = "Could not get '"+ObsModetesterCompName+"' component!!"
                print m
                self.logger.severe(m)
                raise Exception, m
            simidl =  "IDL:alma/ACSSim/Simulator:1.0"
            self.simulator = self.client.getDefaultComponent(simidl)
            if self.simulator == None:
                m = "Could not get component for '"+simidl+"'!!"
                print m
                self.logger.severe(m)
                raise Exception, m
            self.testutil.modifySimulationCode(self.simulator)
        except Exception, e:
            m = "Exception modifying simulator code:\n" + str(e)
            print m
            self.logger.info(m)
            raise

        self.tester = self.client.getComponent('SFI_TESTER')
        if self.tester == None:
            m = "Could not get SFI_TESTER component!!"
            print m
            self.logger.severe(m)
            raise Exception, m
        
        #self.simulator.removeAllMethods()
        code  = "LOGGER.logInfo('allocate called')\n"
        code += "LOGGER.logInfo('Arg antenna = ' + parameters[0])\n"
        code += "setGlobalData(SELF.getName()+':allocAntenna', parameters[0])\n"
        code += "None";
        self.simulator.setMethodIF("IDL:alma/Control/MountController:1.0",
                                   "allocate", code, 0)
        code  = "LOGGER.logInfo('getAllocatedAntennaName called')\n"
        code += "antName = getGlobalData(SELF.getName()+':allocAntenna')\n"
        code += "if antName is None:\n"
        code += "    antName = 'None'\n"
        code += "return antName"
        self.simulator.setMethodIF("IDL:alma/Control/MountController:1.0",
                                   "getAllocatedAntennaName", code, 0)

    def tearDown(self):
        n = self.simulator._get_name()
        self.client.releaseComponent(self.simulator._get_name())
        self.client.releaseComponent('SFI_TESTER')

    def testSetTarget(self):
        sfioff = self.tester.getSFIOffshoot()
        sfi = SFIObservingMode(sfioff, self.logger)
        source = EquatorialSource('1:2:3', '-85.6.7')
        sspec = SpectralSpec()
        bs = BaseBandSpecification()
        bs.weighting = 100.0
        bs.baseBandName = 'BB_1'
        bs.centerFrequency = Frequency(100e9)
        sspec.addBaseband(bs)
        sspec.FrequencySetup.receiverBand  = "ALMA_RB_03"
        #print sspec.toxml()
        #print sspec.getXMLString()
        sfi.setTarget(source, sspec)
        
    def testModifySpectralSpec(self):
        """If the Correlator Configuration is modified, then the CCL.SpectralSpec
        object is flagged dirty, and the Correlator Configuration needs to be
        loaded again in CORR.OBSERVATION_CONTROL.
        """
        
        code  = """LOGGER.logInfo('configureSubScanSequence called')
subscanConfigs = parameters[1]
spectralSpecXML = subscanConfigs[0].spectralSpecXML
setGlobalData('spectralSpecXML', spectralSpecXML)
return [1]"""
        self.simulator.setMethod("CORR/OBSERVATION_CONTROL", "configureSubScanSequence",
                code, 0);
 
        # Set Target using a SpectralSpec with default values.
        sfioff = self.tester.getSFIOffshoot()
        sfi = SFIObservingMode(sfioff, self.logger)
        source = EquatorialSource('1:2:3', '-85.6.7')
        spec = SpectralSpec()
        specxmlobj = XmlObject(spec._SpectralSpec__xml)
        # Check initial values
        self.assertEquals(-1, specxmlobj.SubscanSpectralSpec.CorrelatorConfigurationID.getValue())
        self.assertFalse(specxmlobj.SubscanSpectralSpec.CorrelatorConfigurationModified.getValue())
        
        config = BLCorrelatorConfig()
        spec.setCorrelatorConfig(config)
        spec.FrequencySetup.receiverBand  = "ALMA_RB_03"
        bs = BaseBandSpecification()
        bs.weighting = 100.0
        bs.baseBandName = 'BB_3'
        bs.centerFrequency = Frequency(95e9)
        spec.FrequencySetup.BaseBandSpecification.append(bs)

        sfi.setTarget(source, spec)
        
        # setTarget should have updated the CorrelatorConfigurationID field
        specxmlobj = XmlObject(spec._SpectralSpec__xml)
        self.assertEquals(1, specxmlobj.SubscanSpectralSpec.CorrelatorConfigurationID.getValue())
        # the SpectralSpec is dirty now, because of the setCorrelatorConfig call
        self.assertTrue(specxmlobj.SubscanSpectralSpec.CorrelatorConfigurationModified.getValue())
        
        self.simulator.removeMethod("CORR/OBSERVATION_CONTROL", "configureSubScanSequence")
        self.simulator.removeGlobalData('spectralSpecXML')

        code  = """LOGGER.logInfo('configureSubScanSequence called again')
subscanConfigs = parameters[1]
spectralSpecXML = subscanConfigs[0].spectralSpecXML
setGlobalData('spectralSpecXML', spectralSpecXML)
return [2]"""
        self.simulator.setMethod("CORR/OBSERVATION_CONTROL", "configureSubScanSequence",
                code, 0);
        code = """LOGGER.logInfo('clearConfigurationIds called')
corrConfigs = parameters[0]
setGlobalData('deletedCorrConfigID', corrConfigs[0])
None"""
        self.simulator.setMethod("CORR/OBSERVATION_CONTROL", "clearConfigurationIds",
                code, 0);

        # Modify the Correlator Configuration and set the target again.
        config = spec.getCorrelatorConfig()
        self.assertNotEqual(None, config)
        config.setIntegrationDuration(20.16)
        spec.setCorrelatorConfig(config)
        specxmlobj = XmlObject(spec._SpectralSpec__xml)
        self.assertTrue(
            specxmlobj.SubscanSpectralSpec.CorrelatorConfigurationModified.
                getValue())
        
        sfi.setTarget(source, spec)
        
        # CorrelatorConfigurationID should be 2 now
        specxmlobj = XmlObject(spec._SpectralSpec__xml)
        self.assertEquals(2, specxmlobj.SubscanSpectralSpec.CorrelatorConfigurationID.getValue())
        
        # Check that the old corr conf ID was cleared in CORR/OBSERVATION_CONTROL
        self.assertEquals('1', self.simulator.getGlobalData('deletedCorrConfigID'))

        self.simulator.removeMethod("CORR/OBSERVATION_CONTROL", "configureSubScanSequence")
        self.simulator.removeGlobalData('spectralSpecXML')
        self.simulator.removeMethod("CORR/OBSERVATION_CONTROL", "clearConfigurationIds")
        self.simulator.removeGlobalData('deletedCorrConfigID')

    def testSecondaryArrayMountController(self):
        sfioff = self.tester.getSFIOffshoot()
        sfi = SFIObservingMode(sfioff, self.logger)
        secArrayMountCtlr = sfi.createSecondaryArrayMountController(['ALMA01'])
        self.assertEquals("CONTROL/ALMA01/MountController",
                          secArrayMountCtlr._ArrayMountController__amc.getMountController('ALMA01'))
        priArrayMountCtlr = sfi.getArrayMountController()
        self.assertEquals("CONTROL/ALMA02/MountController",
                          priArrayMountCtlr._ArrayMountController__amc.getMountController('ALMA02'))
        try:
            priArrayMountCtlr._ArrayMountController__amc.getMountController('ALMA01')
        except ControlExceptions.IllegalParameterErrorEx, ex:
            # fine, this exception is expected
            return
        self.fail("IllegalParameterErrorEx was not thrown")

    def testReloadSingleFieldInterferometryOffshoot(self):
        sfioff = self.tester.getSFIOffshoot()
        self.tester.deactivateSFIOffshoot()
        sfioff = self.tester.getSFIOffshoot()
        self.tester.deactivateSFIOffshoot()    

def suite():
    suite = unittest.TestSuite()
    suite.addTest(SFIObsModeInContainerTest("testSetTarget"))
    # suite.addTest(SFIObsModeInContainerTest("testModifySpectralSpec"))
    # suite.addTest(SFIObsModeInContainerTest("testSecondaryArrayMountController"))
    # suite.addTest(SFIObsModeInContainerTest("testReloadSingleFieldInterferometryOffshoot"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')
