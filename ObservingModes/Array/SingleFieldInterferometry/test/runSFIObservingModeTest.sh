#!/bin/bash
#******************************************************************************
# @(#) $Id$
#
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2008 - 2009
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
#******************************************************************************

unset DISPLAY
export CLASSPATH=\
../lib/SingleFieldInterferometryTester.jar:\
../lib/ACScomponents/SingleFieldInterferometryTest.jar:\
../lib/ACScomponents/SingleFieldInterferometryObservingModeImpl.jar:\
$CLASSPATH

# Components or base classes for components used in testing
export CLASSPATH=\
${INTROOT}/lib/ACScomponents/ObservingModeBaseImpl.jar:\
${INTROOT}/lib/ACScomponents/ArrayMountControllerImpl.jar:\
${INTROOT}/lib/ACScomponents/InterferometryControllerImpl.jar:\
${INTROOT}/lib/ACScomponents/LocalOscillatorImpl.jar:\
${INTROOT}/lib/ACScomponents/ObservingModeTesterImpl.jar:\
${INTROOT}/lib/ACScomponents/SquareLawControllerImpl.jar:\
$CLASSPATH


declare TEST_CLASS=alma.Control.ObservingModes.SFIObservingModeTest
declare TEST_SUITE=1
declare TEST_LOG=/dev/stdout

if test $# -gt 0; then
  TEST_SUITE=$1
  if test $# -gt 1; then
    TEST_LOG=$2
  fi
fi

acsStartJava -Dsuite="$TEST_SUITE" -endorsed junit.textui.TestRunner "$TEST_CLASS" &> "$TEST_LOG"

RESULT=$?
if [ "$RESULT" = "0" ]; then
    printf "OK\n"
else
    printf "ERROR\n"
fi
