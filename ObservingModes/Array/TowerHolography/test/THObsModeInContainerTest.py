#! /usr/bin/env python
#*******************************************************************************
# ALMA - Atacama Large Millimiter Array
# (c) Associated Universities Inc., 2005 
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# "@(#) $Id$"
#

from Acspy.Clients.SimpleClient import PySimpleClient
from Acspy.Util.XmlObjectifier import XmlObject
from CCL.TowerHolographyObservingMode import TowerHolographyObservingMode
from CCL.logging import getLogger
from CCL.ScanIntent import ScanIntent
#from CCL.SubScanIntent import SubScanIntent
import Control
import ObservingModeTester_idl
import SubscanIntentMod
import unittest

class THObsModeInContainerTest(unittest.TestCase):
    
    def setUp(self):
        self.logger = getLogger()
        self.client = PySimpleClient()
        try:
            # Insert methods into python simulators
            ObsModetesterCompName = "CONTROL/ObservingModeTester"
            self.testutil  = self.client.getComponent(ObsModetesterCompName)
            if self.testutil == None:
                m = "Could not get '"+ObsModetesterCompName+"' component!!"
                print m
                self.logger.severe(m)
                raise Exception, m
            simidl =  "IDL:alma/ACSSim/Simulator:1.0"
            self.simulator = self.client.getDefaultComponent(simidl)
            self.testutil.modifySimulationCode(self.simulator)
        except Exception, e:
            m = "Exception modifying simulator code:\n" + str(e)
            print m
            self.logger.info("Exception:" + str(e))
            raise Exception, m
        self.tester = self.client.getComponent('TH_TESTER')
        
    def tearDown(self):
        self.client.releaseComponent(self.simulator._get_name())
        self.client.releaseComponent('TH_TESTER')

    def testTowerHolographyObservingMode(self):
        code = """LOGGER.logInfo('getTowerXYZPosition called')
(0.0, 0.0, 0.0)"""
        self.simulator.setMethodIF("IDL:alma/Control/TowerHolography:1.0",
                                   "getTowerXYZPosition",
                                   code,
                                   0)
        th = TowerHolographyObservingMode(self.tester.getTHOffshoot(), 
                self.logger)
        thMC = th.getTowerHolographyController()
        scanIntent = ScanIntent("MAP_ANTENNA_SURFACE")
        th.beginScan(scanIntent, "HolographyScan", 1)
        th.beginSubscan([SubscanIntentMod.REFERENCE])
        calTime = 1000
        calTimeout = th.startPhaseCal(duration=calTime)
        subScanData = thMC.getSubscanData(timeout=1.2*calTimeout)
        subScanData.subscanName = "Phase Cal: 1"
        th.sendSubscanData(subScanData)
        th.endSubscan()        
        th.endScan()
        
def suite():
    suite = unittest.TestSuite()
    suite.addTest(THObsModeInContainerTest("testTowerHolographyObservingMode"))
    return suite

if __name__ == '__main__':
    unittest.main(defaultTest='suite')
