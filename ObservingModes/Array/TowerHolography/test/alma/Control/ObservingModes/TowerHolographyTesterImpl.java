/* ALMA - Atacama Large Millimiter Array
 * (c) Associated Universities Inc., 2006 
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

package alma.Control.ObservingModes;

import java.util.logging.Logger;

import alma.ACS.ComponentStates;
import alma.Control.AntennaCharacteristics;
import alma.Control.ResourceId;
import alma.Control.TowerHolographyObservingMode;
import alma.Control.TowerHolographyObservingModeHelper;
import alma.Control.TowerHolographyObservingModePOATie;
import alma.Control.TowerHolographyObservingModeTesterOperations;
import alma.Control.Common.DynamicResource;
import alma.Control.Common.PublisherResource;
import alma.Control.Common.Resource;
import alma.Control.Common.ResourceManager;
import alma.JavaContainerError.wrappers.AcsJContainerServicesEx;
import alma.ObservingModeExceptions.ObsModeInitErrorEx;
import alma.ObservingModeExceptions.wrappers.AcsJObsModeInitErrorEx;
import alma.ResourceExceptions.wrappers.AcsJResourceErrorEx;
import alma.acs.component.ComponentLifecycle;
import alma.acs.container.ContainerServices;
import alma.acs.nc.SimpleSupplier;
import alma.asdmIDLTypes.IDLEntityRef;
import alma.asdmIDLTypes.IDLLength;
import alma.offline.DataCapturer;

public class TowerHolographyTesterImpl implements ComponentLifecycle, 
        TowerHolographyObservingModeTesterOperations {

    private ContainerServices container;
    private Logger logger;    
    
    private int corrConfigID = 0;
    private int scan = 0;
    private int subScan = 0;
    
    private TowerHolographyObservingModeImpl thObsMode;
    private TowerHolographyObservingModePOATie thObsModeTie;
    
    // -----------------------------------------------------------
    // Implementation of ComponentLifecycle
    // -----------------------------------------------------------
    @Override
    public void initialize(ContainerServices containerServices) {
        container = containerServices;
        logger = containerServices.getLogger();
        logger.info("initialize() called...");
        
        Resource<SimpleSupplier> pubres = 
            new PublisherResource(container, "CONTROL_SYSTEM", true);
        ResourceManager.getInstance(container).addResource(pubres);
        Resource<DataCapturer> dcres =
            new DynamicResource(container, "Array001/DATACAPTURER", 
                "IDL:alma/offline/DataCapturer:1.0");        
        try {
            ResourceManager.getInstance(container).acquireResources();
        } catch (AcsJResourceErrorEx ex) {
            String msg = "Error when acquiring a critical resource: ";
            logger.severe(msg);
        }
    }

    @Override
    public void execute() {
        logger.info("execute() called...");
    }

    public void cleanUp() {
        logger.info("cleanUp() called");
        if (thObsMode != null) {
            logger.fine("Deactivating SFI Offshoot.");            
            thObsMode.cleanUp();
            if (thObsModeTie != null) {
                try {
                    container.deactivateOffShoot(thObsModeTie);
                } catch (AcsJContainerServicesEx ex) {
                    String msg = "Error deactivating SFI Offshoot.";
                    logger.fine(msg);
                    ex.log(logger);
                }
                thObsModeTie = null;
            }
            thObsMode = null;
        }
        ResourceManager.getInstance(container).releaseResources();
    }

    @Override
    public void aboutToAbort() {
        cleanUp();
        logger.info("managed to abort...");
        System.out.println("Foo component managed to abort...");
    }

    // -----------------------------------------------------------
    // Implementation of ACSComponent
    // -----------------------------------------------------------

    @Override
    public ComponentStates componentState() {
        return container.getComponentStateManager().getCurrentState();
    }

    @Override
    public String name() {
        return container.getName();
    }


	        
    // -----------------------------------------------------------
    // Implementation of TowerHolographyObservingModeTester
    // -----------------------------------------------------------

    public TowerHolographyObservingMode getTHOffshoot() throws ObsModeInitErrorEx {
        String[] antNames = {"ALMA01"};
        try {
            ObservingModeArray array = 
                ObservingModeTesterImpl.getFakeArray(antNames);
            thObsMode = new TowerHolographyObservingModeImpl(container, array);
            thObsModeTie = new TowerHolographyObservingModePOATie(thObsMode);
            TowerHolographyObservingMode sfiOffshoot = 
                TowerHolographyObservingModeHelper.narrow(
                    container.activateOffShoot(thObsModeTie));
            return sfiOffshoot;
        } catch (ObsModeInitErrorEx ex) {
            logger.fine("Error creating Optical Pointing Offshot: " + ex.toString());
            AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
            throw exhlp.toObsModeInitErrorEx();
        } catch (AcsJContainerServicesEx ex) {
            logger.fine("Error creating Optical Pointing Offshot: " + ex.toString());
            AcsJObsModeInitErrorEx exhlp = new AcsJObsModeInitErrorEx(ex);
            throw exhlp.toObsModeInitErrorEx();
        }        
    }
}
