s/[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][ T][0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9]\{1,3\}/----------T--:--:--.---/g
s/tests in [0-9]*.[0-9]*s/tests ran in -.---s/g
s/test in [0-9]*.[0-9]*s/tests ran in -.---s/g
s/Time: .*/Time: <test exec time>/
s/Process = .*/Process = <pid>/
s/Thread = .*/Thread = <thread>/
s/Line Number = .*/Line Number = <line-number>/
s/Host = .*/Host = <host>/
/^.*Client disconnect.*$/d
